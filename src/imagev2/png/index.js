import bgForgot from './bg-forgot.png';
import bgLogin from './bg-login.png';
import bgProductV2 from './bg-product-v2.png';
import bgProduct from './bg-product.png';
import bgSignUp from './bg-sign-up.png';
import bottleLogin from './bottle-login.png';
import box from './box.png';
import btAdd from './bt-add.png';
import discoveryDetail from './discovery-detail.png';
import dualCrayon from './dual-crayon.png';
import homeScent from './home-scent.png';
import logoFerrariMaison21g from './logo-ferrari-maison21g.png';
import logo from './logo.png';
import miniCandle from './mini-candle.png';
import oilBurnerPng from './oil-burner.png';
import waxCreation from './wax-creation.png';
import outStock from './out-stock.png';
import single3ml from './single-3ml.png';
import giftWrapping from './gift-wrapping.png';

export {
  bgProduct,
  bgProductV2,
  box,
  bgLogin,
  bottleLogin,
  bgSignUp,
  bgForgot,
  discoveryDetail,
  waxCreation,
  homeScent,
  miniCandle,
  btAdd,
  oilBurnerPng,
  logo,
  dualCrayon,
  logoFerrariMaison21g,
  outStock,
  single3ml,
  giftWrapping,
};
