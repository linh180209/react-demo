import icDown from '../../image/icon/ic-down.svg';
import amexCo from './amex-co.svg';
import arrowFerrari from './arrow-down-ferrari.svg';
import bottleCheckOut from './bottle-check-out.svg';
import bottleCustome from './bottle-custome.svg';
import icNextFerrari from './bt-next-ferrari.svg';
import candle1 from './candle-1.svg';
import candle2 from './candle-2.svg';
import candleColor from './candle-color.svg';
import candleHolder from './candle-holder.svg';
import icCarDiffUser from './car-diffuser-icon.svg';
import cartEmpty from './cart-empty.svg';
import cottonPouch from './cottonPouch.svg';
import dola from './dola.svg';
import ecoBox from './ecoBox.svg';
import emptyScent from './empty-scent.svg';
import facebook from './facebook.svg';
import giftNotBgSuccess from './gift-not-bg-success.svg';
import giftNotBg from './gift-not-bg.svg';
import icQuote from './gift-quote.svg';
import gift from './gift.svg';
import google from './google.svg';
import groupBottle from './group-bottle.svg';
import home from './home.svg';
import icBalance from './ic-balance.svg';
import icBlackVideo from './ic-black-video.svg';
import icBottleCustom from './ic-bottle-custome.svg';
import icBottle from './ic-bottle-outline.svg';
import icCandleOne from './ic-candle-1.svg';
import icCandleTwo from './ic-candle-2.svg';
import icCandle from './ic-candle.svg';
import icCitrus from './ic-citrus.svg';
import icScentFamily from './ic-dots.svg';
import icDuration from './ic-duration.svg';
import icFresh from './ic-fresh.svg';
import icGiftCollections from './ic-gift-collections.svg';
import icScentHeart from './ic-heart.svg';
import icMiniCandleHolder from './ic-holder-mini.svg';
import icOriental from './ic-oriental.svg';
import icPerfumeDiy from './ic-perfume-diy.svg';
import icScentNote from './ic-rank-energy.svg';
import icRibbon from './ic-ribbon.svg';
import icSeductionActive from './ic-seduction-active.svg';
import icSeduction from './ic-seduction.svg';
import icSpeedo from './ic-speedo.svg';
import icSpicy from './ic-spicy.svg';
import icScentMood from './ic-sun.svg';
import icTitlePerfumeDiy from './ic-title-perfumeDiy.svg';
import icVideo from './ic-video.svg';
import icWorkActive from './ic-work-active.svg';
import icWork from './ic-work.svg';
import iconBottle from './icon-bottle.svg';
import icBurner from './icon-burner.svg';
import icComment from './icon-comment.svg';
import icDot from './icon-dot.svg';
import logFerrariBlack from './icon-ferrari-black.svg';
import icGiftBg from './icon-gift-bg.svg';
import icGift from './icon-gift.svg';
import iconLeft from './icon-left.svg';
import icMiniCandle1 from './icon-mini-candle-1.svg';
import icMiniCandle2 from './icon-mini-candle-2.svg';
import icMiniCandle3 from './icon-mini-candle-3.svg';
import icMiniCandle4 from './icon-mini-candle-4.svg';
import icNotes from './icon-notes.svg';
import icSuccess from './icon-success.svg';
import logoBlack from './logo-black.svg';
import icLogoFerrariRed from './logo-ferrari-red.svg';
import logoGoldText from './logo-gold-text.svg';
import logoGold from './logo-gold.svg';
import logoWhite from './logo-white.svg';
import mcCo from './mc-co.svg';
import mc from './mc.svg';
import icOil1 from './oil-1.svg';
import icOil2 from './oil-2.svg';
import oilBurner from './oil-burner.svg';
import paypalBlack from './paypal-black.svg';
import paypalCo from './paypal-co.svg';
import paypal from './paypal.svg';
import perfumeCustom from './perfume-custom.svg';
import perfumeStep1 from './perfume-step-1.svg';
import perfumeStep2 from './perfume-step-2.svg';
import perfume from './perfume.svg';
import productSelect from './product-select.svg';
import product from './product.svg';
import promo from './promo.svg';
import icReed from './reed-icon.svg';
import rewards from './rewards.svg';
import startGrayV2 from './start-gray-v2.svg';
import startv2 from './startv2.svg';
import startOutline from './star_outline.svg';
import visaCo from './visa-co.svg';
import visa from './visa.svg';
import starOutline from './star-icon.svg';

export {
  rewards,
  product,
  perfume,
  home,
  promo,
  logoBlack,
  dola,
  paypal,
  mc,
  visa,
  gift,
  giftNotBg,
  giftNotBgSuccess,
  cartEmpty,
  google,
  facebook,
  paypalBlack,
  ecoBox,
  cottonPouch,
  visaCo,
  mcCo,
  amexCo,
  paypalCo,
  bottleCheckOut,
  bottleCustome,
  productSelect,
  startv2,
  startGrayV2,
  groupBottle,
  emptyScent,
  icBottleCustom,
  icSpicy,
  icCitrus,
  icFresh,
  icOriental,
  oilBurner,
  iconBottle,
  icCandle,
  icCandleOne,
  icCandleTwo,
  icSuccess,
  logoGold,
  iconLeft,
  startOutline,
  icWorkActive,
  icWork,
  icSeduction,
  icSeductionActive,
  perfumeStep1,
  perfumeStep2,
  perfumeCustom,
  candle1,
  candle2,
  candleHolder,
  candleColor,
  icBurner,
  icOil1,
  icOil2,
  icReed,
  icCarDiffUser,
  icComment,
  icMiniCandle1,
  icMiniCandle2,
  icMiniCandle3,
  icMiniCandle4,
  icMiniCandleHolder,
  icRibbon,
  icQuote,
  icVideo,
  icBlackVideo,
  icDuration,
  icBottle,
  icDown,
  arrowFerrari,
  logoWhite,
  icNextFerrari,
  icBalance,
  icSpeedo,
  logFerrariBlack,
  icLogoFerrariRed,
  icDot,
  icPerfumeDiy,
  icTitlePerfumeDiy,
  logoGoldText,
  icNotes,
  icGift,
  icGiftBg,
  icScentNote,
  icScentMood,
  icScentHeart,
  icScentFamily,
  icGiftCollections,
  starOutline,
};

