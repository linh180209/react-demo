*CMS*
```
{
  letsGuide: {
    startPage: {
      description: @StyleText,
      button: @StyleButton,
    },
    textQuestion: {
      question: @StyleText,
      answer: @StyleText,
      button: @StyleButton
    },
    skinQuestion: {
      question: @StyleText,
      colors: [], // 10 element
    },
    yesNoQuestion: {
      question: @StyleText,
      answer: @StyleText,
    },
    outcomePersonal: {
      personality: @StyleText,
      descriptionP: @StyleText,
      title: @StyleText,
      descriptionProduct: @StyleText,
      product: @StyleText,
      button: @StyleButton,
    },
    benefit: {
      question: @StyleText,
      anwser: @StyleText,
    },
    outcome: {
      description: @StyleText,
      benefit: @StyleText,
      button: @StyleButton,
    }
  }
}
```

*StyleButton*
```
{
  width: String,
  height: String,
  backgroundColor: String,
  fontSize: String,
  fontFamily: String,
  fontStyle: String,
  color: String,
  border: String,
  borderRadius: String,
}
```

*StyleText*
```
{
  fontSize: String,
  fontFamily: String,
  fontStyle: String,
  color: String,
}
```