import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import _, { isEmpty } from 'lodash';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import VideoAcademy from '../../components/AcademyItem/videoAcademy';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import LinkCT from '../../componentsv2/linkCT';
import ScentCharacterV2 from '../../componentsv2/scentCharacterV2';
import { GET_BASKET_URL, GET_PRODUCT_FOR_CART, GUEST_CREATE_BASKET_URL, SUBSCRIBER_NEWLETTER_URL } from '../../config';
import { loadAllBasket } from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import { loginUpdateUserInfo } from '../../Redux/Actions/login';
import {
  fetchCMSHomepage, getLink, getLinkFromButtonBlock, getNameFromButtonBlock, trackGTMSubmitEmail, validateEmail
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { fetchScentDetail } from '../../Redux/Helpers/fetchAPI';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import { useMergeState } from '../../Utils/customHooks';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';
import FooterV2 from '../footer';
import Breadcrumb from './components/breadcrumb';
import ExploreMoreScents from './components/exploreMoreScents';
import NaturalProperties from './components/naturalProperties';
import RecommendedMix from './components/recommendedMix';
import ScentDescription from './components/scentDescription';
import Subscribe from './components/subscribe';
import './styles.scss';

const getCMS = (data) => {
  if (data) {
    const { body } = data;
    const buttonBlocks = _.filter(body, x => x?.type === 'button_block');
    const menuBlocks = _.find(body, x => x?.type === 'menu_block');
    const iconBlocks = _.find(body, x => x?.type === 'icons_block');
    const imageBlock = _.find(body, x => x?.type === 'image_block');

    return {
      buttonBlocks,
      menuBlocks,
      iconBlocks,
      imageBlock,
    };
  }
  return {};
};

function GlossaryV2() {
  const dispatch = useDispatch();
  const history = useHistory();

  const cms = useSelector(state => state.cms);
  const basket = useSelector(state => state.basket);
  const login = useSelector(state => state.login);
  const { id } = useParams();

  const [dataState, setDataState] = useMergeState({
    buttonBlocks: [],
    menuBlocks: {},
    breadcrumb: [],
  });
  const [scentDetail, setScentDetail] = useState();
  const [subscribe, setSubscribe] = useState({
    email: '',
    policyChecked: false,
  });

  const handleGetDataInit = async () => {
    const ingredientBlock = _.find(cms, x => x.title === 'Ingredient Detail');

    if (isEmpty(ingredientBlock)) {
      try {
        const results = await fetchCMSHomepage('ingredient-detail');
        dispatch(addCmsRedux(results));

        const cmsState = getCMS(results);
        setDataState(cmsState);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const cmsState = getCMS(ingredientBlock);
      setDataState(cmsState);
    }
  };

  const handleGetScentDetail = async () => {
    dispatch(loadingPage(true));

    try {
      const detail = await fetchScentDetail(id);

      if (!isEmpty(detail)) {
        dispatch(loadingPage(false));
        setScentDetail(detail);
      }
    } catch (error) {
      dispatch(loadingPage(false));
      toastrError(error.message);
    }
  };

  const handleSubscribeChange = (e, name) => {
    setSubscribe(prevState => ({ ...prevState, [name]: name === 'email' ? e.target.value : e.target.checked }));
  };

  const assignEmailToCart = (email) => {
    if (!basket.id) {
      const options = {
        url: GUEST_CREATE_BASKET_URL,
        method: 'POST',
        body: {
          subtotal: 0,
          total: 0,
          email,
        },
      };
      fetchClient(options).then((result) => {
        if (result && !result.isError) {
          dispatch(loadAllBasket(result));
        }
      });
      return;
    }
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basket.id),
      method: 'PUT',
      body: {
        email,
      },
    };
    fetchClient(option);
  };

  const handleSubscribeClick = () => {
    // Validate subscribe form
    if (!subscribe.email) {
      toastrError('Please enter your email address');
      return;
    }
    if (!validateEmail(subscribe.email)) {
      toastrError('Email is not avaiable');
      return;
    }


    if (!subscribe.policyChecked) {
      toastrError('Please read accept all conditions');
      return;
    }

    assignEmailToCart(subscribe.email);

    const options = {
      url: SUBSCRIBER_NEWLETTER_URL,
      method: 'POST',
      body: {
        email: subscribe.email,
        user: login && login.user ? login.user.id : undefined,
      },
    };

    fetchClient(options).then((result) => {
      if (result.isError) {
        if (result.email) {
          toastrError('You already have registered');
        } else {
          toastrError('You have already subscribed to our newsletter');
        }
        return;
      }
      auth.setEmail(subscribe.email);
      trackGTMSubmitEmail(subscribe.email);
      toastrSuccess('Email sent successfully');
      if (options.body.user) {
        dispatch(loginUpdateUserInfo({ is_get_newsletters: true }));
      }
    }).catch((err) => {
      toastrError(err);
    });
  };

  const handleCreateScentMixClick = async () => {
    try {
      dispatch(loadingPage(true));
      const product = await getProduct(id);

      if(!isEmpty(product)) {
        dispatch(loadingPage(false));

        if(product?.id) {
          history.push(`/${auth.getCountry()}/product/Perfume/${product.id}`);
        }
      }
    } catch (error) {
      toastrError(error.message);
      dispatch(loadingPage(false));
    }
  }

  const getProduct = async (scentId) => {
    const url = `${GET_PRODUCT_FOR_CART}?combo=${scentId}&type=perfume`;
    const option = {
      url,
      method: 'GET',
    };
    return fetchClient(option);
  }

  useEffect(() => {
    handleGetDataInit();
  }, []);

  useEffect(() => {
    if (id) {
      handleGetScentDetail();

      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  }, [id]);

  // Format breadcrumb
  useEffect(() => {
    if (!isEmpty(dataState.menuBlocks) && scentDetail?.id) {
      const countryCode = auth.getCountry();

      let breadcrumb = [];
      const home = {
        name: dataState.menuBlocks?.value?.text,
        url: dataState.menuBlocks?.value?.link ? replaceCountryInMetaTags(countryCode, dataState.menuBlocks?.value?.link) : '',
      };
      breadcrumb.push(home);

      const submenu = dataState.menuBlocks?.value?.submenu?.length > 0 ? _.map(dataState.menuBlocks?.value?.submenu, x => ({
        name: x?.value?.text,
        url: x?.value?.link ? replaceCountryInMetaTags(countryCode, x?.value?.link) : '',
      })) : [];

      const current = {
        name: scentDetail?.name,
        url: '',
      };
      breadcrumb = [...breadcrumb, ...submenu];
      breadcrumb.push(current);

      setDataState({ breadcrumb });
    }
  }, [dataState.menuBlocks, scentDetail]);

  const naturalIngredientsTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'natural_ingredients');
  const recommendedMixTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'recommended_mix');
  const recommendedMixDes = getLinkFromButtonBlock(dataState.buttonBlocks, 'recommended_mix');
  const exploreMoreScentsTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'explore_more_scents');
  const exploreMoreScentDes = getLinkFromButtonBlock(dataState.buttonBlocks, 'explore_more_scents');
  const viewAllScentsBtn = getNameFromButtonBlock(dataState.buttonBlocks, 'view_all_scents_btn');
  const viewAllScentsUrl = getLinkFromButtonBlock(dataState.buttonBlocks, 'view_all_scents_btn');

  return (
    <div className="glossary-v2">
      <HeaderHomePageV3 />

      <div className="glossary-wrapper">
        <Breadcrumb menus={dataState.breadcrumb} />
        <div className="glossary-detail padding-y">
          <div className="block-video">
            {scentDetail?.videos?.length > 0 && (
              <VideoAcademy
                isChangeFullScreen
                url={scentDetail?.videos[0]?.video || undefined}
                poster={scentDetail?.videos[0]?.placeholder || undefined}
                isControl
                autoPlay={false}
              />
            )}
          </div>
          <div className="block-description">
            <ScentDescription
              scentDetail={scentDetail}
              buttonBlocks={dataState.buttonBlocks}
            />
          </div>
          <div className="block-charts">
            <ScentCharacterV2
              data={scentDetail}
              isScentDetail
            />
          </div>
        </div>

        <div className="glossary-ingredients">
          <NaturalProperties
            iconBlocks={dataState.iconBlocks}
          />
        </div>

        <div className="glossary-natural padding-y">
          <div className="glossary-head">
            <div className="glossary-title">{naturalIngredientsTitle}</div>
          </div>

          <div className="block-video">
            {scentDetail?.videos?.length > 0 && (
              <VideoAcademy
                isChangeFullScreen
                url={scentDetail?.videos[0]?.video || undefined}
                poster={scentDetail?.videos[0]?.placeholder || undefined}
                isControl
                autoPlay={false}
              />
            )}
          </div>
        </div>

        <div className="glossary-recommended padding-y">
          <div className="glossary-head">
            <div className="glossary-title">{recommendedMixTitle}</div>
            <div className="glossary-sub-title">
              {recommendedMixDes}
              &nbsp;
              {scentDetail?.name}
              :
            </div>
          </div>
          {scentDetail?.suggestions?.length > 0 && (
            <RecommendedMix
              dataMix={scentDetail?.suggestions}
              onCreateScentMix={handleCreateScentMixClick}
            />
          )}
        </div>

        <div className="glossary-subscribe">
          <Subscribe
            buttonBlocks={dataState.buttonBlocks}
            imageBlock={dataState.imageBlock}
            subscribe={subscribe}
            onSubscribeChange={handleSubscribeChange}
            onSubscribeClick={handleSubscribeClick}
          />
        </div>

        <div className="glossary-explore padding-y">
          <div className="glossary-block">
            <div className="glossary-head">
              <div className="glossary-title">{exploreMoreScentsTitle}</div>
              <div className="glossary-sub-title">{exploreMoreScentDes}</div>
            </div>

            <LinkCT className="glossary-more-scents" to={getLink(viewAllScentsUrl)}>
              <div className="view-btn">
                {viewAllScentsBtn}
                <ArrowForwardIcon />
              </div>
            </LinkCT>
          </div>

          {scentDetail?.suggestions?.length > 0 && (
            <ExploreMoreScents moreScents={scentDetail?.family_scents} />
          )}

          <div className="glossary-block second-block">
            <LinkCT className="glossary-more-scents second-btn" to={getLink(viewAllScentsUrl)}>
              <div className="view-btn">
                {viewAllScentsBtn}
                <ArrowForwardIcon />
              </div>
            </LinkCT>
          </div>
        </div>
      </div>

      <FooterV2 />
    </div>
  );
}

export default GlossaryV2;
