import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { icScentNote, icScentHeart, icScentMood, icScentFamily } from '../../../../imagev2/svg';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import './styles.scss';

function ScentDescription({ scentDetail, buttonBlocks }) {
  const [NOTES, SCENT_FAMILY, MOOD] = ['scent_note', 'scent_family', 'mood'];
  const listTags = ['scent_note', 'scent_family', 'mood'];
  const [tags, setTags] = useState([]);

  const scentHistory = getNameFromButtonBlock(buttonBlocks, 'scent_history');
  const scentBenefit = getNameFromButtonBlock(buttonBlocks, 'scent_benefits_for_your_mind_body_soul');

  useEffect(() => {
    if(scentDetail?.tags?.length > 0) {
      const initTags = [];
      _.forEach(scentDetail.tags, tag => {
        let existing = _.filter(initTags, x => x?.group_key === tag?.group_key);

        if(existing?.length > 0) {
          const existingIdx = _.indexOf(initTags, existing[0]);
          initTags[existingIdx].name = initTags[existingIdx].name + ' & ' + tag.name;
        }else {
          if (typeof tag.name === 'string') {
            initTags.push({...tag, name: tag.name});
          }
        }
      });

      setTags(initTags);
    }
  }, [scentDetail?.tags]);

  const handleRenderIcon = (tagName) => {
    switch (tagName) {
      case NOTES:
        return icScentNote;

      case SCENT_FAMILY:
        return icScentFamily;

      case MOOD:
        return icScentMood;

      default: return <div />
    }
  };

  return (
    <div className="scent-description">
      <div className="scent-block">
        <div className="block-item block-title">
          <div className="div-top">
            <div className="scent-title text-ct">{scentDetail?.family}</div>
            <div className="scent-name text-ct">{scentDetail?.name}</div>
            <ul className="scent-properties">
              {_.map(scentDetail?.ingredient?.scents || [], x => <li>{x}</li>)}
            </ul>
          </div>
          <div className="div-bottom">
            <div className="block-custom">
              <div className="block-icon">
                <img loading='lazy' src={icScentHeart} alt="icon" />
              </div>
              <div className="block-text">{scentBenefit}</div>
            </div>

            <ul className="benefits">
              {_.map(scentDetail?.ingredient?.mind_enhancements || [], x => (
                <li>
                  <div className="dot" />
                  {x}
                </li>
              ))}
            </ul>
          </div>
        </div>

        <div className="block-item block-content">
          <div className="div-title">{scentHistory}</div>
          <div className="div-description">
            <div className="block-top">
              {ReactHtmlParser(scentDetail?.description)}
            </div>
            <div className="divider" />
            <div className="block-middle">
              <ul className="block-bottom">
                {_.map(tags, tag => (
                  listTags.includes(tag?.group_key) && (
                    <li key={tag?.group_key}>
                      <div className="block-custom">
                        <div className="block-icon">
                          <img src={handleRenderIcon(tag?.group_key)} alt="icon" />
                        </div>
                        <div className="block-text">
                          {tag?.group}
                        </div>
                      </div>
                      <div className="block-name">
                        {tag?.name}
                      </div>
                    </li>
                  )))
                }
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ScentDescription;
