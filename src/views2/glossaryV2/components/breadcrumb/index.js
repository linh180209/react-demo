import _ from 'lodash';
import React, { memo } from 'react';
import LinkCT from '../../../../componentsv2/linkCT';
import { getLink } from '../../../../Redux/Helpers';
import './styles.scss';

function Breadcrumb({ menus }) {
  return (
    <div className="glossary-breadcrumb">
      <div className="list">
        {_.map(menus, (x, idx) => {
          if (x.url) {
            return (
              <LinkCT key={idx} className="link-redirect" to={getLink(x.url)}>
                {x.name}
                <span className="slash">/</span>
              </LinkCT>
            );
          } return <div key={idx} className="current">{x.name}</div>;
        })}
      </div>
    </div>
  );
}

export default memo(Breadcrumb);
