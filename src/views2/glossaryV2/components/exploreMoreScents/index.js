import _ from 'lodash';
import React from 'react';
import { useHistory } from 'react-router-dom';
import auth from '../../../../Redux/Helpers/auth';
import './styles.scss';

function ExploreMoreScents({ moreScents }) {
  const history = useHistory();

  const handleClick = (id) => {
    history.push(`/${auth.getCountry()}/ingredients-glossarium/${id}`);
  }
  return (
    <ul className="explore-more-scents">
      {_.map(moreScents, x => (
        <li key={x?.id} className="scent" onClick={() => handleClick(x?.id)}>
          <div className="div-image">
            <img loading='lazy' src={x?.images?.length > 0 ? _.find(x.images, y => y?.type === 'main')?.image : ''} alt="scent" />
          </div>
          <div className="div-content">
            <div className="div-name">{x?.name}</div>
            <div className="div-family">{x?.family}</div>
          </div>
        </li>
      ))}
    </ul>
  );
}

export default ExploreMoreScents;
