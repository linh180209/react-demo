import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import _ from 'lodash';
import React from 'react';
import { useHistory } from 'react-router-dom';
import auth from '../../../../Redux/Helpers/auth';
import './styles.scss';

function RecommendedMix({ dataMix, onCreateScentMix }) {
  const history = useHistory();

  const handleClick = (id) => {
    history.push(`/${auth.getCountry()}/ingredients-glossarium/${id}`);
  }

  return (
    <div className="recommended-mix">
      <ul className="mix-scents">
        {_.map(dataMix, x => (
          <li
            key={x?.id}
            className="recommended-scent"
            // onClick={() => handleClick(x?.id)}
          >
            <div className="scent-group">{x?.group}</div>
            <div className="scent-image">
              <img loading='lazy' src={_.find(x?.images, y => y?.type === 'main')?.image} alt="scent" />
              <div className="scent-name">{x?.name}</div>
            </div>
          </li>
        ))}
      </ul>

      <button
        className="recommended-create"
        onClick={onCreateScentMix}
      >
        <div className="create-btn">
          Create Scent Mix in 30ml Perfume
          <ArrowForwardIcon />
        </div>
      </button>
    </div>
  );
}

export default RecommendedMix;
