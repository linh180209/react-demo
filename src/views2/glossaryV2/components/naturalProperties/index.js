import _ from 'lodash';
import React from 'react';
import './styles.scss';

function NaturalProperties({ iconBlocks }) {
  return (
    <div className="natural-properties">
      {iconBlocks?.value?.icons.length > 0 && (
        <ul className="properties">
          {_.map(iconBlocks?.value?.icons, x => (
            <li key={x?.id}>
              <div className="block-image">
                <img loading='lazy' src={x?.value?.image?.image} alt="natural" />
              </div>
              <div className="block-title">{x?.value?.text}</div>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
}

export default NaturalProperties;
