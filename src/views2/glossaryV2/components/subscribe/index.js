import React from 'react';
import ButtonCT from '../../../../componentsv2/buttonCT';
import CheckBoxCT from '../../../../componentsv2/checkBoxCT';
import InputCT from '../../../../componentsv2/inputCT';
import { getLinkFromButtonBlock, getNameFromButtonBlock } from '../../../../Redux/Helpers';
import './styles.scss';

function Subscribe({
  buttonBlocks, imageBlock, subscribe, onSubscribeChange, onSubscribeClick,
}) {
  const subscribeTitle = getNameFromButtonBlock(buttonBlocks, 'subscribe_to_our_newsletter');
  const subscribeDes = getLinkFromButtonBlock(buttonBlocks, 'subscribe_to_our_newsletter');
  const iHaveRead = getNameFromButtonBlock(buttonBlocks, 'i_have_read_and_accept_all');
  const termAndConditions = getNameFromButtonBlock(buttonBlocks, 'term_and_conditions');
  const and = getNameFromButtonBlock(buttonBlocks, 'and');
  const privacyPolicy = getNameFromButtonBlock(buttonBlocks, 'privacy_policy');
  const subscribeBtn = getNameFromButtonBlock(buttonBlocks, 'subscribe_btn');

  return (
    <div className="subscribe">
      <div className="subscribe-block">
        <div className="block-image">
          <img loading='lazy' src={imageBlock?.value?.image} alt="bg" />
        </div>
        <div className="block-content">
          <div className="div-group">
            <div className="div-title">{subscribeTitle}</div>
            <div className="div-description">{subscribeDes}</div>
          </div>
          <div className="div-form">
            <div className="div-field">
              <div className="text-field">
                <InputCT
                  placeholder="Email address"
                  value={subscribe.email}
                  onChange={e => onSubscribeChange(e, 'email')}
                />
              </div>

              <ButtonCT
                className="subscribe-btn"
                name={subscribeBtn}
                color="secondary"
                onClick={onSubscribeClick}
              />

              <div className="div-check-policy">
                <CheckBoxCT
                  className={`checkbox-policy ${subscribe.policyChecked ? 'checked' : ''}`}
                  checked={subscribe.policyChecked}
                  onChange={e => onSubscribeChange(e, 'policyChecked')}
                  label={(
                    <div className="div-label">
                      {iHaveRead}
                      {' '}
                      <span>{termAndConditions}</span>
                      {' '}
                      {and}
                      {' '}
                      <span>{privacyPolicy}</span>
                    </div>
                  )}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Subscribe;
