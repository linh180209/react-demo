import React, { useState, useEffect } from 'react';
import { Modal, ModalBody } from 'reactstrap';
import CloseIcon from '@mui/icons-material/Close';
import { useParams } from 'react-router-dom';
import moment from 'moment';
import classnames from 'classnames';
import _ from 'lodash';
import { useSelector, useDispatch } from 'react-redux';
import { loadAllBasketSubscription } from '../../Redux/Actions/basketSubscription';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import { icSuccess, logoBlack } from '../../imagev2/svg';
import { isBrowser, isMobile } from '../../DetectScreen';
import ButtonCT from '../../componentsv2/buttonCT';
import HeaderCT from '../../componentsv2/headerCT';
import './styles.scss';
import {
  addFontCustom,
  addOrderCartNamogoo,
  fetchCMSHomepage,
  generaCurrency, getNameFromButtonBlock, getSEOFromCms, googleAnalitycs, pageViewCheckoutSuccessAmplitude, segmentOrderCompleted, segmentSubscribedClub21G, sendOrderCompletedViewedHotjarEvent, setPrerenderReady, trackGTMEventPurchase, trackGTMPurchase,
} from '../../Redux/Helpers';
import { GET_ORDER_URL, GET_USER_ACTIONS } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import auth from '../../Redux/Helpers/auth';
import { useMergeState } from '../../Utils/customHooks';
import initialState from '../../Redux/Reducers/initialState';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import USPBlock from '../../componentsv2/uspBlock';
import FooterV2 from '../footer';

const getCms = (cms) => {
  if (!cms) {
    return { seo: {} };
  }
  const seo = getSEOFromCms(cms);
  const { body } = cms;
  const buttonBlock = _.filter(body, x => x.type === 'button_block');
  const imageBlockIcon = _.filter(body, x => x.type === 'image_block');
  const iconBlock = _.find(body, x => x.type === 'icons_block')?.value;
  return {
    seo, imageBlockIcon, buttonBlock, iconBlock,
  };
};

function PaySuccess(props) {
  const dispatch = useDispatch();
  const { orderId } = useParams();
  const cms = useSelector(state => state.cms);
  const basket = useSelector(state => state.basket);
  const login = useSelector(state => state.login);

  const [isOpenPopup, setIsOpenPopup] = useState(false);
  const [state, setState] = useMergeState({
    resPayment: {},
    totalPoint: undefined,
  });

  const onClosePopup = () => {
    setIsOpenPopup(false);
  };

  const onOpenPopup = () => {
    setIsOpenPopup(true);
  };

  const renderPriceString = amount => generaCurrency(amount, true);

  const exchangeRateCalculate = (price, rate) => {
    if (!price) {
      return '0.00';
    }
    const exchangeRate = rate || 1;
    return `${(parseFloat(price, 10) / exchangeRate).toFixed(2)}`;
  };

  const handleShowPoint = (total) => {
    const options = {
      url: GET_USER_ACTIONS,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        const ele = _.find(result, x => x.type === 'buy');
        if (ele) {
          setState({ totalPoint: ele.point * total / ele.spend });
          onOpenPopup();
        }
      }
    }).catch((err) => {
      console.log(err.mesage);
    });
  };

  const fetchData = (id) => {
    const options = {
      url: GET_ORDER_URL.replace('{id}', id),
      method: 'GET',
    };
    dispatch(loadingPage(true));
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        const login = auth.getLogin();
        if (login && login.user && login.user.id) {
          handleShowPoint(result.total);
        } else if (result.cart === basket.id) {
          dispatch(loadAllBasketSubscription(initialState.basket));
          localStorage.removeItem('basketId');
        }

        setState({
          resPayment: result,
        });

        const {
          line_items: lineItems, current_timestamp: currentTimestamp, transaction_timestamp: transactionTimestamp, exchange_rate: exchangeRate,
          promo,
        } = result;
        const items = _.map(lineItems, d => (
          {
            id: `${d.item.id}`,
            name: d.item.name,
            price: exchangeRateCalculate(d.item.price, exchangeRate),
            quantity: d.quantity,
            category: d.item.product.type.name,
            total: exchangeRateCalculate(d.total, exchangeRate),
            currency: 'sgd',
          }
        ));
        const itemsSegment = _.map(lineItems, d => (
          {
            product_id: `${d.item.id}`,
            product_name: d.item.name,
            price: parseFloat(exchangeRateCalculate(d.item.price, exchangeRate)),
            quantity: d.quantity,
            category: d.item.product.type.name,
            image_url: d.image_display,
            total: exchangeRateCalculate(d.total, exchangeRate),
            currency: 'sgd',
          }
        ));
        const transaction = {
          id: `${result.id}`,
          revenue: exchangeRateCalculate(result.total, exchangeRate),
          shipping: exchangeRateCalculate(result.shipping, exchangeRate),
          currency: 'sgd',
          coupon: promo ? promo.code : '',
        };
        const ecommerce = {
          currencyCode: 'sgd',
          purchase: {
            actionField: transaction,
            products: items,
          },
        };
        const dataNamogoo = {
          orderTotalBeforeDiscount: exchangeRateCalculate(result.subtotal, exchangeRate),
          orderTotalAfterDiscount: exchangeRateCalculate(parseFloat(result.subtotal) - parseFloat(result.discount), exchangeRate),
          couponCode: promo ? promo.code : '',
          numOfItems: items.length,
          currency: 'sgd',
        };
        addOrderCartNamogoo(dataNamogoo);
        trackGTMPurchase(ecommerce);
        trackGTMEventPurchase(transactionTimestamp, currentTimestamp);

        const dataSegment = {
          checkout_id: result?.cart,
          order_id: `${result.id}`,
          total: exchangeRateCalculate(result.total, exchangeRate), // total just called total after shipping and what not
          shipping: parseFloat(exchangeRateCalculate(result.shipping, exchangeRate)),
          discount: parseFloat(exchangeRateCalculate(result.discount, exchangeRate)),
          coupon: promo ? promo.code : '',
          currency: 'sgd',
          products: itemsSegment,
          revenue: parseFloat(exchangeRateCalculate(result.subtotal, exchangeRate)), // revenue is BEFORE DISCOUNT
          product_name: itemsSegment.map(item => item?.product_name),
          product_category: itemsSegment.map(item => item.category),
          product_id: itemsSegment.map(item => item.product_id),
          shop_source: 'online',

        };
        if (result?.is_subscription) {
          segmentSubscribedClub21G(dataSegment);
        } else {
          segmentOrderCompleted(dataSegment);
        }
      }
      dispatch(loadingPage(false));
    }).catch(() => {
      dispatch(loadingPage(false));
    });
  };

  const fetchDataCMS = async () => {
    let cmsPay = _.find(cms, x => x.title === 'Pay success');
    if (!cmsPay) {
      cmsPay = await fetchCMSHomepage('pay-success');
      const dataCms = getCms(cmsPay);
      setState(dataCms);
      dispatch(addCmsRedux(cmsPay));
    } else {
      const dataCms = getCms(cmsPay);
      setState(dataCms);
    }
  };

  useEffect(() => {
    // pageViewCheckoutSuccessAmplitude();
    setPrerenderReady();
    fetchDataCMS();
    if (orderId) {
      fetchData(orderId);
    }
    googleAnalitycs('/pay/success');
    sendOrderCompletedViewedHotjarEvent();
  }, []);

  const congurationBt = getNameFromButtonBlock(state.buttonBlock, 'Congratulations');
  const thanksBt = getNameFromButtonBlock(state.buttonBlock, 'Thank you for your purchase');
  const yourOrderbt = getNameFromButtonBlock(state.buttonBlock, 'Your order has been confirmed');
  const orderBt = getNameFromButtonBlock(state.buttonBlock, 'Order');
  const returnBt = getNameFromButtonBlock(state.buttonBlock, 'Return To Store');
  const gotoBt = getNameFromButtonBlock(state.buttonBlock, 'Go to your order');
  const orderSummarryBt = getNameFromButtonBlock(state.buttonBlock, 'order summary');
  const dateOfBt = getNameFromButtonBlock(state.buttonBlock, 'Date of Order');
  const paymentBt = getNameFromButtonBlock(state.buttonBlock, 'Payment Method');
  const shippingBt = getNameFromButtonBlock(state.buttonBlock, 'Shipping');
  const discountBt = getNameFromButtonBlock(state.buttonBlock, 'Discount');
  const pointsEarnedBt = getNameFromButtonBlock(state.buttonBlock, 'Points Earned');
  const pointsBt = getNameFromButtonBlock(state.buttonBlock, 'points');
  const TOTALBt = getNameFromButtonBlock(state.buttonBlock, 'TOTAL');
  const itemsBt = getNameFromButtonBlock(state.buttonBlock, 'Items');
  const yourPointBt = getNameFromButtonBlock(state.buttonBlock, 'You have earned points');
  const myTotalPoints = getNameFromButtonBlock(state.buttonBlock, 'My Total Points');

  const dataPoint = (
    <div className="data-point-div">
      <img src={logoBlack} alt="icon" />
      <div className="other-Tagline">
        {congurationBt}
      </div>
      <div className="other-Tagline">
        +
        {parseInt(state.totalPoint, 10)}
        {' '}
        {pointsBt}
        !
      </div>
      <div className="info-text">
        {yourPointBt}
      </div>
      <div className="line-1">
        <div>
          {pointsEarnedBt}
        </div>
        <div>
          <b>
            +
            {parseInt(state.totalPoint, 10)}
          </b>
        </div>
      </div>
      <div className="line-1">
        <div>
          {myTotalPoints}
        </div>
        <div>
          <b>{login?.user?.point}</b>
        </div>
      </div>
    </div>
  );
  const popupPoint = (
    <Modal className={classnames('modal-point', addFontCustom())} centered isOpen={isOpenPopup}>
      <ModalBody>
        <button type="button" className="button-bg__none bt-close" onClick={onClosePopup}>
          <CloseIcon style={{ color: '#2c2c2c' }} />
        </button>
        {dataPoint}
      </ModalBody>
    </Modal>
  );
  return (
    <div>
      <HeaderHomePageV3 />
      {popupPoint}
      <div className="pay-success">
        <div className="div-content">
          <div className="d-left">
            <img src={icSuccess} alt="icon" />
            <div className="text-1 tc">
              {thanksBt}
            </div>
            <div className="body-text-s-regular">
              {yourOrderbt}
            </div>
            <div className="text-2">
              <b>
                {orderBt}
                :
                {' '}
              </b>
              {state.resPayment?.id}
            </div>
            <ButtonCT name={returnBt} size="medium" />
            <button className="button-bg__none bt-goto" type="button">
              {gotoBt}
            </button>
          </div>
          {isMobile && state.totalPoint && <div className="div-line-mobile" />}
          {isMobile && state.totalPoint && dataPoint}
          {isMobile && state.totalPoint && <div className="div-line-mobile" />}
          <div className="div-card">
            <HeaderCT type={isBrowser ? 'Heading-M' : 'Heading-XXS'}>
              {orderSummarryBt}
            </HeaderCT>
            <div className="line-info">
              <div>
                {dateOfBt}
              </div>
              <div>
                <b>{moment(state.resPayment?.date_paid || state.resPayment?.date_modified).format('DD MMM YYYY')}</b>
              </div>
            </div>
            <div className="line-info">
              <div>
                {paymentBt}
              </div>
              <div>
                <b>{state.resPayment?.payment_method === 'checkout.com' ? 'Credit/debit card' : state.resPayment?.payment_method}</b>
              </div>
            </div>
            <div className="line-info">
              <div>
                {shippingBt}
              </div>
              <div>
                <b>{renderPriceString(state.resPayment?.shipping)}</b>
              </div>
            </div>
            <div className="line-info">
              <div>
                {discountBt}
              </div>
              <div>
                <b>
                  {parseFloat(state.resPayment?.discount, 10) !== 0 ? '-' : ''}
                  {renderPriceString(state.resPayment?.discount)}
                </b>
              </div>
            </div>
            {
              state.totalPoint && (
                <div className="line-info point-info">
                  <div>
                    {pointsEarnedBt}
                  </div>
                  <div>
                    <img src={logoBlack} alt="icon" />
                    <b className="mr-1">
                      {parseInt(state.totalPoint, 10)}
                      G
                    </b>
                    {pointsBt}
                  </div>
                </div>
              )
            }
            <div className="div-line" />
            <div className="line-total">
              <div>
                <b>{TOTALBt}</b>
                {' '}
                (
                {state.resPayment?.line_items?.length}
                {' '}
                {itemsBt}
                )
              </div>
              <div>
                <b>{renderPriceString(state.resPayment?.total)}</b>
              </div>
            </div>
          </div>
        </div>
        <USPBlock data={state.iconBlock?.icons} />
      </div>
      <FooterV2 />
    </div>
  );
}

export default PaySuccess;
