import React, { useState, useEffect } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';
import ButtonCT from '../../componentsv2/buttonCT';
import HeaderCT from '../../componentsv2/headerCT';
import './styles.scss';

function TestComponent(props) {
  return (
    <div className="test-component">
      <HeaderCT type="Heading-S">
        Medium Primary
      </HeaderCT>
      <ButtonCT name="Button text" className="mr-3" />
      <ButtonCT size="small" name="Button text" className="mr-3" />
      <ButtonCT size="small" name="Button text" className="mr-3" disabled />
      <ButtonCT color="secondary" name="Button text" className="mr-3" />
      <ButtonCT size="small" color="secondary" name="Button text" className="mr-3" />
      <ButtonCT size="small" color="secondary" name="Button text" className="mr-3" disabled />
      <ButtonCT variant="outlined" name="Button text" className="mr-3" />
      <ButtonCT variant="outlined" color="secondary" name="Button text" className="mr-3" />
      <ButtonCT name="Button text" className="mr-3" endIcon={<DeleteIcon />} />
      <ButtonCT name="" className="mr-3" endIcon={<DeleteIcon />} />
      <ButtonCT onlyIcon={<DeleteIcon />} />
    </div>
  );
}

export default TestComponent;
