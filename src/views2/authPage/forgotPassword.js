import EmailIcon from '@mui/icons-material/Email';
import _ from 'lodash';
// import ReactGA from 'react-ga';
import moment from 'moment';
import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';
import { MetaTags } from 'react-meta-tags';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import ButtonCT from '../../componentsv2/buttonCT';
import CardCT from '../../componentsv2/cardCT';
import HeaderCT from '../../componentsv2/headerCT';
import InputCT from '../../componentsv2/inputCT';
import {
  ASSIGN_BASKET_URL, CREATE_BASKET_URL, GET_BASKET_URL, LOGIN_FACEBOOK_URL, LOGIN_GOOGLE_URL, PASSWORD_RESET_URL, PUT_OUTCOME_URL,
} from '../../config';
import { facebook, google } from '../../imagev2/svg';
import { loadAllBasket } from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import { loginCompleted } from '../../Redux/Actions/login';
import {
  addClassIntoElemenetID, fetchCMSHomepage, generateHreflang, generateUrlWeb, getLinkFromButtonBlock, getNameFromButtonBlock, getSEOFromCms, gotoShopHome, isCheckNull, postPointRedeem, removeClassIntoElemenetID, removeLinkHreflang, trackGTMLogin, trackGTMSignUp, validateEmail,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import '../../styles/_login.scss';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';
import './styles.scss';

const getCMS = (cms) => {
  if (!cms) {
    return { seo: {}, buttonBlock: [] };
  }
  const seo = getSEOFromCms(cms);
  const { body } = cms;
  const buttonBlock = _.filter(body, x => x.type === 'button_block');
  const imageBlocks = _.filter(body, x => x.type === 'image_block');
  return {
    seo, buttonBlock, imageBlocks,
  };
};

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonBlock: [],
      imageBlocks: [],
      listError: [],
      messageSuccess: undefined,
      messageError: undefined,
    };

    this.data = {
      email: '',
      password: '',
      firtName: '',
      lastName: '',
      rEmail: '',
      rPassword1: '',
      rPassword2: '',
      fEmail: '',
      rchecked: false,
    };
  }

  componentDidMount() {
    addClassIntoElemenetID('html-root', 'bg-yellow');
    this.fetchCms();
    if (this.props.login?.user?.id) {
      this.props.history.push(generateUrlWeb('/account'));
    }
  }

  componentWillUnmount = () => {
    removeClassIntoElemenetID('html-root', 'bg-yellow');
    removeLinkHreflang();
  }

  resetError = () => {
    this.setState({
      listError: [], messageSuccess: undefined, messageError: undefined,
    });
  }

  fetchCms = () => {
    const { cms } = this.props;
    const cmsLoginPage = _.find(cms, x => x.title === 'Login Page');
    if (!cmsLoginPage) {
      fetchCMSHomepage('login-page').then((result) => {
        this.props.addCmsRedux(result);
        const cmsData = getCMS(result);
        this.setState(cmsData);
      });
    } else {
      const cmsData = getCMS(cmsLoginPage);
      this.setState(cmsData);
    }
  }

  resetPassWord = (email) => {
    const body = {
      email,
    };
    const options = {
      url: PASSWORD_RESET_URL,
      body,
      method: 'POST',
    };
    console.log('options', options);
    fetchClient(options).then((result) => {
      if (!result.isError) {
        const { buttonBlock } = this.state;
        const successText = getNameFromButtonBlock(buttonBlock, 'success-forgot-password');
        this.setState({ messageSuccess: successText, messageError: undefined });
        return;
      }
      throw new Error(result.message);
    }).catch((e) => {
      console.log('error', e);
      this.setState({ messageSuccess: undefined, messageError: e.message });
    });
  }

  onSubmitForgot = (e) => {
    e.preventDefault();
    if (this.isValidForgot(this.data)) {
      this.resetPassWord(this.data.fEmail);
    }
  }

  onChange = (e) => {
    const { value, name } = e.target;
    this.data[name] = value;
    this.forceUpdate();
  };

  getBasket = (id) => {
    const options = {
      url: GET_BASKET_URL.replace('{cartPk}', id),
      method: 'GET',
    };
    return fetchClient(options, true);
  }

  assignBasketId = (cart, basket) => {
    const items = [];
    _.forEach(basket.items, (x) => {
      items.push(x.id);
    });
    const options = {
      url: ASSIGN_BASKET_URL.replace('{id}', cart),
      method: 'POST',
      body: {
        items,
      },
    };
    return fetchClient(options, true);
  }

  updateOutComeUser = (user, outcome) => {
    if (isCheckNull(outcome)) {
      return;
    }
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', outcome),
      method: 'PUT',
      body: {
        user: parseInt(user, 10),
      },
    };
    fetchClient(options);
  }

  gotoForgotPassword = () => {
    this.props.history.push(generateUrlWeb('/forgotPassword'));
  }

  isValidForgot = (data) => {
    const listError = [];
    const { fEmail } = data;
    this.resetError();
    _.forEach(['fEmail'], (d) => {
      if (!data[d]) {
        listError.push(d);
      }
      if (d === 'fEmail' && !validateEmail(fEmail)) {
        listError.push(d);
      }
    });
    if (listError.length > 0) {
      this.setState({ listError });
      return false;
    }
    return true;
  }

  createBasket = (user) => {
    const { id } = user;
    const options = {
      url: CREATE_BASKET_URL.replace('{userId}', id),
      method: 'POST',
      body: {},
    };
    return fetchClient(options, true);
  }

  handleAfterLogin = (result) => {
    auth.login(result);
    const outcome = auth.getOutComeId();
    if (outcome && !result.user.outcome) {
      result.user.outcome = outcome;
      this.updateOutComeUser(result.user.id, outcome);
    }

    this.props.loginCompleted(result);
    const { user } = result;
    if (this.props.basket && this.props.basket.id && this.props.basket.items && this.props.basket.items.length > 0) {
      this.assignBasketId(result.user.cart, this.props.basket).then((res) => {
        if (res) {
          this.handleLoginSuccessful(res, user);
        } else {
          throw new Error();
        }
      }).catch((err) => {
        this.setState({ messageError: err.message, messageSuccess: undefined });
        this.props.loadingPage(false);
      });
      return;
    }
    if (user.cart) {
      this.getBasket(user.cart).then((basket) => {
        this.handleLoginSuccessful(basket, user);
      });
      return;
    }
    this.createBasket(user).then((basket) => {
      this.handleLoginSuccessful(basket, user);
    });
  }

  handleLoginSuccessful = (basket, user) => {
    const { buttonBlock } = this.state;

    this.setState({ messageSuccess: getNameFromButtonBlock(buttonBlock, 'You_are_login_successfully'), messageError: undefined });

    this.props.loadAllBasket(basket);
    if (user.is_b2b) {
      this.props.history.push(generateUrlWeb('/b2b/landing'));
    } else {
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    }
    this.props.loadingPage(false);
  }

  responseFacebook = (response) => {
    const { accessToken } = response;
    if (accessToken) {
      // const urlAvatar = handleDownloadFile('http://graph.facebook.com/108326343695312/picture?width=256&height=256&access_token=EAAD7DYx6D6sBANOrmEyGtVcer2r1RZBN4N3Tj6IihCo0TZA3qQVErbCVeslwTEkWUETdxiO22GHZAjBSAWLT0mzdOZA5ZC0joN1E0OscwoXyzNhlZCqvLErzhYGXz6ZBrUEU0s50ZBmiaejZCmZAoxolB0ZBD4v9stXFoz86ci3cOrBuWPGRpQRhrfZC5UZCqYB9z9qQ2RYC49yCPNWmiCrnG1pfJ');
      // auth.setUrlAvatar(urlAvatar);
      this.loginSocial(accessToken, LOGIN_FACEBOOK_URL);
    }
  }

  responseGoogle = (response) => {
    const { accessToken } = response;
    if (accessToken) {
      this.loginSocial(accessToken, LOGIN_GOOGLE_URL);
    }
  }

  loginSocial = (accessToken, url) => {
    const options = {
      url,
      method: 'POST',
      body: {
        access_token: accessToken,
      },
    };
    fetchClient(options).then((result) => {
      if (!result.isError) {
        // this.props.loginCompleted(result);
        // toastrSuccess('You are login successfully');
        // this.props.onClose();

        // tracking GTM
        if (url === LOGIN_GOOGLE_URL) {
          if (moment().valueOf() - moment(result.user.date_created).valueOf() < 5 * 60 * 1000) {
            trackGTMSignUp('google', result.user.id);
          } else {
            trackGTMLogin('google', result.user.id);
          }
        } else if (url === LOGIN_FACEBOOK_URL) {
          if (moment().valueOf() - moment(result.user.date_created).valueOf() < 5 * 60 * 1000) {
            trackGTMSignUp('facebook', result.user.id);
          } else {
            trackGTMLogin('facebook', result.user.id);
          }
        }

        if (!_.isEmpty(this.props.refPoint)) {
          postPointRedeem(this.props.refPoint);
        }
        this.handleAfterLogin(result);
      } else {
        throw new Error(result.message);
      }
    }).catch((e) => {
      this.setState({ messageSuccess: undefined, messageError: e.message });
    });
  }

  onClickSignup = () => {
    this.props.history.push(generateUrlWeb('/signup'));
  }

  openLink = (link) => {
    window.open(generateUrlWeb(link));
  }

  getStyleIcon = value => (value ? { color: '#2c2c2c' } : { color: '#8D826E' })

  render() {
    const {
      buttonBlock, listError,
    } = this.state;
    const { urlRedirect } = this.props;
    const {
      fEmail,
    } = this.data;
    const emailBt = getNameFromButtonBlock(buttonBlock, 'Email');
    const dontAccountBt = getNameFromButtonBlock(buttonBlock, 'Dont_have_an_Account');
    const orSignInBt = getNameFromButtonBlock(buttonBlock, 'or do it via other accounts');
    const enterEmailLoginBt = getNameFromButtonBlock(buttonBlock, 'enter-email-login');
    const signUpHearBt = getNameFromButtonBlock(buttonBlock, 'Sign UP here');
    const resetPasswordBt = getNameFromButtonBlock(buttonBlock, 'RESET PASSWORD');
    const subResetBt = getNameFromButtonBlock(buttonBlock, 'sub-reset-password');
    const sendLinkBt = getNameFromButtonBlock(buttonBlock, 'SEND LINK');
    const typeYourEmailBt = getNameFromButtonBlock(buttonBlock, 'Type your email');
    const seoTitleCms = getNameFromButtonBlock(buttonBlock, 'seo_forgotpassword_page');
    const seoDescription = getLinkFromButtonBlock(buttonBlock, 'seo_forgotpassword_page');
    const countryName = auth.getCountryName();
    const seoTitle = replaceCountryInMetaTags(countryName, seoTitleCms);

    const loginWithSocial = (
      <React.Fragment>
        <div className="div-row text-or">
          <div className="line-black" />
          <div className="text">{orSignInBt}</div>
          <div className="line-black" />
        </div>

        <div className="div-row div-bt-login">
          <GoogleLogin
            clientId="1051429470092-1ktl08tfm23q3b7i2au9ivv6pnil51g1.apps.googleusercontent.com"
            onSuccess={this.responseGoogle}
            onFailure={this.onFailure}
            render={renderProps => (
              <div className="bt-social google" onClick={renderProps.onClick}>
                <img src={google} alt="" />
              </div>
            )}
          />
          <FacebookLogin
            appId="276035609956267"
            callback={this.responseFacebook}
            render={renderProps => (
              <div className="bt-social facebook" onClick={renderProps.onClick}>
                <img src={facebook} alt="" />
              </div>
            )}
          />
        </div>
      </React.Fragment>
    );


    const forgotPasswordHtml = (
      <CardCT className="card-login">
        <form onSubmit={this.onSubmitForgot}>
          <HeaderCT type="Heading-M" className="tc">
            {resetPasswordBt}
          </HeaderCT>
          <div className="body-text-s-regular m-size tc">
            {subResetBt}
          </div>
          <div className="line-1">
            <InputCT
              label={emailBt}
              type="text"
              placeholder={typeYourEmailBt}
              iconLeft={<EmailIcon style={this.getStyleIcon(fEmail)} />}
              name="fEmail"
              value={fEmail}
              onChange={this.onChange}
              className={_.includes(listError, 'fEmail') ? 'error' : ''}
              messageError={_.includes(listError, 'fEmail') ? enterEmailLoginBt : ''}
            />
          </div>

          <ButtonCT
            name={sendLinkBt}
            className="w-100 bt-signin"
            onClick={this.onSubmitForgot}
            type="submit"
          />

          {
            this.state.messageSuccess && (
              <div className="message-show">
                <span>
                  {this.state.messageSuccess}
                </span>
              </div>
            )
          }

          {
            this.state.messageError && (
              <div className="message-show error">
                <span>
                  {this.state.messageError}
                </span>
              </div>
            )
          }
          <div className="div-row div-ready-login">
            <span className="text">
              {dontAccountBt}
            </span>
            <button type="button" className="button-bg__none" onClick={this.onClickSignup}>
              {signUpHearBt}
            </button>
          </div>

          {loginWithSocial}
        </form>
      </CardCT>
    );

    const bgImage = _.find(this.state.imageBlocks, x => x.value?.caption === 'forgot');

    return (
      <div className="auth-page">
        <MetaTags>
          <title>{seoTitle}</title>
          <meta name="description" content={seoDescription} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/forgotPassword')}
        </MetaTags>
        <HeaderHomePageV3 />

        <div className="content-page" style={{ backgroundImage: `url(${bgImage?.value?.image})` }}>
          <div className="content-left">
            {/* <img src={isLoginComponent ? bgLogin : isForgotComponent ? bgForgot : bgSignUp} alt="bg" /> */}
          </div>
          <div className="content-right">
            {forgotPasswordHtml}
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  loginCompleted,
  loadingPage,
  loadAllBasket,
  addCmsRedux,
};

function mapStateToProps(state) {
  return {
    refPoint: state.refPoint,
    cms: state.cms,
    basket: state.basket,
    // showAskRegion: state.showAskRegion,
    login: state.login,
    countries: state.countries,
  };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ForgotPassword));
