import EmailIcon from '@mui/icons-material/Email';
import LockIcon from '@mui/icons-material/Lock';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import _ from 'lodash';
// import ReactGA from 'react-ga';
import moment from 'moment';
import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import GoogleLogin from 'react-google-login';
import { MetaTags } from 'react-meta-tags';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import ButtonCT from '../../componentsv2/buttonCT';
import CardCT from '../../componentsv2/cardCT';
import CheckBoxCT from '../../componentsv2/checkBoxCT';
import HeaderCT from '../../componentsv2/headerCT';
import InputCT from '../../componentsv2/inputCT';
import {
  ASSIGN_BASKET_URL, CREATE_BASKET_URL, GET_BASKET_URL, LOGIN_FACEBOOK_URL, LOGIN_GOOGLE_URL, PUT_OUTCOME_URL, SIGN_UP_URL,
} from '../../config';
import { facebook, google } from '../../imagev2/svg';
import { loadAllBasket } from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import { loginCompleted } from '../../Redux/Actions/login';
import {
  addClassIntoElemenetID, fetchCMSHomepage, generateHreflang, generateUrlWeb,
  getLinkFromButtonBlock,
  getNameFromButtonBlock, getSEOFromCms, gotoShopHome,
  isCheckNull, postPointRedeem, removeClassIntoElemenetID, removeLinkHreflang, segmentSignUp,
  trackGTMLogin, trackGTMSignUp, validateEmail,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import '../../styles/_login.scss';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';
import './styles.scss';

const getCMS = (cms) => {
  if (!cms) {
    return { seo: {}, buttonBlock: [] };
  }
  const seo = getSEOFromCms(cms);
  const { body } = cms;
  const buttonBlock = _.filter(body, x => x.type === 'button_block');
  const imageBlocks = _.filter(body, x => x.type === 'image_block');
  return {
    seo, buttonBlock, imageBlocks,
  };
};

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonBlock: [],
      imageBlocks: [],
      listError: [],
      isPasswordNotMatch: false,
      isErrorRegistered: false,
      messageSuccess: undefined,
      messageError: undefined,
    };

    this.data = {
      email: '',
      password: '',
      firtName: '',
      lastName: '',
      rEmail: '',
      rPassword1: '',
      rPassword2: '',
      fEmail: '',
      rchecked: false,
    };
  }

  componentDidMount() {
    addClassIntoElemenetID('html-root', 'bg-yellow');
    this.fetchCms();
    if (this.props.login?.user?.id) {
      this.props.history.push(generateUrlWeb('/account'));
    }
  }

  componentWillUnmount = () => {
    removeClassIntoElemenetID('html-root', 'bg-yellow');
    removeLinkHreflang();
  }

  resetError = () => {
    this.setState({
      listError: [], isPasswordNotMatch: false, isErrorRegistered: false, messageSuccess: undefined, messageError: undefined,
    });
  }

  fetchCms = () => {
    const { cms } = this.props;
    const cmsLoginPage = _.find(cms, x => x.title === 'Login Page');
    if (!cmsLoginPage) {
      fetchCMSHomepage('login-page').then((result) => {
        this.props.addCmsRedux(result);
        const cmsData = getCMS(result);
        this.setState(cmsData);
      });
    } else {
      const cmsData = getCMS(cmsLoginPage);
      this.setState(cmsData);
    }
  }

  onSubmitRegister = (e) => {
    e.preventDefault();
    if (this.isValidRegister(this.data)) {
      this.signUp(this.data);
    }
  }

  onChange = (e) => {
    const { value, name } = e.target;
    this.data[name] = value;
    this.forceUpdate();
  };

  getBasket = (id) => {
    const options = {
      url: GET_BASKET_URL.replace('{cartPk}', id),
      method: 'GET',
    };
    return fetchClient(options, true);
  }

  assignBasketId = (cart, basket) => {
    const items = [];
    _.forEach(basket.items, (x) => {
      items.push(x.id);
    });
    const options = {
      url: ASSIGN_BASKET_URL.replace('{id}', cart),
      method: 'POST',
      body: {
        items,
      },
    };
    return fetchClient(options, true);
  }

  updateOutComeUser = (user, outcome) => {
    if (isCheckNull(outcome)) {
      return;
    }
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', outcome),
      method: 'PUT',
      body: {
        user: parseInt(user, 10),
      },
    };
    fetchClient(options);
  }

  gotoForgotPassword = () => {
    this.props.history.push(generateUrlWeb('/forgotPassword'));
  }

  isValidRegister = (data) => {
    const listError = [];
    const {
      rEmail, rPassword1, rPassword2,
    } = data;
    this.resetError();

    _.forEach(['firtName', 'lastName', 'rEmail', 'rPassword1', 'rPassword2', 'rchecked'], (d) => {
      if (!data[d]) {
        listError.push(d);
      }
      if (d === 'rEmail' && !validateEmail(rEmail)) {
        listError.push(d);
      }
    });
    if (listError.length > 0) {
      this.setState({ listError, isPasswordNotMatch: false, isErrorRegistered: false });
      return false;
    }

    if (rPassword1 !== rPassword2) {
      this.setState({ isPasswordNotMatch: true, listError: [], isErrorRegistered: false });
      return false;
    }
    return true;
  }

  createBasket = (user) => {
    const { id } = user;
    const options = {
      url: CREATE_BASKET_URL.replace('{userId}', id),
      method: 'POST',
      body: {},
    };
    return fetchClient(options, true);
  }

  handleAfterLogin = (result) => {
    auth.login(result);
    const outcome = auth.getOutComeId();
    if (outcome && !result.user.outcome) {
      result.user.outcome = outcome;
      this.updateOutComeUser(result.user.id, outcome);
    }

    this.props.loginCompleted(result);
    const { user } = result;
    if (this.props.basket && this.props.basket.id && this.props.basket.items && this.props.basket.items.length > 0) {
      this.assignBasketId(result.user.cart, this.props.basket).then((res) => {
        if (res) {
          this.handleLoginSuccessful(res, user);
        } else {
          throw new Error();
        }
      }).catch((err) => {
        this.setState({ messageError: err.message, messageSuccess: undefined });
        this.props.loadingPage(false);
      });
      return;
    }
    if (user.cart) {
      this.getBasket(user.cart).then((basket) => {
        this.handleLoginSuccessful(basket, user);
      });
      return;
    }
    this.createBasket(user).then((basket) => {
      this.handleLoginSuccessful(basket, user);
    });
  }

  handleLoginSuccessful = (basket, user) => {
    const { buttonBlock } = this.state;

    this.setState({ messageSuccess: getNameFromButtonBlock(buttonBlock, 'You_are_login_successfully'), messageError: undefined });

    this.props.loadAllBasket(basket);
    if (user.is_b2b) {
      this.props.history.push(generateUrlWeb('/b2b/landing'));
    } else {
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    }
    this.props.loadingPage(false);
  }

  responseFacebook = (response) => {
    const { accessToken } = response;
    if (accessToken) {
      // const urlAvatar = handleDownloadFile('http://graph.facebook.com/108326343695312/picture?width=256&height=256&access_token=EAAD7DYx6D6sBANOrmEyGtVcer2r1RZBN4N3Tj6IihCo0TZA3qQVErbCVeslwTEkWUETdxiO22GHZAjBSAWLT0mzdOZA5ZC0joN1E0OscwoXyzNhlZCqvLErzhYGXz6ZBrUEU0s50ZBmiaejZCmZAoxolB0ZBD4v9stXFoz86ci3cOrBuWPGRpQRhrfZC5UZCqYB9z9qQ2RYC49yCPNWmiCrnG1pfJ');
      // auth.setUrlAvatar(urlAvatar);
      this.loginSocial(accessToken, LOGIN_FACEBOOK_URL);
    }
  }

  responseGoogle = (response) => {
    const { accessToken } = response;
    if (accessToken) {
      this.loginSocial(accessToken, LOGIN_GOOGLE_URL);
    }
  }

  loginSocial = (accessToken, url) => {
    const options = {
      url,
      method: 'POST',
      body: {
        access_token: accessToken,
      },
    };
    fetchClient(options).then((result) => {
      if (!result.isError) {
        // this.props.loginCompleted(result);
        // toastrSuccess('You are login successfully');
        // this.props.onClose();

        // tracking GTM
        if (url === LOGIN_GOOGLE_URL) {
          if (moment().valueOf() - moment(result.user.date_created).valueOf() < 5 * 60 * 1000) {
            trackGTMSignUp('google', result.user.id);
            segmentSignUp(result?.user, 'google');
            // segmentSignUpIdentify(result.user);
          } else {
            trackGTMLogin('google', result.user.id);
          }
        } else if (url === LOGIN_FACEBOOK_URL) {
          if (moment().valueOf() - moment(result.user.date_created).valueOf() < 5 * 60 * 1000) {
            trackGTMSignUp('facebook', result.user.id);
            segmentSignUp(result?.user, 'facebook');
            // segmentSignUpIdentify(result.user);
          } else {
            trackGTMLogin('facebook', result.user.id);
          }
        }

        if (!_.isEmpty(this.props.refPoint)) {
          postPointRedeem(this.props.refPoint);
        }
        this.handleAfterLogin(result);
      } else {
        throw new Error(result.message);
      }
    }).catch((e) => {
      this.setState({ messageSuccess: undefined, messageError: e.message });
    });
  }

  onClickLogin = () => {
    this.props.history.push(generateUrlWeb('/login'));
  }

  signUp = (data) => {
    const {
      firtName, lastName, rEmail, rPassword1,
    } = data;
    const options = {
      url: SIGN_UP_URL,
      method: 'POST',
      body: {
        first_name: firtName,
        last_name: lastName,
        email: rEmail,
        password: rPassword1,
      },
    };
    fetchClient(options).then((result) => {
      if (!result.isError) {
        // this.loginApi(rEmail, rPassword1);
        // tracking GTM
        trackGTMSignUp('normal', result.user_id);
        segmentSignUp({
          firtName,
          lastName,
          email: rEmail,
          user_id: result?.user_id,
        }, 'normal');
        // segmentSignUpIdentify(result);

        if (!_.isEmpty(this.props.refPoint)) {
          postPointRedeem(this.props.refPoint);
        }
        const messageSuccess = getNameFromButtonBlock(this.state.buttonBlock, 'message_success_register');
        this.setState({ messageSuccess, messageError: undefined });
        // toastrSuccess('Please, verification email!');
        // if (this.props.isPageLogin) {
        //   this.props.history.push(generateUrlWeb('/'));
        // } else {
        //   this.props.onClose();
        // }
        return;
      }
      throw (new Error(result.message));
    }).catch((e) => {
      if (e.message === 'Email already exists.') {
        this.setState({ isErrorRegistered: true });
      } else {
        this.setState({ messageSuccess: undefined, messageError: e.message });
      }
    });
  }

  openLink = (link) => {
    window.open(generateUrlWeb(link));
  }

  getStyleIcon = value => (value ? { color: '#2c2c2c' } : { color: '#8D826E' })

  render() {
    const {
      buttonBlock, listError, isPasswordNotMatch, isErrorRegistered,
    } = this.state;
    const { urlRedirect } = this.props;
    const {
      firtName, lastName, rEmail, rPassword1, rPassword2,
    } = this.data;
    const passBt = getNameFromButtonBlock(buttonBlock, 'Password');
    const firstNameBt = getNameFromButtonBlock(buttonBlock, 'First_Name');
    const lastNameBt = getNameFromButtonBlock(buttonBlock, 'Last_Name');
    const emailAddressBt = getNameFromButtonBlock(buttonBlock, 'Email_Address');
    const confirmPassBt = getNameFromButtonBlock(buttonBlock, 'Confirm_Password');
    const alreadyHaveAccountBt = getNameFromButtonBlock(buttonBlock, 'Already_have_an_Account');
    const createBt = getNameFromButtonBlock(buttonBlock, 'CREATE YOUR ACCOUNT');
    const subCreateBt = getNameFromButtonBlock(buttonBlock, 'sub-title-create-account');
    const signInHere = getNameFromButtonBlock(buttonBlock, 'Sign in here');
    const orSignInBt = getNameFromButtonBlock(buttonBlock, 'or do it via other accounts');
    const bycreatingBt = getNameFromButtonBlock(buttonBlock, 'By creating an account means I’ve read and accept all');
    const termsBt = getNameFromButtonBlock(buttonBlock, 'Terms and Conditions');
    const andBt = getNameFromButtonBlock(buttonBlock, 'and');
    const privacyBt = getNameFromButtonBlock(buttonBlock, 'Privacy Policy');
    const errorFirstBt = getNameFromButtonBlock(buttonBlock, 'error_firstname');
    const errorLastBt = getNameFromButtonBlock(buttonBlock, 'error_lastname');
    const errorEmailBt = getNameFromButtonBlock(buttonBlock, 'error_email');
    const errorPasswordBt = getNameFromButtonBlock(buttonBlock, 'error_password');
    const errorConfirmBt = getNameFromButtonBlock(buttonBlock, 'error_confirm');
    const passwordNotBt = getNameFromButtonBlock(buttonBlock, 'error_password_not_match');
    const checkboxErrorBt = getNameFromButtonBlock(buttonBlock, 'error_checkbox');
    const emailRegisteredBt = getNameFromButtonBlock(buttonBlock, 'email_registered');
    const signUpBt = getNameFromButtonBlock(buttonBlock, 'Sign up');
    const confirmYourPasswordBt = getNameFromButtonBlock(buttonBlock, 'Confirm your password');
    const enterYourPassword = getNameFromButtonBlock(buttonBlock, 'Enter your password');
    const typeYourEmailBt = getNameFromButtonBlock(buttonBlock, 'Type your email');
    const typeYourLastnameBt = getNameFromButtonBlock(buttonBlock, 'Type your last name');
    const typeYourFirstnameBt = getNameFromButtonBlock(buttonBlock, 'Type your first name');
    const seoTitleCms = getNameFromButtonBlock(buttonBlock, 'seo_signup_page');
    const seoDescription = getLinkFromButtonBlock(buttonBlock, 'seo_signup_page');
    const countryName = auth.getCountryName();
    const seoTitle = replaceCountryInMetaTags(countryName, seoTitleCms);

    const loginWithSocial = (
      <React.Fragment>
        <div className="div-row text-or">
          <div className="line-black" />
          <div className="text">{orSignInBt}</div>
          <div className="line-black" />
        </div>

        <div className="div-row div-bt-login">
          <GoogleLogin
            clientId="1051429470092-1ktl08tfm23q3b7i2au9ivv6pnil51g1.apps.googleusercontent.com"
            onSuccess={this.responseGoogle}
            onFailure={this.onFailure}
            render={renderProps => (
              <div className="bt-social google" onClick={renderProps.onClick}>
                <img src={google} alt="" />
              </div>
            )}
          />
          <FacebookLogin
            appId="276035609956267"
            callback={this.responseFacebook}
            render={renderProps => (
              <div className="bt-social facebook" onClick={renderProps.onClick}>
                <img src={facebook} alt="" />
              </div>
            )}
          />
        </div>
      </React.Fragment>
    );

    const registerHtmlV2 = (
      <CardCT className="card-login">
        <form onSubmit={this.onSubmitRegister}>
          <HeaderCT type="Heading-M" className="tc">
            {createBt}
          </HeaderCT>
          <div className="body-text-s-regular m-size tc">
            {subCreateBt}
          </div>
          <div className="line-2">
            <InputCT
              label={firstNameBt}
              type="text"
              placeholder={typeYourFirstnameBt}
              name="firtName"
              value={firtName}
              onChange={this.onChange}
              iconLeft={<PersonOutlineIcon style={this.getStyleIcon(firtName)} />}
              className={_.includes(listError, 'firtName') ? 'error' : ''}
              messageError={_.includes(listError, 'firtName') ? errorFirstBt : ''}
            />
            <InputCT
              label={lastNameBt}
              type="text"
              placeholder={typeYourLastnameBt}
              iconLeft={<PersonOutlineIcon style={this.getStyleIcon(lastName)} />}
              name="lastName"
              value={lastName}
              onChange={this.onChange}
              className={_.includes(listError, 'lastName') ? 'error' : ''}
              messageError={_.includes(listError, 'lastName') ? errorLastBt : ''}
            />
          </div>
          <div className="line-1">
            <InputCT
              label={emailAddressBt}
              type="text"
              placeholder={typeYourEmailBt}
              iconLeft={<EmailIcon style={this.getStyleIcon(rEmail)} />}
              name="rEmail"
              value={rEmail}
              onChange={this.onChange}
              className={_.includes(listError, 'rEmail') || isErrorRegistered ? 'error' : ''}
              messageError={_.includes(listError, 'rEmail') ? errorEmailBt : isErrorRegistered ? emailRegisteredBt : ''}
            />
          </div>
          <div className="line-1">
            <InputCT
              label={passBt}
              type="password"
              placeholder={enterYourPassword}
              iconLeft={<LockIcon style={this.getStyleIcon(rPassword1)} />}
              name="rPassword1"
              value={rPassword1}
              onChange={this.onChange}
              className={_.includes(listError, 'rPassword1') || isPasswordNotMatch ? 'error' : ''}
              messageError={_.includes(listError, 'rPassword1') ? errorPasswordBt : isPasswordNotMatch ? passwordNotBt : ''}
            />
          </div>
          <div className="line-1">
            <InputCT
              label={confirmPassBt}
              type="password"
              placeholder={confirmYourPasswordBt}
              iconLeft={<LockIcon style={this.getStyleIcon(rPassword2)} />}
              name="rPassword2"
              value={rPassword2}
              onChange={this.onChange}
              className={_.includes(listError, 'rPassword2') || isPasswordNotMatch ? 'error' : ''}
              messageError={_.includes(listError, 'rPassword2') ? errorConfirmBt : isPasswordNotMatch ? passwordNotBt : ''}
            />
          </div>
          <CheckBoxCT
            onChange={e => this.onChange({ target: { value: e.target.checked, name: 'rchecked' } })}
            checked={this.data.rchecked}
            messageError={_.includes(listError, 'rchecked') ? checkboxErrorBt : undefined}
            label={(
              <div>
                {bycreatingBt}
                {' '}
                <button
                  onClick={() => this.openLink('/terms-conditions')}
                  className="button-bg__none"
                  type="button"
                >
                  <b>{termsBt}</b>
                </button>
                {' '}
                {andBt}
                {' '}
                <button
                  onClick={() => this.openLink('/terms-conditions')}
                  className="button-bg__none"
                  type="button"
                >
                  <b>{privacyBt}</b>
                </button>
                .
              </div>
            )}
          />

          <ButtonCT
            name={signUpBt}
            className="w-100"
            onClick={this.onSubmitRegister}
          />

          {
            this.state.messageSuccess && (
              <div className="message-show">
                <span>
                  {this.state.messageSuccess}
                </span>
              </div>
            )
          }

          {
            this.state.messageError && (
              <div className="message-show error">
                <span>
                  {this.state.messageError}
                </span>
              </div>
            )
          }
          <div className="div-row div-ready-login">
            <span className="text">
              {alreadyHaveAccountBt}
            </span>
            <button type="button" className="button-bg__none" onClick={this.onClickLogin}>
              {signInHere}
            </button>
          </div>

          {loginWithSocial}

        </form>
      </CardCT>
    );

    const bgImage = _.find(this.state.imageBlocks, x => x.value?.caption === 'signup');

    return (
      <div className="auth-page">
        <MetaTags>
          <title>{seoTitle}</title>
          <meta name="description" content={seoDescription} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/signup')}
        </MetaTags>

        <HeaderHomePageV3 />

        <div className="content-page" style={{ backgroundImage: `url(${bgImage?.value?.image})` }}>
          <div className="content-left">
            {/* <img src={isLoginComponent ? bgLogin : isForgotComponent ? bgForgot : bgSignUp} alt="bg" /> */}
          </div>
          <div className="content-right">
            {registerHtmlV2 }
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = {
  loginCompleted,
  loadingPage,
  loadAllBasket,
  addCmsRedux,
};

function mapStateToProps(state) {
  return {
    refPoint: state.refPoint,
    cms: state.cms,
    basket: state.basket,
    // showAskRegion: state.showAskRegion,
    login: state.login,
    countries: state.countries,
  };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Register));
