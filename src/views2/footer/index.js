import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { useSelector, useDispatch } from 'react-redux';
import classnames from 'classnames';
import InputCT from '../../componentsv2/inputCT';
import HeaderCT from '../../componentsv2/headerCT';
import ButtonCT from '../../componentsv2/buttonCT';
import CheckBoxCT from '../../componentsv2/checkBoxCT';
import LinkCT from '../../componentsv2/linkCT';
import { logoGold } from '../../imagev2/svg';
import './styles.scss';
import {
  fetchCMSHomepage, getLink, getLinkFromButtonBlock, getNameFromButtonBlock, trackGTMSubmitEmail, validateEmail,
} from '../../Redux/Helpers';
import addCmsRedux from '../../Redux/Actions/cms';
import { useMergeState } from '../../Utils/customHooks';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import { GET_BASKET_URL, GUEST_CREATE_BASKET_URL, SUBSCRIBER_NEWLETTER_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { loadAllBasket } from '../../Redux/Actions/basket';
import auth from '../../Redux/Helpers/auth';
import { loginUpdateUserInfo } from '../../Redux/Actions/login';

const getCMS = (data) => {
  if (data) {
    const { body } = data;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    const menuBlocks = _.filter(body, x => x.type === 'menu_block');
    const imageBlocks = _.filter(body, x => x.type === 'image_block');
    const iconBlock = _.find(body, x => x.type === 'icons_block');
    return {
      buttonBlocks,
      menuBlocks,
      imageBlocks,
      iconBlock,
    };
  }
  return {
    buttonBlocks: [],
    menuBlocks: [],
    imageBlocks: [],
    iconBlock: {},
  };
};

function FooterV2(props) {
  const dispatch = useDispatch();
  const cms = useSelector(state => state.cms);
  const basket = useSelector(state => state.basket);
  const login = useSelector(state => state.login);

  const [state, setState] = useMergeState({
    checked: true,
    email: '',
  });

  const fetchDataCms = async () => {
    const footerCms = _.find(cms, x => x.title === 'Footer V2');
    if (!footerCms) {
      try {
        const cmsData = await fetchCMSHomepage('footer-v2');
        dispatch(addCmsRedux(cmsData));
        const dataCms = getCMS(cmsData);
        setState(dataCms);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const dataCms = getCMS(footerCms);
      setState(dataCms);
    }
  };

  const assignEmailToCart = (email) => {
    if (!basket.id) {
      const options = {
        url: GUEST_CREATE_BASKET_URL,
        method: 'POST',
        body: {
          subtotal: 0,
          total: 0,
          email,
        },
      };
      fetchClient(options).then((result) => {
        if (result && !result.isError) {
          dispatch(loadAllBasket(result));
        }
      });
      return;
    }
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basket.id),
      method: 'PUT',
      body: {
        email,
      },
    };
    fetchClient(option);
  };

  const postNewLetter = () => {
    if (!validateEmail(state.email)) {
      toastrError('Email is not avaiable');
      return;
    }
    if (!state.checked) {
      toastrError('Please read accept all conditions');
      return;
    }

    assignEmailToCart(state.email);

    const options = {
      url: SUBSCRIBER_NEWLETTER_URL,
      method: 'POST',
      body: {
        email: state.email,
        user: login && login.user ? login.user.id : undefined,
      },
    };

    fetchClient(options).then((result) => {
      if (result.isError) {
        if (result.email) {
          toastrError('You already have registered');
        } else {
          toastrError('You have already subscribed to our newsletter');
        }
        return;
      }
      auth.setEmail(state.email);
      trackGTMSubmitEmail(state.email);
      toastrSuccess('Email sent successfully');
      if (options.body.user) {
        dispatch(loginUpdateUserInfo({ is_get_newsletters: true }));
      }
    }).catch((err) => {
      toastrError(err);
    });
  };

  useEffect(() => {
    fetchDataCms();
  }, []);

  const iReadBt = getNameFromButtonBlock(state.buttonBlocks, 'I’ve read and accept all');
  const andBt = getNameFromButtonBlock(state.buttonBlocks, 'and');
  const temrmsBt = getNameFromButtonBlock(state.buttonBlocks, 'Terms and Conditions');
  const linkTemrmsBt = getLinkFromButtonBlock(state.buttonBlocks, 'Terms and Conditions');
  const privacyBt = getNameFromButtonBlock(state.buttonBlocks, 'Privacy Policy');
  const linkPrivacyBt = getLinkFromButtonBlock(state.buttonBlocks, 'Privacy Policy');

  const labelEmail = () => (
    <div className="label-email">
      {iReadBt}
      {' '}
      <LinkCT to={getLink(linkTemrmsBt)}>
        {temrmsBt}
      </LinkCT>
      {' '}
      {andBt}
      {' '}
      <LinkCT to={getLink(linkPrivacyBt)}>
        {privacyBt}
      </LinkCT>
    </div>
  );

  const receiveBt = getNameFromButtonBlock(state.buttonBlocks, 'RECEIVE EXCLUSIVE EBOOK WITH ALL THE PERFUMERY TRICKS');
  const emailBt = getNameFromButtonBlock(state.buttonBlocks, 'Email address');
  const submitBt = getNameFromButtonBlock(state.buttonBlocks, 'Submit');
  const emailAddressBt = getNameFromButtonBlock(state.buttonBlocks, 'email');
  const phoneBt = getNameFromButtonBlock(state.buttonBlocks, 'phone');
  const copyBt = getNameFromButtonBlock(state.buttonBlocks, 'Copyright © 2021 Maison 21G. All rights reserved');
  return (
    <div className="footer-v2">
      <div className="content-footer">
        <div className="footer-top">
          <div className="form-wrapper">
            <HeaderCT type="Heading-M">
              {receiveBt}
            </HeaderCT>
            <div className="w-form">
              <div className="wrap-input">
                <InputCT
                  value={state.email}
                  onChange={e => setState({ email: e.target.value })}
                  placeholder={emailBt}
                  className="input-email"
                />
                <ButtonCT
                  color="secondary"
                  name={submitBt}
                  onClick={postNewLetter}
                />
              </div>
              <CheckBoxCT
                onChange={(e) => {
                  const { checked } = e.target;
                  setState({ checked });
                }}
                checked={state.checked}
                label={labelEmail()}
                className="checkbox-footer"
              />
            </div>
          </div>
          <div className="footer-logo">
            <img src={logoGold} alt="icon" />
          </div>
        </div>

        <div className={classnames('footer-grid', state.menuBlocks?.length === 5 ? 'grid-5' : '')}>
          {
            _.map(state.menuBlocks || [], d => (
              <div className="item-col">
                <div className="vertical-menu-title">
                  {d?.value?.text}
                </div>
                <ul className="w-list-unstyled">
                  {
                    _.map(d?.value?.submenu, k => (
                      <li className="vertical-menu-list-item">
                        <LinkCT to={getLink(k?.value?.link)}>
                          {k?.value?.name}
                        </LinkCT>
                      </li>
                    ))
                  }
                </ul>
              </div>
            ))
          }
          <div className="footer-right">
            <div className="cc-wrapper">
              {
                _.map(state.imageBlocks, d => (
                  <img src={d?.value?.image} alt="icon" loading="lazy" />
                ))
              }
            </div>
            <div className="footer-contact-wrapper">
              <a href={`mailto:${emailAddressBt}`}>
                {emailAddressBt}
              </a>
              <a href={`tel:${phoneBt}`} className="contact-link-footer">
                {phoneBt}
              </a>
            </div>
            <div className="sm-icons-wrapper">
              {
                _.map(state.iconBlock?.value?.icons || [], d => (
                  <a href={d?.value?.image?.caption}>
                    <img src={d?.value?.image?.image} loading="lazy" alt="" />
                  </a>
                ))
              }
            </div>
          </div>
        </div>
        <div className="footer-bottom">
          <div className="copy-right">
            {copyBt}
          </div>
        </div>
      </div>
    </div>
  );
}

export default FooterV2;
