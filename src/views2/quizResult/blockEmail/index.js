import React, { useState, useEffect } from 'react';
import ReplayIcon from '@mui/icons-material/Replay';
import LinkCT from '../../../componentsv2/linkCT';
import ButtonCT from '../../../componentsv2/buttonCT';
import CheckBoxCT from '../../../componentsv2/checkBoxCT';
import InputCT from '../../../componentsv2/inputCT';
import {
  getAltImageV2, getLinkFromButtonBlock, getNameFromButtonBlock, getNameFromCommon,
} from '../../../Redux/Helpers';
import './styles.scss';

function BlockEmail(props) {
  const [email, setEmail] = useState(props.email);
  const [isAgreeEmail, setIsAgreeEmail] = useState(true);
  const onChangeEmail = (e) => {
    const { value } = e.target;
    setEmail(value);
  };

  const onCheckBox = () => {
    setIsAgreeEmail(!isAgreeEmail);
  };

  return (
    <div className="block-email">
      <img loading="lazy" src={props.imageBlock?.value?.image} alt={getAltImageV2(props.imageBlock?.value)} />
      <div className="div-content-text">
        <div className="title-header">
          {getNameFromButtonBlock(props.buttonBlocks, 'compare your scents with others')}
        </div>
        <div className="text-des">
          {getNameFromButtonBlock(props.buttonBlocks, 'Now, that you have found your perfect perfume match')}
        </div>
        <div className="div-bottom-text">
          <div className="div-input">
            <InputCT
              type="text"
              placeholder={getNameFromButtonBlock(props.buttonBlocks, 'Enter your friend’s email')}
              name="email"
              value={email}
              onChange={onChangeEmail}
              className="input-email"
            />
            <ButtonCT
              disabled={!isAgreeEmail}
              name="Submit"
              onClick={() => props.onClickSend(email)}
            />
          </div>
          <div className="div-row row-check-box">
            <CheckBoxCT
              onChange={onCheckBox}
              name="create_address"
              checked={isAgreeEmail}
              label=""
              className="medium check-box-email"
            />
            <div className="link-text">
              {getNameFromButtonBlock(props?.buttonBlocks, 'I’ve read and accept all')}
              {' '}
              <LinkCT to={getLinkFromButtonBlock(props?.buttonBlocks, 'Terms and Conditions')}>{getNameFromButtonBlock(props?.buttonBlocks, 'Terms and Conditions')}</LinkCT>
              {' '}
              {getNameFromButtonBlock(props?.buttonBlocks, 'and')}
              {' '}
              <LinkCT to={getLinkFromButtonBlock(props?.buttonBlocks, 'Privacy Policy')}>{getNameFromButtonBlock(props?.buttonBlocks, 'Privacy Policy')}</LinkCT>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default BlockEmail;
