import React from 'react';
import MetaTags from 'react-meta-tags';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../footer';
import BlockDiscovery from './blockDiscovery';
import BlockEmail from './blockEmail';
import BlockInfo from './blockInfo';
import './styles.scss';

function QuizResult(props) {
  return (
    <div className="div-col">
      <MetaTags>
        <title>
          {props.seo ? props.seo.seoTitle : ''}
        </title>
        <meta
          name="description"
          content={props.seo ? props.seo.seoDescription : ''}
        />
        <meta
          name="keywords"
          content={props.seo ? props.seo.seoKeywords : ''}
        />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
      </MetaTags>
      <HeaderHomePageV3 />
      <div className="quiz-result">
        <BlockInfo
          personalityChoice={props.personalityChoice}
          dataPersionality={props.dataPersionality}
          buttonBlocks={props.buttonBlocks}
          cmsCommon={props.cmsCommon}
          onClickRetake={props.onClickRetake}
          namePath={props.namePath}
        />
        <BlockDiscovery
          ctaBlock={props.ctaBlock}
          buttonBlocks={props.buttonBlocks}
          customeBottle={props.customeBottle}
          infoGift={props.infoGift}
          dataDay={props.dataDay}
          dataNight={props.dataNight}
          cmsCommon={props.cmsCommon}
          mixes={props.mixes}
          onClickAddToCart={props.onClickAddToCart}
          onClickImageV4={props.onClickImageV4}
          namePath={props.namePath}
        />
        {
          !['sephora'].includes(props.namePath) && (
            <BlockEmail
              buttonBlocks={props.buttonBlocks}
              email={props.email}
              onClickSend={props.onClickSend}
              imageBlock={props.imageBlock}
            />
          )
        }
      </div>
      <FooterV2 />
    </div>
  );
}

export default QuizResult;
