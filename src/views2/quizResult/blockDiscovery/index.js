/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import CloseIcon from '@mui/icons-material/Close';
import classnames from 'classnames';
import { Modal, ModalBody } from 'reactstrap';
import HtmlParser from 'react-html-parser';
import _ from 'lodash';
import React, { useMemo, useState, useEffect } from 'react';
import BottleCustom from '../../../components/B2B/CardItem/bottleCustom';
import ButtonCT from '../../../componentsv2/buttonCT';
import CustomeBottleV4 from '../../../componentsv2/customeBottleV4';
import ScentCharacterV2 from '../../../componentsv2/scentCharacterV2';
import { COLOR_CUSTOME, FONT_CUSTOME } from '../../../constants';
import { isMobile991fn } from '../../../DetectScreen';
import icDualCrayon from '../../../image/bottle_dual-crayon.png';
import { dualCrayon, single3ml } from '../../../imagev2/png';
import bottleImage from '../../../imagev2/png/bottle-login.png';
import {
  icSeduction, icSeductionActive, icWork, icWorkActive,
} from '../../../imagev2/svg';
import {
  addClassIntoElemenetID, addFontCustom, generaCurrency, getNameFromButtonBlock, getNameFromCommon, removeClassIntoElemenetID,
} from '../../../Redux/Helpers';
import { useMergeState } from '../../../Utils/customHooks';
import BlockVideo from './blockVideo';
import './styles.scss';

function BlockDiscovery(props) {
  const { infoGift, customeBottle } = props;
  const [isShowModalSephora, setModalSephora] = useState(false);
  const [scentSelected, setScentSelected] = useState();
  const [state, setState] = useMergeState({
    dataCustom: {
      currentImg: !_.isEmpty(infoGift) ? infoGift.image : customeBottle ? customeBottle.currentImg : undefined,
      nameBottle: !_.isEmpty(infoGift) && infoGift.isDisplayName ? infoGift.name : customeBottle ? customeBottle.nameBottle : '',
      font: !_.isEmpty(infoGift) ? infoGift.font : customeBottle ? customeBottle.font : FONT_CUSTOME.JOST,
      color: !_.isEmpty(infoGift) ? infoGift.color : customeBottle ? customeBottle.color : COLOR_CUSTOME.BLACK,
    },
    isChillRelax: false,
    isSeducation: [1, 3].includes(props.mixes),
    isSpecialOccasion: false,
    isWorkSocial: [2, 4].includes(props.mixes),
    isRollOn: false,
    isOpenCustomeBottle: false,
    arrayThumbs: [],
  });

  const onClickButtonToggle = (id) => {
    setState({
      isWorkSocial: id === 4,
      isChillRelax: id === 1,
      isSeducation: id === 2,
      isSpecialOccasion: id === 3,
    });
    setScentSelected(undefined);
  };

  const dataOption = () => ([
    {
      image: icWork,
      imageActive: icWorkActive,
      name: getNameFromCommon(props.cmsCommon, 'WORK & SOCIAL'),
      id: 4,
      selected: state.isWorkSocial,
    },
    {
      image: icSeduction,
      imageActive: icSeductionActive,
      name: getNameFromCommon(props.cmsCommon, 'seduction'),
      id: 2,
      selected: state.isSeducation,
    },
  ]);

  const onGotoProduct = (datas) => {
    props.onClickImageV4(datas, state.dataCustom, state.isRollOn);
  };

  const onClickAddToCart = (item, datas) => {
    props.onClickAddToCart(item, null, state.dataCustom, datas);
  };

  const getImage = (products, index) => {
    const image1 = products && products.length > index ? _.find(products[index].product.images, x => x.type === 'main') : undefined;
    return image1;
  };

  const onSaveCustomer = (data) => {
    const { dataCustom } = state;
    const {
      image, name, color, font, imagePremade,
    } = data;
    dataCustom.color = color;
    dataCustom.font = font;
    dataCustom.imagePremade = imagePremade;
    if (_.isEmpty(image)) {
      dataCustom.currentImg = undefined;
      dataCustom.nameBottle = name;
      setState({ dataCustom });
      return;
    }
    dataCustom.currentImg = image;
    dataCustom.nameBottle = name;
    setState({ dataCustom });
  };

  const handleCropImage = (img) => {
    state.dataCustom.currentImg = img;
    setState({ dataCustom: state.dataCustom });
  };

  const onClickOnBottle = (e) => {
    if (props.namePath === 'sephora') {
      return;
    }
    e.stopPropagation();
    setState({ isOpenCustomeBottle: true });
  };

  useEffect(() => {
    addClassIntoElemenetID('id-container-full', 'overflow-none');
    return (() => {
      removeClassIntoElemenetID('id-container-full', 'overflow-none');
    });
  }, []);

  const combos = useMemo(() => {
    const datas = state.isWorkSocial ? props.dataDay : props.dataNight;
    return _.filter(datas?.combo, d => d.product?.type === 'Scent');
  }, [state.isWorkSocial, props.dataDay, props.dataNight]);


  const modalSephora = () => {
    const congratulationBt = getNameFromButtonBlock(props.buttonBlocks, 'congratulations');
    const youMixBt = getNameFromButtonBlock(props.buttonBlocks, 'you have successfully chosen your unique scent creation mix');
    const hereBt = getNameFromButtonBlock(props.buttonBlocks, 'Here’s what to do next');
    const closeBt = getNameFromButtonBlock(props.buttonBlocks, 'CLOSE');
    console.log('ctaBlock', props.ctaBlock);
    return (
      <Modal className={classnames('modal-full-screen modal-sephora', addFontCustom())} isOpen={isShowModalSephora} fade={false}>
        <ModalBody>
          <div className="div-header">
            <div>
              {congratulationBt}
            </div>
            <button
              className="bt-close"
              type="button"
              onClick={() => setModalSephora(false)}
            >
              <CloseIcon style={{ color: '#2C2C22' }} />
            </button>
          </div>
          <div className="div-image">
            <div>
              <img src={props.ctaBlock?.value?.image?.image} alt="perfume" />
            </div>
          </div>
          <div className="des-1">
            {youMixBt}
          </div>
          <div className="des-2">
            {hereBt}
          </div>
          <div className="des-3">
            {HtmlParser(props.ctaBlock?.value?.description)}
          </div>
          <ButtonCT
            name={closeBt}
            variant="outlined"
            color="secondary"
            onClick={() => setModalSephora(false)}
          />
        </ModalBody>
      </Modal>
    );
  };


  const {
    currentImg, nameBottle, isBlack, font, color,
  } = state.dataCustom || {};
  const datas = state.isWorkSocial ? props.dataDay : props.dataNight;
  const currentItem = _.find(datas?.items, d => d.is_sample === state.isRollOn);
  const otherItem = _.find(datas?.items, d => d.is_sample !== state.isRollOn);
  const titleQuiz = getNameFromButtonBlock(props.buttonBlocks, 'discover your perfume matches');
  const titleSephora = getNameFromButtonBlock(props.buttonBlocks, 'here are your perfume matches');
  const desSephoraBt = getNameFromButtonBlock(props.buttonBlocks, '1 perfume base');
  return (
    <div className={classnames('block-discovery', props.namePath === 'sephora' ? 'sephora-discovery' : '')}>

      {props.namePath === 'sephora' && modalSephora()}

      <div className="header-title">
        {props.namePath === 'sephora' ? titleSephora : titleQuiz}
      </div>
      <div className="toggle-button">
        {
          _.map(dataOption() || [], d => (
            <button
              className={classnames('button-t', d.selected ? 'active' : '')}
              type="button"
              onClick={() => onClickButtonToggle(d.id)}
            >
              <img loading="lazy" src={d.selected ? d.imageActive : d.image} alt="icon" />
              {d.name}
            </button>
          ))
        }
      </div>
      <div className="div-content">
        <div className="div-left-bottle">
          <div
            className="div-image-combo"
            onClick={() => (props.namePath === 'quiz' ? onGotoProduct(datas) : () => {})}
          >
            <div className="image-bg">
              <img loading="lazy" src={getImage(combos, 0)?.image} alt="scent" />
              <img loading="lazy" src={getImage(combos, 1)?.image} alt="scent" />
              <div className="text-scent-1">
                {combos[0].name}
              </div>
              <div className="text-scent-2">
                {combos[1].name}
              </div>

            </div>
            {
              !state.isRollOn && (
                <React.Fragment>
                  <div className="single-3ml img-left">
                    <img src={single3ml} alt="icon" />
                  </div>
                  <div className="single-3ml img-right">
                    <img src={single3ml} alt="icon" />
                  </div>
                </React.Fragment>
              )
            }
            <div className="div-img-bottle">
              {
                  state.isRollOn ? (
                    <img
                      className="image-dual-crayon"
                      src={icDualCrayon}
                      alt="Dual Crayyon"
                      // onClick={() => onGotoProduct(datas)}
                    />
                  ) : (
                    <BottleCustom
                      isImageText={!!currentImg}
                      image={currentImg}
                      // onGotoProduct={() => onGotoProduct(datas)}
                      onGotoProduct={onClickOnBottle}
                      isBlack={isBlack}
                      font={font}
                      color={color}
                      eauBt={getNameFromCommon(props.cmsCommon, 'Eau_de_parfum')}
                      mlBt={getNameFromCommon(props.cmsCommon, '25ml')}
                      isDisplayName={!!nameBottle}
                      name={nameBottle}
                      combos={combos}
                    />
                  )
                }

            </div>
          </div>
          <div className="name-product">
            {state.isRollOn ? getNameFromButtonBlock(props.buttonBlocks, 'try mini perfume (dual crayon)') : getNameFromButtonBlock(props.buttonBlocks, 'your Perfume creation (30 ml)')}
          </div>
          {
            props.namePath === 'sephora' && (
              <div className="des-product">
                {desSephoraBt}
              </div>
            )
          }
          <div className="price-product">
            {generaCurrency(currentItem ? currentItem.price : '')}
          </div>
          {
            props.namePath === 'quiz' && (
              <button className="button-change" type="button" onClick={() => setState({ isRollOn: !state.isRollOn })}>
                <img loading="lazy" src={state.isRollOn ? bottleImage : dualCrayon} alt="icon" />
                <div className="div-text">
                  <div className="title-change">
                    {state.isRollOn ? getNameFromButtonBlock(props.buttonBlocks, 'your Perfume creation (30 ml)') : getNameFromButtonBlock(props.buttonBlocks, 'try mini perfume (dual crayon)')}
                  </div>
                  <div className="price">
                    {generaCurrency(otherItem ? otherItem.price : '')}
                  </div>
                  {
                  !isMobile991fn() && (
                    <ArrowForwardIcon style={{ color: '#2c2c2c' }} />
                  )
                }
                </div>
                {
                  isMobile991fn() && (
                    <ArrowForwardIcon style={{ color: '#2c2c2c' }} />
                  )
                }
              </button>
            )
          }
          <ButtonCT
            name={getNameFromButtonBlock(props.buttonBlocks, 'Purchase Perfume')}
            endIcon={<AddShoppingCartIcon style={{ color: '#EBD8B8' }} />}
            onClick={() => (props.namePath === 'sephora' ? setModalSephora(true) : onClickAddToCart(currentItem, datas))}
          />
        </div>
        <div className="div-right-content">
          <ScentCharacterV2
            datas={combos}
            cmsCommon={props.cmsCommon}
            profile={datas?.profile}
            className={isMobile991fn() ? '' : 'text-block-info-v2'}
            isPerfumeProduct
            onClickScent={d => setScentSelected(d)}
            // name={_.isEmpty(scentSelected) ? (state.isWorkSocial ? getNameFromCommon(props.cmsCommon, 'WORK & SOCIAL') : getNameFromCommon(props.cmsCommon, 'seduction')) : undefined}
            title={_.isEmpty(scentSelected) ? (state.isWorkSocial ? getNameFromCommon(props.cmsCommon, 'Elegant & Confident') : getNameFromCommon(props.cmsCommon, 'Attractive & Sexy')) : undefined}
            description={_.isEmpty(scentSelected) ? (state.isRollOn ? getNameFromButtonBlock(props.buttonBlocks, 'Your dual crayon holds') : getNameFromButtonBlock(props.buttonBlocks, 'Your perfume creation des')) : undefined}
          />
          <BlockVideo
            combos={_.isEmpty(scentSelected) ? combos : _.filter(combos, x => x.product?.id === scentSelected.id)}
            isWorkSocial={state.isWorkSocial}
            title={!_.isEmpty(scentSelected) ? getNameFromButtonBlock(props.buttonBlocks, 'visualisation of scent') : undefined}
          />
        </div>
      </div>
      {
        state.isOpenCustomeBottle && (
        <CustomeBottleV4
          cmsCommon={props.cmsCommon}
          handleCropImage={handleCropImage}
          currentImg={state.dataCustom.currentImg}
          nameBottle={state.dataCustom.nameBottle}
          font={state.dataCustom.font}
          color={state.dataCustom.color}
          closePopUp={() => {
            setState({ isOpenCustomeBottle: false });
          }
          }
          onSave={onSaveCustomer}
          dataGtmtracking="funnel-1-step-15-save-customize"
          dataGtmtrackingClose="funnel-1-step-15-close-customize"

        // itemBottle={{
        //   price: this.bottle ? this.bottle.items[0].price : '',
        // }}
        // cmsTextBlocks={textBlock}
        // name1={combos && combos.length > 0 ? combos[0].name : ''}
        // name2={combos && combos.length > 1 ? combos[1].name : ''}
          name1=""
        />
        )
      }
    </div>
  );
}

export default BlockDiscovery;
