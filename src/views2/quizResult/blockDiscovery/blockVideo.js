import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import classnames from 'classnames';
import updateIngredientsData from '../../../Redux/Actions/ingredients';
import VideoAcademy from '../../../components/AcademyItem/videoAcademy';
import { toastrError } from '../../../Redux/Helpers/notification';
import { fetchScentLib } from '../../../Redux/Helpers/fetchAPI';

function BlockVideo(props) {
  const dispatch = useDispatch();
  const ingredients = useSelector(state => state.ingredients);
  const [listVideo, setListVideo] = useState([]);

  const getListVideos = () => {
    const videos = [];
    _.forEach(props.combos, (d) => {
      const ingredient = _.find(ingredients || [], x => x.id === d.product?.id);
      if (ingredient?.videos?.length) {
        videos.push(ingredient);
      }
    });
    console.log('videos', videos);
    setListVideo(videos);
  };

  const fetchIngredient = async () => {
    try {
      const data = await fetchScentLib();
      dispatch(updateIngredientsData(data));
    } catch (error) {
      toastrError(error.message);
    }
  };

  useEffect(() => {
    getListVideos();
  }, [props.combos, ingredients, props.isWorkSocial]);

  useEffect(() => {
    if (!ingredients || ingredients.length === 0) {
      fetchIngredient();
    }
  }, []);

  return (
    <div className={classnames('block-video', listVideo.length === 1 ? 'full-video' : '')}>
      {
        _.map(listVideo || [], d => (
          <div className="block-video-item">
            <div className="text-video">
              {props.title || d.name}
            </div>
            <div className="video-content">
              <VideoAcademy url={d.videos[0]?.video} poster={d.videos[0]?.placeholder} isControl />
            </div>
          </div>
        ))
      }
    </div>
  );
}

export default BlockVideo;
