import _ from 'lodash';
import React from 'react';
import classnames from 'classnames';
import ReplayIcon from '@mui/icons-material/Replay';
import { getNameFromButtonBlock, getNameFromCommon } from '../../../Redux/Helpers';
import './styles.scss';

function BlockInfo(props) {
  const isSephora = props.namePath === 'sephora';
  return (
    <div className={classnames('block-info-result', isSephora ? 'sephora-result' : '')}>
      {!isSephora && <img loading="lazy" className="div-image" src={props.personalityChoice?.image_personality_email} alt="left" />}
      <div className="div-right-info">
        <button
          onClick={props.onClickRetake}
          type="button"
          className="button-bg__none bt-retake"
        >
          <ReplayIcon />
          {getNameFromCommon(props.cmsCommon, 'RETAKE_QUIZ')}
        </button>
        <div className="div-header div-col">
          <div className="title-header">
            {getNameFromButtonBlock(props.buttonBlocks, 'YOUR PERSONALITY IS')}
          </div>
          <div className="div-name div-row">
            <div className="image-icon">
              <img loading="lazy" src={props.dataPersionality?.image} alt="icon" />
            </div>
            <div className="header_2" style={{ color: props.dataPersionality?.color }}>
              {props.personalityChoice?.personality?.title}
            </div>
          </div>
        </div>
        {
          !isSephora && (
            <React.Fragment>
              <div className="body-text-s-regular m-size heading">
                {getNameFromButtonBlock(props.buttonBlocks, 'your PROFILE strengths')}
              </div>
              <div className="list-tab">
                {
                  _.map(props.personalityChoice?.personality?.adjectives, d => (
                    <div>
                      {d.name}
                    </div>
                  ))
                }
              </div>
              <div className="des">
                {props.personalityChoice?.personality?.description}
              </div>
              <div className="body-text-s-regular m-size heading">
                {getNameFromButtonBlock(props.buttonBlocks, 'Your Scent Style')}
              </div>
              <div className="list-icon">
                {
                  _.map(props.personalityChoice?.personality?.scents, d => (
                    <div className="item-icon div-col">
                      <img
                        loading="lazy"
                        src={d.image}
                        alt="icon"
                      />
                      <div>
                        {d?.name}
                      </div>
                    </div>
                  ))
                }
              </div>
            </React.Fragment>
          )
        }
      </div>
    </div>
  );
}

export default BlockInfo;
