import _ from 'lodash';
import moment from 'moment';
import React, { memo, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { addProductBasket, createBasketGuest } from '../../../../Redux/Actions/basket';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import auth from '../../../../Redux/Helpers/auth';
import { GIFT_RECIPIENT_FORM, GIFT_WORKSHOP } from '../../constants';
import './styles.scss';
import WorkshopDetail from './workshopDetail';

function WorkshopTabs({ dataWorkshop }) {
  const dispatch = useDispatch();
  const basket = useSelector(state => state.basket);

  const [tabActive, setTabActive] = useState();
  const [tabDetail, setTabDetail] = useState({});
  const [quantity, setQuantity] = useState({ value: 1, label: '1' });
  const [orderItem, setOrderItem] = useState({});

  useEffect(() => {
    if (dataWorkshop.productBlocks.length > 0) {
      setTabActive(dataWorkshop.productBlocks[0]?.id);
      setTabDetail(dataWorkshop.productBlocks[0]);
    }
  }, [dataWorkshop.productBlocks]);


  const handleActiveTabClick = (tabId) => {
    const detail = _.find(dataWorkshop.productBlocks, item => item.id === tabId);
    if (detail?.id) {
      setTabDetail(detail);
    }
    setTabActive(tabId);
    setQuantity({ value: 1, label: '1' });
  };

  const handleClearOrderItem = () => {
    setOrderItem({});
    setQuantity({ value: 1, label: '1' });
  };

  const handleQuantityChange = (e) => {
    const value = e.target.value;
    setQuantity({value: Number(value), label: value});
  }

  const handleSaveInfo = (data) => {
    const { senderDetails, recipientDetails, isCreateNewGift } = data;
    const price = parseInt(tabDetail?.detail?.price, 10) * quantity?.value;
    const giftWorkshopName = getNameFromButtonBlock(dataWorkshop?.buttonBlocks, 'gift_workshop_name');

    const info = {
      name: tabDetail?.detail?.name,
      currency: auth.getNameCurrency(),
      price_custom: price,
      quantity: quantity?.value,
      item: tabDetail?.detail?.id,
      meta: {
        giftName: giftWorkshopName,
        typeOfGift: GIFT_WORKSHOP,
        type: GIFT_RECIPIENT_FORM,
        isCreateNewGift,
        sender: {
          name: senderDetails.senderName,
          email: senderDetails.senderEmail,
        },
        receiver: {
          name: recipientDetails.recipientName,
          email: recipientDetails.recipientEmail,
          country: recipientDetails.recipientCountry || auth.getCountry(),
          date_sent: moment(recipientDetails.dateOfEmail).valueOf() / 1000,
          note: senderDetails.note,
          phone: recipientDetails.recipientPhone,
        },
      },
    };

    const dataTemp = {
      idCart: basket.id,
      item: info,
    };

    if (!basket.id) {
      dispatch(createBasketGuest(dataTemp));
    } else {
      dispatch(addProductBasket(dataTemp));
    }

    handleClearOrderItem();
  };

  return (
    <div className="workshop-tabs padding-y">
      <div className="workshop-tabs-header">
        {_.map(dataWorkshop.productBlocks || [], ws => (
          <div
            key={ws.id}
            className={`tab-item ${tabActive === ws.id ? 'tab-item-active' : ''}`}
            onClick={() => handleActiveTabClick(ws.id)}
          >
            {ws.value?.text}
          </div>
        ))}
      </div>
      <div className="workshop-tabs-content">
        <WorkshopDetail
          dataWorkshop={dataWorkshop}
          workshopDetail={tabDetail}
          quantity={quantity}
          orderItem={orderItem}
          basket={basket}
          onQuantityChange={handleQuantityChange}
          onAddToCart={value => setOrderItem(value)}
          onClearOrderItem={handleClearOrderItem}
          onSaveInfo={handleSaveInfo}
        />
      </div>
    </div>
  );
}

export default memo(WorkshopTabs);
