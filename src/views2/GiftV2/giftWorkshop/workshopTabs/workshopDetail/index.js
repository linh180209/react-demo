import { Drawer } from '@mui/material';
import _, { isEmpty } from 'lodash';
import React, { memo, useEffect, useState } from 'react';
import reactHtmlPare from 'react-html-parser';
import { Modal, ModalBody } from 'reactstrap';
import HeaderHomePageV3 from '../../../../../components/HomePage/HeaderHomePageV3';
import ButtonCT from '../../../../../componentsv2/buttonCT';
import InputCT from '../../../../../componentsv2/inputCT';
import { isMobile991fn } from '../../../../../DetectScreen';
import { icBottle, icDuration } from '../../../../../imagev2/svg';
import { addFontCustom, generaCurrency, getNameFromButtonBlock } from '../../../../../Redux/Helpers';
import '../../../../../styles/product-detail.scss';
import useWindowEvent from '../../../../../Utils/useWindowEvent';
import ImageBlock from '../../../../../views/ProductB2C/imageBlock';
import { QUANTITY_OPTIONS } from '../../../constants';
import DetailBlock from '../../../giftBundlesDetail/detailBlock';
import '../../../giftBundlesDetail/styles.scss';
import './styles.scss';

function WorkshopDetail({
  dataWorkshop, workshopDetail, quantity, orderItem, basket, onQuantityChange, onAddToCart, onClearOrderItem, onSaveInfo,
}) {
  const [viewFullImages, setViewFullImages] = useState({
    isVisible: false,
    images: [],
  });
  const [isMobile, setMobile] = useState(false);
  const [isOpenDrawer, setOpenDrawer] = useState(false);

  const handleInitResize = () => {
    onClearOrderItem();

    if(isMobile991fn()) setMobile(true);
    else setMobile(false);
  };

  useEffect(() => {
    if (workshopDetail?.detail?.id) {
      const images = _.map(workshopDetail?.images, x => ({
        original: x?.value?.image?.image,
        thumbnail: x?.value?.image?.image,
      }));

      setViewFullImages(prevState => ({ ...prevState, images }));
    }
  }, [workshopDetail]);

  useWindowEvent('resize', handleInitResize, window);

  const handleAddedToCart = (dataWorkshop) => {

    if (isMobile || isMobile991fn()) {
      setOpenDrawer(true);
    } else {
      setOpenDrawer(false);
    }

    // console.log("dataworkshop", dataWorkshop);

    onAddToCart(dataWorkshop);
  };

  const workshopOveview = getNameFromButtonBlock(dataWorkshop.buttonBlocks, 'workshop_overview');
  const whatYouGet = getNameFromButtonBlock(dataWorkshop.buttonBlocks, 'what_you_will_get');
  const howItWorks = getNameFromButtonBlock(dataWorkshop.buttonBlocks, 'how_it_works');
  const addToCart = getNameFromButtonBlock(dataWorkshop.buttonBlocks, 'add_to_cart');
  const minute = getNameFromButtonBlock(dataWorkshop.buttonBlocks, 'minute')

  const handleCloseModalClick = () => {
    setViewFullImages(prevState => ({ ...prevState, isVisible: false }));
  };

  const handleViewListImageClick = () => {
    setViewFullImages(prevState => ({ ...prevState, isVisible: true }));
  };

  const passedGiftDetail = {
    type: 'gift_workshop',
    name: workshopDetail?.detail?.name,
    price: workshopDetail?.detail?.price,
    id: workshopDetail?.detail?.id
  }

  return (
    <div className="workshop-detail">
      <div className="workshop-detail-overview">
        <div className="ws-images">
          {_.map(workshopDetail?.images || [], (img, idx) => (
            <div key={idx} className="ws-image" onClick={handleViewListImageClick}>
              <img loading="lazy" src={img?.value?.image?.image} alt="gift" />
            </div>
          ))}
        </div>

        <div className="ws-content">
          <div className="ws-title">{workshopOveview}</div>
          <div className="ws-description">{reactHtmlPare(workshopDetail?.value?.description)}</div>
        </div>
      </div>
      {isEmpty(orderItem) ? (
        <div className={`workshop-detail-adding ${isMobile || isMobile991fn() ? '' : 'animated faster fadeInRight'}`}>
          <div className="ws-basic-info">
            <div className="ws-price">
              {generaCurrency(workshopDetail?.detail?.price, true)}
              <span>
                &nbsp;/
                {workshopDetail?.value?.link}
              </span>
            </div>

            <div className="ws-duration">
              <img src={icDuration} alt="gift-icon" />
              {workshopDetail?.duration}
              {' '}
              {minute}
            </div>
            <div className="ws-list">
              <div className="ws-list-title">{whatYouGet}</div>
              {_.map(workshopDetail?.whatYouGet, (item, idx) => (
                <div className="ws-list-item" key={idx}>
                  <img src={icBottle} alt="gift-icon" />
                  {item?.value}
                </div>
              ))}
            </div>
            <div className="ws-list">
              <div className="ws-list-title">{howItWorks}</div>
              {_.map(dataWorkshop?.textHowItWorks?.value?.texts, (item, idx) => (
                <div className="ws-list-item no-icon" key={idx}>{item?.value}</div>
              ))}
            </div>
          </div>

          <div className={`ws-add-to-cart ${isMobile || isMobile991fn() ? 'block-fixed' : ''}`}>
            <div className="ws-select">
              <InputCT
                type="select"
                value={quantity}
                onChange={value => onQuantityChange(value)}
                classNamePrefix="ws-select"
                options={QUANTITY_OPTIONS}
              />
            </div>
            <div className="ws-btn">
              <ButtonCT name={addToCart} onClick={() => handleAddedToCart(dataWorkshop)} />
            </div>
          </div>
        </div>
      ) : (
        (isMobile || isMobile991fn()) ? (
          <Drawer
            open={isOpenDrawer}
            className="gift-drawer"
            anchor="right"
          >
            <HeaderHomePageV3 />
            <div className="block-description">
              {!isEmpty(orderItem) && (
                <DetailBlock
                  isDrawer
                  giftDetail={passedGiftDetail}
                  basket={basket}
                  buttonBlocks={dataWorkshop.buttonBlocks}
                  onClearGiftState={onClearOrderItem}
                  onSaveInfo={onSaveInfo}
                  onCloseDrawer={onClearOrderItem}
                  isMobile={isMobile}
                />
              )}
            </div>
          </Drawer>
        ) : (
          <DetailBlock
            giftDetail={passedGiftDetail}
            basket={basket}
            buttonBlocks={dataWorkshop.buttonBlocks}
            onClearGiftState={onClearOrderItem}
            onSaveInfo={onSaveInfo}
            isMobile={isMobile}
          />
        )
      )}

      <Modal
        className={`modal-view-full-image modal-workshop ${addFontCustom()}`}
        isOpen={viewFullImages.isVisible}
        toggle={handleCloseModalClick}
      >
        <ModalBody>
          <ImageBlock
            images={viewFullImages?.images}
            datas={[]}
            dataCustom={{}}
            onClose={handleCloseModalClick}
            className="product-detail-v2 product-detail-v3"
            isPopUp
            isQuizPage
          />
        </ModalBody>
      </Modal>
    </div>
  );
}

export default memo(WorkshopDetail);
