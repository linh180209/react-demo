import _, { isEmpty } from 'lodash';
import React, { useEffect } from 'react';
import { MetaTags } from 'react-meta-tags';
import { useDispatch, useSelector } from 'react-redux';
import HeaderHomePageV3 from '../../../components/HomePage/HeaderHomePageV3';
import BlockFAQV2 from '../../../componentsv2/blockFAQV2';
import addCmsRedux from '../../../Redux/Actions/cms';
import loadingPage from '../../../Redux/Actions/loading';
import {
  fetchCMSHomepage, generateHreflang, getLinkFromButtonBlock, getNameFromButtonBlock, removeLinkHreflang, segmentGiftingOfferPageViewed
} from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';
import { generateScriptFAQ, removeScriptFAQ } from '../../../Utils/addScriptSchema';
import { useMergeState } from '../../../Utils/customHooks';
import { replaceCountryInMetaTags } from '../../../Utils/replaceCountryInMetaTags';
import ReviewsV3 from '../../../views/ProductB2C/reviewsV3';
import FooterV2 from '../../footer';
import Breadcrumb from '../components/breadcrumb';
import GiftBanner from '../components/giftBanner';
import HowItWorks from '../components/howItWorks';
import '../giftv2.scss';
import { handleBreadcrumbCreate } from '../utils';
import './styles.scss';
import WorkshopTabs from './workshopTabs';

const handleWorkshopProductGet = (images, proBlocks, itemBlocks, whatYouGets) => {
  const products = proBlocks?.value?.product;
  const productBlocks = _.map(products, (x) => {
    const product = _.find(images, y => y?.value?.image_background?.caption === x?.value?.image?.caption);
    const detail = _.find(itemBlocks?.value?.items, z => z?.value?.description === x?.value?.image?.caption);
    const whatYouGet = _.find(whatYouGets, w => w?.value?.header?.header_text === `${x?.value?.image?.caption}_what_you_get`);

    const defaultDuration = '60'; // Mins

    if (!isEmpty(product) && !isEmpty(detail)) {
      return {
        ...x,
        images: product?.value?.icons,
        duration: product?.value?.opacity || defaultDuration,
        detail: { ...detail?.value?.item },
        whatYouGet: whatYouGet?.value?.texts,
      };
    }
  });
  return productBlocks?.length > 0 ? productBlocks : [];
};

const handleGetCMSData = (data) => {
  if (!isEmpty(data)) {
    const iconBlocks = _.filter(data.body, x => x.type === 'icons_block');

    const reviewCms = _.find(iconBlocks, x => x?.value?.image_background?.caption === `our_client_workshop`);
    let reviews = [];

    if(!isEmpty(reviewCms)) {
      reviews = _.map(reviewCms?.value?.icons, x => ({
        name: x?.value?.image?.caption,
        comment: x?.value?.text,
        giftType: x?.value?.header?.header_text,
      }));
    }
    const buttonBlocks = _.filter(data.body, x => x.type === 'button_block');
    const imageBlocks = _.filter(data.body, x => x.type === 'image_block');
    const textBlocks = _.filter(data.body, x => x.type === 'texts_block');
    const textHowItWorks = _.find(data.body, x => x.type === 'texts_block' && x.value.header.header_text === 'gift_workshop_how_it_works');
    const textBlocksHeader = _.find(data.body, x => x.type === 'texts_block' && x.value.header.header_text === 'title');
    const faqData = _.find(data.body, x => x.type === 'faqs_block');
    const faqFilter = _.filter(faqData.value, x => x.value?.header?.header_text === 'gift_workshop_faq');
    const faqBlocks = _.map(faqFilter, x => ({ ...x, value: { ...x.value, faqs: x.value.faq } }));
    const products = _.find(data.body, x => x.type === 'products_block' && x?.value?.header?.header_text === 'gift_workshop');
    const itemBlocks = _.find(data.body, x => x.type === 'items_block');
    const menuBlocks = _.find(data.body, x => x?.type === 'menu_block' && x?.value?.text === 'workshop_breadcrumb');
    const breadcrumb = handleBreadcrumbCreate(menuBlocks?.value?.submenu, auth.getCountry());

    // Generate Workshop
    const productBlocks = handleWorkshopProductGet(iconBlocks, products, itemBlocks, textBlocks);

    return {
      iconBlocks,
      productBlocks,
      buttonBlocks,
      imageBlocks,
      textBlocks,
      textBlocksHeader,
      textHowItWorks,
      faqBlocks,
      breadcrumb,
      reviews,
    };
  }
  return {};
};

function GiftWorkshop() {
  const dispatch = useDispatch();
  const cms = useSelector(state => state.cms);
  const countries = useSelector(state => state.countries);

  const [dataState, setDataState] = useMergeState({
    buttonBlocks: [],
    iconBlocks: [],
    textBlocks: [],
    textHotItWorks: {},
    textBlocksHeader: {},
    imageBlocks: [],
    productBlocks: [],
    faqBlocks: [],
    breadcrumb: [],
    reviews: [],
  });

  const handleFetchDataInit = async () => {
    const cmsBlock = _.find(cms, x => x.title === 'GiftV2');
    let cmsData = {};

    if (isEmpty(cmsBlock)) {
      dispatch(loadingPage(true));

      try {
        const cmsResponse = await fetchCMSHomepage('gift-v2');
        dispatch(addCmsRedux(cmsResponse));
        cmsData = handleGetCMSData(cmsResponse);

        dispatch(loadingPage(false));
      } catch (error) {
        dispatch(loadingPage(false));
      }
    } else {
      cmsData = handleGetCMSData(cmsBlock);
    }

    setDataState(cmsData);
  };

  useEffect(() => {
    handleFetchDataInit();
    segmentGiftingOfferPageViewed("Gift Workshop Experience");
    return () => {
      removeLinkHreflang();
      removeScriptFAQ();
    };
  }, []);

  useEffect(() => {
    if (dataState.faqBlocks?.length > 0) {
      generateScriptFAQ(dataState.faqBlocks, true);
    }
  }, [dataState.faqBlocks]);

  const howItWorksTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'how_it_works');
  const howItWorksData = _.find(dataState.iconBlocks, x => x?.value?.image_background?.caption === 'gift_workshop_how_it_works');
  const banner = _.find(dataState.imageBlocks, x => x?.value?.caption === 'gift_workshop_banner');
  const countryName = auth.getCountryName();
  const titleCms = getNameFromButtonBlock(dataState.buttonBlocks, 'seo_gift_workshop');
  const desCms = getLinkFromButtonBlock(dataState.buttonBlocks, 'seo_gift_workshop');
  const seoTitle = replaceCountryInMetaTags(countryName, titleCms);
  const seoDes = replaceCountryInMetaTags(countryName, desCms);
  const ourClientTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'our_client_gift_workshop');
  const ourClientDes = getLinkFromButtonBlock(dataState.buttonBlocks, 'our_client_gift_workshop');

  return (
    <div className="gift-workshop">
      <MetaTags>
        <title>
          {seoTitle}
        </title>
        <meta name="description" content={seoDes} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/gift/workshop')}
      </MetaTags>
      <HeaderHomePageV3 />

      <div className="gift-wrapper">
        <Breadcrumb breadcrumb={dataState.breadcrumb} />
      </div>

      <GiftBanner
        classNames="gift-workshop-banner gift-dark-banner"
        bannerUrlDesktop={banner?.value?.image}
        bannerUrlMobile={banner?.value?.image}
      >
        <div className="bespoke-banner-wrapper wrapper">
          <div className="block-content-title main-title" style={{ whiteSpace: 'pre-line' }}>
            {dataState.textBlocksHeader?.value?.texts?.length > 0 ? dataState.textBlocksHeader.value.texts[4]?.value : ''}
          </div>
          <div className="block-content-description">
            {dataState.textBlocks?.value?.texts?.length > 0 ? dataState.textBlocks?.value?.texts[4]?.value : ''}
          </div>
        </div>
      </GiftBanner>

      <div className="gift-content">
        <HowItWorks data={howItWorksData} title={howItWorksTitle} />

        <WorkshopTabs dataWorkshop={dataState} />

        {/* <OurClients
          buttonBlocks={dataState.buttonBlocks}
          iconBlocks={dataState.iconBlocks}
          typeOfGift='workshop'
        /> */}

        <ReviewsV3
          dataFAQ={dataState}
          buttonBlocks={dataState.buttonBlocks}
          pageType='gift'
          title={ourClientTitle}
          description={ourClientDes}
        />

        <div className="gift-faqs">
          <BlockFAQV2
            dataFAQ={dataState.faqBlocks?.length > 0 ? dataState.faqBlocks[0]?.value : []}
            buttonBlocks={dataState.buttonBlocks}
          />
        </div>
      </div>
      <FooterV2 />
    </div>
  );
}

export default GiftWorkshop;
