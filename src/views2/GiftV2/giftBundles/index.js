import _, { isEmpty } from 'lodash';
import React, { useEffect } from 'react';
import { MetaTags } from 'react-meta-tags';
import { useDispatch, useSelector } from 'react-redux';
import HeaderHomePageV3 from '../../../components/HomePage/HeaderHomePageV3';
import BlockFAQV2 from '../../../componentsv2/blockFAQV2';
import addCmsRedux from '../../../Redux/Actions/cms';
import loadingPage from '../../../Redux/Actions/loading';
import {
  fetchCMSHomepage, generateHreflang, getLinkFromButtonBlock, getNameFromButtonBlock, removeLinkHreflang, segmentGiftingOfferPageViewed
} from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';
import { generateScriptFAQ, removeScriptFAQ } from '../../../Utils/addScriptSchema';
import { useMergeState } from '../../../Utils/customHooks';
import { replaceCountryInMetaTags } from '../../../Utils/replaceCountryInMetaTags';
import ReviewsV3 from '../../../views/ProductB2C/reviewsV3';
import FooterV2 from '../../footer';
import Breadcrumb from '../components/breadcrumb';
import GiftBanner from '../components/giftBanner';
import HowItWorks from '../components/howItWorks';
import '../giftv2.scss';
import { handleBreadcrumbCreate } from '../utils';
import ShopGiftBundles from './shopGiftBundles';

function GiftBundles() {
  const dispatch = useDispatch();
  const { cms, countries } = useSelector(state => state);

  const [dataState, setDataState] = useMergeState({
    buttonBlocks: [],
    iconBlocks: [],
    textBlocks: [],
    textBlocksHeader: [],
    imageBlocks: [],
    productBlocks: [],
    faqBlocks: [],
    breadcrumb: [],
    reviews: []
  });

  const handleGetCMSData = (data) => {
    if (!isEmpty(data)) {
      const products = _.filter(data.body, x => x.type === 'product_block' && x.value.text === 'gift_bundles');
      // random 4 gift bundles to display on shop gift bundles
      const productBlocks = _.sampleSize(products, 6);

      const iconBlocks = _.filter(data.body, x => x.type === 'icons_block');
      const reviewCms = _.find(iconBlocks, x => x?.value?.image_background?.caption === `our_client_bundles`);
      let reviews = [];

      if(!isEmpty(reviewCms)) {
        reviews = _.map(reviewCms?.value?.icons, x => ({
          name: x?.value?.image?.caption,
          comment: x?.value?.text,
          giftType: x?.value?.header?.header_text,
        }));
      }

      const buttonBlocks = _.filter(data.body, x => x.type === 'button_block');
      const imageBlocks = _.filter(data.body, x => x.type === 'image_block');

      const menuBlocks = _.find(data.body, x => x.type === 'menu_block' && x?.value?.text === 'bundles_breadcrumb');
      // create breadcrumb;
      const breadcrumb = handleBreadcrumbCreate(menuBlocks?.value?.submenu, auth.getCountry());

      const textBlocks = _.find(data.body, x => x.type === 'texts_block' && x.value.header.header_text !== 'title');
      const textBlocksHeader = _.find(data.body, x => x.type === 'texts_block' && x.value.header.header_text === 'title');
      const faqData = _.find(data.body, x => x.type === 'faqs_block');
      const faqFilter = _.filter(faqData.value, x => x.value?.header?.header_text === 'gift_bundles_faq');
      const faqBlocks = _.map(faqFilter, x => ({ ...x, value: { ...x.value, faqs: x.value.faq } }));

      return {
        iconBlocks,
        productBlocks,
        buttonBlocks,
        imageBlocks,
        textBlocks,
        textBlocksHeader,
        faqBlocks,
        breadcrumb,
        reviews
      };
    }
    return {};
  };

  const handleFetchDataInit = async () => {
    const cmsBlock = _.find(cms, x => x.title === 'GiftV2');
    let cmsData = {};

    if (!cmsBlock) {
      dispatch(loadingPage(true));
      const cmsResponse = await fetchCMSHomepage('gift-v2');

      if (!isEmpty(cmsResponse)) {
        dispatch(addCmsRedux(cmsResponse));
        cmsData = handleGetCMSData(cmsResponse);
        dispatch(loadingPage(false));
      }
    } else {
      cmsData = handleGetCMSData(cmsBlock);
    }

    setDataState(cmsData);
  };

  useEffect(() => {
    handleFetchDataInit();
    segmentGiftingOfferPageViewed("Gift Collections");
    return () => {
      removeLinkHreflang();
      removeScriptFAQ();
    };
  }, []);

  useEffect(() => {
    if (dataState.faqBlocks?.length > 0) {
      generateScriptFAQ(dataState.faqBlocks, true);
    }
  }, [dataState.faqBlocks]);

  const howItWorksTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'how_it_works');
  const howItWorksData = _.find(dataState.iconBlocks, x => x?.value?.image_background?.caption === 'gift_bundles_how_it_works');
  const banner = _.find(dataState.imageBlocks, x => x?.value?.caption === 'gift_bundles_banner');
  const shopGiftTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'gift_bundles');

  const countryName = auth.getCountryName();
  const titleCms = getNameFromButtonBlock(dataState.buttonBlocks, 'seo_gift_bundles');
  const desCms = getLinkFromButtonBlock(dataState.buttonBlocks, 'seo_gift_bundles');
  const seoTitle = replaceCountryInMetaTags(countryName, titleCms);
  const seoDes = replaceCountryInMetaTags(countryName, desCms);
  const ourClientTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'our_client_gift_bundles');
  const ourClientDes = getLinkFromButtonBlock(dataState.buttonBlocks, 'our_client_gift_bundles');

  return (
    <div className="gift-type gift-bundles">
      <MetaTags>
        <title>
          {seoTitle}
        </title>
        <meta name="description" content={seoDes} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/gift/gift-collections')}
      </MetaTags>

      <HeaderHomePageV3 />

      <div className="gift-wrapper">
        <Breadcrumb breadcrumb={dataState.breadcrumb} />
      </div>

      <GiftBanner
        classNames="gift-bundles-banner gift-dark-banner"
        bannerUrlDesktop={banner?.value?.image}
        bannerUrlMobile={banner?.value?.image}
      >
        <div className="wrapper">
          <div className="block-content-title main-title">
            {dataState.textBlocksHeader?.value?.texts?.length > 0 ? dataState.textBlocksHeader?.value?.texts[1]?.value : ''}
          </div>
          <div className="block-content-description">
            {dataState.textBlocks?.value?.texts?.length > 0 ? dataState.textBlocks?.value?.texts[1]?.value : ''}
          </div>
        </div>
      </GiftBanner>

      <div className="gift-content">
        <HowItWorks
          data={howItWorksData}
          title={howItWorksTitle}
          isLandingPage={false}
        />

        <ShopGiftBundles
          title={shopGiftTitle}
          datas={dataState.productBlocks}
          buttonBlocks={dataState.buttonBlocks}
          totalInRow={3} // display 3 bundles in row
        />

        {/* <OurClients
          buttonBlocks={dataState.buttonBlocks}
          iconBlocks={dataState.iconBlocks}
          typeOfGift='bundles'
        /> */}

        <ReviewsV3
          dataFAQ={dataState}
          buttonBlocks={dataState.buttonBlocks}
          pageType='gift'
          title={ourClientTitle}
          description={ourClientDes}
        />

        <div className="gift-faqs">
          <BlockFAQV2
            dataFAQ={dataState.faqBlocks?.length > 0 ? dataState.faqBlocks[0]?.value : []}
            buttonBlocks={dataState.buttonBlocks}
          />
        </div>
      </div>

      <FooterV2 />
    </div>
  );
}

export default GiftBundles;
