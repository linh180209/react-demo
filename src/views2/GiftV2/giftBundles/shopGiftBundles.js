import _ from 'lodash';
import React from 'react';
import { useHistory } from 'react-router-dom';
import { generaCurrency, generateUrlWeb, getNameFromButtonBlock } from '../../../Redux/Helpers';
import { starOutline } from '../../../imagev2/svg';

function ShopGiftBundles(props) {
  const history = useHistory();
  const { title, datas, buttonBlocks, totalInRow } = props;
  const viewProductBtn = getNameFromButtonBlock(buttonBlocks, 'view_product');

  const handleAddToCartClick = (data) => {
    history.push(generateUrlWeb(`/gift/gift-collections/${data?.value?.product?.id}`));
  };

  return (
    <div className="shop-gift-bundles padding-y">
      <div className="main-title margin-y-b">{title}</div>
      <div className={`gift-bundles total-in-row-${totalInRow}`}>
        {
          _.map(datas || [], (x, idx) => (
            <div key={idx} className="bundle-item">
              <div className="bundle-image" onClick={() => handleAddToCartClick(x)}>
                <img src={_.find(x?.value?.product?.images, d => d.type === 'main')?.image} loading="lazy" alt="gift" />
              </div>
              <div className="bundle-detail">
                <div className='bundle-actual-name'>
                  {x?.value?.product?.actual_type_name && (
                    <React.Fragment>
                      <img src={starOutline} alt='icon' />
                      {x?.value?.product?.actual_type_name}
                    </React.Fragment>
                  )}
                </div>
                <div className="bundle-name">{x?.value?.product?.name}</div>
                <div className="bundle-description">{x?.value?.product?.short_description}</div>
                <div className="bundle-description2">{x?.value?.product?.short_description2}</div>
                <div className="bundle-price">
                  {generaCurrency(x?.value?.product?.items?.length > 0 ? x?.value?.product?.items[0]?.price : x?.value?.price)}
                </div>
              </div>
              <button className="bundle-btn" type="button" onClick={() => handleAddToCartClick(x)}>
                {viewProductBtn}
              </button>
            </div>
          ))
        }
      </div>
    </div>
  );
}

export default ShopGiftBundles;
