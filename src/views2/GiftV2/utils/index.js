import _ from "lodash";
import { replaceCountryInMetaTags } from "../../../Utils/replaceCountryInMetaTags";

export const handleBreadcrumbCreate = (data, countryCode) => {
  const breadcrumb = [];
  _.map(data, x => breadcrumb.push({
    name: x?.value?.text,
    url: replaceCountryInMetaTags(countryCode, x?.value?.link),
  }));

  return breadcrumb;
}