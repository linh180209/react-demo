export const GIFT_USE_RECIPIENT_FORM = ['bespoke_perfume', 'Evoucher', 'gift_workshop'];
export const GIFT_DOES_NOT_NEED_SHIPPING = ['Evoucher', 'gift_workshop'];
export const GIFT_USE_GIFTING_DETAIL_FORM = ['gift_bundles', 'Physical'];
export const GIFT_RECIPIENT_FORM = 'gift_recipient_form';
export const GIFT_DETAIL_FORM = 'gift_detail_form';
export const GIFT_AVAILABLE_IN_COUNTRIES = ['vn', 'sg', 'ae'];
export const GIFT_FORM_STATUS = {
  SENDER_FORM: 1,
  RECIPIENT_FORM: 2,
  EXISTING_FORM: 3,
};
export const [GIFT_E_VOUCHER, GIFT_PHYSICAL_VOUCHER, GIFT_BESPOKE_PERFUME, GIFT_WORKSHOP, GIFT_BUNDLES] = ['Evoucher', 'Physical', 'bespoke_perfume', 'gift_workshop', 'gift_bundles'];
export const QUANTITY_OPTIONS = [
  { value: 1, label: '1' },
  { value: 2, label: '2' },
  { value: 3, label: '3' },
  { value: 4, label: '4' },
  { value: 5, label: '5' },
  { value: 6, label: '6' },
  { value: 7, label: '7' },
  { value: 8, label: '8' },
];
export const COLLECTIONS = {
  DESCRIPTION_TAB: 1,
  SCENT_CHARACTERISTICS_TAB: 2,
  COLLECTION_TABS: [
    { id: 1, name: 'Description'},
    { id: 2, name: 'Scent Characteristics'},
  ],
}
export const IS_GIFTING = ['Evoucher', 'Physical', 'bespoke_perfume', 'gift_workshop', 'gift_bundles'];
export const [CUSTOM_AMOUNT_FROM, CUSTOM_AMOUNT_TO] = [10, 1000];
