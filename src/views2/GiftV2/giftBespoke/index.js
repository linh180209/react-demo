import _, { isEmpty } from 'lodash';
import React, { useEffect, useState } from 'react';
import MetaTags from 'react-meta-tags';
import { useDispatch, useSelector } from 'react-redux';
import HeaderHomePageV3 from '../../../components/HomePage/HeaderHomePageV3';
import ModalVideo from '../../../components/ModalVideo';
import BlockFAQV2 from '../../../componentsv2/blockFAQV2';
import { GET_SCENT_LIB_GLOSSAIRE } from '../../../config';
import { isMobile991fn } from '../../../DetectScreen';
import addCmsRedux from '../../../Redux/Actions/cms';
import updateIngredientsData from '../../../Redux/Actions/ingredients';
import loadingPage from '../../../Redux/Actions/loading';
import {
  fetchCMSHomepage, generateHreflang, getLinkFromButtonBlock, getNameFromButtonBlock, removeLinkHreflang, segmentGiftingOfferPageViewed
} from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import { generateScriptFAQ, removeScriptFAQ } from '../../../Utils/addScriptSchema';
import { useMergeState } from '../../../Utils/customHooks';
import { replaceCountryInMetaTags } from '../../../Utils/replaceCountryInMetaTags';
import useWindowEvent from '../../../Utils/useWindowEvent';
import ReviewsV3 from '../../../views/ProductB2C/reviewsV3';
import FooterV2 from '../../footer';
import Breadcrumb from '../components/breadcrumb';
import GiftBanner from '../components/giftBanner';
import HowItWorks from '../components/howItWorks';
import '../giftv2.scss';
import { handleBreadcrumbCreate } from '../utils';
import GiftExperience from './giftExperience';
import GiftImage from './giftImage';
import ScrollInteraction from './scrollInteraction';
import './styles.scss';

const handleGetCMSData = (data) => {
  if (data) {
    const iconBlocks = _.filter(data.body, x => x.type === 'icons_block');
    const reviewCms = _.find(iconBlocks, x => x?.value?.image_background?.caption === `our_client_bespoke_perfume`);
    let reviews = [];

    if(!isEmpty(reviewCms)) {
      reviews = _.map(reviewCms?.value?.icons, x => ({
        name: x?.value?.image?.caption,
        comment: x?.value?.text,
        giftType: x?.value?.header?.header_text,
      }));
    }
    const buttonBlocks = _.filter(data.body, x => x.type === 'button_block');
    const imageBlocks = _.filter(data.body, x => x.type === 'image_block');
    const textBlocks = _.find(data.body, x => x.type === 'texts_block' && x.value.header.header_text !== 'title' && x.value?.header?.header_text !== 'gift_bespoke_include');
    const textBlocksHeader = _.find(data.body, x => x.type === 'texts_block' && x?.value?.header?.header_text === 'title');
    const textInclude = _.find(data.body, x => x.type === 'texts_block' && x?.value?.header?.header_text === 'gift_bespoke_include');
    const faqData = _.find(data.body, x => x.type === 'faqs_block');
    const faqFilter = _.filter(faqData.value, x => x.value?.header?.header_text === 'gift_bespoke_perfume_faq');
    const faqBlocks = _.map(faqFilter, x => ({ ...x, value: { ...x.value, faqs: x.value.faq } }));
    const productBlock = _.find(data.body, x => x.type === 'product_block' && x.value.image?.caption === 'bespoke_perfume');
    const menuBlocks = _.find(data.body, x => x?.type === 'menu_block' && x?.value?.text === 'bespoke_perfume_breadcrumb');
    const breadcrumb = handleBreadcrumbCreate(menuBlocks?.value?.submenu, auth.getCountry());

    return {
      iconBlocks,
      buttonBlocks,
      imageBlocks,
      textBlocks,
      textBlocksHeader,
      faqBlocks,
      textInclude,
      productBlock,
      breadcrumb,
      reviews,
    };
  }
  return {};
};

function GiftBespoke() {
  const cms = useSelector(state => state.cms);
  const countries = useSelector(state => state.countries);
  const ingredients = useSelector(state => state.ingredients);

  const dispatch = useDispatch();

  const [dataState, setDataState] = useMergeState({
    buttonBlocks: [],
    iconBlocks: [],
    textBlocks: [],
    textBlocksHeader: [],
    imageBlocks: [],
    productBlock: {},
    faqBlocks: [],
    textInclude: {},
    breadcrumb: [],
    reviews: [],
  });
  const [isVisible, setVisible] = useState(false);
  const [isMobile, setMobile] = useState(false);

  const fetchListScent = () => {
    const options = {
      url: GET_SCENT_LIB_GLOSSAIRE,
      method: 'GET',
    };
    return fetchClient(options);
  };

  const handleFetchDataInit = async () => {
    const cmsBlock = _.find(cms, x => x.title === 'GiftV2');
    let cmsData = {};

    if (!cmsBlock || ingredients.length === 0) {
      const pending = [fetchCMSHomepage('gift-v2'), fetchListScent()];
      dispatch(loadingPage(true));

      try {
        const result = await Promise.all(pending);
        dispatch(addCmsRedux(result[0]));
        dispatch(updateIngredientsData(result[1]));
        cmsData = handleGetCMSData(result[0]);
      } catch (error) {
        toastrError(error.message);
      }
      dispatch(loadingPage(false));
    } else {
      cmsData = handleGetCMSData(cmsBlock);
    }
    setDataState(cmsData);
  };

  const handleWatchVideoClick = () => {
    setVisible(true);
  };

  useEffect(() => {
    handleFetchDataInit();
    segmentGiftingOfferPageViewed("Gift Personalised Perfume Experience");
    return () => {
      removeLinkHreflang();
      removeScriptFAQ();
    };
  }, []);

  useEffect(() => {
    if (dataState.faqBlocks?.length > 0) {
      generateScriptFAQ(dataState.faqBlocks, true);
    }
  }, [dataState.faqBlocks]);

  const handleInitResize = () => {
    if(isMobile991fn()) setMobile(true);
    else setMobile(false);
  };

  useWindowEvent('resize', handleInitResize);

  const seoCmsTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'seo_gift_bespoke_perfume');
  const seoCmsDes = getLinkFromButtonBlock(dataState.buttonBlocks, 'seo_gift_bespoke_perfume');
  const countryName = auth.getCountryName();
  const seoTitle = replaceCountryInMetaTags(countryName, seoCmsTitle);
  const seoDes = replaceCountryInMetaTags(countryName, seoCmsDes);

  const howItWorksTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'how_it_works');
  const howItWorksData = _.find(dataState.iconBlocks, x => x?.value?.image_background?.caption === 'gift_bespoke_how_it_works');
  const banner = _.find(dataState.imageBlocks, x => x?.value?.caption === 'gift_bespoke_perfume_banner');
  const bannerVideo = getNameFromButtonBlock(dataState.buttonBlocks, 'gift_bespoke_banner_video');
  const bannerVideoUrl = getLinkFromButtonBlock(dataState.buttonBlocks, 'gift_bespoke_banner_video');
  const mixMatchTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'gift_bespoke_mix_match');
  const customBottleTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'gift_bespoke_infinite_bottle_design');
  const customBottleImage = _.find(dataState.imageBlocks, x => x?.value?.caption === 'gift_bespoke_custom_bottle');
  const customBottleMobileImage = _.find(dataState.imageBlocks, x => x?.value?.caption === 'gift_bespoke_custom_bottle_mobile');
  const ourClientTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'our_client_gift_bespoke_perfume');
  const ourClientDes = getLinkFromButtonBlock(dataState.buttonBlocks, 'our_client_gift_bespoke_perfume');

  return (
    <div className="gift-bespoke">
      <MetaTags>
        <title>
          {seoTitle}
        </title>
        <meta name="description" content={seoDes} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/gift/bespoke-perfume')}
      </MetaTags>
      <HeaderHomePageV3 />

      <div className="gift-wrapper">
        <Breadcrumb breadcrumb={dataState.breadcrumb} />
      </div>

      <GiftBanner
        classNames="gift-bespoke-banner gift-dark-banner"
        bannerUrlDesktop={banner?.value?.image}
        bannerUrlMobile={banner?.value?.image}
      >
        <div className="bespoke-banner-wrapper wrapper">
          <div className="block-content-title main-title">
            {dataState.textBlocksHeader?.value?.texts?.length > 0 ? dataState.textBlocksHeader?.value?.texts[2]?.value : ''}
          </div>
          <div className="block-content-description">
            {dataState.textBlocks?.value?.texts?.length > 0 ? dataState.textBlocks?.value?.texts[2]?.value : ''}
          </div>
          {/* <div className="block-content-video" onClick={handleWatchVideoClick}>
            {bannerVideo}
            <img
              src={isMobile || isMobile480fn() ? icBlackVideo : icVideo}
              alt="gift-icon"
            />
          </div> */}
        </div>
      </GiftBanner>

      <div className="gift-content no-padding">
        <HowItWorks data={howItWorksData} title={howItWorksTitle} />
      </div>

      <ScrollInteraction
        title={mixMatchTitle}
        data={ingredients}
      />

      <div className="gift-content no-padding">
        <GiftImage
          title={customBottleTitle}
          imageUrlDesktop={customBottleImage?.value?.image}
          imageUrlMobile={customBottleMobileImage?.value?.image}
        />

        <GiftExperience data={dataState} isMobile={isMobile} />

        {/* <OurClients
          buttonBlocks={dataState.buttonBlocks}
          iconBlocks={dataState.iconBlocks}
          typeOfGift='bespoke_perfume'
        /> */}

        <ReviewsV3
          dataFAQ={dataState}
          buttonBlocks={dataState.buttonBlocks}
          pageType='gift'
          title={ourClientTitle}
          description={ourClientDes}
        />

        <div className="gift-faqs">
          <BlockFAQV2
            dataFAQ={dataState.faqBlocks?.length > 0 ? dataState.faqBlocks[0]?.value : []}
            buttonBlocks={dataState.buttonBlocks}
          />
        </div>
      </div>

      <ModalVideo
        isOpen={isVisible}
        limitHeight
        video={bannerVideoUrl}
        poster={banner?.value?.image}
        onClose={() => setVisible(false)}
      />
      <FooterV2 />
    </div>
  );
}

export default GiftBespoke;
