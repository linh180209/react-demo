import Drawer from '@mui/material/Drawer';
import _, { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import HeaderHomePageV3 from '../../../../components/HomePage/HeaderHomePageV3';
import { isMobile991fn } from '../../../../DetectScreen';
import { addProductBasket, createBasketGuest } from '../../../../Redux/Actions/basket';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import auth from '../../../../Redux/Helpers/auth';
import '../../../../styles/product-detail.scss';
import ImageBlock from '../../../../views/ProductB2C/imageBlock';
import { GIFT_BESPOKE_PERFUME, GIFT_RECIPIENT_FORM, } from '../../constants';
import DesBlock from '../../giftBundlesDetail/desBlock';
import DetailBlock from '../../giftBundlesDetail/detailBlock';
import '../../giftBundlesDetail/styles.scss';
import '../../giftv2.scss';

function GiftExperience({ data, isMobile }) {
  const dispatch = useDispatch();
  const basket = useSelector(state => state.basket);

  const giftBespokeCms = _.find(data.iconBlocks, x => x?.value?.image_background?.caption === 'gift_bespoke_perfume_slide');
  const giftBespokeSlider = _.map(giftBespokeCms?.value?.icons, x => ({ original: x?.value?.image?.image, thumbnail: x?.value?.image?.image }));
  const giftBespokeInclude = getNameFromButtonBlock(data.buttonBlocks, 'gift_include');
  const giftBespokePerfumeName = getNameFromButtonBlock(data.buttonBlocks, 'gift_bespoke_pefume_name');

  const giftDetail = {
    id: data.productBlock?.value?.product?.items?.length > 0 ? data.productBlock?.value?.product?.items[0]?.id : '',
    name: data?.productBlock?.value?.text,
    description: data?.productBlock?.value?.description,
    price: data?.productBlock.value?.product?.items?.length > 0 ? data?.productBlock?.value?.product?.items[0]?.price : data?.productBlock?.value?.price,
    type: GIFT_BESPOKE_PERFUME,
    image: giftBespokeSlider?.length > 0 ? giftBespokeSlider[0]?.original : '',
  };

  const [orderItem, setOrderItem] = useState({
    quantity: 1,
    info: {},
  });
  const [isOpenDrawer, setOpenDrawer] = useState(false);

  const handleClearOrderItem = () => {
    setOrderItem(prevOrder => ({ ...prevOrder, info: {}, quantity: 1 }));
  };

  const handleChangeQuantity = (e) => {
    const { value } = e.target;
    const quantity = parseInt(value, 10);
    setOrderItem(prevOrder => ({ ...prevOrder, quantity }));
  };

  const handleSaveInfo = (data) => {
    const { senderDetails, recipientDetails, isCreateNewGift } = data;
    const price = giftDetail.price * orderItem.quantity;

    const info = {
      name: giftDetail.name,
      currency: auth.getNameCurrency(),
      price_custom: price,
      quantity: orderItem.quantity,
      item: giftDetail.id,
      meta: {
        giftName: giftBespokePerfumeName,
        type: GIFT_RECIPIENT_FORM,
        typeOfGift: GIFT_BESPOKE_PERFUME,
        isCreateNewGift,
        sender: {
          name: senderDetails.senderName,
          email: senderDetails.senderEmail,
        },
        receiver: {
          name: recipientDetails.recipientName,
          email: recipientDetails.recipientEmail,
          country: recipientDetails.recipientCountry || auth.getCountry(),
          date_sent: moment(recipientDetails.dateOfEmail).valueOf() / 1000,
          note: senderDetails.note,
          phone: recipientDetails.recipientPhone,
        },
      },
    };

    const dataTemp = {
      idCart: basket.id,
      item: info,
    };

    if (!basket.id) {
      dispatch(createBasketGuest(dataTemp));
    } else {
      dispatch(addProductBasket(dataTemp));
    }

    handleClearOrderItem();
  };

  const handleAddedToCart = (data) => {
    setOrderItem(prevOrder => ({ ...prevOrder, info: data }));

    if (isMobile || isMobile991fn()) {
      setOpenDrawer(true);
    } else {
      setOpenDrawer(false);
    }
  };

  const handleCloseDrawer = () => {
    setOpenDrawer(false);
    handleClearOrderItem();
  };

  return (
    <div className="gift-detail-v2 padding-y">
      <div className="block-info no-padding">
        <div className="image-slider">
          <ImageBlock
            images={giftBespokeSlider}
            datas={[]}
            dataCustom={{}}
            className="product-detail-v2 product-detail-v3"
          />
        </div>
        <div className="block-description block-common">
          {isEmpty(orderItem.info) ? (
            <DesBlock
              includeTitle={giftBespokeInclude}
              includeDes={data.textInclude?.value?.texts || []}
              giftDetail={giftDetail}
              buttonBlocks={data.buttonBlocks}
              quantity={orderItem.quantity}
              isDrawer={isMobile || isMobile991fn()}
              onChangeQuantity={handleChangeQuantity}
              onAddToCart={handleAddedToCart}
            />
          ) : (
            (isMobile || isMobile991fn()) ? (
              <Drawer
                open={isOpenDrawer}
                className="gift-drawer"
                anchor="right"
              >
                <HeaderHomePageV3 />
                <div className="block-description">
                  {!isEmpty(orderItem.info) && (
                    <DetailBlock
                      isDrawer
                      giftDetail={giftDetail}
                      basket={basket}
                      buttonBlocks={data.buttonBlocks}
                      onClearGiftState={handleClearOrderItem}
                      onSaveInfo={handleSaveInfo}
                      onCloseDrawer={handleCloseDrawer}
                      isMobile={isMobile}
                    />
                  )}
                </div>
              </Drawer>
            ) : (
              <DetailBlock
                giftDetail={giftDetail}
                basket={basket}
                buttonBlocks={data.buttonBlocks}
                onClearGiftState={handleClearOrderItem}
                onSaveInfo={handleSaveInfo}
                isMobile={isMobile}
              />
            )
          )}
        </div>
      </div>
    </div>
  );
}

export default GiftExperience;
