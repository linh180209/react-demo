import React, { useState } from 'react';
import { isMobile767fn } from '../../../../DetectScreen';
import useWindowEvent from '../../../../Utils/useWindowEvent';
import './styles.scss';

function GiftImage({ title, imageUrlDesktop, imageUrlMobile }) {
  const [isMobile, setMobile] = useState(false);

  const handleInitResize = () => {
    if(isMobile767fn()) setMobile(true);
    else setMobile(false);
  };

  useWindowEvent('resize', handleInitResize, window);

  return (
    <div className="gift-bespoke-image">
      <div className="bespoke-image-wrapper">
        <img loading="lazy" src={isMobile || isMobile767fn() ? imageUrlMobile : imageUrlDesktop} alt="gift" />
      </div>
      <div className="bespoke-image-content">
        <div className="main-title">{title}</div>
      </div>
    </div>
  );
}

export default GiftImage;
