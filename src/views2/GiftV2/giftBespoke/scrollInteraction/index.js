import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { isMobile480fn } from '../../../../DetectScreen';
import '../../giftv2.scss';
import './styles.scss';

function ScrollInteraction({ title, data }) {
  const [y, setY] = useState(window.screenY);

  const handleScrollInteraction = (e) => {
    const window = e.currentTarget;
    setY(window.scrollY);
  };

  useEffect(() => {
    window.addEventListener('scroll', e => handleScrollInteraction(e));

    return () => {
      window.removeEventListener('scroll', e => handleScrollInteraction(e));
    };
  }, []);

  const handleRendersScrollItems = () => {
    // Mobile -> map 3, desktop -> map 2
    const totalLines = isMobile480fn() ? 3 : 2;
    const totalItems = parseInt(data.length / totalLines, 10);
    let from = 0;
    let to = 0;
    const dom = [];

    for (let i = 0; i < totalLines; i += 1) {
      from = to;
      to = ((i + 1) * totalItems);
      if (i === totalLines - 1) to += (data.length - to);

      const list = data.slice(from, to);
      dom.push(
        <div
          className="block-scents-inline"
          style={{
            willChange: 'transform',
            transformStyle: 'preserve-3d',
            transform: `translate3d(${(i + 1) % 2 === 0 ? '-' : ''}${y / 120}%, 0px, 0px) scale3d(1, 1, 1)`,
          }}
        >
          {_.map(list, (scent) => {
            if (scent?.images?.length > 0) {
              const scentImage = _.find(scent?.images, x => x.type === 'main');
              return (
                <div key={scent.id} className="block-scents-item" style={{ width: `calc(100vw / ${list.length})` }}>
                  <img loading="lazy" src={scentImage?.image} alt="gift" />
                </div>
              );
            }
          })}
        </div>,
      );
    }
    return dom;
  };

  return (
    <div className="scroll-interaction padding-y">
      <div className="main-title margin-y-b">{title}</div>
      <div className="block-scents">
        {handleRendersScrollItems().map(item => item)}
      </div>
    </div>
  );
}

export default ScrollInteraction;
