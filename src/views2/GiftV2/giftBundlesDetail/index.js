import Drawer from '@mui/material/Drawer';
import _, { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams, withRouter } from 'react-router-dom';
import HeaderHomePageV3 from '../../../components/HomePage/HeaderHomePageV3';
import BlockFAQV2 from '../../../componentsv2/blockFAQV2';
import { GET_PRODUCT_URL } from '../../../config';
import { isMobile991fn } from '../../../DetectScreen';
import { addProductBasket, createBasketGuest } from '../../../Redux/Actions/basket';
import addCmsRedux from '../../../Redux/Actions/cms';
import loadingPage from '../../../Redux/Actions/loading';
import {
  fetchCMSHomepage, getLinkFromButtonBlock, getNameFromButtonBlock, segmentGiftingCollectionItemPageViewed
} from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { useMergeState } from '../../../Utils/customHooks';
import useWindowEvent from '../../../Utils/useWindowEvent';
import ImageBlock from '../../../views/ProductB2C/imageBlock';
import ReviewsV3 from '../../../views/ProductB2C/reviewsV3';
import FooterV2 from '../../footer';
import Breadcrumb from '../components/breadcrumb';
import { COLLECTIONS, GIFT_BUNDLES, GIFT_DETAIL_FORM } from '../constants';
import ShopGiftBundles from '../giftBundles/shopGiftBundles';
import '../giftv2.scss';
import { handleBreadcrumbCreate } from '../utils';
import DesBlock from './desBlock';
import DesScent from './desScent';
import DetailBlock from './detailBlock';
import './styles.scss';

function GiftBundlesDetail() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const cms = useSelector(state => state.cms);
  const basket = useSelector(state => state.basket);

  const [dataState, setDataState] = useMergeState({
    buttonBlocks: [],
    productBlocks: [],
    giftBundles: [],
    faqBlocks: [],
    menuBlocks: {},
    iconBlocks: [],
    reviews: [],
    collectionDetail: {},
  });
  const [orderItem, setOrderItem] = useState({
    info: {},
    quantity: 1,
  });
  const [isOpenDrawer, setOpenDrawer] = useState(false);
  const [isMobile, setMobile] = useState(false);
  const [tabActive, setTabActive] = useState(COLLECTIONS.DESCRIPTION_TAB);

  useEffect(() => {
    const runThis = async () => {
      await handleFetchDataInit();
      await handleFetchCollectionDetail();

      const gift = dataState.productBlocks?.find?.(x => x.value?.product?.id == id);
      const product = gift?.value?.product?.items?.length > 0 ? gift?.value?.product?.items[0] : {};

      const giftDetail = {
        id: product?.id || '',
        name: gift?.value?.product?.name,
        price: product?.price || gift?.value?.price,
        type: gift?.value?.text,
      };
      segmentGiftingCollectionItemPageViewed(giftDetail);
    };

    runThis();

    const unlisten = history.listen(() => {
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    });

    return () => {
      unlisten();
      setOrderItem(prevOrder => ({ ...prevOrder, info: {}, quantity: 1 }));
      setDataState({});
      setTabActive(COLLECTIONS.DESCRIPTION_TAB);
    };
  }, [id]);

  const handleInitResize = () => {
    handleClearGiftState();

    if (isMobile991fn()) setMobile(true);
    else setMobile(false);
  };

  useWindowEvent('resize', handleInitResize, window);

  const gift = _.find(dataState.productBlocks, x => x.value?.product?.id == id);
  const product = gift?.value?.product?.items?.length > 0 ? gift?.value?.product?.items[0] : {};

  const giftDetail = {
    id: product?.id || '',
    name: gift?.value?.product?.name,
    description: gift?.value?.product?.description,
    price: product?.price || gift?.value?.price,
    type: gift?.value?.text,
  };
  const images = gift?.value?.product?.images?.map(x => ({
    original: x?.image,
    thumbnail: x?.image,
  }));

  const handleGetCMSData = (data) => {
    if (data) {
      const productBlocks = _.filter(data.body, x => x.type === 'product_block' && x.value.text === 'gift_bundles');
      const productFilter = _.filter(productBlocks, x => x?.value?.product?.id != id);
      // random 4 products to display on shop gift bundles
      const giftBundles = _.sampleSize(productFilter, 4);

      const buttonBlocks = _.filter(data.body, x => x.type === 'button_block');
      const iconBlocks = _.filter(data.body, x => x.type === 'icons_block');
      const reviewCms = _.find(iconBlocks, x => x?.value?.image_background?.caption === 'our_client_bundles');
      let reviews = [];

      if (!isEmpty(reviewCms)) {
        reviews = _.map(reviewCms?.value?.icons, x => ({
          name: x?.value?.image?.caption,
          comment: x?.value?.text,
          giftType: x?.value?.header?.header_text,
        }));
      }
      const menuBlocks = _.find(data.body, x => x.type === 'menu_block' && x?.value?.text === 'bundles_breadcrumb');
      const faqData = _.find(data.body, x => x.type === 'faqs_block');
      const faqFilter = _.filter(faqData.value, x => x.value?.header?.header_text === 'gift_bundles_faq');
      const faqBlocks = _.map(faqFilter, x => ({ ...x, value: { ...x.value, faqs: x.value.faq } }));
      return {
        productBlocks,
        buttonBlocks,
        faqBlocks,
        menuBlocks,
        giftBundles,
        iconBlocks,
        reviews,
      };
    }
    return {};
  };

  const handleFetchDataInit = async () => {
    const cmsBlock = _.find(cms, x => x.title === 'GiftV2');
    let cmsData = {};

    if (isEmpty(cmsBlock)) {
      dispatch(loadingPage(true));

      try {
        const cmsResponse = await fetchCMSHomepage('gift-v2');
        dispatch(addCmsRedux(cmsResponse));
        cmsData = handleGetCMSData(cmsResponse);

        dispatch(loadingPage(false));
      } catch (error) {
        dispatch(loadingPage(false));
      }
    } else {
      cmsData = handleGetCMSData(cmsBlock);
    }

    setDataState(cmsData);
  };

  const handleFetchCollectionDetail = async () => {
    dispatch(loadingPage(true));

    try {
      const response = await fetchProduct(id);

      if(!isEmpty(response)) {
        console.log(response);
        setDataState({collectionDetail: response});
      }
      dispatch(loadingPage(false));
    } catch (error) {
      dispatch(loadingPage(false));
    }
  }

  const fetchProduct = (id) => {
    const options = {
      url: GET_PRODUCT_URL.replace('{id}', id),
      method: 'GET',
    };
    return fetchClient(options);
  }

  const handleGenerateBreadcrumb = (menus, products) => {
    const countryCode = auth.getCountry();
    const breadcrumb = handleBreadcrumbCreate(menus?.value?.submenu, countryCode);
    const product = _.find(products, x => x?.value?.product?.id == id);

    const giftBread = {
      name: product?.value?.product?.name,
      url: '',
    };
    breadcrumb.push(giftBread);

    return breadcrumb;
  };

  const handleAddToCart = (data) => {
    setOrderItem(prevOrder => ({ ...prevOrder, info: data }));

    if (isMobile || isMobile991fn()) {
      setOpenDrawer(true);
    } else {
      setOpenDrawer(false);
    }
  };

  const handleChangeQuantity = (e) => {
    const { value } = e.target;
    const quantity = parseInt(value, 10);
    setOrderItem(prevOrder => ({ ...prevOrder, quantity }));
  };

  const handleSaveInfo = (data) => {
    const { giftingDetails, isCreateNewGift } = data;
    const price = giftDetail.price * orderItem.quantity;

    const info = {
      name: giftDetail.name,
      currency: auth.getNameCurrency(),
      price_custom: price,
      quantity: orderItem.quantity,
      item: giftDetail.id,
      meta: {
        type: GIFT_DETAIL_FORM,
        typeOfGift: GIFT_BUNDLES,
        isCreateNewGift,
        sender: {
          name: giftingDetails.from,
        },
        receiver: {
          name: giftingDetails.to,
          date_sent: moment(new Date()).valueOf() / 1000,
          note: giftingDetails.note,
          country: auth.getCountry()
        },
      },
    };

    const dataTemp = {
      idCart: basket.id,
      item: info,
    };

    if (!basket.id) {
      dispatch(createBasketGuest(dataTemp));
    } else {
      dispatch(addProductBasket(dataTemp));
    }

    setOrderItem(prevOrder => ({
      ...prevOrder,
      info: {},
      quantity: 1,
    }));
  };

  const handleCloseDrawer = () => {
    setOpenDrawer(false);
    handleClearGiftState();
  };

  const handleClearGiftState = () => {
    setOrderItem(prevOrder => ({ ...prevOrder, info: {} }));
  };

  const shopGiftTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'shop_other_gifts');
  const breadcrumb = handleGenerateBreadcrumb(dataState.menuBlocks, dataState.productBlocks);
  const ourClientTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'our_client_gift_bundles');
  const ourClientDes = getLinkFromButtonBlock(dataState.buttonBlocks, 'our_client_gift_bundles');

  return (
    <div>
      <HeaderHomePageV3 />
      <div className="gift-detail-v2">
        <div className="gift-wrapper">
          <Breadcrumb breadcrumb={breadcrumb} />
        </div>

        <div className="gift-content">
          <div className="block-info">
            <div className="image-slider">
              <ImageBlock
                images={images}
                datas={[]}
                dataCustom={{}}
                className="product-detail-v2 product-detail-v3"
              />
            </div>
            <div className="block-description">
              {isEmpty(orderItem.info) ? (
                <React.Fragment>
                  <DesScent collectionDetail={dataState.collectionDetail} />
                  <DesBlock
                    collectionId={id}
                    tabCollectionActive={tabActive}
                    giftDetail={giftDetail}
                    buttonBlocks={dataState.buttonBlocks}
                    collectionDetail={dataState.collectionDetail}
                    quantity={orderItem.quantity}
                    isDrawer={isMobile || isMobile991fn()}
                    onChangeQuantity={handleChangeQuantity}
                    onAddToCart={handleAddToCart}
                    onTabClick={(value) => setTabActive(value)}
                  />
                </React.Fragment>
              )
                : (
                  (isMobile || isMobile991fn()) ? (
                    <Drawer
                      open={isOpenDrawer}
                      className="gift-drawer"
                      anchor="right"
                    >
                      <HeaderHomePageV3 />
                      <div className="block-description">
                        {!isEmpty(orderItem.info) && (
                        <DetailBlock
                          isDrawer
                          giftDetail={giftDetail}
                          basket={basket}
                          buttonBlocks={dataState.buttonBlocks}
                          onClearGiftState={handleClearGiftState}
                          onSaveInfo={handleSaveInfo}
                          onCloseDrawer={handleCloseDrawer}
                          isMobile={isMobile}
                        />
                        )}
                      </div>
                    </Drawer>
                  ) : (
                    <DetailBlock
                      giftDetail={giftDetail}
                      basket={basket}
                      buttonBlocks={dataState.buttonBlocks}
                      onClearGiftState={() => setOrderItem(prevOrder => ({ ...prevOrder, info: {} }))}
                      onSaveInfo={handleSaveInfo}
                      isMobile={isMobile}
                    />
                  )
                )}
            </div>
          </div>

          <ShopGiftBundles
            title={shopGiftTitle}
            datas={dataState.giftBundles}
            buttonBlocks={dataState.buttonBlocks}
            totalInRow={4} // display 4 bundles in row.
          />

          {/* <OurClients
            buttonBlocks={dataState.buttonBlocks}
            iconBlocks={dataState.iconBlocks}
            typeOfGift='bundles'
          /> */}

          <ReviewsV3
            dataFAQ={dataState}
            buttonBlocks={dataState.buttonBlocks}
            pageType="gift"
            title={ourClientTitle}
            description={ourClientDes}
          />

          <div className="gift-faqs">
            <BlockFAQV2
              dataFAQ={dataState.faqBlocks?.length > 0 ? dataState.faqBlocks[0]?.value : []}
              buttonBlocks={dataState.buttonBlocks}
            />
          </div>
        </div>
      </div>
      <FooterV2 />
    </div>

  );
}

export default withRouter(GiftBundlesDetail);
