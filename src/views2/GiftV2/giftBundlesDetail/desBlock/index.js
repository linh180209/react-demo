import classnames from 'classnames';
import _ from 'lodash';
import React from 'react';
import reactHtmlPare from 'react-html-parser';
import ReceiveGrams from '../../../../components/receiveGrams';
import ButtonCT from '../../../../componentsv2/buttonCT';
import ScentCharacterV2 from '../../../../componentsv2/scentCharacterV2';
import icAdd from '../../../../image/icon/ic-add-product.svg';
import icSub from '../../../../image/icon/ic-sub-product.svg';
import { generaCurrency, getNameFromButtonBlock } from '../../../../Redux/Helpers';
import { COLLECTIONS, GIFT_BUNDLES, GIFT_PHYSICAL_VOUCHER } from '../../constants';
import './styles.scss';

function DesBlock(props) {
  const {
    giftDetail, buttonBlocks, includeTitle, includeDes, isDrawer, voucherDetail, voucherType, quantity, collectionId, collectionDetail, tabCollectionActive, onVoucherTypeClick, onChangeQuantity, onAddToCart, onTabClick
  } = props;

  const addToCartBtn = getNameFromButtonBlock(buttonBlocks, 'add_to_cart');
  const chooseButtonTypeTitle = getNameFromButtonBlock(buttonBlocks, 'choose your voucher type');

  const getImageMain = (data) => {
    if(data?.length > 0) {
      return _.find(data[0]?.product?.images, x => x.type === 'main')?.image;
    }
  }

  const renderQuanityV2 = (
    <div className={classnames('div-quantity')}>
      <div className="div-number">
        <button
          onClick={() => onChangeQuantity({ target: { value: quantity - 1 } })}
          className={classnames('button-bg__none', quantity < 2 ? 'disabled' : '')}
          type="button"
          disabled={quantity < 2}
        >
          <img src={icSub} alt="sub" />
        </button>

        <div className="quantity">
          {quantity}
        </div>
        <button
          onClick={() => onChangeQuantity({ target: { value: quantity + 1 } })}
          className="button-bg__none"
          type="button"
        >
          <img src={icAdd} alt="sub" />
        </button>
      </div>
    </div>
  );

  const renderDescriptionTab = () => (
    <React.Fragment>
      {collectionId && collectionDetail?.combo?.length > 0 && (
        <ul className='list-scents'>
          {_.map(collectionDetail?.combo.filter(x => x?.product?.combo?.length), y => (
            <li className='scent' key={y?.product?.name}>
              <img src={getImageMain(y?.product?.combo)} alt='image' />
              <div className='scent-name'>{y?.product?.name}</div>
            </li>
          ))}
        </ul>
      )}
      {collectionId && (
        <React.Fragment>
          <div className='des-properties'>
            <span>Mood: </span>{_.find(collectionDetail?.tags, x => x?.group_key === 'mood')?.name}
          </div>
          <div className='des-properties'>
            <span>Intensity: </span>
            <div className="list-point">
              {
                _.map(_.range(5), d => (
                  <div className={classnames('point-i', parseInt(collectionDetail?.profile?.intensity, 10) >= (d + 1) ? 'active' : '')} />
                ))
              }
            </div>
          </div>
        </React.Fragment>
      )}
      {giftDetail?.description && (
        <div className="des-text">
          {reactHtmlPare(giftDetail?.description)}
        </div>
      )}
    </React.Fragment>
  )

  return (
    <div className={`des-block ${!isDrawer ? 'animated faster fadeInRight' : ''}`}>
      <div className="title-text">
        {giftDetail?.name}
      </div>
      {collectionId && (
        <div className='des-tabs'>
          {_.map(collectionDetail?.combo?.length > 1 ? COLLECTIONS.COLLECTION_TABS : COLLECTIONS.COLLECTION_TABS.slice(0, 1), x => (
            <div
              className={`title-des ${tabCollectionActive === x.id ? 'active' : ''}`}
              key={x.id}
              onClick={() => onTabClick(x?.id)}
            >
              {x.name}
            </div>
          ))}
        </div>
      )}
      {tabCollectionActive === COLLECTIONS.DESCRIPTION_TAB && renderDescriptionTab()}
      {giftDetail?.description && giftDetail?.type !== GIFT_BUNDLES && (
        <div className="des-text">
          {reactHtmlPare(giftDetail?.description)}
        </div>
      )}
      {tabCollectionActive === COLLECTIONS.SCENT_CHARACTERISTICS_TAB && (
        <ScentCharacterV2
          datas={collectionDetail?.combo.filter(x => x?.product?.combo?.length)}
          pageType='gift'
        />
      )}

      {includeDes?.length > 0 && (
        <div className="div-gift-include">
          <div className="text-title">
            {includeTitle}
          </div>
          {_.map(includeDes, (x, idx) => (
            <div className="line-step" key={idx}>
              {x?.value}
            </div>
          ))}
        </div>
      )}
      {voucherType && (
        <div className="div-voucher-tabs">
          <div className="div-title">{chooseButtonTypeTitle}</div>
          <div className="div-group">
            {_.map(voucherType, x => (
              <ButtonCT
                key={x?.id}
                type="button"
                className={`div-btn ${x?.id === voucherDetail?.id && 'active'}`}
                name={x?.value?.text}
                onClick={() => onVoucherTypeClick(x)}
              />
            ))}
          </div>
          {voucherDetail?.id && (
            <div className="div-content">
              {voucherDetail?.value?.description ? reactHtmlPare(voucherDetail?.value?.description) : ''}
            </div>
          )}
        </div>
      )}

      <div className="div-point-price">
        <div className="price">
          {generaCurrency(giftDetail?.price)}
        </div>
        <ReceiveGrams
          isPointProductDetail
          buttonBlocks={buttonBlocks}
          price={giftDetail?.price}
        />
      </div>
      <div id="id-add-card-block" className="add-card-block">
        {renderQuanityV2}
        <ButtonCT
          name={addToCartBtn}
          onClick={onAddToCart}
          disabled={giftDetail?.class === 'custom' && voucherDetail?.value?.link === GIFT_PHYSICAL_VOUCHER}
        />
      </div>
    </div>
  );
}

export default DesBlock;
