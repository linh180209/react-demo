import React from 'react';
import './styles.scss';
import { icGiftCollections } from '../../../../imagev2/svg';

function DesScent({ collectionDetail }) {
  return (
    <ul className='scent-tags'>
      <li className='tag tag-has-icon'>
        <div className='icon'>
          <img src={icGiftCollections} alt='icon' />
        </div>
        <div className='name'>Gift Collections</div>
      </li>
      {collectionDetail?.actual_type?.alt_name && (
        <li className='tag'>
          <div className='name'>{collectionDetail.actual_type.alt_name}</div>
        </li>
      )}
    </ul>
  )
}

export default DesScent