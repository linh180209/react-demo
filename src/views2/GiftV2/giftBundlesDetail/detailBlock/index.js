import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import CheckIcon from '@mui/icons-material/Check';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import { TextareaAutosize } from '@mui/material';
import classnames from 'classnames';
import * as EmailValidator from 'email-validator';
import _, { isEmpty } from 'lodash';
import moment from 'moment';
import React, { memo, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ButtonCT from '../../../../componentsv2/buttonCT';
import InputCT from '../../../../componentsv2/inputCT';
import { GET_SHIPPING_URL } from '../../../../config';
import updateShippings from '../../../../Redux/Actions/shippings';
import { getLinkFromButtonBlock, getNameFromButtonBlock, segmentGiftingRecipientInfoViewed, segmentGiftingSenderInfoViewed } from '../../../../Redux/Helpers';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../../Redux/Helpers/notification';
import { useMergeState } from '../../../../Utils/customHooks';
import DatePickerCT from '../../components/datepickerCT';
import {
  GIFT_DETAIL_FORM, GIFT_DOES_NOT_NEED_SHIPPING, GIFT_FORM_STATUS, GIFT_RECIPIENT_FORM, GIFT_USE_GIFTING_DETAIL_FORM, GIFT_USE_RECIPIENT_FORM
} from '../../constants';
import './styles.scss';

const { SENDER_FORM, RECIPIENT_FORM, EXISTING_FORM } = GIFT_FORM_STATUS;

function DetailBlock(props) {
  const {
    giftDetail, basket, onClearGiftState, onSaveInfo, buttonBlocks, isDrawer, onCloseDrawer,
  } = props;

  const dispatch = useDispatch();
  const shippings = useSelector(state => state.shippings);
  const today = moment(new Date());

  const [state, setState] = useMergeState({
    giftingDetails: {
      from: '',
      to: '',
      note: '',
    },
    senderDetails: {
      senderName: '',
      senderEmail: '',
      note: '',
    },
    recipientDetails: {
      recipientName: '',
      recipientPhone: '',
      recipientCountry: '',
      recipientEmail: '',
      dateOfEmail: today,
      shippingDetail: {
        method: 'Standard',
        fee: 0,
        countryName: '',
      },
    },
    listError: [],

    formStage: SENDER_FORM,
    isExistDataForm: false,
    isCreateNewGift: false,
  });

  const fetchListShippingAddress = () => {
    const options = {
      url: GET_SHIPPING_URL,
      method: 'GET',
    };
    return fetchClient(options);
  };

  useEffect(async () => {
    if (shippings?.length === 0) {
      const result = await fetchListShippingAddress();
      if (result && !result.isError) {
        dispatch(updateShippings(result));
      }
    }
  }, [shippings]);

  useEffect(() => {
    // check gift type has existed before
    // so that display the correct form
    if (basket?.items?.length > 0) {
      const type = _.includes(GIFT_USE_RECIPIENT_FORM, giftDetail.type) ? GIFT_RECIPIENT_FORM : GIFT_DETAIL_FORM;
      const isGift = _.find(basket.items, x => x?.meta?.type === type);

      if (!isEmpty(isGift)) {
        // Check current giftType & existed giftType has belong to recipient form
        if (
          _.includes(GIFT_USE_RECIPIENT_FORM, giftDetail.type)
          && isGift?.meta?.type === GIFT_RECIPIENT_FORM
        ) {
          const countryCode = isGift?.meta?.receiver?.country;
          const shipping = _.find(shippings, x => x?.country?.code === countryCode);

          setState({
            senderDetails: {
              ...state.senderDetails,
              senderName: isGift?.meta?.sender?.name,
              senderEmail: isGift?.meta?.sender?.email,
              note: isGift?.meta?.receiver?.note,
            },
            recipientDetails: {
              ...state.recipientDetails,
              recipientName: isGift?.meta?.receiver?.name,
              recipientPhone: isGift?.meta?.receiver?.phone,
              recipientCountry: isGift?.meta?.receiver?.country,
              recipientEmail: isGift?.meta?.receiver?.email,
              dateOfEmail: new Date(isGift?.meta?.receiver?.date_sent * 1000),
              shippingDetail: {
                method: shipping?.type,
                fee: shipping?.shipping ? Number(shipping?.shipping) : 0,
                countryName: shipping?.country?.name,
              },
            },
            isExistDataForm: true,
            formStage: EXISTING_FORM,
          });
        }

        if (
          _.includes(GIFT_USE_GIFTING_DETAIL_FORM, giftDetail.type)
          && isGift?.meta?.type === GIFT_DETAIL_FORM
        ) {
          setState({
            giftingDetails: {
              ...state.giftingDetails,
              from: isGift?.meta?.sender?.name,
              to: isGift?.meta?.receiver?.name,
              note: isGift?.meta?.receiver?.note,
            },
            isExistDataForm: true,
            formStage: EXISTING_FORM,
          });
        }
      }
    }
  }, [basket, shippings]);

  useEffect(() => {
    segmentGiftingSenderInfoViewed(giftDetail);
  }, [])

  const getStyleIcon = value => (value ? { color: '#2c2c2c' } : { color: '#8D826E' });

  const handleBackClick = (stage) => {
    if (stage === SENDER_FORM) {
      onClearGiftState();
      setState({ formStage: stage });
      if (isDrawer) {
        onCloseDrawer();
      }
    } else setState({ formStage: stage - 1 });
  };

  const handleNextClick = () => {
    const { senderEmail, senderName } = state.senderDetails;
    const listError = [];
    const fieldError = [];
    if (!senderName || !senderEmail) {
      if (!senderName) {
        listError.push('senderName');
        fieldError.push('Sender’s name');
      }
      if (!senderEmail) {
        listError.push('senderEmail');
        fieldError.push('Sender’s email');
      }

      toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
      setState({ listError });
      return false;
    }

    // check '@' include Sender name.
    if (senderName.includes('@')) {
      listError.push('senderName');
      fieldError.push('Sender’s name');
    }

    if (!EmailValidator.validate(senderEmail)) {
      listError.push('senderEmail');
      toastrError('The Sender’s email is not valid');
      return false;
    }

    segmentGiftingRecipientInfoViewed(senderEmail, senderName, giftDetail);
    setState({ formStage: RECIPIENT_FORM, listError: [] });
  };

  const handleSaveInformationsClick = (formType) => {
    const listError = [];
    const fieldError = [];
    switch (formType) {
      case 'recipientDetails':
      {
        const {
          recipientName, recipientPhone, recipientEmail, dateOfEmail, recipientCountry
        } = state.recipientDetails;
        if (
            !recipientName || !recipientPhone ||
            !recipientEmail || !dateOfEmail ||
            (!recipientCountry && !_.includes(GIFT_DOES_NOT_NEED_SHIPPING, giftDetail.type))
          ) {
          if (!recipientName) {
            listError.push('recipientName');
            fieldError.push('Recipient’s name');
          }
          if (!recipientPhone) {
            listError.push('recipientPhone');
            fieldError.push('Recipient’s phone');
          }
          if (!recipientEmail) {
            listError.push('recipientEmail');
            fieldError.push('Recipient’s email');
          }
          if (!dateOfEmail) {
            listError.push('dateOfEmail');
            fieldError.push('Date of email');
          }

          if(!recipientCountry) {
            listError.push('recipientCountry');
            fieldError.push('Shipping Country Destination');
          }
 
          toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
          setState({ listError });
          return false;
        }

        if (!EmailValidator.validate(recipientEmail)) {
          listError.push('recipientEmail');
          toastrError('The Recipient’s email is not valid');
          setState({ listError });
          return false;
        }
        if (!recipientPhone?.length || recipientPhone?.length <= 5) {
          listError.push('recipientPhone');
          toastrError('The Recipient’s phone is not valid');
          setState({ listError });
          return false;
        }

        onSaveInfo(state);
        break;
      }

      case 'giftingDetails':
      {
        const { from, to } = state.giftingDetails;
        if (!from || !to) {
          if (!from) {
            listError.push('from');
            fieldError.push('Sender’s name');
          }
          if (!to) {
            listError.push('to');
            fieldError.push('Recipient’s name');
          }

          toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
          setState({ listError });
          return false;
        }
        //segmentGiftingRecipientInfoViewed(null, from, giftDetail);
        onSaveInfo(state);
        break;
      }

      default: break;
    }
  };

  const handleCreateNewFormClick = (formType) => {
    if (formType === 'giftingDetails') {
      setState({
        formStage: SENDER_FORM,
        isExistDataForm: false,
        giftingDetails: {
          to: '',
          from: '',
          note: '',
        },
        isCreateNewGift: true,
      });
    } else {
      setState({
        formStage: SENDER_FORM,
        isExistDataForm: false,
        senderDetails: {
          senderName: '',
          senderEmail: '',
          note: '',
        },
        recipientDetails: {
          recipientName: '',
          recipientPhone: '',
          recipientCountry: '',
          recipientEmail: '',
          dateOfEmail: today,
          shippingDetail: {
            method: 'Standard',
            fee: 0,
            countryName: '',
          },
        },
        isCreateNewGift: true,
      });
    }
  };

  const handleUseTheSameFormClick = () => {
    const { recipientDetails } = state;
    const shipping = _.find(shippings, x => x?.country?.code === recipientDetails.recipientCountry);

    if(isEmpty(shipping)) {
      setState({ formStage: RECIPIENT_FORM, isCreateNewGift: false, });
    }

    handleSaveInformationsClick('recipientDetails');
  }

  const onChange = (e) => {
    setState({ listError: [] });
    const { name, value, detail } = e.target;

    if (name === 'recipientCountry') {
      const country = _.find(shippings, x => x?.country?.code === value?.target?.value);
      if (country?.id) {
        setState({
          [detail]: {
            ...state[detail],
            [name]: value?.target?.value,
            shippingDetail: {
              ...[name].shippingDetail,
              method: country?.type,
              fee: country?.shipping,
              countryName: country?.country?.name,
            },
          },
        });
      }
    }

    setState({
      [detail]: {
        ...state[detail],
        [name]: name === 'recipientPhone' || name === 'dateOfEmail' ? value : value?.target?.value,
      },
    });
  };

  // Button block
  const giftingDetailsTitle = getNameFromButtonBlock(buttonBlocks, 'gifting_details');
  const senderDetailsTitle = getNameFromButtonBlock(buttonBlocks, 'sender_details');
  const senderNameLabel = getNameFromButtonBlock(buttonBlocks, 'sender_name');
  const senderNamePlaceholder = getLinkFromButtonBlock(buttonBlocks, 'sender_name');
  const senderEmailLabel = getNameFromButtonBlock(buttonBlocks, 'sender_email');
  const senderEmailPlaceholder = getLinkFromButtonBlock(buttonBlocks, 'sender_email');
  const yourNoteLabel = getNameFromButtonBlock(buttonBlocks, 'your_note');
  const yourNotePlaceholder = getLinkFromButtonBlock(buttonBlocks, 'your_note');
  const fromLabel = getNameFromButtonBlock(buttonBlocks, 'from');
  const toLabel = getNameFromButtonBlock(buttonBlocks, 'to');
  const createNewFormBtn = getNameFromButtonBlock(buttonBlocks, 'create_new_form_btn');
  const useSameDetailBtn = getNameFromButtonBlock(buttonBlocks, 'use_same_detail_btn');
  const youHaveDetail = getNameFromButtonBlock(buttonBlocks, 'you_already_have_detail');
  const doYouWantToUseDetail = getNameFromButtonBlock(buttonBlocks, 'do_you_want_to_use_this_detail');
  const recipientTitle = getNameFromButtonBlock(buttonBlocks, 'recipient');
  const recipientNameLabel = getNameFromButtonBlock(buttonBlocks, 'recipient_name');
  const recipientNamePlaceholder = getLinkFromButtonBlock(buttonBlocks, 'recipient_name');
  const recipientEmailLabel = getNameFromButtonBlock(buttonBlocks, 'recipient_email');
  const recipientEmailPlaceholder = getLinkFromButtonBlock(buttonBlocks, 'recipient_email');
  const recipientPhoneLabel = getNameFromButtonBlock(buttonBlocks, 'recipient_phone_number');
  const shippingLabel = getNameFromButtonBlock(buttonBlocks, 'shipping');
  const youHaveExistingGiftingDetails = getNameFromButtonBlock(buttonBlocks, 'you_have_existing_gifting_details');
  const doYouWantToUseTheseDetails = getNameFromButtonBlock(buttonBlocks, 'do_you_want_to_use_these_details');
  const editBtn = getNameFromButtonBlock(buttonBlocks, 'edit_btn');
  const recipientDetailTitle = getNameFromButtonBlock(buttonBlocks, 'recipient_details');
  const shippingCountryDestinationLabel = getNameFromButtonBlock(buttonBlocks, 'shipping_country_destination');
  const dateOfEmailLabel = getNameFromButtonBlock(buttonBlocks, 'date_of_email');
  const dateOfEmailPlaceholder = getLinkFromButtonBlock(buttonBlocks, 'date_of_email');
  const weWillUpdateYouOnStatus = getNameFromButtonBlock(buttonBlocks, 'we_will_update_you_on_the_status_of_your_gift_order_via_email');
  const recipientWillReceiveEmail = getNameFromButtonBlock(buttonBlocks, 'recipient_will_receive_email');
  const backBtn = getNameFromButtonBlock(buttonBlocks, 'back_btn');
  const saveInfoBtn = getNameFromButtonBlock(buttonBlocks, 'save_information_btn');
  const nextBtn = getNameFromButtonBlock(buttonBlocks, 'next_btn');

  // FORM GIFTING DETAIL
  const contentSendDetail = () => (
    <React.Fragment>
      <div className="title-header">
        <div>
          {_.includes(GIFT_USE_RECIPIENT_FORM, giftDetail.type) ? senderDetailsTitle : giftingDetailsTitle}
        </div>
        <button
          type="button"
          className="button-bg__none"
          onClick={() => setState({ formStage: SENDER_FORM })}
        >
          {editBtn}
        </button>
      </div>
      <div className="line-text">
        <b className="mr-2">
          {_.includes(GIFT_USE_RECIPIENT_FORM, giftDetail.type) ? senderNameLabel : toLabel}
        </b>
        {_.includes(GIFT_USE_RECIPIENT_FORM, giftDetail.type) ? state.senderDetails.senderName : state.giftingDetails.to}
      </div>
      <div className="line-text">
        <b className="mr-2">
          {_.includes(GIFT_USE_RECIPIENT_FORM, giftDetail.type) ? senderEmailLabel : fromLabel}
        </b>
        {_.includes(GIFT_USE_RECIPIENT_FORM, giftDetail.type) ? state.senderDetails.senderEmail : state.giftingDetails.from}
      </div>
      <div className="line-text-2">
        <div className="title-2 mr-2">
          {yourNoteLabel}
        </div>
        <div className="des">
          {_.includes(GIFT_USE_RECIPIENT_FORM, giftDetail.type) ? state.senderDetails.note : state.giftingDetails.note}
        </div>
      </div>
    </React.Fragment>
  );

  // FORM EXISTED GIFTING DETAIL
  const htmlExistGiftDetail = () => (
    <div className="detail-gift-block existing-details">
      {/* FORM */}
      <div className="detail-gift-form animated faster fadeInRight">
        <div className={`title-text ${isDrawer ? 'title-flex' : ''}`}>
          {isDrawer && <div className="back-btn" onClick={onCloseDrawer}><ArrowBackIcon /></div>}
          {youHaveExistingGiftingDetails}
        </div>
        <div className="des-text">
          {doYouWantToUseTheseDetails}
        </div>
        <div className="gift-sum-detail">
          {contentSendDetail()}
        </div>
        <div className="space-div" />
      </div>

      {/* BUTTONS */}
      <div className="list-btn equal-width">
        <ButtonCT
          name={createNewFormBtn}
          variant="outlined"
          onClick={() => handleCreateNewFormClick('giftingDetails')}
        />
        <ButtonCT
          name={useSameDetailBtn}
          onClick={() => handleSaveInformationsClick('giftingDetails')}
        />
      </div>
    </div>
  );

  // FORM EXISTED RECIPIENT DETAIL
  const htmlRecipentExist = () => (
    <div className="detail-gift-block existing-details">
      {/* FORM */}
      <div className='detail-gift-form animated faster fadeInRight'>
        <div className={`title-text ${isDrawer ? 'title-flex' : ''}`}>
          {isDrawer && <div className="back-btn" onClick={onCloseDrawer}><ArrowBackIcon /></div>}
          {youHaveDetail}
        </div>
        <div className="des-text">
          {doYouWantToUseDetail}
        </div>
        <div className="gift-sum-detail">
          <div className="title-header">
            <div>
              {recipientTitle}
            </div>
            <button
              type="button"
              className="button-bg__none"
              onClick={() => setState({ formStage: RECIPIENT_FORM })}
            >
              {editBtn}
            </button>
          </div>
          <div className="line-text">
            <b className="mr-2">{recipientNameLabel}</b>
            {state.recipientDetails.recipientName}
          </div>
          <div className="line-text">
            <b className="mr-2">{recipientEmailLabel}</b>
            {state.recipientDetails.recipientEmail}
          </div>
          <div className="line-text">
            <b className="mr-2">{recipientPhoneLabel}</b>
            {state.recipientDetails.recipientPhone}
          </div>
          {!_.includes(GIFT_DOES_NOT_NEED_SHIPPING, giftDetail.type) && (
            <div className="line-text">
              <b className="mr-2">{shippingLabel}</b>
              {state.recipientDetails.shippingDetail.countryName}
              {' '}
              (
              <span className="method">
                {state.recipientDetails.shippingDetail.method}&nbsp;
              </span>
              $
              {' '}
              {state.recipientDetails.shippingDetail.fee}
              )
            </div>
          )}
          <div className="mt-3" />
          {contentSendDetail()}
        </div>
        <div className="space-div" />
      </div>

      {/* BUTTONS */}
      <div className="list-btn equal-width">
        <ButtonCT
          name={createNewFormBtn}
          variant="outlined"
          onClick={() => handleCreateNewFormClick('recipientDetails')}
        />
        <ButtonCT
          name={useSameDetailBtn}
          onClick={handleUseTheSameFormClick}
        />
      </div>
    </div>
  );

  // FORM RECIPIENT DETAIL
  const htmlRecipentDetail = () => (
    <div className="detail-gift-block">

      {/* FORM */}
      <div className='detail-gift-form animated faster fadeInRight'>
        <div className={`title-text ${isDrawer ? 'title-flex' : ''}`}>
          {isDrawer && <div className="back-btn" onClick={onCloseDrawer}><ArrowBackIcon /></div>}
          {recipientDetailTitle}
        </div>
        <InputCT
          label={recipientNameLabel}
          type="text"
          placeholder={recipientNamePlaceholder}
          iconLeft={<PermIdentityIcon style={getStyleIcon(state.recipientDetails.recipientName)} />}
          name="recipientName"
          value={state.recipientDetails.recipientName}
          onChange={value => onChange({ target: { detail: 'recipientDetails', name: 'recipientName', value } })}
          className={_.includes(state.listError, 'recipientName') ? 'error' : ''}
        />
        <InputCT
          label={recipientPhoneLabel}
          type="phone"
          className={_.includes(state.listError, 'recipientPhone') ? 'error' : ''}
          placeholder="phone"
          value={state.recipientDetails.recipientPhone}
          name="recipientPhone"
          onChange={value => onChange({ target: { detail: 'recipientDetails', name: 'recipientPhone', value } })}
        />
        {!_.includes(GIFT_DOES_NOT_NEED_SHIPPING, giftDetail.type) && (
          <InputCT
            type="select-checkout"
            placeholder="Singapore (+$0 for Shipping)"
            name="recipientCountry"
            optionShippingFormat
            options={_.orderBy(shippings || [], ['country[name]', ['asc']])}
            label={shippingCountryDestinationLabel}
            value={state.recipientDetails.recipientCountry}
            className={_.includes(state.listError, 'recipientCountry') ? 'error' : ''}
            onChange={value => onChange({ target: { detail: 'recipientDetails', name: 'recipientCountry', value } })}
          />
        )}
        <InputCT
          label={recipientEmailLabel}
          type="text"
          placeholder={recipientEmailPlaceholder}
          iconLeft={<MailOutlineIcon style={getStyleIcon(state.recipientDetails.recipientEmail)} />}
          name="recipientEmail"
          value={state.recipientDetails.recipientEmail}
          className={classnames(state.listError.includes('recipientEmail') ? 'error' : '')}
          onChange={value => onChange({ target: { detail: 'recipientDetails', name: 'recipientEmail', value } })}
        />
        <DatePickerCT
          label={dateOfEmailLabel}
          placeholder={dateOfEmailPlaceholder}
          className={classnames(state.listError.includes('dateOfEmail') ? 'error' : '')}
          value={moment(state.recipientDetails.dateOfEmail)}
          minDate={today}
          onChange={value => onChange({
            target: {
              detail: 'recipientDetails',
              name: 'dateOfEmail',
              value
            }
          })}
        />
        <div className="des-text">
          {recipientWillReceiveEmail}
        </div>
      </div>
      {/* BUTTONS */}
      <div className="list-btn">
        <ButtonCT
          name={backBtn}
          color="secondary"
          className="back-btn"
          onClick={() => handleBackClick(RECIPIENT_FORM)}
        />
        <ButtonCT
          name={saveInfoBtn}
          endIcon={<CheckIcon style={{ color: '#EBD8B8' }} />}
          onClick={() => handleSaveInformationsClick('recipientDetails')}
        />
      </div>
    </div>
  );

  // FORM GIFTING DETAIL
  const htmlGiftingDetails = () => (
    <div className="detail-gift-block">
      {/* FORM */}
      <div className='detail-gift-form animated faster fadeInRight'>
        <div className={`title-text ${isDrawer ? 'title-flex' : ''}`}>
          {isDrawer && <div className="back-btn" onClick={onCloseDrawer}><ArrowBackIcon /></div>}
          {giftingDetailsTitle}
        </div>
        <InputCT
          label={fromLabel}
          type="text"
          placeholder={senderNamePlaceholder}
          iconLeft={<PermIdentityIcon style={getStyleIcon(state.senderName)} />}
          name="from"
          value={state.giftingDetails.from}
          onChange={value => onChange({ target: { detail: 'giftingDetails', name: 'from', value } })}
          className={_.includes(state.listError, 'from') ? 'error' : ''}
        />
        <InputCT
          label={toLabel}
          placeholder={recipientNamePlaceholder}
          type="text"
          iconLeft={<PermIdentityIcon style={getStyleIcon(state.recipientName)} />}
          name="to"
          value={state.giftingDetails.to}
          onChange={value => onChange({ target: { detail: 'giftingDetails', name: 'to', value } })}
          className={_.includes(state.listError, 'to') ? 'error' : ''}
        />

        <div className="text-note">
          <div className="label-text">
            {yourNoteLabel}
          </div>
          <TextareaAutosize
            placeholder={yourNotePlaceholder}
            value={state.giftingDetails.note}
            onChange={value => onChange({ target: { detail: 'giftingDetails', name: 'note', value } })}
            className="text-are"
          />
        </div>
      </div>

      {/* BUTTONS */}
      <div className="list-btn">
        <ButtonCT
          className="back-btn"
          name={backBtn}
          color="secondary"
          onClick={() => handleBackClick(SENDER_FORM)}
        />
        <ButtonCT
          name={saveInfoBtn}
          endIcon={<CheckIcon style={{ color: '#EBD8B8' }} />}
          onClick={() => handleSaveInformationsClick('giftingDetails')}
        />
      </div>
    </div>
  );

  // FORM SENDER DETAIL
  const htmlSenderDetails = () => (
    <div className="detail-gift-block">

      {/* FORM */}
      <div className='detail-gift-form animated faster fadeInRight'>
        <div className={`title-text ${isDrawer ? 'title-flex' : ''}`}>
          {isDrawer && <div className="back-btn" onClick={onCloseDrawer}><ArrowBackIcon /></div>}
          {senderDetailsTitle}
        </div>
        <InputCT
          label={senderNameLabel}
          type="text"
          placeholder={senderNamePlaceholder}
          iconLeft={<PermIdentityIcon style={getStyleIcon(state.senderDetails.senderName)} />}
          name="from"
          value={state.senderDetails.senderName}
          onChange={value => onChange({ target: { detail: 'senderDetails', name: 'senderName', value } })}
          className={_.includes(state.listError, 'senderName') ? 'error' : ''}
        />
        <InputCT
          label={senderEmailLabel}
          type="text"
          placeholder={senderEmailPlaceholder}
          iconLeft={<MailOutlineIcon style={getStyleIcon(state.senderDetails.senderEmail)} />}
          name="to"
          value={state.senderDetails.senderEmail}
          onChange={value => onChange({ target: { detail: 'senderDetails', name: 'senderEmail', value } })}
          className={_.includes(state.listError, 'senderEmail') ? 'error' : ''}
        />

        <div className="text-note">
          <div className="label-text">
            {yourNoteLabel}
          </div>
          <TextareaAutosize
            placeholder={yourNotePlaceholder}
            value={state.senderDetails.note}
            onChange={value => onChange({ target: { detail: 'senderDetails', name: 'note', value } })}
            className="text-are"
          />
        </div>
        <div className="des-text">
          {weWillUpdateYouOnStatus}
        </div>
      </div>
      {/* BUTTONS */}
      <div className="list-btn">
        <ButtonCT
          className="back-btn"
          name={backBtn}
          color="secondary"
          onClick={() => handleBackClick(SENDER_FORM)}
        />
        <ButtonCT
          name={nextBtn}
          className="next-btn"
          endIcon={<ArrowForwardIcon style={{ color: '#EBD8B8' }} />}
          onClick={handleNextClick}
        />
      </div>
    </div>
  );

  return (
    <div className="block-detail-wrapper">
      {state.formStage === SENDER_FORM
        && (_.includes(GIFT_USE_RECIPIENT_FORM, giftDetail.type)
          ? htmlSenderDetails()
          : htmlGiftingDetails())
      }

      {state.formStage === RECIPIENT_FORM && htmlRecipentDetail()}

      {(state.formStage === EXISTING_FORM && state.isExistDataForm)
        && (_.includes(GIFT_USE_RECIPIENT_FORM, giftDetail.type)
          ? htmlRecipentExist()
          : htmlExistGiftDetail())
      }
    </div>
  );
}

export default memo(DetailBlock);
