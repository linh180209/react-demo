import classnames from 'classnames';
import React from 'react';
import { isMobile480fn } from '../../../../DetectScreen';
import '../../giftv2.scss';
import './styles.scss';

function GiftBanner({
  classNames, bannerUrlDesktop, bannerUrlMobile, children, isMobile
}) {

  return (
    <div className={classnames('gift-banner', classNames)}>
      <div className="block-image">
        <img loading="lazy" src={(isMobile || isMobile480fn()) ? bannerUrlMobile : bannerUrlDesktop} alt="gift" />
        <div className="block-content">
          {children}
        </div>
      </div>
    </div>
  );
}

export default GiftBanner;
