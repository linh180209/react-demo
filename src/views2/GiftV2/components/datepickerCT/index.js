import { TextField } from '@mui/material';
import { DatePicker } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import React from 'react';
import './styles.scss';

function DatePickerCT({ label, value, className, placeholder, onChange, minDate }) {
  return (
    <div className='wrapper-ct'>
      <div className='label'>{label}</div>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DatePicker
          className='datepicker-ct'
          value={value}
          disablePast
          minDate={minDate}
          views={['day']}
          onChange={onChange}
          renderInput={(params) => (
            <TextField
              {...params}
              disabled
              inputProps={{
                ...params.inputProps,
                className: className,
                placeholder: placeholder,
                readOnly: true,
              }}
            />
          )}
        />
      </LocalizationProvider>
    </div>
  )
}

export default DatePickerCT