import _, { isEmpty } from 'lodash';
import React, { forwardRef, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { generateUrlWeb, segmentGiftingOfferPageInitiated } from '../../../../Redux/Helpers';
import auth from '../../../../Redux/Helpers/auth';
import { GIFT_AVAILABLE_IN_COUNTRIES } from '../../constants';
import './styles.scss';

const GiftItems = forwardRef(({ giftItems }, ref) => {
  const country = auth.getCountry();
  const history = useHistory();
  const [items, setItems] = useState([]);

  // Gift workshop only exist on SG, VN, AE country
  useEffect(() => {
    if(!isEmpty(giftItems) && giftItems?.value?.product?.length > 0) {
      const items = giftItems.value.product;

      if(_.includes(GIFT_AVAILABLE_IN_COUNTRIES, country)) setItems(items);
      else {
        let filterItems = _.filter(items, x => x?.value?.link !== 'workshop');
        setItems(filterItems);
      }
    }
  }, [giftItems])

  return (
    <div ref={ref} className="gift-items padding-y">
      <div className="gift-items__title main-title margin-y-b">{giftItems?.value?.text}</div>

      <div className={`gift-items__block ${_.includes(GIFT_AVAILABLE_IN_COUNTRIES, country) ? 'block-full' : ''}`}>
        {items?.map?.((gift, idx) => (
          <div
            key={idx}
            className={`gift-block item-${idx + 1}`}
            onClick={() => { 
                segmentGiftingOfferPageInitiated(gift?.value?.text);
                history.push(generateUrlWeb(`/gift/${gift?.value?.link}`));  
            }}
          >
            <div className="gift-block__image gift-image">
              <img loading="lazy" src={gift?.value?.image?.image} alt="gift" />
            </div>
            <div className="gift-block__content">
              <div className="title">{gift?.value?.text}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
});

export default GiftItems;
