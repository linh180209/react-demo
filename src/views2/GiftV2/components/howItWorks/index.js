import _ from 'lodash';
import React from 'react';
import '../../giftv2.scss';
import './styles.scss';

function HowItWorks({ data, title, isLandingPage }) {
  return (
    <div className="how-it-works padding-y">
      <div className="main-title margin-y-b">
        {title}
      </div>
      <div
        className={`div-steps ${isLandingPage ? 'div-landingpage' : 'div-giftpage'}`}
        style={{ gridTemplateColumns: `repeat(${data?.value?.icons?.length}, 1fr)` }}
      >
        {_.map(data?.value?.icons || [], (d, idx) => (
          <div className="block-step" key={idx}>
            <div className="div-number">{idx + 1}</div>
            <div className="div-content">
              <div
                className={`div-title ${d?.value?.text ? 'mb-2' : ''}`}
              >
                {d?.value?.image?.caption}
              </div>
              {d?.value?.text && <div className="div-description">{d?.value?.text}</div>}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default HowItWorks;
