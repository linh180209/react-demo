import _ from 'lodash';
import React, { useEffect } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import { isMobile480fn, isMobile991fn } from '../../../../DetectScreen';
import { getLinkFromButtonBlock, getNameFromButtonBlock } from '../../../../Redux/Helpers';
import { useMergeState } from '../../../../Utils/customHooks';
import useWindowEvent from '../../../../Utils/useWindowEvent';
import './styles.scss';

function OurClients(props) {
  const { buttonBlocks, iconBlocks, typeOfGift } = props;

  // const [settings, setSettings] = useMergeState({
  //     dots: true,
  //     slidesToShow: 1,
  //     slidesToScroll: 1,
  //     autoplay: true,
  //     autoplaySpeed: 2000,
  //     pauseOnHover: true,
  //     arrows: false,
  //     infinite: true,
  //     variableWidth: true
  // });

  // const [screenMode, setScreenMode] = useMergeState({
  //   isMobile: false,
  //   isTablet: false,
  // })

  // const handleInitResize = () => {
  //   // check size mobile for banner, tablet under for our clients.
  //   // our clients have 2 slides type for each mode.
  //   if(isMobile991fn()) {
  //     if(isMobile480fn()) setScreenMode({ isMobile: true, isTablet: false });
  //     else setScreenMode({ isTablet: true, isMobile: false });
  //   }else {
  //     setScreenMode({ isTablet: false, isMobile: false });
  //   }
  // };

  // useWindowEvent('resize', handleInitResize, window);

  // useEffect(() => {
  //   if(screenMode.isTablet || isMobile991fn()) {
  //     setSettings({
  //       className: 'slider variable-width',
  //       variableWidth: true,
  //       autoplay: false,
  //     });
  //   }

  //   if(screenMode.isMobile || isMobile480fn()) {
  //     setSettings({
  //       className: 'slider',
  //       variableWidth: false,
  //       autoplay: true,
  //     });
  //   }
  // }, [screenMode]);

  const ourClientTitle = getNameFromButtonBlock(buttonBlocks, 'our_client_gift_page');
  const ourClientDes = getLinkFromButtonBlock(buttonBlocks, 'our_client_gift_page');
  const ourClientData = _.find(iconBlocks, x => x?.value?.image_background?.caption === `our_client_${typeOfGift}`);

  return (
    <div className="our-clients padding-y">
      <div className="our-clients__head margin-y-b">
        <div className="block-introduce">
          <div className="main-title">{ourClientTitle}</div>
          <p className="description">
            {ourClientDes}
          </p>
        </div>
      </div>

      {/* {screenMode.isMobile || screenMode.isTablet || isMobile991fn() || isMobile480fn() ? (
        <Slider {...settings}>
          {_.map(ourClientData?.value?.icons || [], (d, idx) => (
            <div className="slide-wrapper" key={idx}>
              <div className="slide-item">
                <div className="slide-item-head">
                  <img src={ourClientData?.value?.image_background?.image} alt="gift-icon" />
                </div>
                <div className="slide-item-content">
                  {d?.value?.text}
                </div>
                <div className="slide-item-footer">
                  <div className="block-author">
                    <div className="name">{d?.value?.image?.caption}</div>
                    <p className="role">{d?.value?.header?.header_text}</p>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </Slider>
      ) : ( */}
        <div className="our-clients__quotes">
          {_.map(ourClientData?.value?.icons || [], (d, idx) => (
            <div className="item-quote" key={idx}>
              <div className="item-quote__top">
                <img src={ourClientData?.value?.image_background?.image} alt="gift-icon" />
              </div>
              <div className="item-quote__middle">
                {d?.value?.text}
              </div>
              <div className="item-quote__bottom">
                <div className="block-author">
                  <div className="name">{d?.value?.image?.caption}</div>
                  <p className="role">{d?.value?.header?.header_text}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      {/* )} */}
    </div>
  );
}

export default OurClients;
