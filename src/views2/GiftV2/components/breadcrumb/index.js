import _ from 'lodash';
import React from 'react';
import LinkCT from '../../../../componentsv2/linkCT';
import { getLink } from '../../../../Redux/Helpers';
import '../../giftv2.scss';
import './styles.scss';

function Breadcrumb({ breadcrumb }) {
  return (
    <div className="gift-breadcrumb">
      {_.map(breadcrumb, (bread, idx) => (
        idx === breadcrumb.length - 1
          ? (
            <div className="current-bread" key={idx}>
              <span className="slash">/</span>
              {bread.name}
            </div>
          )
          : (
            <LinkCT
              key={idx}
              to={getLink(bread.url)}
              className="prev-bread"
            >
              {idx !== 0 ? (
                <div>
                  <span className="slash">/</span>
                  {bread.name}
                </div>
              ) : bread.name}
            </LinkCT>
          )
      ))}
    </div>
  );
}

export default Breadcrumb;
