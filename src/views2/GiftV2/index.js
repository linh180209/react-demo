import _ from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import MetaTags from 'react-meta-tags';
import { useDispatch, useSelector } from 'react-redux';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import BlockFAQV2 from '../../componentsv2/blockFAQV2';
import ButtonCT from '../../componentsv2/buttonCT';
import { isMobile480fn } from '../../DetectScreen';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import {
  fetchCMSHomepage, generateHreflang, getLinkFromButtonBlock, getNameFromButtonBlock, removeLinkHreflang
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { toastrError } from '../../Redux/Helpers/notification';
import {
  generateScriptFAQ, removeScriptFAQ
} from '../../Utils/addScriptSchema';
import { useMergeState } from '../../Utils/customHooks';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';
import useWindowEvent from '../../Utils/useWindowEvent';
import FooterV2 from '../footer';
import GiftBanner from './components/giftBanner';
import GiftItems from './components/giftItems';
import HowItWorks from './components/howItWorks';
import OurClients from './components/ourClients';
import './giftv2.scss';

const handleGetCMSData = (data) => {
  if (data) {
    const imageBg = data.image_background;
    const iconBlocks = _.filter(data.body, x => x.type === 'icons_block');
    const productBlocks = _.find(data.body, x => x.type === 'products_block' && x.value.header.header_text === 'Maison 21g Gift');
    const buttonBlocks = _.filter(data.body, x => x.type === 'button_block');
    const imageBlocks = _.filter(data.body, x => x.type === 'image_block');
    const columnBlocks = _.filter(data.body, x => x.type === 'columns_block');
    const textBlocks = _.find(data.body, x => x.type === 'texts_block' && x.value.header.header_text !== 'title');
    const textBlocksHeader = _.find(data.body, x => x.type === 'texts_block' && x.value.header.header_text === 'title');
    const faqData = _.find(data.body, x => x.type === 'faqs_block');
    const faqFilter = _.filter(faqData?.value, x => x.value?.header?.header_text === 'gift_landingpage_faq');
    const faqBlocks = _.map(faqFilter, x => ({ ...x, value: { ...x.value, faqs: x.value.faq } }));

    return {
      imageBg,
      iconBlocks,
      productBlocks,
      buttonBlocks,
      imageBlocks,
      textBlocks,
      textBlocksHeader,
      columnBlocks,
      faqBlocks,
    };
  }
  return {};
};

function GiftPageV2() {
  const dispatch = useDispatch();
  const ref = useRef();
  const cms = useSelector(state => state.cms);
  const countries = useSelector(state => state.countries);

  const [dataState, setDataState] = useMergeState({
    buttonBlocks: [],
    iconBlocks: [],
    textBlocks: {},
    textBlocksHeader: {},
    imageBlocks: [],
    productBlocks: {},
    faqBlocks: [],
  });
  const [isMobile, setIsMobile] = useState(false);

  const handleFetchDataInit = async () => {
    const cmsBlock = _.find(cms, x => x.title === 'GiftV2');
    let cmsData = {};

    if (!cmsBlock) {
      dispatch(loadingPage(true));
      try {
        const cmsResponse = await fetchCMSHomepage('gift-v2');
        cmsData = handleGetCMSData(cmsResponse);
        dispatch(addCmsRedux(cmsResponse));
      } catch (error) {
        toastrError(error.message);
      }
      dispatch(loadingPage(false));
    } else {
      cmsData = handleGetCMSData(cmsBlock);
    }
    setDataState(cmsData);
  };

  const handleStartGiftingClick = () => {
    if (ref.current) ref.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
  };

  useEffect(() => {
    handleFetchDataInit();

    return (() => {
      removeLinkHreflang();
      removeScriptFAQ();
    });
  }, []);

  const handleInitResize = () => {
    if(isMobile480fn()) setIsMobile(true);
    else setIsMobile(false);
  };

  useWindowEvent('resize', handleInitResize, window);

  useEffect(() => {
    if (dataState.faqBlocks.length > 0) {
      generateScriptFAQ(dataState.faqBlocks, true);
    }
  }, [dataState.faqBlocks]);

  const startGiftingBtn = getNameFromButtonBlock(dataState.buttonBlocks, 'start_gifting_btn');
  const howItWorksTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'how_it_works');
  const howItWorksData = _.find(dataState.iconBlocks, x => x?.value?.image_background?.caption === 'gift_landingpage_how_it_works');
  const bannerMobile = _.find(dataState.imageBlocks, x => x?.value?.caption === 'gift_landingpage_mobile_banner');

  const seoCmsTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'seo_gift_landingpage');
  const seoCmsDes = getLinkFromButtonBlock(dataState.buttonBlocks, 'seo_gift_landingpage');
  const countryName = auth.getCountryName();
  const seoTitle = replaceCountryInMetaTags(countryName, seoCmsTitle);
  const seoDes = replaceCountryInMetaTags(countryName, seoCmsDes);

  return (
    <div className="gift-pagev2">
      <MetaTags>
        <title>
          {seoTitle}
        </title>
        <meta name="description" content={seoDes} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/gift')}
      </MetaTags>
      <HeaderHomePageV3 />

      <GiftBanner
        bannerUrlDesktop={dataState?.imageBg}
        bannerUrlMobile={bannerMobile?.value?.image}
        isMobile={isMobile}
      >
        <div className="wrapper">
          <div className="banner-title main-title">
            {dataState.textBlocksHeader?.value?.texts?.length > 0 ? dataState.textBlocksHeader?.value?.texts[0]?.value : ''}
          </div>
          <div className="banner-description" style={{ whiteSpace: 'pre-line' }}>
            {dataState.textBlocks?.value?.texts?.length > 0 ? dataState.textBlocks?.value?.texts[0]?.value : ''}
          </div>
          <ButtonCT type="button" className="banner-btn" name={startGiftingBtn} onClick={handleStartGiftingClick} />
        </div>
      </GiftBanner>

      <div className="gift-content">
        <HowItWorks
          data={howItWorksData}
          title={howItWorksTitle}
          isLandingPage
        />

        <GiftItems ref={ref} giftItems={dataState.productBlocks} />

        <OurClients
          buttonBlocks={dataState.buttonBlocks}
          iconBlocks={dataState.iconBlocks}
          typeOfGift='landingpage'
        />

        <div className="gift-faqs">
          <BlockFAQV2
            dataFAQ={dataState.faqBlocks?.length > 0 ? dataState.faqBlocks[0]?.value : []}
            buttonBlocks={dataState.buttonBlocks}
          />
        </div>
      </div>
      <FooterV2 />
    </div>
  );
}

export default GiftPageV2;
