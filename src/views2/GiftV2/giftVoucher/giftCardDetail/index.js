import { Drawer } from '@mui/material';
import _, { isEmpty } from 'lodash';
import moment from 'moment';
import React, {
  forwardRef, memo, useEffect, useState
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import HeaderHomePageV3 from '../../../../components/HomePage/HeaderHomePageV3';
import { isMobile991fn } from '../../../../DetectScreen';
import { icNotes } from '../../../../imagev2/svg';
import { addProductBasket, createBasketGuest } from '../../../../Redux/Actions/basket';
import { generaCurrency, getLinkFromButtonBlock, getNameFromButtonBlock } from '../../../../Redux/Helpers';
import auth from '../../../../Redux/Helpers/auth';
import useWindowEvent from '../../../../Utils/useWindowEvent';
import {
  GIFT_DETAIL_FORM, GIFT_E_VOUCHER, GIFT_PHYSICAL_VOUCHER, GIFT_RECIPIENT_FORM
} from '../../constants';
import DesBlock from '../../giftBundlesDetail/desBlock';
import DetailBlock from '../../giftBundlesDetail/detailBlock';
import '../../giftBundlesDetail/styles.scss';
import './styles.scss';

const GiftCardDetail = forwardRef(({
  giftSelected,
  dataState,
  voucher,
  onVoucherTypeClick,
}, ref) => {
  const title = getNameFromButtonBlock(dataState.buttonBlocks, 'gift_card_custom_amount');
  const currentlyUnavailable = getNameFromButtonBlock(dataState.buttonBlocks, 'currently_unavailable');
  const note = getLinkFromButtonBlock(dataState.buttonBlocks, 'currently_unavailable');
  const giftCardOnlineName = getNameFromButtonBlock(dataState.buttonBlocks, 'gift_e_card_name');
  const giftCardPhysicalName = getNameFromButtonBlock(dataState.buttonBlocks, 'gift_physical_card_name');

  const dispatch = useDispatch();
  const basket = useSelector(state => state.basket);

  const [orderItem, setOrderItem] = useState({
    quantity: 1,
    info: {},
  });

  const [isMobile, setMobile] = useState(false);
  const [isOpenDrawer, setOpenDrawer] = useState(false);

  const handleClearGiftState = () => {
    setOrderItem(prevOrder => ({ ...prevOrder, info: {}, quantity: 1 }));
  };

  const handleInitResize = () => {
    handleClearGiftState();

    if(isMobile991fn()) setMobile(true);
    else setMobile(false);
  };

  const handleAddToCartClick = () => {
    const info = _.find(giftSelected?.items, x => x?.value?.item?.variant === voucher?.value?.link);

    console.log(info, giftSelected?.items, voucher?.value?.link)

    if(!isEmpty(info)) {
      setOrderItem(prevOrder => ({
        ...prevOrder,
        info: {
          ...prevOrder.info,
          ...info,
          groupId: giftSelected?.id,
          type: voucher?.value?.link
        }
      }));
    }

    if (isMobile || isMobile991fn()) {
      setOpenDrawer(true);
    } else {
      setOpenDrawer(false);
    }
  };

  const handleChangeQuantity = (e) => {
    const { value } = e.target;
    const quantity = parseInt(value, 10);
    setOrderItem(prevOrder => ({ ...prevOrder, quantity }));
  };

  const handleSaveInfo = (data) => {
    const { senderDetails, recipientDetails, giftingDetails, isCreateNewGift } = data;
    const { info, quantity } = orderItem;
    const price = giftSelected.price * quantity;
    const sender = {};
    const receiver = {};

    if (info.value?.item?.variant === GIFT_E_VOUCHER) {
      sender.name = senderDetails.senderName;
      sender.email = senderDetails.senderEmail;
      receiver.name = recipientDetails.recipientName;
      receiver.email = recipientDetails.recipientEmail;
      receiver.country = recipientDetails.recipientCountry || auth.getCountry();
      receiver.date_sent = moment(recipientDetails.dateOfEmail).valueOf() / 1000;
      receiver.note = senderDetails.note;
      receiver.phone = recipientDetails.recipientPhone;
    } else {
      sender.name = giftingDetails.from;
      receiver.name = giftingDetails.to;
      receiver.date_sent = moment(new Date()).valueOf() / 1000;
      receiver.note = giftingDetails.note;
    }

    const model = {
      name: info.value?.item?.name,
      currency: auth.getNameCurrency(),
      price_custom: price,
      quantity: quantity,
      item: info.value?.item?.id,
      meta: {
        giftName: info.value?.item?.variant === GIFT_E_VOUCHER ? giftCardOnlineName : giftCardPhysicalName,
        sender,
        receiver,
        typeOfGift: info.value?.item?.variant,
        type: info.value?.item?.variant === GIFT_E_VOUCHER ? GIFT_RECIPIENT_FORM : GIFT_DETAIL_FORM,
        isCreateNewGift,
      },
    };

    const dataTemp = {
      idCart: basket.id,
      item: model,
    };

    if (!basket.id) {
      dispatch(createBasketGuest(dataTemp));
    } else {
      dispatch(addProductBasket(dataTemp));
    }

    handleClearGiftState();
  };

  const handleCloseDrawer = () => {
    setOpenDrawer(false);
    handleClearGiftState();
  };

  useWindowEvent('resize', handleInitResize, window);

  useEffect(() => {
    if (orderItem.info?.groupId && orderItem.info.groupId !== giftSelected?.id) {
      handleClearGiftState();
    }
  }, [orderItem.info?.groupId, giftSelected?.id]);

  return (
    <div
      className={`gift-card-detail ${giftSelected?.id && giftSelected?.price ? 'padding-y' : ''}`}
      ref={ref}
    >
      <div className='block-wrapper'>
        <div className={`block-image gift-${giftSelected?.class}`}>
          <div className='gift-card-img'>
            <img src={giftSelected?.ribbonDetail} alt='icon' />
          </div>
          <div className="block-content">
            {giftSelected?.class === 'custom' ? <div className="text">{title}</div> : (
              <div className={`text-price ${generaCurrency(giftSelected?.price)?.length > 3 ? 'decrease-fsz' : ''}`}>
                {generaCurrency(giftSelected?.price)}
              </div>
              )}
            <div className="div-card-name">
              <img src={giftSelected?.image} alt="bg" />
              <div className={`text-name ${giftSelected?.title?.length > 10 ? 'decrease-fsz' : ''}`}>
                {giftSelected?.title}
              </div>
            </div>
          </div>
        </div>

        <div className='block-detail block-voucher'>
          {isEmpty(orderItem.info) ? (
            <DesBlock
              buttonBlocks={dataState.buttonBlocks}
              giftDetail={giftSelected}
              voucherType={dataState.voucherType}
              voucherDetail={voucher}
              onVoucherTypeClick={onVoucherTypeClick}
              quantity={orderItem.quantity}
              isDrawer={isMobile || isMobile991fn()}
              onChangeQuantity={handleChangeQuantity}
              onAddToCart={handleAddToCartClick}
            />
          ) : (
            (isMobile || isMobile991fn()) ? (
              <Drawer
                open={isOpenDrawer}
                className="gift-drawer"
                anchor="right"
              >
                <HeaderHomePageV3 />
                <div className="block-description">
                  {!isEmpty(orderItem.info) && (
                    <DetailBlock
                      isDrawer
                      giftDetail={orderItem.info}
                      basket={basket}
                      buttonBlocks={dataState.buttonBlocks}
                      onClearGiftState={handleClearGiftState}
                      onSaveInfo={handleSaveInfo}
                      onCloseDrawer={handleCloseDrawer}
                      isMobile={isMobile}
                    />
                  )}
                </div>
              </Drawer>
            ) : (
              <DetailBlock
                giftDetail={orderItem.info}
                basket={basket}
                buttonBlocks={dataState.buttonBlocks}
                onClearGiftState={handleClearGiftState}
                onSaveInfo={handleSaveInfo}
                isMobile={isMobile}
              />
            )
          )}
        </div>
      </div>

      {giftSelected?.class === 'custom' && voucher?.value?.link === GIFT_PHYSICAL_VOUCHER && (
        <div className='block-bottom'>
          <div className='div-notes'>
            <div className='div-notes-title'>
              <div className='div-icon'>
                <img src={icNotes} alt='gift-icon' />
              </div>
              <div className='div-text'>{currentlyUnavailable}</div>
            </div>
            <div className='div-notes-description'>
              {note}
            </div>
          </div>
        </div>
      )}
    </div>
  );
});

export default memo(GiftCardDetail);
