import _, { isEmpty } from 'lodash';
import React, { memo, useEffect, useState } from 'react';
import ButtonCT from '../../../../../componentsv2/buttonCT';
import InputCT from '../../../../../componentsv2/inputCT';
import { generaCurrency, getLinkFromButtonBlock, getNameFromButtonBlock } from '../../../../../Redux/Helpers';
import auth from '../../../../../Redux/Helpers/auth';
import { toastrError } from '../../../../../Redux/Helpers/notification';
import { useMergeState } from '../../../../../Utils/customHooks';
import { CUSTOM_AMOUNT_FROM, CUSTOM_AMOUNT_TO } from '../../../constants';
import './styles.scss';

function GiftCardItem(props) {
  const {
    data, giftSelected, dataVoucher, index, exchangeRates, onSelectGift, onUpdateGiftSelected,
  } = props;
  const TOTAL_GIFT_CARDS = 4;

  const [customAmount, setCustomAmount] = useState('');
  const [exchangeAmount, setExchangeAmount] = useMergeState({
    from: CUSTOM_AMOUNT_FROM,
    to: CUSTOM_AMOUNT_TO,
  });

  useEffect(() => {
    if(exchangeRates.length > 0) {
      const code = auth.getNameCurrency();
      const currency = _.find(exchangeRates, x => x?.dest === code);

      if(!isEmpty(currency)) {
        setExchangeAmount({
          from: CUSTOM_AMOUNT_FROM * Number(currency?.value),
          to: CUSTOM_AMOUNT_TO * Number(currency?.value),
        })
      }
    }
  }, [exchangeRates])

  const title = getNameFromButtonBlock(dataVoucher.buttonBlocks, 'gift_card_custom_amount');
  const description = getLinkFromButtonBlock(dataVoucher.buttonBlocks, 'gift_card_custom_amount');
  // const amountFrom = getNameFromButtonBlock(dataVoucher.buttonBlocks, 'gift_card_custom_amount_from');
  // const amountTo = getNameFromButtonBlock(dataVoucher.buttonBlocks, 'gift_card_custom_amount_to');
  const upto = getNameFromButtonBlock(dataVoucher.buttonBlocks, 'upto');
  const saveAmountBtn = getNameFromButtonBlock(dataVoucher.buttonBlocks, 'gift_card_save_amount');
  const textPlaceholder = getLinkFromButtonBlock(dataVoucher.buttonBlocks, 'gift_card_save_amount');

  const handleAmountChange = (e) => {
    setCustomAmount(e.target.value);
  };

  const handleSetGiftPrice = () => {
    if (Number(customAmount) < Number(exchangeAmount.from) || Number(customAmount) > Number(exchangeAmount.to)) {
      toastrError(`Please enter the custom amount minimal = ${generaCurrency(exchangeAmount.from)} up to ${generaCurrency(exchangeAmount.to)}`);
      return;
    }
    onUpdateGiftSelected(customAmount);
  };

  return (
    <div
      className={`gift-card-wrapper gift-card-${index + 1} ${giftSelected?.id ? giftSelected?.id === data?.id ? 'gift-card-active' : 'gift-card-inactive' : ''}`}
      onClick={() => onSelectGift(data)}
    >

      <div className={`gift-card-item gift-${data?.class}`}>
        <div className={`gift-card-img ${index + 1 === TOTAL_GIFT_CARDS ? 'width-set' : ''}`}>
          <img src={data?.ribbon} alt='icon' />
        </div>

        {index + 1 !== TOTAL_GIFT_CARDS ? (
          <div className="content-text">
            <div className={`text-price ${generaCurrency(data?.price).length > 3 ? 'decrease-fsz' : ''}`}>
              {generaCurrency(data?.price)}
            </div>
            <div className="div-card-name">
              {index + 1 !== 4 ? <img src={data?.image} alt="bg" /> : <div />}
              <div className={`text-name ${data?.title?.length > 10 ? 'decrease-fsz' : ''}`}>
                {data?.title}
              </div>
            </div>
          </div>
        ) : (
          <div className="custom-content-text">
            <div className="content-title">{title}</div>
            <div className="content-description">
              {description}{' '}
              {generaCurrency(exchangeAmount.from)}{' '}
              {upto}{' '}
              {generaCurrency(exchangeAmount.to)}
            </div>
            <div className="content-submission" onClick={e => e.stopPropagation()}>
              <div className="text-field">
                <InputCT
                  type="number"
                  value={customAmount}
                  placeholder={textPlaceholder}
                  onChange={handleAmountChange}
                  onFocus={() => onSelectGift(data)}
                />
              </div>
              <ButtonCT
                className="save-btn"
                disabled={giftSelected?.id !== data?.id || !customAmount}
                name={saveAmountBtn}
                onClick={handleSetGiftPrice}
              />
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default memo(GiftCardItem);
