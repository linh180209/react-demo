import _ from 'lodash';
import React, { memo } from 'react';
import { getLinkFromButtonBlock, getNameFromButtonBlock } from '../../../../Redux/Helpers';
import '../../giftv2.scss';
import GiftCardItem from './giftCardItem';
import './styles.scss';

function GiftCard(props) {
  const { buttonBlocks, allVouchers } = props.dataVoucher;

  const title = getNameFromButtonBlock(buttonBlocks, 'gift_cards');
  const description = getLinkFromButtonBlock(buttonBlocks, 'gift_cards');
  return (
    <div className="gift-card padding-y">
      <div className="main-title">
        {title}
      </div>
      <div className="des-text">
        {description}
      </div>
      <div className="list-cards">
        {_.map(allVouchers, (item, idx) => <GiftCardItem key={item?.id} index={idx} data={item} {...props} />)}
      </div>
    </div>
  );
}

export default memo(GiftCard);
