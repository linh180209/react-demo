import _, { isEmpty } from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import { MetaTags } from 'react-meta-tags';
import { useDispatch, useSelector } from 'react-redux';
import HeaderHomePageV3 from '../../../components/HomePage/HeaderHomePageV3';
import BlockFAQV2 from '../../../componentsv2/blockFAQV2';
import { GET_EXCHANGE_RATE } from '../../../config';
import addCmsRedux from '../../../Redux/Actions/cms';
import loadingPage from '../../../Redux/Actions/loading';
import {
  fetchCMSHomepage, generaCurrency, generateHreflang, getLinkFromButtonBlock, getNameFromButtonBlock, removeLinkHreflang, segmentGiftingOfferPageViewed
} from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import { generateScriptFAQ, removeScriptFAQ } from '../../../Utils/addScriptSchema';
import { useMergeState } from '../../../Utils/customHooks';
import { replaceCountryInMetaTags } from '../../../Utils/replaceCountryInMetaTags';
import ReviewsV3 from '../../../views/ProductB2C/reviewsV3';
import FooterV2 from '../../footer';
import Breadcrumb from '../components/breadcrumb';
import GiftBanner from '../components/giftBanner';
import HowItWorks from '../components/howItWorks';
import { GIFT_AVAILABLE_IN_COUNTRIES, GIFT_E_VOUCHER } from '../constants';
import '../giftv2.scss';
import { handleBreadcrumbCreate } from '../utils';
import GiftCard from './giftCard';
import GiftCardDetail from './giftCardDetail';

function GiftVoucher() {
  const dispatch = useDispatch();
  const cms = useSelector(state => state.cms);
  const countries = useSelector(state => state.countries);

  const scrolRef = useRef();
  const [giftSelected, setGiftSelected] = useState({});
  const [dataState, setDataState] = useMergeState({
    buttonBlocks: [],
    iconBlocks: [],
    textBlocks: {},
    textBlocksHeader: {},
    imageBlocks: [],
    faqBlocks: [],
    voucherType: [],
    breadcrumb: [],
    reviews: [],
    allVouchers: [],
  });
  const [voucher, setVoucher] = useState({});
  const [exchangeRates, setExchangeRates] = useState([]);

  const handleGetCMSData = (data) => {
    if (data) {
      const iconBlocks = _.filter(data.body, x => x.type === 'icons_block');
      const reviewCms = _.find(iconBlocks, x => x?.value?.image_background?.caption === 'our_client_vouchers');
      let reviews = [];

      if(!isEmpty(reviewCms)) {
        reviews = _.map(reviewCms?.value?.icons, x => ({
          name: x?.value?.image?.caption,
          comment: x?.value?.text,
          giftType: x?.value?.header?.header_text,
        }));
      }
      const buttonBlocks = _.filter(data.body, x => x.type === 'button_block');
      const imageBlocks = _.filter(data.body, x => x.type === 'image_block');
      const textBlocks = _.find(data.body, x => x.type === 'texts_block' && x.value.header.header_text !== 'title');
      const textBlocksHeader = _.find(data.body, x => x.type === 'texts_block' && x.value.header.header_text === 'title');
      const faqData = _.find(data.body, x => x.type === 'faqs_block');
      const faqFilter = _.filter(faqData.value, x => x.value.header.header_text === 'gift_vouchers_faq');
      const faqBlocks = _.map(faqFilter, x => ({ ...x, value: { ...x.value, faqs: x.value.faq } }));
      const menuBlocks = _.find(data.body, x => x?.type === 'menu_block' && x?.value?.text === 'vouchers_breadcrumb');
      const breadcrumb = handleBreadcrumbCreate(menuBlocks?.value?.submenu, auth.getCountry());

      const itemBlocks = _.find(data.body, x => x?.type === 'items_block' && x?.value?.header?.header_text === 'gift_vouchers');
      const cardImg = _.find(iconBlocks, x => x?.value?.image_background?.caption === 'gift_vouchers_images');
      const cardDetailImg = _.find(iconBlocks, x => x?.value?.image_background?.caption === 'gift_cards_detail_images');

      const voucherItems = !isEmpty(itemBlocks) ? itemBlocks?.value?.items : [];

      //CardBlock that include: img background of voucher.
      const voucherAssets = _.find(data.body, x => x.type === 'products_block' && x.value.header.header_text === 'gift_vouchers_cards');
      const vouchers = _.map(voucherAssets?.value?.product || [], x => {
        const groups = _.filter(voucherItems, y => y?.value?.name === x?.value?.link);
        const cardRibbon = _.find(cardImg?.value?.icons, z => z?.value?.text === x?.value?.link);
        const cardRibbonDetail = _.find(cardDetailImg?.value?.icons, z => z?.value?.text === x?.value?.link);

        if(groups.length > 0) {
          return {
            name: x?.value?.link,
            title: x?.value?.text,
            id: x?.id,
            price: groups?.length > 0 ? groups[0]?.value?.item?.price : x?.value?.price,
            image: x?.value?.image?.image,
            class: x?.value?.image?.caption,
            items: groups,
            ribbon: cardRibbon?.value?.image?.image,
            ribbonDetail: cardRibbonDetail?.value?.image?.image,
          }
        }
      });

      // voucher type includes: e-voucher, physical vouhcer
      // physical voucher only existed in SG, VN, AE
      const voucherCms = _.find(data.body, x => x.type === 'products_block' && x.value.header.header_text === 'gift_vouchers_type');
      const countryCode = auth.getCountry();
      const voucherType = _.includes(GIFT_AVAILABLE_IN_COUNTRIES, countryCode) ? voucherCms.value.product : _.filter(voucherCms.value.product, x => x.value.link === GIFT_E_VOUCHER);

      if (voucherType?.length > 0) {
        setVoucher(voucherType[0]);
      }

      return {
        iconBlocks,
        buttonBlocks,
        imageBlocks,
        textBlocks,
        textBlocksHeader,
        faqBlocks,
        voucherType,
        breadcrumb,
        reviews,
        allVouchers: vouchers,
      };
    }
    return {};
  };

  const handleSetGiftSelected = (data) => {
    const model = {
      ...data,
      name: auth.getCountry() === 'vn' ?
        `${data?.title} ${generaCurrency(data?.price)} ` :
        `${generaCurrency(data?.price)} ${data?.title}`,
    };
    setGiftSelected(model);
  };

  const handleUpdateGiftSelected = (value) => {
    const data = {
      ...giftSelected,
      price: value,
    };
    handleSetGiftSelected(data);
  };

  const handleFetchDataInit = async () => {
    const cmsBlock = _.find(cms, x => x.title === 'GiftV2');
    let cmsData = {};

    if (isEmpty(cmsBlock)) {
      dispatch(loadingPage(true));
      const pending = [fetchCMSHomepage('gift-v2'), fetchExchangeRates()];

      try {
        const response = await Promise.all(pending);

        const cmsResponse = response[0];
        dispatch(addCmsRedux(cmsResponse));
        cmsData = handleGetCMSData(cmsResponse);

        const exchangeRates = response[1] || [];
        setExchangeRates(exchangeRates);
      } catch (error) {
        dispatch(loadingPage(false));
        toastrError(error.message);
      }
      dispatch(loadingPage(false));
    } else {
      cmsData = handleGetCMSData(cmsBlock);
    }

    setDataState(cmsData);
  };

  const fetchExchangeRates = () => {
    const option = {
      url: GET_EXCHANGE_RATE,
      method: 'GET',
    };
    return fetchClient(option, true);
  }

  useEffect(() => {
    if (giftSelected?.id && giftSelected?.price && parseFloat(giftSelected.price) !== 0) {
      scrolRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
  }, [giftSelected]);

  useEffect(() => {
    handleFetchDataInit();
    segmentGiftingOfferPageViewed("Gift Cards");
    return () => {
      removeLinkHreflang();
      removeScriptFAQ();
    };
  }, []);

  useEffect(() => {
    if (dataState.faqBlocks?.length > 0) {
      generateScriptFAQ(dataState.faqBlocks, true);
    }
  }, [dataState.faqBlocks]);

  const howItWorksTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'how_it_works');
  const howItWorksData = _.find(dataState.iconBlocks, x => x?.value?.image_background?.caption === 'gift_vouchers_how_it_works');
  const banner = _.find(dataState.imageBlocks, x => x?.value?.caption === 'gift_vouchers_banner');
  const countryName = auth.getCountryName();
  const titleCms = getNameFromButtonBlock(dataState.buttonBlocks, 'seo_gift_vouchers');
  const desCms = getLinkFromButtonBlock(dataState.buttonBlocks, 'seo_gift_vouchers');
  const seoTitle = replaceCountryInMetaTags(countryName, titleCms);
  const seoDes = replaceCountryInMetaTags(countryName, desCms);
  const ourClientTitle = getNameFromButtonBlock(dataState.buttonBlocks, 'our_client_gift_vouchers');
  const ourClientDes = getLinkFromButtonBlock(dataState.buttonBlocks, 'our_client_gift_vouchers');

  return (
    <div className="gift-type gift-voucher">
      <MetaTags>
        <title>
          {seoTitle}
        </title>
        <meta name="description" content={seoDes} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/gift/gift-cards')}
      </MetaTags>
      <HeaderHomePageV3 />

      <div className="gift-wrapper">
        <Breadcrumb breadcrumb={dataState.breadcrumb} />
      </div>

      <GiftBanner
        classNames="gift-voucher-banner gift-dark-banner"
        bannerUrlDesktop={banner?.value?.image}
        bannerUrlMobile={banner?.value?.image}
      >
        <div className="wrapper">
          <div className="block-content-title main-title">
            {dataState.textBlocksHeader?.value?.texts?.length > 0 ? dataState.textBlocksHeader?.value?.texts[3]?.value : ''}
          </div>
          <div className="block-content-description">
            {dataState.textBlocks?.value?.texts?.length > 0 ? dataState.textBlocks?.value?.texts[3]?.value : ''}
          </div>
        </div>
      </GiftBanner>

      <div className="gift-content">
        <HowItWorks data={howItWorksData} title={howItWorksTitle} />

        <GiftCard
          dataVoucher={dataState}
          giftSelected={giftSelected}
          exchangeRates={exchangeRates}
          onSelectGift={handleSetGiftSelected}
          onUpdateGiftSelected={handleUpdateGiftSelected}
        />

        {giftSelected?.id && giftSelected?.price && parseFloat(giftSelected.price) > 0 && (
          <GiftCardDetail
            ref={scrolRef}
            giftSelected={giftSelected}
            dataState={dataState}
            voucher={voucher}
            onVoucherTypeClick={(data) => setVoucher(data)}
          />
        )}

        <ReviewsV3
          dataFAQ={dataState}
          buttonBlocks={dataState.buttonBlocks}
          pageType='gift'
          title={ourClientTitle}
          description={ourClientDes}
        />

        <div className="gift-faqs">
          <BlockFAQV2
            dataFAQ={dataState.faqBlocks?.length > 0 ? dataState.faqBlocks[0]?.value : []}
            buttonBlocks={dataState.buttonBlocks}
          />
        </div>
      </div>
      <FooterV2 />
    </div>
  );
}

export default GiftVoucher;
