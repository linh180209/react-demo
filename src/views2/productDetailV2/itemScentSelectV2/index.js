import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';
import AddIcon from '@mui/icons-material/Add';
import classnames from 'classnames';
import { isMobile } from '../../../DetectScreen/detectIFrame';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import icInfo from '../../../image/icon/icNewInfo.svg';
import ButtonCT from '../../../componentsv2/buttonCT';
import './styles.scss';

function ItemScentSelectV2(props) {
  const onClickRemove = (e) => {
    e.stopPropagation();
    props.onClickRemove(props.data);
  };

  const onClickChooseScent = () => {
    props.onClickChooseScent(props.data);
  };

  const onClickrecommend = (data) => {
    console.log('onClickrecommend', data);
    if (props.onClickrecommend) {
      props.onClickrecommend(data);
    }
  };
  const recommendBt = getNameFromButtonBlock(props.buttonBlocks, 'RECOMMENDED');
  const AddscentBt = getNameFromButtonBlock(props.buttonBlocks, 'Add scent');
  const recommendHtml = (
    props.recommendData && (
      <div className="recommend-data">
        <h3>{recommendBt.replace('{scent_name}', props.recommendDataOfScent)}</h3>
        <div className="list-scent">
          {_.map(props.recommendData, d => (
            <div
              className="image-content"
              onClick={(e) => {
                e.stopPropagation();
                onClickrecommend(d);
              }}
            >
              <img className="scent" src={d.image} alt="icon" />
              <img className="info" src={icInfo} alt="info" />
            </div>
          ))}
        </div>
      </div>
    )
  );

  return (
    <div
      className="item-scent-select-v2"
      onClick={onClickChooseScent}
    >
      <div className="item-scent-select-body">
        {
          props.subHeader && (
            <div className="sub-header">
              {props.subHeader}
            </div>
          )
        }
        {
          props.imageHeader && (
            <div className="image-header">
              <img src={props.imageHeader} alt="header" />
            </div>
          )
        }
        <div className="image-scent">
          <img src={props.image} alt="scent" className={props.isShowDelete ? '' : 'icon'} />
        </div>
        <div className="name-scent div-col">
          <div className="div-row items-center">
            <h3>
              {props.title}
            </h3>
            <span>
              {props.message}
            </span>
          </div>
          <span>
            {props.description}
          </span>
        </div>
        {
          props.isShowAddScent && (
            <ButtonCT
              onlyIcon={isMobile || props.isAddIcon ? <AddIcon style={{ corlor: '#2C2C22' }} /> : undefined}
              name={AddscentBt}
              size="medium"
              variant="outlined"
              className={classnames('bt-add-scent', props.isAddIcon ? 'bt-icon' : '')}
            />
          )
        }
        {
          props.isShowDelete && (
            <ButtonCT
              onlyIcon={<DeleteOutlineIcon style={{ corlor: '#2C2C22' }} />}
              size="small"
              variant="outlined"
              onClick={onClickRemove}
              className="bt-delete-scent"
            />
          )
        }
        {
          props.textRight && (
            <div className="tex-right">
              {props.textRight}
            </div>
          )
        }
      </div>
      {/* {props.recommendData && <div className="line" style={{ marginTop: '15px' }} />} */}
      {/* {recommendHtml} */}
    </div>
  );
}

export default ItemScentSelectV2;
