import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import BlockFAQ from '../../../componentsv2/blockFAQ';

const formatFAQ = (faqs) => {
  if (!faqs) {
    return {};
  }
  const newFormat = _.map(faqs, d => ({
    value: {
      header: {
        header_text: d?.title,
      },
      faq: _.map(d?.lines || [], x => ({
        value: {
          header: {
            header_text: x?.title,
          },
          paragraph: x?.description,
        },
      })),
    },
  }));
  return newFormat;
};

function FormatFAQ(props) {
  const [faqsBlock, setFAQ] = useState(formatFAQ(props.faqs));

  useEffect(() => {
    const list = formatFAQ(props.faqs);
    setFAQ(list);
  }, [props.faqs]);

  return (
    <div className="div-faqs">
      <BlockFAQ faqsBlock={faqsBlock} />
    </div>
  );
}

export default FormatFAQ;
