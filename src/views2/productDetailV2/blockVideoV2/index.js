import React, { useState, useEffect } from 'react';
import YouTube from 'react-youtube';

import HeaderCT from '../../../componentsv2/headerCT';
import { isMobile } from '../../../DetectScreen/detectIFrame';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import USPBlock from '../../../componentsv2/uspBlock';
import './style.scss';

function BlockVideoV2(props) {
  const [idVideo, setIdVideo] = useState(props.videoBlocks[0]?.value?.text);

  const onClickVideo = (id) => {
    setIdVideo({ idVideo: id });
    const ele = document.getElementById('id-block-video');
    if (ele && isMobile) {
      ele.scrollIntoView({ behavior: 'smooth' });
    }
  };

  const videoItem = d => (
    <button type="button" className="bt-image-video" onClick={() => onClickVideo(d.value.text)}>
      <img loading="lazy" src={d.value.image.image} alt="video" />
      <span>{d.value.image.caption}</span>
    </button>
  );

  const openYouTube = () => {
    window.open(`https://www.youtube.com/watch?v=${idVideo}`);
  };

  useEffect(() => {
    setIdVideo(props.videoBlocks[0]?.value?.text);
  }, [props.videoBlocks]);

  const { buttonBlocks, videoBlocks } = props;
  const opts = {
    width: isMobile ? window.innerWidth - 110 : `${(window.innerWidth - 220) * 0.74}`,
    height: isMobile ? (window.innerWidth - 110) * 9 / 16 : `${(window.innerWidth - 220) * 0.74 * 9 / 16}`,
    playerVars: {
      autoplay: 0,
    },
  };

  const learnToBt = getNameFromButtonBlock(buttonBlocks, 'learn_to_mix');
  return (
    <div className="div-block-list-video-v2">
      <HeaderCT type={isMobile ? 'Heading-S' : 'Heading-XL'}>
        {learnToBt}
      </HeaderCT>
      <div className="video">
        <YouTube videoId={idVideo} opts={opts} />
      </div>
      <USPBlock data={props.iconsBlockDiscovery?.value?.icons} />
    </div>
  );
}

export default BlockVideoV2;
