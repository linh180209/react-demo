import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import ItemScentSelectV2 from '../itemScentSelectV2';
import BottleCustom from '../../../components/B2B/CardItem/bottleCustom';
import ButtonCT from '../../../componentsv2/buttonCT';
import HeaderCT from '../../../componentsv2/headerCT';
import auth from '../../../Redux/Helpers/auth';
import { COLOR_CUSTOME } from '../../../constants';
import { isMobile, isBrowser } from '../../../DetectScreen';
import {
  groupBottle, emptyScent, productSelect, icBottleCustom, bottleCustome, oilBurner, icCandle, icCandleOne, icCandleTwo,
} from '../../../imagev2/svg';
import icSub from '../../../image/icon/ic-sub.svg';
import icPlus from '../../../image/icon/ic-plus.svg';
import { getNameFromButtonBlock, generaCurrency, scrollToTargetAdjusted } from '../../../Redux/Helpers';
import './styles.scss';
import PickIngredient from '../../../components/ProductAllV2/ProductBlock/pickIngredient';
import IngredientBlock from '../ingredientBlock';
import ReceiveGrams from '../../../components/receiveGrams';

function SelectProductScent(props) {
  const [isShowSelectScent, setIsShowSelectScent] = useState(false);
  const [dataCurrentSelect, setDataCurrentSelect] = useState({});
  const [itemWaxSelect, setItemWaxSelect] = useState(props.type.name === 'hand_sanitizer' ? props.itemHandlerSanitizer : props.type.name === 'home_scents' ? props.items[0] : (_.find(props.items, x => x.is_sample === false) || props.items[0]));
  const [combosState, setCombosState] = useState(_.cloneDeep(props.combosChooseScent));

  const updateDataCurrentSelect = (data) => {
    setDataCurrentSelect(data);
  };

  const onClickSelectScent = () => {
    setIsShowSelectScent(true);
  };

  const onClickSelectScentCancel = () => {
    setIsShowSelectScent(false);
    setDataCurrentSelect({});
  };

  const onClickCloseScent = () => {
    onClickSelectScentCancel();
    props.onClickClose();
  };

  const getImageFromProduct = (data) => {
    const ele = _.find(data.images, d => d.type === 'unisex');
    if (ele) {
      return ele.image;
    }
    return '';
  };

  const onClickRemoveScent = (data, index) => {
    console.log('onClickRemoveScent', data, index);
    const { itemCombo } = props;
    delete itemCombo.id;
    // _.remove(combosState, x => x.id === data.id);
    const indexEle = _.findIndex(combosState, x => x.id === data.id);
    if (indexEle > -1) {
      combosState.splice(indexEle, 1);
    }
    console.log('combosState', combosState);
    setCombosState(_.cloneDeep(combosState));
    props.updateScentSelect(combosState);
    props.onClickApply(undefined, index);
  };

  const gotoAnchorScent = () => {
    scrollToTargetAdjusted('anchor-character');
  };

  useEffect(() => {
    setCombosState(_.cloneDeep(props.combosChooseScent));
  }, [props.combosChooseScent]);

  useEffect(() => {
    const item = props.type.name === 'hand_sanitizer' ? props.itemHandlerSanitizer : props.type.name === 'home_scents' ? props.items[0] : (_.find(props.items, x => x.is_sample === false) || props.items[0]);
    setItemWaxSelect(item);
  }, [props.type, props.itemHandlerSanitizer, props.items]);

  useEffect(() => {
    setIsShowSelectScent(props.isShowSelectScent);
  }, [props.isShowSelectScent]);

  useEffect(() => {
    if (auth.getIsAddScent()) {
      auth.setIsAddScent(false);
      scrollToTargetAdjusted('anchor-select-scent');
    }
  }, []);

  const {
    type, arrayThumbs, itemCombo, combos, buttonBlocks,
  } = props;

  const ingredient1St = getNameFromButtonBlock(buttonBlocks, '1ST INGREDIENT');
  const ingredient2ndBt = getNameFromButtonBlock(buttonBlocks, '2ND INGREDIENT');
  const ingredient3rdBt = getNameFromButtonBlock(buttonBlocks, '3RD INGREDIENT');
  const ingredient4thBt = getNameFromButtonBlock(buttonBlocks, '4TH INGREDIENT');
  const chooseIngredientBt = getNameFromButtonBlock(buttonBlocks, 'CHOOSE INGREDIENT');
  const requiredBt = getNameFromButtonBlock(buttonBlocks, 'Required');
  const outStockBt = getNameFromButtonBlock(buttonBlocks, 'Out Of Stock');
  const optionCandleBt = getNameFromButtonBlock(buttonBlocks, 'Optional Candle Holder') || '';
  const optionalBt = getNameFromButtonBlock(buttonBlocks, 'Optional');
  const viewScentBt = getNameFromButtonBlock(buttonBlocks, 'View Scent Characteristics');
  const productSummaryBt = getNameFromButtonBlock(buttonBlocks, 'PRODUCT SUMMARY');
  const personalizeYourBt = getNameFromButtonBlock(buttonBlocks, 'personalize YOUR BOTTLE');
  const engraveBt = getNameFromButtonBlock(buttonBlocks, 'Engrave and add decorations to your bottle here with your own design to make it perfect');
  const scentRequired1st = getNameFromButtonBlock(buttonBlocks, '1st Scent Required');
  const scentRequired2nd = getNameFromButtonBlock(buttonBlocks, '2nd Scent Optional');
  const select3rd = getNameFromButtonBlock(buttonBlocks, 'Select the 3rd scent');
  const select4th = getNameFromButtonBlock(buttonBlocks, 'Select the 4th scent');
  const SelectBt = getNameFromButtonBlock(buttonBlocks, 'Select');
  const yourScentBt = getNameFromButtonBlock(buttonBlocks, 'Your Scent Characteristics');
  const noIngredientBt = getNameFromButtonBlock(buttonBlocks, 'No ingredients selected. Select your ingredients');
  const addToCartBt = getNameFromButtonBlock(buttonBlocks, 'Add to cart');
  const PersonalizeBottleBt = getNameFromButtonBlock(buttonBlocks, 'Personalize Bottle');
  const OptionalBt = getNameFromButtonBlock(buttonBlocks, 'Optional');
  const scentRequiredBt = getNameFromButtonBlock(buttonBlocks, 'Scent Required');
  const scentRequired3rdBt = getNameFromButtonBlock(buttonBlocks, '3rd Scent Required');
  const scentRequired4thBt = getNameFromButtonBlock(buttonBlocks, '4th Scent Required');
  const lucioleBt = getNameFromButtonBlock(buttonBlocks, 'Luciole Oil Burner');

  const isShowBtCustome = (type.name === 'Creation' && arrayThumbs && arrayThumbs.length > 0) || ((type.name === 'Perfume' || type.name === 'bundle_creation' || type.name === 'perfume_diy') && !itemCombo.isSample) || type.name === 'Elixir';
  const isShowDesGenerateCombo = (type.name === 'Perfume' || type.name === 'bundle_creation' || type.name === 'perfume_diy' || type.name === 'car_diffuser' || type.name === 'reed_diffuser') && combos && combos.length > 0;
  const isDualCandles = type.name === 'dual_candles';
  const isSingleCandle = type.name === 'single_candle';
  const isMiniCandle = type.name === 'mini_candles';
  const isWaxPerfumeCreate = type.name === 'Wax_Perfume';
  const isOilBurner = type.name === 'oil_burner';
  const isDualCrayons = type.name === 'dual_crayons';
  const lineChooseScent = (
    <React.Fragment>
      {
        isOilBurner && (
          <ItemScentSelectV2
            data={undefined}
            image={oilBurner}
            title={lucioleBt}
            description={undefined}
            // onClickRemove={data => this.onClickRemoveScent(data, 1)}
            // onClickChooseScent={data => this.props.onClickChooseScent(data, 1)}
            onClickChooseScent={() => {}}
            textRight="1x"
          />
        )
      }
      <ItemScentSelectV2
        data={combosState && combosState.length > 0 ? combosState[0] : undefined}
        image={combosState && combosState.length > 0 && !_.isEmpty(combosState[0]) ? getImageFromProduct(combosState[0].product) : ['single_candle', 'dual_candles'].includes(type?.name) ? icCandleOne : ['mini_candles'].includes(type?.name) ? icCandle : productSelect}
        title={combosState && combosState.length > 0 && !_.isEmpty(combosState[0]) ? combosState[0].name : isWaxPerfumeCreate ? chooseIngredientBt : ingredient1St}
        description={combosState && combosState.length > 0 && !_.isEmpty(combosState[0]) ? ingredient1St : isWaxPerfumeCreate ? undefined : requiredBt}
        isShowDelete={combosState && combosState.length > 0 && !_.isEmpty(combosState[0])}
        onClickRemove={data => onClickRemoveScent(data, 1)}
        buttonBlocks={props.buttonBlocks}
        onClickChooseScent={data => props.onClickChooseScent(data, 1)}
        message={combosState && combosState.length > 0 && combosState[0]?.is_out_of_stock ? outStockBt : undefined}
        isShowAddScent={!combosState || combosState.length === 0 || _.isEmpty(combosState[0])}
        isAddIcon={isOilBurner}
      />
      {
        !['Wax_Perfume'].includes(type.name) && (
          <ItemScentSelectV2
            data={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? combosState[1] : undefined}
            image={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? getImageFromProduct(combosState[1].product) : ['single_candle', 'dual_candles'].includes(type?.name) ? icCandleTwo : ['mini_candles'].includes(type?.name) ? icCandle : productSelect}
            title={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? combosState[1].name : ingredient2ndBt}
            description={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? ingredient2ndBt : (isDualCandles || isMiniCandle) ? requiredBt : isSingleCandle ? optionCandleBt.replace('{price}', generaCurrency(itemCombo ? itemCombo.price : '')) : optionalBt}
            isShowDelete={combosState && combosState.length > 1 && !_.isEmpty(combosState[1])}
            onClickRemove={data => onClickRemoveScent(data, 2)}
            onClickChooseScent={data => props.onClickChooseScent(data, 2)}
            // recommendData={props.recommendData?.suggestions}
            // recommendDataOfScent={props.recommendData?.name}
            onClickrecommend={props.onClickrecommend}
            buttonBlocks={props.buttonBlocks}
            message={combosState && combosState.length > 1 && combosState[1]?.is_out_of_stock ? outStockBt : undefined}
            isShowAddScent={!combosState || combosState.length < 2 || _.isEmpty(combosState[1])}
            isAddIcon={isOilBurner}
          />
        )
      }
      {
        isMiniCandle && (
          <React.Fragment>
            <ItemScentSelectV2
              data={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? combosState[2] : undefined}
              image={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? getImageFromProduct(combosState[2].product) : icCandle}
              title={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? combosState[2].name : ingredient3rdBt}
              description={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? ingredient3rdBt : select3rd}
              isShowDelete={combosState && combosState.length > 2 && !_.isEmpty(combosState[2])}
              onClickRemove={data => onClickRemoveScent(data, 3)}
              onClickChooseScent={data => props.onClickChooseScent(data, 3)}
              buttonBlocks={props.buttonBlocks}
              message={combosState && combosState.length > 2 && combosState[2]?.is_out_of_stock ? outStockBt : undefined}
              isShowAddScent={!combosState || combosState.length < 3 || _.isEmpty(combosState[2])}
            />
            <ItemScentSelectV2
              data={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? combosState[3] : undefined}
              image={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? getImageFromProduct(combosState[3].product) : icCandle}
              title={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? combosState[3].name : ingredient4thBt}
              description={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? ingredient4thBt : select4th}
              isShowDelete={combosState && combosState.length > 3 && !_.isEmpty(combosState[3])}
              onClickRemove={data => onClickRemoveScent(data, 4)}
              onClickChooseScent={data => props.onClickChooseScent(data, 4)}
              buttonBlocks={props.buttonBlocks}
              message={combosState && combosState.length > 3 && combosState[3]?.is_out_of_stock ? outStockBt : undefined}
              isShowAddScent={!combosState || combosState.length < 4 || _.isEmpty(combosState[3])}
            />
          </React.Fragment>
        )
      }
      {
        isShowBtCustome && isMobile && (
          <ItemScentSelectV2
            data={undefined}
            image={props.currentImg || bottleCustome}
            title={props.nameBottle || PersonalizeBottleBt}
            description={props.currentImg || props.nameBottle ? PersonalizeBottleBt : OptionalBt}
            onClickChooseScent={props.toggleCustom}
            buttonBlocks={props.buttonBlocks}
          />
        )
      }
      {
        combosState?.length > 0 && isMobile && (
          <div className="div-view-scent" onClick={gotoAnchorScent}>
            <div>
              {viewScentBt}
            </div>
            <ArrowDownwardIcon style={{ color: '#2c2c2c' }} />
          </div>
        )
      }
    </React.Fragment>
  );

  const productSummaryHtml = (
    <div className="product-summary">
      {
        isBrowser && (
          <HeaderCT type={isMobile ? 'Heading-S' : 'Heading-M'}>
            {productSummaryBt}
          </HeaderCT>
        )
      }

      {isBrowser && lineChooseScent}
      {
        isShowBtCustome && (
          <div className="custome-bottle" onClick={props.toggleCustom}>
            <div className="div-image">
              <img src={icBottleCustom} alt="icon" />
            </div>
            <div className="text-bottle">
              <div className={classnames('other-Tagline', isMobile ? 's-size' : '')}>
                {personalizeYourBt}
              </div>
              <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
                {engraveBt}
              </div>
            </div>
            {
              !isMobile && (
                <div className="icon-next">
                  <ArrowForwardIcon style={{ color: '#2C2C22' }} />
                </div>
              )
            }
          </div>
        )
      }

      <div className="text-price">
        {generaCurrency(itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : '')}
      </div>

      <ReceiveGrams isPointProductDetail buttonBlocks={buttonBlocks} price={itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : 0} />

      <div className="line-button div-row">
        <div className="div-button-quantity div-row">
          <button
            disabled={props.quantity < 2}
            type="button"
            className={classnames('button-bg__none pr-4', props.quantity < 2 ? 'disabled' : '')}
            onClick={() => props.onChangeQuantity({ target: { value: props.quantity - 1 } })}
          >
            {
              props.isNotShowSub
                ? (
                  <div style={{ width: '11px' }} />
                )
                : (
                  <img loading="lazy" src={icSub} alt="icSub" />
                )
            }
          </button>
          <span>
            {props.quantity}
          </span>
          <button
            type="button"
            // disabled={isNotShowPlus}
            className="button-bg__none pl-4"
            onClick={() => props.onChangeQuantity({ target: { value: props.quantity + 1 } })}
          >
            {
              props.isNotShowPlus
                ? (
                  <div style={{ width: '11px' }} />
                )
                : (
                  <img loading="lazy" src={icPlus} alt="icPlus" />
                )
            }
          </button>
        </div>
        <ButtonCT
          name={addToCartBt}
          size={isMobile ? 'medium' : undefined}
          onClick={() => {
            if (type.name === 'hand_sanitizer') {
              props.addToCartItem(itemWaxSelect);
            } else {
              props.onClickAddtoCart();
            }
          }
          }
        />
      </div>
    </div>
  );

  const isWax = props.type.name === 'Wax_Perfume';
  const scentBottleHtml = (
    <div className={classnames('block-scent-bottle', isWax ? 'one-image' : '', isMiniCandle ? 'mini-candle' : '')}>
      <div className="div-empty-scent image-left" onClick={() => props.onClickChooseScent(combosState && combosState.length > 0 ? combosState[0] : undefined, 1)}>
        {
            combosState?.length > 0 && !_.isEmpty(combosState[0]) ? (
              <img className="full-image" src={getImageFromProduct(combosState[0].product)} alt="scent" />
            ) : (
              <div className="content">
                {
                  !isMiniCandle && <img className="img-empty" src={isBrowser && !isWax ? groupBottle : productSelect} alt="icon" />
                }
                <div className="des">
                  {isWax ? scentRequiredBt : isBrowser ? scentRequired1st : '1st' }
                </div>
                {
                  isBrowser && (
                    <ButtonCT name={SelectBt} color="secondary" onClick={() => props.onClickChooseScent(combosState && combosState.length > 0 ? combosState[0] : undefined, 1)} />
                  )
                }
              </div>
            )
          }
      </div>
      <div className="div-empty-scent image-right" onClick={() => props.onClickChooseScent(combosState && combosState.length > 1 ? combosState[1] : undefined, 2)}>
        {
            combosState?.length > 1 && !_.isEmpty(combosState[1]) ? (
              <img className="full-image" src={getImageFromProduct(combosState[1].product)} alt="scent" />
            ) : (
              <div className="content">
                {
                  !isMiniCandle && <img className="img-empty" src={isBrowser ? groupBottle : productSelect} alt="icon" />
                }
                <div className="des">
                  {isBrowser ? scentRequired2nd : '2nd' }
                </div>
                {
                  isBrowser && (
                    <ButtonCT name={SelectBt} color="secondary" onClick={() => props.onClickChooseScent(combosState && combosState.length > 1 ? combosState[1] : undefined, 2)} />
                  )
                }
              </div>
            )
          }
      </div>
      {
        isMiniCandle && (
          <React.Fragment>
            <div className="div-empty-scent image-left-bottom" onClick={() => props.onClickChooseScent(combosState && combosState.length > 2 ? combosState[2] : undefined, 3)}>
              {
                  combosState?.length > 2 && !_.isEmpty(combosState[2]) ? (
                    <img className="full-image" src={getImageFromProduct(combosState[2].product)} alt="scent" />
                  ) : (
                    <div className="content">
                      <div className="des">
                        {isBrowser ? scentRequired3rdBt : '3rd'}
                      </div>
                      {
                        isBrowser && (
                          <ButtonCT name={SelectBt} color="secondary" onClick={() => props.onClickChooseScent(combosState && combosState.length > 2 ? combosState[2] : undefined, 3)} />
                        )
                      }
                    </div>
                  )
                }
            </div>
            <div className="div-empty-scent image-right-bottom" onClick={() => props.onClickChooseScent(combosState && combosState.length > 3 ? combosState[3] : undefined, 4)}>
              {
                  combosState?.length > 3 && !_.isEmpty(combosState[3]) ? (
                    <img className="full-image" src={getImageFromProduct(combosState[3].product)} alt="scent" />
                  ) : (
                    <div className="content">
                      <div className="des">
                        {isBrowser ? scentRequired4thBt : '4th'}
                      </div>
                      {
                        isBrowser && (
                          <ButtonCT name={SelectBt} color="secondary" onClick={() => props.onClickChooseScent(combosState && combosState.length > 3 ? combosState[3] : undefined, 4)} />
                        )
                      }
                    </div>
                  )
                }
            </div>
          </React.Fragment>
        )
      }
      <div className={classnames('div-image-type', ['Wax_Perfume', 'home_scents'].includes(props.type?.name) ? 'wax-type' : '')}>
        {
          ['Wax_Perfume', 'home_scents', 'mini_candles', 'oil_burner', 'dual_crayons', 'single_candle', 'dual_candles', 'reed_diffuser', 'car_diffuser'].includes(props.type?.name) ? (
            <div className="image-product">
              <img src={_.find(props.dataFAQ?.images || [], x => x.type === 'main')?.image} alt="icon" />
            </div>
          ) : (
            <BottleCustom
              isImageText={!!props.dataCustom?.image}
              onGotoProduct={() => {}}
              image={props.dataCustom?.image}
              isBlack={props.dataCustom?.color === COLOR_CUSTOME.BLACK}
              font={props.dataCustom?.font}
              color={props.dataCustom?.color}
              eauBt={getNameFromButtonBlock(props.buttonBlocks, 'Eau de parfum')}
              mlBt={getNameFromButtonBlock(props.buttonBlocks, 'ml25')}
              isDisplayName
              name={props.dataCustom?.nameBottle}
              combos={props.dataCustom?.combos}
              classWrap="bottle-product-detail-v2"
            />
          )
        }
      </div>
    </div>
  );

  const blockCharecterHtml = (
    <div className="block-character-scent">
      <div id="anchor-character" />
      {
      combosState && combosState.length > (isMiniCandle ? 3 : 0) ? (
        <IngredientBlock
          cmsCommon={props.cmsCommon}
          profile={props.profile}
          data={combosState.length === 1 ? combosState[0]?.product : undefined}
          datas={combosState.length > 1 ? combosState : undefined}
          className={isMobile ? 'mobile-character' : ''}
          isMiniCandle={isMiniCandle}
        />
      ) : (
        <div className="char-empty">
          <HeaderCT type={isMobile ? 'Heading-S' : 'Heading-M'}>
            {yourScentBt}
          </HeaderCT>
          <img src={emptyScent} alt="icon" />
          <div className={classnames('body-text-s-regular', isMobile ? '' : 'l-size')}>
            {noIngredientBt}
          </div>
        </div>
      )
    }
    </div>
  );
  const frontHtml = (
    <div className="div-select-scent-front">

      {scentBottleHtml}

      {blockCharecterHtml}

      {productSummaryHtml}
    </div>
  );

  const backHtml = (
    <div className="div-select-scent-back">
      <div className="select-scent-back">
        <PickIngredient
          // ref={this.refPickIngredient}
          onClickCloseScent={onClickCloseScent}
          dataSelection={props.dataScentSelection}
          dataScents={props.dataScents}
          onClickApply={props.onClickApply}
          loadingPage={props.loadingPage}
          onClickIngredient={props.onClickIngredient}
          typeSelection={props.typeSelection}
          buttonBlocks={props.buttonBlocks}
          onChangeFilter={props.onChangeFilter}
          onChangeSearch={props.onChangeSearch}
          // shareRefFilterSearchScent={this.props.shareRefFilterSearchScent}
          isDualCandles={props.isDualCandles}
          isShowHome={props.isShowHome}
          onClickChangeImageCandle={props.onClickChangeImageCandle}
          isShowIngredientDetail={props.isShowIngredientDetail}
          // isOnlyShowIngredientLocal={this.state.isOnlyShowIngredientLocal}
          isHidenHeader
          titleHeader="Select scent"
          className="pick-ingredients-product-detail-v2"
          isPickIngredientDetailV2
          updateDataCurrentSelect={updateDataCurrentSelect}
          scentRecommend={props.recommendData}
        />
      </div>
      <div className="info-character">
        {
          !_.isEmpty(dataCurrentSelect) ? (
            <IngredientBlock data={dataCurrentSelect} />
          ) : (
            <div className="info-empty div-col">
              <img src={emptyScent} alt="icon" />
              <div className={classnames('body-text-s-regular', isMobile ? '' : 'l-size')}>
                {noIngredientBt}
              </div>
            </div>
          )
        }
      </div>
    </div>
  );

  if (isMobile) {
    return (
      <div className="div-select-scent-flip div-col">
        <div id="anchor-select-scent" />
        <div className="select-scent div-row">
          {scentBottleHtml}
          <div className="list-scent-mobile div-col">
            {lineChooseScent}
          </div>
        </div>
        {productSummaryHtml}
        {blockCharecterHtml}
      </div>
    );
  }
  return (
    <div
      style={!_.isEmpty(dataCurrentSelect) ? { height: '650px' } : isShowSelectScent ? { height: '650px' } : {}}
      className={classnames('div-select-scent-flip', isShowSelectScent ? 'show-choose-scent' : '')}
    >
      <div id="anchor-select-scent" />
      <div className="flip-card-inner">
        {frontHtml}
        {isShowSelectScent && backHtml}
      </div>
    </div>

  );
}

export default SelectProductScent;
