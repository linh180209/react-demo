import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import { useSelector, useDispatch } from 'react-redux';
import { isMobile } from '../../../DetectScreen';
import {
  icSpicy,
  icCitrus,
  icFresh,
  icOriental,
  emptyScent,
} from '../../../imagev2/svg';
import './styles.scss';
import VideoAcademy from '../../../components/AcademyItem/videoAcademy';
import ColumnChart from '../../../componentsv2/columnChart';
import ProgressCircleV2 from '../../../views/ResultScentV2/progressCircleV2';
import { fetchCMSHomepage, getNameFromCommon, getNameFromButtonBlock } from '../../../Redux/Helpers';
import addCmsRedux from '../../../Redux/Actions/cms';
import updateIngredientsData from '../../../Redux/Actions/ingredients';
import { toastrError } from '../../../Redux/Helpers/notification';
import { fetchScentLib } from '../../../Redux/Helpers/fetchAPI';

const getCMS = (data) => {
  if (data) {
    const { body } = data;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    return {
      buttonBlocks,
    };
  }
  return {
    buttonBlocks: [],
  };
};

function IngredientBlock(props) {
  const dispatch = useDispatch();
  const cms = useSelector(state => state.cms);
  const ingredients = useSelector(state => state.ingredients);
  const [dataIngredient, setDataIngredient] = useState();
  const [comboData, setComboData] = useState();
  const [index, setIndex] = useState(0);
  const [buttonBlocks, setButtonBlocks] = useState([]);

  const fetchDataInit = () => fetchCMSHomepage('ingredient-detail');

  const fetchData = async () => {
    const ingredientBlock = _.find(cms, x => x.title === 'Ingredient Detail');
    if (!ingredients || ingredients.length === 0 || !ingredientBlock) {
      const pending = [fetchDataInit(), fetchScentLib()];
      try {
        const results = await Promise.all(pending);
        dispatch(addCmsRedux(results[0]));
        dispatch(updateIngredientsData(results[1]));
        const cmsState = getCMS(results[0]);
        setButtonBlocks(cmsState?.buttonBlocks);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const cmsState = getCMS(ingredientBlock);
      setButtonBlocks(cmsState?.buttonBlocks);
    }
  };

  useEffect(() => {
    if (!_.isEmpty(props.data)) {
      let data;
      if (props.data?.ingredient?.id) {
        data = _.find(ingredients, x => x.ingredient?.id === props.data?.ingredient?.id);
      } else {
        data = _.find(ingredients, x => x.id === props.data.id);
      }
      setDataIngredient(data);
    }
  }, [props.data, ingredients]);

  useEffect(() => {
    if (!_.isEmpty(props.datas) && !_.isEmpty(props.cmsCommon)) {
      const comboScent = getNameFromCommon(props.cmsCommon, 'COMBO_SCENT');
      const listDatas = [];
      listDatas.push({
        id: 0,
        image1: _.find(props.datas[0]?.product.images || [], x => x.type === 'unisex')?.image,
        image2: _.find(props.datas[1]?.product.images || [], x => x.type === 'unisex')?.image,
        name: `${props.datas[0].name} & ${props.datas[1].name}`,
        title: comboScent,
        profile: props.profile,
      });
      listDatas.push(props.datas[0].product);
      listDatas.push(props.datas[1].product);
      if (props.isMiniCandle) {
        listDatas.push(props.datas[2].product);
        listDatas.push(props.datas[3].product);
      }
      console.log('listDatas', listDatas);
      setComboData(listDatas);
    }
  }, [props.datas, ingredients]);

  useEffect(() => {
    fetchData();
  }, []);

  if (_.isEmpty(props.datas) && _.isEmpty(dataIngredient)) {
    const emptyBt = getNameFromButtonBlock(buttonBlocks, 'No ingredients selected. Select your ingredients');
    return (
      <div className="ingredients-block">
        <div className="div-empty">
          <img src={emptyScent} alt="icon" />
          <div className={classnames('body-text-s-regular', isMobile ? '' : 'l-size')}>
            {emptyBt}
          </div>
        </div>
      </div>
    );
  }

  if (!_.isEmpty(dataIngredient)) {
    const {
      name, videos, ingredient, profile,
    } = dataIngredient;
    const EVOLUTIONBt = getNameFromButtonBlock(buttonBlocks, 'EVOLUTION OF SCENT');
    const ScentBt = getNameFromButtonBlock(buttonBlocks, 'Scent Characteristics');
    const LastingBt = getNameFromButtonBlock(buttonBlocks, 'Lasting');
    const StrengthBt = getNameFromButtonBlock(buttonBlocks, 'Strength');
    return (
      <div className={classnames('ingredients-block', props.className)}>
        <div className="block-header">
          <div className="content-block-header div-col">
            <div className={classnames('other-Tagline tc', isMobile ? 's-size' : '')}>
              <b>{ingredient?.title}</b>
            </div>
            <div className="text-header tc">
              {name}
            </div>
            <div className="list-char div-row">
              {
                _.map(ingredient?.scents, x => (
                  <div>
                    {x}
                  </div>
                ))
              }
            </div>
          </div>
        </div>

        <div className="div-video">
          <VideoAcademy url={videos[0]?.video} poster={videos[0]?.placeholder} isControl />
        </div>

        <div className="block-bottom">
          <div className="content-block-bottom div-col">
            <div className="block-evolution div-col">
              <div className={classnames('other-Tagline w-100 tc', isMobile ? 's-size' : '')}>
                {EVOLUTIONBt}
              </div>
              <div className="div-image">
                <img loading="lazy" src={ingredient?.scent_evolution} alt="evolution" />
                {
                  _.map(ingredient?.accords, x => (
                    <span>{x}</span>
                  ))
                }
              </div>
            </div>

            <hr />

            <div className="block-scent">
              <div className="block-scent-1">
                <div className={classnames('other-Tagline tc', isMobile ? 's-size' : '')}>
                  {ScentBt}
                </div>
                <div className="div-progress">
                  <ProgressCircleV2 title={LastingBt} percent={profile ? parseInt(parseFloat(profile.strength) * 100, 10) : 0} />
                  <ProgressCircleV2 title={StrengthBt} percent={profile ? parseInt(parseFloat(profile.duration) * 100, 10) : 0} />
                </div>
              </div>
              <div className="block-scent-2">
                <div className={classnames('other-Tagline tc', isMobile ? 's-size' : '')}>
                  {ScentBt}
                </div>
                <div className="block-column">
                  {
                    _.map(profile ? profile.accords : [], d => (
                      <ColumnChart title={d.name} color={d.color} weight={d.weight} />
                    ))
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  const dataSelect = _.find(comboData, x => x.id === index);
  const listIcon = [icSpicy, icOriental, icFresh, icCitrus];
  const ScentBt = getNameFromButtonBlock(buttonBlocks, 'Scent Characteristics');
  const LastingBt = getNameFromButtonBlock(buttonBlocks, 'Lasting');
  const StrengthBt = getNameFromButtonBlock(buttonBlocks, 'Strength');
  return (
    <div className={classnames('ingredients-block', props.className)}>
      <div className="block-header">
        <div className="content-block-header div-col">
          <div className={classnames('other-Tagline tc', isMobile ? 's-size' : '')}>
            <b>{dataSelect?.title || dataSelect?.ingredient?.title }</b>
          </div>
          <div className="text-header tc">
            {dataSelect?.name}
          </div>
        </div>
      </div>
      <div className={classnames('div-button-scent', props.isMiniCandle ? 'mini-candle' : '')}>
        {
          _.map(comboData, d => (
            d.id === 0 ? (
              <div
                onClick={() => setIndex(0)}
                className={classnames('div-bt-image-combo image-2', d.id === index ? 'active' : '')}
              >
                <img
                  loading="lazy"
                  src={d.image1}
                  alt="scent"
                />
                <img
                  loading="lazy"
                  src={d.image2}
                  alt="scent"
                />
                <div className="name-title">
                  {d.title}
                </div>
              </div>
            ) : (
              <div
                onClick={() => setIndex(d.id)}
                className={classnames('div-bt-image-combo', d.id === index ? 'active' : '')}
              >
                <img
                  loading="lazy"
                  src={_.find(d?.images || [], x => x.type === 'unisex')?.image}
                  alt="scent"
                />
              </div>
            )
          ))
        }
      </div>

      <div className="block-bottom">
        <div className={classnames('content-block-bottom div-col', dataSelect?.id === 0 ? 'combo-version' : '')}>
          <div className="block-scent">
            <div className={classnames('block-scent-1', dataSelect?.id === 0 ? 'div-combo' : '')}>
              {
                dataSelect?.id !== 0 && (
                  <div className={classnames('other-Tagline tc', isMobile ? 's-size' : '')}>
                    {ScentBt}
                  </div>
                )
              }

              <div className="div-progress">
                <ProgressCircleV2 title={LastingBt} percent={parseInt(dataSelect?.profile?.duration * 100, 10)} />
                <ProgressCircleV2 title={StrengthBt} percent={parseInt(dataSelect?.profile?.strength * 100, 10)} />
              </div>
            </div>
            <div className={classnames('block-scent-2', dataSelect?.id === 0 ? 'progress-combo' : '')}>
              {
                dataSelect?.id !== 0 && (
                  <div className={classnames('other-Tagline tc', isMobile ? 's-size' : '')}>
                    {ScentBt}
                  </div>
                )
              }

              <div className="block-column">
                {
                  _.map(dataSelect?.profile?.accords, (d, i) => (
                    <ColumnChart icon={listIcon[i]} title={d.name} color={d.color} weight={d.weight} />
                  ))
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default IngredientBlock;
