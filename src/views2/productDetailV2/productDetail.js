/* eslint-disable react/sort-comp */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'react-bootstrap';
import _ from 'lodash';
import classnames from 'classnames';
import { Modal, ModalBody } from 'reactstrap';
import 'react-image-crop/dist/ReactCrop.css';
import 'react-image-gallery/styles/css/image-gallery.css';
import { isIOS } from 'react-device-detect';
import CardKnowMore from '../../components/CardKnowMore';

import '../../styles/product-detail.scss';
import '../../../node_modules/slick-carousel/slick/slick.css';
import '../../../node_modules/slick-carousel/slick/slick-theme.css';
import {
  generateUrlWeb, getCmsCommon, getNameFromButtonBlock, addFontCustom, onClickProduct, scrollToTargetAdjusted, getLinkFromButtonBlock,
} from '../../Redux/Helpers';

import ImageBlock from '../../views/ProductB2C/imageBlock';
import TextBlock from '../../views/ProductB2C/textBlock';
import CustomeBottleV4 from '../../componentsv2/customeBottleV4';
import { FONT_CUSTOME, COLOR_CUSTOME } from '../../constants';
import DiscoverScent from '../../views/ProductB2C/discoverScent'; // don't move
import BlockVideoV2 from './blockVideoV2';
import { toastrError } from '../../Redux/Helpers/notification';
import PremadeProduct from '../../components/ProductAllV2/ProductBlock/premadeProduct';
import {
  GET_ALL_SCENTS_PRODUCTS_CANDLE, GET_ALL_SCENTS_PRODUCTS_DUAL_CRAYONS, GET_ALL_SCENTS_PRODUCTS_HOME_SCENTS, GET_ALL_SCENTS_PRODUCTS_PERFUME, GET_ALL_SCENTS_PRODUCTS_WAX, GET_PRODUCT_FOR_CART, GET_BEST_SELLER_PERFUME, GET_SCENTED_CANDLES, GET_SCENTED_DUAL_CRAYONS, GET_SCENTED_HOME, GET_SCENTED_REED_DIFFUSER, GET_ALL_SCENTS_PRODUCTS_PERFUME_DIY, GET_SCENTED_CAR_DIFFUSER, GET_ALL_SCENTS_PRODUCTS_MINI_CANDLE, GET_SCENTED_MINI_CANDLES, GET_ALL_SCENTS_PRODUCTS_OIL_BURNER, GET_SCENTED_OIL_BURNER, GET_ALL_SCENTS_PRODUCTS_REED_DIFFUSER,
} from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { isBrowser, isMobile } from '../../DetectScreen';
import SelectScent from '../../views/MMO2/components/MultiStep/selectScent';
import SelectProductScent from './selectProductScent';
import auth from '../../Redux/Helpers/auth';

const preDataGroup = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { tags } = x;
    _.forEach(tags, (tag) => {
      const group = _.find(dataScents, d => d.tag === tag.name);
      if (group) {
        group.datas.push(x);
      } else {
        dataScents.push({
          tag: tag.name,
          group: tag.group,
          datas: [x],
        });
      }
    });
  });
  return dataScents;
};

const preDataAlphabet = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { name } = x;
    const firstChart = name.charAt(0);
    const group = _.find(dataScents, d => d.tag.toLowerCase() === firstChart.toLowerCase());
    if (group) {
      group.datas.push(x);
    } else {
      dataScents.push({
        tag: firstChart,
        datas: [x],
      });
    }
  });
  return dataScents;
};

const preDataScents = (scents) => {
  const dataScents = [];

  dataScents.push({
    datas: _.orderBy(preDataGroup(scents), ['tag'], ['asc']),
    title: ['Scent Family', 'Gender', 'mood', 'Scent Note'],
  });

  dataScents.push({
    datas: _.orderBy(preDataAlphabet(scents), ['tag'], ['asc']),
    title: ['Alphabet'],
  });

  dataScents.push({
    datas: [{
      tag: '',
      datas: _.orderBy(scents, ['popularity'], ['desc']),
    }],
    title: ['Popularity'],
  });
  return dataScents;
};

const addScentNoteIntoScents = (scents, scentNotes) => {
  const scentClones = _.cloneDeep(scents);
  _.forEach(scentNotes, (d) => {
    const { name, tags } = d;
    _.forEach(tags, (tag) => {
      const { products } = tag;
      _.forEach(products, (product) => {
        const ele = _.find(scentClones, x => x.id === product.id);
        if (ele) {
          _.assign(ele, product);
          ele.tags.push({
            name,
            group: 'Scent Note',
          });
        }
      });
    });
  });
  return scentClones;
};

const getDataCustome = (propsState) => {
  let name;
  let imageCustome;
  let isCustome;
  let font = FONT_CUSTOME.JOST;
  let idCartItem;
  let quantity;
  let color = COLOR_CUSTOME.BLACK;
  if (propsState) {
    const { custome } = propsState;
    if (custome) {
      name = custome.name;
      imageCustome = custome.image;
      isCustome = custome.isCustome;
      font = custome.font;
      color = custome.color;
      idCartItem = custome.idCartItem;
      quantity = custome.quantity;
    }
  }
  return {
    name, imageCustome, isCustome, font, idCartItem, quantity, color,
  };
};

class ProductDetail extends PureComponent {
  constructor(props) {
    super(props);
    this.images = [];
    const {
      name, isCustome, font, color, idCartItem, quantity,
    } = getDataCustome(
      props.propsState,
    );
    console.log('quantity', quantity, _.isEmpty(quantity));
    this.state = {
      isShowKnowMore: false,
      isShowViewFullImage: false,
      dataPopup: {
        title: '',
        content: '',
        imageKnowMore: '',
      },
      nameBottle: name,
      arrayThumbs: [],
      currentImg: '',
      isShowCustom: isCustome,
      font,
      color,
      idCartItem,
      quantity: quantity || 1,
      scentedHomes: [],
      scentedDualCrayons: [],
      scentedCandles: [],
      scentedReedDiffUser: [],
      scentedCarDiffUser: [],
      scentedMiniCandle: [],
      scentedOilBurner: [],
      bestSeller: [],
      dataReview: {},
      isShowSelectScent: false,
      dataScentSelection: {},
      type: props.type,
      indexSelect: 1,
      isChangeType: false,
    };
    this.refCustome = React.createRef();
    this.waxPerfume = {};
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { propsState, basket, type } = nextProps;
    if (propsState && JSON.stringify(propsState) !== JSON.stringify(prevState.propsState)) {
      const {
        name, imageCustome, isCustome, font, color, idCartItem, quantity,
      } = getDataCustome(
        propsState,
      );
      const isShowCustom = isCustome;
      const nameBottle = name;
      const isCroped = true;

      _.assign(objectReturn, {
        font,
        color,
        propsState,
        isShowCustom,
        nameBottle,
        isCroped,
        currentImg: imageCustome,
        idCartItem,
        quantity: quantity || 1,
      });
      _.assign(objectReturn, { propsState });
    }

    // close pick scents when change type
    if (type !== prevState.type) {
      _.assign(objectReturn, { type, isShowSelectScent: false, isChangeType: true });
    }

    const {
      scents, scentsWax, scentsCandle, scentsHome, scentsDualCrayon,
      scentsDiy, scentNotes, scentsMiniCandle, scentsOilBurner, scentsReedDiffuser,
    } = nextProps;
    if (scents !== prevState.scents && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scents?.length > 0) {
      const dataScentsPerfume = preDataScents(addScentNoteIntoScents(scents, scentNotes));
      _.assign(objectReturn, { scents, dataScentsPerfume });
    }
    if (scentsDiy !== prevState.scentsDiy && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsDiy?.length > 0) {
      const dataScentsDiyPerfume = preDataScents(addScentNoteIntoScents(scentsDiy, scentNotes));
      _.assign(objectReturn, { scentsDiy, dataScentsDiyPerfume });
    }
    if (scentsWax !== prevState.dataScentsWax && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsWax?.length > 0) {
      const dataScentsWax = preDataScents(addScentNoteIntoScents(scentsWax, scentNotes));
      _.assign(objectReturn, { scentsWax, dataScentsWax });
    }
    if (scentsCandle !== prevState.scentsCandle && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsCandle?.length > 0) {
      const dataScentsCandle = preDataScents(addScentNoteIntoScents(scentsCandle, scentNotes));
      _.assign(objectReturn, { scentsCandle, dataScentsCandle });
    }
    if (scentsMiniCandle !== prevState.scentsMiniCandle && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsMiniCandle?.length > 0) {
      const dataScentsMiniCandle = preDataScents(addScentNoteIntoScents(scentsMiniCandle, scentNotes));
      _.assign(objectReturn, { scentsMiniCandle, dataScentsMiniCandle });
    }
    if (scentsHome !== prevState.scentsHome && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsHome?.length > 0) {
      const dataScentsHome = preDataScents(addScentNoteIntoScents(scentsHome, scentNotes));
      _.assign(objectReturn, { scentsHome, dataScentsHome });
    }
    if (scentsDualCrayon !== prevState.scentsDualCrayon && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsDualCrayon?.length > 0) {
      const dataScentsDualCrayon = preDataScents(addScentNoteIntoScents(scentsDualCrayon, scentNotes));
      _.assign(objectReturn, { scentsDualCrayon, dataScentsDualCrayon });
    }
    if (scentsOilBurner !== prevState.scentsOilBurner && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsOilBurner?.length > 0) {
      const dataScentsOilBurner = preDataScents(addScentNoteIntoScents(scentsOilBurner, scentNotes));
      _.assign(objectReturn, { scentsDualCrayon, dataScentsOilBurner });
    }

    if (scentsReedDiffuser !== prevState.scentsReedDiffuser && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsReedDiffuser?.length > 0) {
      const dataScentsReedDiffuser = preDataScents(addScentNoteIntoScents(scentsReedDiffuser, scentNotes));
      _.assign(objectReturn, { scentsDualCrayon, dataScentsReedDiffuser });
    }

    return !_.isEmpty(objectReturn) ? objectReturn : {};
  }

    scrollToSample = () => {
      const element = document.getElementById('idSample');
      if (element) {
        element.scrollIntoView({ behavior: 'smooth' });
      }
    };

    addToCartItem = (select) => {
      const index = _.findIndex(this.props.data.items, x => x.id === select.id);
      const data = _.cloneDeep(this.props.data);
      if (index > -1) {
        const d = data.items[index];
        data.name = `${d.name} ${d.variant_values && d.variant_values.length > 0 ? `(${d.variant_values[0]})` : ''}`;
      }
      this.handleSubmitAddToCart(data, index);
    }

    updateWaxPerfume = (data) => {
      this.waxPerfume = data;
      this.generateData(this.props.data);
    }

    onSaveCustomer = (data) => {
      const {
        image, name, color, font,
      } = data;
      if (_.isEmpty(image)) {
        this.setState({
          currentImg: undefined,
          nameBottle: name,
          font,
          color,
        });
        return;
      }
      this.setState({
        currentImg: image,
        nameBottle: name,
        font,
        color,
      });
    }

    setDataReview = (data) => {
      this.setState({ dataReview: data });
    }

    fetchScentedHomes = () => {
      const option = {
        url: GET_SCENTED_HOME,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        console.log('fetchScentedHomes', response);
        this.setState({ scentedHomes: response });
      });
    }

    fetchScentedDualCrayons = () => {
      const option = {
        url: GET_SCENTED_DUAL_CRAYONS,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedDualCrayons: response });
      });
    }

    fetchScentedCanldes = () => {
      const option = {
        url: GET_SCENTED_CANDLES,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedCandles: response });
      });
    }

    fetchBestSeller = () => {
      const option = {
        url: GET_BEST_SELLER_PERFUME,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ bestSeller: response });
      });
    }

    fetchScentedReedDiffUser = () => {
      const option = {
        url: GET_SCENTED_REED_DIFFUSER,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedReedDiffUser: response });
      });
    }

    fetchScentedCarDiffUser = () => {
      const option = {
        url: GET_SCENTED_CAR_DIFFUSER,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedCarDiffUser: response });
      });
    }

    fetchScentedMiniCandle = () => {
      const option = {
        url: GET_SCENTED_MINI_CANDLES,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedMiniCandle: response });
      });
    }

    fetchScentedOilBurner = () => {
      const option = {
        url: GET_SCENTED_OIL_BURNER,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedOilBurner: response });
      });
    }

    removeCustomization = () => {
      this.setState({
        currentImg: undefined,
        nameBottle: undefined,
        font: FONT_CUSTOME.JOST,
        color: COLOR_CUSTOME.BLACK,
      });
    }

    prepareDataItemCart = async (dataSelect, index = 0) => {
      const { pathname, search } = window.location;
      const {
        basket,
        type,
      } = this.props;
      const urlPro = pathname !== '/products-questionnaire' ? `${pathname}${search || ''}` : undefined;
      const { items, combo } = dataSelect;
      const itemCombo = items && items.length > index ? items[index] : undefined;
      const itemBottle = _.find(combo || [], x => x.product.type === 'Bottle');
      const typePro = type ? (type === 'elixir' ? 'unisex' : null) : null;
      const imageBottle = itemBottle
        ? _.find(itemBottle.product.images, x => x.type === typePro)
        : undefined;
      const bottleImage = this.state.currentImg
        ? await fetch(this.state.currentImg).then(r => r.blob())
        : undefined;
      if (itemCombo) {
        console.log('itemCombo==', itemCombo);
        const { price, id, is_featured: isFeatured } = itemCombo;
        const data = {
          id: this.state.idCartItem,
          is_featured: isFeatured,
          item: id,
          font: this.state.font,
          color: this.state.color,
          is_display_name: (!bottleImage && !!this.state.nameBottle) || !!this.state.nameBottle,
          is_customized: !!bottleImage,
          name: this.state.nameBottle ? this.state.nameBottle : dataSelect.name,
          price,
          quantity: this.state.quantity,
          total: bottleImage
            ? parseFloat(price) * this.state.quantity + this.itemBottle.price
            : parseFloat(price) * this.state.quantity,
          image_urls: imageBottle ? [imageBottle.image] : undefined,
          file: bottleImage
            ? new File([bottleImage], 'product.png')
            : undefined,
          imagePremade: _.find(this.state.arrayThumbs, x => x.image === this.state.currentImg) ? _.find(this.state.arrayThumbs, x => x.image === this.state.currentImg).id : undefined,
          meta: {
            url: urlPro,
          },
        };
        const dataTemp = {
          idCart: basket.id,
          item: data,
        };
        return dataTemp;
      }
    };

    handleSubmitAddToCart = async (dataSelect, index = 0) => {
      const {
        createBasketGuest,
        addProductBasket,
        basket,
      } = this.props;
      const { items } = dataSelect;
      if (_.isEmpty(items[0]) || !items[0].id) {
        toastrError('Please create a product');
        return;
      }
      const dataTemp = await this.prepareDataItemCart(dataSelect, index);
      if (!basket.id) {
        createBasketGuest(dataTemp);
      } else {
        addProductBasket(dataTemp);
      }
    };

    handleUpdateCart = async (dataSelect, index = 0) => {
      const {
        updateProductBasket,
      } = this.props;
      const dataTemp = await this.prepareDataItemCart(dataSelect, index);
      if (this.state.idCartItem) {
        _.assign(dataTemp, { isUpdateFromProductDetail: true });
        updateProductBasket(dataTemp);
      }
    }

    fetchScentItem = (type) => {
      if (type === 'Perfume' || type === 'bundle_creation' || type === 'car_diffuser') {
        this.fetchAllScent();
      } else if (type === 'perfume_diy') {
        this.fetchAllScentDiy();
      } else if (type === 'Wax_Perfume') {
        this.fetchAllScentWax();
      } else if (['single_candle', 'dual_candles'].includes(type)) {
        this.fetchAllScentCandle();
      } else if (type === 'mini_candles') {
        this.fetchAllScentMiniCandle();
      } else if (type === 'home_scents') {
        this.fetchAllScentHome();
      } else if (type === 'dual_crayons') {
        this.fetchAllDualCrayonScent();
      } else if (type === 'oil_burner') {
        this.fetchAllOilBurnerScent();
      } else if (type === 'reed_diffuser') {
        this.fetchAllReedDiffuserScent();
      }
    }

    componentDidMount = () => {
      const { data } = this.props;
      this.generateData(data);
      this.handleItemDelete();

      let dataScentsPerfume = [];
      let dataScentsWax = [];
      let dataScentsCandle = [];
      let dataScentsMiniCandle = [];
      let dataScentsHome = [];
      let dataScentsDualCrayon = [];
      let dataScentsDiyPerfume = [];
      let dataScentsOilBurner = [];
      let dataScentsReedDiffuser = [];
      const { type } = data;
      this.fetchScentItem(type.name);

      const {
        scents, scentsWax, scentsCandle, scentsHome, scentsDualCrayon,
        scentsDiy, scentNotes, scentsMiniCandle, scentsOilBurner, scentsReedDiffuser,
      } = this.props;
      dataScentsPerfume = preDataScents(addScentNoteIntoScents(scents, scentNotes));
      dataScentsWax = preDataScents(addScentNoteIntoScents(scentsWax, scentNotes));
      dataScentsCandle = preDataScents(addScentNoteIntoScents(scentsCandle, scentNotes));
      dataScentsMiniCandle = preDataScents(addScentNoteIntoScents(scentsMiniCandle, scentNotes));
      dataScentsHome = preDataScents(addScentNoteIntoScents(scentsHome, scentNotes));
      dataScentsDualCrayon = preDataScents(addScentNoteIntoScents(scentsDualCrayon, scentNotes));
      dataScentsDiyPerfume = preDataScents(addScentNoteIntoScents(scentsDiy, scentNotes));
      dataScentsOilBurner = preDataScents(addScentNoteIntoScents(scentsDiy, scentsOilBurner));
      dataScentsReedDiffuser = preDataScents(addScentNoteIntoScents(scentsDiy, scentsReedDiffuser));
      this.setState({
        dataScentsPerfume,
        dataScentsWax,
        dataScentsCandle,
        dataScentsHome,
        dataScentsDualCrayon,
        dataScentsDiyPerfume,
        dataScentsMiniCandle,
        dataScentsOilBurner,
        dataScentsReedDiffuser,
      });
    };

    handleItemDelete = () => {
      if (this.props.basket && this.state.idCartItem) {
        if (!_.find(this.props.basket.items, x => x.id === this.state.idCartItem)) {
          this.props.propsState.custome.idCartItem = undefined;
          this.setState({ idCartItem: undefined, isShowCustom: false });
        }
      }
    }

    componentDidUpdate(prevProps, prevState) {
      console.log('===product Detail====');
      const { data, type } = this.props;
      const { isChangeType } = this.state;
      if (JSON.stringify(data) !== JSON.stringify(prevProps.data)) {
        this.generateData(data);
        console.log('===product Detail==== 1');
      }
      if (isChangeType) {
        this.state.isChangeType = false;
        this.fetchScentItem(type);
        console.log('===product Detail==== 2');
      }
    }

    onChangeQuantity = (e) => {
      const { value } = e.target;
      const quantity = parseInt(value, 10);
      this.setState({ quantity });
    };

    onClickAddtoCart = async () => {
      const { data } = this.props;
      this.handleSubmitAddToCart(data);
    };

    onClickUpdatetoCart = async () => {
      const { data } = this.props;
      this.handleUpdateCart(data);
    }

    onCloseKnowMore = () => {
      this.setState({ isShowKnowMore: false });
    };

    generateData = (data) => {
      this.thumbs = [];
      this.images = [];
      const {
        combo, name, type, items, displayName,
      } = data;
      const { videos } = this.props.dataFAQ;

      const imageType = items[0]?.is_best_seller ? _.find(items[0]?.images, x => x.type === 'main')?.image : undefined;
      if (imageType) {
        this.images.push({
          original: imageType,
          thumbnail: imageType,
        });
      }

      const bottles = _.find(combo, x => x.product.type === 'Bottle');
      const products = _.filter(combo, x => x.product.type !== 'Bottle');

      // add bottome image when product do NOT add name scent on bottle
      const creationScentInital = products.length === 0 || ['Wax_Perfume', 'dual_crayons'].includes(type.name);

      // add bottle custom
      if (type.name === 'bundle_creation') {
        // this.images.push({
        //   original: bottleBundleCreation,
        //   thumbnail: bottleBundleCreation,
        //   isBundleCreation: true,
        // });
      }
      if (type.name === 'Perfume' || type.name === 'bundle_creation' || type.name === 'perfume_diy') {
        if (items[0].isSample) {
          // this.images.push({
          //   original: bottleRollon,
          //   thumbnail: bottleRollon,
          //   isSample: true,
          // });
        } else {
          this.images.push({
            dataCustomBottle: {},
          });
        }
      } else if (type.name === 'dual_crayons') {
        // this.images.push({
        //   original: bottleDualCaryon,
        //   thumbnail: bottleDualCaryon,
        //   isDualCrayons: true,
        // });
      } else if (type.name === 'dual_candles') {
        // this.images.push({
        //   original: candle3,
        //   thumbnail: candle3,
        //   isDualCandles: true,
        // });
      } else if (type.name === 'single_candle') {
        // this.images.push({
        //   original: singleCandle,
        //   thumbnail: singleCandle,
        //   isDualCandles: true,
        // });
      } else if (type.name === 'Wax_Perfume') {
        // this.images.push({
        //   original: solidBottle,
        //   thumbnail: solidBottle,
        //   isWaxPerfume: true,
        // }, {
        //   original: waxImage,
        //   thumbnail: waxImage,
        // });
      } else if (type.name === 'home_scents') {
        this.images.push({
          dataCustomBottle: {},
          isHomeScent: true,
        });
      }
      // else if (type.name === 'reed_diffuser') {
      //   this.images.push({
      //     original: diffUser,
      //     thumbnail: diffUser,
      //     isReedDiffuser: true,
      //   });
      // }
      this.nameProduct = this.props.datas.length > 1 ? data.shortTitle : (displayName || name);
      const imageProduct = bottles && bottles.product.images
        ? _.filter(bottles.product.images, (x) => {
          if (x) {
            if (x.type === null) {
              return true;
            } if (x.type === 'suggestion') {
              this.thumbs.push(x);
            }
          }
          return false;
        })
        : [];
      this.setState({
        arrayThumbs: this.thumbs,
      });
      // if (['gift_bundle', 'Perfume', 'Elixir', 'dual_candles', 'single_candle', 'home_scents', 'dual_crayons', 'bundle_creation', 'reed_diffuser', 'perfume_diy'].includes(type.name)) {
      //   _.forEach(products, (d) => {
      //     const i = _.filter(d.product.images, x => x.type === null);
      //     imageProduct = imageProduct.concat(i);
      //   });
      // }

      // move image hardcode to bottom
      const imageFirst = this.images?.length ? this.images[0] : undefined;
      if (creationScentInital) {
        this.images = [];
      }
      _.forEach(imageProduct, (x) => {
        const i = {
          original: x.image,
          thumbnail: x.image,
        };
        this.images.push(i);
      });
      if (creationScentInital && imageFirst) {
        this.images.push(imageFirst);
      }

      _.forEach(videos, (x) => {
        const i = {
          original: x.placeholder,
          thumbnail: x.placeholder,
          embedUrl: x.video,
        };
        this.images.push(i);
      });


      // if wax only display in items image
      if (!_.isEmpty(this.waxPerfume)) {
        const { images } = this.waxPerfume;
        _.forEach(_.filter(images || [], d => d.type === null), (x) => {
          const i = {
            original: x.image,
            thumbnail: x.image,
          };
          this.images.push(i);
        });
      }
      this.forceUpdate();
    };

    isActive = (shortTitle) => {
      const { data } = this.props;
      return shortTitle === data.shortTitle;
    };

    handleActiveImgOnBottle = (img) => {
      // this.refCustome.current.handleActiveImgOnBottle(img);
      this.setState({ currentImg: img });
    };

    handleChangeNameBottle = (text) => {
      this.refCustome.current.setNameBottle(text);
      this.setState({ nameBottle: text });
    }

    handleCropImage = (img) => {
      this.setState({ currentImg: img });
    }

    toggleCustom = () => {
      const { isShowCustom } = this.state;
      if (!isShowCustom) {
        this.handleItemDelete();
      }
      this.setState({ isShowCustom: !isShowCustom });
    }

    addRemoveClass = (addClass, removeClass) => {
      const element = document.getElementById('div-ingredient-detail');
      if (element && removeClass) {
        element.classList.remove(removeClass);
      }
      const arr = element.className.split(' ');
      if (arr.indexOf(addClass) === -1) {
        element.className += ` ${addClass}`;
      }
    }

    onClickGotoProduct = (product) => {
      const url = onClickProduct(product);
      if (url) {
        this.props.history.push(generateUrlWeb(url));
      }
    }

    onClickChooseScent = (data, index, isOnlyShowIngredientLocal = false) => {
      scrollToTargetAdjusted('anchor-select-scent');
      const { buttonBlocks } = this.props;
      const key = index === 1 ? '1ST INGREDIENT' : index === 2 ? '2ND INGREDIENT' : index === 3 ? '3RD INGREDIENT' : '4TH INGREDIENT';
      const dataScentSelection = {
        subTitle: getNameFromButtonBlock(buttonBlocks, key),
        index,
      };
      if (data) {
        _.assign(dataScentSelection, data.product);
      }
      this.setState({
        dataScentSelection,
        isShowSelectScent: true,
        indexSelect: index,
        isOnlyShowIngredientLocal,
      });
    }

    fetchAllScentWax = () => {
      const { scentsWax } = this.props;
      if (scentsWax && scentsWax.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_WAX,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsWaxToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllDualCrayonScent = () => {
      const { scentsDualCrayon } = this.props;
      if (scentsDualCrayon && scentsDualCrayon.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_DUAL_CRAYONS,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsDualCrayonsToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllOilBurnerScent = () => {
      const { scentsOilBurner } = this.props;
      if (scentsOilBurner && scentsOilBurner.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_OIL_BURNER,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsOilBurnerToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllReedDiffuserScent = () => {
      const { scentsReedDiffuser } = this.props;
      if (scentsReedDiffuser && scentsReedDiffuser.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_REED_DIFFUSER,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsReedDifffUserToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllScent = () => {
      const { scents } = this.props;
      if (scents && scents.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_PERFUME,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllScentDiy = () => {
      const { scentsDiy } = this.props;
      if (scentsDiy && scentsDiy.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_PERFUME_DIY,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsDiyToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllScentHome = () => {
      const { scentsHome } = this.props;
      if (scentsHome && scentsHome.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_HOME_SCENTS,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsHomeToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllScentCandle = () => {
      const { scentsCandle } = this.props;
      if (scentsCandle && scentsCandle.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_CANDLE,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsCandleToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllScentMiniCandle = () => {
      const { scentsMiniCandle } = this.props;
      if (scentsMiniCandle && scentsMiniCandle.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_MINI_CANDLE,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsMiniCandleToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    getProduct = async (scents, type) => {
      let url;
      if (type === 'mini_candles') {
        url = `${GET_PRODUCT_FOR_CART}?combo=${scents[0].id},${scents[1].id},${scents[2].id},${scents[3].id}&type=${type}`;
      } else {
        url = `${GET_PRODUCT_FOR_CART}?combo=${scents[0].id}${scents.length > 1 ? `,${scents[1].id}` : ''}&type=${type}`;
      }
      const option = {
        url,
        method: 'GET',
      };
      return fetchClient(option);
    }

    getProductCart = async (scents, isShowRollOn, isSolidPerfume, isDualCandles, isShowHome, isDualCrayons, isBundleCreation, isReedDiffuser, isPerfumeDiy, isCarDiffuser, isMiniCandle, isOilBurner) => {
      try {
        const type = isOilBurner ? 'oil_burner' : isMiniCandle ? 'mini_candles' : isCarDiffuser ? 'car_diffuser' : isPerfumeDiy ? 'perfume_diy' : isReedDiffuser ? 'reed_diffuser' : isBundleCreation ? 'bundle_creation' : isDualCrayons ? 'dual_crayons' : isDualCandles ? (scents.length > 1 ? 'dual_candles' : 'single_candle') : isSolidPerfume ? 'wax_perfume' : isShowHome ? 'home_scents' : (scents.length > 1 ? 'perfume' : isShowRollOn ? 'scent' : 'perfume');
        const product = await this.getProduct(scents, type);
        return product;
      } catch (error) {
        console.error(error.mesage);
      }
    }

    onClickGotoPerfumeProductFromCrayons = async (combo) => {
      const scents = _.map(combo, d => (
        {
          id: d?.product?.combo[0]?.product?.id,
        }
      ));
      if (scents.length === 0) {
        this.props.history.push(generateUrlWeb('/productv2/creation_perfume'));
      } else {
        const product = await this.getProductCart(scents, false, false, false, false);
        this.props.history.push(generateUrlWeb(`/productv2/Perfume/${product.id}`));
      }
    }

    onClickApply = async (data, index) => {
      console.log('objectdata, index', data, index);
      const scents = [];
      if (!data) {
        if (this.combosState && this.combosState.length > 0) {
          scents.push(this.combosState[0].product);
        }
      } else if (index === 1) {
        scents.push(data);
        if (this.combosState && this.combosState.length > 1) {
          scents.push(this.combosState[1].product);
        }
      } else if (this.combosState && this.combosState.length > 0) {
        scents.push(this.combosState[0].product);
        scents.push(data);
      } else {
        scents.push(data);
      }

      auth.setIsAddScent(true);

      const { type } = this.props.data;
      if (type.name === 'Wax_Perfume') {
        if (scents.length === 0) {
          this.props.history.push(generateUrlWeb('/productv2/wax'));
        } else {
          const product = await this.getProductCart(scents, false, true, false, false);
          this.props.history.push(generateUrlWeb(`/productv2/wax/${product.id}`));
        }
        this.setState({ isShowSelectScent: false });
      } else if (type.name === 'home_scents') {
        if (scents.length === 0) {
          this.props.history.push(generateUrlWeb('/productv2/home_scents'));
        } else {
          const product = await this.getProductCart(scents, false, false, false, true);
          this.props.history.push(generateUrlWeb(`/productv2/home_scents/${product.id}`));
        }
        this.setState({ isShowSelectScent: false });
      } else if (['single_candle', 'dual_candles'].includes(type.name)) {
        const formatScents = _.map(scents, d => (d.combo ? d.combo[0].product : d));
        if (formatScents.length === 0) {
          this.props.history.push(generateUrlWeb(window.localStorage.pathOldProduct || '/productv2/dual_candles'));
        } else {
          const product = await this.getProductCart(formatScents, false, false, true, false);
          if (window.location.pathname === generateUrlWeb('/productv2/dual-candles')
          || window.location.pathname === generateUrlWeb('/productv2/dual_candles')) {
            window.localStorage.pathOldProduct = '/productv2/dual_candles';
          } else if (window.location.pathname === generateUrlWeb('/productv2/single-candle')
          || window.location.pathname === generateUrlWeb('/productv2/single_candle')) {
            window.localStorage.pathOldProduct = '/productv2/single-candle';
          }
          this.props.history.push(generateUrlWeb(formatScents.length > 1 ? `/productv2/dual_candles/${product.id}` : `/productv2/single-candle/${product.id}`));
        }
        this.setState({ isShowSelectScent: false });
      } else if (type.name === 'Perfume') {
        const isShowRollOn = window.location.pathname.includes('Scent') || window.location.pathname.includes('miniature_perfume');
        if (scents.length === 0) {
          this.setState({ isShowSelectScent: false });
          this.props.history.push(generateUrlWeb(isShowRollOn ? '/productv2/miniature_perfume' : '/productv2/creation_perfume'));
          return;
        }
        const product = await this.getProductCart(scents, false, false, false, false);
        const custome = isShowRollOn ? {} : {
          image: this.state.currentImg,
          name: this.state.nameBottle,
          font: this.state.font,
          color: this.state.color,
          quantity: this.state.quantity,
        };
        this.props.history.push(generateUrlWeb(isShowRollOn ? `/productv2/Scent/${product.id}` : `/productv2/Perfume/${product.id}`), { custome });
        this.setState({ isShowSelectScent: false });
      } else if (type.name === 'perfume_diy') {
        // const formatScents = _.map(scents, d => (d.combo ? _.find(d.combo, x => x.product.type === 'Scent')?.product : d));
        if (scents.length === 0) {
          this.setState({ isShowSelectScent: false });
          this.props.history.push(generateUrlWeb('/productv2/perfume_diy'));
          return;
        }
        const product = await this.getProductCart(scents, false, false, false, false, false, false, false, true);
        const custome = {
          image: this.state.currentImg,
          name: this.state.nameBottle,
          font: this.state.font,
          color: this.state.color,
          quantity: this.state.quantity,
        };
        this.props.history.push(generateUrlWeb(`/productv2/perfume_diy/${product.id}`), { custome });
        this.setState({ isShowSelectScent: false });
      } else if (type.name === 'car_diffuser') {
        // const formatScents = _.map(scents, d => (d.combo ? _.find(d.combo, x => x.product.type === 'Scent')?.product : d));
        if (scents.length === 0) {
          this.setState({ isShowSelectScent: false });
          this.props.history.push(generateUrlWeb('/productv2/car_diffuser'));
          return;
        }
        const product = await this.getProductCart(scents, false, false, false, false, false, false, false, false, true);
        this.props.history.push(generateUrlWeb(`/productv2/car_diffuser/${product.id}`));
        this.setState({ isShowSelectScent: false });
      } else if (type.name === 'mini_candles') {
        const itemsCombo = _.filter(this.props.data.combo, x => x.product.type !== 'Bottle');
        if (data) {
          const newItem = {
            is_low_stock: data.is_low_stock,
            is_out_of_stock: data.is_out_of_stock,
            name: data.name,
            product: {
              id: data.id,
              type: 'mini_candle',
              images: [{
                image: data.image,
                type: 'unisex',
              }],
            },
          };
          if (itemsCombo.length < index) {
            this.props.data.combo.push(newItem);
          } else {
            const indexEle = _.findIndex(this.props.data.combo, d => d.product?.id === itemsCombo[index - 1].product?.id);
            this.props.data.combo[indexEle] = newItem;
          }
        } else {
          const indexEle = _.findIndex(this.props.data.combo, d => d.product?.id === itemsCombo[index - 1].product?.id);
          this.props.data.combo.splice(indexEle, 1);
        }
        const itemsComboNew = _.filter(this.props.data.combo, x => x.product.type !== 'Bottle');
        if (itemsComboNew.length === 4) {
          const product = await this.getProductCart(_.map(itemsComboNew, d => d.product), false, false, false, false, false, false, false, false, false, true);
          this.props.history.push(generateUrlWeb(`/productv2/mini_candles/${product.id}`));
        }
        this.setState({ isShowSelectScent: false });
      } else if (type.name === 'dual_crayons') {
        const formatScents = _.map(scents, d => (d.combo ? d.combo[0].product : d));
        if (formatScents.length === 0) {
          this.props.history.push(generateUrlWeb('/productv2/dual_crayons'));
        } else {
          const product = await this.getProductCart(formatScents, false, false, false, false, true);
          this.props.history.push(generateUrlWeb(`/productv2/dual_crayons/${product.id}`));
        }
        this.setState({ isShowSelectScent: false });
      } else if (type.name === 'bundle_creation') {
        const formatScents = _.map(scents, d => (d.combo ? d.combo[0].product : d));
        if (formatScents.length === 0) {
          this.props.history.push(generateUrlWeb('/productv2/bundle_creation'));
        } else {
          const product = await this.getProductCart(formatScents, false, false, false, false, false, true);
          this.props.history.push(generateUrlWeb(`/productv2/bundle_creation/${product.id}`));
        }
        this.setState({ isShowSelectScent: false });
      } else if (type.name === 'reed_diffuser') {
        const formatScents = _.map(scents, d => (d.combo ? d.combo[0].product : d));
        if (formatScents.length === 0) {
          this.props.history.push(generateUrlWeb('/productv2/reed_diffuser'));
        } else {
          const product = await this.getProductCart(formatScents, false, false, false, false, false, false, true);
          this.props.history.push(generateUrlWeb(`/productv2/reed_diffuser/${product.id}`));
        }
        this.setState({ isShowSelectScent: false });
      } else if (type.name === 'oil_burner') {
        const formatScents = _.map(scents, d => (d.combo ? d.combo[0].product : d));
        if (formatScents.length === 0) {
          this.props.history.push(generateUrlWeb('/productv2/oil_burner'));
        } else {
          const product = await this.getProductCart(formatScents, false, false, false, false, false, false, false, false, false, false, true);
          this.props.history.push(generateUrlWeb(`/productv2/oil_burner/${product.id}`));
        }
        this.setState({ isShowSelectScent: false });
      }
      // this.props.loadingPage(false);
    }

    updateScentSelect = (data) => {
      this.combosState = data;
    }

    onClickClose = () => {
      this.setState({ isShowSelectScent: false });
    }

    onClickViewFullImage = () => {
      this.setState({ isShowViewFullImage: false });
    }

    onChangeTypePerfume = (data) => {
      _.assign(this.state, {
        currentImg: undefined,
        nameBottle: undefined,
        font: FONT_CUSTOME.JOST,
        color: COLOR_CUSTOME.BLACK,
      });
      this.props.onChangeTypePerfume(data);
    }

    onClickWaxPerfume = (item) => {
      const { data } = this.props;
      this.props.history.push(generateUrlWeb(`/productv2/hand_sanitizer/${data.id}/${item.id}`));
    }

    onClickImage = (e) => {
      const { localName } = e.target;
      if (localName === 'img') {
        this.setState({ isShowViewFullImage: true });
      }
    }

    onClickrecommend = (data, type) => {
      const scents = type?.name === 'reed_diffuser' ? this.props.scentsReedDiffuser : type?.name === 'perfume_diy' ? this.props.scentsDiy : ['Perfume', 'car_diffuser'].includes(type?.name) ? this.props.scents : this.props.scentsDualCrayon;
      const datascent = _.find(scents, x => x.id === data.id);
      if (datascent) {
        this.onClickChooseScent({ product: datascent }, 2, true);
      }
    }

    getProductIdRecommended = (type, combosChooseScent) => {
      if (type === 'dual_crayons') {
        const scent = _.find(combosChooseScent?.product?.combo, x => x.product.type === 'Scent');
        return scent?.product?.id;
      }
      return combosChooseScent?.product?.id;
    }

    gotoLink = (link) => {
      if (!link) {
        return;
      }
      if (link.includes('https://')) {
        window.location.href = link;
        return;
      }
      this.props.history.push(generateUrlWeb(link));
    }

    render() {
      const cmsCommon = getCmsCommon(this.props.cms);
      const {
        quantity, scentedHomes, scentedCandles,
        scentedDualCrayons, bestSeller, scentedReedDiffUser,
        scentedCarDiffUser, scentedMiniCandle, scentedOilBurner,
        isShowKnowMore,
        dataPopup,
        isShowCustom,
        dataScentSelection,
        dataScentsPerfume, dataScentsWax, dataScentsCandle, dataScentsHome,
        isShowSelectScent, dataReview, isShowViewFullImage, dataScentsDualCrayon,
        dataScentsDiyPerfume, dataScentsMiniCandle, dataScentsOilBurner, dataScentsReedDiffuser,
      } = this.state;
      const {
        data, datas, recommendCms, itemHandlerSanitizer, isBestSeller,
      } = this.props;

      const cmsTextBlocks = _.filter(recommendCms, x => x.type === 'text_block');
      const {
        items, combo, profile, type,
      } = data;
      const combosChooseScent = _.filter(combo, x => _.isEmpty(x) || x.product.type !== 'Bottle');
      const combos = _.filter(combo, x => !_.isEmpty(x) && x.product.type !== 'Bottle');
      this.itemBottle = _.find(combo, x => !_.isEmpty(x) && x.product.type === 'Bottle');
      const itemCombo = items && items.length > 0 ? items[0] : undefined;
      const isWaxPerfume = data.type.name === 'Wax_Perfume' || data.type.name === 'hand_sanitizer' || data.type.name === 'home_scents_premade';
      const isHandSanitizer = data.type.name === 'hand_sanitizer' || data.type.name === 'discovery_box' || data.type.name === 'mask_sanitizer' || data.type.name === 'toilet_dropper' || data.type.name === 'holder' || data.type.name === 'home_scents_premade';
      const isCuratedCandle = data.type.name === 'dual_candles' && data.is_featured;

      if (isWaxPerfume && _.isEmpty(this.waxPerfume)) {
        this.waxPerfume = !_.isEmpty(itemHandlerSanitizer) ? itemHandlerSanitizer : (_.find(items, x => !x.is_sample) ? _.find(items, x => !x.is_sample) : items[0]);
      } else {
        this.waxPerfume = {};
      }
      const preMakeBt = getNameFromButtonBlock(this.props.buttonBlocks, 'Pre_Made_Home_Scent');
      const recommendBt = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Duo Candles');
      const recommendCrayons = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Duo Crayons');
      const recommendReedDiffUser = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Reed Diffuser');
      const recommendCarDiffUser = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Car Diffuser');
      const recommendMiniCandle = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Mini Candle');
      const recommendOilBurner = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Oil Burner');
      const bestSellersBt = getNameFromButtonBlock(this.props.buttonBlocks, 'Best Sellers');
      const homeBt = getNameFromButtonBlock(this.props.buttonBlocks, 'Home');
      const createScentBt = getNameFromButtonBlock(this.props.buttonBlocks, 'all products');
      const dataCustom = {
        image: this.state.currentImg,
        nameBottle: this.state.nameBottle,
        font: this.state.font,
        color: this.state.color,
        combos,
      };

      const dataScents = _.cloneDeep(data.type.name === 'reed_diffuser' ? dataScentsReedDiffuser : data.type.name === 'oil_burner' ? dataScentsOilBurner : data.type.name === 'mini_candles' ? dataScentsMiniCandle : data.type.name === 'perfume_diy' ? dataScentsDiyPerfume : data.type.name === 'dual_crayons' ? dataScentsDualCrayon : (data.type.name === 'Perfume' || data.type.name === 'bundle_creation' || data.type.name === 'reed_diffuser' || data.type.name === 'car_diffuser') ? dataScentsPerfume : (data.type.name === 'dual_candles' || data.type.name === 'single_candle') ? dataScentsCandle : data.type.name === 'home_scents' ? dataScentsHome : dataScentsWax);

      // remove scent selected
      if (['reed_diffuser', 'dual_crayons'].includes(data.type.name)) {
        _.forEach(dataScents, (k) => {
          _.remove(k.datas[0]?.datas || [], x => _.find(combos, d => (d.product.combo ? d.product.combo[0].product.id : d.product.id) === x.id));
        });
      }
      const recommendData = ['Perfume', 'dual_crayons', 'reed_diffuser', 'perfume_diy', 'car_diffuser'].includes(data.type.name) && this.props.mmos?.length > 0 && combosChooseScent?.length === 1 ? _.find(this.props.mmos, x => x.id === this.getProductIdRecommended(data.type.name, combosChooseScent[0])) : undefined;

      return (
        <div
          className="div-product-detail-v2 div-col"
          // style={{ height: heightDetail }}
        >
          <CardKnowMore
            onCloseKnowMore={this.onCloseKnowMore}
            isShowKnowMore={isShowKnowMore}
            data={dataPopup}
          />
          <Modal
            className={`modal-view-full-image ${addFontCustom()}`}
            isOpen={isShowViewFullImage}
            toggle={this.onClickViewFullImage}
          >
            <ModalBody>
              <ImageBlock
                images={this.images}
                datas={[]}
                isActive={this.isActive}
                onChangeProduct={this.props.onChangeProduct}
                dataCustom={dataCustom}
                buttonBlocks={this.props.buttonBlocks}
                onClose={this.onClickViewFullImage}
                isPopUp
                isSelectedScent={combos?.length > 0}
                isProductDetailV2
                className={classnames('product-detail-v2', isMobile ? 'modal-custom-mobile' : '')}
              />
            </ModalBody>
          </Modal>
          <div id="id-detail-1" className="show-page-1 div-page1">
            {
              isBrowser && (
                <div className="div-route-link">
                  <button
                    className="button-bg__none"
                    type="button"
                    onClick={() => this.gotoLink(getLinkFromButtonBlock(this.props.buttonBlocks, 'Home'))}
                  >
                    {homeBt}
                  </button>
                  <span>/</span>
                  <button
                    className="button-bg__none"
                    type="button"
                    onClick={() => this.gotoLink(getLinkFromButtonBlock(this.props.buttonBlocks, isBestSeller ? 'Best Sellers' : 'all products'))}
                  >
                    {isBestSeller ? bestSellersBt : createScentBt}
                  </button>
                  <span>/</span>
                  <button className="button-bg__none active" type="button">
                    {(isCuratedCandle ? data.short_description : undefined) || this.nameProduct}
                  </button>
                </div>
              )
            }

            <div className="div-hero">
              <div className={classnames('image-block-v2', 'best-seller')}>
                <ImageBlock
                  images={this.images}
                  datas={datas}
                  isActive={this.isActive}
                  onChangeProduct={this.props.onChangeProduct}
                  dataCustom={dataCustom}
                  buttonBlocks={this.props.buttonBlocks}
                  onClickImage={this.onClickImage}
                  isSelectedScent={combos?.length > 0}
                  className="product-detail-v2 product-detail-v3"
                />
              </div>
              <div className="text-block-v2">
                {
                  isShowSelectScent && (
                    isMobile && (
                      <Modal
                        isOpen={isShowSelectScent}
                        toggle={this.onClickClose}
                        className={`modal-scent-mobile-v2 ${addFontCustom()}`}
                        fade={false}
                      >
                        <SelectScent
                          scentNotes={dataScents}
                          dataSelection={dataScentSelection}
                          buttonBlocks={this.props.buttonBlocks}
                          onClickApply={d => this.onClickApply(d, this.state.indexSelect)}
                          onClickCancel={this.onClickClose}
                          onClickIngredient={this.props.onClickIngredient}
                          isHeaderMobile
                          isNotPreData
                          indexSelect={this.state.indexSelect}
                          isDualCandles={['single_candle', 'dual_candles', 'mini_candles'].includes(data.type.name)}
                          isShowHome={data.type.name === 'Home'}
                          isShowIngredientDetail
                          isOnlyShowIngredientLocal={this.state.isOnlyShowIngredientLocal}
                          scentRecommend={recommendData}
                          isProductDetailV2
                        />
                      </Modal>
                    )
                  )
                }
                <TextBlock
                  isProductDetailV2
                  login={this.props.login}
                  arrayThumbs={this.state.arrayThumbs}
                  nameProduct={this.nameProduct}
                  nameProductCustom={isCuratedCandle ? data.short_description : undefined}
                  descriptionCustom={isCuratedCandle ? data.description : undefined}
                  itemCombo={itemCombo}
                      // priceSample={priceSample}
                  scrollToSample={this.scrollToSample}
                  cmsCommon={cmsCommon}
                  iconsBlock={this.props.iconsBlockHowtoMix}
                  itemBottle={this.itemBottle}
                  onChangeQuantity={this.onChangeQuantity}
                  quantity={quantity}
                  onClickAddtoCart={this.onClickAddtoCart}
                  combos={combos}
                  combosChooseScent={combosChooseScent}
                  toggleCustom={this.toggleCustom}
                  propsState={this.props.propsState}
                  type={data.type || {}}
                  altName={data && data.type ? data.type.alt_name : ''}
                  isWaxPerfume={isWaxPerfume}
                  isHandSanitizer={isHandSanitizer}
                  items={items}
                  addToCartItem={this.addToCartItem}
                  updateWaxPerfume={this.updateWaxPerfume}
                  onClickIngredient={this.props.onClickIngredient}
                  currentImg={this.state.currentImg}
                  nameBottle={this.state.nameBottle}
                  removeCustomization={this.removeCustomization}
                  idCartItem={this.state.idCartItem}
                  dataReview={dataReview}
                  buttonBlocks={this.props.buttonBlocks}
                  onClickChooseScent={this.onClickChooseScent}
                  updateScentSelect={this.updateScentSelect}
                  onClickApply={this.onClickApply}
                  bottlePrice={this.props.bottlePrice}
                  onChangeTypePerfume={this.onChangeTypePerfume}
                  onClickGotoPerfumeProductFromCrayons={this.onClickGotoPerfumeProductFromCrayons}
                  onClickWaxPerfume={this.onClickWaxPerfume}
                  itemHandlerSanitizer={this.props.itemHandlerSanitizer}
                  recommendData={recommendData}
                  onClickrecommend={d => this.onClickrecommend(d, type)}
                  dataFAQ={this.props.dataFAQ}
                />
              </div>
            </div>
            {
              ['Perfume', 'single_candle', 'dual_candles', 'home_scents', 'Wax_Perfume', 'dual_crayons', 'bundle_creation', 'reed_diffuser', 'perfume_diy', 'car_diffuser', 'mini_candles', 'oil_burner'].includes(type.name) && (
                <div className="select-scent">
                  <SelectProductScent
                    arrayThumbs={this.state.arrayThumbs}
                    isShowSelectScent={isShowSelectScent}
                    buttonBlocks={this.props.buttonBlocks}
                    combosChooseScent={combosChooseScent}
                    items={items}
                    combos={combos}
                    itemCombo={itemCombo}
                    dataCustom={dataCustom}
                    toggleCustom={this.toggleCustom}
                    addToCartItem={this.addToCartItem}
                    onClickAddtoCart={this.onClickAddtoCart}
                    quantity={quantity}
                    onChangeQuantity={this.onChangeQuantity}
                    type={data.type || {}}
                    onClickChooseScent={this.onClickChooseScent}
                    onClickClose={this.onClickClose}
                    dataScentSelection={dataScentSelection}
                    dataScents={dataScents}
                    onClickApply={this.onClickApply}
                    updateScentSelect={this.updateScentSelect}
                    loadingPage={this.props.loadingPage}
                    onClickIngredient={this.props.onClickIngredient}
                    typeSelection={this.props.typeSelection}
                    onChangeFilter={this.props.onChangeFilter}
                    onChangeSearch={this.props.onChangeSearch}
                    itemHandlerSanitizer={this.props.itemHandlerSanitizer}
                    // shareRefFilterSearchScent={this.props.shareRefFilterSearchScent}
                    isDualCandles={['single_candle', 'dual_candles', 'mini_candles'].includes(data.type.name)}
                    isShowHome={data.type.name === 'Home'}
                    onClickChangeImageCandle={this.onClickChangeImageCandle}
                    recommendData={recommendData}
                    profile={profile}
                    cmsCommon={cmsCommon}
                    currentImg={this.state.currentImg}
                    nameBottle={this.state.nameBottle}
                    dataFAQ={this.props.dataFAQ}
                    // isOnlyShowIngredientLocal={this.state.isOnlyShowIngredientLocal}
                  />
                </div>
              )
            }

            {/* {
              data.type.name === 'gift_bundle' ? (
                <ScentCharacterMulti cmsCommon={cmsCommon} combos={combos} onClickIngredient={this.props.onClickIngredient} profile={profile} type={type} />
              ) : (!isHandSanitizer && combos && combos.length > 0 && !this.props.isCreateProduct && (itemCombo && itemCombo.id) ? (
                <ScentCharacter cmsCommon={cmsCommon} combos={combos} onClickIngredient={this.props.onClickIngredient} profile={profile} type={type} ingredients={this.props.ingredients} />
              ) : (<div />))
            } */}
            {/* {
              data.type.name === 'dual_crayons' && (
                <DiscoverScent data={this.props.data} buttonBlocks={this.props.buttonBlocks} cmsCommon={cmsCommon} onClickIngredient={this.props.onClickIngredient} headerAndParagraphBocks={this.props.headerAndParagraphBocks} />
              )
            } */}
            {
              data.type.name === 'discovery_box' && (
                <React.Fragment>
                  <BlockVideoV2 buttonBlocks={this.props.buttonBlocks} videoBlocks={this.props.videoBlocks} iconsBlockDiscovery={this.props.iconsBlockDiscovery} />
                  {/* <BlockListVideo buttonBlocks={this.props.buttonBlocks} videoBlocks={this.props.videoBlocks} />
                  <DiscoverScent
                    scentNotes={this.props.scentNotes}
                    buttonBlocks={this.props.buttonBlocks}
                    cmsCommon={cmsCommon}
                    onClickIngredient={this.props.onClickIngredient}
                    headerAndParagraphBocks={this.props.headerAndParagraphBocks}
                    addScentNotesToStore={this.props.addScentNotesToStore}
                  /> */}
                </React.Fragment>
              )
            }
            {
              data.type.name === 'home_scents' && (
                <div className="list-pre-made">
                  <PremadeProduct
                    isProductDetailV2
                    basket={this.props.basket}
                    createBasketGuest={this.props.createBasketGuest}
                    addProductBasket={this.props.addProductBasket}
                    title={preMakeBt}
                    data={scentedHomes}
                    onClickGotoProduct={this.onClickGotoProduct}
                    fetchData={this.fetchScentedHomes}
                    isShowDots
                  />
                </div>
              )
            }
            {
              data.type.name === 'dual_crayons' && (
                <div className="list-pre-made">
                  <PremadeProduct
                    isProductDetailV2
                    basket={this.props.basket}
                    createBasketGuest={this.props.createBasketGuest}
                    addProductBasket={this.props.addProductBasket}
                    title={recommendCrayons}
                    data={scentedDualCrayons}
                    onClickGotoProduct={this.onClickGotoProduct}
                    fetchData={this.fetchScentedDualCrayons}
                    isShowDots
                  />
                </div>
              )
            }
            {
              (data.type.name === 'dual_candles' || (data.type.name === 'single_candle' && combos?.length === 0)) && (
                <div className="list-pre-made ">
                  <PremadeProduct
                    isProductDetailV2
                    basket={this.props.basket}
                    createBasketGuest={this.props.createBasketGuest}
                    addProductBasket={this.props.addProductBasket}
                    title={recommendBt}
                    data={scentedCandles}
                    onClickGotoProduct={this.onClickGotoProduct}
                    fetchData={this.fetchScentedCanldes}
                    isShowDots
                  />
                </div>
              )
            }
            {
              (data.type.name === 'Perfume' && (combos?.length > 1 || combos?.length === 0)) && (
                <div className="list-pre-made">
                  <PremadeProduct
                    isProductDetailV2
                    basket={this.props.basket}
                    createBasketGuest={this.props.createBasketGuest}
                    addProductBasket={this.props.addProductBasket}
                    title={bestSellersBt}
                    data={bestSeller}
                    onClickGotoProduct={this.onClickGotoProduct}
                    fetchData={this.fetchBestSeller}
                    isShowDots
                  />
                </div>
              )
            }
            {
              data.type.name === 'reed_diffuser' && (
                <div className="list-pre-made">
                  <PremadeProduct
                    isProductDetailV2
                    basket={this.props.basket}
                    createBasketGuest={this.props.createBasketGuest}
                    addProductBasket={this.props.addProductBasket}
                    title={recommendReedDiffUser}
                    data={scentedReedDiffUser}
                    onClickGotoProduct={this.onClickGotoProduct}
                    fetchData={this.fetchScentedReedDiffUser}
                    isShowDots
                  />
                </div>
              )
            }
            {
              data.type.name === 'car_diffuser' && (
                <div className="list-pre-made">
                  <PremadeProduct
                    isProductDetailV2
                    basket={this.props.basket}
                    createBasketGuest={this.props.createBasketGuest}
                    addProductBasket={this.props.addProductBasket}
                    title={recommendCarDiffUser}
                    data={scentedCarDiffUser}
                    onClickGotoProduct={this.onClickGotoProduct}
                    fetchData={this.fetchScentedCarDiffUser}
                    isShowDots
                  />
                </div>
              )
            }
            {
              data.type.name === 'mini_candles' && (
                <div className="list-pre-made">
                  <PremadeProduct
                    isProductDetailV2
                    basket={this.props.basket}
                    createBasketGuest={this.props.createBasketGuest}
                    addProductBasket={this.props.addProductBasket}
                    title={recommendMiniCandle}
                    data={scentedMiniCandle}
                    onClickGotoProduct={this.onClickGotoProduct}
                    fetchData={this.fetchScentedMiniCandle}
                    isShowDots
                  />
                </div>
              )
            }
            {
              data.type.name === 'oil_burner' && (
                <div className="list-pre-made">
                  <PremadeProduct
                    isProductDetailV2
                    basket={this.props.basket}
                    createBasketGuest={this.props.createBasketGuest}
                    addProductBasket={this.props.addProductBasket}
                    title={recommendOilBurner}
                    data={scentedOilBurner}
                    onClickGotoProduct={this.onClickGotoProduct}
                    fetchData={this.fetchScentedOilBurner}
                    isShowDots
                  />
                </div>
              )
            }
          </div>
          {
            isShowCustom
              ? (
                <CustomeBottleV4
                  ref={this.refCustome}
                  cmsCommon={cmsCommon}
                  handleCropImage={this.handleCropImage}
                  propsState={this.props.propsState}
                  arrayThumbs={this.state.arrayThumbs}
                  currentImg={this.state.currentImg}
                  nameBottle={this.state.nameBottle}
                  font={this.state.font}
                  color={this.state.color}
                  handleActiveImgOnBottle={this.handleActiveImgOnBottle}
                  closePopUp={() => this.setState({ isShowCustom: false })}
                  onSave={this.onSaveCustomer}
                  itemBottle={this.itemBottle}
                  cmsTextBlocks={cmsTextBlocks}
                  name1={combos && combos.length > 0 ? combos[0].name : ''}
                  name2={combos && combos.length > 1 ? combos[1].name : ''}
                  onClickUpdatetoCart={this.onClickUpdatetoCart}
                  idCartItem={this.state.idCartItem}
                />
              ) : (<div />)
          }
        </div>
      );
    }
}

ProductDetail.propTypes = {
  priceSample: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  data: PropTypes.shape(PropTypes.object).isRequired,
  datas: PropTypes.arrayOf(PropTypes.object).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  basket: PropTypes.arrayOf(PropTypes.object).isRequired,
  onChangeProduct: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  propsState: PropTypes.shape(PropTypes.object).isRequired,
  cms: PropTypes.shape().isRequired,
  recommendCms: PropTypes.arrayOf().isRequired,
  updateProductBasket: PropTypes.func.isRequired,
  ingredients: PropTypes.arrayOf().isRequired,
};

export default ProductDetail;
