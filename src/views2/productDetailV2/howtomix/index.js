import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import {
  Modal, ModalBody,
} from 'reactstrap';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import ClearIcon from '@mui/icons-material/Clear';
import './styles.scss';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';

function HowToMixV2(props) {
  const [isOpen, setIsOpen] = useState(false);
  const gotoSelectedScent = () => {

  };

  const setOpen = () => {
    setIsOpen(true);
  };

  const setClose = () => {
    setIsOpen(false);
  };
  const howtoMixBt = getNameFromButtonBlock(props.buttonBlocks, 'How to mix');
  return (
    <div className="how-to-mix-v2">
      <button type="button" className="button-bg__none" onClick={setOpen}>
        <ErrorOutlineIcon style={{ color: '#2c2c2c' }} />
        <div className="how-text">
          <div className="other-Tagline">
            <b>{howtoMixBt}</b>
          </div>
          <div className="body-text-s-regular">
            {props.iconsBlock?.value?.opacity}
          </div>
        </div>
      </button>
      <Modal className="modal-how-to-mix" isOpen={isOpen} centered>
        <ModalBody>
          <div className="header-modal">
            <div className="left-text">
              <div className="other-Tagline">
                <b>{howtoMixBt}</b>
              </div>
              <div className="body-text-s-regular">
                {props.iconsBlock?.value?.opacity}
              </div>
            </div>
            <button type="button" className="button-bg__none bt-close" onClick={setClose}>
              <ClearIcon />
            </button>
          </div>
          <div className="detail-how-mix">
            {
            _.map(props.iconsBlock?.value?.icons || [], d => (
              <div className="item-how-mix div-row" onClick={gotoSelectedScent}>
                <img src={d.value?.image?.image} alt="icon" />
                <div className="div-col">
                  <span>
                    {d.value?.header?.header_text}
                  </span>
                  <span>
                    {d.value?.text}
                  </span>
                </div>
              </div>
            ))
          }
          </div>
        </ModalBody>
      </Modal>
    </div>
  );
}

export default HowToMixV2;
