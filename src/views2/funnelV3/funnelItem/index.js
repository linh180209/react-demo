import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import ButtonCT from '../../../componentsv2/buttonCT';
import LinkCT from '../../../componentsv2/linkCT';
import { isMobile767fn } from '../../../DetectScreen';
import { getLink } from '../../../Redux/Helpers';
import './styles.scss';

function FunnelV3Item({ data, title, isMobile }) {
  return (
    <LinkCT className='funnel-item' to={getLink(data?.value?.link)} >
      <div
        className='funnel-item-img'
        style={{backgroundImage: `url(${(isMobile || isMobile767fn()) ? data?.value?.mbImage?.image : data?.value?.image?.image})`}}
      >
      </div>
      <div className='funnel-item-content'>
        <div className='div-wrapper'>
          <div className='div-block'>
            <div className='block-icon'>
              <img loading='lazy' src={data?.value?.icon} alt='funnel-icon' />
            </div>
            <div className='block-text'>
              <div className='div-title'>{data?.value?.text}</div>
              <div className='div-description'>{ReactHtmlParser(data?.value?.description)}</div>
            </div>
          </div>

          <ButtonCT name={title} color='secondary' className='div-button' />
        </div>
      </div>
    </LinkCT>
  )
}

export default FunnelV3Item