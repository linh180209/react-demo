import _, { isEmpty } from 'lodash';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import { isMobile480fn, isMobile767fn } from '../../DetectScreen';
import loadingPage from '../../Redux/Actions/loading';
import { fetchCMSHomepage, getNameFromButtonBlock } from '../../Redux/Helpers';
import { toastrError } from '../../Redux/Helpers/notification';
import { useMergeState } from '../../Utils/customHooks';
import useWindowEvent from '../../Utils/useWindowEvent';
import FooterV2 from '../footer';
import FunnelV3Item from './funnelItem';
import './styles.scss';

const getCms = (cms) => {
  if (cms) {
    const { body } = cms;

    const cmsCta = _.filter(body, x => x?.type === 'cta_block');
    const mainTitle = cms.header_text;
    const buttonBlocks = _.filter(body, x => x?.type === 'button_block');
    const iconBlocks = _.find(body, x => x?.type === 'icons_block');
    const imageBlocks = _.filter(body, x => x?.type === 'image_block');

    // map to get icon, then merge into object cta.
    const ctaBlocks = _.map(iconBlocks?.value?.icons, x => {
      const icon = _.find(cmsCta, y => y?.value?.image?.caption === x?.value?.image?.caption);
      const mbImage = _.find(imageBlocks, z => z?.value?.caption === x?.value?.image?.caption);

      if(!isEmpty(icon) || !isEmpty(mbImage)) return ({
        ...icon,
        value: {
          ...icon?.value,
          icon: x?.value?.image?.image,
          mbImage: mbImage?.value,
        }
      });
    });

    return {
      ctaBlocks,
      mainTitle,
      buttonBlocks,
    };
  }
  return {};
};

function FunnelV3() {
  const dispatch = useDispatch();
  const cms = useSelector(state => state.cms);

  const [funnelState, setFunnelState] = useMergeState({
    ctaBlocks: [],
    mainTitle: '',
    buttonBlocks: [],
  });
  const [isMobile, setMobile] = useState(false);

  useEffect(() => {
    handleInitDataGet();
  }, []);

  const onChangeSize = () => {
    if(isMobile767fn()) setMobile(true);
    else setMobile(false);
  }

  useWindowEvent('resize', onChangeSize, window);

  const handleInitDataGet = async () => {
    const cmsBlocks = _.find(cms, x => x.title === 'Funnel V3');
    let cmsData = {};

    if(isEmpty(cmsBlocks)) {
      dispatch(loadingPage(true));

      try {
        const cmsResponse = await fetchCMSHomepage('funnel-v3');
        if(!isEmpty(cmsResponse)) {
          dispatch(loadingPage(false));

          cmsData = getCms(cmsResponse);
        }
      } catch (error) {
        dispatch(loadingPage(false));
        toastrError(error.message);
      }
    }else {
      cmsData = getCms(cmsBlocks);
    }

    setFunnelState(cmsData);
  }

  const startCreationBtn = getNameFromButtonBlock(funnelState.buttonBlocks, 'start_creation_btn');

  return (
    <div className='funnelV3'>
      <HeaderHomePageV3 />

      <div className='funnelV3-content'>
        <div className='funnelV3-title'>
          {funnelState.mainTitle}
        </div>

        <ul className='funnelV3-list'>
          {_.map(funnelState?.ctaBlocks, (x) => (
            <li key={x?.id} className='funnelV3-item'>
              <FunnelV3Item data={x} title={startCreationBtn} isMobile={isMobile} />
            </li>
          ))}
        </ul>
      </div>
      <FooterV2 />
    </div>
  )
}

export default FunnelV3