import React, { useState, useEffect } from 'react';
import ReplayIcon from '@mui/icons-material/Replay';
import { logoFerrariMaison21g } from '../../../imagev2/png';
import './styles.scss';

function HeaderResult(props) {
  return (
    <div className="header-result">
      <div className="content-header">
        <button
          onClick={props.retakeQuiz}
          className="button-bg__none"
          type="button"
        >
          <ReplayIcon style={{ color: '#ffffff' }} />
          Retake Quiz
        </button>
        <img src={logoFerrariMaison21g} alt="logo" />
      </div>
    </div>
  );
}

export default HeaderResult;
