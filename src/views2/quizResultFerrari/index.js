import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { useHistory, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { MetaTags } from 'react-meta-tags';
import { PUT_OUTCOME_URL } from '../../config';
import {
  fetchCMSHomepage, generateHreflang, generateUrlWeb, getNameFromButtonBlock, getSEOFromCms, isValidateValue,
} from '../../Redux/Helpers';
import { scrollToNext } from '../quiz/handler';
import LandingFerrari from '../quizFerrari/identify/landing';
import BlockEmail from './blockEmail';
import BlockPersional from './blockPersional';
import BlockScent from './blockScent';
import loadingPage from '../../Redux/Actions/loading';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import auth from '../../Redux/Helpers/auth';
import FooterResult from './footerResult';
import addCmsRedux from '../../Redux/Actions/cms';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';

export const handleCMSPage = (cmsPage) => {
  const seo = getSEOFromCms(cmsPage);
  const buttonBlocks = _.filter(cmsPage?.body || [], x => x.type === 'button_block');
  const imagetxtBlocks = _.filter(cmsPage?.body, x => x.type === 'imagetxt_block');
  return {
    seo,
    buttonBlocks,
    imagetxtBlocks,
  };
};

function QuizResultFerrari(props) {
  const cms = useSelector(state => state.cms);
  const countries = useSelector(state => state.countries);
  const history = useHistory();
  const { id } = useParams();
  const [data, setData] = useState();
  const [cmsState, setCmsState] = useState({});

  const dispatch = useDispatch();
  const retakeQuiz = () => {
    history.push(generateUrlWeb('/quiz-ferrari'));
  };

  const fetchCMS = async () => {
    const cmsPersonal = _.find(cms, x => x.title === 'Personality Outcome Ferrari');
    if (!cmsPersonal) {
      try {
        const result = await fetchCMSHomepage('personality-outcome-ferrari');
        dispatch(addCmsRedux(result));
        const cmsData = handleCMSPage(result);
        setCmsState(cmsData);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const cmsData = handleCMSPage(cmsPersonal);
      setCmsState(cmsData);
    }
  };

  const fetchData = async () => {
    if (!id) {
      retakeQuiz();
    }
    const option = {
      url: PUT_OUTCOME_URL.replace('{id}', id),
      method: 'GET',
    };
    dispatch(loadingPage(true));
    try {
      const result = await fetchClient(option);
      setData(result);
      console.log('result', result);
    } catch (error) {
      toastrError(error.message);
      retakeQuiz();
    }
    dispatch(loadingPage(false));
  };

  const onClickSend = async (email) => {
    if (!isValidateValue(email)) {
      toastrError('Please enter the email!');
      return;
    }
    const option = {
      url: PUT_OUTCOME_URL.replace('{id}', id),
      method: 'PUT',
      body: {
        email,
        send_email: true,
      },
    };
    dispatch(loadingPage(true));
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        auth.setEmail(email);
        toastrSuccess('Sending Email is successful!');
        dispatch(loadingPage(false));
        return;
      }
      throw new Error('Sending Email is failed!');
    }).catch((err) => {
      toastrError(err.message);
      dispatch(loadingPage(false));
    });
  };

  useEffect(() => {
    fetchCMS();
    fetchData();
  }, []);
  const countryName = auth.getCountryName();
  const titlePersonality = getNameFromButtonBlock(cmsState?.buttonBlocks, 'You are {persionality}');
  return (
    <div>
      <MetaTags>
        <title>
          {replaceCountryInMetaTags(countryName, cmsState?.seo?.seoTitle)}
        </title>
        <meta name="description" content={replaceCountryInMetaTags(countryName, cmsState?.seo?.seoDescription)} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/quiz-ferrari')}
      </MetaTags>
      <div className="quiz-result-ferrari">
        <LandingFerrari
          id="id-header-landing-ferrari"
          cmsState={cmsState}
          isShowUrlImage
          urlImage={data?.personality_choice?.personality?.image2}
          title={titlePersonality.replace('{persionality}', (data?.personality_choice?.personality?.title || '...'))}
          onClick={() => {
            scrollToNext('id-block-persional');
          }}
        />
        <BlockPersional
          cmsState={cmsState}
          personality={data?.personality_choice?.personality}
        />
        <BlockScent
          cmsState={cmsState}
          productsDay={data?.products_day}
          productsNight={data?.products_night}
          mix={data?.mixes?.length > 0 ? data?.mixes[0] : 0}
          retakeQuiz={retakeQuiz}
        />
        {/* <BlockEmail
          cmsState={cmsState}
          onClickSend={onClickSend}
        /> */}
        <FooterResult />
      </div>
    </div>
  );
}

export default QuizResultFerrari;
