import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import './styles.scss';
import _ from 'lodash';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';

function BlockPersional(props) {
  const [index, setIndex] = useState(1);
  const persionalBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Your Personality Strengths');
  const scentBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Your Scent Preference');
  return (
    <div
      id="id-block-persional"
      className="block-persional"
    >
      <div className="context-persional">
        <div className="list-switch">
          <button
            onClick={() => setIndex(1)}
            type="button"
            className={classnames('button-bg__none', index === 1 ? 'active' : '')}
          >
            {persionalBt}
          </button>
          <button
            onClick={() => setIndex(2)}
            type="button"
            className={classnames('button-bg__none', index === 2 ? 'active' : '')}
          >
            {scentBt}
          </button>
        </div>
        <div className="div-info-detail">
          {
            _.map(index === 1 ? props.personality?.adjectives : props.personality?.scents, d => (
              <div className="line-text">
                {d?.name}
              </div>
            ))
          }
        </div>
        <div className="des-text">
          {
            index === 1 ? props.personality?.description : props.personality?.description2
          }
        </div>
      </div>
    </div>
  );
}

export default BlockPersional;
