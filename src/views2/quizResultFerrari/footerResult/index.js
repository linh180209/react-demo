import React, { useState, useEffect } from 'react';
import { logoFerrariMaison21g } from '../../../imagev2/png';
import './styles.scss';

function FooterResult(props) {
  return (
    <div className="footer-result">
      <img src={logoFerrariMaison21g} alt="logo" />
    </div>
  );
}

export default FooterResult;
