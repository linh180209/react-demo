import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import CheckBoxCT from '../../../componentsv2/checkBoxCT';
import InputCT from '../../../componentsv2/inputCT';
import LinkCT from '../../../componentsv2/linkCT';
import testImage from '../../../image/test/1.png';
import { getLinkFromButtonBlock, getNameFromButtonBlock } from '../../../Redux/Helpers';
import { useMergeState } from '../../../Utils/customHooks';
import './styles.scss';

function BlockEmail(props) {
  const login = useSelector(state => state.login);
  const basket = useSelector(state => state.basket);

  const [state, setState] = useMergeState({
    email: undefined,
    isAgreeEmail: true,
  });

  const onChangeEmail = (e) => {
    const { value } = e.target;
    setState({ email: value });
    _.assign(props.dataAll, { email: value });
  };

  const onCheckBox = () => {
    setState({ isAgreeEmail: !state.isAgreeEmail });
    _.assign(props.dataAll, { isAgreeEmail: !state.isAgreeEmail });
  };

  useEffect(() => {
    if (!state.email) {
      const email = login?.user?.email || basket?.email;
      if (email) {
        setState({
          email,
        });
      }
    }
  }, [login, basket]);

  const imageBlock = _.find(props.cmsState?.imagetxtBlocks, x => x.value?.image?.caption === 'image-email');
  const shareBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'SHARE YOUR UNIQUE SCENT DESIGNER EXPERIENCE');
  const nowBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Now you have found');
  const enterBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Enter your friend’s email');
  const submitBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Submit');
  return (
    <div className="block-email-result">
      <div className="content-email" style={{ backgroundImage: `url(${imageBlock?.value?.image?.image})` }}>
        <div className="content-text">
          <div className="title-email">
            {shareBt}
          </div>
          <div className="des-text">
            {nowBt}
          </div>
          <div className="div-email">
            <InputCT
              type="text"
              placeholder={enterBt}
              name="email"
              value={state.email}
              onChange={onChangeEmail}
              className="input-email"
            />
            <button
              onClick={() => props.onClickSend(state.email)}
              disabled={!state.isAgreeEmail || !state.email}
              className="bt-submit"
              type="button"
            >
              {submitBt}
            </button>
          </div>
          <div className="div-row row-check-box">
            <CheckBoxCT
              onChange={onCheckBox}
              name="create_address"
              checked={state.isAgreeEmail}
              label=""
              className="medium check-box-email"
            />
            <div className="link-text">
              {getNameFromButtonBlock(props?.cmsState?.buttonBlocks, 'I’ve read and accept all') || 'I’ve read and accept all'}
              {' '}
              <LinkCT to={getLinkFromButtonBlock(props?.cmsState?.buttonBlocks) || 'Terms and Conditions'}>{getNameFromButtonBlock(props?.cmsState?.buttonBlocks, 'Terms and Conditions') || 'Terms and Conditions'}</LinkCT>
              {' '}
              {getNameFromButtonBlock(props?.cmsState?.buttonBlocks, 'and') || 'and'}
              {' '}
              <LinkCT to={getLinkFromButtonBlock(props?.cmsState?.buttonBlocks, 'Privacy Policy')}>{getNameFromButtonBlock(props?.cmsState?.buttonBlocks, 'Privacy Policy') || 'Privacy Policy'}</LinkCT>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default BlockEmail;
