import React, { useState, useEffect } from 'react';
import TimerOutlinedIcon from '@mui/icons-material/TimerOutlined';
import _ from 'lodash';
import BubbleChartOutlinedIcon from '@mui/icons-material/BubbleChartOutlined';
import BarChartOutlinedIcon from '@mui/icons-material/BarChartOutlined';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import ProgressCircleV3 from '../../../../views/ResultScentV2/progressCircleV3';
import ColumnChart from '../../../../componentsv2/columnChart';
import LineChart from '../../../../componentsv2/lineChart';
import './styles.scss';
import VideoAcademy from '../../../../components/AcademyItem/videoAcademy';

function ItemScentMatch(props) {
  const desBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Your car scent');
  return (
    <div className="item-scent-match">
      <div className="sub-text">
        {props?.title}
      </div>
      <div className="title-text">
        {props?.subTitle}
      </div>
      <div className="des-text">
        {desBt}
      </div>
      {
        props.data && (
          <React.Fragment>
            <div className="div-chart">
              <div className="div-chart-scent">
                <div className="chart-lasting">
                  <div className="div-title div-row">
                    <div className="div-icon">
                      <TimerOutlinedIcon />
                    </div>
                    {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Lasting') || 'Lasting'}
                  </div>
                  <div className="chart-content">
                    <ProgressCircleV3
                      percent={props.data?.profile?.duration * 100}
                      trail="#DA291C"
                      fill="#30303000"
                      stroke="#303030"
                      textColor="#FFFFFF"
                    />
                  </div>
                </div>

                <div className="chart-strength">
                  <div className="div-title div-row">
                    <div className="div-icon">
                      <BubbleChartOutlinedIcon />
                    </div>
                    {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Strength') || 'Strength'}
                  </div>
                  <div className="chart-column">
                    <ColumnChart
                      isDisableTitle
                      color={props.data?.profile?.strength >= 0.5 ? '#DA291C' : '#303030'}
                      weight={50}
                    />
                    <ColumnChart
                      isDisableTitle
                      color={props.data?.profile?.strength >= 0.7 ? '#DA291C' : '#303030'}
                      weight={75}
                    />
                    <ColumnChart
                      isDisableTitle
                      color={props.data?.profile?.strength >= 0.8 ? '#DA291C' : '#303030'}
                      weight={100}
                    />
                  </div>
                </div>
              </div>
              <div className="chart-scent">
                <div className="div-title div-row">
                  <div className="div-icon">
                    <BarChartOutlinedIcon />
                  </div>
                  {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Scent Characteristics') || 'Scent Characteristics'}
                </div>
                {
                  _.map(props.data?.profile?.accords, d => (
                    <LineChart title={d?.name} color={d.color} value={d.weight} />
                  ))
                }
              </div>
            </div>
            <div className="list-video">
              {
                props.data?.combo[0]?.product?.videos?.length > 0 && (
                  <div className="item-video">
                    <div className="wrap-video">
                      <VideoAcademy
                        url={props.data?.combo[0]?.product?.videos[0]?.video}
                        poster={props.data?.combo[0]?.product?.videos[0]?.placeholder}
                        isControl
                      />
                    </div>
                    <div className="text-name">
                      {props.data?.combo[0]?.name}
                    </div>
                  </div>
                )
              }
              {
                props.data?.combo[1]?.product?.videos?.length > 0 && (
                  <div className="item-video">
                    <div className="wrap-video">
                      <VideoAcademy
                        url={props.data?.combo[1]?.product?.videos[0]?.video}
                        poster={props.data?.combo[1]?.product?.videos[0]?.placeholder}
                        isControl
                      />
                    </div>
                    <div className="text-name">
                      {props.data?.combo[1]?.name}
                    </div>
                  </div>
                )
              }
            </div>
          </React.Fragment>
        )
      }
    </div>
  );
}

export default ItemScentMatch;
