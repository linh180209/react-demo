import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import HeaderResult from '../headerResult';
import ItemScentMatch from './itemScentMatch';
import './styles.scss';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';

function BlockScent(props) {
  const [index, setIndex] = useState(props.mix);

  useEffect(() => {
    setIndex(props.mix);
  }, [props.mix]);

  const yourCarBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Your car scent match');
  const relaxationBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Relaxation & Destress');
  const freshnessBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Freshness & Purity');
  const delightBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Delight & Pleasure');
  const eleganceBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Elegance & Distinction');
  const toChillBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'To fee chill');
  const toConfidentBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'To feel confident');
  const toGoodBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'To feel good');
  const toUniqueBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'To feel unique');
  return (
    <div className="block-scent">
      <div className="div-content-block">
        <HeaderResult retakeQuiz={props.retakeQuiz} />
        <div className="title-scent">
          {yourCarBt}
        </div>
        <div className="list-options">
          <button
            type="button"
            onClick={() => setIndex(0)}
            className={classnames('button-bg__none item-option', index === 0 ? 'active' : '')}
          >
            {relaxationBt}
          </button>
          <button
            type="button"
            onClick={() => setIndex(1)}
            className={classnames('button-bg__none item-option', index === 1 ? 'active' : '')}
          >
            {freshnessBt}
          </button>
          <button
            type="button"
            onClick={() => setIndex(2)}
            className={classnames('button-bg__none item-option', index === 2 ? 'active' : '')}
          >
            {delightBt}
          </button>
          <button
            type="button"
            onClick={() => setIndex(3)}
            className={classnames('button-bg__none item-option', index === 3 ? 'active' : '')}
          >
            {eleganceBt}
          </button>
        </div>
        <ItemScentMatch
          cmsState={props.cmsState}
          title={index === 0 ? relaxationBt : index === 1 ? freshnessBt : index === 2 ? delightBt : eleganceBt}
          subTitle={index === 0 ? toChillBt : index === 1 ? toConfidentBt : index === 2 ? toGoodBt : toUniqueBt}
          data={index === 0 ? (props.productsDay?.length > 0 ? props.productsDay[0] : undefined) : index === 1 ? (props.productsDay?.length > 1 ? props.productsDay[1] : undefined) : index === 2 ? (props.productsNight?.length > 0 ? props.productsNight[0] : undefined) : (props.productsNight.length > 1 ? props.productsNight[1] : undefined)}
        />
      </div>
    </div>
  );
}

export default BlockScent;
