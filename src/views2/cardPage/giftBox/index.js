import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import ItemProduct from '../../../componentsv2/itemProduct';
import { GET_GIFT_WRAP } from '../../../config';
import { giftWrapping } from '../../../imagev2/png';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import { createBasketGuest, addProductBasket } from '../../../Redux/Actions/basket';
import { generaCurrency, getNameFromButtonBlock } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';

function GiftBox(props) {
  const basket = useSelector(state => state.basket);
  const dispatch = useDispatch();
  const [dataWrap, setDataWrap] = useState(undefined);


  const onClickAddToCart = () => {
    const { id, name, price } = dataWrap;
    const data = {
      item: id,
      is_featured: dataWrap.is_featured,
      name,
      price,
      quantity: 1,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    if (!basket.id) {
      dispatch(createBasketGuest(dataTemp));
    } else {
      dispatch(addProductBasket(dataTemp));
    }
  };

  const getGiftWrap = () => {
    const option = {
      url: GET_GIFT_WRAP,
      method: 'GET',
    };
    fetchClient(option).then((response) => {
      if (response && !response.isError) {
        const data = _.find(response, d => d.variant_values.length > 0 && d.variant_values[0] === 'Christmas');
        setDataWrap(data);
      } else {
        throw new Error(response.message);
      }
    }).catch((error) => {
      toastrError(error);
    });
  };

  useEffect(() => {
    getGiftWrap();
  }, []);

  const addToCartBt = getNameFromButtonBlock(props.buttonBlocks, 'ADD_TO_BAG');
  const thegiftboxBt = getNameFromButtonBlock(props.buttonBlocks, 'The gift box');
  const thegiftboxMobileBt = getNameFromButtonBlock(props.buttonBlocks, 'The gift box mobile');
  const giftBt = getNameFromButtonBlock(props.buttonBlocks, 'Gift');

  return (
    <div className="gift-box">
      <ItemProduct
        image={giftWrapping}
        title={giftBt}
        name={dataWrap?.name}
        subName={isMobile ? thegiftboxMobileBt : thegiftboxBt}
        price={generaCurrency(dataWrap?.price)}
        buttonName={addToCartBt}
        onClick={onClickAddToCart}
      />
    </div>
  );
}

export default GiftBox;
