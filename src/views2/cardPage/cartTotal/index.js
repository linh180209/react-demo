import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';
import ButtonCT from '../../../componentsv2/buttonCT';
import CardCT from '../../../componentsv2/cardCT';
import HeaderCT from '../../../componentsv2/headerCT';
import {
  logoBlack, dola, visa, mc, paypal,
} from '../../../imagev2/svg';
import {
  getNameFromButtonBlock, generaCurrency, generateUrlWeb, scrollToTargetAdjusted, onClickContinuteShoppingAmplitude, onClickCheckOutAmplitude, sendInitiateCheckoutClickHotjarEvent, segmentCheckoutStarted,
} from '../../../Redux/Helpers';
import './styles.scss';
import auth from '../../../Redux/Helpers/auth';
import { useMergeState } from '../../../Utils/customHooks';
import { GET_USER_ACTIONS } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import NewPaymentMethod from '../newPaymentMethod';
import { isBrowser } from '../../../DetectScreen';
import CardRedeem from '../cardRedeem';

function CardTotal(props) {
  const history = useHistory();
  const basket = useSelector(state => state.basket);
  const shippings = useSelector(state => state.shippings);
  const [state, setState] = useMergeState({
    shippingInfo: undefined,
    pointUser: 1,
    spendUser: 1,
  });

  const handleShowPoint = () => {
    const options = {
      url: GET_USER_ACTIONS,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        const ele = _.find(result, x => x.type === 'buy');
        if (ele) {
          setState({ pointUser: ele?.point, spendUser: ele?.spend });
        }
      }
    }).catch((err) => {
      console.log(err.mesage);
    });
  };

  const gotoFAQ = () => {
    scrollToTargetAdjusted('id-faq');
  };

  const gotoCheckOut = () => {
    onClickCheckOutAmplitude();
    sendInitiateCheckoutClickHotjarEvent();
    segmentCheckoutStarted(basket);
    history.push(generateUrlWeb('/checkout'));
  };

  const gotoContinue = () => {
    console.log('gotoContinue');
    // onClickContinuteShoppingAmplitude();
    history.push(generateUrlWeb('/all-products'));
  };

  useEffect(() => {
    handleShowPoint();
  }, []);

  useEffect(() => {
    if (shippings.length > 0) {
      const shippingInfo = _.find(shippings, d => d.country.code === auth.getCountry());
      setState({ shippingInfo });
    }
  }, [shippings]);

  const bookingSummaryBt = getNameFromButtonBlock(props.buttonBlocks, 'booking summary');
  const subTotalBt = getNameFromButtonBlock(props.buttonBlocks, 'Sub total');
  const discountBt = getNameFromButtonBlock(props.buttonBlocks, 'Discount');
  const totalBt = getNameFromButtonBlock(props.buttonBlocks, 'Total');
  const continuteBt = getNameFromButtonBlock(props.buttonBlocks, 'CONTINUE SHOPPING');
  const checkoutBt = getNameFromButtonBlock(props.buttonBlocks, 'Checkout');
  const receiveGrams = getNameFromButtonBlock(props.buttonBlocks, 'receive_grams');
  const receiveGramsLeft = getNameFromButtonBlock(props.buttonBlocks, 'receive_grams_left');
  const receiveGramsRight = getNameFromButtonBlock(props.buttonBlocks, 'receive_grams_right');
  const estimatedBt = getNameFromButtonBlock(props.buttonBlocks, 'estimated shipping cost');
  const getNowPayLater = getNameFromButtonBlock(props.buttonBlocks, 'Get_Now_Pay_Later');
  const newPaymentBt = getNameFromButtonBlock(props.buttonBlocks, 'New payment method');
  const getNowBt = getNameFromButtonBlock(props.buttonBlocks, 'Get now with pay later');
  const learnMoreBt = getNameFromButtonBlock(props.buttonBlocks, 'Learn more');
  const availablePaymentBt = getNameFromButtonBlock(props.buttonBlocks, 'available payment method');
  const freeDeliveryOverBt = getNameFromButtonBlock(props.buttonBlocks, 'FREE_DELIVERY_OVER');
  const freeDeliveryBt = getNameFromButtonBlock(props.buttonBlocks, 'FREE_DELIVERY');
  if (!basket) {
    return null;
  }

  const newPrice = parseFloat(basket?.discount) > 0 ? parseFloat(basket?.subtotal) - parseFloat(basket?.discount) : -1;
  const finalPrice = newPrice === -1 ? parseFloat(basket?.subtotal) : newPrice;
  const shippingValue = state.shippingInfo && parseFloat(state.shippingInfo?.threshold) > 0 && (parseFloat(state.shippingInfo?.threshold) < parseFloat(finalPrice)) ? 0 : state.shippingInfo?.shipping;
  const point = finalPrice ? parseInt(finalPrice * state.pointUser / state.spendUser, 10) : 0;

  const subTotalHtml = (
    <div className="line-1 div-row">
      <span>
        {subTotalBt}
      </span>
      <span>
        {generaCurrency(basket?.subtotal)}
      </span>
    </div>
  );

  const discountHtml = (
    <div className="line-1 div-row">
      <span>
        {discountBt}
      </span>
      <span>
        {'-'}
        {' '}
        {generaCurrency(basket?.discount)}
      </span>
    </div>
  );

  const shippingHtml = (
    <div className="line-4 div-row">
      <span>
        {estimatedBt.replace('{country}', state.shippingInfo?.country?.name)}
      </span>
      <span>
        {generaCurrency(shippingValue, true)}
      </span>
    </div>
  );

  const pointCardHtml = (
    <div className="div-point-card">
      {
        isBrowser ? (
          <div className="line-3 div-row">
            <img src={logoBlack} alt="icon" />
            <span>
              {receiveGramsLeft}
              {' '}
              <b>
                {point > 0 ? point : 0}
                G
              </b>
              {' '}
              {receiveGramsRight}
            </span>
          </div>
        ) : (
          <div className="line-3 div-row">
            <span>
              Points Earnded
            </span>
            <div className="div-point-line">
              <img src={logoBlack} alt="icon" />
              {point}
              G
            </div>
          </div>
        )
      }

    </div>
  );

  const conditionHtml = (
    <div className="line-5 div-row" onClick={gotoFAQ}>
      <img src={dola} alt="icon" />
      <span>{getNowPayLater}</span>
    </div>
  );

  const totalHtml = (
    <div className="line-2 div-row">
      <span>
        {totalBt}
      </span>
      <span>
        {generaCurrency(basket?.total)}
      </span>
    </div>
  );

  return (
    <div className="card-total">
      <CardCT className="card-t">
        <div className="div-free-delivery">
          {
            state.shippingInfo && parseFloat(state.shippingInfo.threshold) > 0 ? (
              `${freeDeliveryOverBt.replace('{price}', generaCurrency(state.shippingInfo.threshold, true))}`
            ) : (
              freeDeliveryBt
            )
          }
        </div>
        {
          isBrowser ? (
            <React.Fragment>
              <HeaderCT type="Heading-M" className="tc">
                {bookingSummaryBt}
              </HeaderCT>

              {subTotalHtml}

              {discountHtml}

              <hr />

              {totalHtml}

              {pointCardHtml}

              {shippingHtml}

              {auth.getCountry() !== 'ae' && conditionHtml}

              <div className="div-button div-col">
                <ButtonCT name={checkoutBt} onClick={gotoCheckOut} />
                <ButtonCT variant="outlined" name={continuteBt} onClick={gotoContinue} />
              </div>

              { auth.getCountry() !== 'ae' && <NewPaymentMethod buttonBlocks={props.buttonBlocks} onClickLearmore={props.onClickLearmore} /> }


              <div className="payment-method div-col">
                <hr />
                <span className="other-Tagline">{availablePaymentBt}</span>
                <div className="list-icon div-row">
                  <img src={visa} alt="visa" />
                  <img src={mc} alt="mc" />
                  {auth.getCountry() !== 'ae' && <img src={paypal} alt="paypal" />}
                </div>
              </div>

            </React.Fragment>
          ) : (
            <React.Fragment>

              <CardRedeem buttonBlocks={props.buttonBlocks} />

              <HeaderCT type="Heading-XXS" className="tc">
                {bookingSummaryBt}
              </HeaderCT>

              {subTotalHtml}

              {discountHtml}

              {shippingHtml}

              {pointCardHtml}

              <hr />

              {totalHtml}

              {auth.getCountry() !== 'ae' && conditionHtml}

            </React.Fragment>
          )
        }
      </CardCT>
    </div>
  );
}

export default CardTotal;
