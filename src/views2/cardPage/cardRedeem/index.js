import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import CheckIcon from '@mui/icons-material/Check';
import CardCT from '../../../componentsv2/cardCT';
import HeaderCT from '../../../componentsv2/headerCT';
import InputCT from '../../../componentsv2/inputCT';
import ButtonCT from '../../../componentsv2/buttonCT';
import {
  getNameFromButtonBlock, isCheckNull, generaCurrency, segmentCouponApplied,
} from '../../../Redux/Helpers';
import { promo } from '../../../imagev2/svg';
import './styles.scss';
import loadingPage from '../../../Redux/Actions/loading';
import { loadAllBasket } from '../../../Redux/Actions/basket';
import { useMergeState } from '../../../Utils/customHooks';
import auth from '../../../Redux/Helpers/auth';
import { ASSIGN_PROMOTION_CODE_CART } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { isBrowser } from '../../../DetectScreen';

function CardRedeem(props) {
  const dispatch = useDispatch();
  const basket = useSelector(state => state.basket);
  const [isCheckPromotion, setIsCheckPromotion] = useState(false);
  const [state, setState] = useMergeState({
    isPromoError: false,
    promotion: basket?.promo,
    isOPenPromo: false,
    promotionCode: basket?.promo?.code,
  });

  const onCheckPromo = async () => {
    try {
      dispatch(loadingPage(true));

      // segmentCouponEntered(basket?.id, state.promotionCode);

      const options = {
        url: `${ASSIGN_PROMOTION_CODE_CART.replace('{id}', basket?.id)}`,
        method: 'PUT',
        body: {
          promo: state.promotionCode,
        },
      };
      const result = await fetchClient(options, true);
      console.log('result', result);
      if (result?.isError) {
        throw new Error();
      } else {
        dispatch(loadAllBasket(result));

        segmentCouponApplied(basket?.id, result?.promo?.id, 
          state.promotionCode, result?.discount);

        // if (!disableToast) {
        //   toastrSuccess(proCodeAvaiBt);
        // }
        setState({
          isPromoError: false,
          promotion: result,
          isOPenPromo: false,
        });
      }
    } catch (error) {
      // toastrError(proCodeNotAvaiBt);
      setState({
        isPromoError: true,
        promotion: undefined,
      });
    } finally {
      dispatch(loadingPage(false));
    }
  };

  const onChangePromo = (e) => {
    const { value } = e.target;
    setState({ promotionCode: value });
  };

  const removePromotion = async () => {
    try {
      dispatch(loadingPage(true));
      const options = {
        url: `${ASSIGN_PROMOTION_CODE_CART.replace('{id}', basket?.id)}`,
        method: 'PUT',
        body: {
          promo: null,
        },
      };
      const result = await fetchClient(options, true);
      if (result?.error) {
        throw new Error();
      } else {
        dispatch(loadAllBasket(result));
        setState({
          isPromoError: false,
          promotion: null,
        });
      }
    } catch (error) {
      // toastrError(error.message);
      setState({
        isOPenPromo: false,
      });
    } finally {
      dispatch(loadingPage(false));
    }
  };

  useEffect(() => {
    const discountCode = auth.getDiscountCode();
    if (!_.isEmpty(basket) && !isCheckNull(discountCode) && basket?.promo?.code !== discountCode) {
      setState({ promotionCode: discountCode });
      setIsCheckPromotion(true);
      auth.setDiscountCode('');
    } else {
      setState({ promotion: basket?.promo, promotionCode: basket?.promo?.code || '' });
    }
  }, [basket]);

  useEffect(() => {
    if (isCheckPromotion) {
      onCheckPromo();
      setIsCheckPromotion(false);
    }
  }, [isCheckPromotion]);

  const isBrowserStyles = isBrowser && !props.isCheckOutPage;
  const redeemVoucher = getNameFromButtonBlock(props.buttonBlocks, 'REDEEM VOUCHER');
  const applyYour = getNameFromButtonBlock(props.buttonBlocks, 'Apply your discount voucher here');
  const discountCode = getNameFromButtonBlock(props.buttonBlocks, 'Discount Code');
  const applyNow = getNameFromButtonBlock(props.buttonBlocks, 'APPLY Now');
  const thisCodeInvalid = getNameFromButtonBlock(props.buttonBlocks, 'This voucher code is invalid');
  const vouhcerReedemedBt = getNameFromButtonBlock(props.buttonBlocks, 'vouhcer reedemed');
  const yougetBt = getNameFromButtonBlock(props.buttonBlocks, 'You get discount');

  const inputHtml = (
    <React.Fragment>
      <div className="div-input">
        <div className="input-wrap">
          <InputCT
            onChange={onChangePromo}
            type="text"
            placeholder={discountCode}
            size="small"
            value={state.promotionCode}
          />
          <img src={promo} alt="promo" />
        </div>
        <ButtonCT name={applyNow} size="small" onClick={onCheckPromo} />
      </div>
      {
        state.isPromoError && (
          <div className="mesage-error">
            {thisCodeInvalid}
          </div>
        )
      }
      {
        state.promotion && (
          <div className="message-success div-row">
            <CheckIcon />
            <div className="left-text">
              {
                isBrowserStyles && (
                  <HeaderCT type="Heading-S">
                    {vouhcerReedemedBt}
                  </HeaderCT>
                )
              }
              <span className="body-text-s-regular"><b>{yougetBt.replace('{price}', generaCurrency(basket?.discount))}</b></span>
            </div>
          </div>
        )
      }
    </React.Fragment>
  );
  return (
    <div className="card-reddeem">
      {
        isBrowserStyles ? (
          <CardCT className="card-r">
            <HeaderCT type="Heading-S">
              {redeemVoucher}
            </HeaderCT>
            <span className="body-text-s-regular">{applyYour}</span>

            {inputHtml}

          </CardCT>
        ) : (
          props.isCheckOutPage ? (
            inputHtml
          ) : (
            <CardCT className="card-mobile">
              {inputHtml}
            </CardCT>
          )
        )
      }
    </div>
  );
}

export default CardRedeem;
