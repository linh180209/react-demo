import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import LinearProgress from '@mui/material/LinearProgress';
import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { MetaTags } from 'react-meta-tags';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import AccordionCT from '../../componentsv2/accordionCT';
import BlockFAQ from '../../componentsv2/blockFAQ';
import ButtonCT from '../../componentsv2/buttonCT';
// import Header from '../../components/HomePage/header';
import CardItemV2 from '../../componentsv2/cardItemV2';
import HeaderCT from '../../componentsv2/headerCT';
import ItemProductV2 from '../../componentsv2/itemProductV2';
import USPBlock from '../../componentsv2/uspBlock';
import { GET_GIFT_FREE } from '../../config';
import { isBrowser, isMobile } from '../../DetectScreen/detectIFrame';
import {
  cartEmpty, gift, giftNotBg, giftNotBgSuccess, mc, paypal, visa,
} from '../../imagev2/svg';
import { addProductGiftBasket, deleteProductBasket, updateProductBasket } from '../../Redux/Actions/basket';
import {
  generaCurrency, generateHreflang, generateUrlWeb, getLinkFromButtonBlock, getNameFromButtonBlock, gotoShopHome, removeLinkHreflang, scrollToTargetAdjusted, segmentCheckoutStarted, sendInitiateCheckoutClickHotjarEvent, trackGTMViewCart,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { generateScriptFAQ, removeScriptFAQ } from '../../Utils/addScriptSchema';
import { useMergeState } from '../../Utils/customHooks';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';
import CardRedeem from './cardRedeem';
import CardTotal from './cartTotal';
import GiftBox from './giftBox';
import NewPaymentMethod from './newPaymentMethod';
import './styles.scss';

const getCmsCardPage = (data) => {
  if (data) {
    const { body } = data;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    const faqsBlock = _.find(body, x => x.type === 'faqs_block')?.value;
    const iconsBlockCard = _.find(body, x => x.type === 'icons_block' && x.value?.image_background?.caption === 'newCart').value;
    return { buttonBlocks, faqsBlock, iconsBlockCard };
  }
  return {
    buttonBlocks: [],
  };
};

function CardPage() {
  const history = useHistory();
  const showAskRegion = useSelector(state => state.showAskRegion);
  const dispatch = useDispatch();
  const basket = useSelector(state => state.basket);
  const cms = useSelector(state => state.cms);
  const countries = useSelector(state => state.countries);
  const isSentGTMData = useRef(false);

  const [state, setState] = useMergeState({
    giftFrees: [],
    listItem: [],
    faqsBlock: [],
    isOpenCustomBottle: false,
    isExpanded: false,
    isStarted: true,
    isEnded: false,
  });

  const updateProductBasketCT = (data) => {
    dispatch(updateProductBasket(data));
  };

  const deleteProductBasketCT = (data) => {
    dispatch(deleteProductBasket(data));
  };

  const fetchFreeGift = () => {
    const options = {
      url: GET_GIFT_FREE,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      const listItem = [];
      _.forEach(result, (d) => {
        _.forEach(d.items, x => (
          listItem.push(
            _.assign(x, { amount: d.amount }),
          )
        ));
      });
      setState({ listItem, giftFrees: _.orderBy(result, ['amount'], ['asc']) });
    }).catch((err) => {
      console.log('err', err);
    });
  };

  const giftFreeProduct = _.filter(basket?.items || [], x => x.is_free_gift);
  const onClickAddGift = (giftItem) => {
    const dataAdd = {
      idCart: basket.id,
      item: {
        item: giftItem.id,
        quantity: 1,
        is_free_gift: true,
        name: giftItem.name,
      },
    };
    const data = [dataAdd];
    dispatch(addProductGiftBasket(data));
  };

  const onClickLearmore = () => {
    scrollToTargetAdjusted('id-faq');
  };

  const onClickGoBack = () => {
    if (history.length === 0) {
      // history.push(generateUrlWeb('/'));
      gotoShopHome();
    } else {
      history.goBack();
    }
  };

  const onClickCheckOut = () => {
    sendInitiateCheckoutClickHotjarEvent();
    segmentCheckoutStarted(basket);
    history.push(generateUrlWeb('/checkout'));
  };

  useEffect(() => {
    if (cms) {
      const cmsCardPage = _.find(cms, x => x.title === 'Card Page');
      if (cmsCardPage) {
        const dataCms = getCmsCardPage(cmsCardPage);
        setState(dataCms);
        generateScriptFAQ(dataCms.faqsBlock, true);
      }
    }
  }, [cms]);

  const onscroll = () => {
    const ele = document.getElementById('id-list-product-detail');
    if (ele) {
      const object = {};
      if (ele.scrollLeft < 10 && !state.isStarted) {
        _.assign(object, { isStarted: true });
      } else if (state.isStarted) {
        _.assign(object, { isStarted: false });
      }

      const maxOffest = ele.scrollWidth - ele.clientWidth;
      if (Math.abs(ele.scrollLeft - maxOffest) < 10 && !state.isEnded) {
        _.assign(object, { isEnded: true });
      } else if (state.isEnded) {
        _.assign(object, { isEnded: false });
      }
      setState(object);
    }
  };

  const scrollToStart = () => {
    const ele = document.getElementById('id-list-product-detail');
    ele.scrollLeft = 0;
    setTimeout(() => {
      setState({ isEnded: false, isStarted: true });
    }, 500);
  };

  const scrollToEnd = () => {
    const ele = document.getElementById('id-list-product-detail');
    ele.scrollLeft = ele.scrollWidth - ele.clientWidth;
    setTimeout(() => {
      setState({ isEnded: true, isStarted: false });
    }, 500);
  };

  useEffect(() => {
    fetchFreeGift();
    return (() => {
      removeScriptFAQ();
      removeLinkHreflang();
    });
  }, []);

  useEffect(() => {
    if (isSentGTMData.current === false && basket.id) {
      isSentGTMData.current = true;
      trackGTMViewCart(basket);
    }
  }, [basket]);

  const getFreeGiftBt = getNameFromButtonBlock(state.buttonBlocks, 'get_free_gifts');
  const freeSamplesBt = getNameFromButtonBlock(state.buttonBlocks, 'free_sameples');
  const whileStockBt = getNameFromButtonBlock(state.buttonBlocks, 'While_stocks_last');
  const selectYourBt = getNameFromButtonBlock(state.buttonBlocks, 'Select your gift');
  const freeGiftBt = getNameFromButtonBlock(state.buttonBlocks, 'Free_gifts_selected');
  const spendMoreBt = getNameFromButtonBlock(state.buttonBlocks, 'Spend more');
  const addGiftBt = getNameFromButtonBlock(state.buttonBlocks, 'ADD GIFT');
  const added = getNameFromButtonBlock(state.buttonBlocks, 'Added');
  const continueBt = getNameFromButtonBlock(state.buttonBlocks, 'Continue Shopping');
  const thereareBt = getNameFromButtonBlock(state.buttonBlocks, 'There are currently no items in your basket');
  const seoTitleCms = getNameFromButtonBlock(state.buttonBlocks, 'seo_cart_page');
  const seoDescription = getLinkFromButtonBlock(state.buttonBlocks, 'seo_cart_page');
  const countryName = auth.getCountryName();
  const seoTitle = replaceCountryInMetaTags(countryName, seoTitleCms);

  const numberGift = `${giftFreeProduct.length}/${state.giftFrees.length}`;
  const amount = state.giftFrees && (state.giftFrees.length > 0) ? state.giftFrees[0].amount : 0;
  const maxValue = state.giftFrees.length > 0 ? state.giftFrees[state.giftFrees.length - 1].amount : 1;
  const minValue = state.giftFrees.length > 0 ? state.giftFrees[0].amount : -1;
  const percent = parseFloat(basket?.total) / maxValue * 100;

  const selectGift = (
    parseFloat(basket?.total) >= minValue ? (
      <div className="div-selected div-col">
        <span className="body-text-s-regular"><b>{selectYourBt}</b></span>
        <span className="body-text-s-regular">
          {numberGift}
          {' '}
          {freeGiftBt}
        </span>
      </div>
    ) : null
  );

  const summary = (
    <div className="summary-gift div-col">

      <div className="content-header div-row">
        <img src={gift} alt="icon" />
        <div className="title-summary div-col">
          <HeaderCT type={isBrowser ? 'Heading-S' : 'Heading-XS'}>
            {getFreeGiftBt}
          </HeaderCT>
          <span>
            {freeSamplesBt}
            {' '}
            <b>{generaCurrency(amount)}</b>
            {' '}
            {whileStockBt}
          </span>
        </div>
      </div>

      <div className="line-progress">
        <LinearProgress variant="determinate" value={percent > 100 ? 100 : percent} />
        {
          _.map(state.giftFrees || [], (d, index) => (
            <div
              style={index !== state.giftFrees.length - 1 ? { left: `${d.amount / maxValue * 100}%` } : {}}
              className={classnames('div-gift', parseFloat(basket?.total) >= d.amount ? 'active' : '', (index === state.giftFrees.length - 1 ? 'gift-end' : ''))}
            >
              <span className="price">{generaCurrency(d.amount)}</span>
              <div className="div-content">
                <span className="body-text-s-regular">
                  {`${index + 1}`}
                  {index === 0 ? 'st' : index === 1 ? 'nd' : 'th'}
                </span>
                <img src={parseFloat(basket?.total) >= d.amount ? giftNotBgSuccess : giftNotBg} alt="icon" />
              </div>
            </div>
          ))
        }
      </div>

      {(state.isExpanded && isMobile) ? null : selectGift}

    </div>
  );

  const details = (
    <div className="div-col div-wrapper-list">
      <div id="id-list-product-detail" className="list-product-detail div-col" onScroll={onscroll}>
        {
          _.map(state.listItem, d => (
            <div className="list-wrap">
              <div className="product-cart">
                <ItemProductV2
                  className="product-gift"
                  isPerfume
                  type="Perfume"
                  name={d.name}
                  image={d.image}
                  // scentName="Custome Scent"
                  buttonName={_.find(giftFreeProduct, x => x.item.id === d.id) ? added : addGiftBt}
                  color={_.find(giftFreeProduct, x => x.item.id === d.id) ? 'secondary' : undefined}
                  isDisabledButton={parseFloat(basket?.total) < d.amount || giftFreeProduct.length >= state.giftFrees.length}
                  buttonSize="small"
                  onClick={() => (_.find(giftFreeProduct, x => x.item.id === d.id) ? {} : onClickAddGift(d))}
                />
              </div>
              {
                parseFloat(basket?.total) < d.amount && (
                  <div className="text-note">{spendMoreBt.replace('{price}', generaCurrency(d.amount - parseFloat(basket?.total)))}</div>
                )
              }
            </div>
          ))
        }
      </div>
      {
        <React.Fragment>
          <button type="button" className={classnames('button-bg__none bt-arrow-left', state.isStarted ? 'hidden' : '')} onClick={scrollToStart}>
            <ArrowBackIosNewIcon />
          </button>
          <button type="button" className={classnames('button-bg__none bt-arrow-right', state.isEnded ? 'hidden' : '')} onClick={scrollToEnd}>
            <ArrowForwardIosIcon />
          </button>
        </React.Fragment>
        }
      {isMobile && selectGift}
    </div>
  );

  const emptyMobileCart = (
    <div className="empty-cart">
      <img src={cartEmpty} alt="icon" />
      <div className="other-Tagline tc">
        {thereareBt}
      </div>
      <ButtonCT
        name={continueBt}
        size="medium"
        onClick={onClickGoBack}
      />
    </div>
  );

  const totalBt = getNameFromButtonBlock(state.buttonBlocks, 'Total');
  const checkoutBt = getNameFromButtonBlock(state.buttonBlocks, 'Checkout');

  const totalMobileHtml = (
    <div className="total-mobile-html">
      <div className="text-total div-col">
        <span className="other-Tagline">
          {totalBt}
        </span>
        <span className="price">
          <b>{generaCurrency(basket?.total)}</b>
        </span>
      </div>
      <ButtonCT name={checkoutBt} size="medium" onClick={onClickCheckOut} />
    </div>
  );

  const shoppingPage = getNameFromButtonBlock(state.buttonBlocks, 'Shopping bag');
  const availablePaymentBt = getNameFromButtonBlock(state.buttonBlocks, 'available payment method');

  return (
    <div>
      {/* <Header isSpecialMenu isDisableScrollShow />
      { showAskRegion && (<div className="div-temp-region" />) } */}
      <MetaTags>
        <title>{seoTitle}</title>
        <meta name="description" content={seoDescription} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/cart')}
      </MetaTags>

      <HeaderHomePageV3 />
      {
        isBrowser ? (
          <div className="cart-page">
            <div className="cart-page-content">
              <div className="shopping-bag">
                <div className="list-item-cart div-col">
                  <HeaderCT type="Heading-L" className="title-page">
                    {shoppingPage}
                  </HeaderCT>

                  {
                    basket?.items?.length < 1 ? (
                      emptyMobileCart
                    ) : (
                      <React.Fragment>
                        {
                          (state.giftFrees?.length && auth.getCountry() !== 'ae') > 0 && (
                            <div className="margin-card-gift">
                              <AccordionCT summary={summary} details={details} />
                            </div>
                          )
                        }
                        {
                          _.map(basket?.items || [], (d, index) => (
                            <CardItemV2
                              updateProductBasket={updateProductBasketCT}
                              deleteProductBasket={deleteProductBasketCT}
                              data={d}
                              idCart={basket?.id}
                              isB2C
                              onCloseCard={() => {}}
                              cms={cms}
                              listCountry={countries}
                              onOpenCustomBottle={() => {}}
                              onCloseCustomBottle={() => {}}
                            />
                          ))
                        }
                        {
                          !['ae'].includes(auth.getCountry()) && (
                            <GiftBox buttonBlocks={state.buttonBlocks} />
                          )
                        }
                      </React.Fragment>
                    )
                  }
                </div>
                <div className="div-reedem div-col">
                  <div className="margin-card-redeem">
                    <CardRedeem buttonBlocks={state.buttonBlocks} />
                  </div>
                  <CardTotal buttonBlocks={state.buttonBlocks} onClickLearmore={onClickLearmore} />

                </div>
              </div>
              <USPBlock data={state.iconsBlockCard?.icons} />
              <BlockFAQ faqsBlock={state.faqsBlock} />
            </div>
          </div>
        ) : (
          <div className="cart-page-mobile">
            {
              basket?.items?.length < 1 ? (
                emptyMobileCart
              ) : (
                <React.Fragment>
                  {totalMobileHtml}
                  <div className="div-content div-col">
                    {
                      (state.giftFrees?.length && auth.getCountry() !== 'ae') && (
                        <div className="margin-card-gift">
                          <AccordionCT summary={summary} details={details} onChange={(event, isExpanded) => setState({ isExpanded })} />
                        </div>
                      )
                    }

                    {
                      _.map(basket?.items || [], (d, index) => (
                        <CardItemV2
                          updateProductBasket={updateProductBasketCT}
                          deleteProductBasket={deleteProductBasketCT}
                          data={d}
                          idCart={basket?.id}
                          isB2C
                          onCloseCard={() => {}}
                          cms={cms}
                          listCountry={countries}
                          onOpenCustomBottle={() => { setState({ isOpenCustomBottle: true }); }}
                          onCloseCustomBottle={() => { setState({ isOpenCustomBottle: false }); }}
                        />
                      ))
                    }
                    {
                      !['ae'].includes(auth.getCountry()) && (
                        <GiftBox buttonBlocks={state.buttonBlocks} />
                      )
                    }

                    <CardTotal buttonBlocks={state.buttonBlocks} onClickLearmore={onClickLearmore} />

                    <ButtonCT name={checkoutBt} onClick={onClickCheckOut} />

                  </div>
                </React.Fragment>
              )
            }
            {auth.getCountry() !== 'ae' && <NewPaymentMethod buttonBlocks={state.buttonBlocks} onClickLearmore={onClickLearmore} />}

            <div className="payment-method div-col">
              <span className="other-Tagline">{availablePaymentBt}</span>
              <div className="list-icon div-row">
                <img src={visa} alt="visa" />
                <img src={mc} alt="mc" />
                {auth.getCountry() !== 'ae' && <img src={paypal} alt="paypal" />}
              </div>
            </div>

            <USPBlock data={state.iconsBlockCard?.icons} />

            <BlockFAQ faqsBlock={state.faqsBlock} />
          </div>
        )
      }
    </div>
  );
}

export default CardPage;
