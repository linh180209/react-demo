import React, { useState, useEffect } from 'react';
import PaymentIcon from '@mui/icons-material/Payment';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import ButtonCT from '../../../componentsv2/buttonCT';
import './styles.scss';
import { isBrowser, isMobile } from '../../../DetectScreen/detectIFrame';

function NewPaymentMethod(props) {
  const newPaymentBt = getNameFromButtonBlock(props.buttonBlocks, 'New payment method');
  const getNowBt = getNameFromButtonBlock(props.buttonBlocks, 'Get now with pay later');
  const learnMoreBt = getNameFromButtonBlock(props.buttonBlocks, 'Learn more');

  return (
    <div className="new-pay-ment div-col" onClick={() => isMobile && props.onClickLearmore()}>
      <span className="other-Tagline">{newPaymentBt}</span>
      <hr />
      <div className="get-now div-row">
        <PaymentIcon fontSize="large" />
        <div className="div-info-left">
          <span className="body-text-s-regular m-size">
            <b>{getNowBt}</b>
          </span>
          {
            isBrowser ? (
              <ButtonCT
                onClick={props.onClickLearmore}
                name={learnMoreBt}
                variant="text"
                size="small"
                endIcon={<ArrowForwardIcon />}
              />
            ) : (
              <ButtonCT
                onClick={props.onClickLearmore}
                size="small"
                variant="text"
                onlyIcon={<ArrowForwardIcon />}
              />
            )
          }
        </div>
      </div>
    </div>
  );
}

export default NewPaymentMethod;
