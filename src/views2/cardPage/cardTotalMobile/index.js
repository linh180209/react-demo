import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import classnames from 'classnames';
import SwipeableDrawerMobile from '../../../componentsv2/swipeableDrawerMobile';
import ButtonCT from '../../../componentsv2/buttonCT';
import './styles.scss';
import CardTotal from '../cartTotal';
import { generaCurrency, generateUrlWeb, getNameFromButtonBlock } from '../../../Redux/Helpers';
import CardRedeem from '../cardRedeem';

function CartTotalMobile(props) {
  const history = useHistory();
  const [open, setOpen] = useState(false);
  const [isMessagePromo, setIsMessagePromo] = useState(false);
  const basket = useSelector(state => state.basket);

  const toggleDrawer = (flags) => {
    setOpen(flags);
  };

  const onClickCheckOut = () => {
    history.push(generateUrlWeb('/checkout'));
  };

  const totalBt = getNameFromButtonBlock(props.buttonBlocks, 'Total');
  const checkoutBt = getNameFromButtonBlock(props.buttonBlocks, 'Checkout');
  const miniHtml = (
    <div className="mini-total" onClick={() => toggleDrawer(true)}>
      <div className="anchor-line" />
      <CardRedeem buttonBlocks={props.buttonBlocks} onAddMessage={isMessage => setIsMessagePromo(isMessage)} />
    </div>
  );

  const fullHtml = (
    <div className="full-total">
      <CardTotal buttonBlocks={props.buttonBlocks} onClickLearmore={props.onClickLearmore} />
    </div>
  );

  return (
    <div className={classnames('cart-total-mobile', open ? 'is-open' : '', props.className)}>
      <div className="footer-bottom">
        <div className="text-total div-col">
          <span className="other-Tagline">
            {totalBt}
          </span>
          <span className="price">
            {generaCurrency(basket?.total)}
          </span>
        </div>
        <ButtonCT name={checkoutBt} size="medium" onClick={onClickCheckOut} />
      </div>
      <SwipeableDrawerMobile
        open={open}
        toggleDrawer={toggleDrawer}
        drawerBleeding={104}
        heightCard={isMessagePromo ? 355 : 312}
        miniHtml={miniHtml}
        fullHtml={fullHtml}
      />
    </div>
  );
}

export default CartTotalMobile;
