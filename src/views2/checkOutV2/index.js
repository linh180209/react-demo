import * as EmailValidator from 'email-validator';
import _ from 'lodash';
import moment from 'moment';
import queryString from 'query-string';
import React, { Component } from 'react';
import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { withRouter } from 'react-router-dom';
import smoothscroll from 'smoothscroll-polyfill';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import ButtonCT from '../../componentsv2/buttonCT';
import ShipBlock from '../../componentsv2/shipBlock';
import USPBlock from '../../componentsv2/uspBlock';
import {
  CHECKOUT_FUNCTION_URL, CHECK_EMAIL_LOGIN_URL, CHECK_PROMOTION_CODE_V2, CREATE_ADDRESS_URL, GET_BASKET_URL, GET_GIFT_FREE, GET_LIST_ADDRESS_URL, GET_SHIPPING_EXPRESS_URL, GET_SHIPPING_URL, PAYPAL_GENERATE_URL, PUT_OUTCOME_URL, STRIPE_GENERATE_URL, UPDATE_ADDRESS_GUEST_URL,
} from '../../config';
import { isMobile } from '../../DetectScreen/detectIFrame';
import {
  clearBasket, clearProductBasket, createBasketGuest, deleteProductBasket, preCheckOutBasket, updateProductArrayBasket, updateProductBasket,
} from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import {
  fetchCMSHomepage, generateHreflang, generateUrlWeb, getCmsCommon, getLinkFromButtonBlock, getNameFromButtonBlock, getNameFromCommon, getSEOFromCms, gotoShopHome, hiddenChatIcon, isAllGift, isCheckNull,
  objectEquals, pageViewCheckoutAmplitude, removeLinkHreflang, scrollTop, segmentCheckoutCustomerDetailsEntered, segmentPaymentInfoEntered, segmentPaymentInfoViewed, segmentShippingInfoEntered, segmentShippingInfoViewed, sendCheckoutStepViewedHotjarEvent, sendCustomerDetailsEnteredClickHotjarEvent,
  sendPaymentInfoEnteredClickHotjarEvent, sendShippingInfoEnteredClickHotjarEvent, trackGTMCheckOut, visibilityChatIcon,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import { addScriptGoogleAPI } from '../../Utils/addScriptGoogle';
import { addScriptCheckout, addScriptStripe } from '../../Utils/addStripe';
import FooterV2 from '../footer';
import ExpressCheckout from './expressCheckout';
import CustomerDetail from './infoUser/customerDetail';
import Payment from './infoUser/payment';
import ShippingInfo from './infoUser/shippingInfo';
import StepCheckOut from './infoUser/stepCheckOut';
import './infoUser/styles.scss';
import OrderDetail from './orderDetail';
import ShoppingBag from './shoppingBag';
import CardSum from './shoppingBag/cardSum';
import './styles.scss';


const getCms = (cms) => {
  if (!cms) {
    return { seo: {} };
  }
  const seo = getSEOFromCms(cms);
  const { body } = cms;
  const textBlock = _.filter(body, x => x.type === 'text_block');
  const feedbackBlock = _.find(body, x => x.type === 'feedbacks_block');
  const buttonBlock = _.filter(body, x => x.type === 'button_block');
  const imageBlockIcon = _.filter(body, x => x.type === 'image_block');
  const iconBlockScent = _.find(body, x => x.type === 'icons_block' && x.value?.image_background?.caption === 'icon-pay-late')?.value;
  const iconsBlockCard = _.find(body, x => x.type === 'icons_block' && x.value?.image_background?.caption === 'newCart')?.value;
  const blogBlock = _.find(body, x => x.type === 'blogs_block')?.value;
  return {
    seo,
    textBlock,
    imageBlockIcon,
    buttonBlock,
    iconsBlockCard,
    blogBlock,
    feedbackBlock,
    iconBlockScent,
  };
};
const listExpressTimeslot = ['8AM - 12PM', '12PM - 6PM', '6PM - 9PM'];
const addressDefaul = {
  street1: undefined,
  city: undefined,
  state: undefined,
  country: undefined,
  postal_code: undefined,
  pan_card: undefined,
  unit_number: undefined,
  detail: undefined,
};

class CheckOutV2 extends Component {
  constructor(props) {
    super(props);
    this.discountCode = auth.getDiscountCode();
    const { basket } = props;
    this.state = {
      promotionCode: basket?.promo?.code ? basket?.promo?.code : (this.discountCode ? this.discountCode : ''),
      noteText: '',
      promotion: basket?.promo?.id ? basket?.promo : {},
      payMethod: auth.getCountry() === 'ae' ? 'checkout.com' : 'stripe',
      bankCode: '',
      stepCheckOut: 1,
      isStep1Done: false,
      isStep2Done: false,
      isStep3Done: false,
      isStep4Done: false,
      isSelfCollect: false,
      isExpress: false,
      expressTimeslot: listExpressTimeslot[0],
      timeDelivery: 'Any Time',
      isBillAddressSame: true,
      seo: {},
      textBlock: [],
      imageBlockIcon: [],
      iconBlock: undefined,
      blogBlock: undefined,
      isShowCreateAccount: false,
      listError: [],
      buttonBlock: [],
      iconBlockTop: [],
      feedbackBlock: {},
      iconBlockScent: {},
      isOpenNote: false,
      isOPenPromo: false,
      isShowChoiseAddress: false,
      listAddress: [],
      isShowOptionAddress: false,
      giftFrees: [],
      isShowFooter: true,
    };
    this.shipping = 0;
    this.giftWrap = false;
    this.shipInfo = {};
    this.billingInfo = _.cloneDeep(addressDefaul);
    this.timeoutShipInfo = undefined;
    this.checkOutData = undefined;
    this.isCheckOutClicked = false;
    this.currentAddress = {};
    this.isCheckCartUpdate = false;
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { location } = nextProps;
    if (location !== prevState.location) {
      _.assign(objectReturn, { location, isUpdateUrl: true });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  componentDidMount() {
    pageViewCheckoutAmplitude();
    sendCheckoutStepViewedHotjarEvent();
    smoothscroll.polyfill();
    if (auth.getCountry() !== 'ae') {
      addScriptStripe();
    }
    addScriptGoogleAPI();
    if (isMobile) {
      // document.addEventListener('scroll', this.trackScrolling);
      hiddenChatIcon();
    }
    // document.body.appendChild(script);
    this.fetchCmsCheckOut();
    this.fetchShippingInfo();
    this.fetchFreeGift();
  }

  componentWillUnmount() {
    removeLinkHreflang();
    this.saveDataCheckOut();
    if (isMobile) {
      // document.removeEventListener('scroll', this.trackScrolling);
      visibilityChatIcon();
    }
  }

  saveDataCheckOut = () => {
    const stateData = _.cloneDeep(this.state);
    delete stateData.stepCheckOut;
    delete stateData.isStep1Done;
    delete stateData.isStep2Done;
    delete stateData.isStep3Done;
    delete stateData.isStep4Done;
    delete stateData.seo;
    delete stateData.textBlock;
    delete stateData.imageBlockIcon;
    delete stateData.iconBlock;
    delete stateData.blogBlock;
    delete stateData.listError;
    delete stateData.buttonBlock;
    delete stateData.isOpenNote;
    delete stateData.isOPenPromo;
    delete stateData.isShowChoiseAddress;
    delete stateData.listAddress;
    delete stateData.isShowOptionAddress;
    delete stateData.giftFrees;
    delete stateData.isShowFooter;
    delete stateData.iconBlockTop;
    delete stateData.feedbackBlock;
    delete stateData.iconBlockScent;
    delete this.shipInfo.idAddress;

    const dataCheckOut = {
      stateData,
      shipInfo: this.shipInfo,
      billingInfo: this.billingInfo,
      currentAddress: this.currentAddress,
    };
    auth.saveDataCheckOut(JSON.stringify(dataCheckOut));
  }

  getDataCheckOut = () => {
    try {
      const sDataCheckOut = auth.getDataCheckOut();
      if (sDataCheckOut) {
        const dataCheckOut = JSON.parse(sDataCheckOut);
        if (!_.isEmpty(dataCheckOut.shipInfo)) {
          this.shipInfo = dataCheckOut.shipInfo;
          this.billingInfo = dataCheckOut.billingInfo || _.cloneDeep(addressDefaul);
          this.shipInfo.country = isCheckNull(this.shipInfo?.country) ? auth.getCountry() : this.shipInfo?.country;
          this.billingInfo.country = isCheckNull(this.billingInfo?.country) ? auth.getCountry() : this.billingInfo?.country;
          this.currentAddress = this.currentAddress || dataCheckOut.currentAddress;
          if (this.state.promotionCode) {
            delete dataCheckOut.stateData?.promotionCode;
            delete dataCheckOut.stateData?.promotion;
          }
          this.setState(dataCheckOut.stateData, () => { this.preCheckOut(); });
        }
      }
    } catch (error) {
      console.log('error', error);
    }
  }

  isBottom = el => (
    el.getBoundingClientRect().bottom <= window.innerHeight
  )

  trackScrolling = () => {
    const wrappedElement = document.getElementById('id-promotion');
    if (this.isBottom(wrappedElement) && this.state.isShowFooter) {
      this.setState({ isShowFooter: false });
    } else if (!this.isBottom(wrappedElement) && !this.state.isShowFooter) {
      this.setState({ isShowFooter: true });
    }
  };

  fetchFreeGift = () => {
    const options = {
      url: GET_GIFT_FREE,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      this.setState({ giftFrees: result });
    }).catch((err) => {
      console.log('err', err);
    });
  }

  isCheckEqualBasket = (basket1, basket2) => {
    const clone1 = _.cloneDeep(basket1);
    const clone2 = _.cloneDeep(basket2);
    _.forEach(clone1.items, (x) => {
      delete x.id;
    });
    delete clone1.meta;
    delete clone1.store_discount;
    delete clone2.meta;
    delete clone2.store_discount;
    _.forEach(clone2.items, (x) => {
      delete x.id;
    });
    return objectEquals(clone1, clone2);
  }

  componentDidUpdate = (prevProps) => {
    const { basket, login } = this.props;
    const isOnlyGift = isAllGift(basket.items);
    delete basket.date_modified;
    if (!this.isCheckCartUpdate && basket && !this.isCheckEqualBasket(basket, prevProps.basket)) {
      this.isCheckCartUpdate = true;
      if (!this.state.promotionCode && !_.isEmpty(basket.promo) && basket.promo.code) {
        this.state.promotionCode = basket.promo.code;
        if (basket?.promo?.id) {
          this.setState({ promotion: basket?.promo });
        } else {
          this.onCheckPromo();
        }
      } else if (this.state.promotionCode) {
        this.onCheckPromo('NOT_SHOW_TOAST');
      } else {
        this.preCheckOut();
      }
    } else {
      this.isCheckCartUpdate = false;
    }
    if (login !== prevProps.login) {
      if (login && login.user && login.user.id) {
        this.shipInfo.name = `${login.user.first_name} ${login.user.last_name}`;
        this.shipInfo.firstName = login.user.first_name;
        this.shipInfo.lastName = login.user.last_name;
        this.shipInfo.email = login.user.email;
        this.shipInfo.phone = login.user.phone;
        this.shipInfo.dob = login.user.date_of_birth ? moment(login.user.date_of_birth).format('MM/DD/YYYY') : '';
        this.shipInfo.gender = login.user.gender ? (login.user.gender || 'Male') : 'Male';
        this.fetchAdress(true);
        this.setState({ listError: [] });
      }
    }

    const { location, isUpdateUrl } = this.state;
    if (isUpdateUrl) {
      this.state.isUpdateUrl = false;
      const values = queryString.parse(location.search);
      const { step } = values;
      if (!step || step === '1') {
        this.gotoStep1Local();
      } else if (step === '2') {
        if (!this.state.isStep1Done) {
          this.gotoStep1();
        } else {
          this.gotoStep2Local();
        }
      } else if (step === '3') {
        if (!this.state.isStep1Done) {
          this.gotoStep1();
        } else if (!this.state.isStep2Done && !isOnlyGift) {
          this.gotoStep2();
        } else {
          this.gotoStep3Local();
        }
      } else if (step === '4') {
        if (!this.state.isStep1Done) {
          this.gotoStep1();
        } else if (!this.state.isStep2Done && !isOnlyGift) {
          this.gotoStep2();
        } else if (!this.state.isStep3Done) {
          this.gotoStep3();
        } else {
          this.gotoStep4Local();
        }
      }
    }
  }

  fetchCmsCheckOut = () => {
    const { cms } = this.props;
    const cmsCheckOut = _.find(cms, x => x.title === 'CheckOut');
    if (!cmsCheckOut) {
      this.props.loadingPage(true);
      fetchCMSHomepage('checkout').then((result) => {
        const cmsData = result;
        this.props.addCmsRedux(cmsData);
        const dataCms = getCms(cmsData);
        this.setState(dataCms);
        this.props.loadingPage(false);
      }).catch((err) => {
        console.log('=err=', err);
        this.props.loadingPage(false);
      });
    } else {
      const dataCms = getCms(cmsCheckOut);
      this.setState(dataCms);
    }
  }

  onChangePromo = (e) => {
    const { value } = e.target;
    this.setState({ promotionCode: value, isPromoError: false });
  }

  onChangeGenderSelect = (value, name) => {
    this.shipInfo.gender = value?.value;
    this.forceUpdate();
  };

  onChangeBillingInfo = (e) => {
    const { value, name } = e.target;
    _.assign(this.billingInfo, { [name]: value });
    this.forceUpdate();
  };

  onChange = (e) => {
    const { value, name } = e.target;
    this.shipInfo[name] = value;
    this.forceUpdate();
    this.setState({ listError: [] });
    if (name === 'firstName' || name === 'lastName') {
      this.shipInfo.name = `${this.shipInfo.firstName} ${this.shipInfo.lastName}`;
    }
    if (name === 'email') {
      if (this.timeOutEmail) {
        clearTimeout(this.timeOutEmail);
        this.timeOutEmail = null;
      }
      this.timeOutEmail = setTimeout(this.checkEmailExist, 2000);
    }
    if (name === 'country') {
      // handle if change country but this country don't support express
      const { isExpress } = this.state;
      if (isExpress) {
        const ele = _.find(this.listShipping, x => x.country.code.toLowerCase() === value.toLowerCase());
        if (ele && !ele.is_express) {
          this.setState({ isExpress: false }, () => {
            this.preCheckOut();
            this.forceUpdate();
          });
          return;
        }
      }
      this.preCheckOut();
      this.forceUpdate();
    }
  }

  removePromotion = () => {
    this.setState({ promotion: undefined, promotionCode: '' }, () => {
      delete this.props.basket.promo;
      this.preCheckOut();
    });
  }

  onCheckPromo = (isNotShowToast) => {
    const { basket } = this.props;
    if (isNotShowToast !== 'NOT_SHOW_TOAST') {
      trackGTMCheckOut(6, basket);
    }

    const cmsCommon = getCmsCommon(this.props.cms);
    const proCodeNotAvaiBt = getNameFromCommon(cmsCommon, 'Promotion_code_is_not_avaialble.');
    const proCodeAvaiBt = getNameFromCommon(cmsCommon, 'Promotion_code_is_avaialble.');

    const { id } = basket;
    if (!id) {
      toastrError(proCodeNotAvaiBt);
      return;
    }

    const { promotionCode } = this.state;
    const options = {
      url: `${CHECK_PROMOTION_CODE_V2.replace('{id}', id)}`,
      method: 'POST',
      body: {
        code: promotionCode,
      },
    };
    fetchClient(options, true).then((result) => {
      const { isError } = result;
      if (isError) {
        if (isMobile && this.state.isShowFooter) {
          this.setState({ isPromoErrorMobile: true });
          setTimeout(() => {
            this.setState({ isPromoErrorMobile: false });
          }, 2000);
        } else if (isNotShowToast !== 'NOT_SHOW_TOAST') {
          toastrError(proCodeNotAvaiBt);
        }
        this.setState({ promotion: '', isPromoError: true }, () => {
          this.preCheckOut(true);
        });
      } else {
        this.setState({
          promotion: result, promotionCode: result.code, isOPenPromo: false, isPromoError: false,
        }, () => {
          this.preCheckOut();
          this.saveDataCheckOut();
        });
        if (isNotShowToast !== 'NOT_SHOW_TOAST') {
          toastrSuccess(proCodeAvaiBt);
        }
      }
    }).catch((err) => {
      if (isNotShowToast !== 'NOT_SHOW_TOAST') {
        toastrError(err.message);
      }
    });
  }


  assignEmailToBasket = (basketId, email) => {
    if (!basketId || !email) {
      return null;
    }
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basketId),
      method: 'PUT',
      body: {
        email,
      },
    };
    return fetchClient(option);
  }

  assignBasketToUser = (basketId, userId) => {
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basketId),
      method: 'PUT',
      body: {
        user: userId,
      },
    };
    return fetchClient(option);
  }

  createAddressFromApplePay = async (body) => {
    const { login } = this.props;
    const isLogined = login && login.user && login.user.id;
    try {
      const address = isLogined ? await this.createAddressWithUser(body) : await this.createAddress(body);
      return address;
    } catch (err) {
      return undefined;
    }
  }


  handleTokenCheckoutCom = (token) => {
    this.onCheckOutFunc(token);
  }

  onClickCheckOutFunc = () => {
    if (this.state.payMethod === 'checkout.com') {
      window.Frames.submitCard();
    } else {
      this.onCheckOutFunc();
    }
  }

  onCheckOutFunc = async (tokenCheckOutCom) => {
    if (this.isCheckOutClicked) {
      return;
    }

    const { basket, login } = this.props;

    // tracking GMT
    trackGTMCheckOut(8, basket);

    this.saveDataCheckOut();

    if (!basket || !basket.id) {
      toastrError('Checkout is failed');
      return;
    }
    const isOnlyGift = isAllGift(basket.items);
    const isLogined = login && login.user && login.user.id;

    // assign user to cart, if basket id !== login cart
    if (isLogined && basket.id !== login.id) {
      this.assignBasketToUser(basket.id, login.id);
    }

    if (!isOnlyGift) {
      if (!this.checkContinuteStep1('payment') || !this.checkContinuteStep2('payment')) {
        return;
      }
    } else if (!this.checkContinuteStep1('payment')) {
      return;
    }

    const {
      payMethod, noteText, isSelfCollect, isExpress, promotion,
      expressTimeslot, bankCode,
    } = this.state;
    if (!this.shipInfo.idAddress) {
      const address = isLogined && this.shipInfo.create_address ? await this.createAddressWithUser(this.shipInfo) : await this.createAddress(this.shipInfo);
      if (!address) {
        toastrError('Check Out Error');
        return;
      }
      this.shipInfo.idAddress = address.id;
    }

    this.props.loadingPage(true);

    let idbillingAddress;
    if (this.state.isBillAddressSame) {
      const resultAddress = await this.createAddress(this.shipInfo);
      idbillingAddress = resultAddress?.id;
    } else {
      const resultAddress = await this.createAddress(_.assign(this.shipInfo, this.billingInfo));
      idbillingAddress = resultAddress?.id;
    }

    const ele = _.find(this.listShipping, x => x.country.code.toLowerCase() === this.shipInfo.country.toLowerCase());
    const options = {
      url: CHECKOUT_FUNCTION_URL.replace('{id}', basket.id),
      method: 'POST',
      body: {
        payment_method: ['atm_card', 'int_card'].indexOf(payMethod) >= 0 ? 'zalopay' : payMethod,
        bank_code: ['zalopayapp', 'momo', 'atm_card', 'int_card'].indexOf(payMethod) ? bankCode : '',
        shipping_address: (isSelfCollect || isOnlyGift) ? undefined : this.shipInfo.idAddress,
        billing_address: idbillingAddress,
        note: noteText,
        shipping: this.shipping,
        is_self_collect: isSelfCollect,
        is_express: isExpress && ele && ele.is_express,
        express_timeslot: isExpress && ele && ele.is_express ? expressTimeslot : undefined,
        personality_choice: this.idPersonalityChoice,
        // delivery_slot: timeDelivery,
        packaging: this.packaging,
        is_gift_wrapped: this.giftWrap,
        gender: this.shipInfo.gender,
        date_of_birth: this.shipInfo.dob ? moment(this.shipInfo.dob, 'MM/DD/YYYY').valueOf() / 1000 : undefined,
        is_get_newsletters: this.shipInfo.is_get_newsletters,
      },
    };
    if (promotion && promotion.id) {
      options.body.promos = [promotion.id];
    }
    if (this.props.refPoint !== '') {
      options.body.ref = this.props.refPoint;
    }
    if (!isLogined) {
      options.body.meta = {
        guest: {
          name: this.shipInfo.name,
          email: this.shipInfo.email,
          cart: basket.id,
          phone: this.shipInfo.phone,
          create_account: isOnlyGift ? false : this.shipInfo.create_account,
        },
      };
    }
    if (tokenCheckOutCom) {
      options.body.meta = _.assign(options.body.meta || {}, {
        'checkout.com': {
          token: tokenCheckOutCom,
        },
      });
    }
    this.isCheckOutClicked = true;

    fetchClient(options, true).then((result) => {
      const { isError, order } = result;
      this.isCheckOutClicked = false;
      this.props.loadingPage(false);
      if (tokenCheckOutCom && result.status === 202) {
        window.location.href = result.link;
        return;
      }
      if (!isError) {
        if (order) {
          this.props.history.push(generateUrlWeb(`/pay/success/${order}`));
          this.props.clearBasket();
          return;
        }
        if (payMethod === 'paypal') {
          const htmlProcess = `${result.substring(0, 6)}id='id-pay'${result.substring(6, result.length)}`;
          const s = document.getElementById('div-button');
          s.innerHTML = htmlProcess;
          const idForm = document.getElementById('id-pay');
          idForm.submit();
        } else if (
          [
            'zalopay',
            'momo',
            'bank_transfer',
            'atm_card',
            'int_card',
            'cod',
          ].indexOf(payMethod) >= 0
        ) {
          if (result.status) {
            window.location.href = result.data.url;
            return;
          }
          toastrError(result.message);
        } else {
          this.redirectStripe(result);
        }
      } else {
        throw new Error(result?.message);
      }
    }).catch((err) => {
      toastrError(err);
      this.isCheckOutClicked = false;
      this.props.loadingPage(false);
    });
  }

  updateAddress = (id, data) => {
    if (!id) {
      return;
    }
    const options = {
      url: UPDATE_ADDRESS_GUEST_URL.replace('{id}', id),
      method: 'PUT',
      body: data,
    };
    fetchClient(options, true).then((result) => {
      console.log('result', result);
    });
  }

  paypalGenerate = (order, total) => {
    const defaultHeaders = {
      'Content-Type': 'application/json',
    };
    const options = {
      method: 'POST',
      body: JSON.stringify({
        order,
        total,
      }),
      headers: defaultHeaders,
    };
    fetch(new Request(PAYPAL_GENERATE_URL, options))
      .then(result => result.text()).then((html) => {
        const htmlProcess = `${html.substring(0, 6)}id='id-pay'${html.substring(6, html.length)}`;
        const s = document.getElementById('div-button');
        s.innerHTML = htmlProcess;
        const idForm = document.getElementById('id-pay');
        idForm.submit();
      }).catch((err) => {
        toastrError(err);
      });
  }

  stripeGenerate = (order, total) => {
    const options = {
      url: STRIPE_GENERATE_URL,
      method: 'POST',
      body: {
        order,
        total,
      },
    };
    fetchClient(options)
      .then((result) => {
        this.redirectStripe(result);
      }).catch((err) => {
        toastrError(err);
      });
  }

  redirectStripe = (formPay) => {
    const { key, session } = formPay;
    const stripe = window.Stripe(key);
    stripe.redirectToCheckout({
      // Make the id field from the Checkout Session creation API response
      // available to this file, so you can provide it as parameter here
      // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
      sessionId: session,
    }).then((result) => {
      console.log('result', result);
    });
  }

  initalData = (address) => {
    console.log('address ==', address);
    const { login } = this.props;
    const defaults = {
      firstName: login && login.user ? `${login.user.first_name}` : '',
      lastName: login && login.user ? `${login.user.last_name}` : '',
      name: login && login.user ? `${login.user.first_name} ${login.user.last_name}` : '',
      dob: login && login.user && login.user.date_of_birth ? moment(login.user.date_of_birth).format('MM/DD/YYYY') : '',
      gender: login && login.user && login.user.gender ? login.user.gender : 'Male',
      street1: address ? address.street1 : '',
      street2: address ? address.street2 : '',
      apartment: address ? address.apartment : '',
      state: address ? address.state : '',
      pan_card: '',
      city: address ? address.city : '',
      postal_code: address ? address.postal_code : '',
      unit_number: address ? address.unit_number : '',
      detail: address ? address.detail : '',
      country: address?.country?.code || auth.getCountry(),
      email: login && login.user ? login.user.email : auth.getEmail(),
      phone: address ? address.phone : (this.shipInfo?.phone || ''), // choose new address maybe clear phone
      billStreet1: '',
      billPostalCode: '',
      billPhone: '',
      billCountry: auth.getCountry(),
      create_account: true,
      create_address: true,
      is_get_newsletters: true,
    };
    this.shipInfo = defaults;
    this.preCheckOut();
    this.forceUpdate();
  }

  isAllGift = (data) => {
    const ele = _.find(data, d => d.item.product.type.name !== 'Gift');
    return !ele;
  }

  preCheckOutApplePay = (country) => {
    this.shipInfo.country = country;
    this.preCheckOut();
  }

  preCheckOut = (isNotCheckPromo = false) => {
    const { country } = this.shipInfo;
    const { id, items, promo } = this.props.basket;
    const { isSelfCollect, isExpress, promotion } = this.state;
    if (!items || items.length === 0) {
      if (!id) {
        return;
      }
      gotoShopHome();
      return;
    }
    const isOnlyGift = this.isAllGift(items);
    const eleCountry = _.find(this.listShipping, x => x.country?.code?.toLowerCase() === country?.toLowerCase());
    if (id && eleCountry) {
      const data = {
        id,
        body: isSelfCollect || isOnlyGift ? {} : {
          shipping_address: {
            country: country ? country?.toLowerCase() : '',
          },
          is_express: !!(isExpress && eleCountry?.is_express),
          is_gift_wrapped: this.giftWrap,
          promos: undefined,
        },
      };

      // check discount code
      if (!isCheckNull(this.discountCode)) {
        this.state.promotionCode = this.discountCode;
        this.discountCode = '';
        auth.setDiscountCode('');
        this.onCheckPromo();
        return;
      }

      if (promotion && promotion.id) {
        data.body.promos = [promotion.id];
      } else if (promo && promo.code && !isNotCheckPromo) {
        this.state.promotionCode = promo.code;
        this.onCheckPromo();
        return;
      }
      this.props.preCheckOutBasket(data);
    }
  }

  isCheckCountry = (country) => {
    const shipMoney = _.find(this.listShipping, d => d.country.code === country);
    return !!shipMoney;
  }

  updateShipping = (country) => {
    const shipMoney = _.find(this.listShipping, d => d.country.code === country);
    let shipping = 0;
    if (shipMoney) {
      const {
        subtotal,
      } = this.props.basket;

      if (subtotal >= shipMoney.threshold) {
        shipping = 0;
      } else {
        shipping = shipMoney.shipping;
      }
    } else {
      shipping = 0;
    }
    return shipping;
  }

  fetchOutCome = () => {
    if (isCheckNull(auth.getOutComeId())) {
      return;
    }
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', auth.getOutComeId()),
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        const { personality_choice: personalityChoice } = result;
        this.idPersonalityChoice = personalityChoice ? personalityChoice.id : undefined;
      }
    }).catch((err) => {
      console.log('err', err);
    });
  }

  fetchShippingInfo = () => {
    const optionExpress = {
      url: GET_SHIPPING_EXPRESS_URL,
      method: 'GET',
    };
    const options = {
      url: GET_SHIPPING_URL,
      method: 'GET',
    };
    Promise.all([fetchClient(options), fetchClient(optionExpress)]).then((results) => {
      _.forEach(results[1], (x) => {
        const ele = _.find(results[0], d => d.country.code === x.country.code);
        if (ele) {
          ele.is_express = x.is_express;
          ele.shippingExpress = x.shipping;
        }
      });
      _.forEach(results[0], (x) => {
        x.name = x.country.name;
      });
      this.listShipping = _.orderBy(results[0], 'name', 'asc');
      this.fetchAdress();
    }).catch((err) => {
      console.log('err', err);
    });
  }

  fetchAdress = (isSkipGetDataSaveCheckout) => {
    const dataUser = _.isEmpty(this.props.login) ? auth.getLogin() : this.props.login;
    if (dataUser && dataUser.user && dataUser.user.id) {
      const option = {
        url: GET_LIST_ADDRESS_URL,
        method: 'GET',
      };
      fetchClient(option, true).then((result) => {
        if (result && !result.isError) {
          this.setState({ listAddress: result, isShowOptionAddress: result && result.length > 0 });
          const address = _.find((result), x => x.is_default_baddress);
          if (!address && result.length > 0) {
            this.initalData(result[result.length - 1]);
            this.currentAddress = result[result.length - 1];
            // this.getDataCheckOut();
            return;
          }
          this.currentAddress = address;
          this.initalData(address);

          if (!isSkipGetDataSaveCheckout) {
            // get data checkout before
            this.getDataCheckOut();
          }

          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        console.log('error: ', error);
      });
    } else {
      this.setState({ isShowOptionAddress: false });
      this.initalData();

      // get data checkout before
      this.getDataCheckOut();
    }
  }

  isAddressSame = () => {
    if (_.isEmpty(this.currentAddress)) {
      return false;
    }
    const {
      country, city, email, postal_code, phone, street1, state, unit_number,
    } = this.currentAddress;
    if (this.shipInfo.country !== (country ? country.code : undefined)) {
      return false;
    }
    if (this.shipInfo.city !== city && !_.isEmpty(this.shipInfo.city && !_.isEmpty(city))) {
      return false;
    }
    if (this.shipInfo.email !== email) {
      return false;
    }
    if (this.shipInfo.postal_code !== postal_code) {
      return false;
    }
    if (this.shipInfo.unit_number !== unit_number) {
      return false;
    }
    if (this.shipInfo.phone !== phone) {
      return false;
    }
    if (this.shipInfo.street1 !== street1) {
      return false;
    }
    if (this.shipInfo.state !== state && !_.isEmpty(this.shipInfo.state && !_.isEmpty(state))) {
      return false;
    }
    return true;
  }

  validityAddress = (data) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const key in data) {
      if (!data[key] && key !== 'idAddress') {
        return false;
      }
    }
    return true;
  }

  createAddressWithUser = async (body) => {
    const { user } = this.props.login;
    if (user && user.id) {
      _.assign(body, { is_active: this.shipInfo.create_address });
      const options = {
        url: GET_LIST_ADDRESS_URL,
        method: 'POST',
        body,
      };
      try {
        const address = await fetchClient(options, true);
        return address;
      } catch (error) {
        console.log('error', error);
      }
      return null;
    }
  }

  createAddress = async (body) => {
    const options = {
      url: CREATE_ADDRESS_URL,
      method: 'POST',
      body,
    };
    try {
      const address = await fetchClient(options);
      return address;
    } catch (error) {
      console.log('error', error);
    }
    return null;
  }

  onChangeTimeDelivery = (e) => {
    const { value } = e.target;
    this.setState({ timeDelivery: value });
  }

  onChangeExpressTimeSlot = (d) => {
    this.setState({ expressTimeslot: d });
  }

  changeNote = (value) => {
    _.assign(this.state, { noteText: value });
  }

  onClickCreateAccount = (e) => {
    const { checked } = e.target;
    this.shipInfo.create_account = checked;
    this.forceUpdate();
  }

  onClickCreateAddress = (e) => {
    const { checked } = e.target;
    this.shipInfo.create_address = checked;
    this.forceUpdate();
  }

  handleUpdatePromoItem = (promo) => {
    const { items } = this.props.basket;
    const { id, gift } = promo;
    const itemPromos = _.filter(items, x => x.item.product.type.toLowerCase() === gift.type.toLowerCase());
    const listItemUpdate = [];
    let quantityPromo = gift.quantity;
    _.forEach(itemPromos, (d) => {
      if (quantityPromo > 0) {
        listItemUpdate.push({
          idCart: this.props.basket.id,
          item: {
            promo: id,
            quantity: d.quantity,
            total: (d.quantity - quantityPromo < 0 ? 0 : (d.quantity - quantityPromo) * d.price),
            id: d.id,
          },
        });
        quantityPromo -= d.quantity;
      }
    });
    this.props.updateProductArrayBasket(listItemUpdate);
  }

  gotoAnchor = (id) => {
    const ele = document.getElementById(id);
    if (ele) {
      ele.scrollIntoView({ behavior: 'smooth' });
    }
  }

  checkEmailExist = () => {
    const { login } = this.props;
    const isLogined = login && login.user && login.user.id;
    if (isLogined) {
      this.setState({ isShowCreateAccount: false });
      this.shipInfo.create_account = false;
      return;
    }
    const { email } = this.shipInfo;
    if (!email) {
      this.setState({ isShowCreateAccount: true });
      return;
    }
    const option = {
      url: CHECK_EMAIL_LOGIN_URL.replace('{email}', email),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.shipInfo.create_account = false;
        this.setState({ isShowCreateAccount: false });
        return;
      }
      throw new Error();
    }).catch(() => {
      this.shipInfo.create_account = true;
      this.setState({ isShowCreateAccount: true });
    });
  }

  gotoStep1 = () => {
    const url = generateUrlWeb('/checkout/?step=1');
    this.props.history.push(url);
  };

  gotoStep1Local = () => {
    this.setState({
      stepCheckOut: 1, isStep1Done: false, isStep2Done: false, isStep3Done: false,
    });
    setTimeout(() => {
      scrollTop();
    }, 500);
  }

  gotoStep2 = () => {
    const url = generateUrlWeb('/checkout/?step=2');
    this.props.history.push(url);
  };

  gotoStep2Local = () => {
    this.setState({ stepCheckOut: 2, isStep2Done: false, isStep3Done: false });
    setTimeout(() => {
      this.gotoAnchor('anchor-step2');
    }, 500);
  }

  gotoStep3 = () => {
    const url = generateUrlWeb('/checkout/?step=3');
    this.props.history.push(url);
  };

  gotoStep3Local = () => {
    this.setState({ stepCheckOut: 3, isStep3Done: false });
    setTimeout(() => {
      this.gotoAnchor('anchor-step3');
    }, 500);
  }

  gotoStep4 = () => {
    const url = generateUrlWeb('/checkout/?step=4');
    this.props.history.push(url);
  };

  gotoStep4Local = () => {
    this.setState({ stepCheckOut: 4 });
    setTimeout(() => {
      this.gotoAnchor('anchor-step4');
    }, 500);
  }

  onClickEditStep1 = () => {
    this.setState({ isStep1Done: false, isStep2Done: false, isStep3Done: false });
    this.gotoStep1();
  };

  onClickEditStep2 = () => {
    this.setState({ isStep2Done: false, isStep3Done: false });
    this.gotoStep2();
  }

  onClickEditStep3 = () => {
    this.setState({ isStep3Done: false });
    this.gotoStep3();
  }

  checkContinuteStep1 = (status) => {
    // const { name, email, phone } = this.shipInfo;
    const {
      email, firstName, lastName, phone,
    } = this.shipInfo;
    const { listError } = this.state;
    const fieldError = [];
    const isPhoneValid = phone?.length && phone?.length > 5;
    this.saveDataCheckOut();
    if (!firstName?.trim() || !email?.trim() || !lastName?.trim() || !isPhoneValid) {
      if (!email?.trim()) {
        fieldError.push('email');
        listError.push('email');
      }
      if (!firstName?.trim()) {
        fieldError.push('first name');
        listError.push('firstName');
      }
      if (!lastName?.trim()) {
        fieldError.push('last name');
        listError.push('lastName');
      }
      if (!isPhoneValid) {
        fieldError.push('phone');
        listError.push('phone');
      }
      toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }

    // check '@' include first name, last name
    if (firstName.includes('@')) {
      fieldError.push('First name');
      listError.push('firstName');
    }
    if (lastName.includes('@')) {
      fieldError.push('Last name');
      listError.push('lastName');
    }
    if (listError.length > 0) {
      toastrError('@ is an invalid character for name');
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }

    if (!EmailValidator.validate(this.shipInfo.email)) {
      listError.push('email');
      toastrError('The email is not available');
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }
    this.checkEmailExist();
    if (status !== 'payment') {
      // pageViewShippingAmplitude();
      this.setState({ isStep1Done: true, isStep2Done: false });
      const isOnlyGift = this.isAllGift(this.props.basket?.items);
      if (isOnlyGift) {
        this.gotoStep3();
      } else {
        this.gotoStep2();
      }
      // segmentCheckoutCompletedStep(0, this.props.basket, this.getShippingMethod(), this.state.payMethod);
      segmentCheckoutCustomerDetailsEntered(this.props.basket, this.shipInfo);
    }

    // assign email into basket
    const { basket } = this.props;
    if (basket?.email !== email) {
      this.assignEmailToBasket(basket?.id, email);
    }

    sendCustomerDetailsEnteredClickHotjarEvent();
    segmentShippingInfoViewed(this.props.basket, this.shipInfo);
    return true;
  }

  checkContinuteStep2 = (status) => {
    const { isSelfCollect, isBillAddressSame, listError } = this.state;
    const {
      street1, postal_code: postalCode, country, pan_card: panCard, unit_number: unitNumber,
    } = this.shipInfo;
    console.log('this.shipInfo', this.shipInfo);
    const fieldError = [];
    const isPanCardError = ['in', 'id'].includes(country) && !panCard?.trim();
    const isPostalCodeError = !['ae', 'sa'].includes(country) && !postalCode?.trim();
    const isUnitNumberError = country === 'sg' && !unitNumber?.trim();
    this.saveDataCheckOut();
    if (!isSelfCollect && (!street1?.trim() || isPostalCodeError || isPanCardError || isUnitNumberError)) {
      if (!street1?.trim()) {
        fieldError.push('address');
        listError.push('street1');
      }
      if (isUnitNumberError) {
        fieldError.push('Unit Number');
        listError.push('unit_number');
      }
      if (isPostalCodeError) {
        fieldError.push('zip code');
        listError.push('postal_code');
      }
      if (isPanCardError) {
        fieldError.push('pan_card');
        listError.push('pan_card');
      }
      toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
      this.setState({ stepCheckOut: 2, isStep2Done: false, listError });
      return false;
    }
    if (!isBillAddressSame) {
      const {
        street1: billStreet1, postal_code: billPostalCode, country: billCountry,
      } = this.billingInfo;
      const isPostalCodeBillingError = !['ae', 'sa'].includes(billCountry) && !billPostalCode?.trim();

      if (!billStreet1 || isPostalCodeBillingError) {
        if (!billStreet1) {
          fieldError.push('address');
          listError.push('billStreet1');
        }
        if (isPostalCodeBillingError) {
          fieldError.push('zip code');
          listError.push('billPostalCode');
        }
        toastrError(`Please enter your billing ${_.join(fieldError, ', ')}`);
        this.setState({ stepCheckOut: 2, isStep2Done: false, listError });
        return false;
      }
    }
    if (status !== 'payment') {
      // pageViewPaymentAmplitude();
      this.setState({ isStep2Done: true, isStep3Done: false });
      this.gotoStep3();
      segmentShippingInfoEntered(this.props.basket, this.getShippingMethod(), this.getShippingAddress(), this.shipInfo?.country);
      segmentPaymentInfoViewed(this.props.basket, this.getShippingMethod(), this.getShippingAddress(), this.shipInfo?.country);
    }
    sendShippingInfoEnteredClickHotjarEvent();

    return true;
  }

  checkContinuteStep3 = () => {
    if (!this.state.payMethod) {
      toastrError('Please select a payment method');
      return;
    }
    if (this.state.payMethod === 'checkout.com') {
      if (!this.isValidCheckoutCom) {
        toastrError('Please enter a valid card number');
        return;
      }
    }
    const isVietNam = this.shipInfo?.country && this.shipInfo?.country.toLowerCase() === 'vn';
    if (isVietNam && ![
      'zalopay',
      'momo',
      'bank_transfer',
      'atm_card',
      'int_card',
      'cod',
    ].includes(this.state.payMethod)) {
      toastrError('Please select a payment method');
      return;
    }
    this.setState({ isStep3Done: true });
    this.gotoStep4();
    segmentPaymentInfoEntered(this.props.basket, this.getShippingMethod(), this.state.payMethod, this.getShippingAddress(), this.shipInfo?.country); // misplaced??

    sendPaymentInfoEnteredClickHotjarEvent();
  }

  updateValidateCheckoutCom = (flags) => {
    this.isValidCheckoutCom = flags;
  }

  prevStepCheckOut = (step) => {
    this.setState({ stepCheckOut: step });
  }

  isShowPackagingOption = (basket) => {
    const { items } = basket;
    let flags = false;
    _.forEach(items, (d) => {
      const { item } = d;
      if (item) {
        const type = item.product.type.name;
        const countrySupport = ['sg', 'au'];
        if ((item.is_sample === false) && (type === 'Perfume' || type === 'Creation' || type === 'Scent' || type === 'Elixir') && (this.shipInfo.country ? countrySupport.includes(this.shipInfo.country.toLowerCase()) : false)) {
          flags = true;
        }
      }
    });
    return flags;
  }

  selectPackagingOption = (id) => {
    this.packaging = id;
  };

  selectGiftWrap = (value) => {
    this.giftWrap = value;
    this.preCheckOut();
  }

  openShowAddress = () => {
    this.setState({ isShowChoiseAddress: true });
  }

  closeShowAddress = () => {
    this.setState({ isShowChoiseAddress: false });
  }

  onChooseNewAddress = (flag) => {
    if (flag) {
      this.initalData();
    } else {
      this.initalData(this.currentAddress);
    }
  }

  onChangeAddress = (address) => {
    this.currentAddress = address;
    this.initalData(address);
  }

  generateListStep = (isOnlyGift, isHasFreeGift, cmsCommon, buttonBlock) => {
    const customerBt = getNameFromCommon(cmsCommon, 'CUSTOMER');
    const shippingBt = getNameFromCommon(cmsCommon, 'Shipping');
    const promotionBt = getNameFromButtonBlock(buttonBlock, 'PROMOTION');
    const paymentBt = getNameFromCommon(cmsCommon, 'PAYMENT');
    const listStep = [
      {
        name: customerBt,
        step: 1,
      },
    ];
    if (!isOnlyGift) {
      listStep.push({
        name: shippingBt,
        step: 2,
      });
    }
    if (isHasFreeGift && !isOnlyGift) {
      listStep.push({
        name: promotionBt,
        step: 3,
      });
    }
    listStep.push({
      name: paymentBt,
      step: isOnlyGift ? 2 : isHasFreeGift ? 4 : 3,
    });
    return listStep;
  }

  prevStepCheckOutStep1 = () => {
    this.prevStepCheckOut(1);
    this.setState({ isStep1Done: false });
    setTimeout(() => {
      this.gotoAnchor('anchor-step1');
    }, 500);
  };

  prevStepCheckOutStep2 = () => {
    this.prevStepCheckOut(2);
    this.setState({ isStep2Done: false });
    setTimeout(() => {
      this.gotoAnchor('anchor-step2');
    }, 500);
  };

  getShippingMethod = () => {
    const { isSelfCollect, isExpress } = this.state;
    if (isSelfCollect) {
      return 'Self Collect';
    } if (isExpress) {
      return 'Express';
    }
    return 'Standard';
  }

  getShippingAddress = () => {
    const {
      street1, city, state, postal_code, country, detail, unit_number: unitNumber,
    } = this.shipInfo;
    const street2 = detail ? `${detail}` : unitNumber ? `${unitNumber}` : '';
    return `${street2 ? `${street2},` : ''}${street1}, ${city}, ${state}, ${postal_code}, ${country}`;
  }

  render() {
    const { basket, login } = this.props;
    const {
      payMethod, noteText, isSelfCollect, isExpress, expressTimeslot,
      stepCheckOut, isStep1Done, isStep2Done, isStep3Done, isBillAddressSame, seo,
      isShowCreateAccount, listError, imageBlockIcon, buttonBlock, listAddress, isShowOptionAddress, giftFrees, isPromoError,
      promotion, iconBlockScent,
    } = this.state;
    const isLogined = login?.user?.id;
    if (!basket) {
      return (<div />);
    }
    const {
      shipping, items,
    } = basket;
    const isOnlyGift = this.isAllGift(items);
    const orBt = getNameFromButtonBlock(buttonBlock, 'OR');
    const completeCheckOutBt = getNameFromButtonBlock(buttonBlock, 'complete checkout');
    const byClickingBt = getNameFromButtonBlock(buttonBlock, 'By clicking Place Order you agree to the');
    const termAndConditionBt = getNameFromButtonBlock(buttonBlock, 'Terms and Conditions');
    const termAndConditionLink = getLinkFromButtonBlock(buttonBlock, 'Terms and Conditions');

    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/checkout')}
        </MetaTags>

        <HeaderHomePageV3 />

        <div className="checkout-v2">
          {isMobile && this.state.iconsBlockCard?.icons?.length && <ShipBlock data={this.state.iconsBlockCard?.icons} />}
          <div className="content-checkout-v2">

            <div className="info-user-v2 div-col">
              <StepCheckOut
                buttonBlocks={buttonBlock}
                stepCheckOut={stepCheckOut}
                isOnlyGift={isOnlyGift}
                onClickEditStep1={this.onClickEditStep1}
                onClickEditStep2={this.onClickEditStep2}
                onClickEditStep3={this.onClickEditStep3}
              />
              {
                stepCheckOut === 1 && !['kr', 'id', 'my', 'sa', 'vn', 'ae'].includes(auth.getCountry())
                && (
                <React.Fragment>
                  <ExpressCheckout
                    buttonBlock={buttonBlock}
                    promotion={this.state.promotion}
                    basket={this.props.basket}
                    createAddressFromApplePay={this.createAddressFromApplePay}
                    listShipping={this.listShipping}
                    preCheckOutApplePay={this.preCheckOutApplePay}
                    loadingPage={this.props.loadingPage}
                  />
                  <div className="div-line-wrap">
                    <div className="line-type" />
                    <div className="other-Tagline">{orBt}</div>
                    <div className="line-type" />
                  </div>
                </React.Fragment>
                )
              }
              {
                (isStep1Done || isStep2Done || isStep3Done) && (
                  <OrderDetail
                    buttonBlock={buttonBlock}
                    cms={this.props.cms}
                    shipInfo={this.shipInfo}
                    billingInfo={this.billingInfo}
                    basket={this.props.basket}
                    isStep1Done={isStep1Done}
                    isStep2Done={isStep2Done}
                    isStep3Done={isStep3Done}
                    isSelfCollect={isSelfCollect}
                    isExpress={isExpress}
                    expressTimeslot={expressTimeslot}
                    isBillAddressSame={isBillAddressSame}
                    payMethod={payMethod}
                    onClickEditStep1={this.onClickEditStep1}
                    onClickEditStep2={this.onClickEditStep2}
                    onClickEditStep3={this.onClickEditStep3}
                  />
                )
              }
              {
                isStep1Done && (isStep2Done || isOnlyGift) && isStep3Done && (
                  <React.Fragment>
                    {
                      isMobile && (
                        <CardSum
                          buttonBlocks={buttonBlock}
                          cms={this.props.cms}
                          noteText={noteText}
                          changeNote={this.changeNote}
                          className="card-sum-mobile"
                          shipInfo={this.shipInfo}
                        />
                      )
                    }
                    <ButtonCT
                      className={classnames('button-complete', auth.getCountry() === 'ae' ? 'big-bt' : '')}
                      name={completeCheckOutBt}
                      onClick={this.onClickCheckOutFunc}
                    />
                    <div className="text-by tc mt-2">
                      {byClickingBt}
                      <a href={termAndConditionLink} className="ml-2">
                        {termAndConditionBt}
                      </a>
                    </div>
                  </React.Fragment>
                )
              }
              {
                !isStep1Done && (
                  <CustomerDetail
                    stepCheckOut={stepCheckOut}
                    isStepDone={isStep1Done}
                    buttonBlock={buttonBlock}
                    cms={this.props.cms}
                    login={login}
                    shipInfo={this.shipInfo}
                    onChange={this.onChange}
                    listError={listError}
                    basket={basket}
                    promotion={promotion}
                    onClickContinute={this.checkContinuteStep1}
                    shippingMethod={this.getShippingMethod()}
                    payMethod={payMethod}
                  />
                )
              }
              {
                !isStep2Done && !isOnlyGift && (
                  <ShippingInfo
                    stepCheckOut={stepCheckOut}
                    isStepDone={isStep2Done}
                    buttonBlock={buttonBlock}
                    cms={this.props.cms}
                    isLogined={isLogined}
                    login={login}
                    currentAddress={this.currentAddress}
                    onChooseNewAddress={this.onChooseNewAddress}
                    onChangeAddress={this.onChangeAddress}
                    listAddress={listAddress}
                    isShowOptionAddress={isShowOptionAddress}
                    isExpress={isExpress}
                    isSelfCollect={isSelfCollect}
                    shipInfo={this.shipInfo}
                    billingInfo={this.billingInfo}
                    onChange={this.onChange}
                    onChangeBillingInfo={this.onChangeBillingInfo}
                    onClickContinute={this.checkContinuteStep2}
                    listShipping={this.listShipping}
                    listExpressTimeslot={listExpressTimeslot}
                    expressTimeslot={this.state.expressTimeslot}
                    onChangeExpressTimeSlot={this.onChangeExpressTimeSlot}
                    shipping={shipping}
                    listError={listError}
                    basket={basket}
                    selectGiftWrap={this.selectGiftWrap}
                    isShowPackagingOption={this.isShowPackagingOption(this.props.basket)}
                    selectPackagingOption={this.selectPackagingOption}
                    isShowCreateAccount={isShowCreateAccount}
                    prevStepCheckOut={this.prevStepCheckOutStep1}
                    isBillAddressSame={isBillAddressSame}
                    onClickCheckBillAddress={e => this.setState({ isBillAddressSame: e })}
                    onClickSelfCollect={() => this.setState({ isSelfCollect: true, isExpress: false, isBillAddressSame: false }, () => { this.preCheckOut(); })}
                    onClickStandard={() => this.setState({ isSelfCollect: false, isExpress: false }, () => { this.preCheckOut(); })}
                    onClickExpress={() => this.setState({ isSelfCollect: false, isExpress: true }, () => { this.preCheckOut(); })}
                    onClickCreateAddress={this.onClickCreateAddress}
                    shippingMethod={this.getShippingMethod()}
                    shippingAddress={this.getShippingAddress()}
                    shippingCountry={this.shipInfo?.country}
                    payMethod={payMethod}
                  />
                )
              }
              <Payment
                isOnlyGift={isOnlyGift}
                cms={this.props.cms}
                stepCheckOut={stepCheckOut}
                isStepDone={isStep3Done}
                buttonBlock={buttonBlock}
                iconBlockScent={iconBlockScent}
                prevStepCheckOut={isOnlyGift ? this.prevStepCheckOutStep1 : this.prevStepCheckOutStep2}
                onClickContinute={this.checkContinuteStep3}
                payMethod={payMethod}
                imageBlockIcon={imageBlockIcon}
                shipInfo={this.shipInfo}

                onClickStripe={() => { this.setState({ payMethod: 'stripe' }); }}
                onClickPaypal={() => { this.setState({ payMethod: 'paypal' }); }}
                onClickPaypalLate={() => { this.setState({ payMethod: 'stripe_pay_later' }); }}
                onClickCheckout={() => { this.setState({ payMethod: 'checkout.com' }); }}

                onClickZaloPay={() => { this.setState({ payMethod: 'zalopay', bankCode: 'zalopayapp' }); }}
                onClickMomo={() => { this.setState({ payMethod: 'momo', bankCode: 'payUrl' }); }}
                onClickBankTransfer={() => { this.setState({ payMethod: 'bank_transfer', bankCode: '' }); }}
                onClickAtmCard={() => { this.setState({ payMethod: 'atm_card', bankCode: 'ATM' }); }}
                onClickIntCard={() => { this.setState({ payMethod: 'int_card', bankCode: 'CC' }); }}
                onClickCod={() => { this.setState({ payMethod: 'cod', bankCode: '' }); }}
                shippingMethod={this.getShippingMethod()}
                updateValidateCheckoutCom={this.updateValidateCheckoutCom}
                handleTokenCheckoutCom={this.handleTokenCheckoutCom}
              />

            </div>

            <ShoppingBag
              changeNote={this.changeNote}
              noteText={noteText}
              buttonBlock={buttonBlock}
              cms={this.props.cms}
              updateProductBasket={this.props.updateProductBasket}
              deleteProductBasket={this.props.deleteProductBasket}
              shipInfo={this.shipInfo}
            />
          </div>
          {!isMobile && <USPBlock data={this.state.iconsBlockCard?.icons} />}
        </div>
        <div id="div-button" className="hidden" />
        <FooterV2 />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    login: state.login,
    basket: state.basket,
    refPoint: state.refPoint,
    cms: state.cms,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  deleteProductBasket,
  updateProductBasket,
  updateProductArrayBasket,
  clearBasket,
  clearProductBasket,
  preCheckOutBasket,
  createBasketGuest,
  addCmsRedux,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CheckOutV2));
