import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import { ecoBox, cottonPouch } from '../../../../imagev2/svg';
import { GET_PACKAGING_OPTIONS_URL } from '../../../../config';
import { getNameFromButtonBlock, getNameFromCommon } from '../../../../Redux/Helpers';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import { useMergeState } from '../../../../Utils/customHooks';
import { isMobile } from '../../../../DetectScreen';
import ChooseOptionCheckOut from '../../../../componentsv2/chooseOptionCheckout';

function PackageOption(props) {
  const [state, setState] = useMergeState({
    idChoise: 0,
    data: [],
  });

  const fetchData = () => {
    const options = {
      url: GET_PACKAGING_OPTIONS_URL,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      setState({ data: result });
    });
  };

  const onChange = (id) => {
    setState({ idChoise: id });
    props.selectPackagingOption(id);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const packagingBt = getNameFromCommon(props.cmsCommon, 'PACKAGING_OPTION');
  const solidOutBt = getNameFromButtonBlock(props.buttonBlock, 'Sold Out');

  return (
    <React.Fragment>
      <div className={classnames('other-Tagline', isMobile ? 's-size' : '')}>
        {packagingBt}
      </div>
      {
        _.map(state.data, d => (
          <ChooseOptionCheckOut
            icon={d?.image}
            text={d?.name}
            checked={state.idChoise === d?.id}
            onClick={() => onChange(d?.id)}
            disabled={d?.is_out_of_stock}
            text2={d?.is_out_of_stock ? solidOutBt : undefined}
          />
        ))
      }
    </React.Fragment>
  );
}

export default PackageOption;
