import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect, useRef } from 'react';
import { PopoverBody, UncontrolledPopover } from 'reactstrap';
import ButtonCT from '../../../../componentsv2/buttonCT';
import CheckBoxCT from '../../../../componentsv2/checkBoxCT';
import ChooseOptionCheckOut from '../../../../componentsv2/chooseOptionCheckout';
import DropdownCheckout from '../../../../componentsv2/dropdownCheckout';
import InputCT from '../../../../componentsv2/inputCT';
import NumberHeader from '../../../../componentsv2/numberHeader';
import { isMobile } from '../../../../DetectScreen/detectIFrame';
import icInfo from '../../../../image/icon/ic-info.svg';
import {
  generaCurrency, getCmsCommon, getNameFromButtonBlock, getNameFromCommon, trackGTMCheckOut,
} from '../../../../Redux/Helpers';
import auth from '../../../../Redux/Helpers/auth';
import { useMergeState } from '../../../../Utils/customHooks';
import useForceRerender from '../../../../Utils/useForceRerender';
import '../styles.scss';


function ShippingInfo(props) {
  const forceRerender = useForceRerender();
  const isSentSegmet = useRef(false);
  const cmsCommon = getCmsCommon(props.cms);
  const [state, setState] = useMergeState({
    isShowChangeAddress: false,
    isNewAddress: false,
    isShowOptionAddress: props.isShowOptionAddress,
  });

  const onChangeInputAutoCompleteBilling = (value, name) => {
    props.onChangeBillingInfo({
      target: {
        value,
        name,
      },
    });
  };

  const onChangeAutoCompleteBilling = async (data, isSearchCity) => {
    const {
      country: autoCompleteCountry,
      address: autoCompleteAddress,
      city: autoCompleteCity,
      state: autoCompleteState,
      zip: autoCompleteZip,
    } = data;
    if (!isSearchCity) {
      props.billingInfo.street1 = autoCompleteAddress;
    }
    props.billingInfo.city = autoCompleteCity;
    props.billingInfo.state = autoCompleteState?.name;
    props.billingInfo.postal_code = autoCompleteZip;
    if (autoCompleteCountry) {
      props.onChangeBillingInfo({
        target: {
          value: autoCompleteCountry?.code?.toLowerCase(),
          name: 'country',
        },
      });
    }
  };

  const clearAutoCompleteBilling = (isSearchCity) => {
    props.billingInfo.city = '';
    props.billingInfo.state = '';
    props.billingInfo.postal_code = '';
    if (!isSearchCity) {
      props.billingInfo.street1 = '';
    }
    props.onChangeBillingInfo({
      target: {
        value: props.billingInfo.street1,
        name: 'street1',
      },
    });
  };

  useEffect(() => {
    if (!props.isBillAddressSame) {
      props.billingInfo.country = props.shipInfo.country;
      clearAutoCompleteBilling();
      forceRerender();
    }
  }, [props.isBillAddressSame]);

  useEffect(() => {
    setState({ isShowOptionAddress: props.isShowOptionAddress });
  }, [props.isShowOptionAddress]);


  useEffect(() => {
    if (props.basket?.id && isSentSegmet.current === false) {
      isSentSegmet.current = true;
    }
  }, [props.basket]);

  useEffect(() => {
    trackGTMCheckOut(3, props.basket);
  }, []);

  const onChangeInputAutoComplete = (value, name) => {
    props.onChange({
      target: {
        value,
        name,
      },
    });
  };

  const onChangeAutoComplete = async (data, isSearchCity) => {
    const {
      country: autoCompleteCountry,
      address: autoCompleteAddress,
      city: autoCompleteCity,
      state: autoCompleteState,
      zip: autoCompleteZip,
    } = data;
    if (!isSearchCity) {
      props.shipInfo.street1 = autoCompleteAddress;
    }
    props.shipInfo.city = autoCompleteCity;
    props.shipInfo.state = autoCompleteState?.name;
    props.shipInfo.postal_code = autoCompleteZip;
    if (autoCompleteCountry) {
      props.onChange({
        target: {
          value: autoCompleteCountry?.code?.toLowerCase(),
          name: 'country',
        },
      });
    }
  };


  const clearAutoComplete = (isSearchCity) => {
    props.shipInfo.city = '';
    props.shipInfo.state = '';
    props.shipInfo.postal_code = '';
    if (!isSearchCity) {
      props.shipInfo.street1 = '';
    }
    props.onChange({
      target: {
        value: props.shipInfo.street1,
        name: 'street1',
      },
    });
  };

  const summary = () => (
    <div className="header-customer-detail">
      <NumberHeader number={2} text="SHIPPING" isHeader />
    </div>
  );

  const chooseTimeSlot = () => (
    <div className="text-extend">
      {
        _.map(props.listExpressTimeslot, d => (
          <ButtonCT
            name={d}
            variant={props.expressTimeslot === d ? undefined : 'outlined'}
            onClick={(e) => {
              e.stopPropagation();
              props.onChangeExpressTimeSlot(d);
            }}
          />
        ))
      }
    </div>
  );

  const detailCreateAccount = () => {
    const addressBt = getNameFromCommon(cmsCommon, 'Address');
    const countryBt = getNameFromCommon(cmsCommon, 'COUNTRY');
    const cityBt = getNameFromCommon(cmsCommon, 'city');
    const zipCodeBt = getNameFromCommon(cmsCommon, 'ZIP_CODE');
    const stateBt = getNameFromCommon(cmsCommon, 'State');

    return (
      <div className="detail-customer-detail create-account">
        <div className="line-2">
          <InputCT
            type="autocomplete"
            label={`${addressBt}*`}
            isMedium={isMobile}
            className={classnames(isMobile ? 'medium' : '', props.listError.includes('street1') ? 'error' : '')}
            isSearchCity={false}
            name="street1"
            id="street1"
            onKeyDown={() => {}}
            placeholder={addressBt}
            country={props.billingInfo?.country || 'sg'}
            value={props.billingInfo?.street1}
            onChangeInput={onChangeInputAutoCompleteBilling}
            onChange={onChangeAutoCompleteBilling}
            eventClear={clearAutoCompleteBilling}
            isError={props.listError.includes('billStreet1')}
            errorMessage=""
          />
        </div>
        <div className="line-1">
          <InputCT
            type="select-checkout"
            placeholder={countryBt}
            name="country"
            onChange={props.onChangeBillingInfo}
            options={props.listShipping}
            label={countryBt}
            value={props.billingInfo?.country}
            className={classnames(isMobile ? 'medium' : '')}
          />
          <InputCT
            type="text"
            label={cityBt}
            placeholder={cityBt}
            value={props.billingInfo?.city}
            onChange={props.onChangeBillingInfo}
            name="city"
            className={classnames(isMobile ? 'medium' : '')}
          />
        </div>
        <div className="line-1">
          {
            !['ae', 'sa'].includes(props.billingInfo?.country) && (
              <InputCT
                type="text"
                placeholder={zipCodeBt}
                name="postal_code"
                value={props.billingInfo.postal_code}
                onChange={props.onChangeBillingInfo}
                label={`${zipCodeBt}*`}
                className={classnames(isMobile ? 'medium' : '', props.listError.includes('billPostalCode') ? 'error' : '', ['us'].includes(props.billingInfo?.country) ? '' : 'w-100')}
              />
            )
          }
          {
          props.billingInfo?.country === 'us'
            ? (
              <InputCT
                type="text"
                placeholder={`${stateBt}`}
                name="state"
                value={props.billingInfo?.state}
                onChange={props.onChangeBillingInfo}
                label={stateBt}
                className={classnames(isMobile ? 'medium' : '')}
              />
            ) : undefined
        }
        </div>
      </div>
    );
  };
  const changeAddressHtml = () => (
    <div className="list-address">
      {
        _.map(props.listAddress, d => (
          <div
            className={classnames('address-item-choose div-col', props.currentAddress?.id === d.id ? 'choosed' : '')}
            onClick={() => {
              props.onChangeAddress(d);
              setState({ isShowChangeAddress: false });
            }}
          >
            <div className={classnames('body-text-s-regular', isMobile ? 's-size' : 'm-size')}>
              <b>{d.name}</b>
            </div>
            <div className={classnames('body-text-s-regular', isMobile ? 's-size' : 'm-size')}>
              {`${d && d.street1 ? d.street1 : ''}${d && d.city ? ` ${d.city}` : ''}${d && d.state ? ` ${d.state}` : ''}${d && d.postal_code ? ` ${d.postal_code}` : ''}`}
            </div>
            <div className={classnames('body-text-s-regular', isMobile ? 's-size' : 'm-size')}>
              {d.phone}
            </div>
          </div>
        ))
      }
    </div>
  );
  const details = () => {
    const isSelfCollectCountry = auth.getCountry() && ['vn'].includes(auth.getCountry().toLowerCase());
    const shippingSelection = _.find(props.listShipping, x => (props.shipInfo && props.shipInfo?.country ? props.shipInfo?.country.toUpperCase() : '') === x.country.code.toUpperCase());
    const shippingCurrency = _.find(props.listShipping, x => (props.currentAddress && props.currentAddress?.country ? props.currentAddress?.country?.code.toUpperCase() : '') === x.country.code.toUpperCase());
    const isExpressCountry = shippingSelection ? shippingSelection.is_express : false;
    const isFreeDelivery = props.basket && shippingSelection ? parseFloat(shippingSelection.threshold) < parseFloat(props.basket?.subtotal) : false;
    if (props.shipInfo?.country !== 'us') {
      props.shipInfo.state = '';
    }

    const shippingMethodyBt = getNameFromCommon(cmsCommon, 'shipping_method');
    const shippingAddressBt = getNameFromCommon(cmsCommon, 'SHIPPING_ADDRESS');
    const selfBt = getNameFromCommon(cmsCommon, 'Self_Collect');
    const pickupBt = getNameFromCommon(cmsCommon, 'Pick_up_your_perfume');
    const duxtonBt = getNameFromCommon(cmsCommon, '77_duxton');
    const standBt = getNameFromCommon(cmsCommon, 'Standard');
    const daysBt = getNameFromCommon(cmsCommon, '2-3_days');
    const freeShipping = getNameFromButtonBlock(props.buttonBlock, 'Free shipping for orders above');
    const expressBt = getNameFromButtonBlock(props.buttonBlock, 'Express');
    const samedayBt = getNameFromButtonBlock(props.buttonBlock, 'Same_day_delivery');
    const freeBt = getNameFromCommon(cmsCommon, 'FREE');
    const addressBt = getNameFromCommon(cmsCommon, 'Address');
    const zipCodeBt = getNameFromCommon(cmsCommon, 'ZIP_CODE');
    const countryBt = getNameFromCommon(cmsCommon, 'COUNTRY');
    const stateBt = getNameFromCommon(cmsCommon, 'State');
    const cityBt = getNameFromCommon(cmsCommon, 'city');
    const panCardBt = getNameFromCommon(cmsCommon, 'PAN/Aadhar Number');
    const panCardIndoBt = getNameFromCommon(cmsCommon, 'PAN/Aadhar Number Indonesia');
    const thisInfoBt = getNameFromButtonBlock(props.buttonBlock, 'This information will be used for custom clearance only');
    const thisInfoIndonesiaBt = getNameFromButtonBlock(props.buttonBlock, 'This information of Indonesia');
    const messageSABt = getNameFromCommon(cmsCommon, 'message_Saudi_Arabia');
    const saveAddressForFutureBt = getNameFromButtonBlock(props.buttonBlock, 'Save_address_for_future_transaction');
    const billingAddress = getNameFromButtonBlock(props.buttonBlock, 'Billing address');
    const defaultBt = getNameFromButtonBlock(props.buttonBlock, 'Default');
    const sameAsBt = getNameFromButtonBlock(props.buttonBlock, 'Same as shipping address');
    const newBt = getNameFromButtonBlock(props.buttonBlock, 'New');
    const addAnBt = getNameFromButtonBlock(props.buttonBlock, 'Add an alternative billing address');
    const newAddressBt = getNameFromButtonBlock(props.buttonBlock, 'New_Address');
    const addNewAddressBt = getNameFromButtonBlock(props.buttonBlock, 'Add_new_address_for_shipping');
    const changeBt = getNameFromButtonBlock(props.buttonBlock, 'Change');
    const selectShippingBt = getNameFromButtonBlock(props.buttonBlock, 'Select Shipping Address');
    const unitNumberBt = getNameFromButtonBlock(props.buttonBlock, 'Unit Number');
    const enterBlockBt = getNameFromButtonBlock(props.buttonBlock, 'Enter Block No./Floor No./Unit No');

    const desStandard = shippingSelection && parseFloat(shippingSelection?.threshold) > 0 && !isFreeDelivery ? `${freeShipping} ${generaCurrency(shippingSelection?.threshold)}` : undefined;

    const formAddresHtml = isAddMoreAddress => (
      <div className={isAddMoreAddress ? 'detail-customer-detail create-account' : 'w-100'}>
        <div className="line-2">
          <InputCT
            type="autocomplete"
            label={`${addressBt}*`}
            placeholder={addressBt}
            isMedium={isMobile}
            country={props.shipInfo?.country}
            value={props.shipInfo?.street1}
            className={classnames(isMobile ? 'medium' : '', props.listError.includes('street1') ? 'error' : '')}
            isSearchCity={false}
            name="street1"
            id="street1"
            onKeyDown={() => {}}
            onChangeInput={onChangeInputAutoComplete}
            onChange={onChangeAutoComplete}
            eventClear={clearAutoComplete}
            isError={props.listError.includes('street1')}
          />
        </div>
        <div className="line-1">
          <InputCT
            type="select-checkout"
            placeholder={countryBt}
            name="country"
            onChange={props.onChange}
            options={props.listShipping}
            label={`${countryBt}*`}
            value={props.shipInfo?.country}
            className={classnames(isMobile ? 'medium' : '')}
          />
          <InputCT
            type="text"
            label={`${cityBt}*`}
            placeholder={cityBt}
            value={props.shipInfo?.city}
            onChange={props.onChange}
            name="city"
            className={classnames(isMobile ? 'medium' : '', props.listError.includes('city') ? 'error' : '')}
          />
        </div>
        {
          props.shipInfo?.country !== 'sg' && (
            <div className="line-1">
              <InputCT
                type="text"
                placeholder="Enter additional address details. e.g: Block No./Floor No./Unit No."
                name="detail"
                onChange={props.onChange}
                label="Address Details"
                value={props.shipInfo?.detail}
                className={classnames('w-100', isMobile ? 'medium' : '')}
              />
            </div>
          )
        }

        <div className="line-1">
          {
            props.shipInfo?.country === 'sg' && (
              <InputCT
                type="text"
                placeholder={enterBlockBt}
                name="unit_number"
                value={props.shipInfo.unit_number}
                onChange={props.onChange}
                label={`${unitNumberBt}*`}
                className={classnames(isMobile ? 'medium' : '', props.listError.includes('unit_number') ? 'error' : '')}
              />
            )
          }
          {
            !['ae', 'sa'].includes(props.shipInfo?.country) && (
              <InputCT
                type="text"
                placeholder={zipCodeBt}
                name="postal_code"
                value={props.shipInfo.postal_code}
                onChange={props.onChange}
                label={`${zipCodeBt}*`}
                className={classnames(isMobile ? 'medium' : '', props.listError.includes('postal_code') ? 'error' : '', ['us', 'in', 'id', 'sa', 'sg'].includes(props.shipInfo?.country) ? '' : 'w-100')}
              />
            )
          }

          {
            props.shipInfo?.country === 'us'
              ? (
                <InputCT
                  type="text"
                  placeholder={`${stateBt}`}
                  name="state"
                  value={props.shipInfo?.state}
                  onChange={props.onChange}
                  label={stateBt}
                  className={classnames(isMobile ? 'medium' : '', props.listError.includes('state') ? 'error' : '')}
                />
              ) : ['in', 'id'].includes(props.shipInfo.country) ? (
                <React.Fragment>
                  <InputCT
                    type="text"
                    placeholder={`${props.shipInfo?.country === 'id' ? panCardIndoBt : panCardBt}`}
                    name="pan_card"
                    value={props.shipInfo?.pan_card}
                    onChange={props.onChange}
                    label={`${props.shipInfo?.country === 'id' ? panCardIndoBt : panCardBt}*`}
                    className={classnames(isMobile ? 'medium' : '', props.listError.includes('pan_card') ? 'error' : '')}
                  />
                  <button
                    style={{
                      position: 'absolute',
                      bottom: '18px',
                      right: '30px',
                    }}
                    id="id-pan-card"
                    className="button-bg__none bt-info"
                    type="button"
                  >
                    <img style={{ width: '15px' }} src={icInfo} alt="info" />
                  </button>
                  <UncontrolledPopover trigger="focus" placement="bottom" target="id-pan-card">
                    <PopoverBody className="popover-checkout">{props.shipInfo.country === 'id' ? thisInfoIndonesiaBt : thisInfoBt}</PopoverBody>
                  </UncontrolledPopover>
                </React.Fragment>
              ) : props.shipInfo?.country === 'sa' ? (
                <div className="text-info w-100">
                  <span className="info-sa">{messageSABt}</span>
                </div>
              ) : undefined
          }
        </div>
        {
          (state.isNewAddress || !state.isShowOptionAddress) && props.isLogined && (
            <CheckBoxCT
              name="create_address"
              checked={props.shipInfo?.create_address}
              onChange={props.onClickCreateAddress}
              label={saveAddressForFutureBt}
              className="checkbox-create-address medium"
            />
          )
        }
      </div>
    );

    return (
      <div className="detail-customer-detail">
        <div className={classnames('other-Tagline', isMobile ? 's-size' : '')}>
          {shippingMethodyBt}
        </div>
        {
          isSelfCollectCountry && (
          <ChooseOptionCheckOut
            title={selfBt}
            title1={`(${pickupBt})`}
            description={duxtonBt}
            checked={props.isSelfCollect}
            onClick={props.onClickSelfCollect}
            textRight={freeBt}
          />
          )
        }
        <ChooseOptionCheckOut
          title={standBt}
          title1={`(${daysBt})`}
          description={desStandard}
          textRight={props.isCheckOutClub || props.isCheckOutGift ? '' : isFreeDelivery ? generaCurrency('0.00') : generaCurrency(shippingSelection ? shippingSelection.shipping : '')}
          checked={!props.isSelfCollect && !props.isExpress}
          onClick={props.onClickStandard}
        />
        {
          isExpressCountry && (
            <ChooseOptionCheckOut
              title={expressBt}
              description={samedayBt}
              textRight={generaCurrency(shippingSelection ? shippingSelection.shippingExpress : '')}
              checked={!props.isSelfCollect && props.isExpress}
              onClick={props.onClickExpress}
              isAccordion
              expanded={!props.isSelfCollect && props.isExpress}
              details={chooseTimeSlot()}
            />
          )
        }
        {
          !props.isSelfCollect && (
            <React.Fragment>
              <div className={classnames('other-Tagline', isMobile ? 's-size' : '')}>
                {shippingAddressBt}
              </div>
              {
                state.isShowOptionAddress && (
                  <React.Fragment>
                    <ChooseOptionCheckOut
                      title={state.isShowChangeAddress ? selectShippingBt : props.currentAddress?.name}
                      description={state.isShowChangeAddress ? undefined : (`${props.currentAddress && props.currentAddress.street1 ? props.currentAddress.street1 : ''}${props.currentAddress && props.currentAddress.city ? ` ${props.currentAddress.city}` : ''}${props.currentAddress && props.currentAddress.state ? ` ${props.currentAddress.state}` : ''}${shippingCurrency ? ` ${shippingCurrency.name}` : ''}${props.currentAddress && props.currentAddress.postal_code ? ` ${props.currentAddress.postal_code}` : ''}`)}
                      checked={!state.isNewAddress}
                      onClick={() => {
                        setState({ isNewAddress: false });
                        props.onChooseNewAddress(false);
                      }}
                      buttonRight={props.listAddress?.length > 1 ? changeBt : undefined}
                      onClickRight={() => setState({ isShowChangeAddress: true })}
                      isAccordion={props.listAddress?.length > 1}
                      expanded={state.isShowChangeAddress}
                      details={changeAddressHtml()}
                    />
                    <ChooseOptionCheckOut
                      title={newAddressBt}
                      description={addNewAddressBt}
                      checked={state.isNewAddress}
                      onClick={() => {
                        setState({ isNewAddress: true });
                        props.onChooseNewAddress(true);
                      }}
                      isAccordion
                      expanded={state.isNewAddress}
                      details={formAddresHtml(true)}
                    />
                  </React.Fragment>
                )
              }
              {
                (!state.isShowOptionAddress) && (
                  formAddresHtml(false)
                )
              }

            </React.Fragment>
          )
        }

        <div className={classnames('other-Tagline', isMobile ? 's-size' : '')}>
          {billingAddress}
        </div>

        <ChooseOptionCheckOut
          title={defaultBt}
          description={sameAsBt}
          checked={props.isBillAddressSame}
          onClick={() => props.onClickCheckBillAddress(true)}
        />
        <ChooseOptionCheckOut
          title={newBt}
          description={addAnBt}
          checked={!props.isBillAddressSame}
          onClick={() => props.onClickCheckBillAddress(false)}
          expanded={!props.isBillAddressSame}
          isAccordion
          details={detailCreateAccount()}
        />
        <div className="div-button-next">
          {
          !isMobile && (
            <div />
          )
        }

          <ButtonCT
            className="bt-next"
            name="Next"
            onClick={props.onClickContinute}
          />
        </div>

      </div>
    );
  };

  return (
    <div className="shipping-info">
      <div id="anchor-step2" />
      <DropdownCheckout
        summary={summary()}
        details={details()}
        expanded={props.stepCheckOut === 2}
        onChange={() => {}}
      />
    </div>
  );
}

export default ShippingInfo;
