import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import EmailIcon from '@mui/icons-material/Email';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import classnames from 'classnames';
import React, { useEffect, useRef } from 'react';
import 'react-phone-number-input/style.css';
import { useHistory } from 'react-router-dom';
import ButtonCT from '../../../../componentsv2/buttonCT';
import CheckBoxCT from '../../../../componentsv2/checkBoxCT';
import DropdownCheckout from '../../../../componentsv2/dropdownCheckout';
import InputCT from '../../../../componentsv2/inputCT';
import NumberHeader from '../../../../componentsv2/numberHeader';
import { isBrowser, isMobile } from '../../../../DetectScreen/detectIFrame';
import {
  generateUrlWeb, getCmsCommon, getNameFromButtonBlock, getNameFromCommon, getSearchPathName, trackGTMCheckOut,
} from '../../../../Redux/Helpers';
import '../styles.scss';

function CustomerDetail(props) {
  const getStyleIcon = value => (value ? { color: '#2c2c2c' } : { color: '#8D826E' });
  const history = useHistory();
  const isSentSegmet = useRef(false);

  const onOpenLogin = () => {
    const { pathname, search } = getSearchPathName(generateUrlWeb('/login'));
    history.push({
      pathname,
      search,
      state: { isCheckout: true },
    });
  };

  useEffect(() => {
    trackGTMCheckOut(2, props.basket);
  }, []);

  useEffect(() => {
    if (props.basket?.id && isSentSegmet.current === false) {
      isSentSegmet.current = true;
    }
  }, [props.basket]);

  const cmsCommon = getCmsCommon(props.cms);
  const alreadyBt = getNameFromCommon(cmsCommon, 'Already_have_an_account');
  const loginBt = getNameFromCommon(cmsCommon, 'Log_in');
  console.log('props.shipInfo', props.shipInfo);
  const accountLogin = () => (
    <div className="account-login">
      <div className="body-text-s-regular">
        {alreadyBt}
      </div>
      <button
        className="button-bg__none"
        type="button"
        onClick={onOpenLogin}
      >
        {loginBt}
      </button>
    </div>
  );

  const summary = () => {
    const isLogined = props.login?.user?.id;
    return (
      <div className="header-customer-detail">
        <NumberHeader number={1} text="Customer details" isHeader />
        {
          isBrowser && !isLogined && accountLogin()
        }
      </div>
    );
  };

  const details = () => {
    const isLogined = props.login?.user?.id;
    const firstNameBt = getNameFromCommon(cmsCommon, 'FIRST_NAME');
    const lastNameBt = getNameFromCommon(cmsCommon, 'LAST_NAME');
    const emailBt = getNameFromCommon(cmsCommon, 'Email');
    const keepBt = getNameFromButtonBlock(props.buttonBlock, 'Keep me up to date on news and offers');
    const phoneBt = getNameFromCommon(cmsCommon, 'PHONE');
    const birthdateBt = getNameFromCommon(cmsCommon, 'Birth_Date');
    const birthdateLabelBt = getNameFromCommon(cmsCommon, 'Birth_Date_Label');
    const weLoveBt = getNameFromCommon(cmsCommon, 'We’d love to surprise you on your Birthday');
    const optionalBt = getNameFromCommon(cmsCommon, 'Optional');

    return (
      <div className="detail-customer-detail">
        {
        !isBrowser && !isLogined && accountLogin()
      }
        <div className="line-1">
          <InputCT
            label={`${firstNameBt}*`}
            type="text"
            placeholder={firstNameBt}
            name="firstName"
            value={props.shipInfo?.firstName}
            onChange={props.onChange}
            iconLeft={<PersonOutlineIcon style={getStyleIcon(props.shipInfo?.firstName)} />}
            className={classnames(isMobile ? 'medium' : '', props.listError.includes('firstName') ? 'error' : '')}
          />
          <InputCT
            label={`${lastNameBt}*`}
            type="text"
            placeholder={lastNameBt}
            iconLeft={<PersonOutlineIcon style={getStyleIcon(props.shipInfo?.lastName)} />}
            name="lastName"
            value={props.shipInfo?.lastName}
            className={classnames(isMobile ? 'medium' : '', props.listError.includes('lastName') ? 'error' : '')}
            onChange={props.onChange}
          />
        </div>
        <div className="line-2">
          <InputCT
            label={`${emailBt}*`}
            type="text"
            placeholder={emailBt}
            iconLeft={<EmailIcon style={getStyleIcon(props.shipInfo?.email)} />}
            name="email"
            value={props.shipInfo?.email}
            className={classnames(isMobile ? 'medium' : '', props.listError.includes('email') ? 'error' : '')}
            onChange={props.onChange}
          />
          <CheckBoxCT
          // checked={isChecked(objectFilter, d)}
            checked={props.shipInfo?.is_get_newsletters}
            className={classnames(isMobile ? 'medium' : '', 'check-keep-me')}
            onChange={() => props.onChange({ target: { name: 'is_get_newsletters', value: !props.shipInfo?.is_get_newsletters } })}
            label={(
              <div>
                {keepBt}
              </div>
          )}
          />
        </div>
        <div className="line-3">
          <InputCT
            label={`${phoneBt}*`}
            type="phone"
            className={classnames(isMobile ? 'medium' : '', props.listError.includes('phone') ? 'error' : '')}
            placeholder={phoneBt}
            value={props.shipInfo?.phone}
            name="phone"
            onChange={value => props.onChange({ target: { name: 'phone', value } })}
          />
          <div className="input-phone">
            <InputCT
              label={birthdateBt}
              type="dob"
              iconLeft={<CalendarTodayIcon style={getStyleIcon(props.shipInfo?.dob)} />}
              className={classnames(isMobile ? 'medium' : '', props.listError.includes('dob') ? 'error' : '')}
              placeholder={`${birthdateLabelBt} (MM/DD/YYYY)`}
              defaultValue={props.shipInfo?.dob}
              value={props.shipInfo?.dob}
              name="phone"
              onChange={props.onChange}
            />
            <span className="des-birth">{weLoveBt}</span>
            <span className={props.isCheckOutClub ? 'hidden' : 'option-birth'}>{`(${optionalBt})`}</span>
          </div>
        </div>
        <div className="div-button-next">
          {isBrowser && <div />}
          <ButtonCT
            className="bt-next medium"
            name="Next"
            onClick={props.onClickContinute}
          />
        </div>

      </div>
    );
  };

  return (
    <div className="customer-detail">
      <div id="anchor-step1" />
      <DropdownCheckout
        summary={summary()}
        details={details()}
        expanded={props.stepCheckOut === 1}
        onChange={() => {}}
      />
    </div>
  );
}

export default CustomerDetail;
