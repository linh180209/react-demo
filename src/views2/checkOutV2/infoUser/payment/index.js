import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect, useRef } from 'react';
import ButtonCT from '../../../../componentsv2/buttonCT';
import ChooseOptionCheckOut from '../../../../componentsv2/chooseOptionCheckout';
import DropdownCheckout from '../../../../componentsv2/dropdownCheckout';
import NumberHeader from '../../../../componentsv2/numberHeader';
import { isMobile } from '../../../../DetectScreen/detectIFrame';
import {
  amexCo, mcCo, paypalCo, visaCo,
} from '../../../../imagev2/svg';
import {
  getAltImageV2, getCmsCommon, getNameFromButtonBlock, getNameFromCommon,
} from '../../../../Redux/Helpers';
import auth from '../../../../Redux/Helpers/auth';
import CheckoutMethod from '../checkoutMethod';

function Payment(props) {
  const isSentSegmet = useRef(false);

  useEffect(() => {
    if (props.basket?.id && isSentSegmet.current === false) {
      isSentSegmet.current = true;
    }
  }, [props.basket]);

  const cmsCommon = getCmsCommon(props.cms);
  const creditBt = getNameFromCommon(cmsCommon, 'Credit/Debit_card');
  const payLateBt = getNameFromCommon(cmsCommon, 'Pay_Later');
  const payPalBt = getNameFromButtonBlock(props.buttonBlock, 'PayPal');
  const paymentBt = getNameFromCommon(cmsCommon, 'PAYMENT');

  const zaloPayBt = getNameFromCommon(cmsCommon, 'ZALO_PAY');
  const momoBt = getNameFromCommon(cmsCommon, 'MOMO');
  const bankTransferBt = getNameFromCommon(cmsCommon, 'BANK_TRANSFER');
  const atmCardBt = getNameFromCommon(cmsCommon, 'ATM_CARD');
  const intCardBt = getNameFromCommon(cmsCommon, 'INT_CARD');
  const codBt = getNameFromCommon(cmsCommon, 'COD');
  const zaloPayNoteBt = getNameFromCommon(cmsCommon, 'ZALO_PAY_NOTE');
  const momoNoteBt = getNameFromCommon(cmsCommon, 'MOMO_NOTE');
  const bankTransferNoteBt = getNameFromCommon(cmsCommon, 'BANK_TRANSFER_NOTE');
  const atmCardNoteBt = getNameFromCommon(cmsCommon, 'ATM_CARD_NOTE');
  const intCardNoteBt = getNameFromCommon(cmsCommon, 'INT_CARD_NOTE');
  const codNoteBt = getNameFromCommon(cmsCommon, 'COD_NOTE');

  const youWillPaypal = getNameFromButtonBlock(props.buttonBlock, 'You will PayPal');
  const youWillStripe = getNameFromButtonBlock(props.buttonBlock, 'You will Stripe');

  const icZaloPay = _.filter(props.imageBlockIcon, x => x.value.caption.toLowerCase() === 'zalo_pay');
  const icMomo = _.filter(props.imageBlockIcon, x => x.value.caption.toLowerCase() === 'momo');
  const icBankTransfer = _.filter(props.imageBlockIcon, x => x.value.caption.toLowerCase() === 'bank_transfer');
  const icAtmCard = _.filter(props.imageBlockIcon, x => x.value.caption.toLowerCase() === 'atm_card');
  const icIntCard = _.filter(props.imageBlockIcon, x => x.value.caption.toLowerCase() === 'int_card');
  const icCod = _.filter(props.imageBlockIcon, x => x.value.caption.toLowerCase() === 'cod');

  const isVietNam = props.shipInfo?.country && props.shipInfo?.country.toLowerCase() === 'vn';

  const summary = () => (
    <div className="header-customer-detail">
      <NumberHeader number={props.isOnlyGift ? 2 : 3} text={paymentBt} isHeader />
    </div>
  );

  const paymentVN = () => (
    <React.Fragment>
      {
        zaloPayBt && (
          <ChooseOptionCheckOut
            icons={_.map(icZaloPay, x => x.value.image)}
            text={zaloPayBt}
            className="check-payment"
            checked={props.payMethod === 'zalopay'}
            onClick={props.onClickZaloPay}
          />
        )
      }
      {
        props.payMethod === 'zalopay' && zaloPayNoteBt && (
          <div className="text-order body-text-s-regular">
            {zaloPayNoteBt}
          </div>
        )
      }

      {
        zaloPayBt && (
          <ChooseOptionCheckOut
            icons={_.map(icMomo, x => x.value.image)}
            text={momoBt}
            className="check-payment"
            checked={props.payMethod === 'momo'}
            onClick={props.onClickMomo}
          />
        )
      }
      {
        props.payMethod === 'momo' && momoNoteBt && (
          <div className="text-order body-text-s-regular">
            {momoNoteBt}
          </div>
        )
      }

      {
        bankTransferBt && (
          <ChooseOptionCheckOut
            icons={_.map(icBankTransfer, x => x.value.image)}
            text={bankTransferBt}
            className="check-payment"
            checked={props.payMethod === 'bank_transfer'}
            onClick={props.onClickBankTransfer}
          />
        )
      }
      {
        props.payMethod === 'bank_transfer' && bankTransferNoteBt && (
          <div className="text-order body-text-s-regular">
            {bankTransferNoteBt}
          </div>
        )
      }

      {
        atmCardBt && (
          <ChooseOptionCheckOut
            icons={_.map(icAtmCard, x => x.value.image)}
            text={atmCardBt}
            className="check-payment"
            checked={props.payMethod === 'atm_card'}
            onClick={props.onClickAtmCard}
          />
        )
      }
      {
        props.payMethod === 'atm_card' && atmCardNoteBt && (
          <div className="text-order body-text-s-regular">
            {atmCardNoteBt}
          </div>
        )
      }

      {
        intCardBt && (
          <ChooseOptionCheckOut
            icons={_.map(icIntCard, x => x.value.image)}
            text={intCardBt}
            className="check-payment"
            checked={props.payMethod === 'int_card'}
            onClick={props.onClickIntCard}
          />
        )
      }
      {
        props.payMethod === 'int_card' && intCardNoteBt && (
          <div className="text-order body-text-s-regular">
            {intCardNoteBt}
          </div>
        )
      }

      {
        codBt && (
          <ChooseOptionCheckOut
            icons={_.map(icCod, x => x.value.image)}
            text={codBt}
            className="check-payment"
            checked={props.payMethod === 'cod'}
            onClick={props.onClickCod}
          />
        )
      }
      {
        props.payMethod === 'cod' && codNoteBt && (
          <div className="text-order body-text-s-regular">
            {codNoteBt}
          </div>
        )
      }
    </React.Fragment>
  );

  const isPaypalAvaiable = auth.getCountry() !== 'ae';
  const isPayLaterAvaiable = auth.getCountry() !== 'ae';

  const paymentMain = () => (
    <React.Fragment>
      {
        auth.getCountry() === 'ae' ? (
          <ChooseOptionCheckOut
            className="check-payment"
            checked={props.payMethod === 'checkout.com'}
            isCheckOutMethod
            onClick={props.onClickCheckout}
            updateValidateCheckoutCom={props.updateValidateCheckoutCom}
            handleTokenCheckoutCom={props.handleTokenCheckoutCom}
          />
        ) : (
          <React.Fragment>
            <ChooseOptionCheckOut
              icons={[visaCo, mcCo, amexCo]}
              text={creditBt}
              className="check-payment"
              checked={props.payMethod === 'stripe'}
              onClick={props.onClickStripe}
            />
            {
              props.payMethod === 'stripe' && (
                <div className="text-order body-text-s-regular">
                  {youWillStripe}
                </div>
              )
            }
          </React.Fragment>
        )
      }
      {
        isPaypalAvaiable && (
          <React.Fragment>
            <ChooseOptionCheckOut
              icons={[paypalCo]}
              text={payPalBt}
              className="check-payment"
              checked={props.payMethod === 'paypal'}
              onClick={props.onClickPaypal}
            />
            {
            props.payMethod === 'paypal' && (
              <div className="text-order body-text-s-regular">
                {youWillPaypal}
              </div>
            )
          }
          </React.Fragment>
        )
      }

      {
        isPayLaterAvaiable && (
          <React.Fragment>
            <ChooseOptionCheckOut
              icons={[visaCo, mcCo, amexCo]}
              text={payLateBt}
              className="check-payment"
              checked={props.payMethod === 'stripe_pay_later'}
              onClick={props.onClickPaypalLate}
            />
            {
              props.payMethod === 'stripe_pay_later' && (
                <div className="text-order">
                  <div className="icons-list div-row">
                    {
                      _.map(props.iconBlockScent?.icons, d => (
                        <div className="div-line div-row">
                          <img src={d.value?.image?.image} alt={getAltImageV2(d?.value?.image)} />
                          <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
                            <b>{d.value?.text}</b>
                          </div>
                        </div>
                      ))
                    }
                  </div>
                  <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
                    {props.iconBlockScent?.opacity}
                  </div>
                </div>
              )
            }
          </React.Fragment>
        )
      }
    </React.Fragment>
  );

  const details = () => (
    <div className="detail-payment">
      {isVietNam ? paymentVN() : paymentMain()}
      <div className="div-button-next">
        {
          !isMobile && (
            <div />
          )
        }

        <ButtonCT
          className="bt-next"
          name="Next"
          onClick={props.onClickContinute}
        />
      </div>

    </div>
  );
  return (
    <div className={classnames('payment-info', props.isStepDone ? 'hidden' : '')}>
      <div id="anchor-step3" />
      <DropdownCheckout
        summary={summary()}
        details={details()}
        expanded={props.stepCheckOut === 3}
        onChange={() => {}}
      />
    </div>
  );
}

export default Payment;
