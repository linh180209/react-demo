import React, { useRef, useEffect } from 'react';
import { KEY_CHECK_OUT } from '../../../../config';
import { toastrError } from '../../../../Redux/Helpers/notification';
import './styles.scss';

const styleInput = {
  base: {
    color: '#2C2C22',
    fontSize: '16px',
  },
  autofill: {
    backgroundColor: 'yellow',
  },
  valid: {
    color: '#2C2C22',
  },
  invalid: {
    color: 'red',
  },
  placeholder: {
    base: {
      color: '#8D826E',
      fontSize: '14px',
    },
  },
};

function CheckoutMethod(props) {
  const validFiled = useRef({
    'card-number': false,
    'expiry-date': false,
    cvv: false,
  });

  const { Frames } = window;
  const onCardValidationChanged = (event) => {
    console.log('CARD_VALIDATION_CHANGED: %o', event, Frames.isCardValid());
    // payButton.disabled = !Frames.isCardValid();
  };

  const onValidationChanged = (event) => {
    const hasError = !event.isValid && !event.isEmpty;
    console.log('FRAME_VALIDATION_CHANGED: %o', event, hasError);
    validFiled.current[event?.element] = event?.isValid;
    props.updateValidateCheckoutCom(validFiled.current['card-number'] && validFiled.current['expiry-date'] && validFiled.current.cvv);
  };

  const onCardTokenizationFailed = (error) => {
    console.log('CARD_TOKENIZATION_FAILED: %o', error);
    Frames.enableSubmitForm();
    toastrError(error);
  };

  const onCardTokenized = (event) => {
    console.log('onCardTokenized event.token', event.token);
    props.handleTokenCheckoutCom(event.token);
  };

  const initalFnc = () => {
    Frames.init({
      publicKey: KEY_CHECK_OUT,
      style: styleInput,
      // localization: 'AR',
    });
    Frames.addEventHandler(Frames.Events.CARD_VALIDATION_CHANGED, onCardValidationChanged);
    Frames.addEventHandler(Frames.Events.FRAME_VALIDATION_CHANGED, onValidationChanged);
    Frames.addEventHandler(Frames.Events.CARD_TOKENIZATION_FAILED, onCardTokenizationFailed);
    Frames.addEventHandler(Frames.Events.CARD_TOKENIZED, onCardTokenized);
  };

  useEffect(() => {
    initalFnc();
  }, []);

  return (
    <div className="checkout-method">
      <div className="card-frame" />
    </div>
  );
}

export default CheckoutMethod;
