import classnames from 'classnames';
import React from 'react';
import NumberHeader from '../../../../componentsv2/numberHeader';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import './styles.scss';

function StepCheckOut(props) {
  const detailsBt = getNameFromButtonBlock(props.buttonBlocks, 'Details');
  const shippingBt = getNameFromButtonBlock(props.buttonBlocks, 'Shipping');
  const paymentBt = getNameFromButtonBlock(props.buttonBlocks, 'Payment');

  return (
    <div className={classnames('step-check-out div-row', props.isOnlyGift ? 'only-gift' : '')}>
      <div className={classnames('div-step-item', props.stepCheckOut < 1 ? 'disabled' : '')} onClick={props.onClickEditStep1}>
        <NumberHeader number={1} text={detailsBt} isDisabled={props.stepCheckOut < 1} />
      </div>
      {
        !props.isOnlyGift && (
          <div className={classnames('div-step-item', props.stepCheckOut < 2 ? 'disabled' : '')} onClick={props.onClickEditStep2}>
            <NumberHeader number={2} text={shippingBt} isDisabled={props.stepCheckOut < 2} />
          </div>
        )
      }
      <div className={classnames('div-step-item', props.stepCheckOut < 3 ? 'disabled' : '')} onClick={props.onClickEditStep3}>
        <NumberHeader number={props.isOnlyGift ? 2 : 3} text={paymentBt} isDisabled={props.stepCheckOut < 3} />
      </div>
    </div>
  );
}

export default StepCheckOut;
