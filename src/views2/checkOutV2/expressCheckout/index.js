import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import classnames from 'classnames';
import React, { useState } from 'react';
import { isSafari } from 'react-device-detect';
import HeaderCT from '../../../componentsv2/headerCT';
import { KEY_STRIPE_APPLE, POST_CHECK_OUT_EXPRESS } from '../../../config';
import { isMobile } from '../../../DetectScreen';
import { paypalBlack } from '../../../imagev2/svg';
import { getNameFromButtonBlock, segmentExpressCheckoutStarted } from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import CheckoutForm from '../../../views/CheckOutB2C/CheckOutExpress/checkoutForm';
import './styles.scss';

const stripePromise = loadStripe(KEY_STRIPE_APPLE);

function ExpressCheckout(props) {
  const [isShowApplePay, setIsShowApplePay] = useState(false);
  const currentBasket = props.basket;

  const updateIsShowAppleExpress = (isShow) => {
    setIsShowApplePay(isShow);
  };

  const onClickPaypal = () => {
    const option = {
      url: POST_CHECK_OUT_EXPRESS.replace('{id}', props.basket?.id),
      method: 'POST',
      body: {
        payment_method: 'paypal',
        paypal_express: true,
        promos: props.promotion?.id ? [props.promotion?.id] : undefined,
      },
    };
    props.loadingPage(true);
    segmentExpressCheckoutStarted(currentBasket);
    fetchClient(option).then((result) => {
      if (result.url) {
        window.location.href = result.url;
      }
      props.loadingPage(false);
    }).catch((error) => {
      toastrError(error.message);
      props.loadingPage(false);
    });
  };

  const expressBt = getNameFromButtonBlock(props.buttonBlock, 'Express Checkout');
  return (
    <div className="express-checkout-v2">
      <div className={classnames('express-only-paypal', isShowApplePay ? 'show-apple' : '')}>
        <div className={isShowApplePay ? 'express-header div-col' : 'hidden'}>
          {expressBt}
        </div>
        <div className="div-list-button">
          {
            isShowApplePay ? (
              <button type="button" className="button-paypal" onClick={onClickPaypal}>
                <img src={paypalBlack} alt="paypal" />
              </button>
            ) : (
              <button className="bt-paypal-only" type="button" data-gtmtracking="checkout-paypal-express" onClick={onClickPaypal}>
                <HeaderCT type={isMobile ? 'Heading-XXS' : 'Heading-S'}>
                  {expressBt}
                </HeaderCT>
                <div className="div-image div-row">
                  <img src={paypalBlack} alt="paypal" />
                  <ArrowForwardIosIcon style={{ color: '#2C2C22' }} />
                </div>
              </button>
            )
          }
          {
          ['sg'].includes(auth.getCountry()) && isSafari && props.basket?.total && parseFloat(props.basket?.total, 10) > 0 && (
            <div className={isShowApplePay ? 'div-button-apple' : 'hidden'}>
              <Elements stripe={stripePromise}>
                <CheckoutForm
                  basket={props.basket}
                  createAddressFromApplePay={props.createAddressFromApplePay}
                  listShipping={props.listShipping}
                  preCheckOutApplePay={props.preCheckOutApplePay}
                  updateIsShowAppleExpress={updateIsShowAppleExpress}
                  isCheckOutV2
                />
              </Elements>
            </div>
          )
          }
        </div>
      </div>

    </div>
  );
}

export default ExpressCheckout;
