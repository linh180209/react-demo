import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import HeaderCT from '../../../componentsv2/headerCT';
import InputCT from '../../../componentsv2/inputCT';
import { GET_USER_ACTIONS } from '../../../config';
import { isBrowser } from '../../../DetectScreen';
import { dola, logoBlack } from '../../../imagev2/svg';
import {
  generaCurrency, generateUrlWeb, getCmsCommon, getNameFromButtonBlock, getNameFromCommon, segmentCheckoutStepViewed,
} from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { useMergeState } from '../../../Utils/customHooks';
import CardRedeem from '../../cardPage/cardRedeem';
import './styles.scss';

function CardSum(props) {
  const [noteText, setNoteText] = useState(props.noteText);
  const basket = useSelector(state => state.basket);
  const shippings = useSelector(state => state.shippings);
  const [state, setState] = useMergeState({
    shippingInfo: undefined,
    pointUser: 1,
    spendUser: 1,
  });
  const isSentSegment = useRef(false);

  const handleShowPoint = () => {
    const options = {
      url: GET_USER_ACTIONS,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        const ele = _.find(result, x => x.type === 'buy');
        if (ele) {
          setState({ pointUser: ele?.point, spendUser: ele?.spend });
        }
      }
    }).catch((err) => {
      console.log(err.mesage);
    });
  };

  const onChangeTextNote = (e) => {
    const { value } = e.target;
    setNoteText(value);
    props.changeNote(value);
  };

  const gotoTermConditions = () => {
    if (auth.getCountry() === 'vn') {
      window.open(generateUrlWeb('/shipping-infomation'));
    } else {
      window.open(generateUrlWeb('/terms-conditions'));
    }
  };

  useEffect(() => {
    setNoteText(props.noteText);
  }, [props.noteText]);

  useEffect(() => {
    if (props.isCheckOutPage && basket?.subtotal && !isSentSegment.current) {
      isSentSegment.current = true;
      // const newPrice = parseFloat(basket?.discount) > 0 ? parseFloat(basket?.subtotal) - parseFloat(basket?.discount) : -1;
      // const finalPrice = newPrice === -1 ? parseFloat(basket?.subtotal) : newPrice;
      // const shippingValue = state.shippingInfo && parseFloat(state.shippingInfo?.threshold) > 0 && (parseFloat(state.shippingInfo?.threshold) < parseFloat(finalPrice)) ? 0 : state.shippingInfo?.shipping;
    }
  }, [state.shippingInfo]);

  useEffect(() => {
    handleShowPoint();
    segmentCheckoutStepViewed(basket);
  }, []);

  useEffect(() => {
    if (shippings.length > 0) {
      const shippingInfo = _.find(shippings, d => d.country.code === props.shipInfo?.country?.toLowerCase());
      setState({ shippingInfo });
    }
  }, [shippings, props.shipInfo, basket]);

  const cmsCommon = getCmsCommon(props.cms);
  const subTotalBt = getNameFromCommon(cmsCommon, 'Subtotal');
  const discountBt = getNameFromCommon(cmsCommon, 'Discount');
  const shippingBt = getNameFromCommon(cmsCommon, 'Shipping');
  const orderSummaryBt = getNameFromCommon(cmsCommon, 'ORDER_SUMMARY');
  const receiveGramsLeft = getNameFromButtonBlock(props.buttonBlocks, 'receive_grams_left');
  const receiveGramsRight = getNameFromButtonBlock(props.buttonBlocks, 'receive_grams_right');
  const pointsEarnded = getNameFromButtonBlock(props.buttonBlocks, 'Points Earnded');
  const noteBt = getNameFromButtonBlock(props.buttonBlocks, 'Note');
  const redeemVoucherBt = getNameFromButtonBlock(props.buttonBlocks, 'REDEEM VOUCHER');
  const applyYourBt = getNameFromButtonBlock(props.buttonBlocks, 'Apply your discount voucher here');
  const totalBt = getNameFromButtonBlock(props.buttonBlocks, 'Total');
  const placeNote = getNameFromButtonBlock(props.buttonBlocks, 'Add a note for our team...');
  const getNowPayLater = getNameFromButtonBlock(props.buttonBlocks, 'Get_Now_Pay_Later');

  if (!basket) {
    return null;
  }
  const shippingInfo = _.find(shippings, d => d.country.code === props.shipInfo?.country?.toLowerCase());
  const newPrice = parseFloat(basket?.discount) > 0 ? parseFloat(basket?.subtotal) - parseFloat(basket?.discount) : -1;
  const finalPrice = newPrice === -1 ? parseFloat(basket?.subtotal) : newPrice;
  const shippingValue = shippingInfo && parseFloat(shippingInfo?.threshold) > 0 && (parseFloat(shippingInfo?.threshold) < parseFloat(finalPrice)) ? 0 : shippingInfo?.shipping;
  const point = finalPrice ? parseInt(finalPrice * state.pointUser / state.spendUser, 10) : 0;
  const subTotalHtml = (
    <div className="line-1 div-row">
      <span>
        {subTotalBt}
      </span>
      <span>
        {generaCurrency(basket?.subtotal)}
      </span>
    </div>
  );

  const discountHtml = (
    <div className="line-1 div-row">
      <span>
        {discountBt}
      </span>
      <span>
        {parseFloat(basket?.discount, 10) !== 0 ? '-' : ''}
        {' '}
        {generaCurrency(basket?.discount)}
      </span>
    </div>
  );

  const shippingHtml = (
    <div className="line-1 div-row">
      <span>
        {shippingBt}
      </span>
      <span>
        {generaCurrency(shippingValue, true)}
      </span>
    </div>
  );

  const totalHtml = (
    <div className="line-2 div-row">
      <span>
        {totalBt}
      </span>
      <span>
        {generaCurrency(basket?.total)}
      </span>
    </div>
  );

  const pointCardHtml = (
    <div className="div-point-card">
      {
        isBrowser ? (
          <div className="line-3 div-row">
            <img src={logoBlack} alt="icon" />
            <span>
              {receiveGramsLeft}
              {' '}
              <b>
                {point > 0 ? point : 0}
                G
              </b>
              {' '}
              {receiveGramsRight}
            </span>
          </div>
        ) : (
          <div className="line-3 div-row">
            <span>
              {pointsEarnded}
            </span>
            <div className="div-point-line">
              <img src={logoBlack} alt="icon" />
              {point}
              G
            </div>
          </div>
        )
      }
    </div>
  );

  const noteHtml = (
    <div className={classnames('div-note-html div-col', auth.getCountry() === 'ae' ? 'hidden' : '')}>
      <div>
        {noteBt}
        :
      </div>
      <InputCT
        type="text"
        placeholder={placeNote}
        value={noteText}
        onChange={onChangeTextNote}
      />
    </div>
  );

  const conditionHtml = (
    <div className="line-5 div-row" onClick={gotoTermConditions}>
      <img src={dola} alt="icon" />
      <span>{getNowPayLater}</span>
    </div>
  );

  const textTax = (
    <div className="text-tax">
      Including 5% VAT
    </div>
  );

  return (
    <div className={classnames('card-sum', props.className)}>
      {
        isBrowser && (
          <React.Fragment>
            <HeaderCT type="Heading-S">
              {redeemVoucherBt}
            </HeaderCT>
            <div className="body-text-s-regular sub-title">
              {applyYourBt}
            </div>
          </React.Fragment>
        )
      }
      <CardRedeem buttonBlocks={props.buttonBlocks} isCheckOutPage />
      {
        !isBrowser && (
        <HeaderCT type="Heading-XXS">
          {orderSummaryBt}
        </HeaderCT>
        )
      }
      {subTotalHtml}
      {shippingHtml}
      {discountHtml}
      {!isBrowser && pointCardHtml}
      <hr />
      {totalHtml}
      {auth.getCountry() === 'ae' && textTax}
      {isBrowser && pointCardHtml}
      {auth.getCountry() !== 'ae' && !isBrowser && conditionHtml}
      {noteHtml}
    </div>
  );
}

export default CardSum;
