import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import CardItemV2 from '../../../componentsv2/cardItemV2';
import HeaderCT from '../../../componentsv2/headerCT';
import CardSum from './cardSum';
import { isMobile } from '../../../DetectScreen';
import SwipeableDrawerMobile from '../../../componentsv2/swipeableDrawerMobile';
import { bottleCheckOut } from '../../../imagev2/svg';
import { getNameFromButtonBlock, generaCurrency } from '../../../Redux/Helpers';

function ShoppingBag(props) {
  const dispatch = useDispatch();

  const basket = useSelector(state => state.basket);
  const cms = useSelector(state => state.cms);
  const countries = useSelector(state => state.countries);
  const [open, setOpen] = useState(false);

  const toggleDrawer = (flags) => {
    setOpen(flags);
  };

  const shoppingBag = getNameFromButtonBlock(props.buttonBlock, 'Shopping bag');
  const showDetails = getNameFromButtonBlock(props.buttonBlock, 'Show Detail');
  const ITEMSbt = getNameFromButtonBlock(props.buttonBlock, 'ITEMS');
  const miniHtml = () => (
    <div className="mini-html-checkout-mobile div-row">
      <div className="div-left div-row">
        <img src={bottleCheckOut} alt="icon" />
        <div className="div-text div-col">
          <div className="body-text-s-regular l-size">
            <b>
              (
              {basket?.items?.length}
              )
              {' '}
              {ITEMSbt}
            </b>
          </div>
          <div className="div-line body-text-s-regular div-row">
            {showDetails}
            <ArrowForwardIosIcon style={{ color: '#56564E' }} />
          </div>
        </div>
      </div>
      <div className="body-text-s-regular l-size">
        <b>{generaCurrency(basket?.total)}</b>
      </div>
    </div>
  );
  const fullHtml = () => (
    <div className="full-html-checkout-mobile div-col">
      <div className="div-header div-row" onClick={() => setOpen(false)}>
        <ArrowBackIosNewIcon />
        <HeaderCT type="Heading-S">
          {shoppingBag}
        </HeaderCT>
      </div>
      {
        _.map(basket?.items || [], (d, index) => (
          <CardItemV2
            updateProductBasket={props.updateProductBasket}
            deleteProductBasket={props.deleteProductBasket}
            data={d}
            idCart={basket?.id}
            isB2C
            onCloseCard={() => {}}
            cms={cms}
            listCountry={countries}
            onOpenCustomBottle={() => {}}
            onCloseCustomBottle={() => {}}
            isStyleMobile
            className="cart-item-checkout"
          />
        ))
      }
      <CardSum
        buttonBlocks={props.buttonBlock}
        cms={props.cms}
        noteText={props.noteText}
        changeNote={props.changeNote}
        shipInfo={props.shipInfo}
      />
    </div>
  );

  if (isMobile) {
    return (
      <div className="div-shopping-mobile">
        <SwipeableDrawerMobile
          open={open}
          toggleDrawer={toggleDrawer}
          drawerBleeding={111}
          isFullHeight
          miniHtml={miniHtml()}
          fullHtml={fullHtml()}
        />
      </div>
    );
  }
  return (
    <div className="div-shopping-bag">
      <div className="card-shopping-bag">
        <HeaderCT type="Heading-M">
          {shoppingBag}
        </HeaderCT>
        <div className="list-carts">
          {
          _.map(basket?.items || [], (d, index) => (
            <CardItemV2
              updateProductBasket={props.updateProductBasket}
              deleteProductBasket={props.deleteProductBasket}
              data={d}
              idCart={basket?.id}
              isB2C
              onCloseCard={() => {}}
              cms={cms}
              listCountry={countries}
              onOpenCustomBottle={() => {}}
              onCloseCustomBottle={() => {}}
              isStyleMobile
              className="cart-item-checkout"
            />
          ))
        }
        </div>
        <CardSum
          isCheckOutPage
          buttonBlocks={props.buttonBlock}
          cms={props.cms}
          noteText={props.noteText}
          changeNote={props.changeNote}
          shipInfo={props.shipInfo}
        />
      </div>
    </div>
  );
}

export default ShoppingBag;
