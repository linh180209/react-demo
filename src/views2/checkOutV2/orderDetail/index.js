import classnames from 'classnames';
import React from 'react';
import ButtonCT from '../../../componentsv2/buttonCT';
import HeaderCT from '../../../componentsv2/headerCT';
import { isMobile } from '../../../DetectScreen';
import {
  generaCurrency, getCmsCommon, getNameFromButtonBlock, getNameFromCommon,
} from '../../../Redux/Helpers';
import './styles.scss';

function OrderDetail(props) {
  const cmsCommon = getCmsCommon(props.cms);
  const orderDetail = getNameFromButtonBlock(props.buttonBlock, 'order details');
  const editbt = getNameFromButtonBlock(props.buttonBlock, 'edit');
  const shippingMethodBt = getNameFromButtonBlock(props.buttonBlock, 'Shipping Method');
  const deliveryBt = getNameFromButtonBlock(props.buttonBlock, 'Delivery Time');
  const shippingAddressBt = getNameFromButtonBlock(props.buttonBlock, 'Shipping Address');
  const billingAddressBt = getNameFromButtonBlock(props.buttonBlock, 'Billing address');
  const selfBt = getNameFromCommon(cmsCommon, 'Self_Collect');
  const standBt = getNameFromCommon(cmsCommon, 'Standard');
  const expressBt = getNameFromButtonBlock(props.buttonBlock, 'Express');
  const creditBt = getNameFromCommon(cmsCommon, 'Credit/Debit_card');
  const payLateBt = getNameFromCommon(cmsCommon, 'Pay_Later');
  const payPalBt = getNameFromButtonBlock(props.buttonBlock, 'PayPal');
  const zaloPayBt = getNameFromCommon(cmsCommon, 'ZALO_PAY');
  const momoBt = getNameFromCommon(cmsCommon, 'MOMO');
  const bankTransferBt = getNameFromCommon(cmsCommon, 'BANK_TRANSFER');
  const atmCardBt = getNameFromCommon(cmsCommon, 'ATM_CARD');
  const intCardBt = getNameFromCommon(cmsCommon, 'INT_CARD');
  const codBt = getNameFromCommon(cmsCommon, 'COD');
  const checkoutBt = getNameFromButtonBlock(props.buttonBlock, 'checkout.com');

  const paymentName = (payment) => {
    switch (payment) {
      case 'stripe':
        return creditBt;
      case 'paypal':
        return payPalBt;
      case 'stripe_pay_later':
        return payLateBt;
      case 'zalopay':
        return zaloPayBt;
      case 'momo':
        return momoBt;
      case 'bank_transfer':
        return bankTransferBt;
      case 'atm_card':
        return atmCardBt;
      case 'int_card':
        return intCardBt;
      case 'cod':
        return codBt;
      case 'checkout.com':
        return checkoutBt;
      default:
        return '';
    }
  };
  const street2 = props.shipInfo?.detail ? `${props.shipInfo?.detail}` : props.shipInfo?.unitNumber ? `${props.shipInfo?.unitNumber}` : '';
  const addressShipping = `${street2 ? `${street2} ` : ''}${props.shipInfo && props.shipInfo?.street1 ? props.shipInfo.street1 : ''}${props.shipInfo && props.shipInfo.city ? ` ${props.shipInfo?.city}` : ''}${props.shipInfo && props.shipInfo.state ? ` ${props.shipInfo.state}` : ''}${props.shipInfo && props.shipInfo.postal_code ? ` ${props.shipInfo.postal_code}` : ''}${` ${props.shipInfo.phone}`}`;
  const billingShipping = props.isBillAddressSame ? addressShipping : `${props.billingInfo && props.billingInfo?.street1 ? props.billingInfo.street1 : ''}${props.billingInfo && props.billingInfo.city ? ` ${props.billingInfo?.city}` : ''}${props.billingInfo && props.billingInfo.state ? ` ${props.billingInfo.state}` : ''}${props.billingInfo && props.billingInfo.postal_code ? ` ${props.billingInfo.postal_code}` : ''}`;
  return (
    <div className="order-detail">
      <div id="anchor-step4" />
      <HeaderCT type={isMobile ? 'Heading-XS' : 'Heading-S'}>
        {orderDetail}
      </HeaderCT>
      {
        props.isStep1Done && (
          <React.Fragment>
            <div className="div-row sum-step">
              <div className="text">
                <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
                  <b>
                    {props.shipInfo?.firstName}
                    {' '}
                    {props.shipInfo?.lastName}
                  </b>
                </div>
                <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
                  {props.shipInfo?.email}
                </div>
                <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
                  {props.shipInfo?.phone}
                </div>
              </div>
              <ButtonCT
                name={editbt}
                variant="outlined"
                size="medium"
                onClick={props.onClickEditStep1}
              />
            </div>
          </React.Fragment>
        )
      }
      {
          props.isStep2Done && (
            <React.Fragment>
              <div className="div-row sum-step">
                <div className="text">
                  <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
                    <b>
                      {shippingMethodBt}
                      {': '}
                    </b>
                    {props.isSelfCollect ? selfBt : props.isExpress ? expressBt : standBt}
                    {' '}
                    {generaCurrency(props.basket?.shipping)}
                  </div>
                  {
                    props.isExpress && (
                      <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
                        <b>
                          {deliveryBt}
                          {': '}
                        </b>
                        {props.expressTimeslot}
                      </div>
                    )
                  }

                  <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
                    <b>
                      {shippingAddressBt}
                      {': '}
                    </b>
                    {addressShipping}
                  </div>
                  <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
                    <b>
                      {billingAddressBt}
                      {': '}
                    </b>
                    {billingShipping}
                  </div>
                </div>
                <ButtonCT
                  name={editbt}
                  variant="outlined"
                  size="medium"
                  onClick={props.onClickEditStep2}
                />
              </div>
            </React.Fragment>
          )
      }
      {
        props.isStep3Done && (
          <React.Fragment>
            <div className="div-row sum-step">
              <div className="text">
                <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
                  <b>
                    {paymentName(props.payMethod)}
                  </b>
                </div>
              </div>
              <ButtonCT
                name={editbt}
                variant="outlined"
                size="medium"
                onClick={props.onClickEditStep3}
              />
            </div>
          </React.Fragment>
        )
      }

    </div>
  );
}

export default OrderDetail;
