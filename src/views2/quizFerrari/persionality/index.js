import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useRecoilState } from 'recoil';
import _ from 'lodash';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import { useMergeState } from '../../../Utils/customHooks';
import { generateQuestions } from '../../../views/Quiz/Questions/AboutYou/handler';
import { scrollToNext } from '../../quiz/handler';
import { stepQuestionRecoil } from '../../quiz/recoil';
import LandingText from '../identify/landing/landingText';
import YesNoQuiz from './yesNoQuiz';

function Persionality(props) {
  const answers = useSelector(state => state.answersFerrari);
  const [stepQuestion, setStepQuestion] = useRecoilState(stepQuestionRecoil);
  const [state, setState] = useMergeState({
    resultYesNo: [],
  });
  const [listQuestion, setListQuestion] = useState([]);

  const updateStepQuestion = (idIndex) => {
    setStepQuestion(idIndex + 1);
  };

  const onClickYesNo = (name, value, idIndex) => {
    console.log('onClickYesNo', { name, value, idIndex });
    if (state.resultYesNo.length > parseInt(name, 10)) {
      state.resultYesNo[parseInt(name, 10)] = value;
    } else {
      state.resultYesNo.push(value);
    }

    // generate again question
    // parseInt(name, 10) + 1 + 1: ferrari
    const list = generateQuestions(answers, state.resultYesNo, parseInt(name, 10) + 1, true);
    setListQuestion(list);

    // increate index question
    updateStepQuestion(idIndex);

    setState({ resultYesNo: state.resultYesNo });
    _.assign(props.dataAll, { resultYesNo: state.resultYesNo });

    if (idIndex === 6) {
      setTimeout(() => {
        scrollToNext('id-header-landing-scent');
      }, 100);
    } else {
      setTimeout(() => {
        scrollToNext(`step-${idIndex + 1}-quiz`);
      }, 100);
    }

    // update data
    props.updateRealTime();
  };

  useEffect(() => {
    if (answers?.length > 0) {
      const list = generateQuestions(answers, state.resultYesNo, 0, true);
      setListQuestion(list);
    }
  }, [answers]);
  console.log('listQuestion', listQuestion);
  const titleBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Now, let’s dive into your personality and your driving style');
  return (
    <React.Fragment>
      <LandingText
        title={titleBt}
        id="id-header-landing-personality"
        onClick={() => {
          scrollToNext('step-2-quiz');
        }}
        isShow={stepQuestion >= 2}
      />
      {
        _.map(listQuestion, (d, index) => (
          <YesNoQuiz
            title={d.title}
            idIndex={2 + index}
            name={d.name}
            numberQuestion={d.numberQuestion}
            answer1={d.answer1}
            answer2={d.answer2}
            idAnswer1={d.idAnswer1}
            idAnswer2={d.idAnswer2}
            icon1={d.icon1}
            icon2={d.icon2}
            onClick={onClickYesNo}
            dataSelected={index < state.resultYesNo.length ? state.resultYesNo[index] : undefined}
            cmsState={props.cmsState}
            isShow={stepQuestion >= (2 + index)}
          />
        ))
      }
    </React.Fragment>
  );
}

export default Persionality;
