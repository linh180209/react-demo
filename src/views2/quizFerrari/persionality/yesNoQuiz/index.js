import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import ItemQuiz from '../../component/ItemQuiz';
import './styles.scss';

function YesNoQuiz(props) {
  return (
    <div
      id={`step-${props.idIndex}-quiz`}
      className={classnames(props.isShow ? 'yes-no-quiz-ferrari height-quiz' : 'hidden')}
    >
      <div className="title-ferrari">
        {props.title}
      </div>
      <div className="wrap-div-yes-no">
        <div className="div-yes-no">
          <ItemQuiz
            text={props.answer1}
            onClick={() => {
              props.onClick(props.name, props.idAnswer1, props.idIndex);
            }}
            name={props.name}
            idIndex={props.idIndex}
            active={props.dataSelected === props.idAnswer1}
          />
        </div>
        <div className="div-yes-no">
          <ItemQuiz
            text={props.answer2}
            onClick={() => {
              props.onClick(props.name, props.idAnswer2, props.idIndex);
            }}
            name={props.name}
            idIndex={props.idIndex}
            active={props.dataSelected === props.idAnswer2}
          />
        </div>
      </div>
    </div>
  );
}

export default YesNoQuiz;
