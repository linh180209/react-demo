import _ from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import MetaTags from 'react-meta-tags';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { RecoilRoot } from 'recoil';
import smoothscroll from 'smoothscroll-polyfill';
import { PUT_OUTCOME_URL } from '../../config';
import { getHeight, isiOS, isMobile767fn } from '../../DetectScreen';
import { logFerrariBlack } from '../../imagev2/svg';
import { updateAnswersFerrariData } from '../../Redux/Actions/answers';
import { loadAllBasket } from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import { updateQuestionsFerrariData } from '../../Redux/Actions/questions';
import {
  addClassIntoElemenetID,
  fetchTranslationsForSegment,
  generateHreflang, generateUrlWeb, googleAnalitycs, isCheckNull, isValidateValue, removeClassIntoElemenetID, removeLinkHreflang, scrollTop, segmentTrackPersonalityQuizStep13Completed,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import { updateCSSMobile } from '../../Utils';
import { useMergeState } from '../../Utils/customHooks';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';
import useWindowEvent from '../../Utils/useWindowEvent';
import {
  assignEmailToCart, fetchDataInitFerrari, geneateBody, getLinkGoogleAnalitycs, getPathNamePage, handleCMSPage, initOutCome, updateEmailToOutcome,
} from '../../views/Quiz/handler';
import Identify from './identify';
import Persionality from './persionality';
import Scents from './scents';
import './styles.scss';

function Quizv4Wrap() {
  // history
  const history = useHistory();
  const location = useLocation();
  // redux
  const dispatch = useDispatch();
  const cms = useSelector(state => state.cms);
  const countries = useSelector(state => state.countries);
  const basket = useSelector(state => state.basket);
  const login = useSelector(state => state.login);
  const answers = useSelector(state => state.answersFerrari);
  const questions = useSelector(state => state.questionsFerrari);

  // ref
  const slug = useRef();
  const namePath = useRef(getPathNamePage(window.location.pathname));
  const idOutCome = useRef();
  const oldBody = useRef();
  const dataAllResult = useRef({
    isAgreeEmail: true,
  });
  // const disableScroll = useRef(false);
  const refScroll = useRef();

  // state
  const [state, setState] = useMergeState({
    infoGift: location?.state?.infoGift || {},
    isStart: namePath.current !== 'quiz',
  });
  const [cmsState, setCmsState] = useState({});

  const fetchData = async () => {
    const cmsPersonal = _.find(cms, x => x.title === 'Personality Ferrari');
    if (!cmsPersonal || answers.length === 0 || questions.length === 0) {
      dispatch(loadingPage(true));
      try {
        const results = await fetchDataInitFerrari();
        dispatch(updateAnswersFerrariData(results[0]));
        await fetchTranslationsForSegment();
        dispatch(updateQuestionsFerrariData(results[1]));
        dispatch(addCmsRedux(results[2]));
        const cmsData = handleCMSPage(results[2]);
        setCmsState(cmsData);
      } catch (error) {
        toastrError(error.message);
      } finally {
        dispatch(loadingPage(false));
      }
    } else {
      const cmsData = handleCMSPage(cmsPersonal);
      setCmsState(cmsData);
      console.log(cmsData);
    }
  };

  const gotoResultPage = (customeBottle) => {
    history.push(generateUrlWeb(`/quiz-ferrari-outcome/${idOutCome.current}`));
    // if (namePath.current === 'quiz') {
    //   if (login?.user?.id) {
    //     dispatch(loginUpdateOutCome(idOutCome.current));
    //   }
    //   auth.setOutComeId(idOutCome.current);
    //   const { pathname, search } = getSearchPathName(generateUrlWeb(`${generateOutComeUrlForQuiz()}${idOutCome.current}${!_.isEmpty(state.infoGift?.codes) ? `/?codes=${state.infoGift?.codes}` : ''}`));
    //   history.push({
    //     pathname,
    //     search,
    //     state: { infoGift: state.infoGift, customeBottle },
    //   });
    // } else if (namePath.current === 'boutique-quiz') {
    //   if (window.location.pathname.includes('/boutique-quiz-v2')) {
    //     const { pathname, search } = getSearchPathName(generateUrlWeb(`/boutique-quiz-v2/outcome/${idOutCome.current}`));
    //     history.push({
    //       pathname,
    //       search,
    //       state: { customeBottle },
    //     });
    //   } else {
    //     history.push(generateUrlWeb(`/boutique-quiz/outcome/${idOutCome.current}`));
    //   }
    // }
  };

  const updateRealTime = async () => {
    const dataAll = dataAllResult.current;
    const body = geneateBody(_.cloneDeep(dataAll));
    // prevent click send multiple
    if (JSON.stringify(body) === JSON.stringify(oldBody.current)) {
      return;
    }
    oldBody.current = body;

    if (isCheckNull(idOutCome.current)) {
      const result = await initOutCome(namePath.current, slug.current);
      idOutCome.current = result?.id;
    }

    if (!isCheckNull(idOutCome.current)) {
      const options = {
        url: PUT_OUTCOME_URL.replace('{id}', idOutCome.current),
        method: 'PUT',
        body,
      };
      fetchClient(options, true);
    }
  };

  const onClickDiscover = () => {
    const dataAll = dataAllResult.current;
    console.log('dataAll', dataAll);
    if (!dataAll?.email) {
      toastrError('Email address required.');
      return;
    }
    if (!isValidateValue(dataAll?.email)) {
      toastrError('Please submit a valid email address.');
      return;
    }
    if (isCheckNull(idOutCome.current)) {
      return;
    }
    assignEmailToCart(dataAll?.email, basket, (result) => {
      dispatch(loadAllBasket(result));
      segmentTrackPersonalityQuizStep13Completed(dataAll?.email, dataAll?.isAgreeEmail);
      toastrSuccess('Your email has been submitted! Check your inbox to view your detailed personality result.');
    });
    updateEmailToOutcome(idOutCome.current, dataAll?.email);

    // go to result
    gotoResultPage(dataAll?.customeBottle);
  };

  const onClickSkipEmail = () => {
    const dataAll = dataAllResult.current;
    // go to result
    gotoResultPage(dataAll?.customeBottle);
  };

  const onClickSkip = (dataAll) => {
    // go to result
    gotoResultPage(dataAll?.customeBottle);
  };

  const onScroll = () => {
    const ele = document.getElementById('step-10-quiz');
    if (refScroll.current && ele) {
      const { scrollTop: scrollTopT, scrollHeight, clientHeight } = refScroll.current;
      if (scrollTopT + clientHeight + 150 > scrollHeight) {
        addClassIntoElemenetID('header-quiz', 'hidden-opacity');
      } else {
        removeClassIntoElemenetID('header-quiz', 'hidden-opacity');
      }
    }
  };

  const onChangeSize = () => {
    updateCSSMobile('quiz-ferrari');
  };

  useWindowEvent('resize', onChangeSize, window);

  // useEffect(() => {
  //   if (login?.user?.outcome && !['boutique-quiz', 'krisshop', 'sephora'].includes(namePath.current)) {
  //     idOutCome.current = login?.user?.outcome || undefined;
  //   }
  // }, [login]);

  useEffect(() => {
    // update CMS for mobile
    updateCSSMobile('quiz-ferrari');

    // fetch Data
    fetchData();

    // scroll top
    smoothscroll.polyfill();
    scrollTop();

    // google analitycs
    googleAnalitycs(getLinkGoogleAnalitycs(namePath.current));

    // get slug
    slug.current = 'ferrari';
    // slug.current = undefined;

    // redirect to result if done before
    // const idOutComeTemp = login?.user?.outcome || auth.getOutComeId();
    // if (namePath.current === 'quiz' && idOutComeTemp && !(location?.state?.isRetake)) {
    //   const { pathname, search } = getSearchPathName(generateUrlWeb(`${generateOutComeUrlForQuiz()}${idOutComeTemp}${!_.isEmpty(state.infoGift) ? `/?codes=${state.infoGift?.codes}` : ''}`));
    //   history.push({
    //     pathname,
    //     search,
    //     state: { stepInstore: 2, infoGift: state.infoGift },
    //   });
    // }
    return (() => {
      removeLinkHreflang();
      window.removeEventListener('scroll');
    });
  }, []);

  if (_.isEmpty(cmsState) || _.isEmpty(answers) || _.isEmpty(questions)) {
    return (<div />);
  }

  const countryName = auth.getCountryName();

  return (
    <div>
      <MetaTags>
        <title>
          {replaceCountryInMetaTags(countryName, cmsState?.seo?.seoTitle)}
        </title>
        <meta name="description" content={replaceCountryInMetaTags(countryName, cmsState?.seo?.seoDescription)} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/quiz-ferrari')}
      </MetaTags>
      <div className="bg-image-logo-ferrari">
        <img src={logFerrariBlack} alt="logo" />
      </div>
      <div
        onScroll={onScroll}
        ref={refScroll}
        id="quiz-ferrari"
        className="quiz-ferrari"
        style={isMobile767fn() ? { height: `${window.innerHeight}px` } : {}}
      >
        <Identify
          cmsState={cmsState}
          dataAll={dataAllResult.current}
          updateRealTime={updateRealTime}
        />
        <Persionality
          cmsState={cmsState}
          dataAll={dataAllResult.current}
          updateRealTime={updateRealTime}
        />
        <Scents
          cmsState={cmsState}
          dataAll={dataAllResult.current}
          updateRealTime={updateRealTime}
          onClickDiscover={onClickDiscover}
          onClickSkipEmail={onClickSkipEmail}
        />
      </div>
    </div>
  );
}

function QuizFerrari() {
  return (
    <RecoilRoot>
      <Quizv4Wrap />
    </RecoilRoot>
  );
}

export default QuizFerrari;
