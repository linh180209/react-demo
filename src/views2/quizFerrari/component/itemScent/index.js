import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import './styles.scss';

function ItemScent(props) {
  return (
    <div
      className={classnames('item-scent-ferrari', props.active ? 'active' : '')}
      onClick={props.onClick}
    >
      <img src={props.image} alt="scent" />
      <div className="bg-scent">
        {props.name}
        <CheckCircleOutlineIcon style={{ color: '#ffffff' }} />
      </div>
    </div>
  );
}

export default ItemScent;
