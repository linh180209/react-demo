import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import { icLogoFerrariRed } from '../../../../imagev2/svg';
import './styles.scss';

function ItemQuiz(props) {
  return (
    <div
      className={classnames('item-quiz-ferrari', props.active ? 'active' : '', props.className)}
      onClick={props.onClick}
    >
      {
        !props.isNoLogoBackground && <img className="bg-logo" src={icLogoFerrariRed} alt="logo" />
      }
      {
        props.icon && (
          <div className="img-icon">
            <img src={props.icon} alt="icons" />
          </div>
        )
      }
      {props.text}
      {
        props.subText && (
          <div className="text-sub">
            {props.subText}
          </div>
        )
      }
    </div>
  );
}

export default ItemQuiz;
