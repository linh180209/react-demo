import React, { useState, useEffect } from 'react';
import { useRecoilValue } from 'recoil';
import classnames from 'classnames';
import { logoWhite } from '../../../../imagev2/svg';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import { stepQuestionRecoil } from '../../../quiz/recoil';
import { updateCSSMobile } from '../../../../Utils';
import './styles.scss';
import useWindowEvent from '../../../../Utils/useWindowEvent';

function HeaderQuiz(props) {
  const stepQuestion = useRecoilValue(stepQuestionRecoil);

  const onChangeSize = () => {
    updateCSSMobile('height-quiz');
  };

  useWindowEvent('resize', onChangeSize, window);
  useEffect(() => {
    updateCSSMobile('height-quiz');
  }, [stepQuestion]);

  const identifyBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Identify');
  const personalityBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Personality');
  const scentsBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Scents');
  return (
    <div id="header-quiz" className="header-quiz">
      <div className="div-logo">

        <img src={logoWhite} alt="logo" />
      </div>
      <div className="tabs">
        <div className={classnames('tabs-item', stepQuestion < 2 ? 'active' : '')}>
          {identifyBt}
        </div>
        <div className={classnames('tabs-item', stepQuestion >= 2 && stepQuestion < 7 ? 'active' : '')}>
          {personalityBt}
        </div>
        <div className={classnames('tabs-item', stepQuestion >= 7 ? 'active' : '')}>
          {scentsBt}
        </div>
      </div>
    </div>
  );
}

export default HeaderQuiz;
