import React, { useState, useEffect } from 'react';
import Slider from 'react-rangeslider';
import classnames from 'classnames';
import 'react-rangeslider/lib/index.css';
import { icNextFerrari } from '../../../../imagev2/svg';
import { isMobile767fn } from '../../../../DetectScreen';
import './styles.scss';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';

const lables = {
  1: '1',
  5: '5',
  10: '10',
  15: '15',
  20: '20',
  25: '25',
  30: '30',
  35: '35',
  40: '40',
  45: '45',
  50: '50',
  55: '55',
  60: '60',
  65: '65',
  70: '70',
  75: '75',
  80: '80',
  85: '85',
  90: '90',
  95: '95',
  100: '100',
};
const labeMobile = {
  1: '1',
  100: '100',
};

function AgeQuiz(props) {
  const [value, setValue] = useState(30);

  const handleChangeHorizontal = (v) => {
    setValue(v);
  };

  const onClickNext = () => {
    props.onClick(props.name, value, props.idIndex);
  };

  const howOldBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'How old are you');
  const nextBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Next');
  return (
    <div
      id={`step-${props.idIndex}-quiz`}
      className={classnames(props.isShow ? 'age-quiz-ferrari height-quiz' : 'hidden')}
    >
      <div className="title">
        {howOldBt}
      </div>
      <div className="div-progress-bar">
        <Slider
          min={1}
          max={100}
          value={value}
          handleLabel={value}
          labels={isMobile767fn() ? labeMobile : lables}
          onChange={handleChangeHorizontal}
        />
      </div>
      <button
        onClick={onClickNext}
        type="button"
        className="button-bg__none"
      >
        {nextBt}
        <img src={icNextFerrari} alt="icon" />
      </button>
    </div>
  );
}

export default AgeQuiz;
