import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import ItemQuiz from '../../component/ItemQuiz';
import './styles.scss';

function GenderQuiz(props) {
  const [valueSeleted, setValueSeleted] = useState();

  const whatBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'WHAT IS YOUR GENDER');
  const maleBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'male');
  const femaleBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'female');
  const neuBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'gender neutral');
  return (
    <div
      id={`step-${props.idIndex}-quiz`}
      className={classnames(props.isShow ? 'geneder-quiz height-quiz' : 'hidden')}
    >
      <div className="div-question">
        <div className="title">
          {whatBt}
        </div>
      </div>
      <div className="div-answer">
        <div className="div-item-answer-gender">
          <ItemQuiz
            text={maleBt}
            onClick={() => {
              props.onClick(props.name, '1', props.idIndex);
              setValueSeleted('1');
            }}
            className={valueSeleted === '1' ? 'active' : ''}
          />
        </div>
        <div className="div-item-answer-gender">
          <ItemQuiz
            text={femaleBt}
            onClick={() => {
              props.onClick(props.name, '2', props.idIndex);
              setValueSeleted('2');
            }}
            className={valueSeleted === '2' ? 'active' : ''}
          />
        </div>
        <div className="div-item-answer-gender">
          <ItemQuiz
            text={neuBt}
            onClick={() => {
              props.onClick(props.name, '3', props.idIndex);
              setValueSeleted('3');
            }}
            className={valueSeleted === '3' ? 'active' : ''}
          />
        </div>
      </div>
    </div>
  );
}

export default GenderQuiz;
