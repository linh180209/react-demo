import _ from 'lodash';
import React from 'react';
import { useLottie } from 'lottie-react';
import { logoFerrariMaison21g } from '../../../../imagev2/png';
import { getAltImageV2 } from '../../../../Redux/Helpers';
import arrowDown from '../../../../imagev2/lottie/arrow_down_animation.json';
import './styles.scss';
import { isMobile767fn, isMobile991fn } from '../../../../DetectScreen';

function LandingFerrari(props) {
  const options = {
    animationData: arrowDown,
    loop: true,
  };
  const { View } = useLottie(options);
  const typeImage = isMobile767fn() ? 'phone' : (isMobile991fn() ? 'tablet' : 'web');
  const imageBlock = _.find(props.cmsState?.imagetxtBlocks, x => x.value?.image?.caption === 'landing-ferrari' && x.value?.image?.type === typeImage);
  console.log('imageBlock', imageBlock);
  return (
    <div
      id={props.id}
      className="div-landing-ferrari height-quiz full"
    >
      <img className="bg-image" src={props.isShowUrlImage ? props.urlImage : imageBlock?.value?.image?.image} alt={getAltImageV2(imageBlock?.value?.image)} />
      <div className="header-log">
        <img src={logoFerrariMaison21g} alt="logo" />
      </div>
      <div className="footer-text">
        <div className="text">
          {props.title || imageBlock?.value?.text}
        </div>
        <button
          type="button"
          className="button-bg__none"
          onClick={props.onClick}
        >
          {View}
        </button>
      </div>
    </div>
  );
}

export default LandingFerrari;
