import React from 'react';
import { useLottie } from 'lottie-react';
import arrowDown from '../../../../imagev2/lottie/arrow_down_animation.json';
import './styles.scss';

function LandingText(props) {
  const options = {
    animationData: arrowDown,
    loop: true,
  };
  const { View } = useLottie(options);

  return (
    <div
      id={props.id}
      className={props.isShow ? 'landing-text height-quiz' : 'hidden'}
    >
      {
        props.subTitle && (
          <div className="text sub-t">
            {props.subTitle}
          </div>
        )
      }
      <div className="text">
        {props.title}
      </div>
      <button
        onClick={props.onClick}
        type="button"
        className="button-bg__none"
      >
        {View}
      </button>
    </div>
  );
}

export default LandingText;
