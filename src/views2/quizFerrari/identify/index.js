import React, { useState, useEffect } from 'react';
import { useRecoilState } from 'recoil';
import _ from 'lodash';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import { useMergeState } from '../../../Utils/customHooks';
import { scrollToNext } from '../../quiz/handler';
import { stepQuestionRecoil } from '../../quiz/recoil';
import AgeQuiz from './ageQuiz';
import GenderQuiz from './genderQuiz';
import HeaderQuiz from './headerQuiz';
import LandingFerrari from './landing';
import LandingText from './landing/landingText';

function Identify(props) {
  const [stepQuestion, setStepQuestion] = useRecoilState(stepQuestionRecoil);
  const [state, setState] = useMergeState({
    gender: undefined,
    age: 30,
  });

  const updateStepQuestion = (idIndex) => {
    setStepQuestion(idIndex + 1);
  };

  const onClick = (name, value, idIndex) => {
    // increate index question
    updateStepQuestion(idIndex);

    setState({ [name]: value });
    _.assign(props.dataAll, { [name]: value });

    if (idIndex === 1) {
      scrollToNext('id-header-landing-personality');
    } else {
      scrollToNext(`step-${idIndex + 1}-quiz`);
    }

    // update data
    props.updateRealTime();
  };

  console.log('Identify', props.cmsState);
  const firstBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'First, well need an introduction');
  return (
    <React.Fragment>
      <LandingFerrari
        id="id-header-landing-ferrari"
        cmsState={props.cmsState}
        onClick={() => {
          scrollToNext('id-header-landing-identify');
        }}
      />
      <HeaderQuiz
        cmsState={props.cmsState}
      />
      <LandingText
        id="id-header-landing-identify"
        title={firstBt}
        onClick={() => {
          scrollToNext('step-0-quiz');
        }}
        isShow
      />
      <AgeQuiz
        dataSelected={state.age}
        onClick={onClick}
        name="age"
        idIndex={0}
        cmsState={props.cmsState}
        isShow={stepQuestion >= 0}
      />
      <GenderQuiz
        cmsState={props.cmsState}
        dataSelected={state.gender}
        onClick={onClick}
        name="gender"
        idIndex={1}
        isShow={stepQuestion >= 1}
      />
    </React.Fragment>
  );
}

export default Identify;
