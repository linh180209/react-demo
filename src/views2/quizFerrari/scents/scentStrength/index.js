import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import ItemQuiz from '../../component/ItemQuiz';
import { icBalance, icSpeedo } from '../../../../imagev2/svg';
import './styles.scss';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';

function ScentStrength(props) {
  const [valueSeleted, setValueSeleted] = useState(props.value);

  const whatBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'what is your preferred car scent strength');
  const BalancedBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Balanced');
  const intenseBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'intense');
  return (
    <div
      id={`step-${props.idIndex}-quiz`}
      className={classnames(props.isShow ? 'div-scent-strength-ferrari height-quiz' : 'hidden')}
    >
      <div className="title-ferrari">
        {whatBt}
      </div>
      <div className="wrap-scent-strength">
        <div className="item-scent-strength">
          <ItemQuiz
            isNoLogoBackground
            icon={icBalance}
            text={BalancedBt}
            className="item-icon"
            onClick={() => {
              props.onClick(props.name, 1, props.idIndex);
              setValueSeleted(1);
            }}
            active={valueSeleted === 1}
          />
        </div>
        <div className="item-scent-strength">
          <ItemQuiz
            isNoLogoBackground
            icon={icSpeedo}
            text={intenseBt}
            className="item-icon"
            onClick={() => {
              props.onClick(props.name, 2, props.idIndex);
              setValueSeleted(2);
            }}
            active={valueSeleted === 2}
          />
        </div>
      </div>
    </div>
  );
}

export default ScentStrength;
