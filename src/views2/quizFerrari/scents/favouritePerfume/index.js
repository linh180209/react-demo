import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import { icNextFerrari } from '../../../../imagev2/svg';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import ItemScent from '../../component/itemScent';
import './styles.scss';
import { toastrError } from '../../../../Redux/Helpers/notification';
import { GET_ALL_SCENT_FAMILY_FERRARI } from '../../../../config';

function FavouritePerfume(props) {
  const [dataProduct, setDataProduct] = useState([]);
  const [value, setValue] = useState(props.value || []);

  const onClickNext = () => {
    props.onClick(props.name, value, props.idIndex);
  };

  const onClickSkip = () => {
    setValue([]);
    props.onClick(props.name, [], props.idIndex);
  };

  const isExistItem = d => _.find(value, x => x.id === d.id);

  const onSelection = (d) => {
    if (isExistItem(d)) {
      _.remove(value, x => x.id === d.id);
    } else {
      value.push(d);
    }
    setValue([...value]);
  };

  const fetchDataFamily = () => {
    const options = {
      url: GET_ALL_SCENT_FAMILY_FERRARI,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        setDataProduct(result);
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      console.log('err', err);
      toastrError(err.message);
    });
  };

  useEffect(() => {
    fetchDataFamily();
  }, []);

  const anyBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'ANY FAVOURITE PERFUME FAMILY FOR THE SCENT OF YOUR CAR');
  const youBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'You may select multiple options');
  const skipBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Skip');
  const nextBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Next');
  return (
    <div
      id={`step-${props.idIndex}-quiz`}
      className={classnames(props.isShow ? 'favourite-perfume height-quiz min-height' : 'hidden')}
    >
      <div className="title-ferrari">
        {anyBt}
        <div className="sub-title-ferrari">
          {youBt}
        </div>
      </div>
      <div className="wrap-favourite-perfume">
        {
          _.map(dataProduct, d => (
            <ItemScent
              image={d.image}
              name={d.name}
              onClick={() => onSelection(d)}
              active={isExistItem(d)}
            />
          ))
        }
      </div>
      <div className="div-bt-list">
        <button
          type="button"
          className="button-bg__none bt-skip-favourite"
          onClick={onClickSkip}
        >
          {skipBt}
        </button>
        <button
          type="button"
          className="button-bg__none bt-next-favourite"
          onClick={onClickNext}
        >
          {nextBt}
          <img src={icNextFerrari} alt="icon" />
        </button>
      </div>
    </div>
  );
}

export default FavouritePerfume;
