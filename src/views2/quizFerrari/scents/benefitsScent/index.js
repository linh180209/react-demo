import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import ItemQuiz from '../../component/ItemQuiz';
import './styles.scss';

function BenefitsScent(props) {
  const [value, setValue] = useState(props.value || []);

  const isExistItem = d => _.find(value, x => x.id === d.id);

  const onClick = (data) => {
    setValue(_.cloneDeep([data]));
    props.onClick(props.name, [data], props.idIndex);
  };

  const benefitsBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Which benefits  do you expect from the scent of your car');
  const chillBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'To feel chill');
  const relaxBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'RELAXATION & DESTRESS');
  const confidentBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'To feel confident');
  const freshnessBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'FRESHNESS & PURITY');
  const goodBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'To feel good');
  const delightBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'DELIGHT & PLEASURE');
  const uniqueBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'To feel unique');
  const elegantBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'ELEGANCE & DISTINCTION');
  return (
    <div
      id={`step-${props.idIndex}-quiz`}
      className={classnames(props.isShow ? 'benefits-scent height-quiz' : 'hidden')}
    >
      <div className="title-ferrari">
        {benefitsBt}
      </div>
      <div className="wrap-benefits-scent">
        <div className="item-benefits-scent">
          <ItemQuiz
            subText={chillBt}
            text={relaxBt}
            className="item-sub-text"
            active={isExistItem({ id: 0 })}
            onClick={() => {
              onClick({ id: 0 });
            }}
          />
        </div>
        <div className="item-benefits-scent">
          <ItemQuiz
            subText={confidentBt}
            text={freshnessBt}
            className="item-sub-text"
            active={isExistItem({ id: 1 })}
            onClick={() => {
              onClick({ id: 1 });
            }}
          />
        </div>
        <div className="item-benefits-scent">
          <ItemQuiz
            subText={goodBt}
            text={delightBt}
            className="item-sub-text"
            active={isExistItem({ id: 2 })}
            onClick={() => {
              onClick({ id: 2 });
            }}
          />
        </div>
        <div className="item-benefits-scent">
          <ItemQuiz
            subText={uniqueBt}
            text={elegantBt}
            className="item-sub-text"
            active={isExistItem({ id: 3 })}
            onClick={() => {
              onClick({ id: 3 });
            }}
          />
        </div>
      </div>
    </div>
  );
}

export default BenefitsScent;
