import React, { useState, useEffect } from 'react';
import { useRecoilState } from 'recoil';
import _ from 'lodash';
import BenefitsScent from './benefitsScent';
import EmailResult from './emailResult';
import FavouritePerfume from './favouritePerfume';
import LandingText from '../identify/landing/landingText';
import ScentStrength from './scentStrength';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import { scrollToNext } from '../../quiz/handler';
import { stepQuestionRecoil } from '../../quiz/recoil';
import { useMergeState } from '../../../Utils/customHooks';

function Scents(props) {
  const [stepQuestion, setStepQuestion] = useRecoilState(stepQuestionRecoil);

  const updateStepQuestion = (idIndex) => {
    setStepQuestion(idIndex + 1);
  };

  const onClick = (name, value, idIndex) => {
    console.log('onClick', name, value, idIndex);

    // increate index question
    updateStepQuestion(idIndex);

    _.assign(props.dataAll, { [name]: value });

    scrollToNext(`step-${idIndex + 1}-quiz`);

    // update data
    props.updateRealTime();
  };

  const title1 = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Splendid');
  const title = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Let’s move on your olfactive preferences to create your car Scent');
  return (
    <React.Fragment>
      <LandingText
        id="id-header-landing-scent"
        title={title}
        subTitle={title1}
        onClick={() => {
          scrollToNext('step-7-quiz');
        }}
        isShow={stepQuestion >= 7}
      />
      <ScentStrength
        cmsState={props.cmsState}
        idIndex={7}
        isShow={stepQuestion >= 7}
        name="strength"
        onClick={onClick}
      />
      <BenefitsScent
        cmsState={props.cmsState}
        idIndex={8}
        isShow={stepQuestion >= 8}
        name="mixes"
        onClick={onClick}
      />
      <FavouritePerfume
        cmsState={props.cmsState}
        onClick={onClick}
        idIndex={9}
        isShow={stepQuestion >= 9}
        name="families"
      />
      <EmailResult
        idIndex={10}
        dataAll={props.dataAll}
        isShow={stepQuestion >= 10}
        cmsState={props.cmsState}
        onClickDiscover={props.onClickDiscover}
        onClickSkipEmail={props.onClickSkipEmail}
      />
    </React.Fragment>
  );
}

export default Scents;
