import React, { useState, useEffect } from 'react';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import _ from 'lodash';
import classnames from 'classnames';
import { useSelector } from 'react-redux';
import { logoFerrariMaison21g } from '../../../../imagev2/png';
import InputCT from '../../../../componentsv2/inputCT';
import CheckBoxCT from '../../../../componentsv2/checkBoxCT';
import ButtonCT from '../../../../componentsv2/buttonCT';
import './styles.scss';
import LinkCT from '../../../../componentsv2/linkCT';
import { getAltImageV2, getLinkFromButtonBlock, getNameFromButtonBlock } from '../../../../Redux/Helpers';
import { useMergeState } from '../../../../Utils/customHooks';
import useWindowEvent from '../../../../Utils/useWindowEvent';

function EmailResult(props) {
  // redux
  const login = useSelector(state => state.login);
  const basket = useSelector(state => state.basket);

  const [state, setState] = useMergeState({
    email: props.dataAll?.email,
    isAgreeEmail: props.dataAll?.isAgreeEmail,
  });

  const onChangeEmail = (e) => {
    const { value } = e.target;
    setState({ email: value });
    _.assign(props.dataAll, { email: value });
  };

  const onCheckBox = () => {
    setState({ isAgreeEmail: !state.isAgreeEmail });
    _.assign(props.dataAll, { isAgreeEmail: !state.isAgreeEmail });
  };

  const hanleScroll = () => {
    const ele = document.getElementById('step-10-quiz');
    console.log('hanleScroll', ele);
    if (ele) {
      console.log('hanleScroll', ele.getBoundingClientRect().top);
    }
  };

  useEffect(() => {
    if (!props.dataAll?.email) {
      const value = login?.user?.email || basket?.email;
      onChangeEmail({
        target: { value },
      });
    }
  }, [login, basket]);

  useWindowEvent('scroll', hanleScroll, window);

  const imageBlock = _.find(props.cmsState?.imagetxtBlocks, x => x.value?.image?.caption === 'result-image');
  const weAreBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'WE ARE PREPARING YOUR PERSONALISED car scent  RECOMMENDATIONS…');
  const emailBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Please enter your email and you will receive your full scent analysis:');
  const yourDriverBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Your driver personality & Scent style');
  const theScentBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'The scents that match your car and your style');
  const emailplaceholdBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Email');
  const myResultBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Reveal My Result');
  const skipResultBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Skip to result');
  if (!props.isShow) {
    return null;
  }
  return (
    <div
      id={`step-${props.idIndex}-quiz`}
      className={classnames(props.isShow ? 'email-result-ferrari height-quiz full' : 'hidden')}
    >
      <img className="img-bg" src={imageBlock?.value?.image?.image} alt={getAltImageV2(imageBlock?.value?.image)} />
      <div className="l-bg" />
      <div className="div-block-text">
        <img className="logo" src={logoFerrariMaison21g} alt="logo" />
        <div className="title-ferrari">
          {weAreBt}
        </div>
        <div className="des">
          {emailBt}
        </div>
        <div className="check-list">
          <CheckCircleOutlineIcon style={{ color: '#ffffff' }} />
          {yourDriverBt}
        </div>
        <div className="check-list">
          <CheckCircleOutlineIcon style={{ color: '#ffffff' }} />
          {theScentBt}
        </div>
        <InputCT
          type="text"
          placeholder={emailplaceholdBt}
          name="email"
          value={state.email}
          onChange={onChangeEmail}
          className="input-email"
        />
        <div className="div-row row-check-box">
          <CheckBoxCT
            onChange={onCheckBox}
            name="create_address"
            checked={state.isAgreeEmail}
            label=""
            className="medium check-box-email"
          />
          <div className="link-text">
            {getNameFromButtonBlock(props?.cmsState?.buttonBlocks, 'I’ve read and accept all')}
            {' '}
            <LinkCT to={getLinkFromButtonBlock(props?.cmsState?.buttonBlocks, 'Terms and Conditions')}>{getNameFromButtonBlock(props?.cmsState?.buttonBlocks, 'Terms and Conditions')}</LinkCT>
            {' '}
            {getNameFromButtonBlock(props?.cmsState?.buttonBlocks, 'and')}
            {' '}
            <LinkCT to={getLinkFromButtonBlock(props?.cmsState?.buttonBlocks, 'Privacy Policy')}>{getNameFromButtonBlock(props?.cmsState?.buttonBlocks, 'Privacy Policy')}</LinkCT>
          </div>
        </div>
        <ButtonCT
          disabled={!state.isAgreeEmail || !state.email}
          name={myResultBt}
          size="medium"
          onClick={props.onClickDiscover}
          className="bt-result"
        />
        <button
          onClick={props.onClickSkipEmail}
          type="button"
          className="button-bg__none bt-skip"
        >
          {skipResultBt}
        </button>
      </div>
    </div>
  );
}

export default EmailResult;
