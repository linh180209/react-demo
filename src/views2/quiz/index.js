import _ from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import MetaTags from 'react-meta-tags';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import { RecoilRoot } from 'recoil';
import smoothscroll from 'smoothscroll-polyfill';
import LandingQuizV2 from '../../componentsv2/landingQuiz';
import { PUT_OUTCOME_URL } from '../../config';
import updateAnswersData from '../../Redux/Actions/answers';
import { loadAllBasket } from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import { loginUpdateOutCome } from '../../Redux/Actions/login';
import updateQuestionsData from '../../Redux/Actions/questions';
import {
  fetchTranslationsForSegment,
  generateHreflang, generateUrlWeb, getCmsCommon, getSearchPathName, googleAnalitycs, isCheckNull, isValidateValue, removeLinkHreflang, scrollTop, segmentTrackPersonalityQuizStep13Completed,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import { useMergeState } from '../../Utils/customHooks';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';
import {
  assignEmailToCart, fetchDataInitV2, geneateBody, generateOutComeUrlForQuiz, getLinkGoogleAnalitycs, getPathNamePage, getSlug, handleCMSPage, initOutCome, updateEmailToOutcome,
} from '../../views/Quiz/handler';
import LandingQuiz from '../../views/Quiz/LandingQuiz';
import AboutQuiz from './aboutQuiz';
import PersionalQuiz from './persionalQuiz';
import ScentQuiz from './scentQuiz';
import './styles.scss';
import Toolbar from './toolbar';

function Quizv4Wrap() {
  // history
  const history = useHistory();
  const location = useLocation();
  // redux
  const dispatch = useDispatch();
  const cms = useSelector(state => state.cms);
  const countries = useSelector(state => state.countries);
  const basket = useSelector(state => state.basket);
  const login = useSelector(state => state.login);
  const answers = useSelector(state => state.answers);
  const questions = useSelector(state => state.questions);

  // ref
  const slug = useRef();
  const namePath = useRef(getPathNamePage(window.location.pathname));
  const idOutCome = useRef();
  const oldBody = useRef();
  const dataAllResult = useRef({
    isAgreeEmail: true,
  });

  // state
  const [state, setState] = useMergeState({
    infoGift: location?.state?.infoGift || {},
    isStart: namePath.current !== 'quiz',
  });
  const [cmsState, setCmsState] = useState({});

  const fetchData = async () => {
    const cmsPersonal = _.find(cms, x => x.title === 'Personality-V2');
    if (!cmsPersonal || answers.length === 0 || questions.length === 0) {
      dispatch(loadingPage(true));
      try {
        const results = await fetchDataInitV2();
        dispatch(updateAnswersData(results[0]));
        await fetchTranslationsForSegment();
        dispatch(updateQuestionsData(results[1]));
        dispatch(addCmsRedux(results[2]));
        const cmsData = handleCMSPage(results[2]);
        setCmsState(cmsData);
      } catch (error) {
        toastrError(error.message);
      } finally {
        dispatch(loadingPage(false));
      }
    } else {
      const cmsData = handleCMSPage(cmsPersonal);
      setCmsState(cmsData);
      console.log(cmsData);
    }
  };

  const gotoResultPage = (customeBottle) => {
    if (namePath.current === 'quiz') {
      if (login?.user?.id) {
        dispatch(loginUpdateOutCome(idOutCome.current));
      }
      auth.setOutComeId(idOutCome.current);
      const { pathname, search } = getSearchPathName(generateUrlWeb(`${generateOutComeUrlForQuiz()}${idOutCome.current}${!_.isEmpty(state.infoGift?.codes) ? `/?codes=${state.infoGift?.codes}` : ''}`));
      history.push({
        pathname,
        search,
        state: { infoGift: state.infoGift, customeBottle },
      });
    } else if (namePath.current === 'boutique-quiz') {
      if (window.location.pathname.includes('/boutique-quiz-v2')) {
        const { pathname, search } = getSearchPathName(generateUrlWeb(`/boutique-quiz-v2/outcome/${idOutCome.current}`));
        history.push({
          pathname,
          search,
          state: { customeBottle },
        });
      } else {
        history.push(generateUrlWeb(`/boutique-quiz/outcome/${idOutCome.current}`));
      }
    } else if (namePath.current === 'sephora') {
      history.push(generateUrlWeb(`/quiz/outcome/sephora/${idOutCome.current}`));
    } else if (namePath.current === 'lazada') {
      history.push(generateUrlWeb(`/lazada/outcome/${idOutCome.current}`));
    }
  };

  // isGotoResultPage: update data realtime then go to result page
  const updateRealTime = async (isGotoResultPage = false) => {
    const dataAll = dataAllResult.current;
    const body = geneateBody(_.cloneDeep(dataAll), namePath.current);
    // prevent click send multiple
    if (JSON.stringify(body) === JSON.stringify(oldBody.current)) {
      return;
    }
    oldBody.current = body;
    try {
      if (isCheckNull(idOutCome.current)) {
        const result = await initOutCome(namePath.current, slug.current);
        idOutCome.current = result?.id;
      }

      _.assign(body, {
        meta: {
          limit_mixes: true,
        },
      });
      if (!isCheckNull(idOutCome.current)) {
        const options = {
          url: PUT_OUTCOME_URL.replace('{id}', idOutCome.current),
          method: 'PUT',
          body,
        };
        if (!isGotoResultPage) {
          fetchClient(options, true);
        } else {
          dispatch(loadingPage(true));
          await fetchClient(options, true);
          dispatch(loadingPage(false));
          gotoResultPage(dataAll?.customeBottle);
        }
      }
    } catch (error) {
      console.log('error', error);
      dispatch(loadingPage(false));
    }
  };

  const onClickDiscover = () => {
    const dataAll = dataAllResult.current;
    console.log('dataAll', dataAll);
    if (!dataAll?.email) {
      toastrError('Email address required.');
      return;
    }
    if (!isValidateValue(dataAll?.email)) {
      toastrError('Please submit a valid email address.');
      return;
    }
    if (isCheckNull(idOutCome.current)) {
      return;
    }
    assignEmailToCart(dataAll?.email, basket, (result) => {
      dispatch(loadAllBasket(result));
      segmentTrackPersonalityQuizStep13Completed(dataAll?.email, dataAll?.isAgreeEmail);
      toastrSuccess('Your email has been submitted! Check your inbox to view your detailed personality result.');
    });
    updateEmailToOutcome(idOutCome.current, dataAll?.email);

    // go to result
    gotoResultPage(dataAll?.customeBottle);
  };

  const onClickSkip = (dataAll) => {
    // go to result
    gotoResultPage(dataAll?.customeBottle);
  };

  useEffect(() => {
    if (login?.user?.outcome && !['boutique-quiz', 'krisshop', 'sephora', 'lazada'].includes(namePath.current)) {
      idOutCome.current = login?.user?.outcome || undefined;
    }
  }, [login]);


  useEffect(() => {
    // fetch Data
    fetchData();

    // scroll top
    smoothscroll.polyfill();
    scrollTop();

    // google analitycs
    googleAnalitycs(getLinkGoogleAnalitycs(namePath.current));

    // get slug
    slug.current = getSlug(namePath.current);

    // redirect to result if done before
    const idOutComeTemp = login?.user?.outcome || auth.getOutComeId();
    if (namePath.current === 'quiz' && idOutComeTemp && !(location?.state?.isRetake)) {
      const { pathname, search } = getSearchPathName(generateUrlWeb(`${generateOutComeUrlForQuiz()}${idOutComeTemp}${!_.isEmpty(state.infoGift) ? `/?codes=${state.infoGift?.codes}` : ''}`));
      history.push({
        pathname,
        search,
        state: { stepInstore: 2, infoGift: state.infoGift },
      });
    }
    return (() => {
      removeLinkHreflang();
      window.removeEventListener('scroll');
    });
  }, []);

  if (_.isEmpty(cmsState) || _.isEmpty(answers) || _.isEmpty(questions)) {
    return (<div />);
  }

  const countryName = auth.getCountryName();

  return (
    <div id="quiz-4" className="quiz-4">
      <MetaTags>
        <title>
          {replaceCountryInMetaTags(countryName, cmsState?.seo?.seoTitle)}
        </title>
        <meta name="description" content={replaceCountryInMetaTags(countryName, cmsState?.seo?.seoDescription)} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/quiz')}
      </MetaTags>
      {
        state.isStart ? (
          namePath.current === 'sephora' ? (
            <LandingQuizV2
              buttonBlocks={cmsState.buttonBlocks}
              videoBlocks={cmsState.videoBlocks}
              headerAndParagraphBocks={cmsState.headerAndParagraphBocks}
              onClick={() => { setState({ isStart: false }); }}
            />
          ) : (
            <LandingQuiz
              cmsCommon={getCmsCommon(cms)}
              buttonBlocks={cmsState.buttonBlocks}
              imageBackground={cmsState.imageBackground}
              headerText={cmsState.headerText}
              namePath={namePath.current}
              paragraphBlock={cmsState.paragraphBlock}
              onClickStart={() => { setState({ isStart: false }); }}
            />
          )
        ) : (
          <React.Fragment>
            <Toolbar cmsState={cmsState} namePath={namePath.current} />
            <AboutQuiz
              dataAll={dataAllResult.current}
              updateRealTime={updateRealTime}
              cmsState={cmsState}
              namePath={namePath.current}
            />
            <PersionalQuiz
              dataAll={dataAllResult.current}
              updateRealTime={updateRealTime}
              cmsState={cmsState}
            />
            <ScentQuiz
              dataAll={dataAllResult.current}
              updateRealTime={updateRealTime}
              onClickDiscover={onClickDiscover}
              cmsState={cmsState}
              namePath={namePath.current}
            />
          </React.Fragment>
        )
      }
    </div>
  );
}

function Quizv4() {
  return (
    <RecoilRoot>
      <Quizv4Wrap />
    </RecoilRoot>
  );
}

export default Quizv4;
