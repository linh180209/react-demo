import { scrollToTargetAdjusted } from '../../Redux/Helpers';

export const scrollToNext = (id) => {
  setTimeout(() => {
    scrollToTargetAdjusted(id);
  }, 200);
};

export const test = () => {};
