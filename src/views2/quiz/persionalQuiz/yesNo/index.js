import classnames from 'classnames';
import React, { useEffect, useState } from 'react';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import ItemYesNo from '../../components/itemYesNo';
import './styles.scss';

function YesNo(props) {
  const [addAnimation, setAddAnimation] = useState();
  const description = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'If you are hesitating');

  const onClick = (name, value, idIndex) => {
    setAddAnimation(value);
    props.onClick(name, value, idIndex);
  };

  useEffect(() => {
    if (addAnimation) {
      setTimeout(() => {
        setAddAnimation(undefined);
      }, 800);
    }
  }, [addAnimation]);

  return (
    <div
      className={classnames(props.isShow ? 'yes-no-quiz height-quiz' : 'hidden')}
    >
      <div id={`step-${props.idIndex}-quiz`} className="index-step" />
      <div className="title-question-quiz">
        <div className="title-q tc">
          {props.title}
        </div>
        <div className="des-q tc">
          {description}
        </div>
      </div>
      <div className={classnames('answers-quiz', addAnimation ? 'layout-animation' : '')}>
        <ItemYesNo
          name={props.name}
          title={props.answer1}
          value={props.idAnswer1}
          onClick={onClick}
          idIndex={props.idIndex}
          icon={props.icon1}
          active={props.dataSelected === props.idAnswer1}
          dataGtmtracking={props.dataGtmtracking}
        />
        <ItemYesNo
          right
          name={props.name}
          title={props.answer2}
          value={props.idAnswer2}
          onClick={onClick}
          idIndex={props.idIndex}
          icon={props.icon2}
          active={props.dataSelected === props.idAnswer2}
        />
      </div>
    </div>
  );
}

export default YesNo;
