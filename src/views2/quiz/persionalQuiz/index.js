import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useRecoilState, useRecoilValue } from 'recoil';
import { getNameFromButtonBlock, segmentTrackPersonalityQuizStepNew } from '../../../Redux/Helpers';
import { useMergeState } from '../../../Utils/customHooks';
import { generateQuestions } from '../../../views/Quiz/Questions/AboutYou/handler';
import ScreenInfo from '../components/screenInfo';
import { scrollToNext } from '../handler';
import {
  stepQuestionRecoil, listPersionalQuizRecoil, totalAboutQuizRecoil,
} from '../recoil';
import './styles.scss';
import YesNo from './yesNo';

function PersionalQuiz(props) {
  const answers = useSelector(state => state.answers);
  const [stepQuestion, setStepQuestion] = useRecoilState(stepQuestionRecoil);
  const [listPersionQuiz, setListPersionQuiz] = useRecoilState(listPersionalQuizRecoil);
  const totalAboutQuiz = useRecoilValue(totalAboutQuizRecoil);
  const [state, setState] = useMergeState({
    resultYesNo: [],
  });

  const updateStepQuestion = (idIndex) => {
    setStepQuestion(idIndex + 1);
  };

  const onClickYesNo = (name, value, idIndex) => {
    if (state.resultYesNo.length > parseInt(name, 10)) {
      state.resultYesNo[parseInt(name, 10)] = value;
    } else {
      state.resultYesNo.push(value);
    }
    segmentTrackPersonalityQuizStepNew({
      name: 'personality',
      value,
      questionId: name,
      stepId: idIndex + 1,
    });
    // generate again question
    const list = generateQuestions(answers, state.resultYesNo, parseInt(name, 10) + 1, true);
    setListPersionQuiz(list);

    // increate index question
    updateStepQuestion(idIndex);

    setState({ resultYesNo: state.resultYesNo });
    _.assign(props.dataAll, { resultYesNo: state.resultYesNo });
    if (idIndex === totalAboutQuiz + 5 - 1) {
      setTimeout(() => {
        scrollToNext('id-header-scent-quizv4');
      }, 500);
    } else {
      setTimeout(() => {
        scrollToNext(`step-${idIndex + 1}-quiz`);
      }, 500);
    }

    // update data
    props.updateRealTime();
  };

  useEffect(() => {
    if (answers?.length > 0) {
      const list = generateQuestions(answers, state.resultYesNo, 0, true);
      setListPersionQuiz(list);
    }
  }, [answers]);
  const titleInfo = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Now, let is dive');
  return (
    <div className="persional-quiz">
      <ScreenInfo
        id="id-header-persional-quizv4"
        titleInfo={titleInfo}
        isShow={stepQuestion >= totalAboutQuiz}
        eleScroll={`step-${totalAboutQuiz}-quiz`}
      />
      {
        _.map(listPersionQuiz, (d, index) => (
          <YesNo
            title={d.title}
            idIndex={totalAboutQuiz + index}
            name={d.name}
            numberQuestion={d.numberQuestion}
            answer1={d.answer1}
            answer2={d.answer2}
            idAnswer1={d.idAnswer1}
            idAnswer2={d.idAnswer2}
            icon1={d.icon1}
            icon2={d.icon2}
            onClick={onClickYesNo}
            dataSelected={index < state.resultYesNo.length ? state.resultYesNo[index] : undefined}
            cmsState={props.cmsState}
            isShow={stepQuestion >= totalAboutQuiz + index}
          />
        ))
      }
    </div>
  );
}

export default PersionalQuiz;
