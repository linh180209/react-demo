import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import classnames from 'classnames';
import React from 'react';
import './styles.scss';

function ItemFamily(props) {
  return (
    <div
      onClick={props.onClick}
      className={classnames('item-family', props.active ? 'active' : '')}
    >
      <img src={props.image} alt="icon" />
      <div className="text-icon">
        <div>
          {props.name}
        </div>
        {
          props.active && (
            <CheckCircleOutlineIcon />
          )
        }
      </div>
    </div>
  );
}

export default ItemFamily;
