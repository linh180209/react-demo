import classnames from 'classnames';
import React from 'react';
import './styles.scss';

function ItemStrong(props) {
  return (
    <div
      onMouseEnter={props.onMouseEnter}
      onMouseLeave={props.onMouseLeave}
      onClick={props.onClick}
      className={classnames('item-strong', props.className, props.active ? 'active' : '')}
    >
      <div className="list-col">
        <div className="content-color" />
      </div>
      <div className="text-title">
        {props.title}
      </div>
    </div>
  );
}

export default ItemStrong;
