import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import CheckCircleOutlinedIcon from '@mui/icons-material/CheckCircleOutlined';
import './styles.scss';

function QuestionGender(props) {
  return (
    <div
      onClick={props.onClick}
      className={classnames('question-gender', props.className, props.isActive ? 'active' : '')}
    >
      <div className="content-question">
        <div className="image-icon">
          <img src={props.icon} alt="icon" />
        </div>
        <div>
          {props.name}
        </div>
        <CheckCircleOutlinedIcon />
      </div>
    </div>
  );
}

export default QuestionGender;
