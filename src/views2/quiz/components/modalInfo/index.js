import CloseIcon from '@mui/icons-material/Close';
import Dialog from '@mui/material/Dialog';
import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import ButtonCT from '../../../../componentsv2/buttonCT';
import './styles.scss';

function ModalInfo(props) {
  const { onClose, selectedValue, open } = props;

  const handleClose = () => {
    onClose(selectedValue);
  };

  return (
    <Dialog onClose={handleClose} open={open} className="modal-info-quiz">
      <div className="modal-title">
        <div className="title-modal">
          {props.dataCMS?.header?.header_text}
        </div>
        <ButtonCT color="secondary" className="bt-close" onlyIcon={<CloseIcon />} onClick={handleClose} />
      </div>
      <div className="modal-content-quiz">
        <div className="des-modal">
          {ReactHtmlParser(props.dataCMS?.description2)}
        </div>
        <div className="image-modal">
          <img src={props.dataCMS?.image?.image} alt="info" />
        </div>
      </div>
    </Dialog>
  );
}

export default ModalInfo;
