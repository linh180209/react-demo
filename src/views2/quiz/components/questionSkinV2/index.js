import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import _ from 'lodash';
import './styles.scss';

const colorQuizs = [
  {
    title: 'fair, ivory, porcelain',
    colorText: '#2C2C22',
    color: '#F7D1B2',
  },
  {
    title: 'beige, sand, medium white',
    colorText: '#2C2C22',
    color: '#E9B78F',
  },
  {
    title: 'honey, gold, light tan',
    colorText: '#2C2C22',
    color: '#D09F7C',
  },
  {
    title: 'moderate brown, chestnut, amber',
    colorText: '#EBD8B8',
    color: '#BD7852',
  },
  {
    title: 'dark brown, espresso',
    colorText: '#EBD8B8',
    color: '#A65F2C',
  },
];
function QuestionSkinV2(props) {
  const [valueSeleted, setValueSeleted] = useState();
  const [isHiddenBg, setIsHiddenBg] = useState(false);

  const onClick = (value) => {
    setValueSeleted(value);
    setIsHiddenBg(false);
    props.onClick(props.name, value?.color, props.idIndex);
  };

  useEffect(() => {
    if (valueSeleted) {
      setTimeout(() => {
        setIsHiddenBg(true);
      }, 1000);
    }
  }, [valueSeleted]);

  return (
    <React.Fragment>
      <div className={classnames('color-selected', valueSeleted ? 'active' : '', isHiddenBg ? 'hidden-active' : '')} style={{ background: valueSeleted?.color, color: valueSeleted?.colorText }}>
        {valueSeleted?.title}
        <CheckCircleOutlineIcon style={{ color: valueSeleted?.colorText, marginTop: '12px', fontSize: '40px' }} />
      </div>
      <div className="question-skin-quiz-v2">
        {
        _.map(colorQuizs, d => (
          <button
            type="button"
            className={classnames('bt-color-quiz', props.dataSelected === d.color ? 'active' : '')}
            style={{ background: d.color, color: d.colorText }}
            onClick={() => onClick(d)}
          >
            {
              d.title
            }
            {
              props.dataSelected === d.color && (
                <CheckCircleOutlineIcon style={{ color: valueSeleted?.colorText, marginTop: '12px', fontSize: '40px' }} />
              )
            }
          </button>
        ))
      }
      </div>
    </React.Fragment>
  );
}

export default QuestionSkinV2;
