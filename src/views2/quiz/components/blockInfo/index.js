import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import classnames from 'classnames';
import ReactHtmlParser from 'react-html-parser';
import React, { useState } from 'react';
import { getNameFromButtonBlock, segmentTrackPersonalityQuizTooltipOpened } from '../../../../Redux/Helpers';
import './styles.scss';
import { isMobile767fn, isMobile991fn } from '../../../../DetectScreen';
import ModalInfo from '../modalInfo';
import ButtonCT from '../../../../componentsv2/buttonCT';

function BlockInfo(props) {
  const { idIndex } = props;
  const [openLearMore, setOpenLearMore] = useState(false);
  const [isOpenModal, setIsOpenModal] = useState(false);

  const toggleOpen = () => {
    setOpenLearMore(!openLearMore);
  };

  const onCloseModal = () => {
    setIsOpenModal(false);
  };

  const onOpenModal = () => {
    setIsOpenModal(true);
    segmentTrackPersonalityQuizTooltipOpened(idIndex);
  };

  return (
    <div className={classnames('block-info-quiz', props.isShort ? 'block-short' : '')}>
      <ModalInfo
        dataCMS={props.dataCMS}
        open={isOpenModal}
        onClose={onCloseModal}
      />
      <div className={classnames('content-info', props.isShowNext ? 'p-show-button' : '')}>
        <div className="title-quiz">
          {props.dataCMS?.header?.header_text}
        </div>
        {
          props.dataCMS?.description && (
            <div className="text-d">
              {ReactHtmlParser(props.dataCMS?.description)}
            </div>
          )
        }
        <div className={classnames('button-more-info', isMobile767fn() ? 'hidden' : '')}>
          <div>
            {getNameFromButtonBlock(props.dataCMS?.buttons, 'Select only 1')}
          </div>
          <button
            onClick={isMobile991fn() || props.isShort ? onOpenModal : toggleOpen}
            className={classnames('button-bg__none', openLearMore ? 'open-more' : '')}
            type="button"
          >
            <div>
              {getNameFromButtonBlock(props.dataCMS?.buttons, 'Learn More')}
            </div>
            <ExpandLessIcon />
          </button>
        </div>
        <div className={classnames('cart-info', openLearMore ? 'is-open' : '')}>
          <div className="text-info">
            <div>
              {props.dataCMS?.header2?.header_text}
            </div>
            <div>
              {ReactHtmlParser(props.dataCMS?.description2)}
            </div>
          </div>
          <div className="image-info">
            <img src={props.dataCMS?.image?.image} alt="icon" />
          </div>
        </div>
        {
          props.isShowNext && (
            <div className="list-button">
              <ButtonCT name="Next" onClick={props.onClickButton} />
            </div>
          )
        }
      </div>
    </div>
  );
}

export default BlockInfo;
