import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import './styles.scss';

function ItemWearPerfume(props) {
  return (
    <div
      onClick={props.onClick}
      className={classnames('item-wear-perfume', props.className, props.active ? 'active' : '')}
    >
      <img src={props.icon} alt="icon" />
      <div className="title-text">
        {props.title}
      </div>
      <div className="des-text">
        {props.description}
      </div>
      {
        props.active && (
          <CheckCircleOutlineIcon />
        )
      }
    </div>
  );
}

export default ItemWearPerfume;
