import classnames from 'classnames';
import React, { useRef, useEffect } from 'react';
import iconInfo from '../../../../imagev2/svg/icon-start-quiz.svg';
import useWindowEvent from '../../../../Utils/useWindowEvent';
import { scrollToNext } from '../../handler';
import './styles.scss';
// import { segmentTrackPersonalityQuizStepNew } from '../../../../Redux/Helpers';

function ScreenInfo(props) {
  const timer = useRef();
  const onClick = () => {
    scrollToNext(props.eleScroll);
  };

  const mouseEnter = () => {
    if (timer.current) {
      clearTimeout(timer.current);
    }
    timer.current = setTimeout(() => {
      onClick();
    }, 2000);
  };

  const mouseLeave = () => {
    if (timer.current) {
      clearTimeout(timer.current);
    }
  };

  const handleScroll = () => {
    if (props.isShow) {
      const ele = document.getElementById(props.id);
      if (ele.getBoundingClientRect().bottom > -1 && ele.getBoundingClientRect().bottom < 100) {
        mouseEnter();
      } else {
        mouseLeave();
      }
    }
  };

  // unmount remove timeout
  useEffect(() => {
    handleScroll();
    return (() => {
      mouseLeave();
    });
  }, []);

  useWindowEvent('scroll', handleScroll, document.getElementById('quiz-4'));

  return (
    <div
      onMouseEnter={mouseEnter}
      // onMouseLeave={mouseLeave}
      className={classnames(props.isShow ? 'screen-info' : 'hidden')}
    >
      <div id={props.id} className="index-step" />
      <div className="text">
        {props.titleInfo}
      </div>
      <button
        className="button-bg__none"
        type="button"
        onClick={onClick}
      >
        <img src={iconInfo} alt="icon" />
      </button>
    </div>
  );
}

export default ScreenInfo;
