import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import classnames from 'classnames';

import './styles.scss';

const colorQuizs = ['#fff4f2', '#f2d4ca', '#e0afa8', '#d4a093', '#c68d82', '#a36a5f', '#905c4f', '#653d35', '#4e2f2a', '#281b19'];
function QuestionSkin(props) {
  const [valueSeleted, setValueSeleted] = useState();
  const [isHiddenBg, setIsHiddenBg] = useState(false);

  const onClick = (value) => {
    setValueSeleted(value);
    setIsHiddenBg(false);
    props.onClick(props.name, value, props.idIndex);
  };

  useEffect(() => {
    if (valueSeleted) {
      setTimeout(() => {
        setIsHiddenBg(true);
      }, 1000);
    }
  }, [valueSeleted]);

  return (
    <React.Fragment>
      <div className={classnames('color-selected', valueSeleted ? 'active' : '', isHiddenBg ? 'hidden-active' : '')} style={{ background: valueSeleted }} />
      <div className="question-skin-quiz">
        {
        _.map(colorQuizs, d => (
          <button
            type="button"
            className={classnames('bt-color-quiz', props.dataSelected === d ? 'active' : '')}
            style={{ background: d }}
            onClick={() => onClick(d)}
          >
            {
              props.dataSelected === d && (
                <CheckCircleOutlineIcon style={{ color: '#B79B53' }} />
              )
            }
          </button>
        ))
      }
      </div>
    </React.Fragment>

  );
}

export default QuestionSkin;
