import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import icon from '../../../../imagev2/svg/ic-question-1-0.svg';
import './styles.scss';

function ItemYesNo(props) {
  const onClick = () => {
    props.onClick(props.name, props.value, props.idIndex);
  };

  return (
    <div
      data-gtmtracking={props.dataGtmtracking}
      className={classnames('item-yes-no', props.right ? 'right' : '', props.active ? 'active' : 'un-active')}
      onClick={onClick}
    >
      <img src={props.icon} alt="icon" />
      <div className="text-info-yes-no tc">
        {props.title}
      </div>
      {
        props.active && (
          <CheckCircleOutlineIcon />
        )
      }
    </div>
  );
}

export default ItemYesNo;
