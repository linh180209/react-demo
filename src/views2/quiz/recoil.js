import { atom, selector } from 'recoil';
import _ from 'lodash';

export const stepQuestionRecoil = atom({
  key: 'stepQuestionRecoil',
  default: 0,
});

export const listAboutQuizRecoil = atom({
  key: 'listAboutQuizRecoil',
  default: [],
});

export const listPersionalQuizRecoil = atom({
  key: 'listPersionalQuizRecoil',
  default: [],
});

export const listScentQuizRecoil = atom({
  key: 'listScentQuizRecoil',
  default: [],
});

export const totalAboutQuizRecoil = selector({
  key: 'totalAboutQuizRecoil',
  get: ({ get }) => {
    const listAboutQuiz = get(listAboutQuizRecoil);
    return listAboutQuiz.length;
  },
});

export const totalPersionalQuizRecoil = selector({
  key: 'totalPersionalQuizRecoil',
  get: ({ get }) => {
    const list = get(listPersionalQuizRecoil);
    return list.length;
  },
});
