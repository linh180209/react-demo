import classnames from 'classnames';
import _ from 'lodash';
import React, { useState } from 'react';
import { isMobile991fn } from '../../../../DetectScreen';
import icon1 from '../../../../imagev2/svg/ic-wear-1.svg';
import icon2 from '../../../../imagev2/svg/ic-wear-2.svg';
import iconInfo from '../../../../imagev2/svg/icon-start-quiz.svg';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import BlockInfo from '../../components/blockInfo';
import ItemWearPerfume from '../../components/itemWearPerfume';
import './styles.scss';

const dataWear = [{
  id: 2,
  name: 'WORK & SOCIAL',
  description: 'Elegant & Confident',
  icon: icon1,
}, {
  id: 3,
  name: 'Seduction',
  description: 'Attractive & Sexy',
  icon: icon2,
}];

function WearPerfume(props) {
  const [value, setValue] = useState(props.value || []);

  const isExistItem = d => _.find(value, x => x.id === d.id);

  const onClickNext = () => {
    props.onClick(props.name, value, props.idIndex);
  };

  const onClick = (data) => {
    setValue(_.cloneDeep([data]));
    setTimeout(() => {
      onClickNext();
    }, 300);
  };

  const bannerBlock = _.find(props.cmsState?.bannerBlocks, x => x?.value?.image?.caption === 'mixes');
  return (
    <div
      className={classnames(props.isShow ? 'wear-perfume height-quiz' : 'hidden')}
    >
      <div id={`step-${props.idIndex}-quiz`} className="index-step" />
      <BlockInfo
        dataCMS={bannerBlock?.value}
        idIndex={props.idIndex + 1}
      />
      <div className={classnames('info-right')}>
        {
          _.map(dataWear, (d, index) => (
            <ItemWearPerfume
              title={getNameFromButtonBlock(props.cmsState?.buttonBlocks, d.name)}
              description={getNameFromButtonBlock(props.cmsState?.buttonBlocks, d.description)}
              active={isExistItem(d)}
              onClick={() => onClick(d)}
              icon={d.icon}
              className={index > 0 ? 'div-bottom' : ''}
            />
          ))
        }
      </div>
    </div>
  );
}

export default WearPerfume;
