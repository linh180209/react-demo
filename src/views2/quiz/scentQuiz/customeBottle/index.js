import AddPhotoAlternateOutlinedIcon from '@mui/icons-material/AddPhotoAlternateOutlined';
import ModeEditOutlineOutlinedIcon from '@mui/icons-material/ModeEditOutlineOutlined';
import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect } from 'react';
import BottleCustom from '../../../../components/B2B/CardItem/bottleCustom';
import ButtonCT from '../../../../componentsv2/buttonCT';
import CustomeBottleV4 from '../../../../componentsv2/customeBottleV4';
import { GET_ALL_PRODUCTS_BOTTLE } from '../../../../config';
import { COLOR_CUSTOME, FONT_CUSTOME } from '../../../../constants';
import { isMobile767fn, isMobile991fn } from '../../../../DetectScreen';
import icBottle from '../../../../image/img-bottle.png';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../../Redux/Helpers/notification';
import { useMergeState } from '../../../../Utils/customHooks';
import BlockInfo from '../../components/blockInfo';
import './styles.scss';

const defaultDataCustom = {
  currentImg: '',
  nameBottle: '',
  imagePremade: undefined,
  font: FONT_CUSTOME.JOST,
  color: COLOR_CUSTOME.BLACK,
};

function CustomeBottle(props) {
  const [state, setState] = useMergeState({
    dataCustom: props.value || _.cloneDeep(defaultDataCustom),
    isOpenCustomeBottle: false,
    arrayThumbs: [],
  });

  const fetchBottle = async () => {
    const options = {
      url: GET_ALL_PRODUCTS_BOTTLE,
      method: 'GET',
    };
    try {
      const result = await fetchClient(options);
      if (result && !result.isError && result.length > 0) {
        const thumbs = [];
        _.forEach(result[0]?.images, (x) => {
          if (x.type === 'suggestion') {
            thumbs.push(x);
          }
        });
        setState({ arrayThumbs: thumbs });
      }
    } catch (err) {
      toastrError(err.message);
    }
  };

  const onSaveCustomer = (data) => {
    const { dataCustom } = state;
    const {
      image, name, color, font, imagePremade,
    } = data;
    dataCustom.color = color;
    dataCustom.font = font;
    dataCustom.imagePremade = imagePremade;
    if (_.isEmpty(image)) {
      dataCustom.currentImg = undefined;
      dataCustom.nameBottle = name;
      setState({ dataCustom });
      return;
    }
    dataCustom.currentImg = image;
    dataCustom.nameBottle = name;
    setState({ dataCustom });
  };

  const onClickSave = () => {
    const { dataCustom } = state;
    props.onClick(props.name, dataCustom, props.idIndex);
  };

  const onClickSkip = () => {
    setState({ dataCustom: _.cloneDeep(defaultDataCustom) });
    props.onClick(props.name, _.cloneDeep(defaultDataCustom), props.idIndex);
  };

  const handleCropImage = (img) => {
    state.dataCustom.currentImg = img;
    setState({ dataCustom: state.dataCustom });
  };

  useEffect(() => {
    fetchBottle();
  }, []);

  const bannerBlock = _.find(props.cmsState?.bannerBlocks, x => x?.value?.image?.caption === 'customeBottle');
  return (
    <div
      className={classnames(props.isShow ? 'custome-bottle-quiz height-quiz' : 'hidden')}
    >
      <div id={`step-${props.idIndex}-quiz`} className="index-step" />
      <BlockInfo
        dataCMS={bannerBlock?.value}
        idIndex={props.idIndex + 1}
      />
      <div className={classnames('info-right')}>
        <div className="custom-bottle-question-quiz">
          {
            state.isOpenCustomeBottle && (
            <CustomeBottleV4
              cmsCommon={props.cmsCommon}
              arrayThumbs={state.arrayThumbs}
              handleCropImage={handleCropImage}
              currentImg={state.dataCustom.currentImg}
              nameBottle={state.dataCustom.nameBottle}
              font={state.dataCustom.font}
              color={state.dataCustom.color}
              closePopUp={() => {
                setState({ isOpenCustomeBottle: false });
              }
              }
              onSave={onSaveCustomer}
              dataGtmtracking="funnel-1-step-15-save-customize"
              dataGtmtrackingClose="funnel-1-step-15-close-customize"

            // itemBottle={{
            //   price: this.bottle ? this.bottle.items[0].price : '',
            // }}
            // cmsTextBlocks={textBlock}
            // name1={combos && combos.length > 0 ? combos[0].name : ''}
            // name2={combos && combos.length > 1 ? combos[1].name : ''}
              name1=""
            />
            )
          }
          <div className="div-bottle">
            <BottleCustom
              isImageText={!!state.dataCustom.currentImg}
              image={state.dataCustom.currentImg}
              isBlack
              isDisplayName
              font={state.dataCustom.font}
              color={state.dataCustom.color}
          // eauBt={eauBt}
              eauBt="Eau_de_parfum"
          // mlBt={mlBt}
              mlBt="25ml"
              name={state.dataCustom.nameBottle}
            />
          </div>
          {
            (state.dataCustom?.currentImg || state.dataCustom?.nameBottle) ? (
              <div className="custome-done">
                <img className={state.dataCustom?.currentImg ? 'cover-image' : ''} src={state.dataCustom?.currentImg || icBottle} alt="" />
                <div className="text-custome div-col">
                  <div className="title-custome">
                    {getNameFromButtonBlock(props?.cmsState?.buttonBlocks, 'custom design')}
                  </div>
                  <div className="name-color div-row" style={{ fontFamily: state.dataCustom.font }}>
                    {state.dataCustom?.nameBottle}
                    <div className="space-color" style={{ background: state.dataCustom.color }} />
                  </div>
                </div>
                {
                  isMobile991fn() ? (
                    <ButtonCT
                      onClick={() => setState({ isOpenCustomeBottle: true })}
                      onlyIcon={<ModeEditOutlineOutlinedIcon />}
                      className="edit-custom"
                    />
                  ) : (
                    <ButtonCT
                      onClick={() => setState({ isOpenCustomeBottle: true })}
                      variant="outlined"
                      name={getNameFromButtonBlock(props?.cmsState?.buttonBlocks, 'Edit')}
                      size="medium"
                      endIcon={<ModeEditOutlineOutlinedIcon />}
                    />
                  )
                }

              </div>
            ) : (
              <button
                type="button"
                className="button-edit-bottle"
                onClick={() => setState({ isOpenCustomeBottle: true })}
              >
                {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'ENGRAVE BOTTLE')}
                <AddPhotoAlternateOutlinedIcon />
              </button>
            )
          }
          <div className="list-button-next">
            {
              isMobile991fn() ? (
                <button
                  onClick={onClickSkip}
                  type="button"
                  className={classnames('button-bg__none bt-skip-quiz-mobile', state.dataCustom?.currentImg || state.dataCustom?.nameBottle ? 'hidden' : '')}
                >
                  {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Skip')}
                </button>
              ) : (
                <ButtonCT
                  onClick={onClickSkip}
                  name={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Skip')}
                  variant="outlined"
                  size="medium"
                  className="bt-skip-quiz"
                />
              )
            }

            {
              (!isMobile991fn() || state.dataCustom?.currentImg || state.dataCustom?.nameBottle) && (
                <ButtonCT
                  onClick={onClickSave}
                  name={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Next')}
                  size="medium"
                  className="bt-next-quiz"
                />
              )
            }
          </div>
        </div>
      </div>
    </div>
  );
}

export default CustomeBottle;
