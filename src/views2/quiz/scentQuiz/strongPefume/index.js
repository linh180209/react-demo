import classnames from 'classnames';
import _ from 'lodash';
import React, { useState } from 'react';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import BlockInfo from '../../components/blockInfo';
import ItemStrong from '../../components/itemStrong';
import './styles.scss';

function StrongPerfume(props) {
  const [valueSeleted, setValueSeleted] = useState(props.value);
  const [indexHover, setIndexHover] = useState(0);

  const onClick = (value) => {
    setValueSeleted(value);
    props.onClick(props.name, value, props.idIndex);
  };

  const onMouseEnter = (index) => {
    console.log('onMouseEnter', index);
    setIndexHover(index);
  };

  const onMouseLeave = () => {
    setIndexHover(0);
  };

  const bannerBlock = _.find(props.cmsState?.bannerBlocks, x => x?.value?.image?.caption === 'strength');
  return (
    <div
      className={classnames(props.isShow ? 'strong-perfume height-quiz' : 'hidden')}
    >
      <div id={`step-${props.idIndex}-quiz`} className="index-step" />

      <BlockInfo
        dataCMS={bannerBlock?.value}
        idIndex={props.idIndex + 1}
      />
      <div className={classnames('info-right')}>
        <div className="group-strong">
          <ItemStrong
            onMouseEnter={() => onMouseEnter(1)}
            onMouseLeave={onMouseLeave}
            title={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Subtle')}
            active={valueSeleted >= 0}
            onClick={() => onClick(0)}
            className={indexHover > 0 ? 'hover-status' : ''}
          />
          <ItemStrong
            onMouseEnter={() => onMouseEnter(2)}
            onMouseLeave={onMouseLeave}
            title={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Balanced')}
            active={valueSeleted >= 1}
            onClick={() => onClick(1)}
            className={indexHover > 1 ? 'hover-status' : ''}
          />
          <ItemStrong
            onMouseEnter={() => onMouseEnter(3)}
            onMouseLeave={onMouseLeave}
            title={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Strong')}
            active={valueSeleted >= 2}
            onClick={() => onClick(2)}
            className={indexHover > 2 ? 'hover-status' : ''}
          />
        </div>
      </div>
    </div>
  );
}

export default StrongPerfume;
