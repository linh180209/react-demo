import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { useRecoilState, useRecoilValue } from 'recoil';
import ScreenInfo from '../components/screenInfo';
import WearPerfume from './wearPerfume';
import PerfumeFamily from './perfumeFamily';
import StrongPerfume from './strongPefume';
import CustomeBottle from './customeBottle';
import EnterEmail from './enterEmail';
import './styles.scss';
import {
  listScentQuizRecoil, stepQuestionRecoil, totalAboutQuizRecoil, totalPersionalQuizRecoil,
} from '../recoil';
import { useMergeState } from '../../../Utils/customHooks';
import { scrollToNext } from '../handler';
import { getNameFromButtonBlock, segmentTrackPersonalityQuizStepNew } from '../../../Redux/Helpers';

const listQuestion = ['strength', 'mixes', 'families', 'customeBottle', 'email'];
const listQuestionV2 = ['strength', 'mixes', 'families'];
function ScentQuiz(props) {
  const [stepQuestion, setStepQuestion] = useRecoilState(stepQuestionRecoil);
  const [listScentQuiz, setListScentQuiz] = useRecoilState(listScentQuizRecoil);
  const totalPersionalQuiz = useRecoilValue(totalPersionalQuizRecoil);
  const totalAboutQuiz = useRecoilValue(totalAboutQuizRecoil);

  const [state, setState] = useMergeState({
    totalQuestion: 0,
  });

  const updateStepQuestion = (idIndex) => {
    setStepQuestion(idIndex + 1);
  };

  const onClick = (name, value, idIndex) => {
    console.log('onClick', name, value, idIndex);
    segmentTrackPersonalityQuizStepNew({
      name,
      value,
    });
    // increate index question
    updateStepQuestion(idIndex);

    setState({ [name]: value });
    _.assign(props.dataAll, { [name]: value });

    scrollToNext(`step-${idIndex + 1}-quiz`);

    // update data
    if (idIndex === (totalPersionalQuiz + totalAboutQuiz + listScentQuiz.length) - 1) {
      // sephora: skip some question => update data then go to result page
      props.updateRealTime(true);
    } else {
      props.updateRealTime();
    }
  };

  const generateQuestions = (d, index) => {
    switch (d) {
      case 'strength':
        return (
          <StrongPerfume
            idIndex={state.totalQuestion + index}
            name="strength"
            onClick={onClick}
            cmsState={props.cmsState}
            isShow={stepQuestion >= (state.totalQuestion + index)}
          />
        );
      case 'mixes':
        return (
          <WearPerfume
            idIndex={state.totalQuestion + index}
            name="mixes"
            onClick={onClick}
            cmsState={props.cmsState}
            isShow={stepQuestion >= (state.totalQuestion + index)}
          />
        );
      case 'families':
        return (
          <PerfumeFamily
            idIndex={state.totalQuestion + index}
            name="families"
            onClick={onClick}
            namePath={props.namePath}
            cmsState={props.cmsState}
            isShow={stepQuestion >= (state.totalQuestion + index)}
          />
        );
      case 'customeBottle':
        return (
          <CustomeBottle
            idIndex={state.totalQuestion + index}
            name="customeBottle"
            onClick={onClick}
            cmsState={props.cmsState}
            isShow={stepQuestion >= (state.totalQuestion + index)}
          />
        );
      case 'email':
        return (
          <EnterEmail
            idIndex={state.totalQuestion + index}
            dataAll={props.dataAll}
            onClickDiscover={props.onClickDiscover}
            cmsState={props.cmsState}
            isShow={stepQuestion >= (state.totalQuestion + index)}
          />
        );
      default:
        return <div />;
    }
  };

  useEffect(() => {
    setState({ totalQuestion: (totalAboutQuiz + totalPersionalQuiz) });
  }, [totalAboutQuiz, totalPersionalQuiz]);

  useEffect(() => {
    setListScentQuiz(props.namePath === 'quiz' ? listQuestion : listQuestionV2);
  }, []);

  const titleInfo = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Splendid');
  return (
    <div className="scent-quiz">
      <ScreenInfo
        id="id-header-scent-quizv4"
        titleInfo={titleInfo}
        isShow={stepQuestion >= state.totalQuestion}
        eleScroll={`step-${state.totalQuestion}-quiz`}
      />
      {
        _.map(listScentQuiz, (d, index) => (
          generateQuestions(d, index)
        ))
      }
    </div>
  );
}

export default ScentQuiz;
