import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import ButtonCT from '../../../../componentsv2/buttonCT';
import { GET_ALL_SCENT_FAMILY_V4, GET_ALL_SCENT_FAMILY_V4_PARTNER } from '../../../../config';
import { getNameFromButtonBlock, scrollToTargetAdjusted } from '../../../../Redux/Helpers';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../../Redux/Helpers/notification';
import BlockInfo from '../../components/blockInfo';
import ItemFamily from '../../components/itemFamily';
import './styles.scss';

function PerfumeFamily(props) {
  const [dataProduct, setDataProduct] = useState([]);
  const [value, setValue] = useState(props.value || []);

  const onClickNext = () => {
    props.onClick(props.name, value, props.idIndex);
  };

  const onClickSkip = () => {
    setValue([]);
    props.onClick(props.name, [], props.idIndex);
  };

  const isExistItem = d => _.find(value, x => x.id === d.id);

  const onSelection = (d) => {
    if (isExistItem(d)) {
      _.remove(value, x => x.id === d.id);
    } else {
      value.push(d);
      scrollToTargetAdjusted('id-bt-next-family');
    }
    setValue([...value]);
  };

  const fetchDataFamily = () => {
    const options = {
      url: props.namePath === 'quiz' ? GET_ALL_SCENT_FAMILY_V4 : GET_ALL_SCENT_FAMILY_V4_PARTNER.replace('{partner}', props.namePath),
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        setDataProduct(result);
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      console.log('err', err);
      toastrError(err.message);
    });
  };

  useEffect(() => {
    fetchDataFamily();
  }, []);

  const bannerBlock = _.find(props.cmsState?.bannerBlocks, x => x?.value?.image?.caption === 'families');

  return (
    <div
      className={classnames(props.isShow ? 'perfume-family height-quiz' : 'hidden')}
    >
      <div id={`step-${props.idIndex}-quiz`} className="index-step" />
      <BlockInfo
        dataCMS={bannerBlock?.value}
        isShort
        idIndex={props.idIndex + 1}
      />
      <div className={classnames('info-right')}>
        {
          _.map(dataProduct, d => (
            <ItemFamily
              image={d.image}
              name={d.name}
              onClick={() => onSelection(d)}
              active={isExistItem(d)}
            />
          ))
        }
      </div>
      <div className="list-button">
        <ButtonCT
          name={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Skip')}
          variant="outlined"
          onClick={onClickSkip}
        />
        <ButtonCT
          name={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Next')}
          onClick={onClickNext}
        />
      </div>
    </div>
  );
}

export default PerfumeFamily;
