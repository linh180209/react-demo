import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import ButtonCT from '../../../../componentsv2/buttonCT';
import CheckBoxCT from '../../../../componentsv2/checkBoxCT';
import InputCT from '../../../../componentsv2/inputCT';
import { isMobile767fn, isMobile991fn } from '../../../../DetectScreen';
import { addClassIntoElemenetID, getNameFromButtonBlock, removeClassIntoElemenetID } from '../../../../Redux/Helpers';
import { useMergeState } from '../../../../Utils/customHooks';
import './styles.scss';

function EnterEmail(props) {
  // redux
  const login = useSelector(state => state.login);
  const basket = useSelector(state => state.basket);

  const [state, setState] = useMergeState({
    email: props.dataAll?.email,
    isAgreeEmail: props.dataAll?.isAgreeEmail,
  });

  const onChangeEmail = (e) => {
    const { value } = e.target;
    setState({ email: value });
    _.assign(props.dataAll, { email: value });
  };

  const onCheckBox = () => {
    setState({ isAgreeEmail: !state.isAgreeEmail });
    _.assign(props.dataAll, { isAgreeEmail: !state.isAgreeEmail });
  };

  const addHiddenBgImage = () => {
    if (isMobile767fn() || (!isMobile767fn() && !isMobile991fn())) {
      return;
    }
    const ele = document.getElementById('id-info-right');
    if (ele) {
      if (ele.clientHeight < ((window.screen.height - 72) / 2)) {
        addClassIntoElemenetID('id-bg-cover', 'hidden');
        addClassIntoElemenetID('id-info-right', 'relative-div');
      } else {
        removeClassIntoElemenetID('id-bg-cover', 'hidden');
        removeClassIntoElemenetID('id-info-right', 'relative-div');
      }
    }
  };

  useEffect(() => {
    if (!props.dataAll?.email) {
      const value = login?.user?.email || basket?.email;
      onChangeEmail({
        target: { value },
      });
    }
  }, [login, basket]);

  useEffect(() => {
    addHiddenBgImage();
  }, [props.isShow]);

  const imageData = _.find(props.cmsState?.imagetxtBlocks, x => x.value?.image?.caption === 'enter-email')?.value;
  return (
    <div
      style={isMobile767fn() ? { height: `${window.innerHeight}px` } : {}}
      className={classnames(props.isShow ? 'enter-email height-quiz' : 'hidden')}
    >
      <div id={`step-${props.idIndex}-quiz`} className="index-step" />
      <div className="image-left">
        <img src={imageData?.image?.image} alt="icon" />
        <div id="id-bg-cover" className="bg-cover" />
      </div>
      <div id="id-info-right" className={classnames('info-right')}>
        <div>
          <div className="div-top-text">
            <div className="text-complete">

              {getNameFromButtonBlock(props.cmsState?.buttonBlocks, '100% COMPLETED')}
            </div>
            <div className="text-header">
              {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'preparing your personalised')}

            </div>
            <div className="text-des">
              {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Save your result on your email and receive')}
            </div>
            <div className="list-item ">
              <CheckCircleOutlineIcon />
              {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Your personality decoding')}

            </div>
            <div className="list-item">
              <CheckCircleOutlineIcon />
              {getNameFromButtonBlock(props.cmsState?.buttonBlocks, '10% off your first purchase')}

            </div>
            <div className="list-item">
              <CheckCircleOutlineIcon />
              {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Your fragrance style')}

            </div>
          </div>
          <div className="div-bottom-text">
            <InputCT
              type="text"
              placeholder={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Email')}
              name="email"
              value={state.email}
              onChange={onChangeEmail}
              className="input-email"
            />
            <div className="div-row row-check-box">
              <CheckBoxCT
                onChange={onCheckBox}
                name="create_address"
                checked={state.isAgreeEmail}
            // onChange={props.onClickCreateAddress}
                label=""
                className="medium check-box-email"
              />
              <div className="link-text">
                {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'I’ve read and accept all')}
                {' '}
                <Link to="#">{getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Terms and Conditions')}</Link>
                {' '}
                {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'and')}
                {' '}
                <Link to="#">{getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Privacy Policy')}</Link>
              </div>
            </div>
            <ButtonCT
              disabled={!state.isAgreeEmail}
              name={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Reveal My Result')}
              size="medium"
              onClick={props.onClickDiscover}
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default EnterEmail;
