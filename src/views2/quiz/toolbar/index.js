import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import React from 'react';
import { useHistory } from 'react-router-dom';
import classnames from 'classnames';
import { useRecoilValue } from 'recoil';
import iconStop from '../../../imagev2/svg/icon-bottle-process.svg';
import iconStart from '../../../imagev2/svg/icon-scent-process.svg';
import { getNameFromButtonBlock, gotoHomeAllProduct } from '../../../Redux/Helpers';
import { stepQuestionRecoil } from '../recoil';
import './styles.scss';
import { isBrowser } from '../../../DetectScreen';

const generatePercent = (step) => {
  switch (step) {
    case 0:
      return 8;
    case 1:
      return 17;
    case 2:
      return 26;
    case 3:
      return 30;
    case 4:
      return 35;
    case 5:
      return 40;
    case 6:
      return 45;
    case 7:
      return 50;
    case 8:
      return 57;
    case 9:
      return 63;
    case 10:
      return 69;
    case 11:
      return 74;
    case 12:
      return 100;
    default:
      return 0;
  }
};
const generatePercentV2 = (step) => {
  switch (step) {
    case 0:
      return 8;
    case 1:
      return 20;
    case 2:
      return 30;
    case 3:
      return 40;
    case 4:
      return 50;
    case 5:
      return 60;
    case 6:
      return 70;
    case 7:
      return 80;
    case 8:
      return 90;
    case 9:
      return 100;
    default:
      return 0;
  }
};
const generatePercentShow = (step) => {
  switch (step) {
    case 0:
      return 5;
    case 1:
      return 10;
    case 2:
      return 15;
    case 3:
      return 25;
    case 4:
      return 35;
    case 5:
      return 45;
    case 6:
      return 55;
    case 7:
      return 65;
    case 8:
      return 70;
    case 9:
      return 75;
    case 10:
      return 80;
    case 11:
      return 85;
    case 12:
      return 100;
    default:
      return 0;
  }
};
const generatePercentShowV2 = (step) => {
  switch (step) {
    case 0:
      return 5;
    case 1:
      return 10;
    case 2:
      return 20;
    case 3:
      return 30;
    case 4:
      return 40;
    case 5:
      return 50;
    case 6:
      return 60;
    case 7:
      return 70;
    case 8:
      return 85;
    case 9:
      return 100;
    default:
      return 0;
  }
};
function Toolbar(props) {
  const history = useHistory();
  const stepQuestion = useRecoilValue(stepQuestionRecoil);
  const funcGeneraPercent = props.namePath === 'quiz' ? generatePercent : generatePercentV2;
  const funcGeneraShowPercent = props.namePath === 'quiz' ? generatePercentShow : generatePercentShowV2;
  const listNotTooltip = props.namePath === 'quiz' ? [2, 7, 11, 12] : [4, 9];
  const onClickBack = () => {
    gotoHomeAllProduct(history);
  };
  return (
    <div className="toolbar-quiz">
      <button
        onClick={onClickBack}
        className="button-bg__none bt-back-quizv4"
        type="button"
      >
        <ArrowBackIcon />
        {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'BACK')}
      </button>
      <div className="process-bar-quiz">
        <div className={classnames('bar-gray', !listNotTooltip.includes(stepQuestion) && isBrowser ? 'add-tooltip' : '')}>
          <div dataTooltip={`${funcGeneraShowPercent(stepQuestion)}%`} className="bar-percen" style={{ width: `${funcGeneraPercent(stepQuestion)}%` }} />
        </div>
        <div className="list-image">
          <img src={iconStart} alt="icon" />
          <div className="step">
            <div className={classnames('number', funcGeneraPercent(stepQuestion) >= 25 ? 'active' : '')}>
              1
            </div>
            <div className="text">
              {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'About')}
            </div>
          </div>
          <div className="step">
            <div className={classnames('number', funcGeneraPercent(stepQuestion) >= 50 ? 'active' : '')}>
              2
            </div>
            <div className="text">
              {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Personality')}
            </div>
          </div>
          <div className="step">
            <div className={classnames('number', funcGeneraPercent(stepQuestion) >= 74 ? 'active' : '')}>
              3
            </div>
            <div className="text">
              {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Scents')}
            </div>
          </div>
          <img src={iconStop} alt="icon" />
        </div>
      </div>
    </div>
  );
}

export default Toolbar;
