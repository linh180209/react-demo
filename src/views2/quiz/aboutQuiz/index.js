import React, { useRef, useEffect } from 'react';
import _ from 'lodash';
import { useRecoilState } from 'recoil';
import { useMergeState } from '../../../Utils/customHooks';
import ScreenInfo from '../components/screenInfo';
import GenderQuiz from './genderQuiz';
import HowOld from './howOld';
import SkinTone from './skinTone';
import { stepQuestionRecoil, listAboutQuizRecoil } from '../recoil';
import { scrollToNext } from '../handler';
import { getNameFromButtonBlock, segmentTrackPersonalityQuizStepNew } from '../../../Redux/Helpers';

const questionQuiz = ['age', 'gender', 'skinTone'];
const questionQuizV2 = ['age', 'gender'];

function AboutQuiz(props) {
  const [stepQuestion, setStepQuestion] = useRecoilState(stepQuestionRecoil);
  const [listAboutQuiz, setListAboutQuiz] = useRecoilState(listAboutQuizRecoil);
  const [state, setState] = useMergeState({
    gender: undefined,
    age: 30,
    resultYesNo: [],
  });
  const isRemoveSkinTone = ['sephora', 'lazada'].includes(props.namePath);

  const updateStepQuestion = (idIndex) => {
    setStepQuestion(idIndex + 1);
  };

  const onClick = (name, value, idIndex, isTooltipOpened = false) => {
    // console.log('onClick', name, value, idIndex);
    segmentTrackPersonalityQuizStepNew({
      name,
      value,
      isTooltipOpened,
    });

    // increate index question
    updateStepQuestion(idIndex);

    setState({ [name]: value });
    _.assign(props.dataAll, { [name]: value });

    // scroll to next question
    if (idIndex === listAboutQuiz.length - 1) {
      scrollToNext('id-header-persional-quizv4');
    } else {
      scrollToNext(`step-${idIndex + 1}-quiz`);
    }

    // update data
    props.updateRealTime();
  };

  const generateQuestions = (d, index) => {
    switch (d) {
      case 'age':
        return (
          <HowOld
            dataSelected={state.age}
            onClick={onClick}
            name="age"
            idIndex={0}
            cmsState={props.cmsState}
            isShow={stepQuestion >= index}
          />
        );
      case 'gender':
        return (
          <GenderQuiz
            dataSelected={state.gender}
            onClick={onClick}
            name="gender"
            idIndex={1}
            cmsState={props.cmsState}
            isShow={stepQuestion >= index}
          />
        );
      case 'skinTone':
        return (
          <SkinTone
            dataSelected={state.skinTone}
            onClick={onClick}
            name="skinTone"
            idIndex={2}
            cmsState={props.cmsState}
            isShow={stepQuestion >= index}
          />
        );
      default:
        return <div />;
    }
  };

  useEffect(() => {
    setListAboutQuiz(isRemoveSkinTone ? questionQuizV2 : questionQuiz);
  }, []);

  const titleInfo = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'First, we will need an introduction from you');
  return (
    <React.Fragment>
      <ScreenInfo
        id="id-header-about-quizv4"
        titleInfo={titleInfo}
        isShow
        eleScroll="step-0-quiz"
      />
      {
        _.map(listAboutQuiz, (d, index) => (
          generateQuestions(d, index)
        ))
      }
    </React.Fragment>
  );
}

export default AboutQuiz;
