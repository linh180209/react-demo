import classnames from 'classnames';
import _ from 'lodash';
import React from 'react';
import BlockInfo from '../../components/blockInfo';
import QuestionSkinV2 from '../../components/questionSkinV2';
import './styles.scss';

function SkinTone(props) {
  const bannerBlock = _.find(props.cmsState?.bannerBlocks, x => x?.value?.image?.caption === 'skinTone');
  return (
    <div
      className={classnames(props.isShow ? 'skin-tone-quiz height-quiz' : 'hidden')}
    >
      <div id={`step-${props.idIndex}-quiz`} className="index-step" />
      <BlockInfo
        dataCMS={bannerBlock?.value}
        idIndex={props.idIndex}
      />
      <div className={classnames('info-right')}>
        <QuestionSkinV2
          onClick={props.onClick}
          dataSelected={props.dataSelected}
          name={props.name}
          idIndex={props.idIndex}
        />
      </div>
    </div>
  );
}

export default SkinTone;
