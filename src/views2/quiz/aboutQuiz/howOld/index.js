import classnames from 'classnames';
import React, { useState } from 'react';
import Slider from 'react-rangeslider';
import 'react-rangeslider/lib/index.css';
import ButtonCT from '../../../../componentsv2/buttonCT';
import { isMobile991fn } from '../../../../DetectScreen';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import './styles.scss';

const lables = {
  1: '1',
  5: '5',
  10: '10',
  15: '15',
  20: '20',
  25: '25',
  30: '30',
  35: '35',
  40: '40',
  45: '45',
  50: '50',
  55: '55',
  60: '60',
  65: '65',
  70: '70',
  75: '75',
  80: '80',
  85: '85',
  90: '90',
  95: '95',
  100: '100',
};
const labeMobile = {
  1: '1',
  100: '100',
};

function HowOld(props) {
  const [value, setValue] = useState(30);

  const handleChangeHorizontal = (v) => {
    setValue(v);
  };

  const onClickNext = () => {
    props.onClick(props.name, value, props.idIndex);
  };

  return (
    <div
      className={classnames(props.isShow ? 'how-old height-quiz' : 'hidden')}
    >
      <div id={`step-${props.idIndex}-quiz`} className="index-step" />
      <div className="tile-how-old">
        {getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'HOW OLD ARE YOU')}
      </div>
      <div className="div-progress-bar">
        <Slider
          min={1}
          max={100}
          value={value}
          handleLabel={value}
          labels={isMobile991fn() ? labeMobile : lables}
          onChange={handleChangeHorizontal}
        />
      </div>
      <ButtonCT
        onClick={onClickNext}
        name={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Next')}
        size="medium"
        className="button-next-quiz"
      />
    </div>
  );
}

export default HowOld;
