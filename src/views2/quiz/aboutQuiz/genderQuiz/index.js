import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import icFemale from '../../../../imagev2/svg/icon-female-quiz.svg';
import icMale from '../../../../imagev2/svg/icon-male-quiz.svg';
import icUnsex from '../../../../imagev2/svg/icon-unsex-quiz.svg';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import BlockInfo from '../../components/blockInfo';
import QuestionGender from '../../components/questionGender';
import './styles.scss';

function GenderQuiz(props) {
  const [valueSeleted, setValueSeleted] = useState();

  const onClickData = (d) => {
    setValueSeleted({
      id: d === '1' ? 'male' : d === '2' ? 'female' : 'unsex',
      name: d === '1' ? 'Male' : d === '2' ? 'Female' : 'Gender Neutral',
    });
  };

  useEffect(() => {
    setTimeout(() => {
      setValueSeleted(undefined);
    }, 1000);
  }, [valueSeleted]);

  const bannerBlock = _.find(props.cmsState?.bannerBlocks, x => x?.value?.image?.caption === 'gender');
  return (
    <div
      className="gender-quiz height-quiz"
    >
      <div id={`step-${props.idIndex}-quiz`} className="index-step" />
      <BlockInfo
        dataCMS={bannerBlock?.value}
        idIndex={props.idIndex}
      />
      <div className={classnames('info-right')}>
        {
          valueSeleted && (
            <QuestionGender
              className={classnames('selected-data', valueSeleted?.id)}
              onClick={() => props.onClick(props.name, '1', props.idIndex)}
              icon={icMale}
              name={getNameFromButtonBlock(props.cmsState?.buttonBlocks, valueSeleted.name)}
            />
          )
        }
        <QuestionGender
          className={classnames('male', props.dataSelected === '1' ? 'active' : '')}
          onClick={() => {
            props.onClick(props.name, '1', props.idIndex);
            onClickData('1');
          }}
          icon={icMale}
          name={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Male')}
        />
        <QuestionGender
          className={classnames('female', props.dataSelected === '2' ? 'active' : '')}
          onClick={() => {
            props.onClick(props.name, '2', props.idIndex);
            onClickData('2');
          }}
          icon={icFemale}
          name={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Female')}
        />
        <QuestionGender
          className={classnames('unsex', props.dataSelected === '3' ? 'active' : '')}
          onClick={() => {
            props.onClick(props.name, '3', props.idIndex);
            onClickData('3');
          }}
          icon={icUnsex}
          name={getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Gender Neutral')}
        />
      </div>
    </div>
  );
}

export default GenderQuiz;
