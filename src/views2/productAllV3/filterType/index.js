import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { useRecoilState, useRecoilValue } from 'recoil';
import { perfume, home, giftNotBg } from '../../../imagev2/svg';

import CheckBoxCT from '../../../componentsv2/checkBoxCT';
import './styles.scss';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import stateRecoil from '../recoil';

function FillterType(props) {
  // recoil
  const filterOptions = useRecoilValue(stateRecoil.filterOptions);
  const [objectFilter, setObjectFilter] = useRecoilState(stateRecoil.objectFilter);

  const onChange = (data, e) => {
    const objectClone = _.cloneDeep(objectFilter);
    if (e.target.checked && !_.find(objectClone?.productTypeParentIds, x => x.id === data.id)) {
      objectClone.productTypeParentIds.push(data);
    } else if (!e.target.checked) {
      _.remove(objectClone?.productTypeParentIds, x => x.id === data.id);
    }
    setObjectFilter(objectClone);
  };

  const isChecked = (objectF, data) => _.find(objectF?.productTypeParentIds, x => x.id === data.id);

  const labelCheckbox = data => (
    <div className="label-checkbox">
      <div className="text-t">
        {data.name}
      </div>
      <img src={data.icon} alt="perfume" />
    </div>
  );
  const itemFilter = data => (
    <div className="item-filter">
      <CheckBoxCT
        onChange={e => onChange(data, e)}
        checked={isChecked(objectFilter, data)}
        label={labelCheckbox(data)}
        className="checkbox-product"
      />
    </div>
  );

  const categoriesBt = getNameFromButtonBlock(props.buttonBlocks, 'Categories');
  const perfumeBt = getNameFromButtonBlock(props.buttonBlocks, 'Perfume');
  const giftingBt = getNameFromButtonBlock(props.buttonBlocks, 'Gifting');
  const homeScentBt = getNameFromButtonBlock(props.buttonBlocks, 'Home Scent');

  const elePerfume = _.find(filterOptions, x => x.key === 'Perfume');
  const eleHomeScents = _.find(filterOptions, x => x.key === 'home_scents');
  const eleGift = _.find(filterOptions, x => x.key === 'Gift');
  return (
    <div className="fillter-type">
      <div className="text-title">
        {categoriesBt}
      </div>
      <div className="div-list-item">
        {
          elePerfume && (
            itemFilter({ name: perfumeBt, icon: elePerfume?.icon, id: elePerfume?.id })
          )
        }
        {
          eleHomeScents && (
            itemFilter({ name: homeScentBt, icon: eleHomeScents?.icon, id: eleHomeScents?.id })
          )
        }
        {
          eleGift && (
            itemFilter({ name: giftingBt, icon: eleGift?.icon, id: eleGift?.id })
          )
        }
      </div>
    </div>
  );
}

export default React.memo(FillterType);
