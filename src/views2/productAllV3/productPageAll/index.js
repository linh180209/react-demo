import React, { useState, useEffect, useRef } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import { useRecoilState, useRecoilValue } from 'recoil';
import { useHistory } from 'react-router-dom';
import SwitchButton from '../../../componentsv2/switchButton';
import './styles.scss';
import FillterType from '../filterType';
import ProductCreation from '../productCreation';
import { isBrowser, isMobile } from '../../../DetectScreen';
import SortFillterMobile from './sortFillterMobile';
import {
  getNameFromButtonBlock, getLinkFromButtonBlock, generateUrlWeb, gotoShopHome, getPlaceholerFromButtonBlock,
} from '../../../Redux/Helpers';
import recoilState from '../recoil';
import useWindowEvent from '../../../Utils/useWindowEvent';

function ProductPageAll(props) {
  const history = useHistory();
  const [isAllProduct, setIsAllProduct] = useRecoilState(recoilState.isAllProduct);
  const objectFilter = useRecoilValue(recoilState.objectFilter);
  const [isShowFilter, setIsShowFilter] = useState(false);
  const [isStickyMobile, setIsStickyMobile] = useState(false);
  const [isScrollUp, setIsScrollUp] = useState(false);
  const oldCurrentScrollPos = useRef(0);

  const onClickLink = (link) => {
    history.push(generateUrlWeb(link));
  };

  const hanleScroll = () => {
    if (isMobile) {
      console.log('window.scrollY', window.scrollY);
      const currentScrollPos = window.pageYOffset;
      const isScrollUpT = oldCurrentScrollPos.current > currentScrollPos;
      oldCurrentScrollPos.current = currentScrollPos;

      setIsScrollUp(isScrollUpT);
      if (window.scrollY > 350) {
        setIsStickyMobile(true);
      } else {
        setIsStickyMobile(false);
      }
    }
  };

  useEffect(() => {
    setTimeout(() => {
      setIsShowFilter(isAllProduct);
    }, 200);
  }, [isAllProduct]);

  useWindowEvent('scroll', hanleScroll, window);

  const createYourScentBt = getNameFromButtonBlock(props.buttonBlocks, 'Create Your Scent');
  const allProductBt = getNameFromButtonBlock(props.buttonBlocks, 'All Products');
  const allProductLinkBt = getLinkFromButtonBlock(props.buttonBlocks, 'All Products');
  const youCanSwitchBt = getNameFromButtonBlock(props.buttonBlocks, 'You can switch between 2 categories');
  const homeBt = getNameFromButtonBlock(props.buttonBlocks, 'HOME');
  const homeLinkBt = getLinkFromButtonBlock(props.buttonBlocks, 'HOME');
  const createScentBt = getNameFromButtonBlock(props.buttonBlocks, 'CREATE SCENT');
  const createScentLinkBt = getLinkFromButtonBlock(props.buttonBlocks, 'CREATE SCENT');

  const placeholderBt = getPlaceholerFromButtonBlock(props.buttonBlocks, 'You can switch between 2 categories');
  const isHiddenToggle = placeholderBt === 'hidden_toggle';

  return (
    <div className={classnames('product-page-all', isAllProduct && isBrowser ? 'show-all-product' : '')}>
      <div className={classnames('w-100', isStickyMobile ? 'div-stick' : '', isScrollUp ? 'scroll-up' : '')}>
        {
          !isHiddenToggle && (
            <SwitchButton
              name1={createYourScentBt}
              name2={allProductBt}
              value={isAllProduct}
              onChange={() => setIsAllProduct(!isAllProduct)}
              tooltip={youCanSwitchBt}
            />
          )
        }
        {
          isBrowser ? (
            <React.Fragment>
              <div className={classnames('switch-text', isHiddenToggle ? 'add-padding' : '')}>
                <button type="button" className={classnames('button-bg__none', homeLinkBt ? 'active-bt' : '')} onClick={() => gotoShopHome()}>
                  {homeBt}
                </button>
                <span>/</span>
                <button type="button" className={classnames('button-bg__none', 'active-bt')} onClick={() => onClickLink(isAllProduct ? allProductLinkBt : createScentLinkBt)}>
                  {isAllProduct ? allProductBt : createScentBt}
                </button>
                {
                  isAllProduct
                  && _.map(objectFilter?.productTypeParentIds, d => (
                    <React.Fragment>
                      <span>/</span>
                      <button type="button" className={classnames('button-bg__none')}>
                        {d.name}
                      </button>
                    </React.Fragment>
                  ))
                }
              </div>
              {
                isShowFilter && (
                  <div className="content-product-all">
                    <FillterType buttonBlocks={props.buttonBlocks} />
                  </div>
                )
              }
            </React.Fragment>
          ) : (
            isShowFilter && (
              <SortFillterMobile buttonBlocks={props.buttonBlocks} />
            )
          )
        }
      </div>

      {
        isShowFilter ? (
          <ProductCreation isAddFillter buttonBlocks={props.buttonBlocks} />
        ) : (
          <ProductCreation buttonBlocks={props.buttonBlocks} />
        )
      }
    </div>
  );
}

export default React.memo(ProductPageAll);
