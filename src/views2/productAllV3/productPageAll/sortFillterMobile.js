import React, { useState, useRef } from 'react';
import {
  Dropdown, DropdownToggle, DropdownMenu, DropdownItem,
} from 'reactstrap';
import _ from 'lodash';
import Drawer from '@mui/material/Drawer';
import { useRecoilState, useSetRecoilState } from 'recoil';

import ImportExportIcon from '@mui/icons-material/ImportExport';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import SortIcon from '@mui/icons-material/Sort';
import { useMergeState } from '../../../Utils/customHooks';
import ButtonCT from '../../../componentsv2/buttonCT';
import FillterTab from '../productCreation/fillterTab';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import stateRecoil from '../recoil';
import { isMobile } from '../../../DetectScreen';

function SortFillterMobile(props) {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [openDrawer, setOpenDrawer] = useState(false);
  const [objectFilter, setObjectFilter] = useRecoilState(stateRecoil.objectFilter);
  const setsFetchMobile = useSetRecoilState(stateRecoil.isFetchMobile);
  const filterClone = useRef();

  const toggle = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const setFilterMobile = (isFetchMobile) => {
    const objectClone = _.cloneDeep(isFetchMobile ? objectFilter : filterClone.current);
    setsFetchMobile(isFetchMobile);
    setObjectFilter(objectClone);
  };

  const toggleDrawer = () => {
    const value = !openDrawer;
    if (isMobile && value) {
      filterClone.current = _.cloneDeep(objectFilter);
      setFilterMobile(false);
    }
    setOpenDrawer(value);
  };

  const onClickSort = (value) => {
    const objectClone = _.cloneDeep(objectFilter);
    objectClone.order_by = value;
    setsFetchMobile(true);
    setObjectFilter(objectClone);
  };

  const alphabetBt = getNameFromButtonBlock(props.buttonBlocks, 'Alphabet');
  const popularBt = getNameFromButtonBlock(props.buttonBlocks, 'Popular');
  const options = [
    {
      name: alphabetBt,
      value: 'name',
    }, {
      name: popularBt,
      value: 'sold',
    },
  ];
  return (
    <div className="sort-fillter-mobile div-row">
      <Dropdown
        isOpen={dropdownOpen}
        toggle={toggle}
        className="dropdown-sort-fillter"
      >
        <DropdownToggle
          tag="span"
          onClick={toggle}
          data-toggle="dropdown"
          aria-expanded={dropdownOpen}
        >
          <div className="div-display-order">
            <ButtonCT
              name="Sort by"
              startIcon={<ImportExportIcon style={{ color: '#2c2c2c' }} />}
              endIcon={<ArrowDropDownIcon style={{ color: '#2c2c2c' }} />}
              size="small"
              color="secondary"
              className="bt-fillter"
              onClick={toggle}
            />
          </div>
        </DropdownToggle>
        <DropdownMenu>
          {
            _.map(options, d => (
              <div
                onClick={() => {
                  onClickSort(d.value);
                  toggle();
                }}
              >
                <div className="drop-down-item-order">
                  <span>
                    {d.name}
                  </span>
                </div>
              </div>
            ))
          }
        </DropdownMenu>
      </Dropdown>

      <ButtonCT
        name="filter"
        startIcon={<SortIcon style={{ color: '#2c2c2c' }} />}
        size="small"
        color="secondary"
        className="bt-fillter"
        onClick={toggleDrawer}
      />

      <Drawer
        anchor="right"
        open={openDrawer}
        onClose={toggleDrawer}
        className="drawer-filter"
      >
        <FillterTab onClose={toggleDrawer} buttonBlocks={props.buttonBlocks} setFilterMobile={setFilterMobile} />
      </Drawer>

    </div>
  );
}

export default React.memo(SortFillterMobile);
