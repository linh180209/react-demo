import React, { useState, useEffect } from 'react';
import Fab from '@mui/material/Fab';
import classnames from 'classnames';
import VerticalAlignTopIcon from '@mui/icons-material/VerticalAlignTop';
import useWindowEvent from '../../../Utils/useWindowEvent';

function FloatinGButton(props) {
  const [isHidden, setIsHidden] = useState(true);

  const onClickTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  const hanleScroll = () => {
    if (window.scrollY > 200) {
      setIsHidden(false);
    } else {
      setIsHidden(true);
    }
  };

  useWindowEvent('scroll', hanleScroll, window);

  return (
    <Fab color="primary" aria-label="add" className={classnames('float-button-top', isHidden ? 'is-hidden' : '')} onClick={onClickTop}>
      <VerticalAlignTopIcon style={{ color: '#2c2c2c' }} />
    </Fab>
  );
}

export default FloatinGButton;
