import _ from 'lodash';
import React, { useEffect } from 'react';
import MetaTags from 'react-meta-tags';
import { useDispatch, useSelector } from 'react-redux';
import { RecoilRoot, useSetRecoilState } from 'recoil';
import smoothscroll from 'smoothscroll-polyfill';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import BlockFAQ from '../../componentsv2/blockFAQ';
import ShipBlock from '../../componentsv2/shipBlock';
import SilderHero from '../../componentsv2/sliderHero';
import { isMobile } from '../../DetectScreen';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import {
  fetchCMSHomepage, generateHreflang, getNameFromButtonBlock, removeLinkHreflang, scrollTop, scrollToTargetAdjusted,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { toastrError } from '../../Redux/Helpers/notification';
import { generateScriptFAQ, removeScriptFAQ } from '../../Utils/addScriptSchema';
import { useMergeState } from '../../Utils/customHooks';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';
import FooterV2 from '../footer';
import FloatinGButton from './floatingButton';
import { fetchFilterOptions, fetchProductType, handleCMSPage } from './handler';
import ProductPageAll from './productPageAll';
import stateRecoil from './recoil';
import './styles.scss';

function ProductAllV3Wrap() {
  // redux
  const dispatch = useDispatch();
  const cms = useSelector(state => state.cms);
  const countries = useSelector(state => state.countries);
  const countryName = auth.getCountryName();
  // const showAskRegion = useSelector(state => state.showAskRegion);

  // recoil
  const setFilterOptions = useSetRecoilState(stateRecoil.filterOptions);
  const setProductTypes = useSetRecoilState(stateRecoil.productTypes);
  const setIsAllProduct = useSetRecoilState(stateRecoil.isAllProduct);

  // state
  const [state, setState] = useMergeState({
    cmsState: {},
  });

  const fetchData = async () => {
    const pending = [];
    // open when show filter product
    // pending.push(fetchFilterOptions());
    pending.push(fetchProductType());
    const cmsProduct = _.find(cms, x => x.title === 'Product All V3');
    if (!cmsProduct) {
      pending.push(fetchCMSHomepage('product-all-v3'));
    } else {
      const cmsData = handleCMSPage(cmsProduct);
      setState({ cmsState: cmsData });
    }
    dispatch(loadingPage(true));
    try {
      const results = await Promise.all(pending);
      if (results.length > 1) {
        dispatch(addCmsRedux(results[1]));
        const cmsData = handleCMSPage(results[1]);
        setState({ cmsState: cmsData });
      }

      setProductTypes(results[0]);
      // open when show filter product
      // setFilterOptions(results[0]);
      // setProductTypes(results[1]);
    } catch (error) {
      console.error('error', error);
      toastrError(error.message);
    }
    dispatch(loadingPage(false));
  };

  const onClickCreatScent = () => {
    setIsAllProduct(false);
    scrollToTargetAdjusted('switch-button-product');
  };

  useEffect(() => {
    if (state.cmsState?.faqsBlock) {
      generateScriptFAQ(state.cmsState.faqsBlock, true);
    }
  }, [state.cmsState]);

  useEffect(() => {
    removeLinkHreflang();
    // onClickContinuteShoppingHomeAmplitude();
    // scroll top
    smoothscroll.polyfill();
    scrollTop();

    // fetch Data
    fetchData();
    return (() => {
      removeScriptFAQ();
    });
  }, []);

  return (
    <div>
      <MetaTags>
        <title>{replaceCountryInMetaTags(countryName, state.cmsState?.seo?.seoTitle)}</title>
        <meta name="description" content={replaceCountryInMetaTags(state.cmsState?.seo?.seoDescription)} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/all-products')}
      </MetaTags>

      {/* <HeaderHomePage isProductPage isShowAskRegion />
      { showAskRegion && (<div className="div-temp-region" />) } */}
      <HeaderHomePageV3 />
      <div className="all-product-v2">
        {
            state.cmsState?.carouselBlock
            && <SilderHero data={state.cmsState?.carouselBlock} onClick={onClickCreatScent} />
          }
        <div className="content-product-v2">
          {/* <ShipBlock data={state.cmsState?.iconsBlock?.icons} /> */}
          <ProductPageAll
            buttonBlocks={state.cmsState?.buttonBlocks}
          />
        </div>
        <BlockFAQ
          faqsBlock={state.cmsState?.faqsBlock}
          title={getNameFromButtonBlock(state.cmsState?.buttonBlocks, 'Frequently Asked Question')}
          description={getNameFromButtonBlock(state.cmsState?.buttonBlocks, 'This is Body')}
        />
        {
          isMobile && (
            <FloatinGButton />
          )
        }
      </div>
      <FooterV2 />
    </div>
  );
}

function ProductAllV3() {
  return (
    <RecoilRoot>
      <ProductAllV3Wrap />
    </RecoilRoot>
  );
}

export default ProductAllV3;
