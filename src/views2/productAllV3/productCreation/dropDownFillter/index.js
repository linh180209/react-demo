/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { useRecoilState } from 'recoil';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import InfoIcon from '@mui/icons-material/Info';
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import ButtonCT from '../../../../componentsv2/buttonCT';
import CheckBoxCT from '../../../../componentsv2/checkBoxCT';
import './styles.scss';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import stateRecoil from '../../recoil';
import { isBrowser } from '../../../../DetectScreen';

function DropDownFilter(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [objectFilter, setObjectFilter] = useRecoilState(stateRecoil.objectFilter);

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const isChecked = (objectF, data) => _.find(objectF?.dataSelected, x => x.id === data.id);

  const onChange = (data, e) => {
    const objectClone = _.cloneDeep(objectFilter);
    if (e.target.checked && !_.find(objectClone?.dataSelected, x => x.id === data.id)) {
      objectClone.dataSelected.push(data);
    } else if (!e.target.checked) {
      _.remove(objectClone?.dataSelected, x => x.id === data.id);
    }
    setObjectFilter(objectClone);
  };

  const open = Boolean(anchorEl);

  const dataDisplay = getNameFromButtonBlock(props.buttonBlocks, props.data?.key);
  const messagePopup = getNameFromButtonBlock(props.buttonBlocks, `${props.data?.key}_hover`);

  return (
    <div className="dropdown-filter">
      <Accordion>
        <AccordionSummary
          expandIcon={<ArrowDropDownIcon style={{ color: '#2c2c2c' }} />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <div className="header-dropdown div-row">
            {
              isBrowser && (
                <React.Fragment>
                  <div
                    className="icon-info-filter"
                    aria-owns={open ? props.id : undefined}
                    aria-haspopup="true"
                    onMouseEnter={handlePopoverOpen}
                    onMouseLeave={handlePopoverClose}
                  >
                    <InfoIcon style={{ color: '#2c2c2c' }} />
                  </div>
                  <Popover
                    id={props.id}
                    sx={{
                      pointerEvents: 'none',
                    }}
                    open={open}
                    anchorEl={anchorEl}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'left',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'left',
                    }}
                    onClose={handlePopoverClose}
                    disableRestoreFocus
                    className="popover-filter"
                  >
                    <Typography style={{
                      fontSize: '14px',
                      color: '#EBD8B8',
                      height: '41px',
                      padding: '0px 10px',
                      lineHeight: '41px',
                      borderRadius: '20px',
                      background: '#2c2c2c',
                    }}
                    >
                      {messagePopup}
                    </Typography>
                  </Popover>
                </React.Fragment>
              )
            }

            <div className="name-title">
              {dataDisplay}
            </div>
          </div>
        </AccordionSummary>
        <AccordionDetails className="accord-filter">
          <div className="list-dropdown">
            {
              _.map(props.data?.data || [], d => (
                <CheckBoxCT
                  checked={isChecked(objectFilter, d)}
                  className="check-filter"
                  onChange={e => onChange(d, e)}
                  label={(
                    <div>
                      {d.display_name}
                    </div>
                  )}
                />
              ))
            }
          </div>
        </AccordionDetails>
      </Accordion>
    </div>
  );
}

export default React.memo(DropDownFilter);
