import React, { useState, useEffect } from 'react';
import SortIcon from '@mui/icons-material/Sort';
import _ from 'lodash';
import { useRecoilValue, useSetRecoilState, useRecoilState } from 'recoil';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import CloseIcon from '@mui/icons-material/Close';
import ArrowRightIcon from '@mui/icons-material/ArrowRight';
import ButtonCT from '../../../../componentsv2/buttonCT';
import CheckBoxCT from '../../../../componentsv2/checkBoxCT';
import DropDownFilter from '../dropDownFillter';
import './styles.scss';
import { isBrowser } from '../../../../DetectScreen';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import stateRecoil from '../../recoil';
import { useMergeState } from '../../../../Utils/customHooks';

const groupFilter = (data) => {
  const filter1 = data.slice(0, data.length / 2);
  const filter2 = data.slice(data.length / 2, data.length);
  return { filter1, filter2 };
};

function FillterTab(props) {
  // recoil
  const filterOptions = useRecoilValue(stateRecoil.filterOptions);
  const resetFilterCheckbox = useSetRecoilState(stateRecoil.resetFilterCheckbox);
  const [objectFilter, setObjectFilter] = useRecoilState(stateRecoil.objectFilter);

  const [isOpenDetail, setIsOpenDetail] = useState(false);
  const [state, setState] = useMergeState({
    filter1: groupFilter(filterOptions)?.filter1,
    filter2: groupFilter(filterOptions)?.filter2,
    dataDetail: undefined,
  });

  const toggleOpenDetail = () => {
    setIsOpenDetail(!isOpenDetail);
  };

  const onClickReset = () => {
    resetFilterCheckbox();
  };

  const onClickRemove = (data) => {
    const objectClone = _.cloneDeep(objectFilter);
    _.remove(objectClone?.dataSelected || [], d => d.id === data.id);
    setObjectFilter(objectClone);
  };

  const onClickItem = (d) => {
    setState({ dataDetail: d });
    toggleOpenDetail();
  };

  const isChecked = (objectF, data) => _.find(objectF?.dataSelected, x => x.id === data.id);

  const onChange = (data, e) => {
    const objectClone = _.cloneDeep(objectFilter);
    if (e.target.checked && !_.find(objectClone?.dataSelected, x => x.id === data.id)) {
      objectClone.dataSelected.push(data);
    } else if (!e.target.checked) {
      _.remove(objectClone?.dataSelected, x => x.id === data.id);
    }
    setObjectFilter(objectClone);
  };

  useEffect(() => {
    setState({
      filter1: groupFilter(filterOptions)?.filter1,
      filter2: groupFilter(filterOptions)?.filter2,
    });
  }, [filterOptions]);

  const filterByBt = getNameFromButtonBlock(props.buttonBlocks, 'Filter By');
  const resetFilterBt = getNameFromButtonBlock(props.buttonBlocks, 'Reset Filter');
  return (
    <React.Fragment>
      {
      isBrowser ? (
        <div className="fillter-tab div-col">
          <div className="header-fillter div-row">
            <div className="div-row">
              <SortIcon />
              <div className="other-Tagline">
                {filterByBt}
              </div>
            </div>
            <ButtonCT
              variant="text"
              name={resetFilterBt}
              className="bt-reset"
              onClick={onClickReset}
            />
          </div>

          <div className="list-fillter div-col">
            {
              state.filter1 && (
                <div className="fillter-group">
                  <hr />
                  {
                    _.map(state.filter1, (d, index) => (
                      <DropDownFilter id={`drops-1-${index}`} data={d} buttonBlocks={props.buttonBlocks} />
                    ))
                  }
                </div>
              )
            }
            {
              state.filter2 && (
                <div className="fillter-group">
                  <hr />
                  {
                    _.map(state.filter2, (d, index) => (
                      <DropDownFilter id={`drops-2-${index}`} data={d} buttonBlocks={props.buttonBlocks} />
                    ))
                  }
                </div>
              )
            }
          </div>
        </div>
      ) : isOpenDetail ? (
        <div className="fillter-tab-mobile filter-detail div-col animated faster fadeInRight">

          <div className="header-back div-row">
            <button
              onClick={toggleOpenDetail}
              type="button"
              className="button-bg__none bt-back-fillter"
            >
              <ArrowBackIosNewIcon style={{ color: '#2C2C22' }} />
              <div className="other-Tagline">
                {getNameFromButtonBlock(props.buttonBlocks, state.dataDetail?.key)}
              </div>
            </button>
            <div />
          </div>

          <div className="list-dropdown">
            <hr />
            {
              _.map(state.dataDetail?.data || [], d => (
                <CheckBoxCT
                  className="check-filter"
                  onChange={e => onChange(d, e)}
                  checked={isChecked(objectFilter, d)}
                  label={(
                    <div>
                      {d.display_name}
                    </div>
                  )}
                />
              ))
            }
          </div>

        </div>
      ) : (

        <div className="fillter-tab-mobile div-col">

          <div className="header-back div-row">
            <button
              onClick={() => {
                props.setFilterMobile(false);
                props.onClose();
              }}
              type="button"
              className="button-bg__none bt-back-fillter"
            >
              <ArrowBackIosNewIcon style={{ color: '#2C2C22' }} />
              <div className="other-Tagline">
                {filterByBt}
              </div>
            </button>
            <button
              onClick={onClickReset}
              type="button"
              className="button-bg__none bt-reset-fillter"
            >
              {resetFilterBt}
            </button>
          </div>
          {
            objectFilter?.dataSelected?.length > 0 && (
              <div className="list-result-filter-mobile">
                {
                  _.map(objectFilter?.dataSelected, d => (
                    <button type="button" className="bt-result-filter" onClick={() => onClickRemove(d)}>
                      {d.display_name}
                      <CloseIcon style={{ color: '#2C2C22' }} />
                    </button>
                  ))
                }
              </div>
            )
          }

          <div className="list-fillter div-col">
            {
              state.filter1 && (
                <div className="fillter-group">
                  <hr />
                  {
                    _.map(state.filter1, (d, index) => (
                      <DropDownFilter id={`drops-1-${index}`} data={d} buttonBlocks={props.buttonBlocks} />
                    ))
                  }
                </div>
              )
            }
            {
              state.filter2 && (
                <div className="fillter-group">
                  <hr />
                  {
                    _.map(state.filter2, (d, index) => (
                      <DropDownFilter id={`drops-2-${index}`} data={d} buttonBlocks={props.buttonBlocks} />
                    ))
                  }
                </div>
              )
            }
          </div>
          <div className="bt-save-mobie">
            <ButtonCT
              name="Save filter"
              size="medium"
              onClick={() => {
                props.onClose();
                props.setFilterMobile(true);
              }}
            />
          </div>
          {/* {
            state.filter1 && (
              <div className="block-filter-mobile div-col">
                <hr />
                {
                  _.map(state.filter1, d => (
                    <div className="item-filter div-row" onClick={() => onClickItem(d)}>
                      <div className="body-text-s-regular m-size">
                        <b>{getNameFromButtonBlock(props.buttonBlocks, d.key)}</b>
                      </div>
                      <ArrowRightIcon style={{ color: '#2C2C22' }} />
                    </div>
                  ))
                }
              </div>
            )
          }
          {
            state.filter2 && (
              <div className="block-filter-mobile div-col">
                <hr />
                {
                  _.map(state.filter2, d => (
                    <div className="item-filter div-row" onClick={() => onClickItem(d)}>
                      <div className="body-text-s-regular m-size">
                        <b>{getNameFromButtonBlock(props.buttonBlocks, d.key)}</b>
                      </div>
                      <ArrowRightIcon style={{ color: '#2C2C22' }} />
                    </div>
                  ))
                }
              </div>
            )
          } */}

        </div>
      )
    }
    </React.Fragment>
  );
}

export default React.memo(FillterTab);
