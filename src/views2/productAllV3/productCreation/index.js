import ImportExportIcon from '@mui/icons-material/ImportExport';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import CloseIcon from '@mui/icons-material/Close';
import _ from 'lodash';
import { useRecoilValue, useRecoilState, useSetRecoilState } from 'recoil';
import React, { useEffect, useRef } from 'react';
import classnames from 'classnames';
import { useHistory, useLocation } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import ButtonCT from '../../../componentsv2/buttonCT';
import ItemProductPage from '../../../componentsv2/itemProductPage';
import FillterTab from './fillterTab';
import './styles.scss';
import { isMobile, isBrowser } from '../../../DetectScreen';
import loadingPage from '../../../Redux/Actions/loading';
import {
  generaCurrency, generateUrlWeb, getNameFromButtonBlock, isCheckNull, onClickProduct,
} from '../../../Redux/Helpers';
import stateRecoil, { defaultObjectFilter } from '../recoil';
import { toastrError } from '../../../Redux/Helpers/notification';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { GET_ALL_PRODUCTS_FILTER } from '../../../config';
import { useMergeState } from '../../../Utils/customHooks';
import { createBasketGuest, addProductBasket } from '../../../Redux/Actions/basket';
import useUpdateEffect from '../../../Utils/useUpdateEffect';
import { generateSearchUrl } from '../handler';
import auth from '../../../Redux/Helpers/auth';

function ProductCreation(props) {
  const { search } = useLocation();

  // redux
  const dispatch = useDispatch();
  const basket = useSelector(state => state.basket);

  // recoil
  const productTypes = useRecoilValue(stateRecoil.productTypes);
  const isFetchMobile = useRecoilValue(stateRecoil.isFetchMobile);
  const setIsAllProduct = useSetRecoilState(stateRecoil.isAllProduct);
  const [objectFilter, setObjectFilter] = useRecoilState(stateRecoil.objectFilter);

  const searchRef = useRef();
  const [state, setState] = useMergeState({
    count: 0,
    products: [],
  });
  const bodyCurrent = useRef();
  const scrollBody = useRef();
  const litmitProduct = useRef();

  const history = useHistory();
  const gotoLink = (link) => {
    if (link.includes('http')) {
      window.open(link);
      return;
    }
    history.push(generateUrlWeb(link));
  };

  const gotoProduct = (data) => {
    console.log('gotoProduct', data);
    const url = onClickProduct({
      type: {
        name: data.product_type.name,
      },
      product: data.product_id,
      id: data.id,
    });
    history.push(generateUrlWeb(url));
  };

  const onClickRemove = (data) => {
    const objectClone = _.cloneDeep(objectFilter);
    _.remove(objectClone?.dataSelected || [], d => d.id === data.id);
    setObjectFilter(objectClone);
  };

  const itemResult = d => (
    <div className="result-item">
      {d.display_name}
      <button type="button" className="button-bg__none" onClick={() => onClickRemove(d)}>
        <CloseIcon style={{ color: '#8D826E' }} />
      </button>
    </div>
  );

  const onClickSort = (value) => {
    const objectClone = _.cloneDeep(objectFilter);
    objectClone.order_by = value;
    setObjectFilter(objectClone);
  };

  const generateFilter = (dataSelected) => {
    const fields = {};
    const productTypeIds = [];
    const tagIds = [];
    const scentNoteIds = [];
    _.forEach(dataSelected || [], (d) => {
      if (d.type === 'boolean') {
        _.assign(fields, { [d.name]: true });
      } else if (d.type === 'product_type') {
        productTypeIds.push(d.id);
      } else if (d.type === 'tag') {
        tagIds.push(d.id);
      } else if (d?.type === 'scent_note') {
        scentNoteIds.push(d.id);
      }
    });
    return {
      fields, productTypeIds, tagIds, scentNoteIds,
    };
  };

  const fetchDataProduct = async (isLoadMore) => {
    console.log('isFetchMobile', isFetchMobile);
    if ((isMobile && !isFetchMobile) || _.isEmpty(objectFilter)) {
      return;
    }
    try {
      const objectClone = _.cloneDeep(objectFilter);
      const filter = generateFilter(objectFilter?.dataSelected, objectFilter.productTypeParentIds);
      const body = {
        offset: isLoadMore ? state.products.length : 0,
        limit: isCheckNull(auth.getLimitProduct()) ? 12 : auth.getLimitProduct(),
        order_by: objectClone.order_by === 'sold' ? '-sold' : objectClone.order_by,
        product_type_parent_ids: _.map(objectFilter.productTypeParentIds, x => x.id),
        fields: filter.fields,
        product_type_ids: filter.productTypeIds,
        tag_ids: filter.tagIds,
        scent_note_ids: filter.scentNoteIds,
      };
      if (JSON.stringify(body) === JSON.stringify(bodyCurrent.current)) {
        dispatch(loadingPage(false));
        return;
      }

      auth.setLimitProduct(undefined);

      dispatch(loadingPage(true));
      bodyCurrent.current = body;
      const option = {
        url: GET_ALL_PRODUCTS_FILTER,
        method: 'POST',
        body,
      };
      const result = await fetchClient(option);
      setState({
        count: result.count,
        products: !result.results ? [] : isLoadMore ? state.products.concat(result.results) : result.results,
      });
      dispatch(loadingPage(false));
      setTimeout(() => {
        if (auth.getPositionScrollProduct() && auth.getPositionScrollProduct() !== 'undefined' && auth.getPositionScrollProduct() !== '0') {
          console.log('auth.getPositionScrollProduct()', auth.getPositionScrollProduct());
          window.scrollTo({ top: auth.getPositionScrollProduct() });
          scrollBody.current = window.scrollY;
          auth.setPositionScrollProduct(0);
        } else {
          scrollBody.current = window.scrollY;
        }
      }, 500);
    } catch (error) {
      console.error('error', error);
      toastrError(error.message);
      dispatch(loadingPage(false));
    }
  };

  const onClickLoadMore = () => {
    fetchDataProduct(true);
  };

  const onClickAddToCart = (d) => {
    const data = {
      item: d.id,
      price: d.price,
      is_featured: d.is_featured,
      quantity: 1,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    if (!basket.id) {
      dispatch(createBasketGuest(dataTemp));
    } else {
      dispatch(addProductBasket(dataTemp));
    }
  };

  useEffect(() => {
    if (!search || !search.includes('objectFilter')) {
      // open when show filter product
      // fetchDataProduct();
    } else if (searchRef.current !== search) {
      searchRef.current = search;
      const query = new URLSearchParams(search);
      const isAddFillterSearch = query.get('isAddFillter');
      const objectFilterSearch = query.get('objectFilter');
      const pObjectFilterSearch = JSON.parse(objectFilterSearch);
      const cloneObjectFilter = _.cloneDeep(objectFilter);
      _.forEach(cloneObjectFilter?.productTypeParentIds || [], (d) => {
        if (d?.icon) {
          delete d.icon;
        }
      });
      if (JSON.stringify(pObjectFilterSearch) === JSON.stringify(defaultObjectFilter) || JSON.stringify(pObjectFilterSearch) !== JSON.stringify(cloneObjectFilter)) {
        setObjectFilter(pObjectFilterSearch);
      }
      if (!!props.isAddFillter !== (isAddFillterSearch === 'true')) {
        setIsAllProduct(isAddFillterSearch === 'true');
      }
    }
  }, [search, objectFilter, props.isAddFillter]);

  // open when show filter product
  // useUpdateEffect(() => {
  //   fetchDataProduct();
  // }, [objectFilter, isFetchMobile]);

  useUpdateEffect(() => {
    const urlSearch = generateUrlWeb(generateSearchUrl(objectFilter, props.isAddFillter ? 'true' : 'false'));
    history.push(urlSearch);
  }, [objectFilter, props.isAddFillter]);

  const handleScroll = () => {
    if (scrollBody.current || scrollBody.current - window.scrollY < 1000) {
      scrollBody.current = window.scrollY;
    }
  };

  const removeRollonUAE = (data) => {
    if (auth.getCountry() !== 'ae') {
      return data;
    }
    return _.filter(data, x => x.name !== 'dual_crayons');
  };

  useEffect(() => {
    document.addEventListener('scroll', handleScroll);
    litmitProduct.current = state.products.length > 12 ? state.products.length : 12;
    console.log('litmitProduct.current', litmitProduct.current);
    return (() => {
      document.removeEventListener('scroll', handleScroll);
    });
  }, [state.products]);

  useEffect(() => (() => {
    auth.setPositionScrollProduct(scrollBody.current);
    console.log('litmitProduct.current 1', litmitProduct.current);
    auth.setLimitProduct(litmitProduct.current);
  }), []);

  const sortByBt = getNameFromButtonBlock(props.buttonBlocks, 'Sort by');
  const alphabetBt = getNameFromButtonBlock(props.buttonBlocks, 'Alphabet');
  const popularBt = getNameFromButtonBlock(props.buttonBlocks, 'Popular');
  const creationBt = getNameFromButtonBlock(props.buttonBlocks, 'Creation');
  const outOfStockBt = getNameFromButtonBlock(props.buttonBlocks, 'Out Of Stock');
  const addToCardBt = getNameFromButtonBlock(props.buttonBlocks, 'add to cart');
  const loadMoreBt = getNameFromButtonBlock(props.buttonBlocks, 'Load 12 More');
  const showtotalBt = getNameFromButtonBlock(props.buttonBlocks, 'SHOW 12 OF 120 PRODUCTS');
  const countMore = state.count - state.products.length > 12 ? 12 : state.count - state.products.length;
  return (
    <div className="w-100">
      <div id="id-create-scent" />
      {
        props.isAddFillter ? (
          <div className="product-creation-filter">
            {
              isBrowser && <FillterTab buttonBlocks={props.buttonBlocks} />
            }
            <div className="content-product">
              {
                isBrowser && (
                  <div className="sort-product div-row">
                    <div className="result-fillter">
                      {
                        _.map(objectFilter?.dataSelected, x => (
                          itemResult(x)
                        ))
                      }
                    </div>
                    <div className="div-row">
                      <div className="other-Tagline">{sortByBt}</div>
                      <ImportExportIcon style={{ color: '#2c2c2c' }} />
                      <ButtonCT
                        name={alphabetBt}
                        size="small"
                        color="secondary"
                        className={classnames('bt-sort bt-alpha', objectFilter.order_by === 'name' ? 'active' : '')}
                        onClick={() => onClickSort('name')}
                      />
                      <ButtonCT
                        name={popularBt}
                        size="small"
                        color="secondary"
                        className={classnames('bt-sort bt-popular', objectFilter.order_by === 'sold' ? 'active' : '')}
                        onClick={() => onClickSort('sold')}
                      />
                    </div>

                  </div>
                )
              }

              <div className="product-creation-list-filter">
                {
                  _.map(state.products || [], d => (
                    <ItemProductPage
                      className="product-base product-fillter"
                      icon={d?.product_type?.parent?.icon}
                      type={d?.product_type?.title}
                      name={d?.name}
                      scentName={d?.scents}
                      price={generaCurrency(d?.price)}
                      image={d?.image}
                      imageBg={d.image_background}
                      imageScent1={d?.image_scent1}
                      imageScent2={d?.image_scent2}
                      nameInSide={addToCardBt}
                      onClick={() => onClickAddToCart(d)}
                      endIcon={<AddShoppingCartIcon />}
                      buttonSize={isMobile ? 'small' : undefined}
                      onClickImage={() => gotoProduct(d)}
                    />
                  ))
                }
              </div>
              {
                countMore > 0 && (
                  <div className="div-load-more">
                    <ButtonCT
                      name={loadMoreBt.replace('{number}', countMore)}
                      variant="outlined"
                      onClick={onClickLoadMore}
                      size={isMobile ? 'medium' : 'large'}
                    />
                    <div>
                      {showtotalBt.replace('{number}', state.products.length).replace('{count}', state.count)}
                    </div>
                  </div>
                )
              }
            </div>
          </div>
        ) : (
          <div className="product-creation">
            {
            _.map(removeRollonUAE(productTypes) || [], d => (
              <ItemProductPage
                className="item-creation"
                name={d?.alt_name}
                imageBg={d?.image_background}
                scentName={d?.short_description}
                price={generaCurrency(d?.price)}
                image={d?.image}
                nameInSide={d?.is_out_of_stock ? outOfStockBt : creationBt}
                onClick={() => gotoLink(d?.link)}
                buttonSize={isMobile ? 'small' : undefined}
                onClickImage={() => gotoLink(d?.link)}
                isHiddenType
                isDisabledButton={d?.is_out_of_stock}
              />
            ))
          }
          </div>
        )
    }
    </div>
  );
}

export default React.memo(ProductCreation);
