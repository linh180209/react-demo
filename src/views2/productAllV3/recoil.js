import { atom, selector } from 'recoil';
import _ from 'lodash';

export const defaultObjectFilter = {
  order_by: 'sold',
  productTypeParentIds: [],
  dataSelected: [],
};

const isFetchMobile = atom({
  key: 'isFetchMobile',
  default: true,
});

const isAllProduct = atom({
  key: 'isAllProduct',
  default: false,
});

const filterOptions = atom({
  key: 'filterOptions',
  default: [],
});

const productTypes = atom({
  key: 'productTypes',
  default: [],
});

const objectFilter = atom({
  key: 'objectFilter',
  default: defaultObjectFilter,
});

const resetFilterCheckbox = selector({
  key: 'resetFilterCheckbox',
  get: () => {},
  set: ({ get, set }) => {
    const objectGet = get(objectFilter);
    const objectClone = _.clone(objectGet);
    objectClone.dataSelected = [];
    return set(objectFilter, objectClone);
  },
});

export default {
  isAllProduct,
  filterOptions,
  productTypes,
  objectFilter,
  resetFilterCheckbox,
  isFetchMobile,
};
