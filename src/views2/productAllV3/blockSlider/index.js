import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import HeaderCT from '../../../componentsv2/headerCT';
import { getAltImageV2 } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';
import './styles.scss';

function BlockSlider(props) {
  const itemSlider = d => (
    <div className="item-silder">
      {
        d?.image?.image && (
          <img className="bg-image" src={d?.image?.image} alt="bg" />
        )
      }
      <div className="div-content-item">
        <div className="div-image">
          <img className="image-left" src={d?.logo?.image} alt={getAltImageV2(d?.logo)} />
        </div>
        <div className="div-text">
          <HeaderCT className="text-h" type={isMobile ? 'Heading-L' : 'Heading-XL'}>
            {d?.header?.header_text}
          </HeaderCT>
        </div>
      </div>
    </div>
  );

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    autoplaySpeed: 5000,
    pauseOnHover: false,
  };

  console.log('props.data', props.data);
  return (
    <div className="block-slider-product-all">
      <Slider {...settings}>
        {
          _.map(props.data?.value || [], d => (
            itemSlider(d?.value)
          ))
        }
      </Slider>
    </div>
  );
}

export default React.memo(BlockSlider);
