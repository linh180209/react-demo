import _ from 'lodash';
import { GET_FILTER_OPTIONS, GET_PRODUCT_TYPES } from '../../config';
import { getSEOFromCms } from '../../Redux/Helpers';
import fetchClient from '../../Redux/Helpers/fetch-client';

export const handleCMSPage = (cmsPage) => {
  const seo = getSEOFromCms(cmsPage);
  const buttonBlocks = _.filter(cmsPage?.body || [], x => x.type === 'button_block');
  const carouselBlock = _.find(cmsPage?.body, x => x.type === 'carousel_block');
  const iconsBlock = _.find(cmsPage?.body, x => x.type === 'icons_block')?.value;
  const videoBlock = _.find(cmsPage?.body, x => x.type === 'video_block')?.value;
  const lessonBlocks = _.filter(cmsPage?.body, x => x.type === 'lesson_block');
  const faqsBlock = _.find(cmsPage?.body, x => x.type === 'faqs_block')?.value;
  return {
    seo,
    buttonBlocks,
    carouselBlock,
    iconsBlock,
    videoBlock,
    lessonBlocks,
    faqsBlock,
  };
};

export const fetchFilterOptions = () => {
  const option = {
    url: GET_FILTER_OPTIONS,
    method: 'GET',
  };
  return fetchClient(option);
};

export const fetchProductType = () => {
  const option = {
    url: GET_PRODUCT_TYPES,
    method: 'GET',
  };
  return fetchClient(option);
};

export const generateSearchUrl = (objectFilter, isAddFillter) => {
  const cloneObject = _.cloneDeep(objectFilter);
  _.forEach(cloneObject?.productTypeParentIds || [], (d) => {
    if (d?.icon) {
      delete d.icon;
    }
  });
  return `/all-products/?isAddFillter=${isAddFillter}&objectFilter=${JSON.stringify(cloneObject)}`;
};
