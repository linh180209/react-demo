import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { segmentViewPageIdentify } from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';

function FunctionSegment(props) {
  const history = useHistory();

  const sendSegmentViewPage = () => {
    const login = auth.getLogin();
    if (login && login.user) {
      segmentViewPageIdentify(login.user);
    }
  };

  useEffect(() => history.listen(() => {
    sendSegmentViewPage();
  }), [history]);
  return (
    null
  );
}

export default FunctionSegment;
