import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import testImage from '../../image/test/7.png';
import { perfume, home, giftNotBg } from '../../imagev2/svg';
import { bgProduct } from '../../imagev2/png';
import ButtonCT from '../buttonCT';
import HeaderCT from '../headerCT';

import './styles.scss';
import { isMobile } from '../../DetectScreen';

function ItemProductV2(props) {
  return (
    <div className={classnames('item-product-v2 div-col', props.className)}>
      <div className="div-content-product">
        <div className="image-product" onClick={props.onClickImage}>
          {
            props.imageFull ? (
              <img loading="lazy" src={props.imageFull} alt="bg" />
            ) : (
              <React.Fragment>
                <img loading="lazy" src={bgProduct} alt="bg" />
                <img loading="lazy" className="product-image" src={props.image} alt="product" />
              </React.Fragment>
            )
          }
        </div>
        <div className="div-card div-col">
          <div className="div-type div-row">
            <div className="div-image">
              <img loading="lazy" src={props.icon ? props.icon : props.isPerfume ? perfume : props.isGift ? giftNotBg : home} alt="icon" />
            </div>
            <span>{props.type}</span>
          </div>
          <HeaderCT type={isMobile ? 'Heading-XXS' : 'Heading-S'}>
            {props.name}
          </HeaderCT>
          {
            props.scentName && (
              <span className={classnames('body-text-s-regular', isMobile ? 's-size' : 'm-size')}>
                {props.scentName}
              </span>
            )
          }
          {
            props.price && (
              <span className="other-Tagline">
                {props.price}
              </span>
            )
          }
          {
            props.nameInSide && (
              <ButtonCT
                name={props.nameInSide}
                endIcon={props.endIcon}
                onClick={props.onClick}
                disabled={props.isDisabledButton}
                size={props.buttonSize}
                color={props.color}
              />
            )
          }
        </div>
      </div>
      {
          props.buttonName && (
            <ButtonCT
              name={props.buttonName}
              endIcon={props.endIcon}
              onClick={props.onClick}
              disabled={props.isDisabledButton}
              size={props.buttonSize}
              color={props.color}
            />
          )
        }
    </div>
  );
}

ItemProductV2.defaultProps = {
  isPerfume: true,
  type: '',
  name: '',
  scentName: '',
  price: '',
  buttonName: '',
  endIcon: undefined,
  onClick: () => {},
  isDisabledButton: false,
  buttonSize: undefined,
  onClickImage: () => {},
};

ItemProductV2.propTypes = {
  isPerfume: PropTypes.bool,
  isDisabledButton: PropTypes.bool,
  type: PropTypes.string,
  name: PropTypes.string,
  scentName: PropTypes.string,
  price: PropTypes.string,
  buttonName: PropTypes.string,
  endIcon: PropTypes.shape(),
  onClick: PropTypes.func,
  onClickImage: PropTypes.func,
  buttonSize: PropTypes.string,
};

export default ItemProductV2;
