import React from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import reactHtmlPare from 'react-html-parser';
import './styles.scss';
import AccordionCT from '../accordionCT';
import { isMobile767fn } from '../../DetectScreen';
import { getNameFromButtonBlock } from '../../Redux/Helpers';

function BlockFAQV2(props) {
  const summary = data => (
    <span className={classnames('other-Tagline', isMobile767fn() ? 's-size' : '')}>
      {data}
    </span>
  );

  const details = data => (
    <span className={classnames('body-text-s-regular', isMobile767fn() ? '' : 'm-size')}>
      {reactHtmlPare(data)}
    </span>
  );
  const frequentlyBt = getNameFromButtonBlock(props.buttonBlocks, 'Frequently Asked Question');
  if (props.dataFAQ?.faqs?.length === 0) {
    return null;
  }
  return (
    <div className="block-faq-v2">
      <div className="header-faq">
        {frequentlyBt}
      </div>
      <div className="list-faq">
        {
          _.map(props.dataFAQ?.faqs, d => (
            <AccordionCT
              summary={summary(d?.title || d?.value?.header?.header_text)}
              details={details(d?.description || d?.value?.paragraph)}
            />
          ))
        }
      </div>
    </div>
  );
}

export default BlockFAQV2;
