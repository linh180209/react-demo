import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import {
  Modal, ModalBody,
} from 'reactstrap';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import { addFontCustom, getNameFromButtonBlock } from '../../Redux/Helpers';
import IngredientBlock from '../../views2/productDetailV2/ingredientBlock';
import ButtonCTV2 from '../buttonCT';
import './style.scss';
import NotifiMe from '../../components/ProductAllV2/ProductBlock/notifiMe';
import { isMobile } from '../../DetectScreen';
import useForceRerender from '../../Utils/useForceRerender';

function IngredientPopUpV2(props) {
  const [isShowNotifiMe, setIsShowNotifiMe] = useState();
  const forceRerender = useForceRerender();

  const onNotifiMe = () => {
    setIsShowNotifiMe(true);
  };

  const onCloseNotifiMe = () => {
    setIsShowNotifiMe(false);
  };

  const handleResize = () => {
    forceRerender();
  };

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return (() => {
      window.removeEventListener('resize', handleResize);
    });
  }, []);

  const scentBt = getNameFromButtonBlock(props.buttonBlocks, 'Scent Characteristics');
  const applyBt = getNameFromButtonBlock(props.buttonBlocks, 'APPLY');
  const cancelBt = getNameFromButtonBlock(props.buttonBlocks, 'CANCEL');
  const notifymeBt = getNameFromButtonBlock(props.buttonBlocks, 'notify me');

  return (
    <React.Fragment>
      <Modal className={classnames('modal-full-screen ingredient-popup-v2', addFontCustom())} isOpen fade={false}>
        <ModalBody style={isMobile ? { height: `${window.innerHeight}px` } : {}}>
          <div className="content-popup">
            <div className="header-popup">
              <button type="button" className="button-bg__none" onClick={props.onCancel}>
                <ArrowBackIosNewIcon style={{ color: '#2c2c2c' }} />
              </button>
              <div className="other-Tagline">
                {scentBt}
              </div>
            </div>
            <IngredientBlock data={props.data} />
          </div>
          <div className="list-button">
            <ButtonCTV2 name={cancelBt} variant="outlined" onClick={props.onCancel} />
            <ButtonCTV2 name={props.data?.is_out_of_stock ? notifymeBt : applyBt} onClick={props.data?.is_out_of_stock ? onNotifiMe : props.onClickApply} />
          </div>
        </ModalBody>
      </Modal>
      {
        isShowNotifiMe && (
          <NotifiMe buttonBlocks={props.buttonBlocks} onClose={onCloseNotifiMe} data={props.data} />
        )
      }
    </React.Fragment>
  );
}

export default IngredientPopUpV2;
