/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import InfoIcon from '@mui/icons-material/Info';
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import ButtonCT from '../buttonCT';
import './styles.scss';
import { isBrowser, isMobile } from '../../DetectScreen';

function SwitchButton(props) {
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  return (
    <div className="switch-button div-row">
      <div id="switch-button-product" />
      <button className="switch-content div-row" type="button" onClick={props.onChange}>
        <div className={classnames('body-text-s-regular', isMobile ? '' : 'l-size', 'item')}>
          {props.name1}
        </div>
        <div className={classnames('body-text-s-regular', isMobile ? '' : 'l-size', 'item')}>
          {props.name2}
        </div>
        <div className={classnames('bt-active', !props.value ? '' : 'r-right')}>
          {!props.value ? props.name1 : props.name2}
        </div>
      </button>
      {
      isBrowser && (
        <React.Fragment>
          <div
            className="icon-info-filter"
            aria-describedby={open ? 'id-switch-button' : undefined}
            aria-haspopup="true"
            onMouseEnter={handleClick}
            onMouseLeave={handleClose}
          >
            <InfoIcon style={{ color: '#2c2c2c' }} />
          </div>

          <Popover
            id="id-switch-button"
            sx={{
              pointerEvents: 'none',
            }}
            open={open}
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'left',
            }}
            onClose={handleClose}
            disableRestoreFocus
          >
            <Typography style={{
              fontSize: '14px',
              color: '#EBD8B8',
              height: '41px',
              padding: '0px 10px',
              lineHeight: '41px',
              borderRadius: '20px',
              background: '#2c2c2c',
            }}
            >
              {props.tooltip}
            </Typography>
          </Popover>
        </React.Fragment>
      )
    }
    </div>
  );
}

export default SwitchButton;
