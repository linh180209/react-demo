import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './styles.scss';
import { useHistory } from 'react-router-dom';
import HeaderCT from '../headerCT';
import ButtonCT from '../buttonCT';
import { generateUrlWeb, getAltImageV2, scrollToTargetAdjusted } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

function SliderHero(props) {
  const history = useHistory();

  const onClick = (link) => {
    if (link) {
      history.push(generateUrlWeb(link));
    } else if (props.onClick) {
      props.onClick();
    }
  };

  const gotoScent = () => {
    scrollToTargetAdjusted('id-create-scent');
  };

  const itemBlock = d => (
    <div className="item-block-hero">
      <img src={d?.value?.logo?.image} alt={getAltImageV2(d?.value?.logo)} />
      <div className="block-text">
        <HeaderCT type={isMobile ? 'Heading-XXS' : 'Heading-L'}>
          {d?.value?.header?.header_text}
        </HeaderCT>
        {
          d?.value?.button?.name && (
            <ButtonCT name={d?.value?.button?.name} onClick={() => (d?.value?.button?.link ? gotoScent() : onClick(d?.value?.button?.link))} size={isMobile ? 'small' : undefined} />
          )
        }
      </div>
    </div>
  );

  const settings = {
    dots: true,
    // infinite: true,
    // speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    autoplaySpeed: 5000,
    initialSlide: 0,
  };
  return (
    <div className="silder-hero">
      <div className="content-hero">
        <Slider
          {... settings}
        >
          {
              _.map(props.data?.value, d => (
                <div>
                  {itemBlock(d)}
                </div>
              ))
          }
        </Slider>
      </div>
    </div>
  );
}

export default React.memo(SliderHero);
