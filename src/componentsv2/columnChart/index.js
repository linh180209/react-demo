import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import { isMobile } from '../../DetectScreen';
import './styles.scss';

function ColumnChart(props) {
  return (
    <div className="column-chart div-col">
      <div className="div-chart div-col">
        <div className="div-column">
          <div className="column-value" style={{ background: props.color, height: `${props.weight}%` }} />
        </div>
        {/* <div className="div-image">
          <img src={props.icon} alt="icon" />
        </div> */}
      </div>
      {
        !props.isDisableTitle && (
          <div className={classnames('body-text-s-regular', isMobile ? 's-s-size' : '')}>
            {props.title}
          </div>
        )
      }

    </div>
  );
}

export default ColumnChart;
