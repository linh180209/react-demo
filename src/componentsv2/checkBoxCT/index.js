import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import './styles.scss';

function CheckBoxCT(props) {
  return (
    <div className={classnames('checkbox-ct', props.className)}>
      <FormControlLabel
        control={<Checkbox checked={props.checked} />}
        label={props.label}
        onChange={props.onChange}
      />
      {
        props.messageError && (
          <div className="message-error">
            {props.messageError}
          </div>
        )
      }
    </div>
  );
}

CheckBoxCT.defaultProps = {
  checked: false,
  className: '',
};

CheckBoxCT.propTypes = {
  checked: PropTypes.bool,
  className: PropTypes.string,
};

export default CheckBoxCT;
