import React, { useState, useEffect } from 'react';
import './styles.scss';

function LineChart(props) {
  return (
    <div className="line-chart div-row">
      <div className="text-title">
        {props.title}
      </div>
      <div className="line-bg">
        <div className="line-value" style={{ background: props.color, width: `${props.value}%` }} />
      </div>
    </div>
  );
}

export default LineChart;
