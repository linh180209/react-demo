/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import StarBorderIcon from '@mui/icons-material/StarBorder';
import _ from 'lodash';
import React from 'react';
import classnames from 'classnames';
import { generaCurrency, getNameFromButtonBlock } from '../../../Redux/Helpers';
import ButtonCT from '../../buttonCT';
import './styles.scss';

function ItemLike(props) {
  const intensityBt = getNameFromButtonBlock(props.buttonBlocks, 'Intensity');
  return (
    <div className="item-like div-col">
      <div className="tag-top">
        {props.data?.short_description}
      </div>
      <img src={props.data?.image} alt="like" onClick={props.onClick} />
      {/* <div className="text-1">
        <StarBorderIcon />
        {props.data?.type?.alt_name}
      </div> */}
      <div className="text-2">
        {props.data?.name}
      </div>
      <div className="text-3">
        {`${_.map(props.data?.ingredients || [], x => _.startCase(x)).join(' & ')} | ${props.data?.family}`}
      </div>
      <div className="text-4">
        <div className="text-title">
          {intensityBt}
          :
        </div>
        {
          _.map(_.range(5), (d, index) => (
            <div className={classnames('point-black', props.data?.intensity > index ? 'active' : '')} />
          ))
        }
      </div>
      <div className="text-5">
        {generaCurrency(props.data?.price)}
      </div>
      <ButtonCT name="View Product" onClick={props.onClick} />
    </div>
  );
}

export default ItemLike;
