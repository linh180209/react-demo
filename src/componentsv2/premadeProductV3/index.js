import _ from 'lodash';
import React, { Component } from 'react';
import { isMobile767fn } from '../../DetectScreen';
import HeaderCT from '../headerCT';
import ItemLike from './itemLike';
import './styles.scss';

class PremadeProductV3 extends Component {
  componentDidMount() {
    if (this.props.fetchData && (!this.props.data || this.props.data.length === 0)) {
      this.props.fetchData();
    }
  }

  onClickAddToCart = (data) => {
    const {
      basket, createBasketGuest, addProductBasket,
    } = this.props;
    const { id, name, price } = data;
    const dataT = {
      item: id,
      name,
      price,
      quantity: 1,
      is_featured: data.is_featured,
    };
    const dataTemp = {
      idCart: basket.id,
      item: dataT,
    };
    if (!basket.id) {
      createBasketGuest(dataTemp);
    } else {
      addProductBasket(dataTemp);
    }
  };

  render() {
    const { data } = this.props;
    if (_.isEmpty(data)) {
      return null;
    }
    return (
      <div className="product-detail-like-v3 div-col">
        <div className="header div-row">
          <div className="header-text tc">
            {this.props.title}
          </div>
        </div>
        <div className="list-item-like">
          {
            _.map(data, d => (
              <ItemLike data={d} onClick={() => this.props.onClickGotoProduct(d)} buttonBlocks={this.props.buttonBlocks} />
            ))
          }
        </div>
      </div>
    );
  }
}


export default PremadeProductV3;
