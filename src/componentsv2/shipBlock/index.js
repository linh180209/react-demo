import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './styles.scss';
import { getAltImageV2 } from '../../Redux/Helpers';
import { isBrowser } from '../../DetectScreen';

function ShipBlock(props) {
  const itemBlock = d => (
    <div className="item-block-ship div-row">
      <img src={d?.value?.image?.image} alt={getAltImageV2(d?.value?.image)} />
      <div className="text-ship">
        {d?.value?.header?.header_text}
      </div>
    </div>
  );
  const settings = {
    dots: true,
    // infinite: true,
    // speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    arrows: false,
    autoplaySpeed: 5000,
    initialSlide: 0,
  };
  return (
    <div className="ship-block div-row">
      {
        isBrowser ? (
          _.map(props.data, d => (
            itemBlock(d)
          ))
        ) : (
          <Slider
            {... settings}
          >
            {
               _.map(props.data, d => (
                 <div>
                   {itemBlock(d)}
                 </div>
               ))
            }
          </Slider>
        )
      }

    </div>
  );
}

export default React.memo(ShipBlock);
