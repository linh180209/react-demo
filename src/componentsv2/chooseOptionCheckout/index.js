import React, { useState, useEffect } from 'react';
import Radio from '@mui/material/Radio';
import classnames from 'classnames';
import _ from 'lodash';
import { isMobile } from '../../DetectScreen';
import './styles.scss';
import AccordionCT from '../accordionCT';
import CheckoutMethod from '../../views2/checkOutV2/infoUser/checkoutMethod';

function ChooseOptionCheckOut(props) {
  const htmlCheckoutMethod = () => (
    <div
      className={classnames('choose-option-checkout div-row', props.className, props.disabled ? 'disabled' : '')}
      onClick={!props.disabled ? props.onClick : () => {}}
    >
      <div className="div-left-choose fit-width div-row">
        <Radio
          checked={props.checked}
          onChange={!props.disabled ? props.onChange : undefined}
          value={props.value}
          name="radio-buttons"
          sx={{
            color: '#2c2c2c',
            '&.Mui-checked': {
              color: '#2c2c2c',
            },
          }}
        />
      </div>
      <CheckoutMethod
        handleTokenCheckoutCom={props.handleTokenCheckoutCom}
        updateValidateCheckoutCom={props.updateValidateCheckoutCom}
      />
    </div>
  );

  const htmlSummary = () => (
    <div
      className={classnames('choose-option-checkout div-row', props.className, props.disabled ? 'disabled' : '')}
      onClick={!props.disabled ? props.onClick : () => {}}
    >
      <div className="div-left-choose div-row">
        <Radio
          checked={props.checked}
          onChange={!props.disabled ? props.onChange : undefined}
          value={props.value}
          name="radio-buttons"
          sx={{
            color: '#2c2c2c',
            '&.Mui-checked': {
              color: '#2c2c2c',
            },
          }}
        />
        {
          props.icon ? (
            <div className="label-left-icon div-row">
              <img src={props.icon} alt="icon" />
              <div className={classnames('body-text-s-regular', isMobile ? ' s-size' : 'm-size')}>
                <b>{props.text}</b>
              </div>
              {
                props.text2 && (
                  <div className="text-2">
                    <i>{props.text2}</i>
                  </div>
                )
              }
            </div>
          ) : props.icons ? (
            <div className="label-left-icon div-icons">
              <div className={classnames('body-text-s-regular', isMobile ? ' s-size' : 'm-size')}>
                <b>{props.text}</b>
              </div>
              <div className="div-icon">
                {
                  _.map(props.icons, d => (
                    <img src={d} alt="icon" />
                  ))
                }
              </div>
            </div>
          ) : (
            <div className="label-left div-col">
              <div className={classnames('body-text-s-regular', isMobile ? ' s-size' : 'm-size')}>
                <b>{props.title}</b>
                {' '}
                {props.title1}
              </div>
              {
                props.description && (
                  <div className={classnames('body-text-s-regular', isMobile ? ' s-size' : 'm-size')}>
                    {props.description}
                  </div>
                )
              }

            </div>
          )
        }
      </div>
      {
        props.textRight && (
          <div className="div-right">
            {props.textRight}
          </div>
        )
      }
      {
        props.buttonRight && (
          <button className="div-right button-bg__none bt-change" type="button" onClick={props.onClickRight}>
            {props.buttonRight}
          </button>
        )
      }
    </div>
  );
  if (props.isAccordion) {
    return (
      <AccordionCT
        className="accordion-drop"
        summary={htmlSummary()}
        details={props.details}
        expanded={props.expanded}
      />
    );
  }
  if (props.isCheckOutMethod) {
    return htmlCheckoutMethod();
  }

  return (
    htmlSummary()
  );
}

export default ChooseOptionCheckOut;
