import React, { useState, useEffect } from 'react';
import AccordionCT from '../accordionCT';
import './styles.scss';

function DropdownCheckout(props) {
  return (
    <div className="div-dropdown-checkout">
      <AccordionCT
        className="dropdown-checkout"
        summary={props.summary}
        details={props.details}
        expanded={props.expanded}
        onChange={props.onChange}
      />
    </div>
  );
}

export default React.memo(DropdownCheckout);
