import React from 'react';
import classnames from 'classnames';
import { isMobile } from '../../DetectScreen';
import HeaderCT from '../headerCT';
import './styles.scss';

function NumberHeader(props) {
  return (
    <div className={classnames('number-header div-row', props.isDisabled ? 'disabled' : '')}>
      <div className="number-text">
        {props.number}
      </div>
      {
        props.isHeader ? (
          <HeaderCT type={isMobile ? 'Heading-XS' : 'Heading-S'}>
            {props.text}
          </HeaderCT>
        ) : (
          <div className={classnames('body-text-s-regular', isMobile ? '' : 'l-size')}>
            <b>{props.text}</b>
          </div>
        )
      }
    </div>
  );
}

export default NumberHeader;
