import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import './styles.scss';

function CardCT(props) {
  return (
    <div className={classnames('cart-ct', props.className)}>
      {props.children}
    </div>
  );
}

export default CardCT;
