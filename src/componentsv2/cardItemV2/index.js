/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import classnames from 'classnames';
import _ from 'lodash';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import AddIcon from '@mui/icons-material/Add';
import {
  getUrlGotoProduct, getAltImage, getCmsCommon, getNameFromCommon, generaCurrency, trackGTMRemoveCart, trackGTMAddToCart, generateUrlWeb,
} from '../../Redux/Helpers/index';
import icSub from '../../image/icon/ic-sub.svg';
import icPlus from '../../image/icon/ic-plus.svg';
import BottleCustom from '../../components/B2B/CardItem/bottleCustom';
import bottleHomeScent from '../../image/bottle_home_card.png';
import bottleBundleCreation from '../../image/bundle_creation_thumnail.jpeg';
import CustomeBottleV4 from '../customeBottleV4';
import { isBrowser } from '../../DetectScreen';
import './styles.scss';
import HeaderCT from '../headerCT';
import ButtonCT from '../buttonCT';
import { gift, candleHolder, icMiniCandleHolder, icGift } from '../../imagev2/svg';
import { GIFT_BUNDLES, GIFT_RECIPIENT_FORM, IS_GIFTING } from '../../views2/GiftV2/constants';


class CardItemV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: props.data.quantity,
      oldQuantity: props.data.quantity,
      oldData: props.data,
      isOpenCustomBottle: false,
      dataCustom: {
        currentImg: props.data.image,
        nameBottle: props.data.is_display_name ? props.data.name : '',
        font: props.data.font,
        color: props.data.color,
        imagePremade: undefined,
      },
    };
    this.timeOutCall = undefined;
  }


  onChange = (e) => {
    const { value } = e.target;
    const { idCart } = this.props;
    this.setState({ quantity: value });
    const data = _.assign({}, this.props.data);
    data.quantity = parseFloat(value, 10);
    data.total = parseFloat(data.quantity, 10) * parseFloat(data.price, 10);
    if (this.props.updateProductBasket) {
      this.props.updateProductBasket({
        idCart,
        item: data,
      });
    }
  }

  onChangeQuantity = (value, increase) => {
    const { idCart } = this.props;
    const data = _.assign({}, this.props.data);
    data.quantity = parseFloat(value, 10);
    if (this.props.updateProductBasket) {
      this.props.updateProductBasket({
        idCart,
        item: data,
        increase,
      });
    }
    // }, 1000);
    this.setState({ quantity: value });
  }

  onIncreateQuantity = () => {
    const { quantity } = this.state;
    this.onChangeQuantity(quantity + 1, true);
    this.setState({ quantity: quantity + 1 });

    // tracking GTM
    // const data = _.cloneDeep(this.props.data);
    // data.quantity = 1;
    // trackGTMAddToCart(data);
  }

  onDecreateQuantity = () => {
    const { quantity } = this.state;
    if (quantity > 1) {
      this.onChangeQuantity(quantity - 1);
      this.setState({ quantity: quantity - 1 });

      // tracking GTM
      const data = _.cloneDeep(this.props.data);
      data.quantity = 1;
      trackGTMRemoveCart(data);
    }
  }

  onClickDelete = () => {
    if (this.props.deleteProductBasket) {
      const { idCart, data } = this.props;
      this.props.deleteProductBasket({
        idCart,
        item: data,
      });
    }
  }

  onGotoProduct = (isCustome) => {
    const { data, isB2C } = this.props;
    if (!isB2C) {
      return;
    }
    const dataUrl = getUrlGotoProduct(data);
    let urlPro = dataUrl.url;
    const { meta, id, quantity } = data;
    if (meta && meta.url) {
      urlPro = meta.url;
    }
    if (urlPro) {
      const {
        image, name, item, is_black: isBlack, is_display_name: isDisplayName, font, color,
      } = data;
      const custome = dataUrl.isPerfume ? {
        image,
        name: isDisplayName ? name : '',
        isCustome,
        font,
        color,
        idCartItem: id,
        quantity,
      } : undefined;
      this.props.history.push(generateUrlWeb(urlPro), { custome });
      if (this.props.onCloseCard) {
        this.props.onCloseCard();
      }
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { data } = nextProps;
    const objectReturn = {};
    if (data && (data.quantity !== prevState.oldQuantity || data !== prevState.oldData)) {
      _.assign(objectReturn, { quantity: data.quantity, oldQuantity: data.quantity, oldData: data });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  getCountry = (code) => {
    const { listCountry } = this.props;
    if (!code || !listCountry) {
      return '';
    }
    const ele = _.find(listCountry, x => x.code === code);
    return ele ? ele.name : '';
  }

  getTimeEmail = (epoch) => {
    if (!epoch) {
      return '';
    }
    return moment.unix(epoch).format('DD/MM/YYYY');
  }

  handleCropImage = (img) => {
    const { dataCustom } = this.state;
    dataCustom.currentImg = img;
    this.setState({ dataCustom });
  };

  onSaveCustomer = (data) => {
    const { dataCustom } = this.state;
    const {
      image, name, color, font, imagePremade,
    } = data;
    dataCustom.color = color;
    dataCustom.font = font;
    dataCustom.imagePremade = imagePremade;
    if (_.isEmpty(image)) {
      dataCustom.currentImg = undefined;
      dataCustom.nameBottle = name;
      this.forceUpdate();
      return;
    }
    dataCustom.currentImg = image;
    dataCustom.nameBottle = name;
    this.forceUpdate();
  }

  prepareDataItemCart = async (dataSelect) => {
    const {
      item: itemCombo, quantity, meta, id: idItem,
    } = dataSelect;
    const { dataCustom } = this.state;
    const bottleImage = dataCustom.currentImg
      ? await fetch(dataCustom.currentImg).then(r => r.blob())
      : undefined;
    if (itemCombo) {
      const { price, id } = itemCombo;
      const data = {
        id: idItem,
        item: id,
        font: dataCustom.font,
        color: dataCustom.color,
        imagePremade: dataCustom.imagePremade,
        is_display_name: (!bottleImage && !!dataCustom.nameBottle) || !!dataCustom.nameBottle,
        is_customized: !!bottleImage || !!dataCustom.imagePremade,
        name: dataCustom.nameBottle ? dataCustom.nameBottle : dataSelect.name,
        price,
        quantity,
        file: bottleImage
          ? new File([bottleImage], 'product.png')
          : undefined,
        meta,
      };
      const dataTemp = {
        idCart: this.props.idCart,
        item: data,
      };
      return dataTemp;
    }
  };

  handleUpdateCart = async (dataSelect, index = 0) => {
    const {
      updateProductBasket,
    } = this.props;
    const dataTemp = await this.prepareDataItemCart(dataSelect, index);
    if (this.props.idCart) {
      _.assign(dataTemp, { isUpdateFromProductDetail: true });
      updateProductBasket(dataTemp);
    }
  }

  onClickUpdatetoCart = async () => {
    const { data } = this.props;
    this.handleUpdateCart(data);
  }

  onClickOpenCustomBottle = () => {
    if (this.props.onOpenCustomBottle) {
      this.props.onOpenCustomBottle();
    }
    this.setState({ isOpenCustomBottle: true });
  }

  render() {
    const {
      data, isCheckOut, cms, isCheckOutClub, isCheckOutGift, isPaySuccess,
      isStyleMobile,
    } = this.props;
    const isStyleBrowser = isBrowser && !isStyleMobile;
    const { quantity, isOpenCustomBottle, dataCustom } = this.state;
    const {
      item, total, combo, image, additional_fee: additionalFee,
      image_display: imgDisplay, meta, name, is_customized: isCustomized,
      is_display_name: isDisplayName, is_black: isBlack, font, color, is_free_gift: isFree,
      is_free, external_product: externalProduct,
    } = data;
    const isPerfumeProduct = ['Perfume', 'Scent', 'dual_crayons', 'Creation', 'discovery_box', 'Wax_Perfume'].includes(item.product.type.name);
    const isGift = item.product.type.name === 'Gift' || IS_GIFTING.includes(meta?.typeOfGift);
    const isGiftBox = item.product.type.name === 'gift_wrap';
    const isCandles = ['single_candle', 'dual_candles'].includes(item.product.type.name);
    const isBestSeller = item.is_best_seller;
    // flags for image display (best seller will display image_display)
    const isShowBottle = ((item.product.type.name === 'Creation' && isCustomized) || item.product.type.name === 'Perfume' || item.product.type.name === 'bundle_creation' || item.product.type.name === 'perfume_diy' || item.product.type.name === 'Elixir' || (isGift && isCustomized)) && !isBestSeller && item.is_sample === false;
    // flags for custome botlle (include best seller)
    const isShowCustomBottle = ((item.product.type.name === 'Creation' && isCustomized) || item.product.type.name === 'Perfume' || item.product.type.name === 'bundle_creation' || item.product.type.name === 'perfume_diy' || item.product.type.name === 'Elixir' || (isGift && isCustomized)) && item.is_sample === false;
    const isBundleCreation = item.product.type.name === 'bundle_creation';
    const isMiniCandle = item.product.type.name === 'mini_candles';
    const isDualCandles = item.product.type.name === 'dual_candles';
    const isHomeScent = item.product.type.name === 'home_scents' && combo && combo.length > 0;
    const isRollOn = (item.product.type.name === 'Perfume' || item.product.type.name === 'Scent') && item.is_sample === true;

    const combos = combo ? _.filter(combo, x => x.product.type === 'Scent') : [];
    const itemBottle = _.find(combo, x => x.product.type === 'Bottle');
    const arrayThumbs = [];
    if (itemBottle && itemBottle.product.images) {
      _.forEach(itemBottle.product.images, (x) => {
        if (x && x.type === 'suggestion') {
          arrayThumbs.push(x);
        }
      });
    }

    const cmsCommon = getCmsCommon(cms);
    const customDesignBt = getNameFromCommon(cmsCommon, 'CUSTOM_DESIGN');
    const editCustomBt = getNameFromCommon(cmsCommon, 'Edit_Customization');
    const removeBt = getNameFromCommon(cmsCommon, 'Remove');
    const textBt = getNameFromCommon(cmsCommon, 'Text');
    const imageTextBt = getNameFromCommon(cmsCommon, 'Image_Text');
    const freeBt = getNameFromCommon(cmsCommon, 'FREE');
    const eauBt = getNameFromCommon(cmsCommon, 'Eau_de_parfum');
    const mlBt = getNameFromCommon(cmsCommon, '25ml');
    const mouthBt = getNameFromCommon(cmsCommon, 'Month');
    const addFreeBt = getNameFromCommon(cmsCommon, 'Add Free Engraving');
    const persionalityBt = getNameFromCommon(cmsCommon, 'Personalize the design of your bottle for free');
    const addBt = getNameFromCommon(cmsCommon, 'Add');
    const perfumeBt = getNameFromCommon(cmsCommon, 'Perfume');
    const giftBt = getNameFromCommon(cmsCommon, 'Gift');
    const homescentBt = getNameFromCommon(cmsCommon, 'Homescent');
    const freeGiftBt = getNameFromCommon(cmsCommon, 'FREE GIFT');
    const colorBt = getNameFromCommon(cmsCommon, 'Color');
    const moonCandleBt = getNameFromCommon(cmsCommon, 'Complimentary Moon Candle holder included');
    const petiteCandleBt = getNameFromCommon(cmsCommon, 'Complimentary Petite candle holder included');

    const isNotShowSub = quantity < 2 || is_free || isPaySuccess;
    const isNotShowPlus = isFree || is_free || isPaySuccess;
    const receiver = meta && meta.receiver ? meta.receiver : {};
    const sender= meta && meta.sender ? meta.sender : {};
    const isImageText = !!image;
    const isText = name && !image;
    const htmlGift = (
      <React.Fragment>
        <span className="sp-text mt-2">
          {`To: ${receiver.name}`}
        </span>
        <span className={receiver.country && !isGift ? 'sp-text' : 'hidden'}>
          {`Country: ${this.getCountry(receiver.country)}`}
        </span>
        <span className="sp-text">
          {meta?.type === GIFT_RECIPIENT_FORM ? `Emailed on: ${this.getTimeEmail(receiver.date_sent)}` : `From: ${sender.name}`}
        </span>
      </React.Fragment>

    );
    // const imgDisplay = imageBottle ? imageBottle.image : '';
    const htmlRollOn = (
      <div className="div-col div-custome mt-0">
        <div className="div-col title-name">
          {
            _.map(combos, (d, index) => (
              <span>
                <i>{`${index > 0 ? '& ' : ''}${d.name}`}</i>
              </span>
            ))
          }
        </div>
      </div>
    );

    const htmlCustome = (
      <div className="div-col div-custome">
        <div className="div-edit">
          <div className="text-custom">
            {textBt}
            {': '}
            {data.name}
          </div>
          {
            isStyleBrowser && (
              <ButtonCT
                name={editCustomBt}
                variant="text"
                size="small"
                onClick={this.onClickOpenCustomBottle}
              />
            )
          }
        </div>
      </div>
    );

    const inCreateDeleteHtml = () => (
      <div className="div-row div-remove justify-between items-center">

        {
          !isStyleBrowser && isShowBottle ? (
            !isImageText ? (
              <ButtonCT
                name={addFreeBt}
                endIcon={<AddIcon style={{ fontSize: '12px' }} />}
                variant="outlined"
                size="small"
                className="button-add-custome"
                onClick={this.onClickOpenCustomBottle}
              />
            ) : (
              <ButtonCT
                name={editCustomBt}
                variant="outlined"
                size="small"
                className="button-edit"
                onClick={this.onClickOpenCustomBottle}
              />
            )
          ) : (<div />)
        }
        {
          isStyleBrowser && (
            <IconButton aria-label="delete" onClick={this.onClickDelete} className="bt-delete">
              <DeleteIcon className="icon-delete" />
            </IconButton>
          )
        }

        {/* <div className={isGift ? 'hidden' : 'div-button'}> */}
        <div className='div-button'>
          <button
            disabled={isNotShowSub}
            type="button"
            className="button-bg__none pr-4"
            onClick={this.onDecreateQuantity}
          >
            {
              isNotShowSub
                ? (
                  <div style={{ width: '11px' }} />
                )
                : (
                  <img loading="lazy" src={icSub} alt="icSub" />
                )
            }
          </button>
          <span>
            {quantity}
          </span>
          <button
            type="button"
            disabled={isNotShowPlus}
            className="button-bg__none pl-4"
            onClick={this.onIncreateQuantity}
          >
            {
              isNotShowPlus
                ? (
                  <div style={{ width: '11px' }} />
                )
                : (
                  <img loading="lazy" src={icPlus} alt="icPlus" />
                )
            }
          </button>
        </div>
      </div>
    );

    const htmlCart = (
      <React.Fragment>
        <div className="div-col items-center justify-between image-custome">

          {
            isBundleCreation ? (
              <div style={{ position: 'relative', display: 'flex', justifyContent: 'center' }}>
                <img
                  loading="lazy"
                  src={bottleBundleCreation}
                  alt={getAltImage(imgDisplay)}
                  style={{
                    width: isStyleBrowser ? '80%' : '100%',
                    cursor: 'pointer',
                    objectFit: 'contain',
                    height: isStyleBrowser ? '90px' : '100px',
                  }}
                  onClick={() => this.onGotoProduct(false)}
                />
              </div>
            ) : (isShowBottle || isHomeScent) ? (
              <BottleCustom
                isImageText={isImageText}
                onGotoProduct={() => this.onGotoProduct(false)}
                image={image}
                isBlack={isBlack}
                font={font}
                color={color}
                eauBt={eauBt}
                mlBt={mlBt}
                isDisplayName={isDisplayName}
                name={name}
                combos={combos}
                bottleImage={isHomeScent ? bottleHomeScent : undefined}
              />
            ) : isRollOn ? (
              <React.Fragment>
                <div style={{ position: 'relative', display: 'flex', justifyContent: 'center' }}>
                  <img
                    loading="lazy"
                    src={imgDisplay}
                    alt={getAltImage(imgDisplay)}
                    style={{
                      width: isStyleBrowser ? '80%' : '100%',
                      cursor: 'pointer',
                      objectFit: 'contain',
                      height: isStyleBrowser ? '90px' : '100px',
                    }}
                    onClick={() => this.onGotoProduct(false)}
                  />
                </div>
              </React.Fragment>
            ) : (
              <img
                loading="lazy"
                src={imgDisplay}
                alt={getAltImage(imgDisplay)}
                style={{
                  width: '80%',
                  cursor: 'pointer',
                  objectFit: 'contain',
                  height: '131px',
                }}
                onClick={() => this.onGotoProduct(false)}
              />
            )
          }
        </div>
        <div className="div-col div-text-content">
          <div className="div-col div-content">
            <div className="div-row justify-between">
              <div className="div-left div-col">
                {
                  isNotShowPlus && (
                    <div className="div-gift-icon">
                      <img src={gift} alt="icon" />
                      <span>{freeGiftBt}</span>
                    </div>
                  )
                }
                {/* <div className="div-header">
                  <span className="sp-title">
                    {isPerfumeProduct ? perfumeBt : (isGift || isGiftBox) ? giftBt : homescentBt}
                  </span>
                </div> */}
                <HeaderCT
                  type={isStyleBrowser ? 'Heading-S' : 'Heading-XXS'}
                  className="sp-text"
                >
                  {/* {item.product.type.alt_name} */}
                  <div className='div-wrapper'>
                    {isGift && <img src={icGift} alt='cart-icon' />}
                    {meta?.typeOfGift && meta.typeOfGift !== GIFT_BUNDLES ? meta?.giftName : item.product.type.alt_name}
                  </div>
                </HeaderCT>
                {
                  isShowBottle ? (
                    <div className={isShowBottle ? 'div-row title-name' : 'hidden'}>
                      {
                        _.map(combos, (d, index) => (
                          <span>
                            {`${index > 0 ? '- ' : ''}${d.name}`}
                          </span>
                        ))
                      }
                    </div>
                  ) : (
                    <div className="div-row title-name">
                      { isBestSeller ? `${_.map(item.ingredients || [], x => _.startCase(x)).join(' - ')}` : data?.name }
                    </div>
                  )
                }
                {/* {
                  isCandles && item?.variant_values?.length > 0 && (
                    <div className="text-color-candle">
                      {colorBt}
                      :
                      {' '}
                      {item.variant_values[0]?.name}
                    </div>
                  )
                } */}
                {
                  (isDualCandles || (isMiniCandle && combo.length > 2)) && (
                    <div className="dual-holder div-row">
                      <img src={isMiniCandle ? icMiniCandleHolder : candleHolder} alt="icon" />
                      {isMiniCandle ? petiteCandleBt : moonCandleBt}
                    </div>
                  )
                }
                <div className={isCheckOut ? 'hidden' : 'sp-text-price'}>
                  {`${generaCurrency(total)}${isCheckOutClub ? `/${mouthBt}` : ''}`}
                </div>

                {isShowCustomBottle && isImageText ? htmlCustome : ''}
                {isGift ? htmlGift : ''}
                {isRollOn && htmlRollOn}
              </div>
              {
                !isStyleBrowser && (
                  <div className="div-delete-style-mobile">
                    <IconButton aria-label="delete" onClick={this.onClickDelete} className="bt-delete">
                      <DeleteIcon className="icon-delete" />
                    </IconButton>
                  </div>
                )
              }
              {isStyleBrowser && inCreateDeleteHtml()}

            </div>
            {
            !isImageText && isShowCustomBottle && (
              <div className={classnames('add-bottle-custom', !isStyleBrowser ? 'hidden' : '')} onClick={this.onClickOpenCustomBottle}>
                <span className="title-add">{addFreeBt}</span>
                {
                  isStyleBrowser && (<span classNam="body-text-s-regular s-size">{persionalityBt}</span>)
                }
                {
                  isStyleBrowser ? (
                    <ButtonCT
                      name={addBt}
                      variant="text"
                      size="small"
                      onClick={this.onClickOpenCustomBottle}
                    />
                  ) : (
                    <ButtonCT
                      variant="text"
                      size="small"
                      onlyIcon={<AddIcon />}
                      onClick={this.onClickOpenCustomBottle}
                    />
                  )
                }

              </div>
            )
          }
          </div>
        </div>
      </React.Fragment>
    );

    return (
      <div className={classnames('new-cartItem-v2 div-col', this.props.className)}>
        <div className="div-row">
          {
            isOpenCustomBottle ? (
              <CustomeBottleV4
                cmsCommon={cmsCommon}
                arrayThumbs={arrayThumbs}
                handleCropImage={this.handleCropImage}
                currentImg={dataCustom.currentImg}
                nameBottle={dataCustom.nameBottle}
                font={dataCustom.font}
                color={dataCustom.color}
                closePopUp={() => {
                  if (this.props.onCloseCustomBottle) {
                    this.props.onCloseCustomBottle();
                  }
                  this.setState({ isOpenCustomBottle: false });
                }
                }
                onSave={this.onSaveCustomer}
                // itemBottle={itemBottle}
                // cmsTextBlocks={textBlock}
                name1={combos && combos.length > 0 ? combos[0].name : ''}
                name2={combos && combos.length > 1 ? combos[1].name : ''}
                onClickUpdatetoCart={this.onClickUpdatetoCart}
                idCartItem={this.props.idCart}
              />
            ) : (<div />)
          }
          {htmlCart}
        </div>
        <div className={classnames('compo-remove', !isStyleBrowser ? 'style-mobile' : '')}>
          {!isStyleBrowser && inCreateDeleteHtml()}
        </div>
      </div>
    );
  }
}

CardItemV2.defaultProps = {
  isPaySuccess: false,
};

CardItemV2.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    total: PropTypes.number,
    image_urls: PropTypes.string,
    quantity: PropTypes.number,
    original_quantity: PropTypes.number,
    id: PropTypes.string,
    item: PropTypes.string,
  }).isRequired,
  deleteProductBasket: PropTypes.func.isRequired,
  updateProductBasket: PropTypes.func.isRequired,
  onCloseCard: PropTypes.func.isRequired,
  idCart: PropTypes.number.isRequired,
  isB2C: PropTypes.bool.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  isCheckOut: PropTypes.bool.isRequired,
  isCheckOutClub: PropTypes.bool.isRequired,
  cms: PropTypes.shape().isRequired,
  style: PropTypes.shape().isRequired,
  listCountry: PropTypes.arrayOf().isRequired,
  isPaySuccess: PropTypes.bool,
};

export default withRouter(CardItemV2);
