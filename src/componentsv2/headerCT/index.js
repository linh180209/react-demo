import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import classnames from 'classnames';
import Typography from '@mui/material/Typography';
import './styles.scss';

const themeBase = createTheme({
  palette: {
    primary: {
      main: '#2C2C22',
    },
  },
  typography: {
    h1: {
      fontFamily: 'unset',
      fontSize: 72,
      fontWeight: 400,
    },
    h2: {
      fontFamily: 'unset',
      fontSize: 48,
      fontWeight: 400,
    },
    h3: {
      fontFamily: 'unset',
      fontSize: 36,
      fontWeight: 400,
    },
    h4: {
      fontFamily: 'unset',
      fontSize: 20,
      fontWeight: 400,
    },
    h5: {
      fontFamily: 'unset',
      fontSize: 18,
      fontWeight: 400,
    },
    h6: {
      fontFamily: 'unset',
      fontSize: 16,
      fontWeight: 400,
    },
  },
});

function HeaderCT(props) {
  const theme = createTheme(themeBase, {

  });

  const onClick = () => {
    if (props.onClick) {
      props.onClick();
    }
  };

  return (
    <ThemeProvider theme={theme}>
      {
        props.type === 'Heading-XL' ? (
          <Typography variant="h1" className={classnames('heading ttu', props.className)} onClick={onClick}>
            {props.children}
          </Typography>
        ) : props.type === 'Heading-L' ? (
          <Typography variant="h2" className={classnames('heading ttu', props.className)} onClick={onClick}>
            {props.children}
          </Typography>
        ) : props.type === 'Heading-M' ? (
          <Typography variant="h3" className={classnames('heading ttu', props.className)} onClick={onClick}>
            {props.children}
          </Typography>
        ) : props.type === 'Heading-S' ? (
          <Typography variant="h4" className={classnames('heading ttu', props.className)} onClick={onClick}>
            {props.children}
          </Typography>
        ) : props.type === 'Heading-XS' ? (
          <Typography variant="h5" className={classnames('heading ttu', props.className)} onClick={onClick}>
            {props.children}
          </Typography>
        ) : props.type === 'Heading-XXS' ? (
          <Typography variant="h6" className={classnames('heading ttu', props.className)} onClick={onClick}>
            {props.children}
          </Typography>
        ) : (null)
      }
    </ThemeProvider>
  );
}

HeaderCT.defaultProps = {
  type: 'Heading-L',
};

HeaderCT.prototype = {
  type: PropTypes.string,

};

export default HeaderCT;
