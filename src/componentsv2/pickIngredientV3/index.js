import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import CloseIcon from '@mui/icons-material/Close';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import './styles.scss';

import icBack from '../../image/icon/ic-prev-silder.svg';
import icInfo from '../../image/icon/ic-info-new.svg';
import icSearch from '../../image/icon/search-b.svg';
import { GET_INGREDIENT_ID_PRODUCTS } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import { getNameFromButtonBlock, sendSelectScentXClickHotjarEvent } from '../../Redux/Helpers';
import {
  getHeight, isBrowser, isMobile, isMobile767fn,
} from '../../DetectScreen';
import ButtonCTV2 from '../buttonCT';
import IngredientPopUpV2 from '../ingredientPopUpV2';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import ButtonCT from '../../components/ButtonCT';
import FilterSearchScent from '../../components/ProductAllV2/ProductBlock/filterSearchScent';
import ScentLine from '../../components/ProductAllV2/ProductBlock/scentLine';
import NotifiMe from '../../components/ProductAllV2/ProductBlock/notifiMe';
import ScentCharacterV2 from '../scentCharacterV2';

class PickIngredientV3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scentSelected: !props.isPickIngredientDetailV2 ? props.dataSelection : {},
      valueFilter: props.isShowHome ? 'Scent Family' : (props.isDualCandles ? 'Popularity' : 'Scent Family'),
      valueSearch: '',
      ingredientMini: props.isOnlyShowIngredientLocal ? {} : undefined,
      dataShowIngredient: props.isPickIngredientDetailV2 ? props.dataSelection : undefined,
      isShowNotifiMe: false,
    };
    this.refFilterSearchScent = React.createRef();
  }

  componentDidMount = () => {
    if (this.props.shareRefFilterSearchScent) {
      this.props.shareRefFilterSearchScent(this.refFilterSearchScent);
    }
    if (this.props.dataSelection?.ingredient) {
      this.onClickIngredientMini(this.props.dataSelection.ingredient);
    }
  };

  onClickShowIngredientV2 = (data) => {
    if (this.props.updateDataCurrentSelect) {
      this.props.updateDataCurrentSelect(data);
    }
  };

  onClick = (data) => {
    console.log('data ==> ', data, this.props.isShowIngredientDetail);
    const { scentSelected } = this.state;
    const { dataSelection } = this.props;
    if (data.id === scentSelected.id) {
      this.setState({ scentSelected: {} });
      this.props.onClickChangeImageCandle(true, dataSelection.index);
      this.onClickShowIngredientV2({});
    } else {
      this.setState({ scentSelected: data });
      this.props.onClickChangeImageCandle(false, dataSelection.index);
      if (this.props.isPickIngredientDetailV2) {
        this.setState({ dataShowIngredient: data });
      } else if (this.props.isShowIngredientDetail) {
        this.onClickIngredientMini(data.ingredient);
      }
      this.onClickShowIngredientV2(data);
    }
  }

  onClickCancelIngredients = () => {
    const { dataSelection } = this.props;
    this.setState({ scentSelected: {}, ingredientMini: undefined });
    this.props.onClickChangeImageCandle(true, dataSelection.index);
    if (this.props.isOnlyShowIngredientLocal) {
      this.props.onClickCloseScent();
    }
  }

  onClickApply = async () => {
    const { scentSelected } = this.state;
    const { dataSelection } = this.props;
    sendSelectScentXClickHotjarEvent(dataSelection?.index);
    this.props.onClickApply(scentSelected, dataSelection.index);
  }

  onNotifiMe = () => {
    this.setState({ isShowNotifiMe: true });
  }

  onCloseNotifiMe = () => {
    this.setState({ isShowNotifiMe: false });
  }

  onClickIngredientMini = (id) => {
    console.log('onClickIngredientMini', id);
    if (typeof id === 'object') {
      this.setState({ ingredientMini: id });
      return;
    }
    const option = {
      url: GET_INGREDIENT_ID_PRODUCTS.replace('{id}', id),
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.setState({ ingredientMini: result });
        this.props.loadingPage(false);
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      this.props.loadingPage(false);
      toastrError(err.message);
    });
  }

  onClickIngredient = (id) => {
    if (typeof id === 'object') {
      this.props.onClickIngredient(id);
      return;
    }
    const option = {
      url: GET_INGREDIENT_ID_PRODUCTS.replace('{id}', id),
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.props.onClickIngredient(result);
        this.props.loadingPage(false);
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      this.props.loadingPage(false);
      toastrError(err.message);
    });
  }

  onChangeValueFilter = (value) => {
    this.setState({ valueFilter: value });
  }

  onChangeValueSearch = (value) => {
    this.setState({ valueSearch: value });
  }

  applyFilter = (dataScents, value) => {
    const datas = _.find(dataScents, x => x.title.includes(value) || x.title.includes(value.toLowerCase()));
    if (value === 'Alphabet' || value === 'Popularity') {
      return datas ? datas.datas : [];
    }
    const datasNew = datas ? datas.datas : [];
    const scentT1 = _.filter(datasNew, x => x.group.toLowerCase() === value.toLowerCase());
    return scentT1;
  }

  applySearch = (dataScents, value) => {
    if (!value) {
      return dataScents;
    }
    const newDataScents = _.cloneDeep(dataScents);
    _.forEach(newDataScents, (d) => {
      const { datas } = d;
      d.datas = _.filter(datas, x => x.name.toLowerCase().includes(value.toLowerCase()));
    });
    return newDataScents;
  }

  renderIngredientLocalV2 = () => (
    <div className="div-ingredient-local">
      <IngredientPopUpV2
        onCancel={() => {
          this.setState({ dataShowIngredient: undefined });
          this.onClick(this.state.scentSelected);
        }}
        onClickApply={this.onClickApply}
        data={this.state.dataShowIngredient}
        buttonBlocks={this.props.buttonBlocks}
      />
    </div>
  )

  renderIngredientLocal = () => (
    <div className="div-ingredient-local">
      <div className="div-content-local">
        <IngredientPopUp
          isShow
          ingredientMinimum
          data={this.state.ingredientMini}
          history={this.props.history}
          onClickCancelIngredientsMini={this.onClickCancelIngredients}
          onClickApplyMini={this.onClickApply}
        />
      </div>
      <div className="div-list-button">
        <ButtonCT
          name={getNameFromButtonBlock(this.props.buttonBlocks, 'APPLY')}
          className="button-black"
          onClick={this.onClickApply}
        />
      </div>
    </div>
  )

  onClickBack = () => {
    const { scentSelected } = this.state;
    if (scentSelected?.id) {
      this.setState({ scentSelected: {} });
      return;
    }
    this.props.onClickCloseScent();
  }

  render() {
    const {
      onClickCloseScent, dataSelection, dataScents, buttonBlocks,
      isDualCandles, scentRecommend, isPerfumeProduct,
      isShowIngredientDetail, isPickIngredientDetailV2, recommendDataOfScent,
    } = this.props;
    const {
      scentSelected, valueFilter, valueSearch, ingredientMini, dataShowIngredient,
    } = this.state;
    const newScentFilter = this.applyFilter(dataScents, valueFilter);
    const newScent = this.applySearch(newScentFilter, valueSearch);

    const pickBt = getNameFromButtonBlock(buttonBlocks, 'Pick_a_Ingredient');
    const cancelBt = getNameFromButtonBlock(buttonBlocks, 'CANCEL');
    const applyBt = getNameFromButtonBlock(buttonBlocks, 'APPLY');
    const notifymeBt = getNameFromButtonBlock(buttonBlocks, 'notify me');
    const recommendBt = getNameFromButtonBlock(buttonBlocks, 'Recommended scents with');

    console.log('scentRecommend', scentRecommend);

    if (dataShowIngredient?.id && isMobile) {
      return this.renderIngredientLocalV2();
    }
    if (ingredientMini && !isPickIngredientDetailV2) {
      return this.renderIngredientLocal();
    }
    const htmlInfoScent = () => (
      <div id="id-info-scent" className={scentSelected?.id ? 'div-info-scent animated faster fadeIn' : 'hidden'}>
        <ScentCharacterV2
          isPerfumeProduct={isPerfumeProduct}
          data={scentSelected}
          isShowVideo
          isChangeFullScreen={isMobile767fn()}
        />
      </div>
    );

    const htmlRecommendScent = () => (
      <div className={classnames('recommend-scent-mobile-v2 div-col')}>
        <h4>
          {recommendBt}
          {' '}
          {recommendDataOfScent}
        </h4>
        <div className="scent-recommend div-row">
          <ScentLine
            onClickIngredient={this.props.onClickIngredient}
            data={this.props.scentRecommend.suggestions}
            onClick={this.onClick}
            scentSelected={scentSelected}
            className={isDualCandles ? 'item-for-candle' : ''}
            buttonBlocks={buttonBlocks}
            isRecommended
            isNoInfo
          />
        </div>
      </div>
    );

    return (
      <div className={classnames('pick-product-detail-v3', this.props.className, isDualCandles ? 'dual-candles' : '')}>
        {
          !isMobile767fn() && htmlInfoScent()
        }
        <div className="list-scent" style={isMobile767fn() ? { height: `${getHeight()}px` } : {}}>
          <div className="div-header-ingredient">
            <button type="button" onClick={this.onClickBack}>
              {isMobile767fn() ? <ArrowBackIosNewIcon style={{ color: '#2c2c2c' }} /> : <CloseIcon style={{ color: '#2c2c2c' }} />}
            </button>
            <div className="div-text-header">
              <h3>
                {pickBt}
                {' '}
                {dataSelection ? dataSelection.subTitle : ''}
              </h3>
            </div>
          </div>
          {
            this.props.titleHeader && (
              <div className="title-header">
                {this.props.titleHeader}
              </div>
            )
          }
          {!_.isEmpty(scentRecommend?.suggestions) && htmlRecommendScent()}
          {
            isMobile767fn() && scentSelected?.id ? (
              htmlInfoScent()
            ) : (
              <React.Fragment>
                <div className={isDualCandles ? 'hidden' : 'div-filter-search'}>
                  <FilterSearchScent
                    isSortScentNotes
                    ref={this.refFilterSearchScent}
                    onChangeFilter={this.props.onChangeFilter}
                    onChangeSearch={this.props.onChangeSearch}
                    valueFilter={valueFilter}
                    valueSearch={valueSearch}
                    onChangeValueFilter={this.onChangeValueFilter}
                    onChangeValueSearch={this.onChangeValueSearch}
                    buttonBlocks={buttonBlocks}
                    isPickIngredientDetailV3
                    isVersionWeb
                  />
                </div>
                <div className={classnames('div-scroll-ingredient', (isDualCandles ? 'small-height' : ''), (isShowIngredientDetail ? 'ingredient-mini-mobile' : ''))}>
                  {
                    _.map(newScent, x => (
                      <ScentLine
                        onClickIngredient={this.props.onClickIngredient}
                        data={x}
                        onClick={this.onClick}
                        scentSelected={scentSelected}
                        className={isDualCandles ? 'item-for-candle' : ''}
                        buttonBlocks={buttonBlocks}
                        isNoInfo
                      />
                    ))
                  }
                </div>
              </React.Fragment>
            )
          }

          <div className="list-button div-row">
            <ButtonCTV2 name={cancelBt} variant="outlined" onClick={onClickCloseScent} />
            <ButtonCTV2 disabled={!scentSelected.id && !dataSelection.id} name={scentSelected?.is_out_of_stock ? notifymeBt : applyBt} onClick={scentSelected?.is_out_of_stock ? this.onNotifiMe : this.onClickApply} />
          </div>
          {
            this.state.isShowNotifiMe && (
              <NotifiMe buttonBlocks={this.props.buttonBlocks} onClose={this.onCloseNotifiMe} data={scentSelected} />
            )
          }
        </div>
      </div>
    );
  }
}

PickIngredientV3.defaultProps = {
  onClickChangeImageCandle: () => {},
  isShowIngredientDetail: false,
};

PickIngredientV3.propTypes = {
  onClickCloseScent: PropTypes.func.isRequired,
  dataScents: PropTypes.arrayOf().isRequired,
  loadingPage: PropTypes.func.isRequired,
  isDualCandles: PropTypes.bool.isRequired,
  onClickChangeImageCandle: PropTypes.func,
  isShowIngredientDetail: PropTypes.bool,
};

export default PickIngredientV3;
