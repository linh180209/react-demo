import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import _ from 'lodash';
import './styles.scss';
import { Input } from 'reactstrap';
import InputMask from 'react-input-mask';
import PhoneInput from 'react-phone-number-input';
import auth from '../../Redux/Helpers/auth';
import 'react-phone-number-input/style.css';
import AutoCompleteInputV2 from './autoCompleteInputV2';

function InputCT(props) {
  if (props.type === 'select-checkout') {
    return (
      <div className={classnames('input-ct div-col', props.style === 'dark' ? 's-dark' : 's-light', props.size, props.className)}>
        {
          props.label && (
            <div className="other-Input-Text">
              {props.label}
            </div>
          )
        }
        <Input
          type="select"
          className={classnames('input-select', props.className, props.optionShippingFormat && !props.value ? 'custom-color' : '')}
          placeholder={props.placeholder}
          name={props.name}
          onChange={props.onChange}
        >
          {props.optionShippingFormat && !props.value && (
            <option className="custom-option" disabled selected={!props.value} value="">
              {props.placeholder}
            </option>
          )}
          {
            _.map(props.options, x => (
              <option value={x.country.code} selected={(props.value ? props.value.toUpperCase() : '') === x.country.code.toUpperCase()}>
                {x.country.name}
                {' '}
                {props.optionShippingFormat ? `(+ S$ ${parseInt(x.shipping)} for Shipping)` : ''}
              </option>
            ))
          }
        </Input>
        <div className="icon-drop-input-select">
          <KeyboardArrowDownIcon style={{ color: '#8D826E' }} />
        </div>
      </div>
    );
  }
  if (props.type === 'select') {
    return (
      <div className={classnames('input-ct div-col', props.style === 'dark' ? 's-dark' : 's-light', props.size, props.className)}>
        {
          props.label && (
            <div className="other-Input-Text">
              {props.label}
            </div>
          )
        }
        <Input
          type="select"
          className={classnames('input-select', props.className)}
          placeholder={props.placeholder}
          name={props.name}
          onChange={props.onChange}
        >
          {
            _.map(props.options, x => (
              <option value={x.value} selected={props.value === x.value}>
                {x.label}
              </option>
            ))
          }
        </Input>
        <div className="icon-drop-input-select">
          <KeyboardArrowDownIcon style={{ color: '#8D826E' }} />
        </div>
      </div>
    );
  }
  if (props.type === 'autocomplete') {
    return (
      <div className={classnames('input-ct div-col', props.style === 'dark' ? 's-dark' : 's-light', props.size, props.className)}>
        {
          props.label && (
            <div className="other-Input-Text">
              {props.label}
            </div>
          )
        }
        <AutoCompleteInputV2
          ref={props.ref}
          isMedium={props.isMedium}
          className={classnames('auto-complete-v2', props.className)}
          isSearchCity={props.isSearchCity}
          name={props.name}
          id={props.id}
          // onKeyDown={() => {}}
          placeholder={props.placeholder}
          country={props.country || 'sg'}
          value={props.value}
          onChangeInput={props.onChangeInput}
          onChange={props.onChange}
          eventClear={props.eventClear}
          isError={props.isError}
          errorMessage={props.errorMessage}
        />
      </div>
    );
  }
  if (props.type === 'dob') {
    return (
      <div className={classnames('input-ct div-col w-100', props.style === 'dark' ? 's-dark' : 's-light', props.size, props.className)}>
        {
          props.label && (
            <div className="other-Input-Text">
              {props.label}
            </div>
          )
        }
        <div className="content content-icon">
          {props.iconLeft}
          <InputMask
            className={classnames('input-dob', props.className)}
            type="text"
            name="dob"
            placeholder={props.placeholder}
            mask="99/99/9999"
            maskChar={null}
            defaultValue={props.defaultValue}
            value={props.value}
            onChange={props.onChange}
          />
        </div>

      </div>
    );
  }
  if (props.type === 'phone') {
    return (
      <div className={classnames('input-ct div-col', props.style === 'dark' ? 's-dark' : 's-light', props.size, props.className)}>
        {
          props.label && (
            <div className="other-Input-Text">
              {props.label}
            </div>
          )
        }
        <PhoneInput
          international
          defaultCountry={_.upperCase(auth.getCountry())}
          countryCallingCodeEditable={false}
          countrySelectProps={{ unicodeFlags: true }}
          className={classnames('input-phone', props.className)}
          placeholder={props.placeholder}
          value={_.isEmpty(props.value) ? props.value : undefined}
          name={props.name}
          onChange={props.onChange}
        />
      </div>
    );
  }
  return (
    <div className={classnames('input-ct div-col', props.style === 'dark' ? 's-dark' : 's-light', props.size, props.className)}>
      {
        props.buttonRight && (
          <button type="button" className="bt-right button-bg__none" onClick={props.onClickButtonRight}>
            {props.buttonRight}
          </button>
        )
      }
      {
        props.label && (
          <div className="other-Input-Text">
            {props.label}
          </div>
        )
      }
      {
        props.iconLeft ? (
          <div className="content content-icon">
            {props.iconLeft}
            <input
              type={props.type}
              value={props.value}
              placeholder={props.placeholder}
              onChange={props.onChange}
              name={props.name}
            />
          </div>
        ) : (
          <div className="content">
            <input
              type={props.type}
              value={props.value}
              placeholder={props.placeholder}
              onChange={props.onChange}
              name={props.name}
              onFocus={props.onFocus}
            />
          </div>
        )
      }
      <div className="div-row">
        {
          props.messageError && (
            <div className="message-error">{props.messageError}</div>
          )
        }
      </div>


    </div>
  );
}

export default InputCT;
