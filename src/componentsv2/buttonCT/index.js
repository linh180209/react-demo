/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import Button from '@mui/material/Button';
import PropTypes from 'prop-types';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import classnames from 'classnames';
import React from 'react';
import './styles.scss';

const themeBase = createTheme({
  palette: {
    primary: {
      main: '#2C2C22',
    },
    secondary: {
      main: '#EBD8B8',
      light: '#FFF4E0',
      dark: '#414138',
    },
  },
});

function ButtonCT(props) {
  const theme = createTheme(themeBase, {
    shape: {
      borderRadius: props.size === 'small' ? 21 : 30,
    },
    typography: {
      button: {
        fontFamily: 'unset',
        fontSize: props.size === 'small' ? 11.75 : props.size === 'medium' ? 14 : 16,
      },
    },
  });

  return (
    <ThemeProvider theme={theme}>
      {
        props.onlyIcon ? (
          <Button
            type={props.type}
            onClick={props.onClick}
            variant={props.variant}
            color={props.color}
            disabled={props.disabled}
            onMouseUp={props.onMouseUp}
            className={classnames('custom-button', props.variant, props.color, props.size, 'icon-button', props.disabled ? 'disabled' : '', props.className)}
          >
            {props.onlyIcon}
          </Button>
        ) : (
          <Button
            onClick={props.onClick}
            variant={props.variant}
            color={props.color}
            disabled={props.disabled}
            startIcon={props.startIcon}
            endIcon={props.endIcon}
            onMouseEnter={props.onMouseEnter}
            onMouseUp={props.onMouseUp}
            className={classnames('custom-button', props.variant, props.color, props.size, props.disabled ? 'disabled' : '', props.className)}
          >
            {props.name}
          </Button>
        )
      }

    </ThemeProvider>
  );
}

ButtonCT.defaultProps = {
  onClick: () => {},
  variant: 'contained',
  size: 'large',
  color: 'primary',
  className: '',
  disabled: false,

};

ButtonCT.propTypes = {
  onClick: PropTypes.func,
  variant: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.string,
  color: PropTypes.string,
  disabled: PropTypes.bool,
};

export default ButtonCT;
