import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import React from 'react';
import _ from 'lodash';
import reactHtmlPare from 'react-html-parser';
import VideoAcademy from '../../components/AcademyItem/videoAcademy';
import { logoGoldText } from '../../imagev2/svg';
import { getNameFromButtonBlock } from '../../Redux/Helpers';
import ButtonCT from '../buttonCT';
import './styles.scss';

function LandingQuizV2(props) {
  const takeQuiz = getNameFromButtonBlock(props.buttonBlocks, 'take quiz');
  const beBt = getNameFromButtonBlock(props.buttonBlocks, 'BE');
  const FreeBt = getNameFromButtonBlock(props.buttonBlocks, 'Free');
  const UniqueBt = getNameFromButtonBlock(props.buttonBlocks, 'Unique');
  const yourScentBt = getNameFromButtonBlock(props.buttonBlocks, 'your scent');
  const titleHeader = _.find(props.headerAndParagraphBocks, x => x.value?.header?.header_text === 'title-landing');

  return (
    <div className="landing-quiz-v2">
      <div className="bg-video">
        <VideoAcademy
          url={props.videoBlocks?.length ? props.videoBlocks[0].value?.video : ''}
          poster={props.videoBlocks?.length ? props.videoBlocks[0].value?.placeholder : ''}
          autoPlay
        />
      </div>
      <div className="bg-gray" />
      <div className="navigation-bar">
        <img src={logoGoldText} alt="logo" />
      </div>
      <div className="content-info">
        <div className="text-title-1">
          <div className="text-be">{beBt}</div>
          <div className="tag-rotate">
            <div className="tag-rotate-contain">
              <div className="tagline">
                {FreeBt}
              </div>
              <div className="tagline">
                {UniqueBt}
              </div>
              <div className="tagline">
                {yourScentBt}
              </div>
            </div>
          </div>
        </div>
        <h1 className="header-title">
          {
            reactHtmlPare(titleHeader?.value?.paragraph)
          }
        </h1>
        <ButtonCT
          name={takeQuiz}
          endIcon={<ArrowForwardIcon />}
          variant="outlined"
          color="secondary"
          onClick={props.onClick}
        />
      </div>
    </div>
  );
}

export default LandingQuizV2;
