import React from 'react';
import classnames from 'classnames';
import './styles.scss';

function ChartColumn(props) {
  return (
    <div className="chart-column">
      <div className="div-columns">
        <div className={classnames('col-1', props.percent > 0 ? 'active' : '')} />
        <div className={classnames('col-2', props.percent > 40 ? 'active' : '')} />
        <div className={classnames('col-3', props.percent > 80 ? 'active' : '')} />
      </div>
      <div className="name">
        {props.title}
      </div>
    </div>
  );
}

export default ChartColumn;
