import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import TabsCT from './tabsCT';
import './styles.scss';

function BlockFAQ(props) {
  return (
    <div className={classnames('block-faq-new', props.className)}>
      <div id="id-faq" />
      {
        props.title && (
          <div className="header-text">
            {props.title}
          </div>
        )
      }
      {
        props.description && (
          <div className="des-text">
            {props.description}
          </div>
        )
      }
      <TabsCT faqsBlock={props.faqsBlock} />
    </div>
  );
}

export default BlockFAQ;
