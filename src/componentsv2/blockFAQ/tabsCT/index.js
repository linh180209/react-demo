import Tab from '@mui/material/Tab';
import classnames from 'classnames';
import _ from 'lodash';
import React, { useState } from 'react';
import reactHtmlPare from 'react-html-parser';

import { isMobile } from '../../../DetectScreen';
import AccordionCT from '../../accordionCT';
import './styles.scss';

function TabPanel(props) {
  return (
    <div className={classnames('tab-panel', props.value !== props.index ? 'hidden' : '')}>
      {props.children}
    </div>
  );
}

function TabsCT(props) {
  const [value, setValue] = useState(0);

  const summary = data => (
    <span className={classnames('other-Tagline', isMobile ? 's-size' : '')}>
      {data}
    </span>
  );

  const details = data => (
    <span className={classnames('body-text-s-regular', isMobile ? '' : 'm-size')}>
      {reactHtmlPare(data)}
    </span>
  );

  return (
    <div className={classnames('tabs-ct', 'tabs-ct-mobile')}>
      <React.Fragment>
        {
          _.map(props.faqsBlock || [], (d, index) => (
            <AccordionCT
              // onChange={(k, e) => { handleExtendMobile(e, index); }}
              className="faq-mobile-item"
              summary={<Tab className="div-tab" label={d?.value?.header?.header_text} />}
              // styleIcon={}
              details={(
                <TabPanel value={value} index={0}>
                  {
                    _.map(d.value?.faq, x => (
                      <AccordionCT className="faq-mobile-item-extend" summary={summary(x?.value?.header?.header_text)} details={details(x?.value?.paragraph)} />
                    ))
                  }
                </TabPanel>
              )}
            />
          ))
        }
      </React.Fragment>
    </div>

  );
}

export default TabsCT;
