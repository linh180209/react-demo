import React, { useState, useEffect } from 'react';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import { Global } from '@emotion/react';

const StyledBox = styled(Box)(({ theme }) => ({
  backgroundColor: '#F1E4CD',
}));

function SwipeableDrawerMobile(props) {
  return (
    <div className="div-swipeableDrawerMobile">
      <Global
        styles={{
          '.MuiDrawer-root > .MuiPaper-root': {
            height: props.isFullHeight ? '100vh' : `${props.heightCard}px`,
            overflow: 'visible',
            background: '#F1E4CD',
          },
        }}
      />
      <SwipeableDrawer
        anchor="bottom"
        open={props.open}
        onClose={() => props.toggleDrawer(false)}
        onOpen={() => props.toggleDrawer(true)}
        swipeAreaWidth={props.open ? 0 : props.drawerBleeding}
        disableSwipeToOpen={false}
        ModalProps={{
          keepMounted: true,
        }}
      >

        <StyledBox
          sx={{
            position: 'absolute',
            top: -props.drawerBleeding,
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            visibility: 'visible',
            right: 0,
            left: 0,
            boxShadow: props.open ? 'none' : '0px -5px 10px rgba(141, 130, 110, 0.5)',
          }}
        >
          {props.miniHtml}
          {props.fullHtml}
        </StyledBox>
        {/* <StyledBox
          sx={{
            px: 2,
            pb: 2,
            height: '100%',
            overflow: 'auto',
          }}
        >
          {props.fullHtml}
        </StyledBox> */}
      </SwipeableDrawer>
    </div>
  );
}

export default SwipeableDrawerMobile;
