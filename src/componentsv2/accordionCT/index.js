import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import _ from 'lodash';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import './styles.scss';

function AccordionCT(props) {
  return (
    <Accordion expanded={props.expanded} className={classnames('accordion-ct', props.mode, props.className)} onChange={props.onChange}>
      <AccordionSummary
        className="accordion-summary-ct"
        expandIcon={<ExpandMoreIcon style={_.assign({ color: '#2C2C22' }, props.styleIcon)} />}
      >
        {props.summary}
      </AccordionSummary>
      <AccordionDetails className="accordion-detail-ct">
        {props.details}
      </AccordionDetails>
    </Accordion>
  );
}

AccordionCT.defaultProps = {
  mode: 'light',
  className: '',
  styleIcon: {},
  onChange: () => {},
};

AccordionCT.propTypes = {
  mode: PropTypes.string,
  className: PropTypes.string,
  styleIcon: PropTypes.shape(PropTypes.object),
  onChange: PropTypes.func,
};

export default AccordionCT;
