import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import HeaderCT from '../headerCT';
import ButtonCT from '../buttonCT';
import './styles.scss';
import { isBrowser, isMobile } from '../../DetectScreen';

function ItemProduct(props) {
  return (
    <div className={classnames('item-product div-row', props.className)}>
      <div className="div-image">
        <img src={props.image} alt="product" />
      </div>
      <div className="div-info">
        <div className="div-left div-col">
          {/* <span className="sp-title">
            {props.title}
          </span> */}
          <HeaderCT type={isBrowser ? 'Heading-S' : 'Heading-XXS'} className="sp-text">
            {props.name}
          </HeaderCT>
          <div className={classnames('body-text-s-regular sub-title', isMobile ? 's-size' : '')}>
            {props.subName}
          </div>
          {
            props.price && (
              isBrowser ? (
                <HeaderCT type="Heading-S" className="sp-price">
                  {props.price}
                </HeaderCT>
              ) : (
                <div className={classnames('body-text-s-regular sub-title s-size text-price')}>
                  {props.price}
                </div>
              )
            )
          }
        </div>
        {
          props.buttonName && (
            <ButtonCT
              onClick={props.onClick}
              variant="contained"
              size="small"
              name={props.buttonName}
            />
          )
        }
      </div>
    </div>
  );
}

export default ItemProduct;
