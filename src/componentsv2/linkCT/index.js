import React from 'react';
import { Link } from 'react-router-dom';

function LinkCT(props) {
  if (props.to.includes('https://')) {
    return (
      <a href={props.to} className={props.className} onMouseEnter={props.onMouseEnter} {...props}>
        {props.children}
      </a>
    );
  }
  return (
    <Link to={props.to} className={props.className} onMouseEnter={props.onMouseEnter} {...props}>
      {props.children}
    </Link>
  );
}

export default LinkCT;
