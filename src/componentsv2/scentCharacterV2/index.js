import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect, useState, memo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ReactHtmlParser from 'react-html-parser';
import TimerOutlinedIcon from '@mui/icons-material/TimerOutlined';
import BarChartOutlinedIcon from '@mui/icons-material/BarChartOutlined';
import BubbleChartOutlinedIcon from '@mui/icons-material/BubbleChartOutlined';
import VideoAcademy from '../../components/AcademyItem/videoAcademy';
import addCmsRedux from '../../Redux/Actions/cms';
import updateIngredientsData from '../../Redux/Actions/ingredients';
import { fetchCMSHomepage, getNameFromButtonBlock } from '../../Redux/Helpers';
import { toastrError } from '../../Redux/Helpers/notification';

import './styles.scss';
import ProgressCircleV3 from '../../views/ResultScentV2/progressCircleV3';
import ColumnChart from '../columnChart';
import LineChart from '../lineChart';
import { isMobile767fn } from '../../DetectScreen';
import { fetchScentLib } from '../../Redux/Helpers/fetchAPI';

const getCMS = (data) => {
  if (data) {
    const { body } = data;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    return {
      buttonBlocks,
    };
  }
  return {
    buttonBlocks: [],
  };
};

function ScentCharacterV2(props) {
  const dispatch = useDispatch();
  const cms = useSelector(state => state.cms);
  const ingredients = useSelector(state => state.ingredients);
  const [dataIngredient, setDataIngredient] = useState();
  const [comboData, setComboData] = useState();
  const [buttonBlocks, setButtonBlocks] = useState([]);
  const fetchDataInit = () => fetchCMSHomepage('ingredient-detail');

  const fetchData = async () => {
    const ingredientBlock = _.find(cms, x => x.title === 'Ingredient Detail');
    if (!ingredients || ingredients.length === 0 || !ingredientBlock) {
      const pending = [fetchDataInit(), fetchScentLib()];
      try {
        const results = await Promise.all(pending);
        dispatch(addCmsRedux(results[0]));
        dispatch(updateIngredientsData(results[1]));
        const cmsState = getCMS(results[0]);
        setButtonBlocks(cmsState?.buttonBlocks);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const cmsState = getCMS(ingredientBlock);
      setButtonBlocks(cmsState?.buttonBlocks);
    }
  };

  const getImageMain = images => _.find(images, x => x.type === 'main')?.image;

  const onClickScent = (d) => {
    setDataIngredient(d);
    if (props.onClickScent) {
      props.onClickScent(d.id === 0 ? undefined : d);
    }
  };

  useEffect(() => {
    if (comboData?.length > 0) {
      setDataIngredient(props.pageType !== 'gift' ? comboData[0] : comboData[1]);
    }
  }, [comboData]);

  useEffect(() => {
    if (!_.isEmpty(props.data)) {
      let data;
      if (props.data?.ingredient?.id) {
        data = _.find(ingredients, x => x.ingredient?.id === props.data?.ingredient?.id);
      } else {
        data = _.find(ingredients, x => x.id === props.data.id);
      }
      setDataIngredient(data);
    }
  }, [props.data, ingredients]);

  useEffect(() => {
    console.log('props.datas', props.datas);
    if (!_.isEmpty(props.datas)) {
      const listDatas = [];
      if (props.datas.length > 1) {
        listDatas.push({
          id: 0,
          image1: _.find(props.datas[0]?.product.images || [], x => x.type === 'main')?.image,
          image2: _.find(props.datas[1]?.product.images || [], x => x.type === 'main')?.image,
          image3: _.find(props.datas[2]?.product.images || [], x => x.type === 'main')?.image,
          image4: _.find(props.datas[3]?.product.images || [], x => x.type === 'main')?.image,
          name: `${props.datas[0].name} & ${props.datas[1].name}`,
          ingredient: { title: `${props.datas[0]?.product?.family} - ${props.datas[1]?.product?.family}` },
          profile: props.profile,
        });
      }

      if (props.datas[0].product?.combo?.length > 0) {
        _.assign(props.datas[0].product, props.datas[0].product?.combo[0]?.product);
      }
      listDatas.push(props.datas[0].product);

      if (props.datas.length > 1) {
        if (props.datas[1].product?.combo?.length > 0) {
          _.assign(props.datas[1].product, props.datas[1].product?.combo[0]?.product);
        }
        listDatas.push(props.datas[1].product);
      }
      if (props.pageType === 'gift') {
        if (props.datas[2]?.product?.combo?.length > 0) {
          _.assign(props.datas[2].product, props.datas[2].product?.combo[0]?.product);
        }
        if (props.datas[3]?.product?.combo?.length > 0) {
          _.assign(props.datas[3].product, props.datas[3].product?.combo[0]?.product);
        }
      }
      if (props.isMiniCandle || props.datas.length > 1) {
        if (props.datas.length > 2) {
          listDatas.push(props.datas[2].product);
        }
        if (props.datas.length > 3) {
          listDatas.push(props.datas[3].product);
        }
      }
      setComboData(listDatas);
    }
  }, [props.datas, ingredients]);

  useEffect(() => {
    fetchData();
  }, []);

  const EVOLUTIONBt = getNameFromButtonBlock(buttonBlocks, 'EVOLUTION OF SCENT');
  const LastingBt = getNameFromButtonBlock(buttonBlocks, 'Lasting');
  const StrengthBt = getNameFromButtonBlock(buttonBlocks, 'Strength');
  const scentCharacterBt = getNameFromButtonBlock(buttonBlocks, 'Scent Characteristics');
  const comboTitle = getNameFromButtonBlock(buttonBlocks, 'combo_title');
  const visualisationOfScent = getNameFromButtonBlock(buttonBlocks, 'visualisation of scent');
  const scentDescription = getNameFromButtonBlock(buttonBlocks, 'scent description');
  const SubtleBt = getNameFromButtonBlock(buttonBlocks, 'Subtle');
  const BalancedBt = getNameFromButtonBlock(buttonBlocks, 'Balanced');
  const StrongBt = getNameFromButtonBlock(buttonBlocks, 'Strong');
  const {
    name, videos, ingredient, profile, images, description, short_description: shortDescription,
  } = dataIngredient || {};

  const descriptionText = props.isPerfumeProduct ? shortDescription : description;

  return (
    <div className={classnames('tab-scent-character', props.className)}>
      {!props.isScentDetail && (
        <div className={classnames('block-text-info', dataIngredient?.id !== 0 ? 'has-bg-image' : '')}>
          {
            dataIngredient?.id !== 0 && (
              <React.Fragment>
                <img className="bg-image" src={getImageMain(images)} alt="bg" />
                <div className="bg-opacity" />
              </React.Fragment>

            )
          }
          <div className="text-info-content">
            <div className="hear-text tc">
              {props.title || ingredient?.title}
            </div>
            <div className="sub-text tc">
              {props.name || name}
            </div>

            {
              props.description && (
                <div className="des-text tc">
                  {props.description || name}
                </div>
              )
            }
            {
              ingredient?.scents?.length > 0 && (
                <div className="list-char div-row">
                  {
                    _.map(ingredient?.scents, x => (
                      <div>
                        {x}
                      </div>
                    ))
                  }
                </div>
              )
            }
          </div>
        </div>
      )}
      {
        comboData?.length > 1 && (
          <div className={classnames('list-button-scent', comboData?.length === 4 ? 'list-3' : comboData?.length === 5 ? 'list-4' : '')}>
            <div className={`list-button-scent-q ${props?.pageType === 'gift' ? 'list-button-not-combo' : ''}`}>
              {
                props?.pageType !== 'gift' && _.map(_.filter(comboData, d => d?.id === 0), d => (
                  <button
                    className={classnames('button-bg__none button-scent scent-2 item-full', dataIngredient?.id === d.id ? 'active' : '')}
                    type="button"
                    onClick={() => onClickScent(d)}
                  >
                    <img src={d?.image1} alt="icon" />
                    <img src={d?.image2} alt="icon" />
                    {d?.image3 && <img src={d?.image3} alt="icon" />}
                    {d?.image4 && <img src={d?.image4} alt="icon" />}
                    <div className="text-name">
                      {comboTitle}
                    </div>
                  </button>
                ))
              }
              {
                _.map(_.filter(comboData, d => d?.id !== 0), (d, index) => (
                  <button
                    className={classnames(`button-bg__none button-scent button-center ${`item-ele-${index}`}`, dataIngredient?.id === d.id ? 'active' : '')}
                    type="button"
                    onClick={() => onClickScent(d)}
                  >
                    <img src={_.find(d.images, x => x.type === 'main')?.image} alt="icon" />
                    <div className="text-name">
                      {d.name}
                    </div>
                  </button>
                ))
              }
            </div>
          </div>
      )}
      <div className="info-scent">
        <div className="info-scent-content">
          {
            dataIngredient?.id !== 0 && (
              <React.Fragment>
                <div className="block-evolution div-col">
                  <div className="title-header tc">
                    {EVOLUTIONBt}
                  </div>
                  <div className="div-image">
                    <img loading="lazy" src={ingredient?.scent_evolution} alt="evolution" />
                    {
                      _.map(ingredient?.accords, x => (
                        <span>{x}</span>
                      ))
                    }
                  </div>
                </div>
                <hr />
              </React.Fragment>
            )
          }
          <div className="div-chart">
            <div className="div-chart-scent">
              <div className="chart-lasting">
                <div className="div-title div-row">
                  <div className="div-icon">
                    <TimerOutlinedIcon />
                  </div>
                  {LastingBt}
                </div>
                <div className="chart-content">
                  <ProgressCircleV3 percent={profile?.duration * 100} trail="#56564E" />
                </div>
              </div>

              <div className="chart-strength">
                <div className="div-title div-row">
                  <div className="div-icon">
                    <BubbleChartOutlinedIcon />
                  </div>
                  {StrengthBt}
                </div>
                <div className="chart-column-product">
                  <ColumnChart
                    title={SubtleBt}
                    color={profile?.strength >= 0.5 ? '#56564E' : '#D4C2A6'}
                    weight={50}
                  />
                  <ColumnChart
                    title={BalancedBt}
                    color={profile?.strength >= 0.7 ? '#56564E' : '#D4C2A6'}
                    weight={75}
                  />
                  <ColumnChart
                    title={StrongBt}
                    color={profile?.strength >= 0.8 ? '#56564E' : '#D4C2A6'}
                    weight={100}
                  />
                </div>
              </div>
            </div>
            <div className="line-row" />
            <div className="chart-scent">
              <div className="div-title div-row">
                <div className="div-icon">
                  <BarChartOutlinedIcon />
                </div>
                {scentCharacterBt}
              </div>
              {
                _.map(profile?.accords || [], d => (
                  <LineChart title={d?.name} color={d.color} value={d.weight} />
                ))
              }
            </div>
            {
              descriptionText && !props.isScentDetail && (
                <div className="div-des-text">
                  <div className="title-des">{scentDescription}</div>
                  <div className="des-text">
                    {ReactHtmlParser(descriptionText)}
                  </div>
                </div>
              )
            }

          </div>
        </div>
      </div>
      {
        props.isShowVideo && videos?.length > 0 && (
          <div className="div-video">
            <div className="title-header tc">
              {visualisationOfScent}
            </div>
            <VideoAcademy
              isChangeFullScreen={props.isChangeFullScreen}
              url={videos ? videos[0]?.video : undefined}
              poster={videos ? videos[0]?.placeholder : undefined}
              isControl
            />
          </div>
        )
      }

    </div>
  );
}

export default memo(ScentCharacterV2);
