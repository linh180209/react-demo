import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import { isBrowser, isMobile } from '../../DetectScreen';
import { getAltImageV2 } from '../../Redux/Helpers';

function ItemBlockUSP(props) {
  return (
    <div className="item-block-usp">
      <img src={props.data?.value?.image?.image} alt={getAltImageV2(props.data?.value?.image)} />
      <div className={classnames('other-Tagline tc', isMobile ? 's-size' : '')}>
        {props.data?.value?.header?.header_text}
      </div>
      {
        isBrowser && (
          <div className="des tc">
            {props.data?.value?.text}
          </div>
        )
      }
    </div>
  );
}

export default ItemBlockUSP;
