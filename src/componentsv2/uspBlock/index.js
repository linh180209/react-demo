import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import ItemBlockUSP from './itemBlock';
import HeaderCT from '../headerCT';
import './styles.scss';
import BlockVideoIngredient from '../../views/ProductB2C/BlockVideoIngredient';

function USPBlock(props) {
  return (
    <div className="block-usp-wrap">
      {
        props.title && (
          <HeaderCT className="header-text" type="Heading-XL">
            {props.title}
          </HeaderCT>
        )
      }
      <div className="div-block-usp">
        {
          _.map(props.data || [], d => (
            <ItemBlockUSP data={d} />
          ))
        }
      </div>
    </div>
  );
}

export default React.memo(USPBlock);
