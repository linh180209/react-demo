import React, { useState, useEffect, useRef } from 'react';
import _ from 'lodash';
import Dropzone from 'react-dropzone';
import loadImage from 'blueimp-load-image';
import UploadOutlinedIcon from '@mui/icons-material/UploadOutlined';
import ModeEditOutlineOutlinedIcon from '@mui/icons-material/ModeEditOutlineOutlined';
import CheckIcon from '@mui/icons-material/Check';
import classnames from 'classnames';
import Slider from '@mui/material/Slider';
import RemoveIcon from '@mui/icons-material/Remove';
import AddIcon from '@mui/icons-material/Add';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import Tooltip from '@mui/material/Tooltip';

import { useMergeState } from '../../../Utils/customHooks';
import BottleCustom from '../../../components/B2B/CardItem/bottleCustom';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import { bgProductV2 } from '../../../imagev2/png';
import { COLOR_CUSTOME, FONT_CUSTOME } from '../../../constants';
import { isBrowser, isMobile } from '../../../DetectScreen';
import InputCT from '../../inputCT';
import ButtonCT from '../../buttonCT';

function MainEditPage(props) {
  const [listThumb, setListThumb] = useState([]);
  const [state, setState] = useMergeState({
    scale: 1.2,
  });
  const [categorySelect, setCategorySelect] = useState();
  const editor = useRef();

  const onClickBottle = () => {
    // if (isMobile) {
    //   props.onScreenUploadImage(true);
    //   props.onChangeGuiUpload(true);
    // }
  };

  const handleChangeScale = (event, newValue) => {
    setState({ scale: newValue });
  };

  const onChangeScale = (newValue) => {
    const temp = newValue < 1 ? 1 : newValue > 3 ? 3 : newValue;
    setState({ scale: temp });
  };

  const onChangeText = (e) => {
    const { value } = e.target;
    if (value.length > 15) {
      return;
    }
    _.assign(props.customeBottle, { name: value });
    props.onChangeCustomeBottle(props.customeBottle);
  };

  const onSelectFile = (files) => {
    if (files && files.length > 0) {
      loadImage(
        files[0],
        (img) => {
          const base64data = img.toDataURL('image/png');
          props.onSetImageUpload(base64data);
          if (isMobile) {
            props.onChangeGuiEdit(true);
          }
          const { customeBottle } = props;
          customeBottle.image = '';
          customeBottle.imagePremade = undefined;
          props.onChangeCustomeBottle(customeBottle);
        },
        { orientation: 1 },
      );
    }
  };

  const onChangeFont = (font) => {
    _.assign(props.customeBottle, { font });
    props.onChangeCustomeBottle(props.customeBottle);
  };

  const onChangeColor = (color) => {
    _.assign(props.customeBottle, { color });
    props.onChangeCustomeBottle(props.customeBottle);
  };

  const onClearBottom = () => {
    const { customeBottle } = props;
    customeBottle.font = FONT_CUSTOME.JOST;
    customeBottle.color = COLOR_CUSTOME.BLACK;
    customeBottle.name = '';
    customeBottle.image = '';
    customeBottle.imagePremade = undefined;
    props.onChangeCustomeBottle(customeBottle);
  };

  const onSave = () => {
    const { customeBottle } = props;
    props.onSave(customeBottle);
    if (props.idCartItem) {
      setTimeout(() => {
        props.onClickUpdatetoCart();
        if (props.propsState) {
          props.propsState.custome.name = customeBottle.name;
          props.propsState.custome.image = customeBottle.image;
          props.propsState.custome.color = customeBottle.color;
          props.propsState.custome.font = customeBottle.font;
          props.propsState.custome.imagePremade = customeBottle.imagePremade;
        }
      }, 500);
    }
    props.closePopUp();
  };

  const onClickChooseImage = (image) => {
    const { customeBottle, arrayThumbs } = props;
    customeBottle.image = image;
    const ele = _.find(arrayThumbs, x => x.image === image);
    if (ele) {
      customeBottle.imagePremade = ele.id;
    }
    props.onChangeCustomeBottle(customeBottle);
    props.onCloseUploadImage();
  };

  const setEditorRef = (e) => {
    editor.current = e;
  };

  const onSaveImageCustom = () => {
    const { customeBottle, uploadImage } = props;
    if (editor.current && uploadImage) {
      const canvas = editor.current.getImage().toDataURL();
      fetch(canvas)
        .then(res => res.blob())
        .then((blob) => {
          customeBottle.imagePremade = '';
          customeBottle.image = window.URL.createObjectURL(blob);
          props.onChangeCustomeBottle(customeBottle);
          props.onCloseUploadImage();
          onSave();
        });
    } else {
      props.onChangeCustomeBottle(customeBottle);
      props.onCloseUploadImage();
      onSave();
    }
  };


  useEffect(() => {
    if (props.arrayThumbs?.length > 0) {
      const lists = [];
      _.forEach(props.arrayThumbs, (d) => {
        const ele = _.find(lists, x => x.category === d.category);
        if (ele) {
          ele.data.push(d);
        } else {
          lists.push({
            category: d.category,
            data: [d],
          });
        }
      });
      setListThumb(lists);
    }
  }, [props.arrayThumbs]);


  const listFont = [FONT_CUSTOME.JOST, FONT_CUSTOME.PLAYFAIR_DISPLAY, FONT_CUSTOME.DANCING_SCRIPT, FONT_CUSTOME.BALSAMIQ_SANS, FONT_CUSTOME.PERMANENT_MARKER, FONT_CUSTOME.NUNITO];


  const eauBt = getNameFromButtonBlock(props.buttonBlocks, 'Eau_de_parfum');
  const mlBt = getNameFromButtonBlock(props.buttonBlocks, '25ml');
  const textBt = getNameFromButtonBlock(props.buttonBlocks, 'TEXT');
  const myPerfumeBt = getNameFromButtonBlock(props.buttonBlocks, 'My_perfume');
  const colorBt = getNameFromButtonBlock(props.buttonBlocks, 'color');
  const fontBt = getNameFromButtonBlock(props.buttonBlocks, 'font');
  const abcBt = getNameFromButtonBlock(props.buttonBlocks, 'ABC');
  const dropBt = getNameFromButtonBlock(props.buttonBlocks, 'Drag_and_drop_or_browse_your_files');
  const clickBt = getNameFromButtonBlock(props.buttonBlocks, 'Click and drag on your image to adjust the position');
  const scaleSizeBt = getNameFromButtonBlock(props.buttonBlocks, 'Scale The Size of Your Image');
  const saveImageBt = getNameFromButtonBlock(props.buttonBlocks, 'Save Image');
  const uploadImageBt = getNameFromButtonBlock(props.buttonBlocks, 'Upload Your Image');
  const formatBt = getNameFromButtonBlock(props.buttonBlocks, 'Format, Size, Max Size');
  const orPickBt = getNameFromButtonBlock(props.buttonBlocks, 'Or pick one of our popular design');
  const ResetBt = getNameFromButtonBlock(props.buttonBlocks, 'Reset');
  const SaveBt = getNameFromButtonBlock(props.buttonBlocks, 'Save engraving');
  const pickBt = getNameFromButtonBlock(props.buttonBlocks, 'pick font style');
  const typeBt = getNameFromButtonBlock(props.buttonBlocks, 'Type your engraving');
  const selectedBt = getNameFromButtonBlock(props.buttonBlocks, 'selected');
  const chooseBt = getNameFromButtonBlock(props.buttonBlocks, 'Choose your design');
  const persionalBt = getNameFromButtonBlock(props.buttonBlocks, 'Personalise your bottle');
  const chooseTextBt = getNameFromButtonBlock(props.buttonBlocks, 'Choose your text');
  const engraveBt = getNameFromButtonBlock(props.buttonBlocks, 'Engrave your bottle');

  const textHtml = (
    <div className="div-text div-col">
      <span className={classnames('title-text other-Tagline', isMobile ? 's-size' : '')}>
        {/* {textBt}
         */}
        {typeBt}
      </span>
      <div className="div-input">
        <InputCT
          onChange={onChangeText}
          type="text"
          placeholder={myPerfumeBt}
          value={props.customeBottle.name}
          size={isMobile ? 'small' : ''}
        />
        <ModeEditOutlineOutlinedIcon style={{ color: '#8D826E' }} />
        <span className="max-text">
          {`${props.customeBottle.name ? props.customeBottle.name.length : '0'}/15`}
        </span>
      </div>
    </div>
  );

  const colorHtml = (
    <div className="div-color div-col">
      <span className={classnames('title-text other-Tagline', isMobile ? 's-size' : '')}>
        {colorBt}
      </span>
      <div className="div-list-color div-row">
        <div className={`outline-bt ${props.customeBottle?.color === COLOR_CUSTOME.BLACK ? 'selected' : ''}`}>
          <button className="bt-color black" type="button" onClick={() => onChangeColor(COLOR_CUSTOME.BLACK)}>
            <CheckIcon />
          </button>
        </div>
        <div className={`outline-bt ${props.customeBottle?.color === COLOR_CUSTOME.WHITE ? 'selected' : ''}`}>
          <button className="bt-color white" type="button" onClick={() => onChangeColor(COLOR_CUSTOME.WHITE)}>
            <CheckIcon />
          </button>
        </div>
        {/* <div className={`outline-bt ${props.customeBottle?.color === COLOR_CUSTOME.GOLD ? 'selected' : ''}`}>
          <button className="bt-color yellow" type="button" onClick={() => onChangeColor(COLOR_CUSTOME.GOLD)}>
            <CheckIcon />
          </button>
        </div> */}
      </div>
    </div>
  );

  const fontHtml = (
    <div className="div-font">
      <div className={classnames('title-text other-Tagline', isMobile ? 's-size' : '')}>
        {pickBt}
      </div>
      <div className="list-font">
        {
          _.map(listFont, d => (
            <div>
              <button type="button" style={{ fontFamily: d }} className={d === props.customeBottle?.font ? 'active' : ''} onClick={() => onChangeFont(d)}>
                {abcBt}
              </button>
            </div>
          ))
        }
      </div>
    </div>
  );

  const titleStep = (index, text, info) => (
    <div className="div-title-step">
      <div className="number-text">
        {index}
      </div>
      <div className="text-title">
        {text}
      </div>
      <Tooltip title={info} arrow placement="right">
        <InfoOutlinedIcon />
      </Tooltip>
    </div>
  );

  return (
    <div className="main-edit-page">
      <div className="div-col">
        <div className="div-bottle">
          {/* <img src={bgProductV2} alt="bg" className="bg-image" /> */}
          <div className="div-image-bottle-v3">
            <BottleCustom
              isResponsive={isMobile}
              withCropImage={90}
              heightCropImage={290}
              // ratioHeight={isMobile ? 0.58 : undefined}
              // heightDefault={isMobile ? '58%' : undefined}
              // classWrap={isMobile ? 'wrap-custome-bottle' : ''}
              setEditorRef={setEditorRef}
              uploadImage={props.uploadImage}
              scale={state.scale}

              isCustomeV3
              onGotoProduct={onClickBottle}
              image={props.customeBottle?.image}
              color={props.customeBottle?.color}
              font={props.customeBottle?.font}
              eauBt={eauBt}
              mlBt={mlBt}
              isDisplayName
              name={props.customeBottle.name}
              combos={[]}
              name1={props.name1}
              name2={props.name2}
            />
          </div>
        </div>

        {
          props.isUploadImage && (
            <div className="div-scale">
              <div className="text-normal">
                {clickBt}
              </div>
              <div className="scale-box div-row">
                <ButtonCT className="text-index" onlyIcon={<RemoveIcon style={{ color: '#F5ECDC' }} />} size="small" onClick={() => onChangeScale(state.scale - 0.1)} />
                <Slider
                  value={state.scale}
                  min={1}
                  step={0.1}
                  max={3}
                  onChange={handleChangeScale}
                  sx={{
                    color: '#BCAD93',
                    '& .MuiSlider-thumb': {
                      width: isBrowser ? '32px' : '24px',
                      height: isBrowser ? '32px' : '24px',
                      color: '#F5ECDC',
                    },
                    '& .MuiSlider-track': {
                      color: '#F5ECDC',
                    },
                  }}
                />
                <ButtonCT className="text-index" onlyIcon={<AddIcon style={{ color: '#F5ECDC' }} />} size="small" onClick={() => onChangeScale(state.scale + 0.1)} />
              </div>
              <div className="text-normal">
                {scaleSizeBt}
              </div>
              {/* <ButtonCT
                name={saveImageBt}
                color="secondary"
                size="medium"
                onClick={onClickContinute}
              /> */}
            </div>
          )
        }
      </div>

      <div className="div-text-custome">

        <div className="bt-upload-image">
          {titleStep(1, chooseBt, persionalBt)}


          <div className="list-category">
            <div>
              <ButtonCT
                name="your image"
                size="small"
                color="secondary"
                className={_.isEmpty(categorySelect) ? 'active' : ''}
                onClick={() => setCategorySelect(undefined)}
              />
            </div>
            {
              _.map(listThumb, d => (
                <div>
                  <ButtonCT
                    name={d.category}
                    size="small"
                    color="secondary"
                    className={categorySelect?.category === d.category ? 'active' : ''}
                    onClick={() => setCategorySelect(d)}
                  />
                </div>
              ))
            }
          </div>
          {
            _.isEmpty(categorySelect) ? (
              <Dropzone onDrop={acceptedFiles => onSelectFile(acceptedFiles)} accept=".jpeg,.png,.jpg" multiple={false}>
                {({ getRootProps, getInputProps }) => (
                  <section className="div-section-more">
                    <div {...getRootProps()}>
                      <input {...getInputProps()} />
                      <UploadOutlinedIcon />
                      <span>
                        {dropBt}
                      </span>
                    </div>
                  </section>
                )}
              </Dropzone>
            ) : (
              <div className="list-image-thumbnail">
                <div className="line-image">
                  {
                    _.map(categorySelect?.data, d => (
                      <button
                        type="button"
                        className={classnames('image-thumbnail', props.customeBottle.image === d.image ? 'active' : '')}
                        onClick={() => onClickChooseImage(d.image)}
                      >
                        <img loading="lazy" src={d.image} alt="thumbnail" />
                        <div className="bg-opacity" />
                        <div className="text-selected">
                          {selectedBt}
                        </div>
                      </button>
                    ))
                  }
                </div>
              </div>
            )
          }
        </div>

        {titleStep(2, chooseTextBt, engraveBt)}
        <div className="div-text-color">
          {textHtml}
          {/* {isBrowser && colorHtml} */}
        </div>

        {fontHtml}

        {/* {!isBrowser && colorHtml} */}
        {colorHtml}
      </div>
      <div className="list-bt-save">
        <ButtonCT name={ResetBt} color="secondary" variant="outlined" onClick={onClearBottom} />
        <ButtonCT name={SaveBt} color="secondary" onClick={props.isUploadImage ? onSaveImageCustom : onSave} />
      </div>
    </div>
  );
}

export default MainEditPage;
