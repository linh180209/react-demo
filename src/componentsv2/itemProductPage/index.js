import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { bgProduct } from '../../imagev2/png';
import ButtonCT from '../buttonCT';
import HeaderCT from '../headerCT';

import './styles.scss';
import { isMobile } from '../../DetectScreen';

function ItemProductPage(props) {
  return (
    <div className={classnames('item-product-page product-base div-col', props.className)}>
      <div className="div-content-product">
        {
          (props.imageBg || (props.imageScent1 && !props.imageScent2) || (!props.imageScent1 && props.imageScent2)) ? (
            <div className="image-product" onClick={props.onClickImage}>
              <img loading="lazy" className="image-bg-full" src={props.imageBg || props.imageScent1 || props.imageScent2} alt="bg" />
              <img loading="lazy" className="product-image" src={props.image} alt="product" />
            </div>
          ) : (
            <div className="image-product" onClick={props.onClickImage}>
              <div className="image-bg div-row">
                <img loading="lazy" src={props.imageScent1} alt="bg" />
                <img loading="lazy" src={props.imageScent2} alt="bg" />
              </div>
              <img loading="lazy" className="product-image" src={props.image} alt="product" />
            </div>
          )
        }

        <div className="div-card div-col">
          <div className="div-text-body">
            {
              !props.isHiddenType && (
                <div className="div-type div-row">
                  <div className="div-image">
                    <img loading="lazy" src={props.icon} alt="icon" />
                  </div>
                  <div className="text-p">{props.type}</div>
                </div>
              )
            }
            <div className="content-text">
              <HeaderCT type={isMobile ? 'Heading-XXS' : 'Heading-S'}>
                {props.name}
              </HeaderCT>
              {
                props.scentName && (
                  <span className={classnames('body-text-s-regular', isMobile ? 's-size' : 'm-size')}>
                    {props.scentName}
                  </span>
                )
              }
            </div>
            {
              props.price && (
                <span className="other-Tagline">
                  {props.price}
                </span>
              )
            }
          </div>
          {
            props.nameInSide && (
              <ButtonCT
                name={props.nameInSide}
                endIcon={props.endIcon}
                onClick={props.onClick}
                disabled={props.isDisabledButton}
                size={props.buttonSize}
                color={props.color}
              />
            )
          }
        </div>
      </div>
    </div>
  );
}

ItemProductPage.defaultProps = {
  type: '',
  name: '',
  scentName: '',
  price: '',
  isHiddenType: false,
  endIcon: undefined,
  onClick: () => {},
  isDisabledButton: false,
  buttonSize: undefined,
  onClickImage: () => {},
};

ItemProductPage.propTypes = {
  isDisabledButton: PropTypes.bool,
  type: PropTypes.string,
  name: PropTypes.string,
  scentName: PropTypes.string,
  price: PropTypes.string,
  isHiddenType: PropTypes.bool,
  endIcon: PropTypes.shape(),
  onClick: PropTypes.func,
  onClickImage: PropTypes.func,
  buttonSize: PropTypes.string,
};

export default ItemProductPage;
