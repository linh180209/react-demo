import AppFlowActions from '../../constants';
import initialState from './initialState';

const productAll = (state = initialState.moods, action) => {
  switch (action.type) {
    case AppFlowActions.GET_PRODUCT_ALL_INIT_COMPLETED:
      return action.data;
    default:
      return state;
  }
};

export default productAll;
