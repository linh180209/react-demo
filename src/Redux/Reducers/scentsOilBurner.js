
import AppFlowActions from '../../constants';
import initialState from './initialState';

const scentsOilBurner = (state = initialState.scentsOilBurner, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_ALL_SCENTS_OIL_BURNER:
      return JSON.parse(JSON.stringify(action.data));
    default:
      return state;
  }
};

export default scentsOilBurner;
