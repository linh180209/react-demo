import AppFlowActions from '../../constants';
import initialState from './initialState';

const benefits = (state = initialState.benefits, action) => {
  switch (action.type) {
    case AppFlowActions.BENEFITS_COMPLETE:
      return action.data;
    default:
      return state;
  }
};

export default benefits;
