
import AppFlowActions from '../../constants';
import initialState from './initialState';

const scentsCandle = (state = initialState.scentsCandle, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_ALL_SCENTS_CANDLE:
      return JSON.parse(JSON.stringify(action.data));
    default:
      return state;
  }
};

export default scentsCandle;
