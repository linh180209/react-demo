
import AppFlowActions from '../../constants';
import initialState from './initialState';

const shippings = (state = initialState.shippings, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_ALL_SHIPPINGS:
      return JSON.parse(JSON.stringify(action.data));
    default:
      return state;
  }
};

export default shippings;
