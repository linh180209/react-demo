import AppFlowActions from '../../constants';
import initialState from './initialState';

const BookWorkshop = (state = initialState.bookWorkshop, action) => {
  switch (action.type) {
    case AppFlowActions.GET_EVENT_LIST_SIMPLY_BOOK_SUCCESS:
      return { ...state, eventList: action.data };
    case AppFlowActions.ADD_ADD_ONS:
      return { ...state, dataAddOns: action.data };
    case AppFlowActions.ADD_TIME:
      return { ...state, time: action.data };
    case AppFlowActions.ADD_DATA_FORM:
      return { ...state, dataForm: action.data };
    case AppFlowActions.UPDATE_DATA_FORM:
      return { ...state, dataForm: { ...state.dataForm, [action.data.field]: action.data.value } };
    case AppFlowActions.ADD_WORKSHOP_BOOKED:
      return { ...state, detailWorkshop: action.data };
    default:
      return state;
  }
};

export default BookWorkshop;
