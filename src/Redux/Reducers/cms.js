import _ from 'lodash';
import AppFlowActions from '../../constants';
import initialState from './initialState';

const cms = (state = initialState.cms, action) => {
  switch (action.type) {
    case AppFlowActions.ADD_CMS_COMPLETE:
    {
      if (!action.data) {
        return state;
      }
      const element = _.find(state, x => x.id === action.data.id);
      if (!element) {
        state.push(action.data);
        return JSON.parse(JSON.stringify(state));
      }
      return state;
    }
    default:
      return state;
  }
};

export default cms;
