import AppFlowActions from '../../constants';
import initialState from './initialState';

const countries = (state = initialState.countries, action) => {
  switch (action.type) {
    case AppFlowActions.COUNTRY_COMPLETE:
      return action.data;
    default:
      return state;
  }
};

export default countries;
