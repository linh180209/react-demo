import AppFlowActions from '../../constants';
import initialState from './initialState';

const scentsDiy = (state = initialState.scents, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_ALL_SCENTS_DIY:
      return JSON.parse(JSON.stringify(action.data));
    default:
      return state;
  }
};

export default scentsDiy;
