import AppFlowActions from '../../constants';
import initialState from './initialState';

const influencers = (state = initialState.influencers, action) => {
  switch (action.type) {
    case AppFlowActions.GET_INFLUENCER_INIT_COMPLETED:
      return action.data;
    default:
      return state;
  }
};

export default influencers;
