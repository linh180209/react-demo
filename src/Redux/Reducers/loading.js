
import initialState from './initialState';
import AppFlowActions from '../../constants';

const loading = (state = initialState.loading, action) => {
  switch (action.type) {
    case AppFlowActions.LOADING_COMPLTE: {
      return action.loading;
    }
    default:
      return state;
  }
};

export default loading;
