import AppFlowActions from '../../constants';
import initialState from './initialState';

const scentNotes = (state = initialState.scentNotes, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_ALL_SCENT_NOTES:
      return JSON.parse(JSON.stringify(action.data));
    default:
      return state;
  }
};

export default scentNotes;
