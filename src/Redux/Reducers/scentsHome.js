
import AppFlowActions from '../../constants';
import initialState from './initialState';

const scentsHome = (state = initialState.scentsHome, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_ALL_SCENTS_HOME:
      return JSON.parse(JSON.stringify(action.data));
    default:
      return state;
  }
};

export default scentsHome;
