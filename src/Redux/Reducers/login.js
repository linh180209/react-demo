import _ from 'lodash';
import AppFlowActions from '../../constants';
import initialState from './initialState';
import auth from '../Helpers/auth';

const login = (state = initialState.login, action) => {
  switch (action.type) {
    case AppFlowActions.LOGIN_COMPLETE:
    {
      auth.login(action.data);
      auth.setOutComeId(action.data.user.outcome);
      return action.data;
    }
    case AppFlowActions.LOGOUT_COMPLETE:
      return action;
    case AppFlowActions.UPDATE_OUTCOME:
    {
      const outcome = action.data;
      state.user.outcome = outcome;
      return JSON.parse(JSON.stringify(state));
    }
    case AppFlowActions.UPDATE_USER_INFO: {
      _.assign(state.user, action.data);
      return JSON.parse(JSON.stringify(state));
    }
    default:
      return state;
  }
};

export default login;
