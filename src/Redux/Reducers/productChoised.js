import AppFlowActions from '../../constants';
import initialState from './initialState';

const productChoised = (state = initialState.productChoised, action) => {
  switch (action.type) {
    case AppFlowActions.ADD_PRODUCTCHOISED_REQUEST:
    {
      const result = _.concat(state, action.data);
      return JSON.parse(JSON.stringify(result));
    }
    case AppFlowActions.CLEAR_PRODUCTCHOISED_REQUEST:
      return [];
    default:
      return state;
  }
};

export default productChoised;
