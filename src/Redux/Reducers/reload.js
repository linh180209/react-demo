import initialState from './initialState';
import AppFlowActions from '../../constants';

function processReload(state = initialState, action) {
  if (action.type === AppFlowActions.RELOAD_PAGE_REQUEST) {
    const { login, basket } = state;
    const returnValue = Object.assign({}, initialState);
    returnValue.login = login;
    returnValue.basket = basket;
    return returnValue;
  }
  return state;
}

export default processReload;
