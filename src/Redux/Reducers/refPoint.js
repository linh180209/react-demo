import AppFlowActions from '../../constants';
import initialState from './initialState';

const refPoint = (state = initialState.refPoint, action) => {
  switch (action.type) {
    case AppFlowActions.UPDATE_REF_COMPLETED:
      return action.data;
    default:
      return state;
  }
};

export default refPoint;
