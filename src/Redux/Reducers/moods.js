import AppFlowActions from '../../constants';
import initialState from './initialState';

const moods = (state = initialState.moods, action) => {
  switch (action.type) {
    case AppFlowActions.GET_MOODS_INIT_COMPLETED:
      return action.data;
    default:
      return state;
  }
};

export default moods;
