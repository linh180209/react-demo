
import AppFlowActions from '../../constants';
import initialState from './initialState';

const scentsReedDiffuser = (state = initialState.scentsReedDiffuser, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_ALL_SCENTS_REED_DIFFUSER:
      return JSON.parse(JSON.stringify(action.data));
    default:
      return state;
  }
};

export default scentsReedDiffuser;
