
import _ from 'lodash';
import initialState from './initialState';
import AppFlowActions from '../../constants';
import { convertStringToPriceBasket } from '../Helpers';
import auth from '../Helpers/auth';

const basket = (state = initialState.basket, action) => {
  switch (action.type) {
    case AppFlowActions.LOAD_BASKET_COMPLETE: {
      const data = _.assign({}, action.data);
      convertStringToPriceBasket(data);
      auth.setBasketId(data.id);
      return data;
    }
    case AppFlowActions.ADD_BASKET_COMPLETE: {
      state.items.push(action.data);
      convertStringToPriceBasket(state);
      return JSON.parse(JSON.stringify(state));
    }
    case AppFlowActions.DELETE_BASKET_COMPLETE: {
      const { id } = action.data;
      if (id) {
        _.remove(state.items, x => x.id === id);
        convertStringToPriceBasket(state);
        return JSON.parse(JSON.stringify(state));
      }
      return state;
    }
    case AppFlowActions.UPDATE_BASKET_COMPLETE: {
      const { id } = action.data;
      const i = _.find(state.items, x => x.id === id);
      if (i) {
        _.assign(i, action.data);
        convertStringToPriceBasket(state);
        return JSON.parse(JSON.stringify(state));
      }
      return state;
    }
    case AppFlowActions.CLEAR_BASKET_REQUEST: {
      state.items = [];
      state.subtotal = 0;
      state.total = 0;
      return JSON.parse(JSON.stringify(state));
    }
    default:
      return state;
  }
};

export default basket;
