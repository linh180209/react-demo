import AppFlowActions from '../../constants';
import initialState from './initialState';

const answers = (state = initialState.answers, action) => {
  switch (action.type) {
    case AppFlowActions.ANWSERS_COMPLETE:
      return action.data;
    default:
      return state;
  }
};

export default answers;
