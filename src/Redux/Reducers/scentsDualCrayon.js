
import AppFlowActions from '../../constants';
import initialState from './initialState';

const scentsDualCrayon = (state = initialState.scentsCandle, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_ALL_SCENTS_DUAL_CRAYONS:
      return JSON.parse(JSON.stringify(action.data));
    default:
      return state;
  }
};

export default scentsDualCrayon;
