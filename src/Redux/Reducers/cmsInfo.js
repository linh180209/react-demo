import AppFlowActions from '../../constants';
import initialState from './initialState';

const cmsInfo = (state = initialState.cmsInfo, action) => {
  switch (action.type) {
    case AppFlowActions.UPDATE_CMS_ALL_PAGE:
      return action.data;
    default:
      return state;
  }
};

export default cmsInfo;
