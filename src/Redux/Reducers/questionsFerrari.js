import AppFlowActions from '../../constants';
import initialState from './initialState';

const questionsFerrari = (state = initialState.questionsFerrari, action) => {
  switch (action.type) {
    case AppFlowActions.QUESTIONS_FERRARI_COMPLETE:
      return action.data;
    default:
      return state;
  }
};

export default questionsFerrari;
