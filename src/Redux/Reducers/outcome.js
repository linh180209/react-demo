
import initialState from './initialState';
import AppFlowActions from '../../constants';

const outcome = (state = initialState.outcome, action) => {
  switch (action.type) {
    case AppFlowActions.UPDATE_OUTCOME_COMPLETE: {
      return action.data;
    }
    default:
      return state;
  }
};

export default outcome;
