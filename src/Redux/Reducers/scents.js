import AppFlowActions from '../../constants';
import initialState from './initialState';

const scents = (state = initialState.scents, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_ALL_SCENTS:
      return JSON.parse(JSON.stringify(action.data));
    default:
      return state;
  }
};

export default scents;
