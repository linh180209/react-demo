
import initialState from './initialState';
import AppFlowActions from '../../constants';

const ingredients = (state = initialState.ingredients, action) => {
  switch (action.type) {
    case AppFlowActions.UPDATE_INGREDIENT_COMPLETE: {
      return action.data;
    }
    default:
      return state;
  }
};

export default ingredients;
