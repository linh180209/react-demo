
import _ from 'lodash';
import initialState from './initialState';
import AppFlowActions from '../../constants';
import { convertStringToPriceBasket } from '../Helpers';

const basketSubscription = (state = initialState.basketSubscription, action) => {
  switch (action.type) {
    case AppFlowActions.LOAD_BASKET_SUBSCRIPTION_COMPLETE: {
      const data = _.assign({}, action.data);
      convertStringToPriceBasket(data);
      return data;
    }
    case AppFlowActions.ADD_BASKET_SUBSCRIPTION_COMPLETE: {
      state.items.push(action.data);
      convertStringToPriceBasket(state);
      return JSON.parse(JSON.stringify(state));
    }
    default:
      return state;
  }
};

export default basketSubscription;
