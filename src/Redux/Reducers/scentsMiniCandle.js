
import AppFlowActions from '../../constants';
import initialState from './initialState';

const scentsMiniCandle = (state = initialState.scentsMiniCandle, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_ALL_SCENTS_MINI_CANDLE:
      return JSON.parse(JSON.stringify(action.data));
    default:
      return state;
  }
};

export default scentsMiniCandle;
