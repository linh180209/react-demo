
import initialState from './initialState';
import AppFlowActions from '../../constants';

const isReady = (state = initialState.isReady, action) => state;

export default isReady;
