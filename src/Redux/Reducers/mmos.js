import AppFlowActions from '../../constants';
import initialState from './initialState';

const mmos = (state = initialState.moods, action) => {
  switch (action.type) {
    case AppFlowActions.GET_MMOS_INIT_COMPLETED:
      return action.data;
    default:
      return state;
  }
};

export default mmos;
