import AppFlowActions from '../../constants';
import initialState from './initialState';

const questions = (state = initialState.questions, action) => {
  switch (action.type) {
    case AppFlowActions.QUESTIONS_COMPLETE:
      return action.data;
    default:
      return state;
  }
};

export default questions;
