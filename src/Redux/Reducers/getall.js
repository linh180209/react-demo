import initialState from './initialState';
import { convertStringToPriceBasket } from '../Helpers';

export function processGetAllApi(state = initialState, action) {
  const { data } = action;
  const { basket } = data;
  if (basket) {
    convertStringToPriceBasket(basket);
  }
  Object.assign(state, data);
  state.isReady = true;
  const returnValue = JSON.parse(JSON.stringify(state));
  return returnValue;
}

export function updateMultiData(state = initialState, action) {
  const { data } = action;
  Object.assign(state, data);
  const returnValue = JSON.parse(JSON.stringify(state));
  return returnValue;
}
