
import AppFlowActions from '../../constants';
import initialState from './initialState';

const scentsWax = (state = initialState.scentsWax, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_ALL_SCENTS_WAX:
      return JSON.parse(JSON.stringify(action.data));
    default:
      return state;
  }
};

export default scentsWax;
