import AppFlowActions from '../../constants';
import initialState from './initialState';

const answersFerrari = (state = initialState.answersFerrari, action) => {
  switch (action.type) {
    case AppFlowActions.ANWSERS_FERRARI_COMPLETE:
      return action.data;
    default:
      return state;
  }
};

export default answersFerrari;
