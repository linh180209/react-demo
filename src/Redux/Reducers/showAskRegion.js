
import initialState from './initialState';
import AppFlowActions from '../../constants';

const showAskRegion = (state = initialState.showAskRegion, action) => {
  switch (action.type) {
    case AppFlowActions.UPDATE_SHOW_ASK_REGION: {
      return action.showAskRegion;
    }
    default:
      return state;
  }
};

export default showAskRegion;
