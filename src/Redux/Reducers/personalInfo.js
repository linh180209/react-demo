import AppFlowActions from '../../constants';
import initialState from './initialState';

const personalInfo = (state = initialState.personalInfo, action) => {
  switch (action.type) {
    case AppFlowActions.SET_PERSONAL_INFO:
      return [...action.data];
    case AppFlowActions.CLEAR_PERSONAL_INFO:
      return [];
    default:
      return state;
  }
};

export default personalInfo;
