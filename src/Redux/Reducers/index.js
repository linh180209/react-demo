import { combineReducers } from 'redux';
import initialState from './initialState';
import auth from '../Helpers/auth';
import AppFlowActions from '../../constants';
import login from './login';
import answers from './answers';
import answersFerrari from './answersFerrari';
import loading from './loading';
import questions from './questions';
import questionsFerrari from './questionsFerrari';
import products from './products';
import benefits from './benefits';
import cms from './cms';
import cmsInfo from './cmsInfo';
import moods from './moods';
import outcome from './outcome';
import mmos from './mmos';
import productAll from './productAll';
import influencers from './influencers';
import basketSubscription from './basketSubscription';
import scents from './scents';
import scentsDiy from './scentsDiy';
import scentsWax from './scentsWax';
import scentsCandle from './scentsCandle';
import scentsMiniCandle from './scentsMiniCandle';
import scentsHome from './scentsHome';
import scentsDualCrayon from './scentsDualCrayon';
import scentsOilBurner from './scentsOilBurner';
import scentsReedDiffuser from './scentsReedDiffuser';
import scentNotes from './scentNotes';
import showAskRegion from './showAskRegion';
import shippings from './shippings';


import { processGetAllApi, updateMultiData } from './getall';
import processReload from './reload';
import personalInfo from './personalInfo';
import productChoised from './productChoised';
import isReady from './isReady';
import basket from './basket';
import refPoint from './refPoint';
import countries from './countries';
import ingredients from './ingredients';
import bookWorkshop from './bookWorkshop';

const appReducer = combineReducers({
  login,
  answers,
  answersFerrari,
  loading,
  questions,
  questionsFerrari,
  products,
  benefits,
  personalInfo,
  productChoised,
  isReady,
  cms,
  cmsInfo,
  moods,
  outcome,
  basket,
  refPoint,
  mmos,
  productAll,
  influencers,
  basketSubscription,
  scents,
  scentsDiy,
  scentsWax,
  scentsCandle,
  scentsMiniCandle,
  scentsHome,
  scentsDualCrayon,
  scentNotes,
  countries,
  ingredients,
  showAskRegion,
  bookWorkshop,
  scentsOilBurner,
  scentsReedDiffuser,
  shippings,
});

function rootReducer(state, action) {
  if (action.type === AppFlowActions.LOGOUT_REQUEST) {
    auth.logout();
    state.basket = {};
    state.login = {};
    state.outcome = {};
    state.personalInfo = [];
    state.productChoised = [];
    state.products = [];
    return JSON.parse(JSON.stringify(state));
  }

  if (action.type === AppFlowActions.GET_ALL_DATA_COMPLETE) {
    return processGetAllApi(state, action);
  }

  if (action.type === AppFlowActions.UPDATE_MULTI_DATA) {
    return updateMultiData(state, action);
  }

  if (action.type === AppFlowActions.RELOAD_PAGE_REQUEST) {
    return processReload(state, action);
  }
  return appReducer(state, action);
}

export default rootReducer;
