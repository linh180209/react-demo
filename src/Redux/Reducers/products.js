import AppFlowActions from '../../constants';
import initialState from './initialState';

const products = (state = initialState.products, action) => {
  // console.log('action products', action);
  switch (action.type) {
    case AppFlowActions.ADD_PRODUCTS_REQUEST:
      state.push(action.data);
      return JSON.parse(JSON.stringify(state));
    case AppFlowActions.CLEAR_PRODUCTS_REQUEST:
      return [];
    default:
      return state;
  }
};

export default products;
