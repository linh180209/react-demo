import AppFlowActions from '../../constants';

export default function updateAnswersData(data) {
  return ({ type: AppFlowActions.ANWSERS_COMPLETE, data });
}

export function updateAnswersFerrariData(data) {
  return ({ type: AppFlowActions.ANWSERS_FERRARI_COMPLETE, data });
}
