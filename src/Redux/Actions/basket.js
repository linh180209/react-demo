import AppFlowActions from '../../constants';

export function loadAllBasket(data) {
  return ({ type: AppFlowActions.LOAD_BASKET_COMPLETE, data });
}

export function addProductBasket(data) {
  return ({ type: AppFlowActions.ADD_BASKET_REQUEST, data });
}

export function reorderProductBasket(data) {
  return ({ type: AppFlowActions.REODER_BASKET_REQUEST, data });
}

export function addProductGiftBasket(data) {
  return ({ type: AppFlowActions.ADD_GIFT_BASKET_REQUEST, data });
}

export function addProductBasketArray(data) {
  return ({ type: AppFlowActions.ADD_BASKET_REQUEST_ARRAY, data });
}

export function preCheckOutBasket(data) {
  return ({ type: AppFlowActions.PRECHECKOUT_BASKET_PRODUCT_REQUEST, data });
}

export function preCheckOutBasketSubscription(data) {
  return ({ type: AppFlowActions.PRECHECKOUT_BASKET_SUBSCRIPTION_PRODUCT_REQUEST, data });
}

export function deleteProductBasket(data) {
  return ({ type: AppFlowActions.DELETE_BASKET_REQUEST, data });
}

export function clearProductBasket(data) {
  return ({ type: AppFlowActions.CLEAR_BASKET_PRODUCT_REQUEST, data });
}

export function updateProductBasket(data) {
  return ({ type: AppFlowActions.UPDATE_BASKET_REQUEST, data });
}

export function updateProductArrayBasket(data) {
  return ({ type: AppFlowActions.UPDATE_BASKET_ARRAY_REQUEST, data });
}

export function createBasketGuest(data) {
  return ({ type: AppFlowActions.CREATE_BASKET_GUEST_REQUEST, data });
}

export function createBasketGuestArray(data) {
  return ({ type: AppFlowActions.CREATE_BASKET_GUEST_REQUEST_ARRAY, data });
}

export function clearBasket() {
  return ({ type: AppFlowActions.CLEAR_BASKET_REQUEST });
}
