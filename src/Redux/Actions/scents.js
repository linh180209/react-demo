import AppFlowActions from '../../constants';

/**
 * @param {*} data
 */
export default function addScentsToStore(data) {
  return ({ type: AppFlowActions.ADD_ALL_SCENTS, data });
}

export function addScentNotesToStore(data) {
  return ({ type: AppFlowActions.ADD_ALL_SCENT_NOTES, data });
}

export function addScentsDiyToStore(data) {
  return ({ type: AppFlowActions.ADD_ALL_SCENTS_DIY, data });
}

export function addScentsWaxToStore(data) {
  return ({ type: AppFlowActions.ADD_ALL_SCENTS_WAX, data });
}

export function addScentsCandleToStore(data) {
  return ({ type: AppFlowActions.ADD_ALL_SCENTS_CANDLE, data });
}

export function addScentsMiniCandleToStore(data) {
  return ({ type: AppFlowActions.ADD_ALL_SCENTS_MINI_CANDLE, data });
}

export function addScentsHomeToStore(data) {
  return ({ type: AppFlowActions.ADD_ALL_SCENTS_HOME, data });
}

export function addScentsDualCrayonsToStore(data) {
  return ({ type: AppFlowActions.ADD_ALL_SCENTS_DUAL_CRAYONS, data });
}

export function addScentsOilBurnerToStore(data) {
  return ({ type: AppFlowActions.ADD_ALL_SCENTS_OIL_BURNER, data });
}

export function addScentsReedDifffUserToStore(data) {
  return ({ type: AppFlowActions.ADD_ALL_SCENTS_REED_DIFFUSER, data });
}
