import AppFlowActions from '../../constants';

export default function updateQuestionsData(data) {
  return ({ type: AppFlowActions.QUESTIONS_COMPLETE, data });
}

export function updateQuestionsFerrariData(data) {
  return ({ type: AppFlowActions.QUESTIONS_FERRARI_COMPLETE, data });
}
