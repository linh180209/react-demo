import AppFlowActions from '../../constants';

/**
 *
 * @param {*} data
 */
export function addProductsRequest(data) {
  return ({ type: AppFlowActions.ADD_PRODUCTS_REQUEST, data });
}

/**
 *
 * @param {*} data
 */
export function clearAllProducts() {
  return ({ type: AppFlowActions.CLEAR_PRODUCTS_REQUEST });
}
