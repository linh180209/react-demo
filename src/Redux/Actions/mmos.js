import AppFlowActions from '../../constants';

export default function updateMmosData(data) {
  return ({ type: AppFlowActions.GET_MMOS_INIT_COMPLETED, data });
}
