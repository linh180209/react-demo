import AppFlowActions from '../../constants';

export function loadAllBasketSubscription(data) {
  return ({ type: AppFlowActions.LOAD_BASKET_COMPLETE, data });
}

export function createBasketSubscription(data) {
  return ({ type: AppFlowActions.CREATE_BASKET_SUBSCRIPTION_REQUEST, data });
}
