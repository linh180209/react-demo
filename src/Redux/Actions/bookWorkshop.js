import AppFlowActions from '../../constants';

/**
 *
 * @param {*} data
 */

export function getEventListSimplyBook() {
  return ({ type: AppFlowActions.GET_EVENT_LIST_SIMPLY_BOOK });
}

export function addAddOns(data) {
  return ({ type: AppFlowActions.ADD_ADD_ONS, data });
}

export function addTime(data) {
  return ({ type: AppFlowActions.ADD_TIME, data });
}

export function addDataForm(data) {
  return ({ type: AppFlowActions.ADD_DATA_FORM, data });
}

export function updateDataForm(data) {
  return ({ type: AppFlowActions.UPDATE_DATA_FORM, data });
}

export function addDetailWorkshopBooked(data) {
  return ({ type: AppFlowActions.ADD_WORKSHOP_BOOKED, data });
}
