
import AppFlowActions from '../../constants';

/** */
export default function loadingPage(data) {
  return ({ type: AppFlowActions.LOADING_COMPLTE, loading: data });
}
