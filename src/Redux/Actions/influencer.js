import AppFlowActions from '../../constants';

export default function updateInfluencerData(data) {
  return ({ type: AppFlowActions.GET_INFLUENCER_INIT_COMPLETED, data });
}
