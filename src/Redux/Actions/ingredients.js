import AppFlowActions from '../../constants';

export default function updateIngredientsData(data) {
  return ({ type: AppFlowActions.UPDATE_INGREDIENT_COMPLETE, data });
}
