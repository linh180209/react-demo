import AppFlowActions from '../../constants';

/**
 * @param {*} data
 */
export default function updateShippings(data) {
  return ({ type: AppFlowActions.ADD_ALL_SHIPPINGS, data });
}
