import AppFlowActions from '../../constants';

/**
 *
 * @param {*} data
 */
export function addProductChoisedRequest(data) {
  return ({ type: AppFlowActions.ADD_PRODUCTCHOISED_REQUEST, data });
}

/**
 *
 * @param {*} data
 */
export function clearAllProductChoised() {
  return ({ type: AppFlowActions.CLEAR_PRODUCTCHOISED_REQUEST });
}
