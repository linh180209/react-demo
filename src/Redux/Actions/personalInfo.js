
import AppFlowActions from '../../constants';

/** */
export function setPersonalInfo(data) {
  return ({ type: AppFlowActions.SET_PERSONAL_INFO, data });
}

export function clearPersonalInfo() {
  return ({ type: AppFlowActions.CLEAR_PERSONAL_INFO });
}
