import AppFlowActions from '../../constants';

export default function updateBenefitsData(data) {
  return ({ type: AppFlowActions.BENEFITS_COMPLETE, data });
}
