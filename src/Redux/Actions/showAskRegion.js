
import AppFlowActions from '../../constants';

/** */
export default function updateShowAskRegion(data) {
  return ({ type: AppFlowActions.UPDATE_SHOW_ASK_REGION, showAskRegion: data });
}
