
import AppFlowActions from '../../constants';

/** */
export default function updateOutComeComplete(data) {
  return ({ type: AppFlowActions.UPDATE_OUTCOME_COMPLETE, data });
}
