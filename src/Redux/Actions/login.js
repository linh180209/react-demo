import AppFlowActions from '../../constants';

/**
 *
 * @param {*} data
 */
export function loginRequest(data) {
  return ({ type: AppFlowActions.LOGIN_REQUEST, data });
}

export function loginCompleted(data) {
  return ({ type: AppFlowActions.LOGIN_COMPLETE, data });
}

export function loginUpdateOutCome(data) {
  return ({ type: AppFlowActions.UPDATE_OUTCOME, data });
}

export function loginUpdateUserInfo(data) {
  return ({ type: AppFlowActions.UPDATE_USER_INFO, data });
}
/**
 *
 * @param {*} data
 */
export function logoutRequest() {
  return ({ type: AppFlowActions.LOGOUT_REQUEST });
}
