import AppFlowActions from '../../constants';

export default function getAllRequest() {
  return ({ type: AppFlowActions.GET_ALL_DATA_REQUEST });
}

export function updateMultiData(data) {
  return ({ type: AppFlowActions.UPDATE_MULTI_DATA, data });
}