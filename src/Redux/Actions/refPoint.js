import AppFlowActions from '../../constants';

/**
 * @param {*} data
 */
export default function updateRefRequest(data) {
  return ({ type: AppFlowActions.UPDATE_REF_COMPLETED, data });
}
