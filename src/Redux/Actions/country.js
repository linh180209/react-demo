import AppFlowActions from '../../constants';

export default function updateAllCountry(data) {
  return ({ type: AppFlowActions.COUNTRY_COMPLETE, data });
}
