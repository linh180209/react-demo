import AppFlowActions from '../../constants';

export default function updateMoodsData(data) {
  return ({ type: AppFlowActions.GET_MOODS_INIT_COMPLETED, data });
}
