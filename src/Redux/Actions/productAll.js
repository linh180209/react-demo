import AppFlowActions from '../../constants';

export default function updateProductAllData(data) {
  return ({ type: AppFlowActions.GET_PRODUCT_ALL_INIT_COMPLETED, data });
}
