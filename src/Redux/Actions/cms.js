import AppFlowActions from '../../constants';

export default function addCmsRedux(data) {
  return ({ type: AppFlowActions.ADD_CMS_COMPLETE, data });
}

export function updateCMSAllPage(data) {
  return ({ type: AppFlowActions.UPDATE_CMS_ALL_PAGE, data });
}

export function getCommonCMSRequest() {
  return ({ type: AppFlowActions.GET_CMS_COMMON_REQUEST });
}
