import {
  put, call, take, fork,
} from 'redux-saga/effects';
import _ from 'lodash';
import AppFlowActions from '../../constants';
import auth from '../Helpers/auth';
import fetchClient from '../Helpers/fetch-client';
import {
  CLEAR_BASKET_URL, GET_COUNTRY_URL,
  GET_MOOD_URL, TOKEN_REFRESH, CREATE_BASKET_URL, GET_BASKET_URL, GET_USER_DETAIL,
} from '../../config';

function resetToken() {
  const options = {
    url: TOKEN_REFRESH,
    method: 'POST',
    body: {
      refresh_token: auth.tokenRefresh(),
      client_id: 'scentdesigner',
    },
  };
  return fetchClient(options);
}

function getBasket(id) {
  const options = {
    url: GET_BASKET_URL.replace('{cartPk}', id),
    method: 'GET',
  };
  return fetchClient(options, true);
}

export function getAllMood() {
  try {
    const options = {
      url: GET_MOOD_URL,
      method: 'GET',
    };

    return fetchClient(options);
  } catch (error) {
    return null;
  }
}

function createBasket(user, token) {
  const { id } = user;
  const options = {
    url: CREATE_BASKET_URL.replace('{userId}', id),
    method: 'POST',
    body: {},
  };
  return fetchClient(options, true, token);
}

function getUser() {
  const options = {
    url: GET_USER_DETAIL,
    method: 'GET',
  };
  return fetchClient(options, true);
}

function clearBasketOnServer(id) {
  const options = {
    url: CLEAR_BASKET_URL
      .replace('{id}', id),
    method: 'DELETE',
  };
  return fetchClient(options, true);
}

export function* getAllRequest() {
  const INFINITE = true;
  while (INFINITE) {
    yield take(AppFlowActions.GET_ALL_DATA_REQUEST);
    let loginData;
    let user;
    try {
      user = auth.tokenRefresh() && auth.tokenRefresh().length === 40 ? yield call(getUser) : undefined;
    } catch (error) {
      console.log('error', error);
    }
    try {
      if (user && user?.id) {
        loginData = {
          user,
          refresh_token: auth.tokenRefresh(),
          token: auth.token(),
        };
      } else {
        loginData = auth.tokenRefresh() && auth.tokenRefresh().length === 40 ? yield call(resetToken) : undefined;
      }
    } catch (error) {
      loginData = undefined;
      auth.logoutNotOutCome();
    }
    if (loginData && !loginData.isError) {
      auth.login(loginData);
    } else {
      auth.logoutNotOutCome();
    }
    const basketStoreID = auth.getBasketId();
    let basket = loginData && !loginData.isError
      ? (loginData.user.cart ? yield call(getBasket, loginData.user.cart)
        : yield call(createBasket, loginData.user, loginData.token))
      : (basketStoreID !== undefined ? yield call(getBasket, basketStoreID) : undefined);
    const login = auth.getLogin();
    if (!basket || (basket && basket.isError)) {
      if (!_.isEmpty(login)) {
      // yield call(clearBasketOnServer, loginData.user.cart);
      // basket = yield call(getBasket, loginData.user.cart);
        basket = yield call(createBasket, loginData.user, loginData.token);
        // if (!basket || (basket && basket.isError)) {
        //   auth.setBasketId(undefined);
        // }
        if (basket && !basket.isError) {
          loginData.user.cart = basket.id;
          auth.setBasketId(basket.id);
          auth.login(loginData);
        }
      } else {
        auth.setBasketId(undefined);
      }
    }
    yield put({
      type: AppFlowActions.GET_ALL_DATA_COMPLETE,
      data: {
        login,
        basket: basket && basket.isError ? undefined : basket,
      },
    });
  }
}


export default function* getAllFlow() {
  yield fork(getAllRequest);
}
