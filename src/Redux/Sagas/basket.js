import {
  put, call, take, fork, select,
} from 'redux-saga/effects';
import _ from 'lodash';
import axios from 'axios';

import {
  ADD_BASKET_URL, EDIT_DELETE_BASKET_URL, GUEST_CREATE_BASKET_URL, CLEAR_BASKET_URL, PRECHECKOUT_BASKET_URL, GET_BASKET_URL,
} from '../../config';
import auth from '../Helpers/auth';
import AppFlowActions from '../../constants';
import fetchClient, { fetchClientFormData } from '../Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../Helpers/notification';
import {
  trackGTMAddToCart, trackGTMRemoveCart,
} from '../Helpers/index';
import emitter from '../Helpers/eventEmitter';

export function generateBodyItemCart(data, url, method) {
  const { item } = data;
  const {
    item: itemId, price, quantity, total, name, image, file, is_display_name: isDisplayName,
    meta, is_customized: isCustomized, is_black: isBlack, currency, price_custom: priceCustom,
    id, font, color, imagePremade, is_free_gift: isFree, external_product: externalProduct,
    is_featured: isFeatured,
  } = item;
  if (!file || imagePremade || (file && file.length === 0)) {
    const options = {
      url,
      method,
      body: {
        id,
        item: itemId,
        is_featured: !!isFeatured,
        currency,
        price_custom: priceCustom,
        // price,
        quantity: Number.isInteger(quantity) ? quantity : 1,
        // total,
        is_display_name: isDisplayName,
        image_suggestion: imagePremade,
        name: name || undefined,
        image: null,
        meta,
        is_customized: !!isCustomized,
        font,
        color,
        is_free_gift: isFree,
      },
    };
    if (externalProduct) {
      _.assign(options.body, { external_product: externalProduct });
    }
    return options;
  }
  const formData = new FormData();
  formData.append('image', file);
  formData.append('item', itemId);
  formData.append('is_featured', !!isFeatured);
  // formData.append('price', price);
  formData.append('quantity', Number.isInteger(quantity) ? quantity : 1);
  // formData.append('total', total);
  formData.append('is_display_name', isDisplayName);
  formData.append('is_customized', !!isCustomized);
  formData.append('font', font);
  formData.append('color', color);
  formData.append('image_suggestion', '');
  if (externalProduct) {
    formData.append('external_product', externalProduct);
  }
  if (name) {
    formData.append('name', name || undefined);
  }
  if (image) {
    formData.append('image', image || undefined);
  }
  if (meta) {
    formData.append('meta', JSON.stringify(meta));
  }
  const options = {
    url,
    method,
    body: formData,
  };
  return options;
}

function reoderBasketToServer(data) {
  const { idCart, idLineItem } = data;
  const option = {
    url: ADD_BASKET_URL.replace('{cartPk}', idCart),
    method: 'POST',
    body: {
      line_item: idLineItem,
    },
  };
  return fetchClient(option);
}

const updateBaskettoServer = (data) => {
  const { idCart, item, isUpdateFromProductDetail } = data;
  if (isUpdateFromProductDetail) {
    const url = EDIT_DELETE_BASKET_URL.replace('{cartPk}', idCart).replace('{id}', item.id);
    const {
      file, imagePremade,
    } = item;
    if (!file || imagePremade) {
      const options = generateBodyItemCart(data, url, 'PUT');
      return fetchClient(options, true);
    }
    const options = generateBodyItemCart(data, url, 'PUT');
    return fetchClientFormData(options, true);
  }

  // if change from cartItem or change from checkout page
  const {
    id, quantity, promo,
  } = item;
  const options = {
    url: EDIT_DELETE_BASKET_URL
      .replace('{cartPk}', idCart)
      .replace('{id}', item.id),
    method: 'PUT',
    body: {
      id,
      quantity: Number.isInteger(quantity) ? quantity : 1,
    },
  };
  if (promo) {
    options.body.promo = promo;
  }
  return fetchClient(options, true);
};

const getCheckUpdateItem = (basket, data) => {
  const { item } = data;

  if (_.isEmpty(basket) || item?.type === 'Perfume') {
    return null;
  }

  // Check for gift.
  // If user don't use the form existed before, let create new one
  const ele = _.find(basket?.items, (x) => {
    if (x.item?.id === item.item) {
      if (item?.meta?.isCreateNewGift) return null;
      return x;
    }
  });

  if (ele) {
    item.quantity += ele.quantity;
    item.id = ele.id;
    return data;
  }
  return null;
};

function addBasketToServer(data, basket) {
  const { idCart, item } = data;

  // check if exist item into basket, then update quanity
  const newData = getCheckUpdateItem(basket, data);
  if (newData) {
    return updateBaskettoServer(newData);
  }
  if (!idCart || !item?.item) {
    return null;
  }
  const url = ADD_BASKET_URL.replace('{cartPk}', idCart);
  const {
    file, imagePremade,
  } = item;

  // tracking gtm
  // trackGTMAddToCart(item);

  if (!file || imagePremade) {
    const options = generateBodyItemCart(data, url, 'POST');
    return fetchClient(options, true);
  }
  const options = generateBodyItemCart(data, url, 'POST');
  return fetchClientFormData(options, true);
}

function deleteBasketToServer(data) {
  const { idCart, item } = data;
  if (!idCart || !item.id) {
    return null;
  }
  const options = {
    url: EDIT_DELETE_BASKET_URL
      .replace('{cartPk}', idCart)
      .replace('{id}', item.id),
    method: 'DELETE',
  };
  // tracking GTM
  trackGTMRemoveCart(item);

  return fetchClient(options, true);
}

const clearBasketOnServer = (id) => {
  const options = {
    url: CLEAR_BASKET_URL
      .replace('{id}', id),
    method: 'DELETE',
  };
  return fetchClient(options, true);
};

const preCheckOutBasket = (data) => {
  const { id, body } = data;
  const options = {
    url: PRECHECKOUT_BASKET_URL
      .replace('{cartPk}', id),
    method: 'POST',
    body,
  };
  return fetchClient(options);
};

function* reorderBasketRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.REODER_BASKET_REQUEST);
    yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: true });
    const { data } = request;
    const result = yield call(reoderBasketToServer, data);
    console.log('== result', result);
    if (result && !result.isError) {
      // tracking gtm
      const { idItem } = data;
      const { items } = result;
      trackGTMAddToCart({ item: idItem }, items?.length > 0 ? items[0].id : undefined);

      // yield put({ type: AppFlowActions.ADD_BASKET_COMPLETE, data: result });
      yield put({ type: AppFlowActions.LOAD_BASKET_COMPLETE, data: result });
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    } else {
      toastrError('Add To Cart');
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    }
  }
}

function* addBasketRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.ADD_BASKET_REQUEST);
    yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: true });
    const { data } = request;
    const basket = yield select(state => state.basket);
    const result = yield call(addBasketToServer, data, basket);
    if (result && !result.isError) {
      // check send emit display popup add to cart
      const newData = getCheckUpdateItem(basket, data);
      if (newData) {
        emitter.emit(AppFlowActions.ADD_TO_CART_POPUP, newData);
      }
      // tracking gtm
      const { item } = data;
      const { items } = result;
      trackGTMAddToCart(item, items?.length > 0 ? items[0].id : undefined);

      // yield put({ type: AppFlowActions.ADD_BASKET_COMPLETE, data: result });
      yield put({ type: AppFlowActions.LOAD_BASKET_COMPLETE, data: result });
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    } else {
      toastrError('Add To Cart');
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    }
  }
}


function* addGiftBasketRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.ADD_GIFT_BASKET_REQUEST);
    yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: true });
    const { data } = request;
    if (data.length === 2 && data[1].item.id) {
      yield call(deleteBasketToServer, data[1]);
    }
    const result = yield call(addBasketToServer, data[0]);
    if (result && !result.isError) {
      // tracking gtm
      const { item } = data[0];
      const { items } = result;
      trackGTMAddToCart(item, items?.length > 0 ? items[0].id : undefined);

      yield put({ type: AppFlowActions.LOAD_BASKET_COMPLETE, data: result });
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    } else {
      toastrError('Add To Cart');
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    }
  }
}


const updateBasketBoutique = (data) => {
  const { idCart } = data;
  const options = {
    url: GET_BASKET_URL
      .replace('{cartPk}', idCart),
    method: 'PUT',
    body: {
      is_boutique: true,
    },
  };
  return fetchClient(options);
};

function* addBasketArrayRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.ADD_BASKET_REQUEST_ARRAY);
    yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: true });
    const { data } = request;
    const isBoutique = data && data.length > 0 && data[0].is_boutique;
    if (isBoutique) {
      yield call(updateBasketBoutique, data[0]);
    }
    for (let i = 0; i < data.length; i += 1) {
      const result = yield call(addBasketToServer, data[i]);
      if (result && !result.isError) {
        // tracking gtm
        const { item } = data[i];
        const { items } = result;
        trackGTMAddToCart(item, items?.length > 0 ? items[0].id : undefined);

        // yield put({ type: AppFlowActions.ADD_BASKET_COMPLETE, data: result });
        yield put({ type: AppFlowActions.LOAD_BASKET_COMPLETE, data: result });
        yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
      } else {
        toastrError('Add To Cart');
        yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
      }
    }
  }
}

function* deleteBasketRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.DELETE_BASKET_REQUEST);
    yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: true });
    const { data } = request;
    const result = yield call(deleteBasketToServer, data);
    if (result) {
      yield put({ type: AppFlowActions.LOAD_BASKET_COMPLETE, data: result });
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    } else {
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    }
  }
}

function* updateBasketRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.UPDATE_BASKET_REQUEST);
    yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: true });
    const { data } = request;
    const result = yield call(updateBaskettoServer, data);
    if (result && !result.isError) {
      const { increase, item } = data;
      if (increase) {
        const dataT = _.cloneDeep(item);
        dataT.quantity = 1;
        trackGTMAddToCart(dataT, item.id);
      }

      // yield put({ type: AppFlowActions.UPDATE_BASKET_COMPLETE, data: item });
      yield put({ type: AppFlowActions.LOAD_BASKET_COMPLETE, data: result });
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
      toastrSuccess('Cart Updated');
    } else {
      toastrError('Cart Updated');
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    }
  }
}

function* updateBasketArrayRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.UPDATE_BASKET_ARRAY_REQUEST);
    const { data } = request;
    for (let i = 0; i < data.length; i += 1) {
      yield call(updateBaskettoServer, data[0]);
    }
  }
}

function* clearBasketOnServerRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.CLEAR_BASKET_PRODUCT_REQUEST);
    const { data } = request;
    yield call(clearBasketOnServer, data);
  }
}

function* preCheckOutBasketRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.PRECHECKOUT_BASKET_PRODUCT_REQUEST);
    const { data } = request;
    const result = yield call(preCheckOutBasket, data);
    if (result && !result.isError) {
      yield put({ type: AppFlowActions.LOAD_BASKET_COMPLETE, data: result });
    } else {
      // toastrError('Pre Check Out');
    }
  }
}

function* preCheckOutBasketSubscriptionRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.PRECHECKOUT_BASKET_SUBSCRIPTION_PRODUCT_REQUEST);
    const { data } = request;
    const result = yield call(preCheckOutBasket, data);
    if (result && !result.isError) {
      yield put({ type: AppFlowActions.LOAD_BASKET_SUBSCRIPTION_COMPLETE, data: result });
    } else {
      // toastrError('Pre Check Out');
    }
  }
}

function createBasketGuest(isBoutique = false) {
  const options = {
    url: GUEST_CREATE_BASKET_URL,
    method: 'POST',
    body: {
      subtotal: 0,
      total: 0,
      is_boutique: isBoutique,
    },
  };
  return fetchClient(options);
}

function createBasketSubscription(user) {
  const options = {
    url: GUEST_CREATE_BASKET_URL,
    method: 'POST',
    body: {
      user,
      is_subscription: true,
    },
  };
  return fetchClient(options);
}

function* createBasketGuestRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.CREATE_BASKET_GUEST_REQUEST);
    const { data } = request;
    const result = yield call(createBasketGuest);
    yield put({ type: AppFlowActions.LOAD_BASKET_COMPLETE, data: result });
    if (result && !result.isError && data) {
      const { id } = result;
      data.idCart = id;
      yield put({ type: AppFlowActions.ADD_BASKET_REQUEST, data });
    }
  }
}

function* createBasketSubscriptionRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.CREATE_BASKET_SUBSCRIPTION_REQUEST);
    yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: true });
    const { data } = request;
    const result = yield call(createBasketSubscription, data.userId);
    if (result && !result.isError && data) {
      yield put({ type: AppFlowActions.LOAD_BASKET_SUBSCRIPTION_COMPLETE, data: result });
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    } else {
      toastrError('Add To Cart');
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    }
  }
}

function* addBasketSubscriptionRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.ADD_BASKET_SUBSCRIPTION_REQUEST);
    const { data } = request;
    const result = yield call(addBasketToServer, data);
    if (result && !result.isError) {
      // tracking gtm
      const { item } = data;
      const { items } = result;
      trackGTMAddToCart(item, items?.length > 0 ? items[0].id : undefined);

      yield put({ type: AppFlowActions.LOAD_BASKET_SUBSCRIPTION_COMPLETE, data: result });
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    } else {
      toastrError('Add To Cart');
      yield put({ type: AppFlowActions.LOADING_COMPLTE, loading: false });
    }
  }
}

function* createBasketGuestArrayRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.CREATE_BASKET_GUEST_REQUEST_ARRAY);
    const { data } = request;
    const isBoutique = data && data.length > 0 && data[0].is_boutique;
    const result = yield call(createBasketGuest, isBoutique);
    yield put({ type: AppFlowActions.LOAD_BASKET_COMPLETE, data: result });
    if (result && !result.isError) {
      const { id } = result;
      _.forEach(data, (d) => { d.idCart = id; });
      yield put({ type: AppFlowActions.ADD_BASKET_REQUEST_ARRAY, data });
    } else {
      toastrError('Create Card');
    }
  }
}

export default function* basketFlow() {
  yield fork(addBasketRequest);
  yield fork(addBasketArrayRequest);
  yield fork(deleteBasketRequest);
  yield fork(updateBasketRequest);
  yield fork(createBasketGuestRequest);
  yield fork(createBasketGuestArrayRequest);
  yield fork(updateBasketArrayRequest);
  yield fork(clearBasketOnServerRequest);
  yield fork(preCheckOutBasketRequest);
  yield fork(createBasketSubscriptionRequest);
  yield fork(addBasketSubscriptionRequest);
  yield fork(preCheckOutBasketSubscriptionRequest);
  yield fork(addGiftBasketRequest);
  yield fork(reorderBasketRequest);
}
