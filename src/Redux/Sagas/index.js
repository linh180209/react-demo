import { fork } from 'redux-saga/effects';
import loginFlow from './login';
import getAllFlow from './getall';
import basketFlow from './basket';
import cmsFlow from './cms';
import BookWorkshopFlow from './bookWorkshop';

export default function* root() {
  yield fork(loginFlow);
  yield fork(getAllFlow);
  yield fork(basketFlow);
  yield fork(cmsFlow);
  yield fork(BookWorkshopFlow);
}
