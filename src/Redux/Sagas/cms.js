import {
  put, call, take, fork, select,
} from 'redux-saga/effects';
import _ from 'lodash';
import AppFlowActions from '../../constants';
import fetchClient from '../Helpers/fetch-client';
import { fetchCMSHomepage } from '../Helpers';
import auth from '../Helpers/auth';

/**
 * Log in saga
 */

export function* cmsCommonRequest() {
  const INFINITE = true;
  while (INFINITE) {
    yield take(AppFlowActions.GET_CMS_COMMON_REQUEST);
    const { cms } = yield select();
    const urlCommon = _.find(cms, x => x.title === 'Common');
    if (!urlCommon) {
      const result = yield call(fetchCMSHomepage, 'common');
      yield put({ type: AppFlowActions.ADD_CMS_COMPLETE, data: result });
    }
  }
}


export default function* cmsFlow() {
  yield fork(cmsCommonRequest);
}
