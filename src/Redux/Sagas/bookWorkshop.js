import {
  put, call, take, fork,
} from 'redux-saga/effects';

import JsonRpcClient from 'react-jsonrpc-client';
import AppFlowActions from '../../constants';
import { getCompanySimplyBook, SIMPLY_BOOK_URL } from '../../config';

function getEventList() {
  try {
    const token = localStorage.getItem('tokenSimplyBook');
    const api = new JsonRpcClient({
      endpoint: SIMPLY_BOOK_URL,
      headers: {
        'X-Company-Login': getCompanySimplyBook(),
        'X-Token': token,
      },
    });
    return api.request(
      'getEventList',
    );
  } catch (error) {
    return null;
  }
}

export function* getEventListSimplyBook() {
  const INFINITE = true;
  while (INFINITE) {
    yield take(AppFlowActions.GET_EVENT_LIST_SIMPLY_BOOK);
    const result = yield call(getEventList);
    if (result) {
      yield put({ type: AppFlowActions.GET_EVENT_LIST_SIMPLY_BOOK_SUCCESS, data: result });
    }
  }
}


export default function* bookWorkshop() {
  yield fork(getEventListSimplyBook);
}
