import {
  put, call, take, fork,
} from 'redux-saga/effects';

import { LOGIN_URL, UPDATE_USER_URL } from '../../config';
import auth from '../Helpers/auth';
import AppFlowActions from '../../constants';

// import login from '../reducers/login';
import fetchClient from '../Helpers/fetch-client';

function fetchUser(data) {
  const { userName, passWord } = data;
  try {
    const options = {
      url: LOGIN_URL,
      method: 'POST',
      body: { userName, passWord },
    };

    return fetchClient(options);
  } catch (error) {
    return null;
  }
}

/**
 * Log in saga
 */

export function* loginRequest() {
  const INFINITE = true;
  while (INFINITE) {
    const request = yield take(AppFlowActions.LOGIN_REQUEST);
    // yield put({ type: AppFlowActions.LOADING_COMPLTE, isLoading: true });

    const { data } = request;

    const result = yield call(fetchUser, { ...data });
    if (result) {
      if (result.isSuccess) {
        auth.login(result);
      }
      yield put({ type: AppFlowActions.LOGIN_COMPLETE, data: result });
      if (result.isSuccess === true) {
        yield put({ type: AppFlowActions.GET_ALL_DATA_REQUEST });
      }

      // yield put({ type: AppFlowActions.LOADING_COMPLTE, isLoading: false });
    }
  }
}


export default function* loginFlow() {
  yield fork(loginRequest);
}
