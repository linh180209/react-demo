// eslint-disable-next-line import/no-cycle
import { fetchClientPreventDulicate } from './fetch-client';

class MemoPromise {
  constructor(getPromise) {
    this.cache = {};

    this.getPromise = getPromise;
  }

  request(uniqueKey, body) {
    if (!uniqueKey) {
      return Promise.reject(new Error('Unique key not passed'));
    }

    if (this.cache[uniqueKey]) {
      return this.cache[uniqueKey];
    }

    const promise = this.getPromise(body);

    this.cache[uniqueKey] = promise

      .then((res) => {
        delete this.cache[uniqueKey];

        return res;
      })

      .catch((err) => {
        delete this.cache[uniqueKey];

        throw err;
      });

    return this.cache[uniqueKey];
  }
}

const memoPromise = new MemoPromise(fetchClientPreventDulicate);

export default memoPromise;
