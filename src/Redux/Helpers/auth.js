import moment from 'moment';
import { isCheckNull } from '.';

const { localStorage } = global.window;

const auth = {
  login(data) {
    const { token, refresh_token: refreshToken } = data;
    localStorage.token = token;
    localStorage.refreshToken = refreshToken;
    localStorage.login = JSON.stringify(data);
  },

  setLanguage(language) {
    localStorage.language = language;
  },

  getLanguage() {
    return isCheckNull(localStorage.language) ? 'en' : localStorage.language;
  },

  setCountry(data) {
    localStorage.country = data;
  },

  setUrlAvatar(urlAvatar) {
    localStorage.urlAvatar = urlAvatar;
  },

  setOutComeId(id) {
    localStorage.outComeId = id;
  },

  setLimitProduct(limit) {
    localStorage.limitProduct = limit;
  },

  getLimitProduct() {
    return localStorage.limitProduct;
  },

  setPositionScrollProduct(scroll) {
    localStorage.positionScrollProduct = scroll;
  },

  getPositionScrollProduct() {
    return localStorage.positionScrollProduct;
  },

  getAskRegion() {
    if (localStorage.showAskRegion && localStorage.showAskRegion.toString().length > 11) {
      const time = parseInt(localStorage.showAskRegion, 10);
      if (moment().valueOf() - time > 1000 * 60 * 60) {
        localStorage.showAskRegion = 'true';
        return true;
      }
    }

    return localStorage.showAskRegion ? (localStorage.showAskRegion === 'true' || localStorage.showAskRegion === true || localStorage.showAskRegion === 'undefined') : true;
  },

  setAskRegion(region) {
    localStorage.showAskRegion = region;
  },

  getDeliveryCountry() {
    if (localStorage.showDeliveryCountry && localStorage.showDeliveryCountry.toString().length > 11) {
      const time = parseInt(localStorage.showDeliveryCountry, 10);
      if (moment().valueOf() - time > 1000 * 60 * 60) {
        localStorage.showDeliveryCountry = 'true';
        return true;
      }
    }

    return localStorage.showDeliveryCountry ? (localStorage.showDeliveryCountry === 'true' || localStorage.showDeliveryCountry === true || localStorage.showDeliveryCountry === 'undefined') : true;
  },

  setIsAddScent(flag) {
    localStorage.isAddScent = flag;
  },

  getIsAddScent() {
    return localStorage.isAddScent === 'true';
  },

  setDeliveryCountry(time) {
    localStorage.showDeliveryCountry = time;
  },

  setAskCookies() {
    localStorage.askCookies = 'true';
  },

  setPopupLandingPage() {
    localStorage.popupLandingPage = 'true';
  },

  setBasketId(id) {
    localStorage.basketId = id;
  },

  setEmail(email) {
    localStorage.email = email;
  },

  saveDataCheckOut(data) {
    localStorage.dataCheckOut = data;
  },

  getDataCheckOut() {
    return localStorage.dataCheckOut;
  },

  setDiscountCode(code) {
    localStorage.discountCode = code;
  },

  getDiscountCode() {
    return localStorage.discountCode || '';
  },

  getEmail() {
    return localStorage.email;
  },

  setCountryHeader(country) {
    localStorage.countryHeader = country;
  },

  getCountryHeader() {
    return localStorage.countryHeader;
  },

  getCountryName() {
    const country = localStorage.country || JSON.stringify({
      code: 'us', currency: { code: 'usd', name: 'US Dollar', symbol: '$' }, language: { code: 'en' }, name: 'United States',
    });
    try {
      return JSON.parse(country).name;
    } catch (error) {
      return 'United States';
    }
  },

  getCountry() {
    const country = localStorage.country || JSON.stringify({
      code: 'us', currency: { code: 'usd', name: 'US Dollar', symbol: '$' }, language: { code: 'en' }, name: 'United States',
    });
    try {
      return JSON.parse(country).code;
    } catch (error) {
      return 'us';
    }
  },

  getRealCountry() {
    return localStorage.country !== 'undefined' ? localStorage.country : undefined;
  },

  getUrlAvatar() {
    return localStorage.urlAvatar;
  },

  getCurrency() {
    const currency = localStorage.country || JSON.stringify({
      code: 'us', currency: { code: 'usd', name: 'US Dollar', symbol: '$' }, language: { code: 'en' }, name: 'United States',
    });
    try {
      return JSON.parse(currency).currency.symbol;
    } catch (error) {
      return '$';
    }
  },

  getNameCurrency() {
    const currency = localStorage.country || JSON.stringify({
      code: 'us', currency: { code: 'usd', name: 'US Dollar', symbol: '$' }, language: { code: 'en' }, name: 'United States',
    });
    try {
      return JSON.parse(currency).currency.code;
    } catch (error) {
      return 'usd';
    }
  },

  getLeftRightCurrency() {
    const country = localStorage.country || JSON.stringify({
      code: 'us',
      currency: {
        code: 'usd', name: 'US Dollar', symbol: '$', is_left: true,
      },
      language: { code: 'en' },
      name: 'United States',
    });
    return JSON.parse(country).currency.is_left;
  },

  getBasketId() {
    const data = localStorage.getItem('basketId');
    return data === 'undefined' ? undefined : data;
  },

  getAskCookies() {
    return localStorage.askCookies === 'true';
  },

  getPopupLandingPage() {
    return localStorage.popupLandingPage === 'true';
  },

  getOutComeId() {
    return localStorage.outComeId === 'undefined' ? undefined : localStorage.outComeId;
  },

  token() {
    return localStorage.token;
  },

  getLogin() {
    return localStorage.login ? JSON.parse(localStorage.login) : undefined;
  },

  tokenRefresh() {
    return localStorage.refreshToken;
  },

  logout() {
    localStorage.clear();
  },

  logoutNotOutCome() {
    const id = localStorage.outComeId;
    const ask = localStorage.askCookies;
    const bkId = localStorage.basketId;
    const cou = localStorage.country;
    const lang = localStorage.language;
    const dCheckOut = localStorage.dataCheckOut;
    const popup = localStorage.popupLandingPage;
    const url = localStorage.urlAvatar;
    const askRegion = localStorage.showAskRegion;
    const showDLCountry = localStorage.showDeliveryCountry;
    const cHeader = localStorage.countryHeader;
    const disCode = localStorage.discountCode;
    this.logout();
    localStorage.askCookies = ask;
    localStorage.basketId = bkId;
    localStorage.language = lang;
    localStorage.dataCheckOut = dCheckOut;
    localStorage.popupLandingPage = popup;
    localStorage.urlAvatar = url;
    localStorage.showAskRegion = askRegion;
    localStorage.showDeliveryCountry = showDLCountry;
    localStorage.countryHeader = cHeader;
    localStorage.discountCode = disCode;
    if (cou) {
      localStorage.country = cou;
    }
    if (id) {
      localStorage.outComeId = id;
    }
  },
};

export default auth;
