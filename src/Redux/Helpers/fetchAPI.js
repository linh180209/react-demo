import { GET_SCENT_DETAIL, GET_SCENT_LIB, GET_SCENT_LIB_GLOSSAIRE } from '../../config';
import fetchClient from './fetch-client';

export const fetchScentLib = () => {
  const options = {
    url: GET_SCENT_LIB,
    method: 'GET',
  };
  return fetchClient(options);
};

export const fetchGlossaryScentLib = () => {
  const options = {
    url: GET_SCENT_LIB_GLOSSAIRE,
    method: 'GET',
  };
  return fetchClient(options);
}

export const fetchScentDetail = (id) => {
  const url = GET_SCENT_DETAIL.replace('{id}', id);
  const option = {
    url,
    method: 'GET',
  }
  return fetchClient(option)
}

export const test = () => {};
