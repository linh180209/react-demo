import toastr from 'toastr';

export const toastrInfo = (title, message) => {
  toastr.info(title, message, {
    progressBar: false,
    positionClass: 'toast-bottom-left',
  });
};

export const toastrSuccess = (title) => {
  toastr.success(title, 'Success', {
    progressBar: false,
    positionClass: 'toast-bottom-left',
  });
};

export const toastrError = (title) => {
  toastr.error(title, 'Error', {
    progressBar: false,
    positionClass: 'toast-top-right',
  });
};
