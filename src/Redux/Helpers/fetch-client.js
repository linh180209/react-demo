import axios from 'axios';
import * as Sentry from '@sentry/react';
import _ from 'lodash';
import { toastrError } from './notification';
import { getCountry } from './index';
import memoPromise from './memoPromise';
import auth from './auth';


require('es6-promise').polyfill();
require('isomorphic-fetch');

async function fetchRequest(myRequest) {
  try {
    const response = await fetch(myRequest);
    if (response.status === 200 || response.status === 201) {
      const body = await response.json();
      return body;
    } if (response.status === 409) {
      return {
        isError: true,
        message: 'The email already exists',
      };
    } if (response.status === 204) {
      return {
        isError: false,
      };
    }
    const body = await response.json();
    return {
      isError: true,
      message: '',
      email: body ? body.email : '',
    };
  } catch (error) {
    Sentry.captureException(error);
    return { isError: true, message: error.message };
  }
}

async function fetchRequestAxios(myRequest) {
  try {
    const response = await axios(myRequest);
    if (response.status === 200 || response.status === 201 || response.status === 202) {
      const body = await response.data;
      return body;
    }
    const body = await response.data;
    return {
      isError: true,
      message: '',
      email: body ? body.email : '',
    };
  } catch (error) {
    Sentry.captureException(error);
    if (error.message === 'Network Error') {
      toastrError('You seem to be offline, please check your connection');
      return null;
    }
    const { data } = error.response;
    if (data.status === 409) {
      return {
        isError: true,
        message: 'The email already exists',
      };
    }
    if (data.status === 204) {
      return {
        isError: false,
      };
    }
    if (data.status === 404) {
      return {
        isError: true,
        status: 404,
        message: data.message,
      };
    }
    return { isError: true, message: data.message };
  }
}

export async function fetchDataImage(url) {
  const response = await axios.get(url, {
    responseType: 'arraybuffer',
  });
  const blob = new Blob([response.data], { type: 'application/pdf' });
  const image = `${window.URL.createObjectURL(blob)}#view=FitW`;
  return image;
}

export function fetchClientAddLanguage({ url, method, body }, language) {
  const defaultHeaders = {
    'Content-Type': 'application/json',
    'accept-country': getCountry(),
    language,
  };
  const options = {
    method,
    url,
    headers: defaultHeaders,
    data: body,
  };
  return fetchRequestAxios(options);
}

function fetchClient({ url, method, body }, isToken = false, token) {
  const defaultHeaders = {
    'Content-Type': 'application/json',
    'accept-country': getCountry(),
    Authorization: isToken ? (token || auth.token()) : undefined,
  };
  const options = {
    method,
    url,
    headers: defaultHeaders,
    data: body,
  };
  return memoPromise.request(url, options);
}

export function fetchClientAlwaysUS({ url, method, body }, isToken = false, token) {
  const defaultHeaders = {
    'Content-Type': 'application/json',
    'accept-country': 'us',
    Authorization: isToken ? (token || auth.token()) : undefined,
  };
  const options = {
    method,
    url,
    headers: defaultHeaders,
    data: body,
  };
  return fetchRequestAxios(options);
}

export function fetchClientPreventDulicate(options) {
  return fetchRequestAxios(options);
}

export function fetchClientFormData({ url, method, body }, isToken = false) {
  const defaultHeaders = {
    Authorization: isToken ? auth.token() : undefined,
    'accept-country': getCountry(),
  };
  const options = {
    method,
    body,
    headers: defaultHeaders,
  };
  const request = new Request(url, options);
  return fetchRequest(request);
}

export default fetchClient;
