import React from 'react';
import _, { last } from 'lodash';
import axios from 'axios';
// import ReactGA from 'react-ga';
import Cookies from 'js-cookie';
import TagManager from 'react-gtm-module';
import JsonRpcClient from 'react-jsonrpc-client';
import moment from 'moment';
import {
  PUT_OUTCOME_URL, POINT_REDEEM_URL, GET_ALL_ANSWERS,
  GET_ALL_QUESTIONS, GET_PAGE_CMS_URL,
  GET_MOOD_URL, GET_PAGE_CMS_HOME_PAGE_URL, GET_PAGE_ALL_CMS_URL,
  GET_COUNTRY_URL, IS_DEV, KEY_GTM, GET_PAGE_DRAFT_CMS_HOME_PAGE_URL, GET_PRODUCT_FOR_CART, GET_PRICES_PRODUCTS,
  SIMPLY_BOOK_URL, GET_SIMPLYBOOK_TOKEN_ADMIN, GET_ALL_SCENT_FAMILY_V4, getCompanySimplyBook, getKeyApiSimplyBook,
  GET_ALL_ANSWERS_FERRARI, GET_ALL_QUESTIONS_FERRARI,
} from '../../config';

import { countriesObject } from '../../constants/countries';

import fetchClient, { fetchClientAlwaysUS } from './fetch-client';
import auth from './auth';
import { toastrError } from './notification';

export const getCountry = () => {
  const array = window.location.pathname.split('/');
  if (array.length < 2 || _.isEmpty(array[1]) || array[1].length !== 2) {
    return auth.getCountry();
  }
  return array[1];
};

export const getCountryCodeRealName = (countryCode = "") => {
  return countriesObject?.[countryCode?.toUpperCase()] ?? "";
}

export const getCountryRealNameFromHeader = () => {
  const countryCode = auth.getCountryHeader();
  return getCountryCodeRealName(countryCode);
}

export const getCodeUrl = () => {
  const array = window.location.pathname.split('/');
  if (array && array.length > 1 && array[1].length === 2) {
    return array[1].toLowerCase();
  }
  return '';
};

const addLinkSearchUTM = (link) => {
  return link;
  // const utmSource = getUTM('utm_source');
  // const utmMedium = getUTM('utm_medium');
  // const utmCampaign = getUTM('utm_campaign');
  // if(!utmSource && !utmMedium && !utmCampaign) {
  //   return link;
  // }
  // if(link.includes('utm_source') || link.includes('utm_medium') || link.includes('utm_campaign') ) {
  //   return link;
  // }
  // let newLink = link;
  // if(!link.includes('?')) {
  //   newLink = `${link}?`;
  // } else {
  //   newLink = `${link}&`;
  // }
  // if(utmSource) {
  //   newLink = `${newLink}utm_source=${utmSource}&`;
  // }
  // if(utmMedium) {
  //   newLink = `${newLink}utm_medium=${utmMedium}&`;
  // }
  // if(utmCampaign) {
  //   newLink = `${newLink}utm_campaign=${utmCampaign}`;
  // }
  // if(newLink.slice(newLink.length - 1) === '&') {
  //   newLink = newLink.slice(0, newLink.length - 1);
  // }
  // return newLink;
}

export const getSearchPathName = (url) => {
  const array = url.split('?');
  console.log('array', array);
  if(array.length > 1) {
    return {pathname: array[0], search: `?${array[1]}`}
  }
  return {pathname: array[0], search: undefined}
}

export const generateUrlWeb = (link, isNotCheckCountry) => {
  const array = link ? link.split('/') : [];
  if (!isNotCheckCountry && array.length > 1 && array[1].length === 2) {
    return addLinkSearchUTM(link);
  }
  const symbolUrl = getCodeUrl();
  if (!isNotCheckCountry && !_.isEmpty(symbolUrl)) {
    return addLinkSearchUTM(`/${symbolUrl}${link}`);
  }
  return addLinkSearchUTM(link);
  // return `/${getCountry()}${link}`;
};

export const gotoUrl = (link, history, isReplaceLink = false) => {
  if (link.includes('http')) {
    if (isReplaceLink) {
      window.location.href = link;
      return;
    }
    window.open(link);
    return;
  }
  const url = generateUrlWeb(link);
  history.push(url);
};

export const validateEmail = (email) => {
  const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};

export const convertHeaderToSize = (headerSize) => {
  switch (headerSize) {
    case 'h4':
      return '1rem';
    case 'h3':
      return '1.17rem';
    case 'h2':
      return '1.5rem';
    case 'h1':
      return '2rem';
    default:
      return '1rem';
  }
};

export const convertStringToPriceBasket = (data) => {
  const { items, shipping } = data;
  data.shipping = parseFloat(shipping);
  _.forEach(items, (i) => {
    i.price = parseFloat(i.price);
    i.total = parseFloat(i.total);
  });
  data.total = parseFloat(data.total);
  // const subtotal = _.reduce(items, (sum, d) => sum + parseFloat(d.total, 10), 0);
  // data.subtotal = subtotal;
  // data.total = subtotal + parseFloat(data.shipping, 10);
};

export const putUpdateOutCome = (idOutCome, likeProducts) => {
  if (isCheckNull(idOutCome)) {
    return;
  }
  const options = {
    url: PUT_OUTCOME_URL.replace('{id}', idOutCome),
    method: 'PUT',
    body: {
      liked_products: likeProducts,
    },
  };
  fetchClient(options, true);
};

export const calculatePromotionRule = (promoRule, total, items) => {
  if (!promoRule) {
    return total;
  }
  let totalTemp = total;
  _.forEach(promoRule, (d) => {
    const { number, product_type: prodType } = d;
    const itemType = _.filter(items, x => x.item.product.type === prodType);
    const totalItem = _.reduce(itemType, (sum, x) => sum + x.quantity, 0);
    if (itemType.length > 0) {
      const totalDiscount = (parseInt(totalItem / number, 10)) * itemType[0].price;
      totalTemp -= totalDiscount;
    }
  });
  return totalTemp;
};

export const calculatePromotion = (total, promotion, items) => {
  if (!promotion) {
    return 0;
  }
  const { type, value, gift } = promotion;
  if (gift) {
    const { quantity } = gift;
    const itemPromos = _.filter(items, x => x.item.product.type.toLowerCase() === gift.type.toLowerCase());
    if (itemPromos.length === 0) {
      return 0;
    }
    const { price } = itemPromos[0];
    const sQuantitys = _.reduce(itemPromos, (sum, x) => sum + x.quantity, 0);
    return quantity < sQuantitys ? quantity * price : sQuantitys * price;
  }
  switch (type) {
    case 'monetary':
      return value;
    case 'percentage':
      return parseFloat(total * value / 100).toFixed(2);
    default:
      return 0;
  }
};

export const mathRound = value => Math.round(value * 1000) / 1000;

export const zeroPad = (num, places = 5) => {
  if (num || num === 0) {
    return num.toString().padStart(places, '0');
  }
  return '';
};


export const getUTM = (content) => {
  const currentSearchQuery = window.location.search;
  const urlParams = new URLSearchParams(currentSearchQuery);
  //get utm_source, utm_medium, and utm_campaign from url
  const fromURL = urlParams?.get(content);
  if (fromURL) {
    return fromURL;
  }
  //check inside cookie
  let fromCookies = Cookies.get(content);
  if (!fromCookies && content === 'utm_source') {
    return 'direct';
  }
  return fromCookies;
}

export const setUTMCookies = () => {
  const currentSearchQuery = window.location.search;
  const urlParams = new URLSearchParams(currentSearchQuery);
  const utm_source = urlParams?.get('utm_source');
  const utm_medium = urlParams?.get('utm_medium');
  const utm_campaign = urlParams?.get('utm_campaign');
  if (utm_source) {
    Cookies.set('utm_source', utm_source, { expires: 7 });
  }
  if (utm_medium) {
    Cookies.set('utm_medium', utm_medium, { expires: 7 });
  }
  if (utm_campaign) {
    Cookies.set('utm_campaign', utm_campaign, { expires: 7 });
  }
}

export const removeAllUTMCookies = () => {
  Cookies.remove('utm_source');
  Cookies.remove('utm_medium');
  Cookies.remove('utm_campaign');
}

export const postPointRedeem = (ref) => {
  const options = {
    url: POINT_REDEEM_URL.replace('{ref}', ref),
    method: 'POST',
    body: {
      action: 'ref_reg',
    },
  };
  fetchClient(options);
};

export const scrollTop = () => {
  window.scrollTo({ top: 0 });
};

export const getUrlScentDesigner = (login) => {
  if (login && login.token && login.user.outcome) {
    return `/quiz/outcome/${login.user.outcome}`;
  }
  return '/quiz';
};

export const onClickLink = (url, login, history) => {
  if (url.includes('https:')) {
    window.open(url);
    return;
  }
  if (url === '/quiz' || url === '/letsguideyou') {
    const link = getUrlScentDesigner(login);
    history.push(generateUrlWeb(link));
    return;
  }
  history.push(generateUrlWeb(url));
};

export const getImageDisplay = (data) => {
  const {
    combo, item,
  } = data;
  const { product, images } = item;
  const { type, images: imageProduct } = product;
  let imgDisplay;
  if (type.name === 'Kit' || type.name === 'Creation') {
    imgDisplay = imageProduct && imageProduct.length > 0 ? imageProduct[0].image : '';
  } else if (type.name === 'Perfume') {
    const itemBottle = combo ? _.find(combo, x => x.product.type === 'Bottle') : undefined;
    const imageBottle = itemBottle ? _.find(itemBottle.product.images, x => x.type === null) : undefined;
    imgDisplay = imageBottle ? imageBottle.image : '';
  } else if (type.name === 'Scent') {
    imgDisplay = images && images.length > 0 ? images[0] : '';
  } else if (type.name === 'Wax_Perfume') {
    const image = _.find(imageProduct, x => x.type === null);
    imgDisplay = image ? image.image : '';
  } else if (type.name === 'Elixir') {
    const image = _.find(imageProduct, x => x.type === 'unisex');
    imgDisplay = image ? image.image : '';
  } else if (type.name === 'Gift') {
    const image = _.find(imageProduct, x => x.type === null);
    imgDisplay = image ? image.image : '';
  }
  return imgDisplay;
};

export const getUrlGotoProduct = (data) => {
  const {
    combo, item,
  } = data;
  const { product } = item;
  const { type } = product;
  let url;
  let isPerfume;
  if (type.name === 'Kit') {
    url = `/product/kit/${product.id}`;
  } else if (type.name === 'Perfume') {
    const items = combo ? _.filter(combo, x => x.product.type === 'Scent') : [];
    if (items.length > 1) {
      url = `/products/${items[0].product.id}/${items[1].product.id}`;
      isPerfume = true;
    }
  } else if (type.name === 'Scent') {
    url = `/product/Scent/${product.id}`;
  } else if (type.name === 'Wax_Perfume') {
    url = `/product/wax/${product.id}`;
  } else if (type.name === 'Elixir') {
    url = `/product/elixir/${product.id}`;
    isPerfume = true;
  } else if (type.name === 'bundle') {
    url = `/product/bundle/${product.id}`;
  } else if (type.name === 'Creation') {
    url = `/product/creation/${product.id}`;
  } else if (type.name === 'hand_sanitizer') {
    url = `/product/hand-sanitizer/${product.id}/${item.id}`;
  } else if (type.name === 'home_scents') {
    url = `/product/home-scents/${product.id}/${item.id}`;
  } else if (type.name === 'discovery_box') {
    url = `/product/discovery_box/${product.id}`;
  } else if (type.name === 'mask_sanitizer') {
    url = `/product/mask_sanitizer/${product.id}`;
  } else if (type.name === 'toilet_dropper') {
    url = `/product/toilet_dropper/${product.id}`;
  } else if (type.name === 'dual_candles') {
    url = `/product/dual-candles/${product.id}`;
  } else if (type.name === 'single_candle') {
    url = `/product/single-candle/${product.id}`;
  } else if (type.name === 'holder') {
    url = `/product/holder/${product.id}`;
  } else if (type.name === 'gift_bundle') {
    url = `/product/gift-bundle/${product.id}`;
  } else if (type.name === 'dual_crayons') {
    url = `/product/dual_crayons/${product.id}`;
  } else if (type.name === 'bundle_creation') {
    url = `/product/bundle_creation/${product.id}`;
  } else if (type.name === 'reed_diffuser') {
    url = `/product/reed_diffuser/${product.id}`;
  } else if (type.name === 'perfume_diy') {
    url = `/product/perfume_diy/${product.id}`;
  } else if (type.name === 'car_diffuser') {
    url = `/product/car_diffuser/${product.id}`;
  } else if (type.name === 'mini_candles') {
    url = `/product/mini_candles/${product.id}`;
  } else if (type.name === 'oil_burner') {
    url = `/product/oil_burner/${product.id}`;
  }
  return { url, isPerfume };
};

export const getUrlGotoProductOderPage = (data) => {
  const {
    combo, item,
  } = data;
  const { product } = item;
  const { type } = product;
  let url;
  let isPerfume;
  if (type.name === 'Kit') {
    url = `/product/kit/${product.id}`;
  } else if (type.name === 'Perfume') {
    url = `/product/Perfume/${product.id}`;
  } else if (type.name === 'Wax_Perfume') {
    url = `/product/wax/${product.id}`;
  } else if (type.name === 'Elixir') {
    url = `/product/elixir/${product.id}`;
  } else if (type.name === 'bundle') {
    url = `/product/bundle/${product.id}`;
  } else if (type.name === 'Scent') {
    url = `/product/Scent/${product.id}`;
  } else if (type.name === 'Creation') {
    url = `/product/creation/${product.id}`;
  } else if (type.name === 'discovery_box') {
    url = `/product/discovery_box/${product.id}`;
  } else if (type.name === 'hand_sanitizer') {
    url = `/product/hand-sanitizer/${product.id}/${item.id}`;
  } else if (type.name === 'toilet_dropper') {
    url = `/product/toilet_dropper/${product.id}`;
  } else if (type.name === 'home_scents') {
    url = `/product/home-scents/${product.id}`;
  } else if (type.name === 'mask_sanitizer') {
    url = `/product/mask_sanitizer/${product.id}`;
  } else if (type.name === 'dual_candles') {
    url = `/product/dual-candles/${product.id}`;
  } else if (type.name === 'single_candle') {
    url = `/product/single-candle/${product.id}`;
  } else if (type.name === 'holder') {
    url = `/product/holder/${product.id}`;
  } else if (type.name === 'gift_bundle') {
    url = `/product/gift-bundle/${product.id}`;
  } else if (type.name === 'dual_crayons') {
    url = `/product/dual_crayons/${product.id}`;
  } else if (type.name === 'bundle_creation') {
    url = `/product/bundle_creation/${product.id}`;
  } else if (type.name === 'reed_diffuser') {
    url = `/product/reed_diffuser/${product.id}`;
  } else if (type.name === 'perfume_diy') {
    url = `/product/perfume_diy/${product.id}`;
  } else if (type.name === 'car_diffuser') {
    url = `/product/car_diffuser/${product.id}`;
  } else if (type.name === 'mini_candles') {
    url = `/product/mini_candles/${product.id}`;
  } else if (type.name === 'oil_burner') {
    url = `/product/oil_burner/${product.id}`;
  }
  return { url, isPerfume };
};

export const onClickProduct = (product) => {
  const { type } = product;
  let url;
  if (type.name === 'Wax_Perfume') {
    url = `/product/wax/${product.product}`;
  } else if (type.name === 'Elixir') {
    url = `/product/elixir/${product.product}`;
  } else if (type.name === 'Kit') {
    url = `/product/kit/${product.product}`;
  } else if (type.name === 'bundle') {
    url = `/product/bundle/${product.product}`;
  } else if (type.name === 'Scent') {
    url = `/product/Scent/${product.product}`;
  } else if (type.name === 'discovery_box') {
    url = '/product/discovery_box/2106';
  } else if (type.name === 'toilet_dropper') {
    url = '/product/toilet_dropper/3791';
  } else if (type.name === 'mask_sanitizer') {
    url = '/product/mask_sanitizer/3790';
  } else if (type.name === 'hand_sanitizer') {
    url = `/product/hand-sanitizer/${product.product}/${product.id}`;
  } else if (type.name === 'home_scents') {
    url = `/product/home-scents/${product.product}/${product.id}`;
  } else if (type.name === 'Perfume') {
    url = `/product/Perfume/${product.product}`;
  } else if (type.name === 'Creation') {
    url = `/product/creation/${product.product}`;
  } else if (type.name === 'dual_candles') {
    url = `/product/dual-candles/${product.product}`;
  } else if (type.name === 'single_candle') {
    url = `/product/single-candle/${product.product}`;
  } else if (type.name === 'holder') {
    url = `/product/holder/${product.product}`;
  } else if (type.name === 'gift_bundle') {
    url = `/product/gift-bundle/${product.product}`;
  } else if (type.name === 'dual_crayons') {
    url = `/product/dual_crayons/${product.product}`;
  } else if (type.name === 'bundle_creation') {
    url = `/product/bundle_creation/${product.product}`;
  } else if (type.name === 'reed_diffuser') {
    url = `/product/reed_diffuser/${product.product}`;
  } else if (type.name === 'perfume_diy') {
    url = `/product/perfume_diy/${product.product}`;
  } else if (type.name === 'car_diffuser') {
    url = `/product/car_diffuser/${product.product}`;
  } else if (type.name === 'mini_candles') {
    url = `/product/mini_candles/${product.product}`;
  } else if (type.name === 'oil_burner') {
    url = `/product/oil_burner/${product.product}`;
  }
  return url;
};

export const getUrlProductReview = (id, type, combo, item) => {
  let url;
  let isPerfume;
  if (type.name === 'Kit') {
    url = `/product/kit/${id}`;
  } else if (type.name === 'Perfume') {
    if (combo.length > 1) {
      url = `/products/${combo[0]}/${combo[1]}`;
      isPerfume = true;
    }
  } else if (type.name === 'Scent') {
    url = `/product/Scent/${id}`;
  } else if (type.name === 'Wax_Perfume') {
    url = `/product/wax/${id}`;
  } else if (type.name === 'Elixir') {
    url = `/product/elixir/${id}`;
    isPerfume = true;
  } else if (type.name === 'bundle') {
    url = `/product/bundle/${id}`;
  } else if (type.name === 'Creation') {
    url = `/product/creation/${id}`;
  } else if (type.name === 'hand_sanitizer') {
    url = `/product/hand-sanitizer/${id}/${item}`;
  } else if (type.name === 'home_scents') {
    url = `/product/home-scents/${id}/${item}`;
  } else if (type.name === 'dual_candles') {
    url = `/product/dual-candles/${id}`;
  } else if (type.name === 'single_candle') {
    url = `/product/single-candle/${id}`;
  } else if (type.name === 'holder') {
    url = `/product/holder/${id}`;
  } else if (type.name === 'gift_bundle') {
    url = `/product/gift-bundle/${id}`;
  } else if (type.name === 'dual_crayons') {
    url = `/product/dual_crayons/${id}`;
  } else if (type.name === 'bundle_creation') {
    url = `/product/bundle_creation/${id}`;
  } else if (type.name === 'reed_diffuser') {
    url = `/product/reed_diffuser/${id}`;
  } else if (type.name === 'perfume_diy') {
    url = `/product/perfume_diy/${id}`;
  } else if (type.name === 'car_diffuser') {
    url = `/product/car_diffuser/${id}`;
  } else if (type.name === 'mini_candles') {
    url = `/product/mini_candles/${id}`;
  } else if (type.name === 'oil_burner') {
    url = `/product/oil_burner/${id}`;
  }
  return { url, isPerfume };
};

export const getUrlGotoProductLandingPage = (data) => {
  const {
    combo, type, id,
  } = data;
  let url;
  if (type === 'Kit') {
    url = `/product/kit/${id}`;
  } else if (type === 'Perfume') {
    const itemsT = combo ? _.filter(combo, x => x.product.type === 'Scent') : [];
    if (itemsT.length > 1) {
      url = `/products/${itemsT[0].product.id}/${itemsT[1].product.id}`;
    }
  } else if (type === 'Wax_Perfume') {
    url = `/product/wax/${id}`;
  } else if (type === 'Elixir') {
    url = `/product/elixir/${id}`;
  } else if (type === 'bundle') {
    url = `/product/bundle/${id}`;
  } else if (type === 'Scent') {
    url = `/product/Scent/${id}`;
  } else if (type === 'Creation') {
    url = `/product/creation/${id}`;
  } else if (type === 'hand_sanitizer') {
    url = `/product/hand-sanitizer/${id}`;
  } else if (type === 'home_scents') {
    url = `/product/home-scents/${id}`;
  } else if (type === 'dual_candles') {
    url = `/product/dual-candles/${id}`;
  } else if (type === 'single_candle') {
    url = `/product/single-candle/${id}`;
  } else if (type.name === 'holder') {
    url = `/product/holder/${id}`;
  } else if (type.name === 'gift_bundle') {
    url = `/product/gift-bundle/${id}`;
  } else if (type.name === 'dual_crayons') {
    url = `/product/dual_crayons/${id}`;
  } else if (type.name === 'bundle_creation') {
    url = `/product/bundle_creation/${id}`;
  } else if (type.name === 'reed_diffuser') {
    url = `/product/reed_diffuser/${id}`;
  } else if (type.name === 'perfume_diy') {
    url = `/product/perfume_diy/${id}`;
  } else if (type.name === 'car_diffuser') {
    url = `/product/car_diffuser/${id}`;
  } else if (type.name === 'mini_candles') {
    url = `/product/mini_candles/${id}`;
  } else if (type.name === 'oil_burner') {
    url = `/product/oil_burner/${id}`;
  }
  return url;
};

export const fetchAnswersFerrari = () => {
  try {
    console.log("fetching Answers");
    const options = {
      url: GET_ALL_ANSWERS_FERRARI,
      method: 'GET',
    };

    return fetchClient(options);
  } catch (error) {
    return null;
  }
};

export const fetchAnswers = () => {
  try {
    console.log("fetching Answers");
    const options = {
      url: GET_ALL_ANSWERS,
      method: 'GET',
    };

    return fetchClient(options);
  } catch (error) {
    return null;
  }
};

export const fetchAnswersAlwaysUS = () => {
  try {
    const options = {
      url: GET_ALL_ANSWERS,
      method: 'GET',
    };

    return fetchClientAlwaysUS(options);
  } catch (error) {
    return null;
  }
};

export const fetchQuestionsFerrari = () => {
  try {
    const options = {
      url: GET_ALL_QUESTIONS_FERRARI,
      method: 'GET',
    };

    return fetchClient(options);
  } catch (error) {
    return null;
  }
};

export const fetchQuestions = () => {
  try {
    const options = {
      url: GET_ALL_QUESTIONS,
      method: 'GET',
    };

    return fetchClient(options);
  } catch (error) {
    return null;
  }
};

export const fetchQuestionsAlwaysUS = () => {
  try {
    const options = {
      url: GET_ALL_QUESTIONS,
      method: 'GET',
    };

    return fetchClientAlwaysUS(options);
  } catch (error) {
    return null;
  }
};

export const fetchCMS = (id) => {
  if (!id) {
    return null;
  }
  try {
    const options = {
      url: `${GET_PAGE_CMS_URL}${id}/`,
      method: 'GET',
    };
    return fetchClient(options);
  } catch (error) {
    return null;
  }
};

export const fetchCMSHomepage = (name) => {
  const isDraft = window.location.pathname.includes('draft');
  const newName = `${name}${getCountry() ? `-${getCountry()}` : ''}`;
  try {
    const options = {
      url: `${isDraft ? GET_PAGE_DRAFT_CMS_HOME_PAGE_URL : GET_PAGE_CMS_HOME_PAGE_URL}${newName}`,
      method: 'GET',
    };
    return fetchClient(options);
  } catch (error) {
    return null;
  }
};

export const fetchAllCMSPage = () => {
  try {
    const options = {
      url: `${GET_PAGE_ALL_CMS_URL}`,
      method: 'GET',
    };
    return fetchClient(options);
  } catch (error) {
    return null;
  }
};

export const fetchCountry = () => {
  const defaultHeaders = {
    'Content-Type': 'application/json',
    // 'accept-country': auth.getCountry(),
  };
  const options = {
    method: 'GET',
    url: GET_COUNTRY_URL,
    headers: defaultHeaders,
  };
  return axios(options);
};

export function getAllMood() {
  try {
    const options = {
      url: GET_MOOD_URL,
      method: 'GET',
    };

    return fetchClient(options);
  } catch (error) {
    return null;
  }
}

export function getSEOFromCms(data) {
  if (!data) {
    return undefined;
  }
  const { meta } = data;
  if (meta) {
    return { seoDescription: meta.search_description, seoTitle: meta.seo_title, seoKeywords: meta.metatag };
  }
  return undefined;
}

export function isNumeric(value) {
  return /^-{0,1}\d+$/.test(value);
}

export function getAltImageV2(data) {
  if (!data || !data.title) {
    return '';
  }
  return data.title;
}

export function getAltImage(url) {
  if (!url) {
    return '';
  }
  const fileName = url.substring(url.lastIndexOf('/') + 1);
  return fileName;
}

export function getCmsCommon(cmsInfo) {
  return _.find(cmsInfo, x => x.title === 'Common');
}

export function getNameFromButtonBlock(block, name) {
  if (!block || block.length === 0) {
    return '';
  }
  const object = _.find(block, x => x.value.name === name);
  if (object) {
    return object.value.text;
  }
  return '';
}

export function getPlaceholerFromButtonBlock(block, name) {
  if (!block || block.length === 0) {
    return '';
  }
  const object = _.find(block, x => x.value.name === name);
  if (object) {
    return object.value.placeholder;
  }
  return '';
}

export function getLinkFromButtonBlock(block, name) {
  if (!block) {
    return '';
  }
  const object = _.find(block, x => x.value.name === name);
  if (object) {
    return object.value.link;
  }
  return '';
}

export function getNameFromCommon(cms, name) {
  if (_.isEmpty(cms)) {
    return '';
  }
  const objectText = _.find(cms.body, x => x.value.name === name);
  if (objectText) {
    return objectText.value.text;
  }
  return '';
}

export function getValueObjectFromCommon(cms, text = "") {
  return (cms?.body || cms?.body?.length > 0) && (text || text.length > 0) ? 
        cms.body.find(x => x.value.text === text) : "";
}

export function getAllValueObjectsFromCommon(cms, texts = []) {
  let currentTexts = _.cloneDeep(texts);
  let answers = [];
  if ((cms?.body?.length > 0) && (texts?.length > 0)) {
    for (let i = 0; i<currentTexts.length; i++) {
      answers.push(
        {
          key: currentTexts[i],
          value: getNameFromCommon(cms, currentTexts[i])
        }
      )
    }
  }
  return answers;
}

// ======Namogoo========
export function addCartNamogoo(price) {
  if (!window.journeyDataLayer) window.journeyDataLayer = [];
  window.journeyDataLayer.push({
    action: 'addCartData',
    params: { cartTotal: price },
  });
}

export function addOrderCartNamogoo(data) {
  if (!window.journeyDataLayer) window.journeyDataLayer = [];
  window.journeyDataLayer.push({
    action: 'addOrderData',
    params: data,
  });
}

// ======AMPLITUDE========

function sendEventAmplitude(event) {
  if (window.amplitude) {
    console.log('sendEventAmplitude', event);
    window.amplitude.getInstance().logEvent(event);
  }
}
export function clickAddToCartAmplitude() {
  sendEventAmplitude('Click - Add_To_Cart');
}

export function onClickContinuteShoppingAmplitude() {
  sendEventAmplitude('Click - Continue_Shopping');
}

export function onClickCheckOutAmplitude() {
  sendEventAmplitude('Click - Checkout');
}

export function pageViewCheckoutAmplitude() {
  sendEventAmplitude('Pageview - Checkout');
}

export function onClickEditCustomerAmplitude() {
  sendEventAmplitude('Click - Edit_Customer Details');
}

export function pageViewShippingAmplitude() {
  sendEventAmplitude('Pageview - Shipping');
}

export function pageViewPaymentAmplitude() {
  sendEventAmplitude('Pageview - Payment');
}

export function pageViewCheckoutSuccessAmplitude() {
  sendEventAmplitude('Pageview - Checkout_Success');
}

export function onClickContinuteShoppingHomeAmplitude() {
  sendEventAmplitude('Click - Continue Shopping');
}

export function pageViewUserLoginAmplitude() {
  sendEventAmplitude('Pageview - User_Login');
}

// ====== SEGMENT TRACKING ======

function sendSegmentData(eventName, data) {
  const dataSend = {
    ...data,
    metadata: {
      utm_source: getUTM('utm_source'),
      utm_medium: getUTM('utm_medium'),
      utm_campaign: getUTM('utm_campaign'),
    }
  }
  console.log(`Segment - ${eventName}`, dataSend);
  if (window.analytics) {
    window.analytics.track(eventName, dataSend);
  }
}

function sendSegmentIdentify(data) {
  const id = auth.getLogin()?.user?.id;
  console.log('Segment - Identify', {
    id: id ?? "anonymousId",
    data
  });
  if (window.analytics) {
    window.analytics.identify(id, data); //id == null =>> fallback to anonymousId by Segment?
  }
}

export function segmentVerifycation(user) {
  const {
    id, email, last_name: lastName, first_name: firtName,
  } = user;
  const data = {
    account_name: email,
    first_name: firtName,
    last_name: lastName,
    email,
    user_id: id
  };
  sendSegmentIdentify(data);
}

export function segmentViewPageIdentify(user) {
  const {
    id, email, last_name: lastName, first_name: firtName, gender, is_club_member: isClubMember, phone, shared_promo: sharedPromo,
  } = user;
  const data = {
    first_name: firtName,
    last_name: lastName,
    email,
    gender,
    is_club_member: isClubMember,
    phone,
    user_id: id
  };
  sendSegmentIdentify(data);
}

export function segmentSignedIdentify(user) {
  const {
    id, email, last_name: lastName, first_name: firtName, gender, is_club_member: isClubMember, phone, shared_promo: sharedPromo,
  } = user;
  const data = {
    account_name: email,
    first_name: firtName,
    last_name: lastName,
    email,
    gender,
    is_club_member: isClubMember,
    phone,
    shared_promo: sharedPromo,
    user_id: id
  };
  sendSegmentIdentify(data);
}

export function segmentSignedIn(user) {
  const {
    email, last_name: lastName, first_name: firtName, gender, is_club_member: isClubMember, phone, shared_promo: sharedPromo, id
  } = user;
  const PIIdata = {
    first_name: firtName,
    last_name: lastName,
    email,
  }
  const dataSend = {
    gender,
    is_club_member: isClubMember,
    phone,
    shared_promo: toString(sharedPromo),
    user_id: id
  };
  sendSegmentData('Account Signed In', dataSend);
  sendSegmentIdentify(PIIdata);
}

export function segmentAccountDashboardTabSelected(data) {
    const eventName = "Account Dashboard Tab Selected"
    //clone deep data
    const dataSend = _.cloneDeep(data);
    if (!data?.account_dashboard_tab_name) {
      dataSend.account_dashboard_tab_name = "account-overview";
    }
    sendSegmentData(eventName, dataSend);
}

export function segmentAccountDashboardReorderItemClicked(data) {
    const eventName = "Account Dashboard Reorder Item Clicked"
    //remade data into this structure
    const dataSend = {
      product_name: data?.name,
      product_id: data?.item?.id,
    }
    sendSegmentData(eventName, dataSend);
}

export function segmentAccountDashboardWriteReviewInitiated(data) {
  const eventName = "Account Dashboard Write Review Initiated";
  const dataSend = {
    product_name: data?.name,
    product_id: data?.item,
  }
  sendSegmentData(eventName, dataSend);
}

export function segmentAccountDashboardReferAFriendCopyButtonClicked(promo_code) {
  const eventName = "Account Dashboard Refer A Friend Copy Button Clicked";
  sendSegmentIdentify({
    promo_code
  })
  sendSegmentData(eventName, {});
}



export function segmentClub21GSubscriptionCheckoutStarted() {
  const dataSend = {
    currency: auth.getNameCurrency(),
    country: auth.getCountry(),
  };
  sendSegmentData('Club21G Subscription Checkout Started', dataSend);
}

export function segmentClub21GCheckoutCustomerDetailsEntered(basket, shipInfo) {
  const dataSend = {
    checkout_id: basket?.id,
    step: 1,
  };
  const PIIdata = {
    email: shipInfo?.email,
    first_name: shipInfo?.firstName,
    last_name: shipInfo?.lastName,
    account_name: shipInfo?.name,
    phone: shipInfo?.phone,
    dob: shipInfo?.dob,
  };
  const momentDob = moment(shipInfo?.dob, 'MM/DD/YYYY');
  if (PIIdata?.dob && (!momentDob.isValid() || !momentDob.isBefore(moment()))) {
    delete PIIdata?.dob;
  }
  sendSegmentData('Club21G Checkout Customer Details Entered', dataSend);
  sendSegmentIdentify(PIIdata);
}

export function segmentClub21GShippingInfoEntered(basket, shippingMethod, shippingAddress, shipInfo) {
  // console.log('current shipinfo', shipInfo);
  const shippingAddress1 = `${shipInfo?.street1}${shipInfo?.street2 ? ',' : ''}${shipInfo?.street2 ?? ""}, ${shipInfo?.country}, ${shipInfo?.postal_code}`;
  const data = {
    checkout_id: basket?.id,
    shipping_method: shippingMethod,
    shipping_country: shipInfo?.country,
    step: 2,
  };  
  const PIIdata = {
    shipping_address: shippingAddress1,
    shipping_country: shipInfo?.country,
  }
  sendSegmentData('Club21G Subscription Shipping Info Entered', data);
  sendSegmentIdentify(PIIdata);
}


export function segmentClub21GPaymentInfoEntered(step, basket, shippingMethod, paymentMethod) {
  console.log("Current Basket", basket);
  const dataSend = {
    checkout_id: basket?.id,
    step,
    shipping_method: shippingMethod,
    payment_method: paymentMethod,
    total: basket?.total,
    currency: auth.getNameCurrency(),
    URL: window.location.href,
    step: 3,
  };
  sendSegmentData('Club21G Subscription Payment Info Entered', dataSend);
}

export function segmentSubscribedClub21G(data) {
  sendSegmentData('Subscribed Club21G', data);
  removeAllUTMCookies();
}


export function segmentCheckoutStarted(basket) {
  const eventName = 'Checkout Started'
  const {
    subtotal, total, discount, items,
  } = basket;
  console.log(`currentBasket`, basket);
  const products = items?.map((d, index) => (
    {
      product_id: `${d?.item?.id}`,
      name: d.name,
      category: d?.item?.product?.type?.name,
      quantity: d.quantity || 1,
      price: d.price,
      url: d?.meta?.url ? `https://shop.maison21g.com${d?.meta?.url}` : "",
      image_url: d.image_display,
      position: index
    }
  ));
  const dataSend = {
    currency: auth.getNameCurrency(),
    discount: discount ? parseFloat(discount) : 0.0,
    cart_id: basket?.id,
    products,
    product_category: products.map(item => item.category),
    product_name: products.map(item => item.name),
    product_price: products.map(item => item.price),
    total: total ? parseFloat(total) : 0.0,
    shop_source: 'online',
    metadata: {
      utm_campaign: getUTM('utm_campaign'),
      utm_source: getUTM('utm_source'),
      utm_medium: getUTM('utm_medium'),
    }
  };


  sendSegmentData(eventName, dataSend);
}

export function segmentWorkshopBookingCompleted(data, unitList) {
  const {
    client, lines, 
  } = data;

  const booking = lines.filter(x => x.type === 'booking').find(d => (
    {
      product_id: `${d?.item?.id}`,
      name: d?.name,
      quantity: d?.qty || 1,
      price: d?.amount,
    }
  ));

  // const addOns = lines.filter(x => x.type === 'product');

  const trackData = {
    number_of_pax: booking?.qty,
    boutique_store: unitList[booking?.bookings?.[0]?.provider_id]?.name,
    payment_method: data?.payment_processor ?? 'stripe',
    invoice_id: data?.id,
    invoice_number: data?.number, 
    booking_id: booking?.booking_ids,
    revenue: data?.deposit,
    total: data?.deposit - data?.discount_amount,
    discount: data?.discount_amount,
    currency: data?.currency,
    metadata: {
      utm_campaign: getUTM('utm_campaign'),
      utm_source: getUTM('utm_source'),
      utm_medium: getUTM('utm_medium'),
    }
  };
  sendSegmentIdentify({
    email: client?.email,
    account_name: client?.name,
    phone: client?.phone
  });
  sendSegmentData('Workshop Booking Completed', trackData);
  removeAllUTMCookies();
}

export function segementWorkshopAddOn(dataSelected) {
  const eventName = 'Workshop Add Ons';
  console.log(dataSelected)
  const products = dataSelected.map(item => ({
    product_id: item?.id,
    product_name: item?.name,
    product_price: item?.price,
    currency: item?.currency?.toLowerCase() || auth.getNameCurrency()
  }))
  const dataSend = {
    currency: auth.getNameCurrency(),
    products,
    product_name: products.map(item => item.product_name),
    product_price: products.map(item => item.product_price),
    product_id: products.map(item => item.product_id),
    URL: window.location.href,
    metadata: {
      utm_campaign: getUTM('utm_campaign'),
      utm_source: getUTM('utm_source'),
      utm_medium: getUTM('utm_medium'),
    }
  };
  sendSegmentData(eventName, dataSend);
}

export function segmentWorkshopDetailsEntered(data) {
  const eventName = 'Workshop Details Entered';
  console.log(data)
  const dataSend = {
    store_name: data?.store.name,
    start_date: moment(data?.date).format('DD-MM-YYYY'),
    start_time: data?.time,
    duration: (data?.arrChecked.length - 1) * 30,
    URL: window.location.href,
    metadata: {
      utm_campaign: getUTM('utm_campaign'),
      utm_source: getUTM('utm_source'),
      utm_medium: getUTM('utm_medium'),
    }
  };
  sendSegmentData(eventName, dataSend);
}

export function segmentWorkshopPersonalDetailsEntered(data) {
  sendSegmentIdentify(data);
  sendSegmentData('Workshop Personal Details Entered', {});
}

export function segmentWorkshopCheckOutInitiated(data, value) {
  const eventName = 'Workshop Checkout Initiated';
  let total_pax = parseInt(window.location.href.split('/').pop() ?? 1);
  const dataSend = {
    currency: data?.currency,
    product_id: data?.id,
    name: data?.name,
    //total pax is the last trailing slash in the URL
    //example: 
    //http://localhost:3003/sg/perfume-workshop/perfume-creation/service/1/count/10
    //means that the count is 10
    //so total pax is 10
    total_pax, 
    price: parseFloat(data?.price),
    url: window.location.href,
    image_url: data?.picture_path ? `https://shop.maison21g.com${data?.picture_path}` : "",
    position: data?.position,
    value,
  };
  sendSegmentData(eventName, dataSend);
}

export function segmentProductView(data) {
  const eventName = `Product Viewed`;
  const { items, type, actual_type, is_featured } = data;
  let comboName = data?.combo?.
      filter?.(item => !(item?.type?.toLowerCase?.()  === 'bottle' || 
                          item?.name?.toLowerCase?.() === 'metal cap' ||
                          item?.name?.toLowerCase?.()  === 'car diffuser box' ||
                          item?.name?.toLowerCase?.()  === 'candle holder' ||
                          item?.name?.toLowerCase?.()  === 'diffuser' ||
                          item?.name?.toLowerCase?.()  === 'burner' ||
                          isNaN(item?.id))).
      map?.(item => item?.name).
      join?.(' - ');
  const dataSend = {
    category: actual_type?.name ?? type?.name,
    currency: auth.getNameCurrency(),
    product_id: items[0]?.id,
    name: is_featured ? comboName : items[0]?.name ?? data?.name,
    is_best_seller: is_featured ?? false,
    price: parseFloat(items[0]?.price),
    quantity: items[0]?.quantity,
    url: window.location.href,
    image_url: items[0]?.image_display,
  };
  if (is_featured && data?.tags?.[0]?.name) {
    dataSend.best_seller_properties = {
      category: actual_type?.name ?? type?.name,
      collection: data?.tags?.[0]?.name,
      mood: data?.short_description ?? data?.displayName,
      //intensity is always between 1 and 5 inclusive
      intensity: parseInt(data?.profile?.intensity % 5),
      name: data?.name ?? type?.name,
      family: data?.family,
    };
  }
  if (type?.name === "discovery_box" || items[0]?.name === "discovery_box") {
    sendScentDiscoverySetViewedHotjarEvent();
  }
  sendSegmentData(eventName, dataSend);
}

export function segmentBestSellerProductViewInitiated(product) {
  const eventName = `Best Seller - Product View Initiated`;
  console.log(`Current Product:`, product);
  const dataSend = {
    name: product?.name,
    product_id: product?.id,
    currency: auth.getNameCurrency(),
    price: parseFloat(product?.price),
    url: window.location.href,
    image_url: product?.image,
    category: product?.type?.name
  };
  sendSegmentData(eventName, dataSend);
}

export function segmentProductViewedScentSelected(scentSelected, dataIndex, englishIngredient) {
  console.log("DATA IN EVENT:", dataIndex);
  const indexName = dataIndex === 1 ? '1st' : 
  dataIndex === 2 ? '2nd' :
  dataIndex === 3 ? '3rd' :
                    `${dataIndex}th`;
  if (!dataIndex) return;
  const eventName = `Product Viewed - Select ${indexName} Ingredient`;
  console.log(`scentSelected`, scentSelected);
  console.log(`data INDEX`, dataIndex);
  const dataSend = {
    product_id: scentSelected?.id,
    ingredient_id: englishIngredient?.id,
    url: window.location.href,
  }
  dataSend[`select_ingredient_${dataIndex}`] = englishIngredient?.name;
  sendSegmentData(eventName, dataSend);
  
}

export function segmentCartViewed(data) {
  const eventName = 'Cart Viewed';
  const { items, id } = data;
  const products = items?.map((d, index) => (
    {
      //category: d?.item?.product?.type?.name,
     // coupon: d?.promo,
      image_url: d.image_display,
      name: d.name,
      //position: index,
      price: d.price,
      product_id: `${d?.item?.id}`,
      //quantity: d.quantity,
      //url: d?.meta.url ? `https://shop.maison21g.com${d?.meta.url}` : "",
    }
  ));

  const dataSend = {
    cart_id: id,
    products,
    //product_name: products.map(item=> item.name),
    //product_category: products.map(item=> item.category),
    //product_id: products.map(item=>item.product_id),
    //product_price: products.map(item=> parseFloat(item.price)),
    currency: auth.getNameCurrency(),
  };

  sendSegmentData(eventName, dataSend);
}

export function segmentCheckoutStepViewed(basket) {
 const eventName = 'Checkout Step Viewed';
  const data = {
    checkout_id: basket?.id,
    currency: auth.getNameCurrency(),
    step: 1,
    subtotal: basket?.subtotal ? parseFloat(basket?.subtotal) : undefined,
    total: basket?.total ? parseFloat(basket?.total) : undefined,
    discount: basket?.discount ? parseFloat(basket?.discount) : undefined,
    URL: window.location.href,
  };

  sendSegmentData(eventName, data);
}

export function segmentExpressCheckoutStarted(basket, paymentMethod = "paypal") {
  const eventName = 'Express Checkout Started';
  console.log("Express Checkout - Current Basket:" , basket);
  const products = basket.items?.map((d, index) => (
    {
      category: d?.item?.product?.type?.name,
      image_url: d.image_display,
      name: d.name,
      position: index,
      price: d.price,
      product_id: `${d?.item?.id}`,
      quantity: d.quantity,
      url: d?.meta.url ? `https://shop.maison21g.com${d?.meta.url}` : "",
    }
  ));
   const data = {
     checkout_id: basket?.id,
     currency: auth.getNameCurrency(),
     step: 4,
     products,
     product_name: products.map(item=> item.name),
     product_category: products.map(item=> item.category),
     product_id: products.map(item=>item.product_id),
     product_price: products.map(item=> parseFloat(item.price)),
     payment_method: paymentMethod,
     subtotal: basket?.subtotal ? parseFloat(basket?.subtotal) : undefined,
     total: basket?.total ? parseFloat(basket?.total) : undefined,
     discount: basket?.discount ? parseFloat(basket?.discount) : undefined,
     URL: window.location.href,
     metadata: {
       utm_source: getUTM('utm_source'),
       utm_medium: getUTM('utm_medium'),
       utm_campaign: getUTM('utm_campaign'),
     }
   };

   sendSegmentData(eventName, data);
 }

export function segmentCheckoutCustomerDetailsEntered(basket, customerInfo) {
  const eventName = 'Checkout Step Completed';
  let data = {
    checkout_id: basket?.id,
    birthdate: customerInfo?.dob,
    country_code: auth.getCountry(),
    step: 2,
  };
  if (!customerInfo?.dob) delete data.birthdate;

 sendSegmentData(eventName, data);
}

export function segmentShippingInfoViewed(basket) {
  const eventName = 'Shipping Info Viewed';
  let data = {
    checkout_id: basket?.id,
    country_code: auth.getCountry(),
    step: 2,
  };

 sendSegmentData(eventName, data);
}


export function segmentShippingInfoEntered(basket, shippingMethod, shippingAddress, shippingCountry) {
  const eventName = 'Shipping Info Entered';
  const data = {
    checkout_id: basket?.id,
    step: 3,
    shipping_method: shippingMethod,
    shipping_address: shippingAddress,
    shipping_country: shippingCountry,
  };

  sendSegmentData(eventName, data);
}

export function segmentPaymentInfoViewed(basket, shippingMethod, shippingAddress, shippingCountry) {
  const eventName = 'Payment Info Viewed';
  const data = {
    checkout_id: basket?.id,
    step: 3,
    shipping_method: shippingMethod,
    shipping_address: shippingAddress,
    shipping_country: shippingCountry,
  };

  sendSegmentData(eventName, data);
}


export function segmentPaymentInfoEntered(basket, shippingMethod, paymentMethod, shippingAddress, shippingCountry) {
  const eventName = 'Payment Info Entered';
  const data = {
    checkout_id: basket?.id,
    step: 4,
    payment_method: paymentMethod,
    shipping_method: shippingMethod,
    shipping_address: shippingAddress,
    shipping_country: shippingCountry,
  };

  sendSegmentData(eventName, data);
}

export function isBestSeller(productId = 0) {
  const soChic = [
    17414,
    16877,
    16870,
    16874,
    16872,
    16906,
    17419
  ];
  if (soChic.findIndex((ele) => ele === productId) !== -1) return "So Chic";
  const passion = [
    17425,
    16880,
    16909,
    16871,
    16873,
    17415,
    17421
  ];
  if (passion.findIndex((ele) => ele === productId) !== -1) return "Passion";
  const freshness = [
    16879,
    17422,
    16908,
    16875
  ];
  if (freshness.findIndex((ele) => ele === productId) !== -1) return "Freshness";
  const extravagance = [
    17423,
    17426,
    16907,
    16878,
    17416
  ];
  if (extravagance.findIndex((ele) => ele === productId) !== -1) return "Extravagance";
  const calm = [
    17417,
    17427,
    17424,
    17420,
    16876
  ]
  if (calm.findIndex((ele) => ele === productId) !== -1) return "Calm";
  return null;
}

export function segmentOrderCompleted(dataInput) {
  const eventName = 'Order Completed';
  //check if one of these data properties is still string.
  //products[].product_id
  //total
  try {
    let data = _.cloneDeep(dataInput);

    if (data?.products && data?.products?.length > 0) {
      data.product_is_best_seller = data?.products?.map?.(_ => false);
      data.product_best_seller_category = data?.products?.map?.(_ => null);
      data.products.forEach((item, index) => {
        if (typeof item?.price === 'string') {
          item.price = parseFloat(item?.price);
        }
        if (typeof item?.product_id === 'string') {
          data.products[index].product_id = parseInt(item?.product_id);
          if (typeof isBestSeller(parseInt(item?.product_id)) === 'string') {
            item.is_best_seller = true;
            item.best_seller_category = isBestSeller(parseInt(item?.product_id));
            data.product_is_best_seller[index] = true;
            data.product_best_seller_category[index] = isBestSeller(parseInt(item.product_id));
          }
        }
        if (typeof item.total === 'string') {
          item.total = parseFloat(item.total);
        }
      }); 

      if (typeof data.total === 'string') {
        data.total = parseFloat(data.total);
      }

        const countryRealName = getCountryRealNameFromHeader() ?? "";
        const dataSend = {
          ...data, 
          country: countryRealName
        }
        removeAllUTMCookies();
        sendSegmentData(eventName, dataSend);
      }
    } catch (e) {
      console.log(e);
    }
}



export function segmentCouponApplied(cardId, couponId, couponName, discountObject) {
  const data = {
    discount: parseFloat(discountObject),
    currency: auth.getNameCurrency(),
    coupon_name: couponName,
    cart_id: cardId,
    coupon_id: couponId,
    
  };
  sendSegmentData('Coupon Applied', data);
}

export function segmentProductReviewed(data) {
  sendSegmentData('Product Reviewed', {...data, metadata: {
    utm_source: getUTM('utm_source'),
    utm_medium: getUTM('utm_medium'),
    utm_campaign: getUTM('utm_campaign'),
    url: window.location.href,
  }
});
}

export function segmentSignUp(user, signUpMethod = "normal") {
  const {
    id, email, lastName, firtName, 
    gender, isClubMember, phone, sharedPromo,
    user_id
  } = user;
  console.log("Segment - User", user)

  const PIIdata = {
    user_id: user_id,
    account_name: `${firtName} ${lastName}`,
    first_name: firtName,
    last_name: lastName,
    email,
    gender,
    sign_up_method: signUpMethod,
    is_club_member: isClubMember,
    phone,
    shared_promo: sharedPromo,
  }
  const data = {
    // account_name: email,
    // first_name: firtName,
    // last_name: lastName,
    // email,
    url: window.location.href,
    country_code: auth.getCountry(),
    
  };
  sendSegmentData('Account Created', data);
  sendSegmentIdentify(PIIdata);
}

export function segmentSubscribedNewsletter(email) {
  sendSegmentData('Subscribed Newsletter', {});
  sendSegmentIdentify({email});
}

export function segmentProductAdded(data, cartID) {
  const eventName = 'Product Added';
  if (data?.meta?.typeOfGift) {
    const newItem = {
      product_id: data?.item,
      product_price: data?.price_custom,
      gifting_offer: data?.meta?.typeOfGift === "gift_bundles" ? 
                    "Gift Collection" :  data?.meta?.typeOfGift === "Evoucher" ? 
                    "Gift Cards" : data?.meta?.typeOfGift === "gift_workshop" ? 
                    "Gift Workshop Experience" :
                    data?.name,
      quantity: data?.quantity,
      gift_name: data?.meta?.typeOfGift === "gift_bundles" || data?.meta?.typeOfGift === "Evoucher" || data?.meta?.typeOfGift === "gift_workshop" ? 
                    data?.name : data?.meta?.giftName,
      currency: auth.getNameCurrency()
    }
    sendSegmentData("Gifting Product Added", newItem);
  } else {
    const newItem = {
      cart_id: cartID,
      image_url: data?.image_urls?.[0] ? data?.image_urls?.[0] : "",
      product_id: data?.item?.id ? `${data.item.id}` : `${data.item}`,
      name: data.name,
      category: data?.type,
      quantity: data?.quantity,
      price: parseFloat(data?.price),
      URL: data?.meta?.url ? `https://shop.maison21g.com${data?.meta.url}` : "",
      currency: auth.getNameCurrency()
    };
    sendSegmentData(eventName, newItem);
  }
}

export function segmentProductRemoved(data) {
  const eventName = 'Product Removed';
  console.log('Product Removed Data', data);

  const newItem = {
    cart_id: auth.getBasketId().toString(),
    category: data?.item?.product?.type?.name ?? "",
    name: data?.name,
    product_id: data?.item?.id,
    price: parseFloat(data?.price),
    quantity: data?.quantity,
  };

  sendSegmentData(eventName, newItem);
}

export function segmentTrackPersonalityQuizInitiated() {
  const eventName = `Personality Quiz Initiated`;
  let data = {
    cta_clicked: true,
    name: eventName,
    url: 'https://shop.maison21g.com/landingquiz',
  }
  sendSegmentData(eventName, data);
}

function convertNameIntoProperQuestion(name) {
  switch (name) {
    case "What_is_your_gender": return "What is your gender?";
    case "What_is_your_age": return "What is your age?";
    case "What_is_your_skin-tone": return "What is your skin tone?";
    default: return null;
  }
}

function convertNameIntoProperAnswer(name = "") {
  switch (name.toLowerCase()) {
    case "male": return "Male";
    case "female": return "Female";
    case "gender-free": return "Gender-neutral"
    default: return null;
  }
}

export function toTitleCase (str) {
  return str.replace(
    /\w\S*/g,
    function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
}

function mapSkinTone (color = "") {
  switch (color?.toLowerCase?.()){
    case "#fff4f2": return "Skin-Light-1"; 
    case "#f2d4ca": return "Skin-Light-2"; 
    case "#e0afa8": return "Skin-Light-3"; 
    case "#d4a093": return "Skin-Light-4"; 
    case "#c68d82": return "Skin-Light-5"; 
    case "#a36a5f": return "Skin-Dark-1"; 
    case "#905c4f": return "Skin-Dark-2"; 
    case "#653d35": return "Skin-Dark-3"; 
    case "#4e2f2a": return "Skin-Dark-4"; 
    case "#281b19": return "Skin-Dark-5"; 
    default: return "Skin-Light-1"
  }
}

function mapQuestionName (name = "" ) {
  switch (name.toLowerCase()) {
    case "age": return 1;
    case "gender": return 2;
    case "skintone": return 3;
    case "personality": return 4;
    case "strength": return 9;
    case "mixes": return 10;
    case "families": return 11;
    case "customebottle": return 12;
    default: return 0;
  }
}


export function segmentTrackPersonalityQuizStep(index, listQuestion, 
  result, is_tooltip_opened = false, cms, 
  englishQuestions4_8 = [], englishAnswers4_8 = [],
  actualQuestions4_8 = [], actualAnswers4_8 = []) {
  const cmsCommon = getCmsCommon(cms);
  // console.log('Segment - cmsCommon', cmsCommon);
  // console.log('Segment - listQuestion', listQuestion);
  // console.log('Segment - result', result);

  const lastQuestion = listQuestion?.find((item) => item.no === index);
  const lastAnswer = result?.find(item => (item?.name === lastQuestion?.name));
  // map the answers properly in proper order: 

  // console.log("Segment - lastQuestion", lastQuestion);
  // console.log("Segment - lastAnswer", lastAnswer);
  
  let correctIndex = 0;

  //mapping of skin-tone
  if (lastQuestion?.name === 'skin-tone') {
    mapSkinTone(lastAnswer?.value);
  }

  switch (lastQuestion?.name) {
    case "gender": correctIndex = 2; break;
    case "age": correctIndex = 1; break;
    case "skin-tone": correctIndex = 3; break;
    case "persional1": correctIndex = 4; break;
    case "persional2": correctIndex = 5; break;
    case "persional3": correctIndex = 6; break;
    case "persional4": correctIndex = 7; break;
    case "persional5": correctIndex = 8; break;
    case "occasion": correctIndex = 9; break;
    case "strength": correctIndex = 10; break;
    case "favorite": correctIndex = 11; break;
    case "perfume-quiz": correctIndex = 12; break;
    case "email": correctIndex = 13; break;
    default: correctIndex = 0; break;
  }

  // console.log("Segment - correctIndex", correctIndex);

  let is_skipped = false;
  const eventName = `Personality Quiz - Step ${correctIndex} Completed`;
  if ((lastQuestion?.name === "favorite" || lastQuestion?.name === "perfume-quiz") && !lastAnswer?.value) {
    is_skipped = true;
  }

  let questionObject = getValueObjectFromCommon(cmsCommon, lastQuestion?.title);
  let answerObject = getValueObjectFromCommon(cmsCommon, lastAnswer?.value);

  let translatedQuestion = convertNameIntoProperQuestion(
      questionObject?.value?.name) ?? lastQuestion?.title;
  let translatedAnswer = convertNameIntoProperAnswer(
      answerObject?.value?.name) ?? lastAnswer?.value;

  if (correctIndex >= 4 && correctIndex <= 8) {
    questionObject = actualQuestions4_8.find((item) => 
      item?.title === translatedQuestion);
    let translatedQuestionObject = englishQuestions4_8.find((item) => 
      item?.id === questionObject?.id);
    answerObject = actualAnswers4_8.find((item) => 
      item?.title === translatedAnswer);
    let translatedAnswerObject = englishAnswers4_8.find((item) =>
      item?.id === answerObject?.id);
    translatedQuestion = translatedQuestionObject?.title; 
    translatedAnswer = translatedAnswerObject?.title;
  }

  if (lastQuestion?.name === 'occasion') {
    translatedQuestion = "Why do you wear perfume?";  

    let possibleAnswers = getAllValueObjectsFromCommon(cmsCommon, [
      "WORK & SOCIAL",
      "SEDUCTIVE",
      "SPECIAL OCCASION",
      "CHILL & RELAX",
    ]);
    translatedAnswer = toTitleCase(
      possibleAnswers.find((item) => item.value === translatedAnswer)?.key
    )// answerObject = possibleAnswers.find((item) => 
    //   item.title === translatedAnswer);
  }

  if (lastQuestion?.name === 'strength') {
    translatedQuestion = "How strong do you like your perfume?";
    if (lastQuestion?.name === 'strength') {
      switch (lastAnswer?.value){
        case 0: translatedAnswer = "subtle"; break;
        case 1: translatedAnswer = "balanced"; break;
        case 2: translatedAnswer = "strong"; break;
      }
    }
  }


  let data = {
      quiz_section: lastQuestion?.name,
      quiz_question: translatedQuestion,
      quiz_answer: translatedAnswer,
      is_tooltip_opened,
      metadata: {
        utm_source: getUTM("utm_source"),
        utm_medium: getUTM("utm_medium"),
        utm_campaign: getUTM("utm_campaign"),
      },
      question_skipped: is_skipped
  }
  if (!is_skipped) {
    delete data.question_skipped;
  }
  if (correctIndex > 0 && correctIndex <= 13) {
    if (correctIndex === 11 && typeof translatedAnswer === 'string') {
      data.quiz_answer = [translatedAnswer];
    }
    sendSegmentData(eventName, data);
  }
}

export function segmentTrackPersonalityQuizStep13Completed(email, marketing_consent = false) {
  sendSegmentData(
    "Personality Quiz - Step 13 Completed",
    {
      quiz_section: "email",
      marketing_consent,
    }
  );
  sendSegmentIdentify({email, marketing_consent});
}

export function segmentTrackPersonalityQuizCompleted(email, dataSet) {
  // console.log("Segment - dataSet", dataSet);
  const data = {
    URL: window.location.href,
    personality_result: dataSet?.personality_choice?.personality?.title,
    country: getCountryRealNameFromHeader(),
  }
  const eventName = `Personality Quiz - Completed`;
  sendSegmentData(eventName, data);
  sendSegmentIdentify({email});
}



let translations;

const flattenAnswers = (ans) => {
  let currentAns = _.cloneDeep(ans);
  for (let i = 0; i < currentAns.length; i++) {
    const currentA = ans[i]?.children && ans[i]?.children?.length > 0 ? 
      (flattenAnswers(ans[i]?.children)) : [];
    currentAns = [...currentAns, ...currentA];
  }
  return currentAns;
}

const fetchDataFamilyAlwaysUS = () => {
  const options = {
    url: GET_ALL_SCENT_FAMILY_V4,
    method: 'GET',
  };
  return fetchClientAlwaysUS(options);
};


export async function fetchTranslationsForSegment() {
  if (!translations) {
    translations = await Promise.all([
      fetchQuestionsAlwaysUS(),
      fetchAnswersAlwaysUS(),
      fetchDataFamilyAlwaysUS(),
    ]);
    console.log(translations);
    const sortCallBack = (a, b) => !isNaN(a?.id) && !isNaN(b?.id) ? a?.id - b?.id : -1;
    translations[1] = flattenAnswers(translations[1]);
    for (let i = 0; i < translations.length; i++) {
      translations[i] = translations[i].sort(sortCallBack);
    }
  }
}

function mapPersonalityId(id, questionOrAnswer) {
  let translationIndex = questionOrAnswer === "answer" ? 1 : 0;
  return translations[translationIndex].find((item) => item?.id === id);
}
function mapFamilyId(id) {
  return translations[2].find((item) => item?.id === id);
}


export async function segmentTrackPersonalityQuizStepNew(data = {}) {
  console.log("translations", translations);
  const questionSection = data?.name;
  let question;
  let answer = data?.value;
  let questionStep = mapQuestionName(data?.name);
  let tempAns;
  let skipped = false;
  switch (data?.name) {
    case "age": question = "What is your age?"; break;
    case "gender": 
      question = "What is your gender?"; 
      answer = answer === "1" ? "Male" : answer === "2" ? "Female" : "Gender-neutral";
    break;
    case "skinTone": 
      question = "What is your skin-tone?"; 
      answer = mapSkinTone(data?.value);
    break;
    case "personality": 
      tempAns = mapPersonalityId(data?.value, "answer")
      question = tempAns?.question ?? data?.questionId;
      answer = tempAns?.title ?? data?.value;
      questionStep = data?.stepId;
    break;
    case "strength":
      question = "How strong do you like your perfume?";
      const mapStrength = (val) => {
        switch (val) {
          case 0: return "subtle";
          case 1: return "balanced";
          case 2: return "strong";
        }
      }
      answer = mapStrength(data?.value);
    break;    
    case "mixes": 
      question = "Why do you wear perfume?";
      answer = data?.value?.length > 1 ? "Work & Social - Seduction" : toTitleCase(data?.value?.[0]?.name);
    break;
    case "families":
      question = "Any favorite perfume family?";
      tempAns = data?.value?.map((item) => mapFamilyId(item?.id)?.name);
      answer = tempAns?.length > 0 ? _.cloneDeep(tempAns) : [];
      if (answer.length === 0) {
        skipped = true;
      }
    break;
    case "customeBottle":
      question = "Personalise your Perfume Bottle.";
      if (!data?.value?.currentImg && !data?.value?.nameBottle && !data?.value?.imagePremade) {
        skipped = true;
      } else {
        skipped = false;
      }
    break;
  }
  const eventName = `Personality Quiz - Step ${questionStep} Completed`;
  const dataSend = {
    quiz_section: questionSection,
    quiz_question: question,
    quiz_answer: answer,
  }

  if (skipped) {
    dataSend.question_skipped = true;
    delete dataSend.quiz_answer;
  }

  if (data?.name === "customeBottle") {
    delete dataSend.quiz_answer;
    dataSend.question_skipped = skipped;
  }

  if (data?.is_tooltip_opened) {
    dataSend.is_tooltip_opened = true;
  }

  sendSegmentData(eventName, dataSend); 
}

export function segmentTrackPersonalityQuizTooltipOpened(step) {
  const eventName = `Personality Quiz - Step ${step} Tooltip Opened`;
  sendSegmentData(eventName, {});
}

export function segmentTrackScentBarQuizInitiated() {
  const eventName = `Scent Bar Quiz - Initiated`;
  let data = {
    URL: 'https://shop.maison21g.com/freestyle',
  }
  sendSegmentData(eventName, data);
  sendHotjarEvent(eventName);
}


export function segmentTrackScentBarQuizStep(index, ingredient) {
  const eventName = `Scent Bar Quiz - ${index}${index === 1 ? "st": index === 2 ? "nd" : "th"} Ingredient Selected`;
  let data = {
    URL: window.location.href,
  }
  data[`select_ingredient_${index}`] = ingredient?.ingredient?.name;

  sendSegmentData(eventName, data);
  sendHotjarEvent(eventName);
}

export function segmentTrackScentBarQuizCompleted(state) {
  const eventName = `Scent Bar Quiz - Completed`;
  const actualState = state?.[0]
  const item = actualState.items?.[0];

  let data = {
    URL: window.location.href,
    image_url: item?.image_display,
    product_category: actualState?.type?.name,
    price: item?.price,
    product_id: (item?.id).toString(),
    product_name: item?.name,
  }
  sendSegmentData(eventName, data);
  sendHotjarEvent(eventName);
}


export function segmentNavbarSubCategorySelected(data) {
  const eventName = `Navbar - Sub Category Selected`;
  sendSegmentData(eventName, data);
}

export function segmentNavbarPersonalityQuizInitiated() {
  const eventName = `Navbar - Personality Quiz Initiated`;
  sendSegmentData(eventName, {});
}

export function segmentGiftingPageViewed() {
  const eventName = `Gifting Page Viewed`;
  sendSegmentData(eventName, {});
}

export function segmentGiftingOfferPageInitiated(gifting_offer = "") {
  const eventName = `Gifting Offer Page Initiated`;
  sendSegmentData(eventName, {
    gifting_offer
  });
}

export function segmentGiftingOfferPageViewed(gifting_offer = "") {
  const eventName = `Gifting Offer Page Viewed`;
  sendSegmentData(eventName, {
    gifting_offer
  });
}

export function segmentGiftingCollectionItemPageViewed(data) {
  const eventName = `Gifting Collection Item Page Viewed`;
  sendSegmentData(eventName, 
    {
      product_id: data?.id,
      product_name: data?.name,
      product_price: typeof data?.price === "string" ? 
        parseFloat(data?.price) :
        data?.price,
      currency: auth.getNameCurrency(),
      gifting_offer: "Gift Collection"
    }
  );
}

export function segmentGiftingSenderInfoViewed(giftDetail, name) {
  const eventName = `Gifting Sender Info Viewed`;
  let data = _.cloneDeep(giftDetail);
  data.price = parseFloat(data?.price ?? "0.0") ;
  if (name) sendSegmentIdentify({name});
  if (data?.type === "Evoucher") {
    const evoucherGiftDetail = data?.value?.item;
    data.price = parseFloat(evoucherGiftDetail?.price ?? "0.0");
    data.id = evoucherGiftDetail?.id;
    data.name = evoucherGiftDetail?.name;
  }
  data = {
      product_id: data?.id,
      product_name: data?.name,
      product_price: parseFloat(data?.price) ?? data?.price,
      currency: auth.getNameCurrency(),
      gifting_offer: data?.type === "gift_bundles" ?  
        "Gift Collection" : data?.type === "Evoucher" ?  
        "Gift Cards" : data?.type === "gift_workshop" ?  
        "Gift Workshop Experience" : 
        data?.name,
  }

  sendSegmentData(eventName, data);
}

export function segmentGiftingRecipientInfoViewed(email, name, giftDetail) {
  const eventName = `Gifting Recipient Info Viewed`;
  let data = _.cloneDeep(giftDetail);
  data.price = parseFloat(data?.price ?? "0.0");
  if (name) sendSegmentIdentify({name});
  if (data?.type === "Evoucher") {
    const evoucherGiftDetail = data?.value?.item;
    data.price = parseFloat(evoucherGiftDetail?.price);
    data.id = evoucherGiftDetail?.id;
    data.name = evoucherGiftDetail?.name;
  }
  data = {
      product_id: data?.id,
      product_name: data?.name,
      product_price: parseFloat(data?.price) ?? data?.price,
      currency: auth.getNameCurrency(),
      gifting_offer: data?.type === "gift_bundles" ?  
        "Gift Collection" : data?.type === "Evoucher" ?  
        "Gift Cards" : data?.type === "gift_workshop" ?  
        "Gift Workshop Experience" : 
        data?.name,
  }
  const PIIs = {
    email, name
  }
  if (!email) delete PIIs.email;
  sendSegmentIdentify(PIIs);
  sendSegmentData(eventName, data);
}


// ======GOOGLE-TAG-MANAGER========

export function initializeTracking() {
  const tagManagerArgs = {
    gtmId: KEY_GTM,
  };
  TagManager.initialize(tagManagerArgs);
}

export function trackGTMViewCart(data) {
  segmentCartViewed(data);
  sendCartViewedHotjarEvent();

  const { items, type } = data;
  const newItems = _.map(items, d => (
    {
      id: `${d?.item?.id}`,
      name: d.name,
      category: type?.name || d.name,
      quantity: d.quantity || 1,
      price: d.price,
    }
  ));

  const tagManagerArgs = {
    dataLayer: {
      event: 'checkout',
      checkout: {
        products: newItems,
      },
    },
  };

  TagManager.dataLayer(tagManagerArgs);
}

export function trackGTMViewItem(data) {
  segmentProductView(data);

  const { items, type } = data;
  const newItems = _.map(items, d => (
    {
      id: `${d.id}`,
      name: d.name,
      category: type?.name || d.name,
      quantity: d.quantity || 1,
      price: d.price,
    }
  ));

  const tagManagerArgs = {
    dataLayer: {
      event: 'detailProductView',
      ecommerce: {
        detail: {
          products: newItems,
        },
      },
    },
  };

  TagManager.dataLayer(tagManagerArgs);
}

export function trackGTMAddToCart(data, cartID) {
  addCartNamogoo(data.price);
  segmentProductAdded(data, cartID);
  sendAddToCartClickHotjarEvent();
  clickAddToCartAmplitude();

  const newItem = {
    id: data?.item?.id ? `${data.item.id}` : `${data.item}`,
    name: data.name,
    quantity: data.quantity,
    price: data.price,
  };
  const tagManagerArgs = {
    dataLayer: {
      event: 'addToCart',
      cartID,
      ecommerce: {
        currencyCode: auth.getNameCurrency(),
        add: {
          products: [newItem],
        },
      },
    },
  };
  TagManager.dataLayer(tagManagerArgs);
}

export function trackGTMRemoveCart(data) {
  segmentProductRemoved(data);

  const newItem = {
    id: `${data.id}`,
    name: data.name,
    quantity: data.quantity,
    price: data.price,
  };
  const tagManagerArgs = {
    dataLayer: {
      event: 'removeFromCart',
      ecommerce: {
        remove: {
          products: [newItem],
        },
      },
    },
  };

  TagManager.dataLayer(tagManagerArgs);
}

export function trackGTMPurchase(transaction) {
  const tagManagerArgs = {
    dataLayer: {
      ecommerce: transaction,
    },
  };

  TagManager.dataLayer(tagManagerArgs);
}

export function trackGTMEventPurchase(transactionTimeStamp, currentServerTimeStamp) {
  const tagManagerArgs = {
    dataLayer: {
      event: 'purchase',
      transactionTimeStamp,
      currentServerTimeStamp,
    },
  };

  TagManager.dataLayer(tagManagerArgs);
}

export function trackGTMLogin(method, userId) {
  const tagManagerArgs = {
    dataLayer: {
      event: 'login',
      login: [
        { method },
        { userId: `${userId}` },
      ],
    },
  };

  TagManager.dataLayer(tagManagerArgs);
}

export function trackGTMSubmitEmail(emailSubmited) {
  segmentSubscribedNewsletter(emailSubmited);

  const tagManagerArgs = {
    dataLayer: {
      event: 'getProfileByMail',
      emailSubmited,
    },
  };

  TagManager.dataLayer(tagManagerArgs);
}

export function trackGTMSignUp(method, userId) {
  const tagManagerArgs = {
    dataLayer: {
      event: 'sign_up',
      login: {
        method,
        userId: `${userId}`,
      },
    },
  };

  TagManager.dataLayer(tagManagerArgs);
}


export function trackGTMLogout(userId) {
  const tagManagerArgs = {
    dataLayer: {
      event: 'logout',
      logout: {
        userId: `${userId}`,
      },
    },
  };
  TagManager.dataLayer(tagManagerArgs);
}

export function trackGTMCheckOut(step, data) {
  const { items, type } = data || {};
  const newItems = _.map(items || [], d => (
    {
      id: `${d.id}`,
      name: d.name,
      category: type?.name || d.name,
      quantity: d.quantity || 1,
      price: d.price,
    }
  ));
  const tagManagerArgs = {
    dataLayer: {
      event: 'checkout',
      ecommerce: {
        checkout: {
          actionField: {
            step,
          },
          products: newItems,
        },
      },
    },
  };
  console.log('trackGTMCheckOut', tagManagerArgs);
  TagManager.dataLayer(tagManagerArgs);
}

export function generaCurrency(value, isFloat = false) {
  if (value === undefined) {
    return '';
  }
  const symbol = auth.getCurrency();
  const isLeft = auth.getLeftRightCurrency();
  const valueNew = isFloat ? parseFloat(value, 10) : parseInt(value, 10);
  const valueFormat = ['vn', 'kr'].includes(auth.getCountry()) && valueNew ? valueNew.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') : valueNew;
  return isLeft ? `${symbol} ${valueFormat}` : `${valueFormat} ${symbol}`;
}

export function getTextFromTextBlock(cms, index) {
  if (!cms) {
    return '';
  }
  if (cms.length > index) {
    return cms[index].value;
  }
  return '';
}

export function isCheckNull(data) {
  return !data || data === 'undefined' || data === 'null';
}

export function googleAnalitycs(link) {
  // ReactGA.pageview(link);
}

export function hiddenChatIcon() {
  const ele = document.getElementsByClassName('intercom-lightweight-app');
  if (ele && ele.length > 0) {
    ele[0].style.visibility = 'hidden';
  }
  const ele1 = document.getElementsByClassName('intercom-namespace');
  if (ele1 && ele1.length > 0) {
    ele1[0].style.visibility = 'hidden';
  }
}

export function visibilityChatIcon() {
  const ele = document.getElementsByClassName('intercom-lightweight-app');
  if (ele && ele.length > 0) {
    ele[0].style.visibility = 'visible';
  }
  const ele1 = document.getElementsByClassName('intercom-namespace');
  if (ele1 && ele1.length > 0) {
    ele1[0].style.visibility = 'visible';
  }
}


export function objectEquals(x, y) {
  if (x === y) return true;
  // if both x and y are null or undefined and exactly the same

  if (!(x instanceof Object) || !(y instanceof Object)) return false;
  // if they are not strictly equal, they both need to be Objects

  if (x.constructor !== y.constructor) return false;
  // they must have the exact same prototype chain, the closest we can do is
  // test there constructor.

  for (const p in x) {
    if (!x.hasOwnProperty(p)) continue;
    // other properties were tested using x.constructor === y.constructor

    if (!y.hasOwnProperty(p)) return false;
    // allows to compare x[ p ] and y[ p ] when set to undefined

    if (x[p] === y[p]) continue;
    // if they have the same strict value or identity then they are equal

    if (typeof (x[p]) !== 'object') return false;
    // Numbers, Strings, Functions, Booleans must be strictly equal

    if (!objectEquals(x[p], y[p])) return false;
    // Objects and Arrays must be tested recursively
  }

  for (const p in y) { if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) return false; }
  // allows x[ p ] to be set to undefined

  return true;
}

export const handleDownloadFile = async (url) => {
  try {
    const response = await axios.get(url, {
      responseType: 'arraybuffer',
    });
    const blob = new Blob([response.data], { type: 'image/png' });
    const urlImage = `${window.URL.createObjectURL(blob)}`;
    return urlImage;
  } catch (error) {
    console.error('Failed to download file', error.message);
  }
};

export const removeLinkHreflang = () => {
  const links = document.getElementsByClassName('hreflang');
  while (links.length > 0) {
    links[0].parentNode.removeChild(links[0]);
  }
};

export const removeMeta = (name) => {
  const links = document.getElementsByClassName(name);
  while (links.length > 0) {
    links[0].parentNode.removeChild(links[0]);
  }
};

export const generateHreflang = (countries, url) => {
  const ele = _.find(countries, x => x.code === auth.getCountry());
  const code = ele?.code && ele.code !== 'us' ? `/${ele.code}` : '';
  const baseUrl = 'https://shop.maison21g.com';
  const homeUrl = `${baseUrl}${code}`;
  const newUrl = `${baseUrl}${code}${url}`;
  const defaultUrl = `${baseUrl}${url}`;

  return (
    <React.Fragment>
      <link rel="home" href={homeUrl} className="hreflang" />
      <link rel="canonical" href={newUrl} className="hreflang" />
      <link rel="alternate" href={defaultUrl} hrefLang="x-default" className="hreflang" />
      {
        _.map(countries || [], x => (
          x.code.toUpperCase() === 'US'
            ? <link rel="alternate" className="hreflang" href={`${baseUrl}${url}`} hrefLang={`${x.language.code}-${x.code.toUpperCase()}`} />
            : <link rel="alternate" className="hreflang" href={`${baseUrl}/${x.code}${url}`} hrefLang={`${x.language.code}-${x.code.toUpperCase()}`} />
        ))
      }
    </React.Fragment>
  );
};


export const scrollToTargetAdjusted = (id) => {
  const element = document.getElementById(id);
  console.log('scrollToTargetAdjusted', id, element);
  if (element) {
    element.scrollIntoView({ behavior: 'smooth' });
  }
};

export const addClassIntoElemenetID = (id, className) => {
  const element = document.getElementById(id);
  if (element) {
    element.classList.add(className);
  }
};

export const removeClassIntoElemenetID = (id, className) => {
  const element = document.getElementById(id);
  if (element) {
    element.classList.remove(className);
  }
};

export const parseToUTC = (date, format) => {
  const m = moment.utc(date, format);
  return m;
};

export const getProductDualCrayonCombo = async (id1, id2) => {
  const option = {
    url: id1 === id2 ? `${GET_PRODUCT_FOR_CART}?combo=${id1}&type=dual_crayons` : `${GET_PRODUCT_FOR_CART}?combo=${id1}${`,${id2}`}&type=dual_crayons`,
    method: 'GET',
  };
  return fetchClient(option);
};

export const changeUrl = (symbol) => {
  let newUrl = window.location.pathname;
  if (window.location.pathname.length > 0 && window.location.pathname.charAt(window.location.pathname.length - 1) === '/') {
    newUrl = window.location.pathname.substring(0, window.location.pathname.length - 1);
  }
  const array = window.location.pathname.split('/');
  if (array && array.length > 1 && array[1].length === 2) {
    newUrl = newUrl.replace(array[1], symbol);
  } else if (window.location.pathname === '/') {
    newUrl = `/${symbol}`;
  } else {
    newUrl = `/${symbol}${newUrl}`;
  }
  if (newUrl !== window.location.pathname) {
    if (window.location.search) {
      newUrl = `${newUrl}${window.location.search}`;
    }
    document.location.href = newUrl;
  }
};

export const setScriptIntoDocument = (id, innerHTML) => {
  const header = document.getElementsByTagName('head')[0];
  const newEle = document.createElement('script');
  newEle.type = 'application/ld+json';
  newEle.innerHTML = innerHTML;
  newEle.async = true;
  newEle.id = id;
  const oldEle = document.getElementById(id);
  if (oldEle) {
    console.log('replaceChild');
    header.replaceChild(newEle, oldEle);
  } else {
    console.log('replaceChild appendChild');
    header.appendChild(newEle);
  }
};

export const getPrice = (type) => {
  try {
    const option = {
      url: GET_PRICES_PRODUCTS.replace('{type}', type),
      method: 'GET',
    };
    return fetchClient(option);
  } catch (error) {
    toastrError('error', error.message);
  }
};

export const isAllGift = (data) => {
  const ele = _.find(data, d => d.item.product.type.name !== 'Gift');
  return !ele;
};
export const fetchTokenSimplyBook = async () => {
  try {
    const api = new JsonRpcClient({
      endpoint: `${SIMPLY_BOOK_URL}login`,
    });
    const response = await api.request(
      'getToken',
      getCompanySimplyBook(),
      getKeyApiSimplyBook(),
    );
    localStorage.setItem('tokenSimplyBook', response);
    return response;
  } catch (error) {
    console.log('fetchTokenSimplyBook', error)
    return null;
  }
};

export const fetchTokenSimplyBookAdmin = async () => {
  try {
    const options = {
      method: 'GET',
      url: GET_SIMPLYBOOK_TOKEN_ADMIN,
      headers: {
        'Content-Type': 'application/json',
        'accept-country': getCountry(),
      },
    };
    const response = await axios(options);
    console.log('fetchTokenSimplyBookAdmin', response.data.token);
    if (response.data.token) {
      localStorage.setItem('tokenSimplyBookAdminToken', response.data.token);
    } else {
      toastrError(response.data.message);
    }
    return response.data.token;
  } catch (error) {
    console.log('fetchTokenSimplyBookAdmin', error)
    return null;
  }
};

export const setPrerenderReady = () => {
  setTimeout(() => {
    window.prerenderReady = true;
  }, 100);
};

export const addFontCustom = () => {
  const pathName = window.location.pathname;
  const array = pathName.split('/');
  if (array && array.length > 1) {
    if (['vn', 'kr'].includes(array[1])) {
      return 'f-open-sans';
    }
    if (['sa'].includes(array[1])) {
      return 'f-PFDinTextAR';
    }
  }
  return '';
};

export const getWidthElement = (id) => {
  const ele = document.getElementById(id);
  if (ele) {
    return ele.offsetWidth;
  }
  return 0;
};


export const preDataGroup = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { tags } = x;
    _.forEach(tags, (tag) => {
      const group = _.find(dataScents, d => d.tag === tag.name);
      if (group) {
        group.datas.push(x);
      } else {
        dataScents.push({
          tag: tag.name,
          group: tag.group,
          datas: [x],
        });
      }
    });
  });
  return dataScents;
};

export const preDataAlphabet = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { name } = x;
    const firstChart = name.charAt(0);
    const group = _.find(dataScents, d => d.tag.toLowerCase() === firstChart.toLowerCase());
    if (group) {
      group.datas.push(x);
    } else {
      dataScents.push({
        tag: firstChart,
        datas: [x],
      });
    }
  });
  return dataScents;
};

export const preDataScents = (scents) => {
  const dataScents = [];

  dataScents.push({
    datas: _.orderBy(preDataGroup(scents), ['tag'], ['asc']),
    title: ['Scent Family', 'Gender', 'mood'],
  });

  dataScents.push({
    datas: _.orderBy(preDataAlphabet(scents), ['tag'], ['asc']),
    title: ['Alphabet'],
  });

  dataScents.push({
    datas: [{
      tag: '',
      datas: _.orderBy(scents, ['popularity'], ['desc']),
    }],
    title: ['Popularity'],
  });
  return dataScents;
};

export const getDiscountSaveStore = () => {
  const discountCode = new URLSearchParams(window.location?.search || '').get('discount_code');
  if (discountCode) {
    auth.setDiscountCode(discountCode);
  }
};

export const isDobValid = (value) => {
  const momentValue = moment(value, 'DD/MM/YYYY');
  if (momentValue && value?.length === 10
    && momentValue.isValid()
  && momentValue < moment()
  && momentValue > moment('01/01/1900')) {
    return true;
  }
  return false;
};

export const isValidateValue = (value) => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(value).toLowerCase());
};

export const isRightAlignedText = () => ['sa'].includes(auth.getCountry());

export const gotoHomeAllProduct = (history) => {
  window.location.href = getLinkHome();
};

export const getLinkHome = () => {
  const symbolUrl = getCodeUrl();
  if (!symbolUrl || symbolUrl === 'us') {
    if(window.location.href.includes('maison21g')) {
      return 'https://www.maison21g.com';
    }
    return 'https://scentdesigner.stage.23c.se'
  }
  if(window.location.href.includes('maison21g')) {
    return `https://www.maison21g.com/${symbolUrl}`;
  }
  return `https://scentdesigner.stage.23c.se/${symbolUrl}`;
};

export const gotoShopHome = () => {
  const link = getLinkHome();
  window.location.href = link;
};

export const getLink = (url) => {
  if (url && url.includes('https://')) {
    return url;
  }
  return generateUrlWeb(url);
};

// ======HOTJAR========
export const sendHotjarEvent = (eventName) => {
  if (window?.hj) {
    window.hj('event', eventName);
    console.log(`Hotjar - ${eventName}`);
  } 
  
  if (!window?.hj) {
    console.log(`${eventName} --- DEV`);
  }
}
//  --- Hotjar Checkout Events
export const sendInitiateCheckoutClickHotjarEvent = () => {
  sendHotjarEvent('Initiate Checkout');
}

export const sendCheckoutStepViewedHotjarEvent = () => {
  sendHotjarEvent('Checkout Step Viewed');
}

export const sendCustomerDetailsEnteredClickHotjarEvent = () => {
  sendHotjarEvent('Customer Details Entered');
}

export const sendShippingInfoEnteredClickHotjarEvent = () => {
  sendHotjarEvent('Shipping Info Entered');
}

export const sendPaymentInfoEnteredClickHotjarEvent = () => {
  sendHotjarEvent('Payment Info Entered');
}

export const sendOrderCompletedViewedHotjarEvent = () => {
  sendHotjarEvent('Order Completed');
}

/**
 * Shopping Events
 * 1. Select Scent 1
 * 2. Select Scent 2
 * 3. Click - Engrave Bottle
 * 4. Click - Add To Cart
 * 5. Cart View -- /cart
 */

export const sendSelectScentXClickHotjarEvent = (index) => {
  if (index === 1) {
    sendHotjarEvent('Select Scent 1');
  }
  if (index === 2) {
    sendHotjarEvent('Select Scent 2');
  }
}


export const sendEngraveBottleClickHotjarEvent = () => {
  sendHotjarEvent('Engrave Bottle');
}

export const sendAddToCartClickHotjarEvent = () => {
  sendHotjarEvent('Add To Cart');
}

export const sendCartViewedHotjarEvent = () => {
  sendHotjarEvent('Cart Viewed');
}

export const sendScentDiscoverySetViewedHotjarEvent = () => {
  //note: see upper implementation => segmentProductView() in this file
  sendHotjarEvent('Scent Discovery Set Viewed');
}

export const sendPerfumeInspirationPerfumeSelectedHotjarEvent = () => {
  sendHotjarEvent('Perfume Inspiration - Perfume Selected');
}
