import React, { Component } from 'react';
import '../../styles/style.scss';
import '../../styles/result-new.scss';
import '../../styles/result-scent.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Fade } from 'react-reveal';
import {
  withOrientationChange,
} from 'react-device-detect';
import { Modal, ModalBody } from 'reactstrap';

import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import { PUT_OUTCOME_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import loadingPage from '../../Redux/Actions/loading';
import addCmsRedux from '../../Redux/Actions/cms';
import {
  addProductBasket, createBasketGuest,
} from '../../Redux/Actions/basket';
import icSearch from '../../image/icon/ic-search-black.svg';
import {
  getNameFromButtonBlock, fetchCMSHomepage, getTextFromTextBlock, getNameFromCommon, getCmsCommon, isCheckNull, generateUrlWeb, setPrerenderReady, addFontCustom, getSearchPathName,
} from '../../Redux/Helpers';
import icClose from '../../image/icon/close-product.svg';
import AdjectiveMobile from '../../components/Personality/AdjectiveMobile';
import Adjective from '../../components/Personality/Adjective';
import icEmail from '../../image/icon/ic-email-input.svg';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import auth from '../../Redux/Helpers/auth';
import { isBrowser, isMobile, isMobileOnly } from '../../DetectScreen';
import FooterV2 from '../../views2/footer';

const getCms = (cms) => {
  if (cms) {
    const { body } = cms;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    const textBlock = _.filter(body || [], x => x.type === 'text_block');
    return {
      buttonBlocks,
      textBlock,
    };
  }
  return {
    buttonBlocks: [],
    textBlock: [],
  };
};
class ResultSubQuiz extends Component {
  constructor(props) {
    super(props);
    this.idOutCome = props.match && props.match.params ? props.match.params.idOutCome : undefined;
    this.state = {
      dataOutCome: undefined,
      isShowDescription: false,
      buttonBlocks: [],
      textBlock: [],
      isShowInfo: false,
      isViewMore: false,
      email: '',
      secondCount: 5,
    };
    this.maxChar = isMobile && !props.isLandscape ? 140 : 150;
  }

  componentDidMount() {
    setPrerenderReady();
    this.fetchCms();
    this.fetchData();
  }

  componentWillUnmount() {
    if (this.intervalSecound) {
      clearInterval(this.intervalSecound);
    }
  }

  startInterVal = () => {
    this.intervalSecound = setInterval(() => {
      const { secondCount } = this.state;
      this.setState({ secondCount: secondCount - 1 }, () => {
        if (secondCount === 0) {
          this.onClickCheckout();
        }
      });
    }, 1000);
  }

  fetchCms = async () => {
    const { cms } = this.props;
    const cmsProduct = _.find(cms, x => x.title === 'Personality Outcome');
    if (!cmsProduct) {
      const cmsData = await fetchCMSHomepage('personality-outcome');
      this.props.addCmsRedux(cmsData);
      const dataCms = getCms(cmsData);
      this.setState(dataCms);
    } else {
      const dataCms = getCms(cmsProduct);
      this.setState(dataCms);
    }
  }

  fetchData = () => {
    this.props.loadingPage(true);
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', this.idOutCome),
      method: 'GET',
    };
    fetchClient(options, true).then((result) => {
      this.props.loadingPage(false);
      this.setState({ dataOutCome: result }, () => {
        this.startInterVal();
      });
    }).catch((err) => {
      this.props.loadingPage(false);
      console.log('err', err);
    });
  }

  onClickCheckout = () => {
    const { dataOutCome } = this.state;
    const { pathname, search } = getSearchPathName(generateUrlWeb('/checkout-sub-quiz'));
    this.props.history.push(
      {
        pathname,
        search,
        state: { item: dataOutCome.item },
      },
    );
  }

  onChangeEmail = (e) => {
    const { value } = e.target;
    this.setState({ email: value });
  }

  onClickSend = () => {
    const { email } = this.state;
    if (!this.isValidateValue(email)) {
      toastrError('Please enter the email!');
      return;
    }
    if (isCheckNull(this.idOutCome)) {
      return;
    }
    // this.assignEmailToCart();
    const option = {
      url: PUT_OUTCOME_URL.replace('{id}', this.idOutCome),
      method: 'PUT',
      body: {
        email,
        send_email: true,
      },
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        auth.setEmail(email);
        toastrSuccess('Sending Email is successful!');
        this.props.loadingPage(false);
        return;
      }
      throw new Error('Sending Email is failed!');
    }).catch((err) => {
      toastrError(err.message);
      this.props.loadingPage(false);
    });
  }

  render() {
    const {
      dataOutCome, isShowDescription, buttonBlocks, isShowInfo, isViewMore, textBlock, secondCount,
    } = this.state;
    const { isLandscape, cms } = this.props;
    if (!dataOutCome) {
      return null;
    }
    const cmsCommon = getCmsCommon(cms);
    const moreBt = getNameFromButtonBlock(buttonBlocks, 'more_personality_info');
    const congurationBt = getNameFromButtonBlock(buttonBlocks, 'Congratulations_your_are_the');
    const congurationYourBt = getNameFromButtonBlock(buttonBlocks, 'Congratulations_your_custom_perfume_are');
    const textBt = getNameFromButtonBlock(buttonBlocks, 'text_description_sub_quiz');
    const getFreeBt = getNameFromButtonBlock(buttonBlocks, 'GET_FREE_PERFUME');
    const reDirectBt = getNameFromButtonBlock(buttonBlocks, 'Redirecting_you');
    const enterEmailBt = getNameFromCommon(cmsCommon, 'Enter_your_email');
    const sendBt = getNameFromCommon(cmsCommon, 'SEND');
    const { personality_choice: personalityChoice, item } = dataOutCome;
    const { personality } = personalityChoice;
    const textDescription = personalityChoice ? isViewMore && personalityChoice.personality.description.length > this.maxChar ? personalityChoice.personality.description.substring(0, this.maxChar) : personalityChoice.personality.description : '';
    console.log('item.image', item.image);
    return (
      <div className="div-result-scent">
        {/* <HeaderHomePage visible={false} isSpecialMenu isDisableScrollShow isNotShowRegion /> */}
        <HeaderHomePageV3 isRemoveMessage />
        <div className="div-result-sub-quiz">
          <div className="div-result-content">
            <h1 className="text-header-sub-quiz">
              {congurationBt}
            </h1>
            <div className="div-header-text">
              <div
                className="text"
                onMouseEnter={isBrowser ? () => this.setState({ isShowDescription: true }) : null}
                onMouseLeave={isBrowser ? () => this.setState({ isShowDescription: false }) : null}
              >
                <img loading="lazy" src={personality ? personality.image : ''} alt="icon" />
                <div className="div-text-name">
                  <div className={`des-text-tx ${isShowDescription ? 'show' : ''}`} style={{ color: `#${personality.color}` }}>
                    {personalityChoice ? personalityChoice.personality.description : ''}
                  </div>
                  <span className={`title-tx ${!isShowDescription ? 'show' : ''}`} style={{ color: `#${personality.color}` }}>
                    {personalityChoice ? `${personalityChoice.personality.title}` : ''}
                  </span>
                </div>
              </div>
              {
                isMobile && !isLandscape ? (
                  <span className="des-text" style={{ color: `#${personality.color}` }}>
                    {personalityChoice ? personalityChoice.personality.description : ''}
                  </span>
                ) : (null)
              }

              <button
                data-gtmtracking="funnel-1-step-15-more-personality-info"
                type="button"
                onClick={() => this.setState({ isShowInfo: true })}
              >
                <img
                  data-gtmtracking="funnel-1-step-15-more-personality-info"
                  src={icSearch}
                  alt="search"
                />
                {moreBt}
              </button>
            </div>
            <div className="div-product">
              <img loading="lazy" src={item.image} alt="scent" />
              <span className="text-name">
                {congurationYourBt}
                {' '}
                <b>{item.name}</b>
              </span>
              <span className="text-des">
                {textBt}
              </span>
              <button type="button" onClick={this.onClickCheckout}>
                {getFreeBt}
              </button>
              <span className="text-redirect">{reDirectBt.replace('{sec}', secondCount)}</span>
            </div>
          </div>
        </div>
        {
          isShowInfo ? (
            <Modal className={`modal-full-screen ${addFontCustom()}`} isOpen>
              <ModalBody>
                <div className="div-full-popup animated fadeIn faster">
                  <div className="div-card-popup-result-quiz">
                    <button
                      className="bt-close"
                      type="button"
                      onClick={() => this.setState({ isShowInfo: false })
            }
                    >
                      <img src={icClose} alt="close" />
                    </button>
                    <Fade delay={1000}>
                      <div className="div-result-new div-col">
                        <div className="div-send-email div-col">
                          {/* <h1 className="tc mt-2">
                            {headerBlock && headerBlock.value ? headerBlock.value.header_text : ''}
                          </h1> */}
                          <Fade delay={1000} top>
                            <div className={isMobileOnly ? 'header div-row mb-2 justify-center items-center' : 'header div-row my-3 justify-center items-center'}>
                              <img loading="lazy" src={personality ? personality.image : ''} alt="authentic" />
                              <h2 className="tc" style={{ color: `#${personality.color}` }}>
                                {personalityChoice ? personalityChoice.personality.title : ''}
                              </h2>
                            </div>
                          </Fade>
                          <span className={isMobile && !isLandscape ? 'tc description' : 'hidden'}>
                            <span>
                              {textDescription}
                            </span>
                            <br />
                          </span>
                          {isMobileOnly
                            ? (
                              <AdjectiveMobile
                                leftTitle={getTextFromTextBlock(textBlock, 16)}
                                rightTitle={getTextFromTextBlock(textBlock, 17)}
                                adjectives={personalityChoice ? personalityChoice.personality.adjectives : []}
                                scents={personalityChoice ? personalityChoice.personality.scents : []}
                              />
                            )
                            : (
                              <Adjective
                                leftTitle={getTextFromTextBlock(textBlock, 16)}
                                rightTitle={getTextFromTextBlock(textBlock, 17)}
                                adjectives={personalityChoice ? personalityChoice.personality.adjectives : []}
                                scents={personalityChoice ? personalityChoice.personality.scents : []}
                              />
                            )}
                          <Fade delay={1000}>
                            <span className={isMobile && !isLandscape ? 'hidden' : 'tc w-80 description'}>
                              <span>
                                {textDescription}
                              </span>
                              <br />
                            </span>
                          </Fade>
                          {!this.props.login.user && (
                          <span className="tc text-title-input">
                            {
                          getTextFromTextBlock(textBlock, 14)
                        }
                          </span>
                          )}
                          {/* {this.props.login.user && this.namePath === 'quiz' && (
                  <span className="tc text-title-input">
                    {
                      getTextFromTextBlock(textBlock, 15)
                    }
                  </span>
                  )} */}

                          <div className={isMobileOnly ? 'div-input div-col' : 'div-input div-row'}>
                            { !this.props.login.user && (
                            <div className="div-email div-row">
                              <img src={icEmail} alt="email" />
                              <input
                                type="email"
                                name="email"
                                value={this.state.email}
                                placeholder={enterEmailBt}
                                onChange={this.onChangeEmail}
                              />
                            </div>
                            )}
                            <button
                              type="button"
                              onClick={this.onClickSend}
                              className={this.props.login.user && this.namePath !== 'boutique-quiz' && this.namePath !== 'krisshop' ? 'hidden' : ''}
                            >
                              {sendBt}
                            </button>
                          </div>
                          {/* {(!this.props.login.user || this.namePath === 'boutique-quiz' || this.namePath === 'partnership-quiz') && (
                            <span
                              className={this.namePath === 'quiz' ? 'tc text-continue' : 'hidden'}
                              onClick={() => this.setState({ stepInstore: 2 })
                              }
                            >
                              Continue without saving
                            </span>
                          )} */}
                        </div>
                      </div>
                    </Fade>
                  </div>
                </div>
              </ModalBody>
            </Modal>
          ) : (null)
        }
        <FooterV2 />
      </div>

    );
  }
}

ResultSubQuiz.propTypes = {
  match: PropTypes.shape().isRequired,
};

function mapStateToProps(state) {
  return {
    basket: state.basket,
    cms: state.cms,
    login: state.login,
  };
}

const mapDispatchToProps = {
  createBasketGuest,
  addProductBasket,
  addCmsRedux,
  loadingPage,
};

export default connect(mapStateToProps, mapDispatchToProps)(withOrientationChange(ResultSubQuiz));
