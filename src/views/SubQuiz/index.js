import React, { Component } from 'react';
import '../../styles/style.scss';
import _ from 'lodash';
import { connect } from 'react-redux';

import '../../styles/sub-quiz.scss';
import HeaderHomePage from '../../components/HomePage/header';
import QuestionGender from '../../components/StepQuestion/questionGender';
import updateAnswersData from '../../Redux/Actions/answers';
import updateQuestionsData from '../../Redux/Actions/questions';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import {
  fetchAnswers, fetchQuestions, fetchCMSHomepage, getCmsCommon, getNameFromCommon, isCheckNull, getNameFromButtonBlock, scrollTop, generateUrlWeb, setPrerenderReady,
} from '../../Redux/Helpers';
import { toastrError } from '../../Redux/Helpers/notification';
import QuestionYesNo from '../../components/StepQuestion/questionYesNo';
import { POST_OUTCOMES_URL, PUT_OUTCOME_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import icWatch from '../../image/icon/ic-watch.svg';
import { isBrowser } from '../../DetectScreen';
import FooterV2 from '../../views2/footer';

const questionFree = [
  {
    type: 'qtGender',
    id: 'qt0',
    no: 0,
    title: 'What is your gender ?',
    name: 'gender',
    answer1: 'Male',
    answer2: 'Female',
    answer3: 'Unisex',
  },
];

const getCms = (cms) => {
  if (cms) {
    const { body, image_background: imageBackground } = cms;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    return {
      buttonBlocks,
      imageBackground,
    };
  }
  return {
    buttonBlocks: [], imageBackground: undefined,
  };
};

class SubQuiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listQuestion: [],
      buttonBlocks: [],
      imageBackground: undefined,
    };
    this.result = [];
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    this.initOutCome();
    this.fetchData();
    this.fetchCMS();
  }

  initOutCome = async () => {
    try {
      const option = {
        url: POST_OUTCOMES_URL,
        method: 'POST',
        body: {},
      };
      const result = await fetchClient(option, true);
      this.idOutCome = result.id;
    } catch (error) {
      console.log('error', error);
    }
  }

  fetchCMS = async () => {
    const { cms } = this.props;
    const cmsPersonal = _.find(cms, x => x.title === 'Personality');
    if (!cmsPersonal) {
      const result = await fetchCMSHomepage('personality');
      this.props.addCmsRedux(result);
      const dataCms = getCms(result);
      this.setState(dataCms);
    } else {
      const dataCms = getCms(cmsPersonal);
      this.setState(dataCms);
    }
  }

  fetchData = async () => {
    const {
      answers, questions,
    } = this.props;
    if (answers.length === 0 || questions.length === 0) {
      this.props.loadingPage(true);
      const pending = [fetchAnswers(), fetchQuestions()];
      try {
        const results = await Promise.all(pending);
        this.props.updateAnswersData(results[0]);
        this.props.updateQuestionsData(results[1]);
        this.props.loadingPage(false);
      } catch (error) {
        toastrError(error.message);
        this.props.loadingPage(false);
      }
    }
  }

  tranlateQuestion = () => {
    const cmsCommon = getCmsCommon(this.props.cms);
    const whatIsYourGenderBt = getNameFromCommon(cmsCommon, 'What_is_your_gender');
    const maleBt = getNameFromCommon(cmsCommon, 'Male');
    const femaleBt = getNameFromCommon(cmsCommon, 'Female');
    const genderFreeBt = getNameFromCommon(cmsCommon, 'Gender-free');
    questionFree[0].title = whatIsYourGenderBt;
    questionFree[0].answer1 = maleBt;
    questionFree[0].answer2 = femaleBt;
    questionFree[0].answer3 = genderFreeBt;
  }

  addResult = (value, name, no, id, idAnswer) => {
    const item = _.find(this.result, x => x.name === name);
    if (item) {
      _.assign(item, {
        value, name, no, id, idAnswer,
      });
    } else {
      this.result.push({
        value, name, no, id, idAnswer,
      });
    }
    this.result = this.result.slice(0, no + 1);
    console.log('this.result', this.result);
  }

  onChange = (value, name, no, id, idAnswer) => {
    if (!this.idOutCome) {
      return;
    }
    const { listQuestion } = this.state;
    this.addResult(value, name, no, id, idAnswer);
    this.makeBodyPost();
    const newList = listQuestion.slice(0, no + 1);
    const newQuestion = this.getQuestion(no + 1);
    if (newQuestion) {
      newList.push(newQuestion);
    }
    this.setState({ listQuestion: newList }, () => {
      this.scrollToElement(`qt${no + 1}`);
    });
  }

  onClickStart = () => {
    const { listQuestion } = this.state;
    if (listQuestion.length === 0) {
      listQuestion.push(questionFree[0]);
      this.setState({ listQuestion }, () => {
        this.scrollToElement('qt0');
      });
    }
  }

  getQuestion = (noTotal) => {
    const { answers } = this.props;
    const no = noTotal - questionFree.length;
    let parent;
    if (no === 0) {
      parent = answers;
    } else {
      let temp = answers;
      for (let i = 0; i < no; i += 1) {
        temp = _.find(temp, x => x.title === this.result[i + questionFree.length].value).children;
      }
      parent = temp;
    }
    if (parent && parent.length > 1) {
      const object = {
        idAnswer1: parent[0].id,
        idAnswer2: parent[1].id,
        type: 'qtYesNo',
        id: `qt${noTotal}`,
        no: noTotal,
        title: parent[0].question,
        name: `persional${no + 1}`,
        answer1: parent[0].title,
        persionalId1: parent[0].personality ? parent[0].personality.id : '',
        type1: parent[0].title.charAt(0).toUpperCase(),
        answer2: parent[1].title,
        type2: parent[1].title.charAt(0).toUpperCase(),
        persionalId2: parent[1].personality ? parent[1].personality.id : '',
      };
      return object;
    }
    return null;
  }

  scrollToElement = (id) => {
    const yOffset = -110;
    const element = document.getElementById(id);
    if (element) {
      const y = element.getBoundingClientRect().top + window.pageYOffset + yOffset;
      window.scrollTo({ top: y, behavior: 'smooth' });
    }
  };

  makeBodyPost = () => {
    const answers = [];
    const gender = _.find(this.result, x => x.name === 'gender');
    for (let i = 1; i < this.result.length; i += 1) {
      if (this.result[i].idAnswer) {
        answers.push(this.result[i].idAnswer);
      }
    }
    this.sendRealTimeData({
      gender: gender ? (gender.value.toLowerCase() === 'male' ? 1 : gender.value.toLowerCase() === 'female' ? 2 : 3) : undefined,
      answers: answers.length > 0 ? answers : undefined,
    });
  }

  sendRealTimeData = (body) => {
    if (!isCheckNull(this.idOutCome)) {
      const options = {
        url: PUT_OUTCOME_URL.replace('{id}', this.idOutCome),
        method: 'PUT',
        body,
      };
      fetchClient(options, true).then(() => {
        if (body.answers && body.answers.length === 5) {
          this.props.history.push(generateUrlWeb(`/sub-quiz/outcome/${this.idOutCome}`));
        }
      });
    }
  }

  render() {
    const { listQuestion, buttonBlocks, imageBackground } = this.state;
    const take5Bt = getNameFromButtonBlock(buttonBlocks, 'Take_this_5_second');
    const gettryBt = getNameFromButtonBlock(buttonBlocks, 'get_a_customized_scent_trial');
    const time5 = getNameFromButtonBlock(buttonBlocks, '5_Seconds');
    const getFreeBt = getNameFromButtonBlock(buttonBlocks, 'get_free_perfume');
    this.tranlateQuestion();
    return (
      <div>
        <HeaderHomePage isNotShowRegion />
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <div className="div-sub-quiz">
          <div className="div-start-page">
            <h1>
              {isBrowser ? gettryBt : take5Bt}
            </h1>
            <span>
              {isBrowser ? take5Bt : gettryBt}
            </span>
            <div className="div-button-start">
              {
                isBrowser && (
                  <div className="div-time">
                    <img src={icWatch} alt="watch" />
                    <span>{time5}</span>
                  </div>
                )
              }
              <button type="button" onClick={this.onClickStart}>
                {getFreeBt}
              </button>
            </div>

          </div>
          <img loading="lazy" src={imageBackground} className="img-bg" alt="bg" />
          <div className="div-question">
            {
              _.map(listQuestion, (question) => {
                switch (question.type) {
                  case 'qtGender':
                    return (
                      <QuestionGender
                        id={question.id}
                        idAnswer1={question.idAnswer1}
                        idAnswer2={question.idAnswer2}
                        idAnswer3={question.idAnswer3}
                        name={question.name}
                        no={question.no}
                        title={question.title}
                        answer1={question.answer1}
                        persionalId1={question.persionalId1}
                        answer2={question.answer2}
                        persionalId2={question.persionalId2}
                        answer3={question.answer3}
                        persionalId3={question.persionalId3}
                        onChange={this.onChange}
                        isButtonYesNo
                      />
                    );
                  case 'qtYesNo':
                    return (
                      <QuestionYesNo
                        id={question.id}
                        idAnswer1={question.idAnswer1}
                        idAnswer2={question.idAnswer2}
                        name={question.name}
                        no={question.no}
                        title={question.title}
                        answer1={question.answer1}
                        persionalId1={question.persionalId1}
                        answer2={question.answer2}
                        persionalId2={question.persionalId2}
                        onChange={this.onChange}
                        subAnswer1={question.subAnswer1}
                        subAnswer2={question.subAnswer2}
                        defaultValue={this.result.length > question.no ? this.result[question.no].idAnswer : 0}
                        isSubQuiz
                      />
                    );
                  default:
                    return (<div />);
                }
              })
            }
          </div>
        </div>
        <FooterV2 />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    answers: state.answers,
    questions: state.questions,
    login: state.login,
    cms: state.cms,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  updateAnswersData,
  updateQuestionsData,
  addCmsRedux,
  loadingPage,
};


export default connect(mapStateToProps, mapDispatchToProps)(SubQuiz);
