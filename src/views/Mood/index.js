/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import MetaTags from 'react-meta-tags';

import HeaderHomePage from '../../components/HomePage/header';
import ButtonMood from '../../components/Mood/button';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { POST_OUTCOMES_URL } from '../../config';
import {
  scrollTop, getAllMood, getSEOFromCms, fetchCMSHomepage, googleAnalitycs, generateHreflang, removeLinkHreflang,
} from '../../Redux/Helpers';
import bg from '../../image/flower-bg.png';
import { toastrError } from '../../Redux/Helpers/notification';
import updateMoodsData from '../../Redux/Actions/moods';
import loadingPage from '../../Redux/Actions/loading';
import addCmsRedux from '../../Redux/Actions/cms';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import { isBrowser, isTablet } from '../../DetectScreen';
import '../../styles/mood-new.scss';
import FooterV2 from '../../views2/footer';

const prePareCms = (cms) => {
  if (cms) {
    const seo = getSEOFromCms(cms);
    const { body, header_text: headerText } = cms;
    const textBlock = _.find(body, x => x.type === 'text_block');
    return { headerText, textBlock, seo };
  }
  return { headerText: '', textBlock: {}, seo: {} };
};

class Mood extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mood: undefined,
      rotate: 0,
      isShowKnowMore: false,
      dataPopup: {
        title: '',
        content: '',
        imageKnowMore: '',
      },
      headerText: '',
      textBlock: {},
      seo: {},
      isShowIngredient: false,
      ingredientDetail: undefined,
    };
    this.tempRotate = 0;
  }

  componentDidMount = () => {
    scrollTop();
    document.addEventListener('scroll', this.handleScroll);
    googleAnalitycs('/mood');
    this.fetchDataInit();
  };

  componentWillUnmount() {
    removeLinkHreflang();
    if (window && this.handleScroll) {
      window.removeEventListener('scroll', this.handleScroll);
    }
  }

  onCloseKnowMore = () => {
    this.setState({ isShowKnowMore: false });
  }

  onOpenKnowMore = (data) => {
    const dataPopup = {
      title: data.name,
      content: data.description,
      imageKnowMore: data.imageKnowMore,
    };
    this.setState({ isShowKnowMore: true, dataPopup });
  }

  onClickMood = (id) => {
    const { moods } = this.props;
    const mood = _.find(moods, x => x.id === id);
    if (mood) {
      this.setState({ mood });
    }
    this.updateInfo(id);
  }

  fetchDataInit = async () => {
    const { moods, cms } = this.props;
    const moodCms = _.find(cms, x => x.title === 'Mood');
    if (!moods || moods.length === 0 || (_.isEmpty(moodCms))) {
      this.props.loadingPage(true);
      const pending = [fetchCMSHomepage('mood'), getAllMood()];
      try {
        const results = await Promise.all(pending);
        const cmsData = results[0];
        const result = results[1];
        this.props.addCmsRedux(cmsData);
        this.props.updateMoodsData(result);
        const { headerText, seo, textBlock } = prePareCms(cmsData);
        this.setState({ headerText, seo, textBlock });
        this.props.loadingPage(false);
      } catch (error) {
        toastrError(error.message);
        this.props.loadingPage(false);
      }
    } else {
      const cmsT = _.find(cms, x => x.title === 'Mood');
      const { headerText, seo, textBlock } = prePareCms(cmsT);
      this.setState({ headerText, seo, textBlock });
    }
  }

  handleScroll = () => {
    let temp = this.state.rotate;
    if (window.scrollY > this.oldY) {
      temp += 1;
    } else {
      temp -= 1;
    }
    this.oldY = window.scrollY;
    this.setState({ rotate: temp });
  }

  updateInfo = (mood) => {
    const body = {
      mood,
    };
    fetchClient({
      url: POST_OUTCOMES_URL,
      method: 'POST',
      body,
    }, true);
  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  render() {
    const { moods } = this.props;
    const {
      rotate, mood, isShowKnowMore, dataPopup,
      headerText, seo, textBlock, isShowIngredient, ingredientDetail,
    } = this.state;
    let products;
    if (mood) {
      products = mood.products;
    }
    const textRotate1 = `${rotate}deg`;
    const textRotate2 = `${-rotate}deg`;
    return (
      <div className="div-mood" onScroll={this.handleScroll}>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/mood')}
        </MetaTags>
        <HeaderHomePage />
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
        />
        {/* <CardKnowMore onCloseKnowMore={this.onCloseKnowMore} isShowKnowMore={isShowKnowMore} data={dataPopup} /> */}
        <div className="div-body">
          {
            isBrowser || isTablet
              ? (<img className="img-left" style={isTablet ? { transform: `rotate(${textRotate1})`, left: '-59%' } : { transform: `rotate(${textRotate1})` }} src={bg} alt="bg" />)
              : (<div />)
          }
          {
            isBrowser || isTablet ? (<img className="img-right" style={isTablet ? { transform: `rotate(${textRotate1})`, right: '-59%' } : { transform: `rotate(${textRotate2})` }} src={bg} alt="bg" />)
              : (<div />)
          }
          <h1 className="title">
            {headerText}
          </h1>
          <span className="description">
            {textBlock.value}
          </span>
          <div className="div-button">
            {
              _.map(moods, m => (<ButtonMood products={products} data={m} onClick={this.onClickMood} onClickIngredient={this.onClickIngredient} />))
            }
          </div>
          <FooterV2 />
        </div>

      </div>
    );
  }
}

Mood.propTypes = {
  moods: PropTypes.arrayOf(PropTypes.object).isRequired,
  updateMoodsData: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  cms: PropTypes.arrayOf().isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    moods: state.moods,
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  updateMoodsData,
  loadingPage,
  addCmsRedux,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Mood));
