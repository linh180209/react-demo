import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import BlockIconLineV2 from '../../components/HomeAlt/BlockIconLine/blockIconLinev2';
import Header from '../../components/HomePage/header';
import addCmsRedux from '../../Redux/Actions/cms';
import {
  fetchCMSHomepage, generateHreflang, getNameFromButtonBlock, getSEOFromCms, removeLinkHreflang, scrollTop, setPrerenderReady,
} from '../../Redux/Helpers';
import '../../styles/style.scss';
import FooterV2 from '../../views2/footer';
import Banner from './components/Banner';
import BrandIntroduce from './components/BrandIntroduce';
import ConsultForm from './components/ConsultForm';
import EventList from './components/EventList';
import OurStory from './components/OurStory';
import TestimonialsB2B from './components/TestimonialsB2B';


const getCms = (Partnership) => {
  const objectReturn = {
    bannerBlock: [],
    textBlock: '',
    imageBlock: [],
    commitmentBlock: {},
    instagramBlock: {},
    featureBlock: {},
    seo: undefined,
    stepBlock: {},
    benefitsBlock: {},
    iconsBlock: [],
    buttonBlocks: [],
    headerParagraphBlocks: [],
    blogsBlocks: {},
    articleBlocks: {},
  };

  if (!_.isEmpty(Partnership)) {
    const seo = getSEOFromCms(Partnership);
    const { body } = Partnership;
    if (body) {
      const bannerBlock = _.filter(body, x => x.type === 'banner_block').map(
        e => e.value,
      );
      const imageBlock = _.filter(body, x => x.type === 'image_block');
      const iconsBlock = _.filter(body, x => x.type === 'icons_block' && x.value?.image_background?.caption !== 'icon-line');
      const iconsBlockBottom = _.find(body, x => x.type === 'icons_block' && x.value?.image_background?.caption === 'icon-line');
      const buttonBlocks = _.filter(body, x => x.type === 'button_block');
      const headerParagraphBlocks = _.filter(
        body,
        x => x.type === 'header_and_paragraph_block',
      );
      const blogsBlocks = _.filter(body, x => x.type === 'blogs_block');
      const articleBlocks = _.filter(body, x => x.type === 'articles_block');
      _.assign(objectReturn, {
        bannerBlock,
        imageBlock,
        blogsBlocks,
        articleBlocks,
        buttonBlocks,
        headerParagraphBlocks,
        iconsBlock,
        iconsBlockBottom,
        seo,
      });
    }
  }
  return objectReturn;
};

class Partnership extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bannerBlock: [],
      imageBlock: [],
      iconsBlock: [],
      iconsBlockBottom: undefined,
      buttonBlocks: [],
      headerParagraphBlocks: [],
      blogsBlocks: undefined,
      articleBlocks: [],
    };
  }

  componentDidMount() {
    setPrerenderReady();
    if (!this.props.location.hash) {
      scrollTop();
    }
    this.fetchDataInit();
    this.scrollToHashId();
  }

  componentDidUpdate(prevProps, prevState) {
    this.scrollToHashId();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }


  fetchDataInit = async () => {
    const { cms } = this.props;
    const Partnership = _.find(cms, x => x.title === 'partnership');
    if (!Partnership) {
      const cmsData = await fetchCMSHomepage('b2b');
      this.props.addCmsRedux(cmsData);
      const dataFromStore = getCms(cmsData);
      this.setState(dataFromStore);
    } else {
      const dataFromStore = getCms(Partnership);
      this.setState(dataFromStore);
    }
  };

  scrollToHashId = () => {
    const { removeHash } = this;
    // get URL hash (minus the hash mark)
    const hash = this.props.location.hash.substring(1);

    // if there's a hash, scroll to that ID
    if (hash && hash !== '#null') {
      // setTimeout and requestAnimationFrame help ensure a true DOM repaint/reflow before we try to scroll
      // - reference: http://stackoverflow.com/a/34999925
      setTimeout(
        window.requestAnimationFrame(() => {
          const el = document.getElementById(hash);

          const elementRect = el.getBoundingClientRect();
          const absoluteElementTop = elementRect.top + window.pageYOffset;
          const top = absoluteElementTop - 70;
          window.scrollTo({ top, behavior: 'smooth' });

          // clean up the hash, so we don't scroll on every prop update
          // removeHash();
        }),
        0,
      );
    } else {
      setTimeout(
        window.requestAnimationFrame(() => {
          window.scrollTo({ top: 0 });
        }),
        400,
      );
    }
  };

  // borrowed from http://stackoverflow.com/questions/1397329/how-to-remove-the-hash-from-window-location-with-javascript-without-page-refresh/5298684#5298684
  removeHash = () => {
    const loc = window.location;
    const hist = window.history;

    // use modern browser history API
    if (hist && 'pushState' in hist) {
      hist.replaceState('', document.title, loc.pathname + loc.search);
      // fallback for older browsers
    } else {
      // prevent scrolling by storing the page's current scroll offset
      const scrollV = document.body.scrollTop;
      const scrollH = document.body.scrollLeft;

      loc.hash = '';
      // restore the scroll offset, should be flicker free
      document.body.scrollTop = scrollV;
      document.body.scrollLeft = scrollH;
    }
  };

  render() {
    const {
      bannerBlock,
      imageBlock,
      seo,
      buttonBlocks,
      headerParagraphBlocks,
      blogsBlocks,
      iconsBlock,
      iconsBlockBottom,
      ingredientBlock,
      articleBlocks,
    } = this.state;
    const { history, login } = this.props;
    const bannerData = bannerBlock[0] ? bannerBlock[0] : '';
    const ConsultFormTitle = getNameFromButtonBlock(
      buttonBlocks,
      'Consult_with_us',
    );
    const privacyData = headerParagraphBlocks[1]
      ? headerParagraphBlocks[1]
      : '';
    const contentData = headerParagraphBlocks[2]
      ? headerParagraphBlocks[2]
      : '';
    const ourIngredientsData = _.filter(
      headerParagraphBlocks,
      x => x.value.header.header_text === 'OUR INGREDIENTS',
    );
    const ourStoryData = headerParagraphBlocks[0]
      ? headerParagraphBlocks[0]
      : '';
    const blogsBlocksData = blogsBlocks ? blogsBlocks[0] : '';
    const imageBlockData = imageBlock || '';

    return (
      <div>
        <MetaTags>
          <title>{seo ? seo.seoTitle : ''}</title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/partnership')}
        </MetaTags>
        <Header />
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <div className="b2b-partnership">
          <Banner data={bannerData} clienteleData={iconsBlock} />
          <OurStory data={ourStoryData} />
          <EventList data={bannerBlock} buttonBlocks={buttonBlocks} removeHash={this.removeHash} location={this.props.location} />
          <BrandIntroduce
            imageBlockData={imageBlockData}
            bannerBlock={bannerBlock}
            buttonBlocks={buttonBlocks}
          />
          {/* <OurIngredients
            imageBlockData={imageBlockData}
            bannerBlock={bannerBlock}
            buttonBlocks={buttonBlocks}
          /> */}
          <TestimonialsB2B
            blogsBlockData={blogsBlocksData}
            articleBlocks={articleBlocks}
            buttonBlocks={buttonBlocks}
          />
          <ConsultForm
            title={ConsultFormTitle}
            privacyData={privacyData}
            contentData={contentData}
            buttonBlocks={buttonBlocks}
          />

          <BlockIconLineV2 data={iconsBlockBottom} />
          {/* <ServiceList
            data={bannerData}
            buttonBlocks={buttonBlocks}
            imageBlockData={imageBlockData}
          /> */}
        </div>
        <FooterV2 />
      </div>
    );
  }
}

Partnership.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  login: PropTypes.shape().isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    login: state.login,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(Partnership),
);
