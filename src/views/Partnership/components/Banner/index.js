import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
// import clubBanner from '../../assets/club-banner.png';
import './styles.scss';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { isMobile } from '../../../../DetectScreen';
// import { generaCurrency } from '../../../../Redux/Helpers';

class Banner extends Component {
  render() {
    const { data, clienteleData } = this.props;
    const sliderSetting = {
      speed: 500,
      slidesToShow: !isMobile ? 7 : 5,
      slidesToScroll: 1,
      centerMode: false,
      arrows: false,
      infinite: false,
      dots: false,
    };
    return (
      data && (
        <div
          className="club-banner d-flex justify-content-center align-items-center"
          style={{
            backgroundImage: `url(${data.image ? data.image.image : ''})`,
          }}
        >
          <div>
            <div className="__header">
              <p className="title">
                {data.header ? data.header.header_text : ''}
              </p>
            </div>
            {/* <div className="__content d-flex flex-column justify-content-center align-items-center">
              <div className="join">
                <a
                  onClick={() => { window.scrollTo({ left: 0, top: !isMobile ? 700 : 520, behavior: 'smooth' }); }}
                >
                  {data.button ? data.button.text : ''}
                </a>
              </div>
            </div> */}
            <div className="__header header-client">
              <p className="description">
                {clienteleData[0]
                  && clienteleData[0].value.image_background.caption}
              </p>
              <Slider
                {...sliderSetting}
                // style={{ margin: isMobile ? "2rem 0 4rem 0" : "2rem 0" }}
              >
                {clienteleData[0]
                  && clienteleData[0].value.icons.map((item, index) => (
                    <div
                      className="icon-client"
                      style={{
                        backgroundImage: `url(${item.value.image.image})`,
                      }}
                    >
                      <img loading="lazy" src={item.value.image.image} />
                    </div>
                  ))}
              </Slider>
            </div>
          </div>
        </div>
      )
    );
  }
}

export default Banner;
