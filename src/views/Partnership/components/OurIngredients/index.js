import React, { Component } from 'react';
import _ from 'lodash';
import htmlPare from 'react-html-parser';
import { Row, Col } from 'reactstrap';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';

import './styles.scss';
import { isMobile } from '../../../../DetectScreen';

class OurIngredients extends Component {
  render() {
    const {
      imageBlockData,
      buttonBlocks,
      bannerBlock,
    } = this.props;
    const bgImage = _.filter(
      imageBlockData,
      x => x.value.caption === 'our-ingredients-bg',
    );
    const data = bannerBlock
      ? _.filter(bannerBlock, x => x.icons.length > 0)
      : [];
    const enquiryText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'enquiry').toUpperCase()
      : '';
    return (
      data && (
        <div className="our-ingredients" id="our-ingredients">
          <Row noGutters>
            {isMobile ? (
              <Col xs={12} style={{ height: '200px' }}>
                <img
                  loading="lazy"
                  src={bgImage[0] ? bgImage[0].value.image : ''}
                  alt="bg"
                  className="bg-right"
                />
              </Col>
            ) : (
              ''
            )}
            <Col md={6} xs={12} className="left-col">
              <div className="d-flex justify-content-center align-items-center pb-4 pt-3">
                <div className="__header d-flex flex-column content">
                  <h3>{data[1] ? data[1].header.header_text : ''}</h3>
                  <p>{data[1] ? htmlPare(data[1].description) : ''}</p>
                  <div style={{ padding: isMobile ? '16px' : '3rem' }}>
                    <div className="d-flex justify-content-center mb-3">
                      {data[1]
                        ? data[1].icons.map((item, index) => (
                          <div className="icon-box" key={index}>
                            <div className="wrapper-icon">
                              <img loading="lazy" src={item.value.image.image} alt="icon" />
                            </div>
                            <p>{item.value.text}</p>
                          </div>
                        ))
                        : ''}
                    </div>
                  </div>
                  <div className="__content d-flex flex-column justify-content-center align-items-center">
                    <div
                      className="join"
                      style={{
                        margin: isMobile ? '2rem 3rem' : '0 0 3rem 0 ',
                        cursor: 'pointer',
                      }}
                    >
                      <ButtonCT
                        className="button-black bg-white"
                        name={enquiryText}
                        onClick={e => () => {
                          document
                            .getElementById('consult-form')
                            .scrollIntoView({
                              behavior: 'smooth',
                              top: !isMobile ? 380 : '',
                            });
                        }}
                      />
                      {/* <a
                        style={{
                          background: 'none',
                          border: '1px solid white',
                          padding: '16px 57px',
                        }}
                        onClick={() => {
                          document
                            .getElementById('consult-form')
                            .scrollIntoView({
                              behavior: 'smooth',
                              top: !isMobile ? 380 : '',
                            });
                        }}
                      >
                        {enquiryText}
                      </a> */}
                    </div>
                  </div>
                </div>
              </div>
            </Col>
            <Col md={6}>
              <img
                src={bgImage[0] ? bgImage[0].value.image : ''}
                alt="bg"
                className="bg-right"
              />
            </Col>
          </Row>
        </div>
      )
    );
  }
}

export default OurIngredients;
