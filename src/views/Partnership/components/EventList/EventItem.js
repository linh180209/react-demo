import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
// import clubBanner from '../../assets/club-banner.png';
import { Fade } from 'react-reveal';
import _ from 'lodash';
import './styles.scss';
import classNames from 'classnames';
import {
  Row, Col, Collapse, Button, CardBody, Card,
} from 'reactstrap';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import EventDetail from './EventDetail';
import icExit from '../../../../image/icon/ic-exit.svg';
import icDownPartnership from '../../../../image/icon/ic-down-partnership.svg';

import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import { isBrowser, isMobile } from '../../../../DetectScreen';
import ButtonCT from '../../../../components/ButtonCT';
// import { generaCurrency } from '../../../../Redux/Helpers';

class StoryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displaySectionBody: false,
      showItem: true,
      showDetail: false,
      dataDetail: {},

    };
    this.handleClickShow = this.handleClickShow.bind(this);
    this.handleClickHide = this.handleClickHide.bind(this);
    this.handleClickImgShow = this.handleClickImgShow.bind(this);
    this.onClickImgHide = this.onClickImgHide.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { keySelected, keyIndex } = nextProps;
    if (keySelected !== keyIndex && prevState.displaySectionBody) {
      return ({ displaySectionBody: false, showDetail: false });
    }
    return null;
  }

  handleClickShow() {
    console.log('this.props.key', this.props.keyIndex);
    this.setState({ displaySectionBody: true });
    this.props.setKeySelected(this.props.keyIndex);
  }

  handleClickHide() {
    this.setState({ displaySectionBody: false });
  }

  handleClickImgShow(data) {
    this.setState({ dataDetail: data, showDetail: true });
  }

  onClickImgHide() {
    this.setState({ showDetail: false });
  }

  componentDidMount = () => {
    const hash = this.props.location ? this.props.location.hash.substring(1) : undefined;
    if (hash) {
      setTimeout(() => {
        hash && this.props.data.header.header_text && hash == this.props.data.header.header_text
          .trim()
          .toLowerCase()
          .replace(/\s/g, '-')
          ? this.setState({ displaySectionBody: true })
          : this.setState({ displaySectionBody: false });

        // clean up the hash, so we don't scroll on every prop update
        this.props.removeHash();
      }, 300);
    }
  };

  render() {
    const { data, buttonBlocks } = this.props;
    const { dataDetail } = this.state;
    const sliderSetting = {
      speed: 500,
      slidesToShow: !isMobile ? 6 : 1,
      slidesToScroll: 1,
      centerMode: !!isMobile,
      arrows: false,
      infinite: false,
      dots: false,
    };

    const enquiryText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'enquiry')
      : '';
    const clickToViewText = buttonBlocks
      ? getNameFromButtonBlock(
        buttonBlocks,
        'click_to_view_offerings',
      )
      : '';
    const clickToCloseText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'click_to_close')
      : '';
    const { displaySectionBody, showDetail } = this.state;
    return (
      <div
        className="d-flex"
        style={{
          height: showDetail && isBrowser ? '600px' : 'auto',
        }}
        id={
          data.header.header_text
            ? data.header.header_text.trim().toLowerCase().replace(/\s/g, '-')
            : ''
        }
      >
        <div
          className={classNames(
            'story-body',
            {
              'hover-item': !displaySectionBody,
            },
            {
              overlay: displaySectionBody,
            },
            {
              'active-item': showDetail,
            },
          )}
          style={{
            backgroundImage: `url(${data.image.image})`,
          }}
          onClick={this.handleClickShow}
        >
          <div className="item-header">
            <div className="content-header">
              <h2>{data.header.header_text}</h2>
              <div
                className="des-header"
                style={{
                  display: isMobile ? 'none' : 'block',
                  opacity: displaySectionBody && 1,
                }}
              >
                {htmlPare(data.description ? data.description : '')}

                <div
                  className={classNames('join', displaySectionBody ? 'hidden' : '')}
                  style={{
                    margin: '0 0 3rem 0 ',
                  }}
                >
                  <ButtonCT
                    className="button-black bg-white"
                    name={enquiryText}
                    onClick={(e) => {
                      e.stopPropagation();
                      document.getElementById('consult-form').scrollIntoView({
                        behavior: 'smooth',
                        top: !isMobile ? 380 : '',
                      });
                    }}
                  />
                  {/* <a
                    style={{
                      background: 'none',
                      border: '1px solid white',
                      padding: '16px 57px',
                    }}
                    onClick={(e) => {
                      e.stopPropagation();
                      document.getElementById('consult-form').scrollIntoView({
                        behavior: 'smooth',
                        top: !isMobile ? 380 : '',
                      });
                    }}
                  >
                    {enquiryText}
                  </a> */}
                </div>
              </div>
            </div>
            <button
              type="button"
              className="btn-show-body"
              style={{
                display: isMobile ? 'none' : 'block',
                opacity: displaySectionBody && 1,
              }}
              onClick={(e) => {
                e.stopPropagation();
                displaySectionBody
                  ? this.handleClickHide()
                  : this.handleClickShow();
              }}
            >
              <p className="text-uppercase" style={{ marginBottom: '10px' }}>
                {!displaySectionBody ? clickToViewText : clickToCloseText}
              </p>
              {!displaySectionBody ? (
                <img
                  style={{ width: '20px' }}
                  src={icDownPartnership}
                  alt="icDownPartnership"
                />
              ) : (
                <img style={{ width: '20px' }} src={icExit} alt="icExit" />
              )}
            </button>
            <Collapse
              isOpen={displaySectionBody}
              onClick={() => this.handleClickShow()}
            >
              {/* <Fade collapse when={displaySectionBody} delay={0}> */}
              <div
                className="des-header-mobile"
                style={{
                  display: isMobile ? 'block' : 'none',
                }}
              >
                {htmlPare(data.description ? data.description : '')}
              </div>
              <div className="item-body">
                {isMobile && displaySectionBody ? (
                  <div className="d-flex btn-close-mobile">
                    <button
                      onClick={(e) => {
                        e.stopPropagation();
                        this.handleClickHide();
                      }}
                    >
                      {clickToCloseText}
                    </button>
                  </div>
                ) : (
                  ''
                )}
                <div className="divider-mobile" />
                {
                  isMobile ? (
                    <Slider
                      {...sliderSetting}
                      style={{ margin: isMobile ? '2rem 0 4rem 0' : '2rem 0' }}
                    >
                      {data
                        && data.events.map((item, index) => (
                          <div
                            className="slider-item text-center"
                            key={index}
                            onClick={(e) => {
                              e.stopPropagation();
                              this.handleClickImgShow(item);
                            }}
                          >
                            <div
                              className="image-item"
                              style={{
                                backgroundImage: `url(${item.value.image.image})`,
                              }}
                            >
                              {/* <img src={item.value.image.image} /> */}
                            </div>
                            <span className="header_4">{item.value.header.header_text}</span>
                          </div>
                        ))}
                    </Slider>
                  ) : (
                    <div className="list-image-icon">
                      {
                        _.map(data?.events || [], (item, index) => (
                          <div
                            className="slider-item"
                            key={index}
                            onClick={(e) => {
                              e.stopPropagation();
                              this.handleClickImgShow(item);
                            }}
                          >
                            <div
                              className="image-item"
                              style={{
                                backgroundImage: `url(${item.value.image.image})`,
                              }}
                            >
                              {/* <img src={item.value.image.image} /> */}
                            </div>
                            <span className="header_4">{item.value.header.header_text}</span>
                          </div>
                        ))
                      }
                    </div>
                  )
                }

              </div>
              {/* <div
                className="join"
                style={{
                  margin: '0 0 3rem 0 ',
                  float: 'none',
                  textAlign: 'center',
                  display: displaySectionBody && !isMobile ? 'block' : 'none',
                }}
              >
                <a
                  style={{
                    background: 'none',
                    border: '1px solid white',
                    padding: '16px 57px',
                    display: 'inline-block',
                    marginBottom: '2rem',
                  }}
                  onClick={(e) => {
                    e.stopPropagation();
                    document.getElementById('consult-form').scrollIntoView({
                      behavior: 'smooth',
                      top: !isMobile ? 380 : '',
                    });
                  }}
                >
                  {enquiryText}
                </a>
              </div> */}
            </Collapse>
            {/* </Fade> */}
          </div>
        </div>
        <div
          style={{ display: displaySectionBody ? 'block' : 'none' }}
          className={classNames(
            'story-detail',
            {
              'active-item': showDetail,
            },
            {
              'story-detail-mobile': isMobile,
            },
          )}
        >
          <EventDetail
            data={dataDetail}
            onClickImgHide={this.onClickImgHide}
            enquiryText={enquiryText}
            clickToCloseText={clickToCloseText}
          />
        </div>
      </div>
    );
  }
}

export default StoryList;
