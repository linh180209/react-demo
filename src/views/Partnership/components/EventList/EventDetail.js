import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
// import clubBanner from '../../assets/club-banner.png';
import { Fade } from 'react-reveal';
import './styles.scss';
import classNames from 'classnames';
import { Row, Col } from 'reactstrap';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';
import icExit from '../../../../image/icon/ic-close-video.svg';
import { isBrowser, isMobile } from '../../../../DetectScreen';
import ButtonCT from '../../../../components/ButtonCT';
// import { generaCurrency } from '../../../../Redux/Helpers';

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <button
      type="button"
      className="bt-arrow-partnership bt-arrow-partnership-right"
      onClick={onClick}
    >
      <i className="fa fa-chevron-right" />
    </button>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <button
      type="button"
      className="bt-arrow-partnership bt-arrow-partnership-left"
      onClick={onClick}
    >
      <i className="fa fa-chevron-left" />
    </button>
  );
}
const sliderSetting = {
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  centerMode: false,
  arrows: !isMobile,
  infinite: true,
  // autoplay: true,
  autoplaySpeed: 5000,
  dots: !!isMobile,
  // nextArrow: <SampleNextArrow />,
  // prevArrow: <SamplePrevArrow />,
};
class EventDetail extends Component {
  constructor(props) {
    super(props);
    this.handleClickImgHide = this.handleClickImgHide.bind(this);
  }

  handleClickImgHide() {
    this.props.onClickImgHide();
  }

  render() {
    const { data, enquiryText } = this.props;
    return (
      <div className={classNames('story-detail-item')}>
        <Row noGutters className="h-100">
          <Col md={6}>
            {/* <div
              className="bg-left"
              style={{
                // backgroundImage: `url(${
                //   data.value ? data.value.image.image : ""
                // })`,
                background: "#0d0d0d",
              }}
            > */}
            <div className="wrapper-slider">
              <Slider {...sliderSetting}>
                <div>
                  <div
                    className="bg-left"
                    style={{
                      backgroundImage: `url(${
                        data.value
                          ? data.value.placeholder.image
                            ? data.value.placeholder.image
                            : data.value.image.image
                          : ''
                      })`,
                    }}
                  />
                </div>
                {data.value
                  && data.value.images
                  && _.map(data.value.images, (item, i) => (
                    <div key={i}>
                      <div
                        className="bg-left"
                        style={{
                          backgroundImage: `url('${item.value.image}')`,
                        }}
                      />
                    </div>
                  ))}
              </Slider>
            </div>
            {/* </div> */}
          </Col>
          <Col md={6}>
            {
              isBrowser ? (
                <div className="d-flex btn-close">
                  <button onClick={this.handleClickImgHide} type="button">
                    <img style={{ width: '20px' }} src={icExit} alt="icExit" />
                  </button>
                </div>
              ) : ''
            }

            <div className="detail-right">
              <h3>{data.value ? data.value.header.header_text : ''}</h3>
              <div className="__slider" />
              {htmlPare(data.value ? data.value.text : '')}
              <div className="footer-detail">
                {
                  isMobile ? (
                    <button onClick={this.handleClickImgHide} type="button" className="button-bg__none bt-close">
                      <span>{this.props.clickToCloseText}</span>
                    </button>
                  ) : null
                }
                <div
                  className="join"
                  style={{
                    margin: isMobile ? '2rem 3rem' : '0 0 3rem 0 ',
                  }}
                >
                  <ButtonCT
                    className="button-black bg-white"
                    name={enquiryText}
                    onClick={(e) => {
                      e.stopPropagation();
                      document.getElementById('consult-form').scrollIntoView({
                        behavior: 'smooth',
                        top: !isMobile ? 380 : '',
                      });
                    }}
                  />
                </div>
                {/* <div className="price">
                  <p className="price-text">
                    {data.value ? data.value.price : ''}
                  </p>
                  {data.value && data.value.price_text && <p>{data.value.price_text}</p>}
                </div> */}
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default EventDetail;
