import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import EventItem from './EventItem';

class EventList extends Component {
  state = {
    keySelected: undefined,
  };

  setKeySelected = (key) => {
    console.log('setKeySelected', key);
    this.setState({ keySelected: key });
  }

  render() {
    const { data, buttonBlocks, removeHash } = this.props;
    const dataEvent = data ? _.filter(data, x => x.events.length > 0) : [];
    return (
      <div className="story-list">
        {/* {dataEvent
          ? dataEvent.map((item, index) => <EventItem setKeySelected={this.setKeySelected} keySelected={this.state.keySelected} key={index} data={item} buttonBlocks={buttonBlocks} removeHash={removeHash} />)
          : ''} */}
        {
            _.map(dataEvent || [], (item, index) => (
              <EventItem setKeySelected={this.setKeySelected} keySelected={this.state.keySelected} keyIndex={index} data={item} buttonBlocks={buttonBlocks} removeHash={removeHash} />
            ))
          }
      </div>
    );
  }
}

export default EventList;
