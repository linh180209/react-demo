import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import { Col, Row, Button } from 'reactstrap';
import { Fade } from 'react-reveal';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { isMobile } from '../../../../DetectScreen';
// import clubBanner from '../../assets/club-banner.png';
// import './styles.scss';
// import { generaCurrency } from '../../../../Redux/Helpers';

class BrandDetails extends Component {
  handleClick = (id) => {
    this.props.showSection(id);
  };

  render() {
    const { data } = this.props;
    const imgShow = data.value && data.value.imagetxts[1]
      ? data.value.imagetxts[1].value.image.image
      : '';
    const title = data.value && data.value.imagetxts[1]
      ? data.value.imagetxts[1].value.text
      : '';
    const content = data.value && data.value.imagetxts[1]
      ? data.value.imagetxts[1].value.description
      : '';

    const sliderSetting = {
      speed: 500,
      slidesToShow: !isMobile ? 3 : 1,
      slidesToScroll: 1,
      centerMode: false,
      arrows: false,
      infinite: false,
      dots: true,
    };
    return (
      <Fade delay={100}>
        <Row noGutters className={isMobile ? 'detail-mobile' : ''}>
          <Col md={6}>
            <div className="content-collab">
              <h3 className="header_2">{title}</h3>
              {htmlPare(content || '')}
            </div>
          </Col>
          <Col md={6}>
            {/* <img src={imgShow} /> */}
            <div
              className="brand-detail-left"
              style={{
                backgroundImage: `url(${imgShow || ''})`,
              }}
            />
          </Col>
        </Row>
        <Row noGutters>
          <Col xs={12}>
            {data.value ? (
              <div>
                {/* <h2 className="text-uppercase text-center caption">
                  {data.value && data.value.reviews
                    ? data.value.reviews.header1.header_text
                    : ''}
                </h2> */}
                <h4
                  className="text-uppercase text-center"
                  style={{ fontSize: '1.3rem' }}
                >
                  {data.value && data.value.reviews
                    ? data.value.reviews.header2.header_text
                    : ''}
                </h4>
              </div>
            ) : (
              ''
            )}
          </Col>
        </Row>
        <div style={isMobile ? {} : { margin: '2rem 0' }} className="div-text-des">
          <Slider {...sliderSetting}>
            {data.value
              && data.value.reviews
              && data.value.reviews.reviews.map((item, index) => (
                <div key={index} className="__item">
                  <div
                    className="content-brand-slider header_3"
                    style={{ width: '100%' }}
                  >
                    <p className="title-item">
                      "
                      {item.value.header.header_text}
                      "
                    </p>
                    <div className="des header_4">
                      {htmlPare(item.value.text ? item.value.text : '')}
                    </div>
                    <p>
                      <span className="header_4">{item.value.name}</span>
                      <br />
                      <span className="header_4">{item.value.job_title}</span>
                    </p>
                  </div>
                </div>
              ))}
          </Slider>
        </div>
      </Fade>
    );
  }
}

export default BrandDetails;
