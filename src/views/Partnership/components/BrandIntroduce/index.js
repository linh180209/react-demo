import React, { Component } from 'react';
import _ from 'lodash';
import htmlPare from 'react-html-parser';
import { Col, Row } from 'reactstrap';
import './styles.scss';
import BrandItem from './BrandItem';
import BrandDetails from './BrandDetails';
import icBack from '../../../../image/icon/ic-back-gray.svg';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';

class BrandIntroduce extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displaySectionBot: false,
      imageShow: '',
      dataDetail: [],
      contentShow: [],
    };
  }

  showSection = (data) => {
    const newImageShow = data.value.imagetxts[0].value.image.image;

    this.setState({
      displaySectionBot: true,
      imageShow: newImageShow,
      dataDetail: data,
    });
  };

  hideSection = (bg) => {
    this.setState({
      displaySectionBot: false,
      imageShow: bg,
    });
  };

  render() {
    const { imageBlockData, bannerBlock, buttonBlocks } = this.props;

    const dataBrand = bannerBlock
      ? _.filter(bannerBlock, x => x.brands.length > 0)
      : [];
    const bgBrandDefault = _.filter(
      imageBlockData,
      x => x.value.caption === 'default_brand_collab_bg',
    );
    const backToBrandsText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'back_to_brands')
      : '';
    const readMorebt = getNameFromButtonBlock(buttonBlocks, 'read more');

    const bgDefault = bgBrandDefault[0] ? bgBrandDefault[0].value.image : '';
    const tilte = dataBrand[0] ? dataBrand[0].header.header_text : '';
    const contentLeft = dataBrand[0] ? dataBrand[0].description : '';
    const { imageShow, dataDetail } = this.state;

    return (
      <div className="collab-brand" id="collab-brand">
        <Row noGutters>
          <Col md={6}>
            <div
              className="div-left"
              style={{
                backgroundImage: `url(${imageShow || bgDefault})`,
              }}
            >
              {/* <img src={imageShow ? imageShow : bgDefault} /> */}
              <div className="content-collab">
                <h3 className="header_2">{tilte}</h3>
                {htmlPare(contentLeft || '')}
              </div>
            </div>
          </Col>
          <Col md={6}>
            {/* map image logo */}
            <div
              className="logo-wrapper"
              style={{
                display: this.state.displaySectionBot ? 'none' : 'grid',
              }}
            >
              {!dataBrand[0]
                ? null
                : _.map(dataBrand[0].brands, x => (
                  <BrandItem
                    data={x}
                    showSection={this.showSection}
                    readMorebt={readMorebt}
                  />
                ))}
            </div>
            <div
              className="content-wrapper"
              style={{
                display: this.state.displaySectionBot ? 'block' : 'none',
              }}
            >
              <button onClick={() => this.hideSection(bgDefault)} className="btn-back" type="button">
                <img src={icBack} alt="back" />
                {backToBrandsText}
              </button>
              <div className="content-detail-brand">
                <img
                  src={dataDetail.value ? dataDetail.value.logo.image : ''}
                  alt="icon"
                />
                <h4>
                  {dataDetail.value ? dataDetail.value.header.header_text : ''}
                </h4>
                {htmlPare(dataDetail.value ? dataDetail.value.text : '')}
              </div>
            </div>
          </Col>
        </Row>
        <div
          className="detail-brand"
          style={{ display: this.state.displaySectionBot ? 'block' : 'none' }}
        >
          <BrandDetails data={dataDetail} />
        </div>
      </div>
    );
  }
}

export default BrandIntroduce;
