import React, { Component } from 'react';
import { Button } from 'reactstrap';

class BrandItem extends Component {
  handleClick=(data) => {
    this.props.showSection(data);
  }

  render() {
    const { data, readMorebt } = this.props;
    return (
      <Button
        onClick={() => this.handleClick(data)}
      >
        <img loading="lazy" src={data.value ? data.value.logo.image : ''} alt="logo" />
        <span>{readMorebt}</span>
      </Button>
    );
  }
}

export default BrandItem;
