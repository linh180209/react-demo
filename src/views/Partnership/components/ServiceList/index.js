import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import { Row, Col } from 'reactstrap';
import icShipping from '../../../../image/icon/ic-shipping.svg';
import icPayLater from '../../../../image/icon/ic-pay-later.svg';
import icSecurePayment from '../../../../image/icon/ic-secure-payment.svg';
import icFreeReturn from '../../../../image/icon/ic-free-return.svg';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';

class ServiceList extends Component {
  render() {
    const { data, buttonBlocks, imageBlockData } = this.props;
    const imageBg = imageBlockData
      ? _.filter(
        imageBlockData,
        x => x.value.caption === 'default_service_bg',
      )
      : '';
    const dhlShippingText = buttonBlocks
      ? getNameFromButtonBlock(
        buttonBlocks,
        'dhl_24_hour_shipping',
      ).toUpperCase()
      : '';
    const buyNowPayLaterText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'buy_now_pay_later').toUpperCase()
      : '';
    const securePaymentText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'secure_payment').toUpperCase()
      : '';
    const freeReturnText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'free_returns').toUpperCase()
      : '';
    return (
      // <Fade top delay={300}>
      <Row
        className="service-list"
        style={{
          backgroundImage: `url(${imageBg[0] ? imageBg[0].value.image : ''})`,
        }}
      >
        <Col md={3} xs={6} className={isMobile ? 'divider-mobile' : ''}>
          <div className="icon-box">
            <img src={icShipping} alt="icon" />
            <p>{dhlShippingText}</p>
          </div>
        </Col>
        <Col md={3} xs={6} className={isMobile ? 'divider-mobile' : ''}>
          <div className="icon-box">
            <img src={icPayLater} alt="icon" />
            <p>{buyNowPayLaterText}</p>
          </div>
        </Col>
        <Col md={3} xs={6} className={isMobile ? 'divider-mobile' : ''}>
          <div className="icon-box">
            <img src={icSecurePayment} alt="icon" />
            <p>{securePaymentText}</p>
          </div>
        </Col>
        <Col md={3} xs={6} className={isMobile ? 'divider-mobile' : ''}>
          <div className="icon-box">
            <img src={icFreeReturn} alt="icon" />
            <p>{freeReturnText}</p>
          </div>
        </Col>
      </Row>
      // </Fade>
    );
  }
}

export default ServiceList;
