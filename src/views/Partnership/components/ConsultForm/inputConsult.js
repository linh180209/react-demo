import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

class inputConsult extends Component {
  onChange = (e) => {
    const { onChange } = this.props;
    const { value, name } = e.target;
    if (onChange) {
      onChange(value, name);
    }
  }

  render() {
    const {
      isTypeSelect, title, defaultValue, name, options, type, disabled,
    } = this.props;
    return (
      <div className="input-account-detail">
        <span style={{ height: '1.2rem' }}>
          {title}
        </span>
        {
          !isTypeSelect
            ? (
              <input
                className="input-text"
                type={type}
                name={name}
                defaultValue={defaultValue}
                onChange={this.onChange}
                disabled={disabled}
              />
            ) : (
              <div className="div-span-account">
                <span className="span-down">
                  <select
                    className="b2b-select"
                    style={{ width: '100%' }}
                    // defaultValue={defaultValue}
                    name={name}
                    onChange={this.onChange}
                  >
                    {
                  _.map(options, d => (
                    <option value={d.value} selected={defaultValue === d.value}>
                      {d.name}
                    </option>
                  ))
                  }
                  </select>
                </span>
              </div>
            )
        }

      </div>
    );
  }
}
inputConsult.defaultProps = {
  isTypeSelect: false,
  options: [],
  type: 'text',
  disabled: false,
};

inputConsult.propTypes = {
  isTypeSelect: PropTypes.bool,
  disabled: PropTypes.bool,
  title: PropTypes.string.isRequired,
  defaultValue: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.object),
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
};

export default inputConsult;
