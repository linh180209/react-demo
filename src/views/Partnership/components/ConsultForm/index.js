import React, { Component } from 'react';
import _ from 'lodash';
import InputConsult from './inputConsult';
import {
  Col, Row, Button, Form, FormGroup, Label, Input,
} from 'reactstrap';
import classnames from 'classnames';
import Select from 'react-select';
import './styles.scss';
import PropTypes from 'prop-types';
import {
  POST_INQUIRIES,
  GET_INQUIRIES_SUBJECTS,
  GET_ALL_COUNTRY_QUIZ,
} from '../../../../config';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import {
  toastrError,
  toastrSuccess,
} from '../../../../Redux/Helpers/notification';
import { getNameFromButtonBlock, validateEmail } from '../../../../Redux/Helpers';
import icDown from '../../../../image/icon/ic-drop-grey.svg';
import auth from '../../../../Redux/Helpers/auth';

const formatCountry = (listCountry) => {
  const countries = [];
  _.forEach(listCountry, (x) => {
    countries.push(
      {
        label: x.name,
        value: x.code,
      },
    );
  });
  return countries;
};

class ConsultForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inquiry: undefined,
      listError: [],
      country: undefined,
      listCountry: [],
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.fetchInquiry();
    this.fetchAllCountry();
  }

  fetchAllCountry = async () => {
    try {
      const option = {
        url: GET_ALL_COUNTRY_QUIZ,
        method: 'GET',
      };
      const result = await fetchClient(option);
      if (result && !result.isError) {
        const listCountry = formatCountry(result);
        const country = _.find(listCountry || [], x => x.value === auth.getCountry());
        this.setState({ listCountry, country: country || listCountry[0] });
      }
    } catch (error) {
      console.log(error);
    }
  }

  fetchInquiry = () => {
    const { login } = this.props;
    const option = {
      url: GET_INQUIRIES_SUBJECTS,
      method: 'GET',
    };

    const pending = [fetchClient(option, true)];

    Promise.all(pending)
      .then((result) => {
        if (result && result.length > 0) {
          this.setState({ inquiry: result });
          return;
        }
        throw new Error(result.message);
      })
      .catch((err) => {
        toastrError(err.message);
      });
  };

  createInquiries = async (body) => {
    const options = {
      url: POST_INQUIRIES,
      method: 'POST',
      body,
    };
    try {
      const res = await fetchClient(options, true);
      toastrSuccess('Your message was sent successfully');
      return res;
    } catch (error) {
      console.log('error', error);
    }
    return null;
  };

  onChange = (e) => {
    if (this.state.listError.length > 0) {
      this.setState({ listError: [] });
    }
  }

  onChangeCountry = (d) => {
    this.setState({ country: d });
  }

  generateSelectInputComponents = () => {
    const inputComponents = {
      DropdownIndicator: selectProps => (
        <img style={{ marginBottom: '0px', marginRight: '10px' }} src={icDown} alt="Arrow down icon" />
      ),
    };
    return inputComponents;
  };

  handleSubmit(event) {
    event.preventDefault();
    const data = new FormData(event.target);
    const listError = [];
    if (!data.get('subject')) {
      listError.push('subject');
    }
    if (!data.get('first_name')) {
      listError.push('first_name');
    }
    if (!data.get('last_name')) {
      listError.push('last_name');
    }
    if (!data.get('email')) {
      listError.push('email');
    }
    if (!data.get('phone')) {
      listError.push('phone');
    }
    if (!data.get('city')) {
      listError.push('city');
    }
    if (!data.get('company_name')) {
      listError.push('company_name');
    }
    if (!data.get('message')) {
      listError.push('message');
    }
    if (listError.length > 0) {
      toastrError('Please enter all the information');
      this.setState({ listError });
    } else {
      const body = {
        company_name: data.get('company_name'),
        first_name: data.get('first_name'),
        last_name: data.get('last_name'),
        email: data.get('email'),
        message: data.get('message'),
        phone: data.get('phone'),
        city: data.get('city'),
        country: this.state.country?.value,
        subject: data.get('subject'),
        status: 'pending',
        meta: {},
      };
      this.createInquiries(body);
    }
  }

  render() {
    const {
      title, privacyData, contentData, buttonBlocks,
    } = this.props;
    const { inquiry, listError } = this.state;
    const inquiryData = inquiry ? inquiry[0] : undefined;

    const firstNamePlaceHolderText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'first_name')
      : '';
    const lastNamePlaceHolderText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'last_name')
      : '';
    const emailPlaceHolderText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'email')
      : '';
    const mobilePhonePlaceHolderText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'mobile_phone')
      : '';
    const companyNamePlaceHolderText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'company_name')
      : '';
    const messagePlaceHolderText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'message')
      : '';
    const submitPlaceHolderText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'submit').toUpperCase()
      : '';
    const cityPlaceHolderText = buttonBlocks
      ? getNameFromButtonBlock(buttonBlocks, 'city')
      : '';

    return (
      //   data && (
      <div className="consult-form" id="consult-form">
        <div className="divider" />
        <div className="__header d-flex flex-column align-items-center">
          <h3 className="title">{title || ''}</h3>
        </div>
        <Form onSubmit={this.handleSubmit}>
          <FormGroup>
            <Input
              type="select"
              name="subject"
              id="subject"
              onChange={this.onChange}
              className={classnames('b2b-select', listError.includes('subject') ? '--error' : '')}
              // style={{
              //   width: '100%',
              //   border: 0,
              //   borderRadius: 'unset',
              //   borderBottom: listError.includes('subject') ? '2px solid #ff0000' : '2px solid #e2e2e2',
              // }}
            >
              {inquiryData
                ? inquiryData.map(item => (
                  <option value={item.id}>{item.name}</option>
                ))
                : ''}
            </Input>
          </FormGroup>
          <Row form>
            <Col md={6}>
              <FormGroup>
                <Input
                  className={classnames('input-text', listError.includes('first_name') ? '--error' : '')}
                  onChange={this.onChange}
                  type="text"
                  name="first_name"
                  id="first_name"
                  placeholder={firstNamePlaceHolderText}
                />
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Input
                  className={classnames('input-text', listError.includes('last_name') ? '--error' : '')}
                  onChange={this.onChange}
                  type="text"
                  name="last_name"
                  id="last_name"
                  placeholder={lastNamePlaceHolderText}
                />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <Input
              className={classnames('input-text', listError.includes('email') ? '--error' : '')}
              onChange={this.onChange}
              type="email"
              name="email"
              id="exampleEmail"
              placeholder={emailPlaceHolderText}
            />
          </FormGroup>
          <Row form>
            <Col md={12}>
              <FormGroup>
                <Input
                  className={classnames('input-text', listError.includes('phone') ? '--error' : '')}
                  onChange={this.onChange}
                  type="text"
                  name="phone"
                  id="phone"
                  placeholder={mobilePhonePlaceHolderText}
                />
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <Select
              value={this.state.country}
              onChange={this.onChangeCountry}
              options={this.state.listCountry}
              className={classnames('input-text input-select', listError.includes('country') ? '--error' : '')}
              components={this.generateSelectInputComponents()}
            />
          </FormGroup>
          <FormGroup>
            <Input
              className={classnames('input-text', listError.includes('city') ? '--error' : '')}
              onChange={this.onChange}
              type="text"
              name="city"
              id="city"
              placeholder={cityPlaceHolderText}
            />
          </FormGroup>
          <FormGroup>
            <Input
              className={classnames('input-text', listError.includes('company_name') ? '--error' : '')}
              onChange={this.onChange}
              type="text"
              name="company_name"
              id="company_name"
              placeholder={companyNamePlaceHolderText}
            />
          </FormGroup>
          <FormGroup>
            {/* <Label for="exampleText" style={{ padding: '0 12px' }}>
              {messagePlaceHolderText}
            </Label> */}
            <Input
              className={classnames('input-text text-area', listError.includes('message') ? '--error' : '')}
              onChange={this.onChange}
              type="textarea"
              name="message"
              id="message"
              placeholder={messagePlaceHolderText}
            />
          </FormGroup>
          <div className="text-center">
            <Button className="btn-submit">{submitPlaceHolderText}</Button>
          </div>
        </Form>
        {
          privacyData?.value?.header?.header_text && (
            <section className="content-section">
              <label>
                {privacyData ? privacyData.value.header.header_text : ''}
              </label>
              <div
                dangerouslySetInnerHTML={{
                  __html: privacyData ? privacyData.value.paragraph : '',
                }}
              />
            </section>
          )
        }

        {
          contentData?.value?.header?.header_text && (
            <section className="content-section" style={{ marginTop: '2rem' }}>
              <label>
                {contentData ? contentData.value.header.header_text : ''}
              </label>
              <div
                dangerouslySetInnerHTML={{
                  __html: contentData ? contentData.value.paragraph : '',
                }}
              />
            </section>
          )
        }

      </div>
      //   )
    );
  }
}
ConsultForm.defaultProps = {
  privacyData: {},
  contentData: {},
};

ConsultForm.propTypes = {
  privacyData: PropTypes.arrayOf(PropTypes.object),
  contentData: PropTypes.arrayOf(PropTypes.object),
};

export default ConsultForm;
