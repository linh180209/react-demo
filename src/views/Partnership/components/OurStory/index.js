import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
// import clubBanner from '../../assets/club-banner.png';
import './styles.scss';
// import { generaCurrency } from '../../../../Redux/Helpers';

class OurStory extends Component {
  render() {
    const { data } = this.props;
    return (
      data && (
        <div className="our-history d-flex justify-content-center align-items-center">
          <div className="__header d-flex flex-column align-items-center content">
            <h2 className="header_2">{data ? data.value.header.header_text : ''}</h2>
            {/* <p>{data.text ? data.text : ""}</p> */}
            {htmlPare(data ? data.value.paragraph : '')}
          </div>
        </div>
      )
    );
  }
}

export default OurStory;
