import classnames from 'classnames';
import _ from 'lodash';
import moment from 'moment';
import React, { Component, lazy, Suspense } from 'react';
import ReactHtmlPare from 'react-html-parser';
import 'react-image-gallery/styles/css/image-gallery.css';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import ButtonCT from '../../../../components/ButtonCT';
import { isBrowser, isMobile } from '../../../../DetectScreen';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import './styles.scss';


const LazyImage = lazy(() => import('../../../../components/LazyImage'));

class TestimonialsB2B extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nav1: null,
      nav2: null,
    };
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2,
    });
  }

  renderItem = data => (
    <div className="div-testimonials-items div-col justify-center items-center">
      {isMobile ? (
        <div className="div-image div-col justify-center items-center mt-3">
          <Suspense fallback={<div />}>
            <LazyImage src={data.value.image.image} />
          </Suspense>
        </div>
      ) : (
        <div className="div-image div-col justify-center items-center" />
      )}
      <div className="div-text tc">{ReactHtmlPare(data.value.description)}</div>
    </div>
  );

  render() {
    const { blogsBlockData, articleBlocks, buttonBlocks } = this.props;
    const data = blogsBlockData ? blogsBlockData.value : '';
    const dataBlock = _.filter(
      articleBlocks,
      x => x.value.header.header_text === 'LATEST ARTICLES',
    );
    console.log('dataBlock', dataBlock[0]?.value?.articles);
    const pressMediaBt = getNameFromButtonBlock(buttonBlocks, 'Press & media');
    const { button, blogs } = data;
    const settings = {
      dots: true,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      // autoplay: true,
      arrows: !isMobile,
      autoplaySpeed: 6000,
      pauseOnHover: false,
    };
    const sliderSetting = {
      speed: 500,
      slidesToShow: !isMobile ? 2 : 1,
      slidesToScroll: 1,
      arrows: !isMobile,
      infinite: true,
    };
    if (isBrowser) {
      _.assign(settings, {
        dotsClass: 'slick-dots slick-thumb dots-custome-landing',
        appendDots: dots => (
          <div
            style={{
              padding: '10px',
            }}
          >
            <ul style={{ margin: '0px' }}>{dots}</ul>
          </div>
        ),
        customPaging: i => (!isMobile ? (
          <div>
            <Suspense fallback={<div />}>
              <LazyImage src={blogs[i].value.image.image} />
            </Suspense>
            <div className="line-active" />
          </div>
        ) : (
          ''
        )),
      });
    }

    const articleSliderMobile = (
      <Slider
        {...sliderSetting}
        asNavFor={this.state.nav2}
        ref={slider => (this.slider1 = slider)}
        style={{ margin: '2rem 0' }}
      >
        {dataBlock[0]
          && dataBlock[0].value.articles
            .map(ele => ele.value)
            .map((item, index) => (
              <div key={index} className="__item">
                <a href={item.link}>
                  <img
                    src={item.image.image}
                    alt={item.image.caption}
                    // className="img-fluid"
                  />
                  <div className="content" style={{ width: '100%' }}>
                    <div className="row pt-3">
                      <div className="col-12">
                        <p className="__item-name">{item.name}</p>
                        <p className="__item-content">{item.text}</p>
                        <p className="__item-date">
                          {moment(item.date).format('MMMM Do YYYY')}
                        </p>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            ))}
      </Slider>
    );

    return (
      <div
        className="div-testimonials"
        id="div-testimonials"
      >
        {pressMediaBt && <h3>{pressMediaBt}</h3>}
        {/* <div className="row justify-content-center">
          {dataBlock[0] && (
            <div className="__title d-flex flex-column justify-content-center">
              <h3>
                {dataBlock[0].value.header
                  ? dataBlock[0].value.header.header_text
                  : ''}
              </h3>
              <p className="text-center">{dataBlock[0].value.text}</p>
            </div>
          )}
        </div> */}
        {/* {!isMobile ? (
          <div className="article-slider">
            <div className="row">
              <div className="col-12">
                <div className="article-slider">
                  <Slider
                    {...sliderSetting}
                    asNavFor={this.state.nav2}
                    ref={slider => (this.slider1 = slider)}
                  >
                    {dataBlock[0]
                      && dataBlock[0].value.articles
                        .map(ele => ele.value)
                        .map((item, index) => (
                          <div key={index} className="__item">
                            <div className="cover">
                              <img
                                src={item.image.image}
                                alt={item.image.caption}
                                className="img-fluid"
                              />
                            </div>
                            <div className="content">
                              <div className="row pt-3">
                                <div className="col-8">
                                  <p className="__item-name">
                                    {item.header.header_text}
                                  </p>
                                  <p className="__item-content">{item.text}</p>
                                  <p className="__item-date">
                                    {moment(item.date).format('MMMM Do YYYY')}
                                  </p>
                                </div>
                                <div className="col-4">
                                  <div className="d-flex align-items-center justify-content-end h-100">
                                    <a href={item.button.link}>
                                      {item.button.text}
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                  </Slider>
                </div>
              </div>
            </div>
          </div>
        ) : (
          articleSliderMobile
        )} */}
        <div
          className="logo-slider"
        >
          <Slider
            asNavFor={this.state.nav1}
            ref={slider => (this.slider2 = slider)}
            {...settings}
          >
            {_.map(blogs, d => this.renderItem(d))}
          </Slider>
        </div>
        <div className="read-more-testi">
          <ButtonCT
            typeBT="LINK"
            name={data?.button?.name}
            className={isMobile ? classnames('link-text', 'link-text-landingpage') : classnames('button-black', 'bg-white')}
            href={data?.button?.link}
          />
        </div>
      </div>
    );
  }
}

export default TestimonialsB2B;
