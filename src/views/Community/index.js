import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Row, Col,
  Card, CardHeader, CardBody, CardFooter,
  Input, Label,
  Button,
  Form, FormGroup,
} from 'reactstrap';


import CommunityItem from '../../components/CommunityItem';
import { googleAnalitycs } from '../../Redux/Helpers';

class Community extends Component {
  state = {
    data: [{
      title: 'Search for adventure',
      type: 'Neroli & vetivert',
      price: 30,
      total: 1,
    },
    {
      title: 'Feel safe & secure',
      type: 'Patchouli & role',
      price: 40,
      total: 4,
    },
    {
      title: 'Search for fun',
      type: 'lavande & oud',
      price: 40,
      total: 4,
    }],
  }

  componentDidMount = () => {
    googleAnalitycs('/community');
  };

  render() {
    const { data } = this.state;
    return (
      <div>
        <Row style={{ marginTop: '30px' }}>
          <Col md="4" xs="12">
            <CommunityItem data={data[0]} />
          </Col>
          <Col md="4" xs="12">
            <CommunityItem data={data[1]} />
          </Col>
          <Col md="4" xs="12">
            <CommunityItem data={data[2]} />
          </Col>
        </Row>
        <Row className="container--community__bt">
          <Button>
            Procced to checkout
          </Button>
        </Row>
      </div>
    );
  }
}
export default Community;
