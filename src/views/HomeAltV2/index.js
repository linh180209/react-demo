/* eslint-disable react/sort-comp */
/* eslint-disable no-shadow */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
// import PropTypes from 'prop-types';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import smoothscroll from 'smoothscroll-polyfill';
import queryString from 'query-string';

import fetchClient from '../../Redux/Helpers/fetch-client';
import {
  scrollTop, fetchCMSHomepage, getCmsCommon, googleAnalitycs, generateHreflang, generateUrlWeb, setScriptIntoDocument, removeLinkHreflang, setPrerenderReady, gotoShopHome,
} from '../../Redux/Helpers';
import {
  CALL_UNSUBCRIBE,
} from '../../config';

import HeaderHomePage from '../../components/HomePage/header';
import addCmsRedux from '../../Redux/Actions/cms';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import BlockCarouselV2 from '../../components/HomeAlt/BlockCarouselV2';
import TestimonialsV2 from '../../components/HomeAlt/TestimonialsV2';
import BlockSayingV2 from '../../components/HomeAlt/BlockSayingV2';
import PopUpLandingPage from './popUpLandingPage';
import BlockIconHeaderV3 from '../../components/HomeAlt/BlockIconHeaderV3';
import BlockIconScroll from '../../components/HomeAlt/BlockIconScroll';

import { createBasketGuest, addProductBasket } from '../../Redux/Actions/basket';
import loadingPage from '../../Redux/Actions/loading';
import AskCookies from '../../components/AskCookies';
import auth from '../../Redux/Helpers/auth';
import logo from '../../image/logomark.png';
import BlockBannerV2 from '../../components/HomeAlt/BlockBannerV2';
import BlockCTA from '../../components/HomeAlt/BlockCTA';
import BlockProductV2 from '../../components/HomeAlt/BlockProductV2';
import BlockWorkShopV2 from '../../components/HomeAlt/BlockWorkShopV2';
import BlockColumn from '../../components/HomeAlt/BlockColumn';
import BlockIconLineV2 from '../../components/HomeAlt/BlockIconLine/blockIconLinev2';
import StepBlockHomeAlt from '../../components/HomeAlt/StepBlock';
import { SCHEMA_MARKUP_ID, SCHEMA_MARKUP_ID_2 } from '../../constants';
import QnABlock from '../../components/QnABlock';
import PopupDelivery from './PopupDelivery';
import PopupCountry from './PopupCountry';
import FooterV2 from '../../views2/footer';

class HomeAltV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAskCookies: false,
      homePageCms: undefined,
      isOpenPopup: false,
    };
  }

  componentDidMount() {
    setPrerenderReady();

    this.addScript1();
    this.addScript2();
    if (this.props.history.location.pathname === '/subscribers/unsub/') {
      const { email } = queryString.parse(this.props.location.search);
      fetchClient({
        url: CALL_UNSUBCRIBE.replace('{email}', email),
        method: 'GET',
      }).then((result) => {
        if (result.message === 'ok') {
          toastrSuccess('Unsubcribe Successful');
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        } else {
          toastrError('Unsubcribe Unsuccessful');
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        }
      }).catch((error) => {
        toastrError('Unsubcribe Unsuccessful');
        // this.props.history.push(generateUrlWeb('/'));
        gotoShopHome();
      });
    }
    smoothscroll.polyfill();
    googleAnalitycs('/');
    this.setState({ isAskCookies: auth.getAskCookies() });
    this.fetchData();
    this.props.loadingPage(false);
    this.addChat();
    const values = queryString.parse(this.props.location.search);
    const { anchor } = values;
    if (anchor) {
      this.gotoAnchor(anchor);
    } else {
      scrollTop();
    }
    if (!auth.getPopupLandingPage()) {
      setTimeout(() => {
        this.onOpenPopup();
      }, 1000 * 15);
    }
  }

  componentDidUpdate = (preProp) => {
    const { location } = this.props;
    if (location && location.search && location.search !== preProp.location.search) {
      const values = queryString.parse(location.search);
      const { anchor } = values;
      if (anchor) {
        this.gotoAnchor(anchor);
      }
    }
  }

  componentWillUnmount = () => {
    removeLinkHreflang();
  }

  gotoAnchor = (name) => {
    setTimeout(() => {
      const ele = document.getElementById(`id-${name}`);
      if (ele) {
        ele.scrollIntoView({ behavior: 'smooth' });
        window.scrollTo(0, 500);
      }
    }, 5000);
  }

  onClickAskCookies = () => {
    this.setState({ isAskCookies: true });
    auth.setAskCookies();
  }

  addChat = () => {
    const s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    if (auth.getCountry() === 'vn') {
      s.innerHTML = `
      (function(d, s, id, t) {
        if (d.getElementById(id)) return;
        var js, fjs = d.getElementsByTagName(s)[0];
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://widget.oncustomer.asia/js/index.js?token=' + t;
        fjs.parentNode.insertBefore(js, fjs);}
      (document, 'script', 'oc-chat-widget-bootstrap', 'a061c015a9baedc9a4c2aa56a183f6db'));    
      `;
    } else {
      s.innerHTML = `
        window.intercomSettings = {
          app_id: "i20hnofe"
        };
        (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/i20hnofe';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
      `;
    }

    document.body.appendChild(s);
  }

  addScript1 = () => {
    setScriptIntoDocument(SCHEMA_MARKUP_ID, `
    {
      "@context": "http://schema.org",
      "@type": "LocalBusiness",
      "description": "Create Customised Perfume and Fragrances",
      "name": "Maison 21G",
      "telephone": "(65) 6226-2677",
      "image": "https://lh5.googleusercontent.com/-GnGYuKP5WZE/AAAAAAAAAAI/AAAAAAAAAAA/VShOWQKHt98/s55-p-k-no-ns-nd/photo.jpg",
      "address": {
      "@type": "PostalAddress",
      "addressLocality": "Tanjong Pagar",
      "addressCountry": "SG",
      "postalCode":"089536",
      "streetAddress": "77 Duxton Rd"
      },
      "openingHours": "Mo,Tu,We,Th,Fr,Sa 12:00-20:00",
      "geo": {
      "@type": "GeoCoordinates",
      "latitude": "1.278448",
      "longitude": "103.843149"
      },
       "aggregateRating": {
          "@type": "AggregateRating",
          "ratingValue": "4.9",
          "reviewCount": "114"
        },
      "sameAs" : [ "https://www.facebook.com/Maison21G", 
      "https://www.instagram.com/maison21g/"]
      }  
    `);
  }

  addScript2 = () => {
    setScriptIntoDocument(SCHEMA_MARKUP_ID_2, `
    { 
      "@context": "http://schema.org", 
      "@type": "WebSite", 
      "url": "https://maison21g.com/", 
      "name": "Maison 21G",
      "description": "Create Customised Perfume and Fragrances",
      "potentialAction": { 
        "@type": "SearchAction", 
        "target": "https://maison21g.com/?s={search_term}", 
        "query-input": "required name=search_term" } 
        }  
    `);
  }

  fetchData = async () => {
    const { cms } = this.props;
    const titleCms = window.location.pathname.includes('/perfume-DIY') ? 'Home Alt V2 DIY' : 'Home Alt V2';
    const splusCms = window.location.pathname.includes('/perfume-DIY') ? 'home-alt-v2-diy' : 'home-alt-v2';
    const homePageCms = _.find(cms, x => x.title === titleCms);
    if (!homePageCms) {
      try {
        const result = await fetchCMSHomepage(splusCms);
        this.props.addCmsRedux(result);
        this.setState({ homePageCms: result });
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      this.setState({ homePageCms });
    }
  }

  onOpenPopup = () => {
    this.setState({ isOpenPopup: true });
  }

  onClosePopup = () => {
    auth.setPopupLandingPage();
    this.setState({ isOpenPopup: false });
  }

  render() {
    const {
      history, login, basket, addProductBasket, createBasketGuest, cms, countries, loadingPage,
    } = this.props;

    const metaHtml = (
      <MetaTags>
        <title>
          Customise your own perfume | House of Scent Designers | Maison 21G
        </title>
        <meta name="description" content="Create your customised perfume at Maison21G. Explore our workshops for friends, bridal parties, corporate events, families. Discover our story, open your senses" />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 months" />
        <meta property="og:image" content={logo} />
        {generateHreflang(countries, '')}
      </MetaTags>
    );

    const { isAskCookies, homePageCms, isOpenPopup } = this.state;
    if (!homePageCms) {
      return (
        <div />
      );
    }

    const { isShowLogin } = this.props.location;
    const cmsCommon = getCmsCommon(cms);
    const itemPopUpBlock = _.find(homePageCms.body, x => x.type === 'item_popup_block');
    const buttonBlocks = _.filter(homePageCms.body, x => x.type === 'button_block');
    return (
      <div className="div-col justify-center items-center">
        {metaHtml}
        {/* <PopupCountry /> */}
        {/* <PopupDelivery countries={countries} buttonBlocks={buttonBlocks} /> */}
        {/* {
          auth.getDeliveryCountry() && <PopupDelivery countries={countries} buttonBlocks={buttonBlocks} />
        } */}
        {
          isOpenPopup && itemPopUpBlock ? (
            <PopUpLandingPage onClose={this.onClosePopup} cmsCommon={cmsCommon} itemPopUpBlock={itemPopUpBlock} createBasketGuest={createBasketGuest} addProductBasket={addProductBasket} basket={basket} history={history} />
          ) : (<div />)
        }
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        {
          _.map(homePageCms.body, (d, index) => {
            switch (d.type) {
              case 'carousel_block':
                return <BlockCarouselV2 heroCms={d} history={history} login={login} index={index} />;
              case 'banner_block':
                return <BlockBannerV2 id={`banner_block-${index}`} data={d} basket={basket} createBasketGuest={createBasketGuest} addProductBasket={addProductBasket} />;
              case 'lesson_block':
                return <BlockCTA data={d} isDisabledText />;
              case 'items_block':
                return <BlockProductV2 data={d} buttonBlocks={buttonBlocks} history={history} basket={basket} createBasketGuest={createBasketGuest} addProductBasket={addProductBasket} loadingPage={loadingPage} isViewProduct />;
              case 'atelier_list_block':
                return <BlockWorkShopV2 data={d} buttonBlocks={buttonBlocks} history={history} index={index} />;
              case 'feedbacks_block':
                return <BlockSayingV2 heroCms={d} history={history} login={login} index={index} />;
              case 'blogs_block':
                return <TestimonialsV2 heroCms={d} />;
              case 'benefits_block':
                return <BlockIconHeaderV3 heroCms={d} />;
              case 'columns_block':
                return <BlockColumn data={d} />;
              case 'icons_block':
                if (d.value.image_background.caption === 'icon-scroll') {
                  return <BlockIconScroll data={d} buttonBlocks={buttonBlocks} />;
                }
                return <BlockIconLineV2 data={d} />;
              case 'steps_block':
                return <StepBlockHomeAlt data={d} />;
              case 'faq_block':
                return <QnABlock data={d.value} />;
              default:
                break;
            }
          })
        }
        <HeaderHomePage
          isShowLogin={isShowLogin}
          isShowBlackSlider
        />
        <FooterV2 />
        {
          isAskCookies ? <div /> : <AskCookies onClick={this.onClickAskCookies} />
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
    login: state.login,
    basket: state.basket,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  addProductBasket,
  createBasketGuest,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomeAltV2));
