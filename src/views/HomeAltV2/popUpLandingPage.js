/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import '../../styles/popup-landing-page.scss';
import icClose from '../../image/icon/ic-close-region.svg';
import { generateUrlWeb, getNameFromCommon } from '../../Redux/Helpers';
import { isBrowser } from '../../DetectScreen';

class PopUpLandingPage extends Component {
  componentDidMount() {
    this.disableScrollBody();
    document.body.addEventListener('touchmove', this.preventDefault, { passive: false });
  }

  componentWillUnmount() {
    this.enableScrollBody();
    document.body.removeEventListener('touchmove', this.preventDefault);
  }

  preventDefault = (e) => {
    e.preventDefault();
  }


  disableScrollBody = () => {
    const name = 'disable-sroll';
    const element = document.getElementById('body');
    const arr = element.className.split(' ');
    if (arr.indexOf(name) === -1) {
      element.className += ` ${name}`;
      // if (!isBrowser && isIOS) {
      //   document.body.style.position = 'fixed';
      //   document.body.style.top = `-${window.scrollY}px`;
      // }
    }
  }

  enableScrollBody = () => {
    const name = 'disable-sroll';
    const element = document.getElementById('body');
    const arr = element.className.split(' ');
    if (arr.indexOf(name) !== -1) {
      element.classList.remove('disable-sroll');
      // if (!isBrowser && isIOS) {
      //   const scrollY = document.body.style.top;
      //   document.body.style.position = '';
      //   document.body.style.top = '';
      //   window.scrollTo(0, parseInt(scrollY || '0', 10) * -1);
      // }
    }
  }

  onClickGotoProduct = () => {
    const { itemPopUpBlock } = this.props;
    const item = itemPopUpBlock ? itemPopUpBlock.value.item : {};
    const { type, product: id } = item;
    let url;
    if (type === 'Kit') {
      url = `/product/kit/${id}`;
    } else if (type === 'Wax_Perfume') {
      url = `/product/wax/${id}`;
    } else if (type === 'Elixir') {
      url = `/product/elixir/${id}`;
    } else if (type === 'bundle') {
      url = `/product/bundle/${id}`;
    } else if (type === 'Scent') {
      url = `/product/Scent/${id}`;
    } else if (type === 'Creation') {
      url = `/product/creation/${id}`;
    } else if (type === 'hand_sanitizer') {
      url = `/product/hand-sanitizer/${id}`;
    } else if (type === 'home_scents') {
      url = `/product/home-scents/${id}`;
    } else if (type === 'discovery_box') {
      url = `/product/discovery_box/${id}`;
    } else if (type === 'mask_sanitizer') {
      url = `/product/mask_sanitizer/${id}`;
    } else if (type === 'toilet_dropper') {
      url = `/product/toilet_dropper/${id}`;
    } else if (type === 'dual_candles') {
      url = `/product/dual-candles/${id}`;
    } else if (type === 'single_candle') {
      url = `/product/single-candle/${id}`;
    } else if (type === 'holder') {
      url = `/product/holder/${id}`;
    } else if (type === 'gift_bundle') {
      url = `/product/gift-bundle/${id}`;
    } else if (type === 'mini_candles') {
      url = `/product/mini_candles/${id}`;
    }
    this.props.history.push(generateUrlWeb(url));
  }

  onClickAddToCart = () => {
    const {
      itemPopUpBlock, addProductBasket, createBasketGuest, basket,
    } = this.props;
    const item = itemPopUpBlock ? itemPopUpBlock.value.item : {};
    const {
      price, id, name,
    } = item;
    const data = {
      item: id,
      is_featured: item.is_featured,
      name,
      price,
      quantity: 1,
      total: parseFloat(price) * 1,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    if (!basket.id) {
      createBasketGuest(dataTemp);
    } else {
      addProductBasket(dataTemp);
    }
    this.props.onClose();
  }

  render() {
    const { onClose, cmsCommon, itemPopUpBlock } = this.props;
    const valueCms = itemPopUpBlock ? itemPopUpBlock.value : {};
    const adddToCart = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
    const noThank = getNameFromCommon(cmsCommon, 'No_thanks_Continue_shopping');
    return (
      <div className="div-popup-landing-page animated faster fadeIn">
        <div className="div-background" onClick={onClose} />
        <div className="div-body-popup">
          <button className="button-close" type="button" onClick={onClose}>
            <img src={icClose} alt="icClose" />
          </button>
          <h3 className="h3-header">
            {valueCms ? valueCms.header.header_text : ''}
          </h3>
          <h5 className="sub-header">
            {valueCms ? valueCms.text : ''}
          </h5>
          <div className="div-body">
            <div className="div-image">
              <img loading="lazy" src={valueCms ? valueCms.item.image : ''} alt="product" onClick={this.onClickGotoProduct} />
            </div>
            <div className="div-scent">
              <span className="span-name">
                {valueCms ? valueCms.text2 : ''}
              </span>
              <div className="div-scent">
                {
                  _.map(valueCms.images, x => (
                    <img loading="lazy" src={x.value.image} alt="scent" />
                  ))
                }
              </div>
              <div className="div-text-small">
                <span className="text-small">
                  {valueCms ? valueCms.text3 : ''}
                </span>
              </div>
              <button type="button" className="button-normal" onClick={this.onClickAddToCart}>
                {adddToCart}
              </button>
            </div>
          </div>
          <hr />
          <div className="div-footer-popup">
            {
              isBrowser ? (
                <span>
                  {valueCms ? valueCms.text4 : ''}
                </span>
              ) : (<div />)
            }

            <button type="button" onClick={onClose}>
              {noThank}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

PopUpLandingPage.propTypes = {
  onClose: PropTypes.func.isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  itemPopUpBlock: PropTypes.shape().isRequired,
  basket: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
  cmsCommon: PropTypes.arrayOf().isRequired,
};

export default PopUpLandingPage;
