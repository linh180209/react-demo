import React, { useState, useEffect } from 'react';
import { Modal, ModalBody } from 'reactstrap';
import Select from 'react-select';
import classnames from 'classnames';
import { countries } from 'country-data';
import _ from 'lodash';
import moment from 'moment';
import ReactCountryFlag from 'react-country-flag';
import { useMergeState } from '../../../Utils/customHooks';
import icDown from '../../../image/icon/ic-down-partnership-black.svg';
import icClose from '../../../image/icon/ic-close-black.svg';
import icHi from '../../../image/icon/ic-hi.svg';
import './styles.scss';
import ButtonCT from '../../../components/ButtonCT';
import auth from '../../../Redux/Helpers/auth';
import { formatCountry, getShippingCountry } from './handler';
import { toastrError } from '../../../Redux/Helpers/notification';
import { addFontCustom, changeUrl, getNameFromButtonBlock } from '../../../Redux/Helpers';

function PopupDelivery(props) {
  const [state, setState] = useMergeState({
    // listShipping: [],
    isOpen: true,
    nSymbol: undefined,
  });

  // const fetchShipping = async () => {
  //   try {
  //     const list = await getShippingCountry();
  //     setState({ listShipping: list });
  //   } catch (error) {
  //     toastrError(error.message);
  //   }
  // };

  // const handleShowPopup = () => {
  //   const ele = _.find(state.listShipping, x => x?.country?.code === auth.getCountry());
  //   if (!ele && auth.getDeliveryCountry()) {
  //     setState({ isOpen: true });
  //   }
  // };

  // useEffect(() => {
  //   setTimeout(() => {
  //     fetchShipping();
  //   }, 2000);
  // }, []);

  // useEffect(() => {
  //   const list = formatCountry(props.countries);
  //   setState({ countries: list });
  // }, [props.countries]);

  // useEffect(() => {
  //   if (state.listShipping?.length > 0) {
  //     handleShowPopup();
  //   }
  // }, [state.listShipping]);

  const onChangeCountry = (symbol) => {
    setState({ nSymbol: symbol });
  };

  const onClickContinute = () => {
    const ele = _.find(props.countries, x => x.code.toLowerCase() === state.nSymbol.value);
    auth.setCountry(JSON.stringify(ele));
    setState({ isOpen: false });
    changeUrl(state.nSymbol.value);
  };

  const closePopUp = () => {
    auth.setDeliveryCountry(moment().valueOf());
    setState({ isOpen: false });
  };

  const generateSelectInputComponents = () => {
    const inputComponents = {
      DropdownIndicator: selectProps => (
        <img style={{ marginBottom: '0px', marginRight: '10px' }} src={icDown} alt="Arrow down icon" />
      ),
    };
    return inputComponents;
  };
  const hereThereBt = getNameFromButtonBlock(props.buttonBlocks, 'Hey there');
  const sorryBt = getNameFromButtonBlock(props.buttonBlocks, 'Sorry, we don’t ship to');
  const changeBt = getNameFromButtonBlock(props.buttonBlocks, 'CHANGE COUNTRY');
  const continueBt = getNameFromButtonBlock(props.buttonBlocks, 'Enter site');
  return (
    <Modal className={classnames('popup-delivery', addFontCustom())} isOpen={state.isOpen} centered>
      <button className="bt-close button-bg__none" type="button" onClick={closePopUp}>
        <img src={icClose} alt="icon" />
      </button>
      <ModalBody>
        <h2 className="header_2">
          {hereThereBt}
          {' '}
          <img src={icHi} alt="hi" />
        </h2>
        <hr />
        <ReactCountryFlag countryCode={props.codeCountry} className="emojiFlag" />
        <br />
        <div className="text-2">
          {sorryBt.replace('{country}', countries[props.codeCountry?.toUpperCase()]?.name)}
        </div>
        <div className="div-country">
          <div className="text-title">
            {changeBt}
          </div>
          <Select
            value={state.country}
            onChange={onChangeCountry}
            options={props.listCountry}
            className={classnames('input-text input-select')}
            components={generateSelectInputComponents()}
            placeholder="Select Country"
          />
          <ButtonCT
            name={continueBt}
            className="button-black"
            disabled={!state.nSymbol}
            onClick={onClickContinute}
          />
        </div>
      </ModalBody>
    </Modal>
  );
}

export default PopupDelivery;
