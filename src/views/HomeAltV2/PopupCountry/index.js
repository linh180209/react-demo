import React, { useState, useEffect } from 'react';
import { Modal, ModalBody } from 'reactstrap';
import classnames from 'classnames';
import ReactCountryFlag from 'react-country-flag';
import icShipping from '../../../image/icon/ic-shipping-v2.svg';
import icMoney from '../../../image/icon/ic-money-v2.svg';
import ButtonCT from '../../../components/ButtonCT';
import icClose from '../../../image/icon/ic-close-black.svg';
import './styles.scss';
import { addFontCustom, getNameFromButtonBlock } from '../../../Redux/Helpers';

function PopupCountry(props) {
  const closePopUp = () => {
    props.onClose();
  };

  const maison21Bt = getNameFromButtonBlock(props.buttonBlocks, 'Maison 21G ships to');
  const shopInBt = getNameFromButtonBlock(props.buttonBlocks, 'Shop in');
  const getShippingBt = getNameFromButtonBlock(props.buttonBlocks, 'Get shipping options for');
  const shopNowBt = getNameFromButtonBlock(props.buttonBlocks, 'Shop now');
  return (
    <Modal className={classnames('popup-country', addFontCustom())} isOpen centered>
      <button className="bt-close button-bg__none" type="button" onClick={closePopUp}>
        <img src={icClose} alt="icon" />
      </button>
      <ModalBody>
        {/* <ReactCountryFlag countryCode={props.nSymbol?.code} className="emojiFlag" /> */}
        <div className="text-des">
          <span>{maison21Bt}</span>
          <span>
            <b>
              {props.nSymbol?.name}
              {/* {' '}
              (
              {props.nSymbol?.currency?.name}
              {' '}
              {props.nSymbol?.currency?.symbol}
              ) */}
            </b>
          </span>
        </div>
        <div className="item">
          <img src={icMoney} alt="icon" />
          <span>
            {shopInBt}
            {' '}
            {props.nSymbol?.currency?.name}
            {' '}
            {props.nSymbol?.currency?.symbol}
          </span>
        </div>
        <div className="item">
          <img src={icShipping} alt="icon" />
          <span>
            {getShippingBt}
            {' '}
            {props.nSymbol?.name}
          </span>
        </div>
        <ButtonCT
          name={shopNowBt}
          className="button-black"
          onClick={props.onClickContinute}
        />
      </ModalBody>
    </Modal>
  );
}

export default PopupCountry;
