/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import MetaTags from 'react-meta-tags';

import Sticky from 'react-sticky-el';
import smoothscroll from 'smoothscroll-polyfill';
import { Container, Row, Col } from 'reactstrap';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import loadingPage from '../../Redux/Actions/loading';
import { loginUpdateUserInfo } from '../../Redux/Actions/login';
import {
  scrollTop,
  getSEOFromCms,
  fetchCMSHomepage,
  googleAnalitycs,
  getCmsCommon,
  getNameFromCommon,
  generateHreflang,
  generateUrlWeb,
  removeLinkHreflang,
  setPrerenderReady,
} from '../../Redux/Helpers';
import './styles.scss';
import ItemScent from './components/ItemScent';
import DetailScent from './components/DetailScent';
import addCmsRedux from '../../Redux/Actions/cms';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { GET_SCENT_LIB_GLOSSAIRE } from '../../config';
import icBackToTop from '../../image/icon/ic-back-to-top.svg';
import icBackToTopMobile from '../../image/icon/ic-scroll-top-mobile.svg';
import { isMobile } from '../../DetectScreen';
import FooterV2 from '../../views2/footer';

const prePareCms = (glossaireCms) => {
  if (glossaireCms) {
    const seo = getSEOFromCms(glossaireCms);
    const { body } = glossaireCms;
    const iconBlocks = _.filter(body, x => x.type === 'icons_block')[0];
    const iconIngredientsBlocks = _.filter(
      body,
      x => x.type === 'icons_block',
    )[1];
    const imgBlocks = _.filter(body, x => x.type === 'image_block')[0];

    const labelAndTextBlocks = _.filter(
      body,
      x => x.type === 'label_and_text_block',
    );
    return {
      seo,
      iconBlocks,
      labelAndTextBlocks,
      imgBlocks,
      iconIngredientsBlocks,
    };
  }
  return {
    seo: undefined,
  };
};
class Glossaire extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seo: undefined,
      activeDetail: null,
      isShowScentDetailView: false,
      data: [],
      idScroll: 'A',
      searchContent: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.keyPress = this.keyPress.bind(this);
  }

  componentDidMount() {
    setPrerenderReady();
    googleAnalitycs(window.location.pathname);
    this.fetchDataInit();
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.addEventListener('scroll', this.handleScroll);
    removeLinkHreflang();
  }


  gotoGlossairy = (id, isScrollInTop) => {
    const ele = document.getElementById(id);
    // if (ele) {
    //   this.handelHideDetail();
    //   setTimeout(() => {
    //     smoothscroll.polyfill();
    //     ele.scrollIntoView({ behavior: "smooth", block: 'center' });
    //     this.setState({ idScroll: id });
    //   }, 500);
    // }
    setTimeout(
      () => {
        const element = document.getElementById(id);
        const elementRect = element.getBoundingClientRect();
        const absoluteElementTop = elementRect.top + window.pageYOffset;
        const middle = absoluteElementTop - (window.innerHeight / 2 - 70);
        const top = !isMobile ? absoluteElementTop - 180 : absoluteElementTop - 160;
        smoothscroll.polyfill();
        isScrollInTop
          ? window.scrollTo({ top, behavior: 'smooth' })
          : window.scrollTo({ top: middle, behavior: 'smooth' });
      },
      isScrollInTop ? 0 : 700,
    );
    this.setState({ idScroll: id });
  };

  handleScrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  handleScroll = () => {
    const el = document.getElementById('service-section');

    if (
      document.body.scrollTop > (isMobile ? 400 : 600)
      || document.documentElement.scrollTop > (isMobile ? 400 : 600) && el.getBoundingClientRect().bottom > window.innerHeight + 190
    ) {
      const ele = document.getElementById('scroll-top-btn');
      if (ele) {
        ele.style.position = 'fixed';
        ele.style.bottom = '0px';
      }
    } else {
      const ele = document.getElementById('scroll-top-btn');
      if (ele) {
        ele.style.position = 'absolute';
        ele.style.bottom = isMobile ? '0px' : '190px';
      }
    }
  };

  handleChange(e) {
    this.setState({ searchContent: e.target.value });
  }

  keyPress(e) {
    if (e.keyCode == 13 && e.target.value.trim() != '') {
      const key = e.target.value.charAt(0).trim();
      this.gotoGlossairy(key.toUpperCase(), false);
    }
  }

  genCharArray = (charA, charZ) => {
    let a = [],
      i = charA.charCodeAt(0),
      j = charZ.charCodeAt(0);
    for (; i <= j; ++i) {
      a.push(String.fromCharCode(i));
    }
    return a;
  };

  fetchScentLib = () => {
    const options = {
      url: GET_SCENT_LIB_GLOSSAIRE,
      method: 'GET',
    };
    return fetchClient(options);
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      match,
    } = nextProps;
    const objectReturn = {};
    const { rawData } = prevState;
    const anchor = match && match.params.name ? match.params.name : undefined;
    if (anchor && rawData && anchor !== prevState.anchor) {
      _.assign(objectReturn, { anchor, isNewUrl: true });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  componentDidUpdate() {
    const { anchor, isNewUrl, rawData } = this.state;
    if (isNewUrl) {
      this.state.isNewUrl = false;
      const ele = _.find(rawData, x => x.name.replace(/ /g, '_').toLowerCase() === anchor.toLowerCase());
      if (ele) {
        this.handelActiveDetail(ele.name.charAt(0).toUpperCase(), ele.id);
      }
    }
  }

  fetchDataInit = async () => {
    const { match } = this.props;
    const { params } = match;
    const { name } = params || {};
    const { cms, mmos } = this.props;
    const mmoCms = _.find(cms, x => x.title === 'Glossaire');
    if (!mmoCms || _.isEmpty(mmos)) {
      this.props.loadingPage(true);
      const pending = [fetchCMSHomepage('glossaire'), this.fetchScentLib()];
      try {
        const results = await Promise.all(pending);
        const cmsData = results[0];
        const dataScentLib = results[1];
        const group = dataScentLib
          .sort((a, b) => a.name.localeCompare(b.name))
          .reduce((r, e) => {
            const key = e.name[0];
            const a = e.images.length > 0 && _.find(e.images, item => item.type == 'main') ? _.find(e.images, item => item.type == 'main').image : null;
            Object.assign(e, { mainImage: a });
            if (!r[key]) r[key] = [];
            r[key].push(e);
            return r;
          }, {});
        const arrAlphabet = this.genCharArray('a', 'z');
        this.props.addCmsRedux(cmsData);
        const {
          seo,
          iconBlocks,
          labelAndTextBlocks,
          imgBlocks,
          iconIngredientsBlocks,
        } = prePareCms(cmsData);

        this.setState({
          seo,
          data: group,
          rawData: dataScentLib,
          arrAlphabet,
          iconBlocks,
          labelAndTextBlocks,
          imgBlocks,
          iconIngredientsBlocks,
        });
        setTimeout(() => {
          if (name) {
            const ele = _.find(dataScentLib, x => x.name.replace(/ /g, '_').toLowerCase() === name.toLowerCase());
            if (ele) {
              this.handelActiveDetail(ele.name.charAt(0).toUpperCase(), ele.id);
            }
          } else {
            const element = document.getElementById('id-scroll');
            if (element) {
              element.scrollIntoView(true);
              // smoothscroll.polyfill();
            }
          }
        }, 1000);
        this.props.loadingPage(false);
      } catch (error) {
        toastrError(error.message);
        this.props.loadingPage(false);
      }
    } else {
      const { cms: cmsPage } = this.props;
      const cmsT = _.find(cmsPage, x => x.title === 'Glossaire');
      const { seo } = prePareCms(cmsT);
      this.setState({
        seo,
        data,
        iconBlocks,
        labelAndTextBlocks,
        rawData: [],
        arrAlphabet: [],
        imgBlocks,
        iconIngredientsBlocks,
      });
    }
  };

  onClickGotoNewUrl = (id, idScent) => {
    const dataDetail = _.find(this.state.rawData, x => x.id === idScent);
    if (dataDetail) {
      this.props.history.push(generateUrlWeb(`/glossaire/${dataDetail.name.replace(/ /g, '_').toLowerCase()}`));
    }
  }


  handelActiveDetail = (id, idScent) => {
    const dataDetailFilter = _.filter(this.state.rawData, x => x.id === idScent)[0] || [];
    this.setState({
      activeDetail: id,
      isShowScentDetailView: true,
      dataScentDetail: dataDetailFilter,
    });
    this.gotoGlossairy(id, true);
  };

  handelHideDetail = () => {
    this.setState({
      isShowScentDetailView: false,
    });
    this.gotoGlossairy(this.state.idScroll, true);
  };

  render() {
    const {
      seo,
      activeDetail,
      isShowScentDetailView,
      data,
      dataScentDetail,
      arrAlphabet,
      idScroll,
      rawData,
      iconBlocks,
      labelAndTextBlocks,
      imgBlocks,
      iconIngredientsBlocks,
    } = this.state;
    const { login, cms } = this.props;
    const gender = login && login.user && login.user.gender ? login.user.gender : 'unisex';

    const scentLibaryText = _.find(
      labelAndTextBlocks,
      x => x.value.label === 'scent_library',
    );
    const searchIngredientsText = _.find(
      labelAndTextBlocks,
      x => x.value.label === 'search_ingredients',
    );
    const cmsCommon = getCmsCommon(cms);
    const strengthBt = getNameFromCommon(cmsCommon, 'STRENGTH');
    const durationBt = getNameFromCommon(cmsCommon, 'DURATION');
    return (
      <div>
        <MetaTags>
          <title>{seo ? seo.seoTitle : ''}</title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/glossaire')}
        </MetaTags>
        {/* <HeaderHomePage isBgBlack /> */}
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <HeaderHomePageV3 isRemoveMessage />

        <div className="wrapper-glossaire" id="id-scroll">
          <div
            className="banner"
            style={{
              backgroundImage: `url(${imgBlocks && imgBlocks.value.image ? imgBlocks.value.image : ''})`,
            }}
          >
            <h1>
              {scentLibaryText ? scentLibaryText.value.text : 'SCENT LIBRARY'}
            </h1>
          </div>
          <Sticky
            className="search-sticky"
            topOffset={-92}
            stickyStyle={{ transform: 'translateY(102px)', top: isMobile ? '-30px' : '-9px' }}
            // boundaryElement="#article-page"
            // hideOnBoundaryHit={false}
          >
            <div className="search-box">
              <div className="wrapper-search">
                <i className="fa fa-search" />
                <input
                  type="text"
                  name="search"
                  placeholder={
                    searchIngredientsText
                      ? searchIngredientsText.value.text
                      : 'Search ingredients'
                  }
                  onKeyDown={this.keyPress}
                  onChange={this.handleChange}
                />
              </div>
            </div>
          </Sticky>
          <div className="wrapper-item-s" style={{ position: 'relative' }}>
            {arrAlphabet && (
              <Sticky
                className="alphabet-sticky"
                topOffset={-130}
                bottomOffset={!isMobile ? 580 : 640}
                stickyStyle={{
                  transform: !isMobile
                    ? 'translateY(140px)'
                    : 'translateY(127px)',
                }}
                boundaryElement=".wrapper-item-s"
                hideOnBoundaryHit={false}
              >
                <div className="alphabet-bar">
                  {_.map(arrAlphabet, (item, i) => (
                    <span
                      key={i}
                      onClick={() => (data[item.toUpperCase()]
                        ? this.gotoGlossairy(item.toUpperCase(), false)
                        : '')
                      }
                      disabled={!data[item.toUpperCase()]}
                      className={`${
                        idScroll == item.toUpperCase() ? 'active' : ''
                      }`}
                    >
                      <i className="fa fa-circle" />
                      <span
                        className="key"
                        style={{
                          color: data[item.toUpperCase()]
                            ? '#000000'
                            : '#8d8d8d',
                        }}
                      >
                        {item}
                      </span>
                    </span>
                  ))}
                </div>
              </Sticky>
            )}

            <div className="wrapper-item-scent">
              {Object.entries(data).map(([key, value], i) => (
                <div key={i}>
                  <ItemScent
                    handelActiveDetail={this.onClickGotoNewUrl}
                    activeDetail={activeDetail}
                    handelHideDetail={this.handelHideDetail}
                    gotoGlossairy={this.gotoGlossairy}
                    isShowScentDetailView={isShowScentDetailView}
                    id={key}
                    keyName={key}
                    data={value}
                    dataScentDetail={dataScentDetail}
                    rawData={rawData}
                    labelAndTextBlocks={labelAndTextBlocks}
                    iconIngredientsBlocks={iconIngredientsBlocks}
                    strengthBt={strengthBt}
                    durationBt={durationBt}
                  />
                </div>
              ))}
            </div>
          </div>
          <div
            className="scroll-top"
            id="scroll-top-btn"
            onClick={() => this.handleScrollTop()}
            // style={{display: 'none'}}
          >
            <img src={isMobile ? icBackToTopMobile : icBackToTop} />
          </div>
          <div id="service-section" />
          {iconBlocks && (
            <Row
              className="service-section"
              style={{
                backgroundImage: `url(${iconBlocks.value.image_background.image})`,
              }}
            >
              {_.map(iconBlocks.value.icons, (x, i) => (
                <Col md={3}>
                  <div className="service-box">
                    <img loading="lazy" src={x.value ? x.value.image.image : ''} />
                    <p>{x.value.text}</p>
                  </div>
                </Col>
              ))}
            </Row>
          )}
        </div>
        <FooterV2 />
      </div>
    );
  }
}

Glossaire.propTypes = {
  loadingPage: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  loginUpdateUserInfo: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    cms: state.cms,
    mmos: state.mmos,
    basket: state.basket,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  loadingPage,
  addCmsRedux,
  loginUpdateUserInfo,
};

export default connect(mapStateToProps, mapDispatchToProps)(Glossaire);
