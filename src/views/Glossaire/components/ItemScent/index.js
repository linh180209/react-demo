import React, { Component, lazy, Suspense } from 'react';
import _ from 'lodash';
import './styles.scss';
import DetailScent from '../DetailScent';

const LazyImage = lazy(() => import('../../../../components/LazyImage'));

class ItemScent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      activeIndex: 0,
      gif: '',
    };
  }

  handelClickItem = (id, idScent) => {
    this.props.handelActiveDetail(id, idScent);
  };

  changeImage = (urlGif) => {
    this.setState({
      gif: urlGif,
    });
  };

  resetImage = (img) => {
    this.setState({
      gif: '',
    });
  };

  render() {
    const {
      // handelActiveDetail,
      activeDetail,
      id,
      handelHideDetail,
      isShowScentDetailView,
      keyName,
      data,
      rawData,
      dataScentDetail,
      labelAndTextBlocks,
      iconIngredientsBlocks,
      gotoGlossairy,
      images,
      strengthBt,
      durationBt,
    } = this.props;
    const { gif } = this.state;
    return (
      <div>
        <div className="item-glossaire" id={id}>
          <h1>{keyName || ''}</h1>
          {activeDetail === id && isShowScentDetailView == true ? (
            <DetailScent
              handelHideDetail={handelHideDetail}
              gotoGlossairy={gotoGlossairy}
              data={dataScentDetail || []}
              dataGroup={data}
              idGroup={id}
              handelClickItem={this.handelClickItem}
              rawData={rawData}
              iconIngredientsBlocks={iconIngredientsBlocks}
              labelAndTextBlocks={labelAndTextBlocks}
              strengthBt={strengthBt}
              durationBt={durationBt}
            />
          ) : (
            <div className="scent-box-group">
              {_.map(data, (item, i) => (
                <div className="image-article" key={i}>
                  <div
                    className="image"
                    onClick={() => this.handelClickItem(id, item.id)}
                  >
                    <Suspense fallback={<div />}>
                      <LazyImage
                        src={item.mainImage ? item.mainImage : ''}
                        className="img"
                      />
                    </Suspense>
                    {/* {!isMobile ? (
                        <Suspense fallback={<div />}>
                          <LazyImage src={gif} className="gif-img" />
                        </Suspense>
                      ) : (
                        ""
                      )} */}
                    <div className="info-scent">
                      <p className="name">{item.ingredient.name || ''}</p>
                      <p
                        className="title"
                        style={{
                          background: 'none',
                          opacity: '0.7',
                        }}
                      >
                        {item.family ? item.family : ''}
                      </p>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default ItemScent;
