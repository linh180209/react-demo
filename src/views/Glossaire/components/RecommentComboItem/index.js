import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import { withRouter } from 'react-router-dom';
import DetailScent from '../DetailScent';
import ProgressCircle from '../../../ResultScentV2/progressCircle';
import ProgressLine from '../../../ResultScentV2/progressLine';
import icDefault from '../../../../image/icon/default-image.png';
import { generateUrlWeb } from '../../../../Redux/Helpers';

class RecommentComboItem extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     data: [],
  //   };
  // }
  render() {
    const {
      data, profile, idCombo, shopThisComboText, strengthBt, durationBt,
    } = this.props;

    return data ? (
      <div className="recomment-combo-item">
        <h4>{data[1].family}</h4>
        <div className="scent-mix">
          <div className="wrapper-scent">
            <div>
              <img
                alt="scent"
                src={
                  _.find(data[0].images, x => x.type == 'main')
                    ? _.find(data[0].images, x => x.type == 'main').image
                    : ''
                }
                style={{ objectFit: 'cover' }}
              />
              <span>{data[0].ingredient.name}</span>
            </div>
            <div>
              <img
                alt="scent"
                src={
                  _.find(data[1].images, x => x.type == 'main')
                    ? _.find(data[1].images, x => x.type == 'main').image
                    : ''
                }
                style={{ objectFit: 'cover', background: 'black' }}
              />
              <span>{data[1].ingredient.name}</span>
            </div>
          </div>
        </div>
        <div className="div-progress">
          <div className="div-process-line">
            {_.map(profile ? profile.accords : [], x => (
              <ProgressLine data={x} isShowPercent />
            ))}
          </div>
          <div className="div-process-circle">
            <ProgressCircle
              title={strengthBt}
              percent={
                profile ? parseInt(parseFloat(profile.strength) * 100, 10) : 0
              }
            />
            <ProgressCircle
              title={durationBt}
              percent={
                profile ? parseInt(parseFloat(profile.duration) * 100, 10) : 0
              }
            />
          </div>
        </div>
        <p
          className="btn-shop"
          onClick={() => (idCombo
            ? this.props.history.push(generateUrlWeb(`/products/${data[0]?.id}/${data[1]?.id}`))
            : '')
          }
        >
          {shopThisComboText ? shopThisComboText.value.text : 'Shop this combo'}
        </p>
      </div>
    ) : (
      ''
    );
  }
}

export default withRouter(RecommentComboItem);
