import PropTypes from 'prop-types';
import React, { Component } from 'react';
import classNames from 'classnames';
import iconFullScreen from '../../../../image/icon-fullscreen-video.svg';

const propTypes = {
  player: PropTypes.object,
  className: PropTypes.string
};

export default class FullScreenButton extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.handelToggleModalVideo(this.props.player.currentSrc);
  }

  render() {
    const { player, className } = this.props;
    const { currentSrc } = player;

    return (
      <a
        ref={c => {
          this.button = c;
        }}
        className={classNames(className, {
          'video-react-control': true,
          'video-react-button': true,
          'video-react-icon' : true,
          'icon-fullscreen-custom-glossaire': true
        })}
        // href={currentSrc}
        download
        style={{
          // backgroundImage: `url(${iconFullScreen})`,
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center',
          cursor: 'pointer',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        }}
        tabIndex="0"
        onClick={this.handleClick}
      >
        <img src={iconFullScreen}/>
      </a>
    );
  }
}
FullScreenButton.propTypes = propTypes;