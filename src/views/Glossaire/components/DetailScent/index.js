import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import { Row, Col, Modal } from 'reactstrap';
import {
  Player, ControlBar, Shortcut, BigPlayButton,
} from 'video-react';
import classnames from 'classnames';
import HtmlParser from 'react-html-parser';
import CircleType from 'circletype';
import { withRouter } from 'react-router-dom';
import icNext from '../../../../image/icon/ic-next-mmo.svg';
import icBack from '../../../../image/icon/back-product.svg';
import FullScreenButton from './FullScreenButton';
import RecommentComboItem from '../RecommentComboItem';
import fetchClient, {
  fetchClientFormData,
} from '../../../../Redux/Helpers/fetch-client';
import icDefault from '../../../../image/icon/default-image.png';
import iconFullScreen from '../../../../image/icon-fullscreen-video.svg';
import { toastrError } from '../../../../Redux/Helpers/notification';
import { CREATE_PRODUCT_URL } from '../../../../config';
import { addFontCustom, generateUrlWeb, getAltImage } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';
// import HTMLEllipsis from "react-lines-ellipsis/lib/html";

class DetailScent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEllipseText: false,
      isOpenModalVideo: false,
      urlVideo: '',
      duration: '',
      isActiveVideo: false,
    };
  }

  setCircleText = () => {
    const ele1 = document.getElementById('span-1');
    if (ele1) {
      new CircleType(document.getElementById('span-1'))
        .radius(150).dir(-1);
    }
    const ele2 = document.getElementById('span-2');
    if (ele2) {
      new CircleType(document.getElementById('span-2'))
        .radius(150).dir(1);
      setTimeout(() => {
        ele2.style.transform = 'rotate(-50deg)';
      }, 0);
    }
    const ele3 = document.getElementById('span-3');
    if (ele3) {
      new CircleType(document.getElementById('span-3'))
        .radius(150).dir(1);
      setTimeout(() => {
        ele3.style.transform = 'rotate(50deg)';
      }, 100);
    }
  }

  onCreateMix = (id1, id2) => {
    // const { data1, data2 } = this.state;
    const { login } = this.props;
    if (login && login.user) {
      this.createProduct(id1, id2);
    } else {
      this.props.history.push(generateUrlWeb(`/products/${id1}/${id2}`));
    }
    this.setState({ isCreated: true });
  };

  createProduct = (id1, id2) => {
    const formData = new FormData();
    formData.append('combo', JSON.stringify([id1, id2]));
    const option = {
      url: CREATE_PRODUCT_URL,
      method: 'POST',
      body: formData,
    };
    fetchClientFormData(option, true)
      .then((result) => {
        if (result) {
          this.props.history.push(generateUrlWeb(`/products/${id1}/${id2}`));
        }
      })
      .catch((err) => {
        toastrError(err);
      });
  };

  handelClickItemDetail = (id, idScent) => {
    this.setState({ isEllipseText: false });
    this.props.handelClickItem(id, idScent);
  };

  toggleModalVideo = (url) => {
    this.setState({
      isOpenModalVideo: !this.state.isOpenModalVideo,
      urlVideo: url,
    });
  };

  toHHMMSS = (secs) => {
    const sec_num = parseInt(secs, 10);
    const hours = Math.floor(sec_num / 3600);
    const minutes = Math.floor(sec_num / 60) % 60;
    const seconds = sec_num % 60;

    return [hours, minutes, seconds]
      .map(v => (v < 10 ? `0${v}` : v))
      .filter((v, i) => v !== '00' || i > 0)
      .join(':');
  };

  componentDidMount = () => {
    this.setState({ isShowText: true }, () => {
      setTimeout(() => {
        this.setCircleText();
      }, 300);
    });
    setTimeout(() => {
      if (this.player) {
        this.setState({
          duration: this.player.getState().player.duration,
        });
      }
    }, 500);
    this.player
      && this.player.subscribeToStateChange(this.handleStateChange.bind(this));
  };

  componentDidUpdate = (prevProps, prevState) => {
    const { data } = this.props;
    if (data !== prevProps.data) {
      this.setState({ isShowText: false });
      setTimeout(() => {
        this.setState({ isShowText: true }, () => {
          setTimeout(() => {
            this.setCircleText();
          }, 300);
        });
      }, 300);
    }
  }

  handleStateChange(state, prevState) {
    // copy player state to this component's state
    this.setState({
      isActiveVideo: state.hasStarted,
    });
  }

  render() {
    const {
      handelHideDetail,
      data,
      dataGroup,
      idGroup,
      handelClickItem,
      rawData,
      iconIngredientsBlocks,
      labelAndTextBlocks,
      strengthBt,
      durationBt,
    } = this.props;
    const {
      isEllipseText,
      isOpenModalVideo,
      urlVideo,
      duration,
      isActiveVideo,
    } = this.state;
    const indexNextScent = dataGroup.findIndex(x => x.id === data.id);
    const dataNextScent = dataGroup ? dataGroup[indexNextScent + 1] : '';
    const dataPrevScent = dataGroup ? dataGroup[indexNextScent - 1] : '';
    const listDataNextScent = dataGroup
      ? dataGroup.slice(indexNextScent + 1)
      : '';
    const dataCombo1 = rawData
      ? rawData.filter(item => (data
        ? data.suggestion_combos[0].combo.indexOf(item.id) !== -1
        : ''))
      : '';
    const dataCombo2 = rawData
      ? rawData.filter(item => (data
        ? data.suggestion_combos[1].combo.indexOf(item.id) !== -1
        : ''))
      : '';
    const dataCombo3 = rawData
      ? rawData.filter(item => (data
        ? data.suggestion_combos[2].combo.indexOf(item.id) !== -1
        : ''))
      : '';
    const backToAllIngredients = labelAndTextBlocks
      ? _.find(
        labelAndTextBlocks,
        x => x.value.label == 'back_to_all_ingredients',
      )
      : '';
    const benefitsForYourMind = labelAndTextBlocks
      ? _.find(
        labelAndTextBlocks,
        x => x.value.label == 'benefits_for_your_mind',
      )
      : '';
    const prev = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label == 'prev')
      : '';
    const next = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label == 'next')
      : '';
    const ingredientHistory = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label == 'ingredient_history')
      : '';
    const personality = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label == 'personality')
      : '';
    const evolutionOfScent = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label == 'evolution_of_scent')
      : '';
    const perfumeStrength = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label == 'perfume_strength')
      : '';
    const recommentCombo = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label == 'recomment_combo')
      : '';
    const recommentIngredients = labelAndTextBlocks
      ? _.find(
        labelAndTextBlocks,
        x => x.value.label == 'recomment_ingredient',
      )
      : '';
    const learnMoreInOurBlog = labelAndTextBlocks
      ? _.find(
        labelAndTextBlocks,
        x => x.value.label == 'learn_more_in_our_blog',
      )
      : '';
    const visitM21Blog = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label == 'visit_m21_blog')
      : '';
    const nextOnTheList = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label == 'next_on_the_list')
      : '';
    const shopThisComboText = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label == 'shop_this_combo')
      : '';
    const sublteBt = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label === 'Sublte')
      : '';
    const strongBt = labelAndTextBlocks
      ? _.find(labelAndTextBlocks, x => x.value.label === 'Strong')
      : '';

    return (
      <div className="detail-scent-glossaire animated fadeIn faster">
        <div className="header-detail">
          <div className="info">
            <h2>{data.name || ''}</h2>
            <span>{data.family || ''}</span>
          </div>
          {!isMobile ? (
            <div className="action-box">
              <button
                className="btn-back"
                onClick={() => {
                  handelHideDetail();
                }}
              >
                {backToAllIngredients
                  ? backToAllIngredients.value.text
                  : 'BACK TO ALL INGREDIENTS'}
                {' '}
                <img src={icNext} alt="Next" />
              </button>
              <div className="group-box">
                <button
                  className="btn-prev"
                  onClick={() => this.handelClickItemDetail(idGroup, dataPrevScent.id)
                  }
                  disabled={!dataPrevScent}
                >
                  <img src={icBack} alt="Back" />
                  {prev ? prev.value.text : 'prev'}
                </button>
                <button
                  className="btn-next"
                  onClick={() => this.handelClickItemDetail(idGroup, dataNextScent.id)
                  }
                  disabled={!dataNextScent}
                >
                  {next ? next.value.text : 'Next'}
                  {' '}
                  <img src={icNext} alt="Next" />
                </button>
              </div>
            </div>
          ) : (
            ''
          )}
        </div>
        <Row className="wrapper-detail">
          <Col md={12}>
            <div
              className={
                data.videos.length > 0 && data.videos[0].video
                  ? 'video-detail'
                  : ''
              }
            >
              {data.videos.length > 0 ? (
                <div style={{ width: '100%', height: '100%' }}>
                  {
                    isMobile ? (
                      <video
                        style={{
                          objectFit: 'cover',
                          width: '100%',
                          height: '100%',
                        }}
                        muted="true"
                        playsinline="true"
                        loop="true"
                        preload="auto"
                        src={data.videos[0].video}
                        controls
                        poster={
                          data.ingredient.top_banner
                            ? data.ingredient.top_banner
                            : ''
                        }
                      />
                    ) : (
                      <React.Fragment>
                        <Player
                          playsInline
                          src={data.videos[0].video}
                          fluid={false}
                          width="100%"
                          height="100%"
                          aspectRatio="4:4"
                          poster={
                          data.ingredient.top_banner
                            ? data.ingredient.top_banner
                            : ''
                        }
                          ref={(player) => {
                            this.player = player;
                          }}
                        >
                          <ControlBar autoHide>
                            <FullScreenButton
                              order={7}
                              handelToggleModalVideo={this.toggleModalVideo}
                            />
                          </ControlBar>
                          <Shortcut dblclickable />
                          <BigPlayButton />
                        </Player>
                        {isActiveVideo == false && duration ? (
                          <div className="info-video-custom">
                            <span>{duration ? this.toHHMMSS(duration) : ''}</span>
                            <a
                              className="icon-fullscreen-custom-glossaire"
                              onClick={() => this.toggleModalVideo(data.videos[0].video)
                            }
                            >
                              <img src={iconFullScreen} />
                            </a>
                          </div>
                        ) : (
                          ''
                        )}
                      </React.Fragment>
                    )
                  }
                  {urlVideo ? (
                    <Modal
                      isOpen={isOpenModalVideo}
                      toggle={this.toggleModalVideo}
                      className={classnames('modal-video-detail', addFontCustom())}
                      fade
                    >
                      <Player
                        playsInline
                        src={urlVideo || ''}
                        // fluid={false}
                        // width={`100%`}
                        // height={`100%`}
                        autoPlay
                      >
                        <ControlBar />
                        <Shortcut dblclickable />
                        <BigPlayButton />
                      </Player>
                    </Modal>
                  ) : (
                    ''
                  )}
                </div>
              ) : (
                ''
              )}
            </div>
          </Col>
        </Row>
        <hr className="hr-benefit" />
        {!isMobile && (
          <div className="item-benefit-wrapper">
            <h3 className="title-benefit">
              {benefitsForYourMind
                ? benefitsForYourMind.value.text
                : 'BENEFITS FOR YOUR MIND, BODY & SOUL'}
            </h3>
            <div className="content-benefit">
              {_.map(data.ingredient.mood_enhancements || [], (item, i) => (
                <span key={i}>
                  <i className="fa fa-plus" />
                  {' '}
                  {item}
                </span>
              ))}
              {_.map(data.ingredient.mind_enhancements || [], (item, i) => (
                <span key={i}>
                  <i className="fa fa-plus" />
                  {' '}
                  {item}
                </span>
              ))}
            </div>
          </div>
        )}
        <hr className="hr-benefit" />
        {isMobile && (
          <div className="item-benefit-wrapper">
            <h3 className="title-benefit">
              {benefitsForYourMind
                ? benefitsForYourMind.value.text
                : 'BENEFITS FOR YOUR MIND, BODY & SOUL'}
            </h3>
            <div className="content-benefit">
              {_.map(data.ingredient.mood_enhancements || [], (item, i) => (
                <span key={i}>
                  <i className="fa fa-plus" />
                  {' '}
                  {item}
                </span>
              ))}
              {_.map(data.ingredient.mind_enhancements || [], (item, i) => (
                <span key={i}>
                  <i className="fa fa-plus" />
                  {' '}
                  {item}
                </span>
              ))}
            </div>
          </div>
        )}
        <Row className="item-ingredient-wrapper">
          <Col lg="4" md="12" xs="12" className="div-col items-center">
            <div className="div-col items-center div-persinal-image">
              <div className="div-title">
                <span>
                  {personality ? personality.value.text : 'PERSONALITY'}
                </span>
              </div>
              {
                this.state.isShowText && (
                  <div className="div-image">
                    <img loading="lazy" src={data.ingredient.scent_dna} alt="persional" />
                    {
                      _.map(data.ingredient.scents, (x, index) => (
                        <div id={`span-${index + 1}`}>{x}</div>
                      ))
                    }
                  </div>
                )
              }
            </div>
          </Col>
          <Col lg="4" md="12" xs="12" className="div-col items-center">
            <div className="div-col items-center div-evolution">
              <div className="div-title">
                <span>
                  {evolutionOfScent
                    ? evolutionOfScent.value.text
                    : 'EVOLUTION OF SCENT'}
                </span>
              </div>
              {
                this.state.isShowText && (
                  <div className="div-image">
                    <img loading="lazy" src={data.ingredient.scent_evolution} alt="evolution" />
                    {
                      _.map(data.ingredient.accords, x => (
                        <span>{x}</span>
                      ))
                    }
                  </div>
                )
              }
            </div>
          </Col>
          <Col lg="4" md="12" xs="12" className="div-col items-center">
            <div className="div-col items-center div-strength">
              <div className="div-title">
                <span>
                  {perfumeStrength
                    ? perfumeStrength.value.text
                    : 'PERFUME STRENGTH'}
                </span>
              </div>
              {
                this.state.isShowText && (
                  <div className="div-image">
                    <img loading="lazy" src={data.ingredient.scent_strength} alt="strength" />
                    <span>{sublteBt ? sublteBt.value.text : ''}</span>
                    <span>{strongBt ? strongBt.value.text : ''}</span>
                    {/* <span>%</span> */}
                  </div>
                )
              }
            </div>
          </Col>
          {/* <Col md={4}>
            <div className="item-ingredient">
              <div className="div-title">
                <span>
                  {personality ? personality.value.text : 'PERSONALITY'}
                </span>
              </div>
              <img alt="scent_dna" src={data.ingredient.scent_dna || ''} />
            </div>
          </Col>
          <Col md={4}>
            <div className="item-ingredient">
              <div className="div-title">
                <span>
                  {evolutionOfScent
                    ? evolutionOfScent.value.text
                    : 'EVOLUTION OF SCENT'}
                </span>
              </div>
              <img
                alt="scent_evolution"
                src={data.ingredient.scent_evolution || ''}
              />
            </div>
          </Col>
          <Col md={4}>
            <div className="item-ingredient">
              <div className="div-title">
                <span>
                  {perfumeStrength
                    ? perfumeStrength.value.text
                    : 'PERFUME STRENGTH'}
                </span>
              </div>
              <img
                alt="scent_strength"
                src={data.ingredient.scent_strength || ''}
              />
            </div>
          </Col> */}
        </Row>
        <Row className="recomment-combo-wrapper">
          <Col md={12}>
            <h3 className="recommend-title">
              {recommentCombo
                ? recommentCombo.value.text
                : 'recommended combos'}
            </h3>
          </Col>
          <Col md={4}>
            <RecommentComboItem
              data={dataCombo1}
              profile={data.suggestion_combos[0].profile}
              onCreateMix={this.onCreateMix}
              shopThisComboText={shopThisComboText}
              idCombo={
                data.suggestion_combos[0]
                  ? data.suggestion_combos[0].combo
                  : null
              }
              strengthBt={strengthBt}
              durationBt={durationBt}
            />
          </Col>
          <Col md={4}>
            <RecommentComboItem
              data={dataCombo2}
              profile={data.suggestion_combos[1].profile}
              onCreateMix={this.onCreateMix}
              shopThisComboText={shopThisComboText}
              idCombo={
                data.suggestion_combos[1]
                  ? data.suggestion_combos[0].combo
                  : null
              }
              strengthBt={strengthBt}
              durationBt={durationBt}
            />
          </Col>
          <Col md={4}>
            <RecommentComboItem
              data={dataCombo3}
              profile={data.suggestion_combos[2].profile}
              onCreateMix={this.onCreateMix}
              shopThisComboText={shopThisComboText}
              idCombo={
                data.suggestion_combos[2]
                  ? data.suggestion_combos[0].combo
                  : null
              }
              strengthBt={strengthBt}
              durationBt={durationBt}
            />
          </Col>
        </Row>
        {/* <div className="recomment-ingredient-wrapper">
          <h3>
            {recommentIngredients
              ? recommentIngredients.value.text
              : 'recommended INGREDIENTS'}
          </h3>
          <div className="items-ingredient">
            {_.map(data.suggestions || [], (item, i) => (
              <div
                className="image-ingredient image-ingredient-hover"
                style={{ cursor: 'pointer' }}
                key={i}
                onClick={() => this.handelClickItemDetail(
                  item.name.toUpperCase().charAt(0),
                  item.id,
                )
                }
              >
                <div
                  className="image"
                  style={{
                    backgroundImage: `url(${
                      _.find(item.images, x => x.type == 'main')
                        ? _.find(item.images, x => x.type == 'main').image
                        : icDefault
                    })`,
                  }}
                />
                <div className="tag">
                  <p className="name">{item.name || ''}</p>
                  <p
                    className="des"
                  >
                    {item.family || ''}
                  </p>
                </div>
              </div>
            ))}
          </div>
        </div> */}

        <div className="div-logo">
          <img loading="lazy" src={data.ingredient.eco} alt={getAltImage(data.ingredient.eco)} />
        </div>

        {/* {iconIngredientsBlocks ? (
          <div className="ic-info-ingredient">
            {iconIngredientsBlocks.value.icons
              && _.map(iconIngredientsBlocks.value.icons, (item, i) => (
                <div className="ic-box" key={i}>
                  <img
                    src={
                      item.value.image.image_webp
                        ? item.value.image.image_webp
                        : ''
                    }
                    alt={item.value.image.caption}
                  />
                  <div className="info-ic">
                    <p>{item.value.text || ''}</p>
                    <p>{item.value.image.caption || ''}</p>
                  </div>
                </div>
              ))}
          </div>
        ) : (
          ''
        )} */}
        <Row className="wrapper-detail">
          <Col md={12}>
            <div className="content-detail">
              <h3>
                {ingredientHistory
                  ? ingredientHistory.value.text
                  : 'ingredient history'}
              </h3>
              <div
                className={`content ${
                  isEllipseText ? 'active-full-content' : ''
                }`}
              >
                {HtmlParser(data.ingredient.description || '')}
                <span
                  className="read-more-btn"
                  onClick={() => this.setState({ isEllipseText: true })}
                >
                  Read more
                </span>
              </div>
            </div>
          </Col>
        </Row>
        <div className="footer-detail">
          {/* <h2>
            {learnMoreInOurBlog
              ? learnMoreInOurBlog.value.text
              : 'learn more in our blog'}
          </h2>
          <a href={data.ingredient.url || ''}>
            {visitM21Blog ? visitM21Blog.value.text : 'VISIT M21G BLOG'}
          </a> */}

          {dataNextScent && (
            <div>
              <h3>
                {nextOnTheList ? nextOnTheList.value.text : 'next on the list'}
              </h3>
              <div
                className={`d-flex flex-wrap ${
                  !isMobile ? 'justify-content-center' : 'justify-content-start'
                }`}
              >
                {listDataNextScent
                  ? _.map(listDataNextScent, (dataNextScent, i) => (
                    <div
                      className="image-ingredient"
                      onClick={() => this.handelClickItemDetail(idGroup, dataNextScent.id)
                        }
                      style={{
                        cursor: 'pointer',
                        maxWidth: !isMobile ? '210px' : '130px',
                        margin: '1em',
                      }}
                    >
                      <div
                        className="image"
                        style={{
                          backgroundImage: `url(${
                            _.find(
                              dataNextScent.images,
                              x => x.type == 'main',
                            )
                              ? _.find(
                                dataNextScent.images,
                                x => x.type == 'main',
                              ).image
                              : icDefault
                          })`,
                        }}
                      />
                      <div className="tag">
                        <p className="name">
                          {dataNextScent ? dataNextScent.ingredient.name : ''}
                        </p>
                        <p className="des">
                          {dataNextScent
                            ? dataNextScent.family
                            : ''}
                        </p>
                      </div>
                    </div>
                  ))
                  : ''}
              </div>
            </div>
          )}
        </div>
        {isMobile ? (
          <div className="action-box action-box-mobile">
            <div className="group-box">
              <button
                className="btn-prev"
                onClick={() => this.handelClickItemDetail(idGroup, dataPrevScent.id)
                }
                disabled={!dataPrevScent}
              >
                <img src={icBack} alt="Back" />
                {prev ? prev.value.text : 'prev'}
              </button>
              <button
                className="btn-next"
                onClick={() => this.handelClickItemDetail(idGroup, dataNextScent.id)
                }
                disabled={!dataNextScent}
              >
                {next ? next.value.text : 'next'}
                {' '}
                <img src={icNext} alt="Next" />
              </button>
            </div>
            <button
              className="btn-back"
              onClick={() => {
                handelHideDetail();
              }}
            >
              <img src={icBack} alt="Back" />
              {' '}
              {backToAllIngredients
                ? backToAllIngredients.value.text
                : 'BACK TO ALL INGREDIENTS'}
            </button>
          </div>
        ) : (
          ''
        )}
        <hr className="hr-footer" />
      </div>
    );
  }
}

export default withRouter(DetailScent);
