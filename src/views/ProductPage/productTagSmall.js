import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getUrlScentDesigner, getAltImageV2, generateUrlWeb } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

class ProductTagSmall extends Component {
  onClickLink = (url) => {
    if (url === '/quiz') {
      const link = getUrlScentDesigner(this.props.login);
      this.props.history.push(generateUrlWeb(link));
      return;
    }
    this.props.history.push(generateUrlWeb(url));
  }

  render() {
    const { data } = this.props;
    const width = isMobile ? (isTablet ? 200 : 100) : 240;
    const height = width * 219 / 240;
    const isPhone = isMobile && !isTablet;
    if (!data) {
      return (<div />);
    }
    const { value } = data;
    return (
      <div
        style={{
          width: `${width}px`,
          height: `${height}px`,
        }}
        className="div-col justify-center items-center relative"
      >
        <img
          loading="lazy"
          style={{
            width: '100%',
            height: '100%',
            borderRadius: '20px',
            objectFit: 'cover',
            zIndex: '-1',
            position: 'absolute',
          }}
          src={value ? value.image.image : ''}
          alt={getAltImageV2(value ? value.image : undefined)}
        />
        <button
          type="button"
          className="w-100 h-100 button-bg__none"
          onClick={() => this.onClickLink(value ? value.link : '')}
        >
          <span style={{
            // color: '#ffffff',
            textAlign: 'center',
            fontSize: isPhone ? '0.8rem' : '1.5rem',
          }}
          >
            {value ? value.text : ''}
          </span>
        </button>
      </div>
    );
  }
}

ProductTagSmall.propTypes = {
  data: PropTypes.shape(PropTypes.object).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default ProductTagSmall;
