import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactHtmlParser from 'react-html-parser';
import { getAltImageV2 } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';


class ProductTag extends Component {
  render() {
    const { data, isShow } = this.props;
    if (!data) {
      return (<div />);
    }
    const { value } = data;
    const isPhone = isMobile && !isTablet;
    return (
      <div
        id={this.props.id}
        style={{
          width: '100%',
          height: isPhone ? '130px' : '210px',
          position: 'relative',
          cursor: 'pointer',
          marginBottom: isShow ? '0px' : '50px',
        }}
        onClick={this.props.toggleFc}
      >
        <img
          loading="lazy"
          className="absolute w-100 h-100"
          style={{
            borderRadius: '20px',
            objectFit: isPhone ? 'fill' : 'cover',
          }}
          src={value ? value.image.image : ''}
          alt={getAltImageV2(value ? value.image : undefined)}
        />
        <div
          className="div-col w-100 h-100 justify-center items-center"
          style={{
            zIndex: '1',
          }}
        >
          <span style={{
            // color: '#ffffff',
            fontSize: isPhone ? '1rem' : '1.5rem',
            zIndex: '1',
            textAlign: 'center',
          }}
          >
            {value ? value.text : ''}
          </span>
          <span
            style={{
              // color: '#ffffff',
              zIndex: '1',
              textAlign: 'center',
              marginTop: isPhone ? '5px' : '20px',
              fontSize: isPhone ? '0.6rem' : '1rem',
            }}
          >
            {value ? ReactHtmlParser(value.description) : ''}
          </span>
          <button
            type="button"
            className="button-bg__none"
            style={{
              fontSize: isPhone ? '1.2rem' : '2rem',
              // color: '#ffffff',
              zIndex: '1',
            }}
          >
            {isShow ? '-' : '+'}
          </button>
        </div>
      </div>
    );
  }
}

ProductTag.propTypes = {
  data: PropTypes.shape(PropTypes.object).isRequired,
  toggleFc: PropTypes.func.isRequired,
  isShow: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
};
export default ProductTag;
