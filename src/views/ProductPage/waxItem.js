import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { generaCurrency, getAltImageV2 } from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { isMobile, isTablet } from '../../DetectScreen';

class WaxItem extends Component {
  render() {
    const { isBig, data } = this.props;
    const { items, images } = data;
    const isPhone = isMobile && !isTablet;
    const width = isPhone ? 160 : (isMobile ? 200 : 240);
    const height = isPhone ? width * 177 / 240 : (isBig ? width * 264 / 240 : width * 177 / 240);
    const imageUrl = _.find(images, x => x.type === 'unisex');
    return (
      <div className="div-col justify-center items-center">
        <div style={{
          width: `${width}px`,
          height: `${height}px`,
        }}
        >
          <img
            loading="lazy"
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'cover',
              borderRadius: '20px',
              cursor: 'pointer',
            }}
            src={imageUrl ? imageUrl.image : ''}
            alt={getAltImageV2(imageUrl)}
            onClick={() => this.props.viewProduct(data.id)}

          />
        </div>
        {
          isTablet ? (
            <div className="div-col justify-center items-center">
              <button
                type="button"
                className="button-bg__none mt-2 mb-2"
                style={{ fontSize: isMobile ? '0.5rem' : '0.8rem', padding: '0px' }}
              >
                {generaCurrency(items && items.length > 0 ? items[0].price : '')}
                {' '}
                BUY
              </button>
            </div>
          ) : (
            <div className="div-col justify-center items-center">
              <span style={{
                color: '#000000',
                marginTop: '10px',
              }}
              >
                {generaCurrency(items && items.length > 0 ? items[0].price : '')}
              </span>
              <div className="div-row justify-center items-center mt-2 mb-2">
                <button
                  type="button"
                  className="button-bg__none"
                  style={{ fontSize: isMobile ? '0.5rem' : '0.8rem', padding: '0px' }}
                  onClick={() => this.props.viewProduct(data.id)}
                >
                  VIEW PRODUCT
                </button>
                <span style={{
                  fontSize: '0.9rem',
                  marginLeft: '0.5rem',
                  marginRight: '0.5rem',
                }}
                >
                  |
                </span>
                <button
                  type="button"
                  className="button-bg__none"
                  style={{ fontSize: isMobile ? '0.5rem' : '0.8rem', padding: '0px' }}
                  onClick={() => this.props.onAddToCart(data)}
                >
                  ADD TO CART
                </button>
              </div>
            </div>
          )
        }
      </div>

    );
  }
}

WaxItem.propTypes = {
  isBig: PropTypes.bool.isRequired,
  data: PropTypes.shape(PropTypes.object).isRequired,
  onAddToCart: PropTypes.func.isRequired,
  viewProduct: PropTypes.func.isRequired,
};

export default WaxItem;
