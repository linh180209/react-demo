/* eslint-disable react/no-did-update-set-state */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import filterIcon from '../../image/icon/filter.svg';
import MenuFilterItem from '../../components/MenuFilterItem';
import { getNameFromCommon } from '../../Redux/Helpers';
import icCheck from '../../image/icon/ic-check-circle.svg';
import icChecked from '../../image/icon/ic-checked-circle.svg';

class MenuFilter extends Component {
  state = {
    cgValue: this.props.valueSearch ? this.props.valueSearch.categories : 'ALL',
    oldCategories: this.props.valueSearch ? this.props.valueSearch.categories : 'ALL',
  }

  componentDidUpdate(prevProps) {
    const { oldCategories } = this.state;
    if (prevProps.valueSearch && oldCategories && prevProps.valueSearch.categories !== oldCategories) {
      this.setState({ cgValue: this.props.valueSearch.categories, oldCategories: this.props.valueSearch.categories });
    }
  }

  changeSearchCategories = (data) => {
    this.setState({ cgValue: data });
    if (this.props.changeSearchCategories) {
      this.props.changeSearchCategories(data);
    }
  }

  render() {
    const { total, valueSearch, cmsCommon } = this.props;
    const filterByBt = getNameFromCommon(cmsCommon, 'FILTER_BY');
    const { cgValue } = this.state;
    const categories = _.find(this.props.dataFilter, x => x.title === 'categories');
    const familles = _.find(this.props.dataFilter, x => x.title === 'famille');
    const filters = _.filter(this.props.dataFilter, x => x.title !== 'categories' && x.title !== 'famille');
    return (
      <div className="div-col ml-5" style={{ marginTop: '70px' }}>
        <div className="div-row">
          <button
            type="button"
            // className="button-bg__none pa0 text-filter text-filter-header"
            className={`${cgValue === 'ALL' ? 'text-filter-active' : 'text-filter'} button-bg__none pa0 text-filter-header`}
            style={{ cursor: 'default' }}
          >
            ALL
          </button>
          <span className="ml-2 mr-2">
            /
          </span>
          <button
            type="button"
            // className="button-bg__none pa0 text-filter-active text-filter-header"
            className={`${cgValue === 'ALL' ? 'text-filter' : 'text-filter-active'} button-bg__none pa0 text-filter-header`}
            style={{ cursor: 'default' }}
          >
            {cgValue === 'ALL' ? (categories.data && categories.data.length > 0 ? categories.data[0].title : '') : cgValue}
          </button>
        </div>
        <div className="div-row pb-2 justify-between items-center text-underline mt-4">
          <span className="text-filter-active text-filter-header">
            {filterByBt}
          </span>
          <img src={filterIcon} alt="filter-icon" />
        </div>
        <div className="div-col pt-3 pb-4 text-underline">
          <span className="text-filter-active mb-2 text-filter-header">
            CATEGORIES
          </span>
          {
            _.map(_.orderBy(categories.data, 'title', 'asc'), x => (
              <button
                type="button"
                // className="button-bg__none pa0 tl text-filter-active"
                className={`${cgValue === x.title ? 'text-filter' : 'text-filter-active'} button-bg__none pa0 tl mt-1`}
                onClick={() => this.changeSearchCategories(x.title)}
              >
                <img src={cgValue === x.title ? icChecked : icCheck} alt="icCheck" className="mr-2" style={{ height: '1rem' }} />
                {x.title}
                {' '}
                {`(${x.items.length})`}
              </button>
            ))
          }
          <button
            type="button"
            // className="button-bg__none pa0 tl text-filter-active"
            className={`${cgValue === 'ALL' ? 'text-filter' : 'text-filter-active'} button-bg__none pa0 tl`}
            onClick={() => this.changeSearchCategories('ALL')}
          >
            <img src={cgValue === 'ALL' ? icChecked : icCheck} alt="icCheck" className="mr-2" style={{ height: '1rem' }} />
            {'ALL '}
            {`(${total})`}
          </button>
        </div>
        
        {/* <MenuFilterItem datas={familles} removeSearchFilter={this.props.removeSearchFilter} addSearchFilter={this.props.addSearchFilter} valueSearch={valueSearch} /> */}
        {
          _.map(_.orderBy(filters, 'title', 'asc'), d => (
            <MenuFilterItem datas={d} removeSearchFilter={this.props.removeSearchFilter} addSearchFilter={this.props.addSearchFilter} valueSearch={valueSearch} />
          ))
        }
      </div>
    );
  }
}

MenuFilter.propTypes = {
  dataFilter: PropTypes.arrayOf(PropTypes.object).isRequired,
  total: PropTypes.number.isRequired,
  changeSearchCategories: PropTypes.func.isRequired,
  removeSearchFilter: PropTypes.func.isRequired,
  addSearchFilter: PropTypes.func.isRequired,
  valueSearch: PropTypes.arrayOf(PropTypes.object).isRequired,
  cmsCommon: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default MenuFilter;
