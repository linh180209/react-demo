import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Row, Col } from 'reactstrap';
import WaxItem from './waxItem';
import { generateUrlWeb } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

class Wax extends Component {
  onAddToCart = (data) => {
    const {
      addProductBasketArray, createBasketGuestArray, basket,
    } = this.props;
    const { items, images } = data;
    if (items && items.length !== 0) {
      const itemObject = items[0];
      const item = {
        item: itemObject.id,
        name: itemObject.name,
        is_featured: itemObject.is_featured,
        price: itemObject.price,
        quantity: 1,
        total: parseFloat(itemObject.price) * 1,
        image_urls: images && images.length > 0 ? [images[0].image] : '',
      };
      const dataTemp = {
        idCart: basket.id,
        item,
      };
      console.log('dataTemp', dataTemp);
      if (!basket || !basket.id) {
        createBasketGuestArray([dataTemp]);
      } else {
        addProductBasketArray([dataTemp]);
      }
    }
  }

  viewProduct = (id) => {
    const { history } = this.props;
    history.push(generateUrlWeb(`/product/wax/${id}`));
  }

  render() {
    const isPhone = isMobile && !isTablet;
    const { data } = this.props;
    const data1 = data.slice(0, data.length / 3);
    const data2 = data.slice(data.length / 3, data.length * 2 / 3);
    const data3 = data.slice(data.length * 2 / 3, data.length);
    const dataP1 = data.slice(0, data.length / 2);
    const dataP2 = data.slice(data.length / 2, data.length);
    return (
      <Row className="w-100 ma0 mt-4">
        <Col
          style={{
            padding: '0px',
          }}
          className="div-col items-start"
          md="4"
          xs="6"
        >
          {
            _.map(isPhone ? dataP1 : data1, (d, index) => {
              if (index % 2 === 0) {
                return (<WaxItem data={d} onAddToCart={this.onAddToCart} viewProduct={this.viewProduct} />);
              }
              return (<WaxItem data={d} onAddToCart={this.onAddToCart} viewProduct={this.viewProduct} />);
            })
          }

        </Col>
        {
          isPhone ? (<div />)
            : (
              <Col
                style={{
                  padding: '0px',
                }}
                className="pa0"
                md="4"
                xs="6"
              >
                {
                  _.map(data2, (d, index) => {
                    if (index % 2 === 0) {
                      return (<WaxItem data={d} onAddToCart={this.onAddToCart} viewProduct={this.viewProduct} />);
                    }
                    return (<WaxItem data={d} onAddToCart={this.onAddToCart} viewProduct={this.viewProduct} />);
                  })
                }
              </Col>
            )
        }
        <Col
          style={{
            padding: '0px',
          }}
          className="div-col items-end"
          md="4"
          xs="6"
        >
          {
            _.map(isPhone ? dataP2 : data3, (d, index) => {
              if (index % 2 === 0) {
                return (<WaxItem data={d} onAddToCart={this.onAddToCart} viewProduct={this.viewProduct} />);
              }
              return (<WaxItem data={d} onAddToCart={this.onAddToCart} viewProduct={this.viewProduct} />);
            })
          }
        </Col>
      </Row>
    );
  }
}

Wax.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  createBasketGuestArray: PropTypes.func.isRequired,
  addProductBasketArray: PropTypes.func.isRequired,
  basket: PropTypes.arrayOf(PropTypes.object).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
export default Wax;
