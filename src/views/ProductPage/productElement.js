import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import info from '../../image/icon/info-white.svg';
import { generaCurrency, getAltImageV2 } from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { isMobile, isTablet } from '../../DetectScreen';

class ProductElement extends Component {
  openKnowMore = () => {
    const { data, isGroup } = this.props;
    console.log('data', data);
    const product = isGroup ? data : _.find(data.combo, x => x.product.type === 'Scent');
    const url = product ? _.find(product.product.images, x => x.type === 'long') : '';
    this.props.onOpenKnowMore(url.image);
  }

  addToCart = () => {
    this.props.addToCart([this.props.data]);
  }

  render() {
    const { isGroup, data, idProduct } = this.props;
    if (!data) {
      return (<div />);
    }
    // const product = isGroup ? data : _.find(data.combo, x => x.product.type === 'Scent');
    const typeImage = isGroup ? 'kit' : 'unisex';
    const urlImage = isGroup ? _.find(data.product.images, x => x.type === typeImage) : _.find(data.images, x => x.type === typeImage);
    const { items } = data;
    const isPhone = isMobile && !isTablet;
    return (
      <div
        className="div-col justify-center items-center"
        style={{
          width: '280px',
          position: 'relative',
          margin: 'auto',
        }}
      >
        <div
          className="div-col justify-center items-center relative"
          style={{
            width: '100%',
            height: '150px',
          }}
        >
          <img
            loading="lazy"
            src={urlImage ? urlImage.image : ''}
            alt={getAltImageV2(urlImage)}
            className="absolute w-100 h-100"
            onClick={() => {
              if (!isGroup) {
                this.props.onClickProduct(data.id);
              } else {
                this.props.onClickProduct(idProduct);
              }
            }}
            style={{
              objectFit: 'cover',
              objectPosition: 'center',
              width: '100%',
              height: '100%',
              cursor: 'pointer',

            }}
          />
          {/* <span style={{
            zIndex: '1',
            color: '#ffffff',
          }}
          >
            Brush of rose
          </span> */}
        </div>
        <span
          className={isGroup ? 'hidden' : 'mt-5'}
          style={{
            fontSize: '0.7rem',
            fontWeight: '600',
          }}
        >
          {generaCurrency(items && items.length > 0 ? items[0].price : '')}
        </span>
        <div className={isGroup ? 'hidden' : 'div-row justify-center items-center mt-2'}>
          <button
            type="button"
            className="button-bg__none"
            style={{
              fontSize: '0.8rem',
            }}
            onClick={() => this.props.onClickProduct(data.id)}
          >
            VIEW PRODUCT
          </button>
          <div style={{
            width: '1px',
            height: '1rem',
            background: '#000000',
            marginLeft: '0.8rem',
            marginRight: '0.8rem',
          }}
          />
          <button
            type="button"
            className="button-bg__none"
            style={{
              fontSize: '0.8rem',
            }}
            onClick={this.addToCart}
          >
            ADD TO CART
          </button>
        </div>
        <button
          type="button"
          className="button-bg__none ma-0 pa-0"
          style={{ position: 'absolute', bottom: isGroup ? '10px' : '40px', right: '0px' }}
          onClick={this.openKnowMore}
        >
          <img style={{ width: '24px' }} src={info} alt="info" />
        </button>
        {
          isPhone ? (<div className="mt-3" />) : (<div />)
        }
      </div>
    );
  }
}

ProductElement.propTypes = {
  isGroup: PropTypes.bool.isRequired,
  data: PropTypes.shape(PropTypes.object).isRequired,
  onOpenKnowMore: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
  onClickProduct: PropTypes.func.isRequired,
  idProduct: PropTypes.number.isRequired,
};

export default ProductElement;
