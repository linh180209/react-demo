/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component, lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import {
  getAltImage, getCmsCommon, getNameFromCommon, generaCurrency,
} from '../../Redux/Helpers';
import icMore from '../../image/icon/testMore.png';
import auth from '../../Redux/Helpers/auth';
import { isMobile, isTablet } from '../../DetectScreen';
import '../../styles/product-all.scss';

const LazyImage = lazy(() => import('../../components/LazyImage'));

class ProductItemNew extends Component {
  onClickAddToCart = () => {
    if (this.props.onClickAddToCart) {
      this.props.onClickAddToCart(this.props.data);
    }
  }

  onClickGotoProduct = () => {
    if (this.props.onClickGotoProduct) {
      this.props.onClickGotoProduct(this.props.data);
    }
  }

  render() {
    const {
      name, image, item, type, ingredient,
    } = this.props.data;
    const { onClickIngredient } = this.props;
    const cmsCommon = getCmsCommon(this.props.cms);
    const addToCartBt = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
    const viewProductBt = getNameFromCommon(cmsCommon, 'VIEW_PRODUCT');
    return (
      <div className="div-col justify-center items-center">
        {/* <Suspense fallback={(
          <div />
      )}
        >
          <LazyImage
            src={image || ''}
            style={{
              width: isMobile && !isTablet ? '133px' : '200px',
              cursor: 'pointer',
            }}
            alt={getAltImage(image || '')}
            onClick={this.onClickGotoProduct}
          />
        </Suspense> */}
        <div className="image-product-all" style={{ width: isMobile && !isTablet ? '133px' : '200px' }}>
          <img
            src={image || ''}
            style={{
              width: '100%',
              cursor: 'pointer',
            }}
            alt={getAltImage(image || '')}
            onClick={this.onClickGotoProduct}
          />
          <button
            type="button"
            className={type.name === 'Kit' || type.name === 'hand_sanitizer' || type.name === 'home_scents' ? 'hidden' : ''}
            onClick={() => onClickIngredient(ingredient)}
          >
            <img src={icMore} alt="icMore" />
          </button>
        </div>

        <span className="mt-3 tc">
          { name || ''}
        </span>
        <span className={item.price_sale ? '' : 'mb-3'} style={item.price_sale ? { textDecoration: 'line-through' } : {}}>
          {generaCurrency(item ? item.price : '')}
        </span>
        {
          item.price_sale ? (
            <span className="mb-3">
              {generaCurrency(item ? item.price_sale : '')}
            </span>
          ) : (<div />)
        }
        <button
          type="button"
          className="button-product"
          onClick={this.onClickAddToCart}
        >
          {addToCartBt}
        </button>
      </div>
    );
  }
}

ProductItemNew.propTypes = {
  data: PropTypes.object.isRequired,
  onClickAddToCart: PropTypes.func.isRequired,
  onClickGotoProduct: PropTypes.func.isRequired,
  cms: PropTypes.shape().object,
  onClickIngredient: PropTypes.func.isRequired,
};

export default ProductItemNew;
