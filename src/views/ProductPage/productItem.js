import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Slider from 'react-slick';
import { Col, Row } from 'reactstrap';
import ProductElement from './productElement';
import leftArrow from '../../image/left-arrow.svg';
import rightArrow from '../../image/right-arrow.svg';
import auth from '../../Redux/Helpers/auth';

// fix carousel
import '../../../node_modules/slick-carousel/slick/slick-theme.css';
import '../../../node_modules/slick-carousel/slick/slick.css';
import { generaCurrency, generateUrlWeb } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';
// end fix carousel

class ProductItem extends Component {
  state = {
    indexSelection: 0,
  }

  onClickBuyFamily = async () => {
    const { data, kitsFamily, history } = this.props;
    const kit = _.find(kitsFamily, x => x.family === data.family);
    if (kit) {
      history.push(generateUrlWeb(`/product/kit/${kit.id}`));
    }
  }

  onClickPrev = () => {
    const { indexSelection } = this.state;
    const index = indexSelection - 1 < 0 ? this.dataItems.length - 1 : indexSelection - 1;
    this.setState({ indexSelection: index });
  }

  onClickNext = () => {
    const { indexSelection } = this.state;
    const index = indexSelection + 1 >= this.dataItems.length ? 0 : indexSelection + 1;
    this.setState({ indexSelection: index });
  }

  addToCart = (data) => {
    const {
      addProductBasketArray, createBasketGuestArray, basket,
    } = this.props;
    const dataTemps = [];
    for (let i = 0; i < data.length; i += 1) {
      const { items, images } = data[i];
      const imageUrl = _.find(images, x => x.type === 'unisex');
      if (items && items.length !== 0) {
        const itemObject = items[0];
        const item = {
          item: itemObject.id,
          name: itemObject.name,
          price: itemObject.price,
          is_featured: itemObject.is_featured,
          quantity: 1,
          total: parseFloat(itemObject.price) * 1,
          image_urls: imageUrl ? [imageUrl.image] : '',
        };
        const dataTemp = {
          idCart: basket.id,
          item,
        };
        dataTemps.push(dataTemp);
      }
    }
    console.log('dataTemps', dataTemps);
    if (!basket || !basket.id) {
      createBasketGuestArray(dataTemps);
    } else {
      addProductBasketArray(dataTemps);
    }
  }

  addToCartKits = () => {
    const {
      data, addProductBasketArray, createBasketGuestArray, basket,
    } = this.props;
    const { items, images } = data;
    if (items && items.length !== 0) {
      const itemObject = items[0];
      console.log('itemObject data', data);
      const item = {
        item: itemObject.id,
        name: itemObject.name,
        is_featured: itemObject.is_featured,
        price: itemObject.price,
        quantity: 1,
        total: parseFloat(itemObject.price) * 1,
        image_urls: images && images.length > 0 ? [images[0].image] : '',
      };
      const dataTemp = {
        idCart: basket.id,
        item,
      };
      if (!basket || !basket.id) {
        createBasketGuestArray([dataTemp]);
      } else {
        addProductBasketArray([dataTemp]);
      }
    }
  }

  viewProduct = (id) => {
    const { isGroup, history } = this.props;
    if (isGroup) {
      history.push(generateUrlWeb(`/product/kit/${id}`));
    } else {
      history.push(generateUrlWeb(`/product/elixir/${id}`));
    }
  }

  render() {
    // fix carousel
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      // autoplay: true,
      autoplaySpeed: 2000,
      cssEase: 'linear',
      responsive: [
        {
          breakpoint: 414,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          },
        },
      ],
    };
    // end fix carousel
    const {
      isGroup, data, onOpenKnowMore, kitsFamily,
    } = this.props;
    if (!data) {
      return (<div />);
    }
    const { indexSelection } = this.state;
    this.dataItems = isGroup ? _.filter(data.combo, x => x.product.type === 'Scent') : data.body;
    const kFamily = isGroup ? 0 : _.find(kitsFamily, x => x.family === data.family, 0);
    const isPhone = isMobile && !isTablet;
    const directLayout = isPhone ? 'div-col' : 'div-row';
    const widthText = isPhone ? 'w-90' : 'w-50';
    const mobileHtml = (
      <div className="relative">
        <ProductElement
          isGroup={isGroup}
          data={this.dataItems[indexSelection]}
          onOpenKnowMore={onOpenKnowMore}
          addToCart={this.addToCart}
          idProduct={data.id}
          onClickProduct={this.viewProduct}
        />
        <img
          style={{
            position: 'absolute',
            left: '0px',
            top: '50%',
            transform: 'translateY(-50%)',
            width: '24px',
            height: '24px',
            cursor: 'pointer',
          }}
          src={leftArrow}
          alt="leftArrow"
          onClick={this.onClickPrev}
        />
        <img
          style={{
            position: 'absolute',
            right: '0px',
            top: '50%',
            transform: 'translateY(-50%)',
            width: '24px',
            height: '24px',
            cursor: 'pointer',
          }}
          onClick={this.onClickNext}
          src={rightArrow}
          alt="rightArrow"
        />
      </div>
    );
    return (

      <div className="row w-100 justify-center items-center">
        <div className={`div-row justify-between items-center mt-4 mb-4 ${widthText}`}>
          <div style={{
            background: '#658F96',
            width: isPhone ? '50px' : '120px',
            height: '1px',
          }}
          />
          <span style={{
            color: '#658F96',
            fontSize: '1.2rem',
            textAlign: 'center',
          }}
          >
            {isGroup ? data.name.toUpperCase() : data.family.toUpperCase()}
          </span>
          <div style={{
            background: '#658F96',
            width: isPhone ? '50px' : '120px',
            height: '1px',
          }}
          />
        </div>
        {
          isMobile
            ? (
          // mobileHtml

              <Col xs={12} sm={12} md={12} lg={12}>
                <div className={(isGroup) ? 'border-carousel-product-all-mobile-2' : ''}>
                  <Slider {...settings}>
                    {
                      _.map(this.dataItems, d => (
                        <ProductElement
                          isGroup={isGroup}
                          data={d}
                          onOpenKnowMore={onOpenKnowMore}
                          addToCart={this.addToCart}
                          onClickProduct={this.viewProduct}
                          idProduct={data.id}
                        />
                      ))
                    }
                  </Slider>
                </div>
              </Col>

            )
            : (
              <div className={`${directLayout} justify-between items-center w-100`}>
                {
                  _.map(this.dataItems, d => (
                    <ProductElement
                      isGroup={isGroup}
                      data={d}
                      onOpenKnowMore={onOpenKnowMore}
                      addToCart={this.addToCart}
                      onClickProduct={this.viewProduct}
                      idProduct={data.id}
                    />
                  ))
                }
              </div>
            )
        }

        <button
          type="button"
          className={isGroup ? 'hidden' : ''}
          style={{
            border: '1px solid #000000',
            borderRadius: '10px',
            marginTop: '25px',
            marginBottom: '25px',
            background: 'transparent',
          }}
          onClick={this.onClickBuyFamily}
        >
          <div className="div-row ml-4 mr-4 mt-1 mb-1">
            <span>
              {generaCurrency(kFamily ? kFamily.items[0].price : '')}
            </span>
            <span style={{
              marginLeft: '20px',
              marginRight: '20px',
            }}
            >
              |
            </span>
            <span>
              BUY FAMILY
            </span>
          </div>
        </button>
        <div className={isGroup ? 'div-col justify-center items-center mt-5' : 'hidden'}>
          <span style={{
            color: '#000000',
            fontSize: '1.2rem',
            textAlign: 'center',
          }}
          >
            {data.name}
          </span>
          <span className="tc w-70 mt-3 mb-4 tc">
            {data.description}
          </span>
          <span style={{
            color: '#000000',
            fontSize: '1.2rem',
          }}
          >
            {generaCurrency(data.items && data.items.length > 0 ? data.items[0].price : 0)}
          </span>
          <div className="div-row justify-center items-center mt-4 mb-5">
            <button
              type="button"
              className="button-bg__none"
              onClick={() => this.viewProduct(data.id)}
            >
              VIEW PRODUCT
            </button>
            <span style={{
              fontSize: '1.2rem',
              marginLeft: '1rem',
              marginRight: '1rem',
            }}
            >
              |
            </span>
            <button
              type="button"
              className="button-bg__none"
              onClick={this.addToCartKits}
            >
              ADD TO CART
            </button>
          </div>
        </div>
      </div>

    );
  }
}

ProductItem.propTypes = {
  isGroup: PropTypes.bool.isRequired,
  data: PropTypes.shape(PropTypes.object).isRequired,
  onOpenKnowMore: PropTypes.func.isRequired,
  basket: PropTypes.arrayOf(PropTypes.object).isRequired,
  createBasketGuestArray: PropTypes.func.isRequired,
  addProductBasketArray: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  kitsFamily: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default ProductItem;
