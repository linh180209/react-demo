import React, { useState, useEffect } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';
import testImage from '../../image/test/1.png';


function CarouselImage(props) {
  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    pauseOnHover: false,
  };

  return (
    <div className="carouse-image">
      <Slider {...settings}>
        {
          _.map(_.range(4), d => (
            <div className="item-silder">
              <img src={testImage} alt="test" />
            </div>
          ))
        }
      </Slider>
    </div>
  );
}

export default CarouselImage;
