import React, { useEffect } from 'react';
import { MetaTags } from 'react-meta-tags';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import HeaderHomePage from '../../components/HomePage/header';
import { generateHreflang, removeLinkHreflang } from '../../Redux/Helpers';
import { createBasketGuest, addProductBasket, updateProductBasket } from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import updateIngredientsData from '../../Redux/Actions/ingredients';
import { useMergeState } from '../../Utils/customHooks';
import FooterV2 from '../../views2/footer';

function ProductDetail(props) {
  const [dataIngredient, setDataIngredient] = useMergeState({
    isShowIngredient: false,
    ingredientDetail: {},
  });

  const onCloseIngredient = () => {
    setDataIngredient({ isShowIngredient: false });
  };

  useEffect(() => (() => {
    removeLinkHreflang();
  }), []);

  return (
    <div className="product-detail-v2">
      <MetaTags>
        {/* <title>
          {seo ? seo.seoTitle : ''}
        </title>
        <meta name="description" content={seo ? seo.seoDescription : ''} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        <link href="https://maison21g.com/all-products" rel="canonical" /> */}
        {generateHreflang(props.countries, '/all-products')}
        <HeaderHomePage isProductPage />
        {/* { props.showAskRegion && (<div className="div-temp-region" />) } */}

        <IngredientPopUp
          isShow={dataIngredient.isShowIngredient}
          data={dataIngredient.ingredientDetail || {}}
          onCloseIngredient={onCloseIngredient}
          history={props.history}
          className="min-top-height"
        />
        <FooterV2 />
      </MetaTags>
    </div>
  );
}

const mapDispatchToProps = {
  createBasketGuest,
  addProductBasket,
  addCmsRedux,
  updateProductBasket,
  updateIngredientsData,
};

function mapStateToProps(state) {
  return {
    basket: state.basket,
    login: state.login,
    cms: state.cms,
    countries: state.countries,
    ingredients: state.ingredients,
    // showAskRegion: state.showAskRegion,
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductDetail));
