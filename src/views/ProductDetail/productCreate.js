import React, { useState, useEffect } from 'react';
import CreatePerfume from '../Gift/createPerfume';
import CarouselImage from './carouselImage';

function ProductCreate(props) {
  return (
    <div className="product-create">
      <div className="div-left">
        <CarouselImage />
      </div>
      <div className="div-right">
        <CreatePerfume />
      </div>
    </div>
  );
}

export default ProductCreate;
