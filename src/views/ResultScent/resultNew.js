/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/sort-comp */
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  withOrientationChange,
} from 'react-device-detect';
import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import { Fade } from 'react-reveal';
import { withRouter } from 'react-router-dom';
import { Modal, ModalBody } from 'reactstrap';
import smoothscroll from 'smoothscroll-polyfill';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import Adjective from '../../components/Personality/Adjective';
import AdjectiveMobile from '../../components/Personality/AdjectiveMobile';
import {
  CLEAR_OUTCOME_URL, GET_BASKET_URL, GET_PRODUCT_FOR_CART, GET_PROMO_PERFUME_URL, GUEST_CREATE_BASKET_URL, PUT_OUTCOME_URL,
} from '../../config';
import { isMobile, isMobileOnly } from '../../DetectScreen/detectIFrame';
import icClose from '../../image/icon/close-product.svg';
import icEmail from '../../image/icon/ic-email-input.svg';
import {
  addProductBasket, addProductBasketArray, createBasketGuest, createBasketGuestArray, loadAllBasket,
} from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import auth from '../../Redux/Helpers/auth';
import fetchClient, { fetchClientAlwaysUS } from '../../Redux/Helpers/fetch-client';
import {
  addFontCustom, fetchCMSHomepage, generateUrlWeb, getCmsCommon, getNameFromCommon, getProductDualCrayonCombo, getSearchPathName, getSEOFromCms, getTextFromTextBlock, googleAnalitycs, isCheckNull, scrollTop, segmentTrackPersonalityQuizCompleted, setPrerenderReady,
} from '../../Redux/Helpers/index';

import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import '../../styles/result-new.scss';
import '../../styles/style.scss';
import QuizResult from '../../views2/quizResult';
import { getPathNamePage } from '../Quiz/handler';
import ResultScentV2 from '../ResultScentV2';
import ResultScentV3 from '../ResultScentV3';


class ResultNew extends Component {
  constructor(props) {
    super(props);
    this.namePath = getPathNamePage(window.location.pathname);
    this.isQuizInstore = this.namePath === 'krisshop';
    const { infoGift, customeBottle } = this.props.location.state ? this.props.location.state : {};
    console.log('infoGift', infoGift, customeBottle);
    const readyEmail = props.login && props.login.user ? props.login.user.email : props.basket ? props.basket.email : undefined;
    this.isPathQuizV2 = window.location.pathname.includes('/quizv3') || window.location.pathname.includes('/quizv2') || window.location.pathname.includes('/quizv2-diy');
    this.isQuizResultV4 = window.location.pathname.includes('/quiz');
    this.state = {
      data: [],
      email: this.isQuizInstore ? undefined : readyEmail,
      promoPerfume: {},
      isViewMore: false,
      isPage2nd: false,
      isShowIngredient: false,
      ingredientDetail: undefined,
      stepInstore: props.location && props.location.state ? props.location.state.stepInstore || 1 : 1,
      indexDay: 0,
      indexNight: 0,
      infoGift,
      isShowInfo: false,
      isViewExternalProduct: false,
      customeBottle,
    };
    this.maxChar = isMobile && !props.isLandscape ? 140 : 150;
    this.dataProductFinal = {};
  }

  componentDidMount() {
    setPrerenderReady();
    smoothscroll.polyfill();
    scrollTop();
    googleAnalitycs(this.namePath === 'quiz' ? '/quiz/outcome' : this.namePath === 'partnership-quiz' ? '/partnership-quiz/outcome/' : this.namePath === 'lazada' ? '/lazada/outcome/' : this.namePath === 'sephora' ? '/sephora/outcome/' : this.namePath === 'sofitel' ? '/sofitel/outcome/' : this.namePath === 'bmw' ? '/bmw/outcome/' : '/boutique-quiz/outcome/');
    this.fetchDataCMS();
    if (this.props.outComeData) {
      // data from account test result
      this.handleOutcomeData(this.props.outComeData);
    } else {
      this.fetchDataInit();
    }
    this.fetchPromoPerfumeCode();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { basket, login } = nextProps;
    if (prevState.email === undefined && (_.isEmpty(prevState.basket) && basket || _.isEmpty(prevState.login) && login)) {
      const email = login && login.user ? login.user.email : basket ? basket.email : '';
      return {
        email, basket, login, isShowSendEmail: !email,
      };
    }
    return null;
  }

  // componentWillUnmount = () => {
  //   if (window && this.handleScroll) {
  //     window.removeEventListener('scroll', this.handleScroll);
  //   }
  // }

  getLinkBack = () => {
    if (this.namePath === 'quiz') {
      if (window.location.pathname.includes('/quizv2-diy')) {
        return '/quizv2-diy';
      }
      if (window.location.pathname.includes('/quiz-diy')) {
        return '/quiz-diy';
      }
      if (window.location.pathname.includes('/quizv2')) {
        return '/quizv2';
      }
      if (window.location.pathname.includes('/quizv3')) {
        return '/quizv3';
      }
      if (window.location.pathname.includes('/quizv4')) {
        return '/quizv4';
      }
      return '/quiz';
    } if (this.namePath === 'krisshop') {
      return '/krisshop';
    } if (this.namePath === 'boutique-quiz') {
      if (window.location.pathname.includes('/boutique-quiz-v2')) {
        return '/boutique-quiz-v2';
      }
      return '/boutique-quiz';
    } if (this.namePath === 'partnership-quiz') {
      return `/partnership-quiz/${this.slug}`;
    } if (this.namePath === 'sephora') {
      return '/sephora';
    } if (this.namePath === 'lazada') {
      return '/lazada';
    } if (this.namePath === 'sofitel') {
      return '/sofitel';
    } if (this.namePath === 'bmw') {
      return '/bmw';
    }
    return '/';
  }

  isValidateValue = (value) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(value).toLowerCase());
  }

  onChangeEmail = (e) => {
    const { value } = e.target;
    this.setState({ email: value });
  }

  assignEmailToCart = () => {
    const { email } = this.state;
    const { basket } = this.props;
    if (!basket.id) {
      const options = {
        url: GUEST_CREATE_BASKET_URL,
        method: 'POST',
        body: {
          subtotal: 0,
          total: 0,
          email,
        },
      };
      fetchClient(options).then((result) => {
        if (result && !result.isError) {
          this.props.loadAllBasket(result);
        }
      });
      return;
    }
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basket.id),
      method: 'PUT',
      body: {
        email,
      },
    };
    fetchClient(option);
  }

  getProductCombo = async (combo) => {
    const option = {
      url: combo[0].product.id === combo[1].product.id ? `${GET_PRODUCT_FOR_CART}?combo=${combo[0].product.id}&type=scent` : `${GET_PRODUCT_FOR_CART}?combo=${combo[0].product.id}${`,${combo[1].product.id}`}&type=perfume`,
      method: 'GET',
    };
    return fetchClient(option);
  }

  getProductDualCrayonCombo = async (combo) => {
    const option = {
      url: combo[0].product.id === combo[1].product.id ? `${GET_PRODUCT_FOR_CART}?combo=${combo[0].product.id}&type=dual_crayons` : `${GET_PRODUCT_FOR_CART}?combo=${combo[0].product.id}${`,${combo[1].product.id}`}&type=dual_crayons`,
      method: 'GET',
    };
    return fetchClient(option);
  }

  getProductFinal = async () => {
    if (!_.isEmpty(this.dataProductFinal)) {
      const combo = _.filter(this.dataProductFinal.data.combo, x => x.product.type === 'Scent');
      const product = await this.getProductCombo(combo);
      return product;
    }
    return undefined;
  }

  onClickSend = async (email) => {
    if (!this.isValidateValue(email)) {
      toastrError('Please enter the email!');
      return;
    }
    if (isCheckNull(this.outComeId)) {
      return;
    }
    this.assignEmailToCart();
    let productsFinal;
    if (this.namePath === 'boutique-quiz') {
      productsFinal = await this.getProductFinal();
    }
    const option = {
      url: PUT_OUTCOME_URL.replace('{id}', this.outComeId),
      method: 'PUT',
      body: {
        email,
        send_email: true,
        products_final: productsFinal ? [{ product: productsFinal.id }] : undefined,
      },
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        auth.setEmail(email);
        toastrSuccess('Sending Email is successful!');
        // this.gotoAnchor('id-point-2');
        this.props.loadingPage(false);
        return;
      }
      throw new Error('Sending Email is failed!');
    }).catch((err) => {
      toastrError(err.message);
      this.props.loadingPage(false);
    });
  }

  onClickImageV4 = async (datas, dataCustom, isDualCrayons) => {
    this.props.loadingPage(true);
    const productDay = _.filter(datas.combo, x => x.product.type === 'Scent') || [];
    const productFinal = isDualCrayons ? await this.getProductDualCrayonCombo(productDay) : await this.getProductCombo(productDay);
    if (dataCustom && !isDualCrayons) {
      const { pathname, search } = getSearchPathName(generateUrlWeb(isDualCrayons ? `/product/dual_crayons/${productFinal.id}` : `/product/Perfume/${productFinal.id}`));
      this.props.history.push({
        pathname,
        search,
        state: {
          custome: dataCustom ? {
            name: dataCustom?.nameBottle,
            isCustome: false,
            font: dataCustom?.font,
            image: dataCustom?.currentImg,
            color: dataCustom?.color,
          } : undefined,
        },
      });
    } else {
      this.props.history.push(
        generateUrlWeb(isDualCrayons ? `/product/dual_crayons/${productFinal.id}` : `/product/Perfume/${productFinal.id}`),
      );
    }
    this.props.loadingPage(false);
  };

  onClickImage = async (isClickNightPerfume, isDualCrayons, dataCustom) => {
    console.log('onClickImage ddd', isClickNightPerfume, isDualCrayons);
    if (this.isQuizInstore) {
      return;
    }
    const { indexDay, indexNight } = this.state;
    const { products_day: prodDays, products_night: prodNights } = this.state.data;
    const productDay = _.filter(prodDays[indexDay].combo, x => x.product.type === 'Scent') || [];
    const productNight = _.filter(prodNights[indexNight].combo, x => x.product.type === 'Scent') || [];
    const productFinal = isDualCrayons ? await this.getProductDualCrayonCombo(isClickNightPerfume ? productNight : productDay) : await this.getProductCombo(isClickNightPerfume ? productNight : productDay);

    if (dataCustom && !isDualCrayons) {
      const { pathname, search } = getSearchPathName(generateUrlWeb(isDualCrayons ? `/product/dual_crayons/${productFinal.id}` : `/product/Perfume/${productFinal.id}`));
      this.props.history.push({
        pathname,
        search,
        state: {
          custome: dataCustom ? {
            name: dataCustom?.nameBottle,
            isCustome: false,
            font: dataCustom?.font,
            image: dataCustom?.currentImg,
            color: dataCustom?.color,
          } : undefined,
        },
      });
    } else {
      this.props.history.push(
        generateUrlWeb(isDualCrayons ? `/product/dual_crayons/${productFinal.id}` : `/product/Perfume/${productFinal.id}`),
      );
    }
  };

  generaDataAddCart = async (item, name, customeBottle, datas) => {
    console.log('datas=====', datas);
    const isDualCrayons = item.is_sample;
    const { isViewExternalProduct } = this.state;
    const { external_product: externalProduct } = this.state.data;
    const { basket } = this.props;
    let itemSelect;
    let customeBT;
    if (isDualCrayons) {
      const scents = _.filter(datas.combo, x => x.product.type === 'Scent');
      const productFinal = await getProductDualCrayonCombo(scents[0]?.product?.id, scents[1]?.product?.id);
      itemSelect = productFinal?.items[0];
      _.assign(itemSelect, { name: productFinal?.name });
      customeBT = {};
    } else {
      itemSelect = item;
      customeBT = customeBottle;
    }
    const {
      currentImg, nameBottle, isBlack, imagePremade, color, font,
    } = customeBT;

    const bottleImage = currentImg
      ? await fetch(currentImg).then(r => r.blob())
      : undefined;
    const data = {
      item: itemSelect.id,
      price: itemSelect.price,
      is_featured: item.is_featured,
      is_black: isBlack,
      is_display_name: (!bottleImage && !!nameBottle) || !!nameBottle || isViewExternalProduct,
      is_customized: !!bottleImage && !!imagePremade,
      imagePremade,
      file: bottleImage
        ? new File([bottleImage], 'product.png')
        : undefined,
      quantity: 1,
      name: nameBottle || (isViewExternalProduct ? undefined : itemSelect.name),
      color,
      font,
      external_product: isViewExternalProduct ? externalProduct?.id : undefined,
    };
    return {
      idCart: basket.id,
      item: data,
    };
  }

  addToCartBoth = async (prodDay, prodNight, customeBottleDay, customeBottleNight) => {
    const { basket, cms } = this.props;
    const cmsCommon = getCmsCommon(cms);
    const dayPerfumebt = getNameFromCommon(cmsCommon, 'Day_perfume');
    const nightPerfumebt = getNameFromCommon(cmsCommon, 'Night_perfume');
    const { items: items1 } = prodDay;
    const item1 = _.find(items1, x => !x.is_sample);
    const dataTemp1 = await this.generaDataAddCart(item1, dayPerfumebt, customeBottleDay);

    const { items: items2 } = prodNight;
    const item2 = _.find(items2, x => !x.is_sample);
    const dataTemp2 = await this.generaDataAddCart(item2, nightPerfumebt, customeBottleNight);

    if (!basket.id) {
      this.props.createBasketGuestArray([dataTemp1, dataTemp2]);
    } else {
      this.props.addProductBasketArray([dataTemp1, dataTemp2]);
    }
  }

  onClickAddToCart = async (item, name, customeBottle, datas) => {
    const { basket } = this.props;
    console.log('onClickAddToCart', datas);
    const dataTemp = await this.generaDataAddCart(item, name, customeBottle, datas);

    console.log('dataTemp : ', dataTemp);
    // segmentTrackPersonalityQuizCompletedProductAdded()
    if (!basket.id) {
      this.props.createBasketGuest(dataTemp);
    } else {
      this.props.addProductBasket(dataTemp);
    }
  }

  onChangeExternalProduct = () => {
    const { isViewExternalProduct } = this.state;
    this.setState({ isViewExternalProduct: !isViewExternalProduct });
  };

  onClickCheckOutGift = (item, name, customeBottle) => {
    console.log('item, name, customeBottle', item, name, customeBottle);
    const { infoGift } = this.state;
    const { pathname, search } = getSearchPathName(generateUrlWeb(`/checkout-gift/?codes=${infoGift.codes}`));
    this.props.history.push({
      pathname,
      search,
      state: {
        codes: infoGift.codes,
        item,
        customeBottle,
      },
    });
  }

  retakeQuiz = () => {
    const { infoGift } = this.state;
    const url = this.getLinkBack();
    const { pathname, search } = getSearchPathName(generateUrlWeb(url.includes('/quiz') ? `${url}${infoGift && infoGift.codes ? `/?codes=${infoGift.codes}` : ''}` : url));
    const objectLinkBack = {
      pathname,
      search,
      state: { isRetake: true, infoGift },
    };
    this.props.history.push(objectLinkBack);
  }

  handleOutcomeData = (result, englishResult) => {
    const dataSet = {};
    const englishDataSet = {};
    if (!result.mixes || result.mixes.length === 0) {
      _.assign(result, { mixes: [1] });
      _.assign(englishResult, { mixes: [1] });
    }
    if (result.mixes[0] === 1) {
      _.assign(dataSet, { indexDay: 0 });
      _.assign(englishDataSet, { indexDay: 0 });
    } else if (result.mixes[0] === 2) {
      _.assign(dataSet, { indexDay: 1 });
      _.assign(englishDataSet, { indexDay: 1 });
    } else if (result.mixes[0] === 3) {
      _.assign(dataSet, { indexNight: 0 });
      _.assign(englishDataSet, { indexNight: 0 });
    } else if (result.mixes[0] === 4) {
      _.assign(dataSet, { indexNight: 1 });
      _.assign(englishDataSet, { indexNight: 1 });
    }
    _.assign(dataSet, { data: result });
    _.assign(englishDataSet, { data: englishResult });

    segmentTrackPersonalityQuizCompleted(this.state?.email, englishDataSet?.data);
    // TODO: segmentPersonalityQuizCompletedIdentify(auth.getId(), {email: this.state?.email} )
    this.setState(dataSet);
  }

  fetchDataCMS = () => {
    const cmsProduct = _.find(this.props.cms, x => x.title === 'Personality Outcome');
    if (!cmsProduct) {
      fetchCMSHomepage('personality-outcome').then((cmsData) => {
        this.props.addCmsRedux(cmsData);
      }).catch((err) => {
        console.log('err', err);
      });
    }
  }

  fetchDataInit = async () => {
    const { login, match } = this.props;
    this.outComeId = match && match.params ? match.params.idOutCome : undefined;
    this.slug = match && match.params ? match.params.slug : undefined;
    // if (login && login.user && login.user.outcome && this.namePath === 'quiz') {
    //   this.outComeId = login.user.outcome;
    // }
    if (isCheckNull(this.outComeId)) {
      this.props.loadingPage(false);
      this.retakeQuiz();
    }
    const option = {
      url: PUT_OUTCOME_URL.replace('{id}', this.outComeId),
      method: 'GET',
    };
    this.props.loadingPage(true);
    const promises = [fetchClient(option), fetchClientAlwaysUS(option)];
    Promise.all(promises).then((results) => {
      this.props.loadingPage(false);
      if (results && !results[0].isError) {
        const { products_day: prodDays } = results[0];
        if (_.isEmpty(prodDays)) {
          this.retakeQuiz();
          return;
        }
        this.handleOutcomeData(results[0], results[1]);
        return;
      }
      if (results[0] && results[0].status === 404) {
        auth.setOutComeId(undefined);
        if (login && login.user && login.user.outcome) {
          login.user.outcome = undefined;
        }
      }
      throw new Error();
    }).catch((e) => {
      console.log(e);
      this.retakeQuiz();
      this.props.loadingPage(false);
    });
  }

  fetchPromoPerfumeCode = () => {
    const option = {
      url: GET_PROMO_PERFUME_URL,
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.setState({ promoPerfume: result });
        return;
      }
      throw new Error();
    }).catch((err) => {
      console.error('err', err);
    });
  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  getImageFromPerfume = (data) => {
    const { combo } = data || {};
    const products = _.filter(combo, x => x.product.type === 'Scent');
    const image1 = products && products.length > 0 ? _.find(products[0].product.images, x => x.type === 'unisex') : undefined;
    const image2 = products && products.length > 1 ? _.find(products[1].product.images, x => x.type === 'unisex') : undefined;
    return { image1, image2 };
  }

  gotoAnchor = (id) => {
    const ele = document.getElementById(id);
    if (ele) {
      ele.scrollIntoView({ behavior: 'smooth' });
    }
  }

  handleScroll = () => {
    const { isPage2nd } = this.state;
    if (window.scrollY < 640 && isPage2nd) {
      this.setState({ isPage2nd: false });
    } else if (window.scrollY > 640 && !isPage2nd) {
      this.setState({ isPage2nd: true });
    }
  }

  callUpdateScentAll = () => {
    const { data } = this.state;
    this.setState({ data: _.cloneDeep(data) });
  }

  dataUpdateProduct = (data) => {
    this.dataProductFinal = data;
  }

  onClickRetakeQuiz = () => {
    const { login } = this.props;
    if (login && login.user && login.user.outcome) {
      const options = {
        url: CLEAR_OUTCOME_URL.replace('{id}', this.outComeId),
        method: 'Delete',
      };
      fetchClient(options);
    }
    this.retakeQuiz();
  }

  onClickMyComnbinian = () => {
    if (this.namePath === 'quiz') {
      return;
    }
    const { pathname, search } = getSearchPathName(generateUrlWeb(`/boutique-quiz/instore/${this.outComeId}`));
    this.props.history.push({
      pathname,
      search,
      state: { email: this.state.email },
    });
  }

  goToProfile = () => {
    this.props.history.push(generateUrlWeb('/account'));
  }

  gotoCustomMix = (data) => {
    if (this.namePath === 'lazada') {
      if (data?.data?.items?.length > 0) {
        window.location.href = data?.data?.items[0]?.lazada_link;
      }
    } else {
      const combo = _.filter(data.data.combo, x => x.product.type === 'Scent');
      const perfumes = _.map(combo, (x) => {
        const { product } = x;
        const image = _.find(product.images, d => d.type === 'unisex');
        _.assign(product, { image: image ? image.image : '' });
        _.assign(product, {
          product: {
            profile: product.profile,
          },
        });
        return product;
      });
      const { pathname, search } = getSearchPathName(window.location.pathname.replace('outcome', 'create-perfume'));
      this.props.history.push({
        pathname,
        search,
        state: { email: this.state.email, perfumes },
      });
    }
  }

  onSwitchIndex = (isNight, value) => {
    const { indexDay, indexNight, data } = this.state;
    // onClickChillRelax =  //0 - 0
    // onClickWorkSocial = //0 - 1
    // onClickSeducation = // 1- 0
    // onClickSpecialOccasion = // 1 - 1
    // }
    if (isNight) { // 0 / 1????? what is this indexing brother???
      this.setState({ indexNight: value !== undefined ? value : (indexNight === 0 ? 1 : 0) });
    } else {
      this.setState({ indexDay: value !== undefined ? value : (indexDay === 0 ? 1 : 0) });
    }
  }

  render() {
    const {
      data, promoPerfume, isViewMore,
      isShowIngredient, ingredientDetail,
      indexNight, indexDay, infoGift, isShowInfo,
    } = this.state;
    const { cms, isLandscape } = this.props;
    const cmsPersonal = _.find(cms, x => x.title === 'Personality Outcome') || {};
    const cmsCommon = getCmsCommon(cms);
    const dayPerfumebt = getNameFromCommon(cmsCommon, 'Day_perfume');
    const nightPerfumebt = getNameFromCommon(cmsCommon, 'Night_perfume');
    const sendBt = getNameFromCommon(cmsCommon, 'SEND');
    const enterEmailBt = getNameFromCommon(cmsCommon, 'Enter_your_email');

    const buttonBlocks = _.filter(cmsPersonal ? cmsPersonal.body : [], x => x.type === 'button_block');
    const faqBlock = _.find(cmsPersonal ? cmsPersonal.body : [], x => x.type === 'faq_block');
    const benefitsBlock = _.find(cmsPersonal ? cmsPersonal.body : [], x => x.type === 'benefits_block');
    const feedbackBlock = _.find(cmsPersonal ? cmsPersonal.body : [], x => x.type === 'feedbacks_block');
    const blogsBlock = _.find(cmsPersonal ? cmsPersonal.body : [], x => x.type === 'blogs_block');
    const ctaBlock = _.find(cmsPersonal ? cmsPersonal.body : [], x => x.type === 'cta_block');
    const imageBlock = _.find(cmsPersonal ? cmsPersonal.body : [], x => x.type === 'image_block' && x.value.caption === 'image-block-email');
    const iconsBlock = _.find(cmsPersonal ? cmsPersonal.body : [], x => x.type === 'icons_block' && x.value?.image_background?.caption === 'icons-v1');
    const seo = getSEOFromCms(cmsPersonal);
    const { header_text: headerText, body } = cmsPersonal;
    const textBlock = _.filter(body || [], x => x.type === 'text_block') || [];

    const {
      products_day: prodDays, products_night: prodNights, personality_choice: personalityChoice, partnership, occasion,
      scents_disliked: scentsDisliked, scents_liked: scentsLiked, mixes, external_product: externalProduct, external_products: externalProducts,
      meta,
    } = data;
    if (!prodDays && !prodNights) {
      return <div />;
    }

    const isLimitMixes = meta?.limit_mixes;
    const textDescription = personalityChoice ? isViewMore && personalityChoice.personality.description.length > this.maxChar ? personalityChoice.personality.description.substring(0, this.maxChar) : personalityChoice.personality.description : '';
    const dataPersionality = { image: personalityChoice && personalityChoice.personality ? personalityChoice.personality.image : '', color: `#${personalityChoice.personality.color}` };

    const isHasEmail = (this.namePath === 'quiz' || this.namePath === 'boutique-quiz' || ['sofitel', 'sephora', 'bmw', 'lazada'].includes(this.namePath) || this.namePath === 'krisshop') && this.props.login && this.props.login.user ? this.props.login.user.email : this.props.basket ? this.props.basket.email : undefined;
    const dataMixes = mixes && mixes.length > 0 ? (((mixes[0] === 2 && prodDays.length < 2) || (mixes[0] === 4 && prodNights.length < 2)) ? 1 : mixes[0]) : 1;

    if (this.isQuizResultV4) {
      return (
        <QuizResult
          seo={seo}
          ctaBlock={ctaBlock}
          buttonBlocks={buttonBlocks}
          cmsCommon={cmsCommon}
          dataPersionality={dataPersionality}
          personalityChoice={personalityChoice}
          customeBottle={this.state.customeBottle}
          infoGift={infoGift}
          mixes={mixes[0]}
          dataDay={prodDays[0]}
          dataNight={prodNights[0]}
          idOutCome={this.outComeId}
          email={this.state.email}
          onClickSend={this.onClickSend}
          onClickAddToCart={this.onClickAddToCart}
          onClickImageV4={this.onClickImageV4}
          imageBlock={imageBlock}
          onClickRetake={this.onClickRetakeQuiz}
          namePath={this.namePath}
        />
      );
    }

    return (
      <div>
        {
          !this.props.isAccountOutcome && (
            <MetaTags>
              <title>
                {seo ? seo.seoTitle : ''}
              </title>
              <meta
                name="description"
                content={seo ? seo.seoDescription : ''}
              />
              <meta
                name="keywords"
                content={seo ? seo.seoKeywords : ''}
              />
              <meta name="robots" content="index, follow" />
              <meta name="revisit-after" content="3 month" />
            </MetaTags>
          )
        }

        {
          !['krisshop', 'sofitel'].includes(this.namePath) && !this.props.isAccountOutcome && (<HeaderHomePageV3 />)
        }

        <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
          className={`min-top-height ${window.location.pathname.includes('outcomev2') && this.namePath === 'quiz' && !this.props.isAccountOutcome ? 'boder-none' : ''}`}
          isKrisshop={this.namePath === 'krisshop'}
        />
        {
          this.isPathQuizV2 && this.namePath === 'quiz' && !this.props.isAccountOutcome ? (
            <ResultScentV3
              dataPersionality={dataPersionality}
              personalityChoice={personalityChoice}
              textDescription={textDescription}
              leftTitle={getTextFromTextBlock(textBlock, 16)}
              rightTitle={getTextFromTextBlock(textBlock, 17)}
              adjectives={personalityChoice?.personality?.adjectives || []}
              scents={personalityChoice?.personality?.scents || []}
              onClickRetake={this.onClickRetakeQuiz}
              buttonBlocks={buttonBlocks}
              cmsCommon={cmsCommon}
              dataDay={prodDays}
              dataNight={prodNights}
              infoGift={infoGift}
              onClickIngredient={this.onClickIngredient}
              textBlock={textBlock}
              onClickAddToCart={this.onClickAddToCart}
              externalProduct={externalProduct}
              externalProducts={externalProducts}
              mixes={dataMixes}
              originalMixes={mixes}
              faqBlock={faqBlock}
              benefitsBlock={benefitsBlock}
              feedbackBlock={feedbackBlock}
              blogsBlock={blogsBlock}
              iconsBlock={iconsBlock}
              customeBottle={this.state.customeBottle}
            />
          ) : (
            <React.Fragment>
              <ResultScentV2
                onChangeExternalProduct={this.onChangeExternalProduct}
                externalProduct={['partnership-quiz', 'sephora', 'sofitel', 'bmw', 'lazada'].includes(this.namePath) ? undefined : externalProduct}
                isViewExternalProduct={this.state.isViewExternalProduct}
                infoGift={infoGift}
                customeBottle={this.state.customeBottle}
                namePath={this.namePath}
                cmsCommon={cmsCommon}
                buttonBlocks={buttonBlocks}
                onClickAddToCart={this.onClickAddToCart}
                addToCartBoth={this.addToCartBoth}
                onClickIngredient={this.onClickIngredient}
                promoPerfume={promoPerfume}
                personalityChoice={personalityChoice}
                dataPersionality={dataPersionality}
                headerText={headerText}
                onClickImage={this.onClickImage}
                mixes={isLimitMixes ? mixes[0] : dataMixes}
                isLimitMixes={isLimitMixes}
                dataDay={{
                  data: isLimitMixes ? prodDays[0] : prodDays[indexDay],
                  subTitle: dayPerfumebt,
                  title: getTextFromTextBlock(textBlock, 12),
                }}
                dataNight={{
                  data: isLimitMixes ? prodNights[0] : prodNights[indexNight],
                  subTitle: nightPerfumebt,
                  title: getTextFromTextBlock(textBlock, 13),
                }}
                onBack={() => this.setState({ stepInstore: 1 })}
                onClickRetake={this.onClickRetakeQuiz}
                onClickMyComnbinian={this.onClickMyComnbinian}
                textBlock={textBlock}
                onSwitchIndex={this.onSwitchIndex}
                isDisableSwitchScentDay={prodDays.length !== 2}
                isDisableSwitchScentNight={prodNights.length !== 2}
                onClickCheckOutGift={this.onClickCheckOutGift}
                showMoreInfo={() => this.setState({ isShowInfo: true })}
                gotoCustomMix={this.gotoCustomMix}
                occasion={occasion}
                idOutCome={this.outComeId}
                scentsDisliked={scentsDisliked}
                scentsLiked={scentsLiked}
                isLandscape={isLandscape}
                callUpdateScentAll={this.callUpdateScentAll}
                dataUpdateProduct={this.dataUpdateProduct}
                isAccountOutcome={this.props.isAccountOutcome}
                basket={this.props.basket}
              />
              {
                isShowInfo ? (
                  <Modal className={`modal-full-screen ${addFontCustom()}`} isOpen>
                    <ModalBody>
                      <div className="div-full-popup">
                        <div className="div-card-popup-result-quiz">
                          <button
                            className="bt-close"
                            type="button"
                            onClick={() => this.setState({ isShowInfo: false })}
                          >
                            <img src={icClose} alt="close" />
                          </button>
                          <Fade delay={1000}>
                            <div className={`${this.namePath === 'partnership-quiz' ? 'partner-ship' : ''} div-result-new div-col`}>
                              <div className="div-send-email div-col">
                                <Fade delay={1000} top>
                                  <div className={isMobileOnly ? 'header div-row mb-2 justify-center items-center' : 'header div-row my-3 justify-center items-center'}>
                                    <img src={dataPersionality.image} alt="authentic" />
                                    <h2 className="tc" style={{ color: dataPersionality.color }}>
                                      {personalityChoice ? personalityChoice.personality.title : ''}
                                    </h2>
                                  </div>
                                </Fade>
                                <span className={isMobile && !isLandscape ? 'tc description' : 'hidden'}>
                                  <span>
                                    {textDescription}
                                  </span>
                                  <br />
                                </span>
                                {isMobileOnly
                                  ? (
                                    <AdjectiveMobile
                                      leftTitle={getTextFromTextBlock(textBlock, 16)}
                                      rightTitle={getTextFromTextBlock(textBlock, 17)}
                                      adjectives={personalityChoice ? personalityChoice.personality.adjectives : []}
                                      scents={personalityChoice ? personalityChoice.personality.scents : []}
                                    />
                                  )
                                  : (
                                    <Adjective
                                      leftTitle={getTextFromTextBlock(textBlock, 16)}
                                      rightTitle={getTextFromTextBlock(textBlock, 17)}
                                      adjectives={personalityChoice ? personalityChoice.personality.adjectives : []}
                                      scents={personalityChoice ? personalityChoice.personality.scents : []}
                                    />
                                  )}
                                <Fade delay={1000}>
                                  <span className={isMobile && !isLandscape ? 'hidden' : 'tc w-80 description'}>
                                    <span>
                                      {textDescription}
                                    </span>
                                    <br />
                                  </span>
                                </Fade>
                                {!isHasEmail && !['sofitel', 'sephora', 'bmw', 'lazada'].includes(this.namePath) && this.namePath !== 'krisshop' && (['boutique-quiz', 'partnership-quiz', 'sephora', 'sofitel', 'bmw'].includes(this.namePath) || !this.props.login.user) && (
                                  <span className="tc text-title-input">
                                    {
                                    getTextFromTextBlock(textBlock, 14)
                                  }
                                  </span>
                                )}
                                {
                                !['sofitel', 'sephora', 'krisshop', 'bmw', 'lazada'].includes(this.namePath) && !isHasEmail && (
                                  <div className={isMobileOnly ? 'div-input div-col' : 'div-input div-row'}>
                                    {(['boutique-quiz', 'partnership-quiz'].includes(this.namePath) || !this.props.login.user) && (
                                    <div className="div-email div-row">
                                      <img src={icEmail} alt="email" />
                                      <input
                                        type="email"
                                        name="email"
                                        value={this.state.email}
                                        placeholder={enterEmailBt}
                                        onChange={this.onChangeEmail}
                                      />
                                    </div>
                                    )}
                                    <button
                                      type="button"
                                      onClick={() => this.onClickSend(this.state.email)}
                                      className={this.props.login.user && this.namePath !== 'boutique-quiz' && !['sofitel', 'sephora', 'bmw', 'lazada'].includes(this.namePath) && this.namePath !== 'krisshop' ? 'hidden' : ''}
                                    >
                                      {sendBt}
                                    </button>
                                  </div>
                                )
                              }
                              </div>
                            </div>
                          </Fade>
                        </div>
                      </div>
                    </ModalBody>
                  </Modal>
                ) : (null)
                  }
            </React.Fragment>
          )
        }
      </div>
    );
  }
}

ResultNew.defaultProps = {
  outComeData: undefined,
  isAccountOutcome: false,
};

ResultNew.propTypes = {
  createBasketGuest: PropTypes.func.isRequired,
  createBasketGuestArray: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  addProductBasketArray: PropTypes.func.isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  loadAllBasket: PropTypes.func.isRequired,
  login: PropTypes.shape().isRequired,
  basket: PropTypes.shape().isRequired,
  cms: PropTypes.shape().isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      idOutCome: PropTypes.string,
    }),
  }).isRequired,
  isLandscape: PropTypes.bool.isRequired,
  outComeData: PropTypes.shape(),
  isAccountOutcome: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    basket: state.basket,
    cms: state.cms,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  createBasketGuest,
  addProductBasket,
  addCmsRedux,
  createBasketGuestArray,
  addProductBasketArray,
  loadingPage,
  loadAllBasket,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withOrientationChange(ResultNew)));
