import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Col, Row } from 'reactstrap';
import smoothscroll from 'smoothscroll-polyfill';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { isIOS } from 'react-device-detect';
import BackGroundVideo from '../../components/backgroundVideo';
import CardKnowMore from '../../components/CardKnowMore';
import Header from '../../components/HomePage/header';
import ScentItem2ImgNew from '../../components/ScentItem/scentItem2ImgNew';
// import ScentItem2Img from '../../components/ScentItem/scentItem2Img';
import { GET_COMBO_URL } from '../../config';
import { addProductBasket, createBasketGuest } from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import fetchClient from '../../Redux/Helpers/fetch-client';
import {
  getSEOFromCms, putUpdateOutCome, scrollTop, getCmsCommon, getNameFromCommon, fetchCMSHomepage, googleAnalitycs, generateUrlWeb, gotoShopHome,
} from '../../Redux/Helpers/index';
import { toastrError } from '../../Redux/Helpers/notification';
import { isBrowser, isMobile, isTablet } from '../../DetectScreen';
import FooterV2 from '../../views2/footer';

class ResultScent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      urlBackground: '',
      urlImage: '',
      currentProducts: [],
      datas: [],
      isShowKnowMore: false,
      description: '',
      dataPopup: {
        title: '',
        content: '',
      },
      likeProducts: [],
      step: 1,
    };
    this.refCarouselText = React.createRef();
  }

    componentDidMount = () => {
      scrollTop();
      smoothscroll.polyfill();
      googleAnalitycs('/quiz/results');

      this.fetchDataInit();

      const { outcome, questions } = this.props;
      const persionality = _.find(questions, x => x.type === 'PERSONALITY');
      const urlBackground = persionality
        ? persionality.video_urls[0]
        : undefined;
      const urlImage = persionality ? persionality.image_urls[0] : undefined;
      this.setState({ urlBackground, urlImage });

      if (!_.isEmpty(outcome)) {
        let description = '';
        let typePersonal = '';
        let arrayDescription = [];
        const { personalityChoise, changeMood, productsPersonal } = outcome;
        if (personalityChoise && personalityChoise.length > 0) {
          const personalityItem = personalityChoise[0];
          description = personalityItem.description;
          typePersonal = personalityItem.choice;
          arrayDescription = personalityItem.personality.description.split(
            '\r\n',
          );
          _.remove(arrayDescription, x => x === '');
        }
        _.forEach(changeMood, (d) => {
          d.title = d.name;
          d.shortTitle = d.shortTitle;
          d.images = d.images;
        });
        // console.log('changeMood', changeMood);
        const pending = [];
        _.forEach(changeMood, (e) => {
          const { product } = e;
          if (product && product.length > 1) {
            pending.push(this.fetchCombo(product[0].id, product[1].id));
            e.idProduct1 = product[0].id;
            e.idProduct2 = product[1].id;
          }
        });
        Promise.all(pending).then((results) => {
          if (results && results.length > 0) {
            _.forEach(results, (d, index) => {
              changeMood[index].perfume = d;
            });
            this.setState({
              description,
              typePersonal,
              arrayDescription,
              datas: changeMood,
              currentProducts: changeMood[0],
              productsPersonal,
              combos: results,
            });
          }
        });
      } else {
        // this.props.history.push(generateUrlWeb('/'));
        gotoShopHome();
      }
    };

    onChangeText = (data, step) => {
      this.setState({ currentProducts: data, step });
    };

    onCloseKnowMore = () => {
      this.setState({ isShowKnowMore: false });
    };

    onOpenKnowMore = (title, content, imageKnowMore) => {
      this.setState({
        isShowKnowMore: true,
        dataPopup: { title, content, imageKnowMore },
      });
    };

    onClickLeftButton = (d) => {
      this.onClickImage(d);
    };

    onClickRightButton = (d) => {
      const { perfume, shortTitle } = d;
      this.onClickAddtoCart(perfume, shortTitle);
    };

    onClickImage = (d) => {
      const { datas } = this.state;
      // console.log('datas', datas);
      const { shortTitle } = d;
      const itemChoose = _.find(datas, x => x.shortTitle === shortTitle);
      const otherItems = _.filter(datas, x => x.shortTitle !== shortTitle);
      const queryProducts = [];
      if (itemChoose) {
        queryProducts.push({
          name: itemChoose.shortTitle,
          ids: [itemChoose.idProduct1, itemChoose.idProduct2],
          images: itemChoose.images,
        });
      }
      _.forEach(otherItems, (item) => {
        const { idProduct1, idProduct2 } = item;
        queryProducts.push({
          name: item.shortTitle,
          ids: [idProduct1, idProduct2],
          images: item.images,
        });
      });
      // console.log('queryProducts', queryProducts);
      const queryString = encodeURIComponent(JSON.stringify(queryProducts));
      this.props.history.push(
        generateUrlWeb(`/products-questionnaire?products=${queryString}`),
      );
      // const { perfume } = d;
      // const { combo } = perfume;
      // const itemScents = _.filter(combo, x => x.product.type === 'Scent');
      // if (itemScents.length > 1) {
      //   this.props.history.push(`/products/${itemScents[0].product.id}/${itemScents[1].product.id}`);
      // }
    };

    onClickInfo = (d) => {
      const { title, content, imageKnowMore } = d;
      this.onOpenKnowMore(title, content, imageKnowMore);
    };

    onClickFavorite = (id) => {
      const { likeProducts } = this.state;
      const likes = _.assign([], likeProducts);
      const idExist = _.find(likes, x => x === id);
      if (idExist) {
        _.remove(likes, x => x === id);
      } else {
        likes.push(id);
      }
      this.updateOutCome(likes);
      // console.log('likeProducts', likes);
      this.setState({ likeProducts: likes });
    };

    onClickAddtoCart = async (perfume, nameCustome) => {
      // console.log('onClickAddtoCart', this.nameProduct);
      const { basket } = this.props;
      const { items, combo } = perfume;
      const itemCombo = items && items.length > 0 ? items[0] : undefined;
      const itemBottle = _.find(combo, x => x.product.type === 'Bottle');
      const imageBottle = itemBottle
        ? _.find(itemBottle.product.images, x => x.type === 'unisex')
        : undefined;
      if (itemCombo) {
        const { price, id, name } = itemCombo;
        const data = {
          item: id,
          is_featured: itemCombo.is_featured,
          name: nameCustome || name,
          price,
          quantity: 1,
          total: parseFloat(price) * 1,
          image_urls: imageBottle ? [imageBottle.image] : undefined,
          combo,
        };
        const dataTemp = {
          idCart: basket.id,
          item: data,
        };
        if (!basket.id) {
          this.props.createBasketGuest(dataTemp);
        } else {
          this.props.addProductBasket(dataTemp);
        }
      }
    };

    onClickPrevious = () => {
      if (this.refCarouselText) {
        this.refCarouselText.current.previous();
      }
    };

    onClickNext = () => {
      if (this.refCarouselText) {
        this.refCarouselText.current.next();
      }
    };

    fetchDataInit = async () => {
      const {
        cms, questions, benefits,
      } = this.props;
      const page = _.find(cms, x => x.title === 'Personality');
      if (
        (!page || !_.isEmpty(questions) || !_.isEmpty(benefits))
      ) {
        try {
          const cmsData = await fetchCMSHomepage('personality');
          this.props.addCmsRedux(cmsData);
        } catch (error) {
          toastrError(error.message);
        }
      }
    };

    fetchCombo = (id1, id2) => {
      const options = {
        url: GET_COMBO_URL.replace('{id1}', id1).replace('{id2}', id2),
        method: 'GET',
      };
      return fetchClient(options);
    };

    updateOutCome = (likeProducts) => {
      const { outcome } = this.props;
      if (outcome.id) {
        putUpdateOutCome(outcome.id, likeProducts);
      }
    };

    backToHome = () => {
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    };

    findOutMore = () => {
      document
        .getElementById('resultHtml')
        .scrollIntoView({ behavior: 'smooth' });
    };

    render() {
      // console.log('outcome', this.props.outcome);
      const widthResult = isMobile && !isTablet ? '100%' : '500px';
      const {
        urlBackground,
        urlImage,
        datas,
        currentProducts,
        isShowKnowMore,
        description,
        typePersonal,
        arrayDescription,
        productsPersonal,
        dataPopup,
        step,
      } = this.state;
      if (!datas || datas.length === 0) {
        return <div />;
      }
      const cmsCommon = getCmsCommon(this.props.cms);
      const testAgain = getNameFromCommon(cmsCommon, 'TEST_AGAIN');
      const dataFilter = [];
      _.forEach(this.state.datas, (item) => {
        const genderTemp = _.find(
          this.props.personalInfo,
          x => x.name === 'gender',
        ).value;
        const gender = genderTemp === 'Gender-free' ? 'unisex' : genderTemp;
        const { product: products, perfume, shortTitle } = item;
        const url1 = products && products.length > 0 && products[0].images
          ? _.find(
            products[0].images,
            x => x.type === gender.toLowerCase(),
          ).image
          : '';
        const url2 = products && products.length > 1 && products[1].images
          ? _.find(
            products[1].images,
            x => x.type === gender.toLowerCase(),
          ).image
          : '';
        const imageKnowMoreObject1 = products && products.length > 0 && products[0].images
          ? _.find(products[0].images, x => x.type === 'long')
          : '';
        const imageKnowMoreObject2 = products && products.length > 1 && products[1].images
          ? _.find(products[1].images, x => x.type === 'long')
          : '';
        const data = {
          perfume,
          shortTitle,
          image_urls: [url1, url2],
          name: perfume.name,
          price:
                    perfume.items && perfume.items.length > 0
                      ? perfume.items[0].price
                      : '',
          products,
          buttonName: '+ ADD TO CART',
          buttonNameLeft: 'VIEW PRODUCT',
          mainTitle: item.name,
          isLike: true,
          isInfo: true,
          info1: {
            title:
            products && products.length > 0 ? products[0].name : '',
            content:
            products && products.length > 0
              ? products[0].description
              : '',
            imageKnowMore: imageKnowMoreObject1
              ? imageKnowMoreObject1.image
              : '',
          },
          info2: {
            title:
            products && products.length > 1 ? products[1].name : '',
            content:
            products && products.length > 1
              ? products[1].description
              : '',
            imageKnowMore: imageKnowMoreObject2
              ? imageKnowMoreObject2.image
              : '',
          },
        };
        dataFilter.push(data);
      });

      const cmsPersonal = _.find(
        this.props.cms,
        x => x.title === 'Personality',
      );
      const seo = getSEOFromCms(cmsPersonal);
      const textBlock = cmsPersonal && cmsPersonal.body
        ? _.find(cmsPersonal.body, x => x.type === 'text_block')
        : undefined;

      const resultHtml = (
        <div id="resultHtml">
          <div className="container_result">
            <span className="text--header">
              {textBlock ? textBlock.value : ''}
            </span>

            <div className="mt3" />
            <span className="text-name animation1">
              {arrayDescription.length > 0 ? arrayDescription[0] : ''}
            </span>

            <span className="text-description animation1">
              {arrayDescription.length > 1 ? arrayDescription[1] : ''}
            </span>
            <div className="mt3" />

            <span className="text-name animation2">
              {arrayDescription.length > 2 ? arrayDescription[2] : ''}
            </span>

            <span className="text-description animation2">
              {arrayDescription.length > 3 ? arrayDescription[3] : ''}
            </span>
          </div>
        </div>
      );

      const settings = {
        infinite: true,
        swipeToSlide: true,
        speed: 500,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        initialSlide: 1,
        centerMode: true,
        centerPadding: '0px',
      };
      const mobileOutCome = (
        <Slider {...settings}>
          {dataFilter.map((data, index) => (
            <ScentItem2ImgNew
              key={index}
              isBigSize
              data={data}
              onClickLeftButton={this.onClickLeftButton}
              onClickRightButton={this.onClickRightButton}
              onClickImage={this.onClickImage}
              onClickInfo={this.onClickInfo}
              cmsCommon={cmsCommon}
            />
          ))}
        </Slider>
      );

      const htmlOutcome = (
        <React.Fragment>
          {dataFilter.map((data, index) => (
            <ScentItem2ImgNew
              key={index}
              isBigSize
              data={data}
              onClickLeftButton={this.onClickLeftButton}
              onClickRightButton={this.onClickRightButton}
              onClickImage={this.onClickImage}
              onClickInfo={this.onClickInfo}
              cmsCommon={cmsCommon}
            />
          ))}
        </React.Fragment>
      );
      return (
        <div>
          <MetaTags>
            <title>
              {seo ? seo.seoTitle : ''}
            </title>
            <meta
              name="description"
              content={seo ? seo.seoDescription : ''}
            />
            <meta
              name="keywords"
              content="Age, Skintone, Gender, Male, Female, Woman,Men, Women, Gender-Free, Gender-Neutral, safe, secure, adventure, search, care, charge, Lead,tradition, Invent, Invention, Build, success, good-time, Organize, future, Surprises, ingredients, Charisma,tailored-scent"
            />
            <meta name="robots" content="index, follow" />
            <meta name="revisit-after" content="3 month" />
          </MetaTags>
          <Header />
          <CardKnowMore
            onCloseKnowMore={this.onCloseKnowMore}
            isShowKnowMore={isShowKnowMore}
            data={dataPopup}
          />
          <BackGroundVideo url={urlBackground} urlImage={urlImage} isVideo={false} />
          <Row
            className="container-scent"
            style={{
              marginLeft: '0px',
              marginRight: '0px',
              height: 'auto',
            }}
          >
            <Col
              md="12"
              xs="12"
              className="col-full"
              style={{
                marginLeft: '0px',
                marginRight: '0px',
                paddingLeft: '0px',
                paddingRight: '0px',
              }}
            >
              <div
                className="container-text"
                style={
                  isMobile && !isTablet && isIOS
                    ? {
                      marginTop: '70px',
                      width: widthResult,
                      height: '10vh',
                    }
                    : { marginTop: '70px', width: widthResult }
                }
              >
                <span
                  className="text"
                  style={
                    isMobile && !isTablet
                      ? { fontSize: '1.1rem' }
                      : { fontSize: '1.3rem' }
                  }
                >
                  {description}
                </span>
              </div>
              <div
                className="container-image"
                style={
                  isBrowser
                    ? {
                      alignItems: 'flex-start',
                      justifyContent: 'space-around',
                      display: 'flex',
                      flexDirection: 'row',
                    }
                    : {}
                }
              >
                {isMobile ? mobileOutCome : isTablet ? tabletOutCome : htmlOutcome}
              </div>
              <div
                className="div-row justify-center items-center"
                style={{
                  position: 'relative',
                  width: '100%',
                  height: '100px',
                  marginBottom: '50px',
                }}
              >
                <div
                  className="div-col justify-center items-center"
                  style={{
                    position: 'absolute',
                    bottom: '35%',
                  }}
                >
                  <button
                    className={isMobile ? 'btn-test-again-mobile' : isTablet ? 'btn-test-again-mobile' : 'btn-test-again-browser'}
                    type="button"
                    onClick={() => {
                      this.props.history.push(
                        generateUrlWeb('/quiz'),
                      );
                    }}
                  >
                    {testAgain}
                  </button>
                </div>
              </div>
            </Col>
          </Row>
          <FooterV2 />
        </div>
      );
    }
}

ResultScent.defaultProps = {};

ResultScent.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  outcome: PropTypes.shape({
    changeMood: PropTypes.arrayOf(Object),
    personalityChoise: PropTypes.arrayOf(Object),
  }).isRequired,
  questions: PropTypes.arrayOf(PropTypes.object).isRequired,
  personalInfo: PropTypes.arrayOf(PropTypes.object).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  cms: PropTypes.shape(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  createBasketGuest,
  addProductBasket,
  addCmsRedux,
};

function mapStateToProps(state) {
  return {
    outcome: state.outcome,
    questions: state.questions,
    personalInfo: state.personalInfo,
    basket: state.basket,
    login: state.login,
    cms: state.cms,
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ResultScent),
);
