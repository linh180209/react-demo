/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import '../../styles/result-new.scss';
import { generaCurrency, getNameFromCommon, getAltImageV2 } from '../../Redux/Helpers';
import bottleNew from '../../image/bottle-new.png';
import icInfo from '../../image/icon/ic-info-question.svg';

class ItemResult extends Component {
  render() {
    const {
      data, subTitle, cmsCommon, title, onOpenKnowMore, onClickImage, onClickIngredient, isQuizInstore, minTitle,
      isGift,
    } = this.props;
    console.log('isGift', isGift);
    const { items, combo, name } = data;
    const item = _.find(items, x => !x.is_sample);
    const products = _.filter(combo, x => x.product.type === 'Scent');
    const image1 = products && products.length > 0 ? _.find(products[0].product.images, x => x.type === 'unisex') : undefined;
    const ingredient1 = products && products.length > 0 ? products[0].product.ingredient : undefined;
    // const imageKnow1 = products && products.length > 0 ? _.find(products[0].product.images, x => x.type === 'long') : undefined;
    const image2 = products && products.length > 1 ? _.find(products[1].product.images, x => x.type === 'unisex') : undefined;
    // const imageKnow2 = products && products.length > 1 ? _.find(products[1].product.images, x => x.type === 'long') : undefined;
    const ingredient2 = products && products.length > 1 ? products[1].product.ingredient : undefined;
    const addToCart = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
    const choosePerfumeBt = getNameFromCommon(cmsCommon, 'choose_perfume');
    return (
      <div className="item-result div-col justify-center items-center">
        <h3>
          {subTitle}
        </h3>
        <span>
          {title}
        </span>
        {
          minTitle ? (
            <span className="min-title mt-2">
              {minTitle}
            </span>
          ) : (<div />)
        }
        <div className="div-image div-row mt-3">
          <div className="image-left">
            <img loading="lazy" onClick={onClickImage} src={image1 ? image1.image : ''} alt={getAltImageV2(image1)} />
            <button
              type="button"
              className="bt-knowmore-left"
              onClick={() => onClickIngredient(ingredient1)}
            >
              <img src={icInfo} alt="info" />
            </button>
          </div>
          <div className="image-right">
            <img loading="lazy" onClick={onClickImage} src={image2 ? image2.image : ''} alt={getAltImageV2(image2)} />
            <button
              type="button"
              className="bt-knowmore-right"
              onClick={() => onClickIngredient(ingredient2)}
            >
              <img src={icInfo} alt="info" />
            </button>
          </div>
          <img onClick={onClickImage} className="img-bottle" src={bottleNew} alt="bottle" />


        </div>
        <div className={`div-shop-item div-row ${isGift ? 'div-center' : ''}`}>
          {
            isGift ? (
              <button type="button" onClick={() => this.props.onClickChoiseGift(data, subTitle)}>
                {choosePerfumeBt}
              </button>
            ) : (
              <React.Fragment>
                <span className={isQuizInstore ? 'hidden' : ''}>
                  <b>
                    {generaCurrency(item ? item.price : '')}
                  </b>
                </span>
                <button
                  type="button"
                  className={isQuizInstore ? 'hidden' : ''}
                  onClick={() => this.props.onClickAddToCart(data, subTitle)}
                >
                  {addToCart}
                </button>
              </React.Fragment>
            )
          }
        </div>
      </div>
    );
  }
}

ItemResult.propTypes = {
  data: PropTypes.shape().isRequired,
  cmsCommon: PropTypes.shape().isRequired,
  subTitle: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  onOpenKnowMore: PropTypes.func.isRequired,
  onClickAddToCart: PropTypes.func.isRequired,
  onClickImage: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  isQuizInstore: PropTypes.bool.isRequired,
  minTitle: PropTypes.string.isRequired,
};

export default ItemResult;
