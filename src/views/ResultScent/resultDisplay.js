import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Col, Row } from 'reactstrap';
import smoothscroll from 'smoothscroll-polyfill';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { isIOS } from 'react-device-detect';
import BackGroundVideo from '../../components/backgroundVideo';
import CardKnowMore from '../../components/CardKnowMore';
import Header from '../../components/HomePage/header';
import ScentItem2ImgNew from '../../components/ScentItem/scentItem2ImgNew';
import { GET_COMBO_URL, GET_USER_OUTCOME, PUT_OUTCOME_URL } from '../../config';
import { addProductBasket, createBasketGuest } from '../../Redux/Actions/basket';
import updateBenefitsData from '../../Redux/Actions/benefits';
import addCmsRedux from '../../Redux/Actions/cms';
import { addProductChoisedRequest, clearAllProductChoised } from '../../Redux/Actions/productChoised';
import { clearAllProducts } from '../../Redux/Actions/products';
import updateQuestionsData from '../../Redux/Actions/questions';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import {
  fetchBenefits, fetchQuestions, getSEOFromCms, putUpdateOutCome, scrollTop, getNameFromCommon, getCmsCommon, fetchCMSHomepage, isCheckNull, googleAnalitycs, generateUrlWeb, gotoShopHome,
} from '../../Redux/Helpers/index';
import { isBrowser, isMobile, isTablet } from '../../DetectScreen';
import FooterV2 from '../../views2/footer';

class ResultDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      urlBackground: '',
      urlImage: '',
      currentProducts: {},
      datas: [],
      isShowKnowMore: false,
      description: '',
      personalProducts: [],
      dataPopup: {
        title: '',
        content: '',
      },
      likeProducts: [],
      step: 1,
    };
    this.refCarouselText = React.createRef();
  }

    componentDidMount = () => {
      scrollTop();
      const { questions } = this.props;
      smoothscroll.polyfill();

      googleAnalitycs('/quiz/results');

      const persionality = _.find(questions, x => x.type === 'PERSONALITY');
      const urlBackground = persionality
        ? persionality.video_urls[0]
        : undefined;
      const urlImage = persionality ? persionality.image_urls[0] : undefined;
      this.setState({ urlBackground, urlImage });
      this.fetchDataInit();
    };

    onChangeText = (data, step) => {
      this.setState({ currentProducts: data, step });
    };

    onCloseKnowMore = () => {
      this.setState({ isShowKnowMore: false });
    };

    onOpenKnowMore = (title, content, imageKnowMore) => {
      this.setState({
        isShowKnowMore: true,
        dataPopup: { title, content, imageKnowMore },
      });
    };

    onClickLeftButton = (d) => {
      this.onClickImage(d);
    };

    onClickRightButton = (d) => {
      const { perfume, shortTitle } = d;
      this.onClickAddtoCart(perfume, shortTitle);
    };

    onClickImage = (d) => {
      const { datas } = this.state;
      const { shortTitle } = d;
      const itemChoose = _.find(datas, x => x.shortTitle === shortTitle);
      const otherItems = _.filter(datas, x => x.shortTitle !== shortTitle);
      const queryProducts = [];
      if (itemChoose) {
        queryProducts.push({
          name: itemChoose.shortTitle,
          ids: [itemChoose.idProduct1, itemChoose.idProduct2],
          images: itemChoose.images,
        });
      }
      _.forEach(otherItems, (item) => {
        const { idProduct1, idProduct2 } = item;
        queryProducts.push({
          name: item.shortTitle,
          ids: [idProduct1, idProduct2],
          images: item.images,
        });
      });
      const queryString = encodeURIComponent(JSON.stringify(queryProducts));
      this.props.history.push(
        generateUrlWeb(`/products-questionnaire?products=${queryString}`),
      );
    };

    onClickInfo = (d) => {
      const { title, content, imageKnowMore } = d;
      this.onOpenKnowMore(title, content, imageKnowMore);
    };

    onClickFavorite = (id) => {
      const { likeProducts } = this.state;
      const likes = _.assign([], likeProducts);
      const idExist = _.find(likes, x => x === id);
      if (idExist) {
        _.remove(likes, x => x === id);
      } else {
        likes.push(id);
      }
      // console.log("likeProducts", likes);
      this.updateOutCome(likes);
      this.setState({ likeProducts: likes });
    };

    onClickAddtoCart = async (perfume, nameCustome) => {
      // console.log("onClickAddtoCart", perfume);
      const { basket } = this.props;
      const { items, combo } = perfume;
      const itemCombo = items && items.length > 0 ? items[0] : undefined;
      const itemBottle = _.find(combo, x => x.product.type === 'Bottle');
      const imageBottle = itemBottle
        ? _.find(itemBottle.product.images, x => x.type === 'unisex')
        : undefined;
      if (itemCombo) {
        const { price, id, name } = itemCombo;
        const data = {
          item: id,
          is_featured: itemCombo.is_featured,
          name: nameCustome || name,
          price,
          quantity: 1,
          total: parseFloat(price) * 1,
          image_urls: imageBottle ? [imageBottle.image] : undefined,
          combo,
        };
        const dataTemp = {
          idCart: basket.id,
          item: data,
        };
        if (!basket.id) {
          this.props.createBasketGuest(dataTemp);
        } else {
          this.props.addProductBasket(dataTemp);
        }
      }
    };

    onClickPrevious = () => {
      if (this.refCarouselText) {
        this.refCarouselText.current.previous();
      }
    };

    onClickNext = () => {
      if (this.refCarouselText) {
        this.refCarouselText.current.next();
      }
    };

    backToHome = () => {
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    };

    handleOutcomeData = (results) => {
      // console.log("handleOutcomeData", results);
      if (results && results.length > 0) {
        const lastResult = results[results.length - 1];
        // console.log("handleOutcomeData 1", lastResult);
        if (
          lastResult.benefit_products
                && lastResult.benefit_products.length > 0
        ) {
          if (lastResult.liked_products) {
            lastResult.liked_products = _.map(
              lastResult.liked_products,
              x => x.id,
            );
            this.setState({
              likeProducts: lastResult.liked_products
                ? lastResult.liked_products
                : [],
            });
          }
          this.processData(results);
        } else {
          this.props.history.push(generateUrlWeb('/quiz'));
        }
      } else {
        this.props.history.push(generateUrlWeb('/quiz'));
      }
    };

    getFetchOutCome = (pending) => {
      const outComeId = auth.getOutComeId();
      const { login } = this.props;
      if (login) {
        const { user } = login;
        if (user && user.id) {
          pending.push(this.fetchOutCome(user.id));
          return { isArray: true };
        }
        if (outComeId) {
          pending.push(this.fetchOutComeId(outComeId));
          return { isArray: false };
        }
        // this.props.history.push(generateUrlWeb('/'));
        gotoShopHome();
      } else if (outComeId) {
        pending.push(this.fetchOutComeId(outComeId));
        return { isArray: false };
      } else {
        // this.props.history.push(generateUrlWeb('/'));
        gotoShopHome();
      }
      return { isArray: false };
    };

    fetchCombo = (id1, id2) => {
      const options = {
        url: GET_COMBO_URL.replace('{id1}', id1).replace('{id2}', id2),
        method: 'GET',
      };
      return fetchClient(options);
    };

    fetchDataInit = async () => {
      const pending = [];
      const {
        cms, benefit, questions,
      } = this.props;
      const page = _.find(cms, x => x.title === 'Personality');
      if (
        (!page)
            || !_.isEmpty(benefit)
            || !_.isEmpty(questions)
      ) {
        pending.push(fetchBenefits());
        pending.push(fetchQuestions());
        pending.push(fetchCMSHomepage('personality'));

        const { isArray } = this.getFetchOutCome(pending);
        // try {
        const results = await Promise.all(pending);
        // console.log("results", results);
        this.props.updateBenefitsData(results[0]);
        this.props.updateQuestionsData(results[1]);
        const cmsData = results[2];
        const outComeData = results[3];
        if (cmsData && !page) {
          this.props.addCmsRedux(cmsData);
        }

        if (outComeData && !outComeData.isError) {
          this.handleOutcomeData(isArray ? outComeData : [outComeData]);
        } else {
          this.props.history.push(generateUrlWeb('/quiz'));
        }
        // } catch (error) {
        //   toastrError(error.message);
        // }
      } else if (page && this.state.datas.length === 0) {
        const { isArray } = this.getFetchOutCome(pending);
        try {
          const results = await Promise.all(pending);
          const outComeData = results[0];
          if (outComeData && !outComeData.isError) {
            this.handleOutcomeData(
              isArray ? outComeData : [outComeData],
            );
            return;
          }
          throw new Error();
        } catch (error) {
          this.props.history.push(generateUrlWeb('/quiz'));
        }
      }
    };

    fetchOutComeId = (id) => {
      if (isCheckNull(id)) {
        // this.props.history.push(generateUrlWeb('/'));
        gotoShopHome();
        return null;
      }
      const options = {
        url: PUT_OUTCOME_URL.replace('{id}', id),
        method: 'GET',
      };
      return fetchClient(options);
    };

    fetchOutCome = (userId) => {
      const options = {
        url: GET_USER_OUTCOME.replace('{user}', userId),
        method: 'GET',
      };
      return fetchClient(options, true);
    };

    updateOutCome = (likeProducts) => {
      const { login } = this.props;
      const { user } = login;
      if (user.outcome) {
        putUpdateOutCome(user.outcome, likeProducts);
      }
    };

    processData = (outcome) => {
      const tDatas = [];
      let chooseProduct;
      let description = null;
      let typePersonal = '';
      let arrayDescription = [];
      let personalProducts = [];
      if (outcome && outcome.length > 0) {
        const {
          benefit_products: benefitProducts,
          products: chooseProducts,
          personality_choice: personalityChoice,
        } = outcome[0];
        personalProducts = outcome[0].personality_products;
        // console.log("tDatas benefitProducts", benefitProducts);
        _.forEach(benefitProducts, (d) => {
          const { products, benefit_choice: benefitChoice } = d;
          const item = {};
          if (benefitChoice) {
            item.title = benefitChoice.benefit.title;
            item.shortTitle = benefitChoice.benefit.short_title;
            item.gender = benefitChoice.gender;
            item.images = benefitChoice.benefit.images;
          }
          if (products && products.length > 1) {
            item.idProduct1 = products[0].id;
            item.idProduct2 = products[1].id;
            const product1 = products[0].items[0];
            const product2 = products[1].items[0];
            product1.idProduct = products[0].id;
            product2.idProduct = products[1].id;
            product1.content = products[0].description;
            product2.content = products[1].description;
            product1.url = products[0].images
              ? _.find(
                products[0].images,
                x => x.type === item.gender.toLowerCase(),
              ).image
              : '';
            product2.url = products[1].images
              ? _.find(
                products[1].images,
                x => x.type === item.gender.toLowerCase(),
              ).image
              : '';
            const imageKnowMoreObject1 = products && products.length > 0 && products[0].images
              ? _.find(products[0].images, x => x.type === 'long')
              : '';
            const imageKnowMoreObject2 = products && products.length > 1 && products[1].images
              ? _.find(products[1].images, x => x.type === 'long')
              : '';
            // console.log("imageKnowMoreObject1", imageKnowMoreObject1);
            product1.imageKnowMore = imageKnowMoreObject1
              ? imageKnowMoreObject1.image
              : '';
            product2.imageKnowMore = imageKnowMoreObject2
              ? imageKnowMoreObject2.image
              : '';
            item.products = [product1, product2];
          }
          tDatas.push(item);
        });
        if (chooseProducts && chooseProducts.length > 1) {
          chooseProduct = _.find(
            tDatas,
            d => (d.idProduct1 === chooseProducts[0].id
                            && d.idProduct2 === chooseProducts[1].id)
                        || (d.idProduct1 === chooseProducts[1].id
                            && d.idProduct2 === chooseProducts[0].id),
          );
          // console.log("chooseProduct", chooseProduct);
        }
        if (personalityChoice) {
          description = personalityChoice.description;
        }
        if (personalityChoice) {
          typePersonal = personalityChoice.choice;
          arrayDescription = personalityChoice.personality.description.split(
            '\r\n',
          );
          _.remove(arrayDescription, x => x === '');
        }
      }
      // console.log("tDatas", tDatas);
      const pending = [];
      _.forEach(tDatas, (e) => {
        const { products } = e;
        if (products && products.length > 1) {
          pending.push(
            this.fetchCombo(
              products[0].idProduct,
              products[1].idProduct,
            ),
          );
        }
      });
      Promise.all(pending).then((results) => {
        if (results && results.length > 0) {
          _.forEach(results, (d, index) => {
            tDatas[index].perfume = d;
          });
          this.setState({
            datas: tDatas,
            currentProducts: tDatas[0],
            description,
            typePersonal,
            arrayDescription,
            personalProducts,
          });
          console.log(description);
        }
      });
    };

    findOutMore = () => {
      document
        .getElementById('resultHtml')
        .scrollIntoView({ behavior: 'smooth' });
    };

    render() {
      const widthResult = isMobile && !isTablet ? '100%' : '500px';
      const {
        urlBackground,
        urlImage,
        datas,
        currentProducts,
        isShowKnowMore,
        description,
        typePersonal,
        arrayDescription,
        personalProducts,
        dataPopup,
        likeProducts,
        step,
      } = this.state;
      if (!datas || datas.length === 0) {
        return <div />;
      }
      const cmsCommon = getCmsCommon(this.props.cms);
      const testAgain = getNameFromCommon(cmsCommon, 'TEST_AGAIN');
      const addToCartBt = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
      const viewProductBt = getNameFromCommon(cmsCommon, 'VIEW PRODUCT');

      const dataFilter = [];
      _.forEach(this.state.datas, (item) => {
        console.log(item);
        const { products, perfume, shortTitle } = item;
        const url1 = products && products.length > 0 ? products[0].url : '';
        const url2 = products && products.length > 1 ? products[1].url : '';
        const imageKnowMore1 = products && products.length > 0
          ? products[0].imageKnowMore
          : '';
        const imageKnowMore2 = products && products.length > 1
          ? products[1].imageKnowMore
          : '';
        const data = {
          perfume,
          shortTitle,
          image_urls: [url1, url2],
          name: perfume.name,
          images: item.images,
          price:
                    perfume.items && perfume.items.length > 0
                      ? perfume.items[0].price
                      : '',
          products,
          mainTitle: item.title,
          buttonName: `+ ${addToCartBt ? addToCartBt.toUpperCase() : ''}`,
          buttonNameLeft: viewProductBt,
          isLike: true,
          isInfo: true,
          info1: {
            title:
                        products && products.length > 0 ? products[0].name : '',
            content:
                        products && products.length > 0
                          ? products[0].content
                          : '',
            imageKnowMore: imageKnowMore1,
          },
          info2: {
            title:
                        products && products.length > 1 ? products[1].name : '',
            content:
                        products && products.length > 1
                          ? products[1].content
                          : '',
            imageKnowMore: imageKnowMore2,
          },
        };

        dataFilter.push(data);
      });
      const cmsPersonal = _.find(
        this.props.cms,
        x => x.title === 'Personality',
      );
      const seo = getSEOFromCms(cmsPersonal);

      const settings = {
        infinite: true,
        swipeToSlide: true,
        speed: 500,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        initialSlide: 1,
        centerMode: true,
        centerPadding: '0px',
      };
      const mobileOutCome = (
        <Slider {...settings}>
          {dataFilter.map((data, index) => (
            <ScentItem2ImgNew
              key={index}
              isBigSize
              data={data}
              onClickLeftButton={this.onClickLeftButton}
              onClickRightButton={this.onClickRightButton}
              onClickImage={this.onClickImage}
              onClickInfo={this.onClickInfo}
              cmsCommon={cmsCommon}
            />
          ))}
        </Slider>
      );

      const tabletOutCome = (
        <Slider {...settings}>
          {dataFilter.map((data, index) => (
            <ScentItem2ImgNew
              key={index}
              isBigSize
              data={data}
              onClickLeftButton={this.onClickLeftButton}
              onClickRightButton={this.onClickRightButton}
              onClickImage={this.onClickImage}
              onClickInfo={this.onClickInfo}
              cmsCommon={cmsCommon}
            />
          ))}
        </Slider>
      );

      const htmlOutcome = (
        <React.Fragment>
          {dataFilter.map((data, index) => (
            <ScentItem2ImgNew
              key={index}
              isBigSize
              data={data}
              onClickLeftButton={this.onClickLeftButton}
              onClickRightButton={this.onClickRightButton}
              onClickImage={this.onClickImage}
              onClickInfo={this.onClickInfo}
              cmsCommon={cmsCommon}
            />
          ))}
        </React.Fragment>
      );
      return (
        <React.Fragment>
          <MetaTags>
            <title>
              {' '}
              {seo ? seo.seoTitle : ''}
              {' '}
            </title>
            <meta
              name="description"
              content={seo ? seo.seoDescription : ''}
            />
            <meta
              name="keywords"
              content="Age, Skintone, Gender, Male, Female, Woman,Men, Women, Gender-Free, Gender-Neutral, safe, secure, adventure, search, care, charge, Lead,tradition, Invent, Invention, Build, success, good-time, Organize, future, Surprises, ingredients, Charisma,tailored-scent"
            />
            <meta name="robots" content="index, follow" />
            <meta name="revisit-after" content="3 month" />
          </MetaTags>
          <Header />
          <CardKnowMore
            onCloseKnowMore={this.onCloseKnowMore}
            isShowKnowMore={isShowKnowMore}
            data={dataPopup}
          />
          <BackGroundVideo url={urlBackground} urlImage={urlImage} isVideo={false} />
          <div className="background--letsguideyou" />
          <Row
            className="container-scent"
            style={{
              marginLeft: '0px',
              marginRight: '0px',
              height: 'auto',
            }}
          >
            <Col
              md="12"
              xs="12"
              className="col-full"
              style={{
                marginLeft: '0px',
                marginRight: '0px',
                paddingLeft: '0px',
                paddingRight: '0px',
                width: '100%',
              }}
            >
              <div
                className="container-text"
                style={isMobile && !isTablet && isIOS ? {
                  marginTop: '70px',
                  width: widthResult,
                  height: '10vh',
                } : { marginTop: '70px', width: widthResult }
                            }
              >
                <span
                  className="text"
                  style={isMobile && !isTablet ? { fontSize: '1.1rem' } : { fontSize: '1.3rem' }
                                }
                >
                  {description}
                </span>
              </div>
              <div
                className="container-image"
                style={isBrowser ? {
                  flex: '1 0 32%', justifyContent: 'space-around', display: 'flex', flexDirection: 'row',
                } : {}}
              >
                {isMobile ? mobileOutCome : isTablet ? tabletOutCome : htmlOutcome}
              </div>
              <div
                className="div-row justify-center items-center"
                style={{
                  position: 'relative',
                  width: '100%',
                  height: '100px',
                  marginBottom: '50px',
                }}
              >
                <div
                  className="div-col justify-center items-center"
                  style={{
                    position: 'absolute',
                    bottom: '35%',
                  }}
                >
                  <button
                    className={isMobile ? 'btn-test-again-mobile' : isTablet ? 'btn-test-again-mobile' : 'btn-test-again-browser'}
                    type="button"
                    onClick={() => {
                      this.props.history.push(
                        generateUrlWeb('/quiz'),
                      );
                    }}
                  >
                    {testAgain}
                  </button>
                </div>
              </div>
            </Col>
          </Row>
          <FooterV2 />
        </React.Fragment>
      );
    }
}

ResultDisplay.defaultProps = {};

ResultDisplay.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  questions: PropTypes.arrayOf(PropTypes.object).isRequired,
  login: PropTypes.shape({
    token: PropTypes.string,
  }).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  updateBenefitsData: PropTypes.func.isRequired,
  updateQuestionsData: PropTypes.func.isRequired,
  cms: PropTypes.shape(PropTypes.object).isRequired,
};

function mapStateToProps(state) {
  return {
    // products: state.products,
    // personalInfo: state.personalInfo,
    questions: state.questions,
    productChoised: state.productChoised,
    benefits: state.benefits,
    login: state.login,
    basket: state.basket,
    cms: state.cms,
  };
}

const mapDispatchToProps = {
  clearAllProducts,
  clearAllProductChoised,
  addProductChoisedRequest,
  createBasketGuest,
  addProductBasket,
  addCmsRedux,
  updateBenefitsData,
  updateQuestionsData,
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ResultDisplay),
);
