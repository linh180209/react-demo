/* eslint-disable react/sort-comp */
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  isSafari,
} from 'react-device-detect';
import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import '../../styles/style.scss';

import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import SelectionScent from '../../components/MMO/selectionScent';
import {
  CREATE_PRODUCT_URL, GET_ALL_PRODUCTS_SCENT, GET_ALL_SCENTS_PRODUCTS, GET_ALL_SCENT_NOTES, GET_PRODUCT_FOR_CART,
} from '../../config';
import { isMobile, isTablet } from '../../DetectScreen';
import {
  addProductBasket,
  createBasketGuest,
} from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import updateMmosData from '../../Redux/Actions/mmos';
import addScentsToStore, { addScentNotesToStore } from '../../Redux/Actions/scents';
import {
  fetchCMSHomepage, generateHreflang,
  generateUrlWeb, getCmsCommon, getNameFromCommon, getProductDualCrayonCombo, getSEOFromCms, removeLinkHreflang, scrollTop, segmentTrackScentBarQuizInitiated, setPrerenderReady,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient, {
  fetchClientFormData,
} from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';
import FooterV2 from '../../views2/footer';
import MultiStep from './components/MultiStep';

const getListTimeStamp = (datas) => {
  console.log('datas', datas);
  const list = [0];
  _.forEach(datas, (d) => {
    list.push(d.value.timestamp);
  });
  return list;
};

const prePareCms = (mmoCms) => {
  if (mmoCms) {
    const seo = getSEOFromCms(mmoCms);
    const { body } = mmoCms;
    const videoBlocks = _.filter(body, x => x.type === 'video_block');
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    const headerBlocks = _.find(body, x => x.type === 'header_block');
    const subTextBlocks = _.filter(body, x => x.type === 'text_block');
    const valueFindVideo = isMobile ? 'phone' : isTablet ? 'tablet' : 'web';
    const imageBlock = _.filter(body, x => x.type === 'image_block');
    const headerAndParagraphBocks = _.filter(
      body,
      x => x.type === 'header_and_paragraph_block',
    );

    const videoBlock = _.find(
      videoBlocks,
      x => x.value.type === valueFindVideo,
    );
    const stepBlock = _.filter(body, x => x.type === 'timestamp_block');
    const listTimeStamp = stepBlock ? getListTimeStamp(stepBlock) : [];
    const urlVideo = videoBlock ? videoBlock.value.video : undefined;
    const placeholder = videoBlock ? videoBlock.value.placeholder : undefined;
    const headerCms = headerBlocks && headerBlocks.value ? headerBlocks.value.header_text : '';
    const buttonCms = _.map(buttonBlocks, d => d.value);
    return {
      listTimeStamp,
      urlVideo,
      placeholder,
      seo,
      headerCms,
      buttonCms,
      buttonBlocks,
      subTextCms: subTextBlocks,
      headerAndParagraphBocks,
      videoBlocks,
      imageBlock,
    };
  }
  return {
    listTimeStamp: [],
    urlVideo: '',
    seo: undefined,
    headerCms: '',
    subTextCms: [],
    buttonCms: [],
    videoBlocks: [],
    buttonBlocks,
    headerAndParagraphBocks,
    imageBlock: [],
  };
};

class MMO2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowSelect: false,
      data1: undefined,
      data2: undefined,
      isCreated: false,
      isRandom: false,
      products: [],
      listTimeStamp: [],
      seo: undefined,
      urlVideo: '',
      headerCms: '',
      subTextCms: '',
      buttonCms: [],
      isFetchData: false,
      isShowIngredient: false,
      ingredientDetail: undefined,
      videoBlocks: [],
      buttonBlocks: [],
      headerAndParagraphBocks: [],
      resultScentNotes: [],
      imageBlock: [],
      datacustome: undefined,
    };
    this.listDataLeft = [];
    this.listDataRight = [];
    this.getTime = undefined;
    this.step = 0;
  }

  fetchAllScent = () => {
    const { scents } = this.props;
    if (scents && scents.length > 0) {
      return;
    }
    const option = {
      url: GET_ALL_SCENTS_PRODUCTS,
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.props.addScentsToStore(result);
        return;
      }
      throw new Error(result.message);
    }).catch((error) => {
      toastrError(error.message);
    });
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    segmentTrackScentBarQuizInitiated();
    this.fetchDataInit();
    this.fetchAllScent();
    this.getTime = setInterval(this.handleVideo, 500);
    // this.refs.player.play();
    // setTimeout(() => {
    //   this.refs.player.pause();
    // }, 500);
  }

  componentDidUpdate(prevProps, prevState) {
    const { isFetchData } = this.state;
    if (isFetchData) {
      this.setState({ isFetchData: false });
      this.fetchDataInit();
    }
    if (prevState.isRandom !== this.state.isRandom && this.state.isRandom) {
      const symbolContainerLeft = document.getElementById('random-left');
      const symbolContainerRight = document.getElementById('random-right');
      if (symbolContainerLeft && !isSafari) {
        const animationLeft = symbolContainerLeft.animate(
          [
            { transform: 'none', filter: 'blur(0)' },
            { filter: 'blur(2px)', offset: 0.5 },
            {
              transform: `translateY(-${80.8}%)`,
              filter: 'blur(0)',
            },
          ],
          {
            duration: 1 * 1000,
            easing: 'ease-in-out',
          },
        );
        const animationPromise = new Promise(
          resolve => (animationLeft.onfinish = resolve),
        );
        animationLeft.play();
        animationPromise.then(() => {
          const data1 = _.cloneDeep(
            this.listDataLeft[this.listDataLeft.length - 1],
          );
          this.setState({ isRandom: false, data1 });
          this.setStep(true, true);
        });
      }
      if (symbolContainerRight && !isSafari) {
        const animationRight = symbolContainerRight.animate(
          [
            { transform: 'none', filter: 'blur(0)' },
            { filter: 'blur(2px)', offset: 0.5 },
            {
              transform: `translateY(${80.8}%)`,
              filter: 'blur(0)',
            },
          ],
          {
            duration: 1 * 1000,
            easing: 'ease-in-out',
          },
        );
        const animationPromise = new Promise(
          resolve => (animationRight.onfinish = resolve),
        );
        animationRight.play();
        animationPromise.then(() => {
          const data2 = _.cloneDeep(this.listDataRight[0]);
          this.setState({ isRandom: false, data2 });
        });
      }
    }
    if (prevState.login !== this.state.login && this.state.results) {
      const products = this.createData(this.state.results);
      this.setState({ products });
    }
  }

  componentWillUnmount() {
    removeLinkHreflang();
    if (this.getTime) {
      clearInterval(this.getTime);
    }
  }

  onOpenSelectionData1 = () => {
    this.selection = 'data1';
    this.setState({ isShowSelect: true });
  };

  onOpenSelectionData2 = () => {
    this.selection = 'data2';
    this.setState({ isShowSelect: true });
  };

  onCloseSelection = () => {
    this.setState({ isShowSelect: false });
  };

  onClickAddMix = (d) => {
    const cmsCommon = getCmsCommon(this.props.cms);
    const removeBt = getNameFromCommon(cmsCommon, 'Remove');

    const data = _.assign({}, d);

    data.buttonName = `- ${removeBt ? removeBt.toUpperCase() : ''}`;
    data.titleTop = undefined;
    if (this.selection === 'data1') {
      this.setState({ isShowSelect: false, data1: data });
      this.setStep(true, this.state.data2);
    } else {
      this.setState({ isShowSelect: false, data2: data });
      this.setStep(this.state.data1, true);
    }
  };

  onClickRemoveScentData1 = () => {
    this.setState({ data1: undefined });
    this.setStep(false, this.state.data2);
  };

  onClickRemoveScentData2 = () => {
    this.setState({ data2: undefined });
    this.setStep(this.state.data1, false);
  };

  handleDataCustome = (data) => {
    this.setState({ datacustome: data });
  };

  getProduct = (id1, id2) => {
    const option = {
      url: `${GET_PRODUCT_FOR_CART}?combo=${id1},${id2}&type=perfume`,
      method: 'GET',
    };
    return fetchClient(option);
  }

  onCreateMix = async (data1, data2, isDualCrayons) => {
    // const { data1, data2 } = this.state;
    const { basket } = this.props;
    const idItem = basket.id;
    // console.log("please", dataTemp);
    const product = isDualCrayons ? await getProductDualCrayonCombo(data1.id, data2.id) : await this.getProduct(data1.id, data2.id);
    const link = generateUrlWeb(`/product/${isDualCrayons ? 'dual_crayons' : 'perfume_diy'}/${product.id}`);
    const { login } = this.props;
    if (login && login.user) {
      this.createProduct(data1.id, data2.id, link);
    } else if (this.state.datacustome) {
      const custome = {
        image: this.state.datacustome.currentImg
          ? this.state.datacustome.currentImg
          : '',
        name: this.state.datacustome.name
          ? this.state.datacustome.name
          : '',
        isCustome: true,
        font: this.state.datacustome.font
          ? this.state.datacustome.font
          : '',
        color: this.state.datacustome.color
          ? this.state.datacustome.color
          : '',
        idCartItem: idItem,
      };
      this.props.history.push(link, {
        custome: isDualCrayons ? {} : custome,
      });
    } else {
      this.props.history.push(link);
    }
    this.setState({ isCreated: true });
  };

  // onClickRandom = () => {
  //   this.listDataLeft = this.randomListAnimation(this.state.products);
  //   this.listDataRight = this.randomListAnimation(
  //     this.state.products,
  //     this.listDataLeft[0]
  //   );
  //   if (isMobile || isSafari || isEdge) {
  //     const data1 = _.cloneDeep(this.listDataLeft[0]);
  //     const data2 = _.cloneDeep(
  //       this.listDataRight[this.listDataRight.length - 1]
  //     );
  //     this.refs.player.seek(0);
  //     this.setStep(true, true);
  //     this.setState({ data1, data2 });
  //   } else {
  //     this.refs.player.seek(0);
  //     this.setState({ isRandom: true });
  //   }
  // };

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { login, mmos } = nextProps;
    if (login !== prevState.login) {
      _.assign(objectReturn, { login });
    }
    if (mmos && mmos !== prevState.mmos) {
      _.assign(objectReturn, { mmos });
    }
    console.log('objectReturn', objectReturn);
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  setStep = (data1, data2) => {
    // console.log("data1", data1);
    // console.log("data2", data2);
    if (!data1 && !data2) {
      this.step = 0;
      this.refs.player.seek(0);
    } else if ((data1 && !data2) || (!data1 && data2)) {
      this.step = 1;
      this.refs.player.seek(0);
    } else if (data1 && data2) {
      this.step = 2;
      // this.refs.player.seek(this.state.listTimeStamp[1]);
    }
  };

  createProduct = (id1, id2, link) => {
    const formData = new FormData();
    formData.append('combo', JSON.stringify([id1, id2]));
    const option = {
      url: CREATE_PRODUCT_URL,
      method: 'POST',
      body: formData,
    };
    fetchClientFormData(option, true)
      .then((result) => {
        if (result) {
          this.props.history.push(link);
        }
      })
      .catch((err) => {
        toastrError(err);
      });
  };

  fetchAllScents = () => {
    const options = {
      url: GET_ALL_PRODUCTS_SCENT,
      method: 'GET',
    };
    return fetchClient(options);
  };

  fetchScentNotes = () => {
    const options = {
      url: GET_ALL_SCENT_NOTES,
      method: 'GET',
    };
    return fetchClient(options);
  };

  fetchDataInit = async () => {
    const { cms, mmos } = this.props;
    const { dataScentNote } = this.state;
    const mmoCms = _.find(cms, x => x.title === 'Freestyle');
    if (!mmoCms || _.isEmpty(mmos) || _.isEmpty(dataScentNote)) {
      this.props.loadingPage(true);
      const pending = [
        fetchCMSHomepage('mmo-v2'),
        this.fetchAllScents(),
        this.fetchScentNotes(),
      ];
      try {
        const results = await Promise.all(pending);
        const cmsData = results[0];
        const result = results[1];
        const resultScentNotes = results[2];
        this.props.addScentNotesToStore(resultScentNotes);
        this.props.addCmsRedux(cmsData);
        this.props.updateMmosData(result);
        const {
          listTimeStamp,
          urlVideo,
          seo,
          placeholder,
          headerCms,
          subTextCms,
          buttonCms,
          buttonBlocks,
          headerAndParagraphBocks,
          videoBlocks,
          imageBlock,
        } = prePareCms(cmsData);
        console.log(seo);
        const seoUpdate = {
          ...seo,
          seoTitle: `Quiz based on your ingredients in ${auth.getCountryName()} | Maison 21G`,
        };
        const products = this.createData(result);
        this.setState({
          products,
          results: result,
          resultScentNotes,
          listTimeStamp,
          urlVideo,
          seo: seoUpdate,
          placeholder,
          headerCms,
          subTextCms,
          buttonCms,
          videoBlocks,
          buttonBlocks,
          headerAndParagraphBocks,
          imageBlock,
        });
        this.props.loadingPage(false);
      } catch (error) {
        toastrError(error.message);
        this.props.loadingPage(false);
      }
    } else {
      const { mmos: results, cms: cmsPage } = this.props;
      const cmsT = _.find(cmsPage, x => x.title === 'MMO-V2');
      const {
        listTimeStamp,
        urlVideo,
        videoBlocks,
        seo,
        headerCms,
        subTextCms,
        buttonCms,
        buttonBlocks,
        resultScentNotes,
        headerAndParagraphBocks,
        imageBlock,
      } = prePareCms(cmsT);
      const products = this.createData(results);
      this.setState({
        products,
        results: mmos,
        listTimeStamp,
        urlVideo,
        videoBlocks,
        resultScentNotes,
        seo,
        headerCms,
        subTextCms,
        buttonCms,
        buttonBlocks,
        headerAndParagraphBocks,
        imageBlock,
      });
    }
  };

  createData = (results) => {
    const products = [];
    _.forEach(results, (d) => {
      const item = {
        name: d.name,
        image_urls: d ? [d.image] : '',
        description: d.description,
        ingredient: d.ingredient,
        id: d.id,
        content2: d.family,
        header2: 'Family',
        suggestions: d.suggestions,
      };
      products.push(item);
    });
    return _.orderBy(products, 'name', 'asc');
  };

  randomListAnimation = (datas, dExist) => {
    const cmsCommon = getCmsCommon(this.props.cms);
    const removeBt = getNameFromCommon(cmsCommon, 'Remove');

    const results = [];
    _.forEach(Array(5), () => {
      const list = _.filter(datas, d => !results.includes(d));
      let data;
      if (datas.length > 9) {
        while (
          (data && data.id === (dExist ? dExist.id : undefined))
          || !data
        ) {
          data = list[Math.floor(Math.random() * list.length)];
        }
      } else {
        data = list[Math.floor(Math.random() * list.length)];
      }
      data.titleTop = undefined;
      data.buttonName = `- ${removeBt ? removeBt.toUpperCase() : ''}`;
      results.push(data);
    });
    return results;
  };

  handleVideo = () => {
    if (this.refs && this.refs.player) {
      const { player } = this.refs.player.getState();
      const { currentTime, paused } = player;
      const timeStop = this.state.listTimeStamp[this.step];
      if (currentTime < timeStop && paused) {
        this.refs.player.play();
      } else if (currentTime >= timeStop && !paused) {
        this.refs.player.pause();
      }
    }
  };

  onClickAddSuggest1 = (id) => {
    // console.log('onClickAddSuggest1', id);
    const { products } = this.state;
    const ele = _.find(products, x => x.id === id);
    if (ele) {
      this.selection = 'data1';
      this.onClickAddMix(ele);
    }
  };

  onClickAddSuggest2 = (id) => {
  //  console.log('onClickAddSuggest2', id);
    const { products } = this.state;
    const ele = _.find(products, x => x.id === id);
    if (ele) {
      this.selection = 'data2';
      this.onClickAddMix(ele);
    }
  };

  onClickIngredient = (ingredientDetail) => {
    // console.log('ingredientDetail CLICKED ', ingredientDetail);
    this.setState({ ingredientDetail, isShowIngredient: true });
  };

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  };

  render() {
    const {
      isShowSelect,
      data1,
      data2,
      isCreated,
      isRandom,
      products,
      urlVideo,
      placeholder,
      seo,
      headerCms,
      buttonCms,
      headerAndParagraphBocks,
      buttonBlocks,
      subTextCms,
      isShowIngredient,
      videoBlocks,
      ingredientDetail,
      resultScentNotes,
      imageBlock,
    } = this.state;
    if (!products || products.length === 0) {
      return null;
    }
    const { login } = this.props;
    const urlStartVideo = videoBlocks
      ? _.filter(videoBlocks, x => x.value.caption === 'intro-mmo')[0].value
        .video
      : '';
    const gender = login && login.user && login.user.gender ? login.user.gender : 'unisex';
    // this.setStep(data1, data2);
    // console.log('placeholder', placeholder, gender);
    const htmlWeb = () => (
      <div className="div-mmo">
        <MultiStep
          buttonBlocks={buttonBlocks}
          dataScentNote={resultScentNotes}
          bottleGif={imageBlock}
          resultScentNotes={resultScentNotes}
          video={urlStartVideo}
          headerAndParagraphBocks={headerAndParagraphBocks}
          headerCms={headerCms}
          onCreateMix={this.onCreateMix}
          handleDataCustome={this.handleDataCustome}
          onClickIngredient={this.onClickIngredient}
          onClickAddSuggest1={this.onClickAddSuggest1}
          onClickAddSuggest2={this.onClickAddSuggest2}
          mmos={this.props.mmos}
        />
      </div>
    );

    const htmlMobile = () => (
      <div className="mmo2-mobile">
        <MultiStep
          buttonBlocks={buttonBlocks}
          dataScentNote={resultScentNotes}
          bottleGif={imageBlock}
          resultScentNotes={resultScentNotes}
          video={urlStartVideo}
          headerCms={headerCms}
          headerAndParagraphBocks={headerAndParagraphBocks}
          onCreateMix={this.onCreateMix}
          handleDataCustome={this.handleDataCustome}
          onClickIngredient={this.onClickIngredient}
          onClickAddSuggest1={this.onClickAddSuggest1}
          onClickAddSuggest2={this.onClickAddSuggest2}
          mmos={this.props.mmos}
        />
      </div>
    );
    const listSuggest = data1 && !data2
      ? data1.suggestions
      : !data1 && data2
        ? data2.suggestions
        : [];
    const suggestIds = _.map(listSuggest, x => x.id);
    const countryName = auth.getCountryName();

    return (
      <div>
        <MetaTags>
          <title>{seo ? replaceCountryInMetaTags(countryName, seo?.seoTitle) : ''}</title>
          <meta
            name="description"
            content={seo ? replaceCountryInMetaTags(countryName, seo?.seoDescription) : ''}
          />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/freestyle')}
        </MetaTags>
        <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
          // className="min-top-height"
        />
        {isShowSelect ? (
          <SelectionScent
            datas={_.filter(
              products,
              x => x.id !== (data1 ? data1.id : undefined)
                && x.id !== (data2 ? data2.id : undefined),
            )}
            close={this.onCloseSelection}
            onClickIngredient={this.onClickIngredient}
            onClickRightButton={this.onClickAddMix}
            cms={this.props.cms}
            suggestIds={suggestIds}
          />
        ) : (
          <div />
        )}
        {/* <HeaderHomePage visible={false} isSpecialMenu isDisableScrollShow isNotShowRegion /> */}
        <HeaderHomePageV3 />
        <div>{isMobile ? htmlMobile() : htmlWeb()}</div>
        <FooterV2 />
      </div>
    );
  }
}

MMO2.propTypes = {
  loadingPage: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  mmos: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  updateMmosData: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    cms: state.cms,
    mmos: state.mmos,
    basket: state.basket,
    countries: state.countries,
    scents: state.scents,
  };
}

const mapDispatchToProps = {
  loadingPage,
  addCmsRedux,
  updateMmosData,
  addProductBasket,
  createBasketGuest,
  addScentsToStore,
  addScentNotesToStore,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MMO2));
