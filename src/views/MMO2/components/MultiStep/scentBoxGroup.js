import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
// import clubBanner from '../../assets/club-banner.png';
import './styles.scss';
import Slider from 'react-slick';
import { Row, Col } from 'reactstrap';
import icChecked from '../../../../image/icon/ic-checked-quiz.svg';
import icBack from '../../../../image/icon/back-product.svg';
import ProgressLine from '../../../ResultScentV2/progressLine';
import ProgressCircle from '../../../ResultScentV2/progressCircle';
import icNextWhite from '../../../../image/icon/ic-next-white.svg';
import icNext from '../../../../image/icon/ic-next-mmo.svg';
import icInfo from '../../../../image/icon/icNewInfo.svg';
import { isBrowser } from '../../../../DetectScreen';

class scentBoxGroup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data:
        this.props.showMixSectionFrom == 'first'
          ? _.filter(this.props.dataScentNote, x => x.type == 'heart')[0]
          : this.props.isDay
            ? _.filter(this.props.dataScentNote, x => x.type == 'top')[0]
            : _.filter(this.props.dataScentNote, x => x.type == 'bot')[0],
      dataNote: [],
      currentView: this.props.showMixSectionFrom != null ? 'main-notes' : '',
      profile: [],
      nameScent: '',
      titleProfile: '',
      isChecked: null,
      isMainScent: this.props.showMixSectionFrom != 'second',
      dataProduct: {
        firstNotes: null,
        secondNotes: null,
      },
    };
  }

  handleSelectNotes = (name, data) => {
    this.setState({
      currentView: 'product',
      dataNote: _.filter(data.tags, x => x.name == name)[0],
    });
  };

  handleSelectProduct = (id, title, data, isMainScent) => {
    const dataProfile = _.filter(
      this.state.dataNote.products,
      x => x.id == id,
    )[0];
    if (id == this.state.isChecked) {
      this.setState({
        profile: dataProfile.profile,
        name: dataProfile.ingredient.name,
        titleProfile: title,
        isChecked: null,
      });
      if (isMainScent) {
        this.setState({
          dataProduct: {
            ...this.state.dataProduct,
            firstNotes: null,
          },
        });
      } else {
        this.setState({
          dataProduct: {
            ...this.state.dataProduct,
            secondNotes: null,
          },
        });
      }
    } else {
      this.setState({
        profile: dataProfile.profile,
        name: dataProfile.ingredient.name,
        titleProfile: title,
        isChecked: id,
      });
      if (isMainScent) {
        this.setState({
          dataProduct: {
            ...this.state.dataProduct,
            firstNotes: data,
          },
        });
      } else {
        this.setState({
          dataProduct: {
            ...this.state.dataProduct,
            secondNotes: data,
          },
        });
      }
    }
  };

  handleSelectNotes3 = () => {
    // console.log("step3", this.state.dataProduct);
    this.props.selectNotesStep3(
      this.state.dataProduct,
      !!this.state.isMainScent,
    );
    this.handleCancel();

    this.props.fetchCombo(
      this.state.dataProduct && this.state.dataProduct.firstNotes
        ? this.state.dataProduct.firstNotes.id
        : this.props.dataStep2.firstNotes.id,
      this.state.dataProduct && this.state.dataProduct.secondNotes
        ? this.state.dataProduct.secondNotes.id
        : this.props.dataStep2.secondNotes.id,
    );
  };

  handleCancel = () => {
    this.props.hideMixSection();
    this.setState({
      currentView: '',
      isChecked: null,
      dataProduct: {
        firstNotes: null,
        secondNotes: null,
      },
    });
  };

  scentInfoBox = data => (data ? (
    <div className="scent-info">
      <div className="div-scent-profile animated faster fadeIn">
        <p>{data && data.name ? data.name : ''}</p>
        <div className="div-progress">
          <div className="div-process-line">
            {_.map(data && data.profile ? data.profile.accords : [], x => (
              <ProgressLine data={x} isShowPercent />
            ))}
          </div>
          <div className="div-process-circle">
            <ProgressCircle
              title={this.props.strengthBt}
              percent={
                  data
                    ? parseInt(parseFloat(data.profile.strength) * 100, 10)
                    : 0
                }
            />
            <ProgressCircle
              title={this.props.durationBt}
              percent={
                  data
                    ? parseInt(parseFloat(data.profile.duration) * 100, 10)
                    : 0
                }
            />
          </div>
        </div>
      </div>
    </div>
  ) : (
    ''
  ));

  onClickIngredient = (item) => {
    console.log('onClickIngredient 2', item);
    if (this.props.onClickIngredient) {
      this.props.onClickIngredient(item.ingredient);
      console.log('onClickIngredient', item);
    }
  }

  render() {
    const {
      dataScentNote,
      isDay,
      isNextView,
      dataStep2,
      hideMixSection,
      buttonBlocks,
      showMixSectionFrom,
    } = this.props;
    console.log('isDay', isDay);
    const {
      dataNote,
      titleProfile,
      name,
      profile,
      isChecked,
      data,
      isMainScent,
      currentView,
      dataProduct,
    } = this.state;
    let dataSecondaryCent = [];
    const oldProductChoosen = isMainScent && dataStep2 ? dataStep2.firstNotes : dataStep2.secondNotes;

    const dataMainScent = dataScentNote
      ? _.filter(dataScentNote, x => x.type == 'heart')[0]
      : '';
    switch (isDay) {
      case true:
        dataSecondaryCent = dataScentNote
          ? _.filter(dataScentNote, x => x.type == 'top')[0]
          : '';
        break;
      case false:
        dataSecondaryCent = dataScentNote
          ? _.filter(dataScentNote, x => x.type == 'bot')[0]
          : '';
        break;
    }

    const gifHeart = _.filter(dataScentNote, x => x.type == 'heart')[0].image;
    const heartNoteText = _.filter(
      buttonBlocks,
      x => x.value.name == 'heart_note_choose',
    )[0].value.text;
    const heartNoteDesText = _.filter(
      buttonBlocks,
      x => x.value.name == 'heart_note_cont',
    )[0].value.text;
    const secondaryScentText = _.filter(
      buttonBlocks,
      x => x.value.name == 'secondary_title',
    )[0].value.text;
    const mainScentText = _.filter(
      buttonBlocks,
      x => x.value.name == 'main_scent_title',
    )[0].value.text;

    const gifTop = _.filter(dataScentNote, x => x.type == 'top')[0].image;
    const freshNoteText = _.filter(
      buttonBlocks,
      x => x.value.name == 'fresh_title',
    )[0].value.text;
    const freshNoteDesText = _.filter(
      buttonBlocks,
      x => x.value.name == 'fresh_content',
    )[0].value.text;
    const cancelText = _.filter(
      buttonBlocks,
      x => x.value.name == 'cancel',
    )[0].value.text;
    const applyText = _.find(buttonBlocks, x => x.value.name == 'apply')
      ? _.find(buttonBlocks, x => x.value.name == 'apply').value.text
      : '';

    const gifBot = _.filter(dataScentNote, x => x.type == 'bot')[0].image;
    const sensualNoteText = _.filter(
      buttonBlocks,
      x => x.value.name == 'sensual_title',
    )[0].value.text;
    const sensualNoteDesText = _.filter(
      buttonBlocks,
      x => x.value.name == 'sensual_content',
    )[0].value.text;
    const gitShow = currentView == 'step2' ? gifHeart : isDay ? gifTop : gifBot;
    const switchView = () => {
      switch (this.state.currentView) {
        case 'main-notes':
          return (
            <div className="main-scent-note">
              <div
                className="main-scent-note-title"
                onClick={e => this.setState({ currentView: '' })}
              >
                <img src={icBack} alt="back" />
                <h3>{data.name}</h3>
              </div>
              <div className="items-main-scent">
                {_.map(data.tags, item => (
                  <div
                    style={{
                      backgroundImage: `url(${item.image ? item.image : ''})`,
                      cursor: 'pointer',
                    }}
                    className="item-main-scent animated faster fadeIn"
                    onClick={e => this.handleSelectNotes(item.name, data)}
                  >
                    <h1>{item.name}</h1>
                    <img
                      src={icNextWhite}
                      alt="Next"
                      style={{
                        position: 'absolute',
                        right: '2rem',
                        width: '12px',
                      }}
                    />
                  </div>
                ))}
              </div>
            </div>
          );
        case 'product':
          return (
            <div>
              <div className="product">
                <div
                  className="product-title"
                  onClick={e => this.setState({
                    currentView: 'main-notes',
                    isChecked: null,
                    dataProduct: null,
                  })
                  }
                >
                  <img src={icBack} alt="back" />
                  <h3>{dataNote.name}</h3>
                </div>
                <div className="product-note">
                  {_.map(dataNote.products, item => (
                    <div
                      className="item-note enable_hover animated faster fadeIn"
                      onClick={() => {
                        oldProductChoosen && item.id === oldProductChoosen.id
                          ? ''
                          : this.handleSelectProduct(
                            item.id,
                            item.family ? item.family : '',
                            item,
                            this.state.isMainScent,
                          );
                      }}
                    >
                      <div className="div-image">
                        <img loading="lazy" src={item.image ? item.image : ''} />
                      </div>
                      <button className="button-bg__none" type="button" onClick={(e) => { e.stopPropagation(); this.onClickIngredient(item); }}>
                        <img src={icInfo} alt="info" />
                      </button>
                      <div
                        className={`div-bg ${
                          oldProductChoosen && item.id === oldProductChoosen.id
                            ? 'active-old-note'
                            : ''
                        }`}
                        style={{
                          opacity: item.id === isChecked ? '0.7' : '0',
                          transition: item.id === isChecked ? '0.2' : '0',
                        }}
                      />
                      <img
                        className={`checked ${
                          oldProductChoosen && item.id === oldProductChoosen.id
                            ? 'visibility-old-note'
                            : ''
                        }`}
                        src={icChecked}
                        alt="checked"
                        style={{
                          visibility:
                            item.id === isChecked ? 'visible' : 'hidden',
                        }}
                      />
                      <span>{item.family && isBrowser ? item.family : ''}</span>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          );
        default:
          return (
            <div>
              <div
                className="header-title"
                onClick={() => this.props.hideMixSection()}
              >
                <img src={icBack} alt="back" />
                <h3>INGREDIENTS</h3>
              </div>
              <div className="d-flex">
                <div className="wrapper-scent-title">
                  <h4>{mainScentText}</h4>
                  <div
                    className="title-scent animated faster fadeInRight"
                    onClick={(e) => {
                      this.setState({
                        currentView: 'main-notes',
                        data: dataMainScent,
                        isMainScent: true,
                      });
                    }}
                    style={{ cursor: 'pointer' }}
                  >
                    <img
                      loading="lazy"
                      src={
                        dataStep2.firstNotes
                          ? dataStep2.firstNotes.image
                          : gifHeart
                      }
                    />
                    <div>
                      <h3>
                        {heartNoteText}
                        <img
                          src={icNext}
                          alt="Next"
                          style={{
                            height: 'auto',
                            margin: '0 0 5px 16px',
                            width: '10px',
                          }}
                        />
                      </h3>
                      <p>{heartNoteDesText}</p>
                    </div>
                  </div>
                </div>
                <div className="wrapper-scent-title">
                  <h4>{secondaryScentText}</h4>
                  <div
                    className="title-scent animated faster fadeInRight"
                    onClick={(e) => {
                      this.setState({
                        currentView: 'main-notes',
                        data: dataSecondaryCent,
                        isMainScent: false,
                      });
                    }}
                    style={{ cursor: 'pointer' }}
                  >
                    <img
                      loading="lazy"
                      src={
                        dataStep2.secondNotes
                          ? dataStep2.secondNotes.image
                          : isDay
                            ? gifTop
                            : gifBot
                      }
                    />
                    <div>
                      <h3>
                        {isDay ? freshNoteText : sensualNoteText}
                        <img
                          src={icNext}
                          alt="Next"
                          style={{
                            height: 'auto',
                            margin: '0 0 5px 16px',
                            width: '10px',
                          }}
                        />
                      </h3>
                      <p>{isDay ? freshNoteDesText : sensualNoteDesText}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
      }
    };

    return (
      <div className="h-100">
        <div className="scent-box-group">{switchView()}</div>
        {currentView == 'product' ? (
          (!dataStep2.firstNotes
            && (showMixSectionFrom == 'first' || isMainScent))
          || (!dataStep2.secondNotes
            && (showMixSectionFrom == 'second' || !isMainScent)) ? (
              <div
                className={`${
                  currentView == 'product' ? 'wrapper-scent-info' : ''
                }`}
              >
                {dataProduct && dataProduct.firstNotes
                  ? this.scentInfoBox(dataProduct.firstNotes)
                  : dataStep2.firstNotes
                    ? this.scentInfoBox(dataStep2.firstNotes)
                    : ''}
                {(dataProduct && dataProduct.firstNotes)
              || (dataProduct && dataProduct.secondNotes) ? (
                <div className="divider" />
                  ) : (
                    ''
                  )}
                {dataProduct && dataProduct.secondNotes
                  ? this.scentInfoBox(dataProduct.secondNotes)
                  : dataStep2.secondNotes
                    ? this.scentInfoBox(dataStep2.secondNotes)
                    : ''}
              </div>
            ) : dataStep2.firstNotes && isMainScent ? (
              <div
                className={`${
                  currentView == 'product' ? 'wrapper-scent-info' : ''
                }`}
              >
                {this.scentInfoBox(dataStep2.firstNotes)}
                {dataProduct && dataProduct.firstNotes ? (
                  <div className="divider" />
                ) : (
                  ''
                )}
                {dataProduct && dataProduct.firstNotes
                  ? this.scentInfoBox(dataProduct.firstNotes)
                  : ''}
              </div>
            ) : (
              <div
                className={`${
                  currentView == 'product' ? 'wrapper-scent-info' : ''
                }`}
              >
                {this.scentInfoBox(dataStep2.secondNotes)}
                {dataProduct && dataProduct.secondNotes ? (
                  <div className="divider" />
                ) : (
                  ''
                )}
                {dataProduct && dataProduct.secondNotes
                  ? this.scentInfoBox(dataProduct.secondNotes)
                  : ''}
              </div>
            )
        ) : (
          ''
        )}

        <div className="d-flex justify-content-center">
          <button className="cancel-pro" onClick={() => this.handleCancel()}>
            {cancelText}
          </button>
          <button
            className="apply-pro"
            onClick={() => this.handleSelectNotes3()}
            disabled={!dataProduct}
          >
            {applyText}
          </button>
        </div>
      </div>
    );
  }
}

export default scentBoxGroup;
