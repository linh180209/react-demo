import React, { useState, useEffect, useRef } from 'react';
import { Col, Row } from 'reactstrap';
import { connect } from 'react-redux';
import _ from 'lodash';
import classnames from 'classnames';
import ArrowBackIosNewIcon from '@mui/icons-material/ArrowBackIosNew';
import '../../../../styles/result-new.scss';
import PickIngredient from '../../../../components/ProductAllV2/ProductBlock/pickIngredient';
import { useMergeState } from '../../../../Utils/customHooks';
import loadingPage from '../../../../Redux/Actions/loading';
import icBack from '../../../../image/icon/ic-back.svg';
import icBackMobile from '../../../../image/icon/ic-prev-silder.svg';
import icSearch from '../../../../image/icon/ic-search-black.svg';
import icClear from '../../../../image/icon/ic-close-search.svg';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';
import './selectScent.scss';

const preDataGroup = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { tags } = x;
    _.forEach(tags, (tag) => {
      const group = _.find(dataScents, d => d.tag === tag.name);
      if (group) {
        group.datas.push(x);
      } else {
        dataScents.push({
          tag: tag.name,
          group: tag.group,
          datas: [x],
        });
      }
    });
  });
  return dataScents;
};

const preDataAlphabet = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { name } = x;
    const firstChart = name.charAt(0);
    const group = _.find(dataScents, d => d.tag.toLowerCase() === firstChart.toLowerCase());
    if (group) {
      group.datas.push(x);
    } else {
      dataScents.push({
        tag: firstChart,
        datas: [x],
      });
    }
  });
  return dataScents;
};

const preDataScents = (scents) => {
  console.log('preDataScents scents', scents);
  const dataScents = [];

  dataScents.push({
    datas: _.orderBy(preDataGroup(scents), ['tag'], ['asc']),
    title: ['Scent Family', 'Gender', 'mood', 'Scent Note'],
  });

  dataScents.push({
    datas: _.orderBy(preDataAlphabet(scents), ['tag'], ['asc']),
    title: ['Alphabet'],
  });

  dataScents.push({
    datas: [{
      tag: '',
      datas: _.orderBy(scents, ['popularity'], ['desc']),
    }],
    title: ['Popularity'],
  });
  console.log('dataScents', dataScents);
  return dataScents;
};

const addScentNoteIntoScents = (scents, scentNotes) => {
  const scentClones = _.cloneDeep(scents);
  _.forEach(scentNotes, (d) => {
    const { name, tags } = d;
    _.forEach(tags, (tag) => {
      const { products } = tag;
      _.forEach(products, (product) => {
        const ele = _.find(scentClones, x => x.id === product.id);
        if (ele) {
          _.assign(ele, product);
          ele.tags.push({
            name,
            group: 'Scent Note',
          });
        }
      });
    });
  });
  return scentClones;
};

const baseValueFilter = [
  {
    title: 'Family',
    value: 'Scent Family',
  },
  {
    title: 'Popularity',
    value: 'Popularity',
  },
  {
    title: 'Alphabet (A-Z)',
    value: 'Alphabet',
  },
  {
    title: 'Mood',
    value: 'Mood',
  },
];
function SelectScent(props) {
  const [dataScents, setDataScents] = useState(props.isNotPreData ? props.scentNotes : []);
  const [state, setState] = useMergeState({
    dataScentSelection: props.dataSelection || {
      subTitle: '1ST INGREDIENT',
    },
    isShowPopupFiltermobile: false,
    isShowSearch: false,
    dataSearch: [],
    dataNeedSearch: [],
    valueSearch: '',
  });

  const timeOutSearch = useRef();
  const refPickIngredient = useRef();
  const valueFilters = useRef(props.isNotPreData ? baseValueFilter : baseValueFilter.concat([
    {
      title: 'Top - Heart -  Bottom',
      value: 'Scent Note',
    }]));

  useEffect(() => {
    if (props.isNotPreData) {
      setDataScents(props.scentNotes);
    } else {
      const scents = addScentNoteIntoScents(props.scents, props.scentNotes);
      const data = preDataScents(scents);
      setDataScents(data);
    }
  }, [props.scents, props.scentNotes, props.isNotPreData]);

  const onChangeValueFilter = (value) => {
    console.log('refPickIngredient.current', refPickIngredient.current);
    refPickIngredient.current.current.onChangeValueSearchFilter({ value, type: 'Filter' });
    setState({ isShowPopupFiltermobile: false });
  };

  const onChangeFilter = () => {
    setState({ isShowPopupFiltermobile: true });
  };

  const shareRefFilterSearchScent = (ref) => {
    refPickIngredient.current = ref;
  };

  const filterData = (value) => {
    if (!value) {
      setState({ dataSearch: state.dataNeedSearch });
      return;
    }
    const dataSearch = _.filter(state.dataNeedSearch, x => x.name.toLowerCase().includes(value.toLowerCase()));
    setState({ dataSearch });
  };

  const onChange = (e) => {
    const { value } = e.target;
    setState({ valueSearch: value });
    if (timeOutSearch.current) {
      clearTimeout(timeOutSearch.current);
    }
    timeOutSearch.current = setTimeout(() => {
      filterData(value);
    }, 200);
  };

  const clickSelectSearch = (data) => {
    console.log(props.scentNotes);
    setState({ dataScentSelection: data, isShowSearch: false });
  };

  const openSearch = (data) => {
    console.log('openSearch', data);
    const dataNeedSearch = [];
    _.forEach(data, (d) => {
      _.forEach(d.datas, (x) => {
        if (!_.find(dataNeedSearch, i => i.id === x.id)) {
          dataNeedSearch.push(x);
        }
      });
    });
    setState({
      isShowSearch: true, dataSearch: dataNeedSearch, dataNeedSearch, valueSearch: '',
    });
  };

  const backBt = getNameFromButtonBlock(props.buttonBlocks, 'back');
  const theScentBt = getNameFromButtonBlock(props.buttonBlocks, 'scentbar_title');
  const chooseBt = getNameFromButtonBlock(props.buttonBlocks, 'Choose your ingredients');
  const pickBt = getNameFromButtonBlock(props.buttonBlocks, 'Pick a Ingredient');
  const st1Bt = getNameFromButtonBlock(props.buttonBlocks, '1ST INGREDIENT');
  const nd2Bt = getNameFromButtonBlock(props.buttonBlocks, '2ND INGREDIENT');
  const searchBt = getNameFromButtonBlock(props.buttonBlocks, 'Search Ingredient');
  if (props.selectItemId === 1) {
    state.dataScentSelection.subTitle = st1Bt;
  } else {
    state.dataScentSelection.subTitle = nd2Bt;
  }

  return (
    <div className={classnames('step3', props.isProductDetailV2 ? 'product-v2' : '')}>
      {
        isMobile ? (
          <div className="pick-scent-mobile">
            <div
              className={state.isShowPopupFiltermobile ? 'div-popup-filter-search-mobile animated faster fadeIn' : 'hidden'}
            >
              <div className="bg-close" onClick={() => setState({ isShowPopupFiltermobile: false })} />
              <div className={state.isShowPopupFiltermobile ? 'div-popup-filter' : 'hidden'}>
                {
                  _.map(valueFilters.current, d => (
                    <button
                      type="button"
                      onClick={() => {
                        onChangeValueFilter(d.value);
                      }}
                    >
                      {d.title}
                    </button>
                  ))
                }
              </div>
            </div>
            {
              props.isHeaderMobile && (
                <div className={classnames('header-mobile', props.isProductDetailV2 ? 'product-detail-v2' : '')}>
                  <button
                    className="button-bg__none"
                    type="button"
                    onClick={() => {
                      if (state.isShowSearch) {
                        setState({ isShowSearch: false });
                      } else {
                        props.onClickCancel();
                      }
                    }}
                  >
                    {
                      props.isProductDetailV2 ? (<ArrowBackIosNewIcon style={{ color: '#2c2c2c' }} />) : (
                        <img src={icBackMobile} alt="back" />
                      )
                    }
                  </button>
                  <h2>
                    {state.isShowSearch ? searchBt : pickBt}
                    {' '}
                    {props.indexSelect === 1 ? st1Bt : nd2Bt}
                  </h2>
                  {/* <span>
                    {props.indexSelect === 1 ? st1Bt : nd2Bt}
                  </span> */}
                </div>
              )
            }
            {
              state.isShowSearch ? (
                <div className={classnames('div-search-mobile', props.isProductDetailV2 ? 'product-detail-v2' : '')}>
                  <div className="div-input">
                    <img src={icSearch} alt="search" />
                    <input type="text" onChange={onChange} value={state.valueSearch} />
                    <button type="button" onClick={() => setState({ valueSearch: '', dataSearch: state.dataNeedSearch })}>
                      <img src={icClear} alt="clear" />
                    </button>
                  </div>
                  <div className="div-result">
                    {
                      _.map(state.dataSearch, d => (
                        <div className="item-result" onClick={() => clickSelectSearch(d)}>
                          <img loading="lazy" src={d.image} alt="scent" />
                          <span>
                            {d.name}
                          </span>
                        </div>
                      ))
                    }
                  </div>
                </div>
              ) : (
                <PickIngredient
                  isFreeStyle
                  isHidenHeader
                  // ref={refPickIngredient}
                  onClickCloseScent={props.onClickCancel}
                  dataSelection={state.dataScentSelection}
                  dataScents={dataScents}
                  onClickApply={props.onClickApply}
                  loadingPage={props.loadingPage}
                  onClickIngredient={props.onClickIngredient}
                  onClickIngredientProductId={props.onClickIngredient}
                  buttonBlocks={props.buttonBlocks}
                  onChangeFilter={onChangeFilter}
                  onChangeSearch={openSearch}
                  shareRefFilterSearchScent={shareRefFilterSearchScent}
                  isDualCandles={props.isDualCandles}
                  isShowHome={props.isShowHome}
                  scentRecommend={props.scentRecommend}
                  isShowIngredientDetail={props.isShowIngredientDetail}
                  isOnlyShowIngredientLocal={props.isOnlyShowIngredientLocal}
                  dataGtmtracking={props.selectItemId === 1 ? 'funnel-3-step-03-scent-1-added-to-mix' : 'funnel-3-step-05-scent-2-added-to-mix'}
                  dataGtmtrackingRecommend={props.selectItemId === 1 ? ' funnel-3-step-05-recommended-scent-1-added-to-mix' : ' funnel-3-step-05-recommended-scent-2-added-to-mix'}
                  isPickIngredientDetailV2={props.isProductDetailV2}
                  className="pick-ingredients-product-detail-v2"
                />
              )
            }
          </div>
        ) : (
          <React.Fragment>
            {
              props.isShowTitle && (
                <div className="div-title-header">
                  <h1 className="title">
                    <button
                      type="button"
                      className="btn-back"
                      onClick={props.onClickCancel}
                    >
                      <img src={icBack} alt="back" />
                      <span>{backBt}</span>
                    </button>
                    {theScentBt}
                  </h1>
                  <h4>{chooseBt}</h4>
                </div>
              )
            }

            <div className="custom-mix animated faster fadeInRight">
              <Row className="w-100 d-flex justify-content-between">
                <Col md="5" />
                <Col md="7" style={{ padding: '0 4rem 0 6rem' }}>
                  <PickIngredient
                    isFreeStyle
                    // ref={refPickIngredient}
                    onClickCloseScent={props.onClickCancel}
                    dataSelection={state.dataScentSelection}
                    dataScents={dataScents}
                    onClickApply={props.onClickApply}
                    loadingPage={props.loadingPage}
                    onClickIngredient={props.onClickIngredient}
                    // typeSelection={this.props.typeSelection}
                    buttonBlocks={props.buttonBlocks}
                    // onChangeFilter={this.props.onChangeFilter}
                    // onChangeSearch={this.props.onChangeSearch}
                    shareRefFilterSearchScent={() => {}}
                    isDualCandles={props.isDualCandles}
                    isShowHome={props.isShowHome}
                    dataGtmtracking={props.selectItemId === 1 ? 'funnel-3-step-03-scent-1-added-to-mix' : 'funnel-3-step-05-scent-2-added-to-mix'}
                  />
                </Col>
              </Row>
            </div>
          </React.Fragment>
        )
      }

    </div>
  );
}

function mapStateToProps(state) {
  return {
    scents: state.scents,
  };
}

const mapDispatchToProps = {
  loadingPage,
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectScent);
