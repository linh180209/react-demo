import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
// import clubBanner from '../../assets/club-banner.png';
import './styles.scss';
import Slider from 'react-slick';
// import { generaCurrency } from '../../../../Redux/Helpers';
import icClose from '../../../../image/icon/ic-close-mmo.svg';
import icTrash from '../../../../image/icon/ic-trash-white.svg';
import icSeductive from '../../../../image/icon/ic-seductive.svg';
import icSeductiveActive from '../../../../image/icon/ic-seductive-active.svg';
import icCheckedMmo from '../../../../image/icon/ic-checked-mmo.svg';
import icBack from '../../../../image/icon/ic-back.svg';
import icNextWhite from '../../../../image/icon/ic-next-white.svg';
import icNext from '../../../../image/icon/ic-next-mmo.svg';

class SelectedBoxMobile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowDeleteBox: false,
      nameDelete: '',
      type: null,
    };
  }

  //   this.props.deleteNotes(1);
  handleOpenDelete = (name, type) => {
    this.setState({
      isShowDeleteBox: true,
      nameDelete: name,
      type,
    });
  };

  handleCloseDelete = () => {
    this.setState({
      isShowDeleteBox: false,
    });
  };

  handelDeleteNote = () => {
    this.props.deleteNotes(this.state.type);
    this.props.switchView(this.state.type == 1 ? 'step2' : 'step2-1', true);
  };

  render() {
    const { dataStep2 } = this.props;
    const { isShowDeleteBox, nameDelete } = this.state;
    console.log('dataStep2', dataStep2);
    return (
      <div className="selected-mix">
        {!isShowDeleteBox ? (
          <div className="selected-box animated faster fadeIn">
            {dataStep2.firstNotes ? (
              <div className="selected-item">
                <div>
                  <h4>{dataStep2.firstNotes.ingredient.name}</h4>
                  <span>{dataStep2.firstNotes.family ? dataStep2.firstNotes.family : ''}</span>
                </div>
                <button
                  onClick={() => this.handleOpenDelete(dataStep2.firstNotes.ingredient.name, 1)
                  }
                >
                  <img src={icClose} alt="close" />
                </button>
              </div>
            ) : (
              ''
            )}
            {dataStep2.secondNotes ? (
              <div className="selected-item">
                <div>
                  <h4>{dataStep2.secondNotes.ingredient.name}</h4>
                  <span>{dataStep2.secondNotes.family ? dataStep2.secondNotes.family : ''}</span>
                </div>
                <button
                  onClick={() => this.handleOpenDelete(dataStep2.secondNotes.ingredient.name, 2)
                  }
                >
                  <img src={icClose} alt="close" />
                </button>
              </div>
            ) : (
              ''
            )}
          </div>
        ) : (
          ''
        )}
        {isShowDeleteBox ? (
          <div className="delete-box animated faster fadeIn">
            <button
              className="btn-close"
              onClick={() => this.handleCloseDelete()}
            >
              <img src={icClose} alt="close" />
            </button>
            <div>
              <h4>Delete</h4>
              <h4>{nameDelete}</h4>
            </div>
            <button onClick={() => this.handelDeleteNote()}>
              <img src={icTrash} alt="trash" />
            </button>
          </div>
        ) : (
          ''
        )}
      </div>
    );
  }
}

export default SelectedBoxMobile;
