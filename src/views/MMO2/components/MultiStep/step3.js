import React, { Component } from 'react';
import './styles.scss';
import {
  Row,
  Col,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
} from 'reactstrap';
import classnames from 'classnames';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import icBack from '../../../../image/icon/ic-redo.svg';
import icClose from '../../../../image/icon/ic-close-region.svg';
import icSearch from '../../../../image/icon/ic-search-black.svg';
import icSearchWhite from '../../../../image/icon/ic-search-white.svg';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../../Redux/Helpers/notification';
import ProgressLine from '../../../ResultScentV2/progressLine';
import ProgressCircle from '../../../ResultScentV2/progressCircle';
import {
  generaCurrency, getNameFromButtonBlock, getProductDualCrayonCombo, addFontCustom, segmentTrackScentBarQuizAddToBag, segmentTrackScentBarQuizCompleted
} from '../../../../Redux/Helpers';

import SelectedBoxMobile from './selectedBoxMobile';
import { GET_COMBO_URL, GET_PRICES_PRODUCTS } from '../../../../config';
import {
  addProductBasket,
  createBasketGuest,
} from '../../../../Redux/Actions/basket';
import addCmsRedux from '../../../../Redux/Actions/cms';
import ScentBox from './scentBox';
import icSearchW from '../../../../image/icon/icSearchProduct.svg';
import { isMobile } from '../../../../DetectScreen';
import SelectScent from './selectScent';

class Step3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataMixProduct: null,
      priceDualCrayons: undefined,
      // isShowModalDetail: false,
      // isShowMixSection: false,
    };
  }
  // toogleModalDetail = () => {
  //   this.setState({ isShowModalDetail: !this.state.isShowModalDetail });
  // };

  fetchCombo = (id1, id2) => {
    const option = {
      url: GET_COMBO_URL.replace('{id1}', id1).replace('{id2}', id2),
      method: 'GET',
    };
    const pending = [fetchClient(option, true)];
    Promise.all(pending)
      .then((result) => {
        if (result && result.length > 0) {
          this.setState({ dataMixProduct: result });
          segmentTrackScentBarQuizCompleted(result);
          this.props.handleFetchComboMix(result);
          if (this.props.dataProduct) {
            _.assign(this.props.dataProduct.items[0], result[0].items[0]);
          }
          return;
        }
        throw new Error(result.message);
      })
      .catch((err) => {
        toastrError(err.message);
      });
  };

  componentDidMount() {
    this.fetchCombo(
      this.props.dataStep2.firstNotes.id,
      this.props.dataStep2.secondNotes.id,
    );
    this.getPrice('dual_crayons').then((result) => {
      this.setState({ priceDualCrayons: result[0].price });
    });
  }

  componentDidUpdate(prevProps) {
    const { dataStep2, currentView } = this.props;
    if (
      isMobile
      && currentView == 'step3'
      && JSON.stringify(prevProps.dataStep2) !== JSON.stringify(dataStep2)
    ) {
      this.fetchCombo(
        this.props.dataStep2.firstNotes.id,
        this.props.dataStep2.secondNotes.id,
      );
    }
  }

  getPrice = (type) => {
    try {
      const option = {
        url: GET_PRICES_PRODUCTS.replace('{type}', type),
        method: 'GET',
      };
      return fetchClient(option);
    } catch (error) {
      toastrError('error', error.message);
    }
  }

  onClickAddToCard = async () => {
    const { dataMixProduct } = this.state;
    console.log('dataMixProduct', dataMixProduct);
    const { isShowRollOn } = this.props;
    const item = dataMixProduct ? dataMixProduct[0] : '';
    const { combo } = dataMixProduct[0];
    const scents = _.filter(combo, x => x.product.type === 'Scent');
    let itemSelect;
    if (isShowRollOn) {
      const productFinal = await getProductDualCrayonCombo(scents[0]?.product?.id, scents[1]?.product?.id);
      itemSelect = productFinal?.items[0];
      _.assign(itemSelect, { name: productFinal?.name });
    } else {
      itemSelect = item.items[0];
    }
    const { basket } = this.props;
    const { dataCustom } = this.props;
    const dataCT = isShowRollOn ? {} : dataCustom;
    const bottleImage = dataCT?.currentImg
      ? await fetch(dataCT?.currentImg).then(r => r.blob())
      : undefined;

    const name = dataCT?.nameBottle
      ? dataCT?.nameBottle
      : itemSelect?.name;
    const data = {
      item: itemSelect.id,
      is_featured: itemSelect.is_featured,
      is_black: dataCT?.isBlack,
      color: dataCT?.color,
      font: dataCT?.font,
      price: itemSelect?.price,
      imagePremade: dataCT?.imagePremade,
      is_display_name:
        (!bottleImage && !!dataCT?.nameBottle) || !!dataCT?.nameBottle,
      is_customized: !!bottleImage || !!dataCT?.imagePremade,
      file: bottleImage ? new File([bottleImage], 'product.png') : undefined,
      quantity: 1,
      name,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    // segmentTrackScentBarQuizAddToBag(dataTemp);
    if (!basket.id) {
      this.props.createBasketGuest(dataTemp);
    } else {
      this.props.addProductBasket(dataTemp);
    }
  };

  onClickApply = (data) => {
    console.log(' this.props.showMixSectionFrom', this.props.showMixSectionFrom);
    this.props.hideMixSection();
    if (!this.props.dataStep2.secondNotes || this.props.showMixSectionFrom === 'second') {
      this.props.dataStep2.secondNotes = data;
      this.props.selectNotesStep3(
        this.props.dataStep2,
        false,
      );
      this.fetchCombo(this.props.dataStep2.firstNotes.id, data.id);
    } else {
      this.props.dataStep2.firstNotes = data;
      this.props.selectNotesStep3(
        this.props.dataStep2,
        false,
      );
      this.fetchCombo(data.id, this.props.dataStep2.secondNotes.id);
    }
  }

  onClickCancel = () => {
    this.props.hideMixSection();
  }

  render() {
    const {
      isNextView,
      dataStep2,
      isDay,
      openCustomeBottle,
      dataScentNote,
      selectNotesStep3,
      dataCustom,
      isShowModalDetail,
      deleteNotes,
      currentView,
      switchView,
      buttonBlocks,
      isShowMixSection,
      showMixSection,
      hideMixSection,
      isDeleted,
      showMixSectionFrom,
      strengthBt,
      durationBt,
      isShowRollOn,
    } = this.props;
    const { dataMixProduct, priceDualCrayons } = this.state;
    const redoText = _.filter(buttonBlocks, x => x.value.name == 'redo')[0]
      .value.text;
    const yourCustomMixText = _.filter(
      buttonBlocks,
      x => x.value.name === 'your_custom_mix',
    )[0].value.text;
    const mixReadyText = _.filter(
      buttonBlocks,
      x => x.value.name === 'mix_ready',
    )[0].value.text;
    const clickToViewMoreDetailText = _.filter(
      buttonBlocks,
      x => x.value.name === 'more_detail',
    )[0].value.text;
    const addToBagText = _.filter(
      buttonBlocks,
      x => x.value.name === 'add_to_bag',
    )[0].value.text;
    const customizeBottleText = _.filter(
      buttonBlocks,
      x => x.value.name === 'customize_bottle',
    )[0].value.text;
    const freshNoteText = _.filter(
      buttonBlocks,
      x => x.value.name === 'fresh_title',
    )[0].value.text;
    const sensualNoteText = _.filter(
      buttonBlocks,
      x => x.value.name === 'sensual_title',
    )[0].value.text;
    const heartNoteText = _.filter(
      buttonBlocks,
      x => x.value.name === 'heart_note_choose',
    )[0].value.text;
    const comboScentText = _.filter(
      buttonBlocks,
      x => x.value.name === 'combo_title',
    )[0].value.text;
    const closeText = _.filter(
      buttonBlocks,
      x => x.value.name === 'close_btn',
    )[0].value.text;
    const customizeText = _.filter(
      buttonBlocks,
      x => x.value.name === 'customize',
    )[0].value.text;
    const viewMoreInfoText = _.find(
      buttonBlocks,
      x => x.value.name === 'view_more_info',
    )
      ? _.find(buttonBlocks, x => x.value.name === 'view_more_info').value.text
      : '';
    const buyForBt = getNameFromButtonBlock(buttonBlocks, 'Buy perfume for');
    const tryRolOnBt = getNameFromButtonBlock(buttonBlocks, 'Try Roll-on for');
    const htmlWeb = (
      <div
        className={`step3 animated faster ${
          isNextView ? 'fadeInLeft' : 'fadeInRight'
        }`}
      >
        <div className="div-title-header">
          <h1 className="title">
            <button
              className="btn-back"
              onClick={() => this.props.toggleModalRedo()}
              // disabled={!dataStep2.firstNotes || !dataStep2.secondNotes}
            >
              <img src={icBack} alt="back" />
              <span>{redoText}</span>
            </button>
            {yourCustomMixText}
          </h1>
        </div>
        {!isShowMixSection
        && !(!dataStep2.firstNotes && !dataStep2.secondNotes) ? (
          <div className="custom-mix animated faster fadeInRight">
            <Row className="w-100 d-flex justify-content-between">
              <Col md="5" />
              <Col md="7" style={{ padding: '0 4rem 0 6rem' }}>
                <div className="title-mix">
                  <h3>
                    {dataStep2.firstNotes && dataStep2.secondNotes
                      ? dataMixProduct
                        ? isDay
                          ? `${dataStep2.secondNotes.ingredient.name} - ${dataStep2.firstNotes.ingredient.name}`
                          : `${dataStep2.firstNotes.ingredient.name} - ${dataStep2.secondNotes.ingredient.name}`
                        : ''
                      : dataStep2.firstNotes
                        ? dataStep2.firstNotes.ingredient.name
                        : dataStep2.secondNotes.ingredient.name}
                  </h3>
                  {dataStep2.firstNotes && dataStep2.secondNotes ? (
                    <h3>
                      {generaCurrency(
                        dataMixProduct ? isShowRollOn ? priceDualCrayons : dataMixProduct[0].items[0].price : '',
                      )}
                    </h3>
                  ) : (
                    ''
                  )}
                </div>
                {
                  dataStep2.firstNotes && dataStep2.secondNotes && (
                    <div className="switch-roll-on">
                      <button
                        onClick={this.props.onSwitchRollOn}
                        type="button"
                        className="button-bg__none"
                      >
                        {isShowRollOn ? `${buyForBt} ${generaCurrency(dataMixProduct ? dataMixProduct[0].items[0].price : '')}` : `${tryRolOnBt} ${generaCurrency(priceDualCrayons)}`}
                      </button>
                    </div>
                  )
                }
                {dataStep2.firstNotes && dataStep2.secondNotes ? (
                  <Row style={{ margin: '0.5rem 0 0 0' }} className="step3">
                    <Col md="3" className="pl-0">
                      <div className="scent-mix-box-mmo">
                        {isDay === true ? (
                          <div className="wrapper-scent-mix">
                            <img
                              alt="note"
                              style={{ objectFit: 'cover' }}
                              src={
                                dataStep2.secondNotes
                                  ? dataStep2.secondNotes.ingredient.bot_banner
                                  : ''
                              }
                            />
                            <img
                              alt="note"
                              style={{ objectFit: 'cover' }}
                              src={
                                dataStep2.firstNotes
                                  ? dataStep2.firstNotes.ingredient.bot_banner
                                  : ''
                              }
                            />
                          </div>
                        ) : (
                          <div className="wrapper-scent-mix">
                            <img
                              alt="note"
                              style={{ objectFit: 'cover' }}
                              src={
                                dataStep2.firstNotes
                                  ? dataStep2.firstNotes.ingredient.bot_banner
                                  : ''
                              }
                            />
                            <img
                              alt="note"
                              style={{ objectFit: 'cover' }}
                              src={
                                dataStep2.secondNotes
                                  ? dataStep2.secondNotes.ingredient.bot_banner
                                  : ''
                              }
                            />
                          </div>
                        )}
                        <span>{comboScentText}</span>
                      </div>
                    </Col>
                    <Col md="9">
                      <div
                        className="info-scent-mix"
                      >
                        <p className="title-info">
                          {/* {heartNoteText} |{" "} */}
                          {isDay
                          && dataStep2.firstNotes
                          && dataStep2.secondNotes
                            ? `${freshNoteText} | ${heartNoteText}`
                            : `${heartNoteText} | ${sensualNoteText}`}
                        </p>
                        <div className="content-info">
                          <div>
                            {!isDay
                              ? dataStep2.firstNotes.short_description
                                + dataStep2.secondNotes.short_description
                              : dataStep2.secondNotes.short_description
                                + dataStep2.firstNotes.short_description}
                          </div>
                          {/* <img src={icNext} alt="next" /> */}
                        </div>
                        <p
                          className="detail-info"
                          onClick={(e) => {
                            e.stopPropagation();
                            this.props.toogleModalDetail();
                          }}
                        >
                          <img src={icSearch} alt="Search" />
                          {' '}
                          <span className="detail-click">
                            {clickToViewMoreDetailText}
                          </span>
                        </p>
                      </div>
                    </Col>
                  </Row>
                ) : (
                  <Row style={{ margin: '2rem 0' }} className="step3">
                    <Col md="3">
                      <div className="scent-mix-box-mmo">
                        <div className="wrapper-scent-mix">
                          <img
                            alt="note"
                            style={{ width: '100%', objectFit: 'cover' }}
                            src={
                              dataStep2.firstNotes || dataStep2.secondNotes
                                ? dataStep2.firstNotes
                                  ? dataStep2.firstNotes.image
                                  : dataStep2.secondNotes.image
                                : ''
                            }
                          />
                        </div>
                        {/* <span>
                          {dataStep2.firstNotes || dataStep2.secondNotes
                            ? dataStep2.firstNotes
                              ? dataStep2.firstNotes.name
                              : dataStep2.secondNotes.name
                            : ""}
                        </span> */}
                      </div>
                    </Col>
                    <Col md="9">
                      <div
                        className="info-scent-mix"
                        // onClick={() => this.props.showMixSection()}
                      >
                        <p className="title-info">
                          {isDay
                            ? dataStep2.firstNotes
                              ? heartNoteText
                              : freshNoteText
                            : dataStep2.firstNotes
                              ? heartNoteText
                              : sensualNoteText}
                        </p>
                        <div className="content-info">
                          <div>
                            {dataStep2.firstNotes || dataStep2.secondNotes
                              ? dataStep2.firstNotes
                                ? dataStep2.firstNotes.short_description
                                : dataStep2.secondNotes.short_description
                              : ''}
                          </div>
                          {/* <img src={icNext} alt="next" /> */}
                        </div>
                        <p
                          className="detail-info"
                          onClick={(e) => {
                            e.stopPropagation();
                            this.props.toogleModalDetail();
                          }}
                        >
                          <img src={icSearch} alt="Search" />
                          {' '}
                          <span className="detail-click">
                            {clickToViewMoreDetailText}
                          </span>
                        </p>
                      </div>
                    </Col>
                  </Row>
                )}

                <div className="div-progress">
                  <div className="div-process-line">
                    {dataStep2.firstNotes && dataStep2.secondNotes
                      ? _.map(
                        dataMixProduct && dataMixProduct[0]
                          ? dataMixProduct[0].profile.accords
                          : [],
                        (x, i) => (
                          <ProgressLine data={x} isShowPercent key={i} />
                        ),
                      )
                      : dataStep2.firstNotes
                        ? _.map(dataStep2.firstNotes.profile.accords, (x, i) => (
                          <ProgressLine data={x} isShowPercent key={i} />
                        ))
                        : _.map(dataStep2.secondNotes.profile.accords, (x, i) => (
                          <ProgressLine data={x} isShowPercent key={i} />
                        ))}
                  </div>
                  <div className="div-process-circle">
                    <ProgressCircle
                      title={strengthBt}
                      percent={
                        dataStep2.firstNotes && dataStep2.secondNotes
                          ? dataMixProduct && dataMixProduct[0]
                            ? parseInt(
                              parseFloat(dataMixProduct[0].profile.strength)
                                  * 100,
                              10,
                            )
                            : 0
                          : dataStep2.firstNotes
                            ? parseInt(
                              parseFloat(
                                dataStep2.firstNotes.profile.strength,
                              ) * 100,
                              10,
                            )
                            : parseInt(
                              parseFloat(
                                dataStep2.secondNotes.profile.strength,
                              ) * 100,
                              10,
                            )
                      }
                    />
                    <ProgressCircle
                      title={durationBt}
                      percent={
                        dataStep2.firstNotes && dataStep2.secondNotes
                          ? dataMixProduct && dataMixProduct[0]
                            ? parseInt(
                              parseFloat(dataMixProduct[0].profile.duration)
                                  * 100,
                              10,
                            )
                            : 0
                          : dataStep2.firstNotes
                            ? parseInt(
                              parseFloat(
                                dataStep2.firstNotes.profile.duration,
                              ) * 100,
                              10,
                            )
                            : parseInt(
                              parseFloat(
                                dataStep2.secondNotes.profile.duration,
                              ) * 100,
                              10,
                            )
                      }
                    />
                  </div>
                </div>
                <button
                  type="button"
                  className="btn-add"
                  onClick={() => {
                    this.state.dataMixProduct
                      ? this.onClickAddToCard(
                        this.state.dataMixProduct[0],
                        this.state.dataMixProduct[0].name,
                      )
                      : '';
                  }}
                  disabled={!dataStep2.firstNotes || !dataStep2.secondNotes}
                >
                  {addToBagText}
                </button>
                <button
                  data-gtmtracking="funnel-3-step-06-1-customize-bottle"
                  type="button"
                  className="btn-custom"
                  onClick={() => openCustomeBottle()}
                  disabled={!dataStep2.firstNotes || !dataStep2.secondNotes}
                >
                  {customizeBottleText}
                </button>
              </Col>
            </Row>
          </div>
          ) : null}

        {isShowMixSection
        || (!dataStep2.firstNotes && !dataStep2.secondNotes) ? (
          <div className="custom-mix-change">
            <Row className="w-100 d-flex justify-content-between">
              <SelectScent
                scentNotes={this.props.resultScentNotes}
                buttonBlocks={this.props.buttonBlocks}
                onClickApply={this.onClickApply}
                onClickCancel={this.onClickCancel}
                onClickIngredient={this.props.onClickIngredient}
                indexSelect={this.props.showMixSectionFrom === 'first' ? 1 : 2}
              />
              {/* <Col md="5" />
              <Col md="7" style={{ padding: '0 3rem 0 3rem' }}>
                <div className="custom-mix-box">
                  <ScentBoxGroup
                    dataScentNote={dataScentNote}
                    isDay={isDay}
                    selectNotesStep3={selectNotesStep3}
                    dataStep2={dataStep2}
                    hideMixSection={this.props.hideMixSection}
                    fetchCombo={this.fetchCombo}
                    buttonBlocks={buttonBlocks}
                    showMixSectionFrom={showMixSectionFrom}
                    showMixSection={this.showMixSection}
                    strengthBt={strengthBt}
                    durationBt={durationBt}
                    onClickIngredient={this.props.onClickIngredient}
                  />
                </div>
              </Col> */}
            </Row>
          </div>
          ) : null}

        <Modal
          isOpen={isShowModalDetail}
          // toggle={this.toogleModalDetail}
          className={classnames('modal-detail-mix', addFontCustom())}
          size="lg"
          centered
        >
          <img
            className="bt-close"
            src={icClose}
            alt="bt-close"
            onClick={this.props.toogleModalDetail}
          />
          <ModalBody className="body-modal-detail">
            <div className="wrapper-modal-detail">
              {dataStep2.firstNotes && dataStep2.secondNotes ? (
                <div className="scent-mix-box-mmo">
                  {isDay == true ? (
                    <div className="wrapper-scent-mix">
                      <img
                        style={{ objectFit: 'cover' }}
                        src={
                          dataStep2.secondNotes
                            ? dataStep2.secondNotes.ingredient.bot_banner
                            : ''
                        }
                      />
                      <img
                        style={{ objectFit: 'cover' }}
                        src={
                          dataStep2.firstNotes
                            ? dataStep2.firstNotes.ingredient.bot_banner
                            : ''
                        }
                      />
                    </div>
                  ) : (
                    <div className="wrapper-scent-mix">
                      <img
                        style={{ objectFit: 'cover' }}
                        src={
                          dataStep2.firstNotes
                            ? dataStep2.firstNotes.ingredient.bot_banner
                            : ''
                        }
                      />
                      <img
                        style={{ objectFit: 'cover' }}
                        src={
                          dataStep2.secondNotes
                            ? dataStep2.secondNotes.ingredient.bot_banner
                            : ''
                        }
                      />
                    </div>
                  )}
                  <span>{comboScentText}</span>
                </div>
              ) : (
                <div className="scent-mix-box-mmo">
                  <div className="wrapper-scent-mix">
                    <img
                      style={{ width: '100%', objectFit: 'cover' }}
                      src={
                        dataStep2.firstNotes || dataStep2.secondNotes
                          ? dataStep2.firstNotes
                            ? dataStep2.firstNotes.image
                            : dataStep2.secondNotes.image
                          : ''
                      }
                    />
                  </div>
                </div>
              )}
              <h3>
                {dataStep2.firstNotes && dataStep2.secondNotes
                  ? dataMixProduct
                    ? isDay
                      ? `${dataStep2.secondNotes.ingredient.name} - ${dataStep2.firstNotes.ingredient.name}`
                      : `${dataStep2.firstNotes.ingredient.name} - ${dataStep2.secondNotes.ingredient.name}`
                    : ''
                  : dataStep2.firstNotes
                    ? dataStep2.firstNotes.ingredient.name
                    : dataStep2.secondNotes.ingredient.name}
              </h3>

              <p style={{ fontSize: '16px', color: '#8d8d8d' }}>
                {dataStep2.firstNotes && dataStep2.secondNotes
                  ? isDay
                    ? `${freshNoteText} | ${heartNoteText}`
                    : `${heartNoteText} | ${sensualNoteText}`
                  : isDay
                    ? dataStep2.firstNotes
                      ? heartNoteText
                      : freshNoteText
                    : dataStep2.firstNotes
                      ? heartNoteText
                      : sensualNoteText}
              </p>
              <div className="content-modal-detail">
                {dataStep2.firstNotes && dataStep2.secondNotes ? (
                  !isDay ? (
                    <div>
                      <p>{dataStep2.firstNotes.short_description}</p>
                      <p>{dataStep2.secondNotes.short_description}</p>
                    </div>
                  ) : (
                    <div>
                      <p>{dataStep2.secondNotes.short_description}</p>
                      <p>{dataStep2.firstNotes.short_description}</p>
                    </div>
                  )
                ) : dataStep2.firstNotes ? (
                  <div>{dataStep2.firstNotes.short_description}</div>
                ) : (
                  <div>{dataStep2.secondNotes.short_description}</div>
                )}
              </div>
              <ModalFooter>
                <Button
                  color="secondary"
                  onClick={this.props.toogleModalDetail}
                >
                  {closeText}
                </Button>
              </ModalFooter>
            </div>
          </ModalBody>
        </Modal>
      </div>
    );
    const htmlMobile = (
      <div
        className={`step3 animated faster step3-mobile ${
          isNextView ? 'fadeIn' : 'fadeIn'
        }`}
      >
        <div className={this.props.isNonHeader ? 'hidden' : 'div-title-header'}>
          <div className="title">
            <div className="btn-header-mobile">
              <button
                className="btn-back"
                onClick={() => this.props.toggleModalRedo()}
                // disabled={!dataStep2.firstNotes || !dataStep2.secondNotes}
              >
                <img src={icBack} alt="back" />
                <span style={{ fontWeight: '500' }}>{redoText}</span>
              </button>
              <button
                className="detail-info"
                onClick={(e) => {
                  e.stopPropagation();
                  this.props.toogleModalDetail();
                }}
              >
                <img src={icSearchWhite} alt="Search" />
                {' '}
                <span
                  style={{
                    fontWeight: '500',
                    fontSize: '11px',
                    textTransform: 'uppercase',
                    letterSpacing: ' 0.2em',
                  }}
                >
                  {viewMoreInfoText}
                </span>
              </button>
            </div>

            <div className="mix-ready">
              <h2>{yourCustomMixText}</h2>
              <span>
                {generaCurrency(
                  dataMixProduct ? isShowRollOn ? priceDualCrayons : dataMixProduct[0].items[0].price : '',
                )}
              </span>
            </div>
            {
              dataStep2.firstNotes && dataStep2.secondNotes && (
                <div className="switch-roll-on">
                  <button
                    onClick={this.props.onSwitchRollOn}
                    type="button"
                    className="button-bg__none"
                  >
                    {isShowRollOn ? `${buyForBt} ${generaCurrency(dataMixProduct ? dataMixProduct[0].items[0].price : '')}` : `${tryRolOnBt} ${generaCurrency(priceDualCrayons)}`}
                  </button>
                </div>
              )
            }
          </div>
        </div>
        <div className="wrapper-mix-info">
          <div className="div-progress" style={{ width: '60%' }}>
            <SelectedBoxMobile
              dataStep2={dataStep2}
              deleteNotes={deleteNotes}
              switchView={switchView}
              isDay={isDay}
            />
            <div className="div-process-circle">
              <ProgressCircle
                title={strengthBt}
                percent={
                  dataStep2.firstNotes && dataStep2.secondNotes
                    ? dataMixProduct && dataMixProduct[0]
                      ? parseInt(
                        parseFloat(dataMixProduct[0].profile.strength) * 100,
                        10,
                      )
                      : 0
                    : dataStep2.firstNotes
                      ? parseInt(
                        parseFloat(dataStep2.firstNotes.profile.strength) * 100,
                        10,
                      )
                      : parseInt(
                        parseFloat(dataStep2.secondNotes.profile.strength)
                          * 100,
                        10,
                      )
                }
              />
              <ProgressCircle
                title={durationBt}
                percent={
                  dataStep2.firstNotes && dataStep2.secondNotes
                    ? dataMixProduct && dataMixProduct[0]
                      ? parseInt(
                        parseFloat(dataMixProduct[0].profile.duration) * 100,
                        10,
                      )
                      : 0
                    : dataStep2.firstNotes
                      ? parseInt(
                        parseFloat(dataStep2.firstNotes.profile.duration) * 100,
                        10,
                      )
                      : parseInt(
                        parseFloat(dataStep2.secondNotes.profile.duration)
                          * 100,
                        10,
                      )
                }
              />
            </div>
            <div className="div-process-line">
              {dataStep2.firstNotes && dataStep2.secondNotes
                ? _.map(
                  dataMixProduct && dataMixProduct[0]
                    ? dataMixProduct[0].profile.accords
                    : [],
                  (x, i) => <ProgressLine data={x} isShowPercent key={i} />,
                )
                : dataStep2.firstNotes
                  ? _.map(dataStep2.firstNotes.profile.accords, (x, i) => (
                    <ProgressLine data={x} isShowPercent key={i} />
                  ))
                  : _.map(dataStep2.secondNotes.profile.accords, (x, i) => (
                    <ProgressLine data={x} isShowPercent key={i} />
                  ))}
            </div>
          </div>
        </div>
        <button
          type="button"
          className={
            isMobile && this.props.isFullFressSensual
              ? 'btn-toggle-scent-bar'
              : 'hidden'
          }
          onClick={() => this.props.toogleModalDetail()}
        >
          <img src={icSearchW} alt="search" className="mr-2" />
          view more info
        </button>
        <div className={this.props.isNonHeader ? 'hidden' : 'wrapper-button'}>
          <button
            type="button"
            className="btn-add"
            onClick={() => {
              this.state.dataMixProduct
                ? this.onClickAddToCard(
                  this.state.dataMixProduct[0],
                  this.state.dataMixProduct[0].name,
                )
                : '';
            }}
            disabled={!dataStep2.firstNotes || !dataStep2.secondNotes}
          >
            {addToBagText}
          </button>
          <button
            data-gtmtracking="funnel-3-step-06-1-customize-bottle"
            type="button"
            className="btn-custom"
            onClick={() => openCustomeBottle()}
            disabled={!dataStep2.firstNotes || !dataStep2.secondNotes}
          >
            {customizeText}
          </button>
        </div>

        {/* {isShowMixSection ||
        (!dataStep2.firstNotes && !dataStep2.secondNotes) ? (
          <div className="custom-mix-change">
            <Row className="w-100 d-flex justify-content-between">
              <Col md="5"></Col>
              <Col md="7" style={{ padding: "0 3rem 0 3rem" }}>
                <div className="custom-mix-box">
                  <ScentBoxGroup
                    dataScentNote={dataScentNote}
                    isDay={isDay}
                    selectNotesStep3={selectNotesStep3}
                    dataStep2={dataStep2}
                    hideMixSection={this.props.hideMixSection}
                    fetchCombo={this.fetchCombo}
                    buttonBlocks={buttonBlocks}
                    showMixSectionFrom = {showMixSectionFrom}
                  />
                </div>
              </Col>
            </Row>
          </div>
        ) : null} */}
        {
          this.props.isShowOnlyScentNotes ? (
            <Modal
              isOpen={this.props.isOpenScentBoxMobile}
              toggle={this.props.toggleScentBoxMobile}
              className={`modal-scent-mobile animated faster ${
                this.props.delaySlideModalMobile ? 'slideInUp' : 'slideOutDown'
              } ${addFontCustom()}`}
              fade={false}
            >
              <ScentBox
                dataScentNote={dataScentNote}
                isDay={isDay}
                data={
                  this.props.showMixSectionFrom == 'first'
                    ? _.filter(
                      this.props.dataScentNote,
                      x => x.type == 'heart',
                    )[0]
                    : this.props.isDay
                      ? _.filter(this.props.dataScentNote, x => x.type == 'top')[0]
                      : _.filter(this.props.dataScentNote, x => x.type == 'bot')[0]
                }
                switchView={switchView}
                selectNotesStep2={this.props.selectNotesStep2}
                isNextView={isNextView}
                toggleScentBoxMobile={this.props.toggleScentBoxMobile}
                // handleSelectNoteMobile={this.handleSelectNoteMobile}
                showMixSectionFrom={showMixSectionFrom}
                currentView={currentView}
                dataStep2={dataStep2}
                buttonBlocks={buttonBlocks}
                isDeleted={isDeleted}
              />
            </Modal>
          ) : (
            <Modal
              isOpen={this.props.isOpenScentBoxMobile}
              toggle={this.props.toggleScentBoxMobile}
              className={`modal-scent-mobile-v2 ${addFontCustom()}`}
            >
              <SelectScent
                scentNotes={this.props.resultScentNotes}
                buttonBlocks={this.props.buttonBlocks}
                onClickApply={this.onClickApply}
                onClickCancel={this.props.toggleScentBoxMobile}
                onClickIngredient={this.props.onClickIngredient}
                isHeaderMobile
                mmos={this.props.mmos}
                indexSelect={this.props.showMixSectionFrom === 'first' ? 1 : 2}
              />
            </Modal>
          )
        }

        {/* <Modal
          isOpen={this.props.isOpenScentBoxMobile}
          toggle={this.props.toggleScentBoxMobile}
          className={`modal-scent-mobile animated faster ${
            this.props.delaySlideModalMobile ? 'slideInUp' : 'slideOutDown'
          }`}
          fade={false}
        >
          <ScentBox
            dataScentNote={dataScentNote}
            isDay={isDay}
            data={
              this.props.showMixSectionFrom == 'first'
                ? _.filter(
                  this.props.dataScentNote,
                  x => x.type == 'heart',
                )[0]
                : this.props.isDay
                  ? _.filter(this.props.dataScentNote, x => x.type == 'top')[0]
                  : _.filter(this.props.dataScentNote, x => x.type == 'bot')[0]
            }
            switchView={switchView}
            selectNotesStep2={this.props.selectNotesStep2}
            isNextView={isNextView}
            toggleScentBoxMobile={this.props.toggleScentBoxMobile}
            // handleSelectNoteMobile={this.handleSelectNoteMobile}
            showMixSectionFrom={showMixSectionFrom}
            currentView={currentView}
            dataStep2={dataStep2}
            buttonBlocks={buttonBlocks}
            isDeleted={isDeleted}
          />
        </Modal> */}
        <Modal
          isOpen={isShowModalDetail}
          // toggle={this.toogleModalDetail}
          className={`modal-detail-mix modal-detail-mobile-mix ${addFontCustom()}`}
          size="lg"
          centered
        >
          <img
            className="bt-close"
            src={icClose}
            alt="bt-close"
            onClick={this.props.toogleModalDetail}
          />
          <ModalBody className="body-modal-detail">
            {dataStep2.firstNotes && dataStep2.secondNotes ? (
              <div className="scent-mix-box-mmo">
                {isDay == true ? (
                  <div className="wrapper-scent-mix">
                    <img
                      style={{ objectFit: 'cover' }}
                      src={
                        dataStep2.secondNotes
                          ? dataStep2.secondNotes.ingredient.bot_banner
                          : ''
                      }
                    />
                    <img
                      style={{ objectFit: 'cover' }}
                      src={
                        dataStep2.firstNotes
                          ? dataStep2.firstNotes.ingredient.bot_banner
                          : ''
                      }
                    />
                  </div>
                ) : (
                  <div className="wrapper-scent-mix">
                    <img
                      style={{ objectFit: 'cover' }}
                      src={
                        dataStep2.firstNotes
                          ? dataStep2.firstNotes.ingredient.bot_banner
                          : ''
                      }
                    />
                    <img
                      style={{ objectFit: 'cover' }}
                      src={
                        dataStep2.secondNotes
                          ? dataStep2.secondNotes.ingredient.bot_banner
                          : ''
                      }
                    />
                  </div>
                )}
                <span>{comboScentText}</span>
              </div>
            ) : (
              <div className="scent-mix-box-mmo">
                <div className="wrapper-scent-mix">
                  <img
                    style={{ width: '100%', objectFit: 'cover' }}
                    src={
                      dataStep2.firstNotes || dataStep2.secondNotes
                        ? dataStep2.firstNotes
                          ? dataStep2.firstNotes.image
                          : dataStep2.secondNotes.image
                        : ''
                    }
                  />
                </div>
              </div>
            )}
            <h3>
              {dataStep2.firstNotes && dataStep2.secondNotes
                ? dataMixProduct
                  ? isDay
                    ? `${dataStep2.secondNotes.name} - ${dataStep2.firstNotes.name}`
                    : `${dataStep2.firstNotes.name} - ${dataStep2.secondNotes.name}`
                  : ''
                : dataStep2.firstNotes
                  ? dataStep2.firstNotes.name
                  : dataStep2.secondNotes.name}
            </h3>

            <p style={{ fontSize: '16px', color: '#8d8d8d' }}>
              {isDay
                ? `${freshNoteText} | ${heartNoteText}`
                : `${heartNoteText} | ${sensualNoteText}`}
            </p>
            <div className="content-modal-detail">
              {dataStep2.firstNotes && dataStep2.secondNotes ? (
                !isDay ? (
                  <div>
                    <p>{dataStep2.firstNotes.short_description}</p>
                    <p>{dataStep2.secondNotes.short_description}</p>
                  </div>
                ) : (
                  <div>
                    <p>{dataStep2.secondNotes.short_description}</p>
                    <p>{dataStep2.firstNotes.short_description}</p>
                  </div>
                )
              ) : dataStep2.firstNotes ? (
                <div>{dataStep2.firstNotes.short_description}</div>
              ) : (
                <div>{dataStep2.secondNotes.short_description}</div>
              )}
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.props.toogleModalDetail}>
              {closeText}
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
    return !isMobile ? htmlWeb : htmlMobile;
  }
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
    login: state.login,
    basket: state.basket,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  addProductBasket,
  createBasketGuest,
};

// export default Step3;
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Step3));
