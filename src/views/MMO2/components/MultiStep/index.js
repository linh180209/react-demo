import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import _ from 'lodash';
import './styles.scss';
import {
  Button, Modal, ModalBody, ModalFooter,
} from 'reactstrap';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { GET_ALL_PRODUCTS_BOTTLE } from '../../../../config';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import auth from '../../../../Redux/Helpers/auth';
import { FONT_CUSTOME, COLOR_CUSTOME } from '../../../../constants';
import CustomeBottleV3 from '../../../../components/CustomeBottleV3';
import icDeleteGold from '../../../../image/icon/ic-delete-gold.svg';
import logoBlack from '../../../../image/icon/logo-custome-black.svg';
import logoWhite from '../../../../image/icon/logo-custome-white.svg';
import logoGold from '../../../../image/icon/logo-custome-gold.svg';
import icBottleImage from '../../../../image/icon/img-bottle-empty.png';
import bottleRollOn from '../../../../image/bottle_roll-on.png';
import bottleDualCaryon from '../../../../image/bottle_dual-crayon.png';
import Step2 from './step2';
import Step1 from './step1';
import Step3 from './step3';
import StartVideo from '../StartVideo';
import addCmsRedux from '../../../../Redux/Actions/cms';
import {
  getCmsCommon,
  getNameFromCommon, getNameFromButtonBlock, addFontCustom, segmentTrackScentBarQuizStep, segmentTrackScentBarQuizCompleted,
} from '../../../../Redux/Helpers/index';
import { toastrError } from '../../../../Redux/Helpers/notification';
import { isMobile } from '../../../../DetectScreen';
import SelectScent from './selectScent';

class MultiStep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentView: 'step2',
      isStartView: true,
      isNextView: true,
      step1: {
        isday: true,
      },
      step2: {
        firstNotes: null,
        secondNotes: null,
      },
      isOpenCustomeBottle: false,
      arrayThumbs: [],
      imageBlocks: [],
      isShowModalDetail: false,
      isOpenScentBoxMobile: false,
      dataMix: [],
      isShowMixSection: false,
      isDeleted: false,
      showMixSectionFrom: null,
      isShowModalRedo: false,
      activeScentDeskFrom: false,
      delaySlideModalMobile: false,
      selectItemId: undefined,
      isShowRollOn: false,
    };
    this.info = {
      your: {
        name: '',
      },
      their: {
        email: '',
        name: '',
        country: auth.getCountry(),
        message: '',
        dateSent: new Date(),
      },
      isCustomBottle: false,
      idVoucher: undefined,
      priceCustom: undefined,
      dataCustom: {
        currentImg: undefined,
        nameBottle: '',
        font: FONT_CUSTOME.JOST,
        color: COLOR_CUSTOME.BLACK,
        imagePremade: undefined,
      },
    };
  }

  showMixSection = (onCLickFrom) => {
    onCLickFrom
      ? this.setState({
        isShowMixSection: true,
        showMixSectionFrom: onCLickFrom,
      })
      : this.setState({ isShowMixSection: true, showMixSectionFrom: null });
  };

  hideMixSection = () => {
    this.setState({ isShowMixSection: false, isOpenScentBoxMobile: false });
  };

  handleFetchComboMix = (data) => {
    this.setState({
      dataMix: data,
    });
  };

  onSwitchRollOn = () => {
    const { isShowRollOn } = this.state;
    this.setState({ isShowRollOn: !isShowRollOn });
  }

  onGotoProduct = (isRollon) => {
    if (this.state.step2.firstNotes && this.state.step2.secondNotes) {
      this.props.onCreateMix(
        this.state.step2.firstNotes,
        this.state.step2.secondNotes,
        isRollon,
      );
    }
    //  this.props.history.push(urlPro, { custome });
  };

  handleSwitchView = (name, status, isDeleted) => {
    name === 'step2-1' ? (this.info.dataCustom.currentImg = undefined) : '';
    status
      ? this.setState({
        currentView: name,
        isNextView: true,
        activeScentDeskFrom: false,
      })
      : this.setState({
        currentView: name,
        isNextView: false,
        activeScentDeskFrom: false,
      });
    this.setState({ isShowMixSection: false, isDeleted });
  };

  handleSelectStep1 = (value) => {
    this.setState({
      step1: {
        isDay: value,
      },
      step2: {
        firstNotes: null,
        secondNotes: null,
      },
    });
  };

  handleSelectNotesStep2 = (data, isChildStep) => {
    !isChildStep
      ? this.setState({
        step2: {
          ...this.state.step2,
          firstNotes: data,
        },
      })
      : this.setState({
        step2: {
          ...this.state.step2,
          secondNotes: data,
        },
      });
  };

  handleSelectNotesStep3 = (data, isMainScent) => {
    if (data.firstNotes && data.secondNotes) {
      this.setState({
        step2: {
          firstNotes: data.firstNotes,
          secondNotes: data.secondNotes,
        },
      });
    } else if (data.firstNotes) {
      this.setState({
        step2: {
          ...this.state.step2,
          firstNotes: data.firstNotes,
        },
      });
    } else if (data.secondNotes) {
      this.setState({
        step2: {
          ...this.state.step2,
          secondNotes: data.secondNotes,
        },
      });
    } else {
      this.setState({
        step2: {
          ...this.state.step2,
        },
      });
    }
  };

  componentDidMount() {
    this.fetchBottle();
  }

  onSaveCustomer = (data) => {
    const { dataCustom } = this.info;
    const {
      image, name, color, font,
    } = data;
    dataCustom.color = color;
    dataCustom.font = font;
    if (_.isEmpty(image)) {
      dataCustom.currentImg = undefined;
      dataCustom.nameBottle = name;
      this.forceUpdate();
      this.props.handleDataCustome(dataCustom);
      return;
    }
    dataCustom.currentImg = image;
    dataCustom.nameBottle = name;
    const imagePremade = _.find(this.arrayThumbs, x => x.image === image)
      ? _.find(this.arrayThumbs, x => x.image === image).id
      : undefined;
    _.assign(dataCustom, { imagePremade });
    this.props.handleDataCustome(dataCustom);
    this.forceUpdate();
  };

  openCustomeBottle = () => {
    this.setState({ isOpenCustomeBottle: true });
  };

  closeCustomeBottle = () => {
    this.setState({ isOpenCustomeBottle: false });
  };

  handleCropImage = (img) => {
    const { dataCustom } = this.info;
    dataCustom.currentImg = img;
    this.forceUpdate();
  };

  fetchBottle = () => {
    const options = {
      url: GET_ALL_PRODUCTS_BOTTLE,
      method: 'GET',
    };
    fetchClient(options)
      .then((result) => {
        if (result && !result.isError && result.length > 0) {
          this.bottle = result[0];
          const thumbs = [];
          _.forEach(this.bottle.images, (x) => {
            if (x.type === 'suggestion') {
              thumbs.push(x);
            }
          });
          this.setState({ arrayThumbs: thumbs });
        }
      })
      .catch((err) => {
        toastrError(err.message);
      });
  };

  deleteNotes = (type) => {
    if (type && type === 1) {
      this.state.step2.firstNotes
        ? this.setState({
          step2: {
            ...this.state.step2,
            firstNotes: null,
          },
        })
        : '';
    } else {
      this.state.step2.secondNotes
        ? this.setState({
          step2: {
            ...this.state.step2,
            secondNotes: null,
          },
        })
        : '';
    }
    if (!this.state.step2.firstNotes || !this.state.step2.secondNotes) {
      this.setState({ currentView: 'step2' });
    }
  };

  toggleScentBoxMobile = (onCLickFrom) => {
    console.log('toggleScentBoxMobile', onCLickFrom, this.state.isOpenScentBoxMobile);
    this.setState({ delaySlideModalMobile: !this.state.delaySlideModalMobile });
    setTimeout(
      () => {
        this.setState({
          isOpenScentBoxMobile: !this.state.isOpenScentBoxMobile,
        });
        onCLickFrom
          ? this.setState({
            isShowMixSection: true,
            showMixSectionFrom: onCLickFrom,
          })
          : this.setState({ isShowMixSection: true, showMixSectionFrom: null });
      },
      this.state.delaySlideModalMobile ? 300 : 0,
    );
  };

  activeSelectScentDesk = () => {
    this.setState({ activeScentDeskFrom: true });
  };

  onClickApply = (data, index) => {
    const { selectItemId, showMixSectionFrom } = this.state;
    const indexSelect = index || selectItemId;
    segmentTrackScentBarQuizStep(indexSelect, data);
    if (indexSelect === 1) {
      this.handleSelectNotesStep2(data);
      this.handleSwitchView(this.state.step2.secondNotes ? 'step3' : 'step2-1', false);
    }
    if (indexSelect === 2) {
      this.handleSelectNotesStep2(data, true);
      this.handleSwitchView(this.state.step2.firstNotes ? 'step3' : 'step2-1', false);
    }
    if (isMobile) {
      this.setState({ isOpenScentBoxMobile: false });
    }
  }

  onClickCancel = () => {
    this.handleSwitchView('step2', false);
  }

  switchView = () => {
    const cmsCommon = getCmsCommon(this.props.cms);
    const strengthBt = getNameFromCommon(cmsCommon, 'STRENGTH');
    const durationBt = getNameFromCommon(cmsCommon, 'DURATION');

    switch (this.state.currentView) {
      case 'step1':
        return (
          <Step1
            switchView={this.handleSwitchView}
            isNextView={this.state.isNextView}
            selectStep1={this.handleSelectStep1}
            isDay={this.state.step1.isDay}
            buttonBlocks={this.props.buttonBlocks}
          />
        );
      case 'step2':
        return (
          <Step2
            dataScentNote={
              this.props.dataScentNote ? this.props.dataScentNote : ''
            }
            bottleGif={this.state.bottleGif}
            scents={this.props.scents}
            resultScentNotes={this.props.resultScentNotes}
            switchView={this.handleSwitchView}
            selectNotesStep2={this.handleSelectNotesStep2}
            isNextView={this.state.isNextView}
            isDay={null}
            toggleScentBoxMobile={this.toggleScentBoxMobile}
            isOpenScentBoxMobile={this.state.isOpenScentBoxMobile}
            delaySlideModalMobile={this.state.delaySlideModalMobile}
            currentView={this.state.currentView}
            buttonBlocks={this.props.buttonBlocks}
            headerAndParagraphBocks={this.props.headerAndParagraphBocks}
            dataStep2={this.state.step2}
            isDeleted={this.state.isDeleted}
            activeScentDeskFrom={this.state.activeScentDeskFrom}
            strengthBt={strengthBt}
            durationBt={durationBt}
            onClickIngredient={this.props.onClickIngredient}
            onClickSelectScent={index => this.setState({ currentView: 'step-select', selectItemId: index })}
            mmos={this.props.mmos}
            onClickApply={this.onClickApply}
            showMixSectionFrom={this.state.showMixSectionFrom}
          />
        );
      case 'step2-1':
        return (
          <Step2
            firstNotes={this.state.step2.firstNotes}
            secondNotes={this.state.step2.secondNotes}
            dataScentNote={
              this.props.dataScentNote ? this.props.dataScentNote : ''
            }
            bottleGif={this.state.bottleGif}
            scents={this.props.scents}
            resultScentNotes={this.props.resultScentNotes}
            switchView={this.handleSwitchView}
            selectNotesStep2={this.handleSelectNotesStep2}
            isNextView={this.state.isNextView}
            isDay={this.state.step1.isDay}
            toggleScentBoxMobile={this.toggleScentBoxMobile}
            isOpenScentBoxMobile={this.state.isOpenScentBoxMobile}
            delaySlideModalMobile={this.state.delaySlideModalMobile}
            currentView={this.state.currentView}
            buttonBlocks={this.props.buttonBlocks}
            headerAndParagraphBocks={this.props.headerAndParagraphBocks}
            dataStep2={this.state.step2}
            isDeleted={this.state.isDeleted}
            activeScentDeskFrom={this.state.activeScentDeskFrom}
            strengthBt={strengthBt}
            durationBt={durationBt}
            onClickIngredient={this.props.onClickIngredient}
            onClickSelectScent={index => this.setState({ currentView: 'step-select', selectItemId: index })}
            mmos={this.props.mmos}
            onClickApply={this.onClickApply}
            showMixSectionFrom={this.state.showMixSectionFrom}
            resetData={this.resetData}
          />
        );
      case 'step3':
        return (
          <Step3
            dataScentNote={
              this.props.dataScentNote ? this.props.dataScentNote : ''
            }
            isDeleted={this.state.isDeleted}
            switchView={this.handleSwitchView}
            dataStep2={this.state.step2}
            isDay={this.state.step1.isDay}
            selectNotesStep2={this.handleSelectNotesStep2}
            openCustomeBottle={this.openCustomeBottle}
            selectNotesStep3={this.handleSelectNotesStep3}
            dataCustom={this.info.dataCustom}
            isShowModalDetail={this.state.isShowModalDetail}
            toogleModalDetail={this.toogleModalDetail}
            deleteNotes={this.deleteNotes}
            buttonBlocks={this.props.buttonBlocks}
            handleFetchComboMix={this.handleFetchComboMix}
            isShowMixSection={this.state.isShowMixSection}
            showMixSectionFrom={this.state.showMixSectionFrom}
            showMixSection={this.showMixSection}
            hideMixSection={this.hideMixSection}
            toggleScentBoxMobile={this.toggleScentBoxMobile}
            currentView={this.state.currentView}
            isOpenScentBoxMobile={this.state.isOpenScentBoxMobile}
            delaySlideModalMobile={this.state.delaySlideModalMobile}
            toggleModalRedo={this.toggleModalRedo}
            strengthBt={strengthBt}
            durationBt={durationBt}
            onClickIngredient={this.props.onClickIngredient}
            resultScentNotes={this.props.resultScentNotes}
            onSwitchRollOn={this.onSwitchRollOn}
            isShowRollOn={this.state.isShowRollOn}
          />
        );
      case 'step-select':
        return (
          <SelectScent
            scentNotes={this.props.resultScentNotes}
            buttonBlocks={this.props.buttonBlocks}
            onClickApply={this.onClickApply}
            onClickCancel={this.onClickCancel}
            isShowTitle
            selectItemId={this.state.selectItemId}
            switchView={this.handleSwitchView}
            onClickIngredient={this.props.onClickIngredient}
          />
        );
      default:
        return (
          <StartVideo
            video={this.props.video}
            headerCms={this.props.headerCms}
            switchView={this.handleSwitchView}
            buttonBlocks={this.props.buttonBlocks}
          />
        );
    }
  };

  toogleModalDetail = () => {
    this.setState({ isShowModalDetail: !this.state.isShowModalDetail });
  };

  toggleModalRedo = () => {
    this.setState({ isShowModalRedo: !this.state.isShowModalRedo });
  };

  resetData = () => {
    this.setState({
      currentView: 'step2',
      step2: {
        firstNotes: null,
        secondNotes: null,
      },
    });
  }

  render() {
    const {
      currentView,
      isStartView,
      isNextView,
      step1,
      step2,
      isOpenCustomeBottle,
      arrayThumbs,
      deleteNotes,
      imageBlock,
      isShowModalRedo,
      isShowRollOn,
    } = this.state;
    const { isDay } = this.state.step1;
    const { dataCustom } = this.info;
    const {
      dataScentNote,
      bottleGif,
      resultScentNotes,
      video,
      headerCms,
      buttonBlocks,
      headerAndParagraphBocks,
    } = this.props;
    const bottle = bottleGif
      ? _.filter(bottleGif, x => x.value.caption === 'bottle-gif')[0].value
        .image
      : '';
    const viewDetailText = _.filter(
      buttonBlocks,
      x => x.value.name === 'view_detail',
    )[0].value.text;
    const yesImSure = _.find(buttonBlocks, x => x.value.name === 'yes_im_sure')
      ? _.find(buttonBlocks, x => x.value.name === 'yes_im_sure').value.text
      : 'Yes i\'m sure';
    const noRedo = _.find(
      buttonBlocks,
      x => x.value.name === 'no_i_don_want_to_redo',
    )
      ? _.find(buttonBlocks, x => x.value.name === 'no_i_don_want_to_redo')
        .value.text
      : '';
    const redoMixHeader = _.find(
      buttonBlocks,
      x => x.value.name === 'redo_mix_header',
    )
      ? _.find(buttonBlocks, x => x.value.name === 'redo_mix_header').value
        .text
      : '';
    const redoContent = _.find(
      headerAndParagraphBocks,
      x => x.value.header.header_text === 'redo_content',
    )
      ? _.find(
        headerAndParagraphBocks,
        x => x.value.header.header_text === 'redo_content',
      ).value.paragraph
      : '';

    const st1Bt = getNameFromButtonBlock(buttonBlocks, '1ST');
    const nd2Bt = getNameFromButtonBlock(buttonBlocks, '2ND');

    const { firstNotes, secondNotes } = step2;
    //console.log('firstNotes, secondNotes', firstNotes, secondNotes);
    return (
      <div
        className={`multi-step-mmo ${
          isMobile ? `multi-step-mmo-mobile mmo-${currentView}-mobile` : ''
        }`}
      >
        {this.switchView()}
        {currentView && currentView != 'step1' ? (
          <div
            className={`bottle-box ${
              ['step3', 'step-select'].includes(currentView) && !isMobile
                ? 'bottle-large'
                : currentView === 'step2-1'
                  || (['step3', 'step-select'].includes(currentView) && isMobile)
                  ? 'bottle-large-v2'
                  : ''
            } ${isShowRollOn ? 'rollon-mobile' : ''}`}
          >
            {
              isShowRollOn ? (
                <div className="wrapper-rollon">
                  {/* <img src={bottleRollOn} alt="roll on" /> */}
                  <img src={bottleDualCaryon} alt="roll on" />
                  <button
                    onClick={() => this.onGotoProduct(true)}
                    className="button-bg__none view-more"
                    type="button"
                  >
                    {viewDetailText}
                  </button>

                </div>
              ) : (
                <div className="wrapper-bottle">
                  {dataCustom.currentImg !== undefined && ['step3', 'step-select'].includes(currentView) ? (
                    <img
                      src={icBottleImage}
                      alt="bottle"
                      // onClick={() => this.onGotoProduct(false)}
                    />
                  ) : (
                    <img
                      src={bottle}
                      alt="bottle"
                      // onClick={() => this.onGotoProduct(false)}
                    />
                  )}
                  {['step3', 'step-select'].includes(currentView) && isDay === true ? (
                    <div
                      className="selection-scent"
                      style={{
                        display:
                          dataCustom.currentImg !== undefined
                          && ['step3', 'step-select'].includes(currentView)
                            ? 'none'
                            : '',
                      }}
                    >
                      <div
                        className="scent"
                        style={{
                          backgroundImage: `url(${
                            step2.firstNotes ? step2.firstNotes.image : ''
                          })`,
                          border: step2.firstNotes ? 'none' : '2px dashed #8d8d8d',
                          cursor: ['step3', 'step-select'].includes(currentView) ? 'pointer' : 'unset',
                        }}
                        onClick={() => {
                          // (!step2.firstNotes || !step2.secondNotes) &&
                          !isMobile && ['step3', 'step-select'].includes(currentView)
                            ? this.showMixSection('first')
                            : ['step3', 'step-select'].includes(currentView)
                              ? this.toggleScentBoxMobile('first')
                              : '';
                        }}
                      >
                        <button
                          type="button"
                          style={{
                            display:
                            ['step3', 'step-select'].includes(currentView) && !isMobile && step2.firstNotes ? 'flex' : 'none',
                          }}
                          onClick={(e) => {
                            e.stopPropagation();
                            this.deleteNotes(1);
                          }}
                        >
                          <img src={icDeleteGold} alt="delete" />
                        </button>
                        <span
                          style={{
                            display: step2.firstNotes ? 'none' : 'block',
                            opacity: '1',
                          }}
                        >
                          {st1Bt}
                        </span>
                      </div>
                      <div
                        className="scent"
                        style={{
                          backgroundImage: `url(${
                            step2.secondNotes ? step2.secondNotes.image : ''
                          })`,
                          border: step2.secondNotes ? 'none' : '2px dashed #8d8d8d',
                          cursor:
                            currentView === 'step2-1' || ['step3', 'step-select'].includes(currentView)
                              ? 'pointer'
                              : '',
                          borderBottom: 0,
                        }}
                        onClick={() => {
                          currentView === 'step2-1'
                            ? this.setState({ currentView: 'step2' })
                            : '';
                          // (!step2.firstNotes || !step2.secondNotes) &&
                          !isMobile && ['step3', 'step-select'].includes(currentView)
                            ? this.showMixSection('second')
                            : ['step3', 'step-select'].includes(currentView)
                              ? this.toggleScentBoxMobile('second')
                              : '';
                        }}
                      >
                        <button
                          type="button"
                          style={{
                            display:
                            ['step3', 'step-select'].includes(currentView) && !isMobile && step2.secondNotes ? 'flex' : 'none',
                          }}
                          onClick={(e) => {
                            e.stopPropagation();
                            this.deleteNotes(2);
                          }}
                        >
                          <img src={icDeleteGold} alt="delete" />
                        </button>
                        <span
                          style={{
                            display: step2.secondNotes ? 'none' : 'block',
                            opacity: '1',
                            // display: 'block',
                          }}
                        >
                          {nd2Bt}
                        </span>
                      </div>
                    </div>
                  ) : (
                    <div
                      className="selection-scent"
                      style={{
                        display:
                          dataCustom.currentImg !== undefined
                          && ['step3', 'step-select'].includes(currentView)
                            ? 'none'
                            : '',
                      }}
                    >
                      <div
                        className="scent"
                        style={{
                          backgroundImage: `url(${
                            step2.firstNotes ? step2.firstNotes.image : ''
                          })`,
                          border: step2.firstNotes ? 'none' : '2px dashed #8d8d8d',
                          // cursor:
                          //   currentView == "step2-1" || currentView == "step3"
                          //     ? "pointer"
                          //     : "",
                          cursor: 'pointer',
                          borderBottom: 0,
                        }}
                        onClick={() => {
                          ['step2', 'step2-1'].includes(this.state.currentView) && !isMobile
                            ? this.setState({ currentView: 'step-select', selectItemId: 1 })
                            : this.state.currentView === 'step2-1'
                              ? this.setState({ currentView: 'step2' })
                              : this.state.currentView === 'step2' && !isMobile
                                ? this.activeSelectScentDesk()
                                : '';
                          // (!step2.firstNotes || !step2.secondNotes) &&
                          !isMobile && ['step3', 'step-select'].includes(currentView)
                            ? this.showMixSection('first')
                            : ['step3', 'step-select'].includes(currentView) || currentView == 'step2'
                              ? this.toggleScentBoxMobile('first')
                              : '';
                        }}
                      >
                        <button
                          type="button"
                          style={{
                            display:
                            ['step3', 'step-select'].includes(currentView) && !isMobile && step2.firstNotes ? 'flex' : 'none',
                          }}
                          onClick={(e) => {
                            e.stopPropagation();
                            this.deleteNotes(1);
                          }}
                        >
                          <img src={icDeleteGold} alt="delete" />
                        </button>
                        <span
                          style={{
                            display: step2.firstNotes ? 'none' : 'block',
                            opacity: '1',
                          }}
                        >
                          {st1Bt}
                        </span>
                      </div>
                      <div
                        className="scent"
                        style={{
                          backgroundImage: `url(${
                            step2.secondNotes ? step2.secondNotes.image : ''
                          })`,
                          border: step2.secondNotes ? 'none' : '2px dashed #8d8d8d',
                          // cursor: currentView == "step3" ? "pointer" : "unset",
                          cursor: 'pointer',
                        }}
                        onClick={() => {
                          // (!step2.firstNotes || !step2.secondNotes) &&
                          ['step2', 'step2-1'].includes(this.state.currentView) && !isMobile
                            ? this.setState({ currentView: 'step-select', selectItemId: 2 })
                            : !isMobile && ['step3', 'step-select'].includes(currentView)
                              ? this.showMixSection('second')
                              : ['step3', 'step-select'].includes(currentView) || currentView === 'step2-1'
                                ? this.toggleScentBoxMobile('second')
                                : currentView === 'step2'
                                  ? this.toggleScentBoxMobile('first')
                                  : '';
                          !isMobile
                          && ((currentView === 'step2' && !step2.firstNotes)
                            || currentView === 'step2-1')
                            ? this.activeSelectScentDesk()
                            : !isMobile && ['step3', 'step-select'].includes(currentView)
                              ? this.setState({ currentView: 'step2-1' })
                              : isMobile && currentView === 'step2' && step2.firstNotes
                                ? this.setState({ currentView: 'step2-1' })
                                : '';
                        }}
                      >
                        <button
                          type="button"
                          style={{
                            display:
                            ['step3', 'step-select'].includes(currentView) && !isMobile && step2.secondNotes ? 'flex' : 'none',
                          }}
                          onClick={(e) => {
                            e.stopPropagation();
                            this.deleteNotes(2);
                          }}
                        >
                          <img src={icDeleteGold} alt="delete" />
                        </button>
                        <span
                          style={{
                            display: step2.secondNotes ? 'none' : 'block',
                            opacity: '1',
                            // display: 'block',
                          }}
                        >
                          {nd2Bt}
                        </span>
                      </div>
                    </div>
                  )}
                  <div
                    className={
                      !isMobile
                        ? 'image-custom-bottle'
                        : 'image-custom-bottle-mobile'
                    }
                    style={{
                      display:
                        dataCustom.currentImg !== undefined && ['step3', 'step-select'].includes(currentView)
                          ? ''
                          : 'none',
                    }}
                  >
                    <img
                      className="image-logo  animated faster fadeIn"
                      alt="back"
                      src={
                        dataCustom.color === COLOR_CUSTOME.BLACK
                          ? logoBlack
                          : dataCustom.color === COLOR_CUSTOME.WHITE
                            ? logoWhite
                            : logoGold
                      }
                    />
                    <img src={dataCustom ? dataCustom.currentImg : ''} alt="im" />
                    <div
                      className="info-custome"
                      style={{
                        color:
                          dataCustom.color === COLOR_CUSTOME.BLACK
                            ? '#0d0d0d'
                            : dataCustom.color === COLOR_CUSTOME.WHITE
                              ? '#ffffff'
                              : '#B79B53',
                      }}
                    >
                      <span style={{ fontFamily: dataCustom.font }}>
                        {dataCustom.nameBottle ? dataCustom.nameBottle : ''}
                      </span>
                      <span>Eau de parfum</span>
                      <span>25ml</span>
                    </div>
                  </div>
                  <p
                    className={`btn-product-detail ${
                      isMobile ? 'btn-product-detail-mobile' : ''
                    }`}
                    style={{
                      display: ['step3', 'step-select'].includes(currentView) ? 'block' : 'none',
                      cursor: 'pointer',
                      visibility: step2.secondNotes && step2.firstNotes ? 'visible' : 'hidden',
                    }}
                    onClick={() => this.onGotoProduct(false)}
                  >
                    {viewDetailText}
                  </p>
                </div>
              )
            }
          </div>
        ) : (
          ''
        )}

        {/* custom bottle */}
        {/* custom bottle modal */}
        {isOpenCustomeBottle ? (
          <CustomeBottleV3
            arrayThumbs={arrayThumbs}
            handleCropImage={this.handleCropImage}
            currentImg={dataCustom.currentImg}
            nameBottle={dataCustom.nameBottle}
            font={dataCustom.font}
            color={dataCustom.color}
            closePopUp={() => {
              this.setState({ isOpenCustomeBottle: false });
            }}
            onSave={this.onSaveCustomer}
            dataGtmtracking="funnel-3-step-06-2-save-customize-bottle"
            // itemBottle={{
            //   price: this.bottle ? this.bottle.items[0].price : '',
            // }}
            // cmsTextBlocks={textBlock}
            name1={firstNotes ? firstNotes.name : ''}
            name2={secondNotes ? secondNotes.name : ''}
          />
        ) : null}
        <Modal
          isOpen={isShowModalRedo}
          // toggle={this.toogleModalDetail}
          className={`modal-redo-mmo ${
            isMobile ? 'modal-redo-mmo-mobile' : ''
          } ${addFontCustom()}`}
          // size=""
          centered
        >
          <ModalBody className="body-modal-redo">
            <h3>{redoMixHeader}</h3>
            <div className="content-redo">{htmlPare(redoContent || '')}</div>
          </ModalBody>

          <ModalFooter>
            <Button
              onClick={() => {
                this.toggleModalRedo();
                this.resetData();
              }}
            >
              {yesImSure || 'Yes i\'m sure'}
            </Button>
            <Button onClick={() => this.toggleModalRedo()}>{noRedo}</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    login: state.login,
    cms: state.cms,
    mmos: state.mmos,
    scents: state.scents,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};

// export default MultiStep;
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(MultiStep),
);
