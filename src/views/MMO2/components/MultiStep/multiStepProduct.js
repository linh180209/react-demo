import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
// import clubBanner from '../../assets/club-banner.png';
import './styles.scss';
import Slider from 'react-slick';
// import { generaCurrency } from '../../../../Redux/Helpers';
import {
  Button, Modal, ModalHeader, ModalBody, ModalFooter,
} from 'reactstrap';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import icFresh from '../../../../image/icon/ic-fresh.svg';
import icFreshActive from '../../../../image/icon/ic-fresh-active.svg';
import icSeductive from '../../../../image/icon/ic-seductive.svg';
import icSeductiveActive from '../../../../image/icon/ic-seductive-active.svg';
import icCheckedMmo from '../../../../image/icon/ic-checked-mmo.svg';
import { GET_ALL_PRODUCTS_BOTTLE } from '../../../../config';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import auth from '../../../../Redux/Helpers/auth';
import { FONT_CUSTOME, COLOR_CUSTOME } from '../../../../constants';
import CustomeBottleV3 from '../../../../components/CustomeBottleV3';
import icDeleteGold from '../../../../image/icon/ic-delete-gold.svg';
import logoBlack from '../../../../image/icon/logo-custome-black.svg';
import logoWhite from '../../../../image/icon/logo-custome-white.svg';
import logoGold from '../../../../image/icon/logo-custome-gold.svg';
import icBottleImage from '../../../../image/icon/img-bottle-empty.png';
import Step2 from './step2';
import Step1 from './step1';
import Step3 from './step3';
import addCmsRedux from '../../../../Redux/Actions/cms';
import { getNameFromButtonBlock, addFontCustom } from '../../../../Redux/Helpers/index';
import { isMobile } from '../../../../DetectScreen';
import SelectScent from './selectScent';

class MultiStep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentView: 'step2',
      isStartView: true,
      isNextView: true,
      step1: {
        isDay: true,
      },
      step2: {
        firstNotes: null,
        secondNotes: null,
      },
      isOpenCustomeBottle: false,
      arrayThumbs: [],
      imageBlocks: [],
      isShowModalDetail: false,
      isOpenScentBoxMobile: false,
      dataMix: [],
      isShowMixSection: false,
      isDeleted: false,
      showMixSectionFrom: null,
      isShowModalRedo: false,
    };
    this.info = {
      your: {
        name: '',
      },
      their: {
        email: '',
        name: '',
        country: auth.getCountry(),
        message: '',
        dateSent: new Date(),
      },
      isCustomBottle: false,
      idVoucher: undefined,
      priceCustom: undefined,
      dataCustom: {
        currentImg: undefined,
        nameBottle: '',
        font: FONT_CUSTOME.JOST,
        color: COLOR_CUSTOME.BLACK,
        imagePremade: undefined,
      },
      delaySlideModalMobile: false,
    };
  }

  showMixSection = (onCLickFrom) => {
    onCLickFrom
      ? this.setState({
        isShowMixSection: true,
        showMixSectionFrom: onCLickFrom,
      })
      : this.setState({ isShowMixSection: true, showMixSectionFrom: null });
    // console.log('asd');
  };

  hideMixSection = () => {
    this.setState({ isShowMixSection: false });
  };

  handleFetchComboMix = (data) => {
    this.setState({
      dataMix: data,
    });
  };

  onGotoProduct = (isCustome) => {
    if (this.state.step2.firstNotes && this.state.step2.secondNotes) {
      this.props.onCreateMix(
        this.state.step2.firstNotes,
        this.state.step2.secondNotes,
      );
    }
    //  this.props.history.push(urlPro, { custome });
  };

  handleSwitchView = (name, status, isDeleted) => {
    console.log('ssss', name, status, isDeleted);
    name === 'step2-1' ? (this.info.dataCustom.currentImg = undefined) : '';
    status
      ? this.setState({ currentView: name, isNextView: true })
      : this.setState({ currentView: name, isNextView: false });
    name === 'step2-1' ? this.setState({ isShowMixSection: false }) : '';

    this.setState({ isDeleted });
  };

  handleSelectStep1 = (value) => {
    // this.setState({
    //   step1: {
    //     isDay: value,
    //   },
    //   step2: {
    //     firstNotes: null,
    //     secondNotes: null,
    //   },
    // });
  };

  handleSelectNotesStep2 = (data, isChildStep) => {
    console.log('sss', data, isChildStep);
    !isChildStep
      ? this.setState({
        step2: {
          ...this.state.step2,
          firstNotes: data,
        },
      })
      : this.setState({
        step2: {
          ...this.state.step2,
          secondNotes: data,
        },
      });
  };

  handleSelectNotesStep3 = (data, isMainScent) => {
    if (data.firstNotes && data.secondNotes) {
      this.setState({
        step2: {
          firstNotes: data.firstNotes,
          secondNotes: data.secondNotes,
        },
      });
    } else if (data.firstNotes) {
      this.setState({
        step2: {
          ...this.state.step2,
          firstNotes: data.firstNotes,
        },
      });
    } else if (data.secondNotes) {
      this.setState({
        step2: {
          ...this.state.step2,
          secondNotes: data.secondNotes,
        },
      });
    } else {
      this.setState({
        step2: {
          ...this.state.step2,
        },
      });
    }
    // isMainScent
    //   ? this.setState({
    //       step2: {
    //         ...this.state.step2,
    //         firstNotes: data,
    //       },
    //     })
    //   : this.setState({
    //       step2: {
    //         ...this.state.step2,
    //         secondNotes: data,
    //       },
    //     });
  };

  componentDidMount() {
    this.fetchBottle();
  }

  onSaveCustomer = (data) => {
    const { dataCustom } = this.info;
    // const { image, name, color, font } = data;
    // dataCustom.color = color;
    // dataCustom.font = font;

    // console.log(data);
    // if (_.isEmpty(image)) {
    //   dataCustom.currentImg = undefined;
    //   dataCustom.nameBottle = name;

    //   this.forceUpdate();
    //   return;
    // }
    // dataCustom.currentImg = image;
    // dataCustom.nameBottle = name;
    // this.forceUpdate();
    const {
      image, name, color, font,
    } = data;
    dataCustom.color = color;
    dataCustom.font = font;
    if (_.isEmpty(image)) {
      dataCustom.currentImg = undefined;
      dataCustom.nameBottle = name;
      this.forceUpdate();
      this.props.handleDataCustome(dataCustom);
      return;
    }
    dataCustom.currentImg = image;
    dataCustom.nameBottle = name;
    const imagePremade = _.find(this.arrayThumbs, x => x.image === image)
      ? _.find(this.arrayThumbs, x => x.image === image).id
      : undefined;
    _.assign(dataCustom, { imagePremade });
    this.props.handleDataCustome(dataCustom);
    this.forceUpdate();
  };

  openCustomeBottle = () => {
    this.setState({ isOpenCustomeBottle: true });
  };

  closeCustomeBottle = () => {
    this.setState({ isOpenCustomeBottle: false });
  };

  handleCropImage = (img) => {
    const { dataCustom } = this.info;
    dataCustom.currentImg = img;
    this.forceUpdate();
  };

  fetchBottle = () => {
    const options = {
      url: GET_ALL_PRODUCTS_BOTTLE,
      method: 'GET',
    };
    fetchClient(options)
      .then((result) => {
        if (result && !result.isError && result.length > 0) {
          this.bottle = result[0];
          const thumbs = [];
          _.forEach(this.bottle.images, (x) => {
            if (x.type === 'suggestion') {
              thumbs.push(x);
            }
          });
          this.setState({ arrayThumbs: thumbs });
        }
      })
      .catch((err) => {
        toastrError(err.message);
      });
  };

  deleteNotes = (type) => {
    if (type && type === 1) {
      this.state.step2.firstNotes
        ? this.setState({
          step2: {
            ...this.state.step2,
            firstNotes: null,
          },
        })
        : '';
      console.log(this.state.step2.firstNotes);
    } else {
      this.state.step2.secondNotes
        ? this.setState({
          step2: {
            ...this.state.step2,
            secondNotes: null,
          },
        })
        : '';
      console.log(this.state.step2.secondNotes);
    }
    if (!this.state.step2.firstNotes || !this.state.step2.secondNotes) {
      this.setState({ currentView: 'step2' });
    }
  };

  toggleScentBoxMobile = (onCLickFrom) => {
    this.setState({ delaySlideModalMobile: !this.state.delaySlideModalMobile });
    setTimeout(() => {
      this.setState({ isOpenScentBoxMobile: !this.state.isOpenScentBoxMobile });
      onCLickFrom
        ? this.setState({
          isShowMixSection: true,
          showMixSectionFrom: onCLickFrom,
        })
        : this.setState({ isShowMixSection: true, showMixSectionFrom: null });
    }, this.state.delaySlideModalMobile ? 300 : 0);
  };

  switchView = () => {
    console.log('isday', this.state.step1);
    switch (this.state.currentView) {
      case 'step1':
        return (
          <Step1
            switchView={this.handleSwitchView}
            isNextView={this.state.isNextView}
            selectStep1={this.handleSelectStep1}
            isDay={this.state.step1.isDay}
            buttonBlocks={this.props.buttonBlocks}
          />
        );
      case 'step2':
        return (
          <Step2
            isShowOnlyScentNotes
            dataScentNote={
              this.props.dataScentNote ? this.props.dataScentNote : ''
            }
            bottleGif={this.state.bottleGif}
            // resultScentNotes={this.props.resultScentNotes}
            switchView={this.handleSwitchView}
            selectNotesStep2={this.handleSelectNotesStep2}
            isNextView={this.state.isNextView}
            isDay={null}
            toggleScentBoxMobile={this.toggleScentBoxMobile}
            delaySlideModalMobile={this.state.delaySlideModalMobile}
            isOpenScentBoxMobile={this.state.isOpenScentBoxMobile}
            currentView={this.state.currentView}
            buttonBlocks={this.props.buttonBlocks}
            headerAndParagraphBocks={this.props.headerAndParagraphBocks}
            dataStep2={this.state.step2}
            isDeleted={this.state.isDeleted}
            isNonHeader
            onClickIngredient={this.props.onClickIngredient}
          />
        );
      case 'step2-1':
        return (
          <Step2
            isShowOnlyScentNotes
            dataScentNote={
              this.props.dataScentNote ? this.props.dataScentNote : ''
            }
            bottleGif={this.state.bottleGif}
            // resultScentNotes={this.props.resultScentNotes}
            switchView={this.handleSwitchView}
            selectNotesStep2={this.handleSelectNotesStep2}
            isNextView={this.state.isNextView}
            isDay={this.state.step1.isDay}
            toggleScentBoxMobile={this.toggleScentBoxMobile}
            delaySlideModalMobile={this.state.delaySlideModalMobile}
            isOpenScentBoxMobile={this.state.isOpenScentBoxMobile}
            currentView={this.state.currentView}
            buttonBlocks={this.props.buttonBlocks}
            headerAndParagraphBocks={this.props.headerAndParagraphBocks}
            dataStep2={this.state.step2}
            isDeleted={this.state.isDeleted}
            isNonHeader
            isFullFressSensual
            onClickIngredient={this.props.onClickIngredient}
          />
        );
      case 'step3':
        return (
          <Step3
            isShowOnlyScentNotes
            dataScentNote={
              this.props.dataScentNote ? this.props.dataScentNote : ''
            }
            isDeleted={this.state.isDeleted}
            switchView={this.handleSwitchView}
            dataStep2={this.state.step2}
            isDay={this.state.step1.isDay}
            selectNotesStep2={this.handleSelectNotesStep2}
            openCustomeBottle={this.openCustomeBottle}
            selectNotesStep3={this.handleSelectNotesStep3}
            dataCustom={this.info.dataCustom}
            isShowModalDetail={this.state.isShowModalDetail}
            delaySlideModalMobile={this.state.delaySlideModalMobile}
            toogleModalDetail={this.toogleModalDetail}
            deleteNotes={this.deleteNotes}
            buttonBlocks={this.props.buttonBlocks}
            handleFetchComboMix={this.handleFetchComboMix}
            isShowMixSection={this.state.isShowMixSection}
            showMixSectionFrom={this.state.showMixSectionFrom}
            showMixSection={this.showMixSection}
            hideMixSection={this.hideMixSection}
            toggleScentBoxMobile={this.toggleScentBoxMobile}
            currentView={this.state.currentView}
            isOpenScentBoxMobile={this.state.isOpenScentBoxMobile}
            toggleModalRedo={this.toggleModalRedo}
            isNonHeader
            isFullFressSensual
            dataProduct={this.props.dataProduct}
            onClickIngredient={this.props.onClickIngredient}
          />
        );
      default:
        return (
          // <StartVideo
          //   video={this.props.video}
          //   headerCms={this.props.headerCms}
          //   switchView={this.handleSwitchView}
          //   buttonBlocks={this.props.buttonBlocks}
          // />
          null
        );
    }
  };

  toogleModalDetail = () => {
    this.setState({ isShowModalDetail: !this.state.isShowModalDetail });
  };

  toggleModalRedo = () => {
    this.setState({ isShowModalRedo: !this.state.isShowModalRedo });
  };

  render() {
    const {
      currentView,
      isStartView,
      isNextView,
      step2,
      isOpenCustomeBottle,
      arrayThumbs,
      deleteNotes,
      imageBlock,
      isShowModalRedo,
    } = this.state;
    const { isDay } = this.state.step1;
    const { dataCustom } = this.info;
    const {
      dataScentNote,
      bottleGif,
      resultScentNotes,
      video,
      headerCms,
      buttonBlocks,
      headerAndParagraphBocks,
    } = this.props;
    const bottle = bottleGif
      ? _.filter(bottleGif, x => x.value.caption === 'bottle-gif')[0].value
        .image
      : '';
    const viewDetailText = _.filter(
      buttonBlocks,
      x => x.value.name === 'view_detail',
    )[0].value.text;
    const yesImSure = _.filter(
      buttonBlocks,
      x => x.value.name === 'yes_im_sure',
    )[0].value.text;
    const noRedo = _.filter(
      buttonBlocks,
      x => x.value.name === 'no_i_don_want_to_redo',
    )[0].value.text;
    const redoMixHeader = _.filter(
      buttonBlocks,
      x => x.value.name === 'redo_mix_header',
    )[0].value.text;

    const st1Bt = getNameFromButtonBlock(buttonBlocks, '1ST');
    const nd2Bt = getNameFromButtonBlock(buttonBlocks, '2ND');
    // const dataNote = dataScentNote ? dataScentNote : '';
    return (
      <div
        className={`multi-step-mmo ${
          isMobile ? `multi-step-mmo-mobile mmo-${currentView}-mobile` : ''
        }`}
      >
        {this.switchView()}
        {currentView && currentView != 'step1' ? (
          <div
            className={`bottle-box ${
              currentView === 'step3' && !isMobile
                ? 'bottle-large'
                : currentView === 'step2-1'
                  || (currentView === 'step3' && isMobile)
                  ? 'bottle-large-v2'
                  : ''
            }`}
          >
            <div className="wrapper-bottle">
              {dataCustom.currentImg != undefined && currentView === 'step3' ? (
                <img
                  src={icBottleImage}
                  alt="bottle"
                  // onClick={() => this.onGotoProduct(false)}
                />
              ) : (
                <img
                  src={bottle}
                  alt="bottle"
                  // onClick={() => this.onGotoProduct(false)}
                />
              )}
              {currentView === 'step3' && isDay === true ? (
                <div
                  className="selection-scent"
                  style={{
                    display:
                      dataCustom.currentImg != undefined
                      && currentView === 'step3'
                        ? 'none'
                        : '',
                  }}
                >
                  <div
                    className="scent"
                    style={{
                      backgroundImage: `url(${
                        step2.secondNotes ? step2.secondNotes.image : ''
                      })`,
                      border: step2.secondNotes ? 'none' : '2px dashed #8d8d8d',
                      cursor: currentView === 'step3' ? 'pointer' : 'unset',
                    }}
                    onClick={() => {
                      // (!step2.firstNotes || !step2.secondNotes) &&
                      !isMobile && currentView === 'step3'
                        ? this.showMixSection('second')
                        : (currentView === 'step3' ? this.toggleScentBoxMobile('second') : '');
                    }}
                  >
                    <button
                      style={{
                        display:
                          currentView === 'step3' && !isMobile
                            ? 'block'
                            : 'none',
                      }}
                      onClick={(e) => {
                        e.stopPropagation();
                        this.deleteNotes(2);
                      }}
                    >
                      <img src={icDeleteGold} alt="delete" />
                    </button>
                    <span
                      style={{
                        display: step2.secondNotes ? 'none' : 'block',
                        opacity: '1',
                      }}
                    >
                      {nd2Bt}
                    </span>
                  </div>
                  <div
                    className="scent"
                    style={{
                      backgroundImage: `url(${
                        step2.firstNotes ? step2.firstNotes.image : ''
                      })`,
                      border: step2.firstNotes ? 'none' : '2px dashed #8d8d8d',
                      cursor:
                        currentView === 'step2-1' || currentView === 'step3'
                          ? 'pointer'
                          : '',
                      borderBottom: 0,
                    }}
                    onClick={() => {
                      currentView === 'step2-1'
                        ? this.setState({ currentView: 'step2' })
                        : '';
                      // (!step2.firstNotes || !step2.secondNotes) &&
                      !isMobile && currentView === 'step3'
                        ? this.showMixSection('first')
                        : (currentView === 'step3' ? this.toggleScentBoxMobile('first') : '');
                    }}
                  >
                    <button
                      style={{
                        display:
                          currentView === 'step3' && !isMobile
                            ? 'block'
                            : 'none',
                      }}
                      onClick={(e) => {
                        e.stopPropagation();
                        this.deleteNotes(1);
                      }}
                    >
                      <img src={icDeleteGold} alt="delete" />
                    </button>
                    <span
                      style={{
                        display: step2.firstNotes ? 'none' : 'block',
                        opacity: '1',
                      }}
                    >
                      {st1Bt}
                    </span>
                  </div>
                </div>
              ) : (
                <div
                  className="selection-scent"
                  onClick={() => (isMobile ? this.toggleScentBoxMobile() : {})}
                  style={{
                    display:
                      dataCustom.currentImg != undefined
                      && currentView === 'step3'
                        ? 'none'
                        : '',
                  }}
                >
                  <div
                    className="scent"
                    style={{
                      backgroundImage: `url(${
                        step2.firstNotes ? step2.firstNotes.image : ''
                      })`,
                      border: step2.firstNotes ? 'none' : '2px dashed #8d8d8d',
                      cursor:
                        currentView === 'step2-1' || currentView === 'step3'
                          ? 'pointer'
                          : '',
                      borderBottom: 0,
                    }}
                    onClick={() => {
                      this.state.currentView === 'step2-1'
                        ? this.setState({ currentView: 'step2' })
                        : '';
                      // (!step2.firstNotes || !step2.secondNotes) &&
                      !isMobile && currentView === 'step3'
                        ? this.showMixSection('first')
                        : (currentView === 'step3' ? this.toggleScentBoxMobile('first') : '');
                    }}
                  >
                    <button
                      style={{
                        display:
                          currentView === 'step3' && !isMobile
                            ? 'block'
                            : 'none',
                      }}
                      onClick={(e) => {
                        e.stopPropagation();
                        this.deleteNotes(1);
                      }}
                    >
                      <img src={icDeleteGold} alt="delete" />
                    </button>
                    <span
                      style={{
                        display:
                          step2.firstNotes
                            ? 'none'
                            : 'block',
                        opacity: '1',
                      }}
                    >
                      {st1Bt}
                    </span>
                  </div>
                  <div
                    className="scent"
                    style={{
                      backgroundImage: `url(${
                        step2.secondNotes ? step2.secondNotes.image : ''
                      })`,
                      border: step2.secondNotes ? 'none' : '2px dashed #8d8d8d',
                      cursor: currentView === 'step3' ? 'pointer' : 'unset',
                    }}
                    onClick={() => {
                      // (!step2.firstNotes || !step2.secondNotes) &&
                      !isMobile && currentView === 'step3'
                        ? this.showMixSection('second')
                        : (currentView === 'step3' ? this.toggleScentBoxMobile('second') : '');
                    }}
                  >
                    <button
                      style={{
                        display:
                          currentView === 'step3' && !isMobile
                            ? 'block'
                            : 'none',
                      }}
                      onClick={(e) => {
                        e.stopPropagation();
                        this.deleteNotes(2);
                      }}
                    >
                      <img src={icDeleteGold} alt="delete" />
                    </button>
                    <span
                      style={{
                        display:
                          step2.secondNotes
                            ? 'none'
                            : 'block',
                        opacity: '1',
                      }}
                    >
                      {nd2Bt}
                    </span>
                  </div>
                </div>
              )}
              <div
                className={
                  !isMobile
                    ? 'image-custom-bottle'
                    : 'image-custom-bottle-mobile'
                }
                style={{
                  display:
                    dataCustom.currentImg != undefined && currentView === 'step3'
                      ? ''
                      : 'none',
                }}
              >
                <img
                  className="image-logo  animated faster fadeIn"
                  src={
                    dataCustom.color === COLOR_CUSTOME.BLACK
                      ? logoBlack
                      : dataCustom.color === COLOR_CUSTOME.WHITE
                        ? logoWhite
                        : logoGold
                  }
                />
                <img src={dataCustom ? dataCustom.currentImg : ''} />
                <div
                  className="info-custome"
                  style={{
                    color:
                      dataCustom.color === COLOR_CUSTOME.BLACK
                        ? '#0d0d0d'
                        : dataCustom.color === COLOR_CUSTOME.WHITE
                          ? '#ffffff'
                          : '#B79B53',
                  }}
                >
                  <span style={{ fontFamily: dataCustom.font }}>
                    {dataCustom.nameBottle ? dataCustom.nameBottle : ''}
                  </span>
                  <span>Eau de parfum</span>
                  <span>25ml</span>
                </div>
              </div>
              {/* <p
                className={`btn-product-detail ${
                  isMobile ? 'btn-product-detail-mobile' : ''
                }`}
                style={{
                  display: currentView === 'step3' ? 'block' : 'none',
                  cursor: 'pointer',
                }}
                onClick={() => this.onGotoProduct(false)}
              >
                {viewDetailText}
              </p> */}
            </div>
          </div>
        ) : (
          ''
        )}

        {/* custom bottle */}
        {/* custom bottle modal */}
        {isOpenCustomeBottle ? (
          <CustomeBottleV3
            arrayThumbs={arrayThumbs}
            handleCropImage={this.handleCropImage}
            currentImg={dataCustom.currentImg}
            nameBottle={dataCustom.nameBottle}
            font={dataCustom.font}
            color={dataCustom.color}
            closePopUp={() => {
              this.setState({ isOpenCustomeBottle: false });
            }}
            onSave={this.onSaveCustomer}
            // itemBottle={{
            //   price: this.bottle ? this.bottle.items[0].price : '',
            // }}
            // cmsTextBlocks={textBlock}
            name1={step2.firstNotes ? step2.firstNotes.name : ''}
            name2={step2.secondNotes ? step2.secondNotes.name : ''}
          />
        ) : null}
        <Modal
          isOpen={isShowModalRedo}
          // toggle={this.toogleModalDetail}
          className={`modal-redo-mmo ${
            isMobile ? 'modal-redo-mmo-mobile' : ''
          } ${addFontCustom()}`}
          // size=""
          centered
        >
          <ModalBody className="body-modal-redo">
            <h3>{redoMixHeader}</h3>
            <div className="content-redo">
              {/* {htmlPare(redoContent ? redoContent : "")} */}
            </div>
          </ModalBody>

          <ModalFooter>
            <Button
              onClick={() => {
                this.toggleModalRedo();
                this.setState({ currentView: '' });
              }}
            >
              {yesImSure}
            </Button>
            <Button onClick={() => this.toggleModalRedo()}>
              {noRedo}
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    login: state.login,
    cms: state.cms,
    mmos: state.mmos,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};

// export default MultiStep;
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(MultiStep),
);
