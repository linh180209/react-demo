import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import _ from 'lodash';
// import clubBanner from '../../assets/club-banner.png';
import './styles.scss';
import Slider from 'react-slick';
import { Row, Col, Modal } from 'reactstrap';
import ScentBox from './scentBox';
import icBack from '../../../../image/icon/ic-back.svg';
import icNone from '../../../../image/icon/ic-none-like.svg';
import ProgressLine from '../../../ResultScentV2/progressLine';
import ProgressCircle from '../../../ResultScentV2/progressCircle';
import borderImage from '../../../../image/border-dashed.png';
import { isMobile } from '../../../../DetectScreen';
import icInfo from '../../../../image/icon/icInfoW.svg';
import { getNameFromButtonBlock, addFontCustom } from '../../../../Redux/Helpers';
import SelectScent from './selectScent';

const addScentNoteIntoScents = (scents, scentNotes) => {
  const scentClones = _.cloneDeep(scents);
  _.forEach(scentNotes, (d) => {
    const { name, tags } = d;
    _.forEach(tags, (tag) => {
      const { products } = tag;
      _.forEach(products, (product) => {
        const ele = _.find(scentClones, x => x.id === product.id);
        if (ele) {
          _.assign(ele, product);
          ele.tags.push({
            name,
            group: 'Scent Note',
          });
        }
      });
    });
  });
  return scentClones;
};

class Step2 extends Component {
  constructor(props) {
    super(props);
    const data = props.firstNotes ? props.firstNotes : props.secondNotes;
    this.state = {
      titleProfile: data ? data.family : '',
      name: data ? data.name : '',
      profile: data ? data.profile : [],
    };
  }

  handleSelectNoteMobile = (profile, name, title) => {
    this.setState({
      titleProfile: title,
      name,
      profile,
    });
  };

  handleClickRecomment = (data, index) => {
    const { scents, resultScentNotes, showMixSectionFrom } = this.props;
    console.log('showMixSectionFrom', showMixSectionFrom);
    const newScents = addScentNoteIntoScents(scents, resultScentNotes);
    const ele = _.find(newScents, x => x.id === data.id);
    console.log('showMixSectionFrom ele', ele);
    this.props.onClickApply(ele, isMobile ? (showMixSectionFrom === 'first' ? 1 : 2) : index);
  }

  onClickIngredient = (data) => {
    const { scents, resultScentNotes } = this.props;
    const newScents = addScentNoteIntoScents(scents, resultScentNotes);
    const ele = _.find(newScents, x => x.id === data.id);
    this.props.onClickIngredient(ele.ingredient);
  }

  renderRecommendItem = (scent, index) => {
    const { buttonBlocks } = this.props;
    const recommendBt = getNameFromButtonBlock(buttonBlocks, 'RECOMMENDED');
    const nd2Bt = getNameFromButtonBlock(buttonBlocks, '2ND INGREDIENT');
    const st1Bt = getNameFromButtonBlock(buttonBlocks, '1ST INGREDIENT');
    const selectBt = getNameFromButtonBlock(buttonBlocks, 'SELECT INGREDIENT');
    return (
      <div className="recommend-item">
        <h3>
          {recommendBt}
        </h3>
        <div className="list-item">
          {
          _.map(scent.suggestions, d => (
            <div className="item" onClick={() => this.handleClickRecomment(d, index)}>
              <img loading="lazy" src={d.image} alt="scent" />
              <button
                data-gtmtracking={index === 1 ? 'funnel-3-step-03-recommended-scent-2-added-to-mix' : 'funnel-3-step-05-recommended-scent-2-added-to-mix'}
                type="button"
                className="button-bg__none"
                onClick={(e) => {
                  e.stopPropagation();
                  this.onClickIngredient(d);
                }}
              >
                <img src={icInfo} alt="info" />
              </button>
            </div>
          ))
        }
        </div>
        <div className="div-select">
          <h4>{index === 1 ? st1Bt : nd2Bt}</h4>
          <button
            data-gtmtracking={index === 1 ? 'funnel-3-step-04-select-scent-1' : 'funnel-3-step-04-select-scent-2'}
            type="button"
            onClick={() => this.props.onClickSelectScent(index)}
          >
            {selectBt}
          </button>
        </div>
      </div>
    );
  }

  render() {
    const {
      dataScentNote,
      bottleGif,
      isDay,
      switchView,
      selectNotesStep2,
      isNextView,
      isOpenScentBoxMobile,
      currentView,
      buttonBlocks,
      headerAndParagraphBocks,
      dataStep2,
      isDeleted,
      activeScentDeskFrom,
      strengthBt,
      durationBt,
      firstNotes,
      secondNotes,
    } = this.props;
    console.log('firstNotes', firstNotes);
    const { titleProfile, name, profile } = this.state;
    const bottle = bottleGif
      ? _.filter(bottleGif, x => x.value.caption === 'bottle-gif')[0].value
        .image
      : '';
    // console.log("dataScentNote", dataScentNote);

    const backText = _.filter(buttonBlocks, x => x.value.name === 'back')[0]
      .value.text;
    const theScentBarText = _.filter(
      buttonBlocks,
      x => x.value.name === 'scentbar_title',
    )[0].value.text;
    const startingOf = _.filter(
      buttonBlocks,
      x => x.value.name === 'choose1_guide_title',
    )[0].value.text;
    const contentStarting = _.filter(
      headerAndParagraphBocks,
      x => x.value.header.header_text === 'mainscent_guide_content',
    )[0]
      ? _.filter(
        headerAndParagraphBocks,
        x => x.value.header.header_text === 'mainscent_guide_content',
      )[0].value.paragraph
      : '';
    const choose2GuideTitle = _.filter(
      buttonBlocks,
      x => x.value.name === 'choose2_guide_title',
    )[0].value.text;
    const contentStep2 = _.filter(
      headerAndParagraphBocks,
      x => x.value.header.header_text === 'choose2_guide_content',
    )[0]
      ? _.filter(
        headerAndParagraphBocks,
        x => x.value.header.header_text === 'choose2_guide_content',
      )[0].value.paragraph
      : '';
    const mainScentText = _.filter(
      buttonBlocks,
      x => x.value.name === 'main_scent_title',
    )[0].value.text;
    const secondaryScentText = _.filter(
      buttonBlocks,
      x => x.value.name === 'secondary_title',
    )[0].value.text;
    const choose1stIngredientText = _.filter(
      buttonBlocks,
      x => x.value.name === 'choose1st_btn',
    )[0].value.text;
    const choose2ndIngredientText = _.filter(
      buttonBlocks,
      x => x.value.name === 'choose_2nd_btn',
    )[0].value.text;
    const heartNoteText = _.filter(dataScentNote, x => x.type == 'heart')[0]
      .name;
    const freshNoteText = _.filter(dataScentNote, x => x.type == 'top')[0]
      .name;
    const sensualNoteText = _.filter(dataScentNote, x => x.type == 'bot')[0]
      .name;
    const chooseyourText = _.find(
      buttonBlocks,
      x => x.value.name === 'choose_your',
    )
      ? _.find(buttonBlocks, x => x.value.name === 'choose_your').value.text
      : '';
    const selectIngridientsToViewCharacters = _.find(
      buttonBlocks,
      x => x.value.name === 'select_ingridients_to_view_characters',
    )
      ? _.find(
        buttonBlocks,
        x => x.value.name === 'select_ingridients_to_view_characters',
      ).value.text
      : '';

    const nd2Bt = getNameFromButtonBlock(buttonBlocks, '2ND INGREDIENT');
    const st1Bt = getNameFromButtonBlock(buttonBlocks, '1ST INGREDIENT');
    const selectBt = getNameFromButtonBlock(buttonBlocks, 'SELECT INGREDIENT');
    const chooseBt = getNameFromButtonBlock(buttonBlocks, 'Choose your ingredients');

    let data = [];
    switch (isDay) {
      case true:
        data = dataScentNote
          ? _.filter(dataScentNote, x => x.type === 'top')[0]
          : '';
        break;
      case false:
        data = dataScentNote
          ? _.filter(dataScentNote, x => x.type === 'bot')[0]
          : '';
        break;
      default:
        data = dataScentNote
          ? _.filter(dataScentNote, x => x.type === 'heart')[0]
          : '';
        break;
    }
    if (this.props.isFullFressSensual) {
      const dataTop = _.filter(
        _.cloneDeep(dataScentNote),
        x => x.type === 'top',
      )[0];
      _.forEach(dataTop.tags, (x) => {
        _.assign(x, { tag: 'FRESH' });
      });
      const dataBot = _.filter(
        _.cloneDeep(dataScentNote),
        x => x.type === 'bot',
      )[0];
      _.forEach(dataBot.tags, (x) => {
        _.assign(x, { tag: 'SENSUAL' });
      });
      dataTop.tags = dataTop.tags.concat(dataBot.tags);
      data = _.cloneDeep(dataTop);
    }
    const guildBox = (
      <div className="animated faster fadeInRight h-100">
        {currentView === 'step2' ? (
          <div className="guild-box h-100">
            <h2>{startingOf}</h2>
            {htmlPare(contentStarting || '')}
          </div>
        ) : (
          <div className="guild-box h-100">
            <h2>{choose2GuideTitle}</h2>
            {htmlPare(contentStep2 || '')}
          </div>
        )}
      </div>
    );
    const htmlWeb = (
      <div className="step2 animated faster fadeInRight">
        <div className="div-title-header">
          <h1 className="title">
            <button
              type="button"
              className={firstNotes || secondNotes ? 'btn-back' : 'hidden'}
              onClick={() => {
                this.props.resetData();
              }}
            >
              <img src={icBack} alt="back" />
              <span>{backText}</span>
            </button>
            {theScentBarText}
            <h4>
              {chooseBt}
            </h4>
          </h1>
        </div>
        <div className="scent-bar">
          <Row className="w-100 d-flex justify-content-between items-center">
            <Col md="4" style={{ position: 'relative' }}>
              <div
                className="wrapper-scent-info"
                style={{
                  background:
                  !firstNotes && !secondNotes
                    ? `url("${borderImage}")`
                    : 'none',
                  backgroundSize: '100% 100%',
                }}
              >
                {!firstNotes ? (
                  <React.Fragment>
                    {
                      secondNotes ? (
                        this.renderRecommendItem(_.find(this.props.mmos, x => x.id === secondNotes.id), 1)
                      ) : (
                        <div className="div-none-like">
                          <h3>
                            {st1Bt}
                          </h3>
                          <img src={icNone} alt="icon" />
                          <span>
                            {selectIngridientsToViewCharacters || 'Select ingridients to view their characters here'}
                          </span>
                          <button
                            data-gtmtracking="funnel-3-step-02-select-scent-1"
                            type="button"
                            onClick={() => this.props.onClickSelectScent(1)}
                          >
                            {selectBt}
                          </button>
                        </div>
                      )
                    }
                  </React.Fragment>
                ) : (
                  <div className="animated faster fadeInRight h-100">
                    {/* {guildBox} */}
                    <div className="scent-info scent-info-desk">
                      <div className="div-scent-profile animated faster fadeIn">
                        {name && titleProfile && (
                        <div className="title-info">
                          <h4>{name}</h4>
                          <span>{titleProfile || ''}</span>
                        </div>
                        )}
                        <div className="div-progress">
                          <div className="div-process-line">
                            {_.map(profile ? profile?.accords : [], x => (
                              <ProgressLine data={x} isShowPercent />
                            ))}
                          </div>
                          {profile?.strength && profile?.duration ? (
                            <div className="div-process-circle">
                              <ProgressCircle
                                title={strengthBt}
                                percent={
                                profile
                                  ? parseInt(
                                    parseFloat(profile?.strength) * 100,
                                    10,
                                  )
                                  : 0
                              }
                              />
                              <ProgressCircle
                                title={durationBt}
                                percent={
                                profile
                                  ? parseInt(
                                    parseFloat(profile?.duration) * 100,
                                    10,
                                  )
                                  : 0
                              }
                              />
                            </div>
                          ) : (
                            ''
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </Col>
            <Col md="4">
              <div
                className="wrapper-scent-info"
                style={{
                  background:
                    !secondNotes && !firstNotes
                      ? `url("${borderImage}")`
                      : 'none',
                  backgroundSize: '100% 100%',
                }}
              >
                {!secondNotes ? (
                  <React.Fragment>
                    {
                    firstNotes ? (
                      this.renderRecommendItem(_.find(this.props.mmos, x => x.id === firstNotes.id), 2)
                    ) : (
                      <div className="div-none-like">
                        <h3>
                          {nd2Bt}
                        </h3>
                        <img src={icNone} alt="icon" />
                        <span>
                          {selectIngridientsToViewCharacters || 'Select ingridients to view their characters here'}
                        </span>
                        <button type="button" onClick={() => this.props.onClickSelectScent(2)}>
                          {selectBt}
                        </button>
                      </div>
                    )
                  }
                  </React.Fragment>
                ) : (
                  <div className="animated faster fadeInRight h-100">
                    <div className="scent-info scent-info-desk">
                      <div className="div-scent-profile animated faster fadeIn">
                        {name && titleProfile && (
                        <div className="title-info">
                          <h4>{name}</h4>
                          <span>{titleProfile || ''}</span>
                        </div>
                        )}
                        <div className="div-progress">
                          <div className="div-process-line">
                            {_.map(profile ? profile?.accords : [], x => (
                              <ProgressLine data={x} isShowPercent />
                            ))}
                          </div>
                          {profile?.strength && profile?.duration ? (
                            <div className="div-process-circle">
                              <ProgressCircle
                                title={strengthBt}
                                percent={
                                profile
                                  ? parseInt(
                                    parseFloat(profile?.strength) * 100,
                                    10,
                                  )
                                  : 0
                              }
                              />
                              <ProgressCircle
                                title={durationBt}
                                percent={
                                profile
                                  ? parseInt(
                                    parseFloat(profile?.duration) * 100,
                                    10,
                                  )
                                  : 0
                              }
                              />
                            </div>
                          ) : (
                            ''
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
    const htmlMobile = (
      <div className="step2 animated faster fadeInRight step2-mobile">
        <div className={this.props.isNonHeader ? 'hidden' : 'div-title-header'}>
          <h1 className="title">
            <button
              type="button"
              className={firstNotes || secondNotes ? 'btn-back' : 'hidden'}
              onClick={() => {
                this.props.resetData();
              }}
            >
              <img src={icBack} alt="back" />
              <span>{backText}</span>
            </button>
            {theScentBarText}
          </h1>
          <h4>
            {chooseBt}
          </h4>
        </div>
        <div className="scent-info">
          <div
            className="div-scent-profile animated faster fadeIn"
            style={{
              display: currentView !== 'step2-1' && 'none',
              animationDelay: '1s',
            }}
          >
            <h4>{dataStep2.firstNotes ? dataStep2.firstNotes.name : ''}</h4>
            <span>
              {dataStep2.firstNotes && dataStep2.firstNotes.family
                ? dataStep2.firstNotes.family
                : ''}
            </span>
            <div className="divider-scent-info" />
            <div className="div-process-circle">
              <ProgressCircle
                title={strengthBt}
                percent={
                  profile
                    ? parseInt(
                      parseFloat(
                        dataStep2.firstNotes
                          ? dataStep2.firstNotes.profile?.strength
                          : '',
                      ) * 100,
                      10,
                    )
                    : 0
                }
              />
              <ProgressCircle
                title={durationBt}
                percent={
                  profile
                    ? parseInt(
                      parseFloat(
                        dataStep2.firstNotes
                          ? dataStep2.firstNotes.profile?.duration
                          : '',
                      ) * 100,
                      10,
                    )
                    : 0
                }
              />
            </div>
            <div className="div-progress">
              <div className="div-process-line">
                {_.map(
                  dataStep2.firstNotes
                    ? dataStep2.firstNotes.profile?.accords
                    : [],
                  x => (
                    <ProgressLine data={x} isShowPercent />
                  ),
                )}
              </div>
            </div>
          </div>
        </div>
        {currentView === 'step2' ? (
          <button
            data-gtmtracking="funnel-3-step-02-select-scent-1"
            type="button"
            className="btn-toggle-scent-bar"
            onClick={() => { this.props.toggleScentBoxMobile('first'); }}
          >
            {choose1stIngredientText}
          </button>
        ) : (
          <button
            data-gtmtracking="funnel-3-step-02-select-scent-2"
            type="button"
            className="btn-toggle-scent-bar"
            onClick={() => { this.props.toggleScentBoxMobile('second'); }}
          >
            {choose2ndIngredientText}
          </button>
        )}
        {
        this.props.isShowOnlyScentNotes ? (
          <Modal
            isOpen={isOpenScentBoxMobile}
            toggle={this.props.toggleScentBoxMobile}
            className={`modal-scent-mobile animated faster ${
              this.props.delaySlideModalMobile ? 'slideInUp' : 'slideOutDown'
            } ${addFontCustom()}`}
            fade={false}
          >
            <ScentBox
              dataScentNote={dataScentNote}
              isDay={isDay}
              data={data}
              switchView={switchView}
              selectNotesStep2={selectNotesStep2}
              isNextView={isNextView}
              toggleScentBoxMobile={this.props.toggleScentBoxMobile}
              handleSelectNoteMobile={this.handleSelectNoteMobile}
              currentView={currentView}
              dataStep2={dataStep2}
              buttonBlocks={buttonBlocks}
              isDeleted={isDeleted}
              onClickIngredient={this.props.onClickIngredient}
            />
          </Modal>
        ) : (
          <Modal
            isOpen={isOpenScentBoxMobile}
            toggle={this.props.toggleScentBoxMobile}
            className={`modal-scent-mobile-v2 ${addFontCustom()}`}
          >
            <SelectScent
              scentNotes={this.props.resultScentNotes}
              buttonBlocks={this.props.buttonBlocks}
              onClickApply={this.handleClickRecomment}
              onClickCancel={this.props.toggleScentBoxMobile}
              onClickIngredient={this.props.onClickIngredient}
              isHeaderMobile
              indexSelect={this.props.showMixSectionFrom === 'first' ? 1 : 2}
              mmos={this.props.mmos}
              scentRecommend={secondNotes && this.props.showMixSectionFrom === 'first'
                ? _.find(this.props.mmos, x => x.id === secondNotes.id)
                : firstNotes && this.props.showMixSectionFrom === 'second' ? _.find(this.props.mmos, x => x.id === firstNotes.id) : {}}
            />
          </Modal>
        )
      }

        {/* <Modal
          isOpen={isOpenScentBoxMobile}
          toggle={this.props.toggleScentBoxMobile}
          className={`modal-scent-mobile animated faster ${
            this.props.delaySlideModalMobile ? 'slideInUp' : 'slideOutDown'
          }`}
          fade={false}
        >
          <ScentBox
            dataScentNote={dataScentNote}
            isDay={isDay}
            data={data}
            switchView={switchView}
            selectNotesStep2={selectNotesStep2}
            isNextView={isNextView}
            toggleScentBoxMobile={this.props.toggleScentBoxMobile}
            handleSelectNoteMobile={this.handleSelectNoteMobile}
            currentView={currentView}
            dataStep2={dataStep2}
            buttonBlocks={buttonBlocks}
            isDeleted={isDeleted}
            onClickIngredient={this.props.onClickIngredient}
          />
        </Modal> */}
      </div>
    );
    return !isMobile ? htmlWeb : htmlMobile;
  }
}

export default Step2;
