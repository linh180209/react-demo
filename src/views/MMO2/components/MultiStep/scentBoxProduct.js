import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import icInfo from '../../../../image/icon/icNewInfo.svg';
import icChecked from '../../../../image/icon/ic-checked-quiz.svg';
import icBack from '../../../../image/icon/back-product.svg';
import icNext from '../../../../image/icon/ic-next-mmo.svg';
import ProgressLine from '../../../ResultScentV2/progressLine';
import icNextWhite from '../../../../image/icon/ic-next-white.svg';
import ProgressCircle from '../../../ResultScentV2/progressCircle';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import { isBrowser, isMobile } from '../../../../DetectScreen';

class ScentBoxProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataNote: [],
      currentView: '',
      disbleBtn: false,
      // data: this.props.dataScentNote[1],
    };
  }

  handleSelectNotes = (name, data) => {
    this.setState({
      currentView: 'product',
      dataNote: _.filter(data.tags, x => x.name === name)[0],
    });
  };

  handleSelectProduct = (data) => {
    console.log('data', data);
    // const dataProfile = _.filter(
    //   this.state.dataNote.products,
    //   x => x.id === id,
    // )[0];
    // this.setState({
    //   profile: dataProfile.profile,
    //   name: dataProfile.ingredient.name,
    //   titleProfile: title,
    //   isChecked: id,
    // });
    // isMobile && this.props.currentView != 'step3'
    //   ? this.props.handleSelectNoteMobile(
    //     dataProfile.profile,
    //     dataProfile.ingredient.name,
    //     title,
    //   )
    //   : '';
    // console.log('this.props.showMixSectionFrom', this.props.showMixSectionFrom);
    // setTimeout(
    //   () => {
    //     this.props.currentView != 'step3'
    //       ? this.props.selectNotesStep2(
    //         data,
    //         this.props.isDay != null,
    //       )
    //       : this.props.selectNotesStep2(
    //         data,
    //         this.props.showMixSectionFrom === 'second',
    //       );

    // !isMobile
    //   ? this.props.switchView(
    //     this.props.isDay === null ? 'step2-1' : 'step3',
    //     true,
    //     false,
    //   )
    //   : this.props.switchView(
    //     this.props.isDay === null
    //         && this.props.dataStep2.secondNotes === null
    //       ? 'step2-1'
    //       : 'step3',
    //     true,
    //     false,
    //   );
    // this.setState({
    //   currentView: '',
    // });
    //     isMobile ? this.props.toggleScentBoxMobile() : '';

    //     this.setState({ disbleBtn: false, isChecked: null });
    //   },
    //   this.props.currentView != 'step3' ? 500 : 0,
    // );
  };

  onClickIngredient = (item) => {
    console.log('onClickIngredient 1', item);
    if (this.props.onClickIngredient) {
      this.props.onClickIngredient(item.ingredient);
      console.log('onClickIngredient', item);
    }
  }

  render() {
    const {
      dataScentNote,
      isDay,
      data,
      buttonBlocks,
      scentSelected,
    } = this.props;
    console.log('isDay', isDay);
    const {
      dataNote,
    } = this.state;
    // console.log("dataScentNote", dataScentNote);
    const gifHeart = dataScentNote
      ? _.filter(dataScentNote, x => x.type === 'heart')[0].image
      : '';
    const heartNoteText = dataScentNote
      ? _.filter(buttonBlocks, x => x.value.name === 'heart_note_choose')[0]
        .value.text
      : '';
    const heartNoteDesText = dataScentNote
      ? _.filter(buttonBlocks, x => x.value.name === 'heart_note_cont')[0]
        .value.text
      : '';

    const gifTop = dataScentNote
      ? _.filter(dataScentNote, x => x.type === 'top')[0].image
      : '';
    const freshNoteText = dataScentNote
      ? _.filter(buttonBlocks, x => x.value.name === 'fresh_title')[0].value
        .text
      : '';
    const freshNoteDesText = dataScentNote
      ? _.filter(buttonBlocks, x => x.value.name === 'fresh_content')[0].value
        .text
      : '';

    const gifBot = dataScentNote
      ? _.filter(dataScentNote, x => x.type === 'bot')[0].image
      : '';
    const sensualNoteText = dataScentNote
      ? _.filter(buttonBlocks, x => x.value.name === 'sensual_title')[0].value
        .text
      : '';
    const sensualNoteDesText = dataScentNote
      ? _.filter(buttonBlocks, x => x.value.name === 'sensual_content')[0]
        .value.text
      : '';

    const chooseyourText = _.find(
      buttonBlocks,
      x => x.value.name === 'choose_your',
    )
      ? _.find(buttonBlocks, x => x.value.name === 'choose_your').value.text
      : '';

    const gitShow = isDay === undefined ? gifHeart : isDay ? gifTop : gifBot;
    const switchView = () => {
      switch (this.state.currentView) {
        case 'main-notes':
          return (
            <div className="main-scent-note">
              <div
                className="main-scent-note-title"
                onClick={() => {
                  this.setState({ currentView: '' });
                  this.props.onClickShowFullBox2(0);
                }}
              >
                <img src={icBack} alt="back" />
                <h3>
                  {data.type === 'heart'
                    ? heartNoteText
                    : data.type === 'top'
                      ? freshNoteText
                      : sensualNoteText}
                </h3>
              </div>
              <div className="items-main-scent">
                {_.map(data.tags, item => (
                  <div
                    style={{
                      backgroundImage: `url(${item.image ? item.image : ''})`,
                      cursor: 'pointer',
                    }}
                    className="item-main-scent animated faster fadeIn"
                    onClick={(e) => {
                      this.handleSelectNotes(item.name, data);
                    }}
                  >
                    <h1>{item.name}</h1>
                    <img
                      src={icNextWhite}
                      alt="Next"
                      style={{
                        position: 'absolute',
                        right: '2rem',
                        width: '12px',
                      }}
                    />
                  </div>
                ))}
              </div>
            </div>
          );
        case 'product':
          return (
            <div className="product">
              {isMobile ? (
                <div
                  className="product-note-title"
                  onClick={e => this.props.toggleScentBoxMobile()}
                >
                  <div className="divider-scent-mobile" />
                  <h4 className="m-0">
                    <span
                      style={{
                        textTransform: 'uppercase',
                        letterSpacing: '0.1em',
                        fontWeight: 'normal',
                      }}
                    >
                      {chooseyourText}
                    </span>
                    {' '}
                    <span
                      style={{
                        textTransform: 'uppercase',
                        fontWeight: '500',
                        letterSpacing: '0.1em',
                      }}
                    >
                      {data.type === 'heart'
                        ? heartNoteText
                        : data.type === 'top'
                          ? freshNoteText
                          : sensualNoteText}
                    </span>
                  </h4>
                </div>
              ) : (
                ''
              )}
              <div
                className="product-title"
                onClick={e => (!isMobile
                  ? this.setState({
                    currentView: 'main-notes',
                  })
                  : this.setState({ currentView: '' }))
                }
              >
                <img src={icBack} alt="back" />
                <h3>{dataNote.name}</h3>
              </div>
              <div className="product-note">
                {_.map(dataNote.products, item => (
                  <div
                    className="item-note enable_hover animated faster fadeIn"
                    onClick={() => {
                      if (!this.state.disbleBtn) {
                        // this.setState({ disbleBtn: true });
                        this.props.selectScent(item);
                      }
                    }}
                  >
                    <div className="div-image">
                      <img loading="lazy" src={item.image ? item.image : ''} />
                    </div>
                    <button className="button-bg__none" type="button" onClick={(e) => { e.stopPropagation(); this.onClickIngredient(item); }}>
                      <img src={icInfo} alt="info" />
                    </button>
                    <div
                      className="div-bg"
                      style={{
                        opacity: item.id === scentSelected.id ? '0.7' : '0',
                        transition: item.id === scentSelected.id ? '0.2' : '0',
                      }}
                    />
                    <img
                      className="checked"
                      src={icChecked}
                      alt="checked"
                      style={{
                        visibility:
                          item.id === scentSelected.id ? 'visible' : 'hidden',
                      }}
                    />
                    <span>
                      {item.family && isBrowser
                        ? item.family
                        : ''}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          );
        default:
          return !isMobile ? (
            <div
              className={`title-scent animated faster ${
                !isMobile ? 'fadeInRight' : ''
              }`}
              onClick={() => {
                this.setState({ currentView: 'main-notes' });
                this.props.onClickShowFullBox2(isDay ? 1 : 2);
              }}
              style={{ cursor: 'pointer' }}
            >
              <img src={gitShow || ''} />
              <div>
                <h3>
                  {data.type === 'heart'
                    ? heartNoteText
                    : data.type === 'top'
                      ? freshNoteText
                      : sensualNoteText}
                  <img
                    src={icNext}
                    alt="Next"
                    style={{
                      height: 'auto',
                      margin: '0 0 5px 16px',
                      width: '10px',
                    }}
                  />
                </h3>
                <p>
                  {data.type === 'heart'
                    ? heartNoteDesText
                    : data.type === 'top'
                      ? freshNoteDesText
                      : sensualNoteDesText}
                </p>
              </div>
            </div>
          ) : (
            <div className="main-scent-note">
              <div
                className="main-scent-note-title"
                onClick={e => this.props.toggleScentBoxMobile()}
              >
                <div className="divider-scent-mobile" />
                <h4 className="m-0">
                  <span
                    style={{
                      textTransform: 'uppercase',
                      letterSpacing: '0.1em',
                      fontWeight: 'normal',
                    }}
                  >
                    {chooseyourText}
                  </span>
                  {' '}
                  <span
                    style={{
                      textTransform: 'uppercase',
                      fontWeight: '500',
                      letterSpacing: '0.1em',
                    }}
                  >
                    {data.type === 'heart'
                      ? heartNoteText
                      : data.type === 'top'
                        ? freshNoteText
                        : sensualNoteText}
                  </span>
                </h4>
              </div>
              <div className="items-main-scent">
                {_.map(data.tags, item => (
                  <div
                    style={{
                      backgroundImage: `url(${item.image ? item.image : ''})`,
                      cursor: 'pointer',
                    }}
                    className="item-main-scent animated faster fadeIn"
                    onClick={e => this.handleSelectNotes(item.name, data)}
                  >
                    <h1>{item.name}</h1>
                    <img
                      src={icNextWhite}
                      alt="Next"
                      style={{
                        position: 'absolute',
                        right: '2rem',
                        width: '12px',
                      }}
                    />
                  </div>
                ))}
              </div>
            </div>
          );
      }
    };
    return (
      <div className={`h-100 ${isMobile ? 'w-100' : ''}`}>
        <div className="scent-box">{switchView()}</div>
        {/* {isChecked && !isMobile && this.state.currentView === 'product' ? (
          <div className="scent-info scent-info-desk">
            <div className="div-scent-profile animated faster fadeIn">
              <h4>{name}</h4>
              <span>{titleProfile || ''}</span>
              <div className="div-progress">
                <div className="div-process-line">
                  {_.map(profile ? profile.accords : [], x => (
                    <ProgressLine data={x} isShowPercent />
                  ))}
                </div>
                <div className="div-process-circle">
                  <ProgressCircle
                    title="STRENGTH"
                    percent={
                      profile
                        ? parseInt(parseFloat(profile.strength) * 100, 10)
                        : 0
                    }
                  />
                  <ProgressCircle
                    title="DURATION"
                    percent={
                      profile
                        ? parseInt(parseFloat(profile.duration) * 100, 10)
                        : 0
                    }
                  />
                </div>
              </div>
            </div>
          </div>
        ) : (
          ''
        )} */}
      </div>
    );
  }
}

export default ScentBoxProduct;
