import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
// import clubBanner from '../../assets/club-banner.png';
import './styles.scss';
import Slider from 'react-slick';
// import { generaCurrency } from '../../../../Redux/Helpers';
import icFresh from '../../../../image/icon/ic-fresh.svg';
import icFreshActive from '../../../../image/icon/ic-fresh-active.svg';
import icSeductive from '../../../../image/icon/ic-seductive.svg';
import icSeductiveActive from '../../../../image/icon/ic-seductive-active.svg';
import icCheckedMmo from '../../../../image/icon/ic-checked-mmo.svg';
import icBack from '../../../../image/icon/ic-back.svg';

class Step1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActiveFresh: false,
      isActiveSeductive: false,
    };
    this.handleActiveFresh = this.handleActiveFresh.bind(this);
    this.handleActiveSeductive = this.handleActiveSeductive.bind(this);
  }

  handleActiveFresh() {
    this.setState({ isActiveFresh: true, isActiveSeductive: false });
    this.props.selectStep1(true);
    setTimeout(() => {
      this.props.switchView('step2', true);
    }, 500);
  }

  handleActiveSeductive() {
    this.setState({ isActiveSeductive: true, isActiveFresh: false });
    this.props.selectStep1(false);
    setTimeout(() => {
      this.props.switchView('step2', true);
    }, 500);
  }

  render() {
    const { isActiveFresh, isActiveSeductive } = this.state;
    const { isNextView, isDay, buttonBlocks } = this.props;

    const backText = _.filter(buttonBlocks, x => x.value.name == 'back')[0]
      .value.text;
    const chooseModeText = _.filter(
      buttonBlocks,
      x => x.value.name == 'choose_mood',
    )[0].value.text;
    const freshAndElegantText = _.filter(
      buttonBlocks,
      x => x.value.name == 'choose_day_title',
    )[0].value.text;
    const dayPerfumeText = _.filter(
      buttonBlocks,
      x => x.value.name == 'choose_mood_day',
    )[0].value.text;
    const seductiveAndActtactiveText = _.filter(
      buttonBlocks,
      x => x.value.name == 'choose_night_title',
    )[0].value.text;
    const nightPerfumeText = _.filter(
      buttonBlocks,
      x => x.value.name == 'choose_night',
    )[0].value.text;

    return (
      <div
        className={`step1 animated faster ${
          isNextView ? 'fadeInRight' : 'fadeInLeft'
        }`}
      >
        <div className="div-title-header">
          <h1 className="title">
            <button
              className="btn-back"
              onClick={() => this.props.switchView('', false)}
            >
              <img src={icBack} alt="back" />
              <span>{backText}</span>
            </button>
            {chooseModeText}
          </h1>
        </div>
        <div className="mode-step">
          <div className="mode-left">
            <button
              onClick={() => this.handleActiveFresh()}
              style={{
                color: isActiveFresh || isDay == true ? '#b79a53' : '#0d0d0d',
              }}
            >
              <img
                src={isActiveFresh || isDay == true ? icFreshActive : icFresh}
                alt="icFresh"
              />
              <h3>{freshAndElegantText}</h3>
              <p>{dayPerfumeText}</p>
              <img src={isActiveFresh || isDay == true ? icCheckedMmo : ''} />
            </button>
          </div>
          <div className="divider-verticle" />
          <div className="mode-right">
            <button
              onClick={() => this.handleActiveSeductive()}
              style={{
                color:
                  isActiveSeductive || isDay == false ? '#b79a53' : '#0d0d0d',
              }}
            >
              <img
                src={
                  isActiveSeductive || isDay == false
                    ? icSeductiveActive
                    : icSeductive
                }
                alt="icSeductive"
              />
              <h3>{seductiveAndActtactiveText}</h3>
              <p>{nightPerfumeText}</p>
              <img
                src={isActiveSeductive || isDay == false ? icCheckedMmo : ''}
              />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Step1;
