import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import {
  Player, ControlBar, Shortcut, BigPlayButton, LoadingSpinner,
} from 'video-react';
// import clubBanner from '../../assets/club-banner.png';
import './styles.scss';
import Slider from 'react-slick';
import { isMobile } from '../../../../DetectScreen';
// import { generaCurrency } from '../../../../Redux/Helpers';

class StartVideo extends Component {
  render() {
    const {
      video, headerCms, switchView, buttonBlocks,
    } = this.props;
    const startText = _.filter(buttonBlocks, x => x.value.name == 'start');
    return (
      <div className="introduce-mmo-start">
        <div className="div-title">
          <h1 className="title">{headerCms}</h1>
          {/* <span className="description">
            {subTextCms && subTextCms.length > 0 ? subTextCms[0].value : ""}
          </span> */}
        </div>
        <div className="video-wrapper">
          <Player
            ref="player"
            playsInline
            loop
            muted
            autobuffer
            disablePauseOnClick
            autoPlay
            src={video}
            fluid={false}
            width={isMobile ? '100%' : '65%'}
          >
            {/* <ControlBar disableCompletely disableDefaultControls /> */}
            <LoadingSpinner />
            <Shortcut dblclickable clickable={false} />
            <BigPlayButton position="center" />
          </Player>
        </div>
        <button className="btn-start" onClick={() => switchView('step1', true)}>
          {startText ? startText[0].value.text : ''}
        </button>
      </div>
    );
  }
}

export default StartVideo;
