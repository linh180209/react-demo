import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';

import AcademyItem from '../../components/AcademyItem';
import HeaderHomePage from '../../components/HomePage/header';
import {
  scrollTop, fetchCMSHomepage, getSEOFromCms, googleAnalitycs,
} from '../../Redux/Helpers/index';
import addCmsRedux from '../../Redux/Actions/cms';
import auth from '../../Redux/Helpers/auth';
import { isMobile, isTablet } from '../../DetectScreen';
import FooterV2 from '../../views2/footer';

const getCms = (academyCms) => {
  const objectReturn = {
    headerText: '',
    textBlock: '',
    lessonBlock: [],
    seo: undefined,
  };
  if (!_.isEmpty(academyCms)) {
    const seo = getSEOFromCms(academyCms);
    const { header_text: headerText, body } = academyCms;
    _.assign(objectReturn, { headerText });
    if (body) {
      const textBlock = _.find(body, x => x.type === 'text_block');
      const lessonBlock = _.filter(body, x => x.type === 'lesson_block');
      _.assign(objectReturn, { textBlock, lessonBlock, seo });
    }
  }
  return objectReturn;
};

class Academy extends Component {
  state = {
    seo: undefined,
  }

  componentDidMount() {
    scrollTop();
    googleAnalitycs('/academy');
    this.fetchDataInit();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const academyCms = _.find(cms, x => x.title === 'Academy');
    if (!academyCms) {
      const cmsData = await fetchCMSHomepage('academy');
      this.props.addCmsRedux(cmsData);
      const {
        headerText, textBlock, lessonBlock, seo,
      } = getCms(cmsData);
      this.setState({
        headerText, textBlock, lessonBlock, seo,
      });
    } else {
      const {
        headerText, textBlock, lessonBlock, seo,
      } = getCms(academyCms);
      this.setState({
        headerText, textBlock, lessonBlock, seo,
      });
    }
  }

  render() {
    const width = isMobile && !isTablet ? 590 / 1.7 : 590;
    const height = isMobile && !isTablet ? 268 / 1.7 : 268;
    const {
      headerText, textBlock, lessonBlock, seo,
    } = this.state;
    const workshop = this.props.match.params.workshop;
    const htmlWeb = (
      <div
        className="div-col div-academy-page justify-center items-center"
      >
        <h1 className="title">
          {headerText}
        </h1>
        <span
          className="mt-4 mb-2"
          style={{
            width: `${width}px`,
            textAlign: 'center',
            fontSize: isMobile && !isTablet ? '0.6rem' : '1rem',
          }}
        >
          {textBlock ? textBlock.value : ''}
        </span>
        {
          _.map(lessonBlock, (d, index) => (
            <AcademyItem width={width} height={height} data={lessonBlock ? d.value : undefined} workshop={workshop} index={index} />
          ))
        }
      </div>
    );
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
        </MetaTags>
        {/* <HeaderHomePage /> */}
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        {htmlWeb}
        <FooterV2 />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};
Academy.defaultProps = {
  match: {
    params: {
      workshop: '',
    },
  },
};
Academy.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      workshop: PropTypes.string,
    }),
  }),
};

export default connect(mapStateToProps, mapDispatchToProps)(Academy);
