import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import windowSize from 'react-window-size';
import { Row, Col, Button } from 'reactstrap';
// import Header from '../../components/Header';
import BackGroundVideo from '../../components/backgroundVideo';
import urlWeb from '../../image/bg_web.mp4';
import urlIpad from '../../image/bg_ipad.mp4';
import urlMobile from '../../image/bg_mobile.mp4';
import CardOption from '../../components/HomePage/CardOption';
import InfluencerItem from '../../components/InfluencerItem';
import { generateUrlWeb } from '../../Redux/Helpers';

class HomePage extends Component {
  render() {
    const { windowWidth } = this.props;
    const urlBg = windowWidth < 600 ? urlMobile : (windowWidth > 768 && windowWidth < 992 ? urlIpad : urlWeb);
    return (
      <div>
        <div className="container-home">
          <BackGroundVideo url={urlBg} />
          <span className="text--top__fix">
            21g
          </span>
          <div className="container-bottom_screen">
            <span>
              start
            </span>
            <i className="fa fa-angle-down" />
          </div>
        </div>
        <div className="container-home__hide">
          {/* <Header /> */}
          <div className="top-info">
            <span>
              earn 10 give 5
            </span>
          </div>
          <div className="body">
            <div className="container_full body-row">
              <Row className="container_full">
                <Col md="4" xs="12">
                  <CardOption
                    title="Scent Designer"
                    name="Let Us Guide you"
                    description="Lorem ipsum dolor sit amet, conse"
                    nameBt="START QUIZ"
                  />
                </Col>
                <Col md="4" xs="12">
                  <CardOption
                    title="Scent Designer"
                    name="Based On Your Mood"
                    description="Lorem ipsum dolor sit amet, conse"
                    nameBt="MY OCCASION"
                  />
                </Col>
                <Col md="4" xs="12">
                  <CardOption
                    title="Scent Designer"
                    name="Make My Own"
                    description="Lorem ipsum dolor sit amet, conse"
                    nameBt="MAKE MY OWN"
                  />
                </Col>
              </Row>
            </div>
          </div>
        </div>
        <div className="body-how">
          <div className="div1">
            <span className="title1">
              About Us
            </span>
            <span className="title2">
              How it works
            </span>
          </div>
          <div className="div2">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo
          </div>
          <div className="div3">
            <Button type="button" className="bt-find">
              FIND OUT MODE
            </Button>
          </div>
          <div className="div4">
            <div className="div-image">
              <img loading="lazy" url="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7IAP-Y7IaUWt525EUbIsoIO3ewMZFvfibKao51VV6hqkpqogl" alt="img" />
            </div>
          </div>
        </div>
        <div className="body-academy">
          <span className="span1">
            About Us
          </span>
          <span className="span2">
            Academy
          </span>
          <span className="span3">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
          </span>
          <Button
            className="bt-learn"
          >
            LEARN
          </Button>
        </div>
        <div className="body_influencer">
          <span className="span1">
            Community
          </span>
          <span className="span2">
            Influencer Creations
          </span>
          <span className="span3">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
          </span>
          <div className="div-infuencer">
            <div className="div1">
              <InfluencerItem className="div-item" />
              <InfluencerItem className="div-item" />
              <InfluencerItem className="div-item" />
            </div>
            <div className="div2">
              <InfluencerItem className="div-item" />
              <InfluencerItem className="div-item" />
              <InfluencerItem className="div-item" />
            </div>
          </div>
          <div className="div-link">
            <Link className="link" to={generateUrlWeb('/seemore')}>
              See more
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

HomePage.propTypes = {
  windowWidth: PropTypes.number.isRequired,
};

export default withRouter(windowSize(HomePage));
