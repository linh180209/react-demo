import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import smoothscroll from 'smoothscroll-polyfill';
import MetaTags from 'react-meta-tags';

import { scrollTop, fetchCMSHomepage } from '../../Redux/Helpers';
import HeaderHomePage from '../../components/HomePage/header';
import StartPage from '../../components/HomePage/startPage';
import ScentDesign from '../../components/HomePage/scentDesign';
import HowItWork from '../../components/HomePage/howitwork';
import ProductPage from '../../components/HomePage/product';
import BlockItem from '../../components/HomePage/blockItem';
import LoginRegister from '../Register/loginRegister';
import addCmsRedux from '../../Redux/Actions/cms';
import { toastrError } from '../../Redux/Helpers/notification';
import AskCookies from '../../components/AskCookies';
import auth from '../../Redux/Helpers/auth';
import FooterV2 from '../../views2/footer';

class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowLogin: false,
      isAskCookies: false,
      homePageCms: undefined,
    };
  }

  componentDidMount() {
    scrollTop();
    smoothscroll.polyfill();
    this.setState({ isAskCookies: auth.getAskCookies() });
    this.fetchData();
  }


  onCloseLogin = () => {
    this.setState({ isShowLogin: false });
  }

  onOpenLogin = () => {
    this.setState({ isShowLogin: true });
  }

  onClickAskCookies = () => {
    this.setState({ isAskCookies: true });
    auth.setAskCookies();
  }

  fetchData = async () => {
    const { cms } = this.props;
    const homePageCms = _.find(cms, x => x.title === 'Scent designer');
    if (!homePageCms) {
      try {
        const result = await fetchCMSHomepage('home-page');
        this.props.addCmsRedux(result);
        this.setState({ homePageCms: result });
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      this.setState({ homePageCms });
    }
  }

  render() {
    const metaHtml = (
      <MetaTags>
        <title>
          Maison 21g | House of Scent Designers
        </title>
        <meta name="description" content="Why not design your own perfume today? Tailor-made fragrances made with the finest natural ingredients, freshly made by you, for you." />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 months" />
      </MetaTags>
    );
    const { isShowLogin, isAskCookies, homePageCms } = this.state;
    const { isReady } = this.props;
    if (!homePageCms || !isReady) {
      return (
        <div>
          {metaHtml}
        </div>
      );
    }
    this.numberBlock = 0;
    return (
      <div>
        {metaHtml}
        <div>
          <div className="div-homepage">
            <HeaderHomePage />
            {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
            {
              _.map(homePageCms.sections, (d) => {
                switch (d.section_name) {
                  case 'HERO':
                    return <StartPage heroCms={d} />;
                  case 'HOW_IT_WORKS':
                    return <HowItWork howitWorkCms={d} />;
                  case 'THREE_COLUMNS':
                    return <ScentDesign scentDesignCms={d} />;
                  case 'PRODUCT':
                    return <ProductPage productCms={d} />;
                  default:
                    this.numberBlock = this.numberBlock + 1;
                    return <BlockItem isRight={this.numberBlock % 2 === 0} cms={d} />;
                }
              })
            }
            {
              isAskCookies ? <div /> : <AskCookies onClick={this.onClickAskCookies} />
            }
            <FooterV2 />
            <LoginRegister onClose={this.onCloseLogin} isOpen={isShowLogin} />

          </div>
        </div>

      </div>
    );
  }
}

HomePage.propTypes = {
  isReady: PropTypes.bool.isRequired,
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    isReady: state.isReady,
    cms: state.cms,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
