import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Home from '../../components/B2B/home';
import Information from '../../components/B2B/information';
import Capsule from '../../components/B2B/capsule';
import Bottles from '../../components/B2B/bottles';
import Packaging from '../../components/B2B/packaging';
import Alcohol from '../../components/B2B/alcohol';
import Account from '../../components/B2B/account';
import { scrollTop } from '../../Redux/Helpers';
import FooterV2 from '../../views2/footer';

class B2BLanding extends Component {
  componentDidMount() {
    scrollTop();
  }

  render() {
    const { cms } = this.props;
    const b2bCms = _.find(cms, x => x.title === 'B2B Landing page');
    if (!b2bCms) {
      return (<div />);
    }
    const {
      alcohol: alcoholCms, bottle: bottleCms, capsule: capsuleCms, image_background: imageBackground, intro: introCms,
      packaging: packagingCms,
    } = b2bCms;
    return (
      <div className="b2blanding">
        <Home imageBg={imageBackground} />
        <Information introCms={introCms} />
        <Capsule isShowBag={false} capsuleCms={capsuleCms} />
        <Bottles isShowBag={false} bottleCms={bottleCms} />
        <Packaging isShowBag={false} packagingCms={packagingCms} />
        <Alcohol isShowBag={false} alcoholCms={alcoholCms} />
        <FooterV2 />
        {/* <Account /> */}
      </div>
    );
  }
}

B2BLanding.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
  };
}

export default connect(mapStateToProps, null)(B2BLanding);
