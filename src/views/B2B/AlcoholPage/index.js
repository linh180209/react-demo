import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';

import Alcohol from '../../../components/B2B/alcohol';
import IconMenu from '../../../components/B2B/iconMenu';
import IconLogo from '../../../components/B2B/iconLogo';
import { scrollTop } from '../../../Redux/Helpers';
import FooterV2 from '../../../views2/footer';

class AlcoholPage extends Component {
  componentDidMount() {
    scrollTop();
  }

  render() {
    const { cms } = this.props;
    const pageCms = _.find(cms, x => x.title === 'Alcohol page');
    if (!pageCms) {
      return (<div />);
    }
    const { body: alcoholCms } = pageCms;
    return (
      <div>
        <IconMenu />
        <IconLogo />
        <Alcohol isDetail alcoholCms={alcoholCms} />
        <FooterV2 />
      </div>
    );
  }
}

AlcoholPage.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
  };
}

export default connect(mapStateToProps, null)(AlcoholPage);
