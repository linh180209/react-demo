import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';

import IconMenu from '../../../components/B2B/iconMenu';
import Packaging from '../../../components/B2B/packaging';
import IconLogo from '../../../components/B2B/iconLogo';
import { scrollTop } from '../../../Redux/Helpers';
import FooterV2 from '../../../views2/footer';

class PackagingPage extends Component {
  componentDidMount() {
    scrollTop();
  }

  render() {
    const { cms } = this.props;
    const pageCms = _.find(cms, x => x.title === 'Packaging page');
    if (!pageCms) {
      return (<div />);
    }
    const { body: packagingCms } = pageCms;
    return (
      <div>
        <IconMenu />
        <IconLogo />
        <Packaging isDetail packagingCms={packagingCms} />
        <FooterV2 />
      </div>
    );
  }
}

PackagingPage.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
  };
}

export default connect(mapStateToProps, null)(PackagingPage);
