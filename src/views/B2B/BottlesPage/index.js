import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';

import IconMenu from '../../../components/B2B/iconMenu';
import Bottles from '../../../components/B2B/bottles';
import IconBags from '../../../components/B2B/iconBags';
import IconLogo from '../../../components/B2B/iconLogo';
import { scrollTop } from '../../../Redux/Helpers';
import FooterV2 from '../../../views2/footer';

class BottlesPage extends Component {
  componentDidMount() {
    scrollTop();
  }

  render() {
    const { cms } = this.props;
    const pageCms = _.find(cms, x => x.title === 'Bottle page');
    if (!pageCms) {
      return (<div />);
    }
    const { body: bottleCms } = pageCms;
    return (
      <div>
        <IconMenu />
        <IconBags className="b2b-bt-fix_top-right" />
        <IconLogo />
        <Bottles isDetail bottleCms={bottleCms} />
        <FooterV2 />
      </div>
    );
  }
}

BottlesPage.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
  };
}

export default connect(mapStateToProps, null)(BottlesPage);
