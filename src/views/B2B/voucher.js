import React, { Component } from 'react';
import {
  Button,
  Input,
} from 'reactstrap';

class Voucher extends Component {
  state = {
    voucher: '',
  };

  onChange = (e) => {
    const { value } = e.target;
    this.setState({ voucher: value });
  }

  onSubmit = (e) => {
    e.preventDefault();
  }

  render() {
    const { voucher } = this.state;
    return (
      <div style={{
        height: '100vh',
        width: '100vw',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      }}
      >
        <div style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          width: '300px',
        }}
        >
          <form
            style={{
              width: '100%',
            }}
            onSubmit={this.onSubmit}
          >
            <h3 style={{
              width: '100%',
            }}
            >
              Voucher
            </h3>
            <Input
              style={{
                marginTop: '10px',
                marginBottom: '25px',
              }}
              type="text"
              name="voucher"
              value={voucher}
              onChange={this.onChange}
            />
            <Button type="submit" className="px-4 btn--login">
              Check Out
            </Button>
          </form>
        </div>
      </div>
    );
  }
}

export default Voucher;
