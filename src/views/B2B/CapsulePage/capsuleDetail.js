import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import bags from '../../../image/icon/bag-white.svg';
import { getAltImage, generaCurrency } from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';

class CapsuleDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      purchaseQuantities: 1,
    };
  }


  onChangeQuantity = (e) => {
    const { value } = e.target;
    this.setState({ purchaseQuantities: parseInt(value, 10) });
  }

  addIntoBasket = () => {
    const { addIntoBasket } = this.props;
    this.props.data.purchaseQuantities = this.state.purchaseQuantities;
    addIntoBasket(this.props.data);
  }

  render() {
    const { data } = this.props;
    const { purchaseQuantities } = this.state;
    const {
      urlImage, description, name, price_b2b: priceB2b, quantity,
    } = data;
    return (
      <div className="capsule-detail">
        <button
          type="button"
          onClick={this.props.onClickClose}
        >
          X
        </button>
        <div className="content">
          <img loading="lazy" src={urlImage} alt={getAltImage(urlImage)} />
          <div>
            <span className="price">
              <strong>
                {generaCurrency(priceB2b)}
              </strong>
            </span>
            <span className="name">
              <strong>
                {name}
              </strong>
            </span>
            <div className="description">
              {description}
            </div>
            <div className="div-span">
              <span>
                Qyt :
                {' '}
              </span>
              <span className="span-down">
                <select
                  className="b2b-select"
                  value={purchaseQuantities}
                  onChange={this.onChangeQuantity}
                >
                  {
                  _.map(new Array(quantity), (d, index) => (
                    <option>
                      {index + 1}
                    </option>
                  ))
                  }
                </select>
              </span>
            </div>
            <button
              type="button"
              onClick={this.addIntoBasket}
            >
              <img className="img-bags_button" src={bags} alt="icon" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

CapsuleDetail.defaultProps = {
};

CapsuleDetail.propTypes = {
  data: PropTypes.shape({
    urlImage: PropTypes.string,
    description: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
    price_b2b: PropTypes.number,
    quantity: PropTypes.number,
    purchaseQuantities: PropTypes.number,
  }).isRequired,
  onClickClose: PropTypes.func.isRequired,
  addIntoBasket: PropTypes.func.isRequired,
};

export default CapsuleDetail;
