import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import _ from 'lodash';
import { connect } from 'react-redux';

import { GET_ALL_SCENT } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import titlecapsules from '../../../image/capsules.png';
import CapsuleItem from '../../../components/CapsuleItem';
import CapsuleDetail from './capsuleDetail';
import IconMenu from '../../../components/B2B/iconMenu';
import IconBags from '../../../components/B2B/iconBags';
import IconLogo from '../../../components/B2B/iconLogo';
import { addProductBasket } from '../../../Redux/Actions/basket';
import { scrollTop } from '../../../Redux/Helpers';
import FooterV2 from '../../../views2/footer';

class CapsulePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      heightPage: 0,
      isShowDetail: false,
      datas: [],
      dataShow: undefined,
    };
    this.refPage = React.createRef();
  }

  componentDidMount = () => {
    scrollTop();
    this.fetchAllScent();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.datas !== this.state.datas && this.state.heightPage === 0) {
      this.setState({ heightPage: this.refPage.current.clientHeight });
    }
    if (prevState.login !== this.state.login && this.state.results) {
      const datas = this.createData(this.state.results);
      this.setState({ datas });
    }
  }

  onClickOpen = (dataShow) => {
    this.setState({ isShowDetail: true, dataShow });
  }

  onClickClose = () => {
    this.setState({ isShowDetail: false });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { login } = nextProps;
    if (login !== prevState.login) {
      _.assign(objectReturn, { login });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  fetchAllScent = () => {
    const { login } = this.props;
    this.gender = login && login.user && login.user.gender ? login.user.gender : 'unisex';
    const option = {
      url: GET_ALL_SCENT,
      method: 'GET',
    };
    fetchClient(option).then((results) => {
      if (results) {
        const datas = this.createData(results);
        this.setState({ datas, results });
      }
    });
  }

  createData = (results) => {
    const datas = [];
    _.forEach(results, (x) => {
      const { product } = x;
      const data = _.assign({}, product);
      data.idScent = x.id;
      data.price = x.price;
      data.price_b2b = x.price_b2b;
      data.quantity = x.quantity;
      data.urlImage = product.images && _.find(product.images, i => i.type === this.gender.toLowerCase()) ? _.find(product.images, i => i.type === this.gender.toLowerCase()).image : undefined;
      data.width = 180;
      data.height = 350;
      datas.push(data);
    });
    return datas;
  }

  scrollToTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  addIntoBasket = (product) => {
    const { basket } = this.props;
    const itemTemp = {
      item: product.idScent,
      item_id: product.idScent,
      name: product.name,
      is_featured: product.is_featured,
      price: product.price_b2b,
      original_quantity: product.quantity,
      quantity: product.purchaseQuantities,
      total: product.price_b2b * product.purchaseQuantities,
      image_urls: _.find(product.images, i => i.type === this.gender.toLowerCase()).image,
      urlImage: _.find(product.images, i => i.type === this.gender.toLowerCase()).image,
    };
    const dataTemp = {
      idCart: basket.id,
      item: itemTemp,
    };
    this.props.addProductBasket(dataTemp);
    this.onClickClose();
  }

  render() {
    const { isCheckOut } = this.props;
    const {
      heightPage, isShowDetail, datas, dataShow,
    } = this.state;
    const lineHeight = heightPage - 160;
    return (
      <div ref={this.refPage} className="capsule-page">
        <div className="header" style={{ position: 'absolute' }}>
          <IconMenu />
          <IconLogo />
          <IconBags className="b2b-bt-fix_top-right" />
        </div>
        <div className="image-title">
          <div className="line line-top" />
          <img loading="lazy" src={titlecapsules} alt="capsules" />
          <div className="line line-bottom" style={{ height: lineHeight }} />
        </div>
        <div className="content">
          <div className="div-filter">
            <div className="div-span">
              <span className="span-down-header">
                <select className="b2b-select">
                  <option>
                    Select Category
                  </option>
                </select>
              </span>
            </div>
            <div className="line-space" />
            <div className="div-span">
              <span className="span-down-header">
                <select className="b2b-select">
                  <option>
                    Sort by
                  </option>
                </select>
              </span>
            </div>
            <div className="line-space" />
            <div className="div-span">
              <span className="span-down-header">
                <select className="b2b-select">
                  <option>
                    Filter
                  </option>
                </select>
              </span>
            </div>
          </div>
          <div className="div-capsule">
            <Row>
              {
                _.map(datas, d => (
                  <Col md="4" sx="12" style={{ marginBottom: '50px' }}>
                    <CapsuleItem
                      data={d}
                      urlImage={d.urlImage}
                      width={d.width}
                      height={d.height}
                      name={d.name}
                      description={d.description}
                      onClickOpen={this.onClickOpen}
                    />
                  </Col>
                ))
              }
            </Row>
          </div>
        </div>
        {
          isShowDetail ? (<CapsuleDetail data={dataShow} onClickClose={this.onClickClose} addIntoBasket={this.addIntoBasket} />) : (<div />)
        }
        <FooterV2 />
      </div>
    );
  }
}

CapsulePage.defaultProps = {
  isCheckOut: false,
};

CapsulePage.propTypes = {
  isCheckOut: PropTypes.string,
  addProductBasket: PropTypes.func.isRequired,
  basket: PropTypes.shape(PropTypes.object).isRequired,
};

const mapDispatchToProps = {
  addProductBasket,
};

function mapStateToProps(state) {
  return {
    basket: state.basket,
    login: state.login,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CapsulePage);
