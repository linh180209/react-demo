import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';

import IconMenu from '../../../components/B2B/iconMenu';
import Account from '../../../components/B2B/account';
import { scrollTop } from '../../../Redux/Helpers';
import FooterV2 from '../../../views2/footer';

class AccountPage extends Component {
  componentDidMount() {
    scrollTop();
  }

  render() {
    const { cms } = this.props;
    const accountCms = _.find(cms, x => x.title === 'Profile page');
    if (!accountCms) {
      return (<div />);
    }
    return (
      <div>
        <IconMenu />
        <Account accountCms={accountCms} />
        <FooterV2 />
      </div>
    );
  }
}

AccountPage.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
  };
}

export default connect(mapStateToProps, null)(AccountPage);
