import React, { Component } from 'react';
import {
  Row, Col, Container,
} from 'reactstrap';
import { isBrowser } from '../../DetectScreen';
import cardimg from '../../image/1.png';
import '../../styles/cardInfluencer.scss';
import { getAltImage } from '../../Redux/Helpers';

export default class CardInfluencer extends Component {
  render() {
    const {
      cardItem, handleClickShowDetail, index, checkFunctionHCSD,
    } = this.props;
    let countCollection = 0;
    for (const x of cardItem.collections) {
      countCollection += x.products.length;
    }
    // const imageDisplay= cardItem.img;
    let responsive;
    const imageDisplay = cardimg;
    if (isBrowser) {
      responsive = 3;
    } else {
      responsive = (!checkFunctionHCSD) ? 4 : 3;
    }

    return (

      <Col md={responsive} xs={6} className={(!checkFunctionHCSD) ? '_view-influencer-add-col' : ''} style={{ marginBottom: '4%', width: 'initial' }}>
        <div className="_view-influencer">
          {(countCollection < 2)
            ? (
              <div className="card-header">
                <div className="card-item-2" onClick={() => { (checkFunctionHCSD) ? handleClickShowDetail(cardItem.id) : ''; }}>
                  <li className="card-item-child">
                    <img src={cardItem.avatar} alt={getAltImage(cardItem.avatar)} className="img_card" />

                    <div className="number-card-div">
                      <span className="card-item-name">
                        {cardItem.name}
                      </span>
                      <p className="number-card">
                        {countCollection}
                      </p>
                      <div className="border-number-card" />
                    </div>

                  </li>
                </div>
              </div>
            ) : (
              <div className="card-header">
                <div className="card-item">
                  <li className="card-item-child">
                    <img loading="lazy" src={imageDisplay} alt={getAltImage(imageDisplay)} className="img_card" />
                    <span className="card-item-name" />
                  </li>
                </div>
                <div className="card-item">
                  <li className="card-item-child">
                    <img loading="lazy" src={imageDisplay} alt={getAltImage(imageDisplay)} className="img_card" />
                    <span className="card-item-name" />
                  </li>
                </div>
                <div className="card-item" onClick={() => { (checkFunctionHCSD) ? handleClickShowDetail(cardItem.id) : ''; }}>
                  <li className="card-item-child">
                    <img loading="lazy" src={cardItem.avatar} alt={getAltImage(cardItem.avatar)} className="img_card" />

                    <div className="number-card-div">
                      <span className="card-item-name">
                        {cardItem.name}
                      </span>

                      <p className="number-card">
                        {countCollection}
                      </p>
                      <div className="border-number-card" />

                    </div>
                  </li>
                </div>
              </div>
            )}


        </div>
      </Col>
    );
  }
}
