import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getNameFromCommon } from '../../Redux/Helpers';
import icNext from '../../image/icon/ic-vector-next.svg';
import { isBrowser } from '../../DetectScreen';


class ItemInfluencer extends Component {
  state = {
    isHover: false,
  };

  toggleHover = () => {
    const { isHover } = this.state;
    this.setState({ isHover: !isHover });
  }

  handleClickShowDetail = () => {
    const { slug } = this.props.data;
    this.props.handleClickShowDetail(slug);
  }

  render() {
    const { isHover } = this.state;
    const { data, cmsCommon, isFocus } = this.props;
    const {
      banner, name, description, title,
    } = data;
    const browserBt = getNameFromCommon(cmsCommon, 'Browse_Collections');
    return (
      <div
        onMouseEnter={this.toggleHover}
        onMouseLeave={this.toggleHover}
        className="items-influencer"
        style={{ backgroundImage: `url(${banner})` }}
        onClick={this.handleClickShowDetail}
      >
        <div className={`${isHover ? 'color-background active' : 'color-background inactive'}`} />
        <div className="div-col div-text justify-center items-center">
          <h2>
            {name}
          </h2>
          <span className="sub-title">
            <b>
              {title}
            </b>
          </span>
          {
            isBrowser || isFocus ? (
              <React.Fragment>
                <span className="description mt-3 mb-3 tc">
                  {description}
                </span>
                <button type="button" className="button-bg__none">
                  {browserBt}
                  <img src={icNext} alt="ic-next" />
                </button>
              </React.Fragment>
            ) : (
              <div />
            )
          }

        </div>
      </div>
    );
  }
}

ItemInfluencer.propTypes = {
  data: PropTypes.shape().isRequired,
  cmsCommon: PropTypes.shape().isRequired,
  handleClickShowDetail: PropTypes.func.isRequired,
  isFocus: PropTypes.bool.isRequired,
};

export default ItemInfluencer;
