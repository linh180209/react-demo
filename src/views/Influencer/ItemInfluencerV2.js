import React, { useState, useEffect } from 'react';
import '../../styles/cardInfluencer.scss';

function ItemInfluencerV2(props) {
  const handleClickShowDetail = () => {
    const { slug } = props.data;
    props.handleClickShowDetail(slug);
  };

  const {
    banner, name, description, title,
  } = props.data;
  return (
    <div className="itemInfluencer-v2" onClick={handleClickShowDetail}>
      <img src={banner} alt="banner" />
      <div>
        <h3>
          {name}
        </h3>
        <span>
          {title}
        </span>
      </div>
    </div>
  );
}

export default ItemInfluencerV2;
