import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { getAltImage, getNameFromCommon, generaCurrency } from '../../Redux/Helpers';
import { isBrowser } from '../../DetectScreen';

class ItemProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFocus: false,
    };
  }

  toggleHover = () => {
    const { isFocus } = this.state;
    if (isBrowser) {
      this.setState({ isFocus: !isFocus });
    }
  }

  getNameScent = (data) => {
    if (!data) {
      return undefined;
    }
    const listName = [];
    _.forEach(data, (x) => {
      listName.push(x.name);
    });
    return _.join(listName, ' & ');
  }

  render() {
    const { isFocus } = this.state;
    const {
      isBig, data, cmsCommon, onClickGotoProduct, isShowPrice,
    } = this.props;
    if (_.isEmpty(data)) {
      return null;
    }
    const { combo } = data;
    const addToCart = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
    const moreBt = getNameFromCommon(cmsCommon, 'MORE_INFO');
    const priceBt = getNameFromCommon(cmsCommon, 'PRICE');
    const ingridientsBt = getNameFromCommon(cmsCommon, 'INGRIDIENTS');
    return (
      <div
        onMouseEnter={this.toggleHover}
        onMouseLeave={this.toggleHover}
        className={`items-product-influencer div-col ${isBig ? 'big' : ''}`}
      >
        <div className="image">
          <img loading="lazy" onClick={() => onClickGotoProduct(data)} src={data.image} alt={getAltImage(data.image)} />
        </div>

        <div className="div-text div-col justify-center items-center">
          <h6 className="ttu tc">
            {data.name}
          </h6>
          <span className="ttu tc">
            {`${data.type.alt_name}`}
          </span>
          {
            isShowPrice ? (
              <span className="tc text-price">
                <b>
                  {generaCurrency(data.item.price)}
                </b>
              </span>
            ) : (<div />)
          }

        </div>
        <div onClick={() => onClickGotoProduct(data)} className={`${isFocus ? 'div-hover div-col' : 'div-hover'}`}>
          <div className="div-text div-col items-between justify-between">
            <h3 className="ttu w-100 tc">
              {data.name}
            </h3>
            <div className={combo ? 'mid div-col justify-center items-center' : 'hidden'}>
              <h6>
                {ingridientsBt}
              </h6>
              <span className="tc w-90">
                {this.getNameScent(combo)}
              </span>
            </div>
            <div className="price mb-3">
              <h5 className="w-100 tc">
                {priceBt}
              </h5>
              <h2 className="w-100 tc">
                {generaCurrency(data.item.price)}
              </h2>
            </div>
          </div>
          <div className="div-button">
            <button
              type="button"
              onClick={() => this.props.onClickAddToCart(data)}
            >
              {addToCart}
            </button>
            <button type="button" onClick={() => onClickGotoProduct(data)}>
              {moreBt}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

ItemProduct.defaultProps = {
  isBig: false,
};

ItemProduct.propTypes = {
  data: PropTypes.shape().isRequired,
  isBig: PropTypes.bool,
  onClickAddToCart: PropTypes.func.isRequired,
  onClickGotoProduct: PropTypes.func.isRequired,
  cmsCommon: PropTypes.shape().isRequired,
  isShowPrice: PropTypes.bool.isRequired,
};

export default ItemProduct;
