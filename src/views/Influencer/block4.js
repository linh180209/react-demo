import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getAltImage } from '../../Redux/Helpers';

class Block4 extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className="div-block-4">
        <div className="block-div-text">
          <img src={data.signature} alt={getAltImage(data.signature)} />
        </div>
        {/* <div className="div-image">
          <img src={testImage} alt={getAltImage(testImage)} />
          <img src={testImage} alt={getAltImage(testImage)} />
          <img src={testImage} alt={getAltImage(testImage)} />
          <img src={testImage} alt={getAltImage(testImage)} />
          <img src={testImage} alt={getAltImage(testImage)} />
        </div> */}
      </div>
    );
  }
}

Block4.propTypes = {
  data: PropTypes.shape().isRequired,
};

export default Block4;
