import React, { Component } from 'react';
import {
  Row, Container,
} from 'reactstrap';
import CardInfluencer from './CardInfluencer';

export default class ListInfluencer extends Component {
  render() {
    const { arrListInfluencer, handleClickShowDetail } = this.props;
    return (
      <Container fluid className="_container-view-influencer">
        <Row>
          {arrListInfluencer.map((x, index) => (
            <CardInfluencer
              checkFunctionHCSD
              index={index}
              key={index}
              cardItem={x}
              handleClickShowDetail={handleClickShowDetail}
            />
          ))}
        </Row>
      </Container>
    );
  }
}
