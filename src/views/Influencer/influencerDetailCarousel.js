/* eslint-disable indent */
/* eslint-disable no-tabs */
import React, { Component } from 'react';
import Slider from 'react-slick';
import { withRouter } from 'react-router-dom';
import { Col } from 'reactstrap';
import _ from 'lodash';
import '../../../node_modules/slick-carousel/slick/slick-theme.css';
import '../../../node_modules/slick-carousel/slick/slick.css';
import { getAltImage, generaCurrency, generateUrlWeb } from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { isMobile, isTablet } from '../../DetectScreen';

class InfluencerDetailCarousel extends Component {
  gotoProduct = (data) => {
    console.log('data', data);
    const { id, type } = data;
    if (type === 'Creation') {
      this.props.history.push(generateUrlWeb(`/product/creation/${id}`));
    }
  }

  render() {
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      // autoplay: true,
      autoplaySpeed: 2000,
      cssEase: 'linear',
      centerPadding: '10px',
      responsive: [
        {
          breakpoint: 414,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          },
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 1,
          },
        },
      ],
    };
    const { carouselData } = this.props;
    const countCarouselData = carouselData.length;
    console.log('carouselData', carouselData);
    const carouselDataDetail = carouselData.map((d, i) => (
      <div className="carousel-item" key={i}>
        <div className="item-img">
          <figure>
            <img loading="lazy" className={_.find(d.images, x => x.type === 'main') ? '' : 'hidden'} src={_.find(d.images, x => x.type === 'main') ? _.find(d.images, x => x.type === 'main').image : ''} alt={getAltImage(_.find(d.images, x => x.type === 'main') ? _.find(d.images, x => x.type === 'main').image : '')} />
            <figcaption className="item-caption">
              <p onClick={() => this.gotoProduct(d)} style={{ cursor: 'pointer' }}>
                {/* {d.description} */}
              </p>
            </figcaption>
          </figure>
        </div>
        <div className="item-price">
          <p>
            {`${d.name} - ${generaCurrency(d.items.reduce((a, b) => a + parseFloat(b.price, 10), 0))}`}
          </p>
        </div>
      </div>
      ));
    if (!isMobile) {
      for (var i = 0; i < 4 - countCarouselData; i++) {
        carouselDataDetail.push(
          <div className="carousel-item" key={i}>
            <div className="item-img">
              <figure>
                {/* <img src="" alt="" /> */}
                <figcaption className="item-caption" />
              </figure>
            </div>
            <div className="item-price">
              <p />
            </div>
          </div>,
        );
      }
    } else if (isTablet) {
        for (var i = 0; i < 4 - countCarouselData; i++) {
          carouselDataDetail.push(
            <div className="carousel-item" key={i}>
              <div className="item-img">
                <figure>
                  {/* <img src="" alt="" /> */}
                  <figcaption className="item-caption" />
                </figure>
              </div>
              <div className="item-price">
                <p />
              </div>
            </div>,
          );
        }
      } else {
        for (var i = 0; i < 2 - countCarouselData; i++) {
          carouselDataDetail.push(
            <div className="carousel-item" key={i}>
              <div className="item-img">
                <figure>
                  {/* <img src="" alt="" /> */}
                  <figcaption className="item-caption" />
                </figure>
              </div>
              <div className="item-price">
                <p />
              </div>
            </div>,
          );
        }
      }


    return (
      <React.Fragment>
        <div className="view-influencer-detail">
          <div className="main-title">
            <h2 className="tit">
              {this.props.title}
            </h2>
          </div>
          <Col md={12} lg={12}>
            <Slider {...settings}>
              {carouselDataDetail}
            </Slider>
          </Col>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(InfluencerDetailCarousel);
