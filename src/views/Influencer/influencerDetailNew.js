/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';

import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import MetaTags from 'react-meta-tags';
import '../../styles/influencer-detail-new.scss';

import Block1 from './block1';
import Block2 from './block2';
import Block3 from './block3';
import Block4 from './block4';

import Header from '../../components/HomePage/header';
import addCmsRedux from '../../Redux/Actions/cms';
import { GET_INFLUENCER_DETAIL_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import {
  getCmsCommon, googleAnalitycs, fetchCMSHomepage, getNameFromButtonBlock, generateUrlWeb, setPrerenderReady, gotoShopHome,
} from '../../Redux/Helpers';
import { createBasketGuest, addProductBasket } from '../../Redux/Actions/basket';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';

const getCms = (influencerBlock) => {
  if (influencerBlock) {
    const { body } = influencerBlock;
    const buttonBlock = _.filter(body, x => x.type === 'button_block');
    return {
      body,
      buttonBlock,
    };
  }
  return {
    body: [],
    buttonBlock: [],
  };
};

class InfluencerDetailNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
    };
  }

  componentDidMount() {
    setPrerenderReady();
    const { match } = this.props;
    const { slug } = match.params;
    if (slug === 'hand-sanitizer') {
      this.props.history.push(generateUrlWeb('/products-all/Hand%20Sanitizer%2015ml'));
      return;
    }
    this.fetchDataInit();
    googleAnalitycs(window.location.pathname);
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const influencerBlock = _.find(cms, x => x.title === 'Influencers');
    if (!influencerBlock) {
      const cmsData = await fetchCMSHomepage('influencers');
      this.props.addCmsRedux(cmsData);
      const { buttonBlock } = getCms(cmsData);
      this.setState({ buttonBlock });
    } else {
      const { buttonBlock } = getCms(influencerBlock);
      this.setState({
        buttonBlock,
      });
    }

    const { match } = this.props;
    const { slug } = match.params;
    const option = {
      url: GET_INFLUENCER_DETAIL_URL.replace('{slug}', slug),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      console.log('result', result);
      if (result && !result.isError) {
        this.setState({ data: result });
        return;
      } if (result && result.status === 404) {
        // this.props.history.push(generateUrlWeb('/'));
        gotoShopHome();
        return;
      }
      throw new Error();
    }).catch(() => {

    });
  }

  onClickAddToCart = (data) => {
    const { item, name } = data;
    const { basket } = this.props;
    const dataItem = {
      item: item.id,
      price: item.price,
      is_featured: item.is_featured,
      quantity: 1,
      name,
    };
    const dataTemp = {
      idCart: basket.id,
      item: dataItem,
    };
    if (!basket.id) {
      this.props.createBasketGuest(dataTemp);
    } else {
      this.props.addProductBasket(dataTemp);
    }
  }

  getLinkFromCombo = (combo) => {
    if (!combo) {
      return '/';
    }
    const listId = [];
    _.forEach(combo, (x) => {
      listId.push(x.id);
    });
    return _.join(listId, '/');
  }

  onClickGotoProduct = (product) => {
    const { type, combo } = product;
    let url;
    if (type.name === 'Wax_Perfume') {
      url = `/product/wax/${product.id}`;
    } else if (type.name === 'Elixir') {
      url = `/product/elixir/${product.id}`;
    } else if (type.name === 'Kit') {
      url = `/product/kit/${product.id}`;
    } else if (type.name === 'bundle') {
      url = `/product/bundle/${product.id}`;
    } else if (type.name === 'Scent') {
      url = `/product/Scent/${product.id}`;
    } else if (type.name === 'Creation') {
      url = `/product/creation/${product.id}`;
    } else if (type.name === 'dual_candles') {
      url = `/product/dual-candles/${product.id}`;
    } else if (type.name === 'single_candle') {
      url = `/product/single-candle/${product.id}`;
    } else if (type.name === 'holder') {
      url = `/product/holder/${product.id}`;
    } else if (type.name === 'gift_bundle') {
      url = `/product/gift-bundle/${product.id}`;
    } else if (type.name === 'mini_candles') {
      url = `/product/mini_candles/${product.id}`;
    }
    console.log('url', url);
    this.props.history.push(generateUrlWeb(url));
  }

  render() {
    const { data, buttonBlock } = this.state;
    const { match } = this.props;
    const { slug } = match.params;

    const seoTitle = getNameFromButtonBlock(buttonBlock, `title ${slug}`);
    const seoKeywords = getNameFromButtonBlock(buttonBlock, `keywords ${slug}`);
    const seoDescription = getNameFromButtonBlock(buttonBlock, `description ${slug}`);

    const cmsCommon = getCmsCommon(this.props.cms);

    if (_.isEmpty(data)) {
      return null;
    }
    return (
      <div>
        <MetaTags>
          <title>
            {seoTitle}
          </title>
          <meta name="description" content={seoDescription} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
        </MetaTags>
        {/* <Header /> */}

        <HeaderHomePageV3 />

        <div className="influencer-detail-new">
          <Block1 data={data} cmsCommon={cmsCommon} onClickAddToCart={this.onClickAddToCart} onClickGotoProduct={this.onClickGotoProduct} />
          <Block2 data={data} />
          <Block3 data={data} cmsCommon={cmsCommon} onClickAddToCart={this.onClickAddToCart} onClickGotoProduct={this.onClickGotoProduct} />
          <Block4 data={data} />
        </div>
        <FooterV2 />
      </div>
    );
  }
}

InfluencerDetailNew.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      slug: PropTypes.string,
    }),
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  cms: PropTypes.shape().isRequired,
  basket: PropTypes.shape().isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    login: state.login,
    basket: state.basket,
  };
}

const mapDispatchToProps = {
  addProductBasket,
  createBasketGuest,
  addCmsRedux,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(InfluencerDetailNew));
