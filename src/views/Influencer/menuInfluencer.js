import React, { Component } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import _ from 'lodash';

import icDown from '../../image/icon/ic-arrow-inffluencer.svg';
import icLayout1 from '../../image/icon/ic-layout-1.svg';
import icLayout2 from '../../image/icon/ic-layout-2.svg';
// import icFilters from '../../image/icon/ic-filters.svg';
// import ItemSubFilter from './itemSubFilter';

class MenuInfluencer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
      optionSort: 'Default sorting',
    };
    this.menuButton = [
      {
        name: 'ALL',
      },
      {
        name: 'HOT PRODUCTS',
      },
      {
        name: 'NEW PRODUCTS',
      },
      {
        name: 'SALE',
      },
    ];
    this.optionSorts = [
      'Default sorting',
      'Sort by popularity',
      'Sort by latest',
      'Sort by price: low to high',
      'Sort by price: high to low',
    ];
  }

  onChange = (name) => {
    this.setState({ optionSort: name });
    this.toggle();
  }

  toggle = () => {
    const { dropdownOpen } = this.state;
    this.setState({
      dropdownOpen: !dropdownOpen,
    });
  }

  render() {
    const { optionSort } = this.state;
    return (
      <div className="div-menu-influencer">
        <div className="menu-left-influencer">
          {
              _.map(this.menuButton, x => (
                <button type="button" className="button-bg__none button-text">
                  {x.name}
                </button>
              ))
            }
        </div>
        <div className="menu-right-influencer">
          <Dropdown
            isOpen={this.state.dropdownOpen}
            toggle={this.toggle}
            className="dropdown-menu-influencer"
          >
            <DropdownToggle
              tag="span"
              onClick={this.toggle}
              data-toggle="dropdown"
              aria-expanded={this.state.dropdownOpen}
            >
              <div className="div-row items-center justify-between drop-down-items">
                <div>
                  <span>
                    {`${optionSort}`}
                  </span>
                </div>
                <img src={icDown} alt="icDown" />

              </div>

            </DropdownToggle>
            <DropdownMenu>
              {
                  _.map(this.optionSorts, d => (
                    <div
                      onClick={() => {
                        this.onChange(d);
                      }}
                    >
                      <div className="drop-down-item">
                        {/* <img src={icFlags} alt="img" /> */}
                        <span className="ml-3">
                          {`${d}`}
                        </span>
                      </div>
                    </div>
                  ))
                }
            </DropdownMenu>
          </Dropdown>
          <button type="button" className="button-bg__none">
            <img src={icLayout1} alt="icLayout1" />
          </button>
          <button type="button" className="button-bg__none">
            <img src={icLayout2} alt="icLayout2" />
          </button>
          {/* <button
            type="button"
            className="button-bg__none"
          >
              Filters
            <img className="ml-2" src={icFilters} alt="icFilter" />
          </button> */}
        </div>
        {/* <div className="div-detail-filter">
          <div className="div-content">
            <ItemSubFilter />
            <ItemSubFilter />
            <ItemSubFilter />
            <ItemSubFilter />
          </div>
        </div> */}
      </div>
    );
  }
}

export default MenuInfluencer;
