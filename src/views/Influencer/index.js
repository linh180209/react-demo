/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Row, Col } from 'reactstrap';

import { withRouter } from 'react-router-dom';
import MetaTags from 'react-meta-tags';
import _ from 'lodash';

import { clear } from 'toastr';
// import Header from '../../components/HomePage/header';
import loadingPage from '../../Redux/Actions/loading';
import { GET_ALL_INFLUENCER } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import updateInfluencerData from '../../Redux/Actions/influencer';
import { toastrError } from '../../Redux/Helpers/notification';
import addCmsRedux from '../../Redux/Actions/cms';
import {
  scrollTop, getSEOFromCms, fetchCMSHomepage, googleAnalitycs, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers';
import ItemInfluencerV2 from './ItemInfluencerV2';
import { isBrowser, isMobile } from '../../DetectScreen';
import BlockColumn from '../../components/HomeAlt/BlockColumn';
import icSearch from '../../image/icon/ic-search-influencer.svg';
import JoinTeam from '../OurTeam/Jointeam';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';

const getCms = (influencerBlock) => {
  if (influencerBlock) {
    const { body } = influencerBlock;
    const blockColumn = _.find(body, x => x.type === 'columns_block');
    const itemsBlock = _.find(body, x => x.type === 'items_block');
    return {
      blockColumn,
      itemsBlock,
    };
  }
  return {
    blockColumn: undefined,
    itemsBlock: undefined,
  };
};

const searchData = (influencers, search) => {
  if (!search) {
    return influencers;
  }
  const influencersData = _.filter(influencers, x => x.name.toLowerCase().includes(search.toLowerCase()));
  return influencersData;
};


class Influencer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: undefined,
      blockColumn: undefined,
      itemsBlock: undefined,
    };
    this.timeOutSearch = null;
  }

  fetchDataInit = async () => {
  // data header text
    const { cms } = this.props;
    const influencerBlock = _.find(cms, x => x.title === 'Influencers');
    if (!influencerBlock) {
      const cmsData = await fetchCMSHomepage('influencers');
      this.props.addCmsRedux(cmsData);
      const dataCms = getCms(cmsData);
      this.setState(
        dataCms,
      );
    } else {
      const dataCms = getCms(influencerBlock);
      _.assign(dataCms, { idSlugFocus: '' });
      this.setState(dataCms);
    }
    // data carousel
    const { influencers } = this.props;
    if (!influencers || influencers.length === 0) {
      this.props.loadingPage(true);
      try {
        const result = await this.getAllInfluencer();
        this.props.updateInfluencerData(result);
      } catch (error) {
        toastrError(error.message);
      }
      this.props.loadingPage(false);
    }
  }

  componentDidMount = () => {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/influencer');
    this.fetchDataInit();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  // get api influencer
  getAllInfluencer = () => {
    try {
      const options = {
        url: GET_ALL_INFLUENCER,
        method: 'GET',
      };

      return fetchClient(options);
    } catch (error) {
      return null;
    }
  }

  handleClickShowDetail = (idClick) => {
    const { idSlugFocus } = this.state;
    if (isMobile && idSlugFocus === idClick || isBrowser) {
      this.props.history.push(generateUrlWeb(`/influencer/${idClick}`));
    } else {
      this.setState({ idSlugFocus: idClick });
    }
  }

  onChangeSearch = (e) => {
    const { value } = e.target;
    if (this.timeOutSearch) {
      clearTimeout(this.timeOutSearch);
    }
    this.timeOutSearch = setTimeout(() => {
      this.timeOutSearch = null;
      this.setState({ searchValue: value });
    }, 500);
  }

  render() {
    let seo = {};
    const influencerBlock = _.find(this.props.cms, x => x.title === 'Influencers');
    if (influencerBlock) {
      seo = getSEOFromCms(influencerBlock);
    }

    const { searchValue, blockColumn, itemsBlock } = this.state;
    const { influencers } = this.props;
    const influencersData = searchData(influencers, searchValue);
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/influencer')}
        </MetaTags>
        {/* <Header />
        { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <HeaderHomePageV3 isRemoveMessage />
        <div className="div-influencer-new div-col">
          {
            blockColumn && (
              <BlockColumn data={blockColumn} />
            )
          }
          {/* {
            _.map(influencers, d => (
              <ItemInfluencer isFocus={d.slug === idSlugFocus} data={d} cmsCommon={cmsCommon} handleClickShowDetail={this.handleClickShowDetail} />
            ))
          } */}
          <div className="div-search">
            <div className="content">
              <input type="text" name="search" onChange={this.onChangeSearch} placeholder="Search Ambassador" />
              <img loading="lazy" src={icSearch} alt="search" />
            </div>

          </div>
          <Row className="div-row">
            {
                _.map(influencersData, d => (
                  <Col xs="12" lg="4">
                    <ItemInfluencerV2 data={d} handleClickShowDetail={this.handleClickShowDetail} />
                  </Col>
                ))
              }
          </Row>
          {
            itemsBlock && (
              <JoinTeam data={itemsBlock} history={this.props.history} />
            )
          }
        </div>
        <FooterV2 />
      </div>
    );
  }
}

Influencer.propTypes = {
  influencers: PropTypes.arrayOf(PropTypes.object).isRequired,
  updateInfluencerData: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

Influencer.propTypes = {
  cms: PropTypes.shape().isRequired,
};

function mapStateToProps(state) {
  return {
    influencers: state.influencers,
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  updateInfluencerData,
  loadingPage,
  addCmsRedux,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Influencer));
