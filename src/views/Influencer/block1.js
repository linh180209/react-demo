import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { getAltImage, onClickLink, getNameFromCommon } from '../../Redux/Helpers';
import icInstagram from '../../image/icon/ic-instagram-whilte.svg';
import icAttach from '../../image/icon/ic-attach.png';
// import '../../styles/_influencer-detail-new.scss';
import ItemProduct from './itemProduct';

class Block1 extends Component {
  gotoNewPage = (url) => {
    onClickLink(url);
  }

  render() {
    const { data, cmsCommon } = this.props;
    const {
      instagram, link, avatar, collections, name, title,
    } = data;
    const uniquePerfumeblend = getNameFromCommon(cmsCommon, 'unique_perfume_blend');
    const collection = collections && collections.length > 0 ? collections[0] : {};

    return (
      <div className="div-block-1">
        <div className="div-left">
          <img loading="lazy" className="full-image" src={avatar} alt={getAltImage(avatar)} />
          <div className="div-text div-row justify-between">
            <div className="div-col">
              <h5>
                {name}
              </h5>
              <span>
                {title}
              </span>
            </div>
            <div className="div-row items-center justify-center">
              <img loading="lazy" onClick={() => this.gotoNewPage(instagram)} className="icon" src={icInstagram} alt="instagram" />
              <img loading="lazy" onClick={() => this.gotoNewPage(link)} className="icon ml-4" src={icAttach} alt="attach" />
            </div>
          </div>
        </div>
        <div className="div-right div-col">
          <h1>
            {collection.name}
          </h1>
          <h3 className="sub-title">
            {`by ${name}`}
          </h3>
          <span className="title-product">
            <b>
              {uniquePerfumeblend}
            </b>
          </span>
          <span className="sub-title-product">
            {collection.title}
          </span>
          <div className="div-product div-row justify-between">
            {
              _.map(_.filter(collection.products, x => x.is_featured === true), d => (
                <ItemProduct
                  isBig
                  data={d}
                  onClickAddToCart={this.props.onClickAddToCart}
                  onClickGotoProduct={this.props.onClickGotoProduct}
                  cmsCommon={cmsCommon}
                />
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

Block1.propTypes = {
  data: PropTypes.shape().isRequired,
  cmsCommon: PropTypes.shape().isRequired,
  onClickAddToCart: PropTypes.func.isRequired,
  onClickGotoProduct: PropTypes.func.isRequired,

};

export default Block1;
