/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Row, Col } from 'reactstrap';
import MenuInfluencer from './menuInfluencer';
import ItemProduct from './itemProduct';
import icArrow from '../../image/icon/arrow-down-filter.png';
import { getNameFromCommon } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

class Block3 extends Component {
  state = {
    isShowMore: false,
  }

  render() {
    const {
      cmsCommon, data, onClickAddToCart, onClickGotoProduct,
    } = this.props;
    const { isShowMore } = this.state;
    const { collections, name } = data;
    const collection = collections && collections.length > 0 ? collections[0] : {};
    const collectionsBt = getNameFromCommon(cmsCommon, 'COLLECTIONS');
    const shopPerfume = getNameFromCommon(cmsCommon, 'shop_perfume');
    const viewMore = getNameFromCommon(cmsCommon, 'VIEW_MORE_INGRIDIENTS');
    const isShowBt = !!(isShowMore && collection.products.length > 6);
    const dataShow = isShowBt ? collection.products.slice(0, 6) : collection.products;
    return (
      <div className="div-block-3">
        <div
          className="div-header"
          style={{
            backgroundImage: `url(${collection.banner})`,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            position: 'relative',
          }}
        >
          <h2 style={{ color: collection.text_color }}>
            {collectionsBt}
          </h2>
          <span style={{ color: collection.text_color }}>
            {`${shopPerfume} ${name}`}
          </span>
        </div>
        {/* <MenuInfluencer /> */}
        <div className="div-list-product">
          <Row>
            {
              _.map(dataShow, d => (
                <Col md="4" xs="6" className="col">
                  <ItemProduct data={d} isBig cmsCommon={cmsCommon} onClickAddToCart={onClickAddToCart} onClickGotoProduct={onClickGotoProduct} isShowPrice={isMobile} />
                </Col>
              ))
            }
          </Row>
          <div className={isShowBt ? 'div-button-more' : 'hidden'}>
            <button
              type="button"
              onClick={() => this.setState({ isShowMore: false })}
            >
              <div className="div-style-button">
                <span>
                  {viewMore}
                </span>
                <img src={icArrow} alt="icArrow" />
              </div>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

Block3.propTypes = {
  cmsCommon: PropTypes.shape().isRequired,
  data: PropTypes.shape().isRequired,
  onClickAddToCart: PropTypes.func.isRequired,
  onClickGotoProduct: PropTypes.func.isRequired,
};
export default Block3;
