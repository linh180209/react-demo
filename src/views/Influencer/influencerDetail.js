import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';

import MetaTags from 'react-meta-tags';

import InfluencerDetailCarousel from './influencerDetailCarousel';
import Header from '../../components/HomePage/header';
import CardInfluencer from './CardInfluencer';
import {
  scrollTop, getCmsCommon, getNameFromCommon, getSEOFromCms, fetchCMSHomepage, googleAnalitycs, generateUrlWeb,
} from '../../Redux/Helpers';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { GET_INFLUENCER_DETAIL_URL } from '../../config';
// import '../../styles/_cardInfluencer.scss';
import '../../styles/influencer-detail.scss';
import FooterV2 from '../../views2/footer';

const getCms = (influencerBlock) => {
  if (influencerBlock) {
    const { body } = influencerBlock;
    return {
      body,
    };
  }
  return {
    body: [],
  };
};

class InfluencerDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headerText: undefined,
      arrList: undefined,
    };
  }

  componentDidMount() {
    scrollTop();
    googleAnalitycs('/influencer');
    this.fetchDataInit();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const objectReturn = {};
    const influencerBlock = _.find(cms, x => x.title === 'Influencers');
    if (!influencerBlock) {
      this.props.loadingPage(true);
      const cmsData = await fetchCMSHomepage('influencers');
      this.props.addCmsRedux(cmsData);
      const headerText = getCms(cmsData);
      _.assign(objectReturn, { headerText });
      this.props.loadingPage(false);
    } else {
      const headerText = getCms(influencerBlock);
      _.assign(objectReturn, { headerText });
    }
    try {
      const data = await this.fetchInfluencerDetail();
      if (data && !data.isError) {
        _.assign(objectReturn, { arrList: data });
        this.setState(objectReturn);
      } else {
        this.gotoBack();
      }
    } catch (error) {
      console.log('error', error);
      this.gotoBack();
    }
  }

  fetchInfluencerDetail = () => {
    const { id } = this.props.match.params;
    if (id) {
      const body = {
        url: GET_INFLUENCER_DETAIL_URL.replace('{id}', id),
        method: 'GET',
      };
      return fetchClient(body);
    }
    return null;
  }

  handleClickCloseDetail = () => {
    this.gotoBack();
  }

  gotoBack = () => {
    this.props.history.push(generateUrlWeb('/influencer'));
  }

  render() {
    const { arrList, headerText } = this.state;
    if (!arrList) {
      return null;
    }
    const cmsCommon = getCmsCommon(this.props.cms);
    const closeBt = getNameFromCommon(cmsCommon, 'CLOSE');

    const data = arrList.collections;
    const influencerData = data.map((e, i) => (
      <InfluencerDetailCarousel carouselData={e.products} title={e.name} key={i} />
    ));
    let seo = {};
    const influencerBlock = _.find(this.props.cms, x => x.title === 'Influencers');
    if (influencerBlock) {
      seo = getSEOFromCms(influencerBlock);
    }
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
        </MetaTags>
        <Header />
        <div className="_view-influencer2-Content">
          {(Object.keys(headerText).length)
            ? headerText.body.map((x, index) => (
              <div key={index}>
                <div>
                  <x.value.header.header_size className="headerTitle">
                    {x.value.header.header_text}
                  </x.value.header.header_size>
                </div>
                <div
                  className="description"
                  dangerouslySetInnerHTML={{
                    __html: x.value.paragraph,
                  }}
                />
              </div>
            )) : ''
          }
          <div className="above-card-detail-item">
            <div className="card-detail-item">
              <button type="button" className="btn-close" onClick={this.handleClickCloseDetail}>
                {closeBt}
              </button>
              <CardInfluencer cardItem={arrList} checkFunctionHCSD={false} />
            </div>
            {influencerData}
          </div>
        </div>
        <FooterV2 />
      </div>
    );
  }
}

InfluencerDetail.propTypes = {
  addCmsRedux: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape(PropTypes),
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
  };
}

const mapDispatchToProps = {
  loadingPage,
  addCmsRedux,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(InfluencerDetail));
