import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isBrowser, isMobile } from '../../DetectScreen';

class Block2 extends Component {
  render() {
    const { collections } = this.props.data;
    const collection = collections && collections.length > 0 ? collections[0] : {};

    const textHtml = (
      <div className="div-text" style={isBrowser && !collection.video ? { width: '80%' } : {}}>
        <h2>
          {collection.title}
        </h2>
        <span>
          {collection.description}
        </span>
      </div>
    );
    const videoHtml = (
      <div className="div-video div-col justify-center items-end">
        <video
          id="myVideo"
          style={{
            objectFit: 'cover',
            width: isMobile ? '100%' : '90%',
          }}
          muted
          playsinline="true"
          autoPlay
          loop
          preload="auto"
          src={collection.video}
          poster=""
          controls
        />
      </div>
    );
    return (
      <div className="div-block-2">
        {
          isBrowser ? (
            <React.Fragment>
              {textHtml}
              {collection.video ? videoHtml : <div />}
            </React.Fragment>
          ) : (
            <React.Fragment>
              {collection.video ? videoHtml : <div />}
              {textHtml}
            </React.Fragment>
          )
        }
      </div>
    );
  }
}

Block2.propTypes = {
  data: PropTypes.shape().isRequired,
};

export default Block2;
