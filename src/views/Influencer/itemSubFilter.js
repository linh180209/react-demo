import React, { Component } from 'react';
import _ from 'lodash';

class ItemSubFilter extends Component {
  constructor(props) {
    super(props);
    this.listData = [
      {
        name: 'Popularity',
      },
      {
        name: 'Newest',
      },
      {
        name: 'Oldest',
      },
      {
        name: 'Price: low to high',
      },
      {
        name: 'Price: hight to low',
      },
    ];
  }

  render() {
    return (
      <div className="item-sub-filter">
        <h2>
          SORT BY
        </h2>
        {
          _.map(this.listData, d => (
            <button type="button">
              {d.name}
            </button>
          ))
        }
      </div>
    );
  }
}

export default ItemSubFilter;
