import React, { Component } from 'react';
import Swiper from 'react-id-swiper';
import 'react-id-swiper/src/styles/css/swiper.css';
import 'react-id-swiper/src/styles/scss/swiper.scss';
import {
  Row, Col, FormGroup, Label, Input, Button,
} from 'reactstrap';
import { googleAnalitycs } from '../../Redux/Helpers';

import logo from '../../image/icon/logo-menu.png';

class DoYourOwn extends Component {
  componentDidMount = () => {
    googleAnalitycs('/doyourown');
  };

  render() {
    const params = {
      spaceBetween: 30,
      effect: 'fade',
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    };

    return (
      <div>
        <img className="img-logo-absolute" src={logo} alt="logo" />
        <div>
          <Row>
            <Col md="1" />
            <Col md="4" style={{ marginTop: '100px' }}>
              <Swiper {...params}>
                <div>
                  <img loading="lazy" className="img-slide" src="https://media.giphy.com/media/lp0UDSUFeX16HwQcmY/giphy.gif" alt="1" />
                </div>
                <div>
                  <img loading="lazy" className="img-slide" src="https://media.giphy.com/media/pjV7UmXqiQK6IPrAX9/giphy.gif" alt="2" />
                </div>
                <div>
                  <img loading="lazy" className="img-slide" src="https://media.giphy.com/media/FR9nGyxk8Z5HG/giphy.gif" alt="3" />
                </div>
                <div>
                  <img loading="lazy" className="img-slide" src="https://media.giphy.com/media/q4bNfKEvgEO5y/giphy.gif" alt="4" />
                </div>
              </Swiper>
            </Col>
            <Col md="2" />
            <Col md="4" style={{ marginTop: '100px' }}>
              <Swiper {...params}>
                <div>
                  <img loading="lazy" className="img-slide" src="https://media.giphy.com/media/lp0UDSUFeX16HwQcmY/giphy.gif" alt="1" />
                </div>
                <div>
                  <img loading="lazy" className="img-slide" src="https://media.giphy.com/media/pjV7UmXqiQK6IPrAX9/giphy.gif" alt="2" />
                </div>
                <div>
                  <img loading="lazy" className="img-slide" src="https://media.giphy.com/media/FR9nGyxk8Z5HG/giphy.gif" alt="3" />
                </div>
                <div>
                  <img loading="lazy" className="img-slide" src="https://media.giphy.com/media/q4bNfKEvgEO5y/giphy.gif" alt="4" />
                </div>
              </Swiper>
            </Col>
            <Col md="1" />
          </Row>
          <Row>
            <Col md="1" />
            <Col md="4" className="text-info__center">
              <span>
                Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes
              </span>
            </Col>
            <Col md="2" />
            <Col md="4" className="text-info__center">
              <span>
                Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id
              </span>
            </Col>
            <Col md="1" />
          </Row>
          <Row style={{ marginTop: '20px' }}>
            <Col md="1" />
            <Col md="2">
              <FormGroup check style={{ marginLeft: '70px' }}>
                <Label check>
                  <Input type="checkbox" id="checkbox1" />
                  {' '}
                  All
                </Label>
              </FormGroup>
              <FormGroup check style={{ marginLeft: '70px' }}>
                <Label check>
                  <Input type="checkbox" id="checkbox2" />
                  {' '}
                  Fruity
                </Label>
              </FormGroup>
            </Col>
            <Col md="2">
              <FormGroup check style={{ marginLeft: '70px' }}>
                <Label check>
                  <Input type="checkbox" id="checkbox1" />
                  {' '}
                  Oud
                </Label>
              </FormGroup>
              <FormGroup check style={{ marginLeft: '70px' }}>
                <Label check>
                  <Input type="checkbox" id="checkbox1" />
                  {' '}
                  Citrus
                </Label>
              </FormGroup>
            </Col>
            <Col md="2" />
            <Col md="2">
              <FormGroup check style={{ marginLeft: '70px' }}>
                <Label check>
                  <Input type="checkbox" id="checkbox1" />
                  {' '}
                  All
                </Label>
              </FormGroup>
              <FormGroup check style={{ marginLeft: '70px' }}>
                <Label check>
                  <Input type="checkbox" id="checkbox2" />
                  {' '}
                  Fruity
                </Label>
              </FormGroup>
            </Col>
            <Col md="2">
              <FormGroup check style={{ marginLeft: '70px' }}>
                <Label check>
                  <Input type="checkbox" id="checkbox1" />
                  {' '}
                  Oud
                </Label>
              </FormGroup>
              <FormGroup check style={{ marginLeft: '70px' }}>
                <Label check>
                  <Input type="checkbox" id="checkbox1" />
                  {' '}
                  Citrus
                </Label>
              </FormGroup>
            </Col>
            <Col md="1" />
          </Row>
          <Row>
            <Col md="1" />
            <Col md="4" className="text-info__center">
              <Button>
                Random
              </Button>
            </Col>
            <Col md="2" />
            <Col md="4" className="text-info__center">
              <Button>
                Random
              </Button>
            </Col>
            <Col md="1" />
          </Row>
          <Row style={{ marginTop: '40px' }}>
            <Col md="6" style={{ textAlign: 'end' }}>
              <Button
                style={{ width: '150px' }}
                color="success"
              >
                Save to organ
              </Button>
            </Col>
            <Col md="6">
              <Button
                style={{ width: '150px' }}
                color="success"
              >
                Order
              </Button>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default DoYourOwn;
