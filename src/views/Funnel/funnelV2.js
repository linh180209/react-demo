/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import '../../styles/funnelPageV2.scss';
import {
  scrollTop, fetchCMSHomepage, getSEOFromCms, googleAnalitycs, getNameFromButtonBlock, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady, getSearchPathName,
} from '../../Redux/Helpers';
import ItemFunnel from './itemFunnel';
import ItemFunnelV2 from './itemFunnelV2';
import Header from '../../components/HomePage/header';
import addCmsRedux from '../../Redux/Actions/cms';
import funnel1 from '../../image/icon/funnel1.svg';
import funnel2 from '../../image/icon/funnel2.svg';
import funnel3 from '../../image/icon/funnel3.svg';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';

const getCms = (funnelBlock) => {
  if (funnelBlock) {
    const { body } = funnelBlock;
    const banner_block = _.filter(body, x => x.type === 'banner_block');
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    const seo = getSEOFromCms(funnelBlock);
    return {
      banner_block, seo, buttonBlocks,
    };
  }
  return {
    banner_block: [], seo: {}, buttonBlocks: [],
  };
};
class FunnelV2 extends Component {
  constructor(props) {
    super(props);
    const { infoGift } = this.props.location.state ? this.props.location.state : {};
    this.state = {
      banner_block: [],
      seo: {},
      infoGift,
      buttonBlocks: [],
    };
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/funnel');
    this.fetchDataInit();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const funnelBlock = _.find(cms, x => x.title === 'Funnel');
    if (!funnelBlock) {
      const cmsData = await fetchCMSHomepage('funnel');
      this.props.addCmsRedux(cmsData);
      const {
        banner_block, seo, buttonBlocks,
      } = getCms(cmsData);
      this.setState({
        banner_block,
        seo,
        buttonBlocks,
      });
    } else {
      const {
        banner_block, seo, buttonBlocks,
      } = getCms(funnelBlock);
      this.setState({
        banner_block,
        seo,
        buttonBlocks,
      });
    }
  }

  onClick = (link) => {
    const { infoGift } = this.state;
    if (infoGift) {
      if (link.toLowerCase() === '/mmo') {
        const { pathname, search } = getSearchPathName(generateUrlWeb(`/mmo-gift/?codes=${infoGift.codes}`));
        this.props.history.push({
          pathname,
          search,
          state: { infoGift },
        });
        return;
      }
      const { pathname, search } = getSearchPathName(generateUrlWeb(`${link}/?codes=${infoGift.codes}`));
      this.props.history.push({
        pathname,
        search,
        state: { infoGift },
      });
    } else {
      this.props.history.push(generateUrlWeb(link));
    }
  }

  render() {
    const {
      banner_block: bannerBlock, seo, buttonBlocks,
    } = this.state;
    const createOwn = getNameFromButtonBlock(buttonBlocks, 'Create_your_own_perfume');
    const howSelected = getNameFromButtonBlock(buttonBlocks, 'SELECT_HOW_YOU_WANT_TO_MAKE_IT');
    return (
      <div className="div-funnel-wrap-v2">
        <div>
          <MetaTags>
            <title>
              {seo ? seo.seoTitle : ''}
            </title>
            <meta name="description" content={seo ? seo.seoDescription : ''} />
            <meta name="robots" content="index, follow" />
            <meta name="revisit-after" content="3 month" />
            {generateHreflang(this.props.countries, '/funnel-quiz')}
          </MetaTags>
          {/* <Header isDisableScrollShow isSpecialMenu isNotShowRegion /> */}
          <HeaderHomePageV3 isRemoveMessage />
          <div className="div-funnel-new">
            <div className="header-text">
              <h2>
                {createOwn}
              </h2>
              <span className="des-text">
                {howSelected}
              </span>
            </div>
            <div className="div-wrap-item-funnel">

              <ItemFunnelV2 icon={funnel1} data={bannerBlock && bannerBlock.length > 0 ? bannerBlock[0].value : undefined} onClick={this.onClick} />
              <ItemFunnelV2 icon={funnel2} data={bannerBlock && bannerBlock.length > 1 ? bannerBlock[1].value : undefined} onClick={this.onClick} isMid />
              <ItemFunnelV2 icon={funnel3} data={bannerBlock && bannerBlock.length > 2 ? bannerBlock[2].value : undefined} onClick={this.onClick} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

FunnelV2.propTypes = {
  addCmsRedux: PropTypes.func.isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(FunnelV2));
