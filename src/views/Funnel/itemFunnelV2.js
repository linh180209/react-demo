import React, { useState, useEffect } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { isMobile } from '../../DetectScreen';
import icTime from '../../image/icon/icTimeFunnel.svg';

function ItemFunnelV2(props) {
  const {
    data, onClick, icon,
  } = props;

  return (
    <div
      type="button"
      onClick={() => onClick(data.button.link)}
      className={`div-item-funnel-v2 ${props.isMid ? 'mid' : ''}`}
    >
      <div className="div-image">
        <img src={icon} alt="icon" />
      </div>
      <div className="info-text">
        <h4>
          {data ? data.header.header_text : ''}
        </h4>
        <div className={isMobile ? 'hidden' : 'line'} />
        <span className="text-des">{data ? ReactHtmlParser(data.description) : ''}</span>
        <div className="div-time">
          <img src={icTime} alt="timer" />
          <span>
            {data ? data.header2.header_text : ''}
          </span>
        </div>
      </div>
    </div>
  );
}

export default ItemFunnelV2;
