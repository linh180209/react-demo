/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactHtmlParser from 'react-html-parser';
import icWatch from '../../image/icon/ic-watch-whilte.svg';
import icNext from '../../image/icon/ic-vector-next.svg';

class ItemFunnel extends Component {
  state = {
    isHover: false,
  };

  toggleHover = () => {
    const { isHover } = this.state;
    this.setState({ isHover: !isHover });
  }

  onClickImage = (link) => {
    // if (this.state.isHover && isMobile) {
    this.props.onClick(link);
    // }
  }

  render() {
    // const { isHover } = this.state;
    const {
      data, onClick,
    } = this.props;
    return (
      <div
        className="div-item-funnel"
        style={{
          backgroundImage: `linear-gradient(to bottom, rgba(21, 36, 54, 0.1), rgba(21, 36, 54, 0.9)), url(${data && data.image ? data.image.image : ''})`,
        }}
        type="button"
        onMouseEnter={this.toggleHover}
        onMouseLeave={this.toggleHover}
        onClick={() => onClick(data.button.link)}
      >
        <div className="div-content">
          <div className="div-header">
            <h1>
              {data ? data.header.header_text : ''}
            </h1>
            <div className="div-subtitle">
              <img src={icWatch} alt="icWatch" />
              <span>
                {data ? data.header2.header_text : ''}
              </span>
            </div>
          </div>
          {/* {
            isMobileHover || isBrowser ? ( */}
          <React.Fragment>
            <div className="text-description animated fadeIn">
              <span>
                {data ? ReactHtmlParser(data.description) : ''}
              </span>
            </div>
            <div className="div-button animated fadeIn">
              <button type="button" onClick={() => onClick(data.button.link)}>
                {data ? data.button.text : ''}
                <img className="ml-1" src={icNext} alt="icNext" />
              </button>
            </div>
          </React.Fragment>
          {/* ) : (<React.Fragment />)
          } */}
        </div>
      </div>
    );
  }
}

ItemFunnel.propTypes = {
  data: PropTypes.shape().isRequired,
  onClick: PropTypes.func.isRequired,
  linkForcus: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
};

export default ItemFunnel;
