/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import MetaTags from 'react-meta-tags';
import '../../styles/funnelPage.scss';
import {
  scrollTop, fetchCMSHomepage, getSEOFromCms, googleAnalitycs, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady, getSearchPathName,
} from '../../Redux/Helpers';
import ItemFunnel from './itemFunnel';

import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';

import addCmsRedux from '../../Redux/Actions/cms';
import FooterV2 from '../../views2/footer';

const getCms = (funnelBlock) => {
  if (funnelBlock) {
    const { body } = funnelBlock;
    const banner_block = _.filter(body, x => x.type === 'banner_block');
    const seo = getSEOFromCms(funnelBlock);
    return {
      banner_block, seo,
    };
  }
  return {
    banner_block: [], seo: {},
  };
};
class Funnel extends Component {
  constructor(props) {
    super(props);
    const { infoGift } = this.props.location.state ? this.props.location.state : {};
    this.state = {
      banner_block: [],
      seo: {},
      infoGift,
    };
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/funnel');
    this.fetchDataInit();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const funnelBlock = _.find(cms, x => x.title === 'Funnel');
    if (!funnelBlock) {
      const cmsData = await fetchCMSHomepage('funnel');
      this.props.addCmsRedux(cmsData);
      const {
        banner_block, seo,
      } = getCms(cmsData);
      this.setState({
        banner_block,
        seo,
      });
    } else {
      const {
        banner_block, seo,
      } = getCms(funnelBlock);
      this.setState({
        banner_block,
        seo,
      });
    }
  }

  onClick = (link) => {
    const { infoGift } = this.state;
    if (infoGift) {
      if (link.toLowerCase() === '/mmo') {
        const { pathname, search } = getSearchPathName(generateUrlWeb(`/mmo-gift/?codes=${infoGift.codes}`));
        this.props.history.push({
          pathname,
          search,
          state: { infoGift },
        });
        return;
      }
      const { pathname, search } = getSearchPathName(generateUrlWeb(`${link}/?codes=${infoGift.codes}`));
      this.props.history.push({
        pathname,
        search,
        state: { infoGift },
      });
    } else {
      this.props.history.push(generateUrlWeb(link));
    }
  }

  render() {
    const {
      banner_block: bannerBlock, seo,
    } = this.state;
    return (
      <div className="div-funnel-wrap">
        <div>
          <MetaTags>
            <title>
              {seo ? seo.seoTitle : ''}
            </title>
            <meta name="description" content={seo ? seo.seoDescription : ''} />
            <meta name="robots" content="index, follow" />
            <meta name="revisit-after" content="3 month" />
            {generateHreflang(this.props.countries, '/funnel')}
          </MetaTags>
          {/* <Header isDisableScrollShow isSpecialMenu isNotShowRegion /> */}
          <HeaderHomePageV3 isRemoveMessage />
          <div className="div-funnel-new">
            <ItemFunnel data={bannerBlock && bannerBlock.length > 0 ? bannerBlock[0].value : undefined} onClick={this.onClick} />
            <ItemFunnel data={bannerBlock && bannerBlock.length > 1 ? bannerBlock[1].value : undefined} onClick={this.onClick} />
            <ItemFunnel data={bannerBlock && bannerBlock.length > 2 ? bannerBlock[2].value : undefined} onClick={this.onClick} />
          </div>
          <FooterV2 />
        </div>
      </div>
    );
  }
}

Funnel.propTypes = {
  addCmsRedux: PropTypes.func.isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Funnel));
