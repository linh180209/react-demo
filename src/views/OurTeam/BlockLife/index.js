import React, { useRef, useEffect } from 'react';
import _ from 'lodash';
import HtmlParser from 'react-html-parser';
import '../../../styles/block-life.scss';
import { getAltImageV2 } from '../../../Redux/Helpers';
import { isBrowser, isMobile } from '../../../DetectScreen';

function BlockLife(props) {
  const mouseDown = useRef(false);
  const startX = useRef();
  const scrollLeft = useRef();


  useEffect(() => {
    const slider = document.getElementById('draw-mouse');
    const startDragging = (e) => {
      mouseDown.current = true;
      startX.current = e.pageX - slider.offsetLeft;
      scrollLeft.current = slider.scrollLeft;
    };

    const stopDragging = () => {
      mouseDown.current = false;
    };

    const moveDragging = (e) => {
      e.preventDefault();
      if (!mouseDown.current) { return; }
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX.current);
      slider.scrollLeft = scrollLeft.current - walk;
    };
    if (isBrowser) {
      slider.addEventListener('mousemove', moveDragging);
      slider.addEventListener('mousedown', startDragging, false);
      slider.addEventListener('mouseup', stopDragging, false);
      slider.addEventListener('mouseleave', stopDragging, false);
    }
    return (() => {
      if (isBrowser) {
        slider.removeEventListener('mousedown', startDragging, false);
        slider.removeEventListener('mouseup', stopDragging, false);
        slider.removeEventListener('mouseleave', stopDragging, false);
        slider.removeEventListener('mousemove', moveDragging);
      }
    });
  }, []);

  return (
    <div className="block-life">
      <h1>
        {props.data.value.header.header_text}
      </h1>
      <div className="des">
        {HtmlParser(props.data.value.benefits[0].value.text)}
      </div>
      <div className="image-bg">
        <img loading="lazy" src={props.data.value?.image_background?.image} alt={getAltImageV2(props.data.value?.image_background)} />
      </div>
      <div id="draw-mouse" className="list-image">
        {
          _.map(props.data.value.benefits, d => (
            <img loading="lazy" src={d.value.image.image} alt={getAltImageV2(d.value.image)} />
          ))
        }
      </div>
      {
        isMobile && (
          <div className="line-bottom" />
        )
      }

    </div>
  );
}

export default BlockLife;
