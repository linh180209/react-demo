import React, { useEffect, useState } from 'react';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';

import '../../styles/our-team.scss';
import '../../styles/our-boutique.scss';
// import Header from '../../components/HomePage/header';
import loadingPage from '../../Redux/Actions/loading';
import addCmsRedux from '../../Redux/Actions/cms';
import {
  fetchCMSHomepage, generateHreflang, getSEOFromCms, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers';
import { useMergeState } from '../../Utils/customHooks';
import BlockColumn from '../../components/HomeAlt/BlockColumn';
import BlockBanner from '../../components/HomeAlt/BlockBanner';
import BlockLife from './BlockLife';
import JoinTeam from './Jointeam';
import OurBoutiqueBlock from '../AboutUs/OurBoutiqueBlock';
import CTABlockOurTeam from './ctaBlockOurTeam';
import BlockBannerV2 from '../../components/HomeAlt/BlockBannerV2';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';

const getCms = (cms) => {
  const objectReturn = {
    seo: undefined,
  };

  if (!_.isEmpty(cms)) {
    const seo = getSEOFromCms(cms);
    _.assign(objectReturn, {
      seo,
    });
  }
  return objectReturn;
};

function OurTeam(props) {
  const [ourTeamCms, setOurTeamCms] = useState({});
  const [state, setState] = useMergeState({
    seo: undefined,
  });

  const fetchData = async () => {
    const ourTeam = _.find(props.cms, x => x.title === 'Our Team');
    if (!ourTeam) {
      const cmsData = await fetchCMSHomepage('our-team');
      props.addCmsRedux(cmsData);
      const dataFromStore = getCms(cmsData);
      setOurTeamCms(cmsData);
      setState(dataFromStore);
    } else {
      const dataFromStore = getCms(ourTeam);
      setState(dataFromStore);
      setOurTeamCms(ourTeam);
    }
  };

  useEffect(() => {
    setPrerenderReady();
    fetchData();
    return (() => {
      removeLinkHreflang();
    });
  }, []);

  return (
    <div>
      <MetaTags>
        <title>
          {state.seo ? state.seo.seoTitle : ''}
        </title>
        <meta name="description" content={state.seo ? state.seo.seoDescription : ''} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(props.countries, '/our-team')}
      </MetaTags>
      {/* <Header />
      { props.showAskRegion && (<div className="div-temp-region" />) } */}
      <HeaderHomePageV3 />
      <div className="our-team">
        {
          _.map(ourTeamCms.body, (d, index) => {
            switch (d.type) {
              case 'columns_block':
                return <BlockColumn data={d} />;
              case 'banner_block':
                return <BlockBannerV2 id={`banner_block-${index}`} data={d} isOurTeamReadMore />;
              case 'benefits_block':
                return <BlockLife data={d} />;
              case 'items_block':
                return <JoinTeam data={d} history={props.history} />;
              case 'stores_block':
                return <OurBoutiqueBlock data={d} className="our-team-boutique" />;
              case 'cta_block':
                return <CTABlockOurTeam data={d} />;
              default:
                return null;
            }
          })
        }
      </div>
      <FooterV2 />
    </div>
  );
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OurTeam));
