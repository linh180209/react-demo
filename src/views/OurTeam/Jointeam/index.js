import React from 'react';
import '../../../styles/join-team-block.scss';
import classNames from 'classnames';
import { gotoUrl } from '../../../Redux/Helpers';
import ButtonCT from '../../../components/ButtonCT';

function JoinTeam(props) {
  console.log('props.data', props.data);
  const onClickButton = (link) => {
    gotoUrl(link, props.history);
  };

  return (
    <div className="join-team-block">
      <h2 className="header_2">
        {props.data.value.text}
      </h2>
      {
        props.data.value.items && props.data.value.items.length > 0 && (
          <div className="header_4">
            {props.data.value.items[0].value.description}
          </div>
        )
      }
      <ButtonCT
        className={classNames('button-black')}
        name={props.data.value.button.name}
        onClick={() => onClickButton(props.data.value.button.link)}
      />
    </div>
  );
}

export default JoinTeam;
