import React, { useState, useEffect } from 'react';
import HtmlParser from 'react-html-parser';
// import { getAltImageV2 } from '../../Redux/Helpers';
import '../../styles/cta-block-our-team.scss';

function CTABlockOurTeam(props) {
  console.log('CTABlockOurTeam', props.data);
  const { value } = props.data;
  return (
    <div className="cta-block-our-team">
      <div className="content">
        <h2 className="header_2">
          {value?.image?.caption}
        </h2>
        <div className="header_4 des">
          {HtmlParser(value?.description)}
        </div>
        {/* <div className="signature">
          <img loading="lazy" src={value?.image?.image} alt={getAltImageV2(value?.image)} />
          <h4>
            {value?.link }
          </h4>
          <span>
            {value?.text }
          </span>
        </div> */}
      </div>
    </div>
  );
}

export default CTABlockOurTeam;
