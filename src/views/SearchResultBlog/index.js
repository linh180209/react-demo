/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import {
  Player, ControlBar, Shortcut, BigPlayButton,
} from 'video-react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import MetaTags from 'react-meta-tags';
import HtmlParser from 'react-html-parser';

import {
  Container, Row, Col, Breadcrumb, BreadcrumbItem,
} from 'reactstrap';
import moment from 'moment';
import HeaderHomePage from '../../components/HomePage/header';
import { toastrError } from '../../Redux/Helpers/notification';
import loadingPage from '../../Redux/Actions/loading';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { loginUpdateUserInfo } from '../../Redux/Actions/login';
import icDefault from '../../image/icon/default-image.png';
import icBackToTop from '../../image/icon/ic-back-to-top.svg';
import icBackToTopMobile from '../../image/icon/ic-scroll-top-mobile.svg';
import {
  scrollTop,
  getSEOFromCms,
  fetchCMSHomepage,
  googleAnalitycs,
  generateUrlWeb,
  setPrerenderReady,
} from '../../Redux/Helpers';
import './styles.scss';
import addCmsRedux from '../../Redux/Actions/cms';
import SearchBar from '../Blogs/components/SearchBar';
import WhatTrending from '../Blogs/components/WhatTrending';
import SubscribeMail from '../Blogs/components/SubscribeMail';
// import { GET_ALL_ARTICLES } from '../../config';
import { isMobile } from '../../DetectScreen';
import FooterV2 from '../../views2/footer';


const prePareCms = (mmoCms) => {
  if (mmoCms) {
    const seo = getSEOFromCms(mmoCms);
    // const { body } = mmoCms;
    return {
      seo,
    };
  }
  return {
    seo: undefined,
  };
};
const prePareDataArticle = (data) => {
  const { body } = data;
  if (data) {
    const whatTrendingBlocks = _.filter(
      body,
      x => x.type === 'trending_block',
    )[0].value;
    const newLetterBlock = _.filter(
      body,
      x => x.type === 'newsletter_block',
    )[0].value;
    return {
      whatTrendingBlocks,
      newLetterBlock,
    };
  }
  return {
    whatTrendingBlocks: [],
    newLetterBlock: [],
  };
};
class SearchResultBlog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seo: undefined,
      num: 6,
      dataSearchFilter: null,
      searchContent: null,
    };
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs(window.location.pathname);
    this.fetchDataInit();
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  fetchDataSearch = (data) => {
    const search = this.props.match.params.search
      ? this.props.match.params.search
      : undefined;
    if (search && data) {
      const dataSearchFilterArticle = data.posts.filter(x => x.title.toLowerCase().includes(search.toLowerCase()));
      if (dataSearchFilterArticle.length <= 0) {
        const dataSearchFilterCat = data.posts.filter(x => x.categories.find(y => y.toLowerCase().includes(search.toLowerCase())));
        this.setState({
          ...this.state,
          dataSearchFilter: dataSearchFilterCat,
          searchContent: search,
        });
        return;
      }
      this.setState({
        ...this.state,
        dataSearchFilter: dataSearchFilterArticle,
        searchContent: search,
      });
      return;

      console.log(this.state.dataSearchFilter);
    }
  };

  // fetchArticles = () => {
  //   const options = {
  //     url: GET_ALL_ARTICLES,
  //     method: 'GET',
  //   };
  //   return fetchClient(options);
  // };

  fetchDataInit = async () => {
    const { cms, mmos } = this.props;
    const { dataArticles } = this.state;
    const mmoCms = _.find(cms, x => x.title === 'MMO-V2');
    if (!mmoCms || _.isEmpty(mmos) || _.isEmpty(dataArticles)) {
      this.props.loadingPage(true);
      const pending = [fetchCMSHomepage('mmo-v2')];
      try {
        const results = await Promise.all(pending);
        const cmsData = results[0];
        const resultDataArticles = results[0];
        this.props.addCmsRedux(cmsData);
        this.fetchDataSearch(resultDataArticles);
        const { seo } = prePareCms(cmsData);
        const { whatTrendingBlocks, newLetterBlock } = prePareDataArticle(
          resultDataArticles,
        );
        this.setState({
          seo,
          dataArticles: resultDataArticles,
          whatTrendingBlocks,
          newLetterBlock,
        });
        this.props.loadingPage(false);
      } catch (error) {
        toastrError(error.message);
        this.props.loadingPage(false);
      }
    } else {
      const { cms: cmsPage } = this.props;
      const cmsT = _.find(cmsPage, x => x.title === 'MMO-V2');
      const { seo } = prePareCms(cmsT);
      const products = this.createData(results);
      this.setState({
        seo,
        dataArticles,
        whatTrendingBlocks,
        newLetterBlock,
      });
    }
  };

  handleScroll = () => {
    if (
      document.body.scrollTop > 600
      || document.documentElement.scrollTop > 600
    ) {
      const ele = document.getElementById('scroll-top-btn');
      if (ele) {
        ele.style.opacity = '1';
      }
    } else {
      const ele = document.getElementById('scroll-top-btn');
      if (ele) {
        ele.style.opacity = '0';
      }
    }
  };

  handleScrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  handleRoute = (slug) => {
    this.props.history.push(generateUrlWeb(`/blog/${slug}`));
    window.location.reload();
  };

  handleLoadMore = () => {
    if (
      (this.state.dataSearchFilter
        && this.state.dataSearchFilter.length > this.state.num)
      || (this.state.dataArticles
        && this.state.dataArticles.posts.length > this.state.num)
    ) {
      this.setState({ num: this.state.num + 6 });
    }
  };

  render() {
    const {
      seo,
      dataArticles,
      whatTrendingBlocks,
      num,
      dataSearchFilter,
      newLetterBlock,
      searchContent,
    } = this.state;
    const { login } = this.props;
    const gender = login && login.user && login.user.gender ? login.user.gender : 'unisex';

    return (
      <div>
        <MetaTags>
          <title>{seo ? seo.seoTitle : ''}</title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
        </MetaTags>
        <HeaderHomePage isBgBlack isSpecialMenu isDisableScrollShow isNotShowRegion />
        <div className="wrapper-category-blogs">
          <SearchBar data={dataArticles} />
          {/* {!dataBannerHeader && (
            <Link className="back-btn" to="/blog">
              <img src={icBack} alt="back" /> back to home
            </Link>
          )} */}
          <div>
            <Container className="container-lg">
              <Row>
                <Col md="8">
                  {dataSearchFilter && dataSearchFilter.length > 0 ? (
                    <h1 className="title-cat">
                      Search Results for
                      {' '}
                      {searchContent || ''}
                    </h1>
                  ) : (
                    <h1 className="title-cat">
                      No result found
                    </h1>
                  )}
                </Col>
              </Row>
              <Row>
                <Col md="8">
                  <div className="articles-box-cat">
                    {dataSearchFilter && dataSearchFilter.length > 0 ? (
                      dataSearchFilter.slice(0, num).map((x, index) => (
                        <div
                          className="article animated fadeIn faster"
                          onClick={() => this.handleRoute(x.slug)}
                          key={index}
                        >
                          <div className="wrapper-image-article">
                            <img src={x.image ? x.image : icDefault} />
                          </div>
                          <div className="content-article">
                            <span className="cat">
                              {x.categories ? _.join(x.categories, ', ') : ''}
                            </span>
                            /blog
                            <Link
                              to={generateUrlWeb(`/blog/${x.slug}`)}
                              className="title-article"
                            >
                              {x.title ? x.title : ''}
                            </Link>
                            <span className="date">
                              {x.first_published_at
                                && moment(x.first_published_at).format(
                                  'ddd, DD MMMM YYYY',
                                )}
                            </span>
                          </div>
                        </div>
                      ))
                    ) : (
                      <div>
                        {dataArticles
                          ? dataArticles.posts.slice(0, num).map((x, index) => (
                            <div
                              className="article animated fadeIn faster"
                              key={index}
                            >
                              <div className="wrapper-image-article" onClick={() => this.handleRoute(x.slug)}>
                                <img src={x.image ? x.image : icDefault} />
                              </div>
                              <div className="content-article">
                                <span className="cat">
                                  {x.categories
                                    ? _.join(x.categories, ', ')
                                    : ''}
                                </span>
                                <Link
                                  to={generateUrlWeb(`/blog/${x.slug}`)}
                                  className="title-article"
                                >
                                  {x.title ? x.title : ''}
                                </Link>
                                <span className="date" onClick={() => this.handleRoute(x.slug)}>
                                  {x.first_published_at
                                      && moment(x.first_published_at).format(
                                        'ddd, DD MMMM YYYY',
                                      )}
                                </span>
                              </div>
                            </div>
                          ))
                          : ''}
                      </div>
                    )}
                  </div>
                  {dataSearchFilter && dataSearchFilter.length > 6 ? (
                    <div className="paging">
                      <p>
                        Showing
                        {' '}
                        {dataSearchFilter.length > num
                          ? num
                          : dataSearchFilter.length}
                        {' '}
                        of
                        {' '}
                        {dataSearchFilter.length}
                        {' '}
                        items
                      </p>
                      <button
                        onClick={() => this.handleLoadMore()}
                        disbled={!dataSearchFilter.length > 6}
                        style={{
                          opacity: dataSearchFilter.length > 6 ? 1 : 0.2,
                        }}
                      >
                        load more
                      </button>
                    </div>
                  ) : dataArticles && dataArticles.posts.length > 6 ? (
                    <div className="paging">
                      <p>
                        Showing
                        {' '}
                        {dataArticles.posts.length > num
                          ? num
                          : dataArticles.posts.length}
                        {' '}
                        of
                        {' '}
                        {dataArticles.posts.length}
                        {' '}
                        items
                      </p>
                      <button
                        onClick={() => this.handleLoadMore()}
                        disbled={!dataArticles.posts.length > 6}
                        style={{
                          opacity: dataArticles.posts.length > 6 ? 1 : 0.2,
                        }}
                      >
                        load more
                      </button>
                    </div>
                  ) : (
                    ''
                  )}
                </Col>
                <Col md="4">
                  <WhatTrending
                    data={whatTrendingBlocks}
                    dataArticles={dataArticles}
                  />
                  <SubscribeMail
                    data={newLetterBlock}
                    login={login}
                    loginUpdateUserInfo={this.props.loginUpdateUserInfo}
                  />
                </Col>
              </Row>
            </Container>
          </div>
          <div
            className="scroll-top-btn"
            id="scroll-top-btn"
            onClick={() => this.handleScrollTop()}
          >
            <img src={isMobile ? icBackToTopMobile : icBackToTop} />
            <span style={{ display: isMobile ? 'none' : 'block' }}>
              back to the top
            </span>
          </div>
          <FooterV2 />
        </div>
      </div>
    );
  }
}

SearchResultBlog.propTypes = {
  loadingPage: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  loginUpdateUserInfo: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    cms: state.cms,
    mmos: state.mmos,
    basket: state.basket,
  };
}

const mapDispatchToProps = {
  loadingPage,
  addCmsRedux,
  loginUpdateUserInfo,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(SearchResultBlog),
);
