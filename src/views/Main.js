/* eslint-disable react/sort-comp */
import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import { MetaTags } from 'react-meta-tags';
import { connect } from 'react-redux';
import {
  BrowserRouter as Router, Route, Switch,
} from 'react-router-dom';
import Loading from '../components/Loading';
import FunctionSegment from '../componentsv2/functionSegment';
import { getCommonCMSRequest } from '../Redux/Actions/cms';
import updateAllCountry from '../Redux/Actions/country';
import getAllRequest from '../Redux/Actions/getall';
import reloadPageRequest from '../Redux/Actions/reload';
import updateShowAskRegion from '../Redux/Actions/showAskRegion';
import {
  addClassIntoElemenetID, changeUrl, fetchCountry, getCodeUrl, getDiscountSaveStore, removeMeta,
} from '../Redux/Helpers';
import auth from '../Redux/Helpers/auth';
import Full from './Full';

const ResetPassWord = React.lazy(() => import('./ForgotPassword/resetPassWord'));
// const LoginB2B = React.lazy(() => import('./LoginPage/loginB2B'));
const VerificationPage = React.lazy(() => import('./Verification'));

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReadyLoad: auth.getRealCountry(),
    };
    this.loadedAll = false;
  }

  specialRedirectLink = (countries) => {
    if (window.location.pathname.includes('/redirect')) {
      const ele = _.find(countries, x => x.code.toLowerCase() === auth.getCountryHeader());
      if (ele) {
        document.location.href = window.location.pathname.replace('/redirect', `/${auth.getCountryHeader()}`) + window.location.search;
      } else {
        document.location.href = window.location.pathname.replace('/redirect', '/') + window.location.search;
      }
      return true;
    }
    return false;
  };

  componentWillMount() {
    if (window.location.pathname.indexOf('/pay/success/') === 0) {
      changeUrl(auth.getCountry());
      return;
    }
    fetchCountry().then((result) => {
      const { headers, data } = result;
      // check special linked
      if (this.specialRedirectLink(data)) {
        return;
      }
      this.props.updateAllCountry(_.orderBy(data, 'name', 'asc'));
      auth.setCountryHeader(headers['accept-country'].toLowerCase());
      const ele = _.find(data, x => x.code.toLowerCase() === headers['accept-country'].toLowerCase());
      const symbolUrl = getCodeUrl();
      if (symbolUrl) {
        const ele1 = _.find(data, x => x.code.toLowerCase() === symbolUrl);
        if (ele1) {
          auth.setCountry(JSON.stringify(ele1));
        } else if (ele) {
          auth.setCountry(JSON.stringify(ele));
          changeUrl(ele?.code);
        }
        this.setState({ isReadyLoad: true });
        return;
      }
      if (ele?.code === 'us' && (ele?.code === symbolUrl || symbolUrl === '')) {
        this.setState({ isReadyLoad: true });
        return;
      }
      if (ele) {
        auth.setCountry(JSON.stringify(ele));
        changeUrl(ele?.code);
      } else {
        auth.setCountry('');
        this.setState({ isReadyLoad: true });
      }
    }).catch(() => {
      this.setState({ isReadyLoad: true });
    });
  }

  componentDidMount() {
    // get discount
    getDiscountSaveStore();

    // set page product all
    auth.setPositionScrollProduct(0);
    auth.setLimitProduct();

    this.getAllData();

    window.onbeforeunload = () => {
      this.props.reloadPageRequest();
    };
  }

  getAllData = () => {
    this.props.getAllRequest();
    this.props.getCommonCMSRequest();
    this.loadedAll = true;
  }

  addFontCustom = () => {
    const pathName = window.location.pathname;
    const array = pathName.split('/');
    if (array && array.length > 1) {
      if (['vn', 'kr'].includes(array[1])) {
        addClassIntoElemenetID('root', 'f-open-sans');
      }
      if (['sa'].includes(array[1])) {
        addClassIntoElemenetID('root', 'f-PFDinTextAR');
        addClassIntoElemenetID('html-root', 'font-plus-2');
      }
    }
  }

  render() {
    const { isReadyLoad } = this.state;
    if (!isReadyLoad) {
      return null;
    }

    // add prerender 301
    const isLinkRedirect = window.location.pathname.includes('/us');
    if (!isLinkRedirect) {
      removeMeta();
    }

    this.addFontCustom();
    return (
      <main>
        <MetaTags>
          {
            isLinkRedirect && (
              <meta className="prerender-301" name="prerender-status-code" content="301" />
            )
          }
        </MetaTags>
        <Loading />
        <Router>
          <FunctionSegment />
          <React.Suspense fallback={<div />}>
            <Switch>
              <Route path="/api/auth/password/reset/confirm/:uidb64/:token/" name="resetPassword" component={ResetPassWord} />
              <Route path="/:language/api/auth/password/reset/confirm/:uidb64/:token/" name="resetPassword" component={ResetPassWord} />
              <Route path="/verify/:token" name="Verify" component={VerificationPage} />
              <Route path="/:language/verify/:token" name="Verify" component={VerificationPage} />
              <Full />

            </Switch>
          </React.Suspense>
        </Router>
      </main>
    );
  }
}

Main.propTypes = {
  getAllRequest: PropTypes.func.isRequired,
  reloadPageRequest: PropTypes.func.isRequired,
  getCommonCMSRequest: PropTypes.func.isRequired,
};


const mapDispatchToProps = {
  getAllRequest,
  reloadPageRequest,
  getCommonCMSRequest,
  updateAllCountry,
  updateShowAskRegion,
};

export default connect(null, mapDispatchToProps)(Main);
// export default Main;
