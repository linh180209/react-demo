/* eslint-disable react/sort-comp */
import classnames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Modal, ModalBody } from 'reactstrap';
import '../../styles/selectionPerfume.scss';
import '../../styles/style.scss';

import MetaTags from 'react-meta-tags';
import CustomeBottleV3 from '../../components/CustomeBottleV3';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import PopupAppointment from '../../components/PopupAppointment';
import { GET_BOTTLE_PRODUCTS, POST_COMBINATIONS_URL } from '../../config';
import { COLOR_CUSTOME, FONT_CUSTOME } from '../../constants';
import { isBrowser, isMobile } from '../../DetectScreen';
import icBack from '../../image/icon/ic-back-combinations.svg';
import icClose from '../../image/icon/ic-close-region.svg';
import icBackMobile from '../../image/icon/icBackMB.svg';
import {
  addProductBasket, createBasketGuest,
} from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import {
  addFontCustom, fetchCMSHomepage, generaCurrency, generateHreflang,
  generateUrlWeb, getCmsCommon, getNameFromButtonBlock, getNameFromCommon, getProductDualCrayonCombo, getSearchPathName, getSEOFromCms, googleAnalitycs, removeLinkHreflang, scrollTop, sendPerfumeInspirationPerfumeSelectedHotjarEvent, setPrerenderReady,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';
import FooterV2 from '../../views2/footer';
import ItemResultScent from '../ResultScentV2/itemResultScent';
import ItemPerfumeSelection from './itemPerfumeSelection';

const getCMS = (data) => {
  if (data) {
    const seo = getSEOFromCms(data);
    const { header_text: headerText, body, image_background: imageBg } = data;
    const btBlock = _.filter(body, x => x.type === 'button_block');
    const imageBlock = _.find(body, x => x.type === 'image_block');
    const textBlock = _.filter(body, x => x.type === 'text_block');
    return {
      headerText, imageBg, btBlock, textBlock, seo, imageBlock,
    };
  }
  return {
    headerText: undefined, imageBg: undefined, btBlock: [], textBlock: [], seo: undefined, imageBlock: undefined,
  };
};

class SelectionPerfume extends Component {
  constructor(props) {
    super(props);
    const { infoGift } = this.props.location.state ? this.props.location.state : {};
    this.state = {
      perfumers: [{
        data: undefined,
      }],
      match: props.match,
      headerText: undefined,
      imageBg: undefined,
      btBlock: [],
      textBlock: [],
      seo: undefined,
      isCenter: true,
      infoGift,
      isShowCustom: false,
      productCustome: undefined,
      bottlePerfume: {},
      dataCustom: {
        currentImg: infoGift ? infoGift.image : undefined,
        nameBottle: infoGift && infoGift.isDisplayName ? infoGift.name : '',
        font: infoGift ? infoGift.font : FONT_CUSTOME.JOST,
        color: infoGift ? infoGift.color : COLOR_CUSTOME.BLACK,
      },
      isShowIngredient: false,
      ingredientDetail: undefined,
      isOpenLegalNotice: false,
      isOpenAppointment: false,
      dataQuizSelect: props.dataQuizSelect,
    };
    console.log('SelectionPerfume =====');
  }

  componentDidMount() {
    setPrerenderReady();
    googleAnalitycs('/create-perfume');
    this.fetchDataInit();
    this.getBottle();
    this.fetchDataFromUrl();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { match } = nextProps;
    if (match !== prevState.match) {
      _.assign(objectReturn, { match, isUpdateUrl: true });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  componentDidUpdate = () => {
    const { isUpdateUrl } = this.state;
    if (isUpdateUrl) {
      this.state.isUpdateUrl = false;
      this.fetchDataFromUrl();
    }
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchDataFromUrl = () => {
    const { product1, product2, product3 } = this.props.match?.params;
    console.log('fetchDataFromUrl', this.props.match?.params);
    const externalProducts = [];
    if (product1 && !product1.includes('?codes=')) {
      externalProducts.push(product1);
    }
    if (product2 && !product2.includes('?codes=')) {
      externalProducts.push(product2);
    }
    if (product3 && !product3.includes('?codes=')) {
      externalProducts.push(product3);
    }
    if (externalProducts.length > 0) {
      this.handleAddDataFromUrl(externalProducts);
    }
  }

  getBottle = async () => {
    try {
      const option = {
        url: GET_BOTTLE_PRODUCTS,
        method: 'GET',
      };
      fetchClient(option).then((result) => {
        if (result && !result.isError && result.length > 0) {
          this.setState({ bottlePerfume: result[0] });
        }
      });
    } catch (error) {
      console.log('error', error);
    }
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const cmsPage = _.find(cms, x => x.title === 'Your Perfume');
    if (!cmsPage) {
      const cmsData = await fetchCMSHomepage('your-perfume');
      this.props.addCmsRedux(cmsData);
      const {
        headerText, imageBg, btBlock, textBlock, seo, imageBlock,
      } = getCMS(cmsData);
      this.setState({
        headerText, imageBg, btBlock, textBlock, seo, imageBlock,
      });
    } else {
      const {
        headerText, imageBg, btBlock, textBlock, seo, imageBlock,
      } = getCMS(cmsPage);
      this.setState({
        headerText, imageBg, btBlock, textBlock, seo, imageBlock,
      });
    }
  }

  fetchProductAll = (externalProducts) => {
    const options = {
      url: POST_COMBINATIONS_URL,
      method: 'POST',
      body: {
        external_products: externalProducts,
      },
    };
    return fetchClient(options);
  }

  fetchProduct = (externalProduct) => {
    const options = {
      url: POST_COMBINATIONS_URL,
      method: 'POST',
      body: {
        external_products: [externalProduct],
      },
    };
    return fetchClient(options);
  }

  handleAddDataFromUrl = async (externalProducts) => {
    try {
      this.props.loadingPage(true);
      const products = await this.fetchProductAll(externalProducts);
      const perfumers = [];
      _.forEach(products.external_products, (d) => {
        const {
          accords, id, image, name,
        } = d;
        perfumers.push({
          data: {
            accords, id, image, name,
          },
          product: d,
        });
      });
      if (perfumers.length < 3) {
        perfumers.push({ data: undefined });
      }
      this.setState({ perfumers, isCenter: false });
    } catch (error) {
      toastrError(error.message);
    } finally {
      this.props.loadingPage(false);
    }
  };

  onClickPerfume = async (data) => {
    if (this.props.isQuizScreen) {
      // this.props.onChangeYourPefume(data.id);
      scrollTop();
      this.setState({ dataQuizSelect: data });
      return;
    }
    const { infoGift } = this.state;
    // console.log('wqwqqq', `${window.location.pathname}/${data.id}`);
    // console.log('wqwqqq 11', `${window.location.pathname}`);
    if (infoGift) {
      const { pathname, search } = getSearchPathName(generateUrlWeb(`${window.location.pathname}${data.id}${`/?codes=${infoGift.codes}`}`));
      this.props.history.push({
        pathname,
        search,
        state: this.props.location.state,
      });
    } else {
      this.props.history.push(generateUrlWeb(`${window.location.pathname}/${data.id}`));
    }
    sendPerfumeInspirationPerfumeSelectedHotjarEvent();

    // console.log('onClickPerfume', data);
    // const { perfumers } = this.state;
    // this.props.loadingPage(true);
    // const product = await this.fetchProduct(data.id);
    // if (perfumers.length === 3) {
    //   perfumers[2].data = data;
    //   _.assign(perfumers[2], { product: product.external_products[0] });
    // } else if (perfumers.length > 0) {
    //   perfumers[perfumers.length - 1].data = data;
    //   _.assign(perfumers[perfumers.length - 1], { product: product.external_products[0] });
    //   perfumers.push({ data: undefined });
    // }
    // this.setState({ perfumers });
    // this.props.loadingPage(false);
  }

  clearPerfume = (data) => {
    const { perfumers } = this.state;
    _.remove(perfumers, x => x.data && x.data.id === data.id);
    if (perfumers.length === 0 || perfumers[perfumers.length - 1].data !== undefined) {
      perfumers.push({ data: undefined });
    }
    this.setState({ perfumers });
  }

  clickShowResult = () => {
    const { perfumers, infoGift } = this.state;
    const externalProducts = [];
    _.forEach(perfumers, (x) => {
      if (x.data !== undefined) {
        externalProducts.push(x.data.id);
      }
    });
    const { pathname, search } = getSearchPathName(generateUrlWeb(`/create-perfume/outcome${infoGift ? `/?codes=${infoGift.codes}` : ''}`));
    this.props.history.push({
      pathname,
      search,
      state: { externalProducts, infoGift },
    });
  }

  updateDataA = (data) => {
    this.dataA = data;
  }

  updateCenterScreen = () => {
    this.setState({ isCenter: false });
  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  handleCropImage = (img) => {
    const { dataCustom } = this.state;
    dataCustom.currentImg = img;
    this.setState({ dataCustom });
  };

  onSaveCustomer = (data) => {
    const { dataCustom } = this.state;
    // const {
    //   croppedImageUrlDone, nameBottle, isOnlyName, isBlack,
    // } = data;
    const {
      image, name, color, font,
    } = data;
    dataCustom.color = color;
    dataCustom.font = font;
    if (_.isEmpty(image)) {
      dataCustom.currentImg = undefined;
      dataCustom.nameBottle = name;
      this.forceUpdate();
      return;
    }
    dataCustom.currentImg = image;
    dataCustom.nameBottle = name;
    const imagePremade = _.find(this.arrayThumbs, x => x.image === image) ? _.find(this.arrayThumbs, x => x.image === image).id : undefined;
    _.assign(dataCustom, { imagePremade });
    this.forceUpdate();
  }

  onClickChoiseGift = (product, name) => {
    const { infoGift, dataCustom } = this.state;
    const { pathname, search } = getSearchPathName(generateUrlWeb(`/checkout-gift/?codes=${infoGift.codes}`));
    this.props.history.push({
      pathname,
      search,
      state: {
        codes: infoGift.codes,
        item: product,
        customeBottle: dataCustom,
      },
    });
  }

  onClickAddToCart = async (item, name, externalProduct, datas) => {
    console.log('item, name, externalProduct', item, name, this.state.dataCustom, externalProduct, datas);
    const isDualCrayons = item.is_sample;
    const { basket } = this.props;
    const { dataCustom } = this.state;
    let itemAddCart;
    let dataCT;
    if (isDualCrayons) {
      const scents = _.filter(datas.combo, x => x.product.type === 'Scent');
      const productFinal = await getProductDualCrayonCombo(scents[0]?.product?.id, scents[1]?.product?.id);
      itemAddCart = productFinal?.items[0];
      _.assign(itemAddCart, { name: productFinal?.name });
      dataCT = {};
    } else {
      itemAddCart = item;
      dataCT = dataCustom;
    }
    const bottleImage = dataCT.currentImg
      ? await fetch(dataCT.currentImg).then(r => r.blob())
      : undefined;
    // const { items } = product;
    // const item = items[0];
    const data = {
      item: itemAddCart.id,
      is_featured: itemAddCart.is_featured,
      is_black: dataCT.isBlack,
      color: dataCT.color,
      font: dataCT.font,
      price: itemAddCart.price,
      imagePremade: dataCT.imagePremade,
      is_display_name: (!bottleImage && !!dataCT.nameBottle) || !!dataCT.nameBottle || !!externalProduct,
      is_customized: !!bottleImage || !!dataCT.imagePremade,
      file: bottleImage
        ? new File([bottleImage], 'product.png')
        : undefined,
      quantity: 1,
      name: isDualCrayons ? itemAddCart.name : dataCT.nameBottle,
      external_product: externalProduct,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    if (!basket.id) {
      this.props.createBasketGuest(dataTemp);
    } else {
      this.props.addProductBasket(dataTemp);
    }
  }

  onClickReset = () => {
    this.props.history.goBack();
    // this.setState({
    //   perfumers: [{
    //     data: undefined,
    //   }],
    // });
  }

  onClickImage = async (data, isCrayons) => {
    const { dataCustom } = this.state;
    const { items, combo } = data;
    const scents = _.filter(combo, x => x.product.type === 'Scent');
    if (isCrayons) {
      const productFinal = await getProductDualCrayonCombo(scents[0]?.product?.id, scents[1]?.product?.id);
      this.props.history.push(generateUrlWeb(`/product/dual_crayons/${productFinal.id}`));
    } else {
      const product = _.find(items, x => !x.is_sample);
      console.log('onClickImage', data);
      if (product) {
        const { pathname, search } = getSearchPathName(generateUrlWeb(`/product/perfume_diy/${product.product.id}`));
        this.props.history.push({
          pathname,
          search,
          state: {
            custome: dataCustom ? {
              name: dataCustom?.nameBottle,
              isCustome: false,
              font: dataCustom?.font,
              image: dataCustom?.currentImg,
              color: dataCustom?.color,
            } : undefined,
          },
        });
      }
    }
    // const { combo } = data;
    // const scents = _.filter(combo, x => x.product.type === 'Scent');
    // if (scents.length === 2) {
    //   this.props.history.push(`/products/${scents[0].product.id}/${scents[1].product.id}`);
    // }
  }

  render() {
    const { isQuizScreen } = this.props;
    const {
      perfumers, headerText, imageBlock, btBlock, textBlock, seo, isCenter, infoGift,
      isShowCustom, dataCustom, bottlePerfume, productCustome, isShowIngredient, ingredientDetail,
      dataQuizSelect,
    } = this.state;
    const cmsCommon = getCmsCommon(this.props.cms);
    const ml30 = getNameFromButtonBlock(btBlock, '30ml');
    const typeInBt = getNameFromButtonBlock(btBlock, 'type_in_below');
    const hereAreYouBt = getNameFromButtonBlock(btBlock, 'Here_are_your_perfume_combinations');
    const typeYourPerfumeBt = getNameFromButtonBlock(btBlock, 'Type_your_perfume_here');
    const basedOnBt = getNameFromButtonBlock(btBlock, 'based_on_your_favorite_perfumes');
    const legalBt = getNameFromButtonBlock(btBlock, 'Legal Notice');
    const haveBt = getNameFromButtonBlock(btBlock, 'Having trouble with your mix');
    const bookAppoitmentBt = getNameFromButtonBlock(btBlock, 'Book an appointment');
    const legalInfoBt = getNameFromButtonBlock(btBlock, 'legal info');
    const whichPerfumeBt = getNameFromButtonBlock(btBlock, 'Which perfume do you currently use');
    const backBt = getNameFromCommon(cmsCommon, 'BACK');
    const nextBt = getNameFromCommon(cmsCommon, 'NEXT');
    const skipBt = getNameFromCommon(cmsCommon, 'SKIP');
    let subTitle;
    console.log('imageBlock', perfumers);
    if (perfumers.length === 1) {
      subTitle = textBlock && textBlock.length > 0 ? textBlock[0].value : '';
    } else if (perfumers.length === 2) {
      subTitle = textBlock && textBlock.length > 7 ? textBlock[7].value : '';
    } else if (perfumers.length === 3) {
      subTitle = textBlock && textBlock.length > 8 ? textBlock[8].value : '';
    }
    const createBt = getNameFromButtonBlock(btBlock, 'show perfume combinations');
    this.arrayThumbs = _.filter(bottlePerfume ? bottlePerfume.images : [], x => x.type === 'suggestion');
    const { product } = productCustome || {};
    const { combo } = product || {};
    const comboProduct = combo ? _.filter(combo, x => x.product.type !== 'Bottle') : [];
    const modalLegal = (
      <Modal className={`modal-legal-note ${addFontCustom()}`} isOpen={this.state.isOpenLegalNotice} centered>
        <ModalBody>
          <div className="body-content">
            <button
              onClick={() => this.setState({ isOpenLegalNotice: false })}
              className="button-bg__none bt-close"
              type="button"
            >
              <img src={icClose} alt="close" />
            </button>
            <h1>
              {legalBt}
            </h1>
            <span>
              {legalInfoBt}
            </span>
          </div>
        </ModalBody>
      </Modal>
    );

    const countryName = auth.getCountryName();
    return (
      <div>
        {
            !isQuizScreen && (
              <React.Fragment>
                <MetaTags>
                  <title>
                    {seo ? replaceCountryInMetaTags(countryName, seo?.seoTitle) : ''}
                  </title>
                  <meta
                    name="description"
                    content={seo ? replaceCountryInMetaTags(countryName, seo?.seoDescription) : ''}
                  />
                  <meta name="robots" content="index, follow" />
                  <meta name="revisit-after" content="3 month" />
                  {generateHreflang(this.props.countries, '/create-perfume')}
                </MetaTags>
                {/* <HeaderHomePage />
                { this.props.showAskRegion && (<div className="div-temp-region" />) } */}

                <HeaderHomePageV3 />

                <IngredientPopUp
                  isShow={isShowIngredient}
                  data={ingredientDetail || {}}
                  onCloseIngredient={this.onCloseIngredient}
                  history={this.props.history}
                />
                {modalLegal}
                <PopupAppointment
                  isOpen={this.state.isOpenAppointment}
                  buttonBlocks={btBlock}
                  imageBackground={imageBlock?.value}
                  onClose={() => this.setState({ isOpenAppointment: false })}
                />
              </React.Fragment>
            )
          }
        {
            isShowCustom
              ? (
                <CustomeBottleV3
                  cmsCommon={cmsCommon}
                  arrayThumbs={this.arrayThumbs}
                  handleCropImage={this.handleCropImage}
                  currentImg={dataCustom.currentImg}
                  nameBottle={dataCustom.nameBottle}
                  font={dataCustom.font}
                  color={dataCustom.color}
                  closePopUp={() => this.setState({ isShowCustom: false })}
                  onSave={this.onSaveCustomer}
                  itemBottle={{
                    price: bottlePerfume ? bottlePerfume.items[0].price : '0',
                  }}
                  cmsTextBlocks={textBlock}
                  name1={comboProduct.length > 0 ? comboProduct[0].name : ''}
                  name2={comboProduct.length > 1 ? comboProduct[1].name : ''}
                />
              ) : (<div />)
          }
        <div className={classnames('div-selection-perfume', isQuizScreen ? 'quiz-screen' : '')}>
          <button
            data-gtmtracking="funnel-2-step-04-perfume-proposal-back"
            type="button"
            onClick={this.onClickReset}
            className={perfumers.length > 1 ? '' : 'hidden'}
          >
            <img
              data-gtmtracking="funnel-2-step-04-perfume-proposal-back"
              src={isMobile ? icBackMobile : icBack}
              alt="icBack"
            />
            {backBt}
          </button>
          <h1 className={perfumers.length <= 1 ? 'center-div' : ''}>
            {isQuizScreen ? whichPerfumeBt : perfumers.length <= 1 ? headerText : hereAreYouBt}
          </h1>
          <span>
            {perfumers.length <= 1 ? typeInBt : basedOnBt}
          </span>
          <div className={isCenter ? 'div-list-item center-item' : 'div-list-item'}>
            {
              perfumers && perfumers.length === 2 && isBrowser
                ? (
                  <div className="item-temp" />
                ) : (<React.Fragment />)
            }
            {
              _.map(perfumers, (x, index) => (_.isEmpty(x.product) ? (
                <ItemPerfumeSelection
                  dataGtmtracking={index === 0 ? 'funnel-2-step-02-favorite-perfume-1' : index === 1 ? 'funnel-2-step-02-favorite-perfume-2' : 'funnel-2-step-02-favorite-perfume-3'}
                  data={x.data}
                  onClickPerfume={this.onClickPerfume}
                  clearPerfume={this.clearPerfume}
                  isAddMore={index > 0 && perfumers.length > 1}
                  title={textBlock && textBlock.length > 3 ? textBlock[3].value : ''}
                  placeholder1={typeYourPerfumeBt}
                  placeholder2={typeYourPerfumeBt}
                  isSearchFunc
                  updateDataA={this.updateDataA}
                  dataA={this.dataA}
                  updateCenterScreen={this.updateCenterScreen}
                  dataQuizSelect={dataQuizSelect}
                  clearDataQuiz={() => this.setState({ dataQuizSelect: undefined })}
                  cmsCommon={cmsCommon}
                />
              ) : (
                <div className={index === perfumers.length - 1 ? 'div-item-result' : 'div-item-result border-right-item'}>
                  <ItemResultScent
                    dataGtmtracking="funnel-2-step-04-perfume-proposal-add-to-cart"
                    data={{
                      data: x.product.product,
                      subTitle: x.product.name,
                      title: textBlock && textBlock.length > 5 ? textBlock[5].value : '',
                      price: generaCurrency(`${parseInt(x.product.product.items[0].price, 10)}`),
                      ml: ml30,
                    }}
                    cmsCommon={cmsCommon}
                    onClickAddToCart={(item, name, customeBottle, data) => this.onClickAddToCart(item, name, x.product.id, data)}
                    onClickIngredient={this.onClickIngredient}
                    onClickImage={(data, isRollon) => this.onClickImage(x.product.product, isRollon)}
                    onClickCustomBottle={() => this.setState({ isShowCustom: true, productCustome: x })}
                    customeBottle={this.state.dataCustom}
                    isDisableSwitchScent
                    onClickCheckOutGift={this.onClickChoiseGift}
                    isGift={!!infoGift}
                    isDisableNameScent
                    isPerfumeCombinations
                    isNewInfo
                  />
                  {
                    isBrowser ? (
                      <span>
                        {textBlock && textBlock.length > 6 ? textBlock[6].value : ''}
                      </span>
                    ) : (<div />)
                  }

                  <ItemPerfumeSelection data={x.product} isNotEdit title={textBlock && textBlock.length > 3 ? textBlock[3].value : ''} cmsCommon={cmsCommon} />
                </div>
              )))
            }
          </div>
          {
            isQuizScreen && (
              <div className="div-button">
                {isMobile && dataQuizSelect && <hr />}
                {
                  isBrowser && (
                    <button
                      type="button"
                      className="out-line"
                      onClick={() => this.props.onChangeYourPefume()}
                    >
                      {skipBt}
                    </button>
                  )
                }
                {
                  dataQuizSelect && (
                    <button
                      type="button"
                      onClick={() => this.props.onChangeYourPefume(dataQuizSelect)}
                    >
                      {nextBt}
                    </button>
                  )
                }

              </div>
            )
          }

          {
            !isQuizScreen && (
              <div className={classnames('legal-appointment', perfumers.length > 1 ? 'mg-bottom' : '')}>
                <button
                  onClick={() => this.setState({ isOpenLegalNotice: true })}
                  type="button"
                  className="button-bg__none underline bt-legal"
                >
                  {legalBt}
                </button>
                <div className="div-appoinment">
                  <span>
                    {haveBt}
                  </span>
                  <button
                    onClick={() => this.setState({ isOpenAppointment: true })}
                    type="button"
                    className="button-bg__none underline"
                  >
                    {bookAppoitmentBt}
                  </button>
                </div>
              </div>
            )
          }

        </div>
        {
          !isQuizScreen && <FooterV2 />
        }

      </div>

    );
  }
}

SelectionPerfume.defaultProps = {
  isQuizScreen: false,
};

SelectionPerfume.propTypes = {
  cms: PropTypes.shape().isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  isQuizScreen: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    login: state.login,
    basket: state.basket,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  createBasketGuest,
  addProductBasket,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SelectionPerfume));
