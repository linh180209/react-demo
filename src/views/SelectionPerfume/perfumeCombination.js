/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import MetaTags from 'react-meta-tags';
import queryString from 'query-string';
import '../../styles/perfume_combinations.scss';

import HeaderHomePage from '../../components/HomePage/header';
import icBack from '../../image/icon/ic-back-combinations.svg';
import ItemResult from '../ResultScent/itemResult';
import ItemPerfumeSelection from './itemPerfumeSelection';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import {
  addProductBasket, createBasketGuest,
} from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import { POST_COMBINATIONS_URL, GET_BOTTLE_PRODUCTS } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import {
  getCmsCommon, fetchCMSHomepage, getSEOFromCms, getNameFromCommon, scrollTop, googleAnalitycs, generaCurrency, getNameFromButtonBlock, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady, getSearchPathName,
} from '../../Redux/Helpers';
import ItemResultScent from '../ResultScentV2/itemResultScent';
import CustomeBottle from '../ProductB2C/customeBottle';
import { FONT_CUSTOME, COLOR_CUSTOME } from '../../constants';
import CustomeBottleV3 from '../../components/CustomeBottleV3';
import BlockIcon from '../../components/HomeAlt/BlockIcon';
import { isBrowser } from '../../DetectScreen';
import FooterV2 from '../../views2/footer';

const getCMS = (data) => {
  if (data) {
    const seo = getSEOFromCms(data);
    const { body, image_background: imageBg } = data;
    const iconBlock = _.find(body, x => x.type === 'icons_block');
    const btBlock = _.filter(body, x => x.type === 'button_block');
    const textBlock = _.filter(body, x => x.type === 'text_block');
    const headerText = _.find(body, x => x.type === 'header_block');
    return {
      headerText, imageBg, btBlock, textBlock, seo, iconBlock,
    };
  }
  return {
    headerText: undefined, imageBg: [], btBlock: [], textBlock: [], seo: undefined, iconBlock: undefined,
  };
};

class PerfumeCombination extends Component {
  constructor(props) {
    super(props);
    const { infoGift } = this.props.location.state ? this.props.location.state : {};
    this.state = {
      isShowIngredient: false,
      ingredientDetail: undefined,
      datas: [],
      headerText: undefined,
      imageBg: undefined,
      btBlock: [],
      textBlock: [],
      seo: undefined,
      infoGift,
      redeemCode: undefined,
      isShowCustom: false,
      productCustome: undefined,
      bottlePerfume: {},
      dataCustom: {
        currentImg: infoGift ? infoGift.image : undefined,
        nameBottle: infoGift && infoGift.isDisplayName ? infoGift.name : '',
        font: infoGift ? infoGift.font : FONT_CUSTOME.JOST,
        color: infoGift ? infoGift.color : COLOR_CUSTOME.BLACK,
      },
    };
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    this.getBottle();

    if (this.props.location && this.props.location.state) {
      const { externalProducts, infoGift } = this.props.location.state;
      this.setState({ redeemCode: infoGift ? infoGift.codes : '', infoGift });
      if (externalProducts) {
        this.fetchData(externalProducts);
      } else {
        this.props.history.push(generateUrlWeb('/create-perfume'));
      }
    } else {
      this.props.history.push(generateUrlWeb('/create-perfume'));
    }
    googleAnalitycs('/create-perfume/outcome');
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchData = async (externalProducts) => {
    const { cms } = this.props;
    const cmsPage = _.find(cms, x => x.title === 'Based on your perfume');
    if (!cmsPage) {
      const cmsData = await fetchCMSHomepage('your-perfume');
      this.props.addCmsRedux(cmsData);
      const {
        headerText, imageBg, btBlock, textBlock, seo, iconBlock,
      } = getCMS(cmsData);
      this.setState({
        headerText, imageBg, btBlock, textBlock, seo, iconBlock,
      });
    } else {
      const {
        headerText, imageBg, btBlock, textBlock, seo, iconBlock,
      } = getCMS(cmsPage);
      this.setState({
        headerText, imageBg, btBlock, textBlock, seo, iconBlock,
      });
    }

    const options = {
      url: POST_COMBINATIONS_URL,
      method: 'POST',
      body: {
        external_products: externalProducts,
      },
    };
    fetchClient(options, true).then((result) => {
      if (result && !result.isError) {
        const { external_products: datas } = result;
        this.setState({ datas });
      }
    });
  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  onClickChoiseGift = (product, name) => {
    const { infoGift, dataCustom } = this.state;
    const { pathname, search } = getSearchPathName(generateUrlWeb(`/checkout-gift/?codes=${infoGift.codes}`));
    this.props.history.push({
      pathname,
      search,
      state: {
        codes: infoGift.codes,
        item: product,
        customeBottle: dataCustom,
      },
    });
  }

  onClickAddToCart = async (item, name) => {
    const { basket } = this.props;
    const { dataCustom } = this.state;
    const bottleImage = dataCustom.currentImg
      ? await fetch(dataCustom.currentImg).then(r => r.blob())
      : undefined;
    // const { items } = product;
    // const item = items[0];
    const data = {
      item: item.id,
      is_featured: item.is_featured,
      is_black: dataCustom.isBlack,
      color: dataCustom.color,
      font: dataCustom.font,
      price: item.price,
      imagePremade: dataCustom.imagePremade,
      is_display_name: (!bottleImage && !!dataCustom.nameBottle) || !!dataCustom.nameBottle,
      is_customized: !!bottleImage || !!dataCustom.imagePremade,
      file: bottleImage
        ? new File([bottleImage], 'product.png')
        : undefined,
      quantity: 1,
      name: dataCustom.nameBottle,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    if (!basket.id) {
      this.props.createBasketGuest(dataTemp);
    } else {
      this.props.addProductBasket(dataTemp);
    }
  }

  onClickImage = (data) => {
    const { combo } = data;
    const scents = _.filter(combo, x => x.product.type === 'Scent');
    if (scents.length === 2) {
      this.props.history.push(generateUrlWeb(`/products/${scents[0].product.id}/${scents[1].product.id}`));
    }
  }

  getBottle = async () => {
    try {
      const option = {
        url: GET_BOTTLE_PRODUCTS,
        method: 'GET',
      };
      fetchClient(option).then((result) => {
        if (result && !result.isError && result.length > 0) {
          this.setState({ bottlePerfume: result[0] });
        }
      });
    } catch (error) {
      console.log('error', error);
    }
  }

  handleCropImage = (img) => {
    const { dataCustom } = this.state;
    dataCustom.currentImg = img;
    this.setState({ dataCustom });
  };

  onSaveCustomer = (data) => {
    const { dataCustom } = this.state;
    // const {
    //   croppedImageUrlDone, nameBottle, isOnlyName, isBlack,
    // } = data;
    const {
      image, name, color, font,
    } = data;
    dataCustom.color = color;
    dataCustom.font = font;
    if (_.isEmpty(image)) {
      dataCustom.currentImg = undefined;
      dataCustom.nameBottle = name;
      this.forceUpdate();
      return;
    }
    dataCustom.currentImg = image;
    dataCustom.nameBottle = name;
    const imagePremade = _.find(this.arrayThumbs, x => x.image === image) ? _.find(this.arrayThumbs, x => x.image === image).id : undefined;
    _.assign(dataCustom, { imagePremade });
    this.forceUpdate();
  }

  render() {
    const {
      isShowIngredient, ingredientDetail, datas, headerText, imageBg, btBlock, textBlock, seo,
      redeemCode, isShowCustom, dataCustom, bottlePerfume, productCustome, iconBlock,
    } = this.state;
    const { cms } = this.props;
    const cmsCommon = getCmsCommon(cms);
    const backBt = getNameFromCommon(cmsCommon, 'BACK');
    const ml30 = getNameFromButtonBlock(btBlock, '30ml');
    this.arrayThumbs = _.filter(bottlePerfume ? bottlePerfume.images : [], x => x.type === 'suggestion');
    const { product } = productCustome || {};
    const { combo } = product || {};
    const comboProduct = combo ? _.filter(combo, x => x.product.type !== 'Bottle') : [];
    console.log('datas infoGift', datas);
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/create-perfume/outcome')}
        </MetaTags>
        <HeaderHomePage />
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <div className="div-perfume-combination" style={{ backgroundImage: `url(${imageBg})` }}>
          <IngredientPopUp
            isShow={isShowIngredient}
            data={ingredientDetail || {}}
            onCloseIngredient={this.onCloseIngredient}
            history={this.props.history}
          />
          {
            isShowCustom
              ? (
                <CustomeBottleV3
                  cmsCommon={cmsCommon}
                  arrayThumbs={this.arrayThumbs}
                  handleCropImage={this.handleCropImage}
                  currentImg={dataCustom.currentImg}
                  nameBottle={dataCustom.nameBottle}
                  font={dataCustom.font}
                  color={dataCustom.color}
                  closePopUp={() => this.setState({ isShowCustom: false })}
                  onSave={this.onSaveCustomer}
                  itemBottle={{
                    price: bottlePerfume ? bottlePerfume.items[0].price : '0',
                  }}
                  cmsTextBlocks={textBlock}
                  name1={comboProduct.length > 0 ? comboProduct[0].name : ''}
                  name2={comboProduct.length > 1 ? comboProduct[1].name : ''}
                />
              ) : (<div />)
          }
          <button
            data-gtmtracking="funnel-2-step-04-perfume-proposal-back"
            type="button"
            onClick={() => this.props.history.push(generateUrlWeb('/create-perfume'))}
          >
            <img data-gtmtracking="funnel-2-step-04-perfume-proposal-back" src={icBack} alt="icBack" />
            {backBt}
          </button>
          <h1>
            {headerText ? headerText.value.header_text : ''}
          </h1>
          <span>
            {textBlock && textBlock.length > 4 ? textBlock[4].value : ''}
          </span>
          <div className="list-perfume-result">
            {
              _.map(datas, (x, index) => (
                <div className={index === datas.length - 1 ? 'div-item-result' : 'div-item-result border-right-item'}>
                  <ItemResultScent
                    dataGtmtracking="funnel-2-step-04-perfume-proposal-add-to-cart"
                    data={{
                      data: x.product,
                      subTitle: x.name,
                      title: textBlock && textBlock.length > 5 ? textBlock[5].value : '',
                      price: generaCurrency(`${parseInt(x.product.items[0].price, 10)}`),
                      ml: ml30,
                    }}
                    cmsCommon={cmsCommon}
                    onClickAddToCart={this.onClickAddToCart}
                    onClickIngredient={this.onClickIngredient}
                    onClickImage={() => this.onClickImage(x.product)}
                    onClickCustomBottle={() => this.setState({ isShowCustom: true, productCustome: x })}
                    customeBottle={this.state.dataCustom}
                    isDisableSwitchScent
                    isOnlyPerfume
                    onClickCheckOutGift={this.onClickChoiseGift}
                    isGift={redeemCode}
                    isDisableNameScent
                    isPerfumeCombinations
                  />
                  {
                    isBrowser ? (
                      <span>
                        {textBlock && textBlock.length > 6 ? textBlock[6].value : ''}
                      </span>
                    ) : (<div />)
                  }

                  <ItemPerfumeSelection data={x} isNotEdit cmsCommon={cmsCommon} />
                </div>
              ))
            }
          </div>
        </div>
        <BlockIcon heroCms={iconBlock} />
        <FooterV2 />
      </div>
    );
  }
}

PerfumeCombination.propTypes = {
  addCmsRedux: PropTypes.func.isRequired,
  cms: PropTypes.shape().isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    state: PropTypes.shape({
      externalProducts: PropTypes.arrayOf(),
      infoGift: PropTypes.shape(),
    }),
  }).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  basket: PropTypes.shape().isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    basket: state.basket,
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  createBasketGuest,
  addProductBasket,
  addCmsRedux,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PerfumeCombination));
