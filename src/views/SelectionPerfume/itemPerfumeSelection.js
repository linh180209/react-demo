/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classnames from 'classnames';
import icSearch from '../../image/icon/ic-search.svg';
import icSearchYellow from '../../image/icon/icSearchYellow.svg';
import icPlus from '../../image/icon/ic-plus-whitle.svg';
import icClose from '../../image/icon/ic-close-region.svg';
import icLoadingBlack from '../../image/icon/loading-black.svg';
import icLoading from '../../image/icon/loading-whilte.svg';
import {
  getAltImage, getAltImageV2, getNameFromCommon, scrollTop,
} from '../../Redux/Helpers';
import { GET_YOUR_PERFUME_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';

class ItemPerfumeSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSearch: [],
      isForcus: false,
      isLoading: false,
    };
    if (props.isSearchFunc) {
      this.dataA = props.dataA;
      this.searchFnc('a');
    }
    this.refInput = React.createRef();
  }

  onChange = (e) => {
    const { value } = e.target;
    this.search = value;
    if (_.isEmpty(value)) {
      this.setState({ dataSearch: [] });
      this.searchFnc('a');
    } else {
      this.searchFnc(value);
    }
  }

  searchFnc = (value) => {
    if (this.timeOutSearching) {
      clearTimeout(this.timeOutSearching);
      this.timeOutSearching = undefined;
    }
    this.timeOutSearching = setTimeout(() => { this.fetchData(value); }, 500);
    this.setState({ isLoading: true });
  }

  fetchData = (value, offset = 1) => {
    if (value === 'a' && this.dataA) {
      this.setState({ dataSearch: this.dataA, isLoading: false });
      return;
    }
    const option = {
      url: GET_YOUR_PERFUME_URL.replace('{search}', value).replace('{offset}', offset),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        // const dataSearch = _.orderBy(result, 'name', 'asc');
        if (result.length > 0) {
          this.setState({ dataSearch: result, isLoading: false });
        } else {
          const noBt = getNameFromCommon(this.props.cmsCommon, 'No perfume found');
          this.setState({ dataSearch: [{ name: noBt, isNoData: true }], isLoading: false });
        }
        // this.offset = this.offset + result.length;
        if (value === 'a') {
          this.dataA = result;
          this.props.updateDataA(this.dataA);
        }
      }
    });
  }

  onFocus = () => {
    if (_.isEmpty(this.search)) {
      this.searchFnc('a');
    }
    this.setState({ isForcus: true });
    this.props.updateCenterScreen();
  }

  onBlur = () => {
    setTimeout(() => {
      if (!this.props.isNotScrollTop) {
        scrollTop();
      }
      this.setState({ isForcus: false });
    }, 300);
  }

  render() {
    const { data } = this.props;
    const {
      onClickPerfume, clearPerfume, isAddMore, isNotEdit,
      placeholder1, placeholder2, title, dataQuizSelect,
    } = this.props;
    const { dataSearch, isForcus, isLoading } = this.state;
    const isSearch = this.search && this.search.length > 2 || isForcus;
    return (
      <div className="div-item-perfume-selection">
        {
          data
            ? (
              <div className="div-result">
                <div className="div-perfume">
                  <img loading="lazy" src={data ? data.image : ''} alt={getAltImageV2(data)} />
                  <span>
                    {data ? data.name : ''}
                  </span>
                  <button type="button" onClick={() => clearPerfume(data)} className={isNotEdit ? 'hidden' : ''}>
                    <img src={icClose} alt="icClose" />
                  </button>
                </div>
                <div className="div-characteristics">
                  <span>
                    {title}
                  </span>
                  <div className="div-list-item">
                    {
                      _.map(data.accords, x => (
                        <div className="div-item-character">
                          <span>
                            {x.name}
                          </span>
                          <div className="div-process">
                            <div className="process-bg" />
                            <div className="process-percent" style={{ width: `${x.weight * 100}%`, backgroundColor: `${x.color}` }} />
                          </div>
                        </div>
                      ))
                    }
                  </div>
                </div>
              </div>
            )
            : dataQuizSelect ? (
              <div className="div-search result-quiz">
                <img
                  className="bottle-scent"
                  src={dataQuizSelect?.image}
                  alt="bottle"
                />
                <span>
                  {dataQuizSelect?.name}
                </span>
                <button type="button" className="button-bg__none bt-close" onClick={this.props.clearDataQuiz}>
                  <img src={icClose} alt="icClose" />
                </button>
              </div>
            ) : (
              <div className={`div-search ${isSearch ? 'result' : ''} ${isAddMore ? 'div-small' : ''}`}>
                <div
                  onClick={() => !(isLoading || isSearch) && this.setState({ isForcus: true }, () => this.refInput && this.refInput.current && this.refInput.current.focus())}
                  className="div-input"
                >
                  <img
                    className={isLoading || isSearch ? 'hidden' : 'icon-search'}
                    src={icSearchYellow}
                    alt={icSearch}
                  />
                  {
                    isLoading || isSearch ? (
                      <input ref={this.refInput} type="text" placeholder={isAddMore ? placeholder2 : placeholder1} onChange={this.onChange} onFocus={this.onFocus} onBlur={this.onBlur} />
                    ) : (
                      <span>
                        {placeholder1}
                      </span>
                    )
                  }
                  <div className={`${isLoading ? '' : 'hidden'}`}>
                    <img className={isLoading ? 'animate-loading' : 'hidden'} src={icLoadingBlack} alt={icSearch} />
                  </div>
                </div>
                <div className={isSearch ? 'div-list-search' : 'hidden'}>
                  {
                  _.map(dataSearch, x => (
                    <div
                      data-gtmtracking={this.props.dataGtmtracking}
                      className={classnames('item-perfume', x.isNoData ? 'text-center' : '')}
                      onClick={() => { onClickPerfume(x); this.setState({ dataSearch: [] }); }}
                    >
                      <img loading="lazy" src={x.image} alt={getAltImage(x.image)} data-gtmtracking={this.props.dataGtmtracking} />
                      <span data-gtmtracking={this.props.dataGtmtracking}>
                        {x.name}
                      </span>
                    </div>
                  ))
                }
                </div>
              </div>
            )
        }
      </div>
    );
  }
}

ItemPerfumeSelection.defaultProps = {
  onClickPerfume: () => {},
};
ItemPerfumeSelection.propTypes = {
  data: PropTypes.shape().isRequired,
  onClickPerfume: PropTypes.func,
  clearPerfume: PropTypes.func.isRequired,
  isAddMore: PropTypes.bool.isRequired,
  isNotEdit: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  placeholder1: PropTypes.string.isRequired,
  placeholder2: PropTypes.string.isRequired,
  isSearchFunc: PropTypes.func.isRequired,
  updateDataA: PropTypes.func.isRequired,
  dataA: PropTypes.arrayOf().isRequired,
  updateCenterScreen: PropTypes.func.isRequired,
};

export default ItemPerfumeSelection;
