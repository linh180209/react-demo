import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  CardGroup,
  Card,
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
} from 'reactstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { toastrError, toastrSuccess } from '../Redux/Helpers/notification';
import { LOGIN_URL } from '../config';
import fetchClient from '../Redux/Helpers/fetch-client';
import loadingPage from '../Redux/Actions/loading';
import getAllRequest from '../Redux/Actions/getall';
import { loginCompleted } from '../Redux/Actions/login';
import googleAnalitycs, { generateUrlWeb, gotoShopHome } from '../Redux/Helpers';

class Login extends Component {
  constructor(props) {
    super(props);
    this.dataLogin = {
      userName: '',
      passWord: '',
    };
  }

  componentDidMount = () => {
    googleAnalitycs('/login');
  };

  onSubmit = (e) => {
    console.log('onSubmit');
    e.preventDefault();
    const { userName, passWord } = this.dataLogin;
    this.loginApi(userName, passWord);
  }

  onChange = (e) => {
    const { value, name } = e.target;
    this.dataLogin[name] = value;
  }

  loginApi = (email, passWord) => {
    this.props.loadingPage(true);
    const body = {
      email,
      password: passWord,
    };
    const options = {
      url: LOGIN_URL,
      method: 'POST',
      body,
    };
    fetchClient(options).then((result) => {
      this.props.loadingPage(false);
      if (!result.isError && !result.message) {
        toastrSuccess('Login is successful');
        this.props.loginCompleted(result);
        if (result.user.is_b2b) {
          this.props.history.push(generateUrlWeb('/voucher'));
        } else {
          this.props.getAllRequest();
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        }
        return;
      }
      throw new Error(result.message);
    }).catch((e) => {
      toastrError(e.message);
      this.props.loadingPage(false);
    });
  }

  registerClick = () => {
    this.props.history.push(generateUrlWeb('/register'));
  }

  forgotPassword = () => {
    this.props.history.push(generateUrlWeb('/forgotPassword'));
  }

  render() {
    return (
      <div className="login app app-edit flex-row align-items-center">
        <Container>
          <Row className="justify-content-center" style={{ width: '100%' }}>
            <Col md="6" xs="12">
              <CardGroup className="login__card-group" style={{ width: '100%' }}>
                <Card className="p-4">
                  <h1 className="mt-4">
                    Login
                  </h1>
                  <h5 className="text-muted mb-4">
                    Sign In to your account
                  </h5>
                  <form onSubmit={this.onSubmit}>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <i className="icon-user" />
                      </InputGroupAddon>
                      <Input
                        type="text"
                        placeholder="Email"
                        name="userName"
                        onChange={this.onChange}
                      />
                    </InputGroup>

                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <i className="icon-lock" />
                      </InputGroupAddon>
                      <Input
                        type="password"
                        placeholder="Password"
                        name="passWord"
                        onChange={this.onChange}
                      />
                    </InputGroup>
                    <Row>
                      <Col xs="12">
                        <Button type="submit" className="px-4 btn--login">
                          Login
                        </Button>
                      </Col>
                    </Row>
                    <Row style={{ justifyContent: 'space-between', marginLeft: '0px', marginRight: '0px' }}>
                      <button
                        type="button"
                        onClick={this.forgotPassword}
                        style={{
                          border: 'none',
                          background: 'transparent',
                          color: '#20a8d8',
                          marginTop: '20px',
                        }}
                      >
                        Forgot Password?
                      </button>
                      <button
                        type="button"
                        onClick={this.registerClick}
                        style={{
                          border: 'none',
                          background: 'transparent',
                          color: '#20a8d8',
                          marginTop: '20px',
                        }}
                      >
                        Don't have an account?
                      </button>
                    </Row>
                  </form>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

Login.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  loadingPage: PropTypes.func.isRequired,
  loginCompleted: PropTypes.func.isRequired,
  getAllRequest: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

const mapDispatchToProps = {
  loadingPage,
  loginCompleted,
  getAllRequest,
};

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login));
