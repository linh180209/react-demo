import React, { Component } from 'react';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import {
  getSEOFromCms, fetchCMSHomepage, scrollTop, googleAnalitycs, getCmsCommon, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers';
import Header from '../../components/HomePage/header';
import addCmsRedux from '../../Redux/Actions/cms';
import Banner from './components/Banner';
import Description from './components/Description';
import Discovery from './components/Discovery';
import Timeline from './components/Timeline';
import Influencer from './components/Influencer';
import Feature from './components/Feature';
import BlockVideo from './components/BlockVideo';
import ImageTextBlock from './components/ImageTextBlock';
import LoginRegister from '../Register/loginRegister';
import './styles.scss';
import { GET_SUBSCRIPTION_ITEMS, GET_PRICES_PRODUCTS } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrInfo } from '../../Redux/Helpers/notification';
import DiscoverBlock from './components/DiscoverBlock';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import BlockIconHeaderV2 from '../../components/HomeAlt/BlockIconHeaderV2';
import BlockTextButton from './components/BlockTextButton';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';
import auth from '../../Redux/Helpers/auth';

const getCms = (data) => {
  const objectReturn = {
    bannerBlock: [],
    feedbacksBlock: [],
    iconsBlock: {},
    seo: undefined,
    buttonBlocks: [],
    ctaBlock: undefined,
    subscriptionBlock: {},
    allBlock: [],
    benefitsBlock: {},
  };

  if (!_.isEmpty(data)) {
    const seo = getSEOFromCms(data);
    const { body } = data;
    if (body) {
      const bannerBlock = _.filter(body, x => x.type === 'banner_block').map(e => e.value);
      const feedbacksBlock = _.filter(body, x => x.type === 'feedbacks_block').map(e => e.value);
      const iconsBlock = _.find(body, x => x.type === 'icons_block').value;
      const buttonBlocks = _.filter(body, x => x.type === 'button_block');
      const ctaBlock = _.find(body, x => x.type === 'cta_block').value;
      const subscriptionBlock = _.find(body, x => x.type === 'subscription_block').value;
      const allBlock = _.filter(body, x => x.type !== 'button_block');
      const benefitsBlock = _.find(body, x => x.type === 'benefits_block');
      _.assign(objectReturn, {
        bannerBlock,
        seo,
        feedbacksBlock,
        iconsBlock,
        buttonBlocks,
        ctaBlock,
        subscriptionBlock,
        allBlock,
        benefitsBlock,
      });
    }
  }
  return objectReturn;
};
class AboutUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowLogin: false,
      bannerBlock: [],
      feedbacksBlock: [],
      iconsBlock: {},
      seo: undefined,
      dataSubscription: [],
      buttonBlocks: [],
      subscriptionBlock: {},
      ingredientDetail: {},
      allBlock: [],
      benefitsBlock: {},
    };
  }

  componentDidMount() {
    setPrerenderReady();
    if (!this.props.location.hash) {
      scrollTop();
    }
    googleAnalitycs('/club21g');
    this.fetchDataInit();
    this.fetchPriceSubscription();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchPriceSubscription = () => {
    const option = {
      url: GET_PRICES_PRODUCTS.replace('{type}', 'subscription'),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && result.length > 0) {
        this.setState({ priceSubscription: result[0].price });
      }
    });
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const aboutUs = _.find(cms, x => x.title === 'club21g');
    if (!aboutUs) {
      const cmsData = await fetchCMSHomepage('club21g');
      this.props.addCmsRedux(cmsData);
      const dataFromStore = getCms(cmsData);
      this.setState(dataFromStore);
    } else {
      const dataFromStore = getCms(aboutUs);
      this.setState(dataFromStore);
    }
    const option = {
      url: GET_SUBSCRIPTION_ITEMS,
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      // const data = result.sort((a, b) => parseInt(a.month, 10) - parseInt(b.month, 10));
      // const data = _.reverse(result);
      this.setState({ dataSubscription: result.slice(result.length - 4 > -1 ? result.length - 4 : 0, result.length) });
    }).catch((error) => {
      toastrError(error.message);
    });
  }

  onCloseLoginPage = () => {
    this.setState({ isShowLogin: false });
  }

  onClickCheckout = () => {
    if (this.props.login && !_.isEmpty(this.props.login.user)) {
      if (this.props.login.user.is_club_member) {
        toastrInfo('You have already subscribed to our club21G');
        return;
      }
      this.props.history.push(generateUrlWeb('/checkout-club'));
    } else {
      // this.setState({
      //   isShowLogin: true,
      // });
      this.props.history.push(generateUrlWeb('/checkout-club'));
    }
  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  render() {
    const {
      isShowLogin, bannerBlock, feedbacksBlock, iconsBlock, seo, dataSubscription, buttonBlocks, ctaBlock, subscriptionBlock,
      ingredientDetail, isShowIngredient, allBlock, benefitsBlock, priceSubscription,
    } = this.state;
    const { history, cms } = this.props;
    const cmsCommon = getCmsCommon(cms);
    const price = !_.isEmpty(subscriptionBlock) ? subscriptionBlock.item.price : '';
    const countryName = auth.getCountryName();

    return (
      <div>
        <MetaTags>
          <title> {seo ? replaceCountryInMetaTags(countryName, seo?.seoTitle) : ''}</title>
          <meta
            name="description"
            content={seo ? replaceCountryInMetaTags(countryName, seo?.seoDescription) : ''}
          />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/club21g')}
        </MetaTags>
        {/* <Header />
        { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <HeaderHomePageV3 />
        <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
        />
        <div className="club21g">
          <LoginRegister onClose={this.onCloseLoginPage} showRegister isOpen={isShowLogin} />
          {
            _.map(allBlock, (d) => {
              switch (d.type) {
                case 'banner_block':
                  if (d.value.image.caption === 'BannerBlock') {
                    return <Banner data={d.value} price={priceSubscription} />;
                  } if (d.value.image.caption === 'DiscoveryBlock') {
                    return <Discovery data={d.value} onClickCheckout={this.onClickCheckout} price={priceSubscription} />;
                  } if (d.value.image.caption === 'ImageTextBlock') {
                    return <ImageTextBlock banner={d.value} onClickCheckout={this.onClickCheckout} />;
                  }
                case 'feedbacks_block':
                  if (d.value.header.header_size === 'h2') {
                    return <Description data={d.value} />;
                  } if (d.value.header.header_size === 'h3') {
                    return <Influencer data={d.value} />;
                  } if (d.value.header.header_size === 'h4') {
                    return <Description className="free-trial-works" data={d.value} history={history} />;
                  }
                case 'timestamp_block':
                  return <Timeline onClickCheckout={this.onClickCheckout} dataSubscription={dataSubscription} buttonBlocks={buttonBlocks} />;
                case 'subscription_block':
                  return <DiscoverBlock data={subscriptionBlock} history={history} cmsCommon={cmsCommon} onClickIngredient={this.onClickIngredient} />;
                case 'cta_block':
                  return <BlockVideo ctaBlock={ctaBlock} />;
                // case 'icons_block':
                //   return <Feature features={iconsBlock} onClickCheckout={this.onClickCheckout} />;
                case 'benefits_block':
                  return <BlockIconHeaderV2 heroCms={benefitsBlock} />;
                case 'columns_block':
                  return <BlockTextButton data={d.value} history={history} />;
                default:
                  return null;
              }
            })
          }

          {/* <Banner data={bannerBlock.length ? bannerBlock[0] : {}} />
          <Description data={feedbacksBlock.length ? feedbacksBlock[0] : {}} />
          <Discovery data={bannerBlock.length ? bannerBlock[1] : {}} onClickCheckout={this.onClickCheckout} />
          <Timeline onClickCheckout={this.onClickCheckout} dataSubscription={dataSubscription} buttonBlocks={buttonBlocks} />
          <DiscoverBlock data={subscriptionBlock} history={history} cmsCommon={cmsCommon} onClickIngredient={this.onClickIngredient} />
          <Influencer data={feedbacksBlock.length ? feedbacksBlock[1] : {}} />
          <ImageTextBlock banner={bannerBlock.length ? bannerBlock[2] : {}} onClickCheckout={this.onClickCheckout} />
          <BlockVideo ctaBlock={ctaBlock} />
          <Feature features={iconsBlock} onClickCheckout={this.onClickCheckout} ctaBlock={ctaBlock} /> */}
        </div>
        <FooterV2 />
      </div>
    );
  }
}

AboutUs.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  login: PropTypes.shape().isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    login: state.login,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutUs));
