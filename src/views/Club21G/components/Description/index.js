import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import { generateUrlWeb, getAltImageV2 } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';

const RenderItem = (props) => {
  const { item, index, isDisableNumber } = props;
  return (
    <div className="item">
      <div className="__image position-relative">
        <img loading="lazy" src={item.avatar.image} alt={getAltImageV2(item.avatar)} />
        {
          !isDisableNumber && (
            <span className="position-absolute">{index + 1}</span>
          )
        }
      </div>
      <div className="__text">
        {item.text}
      </div>
    </div>
  );
};

class Description extends Component {
  render() {
    const { data, className } = this.props;
    const listImage = _.filter(data.feedbacks || [], x => x.value.avatar.image);
    const buttonData = _.find(data.feedbacks || [], x => !x.value.avatar.image);
    return data && (
      <div className={`club-description ${className}`}>
        <div className="__header d-flex flex-column align-items-center">
          <h3 className="tc">{data.header ? data.header.header_text : ''}</h3>
          <p>{data.text ? data.text : ''}</p>
        </div>

        <div className={`__content d-flex justify-content-between ${isMobile ? 'flex-column align-items-center' : ''}`}>
          {listImage.map(e => e.value).map((item, index) => <RenderItem item={item} index={index} isDisableNumber={!!buttonData} />)}
        </div>

        {
          buttonData && (
            <div className="bt-link d-flex flex-column align-items-center justify-content-center">
              <button
                type="button"
                onClick={() => {
                  this.props.history.push(generateUrlWeb(buttonData.value.link));
                }}
              >
                {buttonData.value.name}
              </button>
            </div>

          )
        }
      </div>
    );
  }
}

export default Description;
