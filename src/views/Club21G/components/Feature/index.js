import React, { Component } from 'react';
import _ from 'lodash';
import slugify from 'react-slugify';
import { Fade } from 'react-reveal';
import { ParallaxBanner, withController } from 'react-scroll-parallax';
import convinceImage from '../../assets/convince.png';
import featureImage from '../../assets/feature.png';
import BlockVideo from '../BlockVideo';
import './styles.scss';
import { isMobileOnly } from '../../../../DetectScreen';

const RenderItem = (props) => {
  const { feature, index } = props;
  return (
    <Fade top delay={(index + 1) * 300}>
      <div className="__item">
        <div className="__item-icon d-flex justify-content-center">
          <img loading="lazy" src={feature.image.image} alt="" className="img-fluid" />
        </div>
        <div className="__item-title text-center">
          {feature.text}
        </div>
      </div>
    </Fade>
  );
};

class Event extends Component {
  componentDidUpdate(prevProps, prevState, snapshot) {
    this.props.parallaxController.update();
  }

  render() {
    this.props.parallaxController.update();
    const {
      features,
    } = this.props;
    return (
      <React.Fragment>
        {features && features.icons && (
          <div
            style={{
              backgroundImage: `url(${features.image_background ? features.image_background.image : ''})`,
              backgroundPosition: 'center',
              backgroundSize: 'cover',
            }}
          >
            <div
              className={`feature d-flex ${isMobileOnly ? 'flex-column' : ''} justify-content-between`}
            >
              {features.icons.map(e => e.value).map((feature, index) => (
                // <RenderItem feature={feature} index={index} />
                <RenderItem feature={feature} index={index} />
              ))}
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}
export default withController(Event);
