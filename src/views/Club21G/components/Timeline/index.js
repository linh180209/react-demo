import React, { Component } from 'react';
import TimelineMobile from './mobile';
import TimelineWeb from './web';
import './styles.scss';
import { isMobile } from '../../../../DetectScreen';

class Timeline extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeClass: [true, true, true, true, true],
    };
  }

  toggleActive = (toggleIndex) => {
    const { activeClass } = this.state;
    const newActiveClass = activeClass.map((ele, index) => {
      if (index === toggleIndex) {
        return !activeClass[index];
      }
      return ele;
    });
    this.setState({
      activeClass: newActiveClass,
    });
  }

  render() {
    const { activeClass } = this.state;
    const { onClickCheckout, dataSubscription, buttonBlocks } = this.props;
    if (isMobile) {
      return <TimelineMobile activeClass={activeClass} toggleActive={this.toggleActive} onClickCheckout={onClickCheckout} dataSubscription={dataSubscription} buttonBlocks={buttonBlocks} />;
    }
    return <TimelineWeb activeClass={activeClass} toggleActive={this.toggleActive} onClickCheckout={onClickCheckout} dataSubscription={dataSubscription} buttonBlocks={buttonBlocks} />;
  }
}

export default Timeline;
