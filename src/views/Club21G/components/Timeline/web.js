import React, { Component } from 'react';
import Slider from 'react-slick';
import _ from 'lodash';
import moment from 'moment';
import timelineComingSoonImg from '../../assets/timeline-coming-soon.png';
import './styles.scss';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import { IS_HIDDEN_CLUB21G } from '../../../../config';

class Timeline extends Component {
  render() {
    const sliderSettings = {
      dots: true,
      fade: true,
      arrows: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
    };

    const {
      activeClass, toggleActive, onClickCheckout, dataSubscription, buttonBlocks,
    } = this.props;
    const scentTimeline = getNameFromButtonBlock(buttonBlocks, 'scent_timeline');
    const discoverOurCurrent = getNameFromButtonBlock(buttonBlocks, 'Discover_our_current');
    const comingSoon = getNameFromButtonBlock(buttonBlocks, 'Coming_soon');
    const nextMonth = getNameFromButtonBlock(buttonBlocks, 'next_month');
    const joinTheClub = getNameFromButtonBlock(buttonBlocks, 'join_the_club');
    return (
      <div className="timeline">
        <div className="__header d-flex flex-column align-items-center">
          <h3>{scentTimeline}</h3>
          <p>{discoverOurCurrent}</p>
        </div>
        <div className="__content position-relative">
          <div className="__content-bar position-absolute" />
          {/* <Slider {...sliderSettings}> */}
          <div>
            <div className="__content-list d-flex justify-content-between">
              {
                _.map(dataSubscription, (x, index) => (
                  <div className="item">
                    <div className="__image position-relative" onMouseLeave={() => { toggleActive(index); }} onMouseEnter={() => { toggleActive(index); }}>
                      <img loading="lazy" src={_.find(x.images, d => d.type === 'subscription') ? _.find(x.images, d => d.type === 'subscription').image : ''} className={`front img-fluid position-absolute ${activeClass[index] === true ? 'fadeIn' : 'fadeOut'}`} alt="" />
                      <img loading="lazy" src={_.find(x.images, d => d.type === 'subscription') ? _.find(x.images, d => d.type === 'hover').image : ''} className="back img-fluid" alt="" />
                    </div>
                    <div className="__milestone d-flex flex-column align-items-center">
                      <span className="__milestone-checkmark d-flex justify-content-center align-items-center active">
                        <i className="fa fa-check" />
                      </span>
                      <p className="__milestone-title">
                        {x.name}
                      </p>
                      <p className="__milestone-month">
                        {getNameFromButtonBlock(buttonBlocks, moment(x.month, 'MM').format('MMM')) }
                      </p>
                    </div>
                  </div>
                ))
              }
              <div className="item--coming">
                <div className="__image  d-flex justify-content-center align-items-center position-relative">
                  <img src={timelineComingSoonImg} className="img-fluid" alt="" />
                </div>
                <div className="__milestone d-flex flex-column align-items-center">
                  <span className="__milestone-checkmark d-flex justify-content-center align-items-center">
                    <i className="fa fa-check" />
                  </span>
                  <p className="__milestone-title">
                    {comingSoon}
                  </p>
                  <p className="__milestone-month">
                    {getNameFromButtonBlock(buttonBlocks, nextMonth)}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        {!IS_HIDDEN_CLUB21G && (
        <div className="d-flex justify-content-center">
          <div className="join" onClick={onClickCheckout}>
            {joinTheClub}
          </div>
        </div>
        )}
      </div>
    );
  }
}

export default Timeline;
