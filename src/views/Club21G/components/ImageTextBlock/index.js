import React, { Component } from 'react';
import { IS_HIDDEN_CLUB21G } from '../../../../config';
import { isMobile } from '../../../../DetectScreen';

export default class ImageTextBlock extends Component {
  render() {
    const { banner, onClickCheckout } = this.props;
    return (
      <React.Fragment>
        {banner && (
          <div className={`convince d-flex ${isMobile ? 'flex-column' : ''}`}>
            <div className="__left" style={{ backgroundImage: `url(${banner.image ? banner.image.image : ''})` }} />
            <div className={`__right d-flex flex-column ${isMobile ? 'align-items-center' : 'align-items-end'}`}>
              <h3 className={`${!isMobile ? 'text-right' : 'text-center'}`}>
                {banner.header ? banner.header.header_text : ''}
              </h3>
              <p className={`${!isMobile ? 'text-right' : 'text-center'}`}>
                {banner.header2 ? banner.header2.header_text : ''}
              </p>
              {
                !IS_HIDDEN_CLUB21G && (
                <div className="__start" onClick={onClickCheckout}>
                  {banner.button ? banner.button.name : ''}
                </div>
                )
              }
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}
