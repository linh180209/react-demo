import React, { Component } from 'react';
import _ from 'lodash';
import { isEdge } from 'react-device-detect';
import ScrollDetect from '../../../../components/HomeAlt/scrollDetect';
import { isMobile, isTablet } from '../../../../DetectScreen';

class BlockVideo extends Component {
  detectScrollTo = () => {
    const ele = document.getElementById('myVideo');
    if (ele && ele.paused) {
      ele.play();
    }
  };

  render() {
    const { ctaBlock } = this.props;
    const videos = ctaBlock ? ctaBlock.videos : [];
    const valueFindVideo = isMobile ? 'phone' : (isTablet ? 'tablet' : 'web');
    const dataVideo = _.find(videos, x => x.value.type === valueFindVideo);
    const videoBlock = dataVideo ? dataVideo.value : undefined;
    return (
      <div className="div-block-video-club">
        <ScrollDetect id="id-video-club" detectScrollTo={this.detectScrollTo} />
        <video
          id="myVideo"
          style={{
            objectFit: 'cover',
            width: isEdge ? 'auto' : '100%',
            height: '100%',
            marginBottom: '-10px',
          }}
          muted="true"
          playsinline="true"
          // autoPlay="true"
          controls
          loop="true"
          preload="auto"
          src={videoBlock ? videoBlock.video : ''}
          poster={videoBlock ? videoBlock.placeholder : ''}
        />
      </div>
    );
  }
}

export default BlockVideo;
