import React, { Component } from 'react';
import '../../../../styles/block-text-button.scss';
import { generateUrlWeb } from '../../../../Redux/Helpers';

class BlockTextButton extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className="div-block-text-button">
        <h1>
          {data.header.header_text}
        </h1>
        <span>
          {data.text}
        </span>
        <button
          type="button"
          onClick={() => {
            this.props.history.push(generateUrlWeb(data.button.link));
          }}
        >
          {data.button.name}
        </button>
      </div>
    );
  }
}

export default BlockTextButton;
