import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import clubBanner from '../../assets/club-banner.png';
import './styles.scss';
import { generaCurrency } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';

class Banner extends Component {
  render() {
    const { data, price } = this.props;
    return data && (
      <div className="club-banner d-flex justify-content-center align-items-center" style={{ backgroundImage: `url(${data.image ? data.image.image : ''})` }}>
        <div>
          <div className="__header">
            <h1 className="title">{data.header ? data.header.header_text : ''}</h1>
            <p className="description">{data.header2 ? data.header2.header_text : ''}</p>
          </div>
          <div className="__content d-flex flex-column justify-content-center align-items-center">
            <div className="price">
              {htmlPare(data.description ? data.description.replace('{price}', generaCurrency(price)) : '')}
            </div>
            <div className="join">
              <a onClick={() => { window.scrollTo({ left: 0, top: !isMobile ? 744 : 520, behavior: 'smooth' }); }}>{data.button ? data.button.text : ''}</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Banner;
