import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import reactHtmlParser from 'react-html-parser';
import ProgressLine from '../../../ResultScentV2/progressLine';
import ProgressCircle from '../../../ResultScentV2/progressCircle';
import icSearch from '../../../../image/icon/search-w.svg';
import icNote from '../../../../image/icon/note-w.svg';
import { generateUrlWeb, getNameFromCommon } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';

class DiscoverBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      indexActive: 0,
    };
  }

  gotoLink = (link) => {
    const { history } = this.props;
    if (link.includes('https:') || link.includes('http:')) {
      window.open(link);
      return;
    }
    history.push(generateUrlWeb(link));
  }

  render() {
    const { indexActive } = this.state;
    const { data, cmsCommon, onClickIngredient } = this.props;
    const { header, item, button } = data;
    const { products } = item || {};
    const dataSelect = products && products.length > indexActive ? products[indexActive] : undefined;
    const imgBg = _.find(dataSelect ? dataSelect.images : [], x => x.type === 'subscription');
    const imgScent = _.find(dataSelect ? dataSelect.images : [], x => x.type === 'unisex');
    const scentbt = getNameFromCommon(cmsCommon, 'Scent_characteristics');
    const strengthBt = getNameFromCommon(cmsCommon, 'STRENGTH');
    const durationBt = getNameFromCommon(cmsCommon, 'DURATION');
    const viewMoreBt = getNameFromCommon(cmsCommon, 'view_more_info');
    return (
      <div className="div-discover-block">
        <div className="div-info">
          <div className="div-image">
            <img loading="lazy" src={imgBg ? imgBg.image : ''} alt="bg" />
          </div>
          <div className="div-wrap-text">
            <h3>
              {header ? header.header_text : ''}
            </h3>
            <div className="div-button-tab">
              <button
                type="button"
                className={indexActive === 0 ? 'active' : ''}
                onClick={() => this.setState({ indexActive: 0 })
              }
              >
                {products && products.length > 0 ? products[0].name : ''}
              </button>
              <button
                type="button"
                className={indexActive === 1 ? 'active' : ''}
                onClick={() => this.setState({ indexActive: 1 })
              }
              >
                {products && products.length > 1 ? products[1].name : ''}
              </button>
            </div>
            <span>
              {dataSelect ? reactHtmlParser(dataSelect.description) : ''}
            </span>
            <div className="div-scent">
              <h3>
                {scentbt}
              </h3>
              <div className="div-scent-info">
                <img loading="lazy" src={imgScent ? imgScent.image : ''} alt="scent" className={isMobile ? 'hidden' : ''} />
                <div className="div-process-line">
                  {
                _.map(dataSelect ? dataSelect.profile.accords : [], x => (
                  <ProgressLine
                    data={x}
                    isShowPercent
                  />
                ))
              }
                </div>
                <div className="div-process-circle">
                  <ProgressCircle title={strengthBt} percent={dataSelect ? parseInt(parseFloat(dataSelect.profile.strength) * 100, 10) : 0} />
                  <ProgressCircle title={durationBt} percent={dataSelect ? parseInt(parseFloat(dataSelect.profile.duration) * 100, 10) : 0} />
                </div>
              </div>
              <div className="div-bt-more">
                <button
                  type="button"
                  onClick={() => this.props.onClickIngredient(dataSelect.ingredient)}
                >
                  <img src={icSearch} alt="search" />
                  {viewMoreBt}
                </button>
                <button
                  type="button"
                  className={isMobile ? 'hidden' : ''}
                  onClick={() => this.gotoLink(button && button.length > 0 ? button[0].value.link : '')}
                >
                  <img src={icNote} alt="note" />
                  {button && button.length > 0 ? button[0].value.text : ''}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DiscoverBlock;

DiscoverBlock.propTypes = {
  data: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
  onClickIngredient: PropTypes.func.isRequired,
};
