import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import discoveryBanner from '../../assets/discovery-banner.png';
import './styles.scss';
import { generaCurrency } from '../../../../Redux/Helpers';
import { IS_HIDDEN_CLUB21G } from '../../../../config';

class Discovery extends Component {
  render() {
    const { data, price } = this.props;
    const { onClickCheckout } = this.props;
    return data && (
      <div className="discovery">
        <div className="background" style={{ backgroundImage: `url(${data.image ? data.image.image : ''})` }}>
          <div className="__content" style={{ width: 'fit-content' }}>
            <div className="__content__title">
              <h2>
                {data.header ? data.header.header_text : ''}
              </h2>
              <h3>
                {data.header2 ? data.header2.header_text.replace('{price}', generaCurrency(price)) : ''}
              </h3>
            </div>
            {!IS_HIDDEN_CLUB21G && (
            <div className="__content__description">
              <div dangerouslySetInnerHTML={{ __html: data.description ? data.description : '' }} />
              <div className="price" onClick={onClickCheckout}>
                {data.button ? data.button.name : ''}
              </div>
              <p className="extra text-left">
                {data.button ? data.button.text.replace('{price}', generaCurrency(price)) : ''}
              </p>
            </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Discovery;
