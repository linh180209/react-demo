import React, { Component } from 'react';
import discoveryBanner from '../../assets/discovery-banner.png';
import './styles.scss';
import { generaCurrency } from '../../../../Redux/Helpers';
import { IS_HIDDEN_CLUB21G } from '../../../../config';

class Discovery extends Component {
  render() {
    const { data, onClickCheckout, price } = this.props;

    return (
      <div className="discovery d-flex flex-column align-items-center">
        <div className="background" style={{ backgroundImage: `url(${data.image ? data.image.image : ''})` }} />
        <div className="__content d-flex flex-column justify-content-center align-items-center" style={{ width: 'fit-content' }}>
          <div className="__content__title">
            <h2 className="text-center">
              {data.header ? data.header.header_text : ''}
            </h2>
            <h3 className="text-center m-0">
              {data.header2 ? data.header2.header_text.replace('{price}', generaCurrency(price)) : ''}
            </h3>
          </div>
          {!IS_HIDDEN_CLUB21G && (
          <div className="__content__description flex-column d-flex align-items-center">
            <div dangerouslySetInnerHTML={{ __html: data.description ? data.description : '' }} />
            <div className="price text-center" onClick={onClickCheckout}>
              {data.button ? data.button.name : ''}
            </div>
            <p className="extra text-center">
              {data.button ? data.button.text : ''}
            </p>
          </div>
          )}
        </div>
      </div>
    );
  }
}

export default Discovery;
