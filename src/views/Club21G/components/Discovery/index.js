import React, { Component } from 'react';
import DiscoveryWeb from './web';
import DiscoveryMobile from './mobile';
import { isMobile } from '../../../../DetectScreen';

class Discovery extends Component {
  render() {
    const { onClickCheckout, data, price } = this.props;
    if (!isMobile) {
      return <DiscoveryWeb data={data} onClickCheckout={onClickCheckout} price={price} />;
    }
    return <DiscoveryMobile data={data} onClickCheckout={onClickCheckout} price={price} />;
  }
}

export default Discovery;
