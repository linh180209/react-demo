import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';
import influencerImage from '../../assets/influencer.png';
import prevArrow from '../../../../image/icon/slider-white-left.svg';
import nextArrow from '../../../../image/icon/slider-white-right.svg';
import './styles.scss';
import { getAltImageV2 } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';

const NavArrow = (props) => {
  const {
    className, style, onClick, imgSrc,
  } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: 'block' }}
      onClick={onClick}
    >
      <img src={imgSrc} alt="" />
    </div>
  );
};

const RenderItem = (props) => {
  const { item } = props;
  return (
    <div>
      <div className="item">
        <div className="__image position-relative">
          <img src={item.avatar.image} className="img-fluid" alt={getAltImageV2(item.avatar)} />
          <span className="position-absolute">{item.instagram}</span>
        </div>
        <div className="__text">
          <span>
            {item.text}
          </span>
        </div>
      </div>
    </div>
  );
};

const sliderSettings = {
  speed: 500,
  slidesToShow: !isMobile ? 5 : 1,
  slidesToScroll: 1,
  dots: isMobile,
  arrows: !isMobile,
  // prevArrow: <NavArrow imgSrc={prevArrow} />,
  // nextArrow: <NavArrow imgSrc={nextArrow} />,
};

class Influencer extends Component {
  render() {
    const { data } = this.props;

    return data && (
      <div className="influencer">
        <div className="__header d-flex flex-column align-items-center">
          <h3>{data.header ? data.header.header_text : ''}</h3>
        </div>
        <div className="__slider">
          {data.feedbacks && data.feedbacks.length && (
            <Slider {...sliderSettings}>
              {data.feedbacks.map(e => e.value).map(item => (
                <RenderItem item={item} />
              ))}
            </Slider>
          )}
        </div>
      </div>
    );
  }
}

export default Influencer;
