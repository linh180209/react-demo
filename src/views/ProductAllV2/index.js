import React, { Component } from 'react';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import '../../styles/product_all_v2.scss';

import HeaderHomePage from '../../components/HomePage/header';
import ProductBlock from '../../components/ProductAllV2/ProductBlock/productBlock';
import TextBlock from '../../components/ProductAllV2/textBlock';
import TextImageBlock from '../../components/ProductAllV2/textImageBlock';
import BecomeBlock from '../../components/ProductAllV2/becomeBlock';
import BlockIcon from '../../components/HomeAlt/BlockIcon';
import BlockStepItem from '../../components/HomeAlt/BlockStep';

import {
  getSEOFromCms, fetchCMSHomepage, scrollTop, googleAnalitycs, getNameFromButtonBlock, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers';
import addCmsRedux from '../../Redux/Actions/cms';
import addScentsToStore, { addScentsWaxToStore, addScentsCandleToStore, addScentsHomeToStore } from '../../Redux/Actions/scents';
import loadingPage from '../../Redux/Actions/loading';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import { createBasketGuest, addProductBasket } from '../../Redux/Actions/basket';
import BlockIconHeader from '../../components/HomeAlt/BlockIconHeader';
import QnABlock from '../../components/QnABlock';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';

const getCms = (cms) => {
  const objectReturn = {
    seo: undefined,
    iconsBlock: {},
    paragraphBlock: {},
    imageTxtBlock: [],
    lessonBlock: undefined,
    headerText: '',
    buttonBlocks: [],
    ctaBlock: [],
    benefitsBlock: {},
    imageBlocks: [],
    headerParagraphBlock: [],
    imageBackground: undefined,
  };
  if (!_.isEmpty(cms)) {
    const seo = getSEOFromCms(cms);
    const { body, header_text: headerText, image_background: imageBackground } = cms;
    if (body) {
      const iconsBlock = _.find(body, x => x.type === 'icons_block');
      const paragraphBlock = _.find(body, x => x.type === 'paragraph_block');
      const imageTxtBlock = _.filter(body, x => x.type === 'imagetxt_block').map(e => e.value);
      const ctaBlock = _.filter(body, x => x.type === 'cta_block').map(e => e.value);
      const imageBlocks = _.filter(body, x => x.type === 'image_block').map(e => e.value);
      const lessonBlock = _.find(body, x => x.type === 'lesson_block').value;
      const buttonBlocks = _.filter(body, x => x.type === 'button_block');
      const benefitsBlock = _.find(body, x => x.type === 'benefits_block');
      const faqBlocks = _.filter(body, x => x.type === 'faq_block');
      const headerParagraphBlock = _.filter(body, x => x.type === 'header_and_paragraph_block');
      // const featureBlock = _.find(body, x => x.type === 'icons_block').value;
      _.assign(objectReturn, {
        seo,
        iconsBlock,
        paragraphBlock,
        imageTxtBlock,
        lessonBlock,
        headerText,
        buttonBlocks,
        ctaBlock,
        benefitsBlock,
        imageBlocks,
        headerParagraphBlock,
        imageBackground,
        faqBlocks,
      });
    }
  }
  return objectReturn;
};
class ProductAllV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seo: undefined,
      iconsBlock: {},
      paragraphBlock: {},
      headerParagraphBlock: [],
      imageTxtBlock: [],
      lessonBlock: undefined,
      headerText: '',
      buttonBlocks: [],
      ctaBlock: [],
      imageBlocks: [],
      benefitsBlock: {},
      faqBlocks: {},
      showStep: 0,
      imageBackground: undefined,
    };
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/all-products');

    this.fetchCMS();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  onChangeShowStep = (step) => {
    this.setState({ showStep: step, typeSelection: step === 1 ? 'explore_perfume' : 'explore_home' });
  }

  changeDescription = (type) => {
    this.setState({ typeSelection: type });
  }

  fetchCMS = async () => {
    const { cms } = this.props;
    const productAll = _.find(cms, x => x.title === 'Product All V2');
    if (!productAll) {
      const cmsData = await fetchCMSHomepage('product-all-v2');
      this.props.addCmsRedux(cmsData);
      const dataFromStore = getCms(cmsData);
      this.setState(dataFromStore);
    } else {
      const dataFromStore = getCms(productAll);
      this.setState(dataFromStore);
    }
  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }


  onClickGotoProduct = (product) => {
    const { type } = product;
    let url;
    if (type.name === 'Wax_Perfume') {
      url = `/product/wax/${product.product}`;
    } else if (type.name === 'Elixir') {
      url = `/product/elixir/${product.product}`;
    } else if (type.name === 'Kit') {
      url = `/product/kit/${product.product}`;
    } else if (type.name === 'bundle') {
      url = `/product/bundle/${product.product}`;
    } else if (type.name === 'Scent') {
      url = `/product/Scent/${product.product}`;
    } else if (type.name === 'hand_sanitizer') {
      url = `/product/hand-sanitizer/${product.product}/${product.id}`;
    } else if (type.name === 'home_scents') {
      url = `/product/home-scents/${product.product}/${product.id}`;
    } else if (type.name === 'Perfume') {
      url = `/product/Perfume/${product.product}`;
    } else if (type.name === 'dual_candles') {
      url = `/product/dual-candles/${product.product}`;
    } else if (type.name === 'single_candle') {
      url = `/product/single-candle/${product.product}`;
    } else if (type.name === 'holder') {
      url = `/product/holder/${product.product}`;
    } else if (type.name === 'gift_bundle') {
      url = `/product/gift-bundle/${product.product}`;
    } else if (type.name === 'dual_crayons') {
      url = `/product/dual_crayons/${product.product}`;
    } else if (type.name === 'bundle_creation') {
      url = `/product/bundle_creation/${product.product}`;
    } else if (type.name === 'reed_diffuser') {
      url = `/product/reed_diffuser/${product.product}`;
    } else if (type.name === 'perfume_diy') {
      url = `/product/perfume_diy/${product.product}`;
    } else if (type.name === 'car_diffuser') {
      url = `/product/car_diffuser/${product.product}`;
    } else if (type.name === 'mini_candles') {
      url = `/product/mini_candles/${product.product}`;
    }
    this.props.history.push(generateUrlWeb(url));
  }

  render() {
    const {
      seo, iconsBlock, paragraphBlock, imageTxtBlock, lessonBlock, buttonBlocks, headerText,
      ctaBlock, isShowIngredient, ingredientDetail, typeSelection, benefitsBlock, imageBlocks,
      showStep, headerParagraphBlock, imageBackground, faqBlocks,
    } = this.state;
    let faqBlock;
    if (window.location.pathname.includes('/explore_perfume')) {
      faqBlock = _.find(faqBlocks || [], d => d.value?.header?.header_text === 'explore_perfume');
    } else if (window.location.pathname.includes('/explore_home')) {
      faqBlock = _.find(faqBlocks || [], d => d.value?.header?.header_text === 'explore_home');
    }
    const headerBlock = ['explore_perfume', 'explore_home'].includes(typeSelection) ? _.find(headerParagraphBlock, x => x.value.header.header_text === typeSelection) : undefined;
    const paragraphBlockProduct = _.find(ctaBlock, x => x.text === typeSelection);
    const { match } = this.props;
    const { params } = match;
    const { tags } = params || {};
    const seoTitle = tags ? getNameFromButtonBlock(buttonBlocks, `title ${tags}`) : (seo ? seo.seoTitle : '');
    const seoKeywords = tags ? getNameFromButtonBlock(buttonBlocks, `keywords ${tags}`) : (seo ? seo.seoKeywords : '');
    const seoDescription = tags ? getNameFromButtonBlock(buttonBlocks, `description ${tags}`) : (seo ? seo.seoDescription : '');

    return (
      <div style={{ position: 'relative' }}>
        <MetaTags>
          <title>
            {seoTitle}
          </title>
          <meta name="description" content={seoDescription} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/all-products')}
        </MetaTags>
        {/* <HeaderHomePage />
        { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <HeaderHomePageV3 />
        <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
        />
        <div className="div-product-v2">
          <ProductBlock
            data={ctaBlock}
            imageBackground={imageBackground}
            buttonBlocks={buttonBlocks}
            imageBlocks={imageBlocks}
            headerText={headerText}
            addScentsToStore={this.props.addScentsToStore}
            addScentsWaxToStore={this.props.addScentsWaxToStore}
            addScentsCandleToStore={this.props.addScentsCandleToStore}
            addScentsHomeToStore={this.props.addScentsHomeToStore}
            scents={this.props.scents}
            scentsWax={this.props.scentsWax}
            scentsCandle={this.props.scentsCandle}
            scentsHome={this.props.scentsHome}
            loadingPage={this.props.loadingPage}
            onClickIngredient={this.onClickIngredient}
            cms={this.props.cms}
            basket={this.props.basket}
            addProductBasket={this.props.addProductBasket}
            createBasketGuest={this.props.createBasketGuest}
            params={params}
            changeDescription={this.changeDescription}
            onClickGotoProduct={this.onClickGotoProduct}
            history={this.props.history}
            onChangeShowStep={this.onChangeShowStep}
          />
          <TextBlock data={headerBlock ? { value: headerBlock.value.paragraph } : paragraphBlockProduct ? { value: paragraphBlockProduct.description } : paragraphBlock} />
          {
            showStep === 0 && (
              <React.Fragment>
                <TextImageBlock data={imageTxtBlock && imageTxtBlock.length > 0 ? imageTxtBlock[0] : undefined} />
                <TextImageBlock isRight data={imageTxtBlock && imageTxtBlock.length > 1 ? imageTxtBlock[1] : undefined} />
              </React.Fragment>
            )
          }
          {
            showStep === 1 && (
              <React.Fragment>
                <TextImageBlock isRight data={imageTxtBlock && imageTxtBlock.length > 2 ? imageTxtBlock[2] : undefined} />
                <TextImageBlock data={imageTxtBlock && imageTxtBlock.length > 3 ? imageTxtBlock[3] : undefined} />
              </React.Fragment>
            )
          }
          {
            showStep === 2 && (
              <React.Fragment>
                <TextImageBlock isRight data={imageTxtBlock && imageTxtBlock.length > 4 ? imageTxtBlock[4] : undefined} />
                <TextImageBlock data={imageTxtBlock && imageTxtBlock.length > 5 ? imageTxtBlock[5] : undefined} />
              </React.Fragment>
            )
          }
          {
            !typeSelection
            && (<BecomeBlock data={lessonBlock} />)
          }

          <BlockIconHeader heroCms={benefitsBlock} />
          {/* <BlockStepItem heroCms={benefitsBlock} history={this.props.history} login={this.props.login} index={1} isBenefitsBlock /> */}
          {
            faqBlock && (
              <div className="qna-block-product">
                <QnABlock data={faqBlock?.value} />
              </div>
            )
          }

        </div>
        <FooterV2 />
      </div>
    );
  }
}

ProductAllV2.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  addScentsToStore: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  basket: PropTypes.shape().isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  addScentsHomeToStore: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    scents: state.scents,
    scentsWax: state.scentsWax,
    scentsCandle: state.scentsCandle,
    scentsHome: state.scentsHome,
    basket: state.basket,
    login: state.login,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  addScentsToStore,
  addScentsWaxToStore,
  loadingPage,
  createBasketGuest,
  addProductBasket,
  addScentsCandleToStore,
  addScentsHomeToStore,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductAllV2));
