import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';
import {
  Label, Input, Modal, ModalBody,
} from 'reactstrap';
import { isMobile, withOrientationChange, isIOS } from 'react-device-detect';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';

import icEmail from '../../image/icon/ic-email-input.svg';
import icRedo from '../../image/icon/icRedo.svg';
import PopUpMixSuccess from './popUpMixSuccess';
import { validateEmail, getNameFromButtonBlock, trackGTMLogin } from '../../Redux/Helpers';
import { toastrError } from '../../Redux/Helpers/notification';
import {
  PUT_OUTCOME_URL, CHECK_EMAIL_LOGIN_URL, LOGIN_FACEBOOK_URL, LOGIN_GOOGLE_URL,
} from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import facebook from '../../image/facebook.png';
import google from '../../image/google.png';
import ItemScentMix from './itemScentMix';

class CutomMixes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: props.email,
      isShowSuccess: false,
      isCreateAccount: false,
      isShowCreateAccount: false,
    };
  }

  onChangeEmail = (e) => {
    const { value } = e.target;
    this.setState({ email: value });
    // if (this.timeOutEmail) {
    //   clearTimeout(this.timeOutEmail);
    //   this.timeOutEmail = null;
    // }
    // this.timeOutEmail = setTimeout(this.checkEmailExist, 1000);
  }

  checkEmailExist = () => {
    const { email } = this.state;
    const option = {
      url: CHECK_EMAIL_LOGIN_URL.replace('{email}', email),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.setState({ isShowCreateAccount: false, isCreateAccount: false });
        return;
      }
      throw new Error();
    }).catch(() => {
      this.setState({ isShowCreateAccount: true, isCreateAccount: true });
    });
  }

  onClickSubmit = () => {
    const { email } = this.state;
    if (email && !validateEmail(email)) {
      toastrError(`${getNameFromButtonBlock(this.props.buttonBlocks, 'Email_is_not_avaiable')}`);
      return;
    }
    this.updateLikeData(email);
  }

  onDeletePerfume = (id) => {
    const { listPerfume } = this.props;
    _.remove(listPerfume, x => x.combo.id === id);
    this.forceUpdate();
    this.props.updateProductMix();
  }

  onClickEdit = (id) => {
    const { listPerfume } = this.props;
    const ele = _.find(listPerfume, x => x.combo.id === id);
    this.props.editCutomMix(ele);
  }

  updateUserIdSocial = (user) => {
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', this.props.idOutCome),
      method: 'PUT',
      body: {
        user,
      },
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        this.setState({ isShowSuccess: true });
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err.message);
    });
  }

  updateLikeData = (email) => {
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', this.props.idOutCome),
      method: 'PUT',
    };
    if (email) {
      _.assign(options, {
        body: {
          email,
          send_email: true,
          is_boutique: true,
        },
      });
    }
    // create account
    const { isCreateAccount } = this.state;
    if (isCreateAccount) {
      _.assign(options.body, {
        meta: {
          guest: {
            email,
            create_account: true,
          },
        },
      });
    }

    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        this.setState({ isShowSuccess: true });
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err.message);
    });
  }

  itemScentMixAdd = () => (
    <div className="div-item-mix add-card" onClick={this.props.onClickCreateCombo}>
      <div className="div-content">
        <span className="add-mix">
          {`+ ${getNameFromButtonBlock(this.props.buttonBlocks, isMobile && !this.props.isLandscape ? 'NEW CREATION' : 'ADD_MIX')}`}
        </span>
      </div>
    </div>
  )

  itemScentMixAddMobile = () => (
    <div className="div-wrap-mix-mobile">
      <div className="div-item-mix-mobile add-card" onClick={this.props.onClickCreateCombo}>
        <div className="div-content">
          <span className="add-mix">
            {`+ ${getNameFromButtonBlock(this.props.buttonBlocks, 'ADD_MIX')}`}
          </span>
        </div>
      </div>
    </div>

  )

  onChangeCheckbox = (e) => {
    const { checked } = e.target;
    this.setState({ isCreateAccount: checked });
  }

  responseFacebook = (response) => {
    console.log(response);
    const { accessToken } = response;
    if (accessToken) {
      this.loginSocial(accessToken, LOGIN_FACEBOOK_URL);
    }
  }

  responseGoogle = (response) => {
    console.log(response);
    const { accessToken } = response;
    if (accessToken) {
      this.loginSocial(accessToken, LOGIN_GOOGLE_URL);
    }
  }

  loginSocial = (accessToken, url) => {
    const options = {
      url,
      method: 'POST',
      body: {
        access_token: accessToken,
      },
    };
    fetchClient(options).then((result) => {
      if (!result.isError) {
        // tracking GTM
        if (url === LOGIN_GOOGLE_URL) {
          trackGTMLogin('google', result.user.id);
        } else if (url === LOGIN_FACEBOOK_URL) {
          trackGTMLogin('facebook', result.user.id);
        }
        this.updateUserIdSocial(result.user.id);
      } else {
        throw new Error(result.message);
      }
    }).catch((e) => {
      toastrError(e.message);
    });
  }

  render() {
    const settings = {
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      pauseOnHover: false,
      // centerMode: true,
      variableWidth: true,
    };
    const {
      listPerfume, buttonBlocks, imageTxtBlocks, isLandscape, namePath,
    } = this.props;
    const { isShowSuccess, isShowCreateAccount } = this.state;

    console.log('listPerfume', listPerfume);
    const newListPerfume = _.map(listPerfume, (x) => {
      const product1 = _.assign({}, x.listScent[0]);
      _.assign(product1.product, { type: 'Scent' });
      _.assign(product1.product, { ingredient: x.listScent[0].ingredient });
      _.assign(product1.product, { images: [{ type: 'unisex', image: x.listScent[0].image }] });
      const product2 = _.assign({}, x.listScent[1]);
      _.assign(product2.product, { type: 'Scent' });
      _.assign(product2.product, { ingredient: x.listScent[1].ingredient });
      _.assign(product2.product, { images: [{ type: 'unisex', image: x.listScent[1].image }] });
      return (
        {
          combo: [product2, product1],
          item: x.combo,
          note: x.note,
          listScent: x.listScent,
        }
      );
    });

    const yourCustomBt = getNameFromButtonBlock(buttonBlocks, 'YOUR_CUSTOM_MIXES');
    const saveBt = getNameFromButtonBlock(buttonBlocks, 'SAVE_AND_CONTINUE_WITH_YOUR_CREATIONS');
    const enterBt = getNameFromButtonBlock(buttonBlocks, 'Enter_your_email');
    const submitBt = getNameFromButtonBlock(buttonBlocks, 'SUBMIT_YOUR_MIX');
    const emailSubmittedBt = getNameFromButtonBlock(buttonBlocks, 'EMAIL_SUBMITTED');
    const showYourBt = getNameFromButtonBlock(buttonBlocks, 'Show_your_device');
    const redoBt = getNameFromButtonBlock(buttonBlocks, 'redo_questionaire');
    const createAccBt = getNameFromButtonBlock(buttonBlocks, 'Create_an_account');
    const orWithAccBt = getNameFromButtonBlock(buttonBlocks, 'or_connect_with');

    const bottomEmail = (
      <div className="div-input-email">
        {
          !this.props.isSubmited ? (
            <React.Fragment>
              <h4>
                {saveBt}
              </h4>
              <div className="div-input">
                <div className="div-create-accout">
                  <div className="div-enter">
                    {
                      !['sofitel', 'sephora', 'bmw'].includes(namePath) && _.isEmpty(this.props.email) && (
                        <div className="input-email">
                          <img src={icEmail} alt="email" />
                          <input
                            type="text"
                            name="email"
                            value={this.state.email}
                            placeholder={enterBt}
                            onChange={this.onChangeEmail}
                          />
                        </div>
                      )
                    }
                    {
                      (isMobile && !this.props.isLandscape && isShowCreateAccount) && (
                        <Label className="label-account" check style={{ cursor: 'pointer' }}>
                          <Input
                            name="create_account"
                            id="idCheckbox"
                            type="checkbox"
                            className="custom-input-filter__checkbox"
                            style={{ height: '42px' }}
                            checked={this.state.isCreateAccount}
                            onChange={this.onChangeCheckbox}
                          />
                          {' '}
                          <Label
                            for="idCheckbox"
                            style={{ pointerEvents: 'none' }}
                          />
                          {createAccBt}
                        </Label>
                      )
                    }
                    <button
                      type="button"
                      onClick={this.onClickSubmit}
                    >
                      {submitBt}
                    </button>
                    <div className={isMobile && !this.props.isLandscape && !['sofitel', 'sephora', 'bmw'].includes(namePath) ? 'div-line-text' : 'hidden'}>
                      <div className="div-line" />
                      <span>
                        {orWithAccBt}
                      </span>
                      <div className="div-line" />
                    </div>
                    <div className={!['sofitel', 'sephora', 'bmw'].includes(namePath) ? 'login-account' : 'hidden'}>
                      <FacebookLogin
                        appId="276035609956267"
                        callback={this.responseFacebook}
                        render={renderProps => (
                          <div className="icon-social facebook" onClick={renderProps.onClick}>
                            <a
                              href
                              onClick={(e) => {
                                e.preventDefault();
                                renderProps.onClick();
                              }}
                            >
                              <img src={facebook} alt="" />
                            </a>
                          </div>
                        )}
                      />
                      <GoogleLogin
                        clientId="1051429470092-1ktl08tfm23q3b7i2au9ivv6pnil51g1.apps.googleusercontent.com"
                        onSuccess={this.responseGoogle}
                        onFailure={this.onFailure}
                        render={renderProps => (
                          <div className="icon-social" onClick={renderProps.onClick}>
                            <a
                              href
                              onClick={(e) => {
                                e.preventDefault();
                                renderProps.onClick();
                              }}
                            >
                              <img src={google} alt="" />
                            </a>
                          </div>
                        )}
                      />
                      {/* <button type="button" className="button-bg__none">
                        <img src={facebook} alt="facebook" />
                      </button>
                      <button type="button" className="button-bg__none">
                        <img src={google} alt="google" />
                      </button> */}
                    </div>
                  </div>
                  {
                    !(isMobile && !this.props.isLandscape) && isShowCreateAccount && (
                      <Label className="label-account" check style={{ cursor: 'pointer' }}>
                        <Input
                          name="create_account"
                          id="idCheckbox"
                          type="checkbox"
                          className="custom-input-filter__checkbox"
                          style={{ height: '42px' }}
                          checked={this.state.isCreateAccount}
                          onChange={this.onChangeCheckbox}
                        />
                        {' '}
                        <Label
                          for="idCheckbox"
                          style={{ pointerEvents: 'none' }}
                        />
                        {createAccBt}
                      </Label>
                    )
                  }

                </div>
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <h3>
                {emailSubmittedBt}
              </h3>
              <span>
                {showYourBt}
              </span>
            </React.Fragment>
          )
        }
      </div>
    );

    return (
      <div className={`div-custom-mixes ${['sofitel'].includes(this.props.namePath) ? 'full-height' : ''}`}>
        {
          isShowSuccess ? (
            <PopUpMixSuccess
              buttonBlocks={buttonBlocks}
              imageTxtBlocks={imageTxtBlocks}
              onClose={() => {
                this.setState({ isShowSuccess: false });
                this.props.setSubmited();
              }}
              isLandscape={isLandscape}
            />
          ) : null
        }
        <button className="bt-back button-bg__none" type="button" onClick={this.props.gotoCustomMix}>
          <img src={icRedo} alt="back" />
          {redoBt}
        </button>
        <div className="div-custom-mixes-content">
          <div className="div-header">
            <h2>
              {yourCustomBt}
            </h2>
          </div>
          <div className="div-list-mixes">
            {
              (isMobile && !isIOS) || (isMobile && !isLandscape) ? (
                <React.Fragment>
                  {
                    _.map(newListPerfume, (d, index) => (
                      // this.itemScentMixMobile(d, index)
                      <ItemScentMix
                        data={d}
                        index={index}
                        cmsCommon={this.props.cmsCommon}
                        onClickIngredient={this.props.onClickIngredient}
                        buttonBlocks={this.props.buttonBlocks}
                        onClickEdit={this.onClickEdit}
                        onDeletePerfume={this.onDeletePerfume}
                        isMobileProps
                        orginalData={listPerfume[index]}
                        updateProductMix={this.props.updateProductMix}
                      />
                    ))
                  }
                  {this.itemScentMixAddMobile()}
                  {bottomEmail}
                </React.Fragment>
              ) : (
                <Slider {...settings}>
                  {
                    _.map(newListPerfume, (d, index) => (
                      // this.itemScentMix(d, index)
                      <ItemScentMix
                        data={d}
                        index={index}
                        cmsCommon={this.props.cmsCommon}
                        onClickIngredient={this.props.onClickIngredient}
                        buttonBlocks={this.props.buttonBlocks}
                        onClickEdit={this.onClickEdit}
                        onDeletePerfume={this.onDeletePerfume}
                        orginalData={listPerfume[index]}
                        updateProductMix={this.props.updateProductMix}
                      />
                    ))
                  }
                  {this.itemScentMixAdd()}
                </Slider>
              )
            }
          </div>
          {(isMobile && !isIOS) || (isMobile && !isLandscape) ? null : bottomEmail}
        </div>
      </div>
    );
  }
}

export default withOrientationChange(CutomMixes);
