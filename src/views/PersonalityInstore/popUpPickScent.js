import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { isMobile } from 'react-device-detect';
import icClose from '../../image/icon/ic-close-region.svg';
import icCloseMobile from '../../image/icon/ic-close-pick-mobile.svg';
import FilterSearchScent from '../../components/ProductAllV2/ProductBlock/filterSearchScent';
import ScentLine from '../../components/ProductAllV2/ProductBlock/scentLine';
import { GET_PRODUCT_FOR_CART, GET_INGREDIENT_ID_PRODUCTS } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { getNameFromButtonBlock } from '../../Redux/Helpers';


class PopupPickScent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scentSelected: props.dataSelection,
      valueFilter: 'Alphabet',
      valueSearch: '',
    };
    this.refFilterSearchScent = React.createRef();
  }

  componentDidMount = () => {
    if (this.props.shareRefFilterSearchScent) {
      this.props.shareRefFilterSearchScent(this.refFilterSearchScent);
    }
  };

  onChangeValueFilter = (value) => {
    this.setState({ valueFilter: value });
  }

  onChangeValueSearch = (value) => {
    this.setState({ valueSearch: value });
  }

  applyFilter = (dataScents, value) => {
    const datas = _.find(dataScents, x => x.title.includes(value) || x.title.includes(value.toLowerCase()));
    if (value === 'Alphabet' || value === 'Popularity') {
      return datas ? datas.datas : [];
    }
    const datasNew = datas ? datas.datas : [];
    const scentT1 = _.filter(datasNew, x => x.group.toLowerCase() === value.toLowerCase());
    return scentT1;
    // const scentT2 = _.filter(datasNew, x => x.group.toLowerCase() !== value.toLowerCase());
    // return scentT1.concat(scentT2);
  }

  applySearch = (dataScents, value) => {
    if (!value) {
      return dataScents;
    }
    const newDataScents = _.cloneDeep(dataScents);
    _.forEach(newDataScents, (d) => {
      const { datas } = d;
      d.datas = _.filter(datas, x => x.name.toLowerCase().includes(value.toLowerCase()));
    });
    return newDataScents;
  }

  getIngredient = (id) => {
    const option = {
      url: GET_INGREDIENT_ID_PRODUCTS.replace('{id}', id),
      method: 'GET',
    };
    return fetchClient(option);
  }

  getProduct = async (scent) => {
    const option = {
      url: `${GET_PRODUCT_FOR_CART}?combo=${scent ? scent.id : ''}&type=scent`,
      method: 'GET',
    };
    return fetchClient(option);
  }

  onClick = async (data) => {
    const { scentSelected } = this.props;
    _.assign(scentSelected, data);
    const datas = await Promise.all([this.getProduct(data), this.getIngredient(data.ingredient)]);
    _.assign(scentSelected, { product: datas[0], ingredient: datas[1] });
    this.props.onFetchComo();
    this.props.onClose();
  }

  render() {
    const { valueFilter, valueSearch } = this.state;
    const {
      scentSelected, buttonBlocks, dataScents, onClose, title, isClosePopup, isLandscape,
    } = this.props;
    const newScentFilter = this.applyFilter(dataScents, valueFilter);
    const newScent = this.applySearch(newScentFilter, valueSearch);
    const pickBt = getNameFromButtonBlock(buttonBlocks, 'Pick_a_ingredient');

    console.log('newScent', newScent);
    return (
      <div className={`div-pop-pick-scent animated faster ${isMobile && !isLandscape ? isClosePopup ? 'fadeOutDown' : 'fadeInUp' : (isClosePopup ? 'fadeOut' : 'fadeIn')}`}>
        {
          isMobile && !isLandscape ? (
            <div style={{ flex: '1', width: '100%' }} onClick={onClose} />
          ) : null
        }
        <div className="div-content">
          <button type="button" className="button-bg__none bt-close" onClick={onClose}>
            <img loading="lazy" src={isMobile && !isLandscape ? icCloseMobile : icClose} alt="icon" />
          </button>
          <div className="div-content-popup">
            <div className="div-header">
              <h4>
                {pickBt}
              </h4>
              <div className={isMobile && !isLandscape ? 'hidden' : 'div-filter-search'}>
                <span className="header-filter">
                  FILTER
                </span>
                <FilterSearchScent
                  ref={this.refFilterSearchScent}
                  onChangeFilter={this.props.onChangeFilter}
                  onChangeSearch={this.props.onChangeSearch}
                  valueFilter={valueFilter}
                  valueSearch={valueSearch}
                  onChangeValueFilter={this.onChangeValueFilter}
                  onChangeValueSearch={this.onChangeValueSearch}
                  buttonBlocks={buttonBlocks}
                  isLandscape={isLandscape}
                />
              </div>
            </div>
            <span className="sub-header">
              {title}
            </span>
            <div className={isMobile && !isLandscape ? 'div-filter-search-mobile' : 'hidden'}>
              <FilterSearchScent
                ref={this.refFilterSearchScent}
                onChangeFilter={this.props.onChangeFilter}
                onChangeSearch={this.props.onChangeSearch}
                valueFilter={valueFilter}
                valueSearch={valueSearch}
                onChangeValueFilter={this.onChangeValueFilter}
                onChangeValueSearch={this.onChangeValueSearch}
                buttonBlocks={buttonBlocks}
                isLandscape={isLandscape}
              />
            </div>
            <div className="div-scroll-ingredient">
              {
              _.map(newScent, x => (
                <ScentLine
                  buttonBlocks={buttonBlocks}
                  data={x}
                  onClick={this.onClick}
                  scentSelected={scentSelected}
                  isNoInfo
                />
              ))
            }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

PopupPickScent.defaultProps = {
  buttonBlocks: [],
  dataScents: [],
};

PopupPickScent.propTypes = {
  buttonBlocks: PropTypes.arrayOf(),
  dataScents: PropTypes.arrayOf(),
};

export default PopupPickScent;
