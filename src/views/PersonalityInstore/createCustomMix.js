import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  isMobile, isIOS, isMobileOnly, withOrientationChange,
} from 'react-device-detect';
import classnames from 'classnames';
import ItemIngredientCustom from './itemIngredientCustom';
import icBack from '../../image/icon/ic-back.svg';
import ProgressLine from '../ResultScentV2/progressLine';
import ProgressCircle from '../ResultScentV2/progressCircle';
import icCombo from '../../image/icon/ic-combo.svg';
import icComboBlack from '../../image/icon/ic-combo-black.svg';
import PopupPickScent from './popUpPickScent';
import bottle1 from '../../image/icon/bottle-cutom-1.svg';
import bottle2 from '../../image/icon/bottle-cutom-2.svg';
import bottle3 from '../../image/icon/bottle-cutom-3.svg';
import bottle4 from '../../image/icon/bottle-cutom-4.svg';
import bottle5 from '../../image/icon/bottle-cutom-5.svg';
import { GET_PRODUCT_FOR_CART } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { getNameFromButtonBlock } from '../../Redux/Helpers';
import icPrev from '../../image/icon/prev-menu.svg';
import icNext from '../../image/icon/next-menu.svg';

class CreateCustomMix extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowSolo: false,
      scents: {
        listScent: props.scentEdit ? props.scentEdit.listScent : [{}, {}],
        combo: props.scentEdit ? props.scentEdit.combo : {},
      },
      indexChoose: 0,
      isClosePopup: false,
      isStarted: true,
      isEnded: true,
    };
  }

  onChoosePick = (index) => {
    this.setState({ indexChoose: index, isClosePopup: false });
  }

  onClickDeleteScent = (data) => {
    const { scents } = this.state;
    _.remove(scents.listScent, x => x.id === data.id);
    if (scents.listScent.length < 5 && !_.isEmpty(scents.listScent[scents.listScent.length - 1])) {
      scents.listScent.push({});
    }
    this.setState({ scents });
  }

  onCloseChoosePick = () => {
    const { scents } = this.state;
    if (scents.listScent.length < 5 && !_.isEmpty(scents.listScent[scents.listScent.length - 1])) {
      scents.listScent.push({});
    }
    this.setState({ isClosePopup: true, scents });
    setTimeout(() => {
      this.setState({ indexChoose: 0 });
      this.scrollToEnd();
    }, 300);
  }

  getProductCombo = async (scents) => {
    const option = {
      url: scents.listScent[0].id === scents.listScent[1].id ? `${GET_PRODUCT_FOR_CART}?combo=${scents.listScent[0].id}&type=scent` : `${GET_PRODUCT_FOR_CART}?combo=${scents.listScent[0].id}${`,${scents.listScent[1].id}`}&type=perfume`,
      method: 'GET',
    };
    return fetchClient(option);
  }

  onClickShowSolo = async () => {
    const { scents, isShowSolo } = this.state;
    const { combo } = scents;
    if (_.isEmpty(combo)) {
      const comboData = await this.getProductCombo(scents);
      _.assign(scents, { combo: comboData });
    }
    this.setState({ isShowSolo: !isShowSolo });
  }

  addListCombo = () => {
    if (this.props.scentEdit) {
      this.props.onClickAddCombo(this.state.scents, true);
    } else {
      this.props.onClickAddCombo(this.state.scents);
    }
  }

  onFetchComo = async () => {
    const { scents } = this.state;
    // if (_.isEmpty(scents.scent1) || _.isEmpty(scents.scent2)) {
    //   return;
    // }
    if (scents.listScent < 2) {
      return;
    }
    const comboData = await this.getProductCombo(scents);
    _.assign(scents, { combo: comboData });
  }

  scrollToStart = () => {
    const ele = document.getElementById('content-create');
    ele.scrollLeft = 0;
  };

  scrollToEnd = () => {
    const ele = document.getElementById('content-create');
    ele.scrollLeft = ele.scrollWidth - ele.clientWidth;
  };

  onscroll = () => {
    const ele = document.getElementById('content-create');
    const { isStarted, isEnded } = this.state;
    if (ele) {
      const object = {};
      if (ele.scrollLeft < 30 && !isStarted) {
        _.assign(object, { isStarted: true });
      } else if (ele.scrollLeft > 30 && isStarted) {
        _.assign(object, { isStarted: false });
      }

      const maxOffest = ele.scrollWidth - ele.clientWidth;
      if (Math.abs(ele.scrollLeft - maxOffest) < 30 && !isEnded) {
        _.assign(object, { isEnded: true });
      } else if (Math.abs(ele.scrollLeft - maxOffest) > 30 && isEnded) {
        _.assign(object, { isEnded: false });
      }
      _.assign(this.state, object);
      this.setState(object);
    }
  };

  getNoText = (index) => {
    const { buttonBlocks } = this.props;
    const st1Bt = getNameFromButtonBlock(buttonBlocks, '1ST');
    const nd2Bt = getNameFromButtonBlock(buttonBlocks, '2ND');
    const rd3Bt = getNameFromButtonBlock(buttonBlocks, '3RD');
    const th4Bt = getNameFromButtonBlock(buttonBlocks, '4TH');
    const th5Bt = getNameFromButtonBlock(buttonBlocks, '5TH');
    return [st1Bt, nd2Bt, rd3Bt, th4Bt, th5Bt][index];
  }

  getTitle = (index) => {
    const { buttonBlocks } = this.props;
    const st1Bt = getNameFromButtonBlock(buttonBlocks, '1ST_INGREDIENT');
    const nd2Bt = getNameFromButtonBlock(buttonBlocks, '2ND_INGREDIENT');
    const rd3Bt = getNameFromButtonBlock(buttonBlocks, '3RD_INGREDIENT');
    const th4Bt = getNameFromButtonBlock(buttonBlocks, '4TH_INGREDIENT');
    const th5Bt = getNameFromButtonBlock(buttonBlocks, '5TH_INGREDIENT');
    return [st1Bt, nd2Bt, rd3Bt, th4Bt, th5Bt][index];
  }

  getBottle = index => [bottle1, bottle2, bottle3, bottle4, bottle5][index]

  render() {
    const {
      isShowSolo, scents, indexChoose, isClosePopup, isStarted, isEnded,
    } = this.state;
    const { dataScents, onClickAddCombo, buttonBlocks } = this.props;
    console.log('indexChoose', indexChoose);
    // const isShowBtView = !_.isEmpty(scents.scent1) && !_.isEmpty(scents.scent2);
    const isMutileScent = _.filter(scents.listScent, x => !_.isEmpty(x)).length > 2;
    const isShowBtView = _.filter(scents.listScent, x => !_.isEmpty(x)).length > 1;
    const strengthBt = getNameFromButtonBlock(buttonBlocks, 'STRENGTH');
    const durationBt = getNameFromButtonBlock(buttonBlocks, 'duration');
    const backBt = getNameFromButtonBlock(buttonBlocks, 'Back');
    const createBt = getNameFromButtonBlock(buttonBlocks, 'CREATE_CUSTOM_MIX');
    const chooseBt = getNameFromButtonBlock(buttonBlocks, 'CHOOSE_YOUR_INGREDIENTS');
    const comboBt = getNameFromButtonBlock(buttonBlocks, 'COMBO_SCENT');
    const saveBt = getNameFromButtonBlock(buttonBlocks, 'SAVE_AND_CONTINUE');
    const st1Bt = getNameFromButtonBlock(buttonBlocks, '1ST_INGREDIENT');
    const st2Bt = getNameFromButtonBlock(buttonBlocks, '2ND_INGREDIENT');
    const comboViewBt = getNameFromButtonBlock(buttonBlocks, 'COMBO_VIEW');
    const soloBt = getNameFromButtonBlock(buttonBlocks, 'SOLO_VIEW');
    if (indexChoose > scents.listScent.length) {
      for (let i = 0; i < 2; i += 1) {
        scents.listScent.push({});
      }
    }
    console.log('scents.listScent', scents.listScent);
    return (
      <div>
        {
          indexChoose
            ? (
              <PopupPickScent
                dataScents={dataScents}
                onClose={this.onCloseChoosePick}
                scentSelected={scents.listScent[indexChoose - 1]}
                title={indexChoose === 1 ? st1Bt : st2Bt}
                onFetchComo={this.onFetchComo}
                buttonBlocks={buttonBlocks}
                isClosePopup={isClosePopup}
                onChangeFilter={this.props.onChangeFilter}
                onChangeSearch={this.props.onChangeSearch}
                shareRefFilterSearchScent={this.props.shareRefFilterSearchScent}
                isLandscape={this.props.isLandscape}
              />
            ) : null
        }

        <div className={`div-create-custom-mix ${['sofitel'].includes(this.props.namePath) ? 'full-height' : ''} animated faster fadeInRight`}>
          <button className="bt-back button-bg__none" type="button" onClick={this.props.onGoBack}>
            <img src={icBack} alt="back" />
            {backBt}
          </button>
          <button
            className={`bt-combo-view ${isShowSolo ? '' : 'bg-white'} ${isShowBtView && !isMutileScent ? '' : 'hidden'}`}
            type="button"
            onClick={this.onClickShowSolo}
          >
            {isShowSolo ? comboViewBt : soloBt}
            <img src={isShowSolo ? icCombo : icComboBlack} alt="combo" />
          </button>

          <div className="div-content-boutique">
            <div className="div-header">
              <h2>
                {createBt}
              </h2>
              <span>
                {chooseBt}
              </span>
            </div>
            <div className="div-content-create">
              {
                !isMobile && !isStarted && (
                  <div className="click-prev animated faster fadeIn">
                    <button
                      onClick={this.scrollToStart}
                      className="button-bg__none"
                      type="button"
                    >
                      <img src={icPrev} alt="icon" />
                    </button>
                  </div>
                )
              }
              {
                !isMobile && !isEnded && (
                  <div className="click-next animated faster fadeIn">
                    <button
                      onClick={this.scrollToEnd}
                      className="button-bg__none"
                      type="button"
                    >
                      <img src={icNext} alt="icon" />
                    </button>
                  </div>
                )
              }
              {
              !isShowSolo ? (
                <div
                  id="content-create"
                  className={classnames('content-create animated faster fadeInLeft', (scents.listScent.length > 2 ? 'scroll-able' : ''))}
                  onScroll={this.onscroll}
                >
                  <ItemIngredientCustom
                    onClick={() => this.onChoosePick(1)}
                    onDelete={() => this.onClickDeleteScent(scents.listScent[0])}
                    data={scents.listScent[0]}
                    className="item-left"
                    title={this.getTitle(0)}
                    no={this.getNoText(0)}
                    iconBottle={this.getBottle(0)}
                    onClickIngredient={this.props.onClickIngredient}
                    buttonBlocks={buttonBlocks}
                  />
                  <ItemIngredientCustom
                    onClick={() => this.onChoosePick(2)}
                    onDelete={() => this.onClickDeleteScent(scents.listScent[1])}
                    data={scents.listScent[1]}
                    className="item-right"
                    title={this.getTitle(1)}
                    no={this.getNoText(1)}
                    iconBottle={this.getBottle(1)}
                    onClickIngredient={this.props.onClickIngredient}
                    buttonBlocks={buttonBlocks}
                  />
                  {
                    _.map(scents.listScent.slice(2, 5), (d, index) => (
                      <ItemIngredientCustom
                        onClick={() => this.onChoosePick(index + 3)}
                        onDelete={() => this.onClickDeleteScent(scents.listScent[index + 2])}
                        data={scents.listScent[index + 2]}
                        className="item-right"
                        title={this.getTitle(index + 2)}
                        no={this.getNoText(index + 2)}
                        iconBottle={this.getBottle(index + 2)}
                        onClickIngredient={this.props.onClickIngredient}
                        buttonBlocks={buttonBlocks}
                      />
                    ))
                  }
                </div>
              ) : (
                <div className="div-result-mix animated faster fadeInRight">
                  <div className="div-image-info">
                    <div className="text-left">
                      <h4>
                        {scents.listScent[0].name}
                      </h4>
                      <span>
                        {scents.listScent[0].ingredient.title}
                      </span>
                    </div>
                    <div className="image-mix">
                      <img className="img-left" src={scents.listScent[0].image} alt="scent" />
                      <img className="img-right" src={scents.listScent[1].image} alt="scent" />
                      {/* <span>
                        {comboBt}
                      </span> */}
                    </div>
                    <div className="text-right">
                      <h4>
                        {scents.listScent[1].name}
                      </h4>
                      <span>
                        {scents.listScent[1].ingredient.title}
                      </span>
                    </div>
                  </div>
                  <div className="div-profile-info">
                    <div className="div-process-line">
                      {
                         _.map(scents.combo.profile ? scents.combo.profile.accords : [], x => (
                           <ProgressLine data={x} isShowPercent />
                         ))
                      }
                    </div>
                    <div className="div-process-circle">
                      <ProgressCircle title={strengthBt} percent={scents.combo.profile ? parseInt(parseFloat(scents.combo.profile.strength) * 100, 10) : 0} />
                      <ProgressCircle title={durationBt} percent={scents.combo.profile ? parseInt(parseFloat(scents.combo.profile.duration) * 100, 10) : 0} />
                    </div>
                  </div>
                </div>
              )
            }
            </div>
            <div style={isMobileOnly && isIOS ? { marginBottom: '30%' } : {}} className={isShowBtView ? 'div-button-save' : 'hidden'}>
              <button type="button" onClick={this.addListCombo}>
                {saveBt}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CreateCustomMix.propTypes = {
  dataScents: PropTypes.arrayOf().isRequired,
  isLandscape: PropTypes.bool.isRequired,
};

export default withOrientationChange(CreateCustomMix);
