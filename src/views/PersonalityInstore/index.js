/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import InputMask from 'react-input-mask';
import moment from 'moment';

import { isBrowser, isMobile } from 'react-device-detect';
import * as EmailValidator from 'email-validator';
import icSearch from '../../image/icon/ic-search-instore.svg';
import {
  getAltImage, fetchCMSHomepage, getSEOFromCms, getCmsCommon, getNameFromCommon, generaCurrency, googleAnalitycs,
  isCheckNull,
  generateUrlWeb,
  setPrerenderReady, gotoShopHome,
} from '../../Redux/Helpers';
import ItemOrder from './itemOrder';
import '../../styles/personaltity-instore.scss';
import logo from '../../image/logo_quiz.svg';
import icEmail from '../../image/icon/ic-email-input.svg';
import {
  GET_ALL_PRODUCTS_SCENT, PRODUCTS_V2_URL, GET_COMBO_URL, CHECKOUT_FUNCTION_URL, PUT_OUTCOME_URL, GET_ORDER_URL, GET_MINI_WAX_PERFUME_URL,
} from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import loadingPage from '../../Redux/Actions/loading';
import addCmsRedux from '../../Redux/Actions/cms';
import {
  createBasketGuestArray, addProductBasketArray, deleteProductBasket, updateProductBasket,
} from '../../Redux/Actions/basket';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';

import icUndo from '../../image/icon/ic-back-instore.svg';
import icChecked from '../../image/icon/ic-checked-instore.svg';
import icCalendar from '../../image/icon/ic-calendar.svg';
import HeaderPartnership from '../../components/HomePage/headerPartnership';

const getCms = (cmsProduct) => {
  const objectReturn = {
    headerText: '',
    body: [],
    seo: undefined,
  };
  if (cmsProduct) {
    const { header_text: headerText, body, image_background: imageBackground } = cmsProduct;
    const imageBlock = _.filter(body, x => x.type === 'image_block');
    const textBlock = _.filter(body, x => x.type === 'text_block');
    const videoBlock = _.find(body, x => x.type === 'video_block');
    const seo = getSEOFromCms(cmsProduct);
    _.assign(objectReturn, {
      headerText, seo, imageBlock, textBlock, videoBlock, imageBackground,
    });
  }
  return objectReturn;
};

function getPathNamePage(path) {
  if (path.includes('/boutique-quiz')) {
    return 'boutique-quiz';
  }
  if (path.includes('/partnership-quiz')) {
    return 'partnership-quiz';
  }
  if (path.includes('/quiz')) {
    return 'quiz';
  }
  return null;
}

class PersonalityInstore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listMenu: [
        {
          name: 'CUSTOM PERFUME',
        },
      ],
      dataShow: [],
      headerText: '',
      seo: {},
      textBlock: [],
      videoBlock: {},
      imageBackground: '',
      searchValue: '',
      selections: [],
      stepShow: 1,
      nameActive: 'CUSTOM PERFUME',
      partnership: {},
    };
    this.dataSubmit = {
      name: '',
      dob: '',
    };
    const { login, match } = this.props;
    this.gender = login && login.user && login.user.gender ? login.user.gender : 'unisex';
    this.idOutCome = match.params.idOutCome;
    this.email = login && login.user ? login.user.email : (this.props.location && this.props.location.state ? this.props.location.state.email : undefined);
    this.namePath = getPathNamePage(window.location.pathname);
    if (!this.email) {
      if (this.namePath === 'boutique-quiz') {
        this.props.history.push(generateUrlWeb('/boutique-quiz'));
      } else {
        gotoShopHome();
      }
    }
  }

  componentDidMount = () => {
    setPrerenderReady();
    if (this.namePath === 'partnership-quiz') {
      this.fetchOutCome();
    }
    this.fetchDataInit();
    googleAnalitycs(this.namePath === 'boutique-quiz' ? '/boutique-quiz/instore' : this.namePath === 'partnership-quiz' ? '/partnership-quiz/instore' : '');
  }

  fetchOutCome = () => {
    if (isCheckNull(this.idOutCome)) {
      return;
    }
    const option = {
      url: PUT_OUTCOME_URL.replace('{id}', this.idOutCome),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        const { partnership } = result;
        if (!partnership) {
          throw new Error();
        }
        this.slugPartnerShip = partnership.slug;
        this.setState({ partnership });
        return;
      }
      throw new Error();
    }).catch(() => {
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    });
  }

  fetchAllScents = () => {
    const options = {
      url: GET_ALL_PRODUCTS_SCENT,
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchCombo = (data) => {
    if (data.length < 2) {
      return undefined;
    }
    const options = {
      url: (GET_COMBO_URL.replace('{id1}', data[0].id)).replace('{id2}', data[1].id),
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchProductsType = (type) => {
    // this.props.loadingPage(true);
    const options = {
      url: PRODUCTS_V2_URL.replace('{type}', type),
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchMiniWaxPerfume = () => {
    const options = {
      url: GET_MINI_WAX_PERFUME_URL,
      method: 'GET',
    };
    return fetchClient(options);
  }

  handleAddItemForWaxPerfume = (data) => {
    _.forEach(data, (d) => {
      d.item = {
        id: d.id,
        price: d.price,
        price_sale: d.price_sale,
      };
    });
  }

  fetchDataInit = async () => {
    let dataCms;
    const { cms } = this.props;
    const cmsPersionalInstore = _.find(cms, x => x.title === 'Personality Instore');
    this.props.loadingPage(true);
    const { listMenu } = this.state;
    const pending = [this.fetchAllScents()];
    if (this.namePath !== 'partnership-quiz') {
      pending.push(this.fetchMiniWaxPerfume());
      pending.push(this.fetchProductsType('wax_perfume'));
      pending.push(this.fetchProductsType('elixir'));
      pending.push(this.fetchProductsType('bundle'));
      pending.push(this.fetchProductsType('sample'));
      pending.push(this.fetchProductsType('kit'));
    }
    if (!cmsPersionalInstore) {
      pending.push(fetchCMSHomepage('personality-instore'));
    } else {
      dataCms = getCms(cmsPersionalInstore);
      const {
        headerText, imageBlock, textBlock, videoBlock, imageBackground, seo,
      } = dataCms;
      this.setState({
        headerText, imageBlock, textBlock, videoBlock, imageBackground, seo,
      });
    }

    try {
      const results = await Promise.all(pending);
      dataCms = dataCms || getCms(this.namePath !== 'partnership-quiz' ? results[7] : results[1]);
      const { imageBlock } = dataCms;
      const scentSort = _.orderBy(results[0], 'name', 'asc');
      _.forEach(results, (x, index) => {
        if (index === 0) {
          listMenu[0].data = scentSort;
          listMenu[0].name = imageBlock[0].value.caption;
          listMenu[0].image = imageBlock[0].value.image;
        } else if (index === 1 && this.namePath !== 'partnership-quiz') {
          this.handleAddItemForWaxPerfume(results[1]);
          const miniWaxPerfume = _.orderBy(results[1], 'name', 'asc');
          listMenu.push({
            data: miniWaxPerfume,
            name: imageBlock[1].value.caption,
            image: imageBlock[1].value.image,
          });
        } else if (index === 7 || (index === 1 && this.namePath === 'partnership-quiz')) {
          const {
            headerText, imageBlock, textBlock, videoBlock, imageBackground, seo,
          } = getCms(this.namePath !== 'partnership-quiz' ? results[7] : results[1]);
          this.setState({
            headerText, imageBlock, textBlock, videoBlock, imageBackground, seo,
          });
          this.props.addCmsRedux(this.namePath !== 'partnership-quiz' ? results[7] : results[1]);
        } else if (this.namePath !== 'partnership-quiz') {
          const name = x && x.length > 0 ? x[0].type.alt_name : undefined;
          if (name) {
            const cmsImage = _.find(imageBlock, d => d.value.caption.toLowerCase() === name.toLowerCase());
            // console.log('cmsImage', cmsImage)
            if (cmsImage) {
              listMenu.push({
                name,
                data: _.orderBy(x, 'name', 'asc'),
                image: cmsImage.value.image,
              });
            }
          }
        }
      });
      console.log('listMenu', listMenu);
      this.setState({ listMenu, dataShow: scentSort });
    } catch (error) {
      console.log('error', error);
    }
    this.props.loadingPage(false);
  }

  getImage = (d) => {
    const { images, image } = d;
    if (images) {
      return (images && _.find(images, x => x.type === 'unisex') ? [_.find(images, x => x.type === 'unisex').image] : undefined);
    }
    return image;
  }

  selectDataShow = (menu) => {
    this.setState({ dataShow: menu.data, nameActive: menu.name });
  }

  onChangeSearch = (e) => {
    const { value } = e.target;
    this.setState({ searchValue: value.toLowerCase() });
  }


  updateSelection = (data) => {
    const { selections } = this.state;
    if (data.type === 'Scent') {
      const scents = _.filter(selections, x => x.type === 'Scent');
      const ele = _.find(selections, x => x.id === data.id);
      if (ele) {
        _.remove(selections, ele);
        this.setState({ selections });
        return;
      }
      if (scents.length === 2) {
        this.setState({ selections: _.remove(selections, x => _.map(scents, d => d.id).includes(x.id)) });
      }
      selections.push(data);
      this.setState({ selections });
    } else {
      selections.push(data);
      this.setState({ selections });
    }
  }

  isAddToCartValid = () => {
    const { selections } = this.state;
    if (_.find(selections, x => x.type !== 'Scent')) {
      return true;
    }
    const scents = _.filter(selections, x => x.type === 'Scent');
    return scents.length > 1;
  }

  onClickAddToCart = async () => {
    const { basket } = this.props;
    const { selections } = this.state;
    const listItems = [];
    try {
      const scents = _.filter(selections, x => x.type === 'Scent');
      console.log('scents', scents);
      if (scents.length > 1) {
        const product = await this.fetchCombo(scents);
        const { items } = product;
        const item = items[0];
        const data = {
          item: item.id,
          price: item.price,
          quantity: 1,
          name: item.name,
        };
        const dataTemp = {
          idCart: basket.id,
          item: data,
          is_boutique: basket.is_boutique ? undefined : true,
        };
        listItems.push(dataTemp);
      }
      const otherItems = _.filter(selections, x => x.type !== 'Scent');
      console.log('otherItems', otherItems);
      _.forEach(otherItems, (d) => {
        const { item } = d;
        const data = {
          item: item.id,
          price: item.price,
          is_featured: item.is_featured,
          quantity: 1,
          name: item.name,
        };
        const dataTemp = {
          idCart: basket.id,
          item: data,
          is_boutique: basket.is_boutique ? undefined : true,
        };
        listItems.push(dataTemp);
      });
      if (!basket.id) {
        this.props.createBasketGuestArray(listItems);
      } else {
        this.props.addProductBasketArray(listItems);
      }
      this.setState({ stepShow: 2, selections: [] });
    } catch (err) {
      console.log('err', err);
    }
  }

  checkOut = () => {
    const { basket } = this.props;
    const options = {
      url: CHECKOUT_FUNCTION_URL.replace('{id}', basket.id),
      method: 'POST',
      body: {
        payment_method: null,
      },
    };
    fetchClient(options, true).then((result) => {
      const { isError, order } = result;
      if (!isError) {
        this.order = order;
        const optionsOrder = {
          url: PUT_OUTCOME_URL.replace('{id}', this.idOutCome),
          method: 'PUT',
          body: {
            order,
          },
        };
        fetchClient(optionsOrder, true);
        this.setState({ stepShow: 3 });
      }
    }).catch((err) => {
      console.log('err', err);
    });
  }

  onChangeEmail = (e) => {
    const { value } = e.target;
    this.email = value;
  }

  onChangeSubmit = (e) => {
    const { value, name } = e.target;
    this.dataSubmit[name] = value;
  }

  onClickSendEmail = () => {
    const option = {
      url: `${GET_ORDER_URL.replace('{id}', this.order)}?email=${this.email}`,
      method: 'GET',
    };
    fetchClient(option, true).then((result) => {
      if (result && !result.isError) {
        toastrSuccess('Send email is successful');
        return;
      }
      throw new Error();
    }).catch((err) => {
      toastrError('Send email is failed');
    });
  }

  onClickSubmit = () => {
    const { name, dob } = this.dataSubmit;
    if (!name || !dob) {
      toastrError('Please eneter your name and date of birth');
      return;
    }
    const dobMoment = moment(dob, 'DD/MM/YYYY');
    if (!dobMoment.isValid() || dobMoment.valueOf() >= moment().valueOf()) {
      toastrError('Date of brith is not available (DD/MM/YYYY)');
      return;
    }
    if (!this.idOutCome) {
      return;
    }
    this.props.loadingPage(true);
    const option = {
      url: PUT_OUTCOME_URL.replace('{id}', this.idOutCome),
      method: 'PUT',
      body: {
        name,
        date_of_birth: dobMoment.valueOf() / 1000,
      },
    };

    fetchClient(option, true).then((result) => {
      this.props.loadingPage(false);
      if (result && !result.isError) {
        // this.onClickSendEmail();
        return;
      }
      throw new Error();
    }).catch((err) => {
      console.log('err', err);
    });
  }

  render() {
    const { cms, login, basket } = this.props;
    if (!basket) {
      return null;
    }
    const {
      listMenu, dataShow, headerText, textBlock, videoBlock, imageBackground, seo, searchValue, selections, stepShow, nameActive,
      partnership,
    } = this.state;
    const cmsCommon = getCmsCommon(cms);
    const addToCart = getNameFromCommon(cmsCommon, 'ADD_TO_MY_SELECTION');
    const cancelBt = getNameFromCommon(cmsCommon, 'CANCEL');
    const totalBt = getNameFromCommon(cmsCommon, 'TOTAL');
    const addMoreBt = getNameFromCommon(cmsCommon, 'ADD_MORE');
    const finishBt = getNameFromCommon(cmsCommon, 'FINISH_ORDER');
    const sendBt = getNameFromCommon(cmsCommon, 'SEND');
    const yesBt = getNameFromCommon(cmsCommon, 'YES_I_WOULD_LIKE_TO');
    const backtoChangeBt = getNameFromCommon(cmsCommon, 'back_to_change_order');
    const takeTheQuizAgainBt = getNameFromCommon(cmsCommon, 'take_the_quiz_again');
    const submitBt = getNameFromCommon(cmsCommon, 'SUBMIT');

    const isLogin = login && login.user && login.user.id;
    const dataFilter = searchValue ? _.filter(dataShow, x => x.name.toLowerCase().includes(searchValue)) : dataShow;

    const choosePerfumeHtml = (
      <div className="div-personaltity-instore">
        <div className="div-scroll-menu">
          <div style={{ height: '30px' }} />
          {
            _.map(listMenu, x => (
              <div className="div-item-menu-instore" onClick={() => this.selectDataShow(x)}>
                <img loading="lazy" src={x.image} alt={getAltImage(x.image)} />
                <span>
                  {x.name}
                </span>
                {
                  nameActive === x.name ? <div className="arrow-left" /> : (<div />)
                }
              </div>
            ))
          }
          <div style={{ height: '200px' }} />
        </div>
        <div className="div-content">
          <div className="div-header">
            <div className="div-title">
              <h1>
                {headerText}
              </h1>
              <span className={nameActive === 'CUSTOM PERFUME' ? '' : 'hidden'}>
                {textBlock.length > 0 ? textBlock[0].value : ''}
              </span>
            </div>
            <div className="div-search">
              <input type="text" placeholder="Search" onChange={this.onChangeSearch} />
              <img src={icSearch} alt="ic-search" />
            </div>
          </div>
          <div className="div-list-perfume">
            <Row>
              {
                _.map(dataFilter, x => (
                  <Col className="col-item" md={isBrowser ? '4' : '6'}>
                    <div
                      className={`div-item-perfume ${_.find(selections, d => d.id === x.id) ? 'active' : ''}`}
                      onClick={() => this.updateSelection(x)}
                    >
                      <div className="div-image">
                        <img className="img-scent" src={this.getImage(x)} alt="scent" />
                        <img className={_.find(selections, d => d.id === x.id) ? 'hover-active animated faster fadeIn' : 'hover-active animated faster fadeOut'} src={icChecked} alt="checked" />
                      </div>
                      <span>
                        {x.name}
                      </span>
                    </div>
                  </Col>
                ))
              }
              <Col md="12" style={{ height: '150px' }} />
            </Row>
          </div>
        </div>
        <div className={selections.length <= 0 ? 'hidden' : (isBrowser ? 'div-button-instore animated faster fadeInUp' : 'div-button-instore-table animated faster fadeInUp')}>
          <span>
            {selections.length}
            {' '}
            { selections.length > 1 ? 'items' : 'item' }
            {' '}
            in list
          </span>
          <div className="div-button">
            <button
              type="button"
              className="bt-instore inactive"
              onClick={() => this.setState({ selections: [] })
            }
            >
              {cancelBt}
            </button>
            {
              this.isAddToCartValid()
                ? (
                  <button type="button" className="bt-instore" onClick={this.onClickAddToCart}>
                    {addToCart}
                  </button>
                ) : (
                  <div className="bt-instore-temp" />
                )
            }
          </div>
        </div>
      </div>
    );
    const orderListHtml = (
      <div className="div-order-list" style={{ backgroundImage: `url(${imageBackground})` }}>
        <h3>
          {textBlock.length > 1 ? textBlock[1].value : ''}
        </h3>
        <div className="div-content list">
          {
            _.map(basket.items, x => (
              <ItemOrder
                idCart={basket.id}
                data={x}
                updateProductBasket={this.props.updateProductBasket}
                deleteProductBasket={this.props.deleteProductBasket}
                cmsCommon={cmsCommon}
              />
            ))
          }
        </div>
        <div className={isBrowser ? 'div-footer-order-browser' : 'div-footer-order'}>
          <div className="div-total">
            <span>
              {totalBt}
            </span>
            <span>
              <b>
                {generaCurrency(basket.total)}
              </b>
            </span>
          </div>
          <div className="div-button">
            <button type="button" className="bt-instore inactive" onClick={() => this.setState({ stepShow: 1 })}>
              {addMoreBt}
            </button>
            <button
              type="button"
              className="bt-instore"
              onClick={this.checkOut}
              disabled={basket.items.length === 0}
            >
              {finishBt}
            </button>
          </div>
        </div>
      </div>
    );

    const sendHtml = (
      <div className="div-send-email-instore" style={{ backgroundImage: `url(${imageBackground})` }}>
        <div className="div-button-back">
          <button type="button" onClick={() => { this.props.history.push(this.namePath === 'boutique-quiz' ? generateUrlWeb('/boutique-quiz') : generateUrlWeb(`/partnership-quiz/${this.slugPartnerShip}`)); }}>
            <img src={icUndo} alt="undo" />
            {takeTheQuizAgainBt}
          </button>
        </div>
        {
          !isBrowser ? (
            <div className="logo">
              <img src={logo} alt="logo" onClick={() => { this.props.history.push(this.namePath === 'boutique-quiz' ? generateUrlWeb('/boutique-quiz') : generateUrlWeb(`/partnership-quiz/${this.slugPartnerShip}`)); }} alt="logo" />
            </div>
          ) : (<div />)
        }

        <div className="div-title">
          <span>
            {textBlock && textBlock.length > 2 ? textBlock[2].value : ''}
          </span>
        </div>
        <div className="div-video">
          <video
            style={{
              objectFit: 'cover',
              height: '100%',
            }}
            muted
            playsinline="true"
            autoPlay
            loop
            preload="auto"
            src={videoBlock && videoBlock.value ? videoBlock.value.video : ''}
            poster={videoBlock && videoBlock.value ? videoBlock.value.placeholder : ''}
          />
        </div>
        <div className="send-email">
          <span className={isLogin ? 'hidden' : ''}>
            {textBlock.length > 3 ? textBlock[3].value : ''}
          </span>
          <div className="div-email">
            {
              isLogin
                ? (
                  <React.Fragment>
                    {/* <button type="button" onClick={this.onClickSendEmail} className="bt-send-email w-100">
                      {yesBt}
                    </button> */}
                    <button type="button" onClick={() => this.props.history.push(generateUrlWeb('/boutique-quiz'))} className="bt-send-email no-active w-100">
                      {takeTheQuizAgainBt}
                    </button>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <div className="div-infor">
                      <div className="div-input">
                        <input
                          type="text"
                          placeholder="Name"
                          name="name"
                          onChange={this.onChangeSubmit}
                          defaultValue={this.dataSubmit.name}
                        />
                      </div>
                      <div className="div-input">
                        {/* <input type='text' placeholder="Date of birth" /> */}
                        <InputMask
                          type="text"
                          name="dob"
                          placeholder="Date of birth"
                          mask="99/99/9999"
                          maskChar={null}
                          onChange={this.onChangeSubmit}
                          defaultValue={this.dataSubmit.dob}
                        />
                        <img src={icCalendar} alt="calendar" />
                      </div>
                    </div>
                    <button type="button" onClick={this.onClickSubmit} className="bt-send-email w-100">
                      {submitBt}
                    </button>
                  </React.Fragment>
                )
            }
          </div>
        </div>
        <div className="div-footer-email">
          <span>
            {textBlock.length > 4 ? textBlock[4].value : ''}
          </span>
        </div>
      </div>
    );
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
        </MetaTags>
        {
          this.namePath === 'partnership-quiz' ? <HeaderPartnership logo={partnership ? partnership.logo : ''} isSpecialMenu isDisableScrollShow isNotShowRegion /> : (<div />)
        }
        <div className={`div-body-product ${this.namePath === 'partnership-quiz' ? 'add-header' : ''}`}>
          {stepShow === 1 ? choosePerfumeHtml : (stepShow === 2 ? orderListHtml : sendHtml)}
        </div>
      </div>
    );
  }
}

PersonalityInstore.propTypes = {
  loadingPage: PropTypes.func.isRequired,
  login: PropTypes.shape().isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  createBasketGuestArray: PropTypes.func.isRequired,
  addProductBasketArray: PropTypes.func.isRequired,
  updateProductBasket: PropTypes.func.isRequired,
  deleteProductBasket: PropTypes.func.isRequired,
  basket: PropTypes.shape().isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      idOutCome: PropTypes.string,
    }),
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  location: PropTypes.shape({
    state: PropTypes.string,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    cms: state.cms,
    basket: state.basket,
  };
}

const mapDispatchToProps = {
  loadingPage,
  addCmsRedux,
  createBasketGuestArray,
  addProductBasketArray,
  updateProductBasket,
  deleteProductBasket,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PersonalityInstore));
