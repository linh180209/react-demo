import React, { Component } from 'react';
import HtmlParser from 'react-html-parser';
import { isBrowser, isTablet } from 'react-device-detect';
import { Modal, ModalBody } from 'reactstrap';
import classnames from 'classnames';
import '../../styles/popup-success-mix.scss';
import icClose from '../../image/icon/ic-close-region.svg';
import { getNameFromButtonBlock, addFontCustom } from '../../Redux/Helpers';

class PopUpMixSuccess extends Component {
  render() {
    const { buttonBlocks, imageTxtBlocks, isLandscape } = this.props;
    const CONGRATULATIONSBt = getNameFromButtonBlock(buttonBlocks, 'CONGRATULATIONS');
    const CONTINUTEBT = getNameFromButtonBlock(buttonBlocks, 'CONTINUTE');
    const youHaveBt = getNameFromButtonBlock(buttonBlocks, 'You_have_successfully');
    return (
      <Modal className={classnames('modal-full-screen', addFontCustom())} isOpen>
        <ModalBody>
          <div className="div-popup-success-mix">
            <div className="div-content">
              {
                isBrowser || (isTablet && isLandscape) ? (
                  <button type="button" className="button-bg__none bt-close" onClick={this.props.onClose}>
                    <img src={icClose} alt="icon" />
                  </button>
                ) : (null)
              }

              <div className="div-content-popup">
                <h3>
                  {CONGRATULATIONSBt}
                </h3>
                <img loading="lazy" src={imageTxtBlocks ? imageTxtBlocks.image.image : ''} alt="gift" />
                <div className="div-info-text">
                  <span className="header1">
                    {youHaveBt}
                  </span>
                  <div className="description">
                    {HtmlParser(imageTxtBlocks ? imageTxtBlocks.description : '')}
                  </div>
                  <button type="button" onClick={this.props.onClose}>
                    {CONTINUTEBT}
                  </button>
                </div>
              </div>
            </div>

          </div>
        </ModalBody>
      </Modal>
    );
  }
}

export default PopUpMixSuccess;
