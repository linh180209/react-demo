import React, { Component } from 'react';
import _ from 'lodash';
import info from '../../image/icon/icNewInfo.svg';
import icDelete from '../../image/icon/delete-red.svg';
import ProgressLine from '../ResultScentV2/progressLine';
import ProgressCircle from '../ResultScentV2/progressCircle';
import { getNameFromButtonBlock } from '../../Redux/Helpers';

class ItemIngredientCustom extends Component {
  render() {
    const {
      className, data, title, onClick, iconBottle, buttonBlocks, no,
    } = this.props;

    const strengthBt = getNameFromButtonBlock(buttonBlocks, 'STRENGTH');
    const durationBt = getNameFromButtonBlock(buttonBlocks, 'duration');
    return (
      <div className={`item-ingredient-custome ${className}`}>
        {
          !_.isEmpty(data) ? (
            <div className="div-profile-info">
              <span className="no">
                {no}
              </span>
              <button
                onClick={this.props.onDelete}
                type="button"
                className="button-bg__none bt-delete"
              >
                <img src={icDelete} alt="delete" />
              </button>
              <div className="div-image-ingredient">
                <img className="img-ingredient" src={data.image} alt="ingredient" onClick={onClick} />
                <span>
                  {data.ingredient.title}
                </span>
                <button type="button" onClick={() => this.props.onClickIngredient(data.ingredient)}>
                  <img src={info} alt="icon" />
                </button>
              </div>
              <div className="profile-info">
                <div className="div-process-circle">
                  <ProgressCircle title={strengthBt} percent={data.product.profile ? parseInt(parseFloat(data.product.profile.strength) * 100, 10) : 0} />
                  <ProgressCircle title={durationBt} percent={data.product.profile ? parseInt(parseFloat(data.product.profile.duration) * 100, 10) : 0} />
                </div>
                <div className="div-process-line">
                  {
                    _.map(data.product.profile ? data.product.profile.accords : [], x => (
                      <ProgressLine data={x} isShowPercent />
                    ))
                  }
                </div>
              </div>
            </div>
          ) : (
            <div className="div-bottle" onClick={onClick}>
              <img src={iconBottle} alt="bottle" />
              <span>
                {title}
              </span>
            </div>
          )
        }
      </div>
    );
  }
}

export default ItemIngredientCustom;
