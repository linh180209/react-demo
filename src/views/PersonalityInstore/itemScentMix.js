import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import ItemImageScent from '../ResultScentV2/itemImageScent';
import { getNameFromButtonBlock } from '../../Redux/Helpers';
import icEdit from '../../image/icon/icEditMix.svg';
import icEditDone from '../../image/icon/icEditDone.svg';
import icDelete from '../../image/icon/ic-delete-boutique.svg';
import icNext from '../../image/icon/ic-next-mix.svg';
import { toastrError } from '../../Redux/Helpers/notification';
import ItemImageScentMix from '../ResultScentV2/itemImageScentMix';

class ItemScentMix extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenEdit: false,
    };
  }

  onClickAddNote = () => {
    if (!this.note) {
      toastrError('Please enter your note');
      return;
    }
    this.setState({ isOpenEdit: false });
    this.updateNote();
  }

  updateNote = () => {
    const { data, orginalData } = this.props;
    _.assign(orginalData, { note: this.note });
    _.assign(data, { note: this.note });
    this.props.updateProductMix();
  }

  onChangeNote = (e) => {
    const { value } = e.target;
    this.note = value;
    if (this.timer) {
      clearTimeout(this.timer);
    }
    this.timer = setTimeout(() => {
      this.updateNote();
    }, 1000);
  }

  render() {
    const {
      data, index, cmsCommon, onClickIngredient, buttonBlocks, onClickEdit, onDeletePerfume,
      isMobileProps,
    } = this.props;
    const noteBt = getNameFromButtonBlock(buttonBlocks, 'NOTE');
    const addYouNoteBt = getNameFromButtonBlock(buttonBlocks, 'Add_you_note');
    const { isOpenEdit } = this.state;
    const web = (
      <div className="div-wrap-mix">
        <div className="div-item-mix">
          <div className="div-content">
            <h4>
              {`${index + 1}${index === 0 ? 'ST' : index === 1 ? 'ND' : index === 2 ? 'RD' : 'TH'} ${getNameFromButtonBlock(this.props.buttonBlocks, 'CREATION')}`}
            </h4>
            <div className={classnames('div-image-scent', 'multiple-scent')}>
              {
                data.listScent.length < 3 ? (
                  <ItemImageScent
                    data={data}
                    cmsCommon={cmsCommon}
                    onClickImage={() => {}}
                    onClickIngredient={onClickIngredient}
                  />
                ) : <ItemImageScentMix data={data} cmsCommon={cmsCommon} />
              }

            </div>
            <button type="button" className="bt-editmix" onClick={() => onClickEdit(data.item.id)}>
              {getNameFromButtonBlock(buttonBlocks, 'EDIT_MIX')}
            </button>
          </div>
          <button
            type="button"
            className="button-bg__none bt-edit"
            onClick={() => this.setState({ isOpenEdit: true })}
          >
            <img src={icEdit} alt="delete" />
          </button>
          <button type="button" className="button-bg__none bt-delete" onClick={() => onDeletePerfume(data.item.id)}>
            <img src={icDelete} alt="delete" />
          </button>
        </div>
        {
          isOpenEdit && (
            <div className="div-note animated faster fadeInLeft">
              <h4>
                {noteBt}
                :
              </h4>
              <textarea defaultValue={data.note} placeholder={addYouNoteBt} onChange={this.onChangeNote} />
              <button type="button" className="bt-edit-done button-bg__none" onClick={this.onClickAddNote}>
                <img src={icEditDone} alt="edit" />
              </button>
            </div>
          )
        }
      </div>
    );
    const mobile = (
      <div className="div-wrap-mix-mobile">
        <div className={`div-item-mix-mobile ${isOpenEdit ? 'open-edit' : ''} ${data.listScent.length > 2 ? 'multiple-scent' : ''}`}>
          <div className="div-left">
            <div className="name">
              <span>
                {`${index + 1}${index === 0 ? 'ST' : index === 1 ? 'ND' : index === 2 ? 'RD' : 'TH'}`}
              </span>
              <span>
                {getNameFromButtonBlock(buttonBlocks, 'CREATION')}
              </span>
            </div>
            <div className="div-bt">
              <button
                type="button"
                className="button-bg__none bt-edit-mobile"
                onClick={() => this.setState({ isOpenEdit: true })}
              >
                <img src={icEdit} alt="mix" />
              </button>
              <button type="button" className="button-bg__none bt-delete-mobile" onClick={() => onDeletePerfume(data.item.id)}>
                <img src={icDelete} alt="delete" />
              </button>
            </div>

          </div>
          <div className="div-mid">
            {
                data.listScent.length < 3 ? (
                  <ItemImageScent
                    data={data}
                    cmsCommon={cmsCommon}
                    onClickImage={() => {}}
                    onClickIngredient={onClickIngredient}
                  />
                ) : <ItemImageScentMix data={data} cmsCommon={cmsCommon} />
              }
            {/* <ItemImageScent
              data={data}
              cmsCommon={cmsCommon}
              onClickImage={() => {}}
              onClickIngredient={onClickIngredient}
            /> */}
          </div>
          <div className="div-right" onClick={() => onClickEdit(data.item.id)}>
            <button type="button" className="button-bg__none">
              <img src={icNext} alt="mix" />
            </button>
          </div>
        </div>
        {
          isOpenEdit && (
            <div className="div-note animated faster fadeInDown">
              <h4>
                {noteBt}
                :
              </h4>
              <input defaultValue={data.note} placeholder={addYouNoteBt} onChange={this.onChangeNote} />
              <button type="button" className="bt-edit-done button-bg__none" onClick={this.onClickAddNote}>
                <img src={icEditDone} alt="edit" />
              </button>
            </div>
          )
        }
      </div>

    );
    return (
      <React.Fragment>
        {isMobileProps ? mobile : web}
      </React.Fragment>
    );
  }
}

ItemScentMix.defaultProps = {
  onClickEdit: () => {},
};

ItemScentMix.Prototype = {
  onClickEdit: PropTypes.func,
};

export default ItemScentMix;
