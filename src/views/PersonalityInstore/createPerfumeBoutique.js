import React, { Component } from 'react';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import _ from 'lodash';
import { MetaTags } from 'react-meta-tags';
import '../../styles/create-perfume-boutique.scss';
import CreateCustomMix from './createCustomMix';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import CutomMixes from './customMixes';
import { GET_ALL_SCENTS_PRODUCTS, PUT_OUTCOME_URL, GET_PRODUCT_FOR_CART } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import loadingPage from '../../Redux/Actions/loading';
import addScentsToStore from '../../Redux/Actions/scents';
import addCmsRedux from '../../Redux/Actions/cms';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import {
  getCmsCommon, fetchCMSHomepage, getSEOFromCms, getNameFromButtonBlock, setPrerenderReady,
} from '../../Redux/Helpers';
import icSearchBlack from '../../image/icon/search-product-black.svg';
import PopUpExit from './popupExit';

function getPathNamePage(path) {
  if (path.includes('/boutique-quiz')) {
    return 'boutique-quiz';
  } if (path.includes('/partnership-quiz')) {
    return 'partnership-quiz';
  } if (path.includes('/krisshop')) {
    return 'krisshop';
  } if (path.includes('/quiz')) {
    return 'quiz';
  } if (path.includes('/sephora')) {
    return 'sephora';
  } if (path.includes('/sofitel')) {
    return 'sofitel';
  } if (path.includes('/bmw')) {
    return 'bmw';
  }
  return null;
}

const preDataGroup = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { tags } = x;
    _.forEach(tags, (tag) => {
      const group = _.find(dataScents, d => d.tag === tag.name);
      if (group) {
        group.datas.push(x);
      } else {
        dataScents.push({
          tag: tag.name,
          group: tag.group,
          datas: [x],
        });
      }
    });
  });
  return dataScents;
};

const preDataAlphabet = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { name } = x;
    const firstChart = name.charAt(0);
    const group = _.find(dataScents, d => d.tag.toLowerCase() === firstChart.toLowerCase());
    if (group) {
      group.datas.push(x);
    } else {
      dataScents.push({
        tag: firstChart,
        datas: [x],
      });
    }
  });
  return dataScents;
};

const preDataScents = (scents) => {
  const dataScents = [];

  dataScents.push({
    datas: _.orderBy(preDataGroup(scents), ['tag'], ['asc']),
    title: ['Scent Family', 'Gender', 'mood'],
  });

  dataScents.push({
    datas: _.orderBy(preDataAlphabet(scents), ['tag'], ['asc']),
    title: ['Alphabet'],
  });

  dataScents.push({
    datas: [{
      tag: '',
      datas: _.orderBy(scents, ['popularity'], ['desc']),
    }],
    title: ['Popularity'],
  });
  console.log('dataScents', dataScents);
  return dataScents;
};

const getCms = (cms) => {
  if (cms) {
    const seo = getSEOFromCms(cms);
    const { body } = cms;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    const imageTxtBlocks = _.find(body, x => x.type === 'imagetxt_block').value;
    return {
      buttonBlocks,
      seo,
      imageTxtBlocks,
    };
  }
  return {
    buttonBlocks: [],
    seo: undefined,
    imageTxtBlocks: undefined,
  };
};

class CreatePerfumeBoutique extends Component {
  constructor(props) {
    super(props);
    const { email, perfumes } = this.props.location.state ? this.props.location.state : {};
    this.state = {
      listPerfume: [],
      isShowAddCombo: false,
      dataScents: [],
      buttonBlocks: [],
      seo: undefined,
      scentEdit: undefined,
      imageTxtBlocks: undefined,
      isShowPopUpSearchMobile: false,
      isShowPopupFiltermobile: false,
      isShowExit: false,
      isSubmited: false,
      email,
      perfumes,
    };
    const { match } = props;
    this.outComeId = match && match.params ? match.params.idOutCome : undefined;
    this.valueFilters = [
      {
        title: 'Family',
        value: 'Scent Family',
      },
      {
        title: 'Popularity',
        value: 'Popularity',
      },
      {
        title: 'Alphabet (A-Z)',
        value: 'Alphabet',
      },
      {
        title: 'Mood',
        value: 'Mood',
      },
    ];
    this.refInput = React.createRef();
    this.namePath = getPathNamePage(window.location.pathname);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const returnObject = {};
    const { scents } = nextProps;
    if (scents !== prevState.scents) {
      const dataScents = preDataScents(scents);
      _.assign(returnObject, { scents, dataScents });
    }
    return !_.isEmpty(returnObject) ? returnObject : null;
  }

  componentDidMount() {
    setPrerenderReady();
    this.fetchAllScent();
    this.fetchCMSPage();
    this.fetchPerfumeFromResult();
  }

  fetchPerfumeFromResult = () => {
    const { perfumes, listPerfume } = this.state;
    if (!perfumes || perfumes.length !== 2) {
      return;
    }
    const scents = {
      listScent: [perfumes[0], perfumes[1]],
      combo: {},
    };
    this.getProductCombo(scents).then((comboData) => {
      _.assign(scents, { combo: comboData });
      listPerfume.push(scents);
      this.setState({ listPerfume }, () => {
        this.updateProductMix();
      });
    });
  }

  fetchAllScent = () => {
    const { scents } = this.props;
    if (scents && scents.length > 0) {
      return;
    }
    const option = {
      url: GET_ALL_SCENTS_PRODUCTS,
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      this.props.loadingPage(false);
      if (result && !result.isError) {
        this.props.addScentsToStore(result);
        return;
      }
      throw new Error(result.message);
    }).catch((error) => {
      this.props.loadingPage(false);
      toastrError(error.message);
    });
  }

  fetchCMSPage = async () => {
    const { cms } = this.props;
    const customizeBottle = _.find(cms, x => x.title === 'Custom Mix');
    if (!customizeBottle) {
      const cmsData = await fetchCMSHomepage('custom-mix');
      this.props.addCmsRedux(cmsData);
      const dataCms = getCms(cmsData);
      this.setState(dataCms);
    } else {
      const dataCms = getCms(customizeBottle);
      this.setState(dataCms);
    }
  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  onClickAddCombo = (data, isUpdate) => {
    const { listPerfume } = this.state;
    if (!isUpdate) {
      listPerfume.push(data);
    }
    this.setState({ listPerfume, isShowAddCombo: false });
    this.updateProductMix();
  }

  onClickCreateCombo = () => {
    this.setState({ isShowAddCombo: true, scentEdit: undefined });
  }

  gotoCustomMix = () => {
    if (!this.state.isShowExit) {
      this.onChangeExit();
      return;
    }
    // this.props.history.push(`/boutique-quiz/outcome/${this.outComeId}`);
    this.props.history.push(window.location.pathname.replace('create-perfume', 'outcome'));
  }

  getProductCombo = async (scents) => {
    const option = {
      url: scents.listScent[0].id === scents.listScent[1].id ? `${GET_PRODUCT_FOR_CART}?combo=${scents.listScent[0].id}&type=scent` : `${GET_PRODUCT_FOR_CART}?combo=${scents.listScent[0].id}${`,${scents.listScent[1].id}`}&type=perfume`,
      method: 'GET',
    };
    return fetchClient(option);
  }

  goBackCreateMix = () => {
    const { listPerfume } = this.state;
    if (!listPerfume || listPerfume.length === 0) {
      this.gotoCustomMix();
    } else {
      this.setState({ isShowAddCombo: false });
    }
  }

  updateProductMix = () => {
    const { listPerfume } = this.state;
    // const products = _.map(listPerfume, x => ({ product: x.combo.id, note: x.note }));
    const products = _.map(_.filter(listPerfume, d => _.filter(d.listScent, f => !_.isEmpty(f)).length <= 2), x => ({ product: x.combo.id, note: x.note }));
    const scentsFinal = _.map(_.filter(listPerfume, d => _.filter(d.listScent, f => !_.isEmpty(f)).length > 2), x => ({ scents: _.map(_.filter(x.listScent, k => !_.isEmpty(k)), j => j.id), note: x.note }));
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', this.outComeId),
      method: 'PUT',
      body: {
        products_final: products,
        scents_final: scentsFinal,
        send_email: true,
        is_boutique: true,
      },
    };
    fetchClient(options);
  }

  editCutomMix = (data) => {
    if (_.isEmpty(data)) {
      return;
    }
    this.setState({ isShowAddCombo: true, scentEdit: data });
  }

  onChangeFilter = (value) => {
    this.setState({ isShowPopupFiltermobile: value, isShowPopUpSearchMobile: false });
  }

  onChangeSearch = (value) => {
    this.setState({ isShowPopupFiltermobile: false, isShowPopUpSearchMobile: value });
    if (value) {
      setTimeout(() => {
        this.refInput.current.focus();
      }, 500);
    }
  }

  onChangeValueSearch = () => {
    this.refFilterSearchScent.current.onChangeValueSearchFilter({ value: this.valueInputSearch, type: 'Search' });
    this.setState({ isShowPopUpSearchMobile: false });
  }

  shareRefFilterSearchScent = (ref) => {
    this.refFilterSearchScent = ref;
  }

  onChangeValueFilter = (value) => {
    this.refFilterSearchScent.current.onChangeValueSearchFilter({ value, type: 'Filter' });
    this.setState({ isShowPopupFiltermobile: false });
  }

  onChangeInputSearch = (e) => {
    const { value } = e.target;
    this.valueInputSearch = value;
  }

  onChangeExit = () => {
    const { isShowExit } = this.state;
    this.setState({ isShowExit: !isShowExit });
  }

  render() {
    const { cms } = this.props;
    const {
      listPerfume, dataScents, isShowIngredient, ingredientDetail, isShowAddCombo,
      seo, buttonBlocks, scentEdit, imageTxtBlocks, isShowPopUpSearchMobile, isShowPopupFiltermobile,
      isShowExit,
    } = this.state;
    const cmsCommon = getCmsCommon(cms);
    const searchBt = getNameFromButtonBlock(buttonBlocks, 'SEARCH');
    console.log('listDislike', this.state.listDislike);
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
        </MetaTags>
        {/* {!['sofitel'].includes(this.namePath) && <HeaderHomePage visible={false} isSpecialMenu isDisableScrollShow isNotShowRegion />} */}
        {!['sofitel'].includes(this.namePath) && <HeaderHomePageV3 isRemoveMessage />}
        <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
          className="min-top-height"
        />
        <div className="div-create-perfume-boutique">
          {isShowExit && <PopUpExit buttonBlocks={buttonBlocks} onClose={this.onChangeExit} onExit={this.gotoCustomMix} />}
          {
            isShowAddCombo ? (
              <CreateCustomMix
                listPerfume={listPerfume}
                dataScents={dataScents}
                onClickIngredient={this.onClickIngredient}
                onClickAddCombo={this.onClickAddCombo}
                onGoBack={() => this.setState({ isShowAddCombo: false })}
                buttonBlocks={buttonBlocks}
                scentEdit={scentEdit}
                onChangeFilter={this.onChangeFilter}
                onChangeSearch={this.onChangeSearch}
                shareRefFilterSearchScent={this.shareRefFilterSearchScent}
                namePath={this.namePath}
              />
            ) : (
              <CutomMixes
                listPerfume={listPerfume}
                onClickCreateCombo={this.onClickCreateCombo}
                cmsCommon={cmsCommon}
                onClickIngredient={this.onClickIngredient}
                gotoCustomMix={this.gotoCustomMix}
                updateProductMix={this.updateProductMix}
                idOutCome={this.outComeId}
                buttonBlocks={buttonBlocks}
                editCutomMix={this.editCutomMix}
                imageTxtBlocks={imageTxtBlocks}
                email={this.state.email}
                isSubmited={this.state.isSubmited}
                setSubmited={() => this.setState({ isSubmited: true })}
                namePath={this.namePath}
              />
            )
          }
        </div>

        {/* can't use on Filter Search Scent */}
        <div
          className={isShowPopUpSearchMobile || isShowPopupFiltermobile ? 'div-popup-filter-search-mobile animated faster fadeIn' : 'hidden'}
        >
          <div className="bg-close" onClick={() => this.setState({ isShowPopUpSearchMobile: false, isShowPopupFiltermobile: false })} />
          <div className={isShowPopupFiltermobile ? 'div-popup-filter' : 'hidden'}>
            {
              _.map(this.valueFilters, d => (
                <button
                  type="button"
                  onClick={() => {
                    this.onChangeValueFilter(d.value);
                  }}
                >
                  {d.title}
                </button>
              ))
            }
          </div>
          {
            isShowPopUpSearchMobile
              ? (
                <div className="div-popup-search">
                  <div className="div-input">
                    <img src={icSearchBlack} alt="search" />
                    <input onChange={this.onChangeInputSearch} ref={this.refInput} type="text" />
                  </div>
                  <button type="button" onClick={this.onChangeValueSearch}>
                    {searchBt}
                  </button>
                </div>
              ) : (null)
          }

        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
    scents: state.scents,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  loadingPage,
  addScentsToStore,
};

export default connect(mapStateToProps, mapDispatchToProps)(CreatePerfumeBoutique);
