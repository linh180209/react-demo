import React, { Component } from 'react';
import icClose from '../../image/icon/ic-close-region.svg';
import { getNameFromButtonBlock } from '../../Redux/Helpers';

export default class PopUpExit extends Component {
  onClickExit = () => {
    this.props.onExit();
  }

  render() {
    const { buttonBlocks, imageTxtBlocks } = this.props;
    const quizBt = getNameFromButtonBlock(buttonBlocks, 'REDO_QUIZ');
    const areYouSureBt = getNameFromButtonBlock(buttonBlocks, 'are_you_sure');
    const thisWillBt = getNameFromButtonBlock(buttonBlocks, 'This_will_erase');
    const saveYourBt = getNameFromButtonBlock(buttonBlocks, 'Save_your_mix');
    const yesBt = getNameFromButtonBlock(buttonBlocks, 'yes_sure');
    const noBt = getNameFromButtonBlock(buttonBlocks, 'no_redo');
    return (
      <div className="div-popup-success popup-exit">
        <div className="div-content">

          <button type="button" className="button-bg__none bt-close" onClick={this.props.onClose}>
            <img src={icClose} alt="icon" />
          </button>


          <div className="div-content-popup">
            <h3>
              {quizBt}
            </h3>
            <div className="div-info-text-exit">
              <span>
                {areYouSureBt}
              </span>
              <span>
                {thisWillBt}
              </span>
              <span>
                {saveYourBt}
              </span>
            </div>
            <div className="bt-exit">
              <button type="button" onClick={this.onClickExit}>
                {yesBt}
              </button>
              <button type="button" onClick={this.props.onClose}>
                {noBt}
              </button>
            </div>
          </div>
        </div>

      </div>
    );
  }
}
