/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { getNameFromCommon } from '../../Redux/Helpers';

class ItemOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: this.props.data.quantity,
      oldQuantity: this.props.data.quantity,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { data } = nextProps;
    const objectReturn = {};
    if (data && data.quantity !== prevState.oldQuantity) {
      _.assign(objectReturn, { quantity: data.quantity, oldQuantity: data.quantity });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  onChangeQuantity = (value) => {
    const { idCart } = this.props;
    const data = _.assign({}, this.props.data);
    data.quantity = parseFloat(value, 10);
    if (this.props.updateProductBasket) {
      this.props.updateProductBasket({
        idCart,
        item: data,
      });
    }
    this.setState({ quantity: value });
  }

  onIncreateQuantity = () => {
    const { quantity } = this.state;
    this.onChangeQuantity(quantity + 1);
  }

  onDecreateQuantity = () => {
    const { quantity } = this.state;
    if (quantity > 1) {
      this.onChangeQuantity(quantity - 1);
    }
  }

  onClickDelete = () => {
    if (this.props.deleteProductBasket) {
      const { idCart, data } = this.props;
      this.props.deleteProductBasket({
        idCart,
        item: data,
      });
    }
  }

  render() {
    const { data, cmsCommon } = this.props;
    console.log('data', data);
    const { item, combo } = data;
    const { quantity } = this.state;
    const products = _.filter(combo, x => x.product.type === 'Scent');
    const removeBt = getNameFromCommon(cmsCommon, 'Remove');
    const editBt = getNameFromCommon(cmsCommon, 'EDIT');
    const customBt = getNameFromCommon(cmsCommon, 'MY_PERFUME_CREATION');
    return (
      <div className="item-order">
        <div className="div-body header">
          <div className="body body-1">
            <span className="name">
              {item.product.type && item.product.type.name === 'Perfume' ? (customBt ? customBt.toUpperCase() : '') : (item ? item.product.type.alt_name : '')}
              {/* {customBt ? customBt.toUpperCase() : ''} */}
            </span>
          </div>
          <div className="body body-2">
            <span className="number">
              x
              {quantity}
            </span>
          </div>
          <div className="body body-3" />
        </div>
        {
          _.map(products, x => (
            <div className="div-body padding-item">
              <div className="body body-1">
                <span className="name">
                  {x.name}
                </span>
              </div>
              <div className="body body-2">
                <span className="number">
                  x
                  {quantity}
                </span>
              </div>
              <div className="body body-3" />
            </div>
          ))
        }

        <div className="div-body padding-button">
          <div className="body body-1">
            <button type="button" className="remove" onClick={this.onClickDelete}>
              {removeBt}
            </button>
          </div>
          <div className="body body-2">
            {
              quantity > 1
                ? (
                  <button type="button" className="edit-total" onClick={this.onDecreateQuantity}>
                  -
                  </button>
                ) : (
                  <div className="edit-total-temp" />
                )
            }

            <span className="text-number">
              {quantity}
            </span>
            <button type="button" className="edit-total" onClick={this.onIncreateQuantity}>
              +
            </button>
          </div>
          <div className="body body-3">
            <button type="button" className="edit hidden">
              {editBt}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

ItemOrder.propTypes = {
  data: PropTypes.shape().isRequired,
  cmsCommon: PropTypes.shape().isRequired,
  deleteProductBasket: PropTypes.func.isRequired,
  idCart: PropTypes.number.isRequired,
  updateProductBasket: PropTypes.func.isRequired,
};

export default ItemOrder;
