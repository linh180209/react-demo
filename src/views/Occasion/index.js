import React, { Component } from 'react';
import _ from 'lodash';


import logo from '../../image/icon/logo-menu.png';
import OccasionImage from '../../components/OccasionImage';
import { googleAnalitycs } from '../../Redux/Helpers';

class Occasion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [{
        url: 'https://media.giphy.com/media/lp0UDSUFeX16HwQcmY/giphy.gif',
        type: 'Romantics',
      },
      {
        url: 'https://media.giphy.com/media/pjV7UmXqiQK6IPrAX9/giphy.gif',
        type: 'Party',
      },
      {
        url: 'https://media.giphy.com/media/FR9nGyxk8Z5HG/giphy.gif',
        type: 'Seduction',
      },
      {
        url: 'https://media.giphy.com/media/q4bNfKEvgEO5y/giphy.gif',
        type: 'Outstanding',
      }],
    };
  }

  componentDidMount = () => {
    googleAnalitycs('/occasion');
  };

  render() {
    const { images } = this.state;
    return (
      <div className="container--occasion">
        <div className="container--occasion__logo">
          <img src={logo} alt="logo" />
        </div>
        <div className="container container--occasion__image text-center">
          <span className="text--header mt-4">
            Choose your perfum based on your mood
          </span>
          <div className="mt-4 container--occasion__image-item">
            {
              _.map(images, data => (
                <OccasionImage data={data} />
              ))
            }
          </div>

        </div>
        <div className="container--occasion__right" />
      </div>
    );
  }
}

export default Occasion;
