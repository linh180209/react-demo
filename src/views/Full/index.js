/* eslint-disable no-return-assign */
import React, { Component } from 'react';
import {
  Switch, Route, Redirect,
} from 'react-router-dom';
// import SelectionPerfume from '../SelectionPerfume';
// import ResultNew from '../ResultScent/resultNew';
// import ProductAllV3 from '../../views2/productAllV3';
// import CheckOutV2 from '../../views2/checkOutV2';
import { setUTMCookies } from '../../Redux/Helpers';

const ProductAllV3 = React.lazy(() => import('../../views2/productAllV3'));
const CheckOutV2 = React.lazy(() => import('../../views2/checkOutV2'));
const ResultNew = React.lazy(() => import('../ResultScent/resultNew'));
const ProductDetailV2 = React.lazy(() => import('../../views2/productDetailV2'));
const PointRedeem = React.lazy(() => import('../points'));
const LetsGuideYou = React.lazy(() => import('../LetsGuideYou'));
const QuizV3 = React.lazy(() => import('../Quiz'));
// const Mood = React.lazy(() => import('../Mood'));
// const MMO = React.lazy(() => import('../MMO'));
const AccountB2CPage = React.lazy(() => import('../AccountB2C/AccountB2CPage'));
const ProductB2C = React.lazy(() => import('../ProductB2C'));
// const CheckOutB2C = React.lazy(() => import('../CheckOutB2C'));
// const CheckOutClub = React.lazy(() => import('../CheckOutB2C/checkOutClub'));
const CheckOutGift = React.lazy(() => import('../CheckOutB2C/checkOutGift'));
// const CheckOutSubQuiz = React.lazy(() => import('../CheckOutB2C/checkOutSubQuiz'));
// const PaySuccess = React.lazy(() => import('../CheckOutB2C/paySuccess'));
const PaySuccessV2 = React.lazy(() => import('../../views2/paySuccess'));
// const FAQPage = React.lazy(() => import('../FAQ'));
// const TermsConditions = React.lazy(() => import('../Terms&Conditions'));
// const AboutUs = React.lazy(() => import('../AboutUs'));
// const ShippingInformation = React.lazy(() => import('../ShippingInformation'));
// const OurCommitment = React.lazy(() => import('../OurCommitment'));
// const OurProducts = React.lazy(() => import('../OurProducts'));
// const GiftPage = React.lazy(() => import('../Gift'));
const GiftPageV2 = React.lazy(() => import('../../views2/GiftV2'));
const GiftBundles = React.lazy(() => import('../../views2/GiftV2/giftBundles'));
const GiftBespoke = React.lazy(() => import('../../views2/GiftV2/giftBespoke'));
const GiftVoucher = React.lazy(() => import('../../views2/GiftV2/giftVoucher'));
const GiftWorkshop = React.lazy(() => import('../../views2/GiftV2/giftWorkshop'));
// const Influencer = React.lazy(() => import('../Influencer'));
// const InfluencerDetailNew = React.lazy(() => import('../Influencer/influencerDetailNew'));
const FunnelV2 = React.lazy(() => import('../Funnel/funnelV2'));
// const Funnel = React.lazy(() => import('../Funnel'));
const FunnelV3 = React.lazy(() => import('../../views2/funnelV3'));
// const HomeAlt = React.lazy(() => import('../HomeAlt'));
// const AddressStore = React.lazy(() => import('../AddressStore'));
// const ResultNew = React.lazy(() => import('../ResultScent/resultNew'));
const PersonalityInstore = React.lazy(() => import('../PersonalityInstore'));
const SelectionPerfume = React.lazy(() => import('../SelectionPerfume'));
const PerfumeCombination = React.lazy(() => import('../SelectionPerfume/perfumeCombination'));
// const Workshop = React.lazy(() => import('../Workshop'));
// const WorkshopV2 = React.lazy(() => import('../WorkshopV2'));
// const WorkshopAll = React.lazy(() => import('../WorkshopAll'));
const BookWorkshop = React.lazy(() => import('../BookWorkshop'));
// const Review = React.lazy(() => import('../Review'));
const Club21G = React.lazy(() => import('../Club21G'));
const ProductAllV2 = React.lazy(() => import('../ProductAllV2'));
const WriteReview = React.lazy(() => import('../WriteReview'));
const GiftReceiverPage = React.lazy(() => import('../GiftReceiverPage'));
const MmoGift = React.lazy(() => import('../MMOGift'));
const CreatePerfumeBoutique = React.lazy(() => import('../PersonalityInstore/createPerfumeBoutique'));
const Partnership = React.lazy(() => import('../Partnership'));
const MMO2 = React.lazy(() => import('../MMO2'));
// const Blogs = React.lazy(() => import('../Blogs'));
// const ArticleBlog = React.lazy(() => import('../ArticleBlog'));
// const CategoriesBlog = React.lazy(() => import('../CategoriesBlog'));
// const SearchResultBlog = React.lazy(() => import('../SearchResultBlog'));
// const LandingQuiz = React.lazy(() => import('../LetsGuideYou/landingQuiz'));
const LandingQuizV3 = React.lazy(() => import('../Quiz/LandingQuiz/landingQuizPage'));
const Glossaire = React.lazy(() => import('../Glossaire'));
const Glossary = React.lazy(() => import('../../views2/glossaryV2'));
// const HomeQuiz = React.lazy(() => import('../HomeQuiz'));
// const SubQuiz = React.lazy(() => import('../SubQuiz'));
// const ResultSubQuiz = React.lazy(() => import('../SubQuiz/resultSubQuiz'));
// const OurBoutique = React.lazy(() => import('../OurBoutique'));
// const HomeAltV2 = React.lazy(() => import('../HomeAltV2'));
// const AboutUsV3 = React.lazy(() => import('../AboutUs/aboutUsV3'));
// const OurTeam = React.lazy(() => import('../OurTeam'));
const StatusPaymentBookingWorkshop = React.lazy(() => import('../StatusPaymentBookingWorkshop'));
const NotFound = React.lazy(() => import('../NotFound/index'));
const AuthPage = React.lazy(() => import('../../views2/authPage'));
const ForgotPassword = React.lazy(() => import('../../views2/authPage/forgotPassword'));
const Register = React.lazy(() => import('../../views2/authPage/register'));
const CardPage = React.lazy(() => import('../../views2/cardPage'));
const Quizv4 = React.lazy(() => import('../../views2/quiz'));
const GiftBundlesDetail = React.lazy(() => import('../../views2/GiftV2/giftBundlesDetail'));
const QuizFerrari = React.lazy(() => import('../../views2/quizFerrari'));
const QuizResultFerrari = React.lazy(() => import('../../views2/quizResultFerrari'));
// const QuizResult = React.lazy(() => import('../../views2/quizResult'));
// const TestComponent = React.lazy(() => import('../../views2/testComponent'));

class Full extends Component {
  componentDidMount() {
    setUTMCookies();
  }

  render() {
    return (
      <div className="app">
        <div className="app-body">
          <main className="main">
            <div id="id-container-full" className="container_full">
              <Switch>
                <Redirect from="/us/*" to="/*" />
                <Redirect from="/us" to="/" />
                <Redirect exact from="/" to="/all-products" />
                <Redirect exact from="/perfume-DIY" to="/all-products" />
                <Redirect exact from="/:language/perfume-DIY" to="/all-products" />
                <Redirect exact from="/sg" to="/sg/all-products" />
                <Redirect exact from="/au" to="/au/all-products" />
                <Redirect exact from="/us" to="/us/all-products" />
                <Redirect exact from="/gb" to="/gb/all-products" />
                <Redirect exact from="/de" to="/de/all-products" />
                <Redirect exact from="/ie" to="/ie/all-products" />
                <Redirect exact from="/it" to="/it/all-products" />
                <Redirect exact from="/lu" to="/lu/all-products" />
                <Redirect exact from="/mc" to="/mc/all-products" />
                <Redirect exact from="/nl" to="/nl/all-products" />
                <Redirect exact from="/pt" to="/pt/all-products" />
                <Redirect exact from="/my" to="/my/all-products" />
                <Redirect exact from="/th" to="/th/all-products" />
                <Redirect exact from="/in" to="/in/all-products" />
                <Redirect exact from="/hk" to="/hk/all-products" />
                <Redirect exact from="/no" to="/no/all-products" />
                <Redirect exact from="/dk" to="/dk/all-products" />
                <Redirect exact from="/jp" to="/jp/all-products" />
                <Redirect exact from="/ae" to="/ae/all-products" />
                <Redirect exact from="/be" to="/be/all-products" />
                <Redirect exact from="/se" to="/se/all-products" />
                <Redirect exact from="/ch" to="/ch/all-products" />
                <Redirect exact from="/ph" to="/ph/all-products" />
                <Redirect exact from="/pk" to="/pk/all-products" />
                <Redirect exact from="/id" to="/id/all-products" />
                <Redirect exact from="/nz" to="/nz/all-products" />
                <Redirect exact from="/ca" to="/ca/all-products" />
                <Redirect exact from="/kr" to="/kr/all-products" />
                <Redirect exact from="/fr" to="/fr/all-products" />
                <Redirect exact from="/ru" to="/ru/all-products" />
                <Redirect exact from="/vn" to="/vn/all-products" />
                <Redirect exact from="/sa" to="/sa/all-products" />
                <Redirect exact from="/hu" to="/hu/all-products" />
                <Redirect exact from="/om" to="/om/all-products" />
                <Redirect exact from="/fi" to="/fi/all-products" />

                <Route path="/:language/login" name="login" component={AuthPage} />
                <Route path="/login" name="login" component={AuthPage} />

                <Route path="/:language/forgotPassword" name="forgotPassword" component={(ForgotPassword)} />
                <Route path="/forgotPassword" name="forgotPassword" component={(ForgotPassword)} />

                <Route path="/:language/signup" name="signup" component={(Register)} />
                <Route path="/signup" name="signup" component={(Register)} />

                <Route path="/point/:ref" name="pointRedeem" component={(PointRedeem)} />
                <Route path="/:language/point/:ref" name="pointRedeem" component={(PointRedeem)} />

                <Redirect from="/:language/letsguideyou/outcome" to="/quiz/outcome" />
                <Route path="/:language/draft/quiz/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />

                <Route path="/quiz-diy/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/:language/quiz-diy/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />

                <Route path="/:language/quiz/outcome/:partner/:idOutCome" name="QuizResult" component={(ResultNew)} />
                <Route path="/quiz/outcome/:partner/:idOutCome" name="QuizResult" component={(ResultNew)} />

                <Route path="/:language/quiz/outcome/:idOutCome" name="QuizResult" component={(ResultNew)} />
                <Route path="/quiz/outcome/:idOutCome" name="QuizResult" component={(ResultNew)} />

                <Route path="/:language/quiz" name="LetsGuideYou" component={(Quizv4)} />
                <Route path="/:language/quiz-ferrari" name="LetsGuideYou" component={(QuizFerrari)} />
                <Route path="/:language/quiz-ferrari-outcome/:id" name="LetsGuideYou" component={(QuizResultFerrari)} />
                <Route path="/ae/ae-quiz" name="LetsGuideYou" component={(Quizv4)} />
                <Route path="/quiz" name="LetsGuideYou" component={(Quizv4)} />
                <Route path="/quiz-ferrari" name="LetsGuideYou" component={(QuizFerrari)} />
                <Route path="/quiz-ferrari-outcome/:id" name="LetsGuideYou" component={(QuizResultFerrari)} />

                <Redirect from="/:language/letsguideyou" to="/quiz" />

                <Route path="/:language/draft/quiz" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/quizv3" name="Quizv3" component={(QuizV3)} />
                <Route path="/:language/quizv3" name="Quizv3" component={(QuizV3)} />

                <Route path="/quizv2" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/quiz-diy" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/quizv2-diy" name="LetsGuideYou" component={(LetsGuideYou)} />

                <Route path="/:language/quizv2" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/:language/quiz-diy" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/:language/quizv2-diy" name="LetsGuideYou" component={(LetsGuideYou)} />

                <Route
                  path="/landingquiz"
                  component={() => {
                    window.location.href = 'https://www.maison21g.com/landingquiz';
                    return null;
                  }}
                />
                <Route
                  path="/:language/landingquiz"
                  component={() => {
                    window.location.href = 'https://www.maison21g.com/landingquiz';
                    return null;
                  }}
                />
                <Route path="/:language/draft/landingquizv3" name="LandingQuizV3" component={(LandingQuizV3)} />
                <Route path="/landingquizv3" name="LandingQuizV3" component={(LandingQuizV3)} />
                <Route path="/:language/landingquizv3" name="LandingQuizV3" component={(LandingQuizV3)} />

                <Route path="/:language/draft/boutique-quiz/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/boutique-quiz/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/:language/boutique-quiz/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/boutique-quiz-v2/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/:language/boutique-quiz-v2/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />

                <Route path="/:language/draft/boutique-quiz/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/boutique-quiz/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/:language/boutique-quiz/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/boutique-quiz-v2/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/:language/boutique-quiz-v2/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />

                <Route path="/:language/draft/boutique-quiz/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/boutique-quiz/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/:language/boutique-quiz/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/boutique-quiz-v2/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/:language/boutique-quiz-v2/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />

                <Route path="/:language/draft/boutique-quiz" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/boutique-quiz" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/:language/boutique-quiz" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/boutique-quiz-v2" name="LetsGuideYou" component={(QuizV3)} />
                <Route path="/:language/boutique-quiz-v2" name="LetsGuideYou" component={(QuizV3)} />

                {/* <Route path="/:language/draft/sephora/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/sephora/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/:language/sephora/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />

                <Route path="/:language/draft/sephora/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/sephora/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/:language/sephora/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />

                <Route path="/:language/draft/sephora/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/sephora/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/:language/sephora/outcome/:idOutCome" name="OutCome" component={(ResultNew)} /> */}

                <Route path="/lazada/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/:language/lazada/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />

                <Route path="/:language/draft/sephora" name="LetsGuideYou" component={(Quizv4)} />
                <Route path="/sephora" name="LetsGuideYou" component={(Quizv4)} />
                <Route path="/:language/sephora" name="LetsGuideYou" component={(Quizv4)} />

                <Route path="/lazada" name="Quiz" component={(Quizv4)} />
                <Route path="/:language/lazada" name="Quiz" component={(Quizv4)} />

                <Route path="/:language/draft/sofitel/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/sofitel/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/:language/sofitel/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />

                <Route path="/:language/draft/sofitel/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/sofitel/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/:language/sofitel/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />

                <Route path="/:language/draft/sofitel/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/sofitel/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/:language/sofitel/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />

                <Route path="/:language/draft/sofitel" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/sofitel" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/:language/sofitel" name="LetsGuideYou" component={(LetsGuideYou)} />

                <Route path="/:language/draft/bmw/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/bmw/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/:language/bmw/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />

                <Route path="/:language/draft/bmw/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/bmw/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/:language/bmw/create-perfume/:idOutCome" name="Create Perfume" component={(CreatePerfumeBoutique)} />

                <Route path="/:language/draft/bmw/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/bmw/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/:language/bmw/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />

                <Route path="/:language/draft/bmw" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/bmw" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/:language/bmw" name="LetsGuideYou" component={(LetsGuideYou)} />

                <Route path="/:language/draft/krisshop/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/krisshop/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />
                <Route path="/:language/krisshop/outcome/:idOutCome" name="OutCome" component={(ResultNew)} />

                <Route path="/:language/draft/krisshop" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/krisshop" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/:language/krisshop" name="LetsGuideYou" component={(LetsGuideYou)} />

                <Route path="/:language/draft/partnership-quiz/outcome/:idOutCome/:slug" name="LetsGuideYou" component={(ResultNew)} />
                <Route path="/partnership-quiz/outcome/:idOutCome/:slug" name="LetsGuideYou" component={(ResultNew)} />
                <Route path="/:language/partnership-quiz/outcome/:idOutCome/:slug" name="LetsGuideYou" component={(ResultNew)} />

                <Route path="/:language/draft/partnership-quiz/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/partnership-quiz/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />
                <Route path="/:language/partnership-quiz/instore/:idOutCome" name="InStore" component={(PersonalityInstore)} />

                <Route path="/:language/draft/partnership-quiz/create-perfume/:idOutCome/:slug" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/partnership-quiz/create-perfume/:idOutCome/:slug" name="Create Perfume" component={(CreatePerfumeBoutique)} />
                <Route path="/:language/partnership-quiz/create-perfume/:idOutCome/:slug" name="Create Perfume" component={(CreatePerfumeBoutique)} />

                <Route path="/:language/draft/partnership-quiz/:slug" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/partnership-quiz/:slug" name="LetsGuideYou" component={(LetsGuideYou)} />
                <Route path="/:language/partnership-quiz/:slug" name="LetsGuideYou" component={(LetsGuideYou)} />

                <Route path="/:language/draft/create-perfume/outcome" name="Combinations" component={(PerfumeCombination)} />
                <Route path="/create-perfume/outcome" name="Combinations" component={(PerfumeCombination)} />
                <Route path="/:language/create-perfume/outcome" name="Combinations" component={(PerfumeCombination)} />

                <Route path="/:language/draft/create-perfume" name="CreatePerfume" component={(SelectionPerfume)} />
                <Route path="/create-perfume/:product1/:product2/:product3" name="CreatePerfume" component={(SelectionPerfume)} />
                <Route path="/create-perfume/:product1/:product2" name="CreatePerfume" component={(SelectionPerfume)} />
                <Route path="/create-perfume/:product1" name="CreatePerfume" component={(SelectionPerfume)} />
                <Route path="/create-perfume" name="CreatePerfume" component={(SelectionPerfume)} />
                <Route path="/:language/create-perfume/:product1/:product2/:product3" name="CreatePerfume" component={(SelectionPerfume)} />
                <Route path="/:language/create-perfume/:product1/:product2" name="CreatePerfume" component={(SelectionPerfume)} />
                <Route path="/:language/create-perfume/:product1" name="CreatePerfume" component={(SelectionPerfume)} />
                <Route path="/:language/create-perfume" name="CreatePerfume" component={(SelectionPerfume)} />

                <Route path="/:language/draft/mmo-gift" name="MMO Gift" component={(MmoGift)} />
                <Route path="/mmo-gift" name="MMO Gift" component={(MmoGift)} />
                <Route path="/:language/mmo-gift" name="MMO Gift" component={(MmoGift)} />

                <Route path="/:language/draft/gift/receiver" name="gift-receiver" component={(GiftReceiverPage)} />
                <Route path="/gift/receiver" name="gift-receiver" component={(GiftReceiverPage)} />
                <Route path="/:language/gift/receiver" name="gift-receiver" component={(GiftReceiverPage)} />

                <Route path="/:language/gift/gift-collections/:id" name="giftItem" component={(GiftBundlesDetail)} />
                <Route path="/gift/gift-collections/:id" name="giftItem" component={(GiftBundlesDetail)} />
                <Redirect from="/:language/gift/gift_bundles" to="/:language/gift/gift-collections" />
                <Redirect from="/gift/gift_bundles" to="/gift/gift-collections" />
                <Route path="/:language/gift/bespoke-perfume" name="giftBespoke" component={(GiftBespoke)} />
                <Route path="/gift/bespoke-perfume" name="giftBespoke" component={(GiftBespoke)} />
                <Route path="/:language/gift/gift-cards" name="giftCards" component={(GiftVoucher)} />
                <Route path="/gift/gift-cards" name="giftCards" component={(GiftVoucher)} />
                <Route path="/:language/gift/gift-collections" name="giftCollections" component={(GiftBundles)} />
                <Route path="/gift/gift-collections" name="giftCollections" component={(GiftBundles)} />
                <Route path="/:language/gift/workshop" name="giftWorkshop" component={(GiftWorkshop)} />
                <Route path="/gift/workshop" name="giftWorkshop" component={(GiftWorkshop)} />
                <Route path="/:language/gift" name="Gift" component={(GiftPageV2)} />
                <Route path="/gift" name="Gift" component={(GiftPageV2)} />

                <Route path="/all-products" name="product-all" component={(ProductAllV3)} />
                <Route path="/:language/all-products" name="product-all" exact component={(ProductAllV3)} />
                <Route path="/:language/all-products/:name" name="product-all" component={(ProductAllV3)} />

                <Redirect from="/:language/all-products-v2/creation_perfume" to="/:language/product/creation_perfume" />
                <Redirect from="/:language/all-products-v2/miniature_perfume" to="/:language/product/miniature_perfume" />
                <Redirect from="/:language/all-products-v2/dual_candles" to="/:language/product/dual_candles" />
                <Redirect from="/:language/all-products-v2/solid_perfume" to="/:language/product/wax" />
                <Redirect from="/:language/all-products-v2/home_scents" to="/:language/product/home_scents" />
                <Redirect from="/:language/all-products-v2/hand_sanitizer" to="/:language/product/hand_sanitizer" />
                <Route path="/:language/draft/all-products-v2/:tags" name="product-all" component={(ProductAllV2)} />
                <Route path="/all-products-v2/:tags" name="product-all" component={(ProductAllV2)} />
                <Route path="/:language/all-products-v2/:tags" name="product-all" component={(ProductAllV2)} />
                <Route path="/:language/draft/all-products-v2" name="product-all" component={(ProductAllV2)} />
                <Route path="/all-products-v2" name="product-all" component={(ProductAllV2)} />
                <Route path="/:language/all-products-v2" name="product-all" component={(ProductAllV2)} />
                <Redirect from="/:language/products-all" to="/all-products" />

                <Route path="/:language/draft/product/:type/:id/:item" name="product" component={(ProductB2C)} />
                <Route path="/product/:type/:id/:item" name="product" component={(ProductB2C)} />
                <Route path="/:language/product/:type/:id/:item" name="product" component={(ProductB2C)} />
                <Route path="/:language/draft/product/:type/:id" name="product" component={(ProductB2C)} />
                <Redirect from="/product/Perfume/:id" to="/product/perfume_diy/:id" />
                <Redirect from="/product/creation_perfume/:id" to="/product/perfume_diy/:id" />
                <Route path="/product/:type/:id" name="product" component={(ProductB2C)} />
                <Redirect from="/:language/product/Perfume/:id" to="/:language/product/perfume_diy/:id" />
                <Redirect from="/:language/product/creation_perfume/:id" to="/:language/product/perfume_diy/:id" />
                <Route path="/:language/product/:type/:id" name="product" component={(ProductB2C)} />
                <Route path="/:language/draft/product/:id1/:id2" name="product" component={(ProductB2C)} />
                <Route path="/product/:id1/:id2" name="product" component={(ProductB2C)} />
                <Route path="/:language/product/:id1/:id2" name="product" component={(ProductB2C)} />
                <Redirect from="/product/creation_perfume" to="/product/perfume_diy" />
                <Route path="/product/:tags" name="product" component={(ProductB2C)} />
                <Redirect from="/:language/product/creation_perfume" to="/:language/product/perfume_diy" />
                <Route path="/:language/product/:tags" name="product" component={(ProductB2C)} />

                <Route path="/productv2/:type/:id/:item" name="product" component={(ProductDetailV2)} />
                <Route path="/:language/productv2/:type/:id/:item" name="product" component={(ProductDetailV2)} />
                <Route path="/productv2/:type/:id" name="product" component={(ProductDetailV2)} />
                <Route path="/:language/productv2/:type/:id" name="product" component={(ProductDetailV2)} />
                <Route path="/productv2/:id1/:id2" name="product" component={(ProductDetailV2)} />
                <Route path="/:language/productv2/:id1/:id2" name="product" component={(ProductDetailV2)} />
                <Route path="/productv2/:tags" name="product" component={(ProductDetailV2)} />
                <Route path="/:language/productv2/:tags" name="product" component={(ProductDetailV2)} />

                <Redirect from="/products" to="/product/discovery_box/2106" />
                <Redirect from="/:language/products" to="/:language/product/discovery_box/2106" />

                <Route path="/:language/draft/checkout" name="checkout" component={CheckOutV2} />
                <Route path="/checkout" name="checkout" component={CheckOutV2} />
                <Route path="/:language/checkout" name="checkout" component={CheckOutV2} />


                {/* <Route path="/:language/draft/checkout-club" name="checkout club" component={(CheckOutClub)} />
                <Route path="/checkout-club" name="checkout club" component={(CheckOutClub)} />
                <Route path="/:language/checkout-club" name="checkout club" component={(CheckOutClub)} /> */}

                <Route path="/:language/draft/checkout-gift" name="checkout gift" component={(CheckOutGift)} />
                <Route path="/checkout-gift" name="checkout gift" component={(CheckOutGift)} />
                <Route path="/:language/checkout-gift" name="checkout gift" component={(CheckOutGift)} />


                <Route path="/:language/draft/pay/success/:orderId" name="paySuccess" component={(PaySuccessV2)} />
                <Route path="/pay/success/:orderId" name="paySuccess" component={(PaySuccessV2)} />
                <Route path="/:language/pay/success/:orderId" name="paySuccess" component={(PaySuccessV2)} />

                <Route path="/:language/draft/freestyle" name="Mmo" component={(MMO2)} />
                <Route path="/freestyle" name="Mmo" component={(MMO2)} />
                <Route path="/:language/freestyle" name="Mmo" component={(MMO2)} />

                <Route path="/:language/perfume-workshop/:workshop/service/:service/count/:count" name="BookWorkshop" component={(BookWorkshop)} />
                <Route path="/perfume-workshop/:workshop/service/:service/count/:count" name="BookWorkshop" component={(BookWorkshop)} />
                <Route path="/:language/perfume-workshop/:payment/:status/:idInvoice" name="BookWorkshop" component={(StatusPaymentBookingWorkshop)} />
                <Route path="/perfume-workshop/:payment/:status/:idInvoice" name="BookWorkshop" component={(StatusPaymentBookingWorkshop)} />
                <Route
                  path="/:language/perfume-workshop/:workshop"
                  component={() => {
                    window.location.href = `https://www.maison21g.com/perfume-workshop/${window.location.pathname.split('/').pop()}`;
                    return null;
                  }}
                />
                <Route
                  path="/perfume-workshop/:workshop"
                  component={() => {
                    window.location.href = `https://www.maison21g.com/perfume-workshop/${window.location.pathname.split('/').pop()}`;
                    return null;
                  }}
                />
                <Route
                  path="/:language/perfume-workshop"
                  component={() => {
                    window.location.href = 'https://www.maison21g.com/sg/perfume-workshop';
                    return null;
                  }}
                />
                <Route
                  path="/perfume-workshop"
                  component={() => {
                    window.location.href = 'https://www.maison21g.com/sg/perfume-workshop';
                    return null;
                  }}
                />

                <Route path="/:language/draft/account/:type/:idItem" name="Account" component={(AccountB2CPage)} />
                <Route path="/account/:type/:idItem" name="Account" component={(AccountB2CPage)} />
                <Route path="/:language/account/:type/:idItem" name="Account" component={(AccountB2CPage)} />
                <Route path="/:language/draft/account/:type" name="Account" component={(AccountB2CPage)} />
                <Route path="/account/:type" name="Account" component={(AccountB2CPage)} />
                <Route path="/:language/account/:type" name="Account" component={(AccountB2CPage)} />
                <Route path="/:language/draft/account" name="Account" component={(AccountB2CPage)} />
                <Route path="/account" name="Account" component={(AccountB2CPage)} />
                <Route path="/:language/account" name="Account" component={(AccountB2CPage)} />

                <Route path="/:language/draft/write-review" name="Write-Review" component={(WriteReview)} />
                <Route path="/write-review" name="Write-Review" component={(WriteReview)} />
                <Route path="/:language/write-review" name="Write-Review" component={(WriteReview)} />

                <Route path="/:language/draft/funnel-quiz" name="funnel" component={(FunnelV2)} />
                <Route path="/funnel-quiz" name="funnel" component={(FunnelV2)} />
                <Route path="/:language/funnel-quiz" name="funnel" component={(FunnelV2)} />

                <Route path="/:language/draft/funnel" name="funnel" component={(FunnelV3)} />
                <Route path="/funnel" name="funnel" component={(FunnelV3)} />
                <Route path="/:language/funnel" name="funnel" component={(FunnelV3)} />

                <Route path="/:language/draft/club21g" name="club21g" component={(Club21G)} />
                <Route path="/club21g" name="club21g" component={(Club21G)} />
                <Route path="/:language/club21g" name="club21g" component={(Club21G)} />

                <Route path="/:language/draft/partnership" name="partnership" component={(Partnership)} />
                <Route path="/partnership" name="partnership" component={(Partnership)} />
                <Route path="/:language/partnership" name="partnership" component={(Partnership)} />

                <Route exact path="/:language/draft/glossaire" name="Glossaire" component={(Glossaire)} />
                <Route exact path="/glossaire" name="Glossaire" component={(Glossaire)} />
                <Route exact path="/:language/glossaire" name="Glossaire" component={(Glossaire)} />
                <Route exact path="/:language/draft/glossaire/:name" name="Glossaire" component={(Glossaire)} />
                <Route exact path="/glossaire/:name" name="Glossaire" component={(Glossaire)} />
                <Route exact path="/:language/glossaire/:name" name="Glossaire" component={(Glossaire)} />

                <Route exact path="/:language/ingredients-glossarium/:id" name="Glossary" component={(Glossary)} />
                <Route exact path="/ingredients-glossarium/:id" name="Glossary" component={(Glossary)} />

                <Route path="/cart" name="Cart" component={(CardPage)} />
                <Route path="/:language/cart" name="Cart" component={(CardPage)} />

                <Route path="*" status={404} name="notFound" component={(NotFound)} />
              </Switch>
            </div>
          </main>
        </div>
        {/* <Footer /> */}
      </div>
    );
  }
}

export default Full;
