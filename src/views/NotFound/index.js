import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { MetaTags } from 'react-meta-tags';
import ButtonCT from '../../components/ButtonCT';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import icBottleNew from '../../image/bottle-not-found.png';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import '../../styles/style.scss';
import '../../styles/not-found.scss';
import {
  fetchCMSHomepage, generateUrlWeb, getCodeUrl, getLinkFromButtonBlock, getNameFromButtonBlock, getSEOFromCms,
  setPrerenderReady,
} from '../../Redux/Helpers';
import { toastrError } from '../../Redux/Helpers/notification';
import FooterV2 from '../../views2/footer';

function NotFound(props) {
  const [cmsNotFound, setCmsNotFound] = useState({});
  const [buttonBlocks, setButtonBlocks] = useState([]);

  const fetchCMS = async () => {
    const { cms } = props;
    const aboutCmsFind = _.find(cms, x => x.title === 'Not Found');
    if (!aboutCmsFind) {
      try {
        const result = await fetchCMSHomepage('not-found');
        props.addCmsRedux(result);
        setCmsNotFound(result);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      setCmsNotFound(aboutCmsFind);
    }
  };

  const isCountryCorrect = () => {
    const symbol = getCodeUrl();
    return _.find(props.countries, x => x.code === symbol);
  };

  useEffect(() => {
    fetchCMS();
    setPrerenderReady();
  }, []);

  useEffect(() => {
    const { body } = cmsNotFound;
    const bt = _.filter(body, x => x.type === 'button_block');
    setButtonBlocks(bt);
  }, [cmsNotFound]);

  const seo = getSEOFromCms(cmsNotFound);
  const metaData = (
    <MetaTags>
      <title>
        {seo ? seo.seoTitle : ''}
      </title>
      <meta name="description" content={seo ? seo.seoDescription : ''} />
      <meta name="robots" content="index, follow" />
      <meta name="revisit-after" content="3 month" />
      <meta name="prerender-status-code" content="404" />
    </MetaTags>
  );

  const thisBt = getNameFromButtonBlock(buttonBlocks, 'This doesn’t make any scents');
  const perfumeBt = getNameFromButtonBlock(buttonBlocks, 'Perfume 404 has not yet to be invented');
  const backBt = getNameFromButtonBlock(buttonBlocks, 'Back to home');
  const linkBackBt = getLinkFromButtonBlock(buttonBlocks, 'Back to home');
  console.log('prerenderReady 1');
  return (
    <div>
      {metaData}
      <HeaderHomePageV3 />
      <div className="div-not-found">
        <div className="text-content">
          <h1>
            {thisBt}
          </h1>
          <span>
            {perfumeBt}
          </span>
          <ButtonCT
            typeBT="LINK"
            className={classnames('secondary-bt', 'animated-hover')}
            name={backBt}
            href={generateUrlWeb(linkBackBt, !isCountryCorrect())}
          />
        </div>
        <div className="image-content">
          <div className="div-image">
            <img src={icBottleNew} alt="icon" />
          </div>
        </div>
      </div>
      <FooterV2 />
    </div>
  );
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NotFound));
