import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';

import icLocal from '../../image/icon/local-yellow.svg';
import icTime from '../../image/icon/time-yellow.svg';
import icPhone from '../../image/icon/phone-yellow.svg';
import { getAltImageV2 } from '../../Redux/Helpers/index';


import { isBrowser } from '../../DetectScreen/detectIFrame';

function ItemBoutique(props) {
  const settings = {
    dots: true,
    arrows: isBrowser,
    // className: props.data.value?.images?.length > 1 && props.isCenterMode ? 'slider-center' : '',
    centerMode: true,
    centerPadding: props.data.value?.images?.length > 1 && props.isCenterMode ? '100px' : '0px',
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
  };
  return (
    <div className={classnames('item-boutique', props.className)}>
      <Slider {...settings}>
        {
        _.map(props.data.value.images, d => (
          <div className="div-image">
            <img loading="lazy" src={d.value.image} alt={getAltImageV2(d.value)} />
          </div>
        ))
      }
      </Slider>
      <div className="card-info div-col justify-center items-center">
        <div className="body-card-info">
          <h2>
            {props.data.value.country}
          </h2>
          <div className="text">
            {props.data.value.image_background.caption}
          </div>
          <div className="line-info div-row">
            <img src={icLocal} alt="icon" />
            <span>
              {props.data.value.address}
            </span>
          </div>
          <div className="line-info div-row">
            <img src={icTime} alt="icon" />
            <span>
              {props.data.value.open_hour}
            </span>
          </div>
          <div className="line-info div-row">
            <img src={icPhone} alt="icon" />
            <span>
              {props.data.value.phone}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ItemBoutique;
