import React, { useState, useEffect } from 'react';
import '../../styles/style.scss';
import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import _ from 'lodash';
import '../../styles/our-boutique.scss';
import ItemBoutique from './itemBoutique';
import Header from '../../components/HomePage/header';
import { isMobile } from '../../DetectScreen/detectIFrame';
import {
  fetchCMSHomepage, generateHreflang, getSEOFromCms, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers';
import { useMergeState } from '../../Utils/customHooks';
import addCmsRedux from '../../Redux/Actions/cms';
import BlockBannerV2 from '../../components/HomeAlt/BlockBannerV2';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';

const getCMS = (cms) => {
  const objectReturn = {
    seo: undefined,
  };
  if (!_.isEmpty(cms)) {
    const seo = getSEOFromCms(cms);
    _.assign(objectReturn, {
      seo,
    });
    const { body, header_text: headerText, image_background: imageBackground } = cms;
    if (body) {
      const storesBlocks = _.find(body, x => x.type === 'stores_block');
      const bannerBlocks = _.filter(body, x => x.type === 'banner_block');
      _.assign(objectReturn, {
        headerText,
        storesBlocks,
        bannerBlocks,
        imageBackground,
      });
    }
  }
  return objectReturn;
};

function OurBoutique(props) {
  const [state, setState] = useMergeState({
    seo: {},
    storesBlocks: undefined,
    bannerBlocks: [],
  });

  const fetchDataInit = async () => {
    const { cms } = props;
    const boutique = _.find(cms, x => x.title === 'Our Boutique');
    if (!boutique) {
      const cmsData = await fetchCMSHomepage('our-boutique');
      props.addCmsRedux(cmsData);
      const dataFromStore = getCMS(cmsData);
      setState(dataFromStore);
    } else {
      const dataFromStore = getCMS(boutique);
      setState(dataFromStore);
    }
  };

  useEffect(() => {
    setPrerenderReady();
    fetchDataInit();
    return (() => {
      removeLinkHreflang();
    });
  }, []);

  return (
    <div>
      <MetaTags>
        <title>
          {state.seo ? state.seo.seoTitle : ''}
        </title>
        <meta name="description" content={state.seo ? state.seo.seoDescription : ''} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(state.countries, '/our-boutique')}
      </MetaTags>
      {/* <Header />
      { props.showAskRegion && (<div className="div-temp-region" />) } */}
      <HeaderHomePageV3 />
      <div className="our-boutique div-col items-center">
        <div className="header div-col justify-center items-center" style={{ backgroundImage: `url(${state.imageBackground})` }} />
        <h1 className="header_1">
          {state.headerText}
        </h1>
        {
          _.map(state.bannerBlocks, (d, index) => (
            <BlockBannerV2 id={`banner_block-${index}`} data={d} isOurTeamReadMore />
          ))
        }
        {/* {
          _.map(state.storesBlocks ? state.storesBlocks.value.stores : [], (d, index) => (
            <ItemBoutique data={d} className={index % 2 === 0 || isMobile ? '' : 'right'} />
          ))
        } */}
        <div className="div-space" />
      </div>
      <FooterV2 />
    </div>
  );
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};

export default connect(mapStateToProps, mapDispatchToProps)(OurBoutique);
