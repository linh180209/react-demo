import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { Collapse } from 'reactstrap';
import ButtonCT from '../../components/ButtonCT';
import BlockIconHeaderV3 from '../../components/HomeAlt/BlockIconHeaderV3';
import BlockIconLineV2 from '../../components/HomeAlt/BlockIconLine/blockIconLinev2';
import TestimonialsV2 from '../../components/HomeAlt/TestimonialsV2';
import QnABlock from '../../components/QnABlock';
import { isBrowser, isMobile } from '../../DetectScreen';
import icWordSocial from '../../image/icon/ic-12-0.svg';
import icSeducation from '../../image/icon/ic-12-1.svg';
import icSpecialOccasion from '../../image/icon/ic-12-2.svg';
import icChillRelax from '../../image/icon/ic-12-3.svg';
import icTake from '../../image/icon/ic-next-persional.svg';
import icTakeBlack from '../../image/icon/ic-retake-black.svg';
import icRetake from '../../image/icon/ic-retake-quiz.svg';
import bg from '../../image/test/4.png';
import updateIngredientsData from '../../Redux/Actions/ingredients';
import loadingPage from '../../Redux/Actions/loading';
import addScentsToStore from '../../Redux/Actions/scents';
import {
  getNameFromButtonBlock, getNameFromCommon, getPrice, getWidthElement, isRightAlignedText,
} from '../../Redux/Helpers';
import { toastrError } from '../../Redux/Helpers/notification';
import { useMergeState } from '../../Utils/customHooks';
import FooterV2 from '../../views2/footer';
import CustomerSay from '../CheckOutB2C/customerSay';
import CardPerfume from './CardPerfume';
import {
  fetchAllScent, fetchDataFAQ, getImageFromFAQ,
} from './handler';
import Occasions from './Occasions';
import RenderItemSlider from './renderItemSlider';
import './styles.scss';
import { fetchScentLib } from '../../Redux/Helpers/fetchAPI';

function ResultScentV3(props) {
  const scents = useSelector(state => state.scents);
  const ingredients = useSelector(state => state.ingredients);
  const dispatch = useDispatch();

  const [isViewMoreMobile, setIsViewMoreMobile] = useState(isBrowser);
  const sliderRef = useRef();
  const [slideNow, setSlideNow] = useState(0);
  const [listProducts, setListProducts] = useState([]);
  const [listImage, setListImage] = useState([]);
  const [state, setState] = useMergeState({
    priceDualCrayons: undefined,
    pricePerfume: undefined,
    mixSelected: props.mixes,
    customeBottle: props.customeBottle || {},
    isPerfumeSelected: true,
  });
  const listImagePerfume = useRef();
  const listImageDualCrayons = useRef();
  const isQuizOutComeV3 = window.location.pathname.includes('/quizv3');

  const getAllScent = async () => {
    if (scents?.length > 0) {
      return;
    }
    try {
      dispatch(loadingPage(true));
      const result = await fetchAllScent();
      dispatch(addScentsToStore(result));
    } catch (err) {
      toastrError(err.message);
    } finally {
      dispatch(loadingPage(false));
    }
  };

  const getImagePerfume = async (type) => {
    const isPerfume = type === 'Perfume';
    try {
      const dataFAQ = await fetchDataFAQ(type);
      const list = getImageFromFAQ(dataFAQ, isPerfume);
      if (isPerfume) {
        listImagePerfume.current = list;
      } else {
        listImageDualCrayons.current = list;
      }
      setListImage(list);
    } catch (error) {
      console.log('error', error);
    }
  };

  const getAllIngredients = async () => {
    if (!props.ingredients || props.ingredients.length === 0) {
      const dataScentLib = await fetchScentLib();
      dispatch(updateIngredientsData(dataScentLib));
    }
  };

  const onChangeType = async (isPerfume) => {
    if (!isPerfume) {
      if (listImageDualCrayons.current) {
        setListImage(listImageDualCrayons.current);
      } else {
        dispatch(loadingPage(true));
        await getImagePerfume('dual_crayons');
        dispatch(loadingPage(false));
      }
      setState({ isPerfumeSelected: false });
    } else {
      setListImage(listImagePerfume.current);
      setState({ isPerfumeSelected: true });
    }
  };

  const updateCustomeBottle = (data) => {
    setState({ customeBottle: data });
  };

  useEffect(() => {
    Promise.all([getPrice('perfume'), getPrice('dual_crayons')]).then((results) => {
      setState({
        pricePerfume: _.find(results[0], x => x.apply_to_sample === false)?.price,
        priceDualCrayons: results[1][0].price,
      });
    });

    getAllScent();
    getAllIngredients();

    if (isQuizOutComeV3) {
      getImagePerfume('Perfume');
    }
  }, []);

  useEffect(() => {
    if (props.mixes && props.dataDay?.length > 0 && props.dataNight?.length > 0 && !_.isEmpty(props.cmsCommon)) {
      const chillBt = getNameFromCommon(props.cmsCommon, 'CHILL & RELAX');
      const seductionBt = getNameFromCommon(props.cmsCommon, 'seduction');
      const specialBt = getNameFromCommon(props.cmsCommon, 'SPECIAL OCCASION');
      const workBt = getNameFromCommon(props.cmsCommon, 'WORK & SOCIAL');
      const elegantBt = getNameFromCommon(props.cmsCommon, 'Elegant & Confident');
      const attractiveBt = getNameFromCommon(props.cmsCommon, 'Attractive & Sexy');
      const standoutBt = getNameFromCommon(props.cmsCommon, 'Stand out & Trendy');
      const freshBt = getNameFromCommon(props.cmsCommon, 'Fresh & Conforting');
      const basedOnBt = getNameFromButtonBlock(props.buttonBlocks, 'Based on perfume by');
      const pChillRelax = {
        title: chillBt,
        description: freshBt,
        data: props.dataDay?.length > 0 ? props.dataDay[0] : '',
        name: 'chill-relax',
        icon: icChillRelax,
        indexMix: 1,
      };
      const pWorkSocial = {
        title: workBt,
        description: elegantBt,
        data: props.dataDay?.length > 1 ? props.dataDay[1] : '',
        name: 'work-social',
        icon: icWordSocial,
        indexMix: 2,
      };
      const pSeducation = {
        title: seductionBt,
        description: attractiveBt,
        data: props.dataNight?.length > 0 ? props.dataNight[0] : '',
        name: 'seducation',
        icon: icSeducation,
        indexMix: 3,
      };
      const pSpecialOccasion = {
        title: specialBt,
        description: standoutBt,
        data: props.dataNight?.length > 1 ? props.dataNight[1] : '',
        name: 'special-occasion',
        icon: icSpecialOccasion,
        indexMix: 4,
      };

      let listProductsTemp = [];
      if (isQuizOutComeV3) {
        const listTemp = [pChillRelax, pSeducation, pSpecialOccasion, pWorkSocial];
        _.forEach(props.originalMixes || [], (d) => {
          const ele = _.find(listTemp, x => x.indexMix === d);
          if (ele) {
            listProductsTemp.push(ele);
          }
        });

        _.forEach(props.externalProducts || [], (d, index) => {
          listProductsTemp.push({
            title: basedOnBt,
            subTitle: d.name,
            description: d.name,
            data: d.product,
            name: 'external-product',
            icon: icSpecialOccasion,
            indexMix: 5 + index,
          });
        });

        _.forEach(listTemp || [], (d) => {
          const ele = _.find(listProductsTemp, x => x.indexMix === d.indexMix);
          if (!ele) {
            listProductsTemp.push(d);
          }
        });
      } else if (props.mixes === 1) {
        listProductsTemp = [pChillRelax, pSeducation, pSpecialOccasion, pWorkSocial];
      } else if (props.mixes === 2) {
        listProductsTemp = [pWorkSocial, pChillRelax, pSeducation, pSpecialOccasion];
      } else if (props.mixes === 3) {
        listProductsTemp = [pSeducation, pChillRelax, pSpecialOccasion, pWorkSocial];
      } else if (props.mixes === 4) {
        listProductsTemp = [pSpecialOccasion, pChillRelax, pSeducation, pWorkSocial];
      }
      if (listProductsTemp) {
        setListProducts(listProductsTemp);
        setState({ mixSelected: props.mixes });
      }
    }
  }, [props.mixes, props.dataDay, props.dataNight, props.cmsCommon]);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplaySpeed: 5000,
    pauseOnHover: false,
    beforeChange: (current, next) => { setSlideNow(next); },
    customPaging: i => (
      <div className="dots-custom" style={slideNow === i ? { background: props.dataPersionality?.color } : {}} />
    ),
  };

  const retakeBt = getNameFromCommon(props.cmsCommon, 'RETAKE_QUIZ');

  const viewMoreBt = getNameFromButtonBlock(props.buttonBlocks, 'VIEW MORE');
  const moreBt = getNameFromButtonBlock(props.buttonBlocks, 'CLOSE');
  const scentTraits = getNameFromButtonBlock(props.buttonBlocks, 'Scent traits');
  const personalityTraits = getNameFromButtonBlock(props.buttonBlocks, 'Personality traits');
  const basedOnBt = getNameFromButtonBlock(props.buttonBlocks, 'Based on perfume by');

  const widthEle = getWidthElement('id_name');
  return (
    <div className={classnames('div-result-scent-v3 div-col', isQuizOutComeV3 ? 'outcome-v3' : '')}>
      <div className="div-persionality-text" style={{ backgroundImage: `url(${bg})` }}>
        {
            isBrowser && (
              <ButtonCT
                dataGtmtracking="funnel-1-step-19-retake-quiz"
                className="bt-icon-left"
                name={retakeBt}
                iconLeft={icRetake}
                onClick={props.onClickRetake}
              />
            )
          }
        <div className="text-persionality">
          <div id="id_name" className="div-name div-row">
            <img src={props.dataPersionality?.image} alt="icon" />
            <h2 className="header_2" style={{ color: props.dataPersionality?.color }}>
              {props.personalityChoice?.personality?.title}
            </h2>
          </div>
          <div className={classnames('header_4', isRightAlignedText() ? 'tr' : 'tl')} style={isBrowser ? { width: `${widthEle}px` } : {}}>
            {props.textDescription}
          </div>

        </div>

        {
            isMobile && !isViewMoreMobile && (
              <button
                onClick={() => setIsViewMoreMobile(true)}
                type="button"
                className="line-button-results bt-view-more button-bg__none"
              >
                {viewMoreBt}
              </button>
            )
        }
        <Collapse isOpen={isViewMoreMobile}>
          <div className="swiper-persionality div-col">
            <div className="relative w-100">
              <Slider ref={sliderRef} {...settings}>
                <RenderItemSlider title={props.leftTitle} listItems={props.adjectives} />
                <RenderItemSlider title={props.rightTitle} listItems={props.scents} />
              </Slider>
              <ButtonCT
                className="bt-icon-right none-border"
                name={slideNow === 0 ? scentTraits : personalityTraits}
                iconRight={icTake}
                onClick={() => sliderRef.current.slickNext()}
              />
            </div>
            {
              isMobile && (
                <button
                  onClick={() => setIsViewMoreMobile(false)}
                  type="button"
                  className="line-button-results button-bg__none"
                >
                  {moreBt}
                </button>
              )
            }
          </div>
        </Collapse>
      </div>
      {
        isQuizOutComeV3 && (
        <Occasions
          onClick={index => setState({ mixSelected: index })}
          listProducts={listProducts}
          mixSelected={state.mixSelected}
          buttonBlocks={props.buttonBlocks}
        />
        )
      }
      <div className="list-scents-all div-col">
        {
          props.dataDay?.length > 0 && props.dataNight?.length > 0 && (
            <React.Fragment>
              {
                _.map(listProducts, (d, index) => (
                  <CardPerfume
                    priceDualCrayons={state.priceDualCrayons}
                    pricePerfume={state.pricePerfume}
                    buttonBlocks={props.buttonBlocks}
                    cmsCommon={props.cmsCommon}
                    title={d.title}
                    icon={d.icon}
                    description={d.description}
                    data={d.data}
                    infoGift={props.infoGift}
                    onClickIngredient={props.onClickIngredient}
                    onClickAddToCart={props.onClickAddToCart}
                    name={d.name}
                    isQuizOutComeV3={isQuizOutComeV3}
                    listImage={listImage}
                    isActive={d.indexMix === state.mixSelected && isQuizOutComeV3 || !isQuizOutComeV3}
                    updateCustomeBottle={isQuizOutComeV3 ? updateCustomeBottle : undefined}
                    customeBottle={state.customeBottle}
                    onChangeType={isQuizOutComeV3 ? onChangeType : undefined}
                    isPerfumeSelected={state.isPerfumeSelected}
                    onClickRetake={props.onClickRetake}
                    disableInfo
                  />
                ))
              }
              {
                // only for quizv2
                props.externalProduct && !isQuizOutComeV3 && (
                  <CardPerfume
                    priceDualCrayons={state.priceDualCrayons}
                    buttonBlocks={props.buttonBlocks}
                    cmsCommon={props.cmsCommon}
                    title={basedOnBt}
                    // icon={d.icon}
                    description={props.externalProduct?.name}
                    data={props.externalProduct?.product}
                    imageExternal={props.externalProduct?.image}
                    infoGift={props.infoGift}
                    onClickIngredient={props.onClickIngredient}
                    onClickAddToCart={props.onClickAddToCart}
                    name="external-product"
                    isActive
                  />
                )
              }
            </React.Fragment>
          )
        }
      </div>
      {
        isQuizOutComeV3 && props.faqBlock?.value && (
          <div className="block-faq">
            <QnABlock data={props.faqBlock?.value} />
          </div>
        )
      }
      {
        isQuizOutComeV3 && props.benefitsBlock?.value && (
          <div className="block-icon">
            <BlockIconHeaderV3 heroCms={props.benefitsBlock} />
          </div>
        )
      }

      {
        isQuizOutComeV3 && props.feedbackBlock?.value && (
          <div className="block-custome-say">
            <CustomerSay data={props.feedbackBlock} isQuizPage />
          </div>
        )
      }

      {
        isQuizOutComeV3 && props.blogsBlock?.value && (
          <div className="block-blogs">
            <TestimonialsV2 heroCms={props.blogsBlock} />
          </div>
        )
      }

      {
        isQuizOutComeV3 && props.iconsBlock?.value && (
          <div className="icon-blocks">
            <BlockIconLineV2 data={props.iconsBlock} />
          </div>
        )
      }

      {
        isQuizOutComeV3 && (
          <FooterV2 />
        )
      }

      {
        isMobile && !isQuizOutComeV3 && (
          <ButtonCT
            className="bt-icon-left button-bg__none"
            name={retakeBt}
            iconLeft={icTakeBlack}
            onClick={props.onClickRetake}
          />
        )
      }
    </div>
  );
}

export default ResultScentV3;
