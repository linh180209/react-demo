import _ from 'lodash';

import { GET_PRODUCT_FOR_CART, GET_PRODUCT_URL } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';

export const getListScentsFromCombos = combo => _.filter(combo, x => x.product?.type === 'Scent');

export const getImageType = (images, type) => _.find(images, x => x.type === type);

export const getPerfumePrice = items => _.find(items, x => !x.is_sample)?.price;

export const getImageCustomBottle = (data) => {
  const bottles = _.find(data?.combo || [], x => x.product.type === 'Bottle');
  const arrayThumbs = _.filter(bottles?.product?.images || [], x => x.type === 'suggestion');
  return arrayThumbs;
};

export const getProduct = async (scents, type) => {
  const option = {
    url: `${GET_PRODUCT_FOR_CART}?combo=${scents[0]?.product?.id}${scents.length > 1 ? `,${scents[1]?.product?.id}` : ''}&type=${type}`,
    method: 'GET',
  };
  return fetchClient(option);
};

export const fetchProductDetail = (id) => {
  const options = {
    url: GET_PRODUCT_URL.replace('{id}', id),
    method: 'GET',
  };
  return fetchClient(options);
};

export const pareQueryToObject = (key) => {
  const strSearch = new URLSearchParams(window.location?.search || '').get(key);
  return strSearch ? JSON.parse(strSearch) : {};
};


export const getLinkWhenDeleteScent = (idProduct, nameQuery, name) => {
  const objectSearch = pareQueryToObject(nameQuery);
  _.assign(objectSearch, {
    [name]: idProduct,
  });
  return encodeURIComponent(JSON.stringify(objectSearch));
};

export const getListScentDeleted = (listScent, id) => {
  const list = _.cloneDeep(listScent);
  _.remove(list, x => x?.id === id);
  return list;
};
