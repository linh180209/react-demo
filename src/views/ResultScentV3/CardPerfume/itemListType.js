import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import ButtonCT from '../../../components/ButtonCT';
import RadioButton from '../../../components/Input/radioButton';
import { generaCurrency, getNameFromButtonBlock } from '../../../Redux/Helpers';
import icInfo from '../../../image/icon/ic-next-quizv2.svg';
import './itemListType.scss';

function ItemListType(props) {
  const onClickMoreInfo = (e) => {
    e.stopPropagation();
    props.onClickMoreInfo();
  };

  const moreInfoBt = getNameFromButtonBlock(props.buttonBlocks, 'more info');
  return (
    <div className={classnames('item-list-type', props.isVersion2 ? 'item-type-v2' : '')} onClick={props.onClick}>
      <div className="div-radio">
        <RadioButton className="radio-result" selected={props.selected} />
      </div>
      <span className={props.selected ? 'text-active' : ''}>
        {props.title}
        <span>
          {generaCurrency(props.price)}
        </span>
      </span>

      {
        props.isVersion2 ? (
          <button
            type="button"
            className="button-bg__none bt-info"
            onClick={e => onClickMoreInfo(e)}
          >
            INFO
            <img src={icInfo} alt="info" />
          </button>
        ) : (
          <div className="price">
            <h4 className="header_4">
              {generaCurrency(props.price)}
            </h4>
            <button
              onClick={e => onClickMoreInfo(e)}
              disabled={props.isDisableViewMore}
              className={classnames('button-bg__none view-more', props.isDisableViewMore ? 'disable' : '')}
              type="button"
            >
              {moreInfoBt}
            </button>
          </div>
        )
      }

    </div>
  );
}

export default ItemListType;
