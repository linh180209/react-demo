import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import _ from 'lodash';
import BlockVideoIngredient from '../../../ProductB2C/BlockVideoIngredient';

function BlockVideoQuiz(props) {
  const ingredients = useSelector(state => state.ingredients);
  const [listVideo, setListVideo] = useState([]);

  const getListVideos = () => {
    const combo = _.filter(props.data?.combo, x => x.product?.type === 'Scent');
    const videos = [];
    _.forEach(combo, (d) => {
      const ingredient = _.find(ingredients || [], x => x.id === d.product?.id);
      if (ingredient?.videos?.length) {
        videos.push(ingredient);
      }
    });
    setListVideo(videos);
  };

  useEffect(() => {
    getListVideos();
  }, [props.data, ingredients]);

  return (
    <div className="block-video-quiz">
      <BlockVideoIngredient cmsCommon={props.cmsCommon} videos={listVideo} />
    </div>
  );
}

export default BlockVideoQuiz;
