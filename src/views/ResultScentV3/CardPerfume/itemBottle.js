import classnames from 'classnames';
import React from 'react';
import BottleCustom from '../../../components/B2B/CardItem/bottleCustom';
import icBrush from '../../../image/icon/ic-brush.svg';
import icInfo from '../../../image/icon/ic-next-quizv2.svg';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import './itemBottle.scss';

function ItemBottle(props) {
  const botteBt = getNameFromButtonBlock(props.buttonBlocks, 'ENGRAVE BOTTLE');
  const chooseYourBt = getNameFromButtonBlock(props.buttonBlocks, 'Choose your picture and message');
  return (
    <div className={classnames('item-bottle div-row', props.disabled ? 'disabled' : '')} onClick={props.disabled ? () => {} : props.onClickCustom}>
      <div className="div-left div-row">
        <BottleCustom
          isImageText={!!props.dataCustom.image}
          image={props.dataCustom.image}
        />
        <div className="div-text div-col">
          <div className="line-1 div-row">
            <img src={icBrush} alt="icon" />
            {botteBt}
          </div>
          <div className="line-2">
            {chooseYourBt}
          </div>
        </div>
      </div>
      <button className="button-bg__none" type="button">
        <img src={icInfo} alt="icon" />
      </button>

    </div>
  );
}

export default ItemBottle;
