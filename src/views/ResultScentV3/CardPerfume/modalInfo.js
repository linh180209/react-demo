import React, { useState, useEffect } from 'react';
import {
  Modal, ModalBody,
} from 'reactstrap';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';

import icClose from '../../../image/icon/ic-close-popup-black.svg';
import testImage from '../../../image/test/1.png';
import './modalInfo.scss';
import { useMergeState } from '../../../Utils/customHooks';
import { addFontCustom } from '../../../Redux/Helpers';

function ModalInfo(props) {
  const [state, setState] = useMergeState({
    listImage: [],
    desBottle: undefined,
    desScent1: undefined,
    desScent2: undefined,
  });

  useEffect(() => {
    if (props.dataModal?.type?.name === 'Perfume') {
      const bottle = _.find(props.dataModal?.combo, x => x.product.type === 'Bottle');
      const scents = _.filter(props.dataModal?.combo, x => x.product.type !== 'Bottle');
      const listImage = _.concat(props.dataModal?.images, bottle.product.images, scents[0]?.product?.images, scents[1]?.product?.images);
      console.log('scents[0]?.product?.images', scents[0]?.product?.images);
      setState({
        listImage: _.filter(listImage, x => x?.type === null),
        desBottle: bottle?.product?.description,
        desScent1: scents[0]?.product?.short_description,
        desScent2: scents[1]?.product?.short_description,
        image1: _.find(scents[0]?.product?.images, x => x.type === 'unisex'),
        image2: _.find(scents[1]?.product?.images, x => x.type === 'unisex'),
        name: props.dataModal?.name,
      });
    } else if (props.dataModal?.type?.name === 'dual_crayons') {
      const bottle = _.find(props.dataModal?.combo, x => x.product.type === 'metal_cap');
      const scents = _.filter(props.dataModal?.combo, x => x.product.type !== 'metal_cap');
      const listImage = _.concat(props.dataModal?.images, bottle?.product?.images, scents[0]?.product?.combo[0]?.product?.images, scents[1]?.product?.combo[0]?.product?.images);
      setState({
        listImage: _.filter(listImage || [], x => x?.type === null),
        desBottle: bottle?.product?.short_description,
        desScent1: scents[0]?.product?.combo[0]?.product?.short_description,
        desScent2: scents[1]?.product?.combo[0]?.product?.short_description,
        image1: _.find(scents[0]?.product?.combo[0]?.product?.images, x => x.type === 'unisex'),
        image2: _.find(scents[1]?.product?.combo[0]?.product?.images, x => x.type === 'unisex'),
        name: props.dataModal?.name,
      });
    }
  }, [props.dataModal]);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows: false,
    autoplaySpeed: 5000,
    pauseOnHover: false,
  };

  return (
    <Modal isOpen={props.isOpen} centered className={`modal-info-result ${addFontCustom()}`}>
      <button
        onClick={props.onCloseModal}
        className="button-bg__none bt-close"
        type="button"
      >
        <img src={icClose} alt="icon" />
      </button>
      <div className="body-modal">
        {
          props.isFullDataScent ? (
            <div className="full-scent">
              <div className="div-combo-image">
                <img src={state.image1?.image} alt="scent" />
                <img src={state.image2?.image} alt="scent" />
                {/* <span>
                  COMBO SCENT
                </span> */}
              </div>
              <h4>
                {state.name}
              </h4>
              {props.slideColor}
              <div className="des">
                {state.desScent1}
                <br />
                {state.desScent2}
              </div>
            </div>
          ) : (
            <React.Fragment>
              <div className="div-image">
                <Slider {...settings}>
                  {
                    _.map(state.listImage, d => (
                      <img className="item-image" src={d.image} alt="icon" />
                    ))
                  }
                </Slider>
              </div>
              <span className="title">
                {props.dataModal?.type?.alt_name}
              </span>
              <div className="des">
                {state.desBottle}
                {/* <br />
                {state.desScent1}
                <br />
                {state.desScent2} */}
              </div>
            </React.Fragment>
          )
        }
      </div>
    </Modal>
  );
}

export default ModalInfo;
