import _ from 'lodash';
import React, {
  useEffect, useRef, useState, useCallback,
} from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import classnames from 'classnames';

import ButtonCT from '../../../components/ButtonCT';
import CustomeBottleV3 from '../../../components/CustomeBottleV3';
import ModalPickScent from '../../../components/ModalPickScent';
import { COLOR_CUSTOME, FONT_CUSTOME } from '../../../constants';
import { isBrowser, isMobile } from '../../../DetectScreen';
import icProduct from '../../../image/icon/product.svg';
import loadingPage from '../../../Redux/Actions/loading';
import icTakeBlack from '../../../image/icon/ic-retake-black.svg';
import { getNameFromButtonBlock, getNameFromCommon } from '../../../Redux/Helpers';
import { toastrError } from '../../../Redux/Helpers/notification';
import { useMergeState } from '../../../Utils/customHooks';
import ItemImageScent from '../../ResultScentV2/itemImageScent';
import ProgressCircle from '../../ResultScentV2/progressCircle';
import ProgressLine from '../../ResultScentV2/progressLine';
import {
  fetchProductDetail, getImageCustomBottle, getLinkWhenDeleteScent, getListScentDeleted, getListScentsFromCombos, getProduct, pareQueryToObject,
} from './handler';
import ItemBottle from './itemBottle';
import ItemsListScent from './itemListScent';
import ItemListType from './itemListType';
import ModalInfo from './modalInfo';
import './styles.scss';
import ImageBlock from '../../ProductB2C/imageBlock';
import BlockVideoQuiz from './BlockVideoQuiz';

const KEY_QUERY = 'custome-scent';
function CardPerfume(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const search = useLocation();
  const [state, setState] = useMergeState({
    listScent: getListScentsFromCombos(props.data || []),
    arrayThumbs: getImageCustomBottle(props.data),
    data: {},
    isPerfumeSelected: !_.isNil(props.isPerfumeSelected) ? props.isPerfumeSelected : true,
    customeBottle: _.isEmpty(props.customeBottle) ? props.customeBottle : {
      currentImg: props.infoGift?.image || undefined,
      nameBottle: props.infoGift?.isDisplayName ? props.infoGift?.name : '',
      font: props.infoGift?.font || FONT_CUSTOME.JOST,
      color: props.infoGift?.color || COLOR_CUSTOME.BLACK,
    },
    isShowCustom: false,
    dataModal: undefined,
    dataPerfumeApi: undefined,
    dataDualCrayonApi: undefined,
    scentSelected: undefined,
    isOpenSearch: false,
    indexSelected: undefined,
  });
  const [dataModal, setDataModal] = useState();
  const [isFullDataScent, setIsFullDataScent] = useState(false);
  const currentQuerySearch = useRef();

  const handleCropImage = (img) => {
    state.customeBottle.currentImg = img;
    setState({ customeBottle: state.customeBottle });
  };

  const onSaveCustomer = (data) => {
    const {
      image, name, color, font,
    } = data;
    state.customeBottle.color = color;
    state.customeBottle.font = font;
    if (_.isEmpty(image)) {
      state.customeBottle.currentImg = undefined;
      state.customeBottle.nameBottle = name;
      const cloneBottle = _.cloneDeep(state.customeBottle);
      setState({ customeBottle: cloneBottle });
      if (props.updateCustomeBottle) {
        props.updateCustomeBottle(cloneBottle);
      }
      return;
    }
    state.customeBottle.currentImg = image;
    state.customeBottle.nameBottle = name;
    const imagePremade = _.find(state.arrayThumbs, x => x.image === state.customeBottle.currentImg) ? _.find(state.arrayThumbs, x => x.image === state.customeBottle.currentImg).id : undefined;
    _.assign(state.customeBottle, { imagePremade });
    const cloneBottle = _.cloneDeep(state.customeBottle);
    setState({ customeBottle: cloneBottle });
    if (props.updateCustomeBottle) {
      props.updateCustomeBottle(cloneBottle);
    }
  };

  const onClickMoreInfo = async (data, isFull) => {
    try {
      dispatch(loadingPage(true));
      const result = await fetchProductDetail(data?.id);
      setIsFullDataScent(isFull);
      setDataModal(result);
    } catch (error) {
      toastrError(error.message);
    } finally {
      dispatch(loadingPage(false));
    }
  };

  const handleClick = (flag) => {
    setState({ isPerfumeSelected: flag });
    if (props.onChangeType) {
      props.onChangeType(flag);
    }
  };

  const onClickDeleteScent = async (listScent, id, name) => {
    try {
      const newList = getListScentDeleted(listScent, id);
      const listAvailable = _.filter(newList, x => !!x.id);
      if (listAvailable?.length > 0) {
        const listId = _.map(listAvailable, x => x.product.id);
        const link = getLinkWhenDeleteScent(listId.join(','), KEY_QUERY, name);
        history.push(`${window.location.pathname}?${KEY_QUERY}=${link}`);
      } else {
        const link = getLinkWhenDeleteScent('none', KEY_QUERY, name);
        history.push(`${window.location.pathname}?${KEY_QUERY}=${link}`);
      }
    } catch (error) {
      toastrError(error.message);
      dispatch(loadingPage(false));
    }
  };

  const onClickAddScent = (data) => {
    const lists = _.cloneDeep(state.listScent);
    const ele = lists[state.indexSelected];
    _.assign(ele, { id: data.id, product: { id: data.id } });
    const listAvailable = _.filter(lists, x => !!x.id);
    const listId = _.map(listAvailable, x => x.product.id);
    const link = getLinkWhenDeleteScent(listId.join(','), KEY_QUERY, props.name);
    setState({ isOpenSearch: false });
    history.push(`${window.location.pathname}?${KEY_QUERY}=${link}`);
  };

  const onClickOpenSelectScent = (data, index) => {
    if (data?.id) {
      setState({
        scentSelected: data?.product,
        isOpenSearch: true,
        indexSelected: index,
      });
    } else {
      setState({
        scentSelected: {},
        isOpenSearch: true,
        indexSelected: index,
      });
    }
  };

  useEffect(() => {
    setState({ isPerfumeSelected: props.isPerfumeSelected });
  }, [props.isPerfumeSelected]);

  useEffect(() => {
    setState({ customeBottle: props.customeBottle });
  }, [props.customeBottle]);

  useEffect(async () => {
    const infoSearch = pareQueryToObject(KEY_QUERY);
    if (!infoSearch[props.name] && props.data) {
      const listScent = getListScentsFromCombos(props.data.combo);
      const arrayThumbs = getImageCustomBottle(props.data);
      setState({
        listScent,
        arrayThumbs,
        data: props.data,
      });

      // fetch data to get data
      getProduct(listScent, 'dual_crayons').then((results) => {
        setState({
          dataDualCrayonApi: results,
        });
      }).catch((err) => {
        toastrError(err.message);
      });
    } else if (props.data && infoSearch[props.name] !== currentQuerySearch.current) {
      const clickHereBt = getNameFromButtonBlock(props.buttonBlocks, 'Click here to open ingredient menu');
      currentQuerySearch.current = infoSearch[props.name];
      if (currentQuerySearch.current === 'none') {
        // add scents missed
        const listScent = [{
          name: clickHereBt,
          isHiddenDelete: true,
          imageIcon: icProduct,
        }, {
          name: clickHereBt,
          isHiddenDelete: true,
          imageIcon: icProduct,
        }];
        setState({
          listScent,
          arrayThumbs: [],
          dataDualCrayonApi: {},
          data: {},
        });
      } else {
        const listId = _.map(currentQuerySearch.current?.split(','), d => ({
          product: {
            id: d,
          },
        }));
        try {
          dispatch(loadingPage(true));
          const results = await Promise.all([getProduct(listId, 'perfume'), getProduct(listId, 'dual_crayons')]);
          const products = await Promise.all([fetchProductDetail(results[0]?.id), fetchProductDetail(results[1]?.id)]);
          const listScent = getListScentsFromCombos(products[0]?.combo);
          const arrayThumbs = getImageCustomBottle(products[0]);

          // add scents missed
          if (listScent.length < 2) {
            listScent.push({
              name: clickHereBt,
              isHiddenDelete: true,
              imageIcon: icProduct,
            });
          }
          setState({
            listScent,
            arrayThumbs,
            dataDualCrayonApi: products[1],
            data: products[0],
          });
        } catch (error) {
          toastrError(error.message);
        } finally {
          dispatch(loadingPage(false));
        }
      }
    }
  }, [props.data, search]);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplaySpeed: 5000,
    pauseOnHover: false,
  };

  const slideScent = (
    <div className="div-slide-scent">
      <div className="div-scent-image">
        <ItemImageScent
          isRollOn={!state.isPerfumeSelected}
          data={state.data}
          onClickIngredient={() => {}}
          onClickImage={() => {}}
          isNightProd={false}
          customeBottle={state.customeBottle}
          cmsCommon={props.cmsCommon}
          isDisableNameScent={false}
          isNewInfo={false}
          isDisableImage
          disableInfo={props.disableInfo}
        />
      </div>
    </div>
  );

  const addToCart = getNameFromCommon(props.cmsCommon, 'ADD_TO_CART');
  const customeBt = getNameFromCommon(props.cmsCommon, 'CUSTOMIZE_BOTTLE');
  const strengthBt = getNameFromCommon(props.cmsCommon, 'STRENGTH');
  const durationBt = getNameFromCommon(props.cmsCommon, 'DURATION');
  const retakeBt = getNameFromCommon(props.cmsCommon, 'RETAKE_QUIZ');

  const st1Bt = getNameFromButtonBlock(props.buttonBlocks, '1st Ingredient');
  const nd2Bt = getNameFromButtonBlock(props.buttonBlocks, '2nd ingredient');
  const includingBt = getNameFromButtonBlock(props.buttonBlocks, 'Including');
  const choiseFormatBt = getNameFromButtonBlock(props.buttonBlocks, 'Choose your format');
  const customeYourBt = getNameFromButtonBlock(props.buttonBlocks, 'Customise your bottle');
  const yourChoseBt = getNameFromButtonBlock(props.buttonBlocks, 'Your Perfume Creation');

  const perfumeBottleBt = getNameFromButtonBlock(props.buttonBlocks, 'Perfume bottle - 30ml');
  const dualCrayonsBt = getNameFromButtonBlock(props.buttonBlocks, 'Dual crayon - 10ml');
  const currentItem = _.find(state.data?.items || [], x => (state.isPerfumeSelected ? x.is_sample === false : x.is_sample === true));


  const slideColor = (
    <div className="div-slide-color">
      <div className="div-process-line">
        {
          _.map(state.data?.profile?.accords || [], x => (
            <ProgressLine data={x} />
          ))
        }
      </div>
      <div className="div-process-circle">
        <ProgressCircle title={strengthBt} percent={parseInt(state.data?.profile ? state.data?.profile?.strength * 100 : 0, 10)} />
        <ProgressCircle title={durationBt} percent={parseInt(state.data?.profile ? state.data?.profile?.duration * 100 : 0, 10)} />
      </div>
    </div>
  );

  const headerStep = useCallback((step, title, disabled) => (
    <div className={classnames('header-step', disabled ? 'disabled' : '')}>
      <div className="step">
        {step}
      </div>
      <span>
        {title}
      </span>
    </div>
  ), []);

  const convertImage = customeBottle => (
    {
      image: customeBottle.currentImg,
      font: customeBottle.font,
      color: customeBottle.color,
      nameBottle: customeBottle.nameBottle,
      combos: _.filter(props.data?.combo || [], x => x.product?.type === 'Scent'),
    }
  );

  if (!props.isActive) {
    return null;
  }
  return (
    <React.Fragment>
      <div className={props.isQuizOutComeV3 ? 'div-card-perfume-v2' : 'div-card-perfume'}>
        {
          !_.isEmpty(dataModal) && (
            <ModalInfo
              isFullDataScent={isFullDataScent}
              onCloseModal={() => setDataModal(undefined)}
              isOpen={!_.isEmpty(dataModal)}
              dataModal={dataModal}
              onClick={onClickAddScent}
              slideColor={state.data?.profile?.accords ? slideColor : null}
            />
          )
        }
        {
          state.isOpenSearch && (
            <ModalPickScent
              isOpen={state.isOpenSearch}
              buttonBlocks={props.buttonBlocks}
              indexSelected={state.indexSelected}
              onCloseModal={() => setState({ isOpenSearch: false })}
              scentSelected={state.scentSelected}
              onClickScent={onClickAddScent}
            />
          )
        }
        {
            state.isShowCustom
              && (
                <CustomeBottleV3
                  cmsCommon={props.cmsCommon}
                  arrayThumbs={state.arrayThumbs}
                  handleCropImage={handleCropImage}
                  currentImg={state.customeBottle.currentImg}
                  nameBottle={state.customeBottle.nameBottle}
                  font={state.customeBottle.font}
                  color={state.customeBottle.color}
                  closePopUp={() => setState({ isShowCustom: false })}
                  onSave={onSaveCustomer}
                  itemBottle={state.data?.combo}
                  cmsTextBlocks={props.textBlock}
                  name1={state.listScent?.length > 0 ? state.listScent[0].name : ''}
                  name2={state.listScent?.length > 1 ? state.listScent[1].name : ''}
                />
              )
          }
        {
          (isMobile || props.isQuizOutComeV3) && (
            props.imageExternal ? (
              <div className="div-name-scents image-external div-col">
                <div className="sp1">
                  {props.isQuizOutComeV3 && <img src={props.icon} alt="icon" />}
                  {props.title}
                </div>
                <div className="div-row">
                  <img className="img-external" src={props.imageExternal} alt="icon" />
                  <span className="sp2">
                    {props.description}
                  </span>
                </div>

              </div>
            ) : (
              <div className="div-name-scents div-col">
                <div className="sp1">
                  {props.isQuizOutComeV3 && <img src={props.icon} alt="icon" />}
                  {props.title}
                </div>
                <span className="sp2">
                  {props.description}
                </span>
              </div>
            )
          )
        }

        {
          props.isQuizOutComeV3 ? (
            <div className="image-perfume">
              <ImageBlock
                images={props.listImage}
                datas={[]}
                // isActive={this.isActive}
                // onChangeProduct={this.props.onChangeProduct}
                dataCustom={convertImage(state.customeBottle)}
                buttonBlocks={props.buttonBlocks}
                // onClickImage={this.onClickImage}
                isSelectedScent
                isQuizPage
                className="quiz-page"
              />
            </div>
          ) : (
            <div className="swiper-perfume">
              <Slider {...settings}>
                {slideScent}
                {state.data?.profile?.accords ? slideColor : null}
              </Slider>
            </div>
          )
        }

        <div className="div-scents-result">
          {
            props.isQuizOutComeV3 ? (
              <ItemsListScent
                datas={state.listScent}
                title={yourChoseBt}
                subTitle={state.data?.name}
                buttonBlocks={props.buttonBlocks}
                onClickViewMore={() => { onClickMoreInfo(state.data, true); }}
                // onClickViewMore={() => props.onClickIngredient(d?.product?.ingredient)}
                isHiddenDelete
                onClickOpenSelectScent={() => {}}
                isVersion2={props.isQuizOutComeV3}
              />
            ) : (
              <React.Fragment>
                {
                  isBrowser && (
                    props.imageExternal ? (
                      <div className="div-name-scents image-external div-row">
                        <div className="div-col">
                          <span className="sp1">
                            {props.title}
                          </span>
                          <span className="sp2">
                            {props.description}
                          </span>
                        </div>
                        <img className="img-external" src={props.imageExternal} alt="icon" />
                      </div>
                    ) : (
                      <div className="div-name-scents div-col">
                        <span className="sp1">
                          {props.title}
                        </span>
                        <span className="sp2">
                          {props.description}
                        </span>
                      </div>
                    )
                  )
                }
              </React.Fragment>
            )
          }
          {
            props.isQuizOutComeV3 && headerStep(1, includingBt)
          }
          <div className="list-scents">
            {
              _.map(state.listScent || [], (d, index) => (
                <ItemsListScent
                  data={d}
                  subTitle={index === 0 ? st1Bt : nd2Bt}
                  buttonBlocks={props.buttonBlocks}
                  onClickViewMore={() => props.onClickIngredient(d?.product?.ingredient)}
                  onClickDelete={id => onClickDeleteScent(state.listScent, id, props.name)}
                  isHiddenDelete={d.isHiddenDelete || props.isQuizOutComeV3}
                  imageIcon={d.imageIcon}
                  onClickOpenSelectScent={onClickOpenSelectScent}
                  index={index}
                  isVersion3={props.isQuizOutComeV3}
                  dataGtmtracking="funnel-1-step-17-more-personality-info"
                />
              ))
            }
          </div>
          {
            props.isQuizOutComeV3 && headerStep(2, choiseFormatBt)
          }
          <div className="list-type">
            <ItemListType
              title={perfumeBottleBt}
              price={props.pricePerfume}
              buttonBlocks={props.buttonBlocks}
              selected={state.isPerfumeSelected}
              onClick={() => handleClick(true)}
              onClickMoreInfo={() => { onClickMoreInfo(state.data); }}
              isDisableViewMore={!state.data?.id}
              isVersion2={props.isQuizOutComeV3}
            />
            <ItemListType
              title={dualCrayonsBt}
              price={props.priceDualCrayons}
              buttonBlocks={props.buttonBlocks}
              selected={!state.isPerfumeSelected}
              onClick={() => handleClick(false)}
              onClickMoreInfo={() => { onClickMoreInfo(state.dataDualCrayonApi); }}
              isDisableViewMore={!state.dataDualCrayonApi?.id}
              isVersion2={props.isQuizOutComeV3}
            />
          </div>
          {
            props.isQuizOutComeV3 && headerStep(3, customeYourBt, !state.isPerfumeSelected)
          }
          {
            props.isQuizOutComeV3 && (
            <ItemBottle
              dataCustom={convertImage(state.customeBottle)}
              onClickCustom={() => setState({ isShowCustom: true })}
              disabled={!state.isPerfumeSelected}
              buttonBlocks={props.buttonBlocks}
            />
            )
          }
          <div className={classnames('list-button', props.isQuizOutComeV3 ? 'list-v2' : '')}>
            {
              state.isPerfumeSelected && !props.isQuizOutComeV3 && (
                <ButtonCT
                  name={customeBt}
                  disabled={!state.data?.id}
                  className="bt-out-line"
                  onClick={() => setState({ isShowCustom: true })}
                />
              )
            }
            {
              isMobile && props.isQuizOutComeV3 && (
                <ButtonCT
                  dataGtmtracking="funnel-1-step-19-retake-quiz"
                  className="bt-icon-left button-bg__none"
                  name={retakeBt}
                  iconLeft={icTakeBlack}
                  onClick={props.onClickRetake}
                />
              )
            }
            <ButtonCT
              name={addToCart}
              className="bt-bg-cart"
              disabled={state.isPerfumeSelected ? !state.data?.id : !state.dataDualCrayonApi?.id}
              onClick={() => props.onClickAddToCart(currentItem, props.description, state.customeBottle, state.data)}
            />
          </div>
        </div>
      </div>
      {props.isQuizOutComeV3 && <BlockVideoQuiz data={state.data} cmsCommon={props.cmsCommon} />}
    </React.Fragment>
  );
}

export default CardPerfume;
