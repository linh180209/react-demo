import classnames from 'classnames';
import React from 'react';
import icDelete from '../../../image/icon/ic-delete-result.svg';
import icInfo from '../../../image/icon/ic-next-quizv2.svg';
import icSwitch from '../../../image/icon/ic-switch-quizv2.svg';
import icInfoQuiz from '../../../image/icon/ic-info-quiz.svg';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import { getImageType } from './handler';
import './itemListScent.scss';

function ItemsListScent(props) {
  const viewMoreBt = getNameFromButtonBlock(props.buttonBlocks, 'view info');
  const changeBt = getNameFromButtonBlock(props.buttonBlocks, 'CHANGE');
  const InfoBt = getNameFromButtonBlock(props.buttonBlocks, 'Info');
  return (
    <div
      data-gtmtracking={props.dataGtmtracking}
      onClick={() => props.onClickOpenSelectScent(props.data, props.index)}
      className={classnames('item-list-scent', props.imageIcon ? 'no-scent' : '', props.isVersion2 ? 'item-v2' : '', props.isVersion3 ? 'item-v3' : '')}
    >
      {
        props.isVersion2 ? (
          <div className="div-image">
            <img
              src={getImageType(props.datas[0]?.product?.images, 'unisex')?.image}
              alt="icon"
            />
            <img
              src={getImageType(props.datas[1]?.product?.images, 'unisex')?.image}
              alt="icon"
            />
          </div>
        ) : (
          <img
            src={props.imageIcon || getImageType(props.data?.product?.images, 'unisex')?.image}
            alt="icon"
          />
        )
      }

      <div className="text">
        <div className="div-row">
          <span>
            {props.title || props.data?.name}
            {
              props.isVersion3 && (
                <button
                  type="button"
                  className="button-bg__none ml-3"
                  onClick={(e) => { e.stopPropagation(); props.onClickViewMore(); }}
                >
                  <img src={icInfoQuiz} alt="icon" />
                </button>
              )
            }
          </span>
          {
            !props.isHiddenDelete && (
              <button
                onClick={(e) => { e.stopPropagation(); props.onClickDelete(props.data?.id); }}
                type="button"
                className="button-bg__none"
              >
                <img src={icDelete} alt="delete" />
              </button>
            )
          }
          {
            props.isVersion3 && (
              <button
                type="button"
                className="button-bg__none bt-change"
                onClick={() => props.onClickOpenSelectScent(props.data, props.index)}
              >
                {changeBt}
                <img src={icSwitch} alt="icon" />
              </button>
            )
          }
        </div>
        <h4 className="header_4">
          {props.subTitle}
          {
            props.isVersion2 && (
              <button
                type="button"
                className="button-bg__none ml-3"
                onClick={(e) => { e.stopPropagation(); props.onClickViewMore(); }}
              >
                <img src={icInfoQuiz} alt="icon" />
              </button>
            )
          }
        </h4>
      </div>
      {
        !props.imageIcon && !props.isVersion3 && (
          props.isVersion2 ? (
            // <button
            //   type="button"
            //   className="button-bg__none bt-info"
            //   onClick={(e) => { e.stopPropagation(); props.onClickViewMore(); }}
            // >
            //   {InfoBt}
            //   <img src={icInfo} alt="info" />
            // </button>
            null
          ) : (
            <button
              onClick={(e) => { e.stopPropagation(); props.onClickViewMore(); }}
              className="button-bg__none view-more"
              type="button"
            >
              <span>
                {viewMoreBt}
              </span>
            </button>
          )
        )
      }

    </div>
  );
}

export default ItemsListScent;
