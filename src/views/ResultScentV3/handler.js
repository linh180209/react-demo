import _ from 'lodash';
import { GET_ALL_SCENTS_PRODUCTS_PERFUME, GET_FAQ_QUESTION, GET_SCENT_LIB } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';

export const fetchAllScent = async () => {
  const option = {
    url: GET_ALL_SCENTS_PRODUCTS_PERFUME,
    method: 'GET',
  };
  return fetchClient(option);
};

export const fetchDataFAQ = async (type) => {
  const option = {
    url: GET_FAQ_QUESTION.replace('{type}', type),
    method: 'GET',
  };
  return fetchClient(option);
};

export const getImageFromFAQ = (data, isPerfume) => {
  const { images, videos } = data;
  const listImage = isPerfume ? [{
    dataCustomBottle: {},
  }] : [];
  _.forEach(images, (x) => {
    if (x.type === null) {
      listImage.push({
        original: x.image,
        thumbnail: x.image,
      });
    }
  });
  _.forEach(videos, (x) => {
    listImage.push({
      original: x.placeholder,
      thumbnail: x.placeholder,
      embedUrl: x.video,
    });
  });
  return listImage;
};
