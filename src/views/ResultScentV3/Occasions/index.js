import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import './styles.scss';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';

function Occasions(props) {
  const discoverBt = getNameFromButtonBlock(props.buttonBlocks, 'Discover your perfume matches');
  const topMatchBt = getNameFromButtonBlock(props.buttonBlocks, 'TOP MATCH');
  return (
    <div className="div-occasions">
      <h2>
        {discoverBt}
      </h2>
      <div className="list-item-occasion">
        {
          _.map(props.listProducts || [], (d, index) => (
            <div
              onClick={() => props.onClick(d.indexMix)}
              className={classnames('item-occasion', props.mixSelected === d.indexMix ? 'active' : '', index === 0 ? 'selected' : '')}
            >
              {
                index === 0 && (
                  <div className="title">
                    {topMatchBt}
                  </div>
                )
              }
              <div className="content">
                <img src={d.icon} alt="icon" />
                <div className="name">
                  <span>{d.title}</span>
                  {d.subTitle && <span>{d.subTitle}</span>}
                </div>
              </div>
            </div>
          ))
        }
      </div>
    </div>
  );
}

export default Occasions;
