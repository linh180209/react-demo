import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

const RenderItem = (props) => {
  const { item } = props;
  const [percent, setPercent] = useState(0);
  useEffect(() => {
    setTimeout(() => {
      setPercent(item.score);
    }, 1000);
  });
  return (
    <div className="__list-item">
      <div className="_list-item-progress">
        <div
          className="_list-item-progress-percentage"
          style={{
            width: `${percent}%`,
            backgroundColor: `${item.color}`,
          }}
        >
          <span
            className="_list-item-progress-dot"
            style={{
              backgroundColor: `${item.color}`,
            }}
          />
        </div>
      </div>
      <div className="_list-item-content d-flex justify-content-start">
        <span className="__title">
          {item.name}
        </span>
      </div>
    </div>
  );
};

function RenderItemSlider(props) {
  return (
    <div className="item-slider div-col">
      <div className="__header d-flex justify-content-center">
        {props.title}
      </div>
      <div className="__list">
        {
          _.map(props.listItems || [], item => (
            <RenderItem item={item} />
          ))
        }
      </div>
    </div>
  );
}

RenderItemSlider.defaultProps = {
  title: '',
  listItems: [],
};

RenderItemSlider.propTypes = {
  title: PropTypes.string,
  listItems: PropTypes.arrayOf(),
};

export default RenderItemSlider;
