import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import updateRefRequest from '../Redux/Actions/refPoint';
import { generateUrlWeb, gotoShopHome } from '../Redux/Helpers';

class PointRedeem extends Component {
  componentDidMount = () => {
    const { match } = this.props;
    const { params } = match;
    if (params.ref) {
      this.props.updateRefRequest(params.ref);
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    }
  };

  render() {
    return (
      <div />
    );
  }
}

PointRedeem.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      ref: PropTypes.string,
    }),
  }).isRequired,
  updateRefRequest: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  updateRefRequest,
};

export default withRouter(connect(null, mapDispatchToProps)(PointRedeem));
