/* eslint-disable react/sort-comp */
/* eslint-disable no-shadow */
import React, { Component } from 'react';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import smoothscroll from 'smoothscroll-polyfill';
import queryString from 'query-string';

import fetchClient from '../../Redux/Helpers/fetch-client';
import {
  scrollTop, fetchCMSHomepage, getCmsCommon, googleAnalitycs, generateHreflang, generateUrlWeb, removeLinkHreflang, gotoShopHome,
} from '../../Redux/Helpers';
import {
  CALL_UNSUBCRIBE,
} from '../../config';
import HeaderHomePage from '../../components/HomePage/header';
import addCmsRedux from '../../Redux/Actions/cms';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import BlockCarousel from '../../components/HomeAlt/BlockCarousel';
import BlockVideo from '../../components/HomeAlt/BlockVideo';
import CTABlockBig from '../../components/HomeAlt/CTABlockBig';
import BlockProduct from '../../components/HomeAlt/BlockProduct';
import Testimonials from '../../components/HomeAlt/Testimonials';
import BlockStepItem from '../../components/HomeAlt/BlockStep';
import BlockWorkShop from '../../components/HomeAlt/BlockWorkShop';
import BlockIcon from '../../components/HomeAlt/BlockIcon';
import BlockSaying from '../../components/HomeAlt/BlockSaying';
import PopUpLandingPage from './popUpLandingPage';
import BlockIconHeader from '../../components/HomeAlt/BlockIconHeader';

import { createBasketGuest, addProductBasket } from '../../Redux/Actions/basket';
import loadingPage from '../../Redux/Actions/loading';
import AskCookies from '../../components/AskCookies';
import auth from '../../Redux/Helpers/auth';
import logo from '../../image/logomark.png';
import FooterV2 from '../../views2/footer';

class HomeAlt extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAskCookies: false,
      homePageCms: undefined,
      isOpenPopup: false,
    };
  }

  componentDidMount() {
    // this.addScript1();
    this.addScript2();
    if (this.props.history.location.pathname === '/subscribers/unsub/') {
      const { email } = queryString.parse(this.props.location.search);
      fetchClient({
        url: CALL_UNSUBCRIBE.replace('{email}', email),
        method: 'GET',
      }).then((result) => {
        if (result.message === 'ok') {
          toastrSuccess('Unsubcribe Successful');
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        } else {
          toastrError('Unsubcribe Unsuccessful');
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        }
      }).catch((error) => {
        toastrError('Unsubcribe Unsuccessful');
        // this.props.history.push(generateUrlWeb('/'));
        gotoShopHome();
      });
    }
    smoothscroll.polyfill();
    googleAnalitycs('/');
    this.setState({ isAskCookies: auth.getAskCookies() });
    this.fetchData();
    this.props.loadingPage(false);
    this.addChat();
    const values = queryString.parse(this.props.location.search);
    const { anchor } = values;
    if (anchor) {
      this.gotoAnchor(anchor);
    } else {
      scrollTop();
    }
    if (!auth.getPopupLandingPage()) {
      setTimeout(() => {
        this.onOpenPopup();
      }, 1000 * 15);
    }
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  componentDidUpdate = (preProp) => {
    const { location } = this.props;
    if (location && location.search && location.search !== preProp.location.search) {
      const values = queryString.parse(location.search);
      const { anchor } = values;
      if (anchor) {
        this.gotoAnchor(anchor);
      }
    }
  }

  gotoAnchor = (name) => {
    setTimeout(() => {
      const ele = document.getElementById(`id-${name}`);
      if (ele) {
        console.log('gotoAnchor 1');
        ele.scrollIntoView({ behavior: 'smooth' });
        window.scrollTo(0, 500);
      }
    }, 5000);
  }

  onClickAskCookies = () => {
    this.setState({ isAskCookies: true });
    auth.setAskCookies();
  }

  addChat = () => {
    const s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.innerHTML = `
        window.intercomSettings = {
          app_id: "i20hnofe"
        };
        (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',w.intercomSettings);}else{var d=document;var i=function(){i.c(arguments);};i.q=[];i.c=function(args){i.q.push(args);};w.Intercom=i;var l=function(){var s=d.createElement('script');s.type='text/javascript';s.async=true;s.src='https://widget.intercom.io/widget/i20hnofe';var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);};if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
      `;
    document.body.appendChild(s);
  }

  addScript1 = () => {
    const s = document.createElement('script');
    s.type = 'application/ld+json';
    s.async = true;
    s.innerHTML = `
    {
      "@context": "http://schema.org",
      "@type": "LocalBusiness",
      "description": "Create Customised Perfume and Fragrances",
      "name": "Maison 21G",
      "telephone": "(65) 6226-2677",
      "image": "https://lh5.googleusercontent.com/-GnGYuKP5WZE/AAAAAAAAAAI/AAAAAAAAAAA/VShOWQKHt98/s55-p-k-no-ns-nd/photo.jpg",
      "address": {
      "@type": "PostalAddress",
      "addressLocality": "Tanjong Pagar",
      "addressCountry": "SG",
      "postalCode":"089536",
      "streetAddress": "77 Duxton Rd"
      },
      "openingHours": "Mo,Tu,We,Th,Fr,Sa 12:00-20:00",
      "geo": {
      "@type": "GeoCoordinates",
      "latitude": "1.278448",
      "longitude": "103.843149"
      },
       "aggregateRating": {
          "@type": "AggregateRating",
          "ratingValue": "4.9",
          "reviewCount": "114"
        },
      "sameAs" : [ "https://www.facebook.com/Maison21G", 
      "https://www.instagram.com/maison21g/"]
      }      
      `;
    document.body.appendChild(s);
  }

  addScript2 = () => {
    const s = document.createElement('script');
    s.type = 'application/ld+json';
    s.async = true;
    s.innerHTML = `
    { 
      "@context": "http://schema.org", 
      "@type": "WebSite", 
      "url": "https://maison21g.com/", 
      "name": "Maison 21G",
      "description": "Create Customised Perfume and Fragrances",
      "potentialAction": { 
        "@type": "SearchAction", 
        "target": "https://maison21g.com/?s={search_term}", 
        "query-input": "required name=search_term" } 
        }       
      `;
    document.body.appendChild(s);
  }

  fetchData = async () => {
    const { cms } = this.props;
    const homePageCms = _.find(cms, x => x.title === 'Home Alt');
    if (!homePageCms) {
      try {
        const result = await fetchCMSHomepage('home-alt');
        this.props.addCmsRedux(result);
        this.setState({ homePageCms: result });
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      this.setState({ homePageCms });
    }
  }

  onOpenPopup = () => {
    this.setState({ isOpenPopup: true });
  }

  onClosePopup = () => {
    auth.setPopupLandingPage();
    this.setState({ isOpenPopup: false });
  }

  render() {
    const {
      history, login, basket, addProductBasket, createBasketGuest, cms, countries,
    } = this.props;

    const metaHtml = (
      <MetaTags>
        <title>
          Customise your own perfume | House of Scent Designers | Maison 21G
        </title>
        <meta name="description" content="Create your customised perfume at Maison21G. Explore our workshops for friends, bridal parties, corporate events, families. Discover our story, open your senses" />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 months" />
        <meta property="og:image" content={logo} />
        {generateHreflang(countries, '/')}
      </MetaTags>
    );

    const { isAskCookies, homePageCms, isOpenPopup } = this.state;
    if (!homePageCms) {
      return (
        <div />
      );
    }

    const { isShowLogin } = this.props.location;
    let isPositionRight = false;
    const cmsCommon = getCmsCommon(cms);
    const itemPopUpBlock = _.find(homePageCms.body, x => x.type === 'item_popup_block');
    return (
      <div className="div-col justify-center items-center">
        {metaHtml}
        <HeaderHomePage isShowLogin={isShowLogin} />
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        {
          isOpenPopup && itemPopUpBlock ? (
            <PopUpLandingPage onClose={this.onClosePopup} cmsCommon={cmsCommon} itemPopUpBlock={itemPopUpBlock} createBasketGuest={createBasketGuest} addProductBasket={addProductBasket} basket={basket} history={history} />
          ) : (<div />)
        }

        {
          _.map(homePageCms.body, (d, index) => {
            switch (d.type) {
              case 'carousel_block':
                return <BlockCarousel heroCms={d} history={history} login={login} index={index} />;
              case 'banner_block':
              {
                isPositionRight = !isPositionRight;
                return <BlockVideo heroCms={d} history={history} login={login} isPositionRight={isPositionRight} index={index} />;
              }
              case 'featured_apps_block':
                return <CTABlockBig heroCms={d} history={history} login={login} isLanding index={index} />;
              case 'featured_products_block':
                return <BlockProduct heroCms={d} history={history} login={login} basket={basket} createBasketGuest={createBasketGuest} addProductBasket={addProductBasket} cmsCommon={cmsCommon} index={index} />;
              case 'steps_block':
                return <BlockStepItem heroCms={d} history={history} login={login} index={index} />;
              case 'feedbacks_block':
                return <BlockSaying heroCms={d} history={history} login={login} index={index} />;
              case 'atelier_list_block':
                return <BlockWorkShop heroCms={d} history={history} index={index} />;
              case 'benefits_block':
                if (d.value.image_background.caption === 'block-icon-header') {
                  return <BlockIconHeader heroCms={d} />;
                }
                return <BlockStepItem heroCms={d} history={history} login={login} index={index} isBenefitsBlock />;
              case 'icons_block':
                return <BlockIcon heroCms={d} />;
              case 'blogs_block':
                return <Testimonials heroCms={d} />;
              default:
                break;
            }
          })
        }

        <FooterV2 />
        {
          isAskCookies ? <div /> : <AskCookies onClick={this.onClickAskCookies} />
        }
      </div>
    );
  }
}


HomeAlt.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
  location: PropTypes.shape(PropTypes.object).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  basket: PropTypes.shape(PropTypes.object).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    login: state.login,
    basket: state.basket,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  addProductBasket,
  createBasketGuest,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomeAlt));
