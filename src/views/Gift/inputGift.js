import React, { Component } from 'react';
import {
  Input,
} from 'reactstrap';
import _ from 'lodash';
import 'react-datepicker/dist/react-datepicker.css';
import InputMask from 'react-input-mask';
import DatePicker from 'react-datepicker';
import { generaCurrency } from '../../Redux/Helpers';
import icUpload from '../../image/icon/ic-upload-more.svg';
import { isMobile } from '../../DetectScreen';

class InputGift extends Component {
  onChange = (e) => {
    const { value, name } = e.target;
    this.props.onChange(value, name);
  }

  onChangeDate = (value) => {
    this.props.onChange(value, this.props.name);
  }

  render() {
    const {
      placeholder, title, type, value, name, options, dataCustom, buttonName,
    } = this.props;
    const forShippingBt = 'For Shipping';
    return (
      <div className="div-input-gift">
        <span>
          {title}
          :
        </span>
        <div className="div-input">
          {
            type === 'DROPDOWN' ? (
              <Input
                type="select"
                className="w-100"
                placeholder="Country"
                name={name}
                onChange={this.onChange}
              >
                {
                _.map(options, x => (
                  <option value={x.country.code.toLowerCase()} selected={x.country.code.toLowerCase() === value.toLowerCase()}>
                    {`${x.country.name} (+ ${generaCurrency(x.shipping)} ${forShippingBt} )`}
                  </option>
                ))
              }
              </Input>
            ) : type === 'DATE-PICKER' ? (
              <DatePicker
                dateFormat="dd/MM/yyyy"
                minDate={new Date()}
                className="input-border w-100"
                onChange={this.onChangeDate}
                selected={value}
              />
            ) : type === 'IMAGE' ? (
              <div className="div-input-image">
                <div className={dataCustom.nameBottle || dataCustom.currentImg ? 'div-left-input' : 'hidden'}>
                  <img loading="lazy" src={dataCustom.currentImg} alt="cutome" />
                  <span>{dataCustom.nameBottle || 'My Perfume'}</span>
                </div>
                <button type="button" onClick={this.props.onClick}>
                  <img src={icUpload} alt="upload" />
                  <span className={isMobile && (dataCustom.nameBottle || dataCustom.currentImg) ? 'hidden' : ''}>
                    {buttonName}
                  </span>
                </button>
              </div>
            ) : type === 'CURRENTCY' ? (
              <InputMask
                className="w-100"
                type="text"
                name={name}
                placeholder={placeholder}
                onChange={this.onChange}
                maskChar={null}
                value={value}
                mask="99999999999999999999"
              />
            ) : (
              <input
                type="text"
                onChange={this.onChange}
                placeholder={placeholder}
                multiple
                value={value}
                name={name}
              />
            )
          }
        </div>

      </div>
    );
  }
}

export default InputGift;
