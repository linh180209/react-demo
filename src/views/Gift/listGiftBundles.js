import React, { Component } from 'react';
import _ from 'lodash';
import { Col, Row } from 'reactstrap';
import { generaCurrency, getAltImageV2, getNameFromButtonBlock } from '../../Redux/Helpers';
import icChecked from '../../image/icon/check-yellow.svg';
import icInfo from '../../image/icon/testMore.png';
import { isBrowser, isMobile } from '../../DetectScreen';

export class ItemProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isHover: false,
    };
  }

  onMouseEnter = () => {
    this.setState({ isHover: true });
  }

  onMouseLeave = () => {
    this.setState({ isHover: false });
  }

  onClickAddTocart = () => {
    const { basket, product } = this.props;
    const {
      id, name, price, is_featured: isFeatured,
    } = product.product.items[0];
    const data = {
      item: id,
      price,
      quantity: 1,
      name,
      is_featured: isFeatured,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    if (!basket.id) {
      this.props.createBasketGuest(dataTemp);
    } else {
      this.props.addProductBasket(dataTemp);
    }
  }

  render() {
    const { isHover } = this.state;
    const { product, basket, buttonBlocks } = this.props;
    const {
      image, price, product: productItem, text,
    } = product;
    const { items } = basket || { items: [] };
    const isAddedCart = _.find(items, x => x.item.id === productItem.id);
    const addToCartBt = getNameFromButtonBlock(buttonBlocks, 'add_to_cart');
    const imageDisplay = _.find(productItem.images, d => d.type === 'main');
    return (
      <div
        className="div-product-gift-item"
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <div className="div-image">
          <img loading="lazy" src={imageDisplay.image} alt={getAltImageV2(imageDisplay)} onClick={() => this.props.onClickGotoProduct(productItem)} />
        </div>
        <span className={(isAddedCart || isHover) && isBrowser ? 'hidden' : 'name'}>
          {productItem.name}
        </span>
        <button
          type="button"
          onClick={isAddedCart ? undefined : this.onClickAddTocart}
          className={(isAddedCart || isHover) && isBrowser ? 'add-cart' : 'hidden'}
        >
          <img loading="lazy" src={icChecked} alt="checked" className={isAddedCart ? '' : 'hidden'} />
          {`${isAddedCart ? '' : '+'} ${addToCartBt}`}
        </button>
        <span className="info">
          {text}
        </span>
        <div className="price">
          <span>
            {generaCurrency(productItem.items[0].price)}
          </span>
          {/* {
            isMobile && (
              <span>
                {generaCurrency(productItem.items[0].price)}
              </span>
            )
          } */}
        </div>
        {/* <div className="price-sale-off div-row">
          {
            isBrowser && (
              <span>
                {generaCurrency(productItem.items[0].price)}
              </span>
            )
          }
          <span>
            -
            {100 - parseInt((price / productItem.items[0].price * 100), 10)}
            %
          </span>
        </div> */}
        <button
          onClick={isAddedCart ? undefined : this.onClickAddTocart}
          type="button"
          className={isMobile ? 'add-cart' : 'hidden'}
        >
          <img src={icChecked} alt="checked" className={isAddedCart ? '' : 'hidden'} />
          {`${isAddedCart ? '' : '+'} ${addToCartBt}`}
        </button>
      </div>
    );
  }
}

class ListGiftBundles extends Component {
  render() {
    const { datas } = this.props;
    return (
      <div className="gift-bundles">
        <Row>
          {
          _.map(datas, x => (
            <Col md="4" xs="6" className="col-item-product">
              <ItemProduct
                product={x.value}
                basket={this.props.basket}
                addProductBasket={this.props.addProductBasket}
                createBasketGuest={this.props.createBasketGuest}
                loadingPage={this.props.loadingPage}
                onClickGotoProduct={this.props.onClickGotoProduct}
                buttonBlocks={this.props.buttonBlocks}
              />
            </Col>
          ))
        }
        </Row>
      </div>
    );
  }
}

export default ListGiftBundles;
