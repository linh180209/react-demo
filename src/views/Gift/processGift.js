import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import smoothscroll from 'smoothscroll-polyfill';
import { scrollTop } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

class ProcessGift extends Component {
  data = [
    'HOW TO GIFT',
    'CHOOSE GIFT',
    'FILL IN DETAILS',
  ]

  componentDidMount() {
    smoothscroll.polyfill();
  }

  onClickGoto = (index) => {
    if (index === 0) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    } else if (index === 1) {
      const element = document.getElementById('id_gift');
      if (element) {
        element.scrollIntoView({ behavior: 'smooth' });
      }
    } else if (index === 2) {
      const element = document.getElementById('id_your');
      if (element) {
        element.scrollIntoView({ behavior: 'smooth' });
      }
    }
  }

  onClickGotoMenu = (index) => {
    this.props.onClickGotoMenu(index);
  }

  render() {
    const { step } = this.props;
    const htmlMobile = (
      <div className="div-fixed-header">
        <div className="div-process-gift-mobile div-row justify-between">
          <div
            className="div-item div-col justify-center items-center"
            onClick={() => {
              if (step - 1 >= 0) {
                this.onClickGotoMenu(0);
              }
            }}
          >
            <div className="icon div-col justify-center items-center">
              <span>
                <i className="fa fa-check" />
              </span>
            </div>
            <div className="text div-col justify-center items-center mt-2">
              <span>
                HOW TO GIFT
              </span>
            </div>
          </div>
          <div className="div-item div-col div-col justify-center items-center">
            <div
              className="icon div-col justify-center items-center"
              onClick={() => {
                if (step - 1 >= 1) {
                  this.onClickGotoMenu(1);
                }
              }}
            >
              <span>
                {
                  step > 2 ? (<i className="fa fa-check" />) : (2)
                }
              </span>
            </div>
            <div
              className="text div-col justify-center items-center mt-2"
              onClick={() => {
                if (step - 1 >= 2) {
                  this.onClickGotoMenu(2);
                }
              }}
            >
              <span>
                CHOOSE GIFT
              </span>
            </div>
          </div>
          <div
            className="div-item div-col div-col justify-center items-center"
            onClick={() => {
              if (step - 1 >= 1) {
                this.onClickGotoMenu(1);
              }
            }}
          >
            <div className={`${step < 3 ? 'in-active icon div-col div-col justify-center items-center' : 'icon div-col div-col justify-center items-center'}`}>
              <span>
                {
                  step > 3 ? (<i className="fa fa-check" />) : (3)
                }
              </span>
            </div>
            <div className="text div-col justify-center items-center mt-2">
              <span>
                FILL IN DETAILS
              </span>
            </div>
          </div>
          <div className={`${step < 3 ? 'line-process-1 active' : 'line-process-1'}`} />
          <div className={`${step < 4 ? 'line-process-2 active' : 'line-process-2'}`} />
        </div>
      </div>

    );
    const htmlWeb = (
      <div className="div-process-gift div-col">
        {
          _.map(this.data, (d, index) => (
            <React.Fragment>
              <div className="div-content div-row items-center">
                <div
                  className={`${step - 1 >= index ? 'div-index div-col justify-center items-center active' : 'div-index div-col justify-center items-center'}`}
                  onClick={() => {
                    if (step - 1 >= index) {
                      this.onClickGoto(index);
                    }
                  }}
                >
                  <span>
                    {
                      (step - 1) > index ? (<i className="fa fa-check" />) : (index)
                    }
                  </span>
                </div>
                <span className="name ml-3">
                  {d}
                </span>
              </div>
              <div className={`${index === this.data.length - 1 ? 'hidden' : index < (step - 1) ? 'line active' : 'line'}`} />
            </React.Fragment>
          ))
        }
      </div>
    );
    return (
      <React.Fragment>
        {
          isMobile ? htmlMobile : htmlWeb
        }
      </React.Fragment>
    );
  }
}

ProcessGift.propTypes = {
  step: PropTypes.number.isRequired,
  onClickGotoMenu: PropTypes.func.isRequired,
};

export default ProcessGift;
