Headerimport React, { Component } from 'react';
import PropTypes from 'prop-types';
import smoothscroll from 'smoothscroll-polyfill';
import ReactHtmlParser from 'react-html-parser';
import { getAltImageV2 } from '../../Redux/Helpers';

class HeaderGift extends Component {
  onClickScoll = () => {
    const element = document.getElementById('id-scroll');
    if (element) {
      element.scrollIntoView({ behavior: 'smooth' });
      smoothscroll.polyfill();
    }
  }

  render() {
    const { cms } = this.props;
    return (
      <div
        style={{
          width: '100vw',
          marginTop: '70px',
        }}
        className="div-hearder-gift height_home_page div-col justify-center items-center"
      >
        <div
          className="div-col justify-between items-center"
        >
          <img
            loading="lazy"
            src={cms ? cms.value.image.image : ''}
            style={{
              height: 'calc(100vh-70px)',
              width: '100vw',
              position: 'absolute',
              zIndex: '-1',
              objectFit: 'cover',
              top: '0px',
            }}
            className="height_home_page"
            alt={getAltImageV2(cms ? cms.value.image : undefined)}
          />
          <div className="header div-col justify-center item-center">
            <h2>
              {cms ? cms.value.header.header_text : ''}
            </h2>
            <span>
              GIFT A MAISON21G PORDUCT TO YOUR LOVED ONES
            </span>
          </div>
          <div className="description">
            {cms ? ReactHtmlParser(cms.value.description) : ''}
          </div>
          <div className="div-button">
            <button
              type="button"
              className="bt-checkout bt-header"
              onClick={this.onClickScoll}
            >
              {cms ? cms.value.button.text : ''}
            </button>
          </div>
          {/* <div className={`${isMobile ? 'w-90' : 'w-70'} div-col justify-center items-center`}>
            <span className="text-title" style={{ color: '#000000' }}>
              {cms ? cms.value.header.header_text : ''}
            </span>
            <span style={{ color: '#3D3D3D', marginTop: '20px', textAlign: 'center' }}>
              {cms ? ReactHtmlParser(cms.value.description) : ''}
            </span>
          </div>
          <div
            style={{
              height: '1rem',
              width: '1px',
              background: '#000000',
              marginTop: '50px',
              marginBottom: '50px',
            }}
          />
          <button
            type="button"
            className="button-bg__none"
            onClick={this.onClickScoll}
          >
            {cms ? cms.value.button.text : ''}
          </button> */}
        </div>
      </div>
    );
  }
}

HeaderGift.propTypes = {
  cms: PropTypes.shape(PropTypes.object).isRequired,
};

export default HeaderGift;
