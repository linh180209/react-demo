import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Row, Col, Label, Input,
} from 'reactstrap';
import _ from 'lodash';
import InputMask from 'react-input-mask';
import moment from 'moment';
import * as EmailValidator from 'email-validator';
import ReactHtmlParser from 'react-html-parser';
import DatePicker from 'react-datepicker';

import ProcessGift from './processGift';
import auth from '../../Redux/Helpers/auth';
import {
  getCmsCommon, getNameFromCommon, onClickLink, generaCurrency,
} from '../../Redux/Helpers';
import icHome from '../../image/icon/ic-home.svg';
import { toastrError } from '../../Redux/Helpers/notification';

import 'react-datepicker/dist/react-datepicker.css';
import { isMobile } from '../../DetectScreen';

class FillDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowTheir: false,
      isCheckBottle: false,
      listError: [],
    };
  }

  onChangeDate = (value) => {
    const { info } = this.props;
    info.their.dateSent = value;
    this.forceUpdate();
  }

  onChange = (e) => {
    const { value, name } = e.target;
    const { info } = this.props;
    switch (name) {
      case 'firtName':
        info.your.firtName = value;
        break;
      case 'lastName':
        info.your.lastName = value;
        break;
      // case 'email':
      //   info.your.email = value;
      //   break;
      case 'priceCustom':
        info.priceCustom = value.replace(/\D/, '');
        if (info.priceCustom) {
          info.indexVoucher = _.findIndex(this.props.giftChoise.value.product.items, x => x.price === '0.00');
        } else {
          info.indexVoucher = _.findIndex(this.props.giftChoise.value.product.items, x => x.price !== '0.00');
        }
        this.forceUpdate();
        break;
      case 'their_firtName':
        info.their.firtName = value;
        break;
      case 'their_lastName':
        info.their.lastName = value;
        break;
      case 'their_email':
        info.their.email = value;
        break;
      case 'country':
        info.their.country = value;
        this.forceUpdate();
        break;
      case 'message':
        info.their.message = value;
        break;
      case 'dateSent':
        info.their.dateSent = value;
        break;
      default:
        break;
    }
    this.setState({ listError: [] });
  }

  onClickCheckBox = (e) => {
    const { checked } = e.target;
    this.setState({ isCheckBottle: checked });
    this.props.info.isCustomBottle = checked;
  }

  onChangeVoucher = (index) => {
    const { info } = this.props;
    info.indexVoucher = index;
    info.priceCustom = '';
    this.forceUpdate();
  }

  onClickContinue = () => {
    const { info } = this.props;
    const { listError } = this.state;
    const fieldError = [];
    if (!info.your.firtName) {
      fieldError.push('First Name');
      listError.push('firtName');
    }
    if (!info.your.lastName) {
      fieldError.push('Last Name');
      listError.push('lastName');
    }
    if (fieldError.length > 0) {
      toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
      this.setState({ listError });
      return;
    }
    this.setState({ isShowTheir: true });
  }

  onClickAddCart = () => {
    const { info, giftChoise } = this.props;
    const { listError } = this.state;
    const fieldError = [];
    if (!info.their.firtName) {
      fieldError.push('First Name');
      listError.push('their_firtName');
    }
    if (!info.their.lastName) {
      fieldError.push('Last Name');
      listError.push('their_lastName');
    }
    if (!info.their.email) {
      fieldError.push('Email');
      listError.push('their_email');
    } else if (!EmailValidator.validate(info.their.email)) {
      fieldError.push('Email');
      listError.push('their_email');
    }
    if (!info.their.dateSent) {
      fieldError.push('Delivery Time');
      listError.push('dateSent');
    }
    if (fieldError.length > 0) {
      toastrError(`Please enter their ${_.join(fieldError, ', ')}`);
      this.setState({ listError });
      return;
    }
    const dateSentEpoch = moment(info.their.dateSent, 'DD/MM/YYYY');
    if (dateSentEpoch.startOf('day').valueOf() < moment().startOf('day').valueOf()) {
      toastrError('Date must be in the future or today');
      listError.push('dateSent');
      this.setState({ listError });
      return;
    }
    this.props.onClickAddCart();
  }

  render() {
    const {
      cms, cmsAll, giftChoise, listShipping, info, bottle, history, tBlock,
    } = this.props;
    const { isShowTheir, isCheckBottle, listError } = this.state;
    const cmsCommon = getCmsCommon(cmsAll);
    const chooseBt = getNameFromCommon(cmsCommon, 'YOUR_CHOOSEN_GIFT');
    const continueBt = getNameFromCommon(cmsCommon, 'CONTINUE');
    const addToCartBt = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
    const forShippingBt = getNameFromCommon(cmsCommon, 'for_shipping');
    const firstNameBt = getNameFromCommon(cmsCommon, 'FIRST_NAME');
    const lastNameBt = getNameFromCommon(cmsCommon, 'LAST_NAME');
    const theirFirstNameBt = getNameFromCommon(cmsCommon, 'Their_First_Name');
    const theirLastNameBt = getNameFromCommon(cmsCommon, 'Their_Last_Name');
    const theirEmailBt = getNameFromCommon(cmsCommon, 'Their_Email');
    const giftMessageBt = getNameFromCommon(cmsCommon, 'Gift_message_optional');
    const deliveryBt = getNameFromCommon(cmsCommon, 'Delivery_Time');
    const enterYourAmount = getNameFromCommon(cmsCommon, 'Enter_your_own_voucher_amount');

    const pricesBottle = bottle ? bottle.items[0].price : '';
    const cmsHeader = cms && cms.length > 0 ? cms[0] : {};
    const yourHeader = cms && cms.length > 1 ? cms[1] : {};
    const theirHeader = cms && cms.length > 2 ? cms[2] : {};
    const htmlWeb = (
      <div className="div-row div-fill-details">
        <div id="id_your" className="div-point_scroll" />
        <div className="w-20 ml-4">
          <ProcessGift step={3} />
        </div>
        <div className="w-60 div-content">
          <div className="div-header div-col">
            <h2>
              {cmsHeader && cmsHeader.value ? cmsHeader.value.header.header_text : ''}
            </h2>
            <span>
              {cmsHeader && cmsHeader.value ? cmsHeader.value.texts[0].value : ''}
            </span>
          </div>
          <div className="div-row div-body">
            <div className="w-30 div-image div-col items-center">
              <span className="tc">
                <b>
                  {chooseBt}
                </b>
              </span>
              <div className="div-col div-image-content mt-2">
                <div className="div-image-gift h-50">
                  <img
                    loading="lazy"
                    src={giftChoise ? giftChoise.value.image.image : ''}
                    alt="gift"
                    onClick={() => onClickLink(giftChoise.value.link, null, history)}
                  />
                </div>
                <div className="div-text div-col h-50">
                  <div className="div-col header tc">
                    <span>
                      {giftChoise ? giftChoise.value.product.name : ''}
                    </span>
                    <span>
                      <b>
                        {`${giftChoise.value.product.gift && giftChoise.value.product.gift.type === 'voucher' ? 'Starting at ' : ''}`}
                        {generaCurrency(giftChoise && giftChoise.value.product.items && giftChoise.value.product.items.length > 0 ? giftChoise.value.product.items[0].price : '')}
                      </b>
                    </span>
                  </div>
                  <div className="div-text-description tc">
                    <span>
                      {giftChoise ? ReactHtmlParser(giftChoise.value.product.description) : ''}
                    </span>
                  </div>
                </div>
              </div>

            </div>
            <div className="w-70 div-details div-col">
              <div className="div-col div-your-details">
                <span className="title-details">
                  {yourHeader && yourHeader.value ? yourHeader.value.header.header_text : ''}
                </span>
                <Row>
                  <Col xs="6">
                    <input
                      className={`border-checkout w-100 mt-3 ${listError.includes('firtName') ? 'error' : ''}`}
                      type="text"
                      name="firtName"
                      placeholder={`${firstNameBt}*`}
                      onChange={this.onChange}
                    />
                  </Col>
                  <Col xs="6">
                    <input
                      className={`border-checkout w-100 mt-3 ${listError.includes('lastName') ? 'error' : ''}`}
                      type="text"
                      name="lastName"
                      placeholder={`${lastNameBt}*`}
                      onChange={this.onChange}
                    />
                  </Col>
                  {/* <Col xs="12">
                    <input
                      className="border-checkout w-100 mt-3"
                      type="email"
                      name="email"
                      placeholder="Email"
                      onChange={this.onChange}
                    />
                  </Col> */}
                </Row>
                <div className="div-choise-option">
                  <div className="div-row justify-between">
                    {
                        giftChoise.value.product.gift.type === 'voucher'
                          ? (
                            _.map(_.filter(giftChoise.value.product.items, x => x.price !== '0.00'), (d, index) => (
                              <button
                                type="button"
                                className={`${!info.priceCustom && info.indexVoucher === index ? 'button-border-bg-black mt-3' : 'button-border-bg-whilte  mt-3'}`}
                                onClick={() => this.onChangeVoucher(index)}
                              >
                                {generaCurrency(d.price)}
                              </button>
                            ))
                          ) : (<div />)
                      }
                  </div>
                </div>
                {
                  giftChoise.value.product.gift.type === 'voucher'
                    ? (
                      <div className="div-amount mt-3">
                        <div className="div-input">
                          <span>
                            {auth.getCurrency()}
                          </span>
                          <input name="priceCustom" type="number" pattern="[0-9]*" placeholder={enterYourAmount} value={info.priceCustom} onChange={this.onChange} />
                        </div>
                      </div>
                    ) : (<div />)
                }

                <div>
                  <button
                    type="button"
                    className="bt-checkout mt-3 button-padding-landing"
                    onClick={this.onClickContinue}
                  >
                    {continueBt}
                  </button>
                </div>
                <div className="line mt-3 mb-3" />
              </div>

              <div className="div-col div-their-details">
                <span className="title-details" style={isShowTheir ? {} : { color: '#8d8d8d' }}>
                  {theirHeader && theirHeader.value ? theirHeader.value.header.header_text : ''}
                </span>
                {
                  isShowTheir ? (
                    <React.Fragment>
                      <Row>
                        <Col xs="6">
                          <input
                            className={`border-checkout w-100 mt-3 ${listError.includes('their_firtName') ? 'error' : ''}`}
                            type="text"
                            name="their_firtName"
                            placeholder={`${theirFirstNameBt}*`}
                            onChange={this.onChange}
                          />
                        </Col>
                        <Col xs="6">
                          <input
                            className={`border-checkout w-100 mt-3 ${listError.includes('their_lastName') ? 'error' : ''}`}
                            type="text"
                            name="their_lastName"
                            placeholder={`${theirLastNameBt}*`}
                            onChange={this.onChange}
                          />
                        </Col>
                        <Col xs="12">
                          <input
                            className={`border-checkout w-100 mt-3 ${listError.includes('their_email') ? 'error' : ''}`}
                            type="email"
                            name="their_email"
                            placeholder={`${theirEmailBt}*`}
                            onChange={this.onChange}
                          />
                        </Col>
                        {
                          giftChoise.value.product.gift.type !== 'voucher'
                            ? (
                              <Col xs="12" className="mt-3">
                                <span style={{ fontSize: '0.8rem' }}>
                                  {tBlock && tBlock.length > 0 ? tBlock[0].value : ''}
                                </span>
                                <Input
                                  type="select"
                                  className="border-checkout w-100 mt-1"
                                  placeholder="Country"
                                  name="country"
                                  onChange={this.onChange}
                                  // value={info.their.country}
                                  style={{ height: '50px' }}
                                >
                                  {
                                  _.map(listShipping, x => (
                                    <option value={x.country.code.toLowerCase()} selected={x.country.code.toLowerCase() === info.their.country.toLowerCase()}>
                                      {`${x.country.name} (+ ${generaCurrency(x.shipping)} ${forShippingBt} )`}
                                    </option>
                                  ))
                                }
                                </Input>
                                <div className="div-row mt-3">
                                  <img src={icHome} alt="icHome" />
                                  <span className="ml-2">
                                    <b>
                                      {/* For security reasons, The gift reciever will fill the rest of the address */}
                                      {tBlock && tBlock.length > 1 ? tBlock[1].value : ''}
                                    </b>
                                  </span>
                                </div>
                                <div className="div-col mt-3">
                                  <span style={{ fontSize: '0.8rem' }}>
                                    {tBlock && tBlock.length > 3 ? tBlock[3].value : ''}
                                  </span>
                                  <DatePicker
                                    dateFormat="dd/MM/yyyy"
                                    minDate={new Date()}
                                    className="border-checkout w-100 mt-1"
                                    onChange={this.onChangeDate}
                                    selected={info.their.dateSent}
                                  />
                                  {/* <InputMask
                                    className={`border-checkout w-100 mt-1 ${listError.includes('dateSent') ? 'error' : ''}`}
                                    type="text"
                                    name="dateSent"
                                    placeholder="dd/mm/yyyy"
                                    onChange={this.onChange}
                                    mask="99/99/9999"
                                    maskChar={null}
                                  /> */}
                                </div>
                              </Col>
                            ) : (
                              <div />
                            )
                        }
                      </Row>
                      <div className="line mt-3" />
                      <div className="div-choise-option">
                        {/* <div className="div-row justify-between">
                          {
                            giftChoise.value.product.gift.type === 'voucher'
                              ? (
                                _.map(giftChoise.value.product.items, (d, index) => (
                                  <button
                                    type="button"
                                    className={`${info.indexVoucher === index ? 'button-homepage-new' : 'button-border-bg-whilte'}`}
                                    onClick={() => this.onChangeVoucher(index)}
                                  >
                                    {generaCurrency(d.price)}
                                  </button>
                                ))
                              ) : (<div />)
                          }
                        </div> */}
                        {
                          giftChoise.value.product.gift.type === 'perfume'
                            ? (
                              <Label check style={{ cursor: 'pointer' }} className="text-filter-active mb-4 mt-4">
                                <Input
                                  id="idCheckbox"
                                  type="checkbox"
                                  className="custom-input-filter__checkbox mt-3"
                                  style={{ height: '50px', borderRadius: '4px' }}
                                  onChange={this.onClickCheckBox}
                                  value={isCheckBottle}
                                />
                                {' '}
                                <Label
                                  for="idCheckbox"
                                  style={{ pointerEvents: 'none' }}
                                />
                                {tBlock && tBlock.length > 2 ? tBlock[2].value : ''}
                                (+
                                {' '}
                                {`${generaCurrency(pricesBottle)}`}
                                )
                              </Label>
                            ) : (
                              <Col xs="12" className="mt-3 pl-0 pr-0">
                                {/* <div className="div-row justify-between">
                                  {
                                    _.map(giftChoise.value.product.items, (d, index) => (
                                      <button
                                        type="button"
                                        className={`${info.indexVoucher === index ? 'button-homepage-new mt-3' : 'button-border-bg-whilte  mt-3'}`}
                                        onClick={() => this.onChangeVoucher(index)}
                                      >
                                        {generaCurrency(d.price)}
                                      </button>
                                    ))
                                  }
                                </div> */}
                                <div className="div-col mt-3 mb-3">
                                  <span style={{ fontSize: '0.8rem' }}>
                                    {tBlock && tBlock.length > 3 ? tBlock[3].value : ''}
                                  </span>
                                  <DatePicker
                                    dateFormat="dd/MM/yyyy"
                                    minDate={new Date()}
                                    className="border-checkout w-100 mt-1"
                                    onChange={this.onChangeDate}
                                    selected={info.their.dateSent}
                                  />
                                  {/* <InputMask
                                    className={`border-checkout w-100 mt-1 ${listError.includes('dateSent') ? 'error' : ''}`}
                                    type="text"
                                    name="dateSent"
                                    placeholder="dd/mm/yyyy"
                                    onChange={this.onChange}
                                    mask="99/99/9999"
                                    maskChar={null}
                                  /> */}
                                </div>
                              </Col>
                            )
                        }
                      </div>
                      <textarea
                        type="text"
                        placeholder={giftMessageBt}
                        onChange={this.onChange}
                        name="message"
                        className="border-checkout  "
                        style={{ height: '8rem' }}
                      />
                      <div>
                        <button
                          type="button"
                          className="bt-checkout mt-3 mb-5"
                          onClick={this.onClickAddCart}
                        >
                          {addToCartBt ? addToCartBt.toUpperCase() : ''}
                        </button>
                      </div>
                    </React.Fragment>
                  ) : (<div />)
                }
                <div className="line mb-3 mt-3" />

              </div>

            </div>
          </div>
        </div>
        <div className="w-20" />
      </div>
    );
    const htmlMobile = (
      <div className="div-fill-details-mobile">
        <div id="id_your" className="div-point_scroll" style={{ marginTop: '-170px' }} />
        <div className="div-col justify-center items-center">
          <ProcessGift step={3} onClickGotoMenu={this.props.onClickGotoMenu} />
        </div>
        <div className="div-gift-select mt-4">

          <div className="div-header div-col items-center">
            <h2>
              {cmsHeader && cmsHeader.value ? cmsHeader.value.header.header_text : ''}
            </h2>
            <span>
              {cmsHeader && cmsHeader.value ? cmsHeader.value.texts[0].value : ''}
            </span>
          </div>

          <div className="div-choise-item div-row mt-4 mb-4">
            <img
              loading="lazy"
              src={giftChoise ? giftChoise.value.image.image : ''}
              alt="test"
              onClick={() => onClickLink(giftChoise.value.link, null, history)}
            />
            <div className="text div-col justify-between">
              <h2>
                {giftChoise ? giftChoise.value.product.name : ''}
              </h2>
              <span>
                {giftChoise ? ReactHtmlParser(giftChoise.value.product.description) : ''}
              </span>
            </div>
            <div className="price div-col justify-center items-end">
              <span className="tr mr-2" style={{ color: '#8d8d8d' }}>
                {/* <b>
                  {`${giftChoise.value.product.gift && giftChoise.value.product.gift.type === 'voucher' ? 'Starting at ' : ''}`}
                  {generaCurrency(giftChoise && giftChoise.value.product.items && giftChoise.value.product.items.length > 0 ? giftChoise.value.product.items[0].price : '')}
                </b> */}
                Selected
              </span>
            </div>
          </div>

          <div className="div-details div-col">
            <div className="div-col div-your-details">
              <span className="title-details">
                <b>
                  {yourHeader && yourHeader.value ? yourHeader.value.header.header_text : ''}
                </b>
              </span>
              <Row style={{ margin: '0px' }}>
                <Col xs="6" className="pr-2">
                  <input
                    className={`border-checkout w-100 mt-3 ${listError.includes('firtName') ? 'error' : ''}`}
                    type="text"
                    name="firtName"
                    placeholder={`${firstNameBt}*`}
                    onChange={this.onChange}
                  />
                </Col>
                <Col xs="6" className="pl-2">
                  <input
                    className={`border-checkout w-100 mt-3 ${listError.includes('lastName') ? 'error' : ''}`}
                    type="text"
                    name="lastName"
                    placeholder={`${lastNameBt}*`}
                    onChange={this.onChange}
                  />
                </Col>
                {/* <Col xs="12">
                  <input
                    className="border-checkout w-100 mt-3"
                    type="email"
                    name="email"
                    placeholder="Email"
                    onChange={this.onChange}
                  />
                </Col> */}
              </Row>

              <div className="div-choise-option">
                <div className="div-row justify-between">
                  {
                  giftChoise.value.product.gift.type === 'voucher'
                    ? (
                      _.map(_.filter(giftChoise.value.product.items, x => x.price !== '0.00'), (d, index) => (
                        <button
                          type="button"
                          className={`${!info.priceCustom && info.indexVoucher === index ? 'button-border-bg-black mt-3' : 'button-border-bg-whilte mt-3'}`}
                          onClick={() => this.onChangeVoucher(index)}
                        >
                          {generaCurrency(d.price)}
                        </button>
                      ))
                    ) : (<div />)
                }
                </div>
              </div>
              {
                giftChoise.value.product.gift.type === 'voucher'
                  ? (
                    <div className="div-amount mt-3">
                      <div className="div-input">
                        <span>
                          {auth.getCurrency()}
                        </span>
                        <InputMask
                          className="border-checkout w-100"
                          type="text"
                          name="priceCustom"
                          placeholder={enterYourAmount}
                          onChange={this.onChange}
                          maskChar={null}
                          mask="99999999999999999999"
                          style={{ height: '48px' }}
                        />
                        {/* <input name="priceCustom" type="number" pattern="[0-9]*" placeholder={enterYourAmount} value={info.priceCustom} onChange={this.onChange} /> */}
                      </div>
                    </div>
                  ) : (<div />)
              }

              <div className="w-100">
                <button
                  type="button"
                  className="bt-checkout mt-3 w-100"
                  onClick={this.onClickContinue}
                >
                  {continueBt}
                </button>
              </div>
              <div className="line mt-3 mb-3" />
            </div>
            <div className="div-col div-their-details">
              <span className="title-details">
                <b>
                  {theirHeader && theirHeader.value ? theirHeader.value.header.header_text : ''}
                </b>
              </span>
              {
                isShowTheir
                  ? (
                    <React.Fragment>
                      <Row style={{ margin: '0px' }}>
                        <Col xs="6" className="pr-2">
                          <input
                            className={`border-checkout w-100 mt-3 ${listError.includes('their_firtName') ? 'error' : ''}`}
                            type="text"
                            name="their_firtName"
                            placeholder={`${theirFirstNameBt}*`}
                            onChange={this.onChange}
                          />
                        </Col>
                        <Col xs="6" className="pl-2">
                          <input
                            className={`border-checkout w-100 mt-3 ${listError.includes('their_lastName') ? 'error' : ''}`}
                            type="text"
                            name="their_lastName"
                            placeholder={`${theirLastNameBt}*`}
                            onChange={this.onChange}
                          />
                        </Col>
                        <Col xs="12">
                          <input
                            className={`border-checkout w-100 mt-3 ${listError.includes('their_email') ? 'error' : ''}`}
                            type="email"
                            name="their_email"
                            placeholder={`${theirEmailBt}*`}
                            onChange={this.onChange}
                          />
                        </Col>
                        {
                          giftChoise.value.product.gift.type !== 'voucher'
                            ? (
                              <Col xs="12" className="mt-3">
                                <span style={{ fontSize: '0.8rem' }}>
                                  {/* Choose the country of the gift reciever */}
                                  {tBlock && tBlock.length > 0 ? tBlock[0].value : ''}
                                </span>
                                <Input
                                  type="select"
                                  className="border-checkout w-100 mt-1"
                                  placeholder="Country"
                                  name="country"
                                  onChange={this.onChange}
                                  style={{ height: '50px' }}
                                >
                                  {
                                  _.map(listShipping, x => (
                                    <option value={x.country.code.toLowerCase()} selected={x.country.code.toLowerCase() === info.their.country.toLowerCase()}>
                                      {`${x.country.name} (+ ${generaCurrency(x.shipping)} ${forShippingBt} )`}
                                    </option>
                                  ))
                                }
                                </Input>
                                <div className="div-row mt-3 items-start">
                                  <img src={icHome} alt="icHome" style={{ marginTop: '5px' }} />
                                  <span className="ml-2">
                                    <b>
                                      {/* For security reasons, The gift reciever will fill the rest of the address */}
                                      {tBlock && tBlock.length > 1 ? tBlock[1].value : ''}
                                    </b>
                                  </span>
                                </div>
                                <div className="div-col mt-3">
                                  <span style={{ fontSize: '0.8rem' }}>
                                    {tBlock && tBlock.length > 3 ? tBlock[3].value : ''}
                                  </span>
                                  <DatePicker
                                    dateFormat="dd/MM/yyyy"
                                    minDate={new Date()}
                                    className="border-checkout w-100 mt-1"
                                    onChange={this.onChangeDate}
                                    selected={info.their.dateSent}
                                  />
                                  {/* <InputMask
                                    className={`border-checkout w-100 mt-1 ${listError.includes('dateSent') ? 'error' : ''}`}
                                    type="text"
                                    name="dateSent"
                                    placeholder="dd/mm/yyyy"
                                    onChange={this.onChange}
                                    mask="99/99/9999"
                                    maskChar={null}
                                  /> */}
                                </div>
                                {/* <Input
                                  type="select"
                                  className="border-checkout w-100 mt-3"
                                  placeholder={deliveryBt}
                                  name="delivery"
                                  style={{ height: '50px' }}
                                  onChange={this.onChangeTimeDelivery}
                                >
                                  {
                                  _.map(this.optionTime, x => (
                                    <option>
                                      {x}
                                    </option>
                                  ))
                                }
                                </Input> */}
                              </Col>
                            ) : (<div />)
                        }

                      </Row>
                      <div className="line mt-3" />
                      {
                        giftChoise.value.product.gift.type === 'perfume'
                          ? (
                            <div className="mt-3 div-choise-option border">
                              <Label check style={{ cursor: 'pointer' }} className="text-filter-active ml-3">
                                <Input
                                  id="idCheckbox"
                                  type="checkbox"
                                  className="custom-input-filter__checkbox mt-3"
                                  style={{ height: '50px' }}
                                  onChange={this.onClickCheckBox}
                                  value={isCheckBottle}
                                />
                                {' '}
                                <Label
                                  for="idCheckbox"
                                  className="mr-3"
                                  style={{ pointerEvents: 'none' }}
                                />
                                {tBlock && tBlock.length > 2 ? tBlock[2].value : ''}
                                (+
                                {' '}
                                {`${generaCurrency(pricesBottle)}`}
                                )
                              </Label>
                            </div>
                          ) : (
                            <div className="div-col mt-3 mb-3">
                              <span style={{ fontSize: '0.8rem' }}>
                                {tBlock && tBlock.length > 3 ? tBlock[3].value : ''}
                              </span>
                              <DatePicker
                                dateFormat="dd/MM/yyyy"
                                minDate={new Date()}
                                className="border-checkout w-100 mt-1"
                                onChange={this.onChangeDate}
                                selected={info.their.dateSent}
                              />
                              {/* <InputMask
                                className={`border-checkout w-100 mt-1 ${listError.includes('dateSent') ? 'error' : ''}`}
                                type="text"
                                name="dateSent"
                                placeholder="dd/mm/yyyy"
                                onChange={this.onChange}
                                mask="99/99/9999"
                                maskChar={null}
                              /> */}
                            </div>
                          )
                      }
                      <textarea
                        type="text"
                        placeholder={giftMessageBt}
                        onChange={this.onChange}
                        name="message"
                        className="border-checkout"
                        style={{ height: '5rem' }}
                      />
                      <div className="w-100">
                        <button
                          type="button"
                          className="bt-checkout mt-3 mb-3 w-100"
                          onClick={this.onClickAddCart}
                        >
                          {addToCartBt ? addToCartBt.toUpperCase() : ''}
                        </button>
                      </div>
                    </React.Fragment>
                  ) : <div />
              }
            </div>
            <div className="line mt-3 mb-5" />
          </div>
        </div>
      </div>
    );

    return (
      <React.Fragment>
        {
          isMobile ? htmlMobile : htmlWeb
        }
      </React.Fragment>
    );
  }
}

FillDetails.propTypes = {
  cms: PropTypes.arrayOf().isRequired,
  giftChoise: PropTypes.shape().isRequired,
  cmsAll: PropTypes.shape().isRequired,
  info: PropTypes.shape().isRequired,
  listShipping: PropTypes.arrayOf().isRequired,
  onClickAddCart: PropTypes.func.isRequired,
  bottle: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
  tBlock: PropTypes.arrayOf().isRequired,
  onClickGotoMenu: PropTypes.shape().isRequired,
};

export default FillDetails;
