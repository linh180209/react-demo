/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import _ from 'lodash';
import '../../styles/style.scss';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import classnames from 'classnames';

import MetaTags from 'react-meta-tags';
import * as EmailValidator from 'email-validator';
import smoothscroll from 'smoothscroll-polyfill';
import moment from 'moment';
import {
  scrollTop, getSEOFromCms, getNameFromCommon, getCmsCommon, fetchCMSHomepage, googleAnalitycs,
  generaCurrency, getNameFromButtonBlock, getAltImageV2, generateHreflang, generateUrlWeb, removeLinkHreflang, getPrice, setPrerenderReady, getLinkFromButtonBlock,
} from '../../Redux/Helpers';
import Header from '../../components/HomePage/header';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import { toastrError } from '../../Redux/Helpers/notification';
import { createBasketGuest, addProductBasket } from '../../Redux/Actions/basket';
import { GET_SHIPPING_URL, GET_ALL_PRODUCTS_BOTTLE } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import auth from '../../Redux/Helpers/auth';
import BlockHowitWork from './blockHowItWork';
import CreatePerfume from './createPerfume';
import CreateCardAmount from './createCardAmount';
import CustomeBottleV3 from '../../components/CustomeBottleV3';
import RecommendItem from '../../components/Products/recommendItem';
import { FONT_CUSTOME, COLOR_CUSTOME } from '../../constants';
import BlockIcon from '../../components/HomeAlt/BlockIcon';
import ListWorkShop from './listWorkShop';
import ListGiftBundles from './listGiftBundles';
import { isBrowser, isMobile } from '../../DetectScreen';

import '../../styles/gift.scss';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';

const getCMS = (data) => {
  if (data) {
    const seo = getSEOFromCms(data);
    const headerText = data.header_text;
    const imageBg = data.image_background;
    const iconBlocks = _.filter(data.body, x => x.type === 'icons_block');
    const colBlock = _.find(data.body, x => x.type === 'columns_block');
    const proBlock = _.find(data.body, x => x.type === 'products_block' && x.value.text !== 'gift_bundles');
    const giftBundles = _.find(data.body, x => x.type === 'products_block' && x.value.text === 'gift_bundles');
    const buttonBlocks = _.filter(data.body, x => x.type === 'button_block');
    const faqBlocks = _.filter(data.body, x => x.type === 'faq_block');
    const benefitsBlock = _.find(data.body, x => x.type === 'benefits_block');
    const imageBlocks = _.filter(data.body, x => x.type === 'image_block');
    const ctaBlocks = _.filter(data.body, x => x.type === 'cta_block');
    const textsBlock = _.find(data.body, x => x.type === 'texts_block' && x.value.header.header_text !== 'title');
    const textsBlockHeader = _.find(data.body, x => x.type === 'texts_block' && x.value.header.header_text === 'title');
    return {
      seo,
      colBlock,
      proBlock,
      textsBlock,
      faqBlocks,
      benefitsBlock,
      headerText,
      imageBg,
      iconBlocks,
      buttonBlocks,
      imageBlocks,
      ctaBlocks,
      giftBundles,
      textsBlockHeader,
    };
  }
  return {
    seo: undefined,
    bannerBlock: undefined,
    colBlock: undefined,
    proBlock: undefined,
    textsBlock: undefined,
    textsBlockHeader: undefined,
    buttonBlocks: [],
    faqBlocks: [],
    benefitsBlock: undefined,
    headerText: '',
    imageBg: '',
    iconBlocks: [],
    imageBlocks: [],
    ctaBlocks: [],
    giftBundles: undefined,
  };
};
class GiftPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seo: undefined,
      colBlock: undefined,
      proBlock: undefined,
      headerText: '',
      benefitsBlock: {},
      textsBlock: undefined,
      textsBlockHeader: undefined,
      iconBlocks: [],
      faqBlocks: [],
      giftChoise: {},
      step: 1,
      isOpenCustomeBottle: false,
      arrayThumbs: [],
      buttonBlocks: [],
      imageBlocks: [],
      ctaBlocks: [],
      workshopPerfume: undefined,
      giftBundles: undefined,
      listShipping: [],
      pricePerfume: undefined,
    };
    this.info = {
      your: {
        name: '',
      },
      their: {
        email: '',
        name: '',
        country: auth.getCountry(),
        message: '',
        dateSent: new Date(),
      },
      isCustomBottle: false,
      idVoucher: undefined,
      priceCustom: undefined,
      dataCustom: {
        currentImg: '',
        nameBottle: '',
        font: FONT_CUSTOME.JOST,
        color: COLOR_CUSTOME.BLACK,
      },
    };
    this.bottle = {};
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      match,
    } = nextProps;
    const objectReturn = {};
    const { proBlock } = prevState;
    const anchor = match && match.params.typeGift ? match.params.typeGift : undefined;
    if (anchor && proBlock && anchor !== prevState.anchor) {
      const giftChoise = _.find(proBlock.value.product, x => x.value.link === anchor);
      _.assign(objectReturn, { giftChoise, anchor });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  componentDidMount = () => {
    setPrerenderReady();
    smoothscroll.polyfill();
    scrollTop();
    googleAnalitycs('/gift');
    this.fetchDataInit();
    this.fetchShippingInfo();
    this.fetchBottle();
    this.fetchPriceBottle();
  }

  fetchPriceBottle = async () => {
    const result = await getPrice('perfume');
    this.setState({ pricePerfume: _.find(result, x => !x.apply_to_sample).price });
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  increateStep = (index) => {
    const { step } = this.state;
    if (step < index) {
      this.setState({ step: index });
    }
  }

  onClickAddCart = async () => {
    const { giftChoise } = this.state;
    if (this.validateDataInfo()) {
      // to do
      const { images, items, gift } = giftChoise.value.product;
      const item = gift.type === 'voucher' ? _.find(items, x => x.id === this.info.idVoucher) : items[0];
      const bottleImage = this.info.dataCustom.currentImg
        ? await fetch(this.info.dataCustom.currentImg).then(r => r.blob())
        : undefined;
      // eslint-disable-next-line no-shadow
      const { createBasketGuest, addProductBasket, basket } = this.props;
      const data = {
        item: item.id,
        is_featured: item.is_featured,
        name: gift.type === 'voucher' ? item.name : (this.info.dataCustom.nameBottle || item.name),
        currency: auth.getNameCurrency(),
        price_custom: gift.type === 'voucher' ? (this.info.priceCustom || undefined) : undefined,
        quantity: 1,
        meta: {
          sender: {
            name: this.info.your.name,
          },
          receiver: {
            name: this.info.their.name,
            email: this.info.their.email,
            country: gift.type === 'voucher' ? undefined : this.info.their.country,
            note: this.info.their.message,
            date_sent: moment(this.info.their.dateSent).valueOf() / 1000,
          },
        },
      };
      if (gift.type !== 'voucher') {
        const imagePremade = _.find(this.state.arrayThumbs, x => x.image === this.info.dataCustom.currentImg) ? _.find(this.state.arrayThumbs, x => x.image === this.info.dataCustom.currentImg).id : undefined;
        _.assign(data, {
          is_display_name: (!bottleImage && !!this.info.dataCustom.nameBottle) || !!this.info.dataCustom.nameBottle,
          is_customized: !!bottleImage || !!imagePremade,
          font: this.info.dataCustom.font,
          color: this.info.dataCustom.color,
          imagePremade,
          file: bottleImage
            ? new File([bottleImage], 'product.png')
            : undefined,
        });
      }
      const dataTemp = {
        idCart: basket.id,
        item: data,
      };

      if (!basket.id) {
        createBasketGuest(dataTemp);
      } else {
        addProductBasket(dataTemp);
      }
    }
  }

  onUpdateChoiseGift = (data) => {
    this.increateStep(3);
    if (this.state.giftChoise && data.id === this.state.giftChoise.id) {
      this.setState({ giftChoise: undefined });
    } else {
      // this.setState({ giftChoise: data });
      this.props.history.push(generateUrlWeb(`/gift/${data.value.link}`));
    }
    setTimeout(() => {
      const element = document.getElementById('id_your');
      if (element) {
        element.scrollIntoView({ behavior: 'smooth' });
      }
    }, 300);
  }

  onClickStart = () => {
    this.increateStep(2);
    setTimeout(() => {
      const element = document.getElementById('id_gift');
      if (element) {
        element.scrollIntoView({ behavior: 'smooth' });
      }
    }, 300);
  }

  validateDataInfo = () => {
    const { buttonBlocks } = this.state;
    const cmsCommon = getCmsCommon(this.props.cms);
    const choiseBt = getNameFromCommon(cmsCommon, 'Please_choise_a_gift');
    const yourInfoBt = getNameFromCommon(cmsCommon, 'Please_enter_all_the_your_information');
    const yourEmailBt = getNameFromCommon(cmsCommon, 'The_your_email_is_not_available');
    const theirInfoBt = getNameFromCommon(cmsCommon, 'Please_enter_all_the_their_information');
    const theirEmailBt = getNameFromCommon(cmsCommon, 'The_their_email_is_not_available');
    const yourEmail = getNameFromButtonBlock(buttonBlocks, 'your_name');
    const theirNameBt = getNameFromButtonBlock(buttonBlocks, 'their_name');
    const theirEmail = getNameFromButtonBlock(buttonBlocks, 'their_email');
    const timeOfEmailBt = getNameFromButtonBlock(buttonBlocks, 'time_of_email');
    const pleaseEnterBt = getNameFromButtonBlock(buttonBlocks, 'Please_enter');

    const { your, their } = this.info;
    const { giftChoise } = this.state;
    const listError = [];
    if (!giftChoise) {
      toastrError(choiseBt);
      return false;
    }
    if (!this.validityFiled(your)) {
      listError.push(yourEmail);
    }
    if (!their.name) {
      listError.push(theirNameBt);
    }
    if (!their.email) {
      listError.push(theirEmail);
    }
    if (!their.dateSent) {
      listError.push(timeOfEmailBt);
    }
    if (listError.length > 0) {
      toastrError(`${pleaseEnterBt} ${_.join(listError, ', ')}`);
      return false;
    }
    if (!EmailValidator.validate(their.email)) {
      toastrError(theirEmailBt);
      return false;
    }
    return true;
  }

  validityFiled = (data) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const key in data) {
      if (!data[key] && key !== 'message') {
        return false;
      }
    }
    return true;
  }

  fetchBottle = () => {
    const options = {
      url: GET_ALL_PRODUCTS_BOTTLE,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError && result.length > 0) {
        this.bottle = result[0];
        const thumbs = [];
        _.forEach(this.bottle.images, (x) => {
          if (x.type === 'suggestion') {
            thumbs.push(x);
          }
        });
        this.setState({ arrayThumbs: thumbs });
      }
    }).catch((err) => {
      toastrError(err.message);
    });
  }

  fetchShippingInfo = () => {
    const options = {
      url: GET_SHIPPING_URL,
      method: 'GET',
    };
    fetchClient(options).then((results) => {
      if (results && !results.isError) {
        _.forEach(results, (x) => {
          x.name = x.country.name;
        });
        const listShipping = _.orderBy(results, 'name', 'asc');
        this.setState({ listShipping });
      } else {
        toastrError('Get list shipping');
      }
    }).catch((err) => {
      console.log('err', err);
      toastrError('Get list shipping');
    });
  }

  getGiftChooseFromCms = (proBlock) => {
    const { match } = this.props;
    const anchor = match && match.params.typeGift ? match.params.typeGift : undefined;
    if (anchor && proBlock) {
      return _.find(proBlock.value.product, x => x.value.link === anchor);
    }
    return null;
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const cmsBlock = _.find(cms, x => x.title === 'Gift');
    if (!cmsBlock) {
      const cmsData = await fetchCMSHomepage('gift');
      this.props.addCmsRedux(cmsData);
      const dataCms = getCMS(cmsData);
      // const giftChoise = this.getGiftChooseFromCms(dataCms.proBlock);
      // _.assign(dataCms, giftChoise);
      this.setState(dataCms);
    } else {
      const dataCms = getCMS(cmsBlock);
      // const giftChoise = this.getGiftChooseFromCms(dataCms.proBlock);
      // _.assign(dataCms, giftChoise);
      this.setState(dataCms);
    }
  }

  onClickGotoMenu = (index) => {
    this.setState({ step: index + 1 });
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  openCustomeBottle = () => {
    this.setState({ isOpenCustomeBottle: true });
  }

  closeCustomeBottle = () => {
    this.setState({ isOpenCustomeBottle: false });
  }

  handleCropImage = (img) => {
    const { dataCustom } = this.info;
    dataCustom.currentImg = img;
    this.forceUpdate();
  };

  onSaveCustomer = (data) => {
    const { dataCustom } = this.info;
    const {
      image, name, color, font,
    } = data;
    dataCustom.color = color;
    dataCustom.font = font;
    if (_.isEmpty(image)) {
      dataCustom.currentImg = undefined;
      dataCustom.nameBottle = name;
      this.forceUpdate();
      return;
    }
    dataCustom.currentImg = image;
    dataCustom.nameBottle = name;
    this.forceUpdate();
  };

  updatePrice = () => {
    this.forceUpdate();
  }

  onClickGotoProduct = (product) => {
    const { type } = product;
    let url;
    if (type === 'Wax_Perfume') {
      url = `/product/wax/${product.id}`;
    } else if (type === 'Elixir') {
      url = `/product/elixir/${product.id}`;
    } else if (type === 'Kit') {
      url = `/product/kit/${product.id}`;
    } else if (type === 'bundle') {
      url = `/product/bundle/${product.id}`;
    } else if (type === 'Scent') {
      url = `/product/Scent/${product.id}`;
    } else if (type === 'hand_sanitizer') {
      url = `/product/hand-sanitizer/${product.id}/${product.item.id}`;
    } else if (type === 'home_scents') {
      url = `/product/home-scents/${product.id}/${product.item.id}`;
    } else if (type === 'dual_candles') {
      url = `/product/dual-candles/${product.id}`;
    } else if (type === 'single_candle') {
      url = `/product/single-candle/${product.id}`;
    } else if (type === 'holder') {
      url = `/product/holder/${product.id}`;
    } else if (type === 'gift_bundle') {
      url = `/product/gift-bundle/${product.id}`;
    } else if (type === 'mini_candles') {
      url = `/product/mini_candles/${product.id}`;
    }
    this.props.history.push(generateUrlWeb(url));
  }

  getNumberFromString = s => (s ? s.replace(/[^0-9]/g, '') : undefined);

  render() {
    const {
      seo, proBlock, textsBlock, faqBlocks, giftChoise,
      headerText, imageBg, iconBlocks, isOpenCustomeBottle, arrayThumbs, buttonBlocks, imageBlocks,
      ctaBlocks, giftBundles, textsBlockHeader, pricePerfume,
    } = this.state;

    const faqBlock = faqBlocks ? _.isEmpty(giftChoise) ? faqBlocks[0] : _.find(faqBlocks, x => x.value.header.header_text === giftChoise.value.image.caption) : undefined;
    const cmsCommon = getCmsCommon(this.props.cms);
    const fromBt = getNameFromButtonBlock(buttonBlocks, 'from');
    const minProduct = _.minBy(giftBundles?.value?.product || [], x => parseFloat(x.value.product.items[0].price));
    const priceGiftBundle = minProduct?.value?.product?.items[0].price;
    const minPerfumeWorkshop = _.minBy(ctaBlocks || [], x => parseFloat(this.getNumberFromString(x.value.image.caption)));
    const pricePerfumeWorkshop = this.getNumberFromString(minPerfumeWorkshop?.value?.image?.caption);

    const products = proBlock?.value?.product ? proBlock.value.product : [];
    _.forEach(products, (d) => {
      if (!_.isEmpty(d.value.product)) {
        d.value.product.items.sort((a, b) => (Number(a.price) - Number(b.price)));
      }
    });
    const { dataCustom } = this.info;
    let price;
    if (!_.isEmpty(giftChoise)) {
      if (_.isEmpty(giftChoise.value.product)) { // For Gift Bundles
        price = priceGiftBundle;
      } else if (giftChoise.value.product.gift.type === 'voucher') { // For Gift Card
        if (_.isEmpty(this.info.priceCustom) && !this.info.idVoucher) {
          this.info.idVoucher = _.find(giftChoise.value.product.items, x => x.price !== '0.00').id;
        }
        price = this.info.priceCustom ? this.info.priceCustom : _.find(giftChoise.value.product.items, x => x.id === this.info.idVoucher).price;
      } else if (giftChoise.value.link === 'perfume_workshop') {
        price = pricePerfumeWorkshop; // For Perfume Workshop
      } else {
        price = pricePerfume; // For Bespoke Perfume
      }
    }
    // const showCutome = (!_.isEmpty(giftChoise) && giftChoise.value.product && giftChoise.value.product.gift.type !== 'voucher') && (this.info.dataCustom.currentImg || this.info.dataCustom.nameBottle);
    const imageShow = !_.isEmpty(giftChoise) ? _.find(imageBlocks, x => x.value.caption === giftChoise.value.image.caption).value : undefined;
    const blockFAQ = (
      <div className="div-faq-gift">
        <div className="div-content-faq-gift">
          {/* {faqBlock?.value?.header?.header_text && <h3 className="tc w-100">{faqBlock?.value?.header?.header_text}</h3>} */}
          <div
            style={{
              width: '100%',
              height: '1px',
              background: 'rgba(38, 38, 38, 0.1)',
            }}
          />
          {
              _.map(faqBlock ? faqBlock.value.faq : [], d => (
                <RecommendItem title={d.value.header.header_text} description={d.value.paragraph} />
              ))
            }
          <div style={{ height: isBrowser ? '200px' : '120px' }} />
        </div>
      </div>
    );

    const isTotalProduct = products ? products.length : 0;
    let href = '/gift';
    let seoTitle = seo?.seoTitle;
    let seoDescription = seo?.seoDescription;
    if (window.location.pathname.includes('bespoke_perfume')) {
      href = '/gift/bespoke_perfume';
      seoTitle = getNameFromButtonBlock(buttonBlocks, 'seo_bespoke_perfume');
      seoDescription = getLinkFromButtonBlock(buttonBlocks, 'seo_bespoke_perfume');
    } else if (window.location.pathname.includes('gift_card')) {
      href = '/gift/gift_card';
      seoTitle = getNameFromButtonBlock(buttonBlocks, 'seo_gift_card');
      seoDescription = getLinkFromButtonBlock(buttonBlocks, 'seo_gift_card');
    } else if (window.location.pathname.includes('gift_bundles')) {
      href = '/gift/gift_bundles';
      seoTitle = getNameFromButtonBlock(buttonBlocks, 'seo_gift_bundles');
      seoDescription = getLinkFromButtonBlock(buttonBlocks, 'seo_gift_bundles');
    } else if (window.location.pathname.includes('perfume_workshop')) {
      href = '/gift/perfume_workshop';
      seoTitle = getNameFromButtonBlock(buttonBlocks, 'seo_perfume_workshop');
      seoDescription = getLinkFromButtonBlock(buttonBlocks, 'seo_perfume_workshop');
    }
    return (
      <div className="div-col items-center">
        <MetaTags>
          <title>
            {seoTitle}
          </title>
          <meta name="description" content={seoDescription} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, href)}
        </MetaTags>
        {/* <Header /> */}
        <HeaderHomePageV3 />
        <div className="div-gift">
          <div className="div-left">
            <img loading="lazy" src={!_.isEmpty(imageShow) ? imageShow.image : imageBg} alt={getAltImageV2(imageShow || imageBg)} />
            <div className="info-text div-col justify-center items-center">
              <h1 style={{ paddingTop: _.isEmpty(giftChoise) && isMobile ? '20px' : '0px' }}>
                {textsBlockHeader ? !_.isEmpty(giftChoise) ? textsBlockHeader.value.texts[giftChoise.value.image.caption].value : textsBlockHeader.value.texts[0].value : ''}
              </h1>
              {
                !_.isEmpty(giftChoise) && (
                  <React.Fragment>
                    <h2>
                      {fromBt}
                      {' '}
                      {generaCurrency(price)}
                    </h2>
                    <div>
                      {textsBlock.value.texts[giftChoise.value.image.caption - 1].value}
                      {/* A fully customisable and unique perfume creation. */}
                    </div>
                  </React.Fragment>
                )
              }
            </div>
          </div>
          <div className="div-right">
            <div className={classnames('div-option-select', !_.isEmpty(giftChoise) ? 'small-select' : '', isTotalProduct < 4 ? 'center-div' : '')}>
              {
                _.map(products, x => (
                  <div
                    onClick={() => this.onUpdateChoiseGift(x)}
                    className={classnames('item-gift', !_.isEmpty(giftChoise) && isBrowser ? 'small-card' : '', x === giftChoise ? 'active' : '')}
                  >
                    <img loading="lazy" src={x ? x.value.image.image : ''} alt={getAltImageV2(x ? x.value.image : '')} />
                    <div className="text">
                      <h4>
                        {x ? x.value.text : ''}
                      </h4>
                      <h5>
                        {fromBt}
                        {' '}
                        {generaCurrency(
                          x.value.link === 'bespoke_perfume' ? pricePerfume
                            : x.value.link === 'gift_bundles' ? priceGiftBundle
                              : x.value.link === 'perfume_workshop' ? pricePerfumeWorkshop
                                : x && !_.isEmpty(x.value.product)
                                  ? (_.filter(x.value.product.items, d => parseInt(d.price, 10) !== 0).length > 0 ? parseInt(_.filter(x.value.product.items, d => parseInt(d.price, 10) !== 0)[0].price, 10) : '')
                                  : parseInt(x.value.price, 10),
                        )}
                      </h5>
                    </div>

                  </div>
                ))
              }
              {
                isMobile && isTotalProduct > 3 && (
                  <div style={{ height: 'auto', width: '15px' }} />
                )
              }
            </div>

            {
              _.isEmpty(giftChoise) ? (
                <React.Fragment>
                  <BlockHowitWork data={iconBlocks && iconBlocks.length > 0 ? iconBlocks[0].value : undefined} />
                  {/* {isBrowser ? null : blockFAQ} */}
                </React.Fragment>
              ) : giftChoise.value.text === 'Workshop'
                ? (
                  <React.Fragment>
                    <BlockHowitWork data={iconBlocks && iconBlocks.length > 0 ? iconBlocks[0].value : undefined} />
                    {isBrowser ? null : blockFAQ}
                  </React.Fragment>
                ) : giftChoise.value.image.caption === '2'
                  ? (
                    <React.Fragment>
                      <ListWorkShop
                        data={ctaBlocks}
                        onClick={d => this.setState({ workshopPerfume: d })}
                        workshopPerfume={this.state.workshopPerfume}
                        buttonBlocks={buttonBlocks}
                      />
                    </React.Fragment>
                  ) : giftChoise.value.image.caption === '4'
                    ? (
                      <ListGiftBundles
                        datas={giftBundles ? giftBundles.value.product : []}
                        addProductBasket={this.props.addProductBasket}
                        createBasketGuest={this.props.createBasketGuest}
                        loadingPage={this.props.loadingPage}
                        onClickIngredient={this.props.onClickIngredient}
                        onClickGotoProduct={this.onClickGotoProduct}
                        basket={this.props.basket}
                        buttonBlocks={buttonBlocks}
                      />
                    ) : (giftChoise.value.product.items.length > 1)
                      ? (
                        <CreateCardAmount
                          buttonBlocks={buttonBlocks}
                          info={this.info}
                          giftChoise={giftChoise}
                          listShipping={this.state.listShipping}
                          updatePrice={this.updatePrice}
                          onClickAddCart={this.onClickAddCart}
                        />
                      )
                      : (
                        <CreatePerfume
                          buttonBlocks={buttonBlocks}
                          info={this.info}
                          listShipping={this.state.listShipping}
                          giftChoise={giftChoise}
                          openCustomeBottle={this.openCustomeBottle}
                          onClickAddCart={this.onClickAddCart}
                        />
                      )
            }
          </div>
        </div>
        <div className="b-faq">
          { blockFAQ }
        </div>

        {
          isOpenCustomeBottle ? (
            <CustomeBottleV3
              cmsCommon={cmsCommon}
              arrayThumbs={arrayThumbs}
              handleCropImage={this.handleCropImage}
              currentImg={dataCustom.currentImg}
              nameBottle={dataCustom.nameBottle}
              font={dataCustom.font}
              color={dataCustom.color}
              closePopUp={() => {
                this.setState({ isOpenCustomeBottle: false });
              }
              }
              onSave={this.onSaveCustomer}
              // itemBottle={{
              //   price: this.bottle ? this.bottle.items[0].price : '',
              // }}
              // cmsTextBlocks={textBlock}
              // name1={combos && combos.length > 0 ? combos[0].name : ''}
              // name2={combos && combos.length > 1 ? combos[1].name : ''}
              name1=""
            />
          ) : (null)
        }
        {
          isBrowser && (
            <BlockIcon heroCms={iconBlocks && iconBlocks.length > 1 ? iconBlocks[1] : undefined} />
          )
        }
        <FooterV2 />
      </div>
    );
  }
}

GiftPage.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  basket: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  history: PropTypes.shape().isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    basket: state.basket,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  createBasketGuest,
  addProductBasket,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GiftPage));
