import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';
import ProcessGift from './processGift';
import icLeft from '../../image/icon/ic-gift-l.svg';
import icRight from '../../image/icon/ic-gift-r.svg';
import auth from '../../Redux/Helpers/auth';
import {
  getCmsCommon, getNameFromCommon, onClickLink, generaCurrency,
} from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

class GiftChoose extends Component {
  state = {
    indexShow: 0,
  }

  onCLickLeft = () => {
    const { indexShow } = this.state;
    if (indexShow >= 3) {
      this.setState({ indexShow: indexShow - 3 });
    }
  }

  onCLickRight = () => {
    const { cms } = this.props;
    const products = cms && cms.value && cms.value.product ? cms.value.product : [];
    const { indexShow } = this.state;
    if (indexShow < (products.length - 3)) {
      this.setState({ indexShow: indexShow + 3 });
    }
  }

  render() {
    const { indexShow } = this.state;
    const { cms, cmsAll, history } = this.props;
    const cmsCommon = getCmsCommon(cmsAll);
    const chooseBt = getNameFromCommon(cmsCommon, 'CHOOSE_GIFT');
    const products = cms && cms.value && cms.value.product ? cms.value.product : [];
    const showProduct = products.slice(indexShow, indexShow + 3);
    const htmlWeb = (
      <div className="div-gift-choose div-row">
        <div id="id_gift" className="div-point_scroll" />
        <div className="w-20 ml-4">
          <ProcessGift step={2} />
        </div>
        <div className="w-60 div-content">
          <div className="div-header div-col">
            <h2>
              {cms ? cms.value.header.header_text : ''}
            </h2>
            <span>
              {cms ? cms.value.text : ''}
            </span>
          </div>
          <div className="div-gift div-row justify-between items-center">
            <img src={icLeft} alt="ic-left" className={`${indexShow > 0 ? 'ic-left' : 'hidden'}`} onClick={this.onCLickLeft} />
            <img src={icRight} alt="ic-right" className={`${products.length > indexShow + 3 ? 'ic-right' : 'hidden'}`} onClick={this.onCLickRight} />
            {
              _.map(showProduct, data => (
                <div className="div-gift-item">
                  <img
                    loading="lazy"
                    className="animated fadeIn slow"
                    src={data ? data.value.image.image : ''}
                    alt="test"
                  />
                  <div className="div-text div-col justify-between items-center">
                    <div className="header">
                      <h3>
                        {data && !_.isEmpty(data.value.product) ? data.value.product.name : data.value.text}
                      </h3>
                      <span>
                        <b>
                          {`${data.value.product.gift && data.value.product.gift.type === 'voucher' ? 'Starting at ' : ''}`}
                          {generaCurrency(data && !_.isEmpty(data.value.product) ? (data.value.product.items && data.value.product.items.length > 0 ? data.value.product.items[0].price : '') : data.value.price)}
                        </b>
                      </span>
                    </div>
                    <div className="description">
                      {data && !_.isEmpty(data.value.product) ? ReactHtmlParser(data.value.product.description) : ReactHtmlParser(data.value.description)}
                    </div>
                    <div className="div-button">
                      <button
                        type="button"
                        className="bt-checkout"
                        style={{ padding: '10px 27px' }}
                        onClick={() => (!_.isEmpty(data.value.product) ? this.props.onUpdateChoiseGift(data) : onClickLink(data.value.link, null, history))}
                      >
                        {chooseBt}
                      </button>
                    </div>
                  </div>
                </div>
              ))
            }
          </div>
        </div>
        <div className="w-20" />
      </div>
    );
    const htmlMobile = (
      <div id="id_gift" className="div-gift-choose-mobile div-col">
        <div className="div-col justify-center items-center">
          <ProcessGift step={2} onClickGotoMenu={this.props.onClickGotoMenu} />
        </div>
        <div className="div-gift-mobile">
          <div className="div-header div-col items-center">
            <h2>
              {cms ? cms.value.header.header_text : ''}
            </h2>
            <span>
              {cms ? cms.value.text : ''}
            </span>
          </div>
          <div className="div-content div-col">

            {
              _.map(products, data => (
                <div
                  onClick={() => (!_.isEmpty(data.value.product) ? this.props.onUpdateChoiseGift(data) : onClickLink(data.value.link, null, history))}
                  className="div-choise-item div-row mt-2"
                >
                  <img
                    loading="lazy"
                    src={data ? data.value.image.image : ''}
                    alt="test"
                  />
                  <div className="text div-col justify-between">
                    <h2>
                      {data && !_.isEmpty(data.value.product) ? data.value.product.name : data.value.text}
                    </h2>
                    <span>
                      {data && !_.isEmpty(data.value.product) ? ReactHtmlParser(data.value.product.description) : ReactHtmlParser(data.value.description)}
                    </span>
                  </div>
                  <div className="price div-col justify-center items-end">
                    <span className="tr mr-2">

                      {`${data.value.product.gift && data.value.product.gift.type === 'voucher' ? 'Starting at ' : ''}`}
                      <b>
                        {generaCurrency(data && !_.isEmpty(data.value.product) ? (data.value.product.items && data.value.product.items.length > 0 ? data.value.product.items[0].price : '') : data.value.price)}
                      </b>
                    </span>
                  </div>
                </div>
              ))
            }


            {/* <div className="div-choise-item div-row mt-2 mb-2">
              <img src={testImage} alt="test" />
              <div className="text div-col justify-between">
                <h2>
                  Signature Perfume
                </h2>
                <span>
                  Your own signature perfume. Be Unique, Be You.
                </span>
              </div>
              <div className="price div-col justify-center items-center">
                <span>
                  <b>
                    S$ 69.00
                  </b>
                </span>
              </div>
            </div> */}

          </div>
        </div>
      </div>
    );
    return (
      <React.Fragment>
        {
          isMobile ? htmlMobile : htmlWeb
        }
      </React.Fragment>
    );
  }
}

GiftChoose.propTypes = {
  cmsAll: PropTypes.arrayOf().isRequired,
  cms: PropTypes.shape().isRequired,
  onUpdateChoiseGift: PropTypes.func.isRequired,
  history: PropTypes.shape().isRequired,
  onClickGotoMenu: PropTypes.func.isRequired,
};

export default GiftChoose;
