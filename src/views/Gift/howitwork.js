import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import PropTypes from 'prop-types';
import ReactHtmlParser from 'react-html-parser';
import arrowLong from '../../image/icon/arrow-long.svg';
import arrowMobile from '../../image/icon/arrow-long-mobile.svg';
import ProcessGift from './processGift';
import icDown from '../../image/icon/icDown.svg';
import { isMobile, isTablet } from '../../DetectScreen';

class HowItWork extends Component {
  render() {
    const { cms } = this.props;
    const image = cms ? (cms.value.images && cms.value.images.length > 0 ? cms.value.images[0].value.image : '') : '';
    const image1 = cms ? (cms.value.images && cms.value.images.length > 1 ? cms.value.images[1].value.image : '') : '';
    const image2 = cms ? (cms.value.images && cms.value.images.length > 2 ? cms.value.images[2].value.image : '') : '';
    const htmlMobile = (
      <div
        className="div-how-it-work-mobile div-col"
        style={{
          backgroundImage: `url(${image})`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
        }}
      >
        <div className="div-header div-col justify-center items-center">
          <h1>
            {cms ? cms.value.header.header_text : ''}
          </h1>
          <span>
            {cms ? cms.value.text : ''}
          </span>
        </div>
        <div className="div-content">
          <div className="div-item-step div-row">
            <img src={image1} alt="testImage" />
            <div className="div-text div-col justify-between">
              <h2>
                {cms && cms.value.columns.length > 0 ? cms.value.columns[0].value.header.header_text : ''}
              </h2>
              <span>
                {cms && cms.value.columns.length > 0 ? ReactHtmlParser(cms.value.columns[0].value.paragraph) : ''}
              </span>
            </div>
          </div>
          <div className="div-icon div-col justify-center items-center mt-1 mb-1">
            <img src={icDown} alt="icDown" />
          </div>
          <div className="div-item-step div-row">
            <img src={image2} alt="testImage" />
            <div className="div-text div-col justify-between">
              <h2>
                {cms && cms.value.columns.length > 1 ? cms.value.columns[1].value.header.header_text : ''}
              </h2>
              <span>
                {cms && cms.value.columns.length > 1 ? ReactHtmlParser(cms.value.columns[1].value.paragraph) : ''}
              </span>
            </div>
          </div>
          <div className="div-button div-col justify-center items-center mt-5 mb-3">
            <button
              type="button"
              className="button-border-bg-whilte button-padding-landing"
              onClick={this.props.onClickStart}
            >
              {cms ? cms.value.button.text : ''}
            </button>
          </div>
        </div>
      </div>
    );
    const htmlWeb = (
      <div
        className="div-how-it-work div-row"
        style={{
          backgroundImage: `url(${image})`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
        }}
      >
        <div className="w-20 ml-4">
          {/* <ProcessGift /> */}
        </div>
        <div className="w-60 div-content">
          <div className="div-header div-col mt-4">
            <h1>
              {cms ? cms.value.header.header_text : ''}
            </h1>
            <span>
              {cms ? cms.value.text : ''}
            </span>
          </div>
          <div className="div-step">
            <Row>
              <Col md="5" xs="12" className="div-col justify-center items-center">
                <div className="step">
                  <img loading="lazy" src={image1} alt="step1" />
                  <div className="text div-col">
                    <span className="title">
                      {cms && cms.value.columns.length > 0 ? cms.value.columns[0].value.header.header_text : ''}
                    </span>
                    <span className="description">
                      {cms && cms.value.columns.length > 0 ? ReactHtmlParser(cms.value.columns[0].value.paragraph) : ''}
                    </span>
                  </div>
                </div>
              </Col>
              <Col md="2" xs="12" className="div-col justify-center items-center">
                <img
                  src={isMobile && !isTablet ? arrowMobile : arrowLong}
                  alt="arrowLong"
                  className={`${isMobile && !isTablet ? 'mt-3 mb-3' : ''}`}
                  style={{ width: '50px' }}
                />
              </Col>
              <Col md="5" xs="12" className="div-col justify-center items-center">
                <div className="step">
                  <img loading="lazy" src={image2} alt="step1" />
                  <div className="text div-col">
                    <span className="title">
                      {cms && cms.value.columns.length > 1 ? cms.value.columns[1].value.header.header_text : ''}
                    </span>
                    <span className="description">
                      {cms && cms.value.columns.length > 1 ? ReactHtmlParser(cms.value.columns[1].value.paragraph) : ''}
                    </span>
                  </div>
                </div>
              </Col>
            </Row>
          </div>
          <div className="div-col justify-center items-center mb-4">
            <button
              type="button"
              className="button-border-bg-whilte button-padding-landing"
              onClick={this.props.onClickStart}
            >
              {cms ? cms.value.button.text : ''}
            </button>
          </div>
        </div>
        <div className="w-20" />
      </div>
    );
    return (
      <React.Fragment>
        {
          isMobile ? htmlMobile : htmlWeb
        }
      </React.Fragment>
      // <div
      //   className="div-col justify-center items-center relative height_home_page"
      //   style={{
      //     padding: isMobile && !isTablet ? '30px 0px 30px 0px' : '0px',
      //   }}
      // >
      //   <img
      //     src={cms && cms.value.images.length > 0 ? cms.value.images[0].value.image : ''}
      //     style={{
      //       width: '100vw',
      //       position: 'absolute',
      //       zIndex: '-1',
      //       objectFit: 'cover',
      //       top: '0px',
      //     }}
      //     alt={getAltImage(cms && cms.value.images.length > 0 ? cms.value.images[0].value.image : undefined)}
      //     className="height_home_page"
      //   />
      //   <div className="div-col justify-center items-center h-100">
      //     <span className="text-title mb-4" style={{ color: '#000000' }}>
      //       {cms ? cms.value.header.header_text : ''}
      //     </span>
      //     <div className={`${isMobile ? 'w-90' : 'w-70'} div-row justify-between items-center mt-5`}>
      //       <Row>
      //         <Col md="5" xs="12">
      //           <div className="div-col justify-center items-center">
      //             <span className="text-title mb-4" style={{ fontSize: '1.5rem', color: '#000000' }}>
      //               {cms && cms.value.columns.length > 0 ? cms.value.columns[0].value.header.header_text : ''}
      //             </span>
      //             <span className="tc">
      //               {cms && cms.value.columns.length > 0 ? ReactHtmlParser(cms.value.columns[0].value.paragraph) : ''}
      //             </span>
      //           </div>
      //         </Col>
      //         <Col md="2" xs="12">
      //           <div className="div-col justify-center items-center h-100">
      //             <img src={isMobile && !isTablet ? arrowMobile : arrowLong} alt="arrowLong" className={`${isMobile && !isTablet ? 'mt-3 mb-3' : ''}`} />
      //           </div>
      //         </Col>
      //         <Col md="5" xs="12">
      //           <div className="div-col justify-center items-center">
      //             <span className="text-title mb-4" style={{ fontSize: '1.5rem', color: '#000000' }}>
      //               {cms && cms.value.columns.length > 1 ? cms.value.columns[1].value.header.header_text : ''}
      //             </span>
      //             <span className="tc">
      //               {cms && cms.value.columns.length > 1 ? ReactHtmlParser(cms.value.columns[1].value.paragraph) : ''}
      //             </span>
      //           </div>
      //         </Col>
      //       </Row>
      //     </div>
      //     <div>
      //       <button
      //         type="button"
      //         style={{
      //           padding: '0.5rem 3rem 0.5rem 3rem',
      //           border: '1px solid #3d3d3d',
      //           borderRadius: '10px',
      //           background: 'transparent',
      //           marginTop: '80px',
      //         }}
      //         onClick={this.props.onClickStart}
      //       >
      //         {cms ? cms.value.button.text : ''}
      //       </button>
      //     </div>
      //   </div>
      // </div>
    );
  }
}

HowItWork.propTypes = {
  cms: PropTypes.shape(PropTypes.object).isRequired,
  onClickStart: PropTypes.func.isRequired,
};

export default HowItWork;
