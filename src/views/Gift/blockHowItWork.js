import React, { Component } from 'react';
import _ from 'lodash';
import { getAltImageV2 } from '../../Redux/Helpers';

class BlockHowitWork extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className="div-how-it-work animated faster fadeIn">
        <h3>
          {data ? data.image_background.caption : ''}
        </h3>
        <div className="list-icon">
          {
            _.map(data ? data.icons : [], x => (
              <div className="item-icon">
                <img loading="lazy" src={x.value.image.image} alt={getAltImageV2(x.value.image)} />
                <h4>
                  {x.value.image.caption}
                </h4>
                <span>
                  {x.value.text}
                </span>
              </div>
            ))
          }
        </div>
      </div>
    );
  }
}

export default BlockHowitWork;
