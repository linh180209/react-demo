import React, { Component } from 'react';
import _ from 'lodash';
import testImage from '../../image/test/1.png';
import icButtton from '../../image/icon/gift-book.svg';

class ItemWorkShopGift extends Component {
  render() {
    return (
      <div className="div-item-shop">
        <img src={testImage} alt="icon" />
        <div className="div-right">
          <div className="div-title">
            <h4>
              PERFUME CREATION
            </h4>
            <span>
              S$120 /person
            </span>
          </div>
          <div className="div-des">
            <span>
              Tailor-make your own scent with your friends.
            </span>
            <div className="icon-choose selected" />
          </div>
        </div>
      </div>
    );
  }
}

class CreateWorkshop extends Component {
  render() {
    return (
      <div className="div-create-workshop">
        <hr />
        {
         _.map(_.range(5), x => (
           <ItemWorkShopGift />
         ))
       }
        <button type="button" className="bt-shop">
          <span>
            CONTINUE ON SIMPLYBOOKME
          </span>
          <img src={icButtton} alt="icon" />
        </button>
        <span className="des">
          We are here to provide a hassle-free option. Choose any gift you prefer, and we will send the gift card to the recipient of your choice. Choose for them to receive it instantly by email or an exclusively printed gift card sent to their home.
        </span>
      </div>
    );
  }
}

export default CreateWorkshop;
