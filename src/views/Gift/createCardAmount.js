import React, { Component } from 'react';
import _ from 'lodash';
import { generaCurrency, getNameFromButtonBlock } from '../../Redux/Helpers';
import InputGift from './inputGift';
import auth from '../../Redux/Helpers/auth';
import icGift from '../../image/icon/icGift.png';

class CreateCardAmount extends Component {
  onChangeVoucher = (index) => {
    const { info } = this.props;
    info.idVoucher = index;
    info.priceCustom = '';
    this.forceUpdate();
    this.props.updatePrice();
  }

  onChange = (value, name) => {
    const { info } = this.props;
    switch (name) {
      case 'firtName':
        info.your.firtName = value;
        break;
      case 'lastName':
        info.your.lastName = value;
        break;
      case 'name':
        info.your.name = value;
        break;
      // case 'email':
      //   info.your.email = value;
      //   break;
      case 'priceCustom':
        info.priceCustom = value.replace(/\D/, '');
        if (info.priceCustom) {
          info.idVoucher = _.find(this.props.giftChoise.value.product.items, x => x.price === '0.00').id;
        } else {
          info.idVoucher = _.find(this.props.giftChoise.value.product.items, x => x.price !== '0.00').id;
        }
        this.forceUpdate();
        this.props.updatePrice();
        break;
      case 'their_firtName':
        info.their.firtName = value;
        break;
      case 'their_lastName':
        info.their.lastName = value;
        break;
      case 'their_name':
        info.their.name = value;
        break;
      case 'their_email':
        info.their.email = value;
        break;
      case 'country':
        info.their.country = value;
        this.forceUpdate();
        break;
      case 'message':
        info.their.message = value;
        break;
      case 'dateSent':
        info.their.dateSent = value;
        break;
      default:
        break;
    }
    this.forceUpdate();
  }

  onClickPurchase = () => {
    this.props.onClickAddCart();
  }

  render() {
    const {
      giftChoise, info, listShipping, buttonBlocks,
    } = this.props;
    const amountBt = getNameFromButtonBlock(buttonBlocks, 'AMOUNT');
    const addYourOwnBt = getNameFromButtonBlock(buttonBlocks, 'Add_your_own_amount');
    const messageBt = getNameFromButtonBlock(buttonBlocks, 'MESSAGE');
    const upToBt = getNameFromButtonBlock(buttonBlocks, 'Up_to_300_characters');
    const countryBt = getNameFromButtonBlock(buttonBlocks, 'COUNTRY');
    const recipientBt = getNameFromButtonBlock(buttonBlocks, 'RECIPIENT');
    const recipientEmailBt = getNameFromButtonBlock(buttonBlocks, 'Recipient_email_address');
    const timeOfEmailBt = getNameFromButtonBlock(buttonBlocks, 'TIME_OF_EMAIL');
    const purchaseBt = getNameFromButtonBlock(buttonBlocks, 'PURCHASE_NOW');
    const weAreHereBt = getNameFromButtonBlock(buttonBlocks, 'We_are_here');
    const toBt = getNameFromButtonBlock(buttonBlocks, 'To');
    const recipientNameBt = getNameFromButtonBlock(buttonBlocks, 'Recipient_name');
    const fromBt = getNameFromButtonBlock(buttonBlocks, 'FROM');
    const yourNameBt = getNameFromButtonBlock(buttonBlocks, 'Your_name');

    giftChoise.value.product.items.sort((a, b) => Number(a.price) - Number(b.price));

    return (
      <div className="div-create-card-amount animated faster fadeIn">
        <hr />
        <InputGift title={toBt} placeholder={recipientNameBt} name="their_name" value={info.their.name} onChange={this.onChange} />
        <InputGift title={fromBt} placeholder={yourNameBt} name="name" value={info.your.name} onChange={this.onChange} />
        <div className="div-amount">
          <h3>
            {amountBt}
          </h3>
          <div className="div-list-amount">
            {
            _.map(_.filter(giftChoise.value.product.items, x => x.price !== '0.00'), (d, index) => (
              <div
                // type="button"
                onClick={() => this.onChangeVoucher(d.id)}
                style={{ backgroundImage: `url(${icGift})` }}
                className={`${!info.priceCustom && info.idVoucher === d.id ? 'active' : ''}`}
              >
                <div className="price div-row">
                  <span>{auth.getCurrency()}</span>
                  <span>{parseInt(d.price, 10)}</span>
                </div>
              </div>
            ))
          }
          </div>
        </div>
        <InputGift title={auth.getCurrency()} placeholder={addYourOwnBt} type="CURRENTCY" name="priceCustom" onChange={this.onChange} />
        <InputGift title={messageBt} placeholder={upToBt} name="message" value={info.their.message} onChange={this.onChange} />
        {/* <InputGift title={countryBt} type="DROPDOWN" options={listShipping} name="country" value={info.their.country} onChange={this.onChange} /> */}
        <InputGift title={recipientBt} placeholder={recipientEmailBt} name="their_email" value={info.their.email} onChange={this.onChange} />
        <InputGift title={timeOfEmailBt} type="DATE-PICKER" name="dateSent" value={info.their.dateSent} onChange={this.onChange} />
        <button
          type="button"
          className="div-purchase"
          onClick={this.onClickPurchase}
        >
          {purchaseBt}
        </button>
        <span className="text-bottom">
          {weAreHereBt}
        </span>

      </div>
    );
  }
}

export default CreateCardAmount;
