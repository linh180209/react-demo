import React from 'react';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';
import icExtendWhite from '../../image/icon/icExtendWhite.svg';
import { getAltImageV2, getNameFromButtonBlock } from '../../Redux/Helpers';

function ListWorkShop(props) {
  const finalBookBt = getNameFromButtonBlock(props.buttonBlocks, 'FINALIZE_YOUR_BOOKING');
  const weAreBt = getNameFromButtonBlock(props.buttonBlocks, 'We_are_here_to_provide');
  return (
    <div className="list-work-shop div-col">
      <div className="list-item">
        <div className="div-col">
          <hr />
          {
        _.map(props.data || [], d => (
          <div className="item-work-shop div-row" onClick={() => props.onClick(d)}>
            <img loading="lazy" src={d.value.image.image} alt={getAltImageV2(d.value.image)} />
            <div className="right div-row justify-between">
              <div className="title div-col">
                <h3>
                  {d.value.text}
                </h3>
                <span>
                  {ReactHtmlParser(d.value.description)}
                </span>
              </div>
              <div className="div-price-button div-col items-end">
                <span>
                  {d.value.image.caption}
                </span>
                <button
                  className={props.workshopPerfume && props.workshopPerfume.id === d.id ? 'active' : ''}
                  type="button"
                />
              </div>
            </div>
          </div>
        ))
      }
        </div>
      </div>
      <button
        type="button"
        className="bt-click-link"
        disabled={!props.workshopPerfume}
        onClick={() => window.open(props.workshopPerfume.value.link)}
      >
        {finalBookBt}
        <img src={icExtendWhite} alt="icon" />
      </button>
      <span className="text-bottom">
        {weAreBt}
      </span>
    </div>
  );
}

export default ListWorkShop;
