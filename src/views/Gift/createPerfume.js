import React, { Component } from 'react';
import _ from 'lodash';
import InputGift from './inputGift';
import { getNameFromButtonBlock } from '../../Redux/Helpers';

class CreatePerfume extends Component {
  onChange = (value, name) => {
    const { info } = this.props;
    switch (name) {
      case 'firtName':
        info.your.firtName = value;
        break;
      case 'lastName':
        info.your.lastName = value;
        break;
      case 'name':
        info.your.name = value;
        break;
      // case 'email':
      //   info.your.email = value;
      //   break;
      case 'priceCustom':
        info.priceCustom = value.replace(/\D/, '');
        if (info.priceCustom) {
          info.indexVoucher = _.findIndex(this.props.giftChoise.value.product.items, x => x.price === '0.00');
        } else {
          info.indexVoucher = _.findIndex(this.props.giftChoise.value.product.items, x => x.price !== '0.00');
        }
        this.forceUpdate();
        break;
      case 'their_firtName':
        info.their.firtName = value;
        break;
      case 'their_lastName':
        info.their.lastName = value;
        break;
      case 'their_name':
        info.their.name = value;
        break;
      case 'their_email':
        info.their.email = value;
        break;
      case 'country':
        info.their.country = value;
        this.forceUpdate();
        break;
      case 'message':
        info.their.message = value;
        break;
      case 'dateSent':
        info.their.dateSent = value;
        break;
      default:
        break;
    }
    this.forceUpdate();
  }

  onClickPurchase = () => {
    this.props.onClickAddCart();
  }

  render() {
    const {
      info, listShipping, openCustomeBottle, buttonBlocks,
    } = this.props;
    const { dataCustom } = info;
    const amountBt = getNameFromButtonBlock(buttonBlocks, 'AMOUNT');
    const addYourOwnBt = getNameFromButtonBlock(buttonBlocks, 'Add_your_own_amount');
    const messageBt = getNameFromButtonBlock(buttonBlocks, 'MESSAGE');
    const upToBt = getNameFromButtonBlock(buttonBlocks, 'Up_to_300_characters');
    const countryBt = getNameFromButtonBlock(buttonBlocks, 'COUNTRY');
    const recipientBt = getNameFromButtonBlock(buttonBlocks, 'RECIPIENT');
    const recipientEmailBt = getNameFromButtonBlock(buttonBlocks, 'Recipient_email_address');
    const timeOfEmailBt = getNameFromButtonBlock(buttonBlocks, 'TIME_OF_EMAIL');
    const purchaseBt = getNameFromButtonBlock(buttonBlocks, 'PURCHASE_NOW');
    const weAreHereBt = getNameFromButtonBlock(buttonBlocks, 'We_are_here');
    const toBt = getNameFromButtonBlock(buttonBlocks, 'To');
    const recipientNameBt = getNameFromButtonBlock(buttonBlocks, 'Recipient_name');
    const fromBt = getNameFromButtonBlock(buttonBlocks, 'FROM');
    const yourNameBt = getNameFromButtonBlock(buttonBlocks, 'Your_name');
    const customeBt = getNameFromButtonBlock(buttonBlocks, 'CUSTOMIZE_BOTTLE');
    const editCutomizationBt = getNameFromButtonBlock(buttonBlocks, 'EDIT_CUSTOMIZATION');
    const addCutomizationBt = getNameFromButtonBlock(buttonBlocks, 'ADD_CUSTOMIZATION');

    return (
      <div className="div-gift-create-perfume animated faster fadeIn">
        <hr />
        <InputGift title={toBt} placeholder={recipientNameBt} name="their_name" value={info.their.name} onChange={this.onChange} />
        <InputGift title={fromBt} placeholder={yourNameBt} name="name" value={info.your.name} onChange={this.onChange} />
        <InputGift title={messageBt} placeholder={upToBt} name="message" value={info.their.message} onChange={this.onChange} />
        <InputGift title={customeBt} type="IMAGE" onClick={openCustomeBottle} dataCustom={dataCustom} buttonName={dataCustom.nameBottle || dataCustom.currentImg ? editCutomizationBt : addCutomizationBt} />
        <InputGift title={countryBt} type="DROPDOWN" options={listShipping} name="country" value={info.their.country} onChange={this.onChange} />
        <InputGift title={recipientBt} placeholder={recipientEmailBt} name="their_email" value={info.their.email} onChange={this.onChange} />
        <InputGift title={timeOfEmailBt} type="DATE-PICKER" name="dateSent" value={info.their.dateSent} onChange={this.onChange} />
        <button type="button" className="div-purchase" onClick={this.onClickPurchase}>
          {purchaseBt}
        </button>
        <span>
          {weAreHereBt}
        </span>
      </div>
    );
  }
}

export default CreatePerfume;
