import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { generaCurrency, getAltImageV2 } from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { isMobile, isTablet } from '../../DetectScreen';

class GiftObject extends Component {
  render() {
    const { data, isActive } = this.props;
    const image = _.find(data ? data.value.product.images : [], x => x.type === null);
    const width = isTablet ? 200 : 305;
    const heightImage = width * 309 / 457;
    return (
      <div
        className="div-col justify-center items-center"
        style={{
          width: `${width}px`,
          marginBottom: isMobile && !isTablet ? '20px' : '0px',
        }}
      >
        <span className="text-title tc" style={{ color: '#000000', fontSize: '1rem' }}>
          {data ? data.value.product.name : ''}
        </span>
        <img
          loading="lazy"
          style={{
            width: '100%',
            height: `${heightImage}px`,
            objectFit: 'cover',
            borderRadius: '20px',
            marginTop: '20px',
            cursor: 'pointer',
            opacity: isActive ? '0.6' : '1',
          }}
          onClick={() => this.props.onClickGift(data)}
          src={image ? image.image : ''}
          alt={getAltImageV2(image)}
        />
        <span className="tc mt-2">
          {data ? data.value.product.description : ''}
        </span>
        <span style={{ fontSize: '1.2rem' }} className="mt-3 mb-3">
          {generaCurrency(data && data.value.product.items && data.value.product.items.length > 0 ? data.value.product.items[0].price : '')}
        </span>
        {/* <span className="tc">
          Of course, they’ll always remember who to thank. The gift box will be personalised with your inscription.
        </span> */}
      </div>
    );
  }
}

GiftObject.propTypes = {
  data: PropTypes.shape(PropTypes.object).isRequired,
  isActive: PropTypes.bool.isRequired,
  onClickGift: PropTypes.func.isRequired,
};

export default GiftObject;
