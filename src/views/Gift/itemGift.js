import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';
import sub from '../../image/icon/sub_black.svg';
import add from '../../image/icon/add_small.svg';
import { isMobile, isTablet } from '../../DetectScreen';

class ItemGift extends Component {
  constructor(props) {
    super(props);
    this.defaultData = {
      firtName: undefined,
      lastName: undefined,
      email: undefined,
      occasion: undefined,
      message: undefined,
    };
  }

  state = {
    isShow: this.props.isShow,
  }

  onToggle = () => {
    const { isShow } = this.state;
    this.setState({ isShow: !isShow });
  }

  onOpen = () => {
    this.setState({ isShow: true });
  }

  render() {
    const { isShow } = this.state;
    const {
      description, title,
    } = this.props;
    const colorTitle = isShow ? '#000' : '#999999';
    return (
      <div
        className="div-item-gift mt-3"
      >
        <button
          type="button"
          className="div-row button-bg__none"
          style={{
            justifyContent: 'center',
            padding: '20px 0px',
            position: 'relative',
            width: '100%',
          }}
          onClick={this.onToggle}
        >
          <span style={isMobile && !isTablet ? {
            color: colorTitle,
            fontSize: '0.8rem',
            width: '100%',
            textAlign: 'left',
            paddingLeft: '20px',
          } : isTablet ? {
            color: colorTitle,
            fontSize: '1rem',
            width: '100%',
            textAlign: 'left',
            paddingLeft: '20px',
          } : {
            color: colorTitle,
            fontSize: '1.1rem',
            width: '100%',
            textAlign: 'left',
            paddingLeft: '20px',
          }}
          >
            {title}
          </span>
          <button
            type="button"
            className="button-bg__none"
            style={{
              position: 'absolute',
              right: '10px',
              top: '50%',
              transform: 'translateY(-50%)',
            }}
            onClick={this.onToggle}
          >
            <img src={isShow ? sub : add} alt="sub" />
          </button>
        </button>
        <div
          className={isShow ? '' : 'hiden'}
          style={{
            textAlign: 'left',
            paddingLeft: '20px',
            fontSize: isTablet ? '0.8rem' : (isMobile ? '0.5rem' : '1rem'),
          }}
        >
          {ReactHtmlParser(description)}
        </div>
      </div>
    );
  }
}

ItemGift.propTypes = {
  description: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  isShow: PropTypes.bool.isRequired,

};

export default ItemGift;
