/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import {
  Player, ControlBar, Shortcut, BigPlayButton,
} from 'video-react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import MetaTags from 'react-meta-tags';

import {
  Container, Row, Col, Breadcrumb, BreadcrumbItem,
} from 'reactstrap';
import moment from 'moment';
import Sticky from 'react-sticky-el';
import HtmlParser from 'react-html-parser';
import { isMobile } from '../../DetectScreen';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import { toastrError } from '../../Redux/Helpers/notification';
import loadingPage from '../../Redux/Actions/loading';
import icBack from '../../image/icon/back-product.svg';
import icFacebook from '../../image/icon/ic-facebook-share.svg';
import icPinterest from '../../image/icon/ic-pinterest-share.svg';
import icMail from '../../image/icon/ic-mail-share.svg';
import icCompact from '../../image/icon/ic-compact-share.svg';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { loginUpdateUserInfo } from '../../Redux/Actions/login';
import icDefault from '../../image/icon/default-image.png';
import icBackToTop from '../../image/icon/ic-back-to-top.svg';
import icBackToTopMobile from '../../image/icon/ic-scroll-top-mobile.svg';
import {
  scrollTop,
  getSEOFromCms,
  fetchCMSHomepage,
  googleAnalitycs,
  generateUrlWeb,
  setPrerenderReady,
} from '../../Redux/Helpers';
import './styles.scss';
import addCmsRedux from '../../Redux/Actions/cms';
import SearchBar from '../Blogs/components/SearchBar';
import WhatTrending from '../Blogs/components/WhatTrending';
import RecentStories from '../Blogs/components/RecentStories';
import SubscribeMail from '../Blogs/components/SubscribeMail';
import Product from '../Blogs/components/Product';
import AuthorSection from './components/AuthorSection';
import ShopOnInsta from './components/ShopOnInsta';
import Comment from './components/Comment';
import RelatedStories from './components/RelatedStories';
import BannerArticle from './components/Banner';
import SliderArticle from './components/SliderArticle';
import {
  // GET_ARTICLE,
  // GET_ALL_ARTICLES,
  GET_COMMENT,
  POST_COMMENT,
} from '../../config';
import FooterV2 from '../../views2/footer';


const prePareCms = (mmoCms) => {
  if (mmoCms) {
    const seo = getSEOFromCms(mmoCms);
    // const { body } = mmoCms;
    return {
      seo,
    };
  }
  return {
    seo: undefined,
  };
};
const prePareDataArticle = (data) => {
  const { body } = data;
  if (data) {
    const newLetterBlock = _.filter(
      body,
      x => x.type === 'newsletter_block',
    )[0].value;
    const whatTrendingBlocks = _.filter(
      body,
      x => x.type === 'trending_block',
    )[0].value;
    const labelBlocks = _.filter(body, x => x.type === 'label_block');
    return {
      newLetterBlock,
      whatTrendingBlocks,
      labelBlocks,
    };
  }
  return {
    newLetterBlock: [],
    whatTrendingBlocks: [],
    labelBlocks: [],
  };
};
class ArticleBlog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seo: undefined,
      dataArticleDetail: null,
      commentData: null,
      isOpenCommentForm: false,
      content: '',
    };
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs(window.location.pathname);
    this.fetchDataInit();
    this.fetchDataArticleDetail();
    this.fetchComment();
    setTimeout(() => {
      const addthisScript = document.createElement('script');
      addthisScript.setAttribute(
        'src',
        'https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f1a93a6abb40133',
      );
      if (document.getElementById('article-page')) { document.body.appendChild(addthisScript); }
    });
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  handleScroll = () => {
    if (
      document.body.scrollTop > 600
      || document.documentElement.scrollTop > 600
    ) {
      const ele = document.getElementById('scroll-top-btn');
      if (ele) {
        ele.style.opacity = '1';
      }
    } else {
      const ele = document.getElementById('scroll-top-btn');
      if (ele) {
        ele.style.opacity = '0';
      }
    }
  };

  handelToggleCommentForm = () => {
    const { isOpenCommentForm } = this.state;
    this.setState({ isOpenCommentForm: !isOpenCommentForm });
  };

  fetchDataArticleDetail = () => {
    const slug = this.props.match.params.slug
      ? this.props.match.params.slug
      : undefined;
    if (slug) {
      // const option = {
      //   url: GET_ARTICLE.replace('{slug}', `${slug}`),
      //   method: 'GET',
      // };

      // const pending = [fetchClient(option, true)];
      const pending = [fetchCMSHomepage(slug)];
      Promise.all(pending)
        .then((result) => {
          if (result && result.length > 0 && !result[0].isError) {
            const seoTitle = result[0].meta?.seo_title;
            const desSeo = result[0].meta?.search_description;
            this.setState({ dataArticleDetail: result, dataSeo: { seoTitle, desSeo } });
            return;
          }
          throw new Error();
        })
        .catch((err) => {
          this.props.history.push(generateUrlWeb('/blog'));
        });
    }
  };

  fetchComment = () => {
    const slug = this.props.match.params.slug
      ? this.props.match.params.slug
      : undefined;
    if (slug) {
      const option = {
        url: GET_COMMENT.replace('{slug}', `${slug}`),
        method: 'GET',
      };

      const pending = [fetchClient(option, true)];

      Promise.all(pending)
        .then((result) => {
          if (result && result.length > 0) {
            const revMyArr = [].concat(result[0]).reverse();
            this.setState({ commentData: revMyArr });
            return;
          }
          throw new Error(result.message);
        })
        .catch((err) => {
          toastrError(err.message);
        });
    }
  };

  postComment = (content) => {
    if (!content) {
      // toastrError("please input ");
      return;
    }
    const slug = this.props.match.params.slug
      ? this.props.match.params.slug
      : undefined;
    const { login } = this.props;

    const options = {
      url: POST_COMMENT.replace('{slug}', `${slug}`),
      method: 'POST',
      body: {
        user: login && login.user ? login.user.id : 90,
        name: login && login.user ? login.user.first_name : 'user',
        content,
      },
    };
    fetchClient(options)
      .then((result) => {
        if (result.isError) {
          toastrError('err');
          return;
        }
        const { commentData, isOpenCommentForm } = this.state;
        this.setState({ commentData: [result, ...commentData], isOpenCommentForm: !isOpenCommentForm });
        // this.setState({ isOpenCommentForm: !this.state.isOpenCommentForm });
      })
      .catch((err) => {
        toastrError(err);
      });
  };

  // fetchArticles = () => {
  //   const options = {
  //     url: GET_ALL_ARTICLES,
  //     method: 'GET',
  //   };
  //   return fetchClient(options);
  // };

  fetchDataInit = async () => {
    const { cms, mmos } = this.props;
    const { dataArticles } = this.state;
    const mmoCms = _.find(cms, x => x.title === 'blog');
    if (!mmoCms || _.isEmpty(mmos) || _.isEmpty(dataArticles)) {
      this.props.loadingPage(true);
      // const pending = [fetchCMSHomepage('blog'), this.fetchArticles()];
      const pending = [fetchCMSHomepage('blog')];
      try {
        const results = await Promise.all(pending);
        const cmsData = results[0];
        const resultDataArticles = results[0];
        this.props.addCmsRedux(cmsData);
        const { seo } = prePareCms(cmsData);
        const {
          newLetterBlock,
          whatTrendingBlocks,
          labelBlocks,
        } = prePareDataArticle(resultDataArticles);
        this.setState({
          seo,
          dataArticles: resultDataArticles,
          newLetterBlock,
          whatTrendingBlocks,
          labelBlocks,
        });
        this.props.loadingPage(false);
      } catch (error) {
        toastrError(error.message);
        this.props.loadingPage(false);
      }
    } else {
      const { cms: cmsPage } = this.props;
      const cmsT = _.find(cmsPage, x => x.title === 'blog');
      const { seo } = prePareCms(cmsT);
      // const products = this.createData(results);
      this.setState({
        seo,
        dataArticles,
        newLetterBlock,
        whatTrendingBlocks,
        labelBlocks,
      });
    }
  };

  handleScrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  handleRoute = (slug) => {
    this.props.history.push(generateUrlWeb(`/blog/${slug}`));
    window.location.reload();
  };

  render() {
    const {
      seo,
      dataArticles,
      dataArticleDetail,
      newLetterBlock,
      whatTrendingBlocks,
      commentData,
      isOpenCommentForm,
      labelBlocks,
    } = this.state;
    const { login } = this.props;

    const gender = login && login.user && login.user.gender ? login.user.gender : 'unisex';

    const filterNextArticle = dataArticles && dataArticles.posts && dataArticleDetail && dataArticleDetail[0].categories
      ? dataArticles.posts.filter(el => (el.categories ? el.categories[0] === dataArticleDetail[0].categories[0] : ''))
      : '';
    const indexCurrArticle = filterNextArticle && dataArticleDetail
      ? filterNextArticle.findIndex(
        x => x.slug === dataArticleDetail[0].meta.slug,
      )
      : 0;

    const dataArticle = dataArticleDetail ? dataArticleDetail[0] : [];
    const dataBannerHeader = dataArticleDetail && dataArticleDetail[0].body
      ? dataArticleDetail[0].body[0] ? dataArticleDetail[0].body[0].value.header_image : null
      : null;
    const dataHeader = dataArticleDetail && dataArticleDetail[0].body
      ? dataArticleDetail[0].body[0] ? dataArticleDetail[0].body[0].value.header : null
      : null;

    const backHome = _.filter(
      labelBlocks,
      x => x.value.name === 'back_to_home',
    );

    return (
      <div>
        <MetaTags>
          <title>{this.state.dataSeo?.seoTitle}</title>
          <meta name="description" content={this.state.dataSeo?.desSeo} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
        </MetaTags>
        {/* <HeaderHomePage isBgBlack isSpecialMenu isDisableScrollShow isNotShowRegion /> */}
        <HeaderHomePageV3 isRemoveMessage />

        <div className="wrapper-article-blogs">
          <SearchBar data={dataArticles || ''} />
          <div>
            {dataBannerHeader && !isMobile ? (
              <BannerArticle
                data={dataBannerHeader}
                dataHeader={dataHeader}
                dataArticle={dataArticle || ''}
              />
            ) : (
              ''
            )}
            {!isMobile ? (
              <Sticky
                className="add-this-sticky"
                topOffset={-80}
                stickyStyle={{ transform: 'translateY(80px)' }}
                boundaryElement="#article-page"
                hideOnBoundaryHit={false}
                disabled={isMobile}
              >
                <div className="add-this-custom">
                  <div
                    className="addthis_sharing_toolbox addthis_toolbox addthis_default_style addthis_32x32_style  animated faster slideInLeft"
                    data-addthis-title={
                      dataArticleDetail ? dataArticleDetail[0].title : ''
                    }
                    data-addthis-url={window.location.href}
                  >
                    <a className="addthis_button_facebook">
                      <img
                        src={icFacebook}
                        width="40"
                        height="40"
                        border="0"
                        alt="Share to Facebook"
                      />
                    </a>
                    <a className="addthis_button_pinterest_share">
                      <img
                        src={icPinterest}
                        width="40"
                        height="40"
                        border="0"
                        alt="Share to Pinterest"
                      />
                    </a>
                    <a className="addthis_button_email">
                      <img
                        src={icMail}
                        width="40"
                        height="40"
                        border="0"
                        alt="Share to Mail"
                      />
                    </a>
                    <a className="addthis_button_compact">
                      <img
                        src={icCompact}
                        width="40"
                        height="40"
                        border="0"
                        alt="Compact"
                      />
                    </a>
                  </div>
                </div>
              </Sticky>
            ) : (
              ''
            )}
            <Container className="container-lg" id="article-page">
              <Row>
                <Col md="12">
                  {
                    isMobile && (
                      <div
                        className="banner-articles"
                        style={{
                          backgroundImage: `url(${
                            dataArticle && dataArticle.image ? dataArticle.image : icDefault
                          })`,
                        }}
                      />
                    )
                  }
                  {isMobile && (
                  <Link className="back-btn" to={generateUrlWeb('/blog')}>
                    <img src={icBack} alt="back" />
                    {' '}
                    {backHome[0] ? backHome[0].value.label : 'Back to home'}
                  </Link>
                  )}
                  {isMobile && (
                    <Breadcrumb>
                      <BreadcrumbItem>
                        <a href="#">blog</a>
                      </BreadcrumbItem>
                      <BreadcrumbItem active>
                        {dataArticle && dataArticle.categories ? dataArticle.categories[0] : ''}
                      </BreadcrumbItem>
                    </Breadcrumb>
                  )}
                  {/* content article */}
                  <div
                    className={`article-content ${
                      isMobile ? 'article-content-mobile' : ''
                    }`}
                  >
                    <div className="info-article">
                      {isMobile && (
                        <div className="info">
                          <h3 className="title">
                            {dataArticle && dataArticle.title
                              ? dataArticle.title
                              : ''}
                          </h3>
                          <div className="div-row justify-center item-center">
                            <span className="writer-name">
                              Written by
                              {' '}
                              <span>
                                {dataArticle && dataArticle.owner ? dataArticle.owner.first_name : ''}
                                {' '}
                                {dataArticle && dataArticle.owner ? dataArticle.owner.last_name : ''}
                              </span>
                            </span>
                            <span className="date-upatde">
                              updated
                              {' '}
                              {dataArticle
                                && moment(dataArticle.last_published_at).format(
                                  'MMM DD, YYYY',
                                )}
                            </span>
                          </div>

                          <div className="add-this-custom-mobile">
                            <div
                              className="addthis_sharing_toolbox addthis_toolbox addthis_default_style addthis_32x32_style  animated faster slideInLeft"
                              data-addthis-title={
                                dataArticleDetail
                                  ? dataArticleDetail[0].title
                                  : ''
                              }
                              data-addthis-url={window.location.href}
                            >
                              <a className="addthis_button_facebook">
                                <img
                                  src={icFacebook}
                                  width="40"
                                  height="40"
                                  border="0"
                                  alt="Share to Facebook"
                                />
                              </a>
                              <a className="addthis_button_pinterest_share">
                                <img
                                  src={icPinterest}
                                  width="40"
                                  height="40"
                                  border="0"
                                  alt="Share to Pinterest"
                                />
                              </a>
                              <a className="addthis_button_email">
                                <img
                                  src={icMail}
                                  width="40"
                                  height="40"
                                  border="0"
                                  alt="Share to Mail"
                                />
                              </a>
                              <a className="addthis_button_compact">
                                <img
                                  src={icCompact}
                                  width="40"
                                  height="40"
                                  border="0"
                                  alt="Compact"
                                />
                              </a>
                            </div>
                          </div>
                        </div>
                      )}
                    </div>
                    {dataArticle && dataArticle.body
                      ? dataArticle.body.map((items, index) => (
                        <div key={index} className="item">
                          {
                            items.value.header.header_text && (
                              <h2 className="title">
                                {items.value.header.header_text}
                              </h2>
                            )
                          }
                          <div className="image-article">
                            {items.value.carousel.length == 0 ? (
                              <div
                                className="image"
                                style={{
                                  backgroundImage: `url(${
                                    items.value.header_image.image
                                      ? items.value.header_image.image
                                      : icDefault
                                  })`,
                                }}
                              >
                                <span className={isMobile ? 'hidden' : ''}>
                                  {items.value.header_image ? items.value.header_image.caption : ''}
                                </span>
                              </div>
                            ) : (
                              ''
                            )}
                          </div>
                          {items.value.carousel.length > 0 ? (
                            <SliderArticle data={items.value.carousel} />
                          ) : (
                            ''
                          )}
                          <div className="content">
                            {HtmlParser(items.value.paragraph)}
                          </div>
                        </div>
                      ))
                      : ''}
                  </div>
                  <div
                    className="up-next-section"
                    style={{
                      display:
                        filterNextArticle
                        && indexCurrArticle
                        && filterNextArticle[indexCurrArticle + 1]
                          ? 'block'
                          : 'none',
                    }}
                  >
                    <span>UP NEXT:</span>
                    <a
                      href={
                        filterNextArticle
                        && indexCurrArticle
                        && filterNextArticle[indexCurrArticle + 1]
                          ? `/blog/${
                            filterNextArticle[indexCurrArticle + 1].slug
                          }`
                          : '#'
                      }
                    >
                      {filterNextArticle
                      && indexCurrArticle
                      && filterNextArticle[indexCurrArticle + 1]
                        ? filterNextArticle[indexCurrArticle + 1].title
                        : '#'}
                    </a>
                  </div>

                  <div className="product-article">
                    <Product data={dataArticle || dataArticles} history={this.props.history} />
                  </div>

                  <Comment
                    postComment={this.postComment}
                    data={commentData}
                    login={login}
                    toggleCommentForm={this.handelToggleCommentForm}
                    isOpenCommentForm={isOpenCommentForm}
                    labelBlocks={labelBlocks}
                  />
                </Col>
                {/* <Col md="4">
                  <AuthorSection
                    data={dataArticle ? dataArticle.owner : ''}
                    labelBlocks={labelBlocks}
                  />
                  {isMobile && (
                    <RecentStories
                      dataArticles={dataArticles}
                      isArticlePage
                    />
                  )}
                  <ShopOnInsta />
                  {!isMobile && (
                    <WhatTrending
                      data={whatTrendingBlocks}
                      dataArticles={dataArticles}
                    />
                  )}
                  {!isMobile && (
                    <SubscribeMail
                      data={newLetterBlock}
                      login={login}
                      loginUpdateUserInfo={this.props.loginUpdateUserInfo}
                    />
                  )}
                  <Product data={dataArticle || dataArticles} />
                </Col> */}
              </Row>
            </Container>
            <RelatedStories
              data={filterNextArticle}
              handleRoute={this.handleRoute}
              indexCurrArticle={indexCurrArticle}
            />
          </div>
          <div
            className="scroll-top-btn"
            id="scroll-top-btn"
            onClick={() => this.handleScrollTop()}
          >
            <img src={isMobile ? icBackToTopMobile : icBackToTop} alt="icon" />
            <span style={{ display: isMobile ? 'none' : 'block' }}>
              back to the top
            </span>
          </div>
          <FooterV2 />
        </div>
      </div>
    );
  }
}

ArticleBlog.propTypes = {
  loadingPage: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  loginUpdateUserInfo: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    cms: state.cms,
    mmos: state.mmos,
    basket: state.basket,
  };
}

const mapDispatchToProps = {
  loadingPage,
  addCmsRedux,
  loginUpdateUserInfo,
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ArticleBlog),
);
