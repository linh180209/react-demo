import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import { Row, Col, Container } from 'reactstrap';
import { Link } from 'react-router-dom';
import icYoutube from '../../../../image/icon/ic-youtube-blog.svg';
import icInstagram from '../../../../image/icon/ic-instagram-blog.svg';
import icFacebook from '../../../../image/icon/ic-facebook-blog.svg';
import icTwitter from '../../../../image/icon/ic-twitter-blog.svg';
import icLinked from '../../../../image/icon/ic-linked-blog.svg';
import defaultAvatar from '../../../../image/icon/default-avatar.png';

import './styles.scss';

class AuthorSection extends Component {
  render() {
    const { data, labelBlocks } = this.props;
    const hi = _.filter(
      labelBlocks,
      x => x.value.name === 'hi_im',
    );
    const ownerAndChief = _.filter(
      labelBlocks,
      x => x.value.name === 'owner_and_chief',
    );
    const more = _.filter(
      labelBlocks,
      x => x.value.name === 'more_about_me',
    );

    return (
      <div className="author-section">
        <div className="author-info">
          <div className="image-article">
            <div
              className="image"
              style={{ backgroundImage: `url(${data && data.avatar ? data.avatar : defaultAvatar})` }}
            />
          </div>
          <div className="info">
            <h4>
              {hi[0] ? hi[0].value.label : "Hi, i'm"}
              {' '}
              {data ? data.last_name : ''}
            </h4>
            <p>{ownerAndChief[0] ? ownerAndChief[0].value.label : 'Owner and chief scent designer at Maison 21G.'}</p>
          </div>
        </div>
        <div className="social-info">
          <a href={data && data.youtube ? `${data.youtube}` : '#'} style={{ display: data && data.youtube ? 'inherit' : 'none' }}>
            <img src={icYoutube} />
          </a>
          <a href={data && data.instagram ? `${data.instagram}` : '#'} style={{ display: data && data.instagram ? 'inherit' : 'none' }}>
            <img src={icInstagram} />
          </a>
          <a href={data && data.facebook ? `${data.facebook}` : '#'} style={{ display: data && data.facebook ? 'inherit' : 'none' }}>
            <img src={icFacebook} />
          </a>
          <a href={data && data.twitter ? `${data.twitter}` : '#'} style={{ display: data && data.twitter ? 'inherit' : 'none' }}>
            <img src={icTwitter} />
          </a>
          <a href={data && data.linkedin ? `${data.linkedin}` : '#'} style={{ display: data && data.linkedin ? 'inherit' : 'none' }}>
            <img src={icLinked} />
          </a>
        </div>
        <Link to="#" className="btn-more">
          {more[0] ? more[0].value.label : 'More about me'}
        </Link>
      </div>
    );
  }
}

export default AuthorSection;
