import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import moment from 'moment';
import { Link } from 'react-router-dom';
import icBack from '../../../../image/icon/ic-back-black.svg';
import icDefault from '../../../../image/icon/default-image.png';
import { generateUrlWeb } from '../../../../Redux/Helpers';

class Banner extends Component {
  render() {
    const { data, dataHeader, dataArticle } = this.props;
    return data && dataHeader ? (
      <div className="banner-articles-blog">
        <div
          className="banner-articles"
          style={{
            backgroundImage: `url(${
              dataArticle && dataArticle.image ? dataArticle.image : icDefault
            })`,
          }}
        />
        <div className="info-banner">
          <Breadcrumb>
            <BreadcrumbItem>
              <Link href={generateUrlWeb('/blog')}>blog</Link>
            </BreadcrumbItem>
            <BreadcrumbItem active>
              <Link
                to={generateUrlWeb(`/blog/categories/${
                  dataArticle ? dataArticle.categories[0]?.split(' ').join('-') : ''
                }`)}
              >
                {dataArticle ? dataArticle.categories[0] : ''}
              </Link>
            </BreadcrumbItem>
          </Breadcrumb>
          <h1 className="title">
            {dataArticle.title ? dataArticle.title : ''}
          </h1>

          <div className="owner-info">
            <span>
              WRITTEN BY
              {' '}
              <b>{dataArticle ? `${dataArticle.owner.first_name} ${dataArticle.owner.last_name}` : ''}</b>
            </span>
            <span>
              UPDATED
              {' '}
              {dataArticle
                && moment(dataArticle.last_published_at).format('MMMM DD, YYYY')}
            </span>
          </div>
          {/* {dataArticle && (
          <div className="tag">
            {dataArticle.tags ? dataArticle.tags[0] : ''}
          </div>
          )}
          {dataArticle ? (
            <span className="cat">{dataArticle.categories[0]}</span>
          ) : (
            ''
          )}
          <span className="time">
            {dataArticle
                && moment(dataArticle.last_published_at).format('MMMM DD, YYYY')}
          </span> */}
          <Link className="back-btn" to={generateUrlWeb('/blog')}>
            <img src={icBack} alt="back" />
            {' '}
            back to home
          </Link>
        </div>
      </div>
    ) : (
      ''
    );
  }
}

export default Banner;
