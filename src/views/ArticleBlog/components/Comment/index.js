import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import Disqus from 'disqus-react';
import moment from 'moment';
import icReply from '../../../../image/icon/ic-reply.svg';
import defaultAvatar from '../../../../image/icon/default-avatar.png';
import { toastrError, toastrSuccess } from '../../../../Redux/Helpers/notification';
import CommentForm from './CommentForm';
import { isMobile } from '../../../../DetectScreen';

class Comment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pageSize: isMobile ? 1 : 6,
    };
  }

  handleLoadMore = () => {
    if (this.props.data.length > this.state.pageSize) {
      this.setState({ pageSize: this.state.pageSize + 6 });
    }
  };

  handleShowAll = () => {
    if (this.props.data.length > 1) {
      this.setState({ pageSize: this.props.data.length });
    }
  };

  render() {
    // const disqusShortname = "maison21g";
    // const disqusConfig = {
    //   //url: "0.0.0.0:30032",
    //   identifier: "456",
    //   //stitle: "Title of Your Article",
    // };

    const { pageSize } = this.state;
    const {
      postComment,
      data,
      login,
      toggleCommentForm,
      isOpenCommentForm,
      labelBlocks,
    } = this.props;
    // const {} = this.state;
    const loginAlert = _.filter(
      labelBlocks,
      x => x.value.name === 'login_alert',
    );
    const leaveComment = _.filter(
      labelBlocks,
      x => x.value.name === 'leave_a_comment',
    );
    const loadMore = _.filter(
      labelBlocks,
      x => x.value.name === 'load_more',
    );
    const reply = _.filter(
      labelBlocks,
      x => x.value.name === 'reply',
    );
    const allComments = _.filter(
      labelBlocks,
      x => x.value.name === 'all_comments',
    );
    return (
      <div className="comment-blog">
        {/* <Disqus.DiscussionEmbed
          shortname={disqusShortname}
          config={disqusConfig}
        /> */}
        <h3 className="leave-comment">{leaveComment[0] ? leaveComment[0].value.label : 'leave a comment'}</h3>
        <div className="header-comment">
          <span>
            {allComments[0] ? allComments[0].value.label : 'All Comments'}
            {' '}
            (
            {data ? data.length : 0}
            )
          </span>
          <button
            className="reply-btn"
            onClick={() => (login && login.user ? toggleCommentForm() : toastrError(loginAlert[0] ? loginAlert[0].value.label : 'Please login to comment this post'))}
          >
            <img src={icReply} />
            {' '}
            {reply[0] ? reply[0].value.label : 'Reply'}
          </button>
        </div>
        {isOpenCommentForm ? (
          <CommentForm
            postComment={postComment}
            login={login}
            toggleCommentForm={toggleCommentForm}
          />
        ) : (
          ''
        )}

        {data
          ? data.slice(0, pageSize).map(x => (
            <div className="comment-item animated fadeIn faster">
              <div className="info-user">
                <div className="image-user">
                  <div
                    className="image"
                    style={{
                      backgroundImage: `url(${
                        x.user && x.user.avatar
                          ? x.user.avatar
                          : defaultAvatar
                      })`,
                    }}
                  />
                </div>
                <div className="info">
                  <span className="name">{x.name ? x.name : ''}</span>
                  <span className="date">
                    {x.date_created
                      ? moment(x.date_created).format('DD/MM/YY h:mma')
                      : ''}
                  </span>
                </div>
              </div>
              <div className="content">{x.content ? x.content : ''}</div>
            </div>
          ))
          : ''}
        <div
          className="paging"
          style={{
            display: data && data.length >= pageSize ? 'block' : 'none',
          }}
        >
          {!isMobile ? (
            <button
              onClick={() => this.handleLoadMore()}
              disbled={data && !data.length > 6}
              style={{ opacity: data && data.length > 6 ? 1 : 0.2 }}
            >
              {loadMore[0] ? loadMore[0].value.label : 'load more'}
            </button>
          ) : (
            <div style={{ display: data && data.length == pageSize ? 'none' : 'block' }}>
              <div className="overlay" />
              <button
                className="btn-show-all"
                onClick={() => this.handleShowAll()}
                disbled={data && !data.length > 1}
                style={{ opacity: data && data.length > 1 ? 1 : 0.2 }}
              >
                Show all
                {' '}
                {data && data.length ? data.length : ''}
                {' '}
                replies
              </button>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default Comment;
