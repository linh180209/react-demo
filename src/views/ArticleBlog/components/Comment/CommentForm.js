import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import { Input } from 'reactstrap';
import defaultAvatar from '../../../../image/icon/default-avatar.png';
import icReply from '../../../../image/icon/ic-reply.svg';

class CommentForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
    };
  }

  changeComment = (e) => {
    this.setState({ content: e.target.value });
  };

  render() {
    const { login, toggleCommentForm, postComment } = this.props;
    return (
      <div className="comment-blog comment-form animated fadeIn faster">
        <div className="comment-item">
          <div className="info-user">
            <div className="image-user">
              <div
                className="image"
                style={{
                  backgroundImage: `url(${
                    login && login.user && login.user.avatar
                      ? login.user.avatar
                      : defaultAvatar
                  })`,
                }}
              />
            </div>
            <div className="info">
              <span className="name">You</span>
              <span className="date">Now</span>
            </div>
          </div>
          <div className="content">
            <Input
              type="textarea"
              className="input-comment"
              name="comment"
              onChange={e => this.changeComment(e)}
            />
          </div>
          <div className="btn-box">
            <button className="btn-cancel" onClick={() => toggleCommentForm()}>
              Cancel
            </button>
            <button className="btn-submit" onClick={() => postComment(this.state.content)}>
              Submit
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default CommentForm;
