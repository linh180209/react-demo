import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import { Row, Col, Container } from 'reactstrap';
import moment from 'moment';
import _ from 'lodash';
import { withRouter, Link } from 'react-router-dom';
import icYoutube from '../../../../image/icon/ic-youtube-blog.svg';
import icInstagram from '../../../../image/icon/ic-instagram-blog.svg';
import icFacebook from '../../../../image/icon/ic-facebook-blog.svg';
import icTwitter from '../../../../image/icon/ic-twitter-blog.svg';
import icLinked from '../../../../image/icon/ic-linked-blog.svg';
import './styles.scss';
import icDefault from '../../../../image/icon/default-image.png';
import { generateUrlWeb } from '../../../../Redux/Helpers';

class RelatedStories extends Component {
  render() {
    const { data, handleRoute, indexCurrArticle } = this.props;
    return (
      <div className="related-stories-section">
        {data ? (
          <Container>
            <h3 className="related-stories-cap">Related Stories</h3>
            <Row>
              {_.map(data.slice(0, 8), (x, i) => (i != indexCurrArticle ? (
                <Col md="3">
                  <div className="article">
                    <div
                      className="image-article"
                      onClick={() => handleRoute(x.slug)}
                    >
                      <div
                        className="image"
                        style={{
                          backgroundImage: `url(${
                            x.image ? x.image : icDefault
                          })`,
                        }}
                      />
                    </div>
                    <div className="info-article">
                      <div className="info-title">
                        <span className="cat">
                          {x.categories
                            ? _.map(x.categories, (x, i) => (
                              <Link to={generateUrlWeb(`/blog/categories/${x.split(' ').join('-')}`)} key={i}>
                                {i === 0 ? x : `, ${x}`}
                              </Link>
                            ))
                            : ''}
                        </span>
                        <span
                          className="date"
                          onClick={() => handleRoute(x.slug)}
                        >
                          {x.first_published_at
                            ? moment().subtract(7, 'd').format('YYYY-MM-DD')
                                < moment(x.first_published_at).format(
                                  'YYYY-MM-DD',
                                )
                              ? moment(x.first_published_at).fromNow()
                              : moment(x.first_published_at).format(
                                'DD/MM/YYYY',
                              )
                            : ''}
                        </span>
                      </div>
                      <h4
                        className="title"
                        onClick={() => handleRoute(x.slug)}
                      >
                        {x.title ? x.title : ''}
                      </h4>
                    </div>
                  </div>
                </Col>
              ) : (
                ''
              )))}
            </Row>
          </Container>
        ) : (
          ''
        )}
      </div>
    );
  }
}

export default withRouter(RelatedStories);
