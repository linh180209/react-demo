import React, { Component } from 'react';
import _ from 'lodash';
import htmlPare from 'react-html-parser';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './styles.scss';
import { Row, Col, Container } from 'reactstrap';
import { Link } from 'react-router-dom';
// import { generaCurrency } from '../../../../Redux/Helpers';

class SliderArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nav1: null,
      nav2: null,
    };
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2,
    });
  }

  render() {
    const { data } = this.props;
    return data.length > 0 ? (
      <div className="slider-article">
        <Slider
          asNavFor={this.state.nav2}
          ref={slider => (this.slider1 = slider)}
          arrows={false}
          className="slider-main"
        >
          {_.map(data, x => (
            <div>
              <div className="image-wrapper">
                <div
                  className="image"
                  style={{
                    backgroundImage: `url(${
                      x.value.image ? x.value.image : ''
                    })`,
                  }}
                />
              </div>
            </div>
          ))}
        </Slider>
        <Slider
          asNavFor={this.state.nav1}
          ref={slider => (this.slider2 = slider)}
          slidesToShow={data.length < 6 ? data.length : 6}
          swipeToSlide
          focusOnSelect
          className="slider-child"
          // variableWidth={true}
        >
          {_.map(data, x => (
            <div>
              <div
                className="wrapper-image"
                style={{
                  backgroundImage: `url(${x.value.image ? x.value.image : ''})`,
                }}
              />
            </div>
          ))}
          {/* <div>
            <div
              className="wrapper-image"
              style={{
                backgroundImage: `url(${testBanner1 ? testBanner1 : ""})`,
              }}
            ></div>
          </div>
          <div>
            <div
              className="wrapper-image"
              style={{
                backgroundImage: `url(${testBanner1 ? testBanner1 : ""})`,
              }}
            ></div>
          </div>
          <div>
            <h3>3</h3>
          </div>
          <div>
            <h3>4</h3>
          </div>
          <div>
            <h3>5</h3>
          </div>
          <div>
            <h3>6</h3>
          </div> */}
          {/* <div>
            <h3>7</h3>
          </div>  */}
        </Slider>
      </div>
    ) : (
      ''
    );
  }
}

export default SliderArticle;
