import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';

const RenderItem = (props) => {
  const { item } = props;
  return (
    <div>
      <div className="__item">
        <div>
          <img loading="lazy" src={item.thumbnail_src} alt="" className="img-fluid" />
        </div>
      </div>
    </div>
  );
};

class ShopOnInsta extends Component {
  constructor(props) {
    super(props);

    this.state = {
      instagramData: [],
    };
  }

  componentDidMount() {
    fetch(
      'https://www.instagram.com/graphql/query/?query_hash=e769aa130647d2354c40ea6a439bfc08&variables={"id":"10954293359", "first":12}',
    )
      .then(response => response.json())
      .then((data) => {
        const instagramData = data.data.user.edge_owner_to_timeline_media.edges.map(
          item => item.node,
        );
        this.setState({
          instagramData,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    const { data } = this.props;
    const { instagramData } = this.state;
    return (
      <div className="shop-insta-blog">
        {instagramData.length !== 0 && (
          <div className="items">
            {instagramData.slice(0, 6).map(item => (
              <RenderItem item={item || {}} />
            ))}
          </div>
        )}
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://www.instagram.com/maison21g/"
          className="btn-shop"
        >
          Shop on instagram
        </a>
      </div>
    );
  }
}

export default ShopOnInsta;
