import classnames from 'classnames';
import _ from 'lodash';
import moment from 'moment';
import React, { useEffect } from 'react';
import Slider from 'react-slick';
import { isMobile480fn, isMobile991fn } from '../../../DetectScreen';
import { icDot } from '../../../imagev2/svg';
import { getLinkFromButtonBlock, getNameFromButtonBlock } from '../../../Redux/Helpers';
import { useMergeState } from '../../../Utils/customHooks';
import useWindowEvent from '../../../Utils/useWindowEvent';
import './styles.scss';

function ReviewsV3(props) {
  const customeReviewBt = getNameFromButtonBlock(props.buttonBlocks, 'Customer Reviews');

  const [settings, setSettings] = useMergeState({
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 3000,
    pauseOnHover: true,
    arrows: true,
    infinite: false,
    variableWidth: false
  });

  const [screenMode, setScreenMode] = useMergeState({
    isMobile: false,
    isTablet: false,
  })

  const handleInitResize = () => {
    // check size mobile, tablet under for review.
    // review have 2 slides type for each mode.

    if(isMobile991fn()) {
      if(isMobile480fn()) setScreenMode({ isMobile: true, isTablet: false });
      else setScreenMode({ isTablet: true, isMobile: false });
    }else {
      setScreenMode({ isTablet: false, isMobile: false });
    }
  };

  useWindowEvent('resize', handleInitResize, window);

  useEffect(() => {
    if(screenMode.isTablet || isMobile991fn()) {
      setSettings({
        className: 'slider variable-width',
        variableWidth: true,
        autoplay: false,
        slidesToShow: 1,
        infinite: false,
      });
    }

    if(screenMode.isMobile || isMobile480fn()) {
      setSettings({
        className: 'slider',
        variableWidth: false,
        autoplay: true,
        slidesToShow: 1,
        infinite: true,
      });
    }

    return () => {
      setSettings({
        className: 'slider',
        variableWidth: false,
        autoplay: false,
        slidesToShow: 3,
        infinite: false,
      });
    }

  }, [screenMode]);

  useEffect(() => {
    if(props.dataFAQ.reviews?.length > 3) {
      setSettings({ arrows: true });
    }
  }, [props.dataFAQ?.reviews]);

  const itemReview = d => (
    <div className='row-review'>
      <div className={classnames(`item-review-v3 ${props?.pageType === 'gift' ? 'item-review-gift' : ''}`)}>
        <img src={icDot} alt="dots" />
        <div className="div-des">
          {d.comment}
        </div>
        <div className="name">
          {d.name}
        </div>
        {props?.pageType ? <div className='gift'>{d?.giftType}</div> : (
          <div className="date">
            {moment(d?.date_created).format('DD MMM YYYY')}
          </div>
        )}
      </div>
    </div>
  );

  if (!props.dataFAQ?.reviews || props.dataFAQ?.reviews?.length === 0) {
    return null;
  }

  return (
    <div className="reviews-v3">
      <div className={`content-reviews ${props.pageType === 'gift' ? '' : 'padding-x'}`}>
        <div className="text-title">
          {props?.pageType === 'gift' ? props.title : customeReviewBt}
          {props?.pageType === 'gift' && props.description && <div className='text-des'>{props.description}</div>}
        </div>
        <Slider {...settings}>
          { _.map(props.dataFAQ?.reviews, d => (
            itemReview(d)
          ))}
        </Slider>
      </div>
    </div>
  );
}

export default ReviewsV3;
