import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classnames from 'classnames';
import ImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css';
import bottleHomeScent from '../../image/bottle_home_card.png';
// import bottlePerfume from '../../image/new_bottle.png';
import bottlePerfume from '../../image/new_bottle.jpeg';
import { isMobile } from '../../DetectScreen';
import BottleCustom from '../../components/B2B/CardItem/bottleCustom';
import { COLOR_CUSTOME, FONT_CUSTOME } from '../../constants';
import { getNameFromButtonBlock } from '../../Redux/Helpers';
import icClose from '../../image/icon/ic-close-video.svg';
import './imageBlock.scss';

const checkCustomedBottle = (dataCustom) => {
  const {
    image, nameBottle, font, color,
  } = dataCustom;
  if (image || nameBottle || font !== FONT_CUSTOME.JOST || color !== COLOR_CUSTOME.BLACK) {
    return true;
  }
  return false;
};

const arraymove = (arr, fromIndex, toIndex) => {
  const element = arr[fromIndex];
  arr.splice(fromIndex, 1);
  arr.splice(toIndex, 0, element);
};
class ImageBlock extends Component {
  componentDidMount = () => {
    const { images, dataCustom, isSelectedScent } = this.props;
    _.forEach(images, (d) => {
      if (d.embedUrl) {
        d.renderItem = () => this._renderVideo(d);
      } else if (d.dataCustomBottle) {
        d.renderItem = () => this._renderBottle(dataCustom, d.isHomeScent, d.imageBottle);
        d.renderThumbInner = () => this._renderBottleThumnail(dataCustom, d.isHomeScent, d.imageBottle);
      } else if (d.isImageContain || d.isSample || d.isDualCandles || d.isWaxPerfume || d.isDualCrayons || d.isBundleCreation || d.isReedDiffuser) {
        d.renderItem = () => this._renderBottleRollon(d);
        d.renderThumbInner = () => this._renderBottleRollonThumnail(d);
      }
    });

    // if customed bottle move image to top
    if (checkCustomedBottle(dataCustom)) {
      this.moveCustomeBottle(images);
    } else if (!isSelectedScent) {
      this.moveCustomeBottle(images, images.length - 1);
    }
  }

  componentDidUpdate(prevProps) {
    const { images, dataCustom, isSelectedScent } = this.props;
    if (images !== prevProps.images || dataCustom !== prevProps.dataCustom) {
      _.forEach(images, (d) => {
        if (d.embedUrl) {
          d.renderItem = () => this._renderVideo(d);
        } else if (d.dataCustomBottle) {
          d.renderItem = () => this._renderBottle(dataCustom, d.isHomeScent, d.imageBottle);
          d.renderThumbInner = () => this._renderBottleThumnail(dataCustom, d.isHomeScent, d.imageBottle);
        } else if (d.isImageContain || d.isSample || d.isDualCandles || d.isWaxPerfume || d.isDualCrayons || d.isBundleCreation) {
          d.renderItem = () => this._renderBottleRollon(d);
          d.renderThumbInner = () => this._renderBottleRollonThumnail(d);
        }
      });

      // if customed bottle move image to top or bottom
      if (checkCustomedBottle(dataCustom)) {
        this.moveCustomeBottle(images, 0);
      } else if (!isSelectedScent) {
        this.moveCustomeBottle(images, images.length - 1);
      }
      this.forceUpdate();
    }
  }

  moveCustomeBottle = (listData, toIndex) => {
    const indexBottle = _.findIndex(listData, d => d.dataCustomBottle);
    if (indexBottle > -1 && indexBottle !== toIndex) {
      arraymove(listData, indexBottle, toIndex);
    }
  }

  _renderBottleRollonThumnail = item => (
    <div className="image-gallery-image-thumbnail">
      <div className="image-cutome-bottle">
        <img className="image-rollon" src={item.thumbnail} alt="thumnail" />
      </div>
    </div>
  )

  _renderBottleThumnail = (item, isHomeScent, imageBottle) => (
    <div className="image-gallery-image-thumbnail">
      <div className="image-cutome-bottle">
        <BottleCustom
          isImageText={!!item.image}
          onGotoProduct={() => {}}
          image={item.image}
          isBlack={item.color === COLOR_CUSTOME.BLACK}
          font={item.font}
          color={item.color}
          eauBt={getNameFromButtonBlock(this.props.buttonBlocks, 'Eau de parfum')}
          mlBt={getNameFromButtonBlock(this.props.buttonBlocks, 'ml25')}
          isDisplayName
          name={item.nameBottle}
          combos={item.combos}
          bottleImage={isHomeScent ? bottleHomeScent : imageBottle}
        />
      </div>
    </div>

  )

  _renderBottleRollon = item => (
    <div className="image-gallery-image">
      <div className="image-cutome-bottle">
        <img className="image-rollon" src={item.original} alt="thumnail" />
      </div>
    </div>

  )

  _renderBottle = (item, isHomeScent, imageBottle) => (
    <div className="image-gallery-image">
      <div className="image-cutome-bottle">
        <BottleCustom
          isImageText={!!item.image}
          onGotoProduct={() => {}}
          image={item.image}
          isBlack={item.color === COLOR_CUSTOME.BLACK}
          font={item.font}
          color={item.color}
          eauBt={getNameFromButtonBlock(this.props.buttonBlocks, 'Eau de parfum')}
          mlBt={getNameFromButtonBlock(this.props.buttonBlocks, 'ml25')}
          isDisplayName
          name={item.nameBottle}
          combos={item.combos}
          bottleImage={isHomeScent ? bottleHomeScent : imageBottle}
        />
      </div>
    </div>

  )

  _renderVideo = item => (
    <div className="image-gallery-image">
      <div
        className="video-wrapper"
      >
        <video
          id="myVideo"
          style={{
            width: '100%',
            // height: '502px',
          }}
          muted="true"
          playsinline="true"
          preload="auto"
          loop
          poster={item.thumbnail}
          controls
        >
          <source src={item.embedUrl} />
        </video>
        {/* <iframe
          width="100%"
          height="502"
          src={item.embedUrl}
          frameBorder="0"
          // allowFullScreen
        /> */}
      </div>
    </div>
  )

  render() {
    const {
      images, datas, isActive, onChangeProduct, isPopUp, isQuizPage, isProductDetailV2,
    } = this.props;
    // console.log('images', images);
    return (
      <div className={classnames('div-image-block div-col', this.props.className)}>
        {
          isPopUp && (
            <button
              onClick={this.props.onClose}
              type="button"
              className="button-bg__none bt-close"
            >
              <img src={icClose} alt="icClose" />
            </button>
          )
        }
        <div className={`${datas.length > 1 ? 'div-name' : 'hidden'}`}>
          {
            _.map(datas, (d, index) => (
              <React.Fragment>
                <button
                  type="button"
                  className={`${isActive(d.shortTitle) ? 'active' : ''} button-bg__none`}
                  onClick={() => onChangeProduct(d)}
                >
                  {d.shortTitle}
                </button>
                {
                  index === (datas.length - 1) ? <div /> : (
                    <span style={{ color: '#8D8D8D' }} className="ml-2 mr-2">
                      -
                    </span>
                  )
                }
              </React.Fragment>

            ))
          }
        </div>
        <div className={classnames('div-image')}>
          <ImageGallery
            useTranslate3D={false}
            items={images}
            infinite={false}
            // disableThumbnailScroll
            showPlayButton={false}
            showFullscreenButton={false}
            thumbnailPosition={isMobile || isQuizPage ? 'bottom' : isProductDetailV2 ? 'right' : 'bottom'}
            onClick={e => this.props.onClickImage(e)}
            // onThumbnailClick={(event, index) => {
            //   const ele = document.getElementsByClassName('image-gallery-thumbnails');
            //   console.log('ele', ele);
            //   if (ele?.length > 0) {
            //     ele[0].scrollTop = 0;
            //   }
            // }}
          />
        </div>
        {/* <div className={isMobile ? "hidden" : "div-share div-col items-start"}>
          <span>
          Share with friends
          </span>
          <img className="mt-1" src={icFB} alt="icFB" />
        </div> */}
      </div>
    );
  }
}

ImageBlock.defaultProps = {
  onClickImage: () => {},
  onClose: () => {},
  isPopUp: false,
};

ImageBlock.propTypes = {
  images: PropTypes.arrayOf().isRequired,
  datas: PropTypes.arrayOf().isRequired,
  isActive: PropTypes.func.isRequired,
  onChangeProduct: PropTypes.func.isRequired,
  onClickImage: PropTypes.func,
  isPopUp: PropTypes.bool,
  onClose: PropTypes.bool,
};

export default ImageBlock;
