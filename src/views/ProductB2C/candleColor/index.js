import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import icCandleColor from '../../../image/icon/ic-candle-color.svg';
import { candleColor } from '../../../imagev2/svg';
import './styles.scss';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import { GET_VARIANT_VALUE } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import homeScent from '../../../imagev2/png/home-scent.png';

function CandleColor(props) {
  const [dataVariant, setDataVariant] = useState([]);
  const getVariant = async () => {
    const option = {
      url: GET_VARIANT_VALUE.replace('{product_type}', props.type),
      method: 'GET',
    };
    try {
      const data = await fetchClient(option);
      console.log('CandleColor', data);
      if (data.length > 0) {
        setDataVariant(data);
        props.updateColorCandle(data[0]?.value, _.filter(data[0]?.images, x => x.type === null));
        console.log('getVariant', data);
      }
    } catch (error) {
      toastrError(error.message);
    }
  };

  useEffect(() => {
    getVariant();
  }, []);

  const optinalBt = getNameFromButtonBlock(props.buttonBlocks, 'Optional');
  const candleColorBt = getNameFromButtonBlock(props.buttonBlocks, 'Candle Color');
  const chooseCandleBt = getNameFromButtonBlock(props.buttonBlocks, 'Choose the color for your Candle');
  const chooseCandle = getNameFromButtonBlock(props.buttonBlocks, 'choose candle color');

  if (dataVariant.length === 0) {
    return null;
  }

  if (props.isVersion2) {
    return (
      <div className="candle-color version-2">
        <div className="div-image">
          <img src={candleColor} alt="icon" />
        </div>
        <div className="des">
          3.
          {' '}
          {chooseCandle}
        </div>
        {
            _.map(dataVariant, d => (
              <button
                onClick={() => props.updateColorCandle(d.value, _.filter(d?.images, x => x.type === null))}
                type="button"
                className={classnames('button-bg__none', props.variantValues === d.value ? 'active' : '')}
              >
                <img src={d.image || _.find(d.images, x => x.type === 'main')?.image} alt="icon" />
                <div>
                  {d?.name}
                </div>
              </button>
            ))
          }
      </div>
    );
  }

  return (
    <React.Fragment>
      <div className="candle-color">
        <div className="div-image">
          <img src={icCandleColor} alt="icon" />
        </div>
        <div className="des">
          <div>
            {candleColorBt}
          </div>
          <div>
            {chooseCandleBt}
          </div>
        </div>
        <div className="option-select">
          {
            _.map(dataVariant, d => (
              <button
                onClick={() => props.updateColorCandle(d.value, _.filter(d?.images, x => x.type === null))}
                type="button"
                className={classnames('button-bg__none', props.variantValues === d.value ? 'active' : '')}
              >
                <img src={d.image || _.find(d.images, x => x.type === 'main')?.image} alt="icon" />
                <div>
                  {d?.name}
                </div>
              </button>
            ))
          }
        </div>
      </div>
      <div className="line" />
    </React.Fragment>
  );
}

export default React.memo(CandleColor);
