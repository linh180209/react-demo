import React, { Component } from 'react';
import _ from 'lodash';
import icStar from '../../image/icon/star.svg';
import icStarSelected from '../../image/icon/star-selected.svg';
import { getNameFromButtonBlock } from '../../Redux/Helpers';

class ReviewLineStar extends Component {
  render() {
    const {
      isMobileColumn, isScore, scoreSatisfaction, scoreScent, scoreProduct,
      buttonBlocks,
    } = this.props;
    const scentBlendBt = getNameFromButtonBlock(buttonBlocks, 'Scent_Blend');
    const productbt = getNameFromButtonBlock(buttonBlocks, 'Product');
    const satisfactionBt = getNameFromButtonBlock(buttonBlocks, 'Satisfaction');
    return (
      <div className={`div-review-line-start ${isMobileColumn ? 'column-mobile' : ''}`}>
        <div className="item-line-start">
          <span>
            {scentBlendBt}
          </span>
          <div className="line-star">
            {
              _.map(_.range(5), (x, index) => (
                <img src={index + 1 <= scoreScent ? icStarSelected : icStar} alt="star" />
              ))
            }
            <span className={isScore ? '' : 'hidden'}>
              {scoreScent}
            </span>
          </div>
        </div>
        <div className="item-line-start">
          <span>
            {productbt}
          </span>
          <div className="line-star">
            {
              _.map(_.range(5), (x, index) => (
                <img src={index + 1 <= scoreProduct ? icStarSelected : icStar} alt="star" />
              ))
            }
            <span className={isScore ? '' : 'hidden'}>
              {scoreProduct}
            </span>
          </div>
        </div>
        <div className="item-line-start">
          <span>
            {satisfactionBt}
          </span>
          <div className="line-star">
            {
              _.map(_.range(5), (x, index) => (
                <img src={index + 1 <= scoreSatisfaction ? icStarSelected : icStar} alt="star" />
              ))
            }
            <span className={isScore ? '' : 'hidden'}>
              {scoreSatisfaction}
            </span>
          </div>
        </div>
      </div>
    );
  }
}

export default ReviewLineStar;
