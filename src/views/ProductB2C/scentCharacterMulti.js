import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import parseHtml from 'react-html-parser';
import classnames from 'classnames';
import icSearch from '../../image/icon/ic-search.svg';
import ProgressLine from '../ResultScentV2/progressLine';
import ProgressCircle from '../ResultScentV2/progressCircle';
import { getNameFromCommon } from '../../Redux/Helpers';
import { isBrowser } from '../../DetectScreen';

class ScentCharacterMulti extends Component {
  constructor(props) {
    super(props);
    console.log('props.combos', props.combos);
    const eleCombo = _.find(props.combos, x => x.product.combo?.length);
    const newCombo = _.filter(eleCombo?.product?.combo || [], x => !['Bottle', 'holder'].includes(x.product.type));
    console.log('newCombo', newCombo);
    this.state = {
      indexSelected: 0,
      comboSelect: newCombo,
      selectItemId: ((eleCombo?.product?.type?.name === 'Perfume' || eleCombo?.product?.type?.name === 'dual_candles') && newCombo.length > 1) ? 0 : (newCombo && newCombo.length > 0 ? newCombo[0].id : undefined),
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { combos } = nextProps;
    if (combos && JSON.stringify(combos) !== JSON.stringify(prevState.combos)) {
      const eleCombo = _.find(combos, x => x.product.combo?.length);
      console.log('eleCombo', eleCombo);
      const newCombo = _.filter(eleCombo?.product?.combo || [], x => !['Bottle', 'holder'].includes(x.product.type));
      return ({
        indexSelected: 0,
        comboSelect: newCombo,
        combos,
        selectItemId: (eleCombo?.product?.type?.name === 'Perfume' || eleCombo?.product?.type?.name === 'dual_candles') && newCombo.length > 1 ? 0 : newCombo[0]?.id,
      });
    }
    return null;
  }

  onChangeIndex = (index) => {
    const newCombo = _.filter(this.props.combos[index].product.combo, x => !['Bottle', 'holder'].includes(x.product.type));
    this.setState({
      indexSelected: index,
      comboSelect: newCombo,
      selectItemId: ((this.props.combos[index].product.type.name === 'Perfume' || this.props.combos[index].product.type.name === 'dual_candles') && newCombo.length > 1) ? 0 : (newCombo && newCombo.length > 0 ? newCombo[0].id : undefined),
    });
  }

  render() {
    const {
      cmsCommon, profile, type,
    } = this.props;
    const { selectItemId, comboSelect, indexSelected } = this.state;
    console.log('comboSelect', comboSelect);
    _.forEach(comboSelect, (d) => {
      if (d.product.type === 'single_candle') {
        _.assign(d.product.images, d.product.combo[0].product.images);
        d.description = d.product.combo[0].description;
        d.product.ingredient = d.product.combo[0].product.ingredient;
      }
    });

    const viewMore = getNameFromCommon(cmsCommon, 'view_more_info');
    const strengthBt = getNameFromCommon(cmsCommon, 'STRENGTH');
    const durationBt = getNameFromCommon(cmsCommon, 'DURATION');
    const scentCharacter = getNameFromCommon(cmsCommon, 'Scent_characteristics');
    const comboScent = getNameFromCommon(cmsCommon, 'COMBO_SCENT');
    const hereAre = getNameFromCommon(cmsCommon, 'Here_are_the_mix_characteristics_for');
    const cloneCombo = _.cloneDeep(comboSelect);
    const isBundle = type && type.name === 'bundle';
    const isDualCandles = type && type.name === 'dual_candles';
    console.log(' 111', type, comboSelect);
    if (type && (type.name === 'Perfume' || type.name === 'dual_candles' || type.name === 'gift_bundle') && comboSelect.length === 2) {
      const item = {
        id: 0,
        image1: _.find(comboSelect[0].product.images || [], x => x.type === 'unisex') ? _.find(comboSelect[0].product.images || [], x => x.type === 'unisex').image : '',
        image2: _.find(comboSelect[1].product.images || [], x => x.type === 'unisex') ? _.find(comboSelect[1].product.images || [], x => x.type === 'unisex').image : '',
        name: `${comboSelect[0].name} & ${comboSelect[1].name}`,
        title: comboScent,
        profile: this.props.combos[indexSelected].product.profile,
      };
      cloneCombo.unshift(item);
    }
    const selectItem = _.find(cloneCombo, x => x.id === selectItemId);
    const profileSelect = selectItem && selectItem.product && selectItem?.product?.profile ? selectItem?.product?.profile : selectItem?.profile;
    const colorProcess = (
      <div className="div-process-line">
        {
          _.map(profileSelect?.accords || [], x => (
            <ProgressLine data={x} isShowPercent />
          ))
        }
      </div>
    );
    const circleProcess = (
      <div className="div-process-circle">
        <ProgressCircle title={strengthBt} percent={profileSelect ? parseInt(parseFloat(profileSelect.strength) * 100, 10) : 0} />
        <ProgressCircle title={durationBt} percent={profileSelect ? parseInt(parseFloat(profileSelect.duration) * 100, 10) : 0} />
      </div>
    );

    const listScent = (
      <div className={!cloneCombo || cloneCombo.length < 2 ? 'hidden' : 'div-button-scent'}>
        {
          _.map(cloneCombo, d => (
            <React.Fragment>
              {
                d.id === 0 ? (
                  <button
                    type="button"
                    onClick={() => this.setState({ selectItemId: d.id })}
                    className={selectItemId !== d.id ? 'none-select' : ''}

                  >
                    <div className="div-bt-image-combo">
                      <img
                        loading="lazy"
                        src={d.image1}
                        alt="scent"
                      />
                      <img
                        loading="lazy"
                        src={d.image2}
                        alt="scent"
                      />
                      <span>{d.title}</span>
                    </div>

                  </button>
                ) : (
                  <button
                    type="button"
                    onClick={() => this.setState({ selectItemId: d.id })}
                    className={selectItemId !== d.id ? 'none-select' : ''}

                  >
                    <div className="div-bt-image-combo">
                      <img
                        loading="lazy"
                        className="full-image"
                        src={_.find(d.product.images || [], x => x.type === 'unisex') ? _.find(d.product.images || [], x => x.type === 'unisex').image : ''}
                        alt="scent"
                      />
                    </div>

                  </button>
                )
              }
            </React.Fragment>

          ))
        }
      </div>
    );
    const scentInfo = (
      <div className="div-info-detail">
        {
          selectItem?.id === 0
            ? (
              <div className="div-image-combo">
                <img
                  loading="lazy"
                  src={selectItem?.image1}
                  alt="scent"
                />
                <img
                  loading="lazy"
                  src={selectItem?.image2}
                  alt="scent"
                />
                {/* <span>{selectItem.title}</span> */}
              </div>
            ) : (
              <img
                src={selectItem ? _.find(selectItem.product.images || [], x => x.type === 'unisex') ? _.find(selectItem.product.images || [], x => x.type === 'unisex').image : '' : ''}
                alt="scent"
              />
            )
        }
        <div className={selectItem?.id === 0 ? 'text-info-combo' : 'hidden'}>
          <span>
            {hereAre}
          </span>
          <span>{selectItem?.name}</span>
        </div>
        <div className={selectItem?.id === 0 ? 'hidden' : 'text-info'}>
          <span>
            {selectItem ? parseHtml(isBundle ? selectItem.product.combo[0].description : selectItem.description) : ''}
          </span>
          <div className="div-bt-view">
            <button
              type="button"
              onClick={() => this.props.onClickIngredient(isBundle ? selectItem.product.combo[0].product.ingredient : selectItem.product.ingredient)}
            >
              <img
                src={icSearch}
                alt="search"
              />
              <span>
                {viewMore}
              </span>
            </button>
          </div>
        </div>

      </div>
    );

    return (
      <div className="div-scent-character div-multiple">
        <h3>
          {scentCharacter}
        </h3>
        <div className="list-button">
          <button
            onClick={() => { this.onChangeIndex(0); }}
            type="button"
            className={classnames('button-bg__none', indexSelected === 0 ? 'active' : '')}
          >
            {this.props.combos && this.props.combos.length > 0 ? this.props.combos[0].product.short_description : ''}
          </button>
          <button
            onClick={() => { this.onChangeIndex(1); }}
            type="button"
            className={classnames('button-bg__none', indexSelected === 1 ? 'active' : '')}
          >
            {this.props.combos && this.props.combos.length > 1 ? this.props.combos[1].product.short_description : ''}
          </button>
        </div>

        {
          isBrowser ? (
            <React.Fragment>
              {listScent}
              <div className="div-info">
                {colorProcess}
                {circleProcess}
                {scentInfo}
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              {listScent}
              {
              selectItem?.id === 0
                ? (
                  <div className="div-image-combo-mobile">
                    <img
                      src={selectItem?.image1}
                      alt="scent"
                    />
                    <img
                      src={selectItem?.image2}
                      alt="scent"
                    />
                    {/* <span>{selectItem.title}</span> */}
                  </div>
                ) : (
                  <img
                    className="img-scent-mobile"
                    src={selectItem ? _.find(selectItem.product.images || [], x => x.type === 'unisex') ? _.find(selectItem.product.images || [], x => x.type === 'unisex').image : '' : ''}
                    alt="scent"
                  />
                )
            }
              {
              selectItemId === 0
                ? (
                  <div className="span-info-mobile">
                    <span>{hereAre}</span>
                    <span>{selectItem?.name}</span>

                  </div>
                ) : (
                  <span className="span-info-mobile">
                    {selectItem ? parseHtml(isBundle ? selectItem.product.combo[0].description : selectItem.description) : ''}
                  </span>
                )
            }
              <div className="div-info">
                {colorProcess}
                {circleProcess}
              </div>
              <div className={selectItem?.id === 0 ? 'hidden' : 'div-bt-view'}>
                <button
                  type="button"
                  onClick={() => this.props.onClickIngredient(isBundle ? selectItem.product.combo[0].product.ingredient : selectItem.product.ingredient)}
                >
                  <img
                    src={icSearch}
                    alt="search"
                  />
                  <span>
                    {viewMore}
                  </span>
                </button>
              </div>
            </React.Fragment>
          )
        }

      </div>
    );
  }
}

ScentCharacterMulti.propTypes = {
  cmsCommon: PropTypes.shape().isRequired,
};

export default ScentCharacterMulti;
