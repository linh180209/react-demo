import React, { useState, useEffect } from 'react';
import { icComment } from '../../../../imagev2/svg';
import './styles.scss';

function ItemComment() {
  return (
    <div className="item-comment">
      <div className="content-text">
        <img src={icComment} alt="icon" />
        <div className="des">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sed fringilla odio. Aliquam sit amet dui eros. Phasellus molestie interdum urna, ut cursus justo auctor id. Ut et cursus massa, id accumsan massa. Vestibulum rhoncus, tortor et sagittis scelerisque, nulla diam ullamcorper quam.
        </div>
      </div>
      <div className="name">
        Zoe Deschanel
      </div>
    </div>
  );
}

export default ItemComment;
