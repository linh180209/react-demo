import React, { useState, useEffect } from 'react';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import ImportExportIcon from '@mui/icons-material/ImportExport';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import _ from 'lodash';
import classnames from 'classnames';
import Slider from 'react-slick';
import ItemComment from './itemComment';
import ButtonCT from '../../../componentsv2/buttonCT';
import './styles.scss';

const widthStep = 432;

function ReviewsV2(props) {
  const [sortValueIndex, setSortValueIndex] = useState(false);
  const [translateX, setTranslateX] = useState(0);

  const toggleSort = () => {
    setSortValueIndex(sortValueIndex === 0 ? 1 : 0);
  };

  const onClickNext = () => {
    const newValue = translateX - widthStep;
    setTranslateX(newValue);
  };

  const onClickPrev = () => {
    const newValue = translateX + widthStep;
    setTranslateX(newValue);
  };

  return (
    <div className="reviews-v2">
      <div className="div-header-review">
        <div className="div-text-left">
          <div className="title-header">
            Customer Reviews
          </div>
          {/* <div className="text-review">
            19 Reviews
          </div> */}
        </div>
        {/* <ButtonCT
          color="secondary"
          onClick={toggleSort}
          name={sortValueIndex === 0 ? 'Newewst' : 'Oldest'}
          startIcon={<ImportExportIcon />}
          endIcon={<ArrowDropDownIcon />}
        /> */}
      </div>
      <div className="slider-comments" style={{ transform: `translateX(${translateX}px)` }}>
        {
          _.map(_.range(5), () => (
            <ItemComment />
          ))
        }
      </div>
      <div className="button-next">
        <button
          onClick={onClickPrev}
          className={classnames('button-bg__none bt-prev', translateX === 0 ? 'disabled' : '')}
          type="button"
          disabled={translateX === 0}
        >
          <ArrowForwardIcon />
        </button>
        <button
          onClick={onClickNext}
          className={classnames('button-bg__none bt-next', translateX === (-widthStep * (5 - 1)) ? 'disabled' : '')}
          type="button"
          disabled={translateX === (-widthStep * (5 - 1))}
        >
          <ArrowForwardIcon />
        </button>
      </div>
    </div>
  );
}

export default ReviewsV2;
