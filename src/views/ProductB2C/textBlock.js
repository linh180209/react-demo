/* eslint-disable react/sort-comp */
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import classnames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import {
  Dropdown, DropdownMenu, DropdownToggle, Input,
} from 'reactstrap';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import '../../../node_modules/slick-carousel/slick/slick-theme.css';
import '../../../node_modules/slick-carousel/slick/slick.css';
import ButtonCT from '../../components/ButtonCT';
import ReceiveGrams from '../../components/receiveGrams';
import AccordionCT from '../../componentsv2/accordionCT';
import ButtonCTV2 from '../../componentsv2/buttonCT';
import HeaderCT from '../../componentsv2/headerCT';
import {
  isBrowser, isMobile, isTablet, isMobile767fn,
} from '../../DetectScreen';
import icBottle from '../../image/bottle-new.png';
import icDualCrayon from '../../image/bottle_dual-crayon.png';
import icRollon from '../../image/bottle_roll-on.png';
import icAdd from '../../image/icon/ic-add-product.svg';
import icCandle1 from '../../image/icon/ic-candle-1.svg';
import icCandle2 from '../../image/icon/ic-candle-2.svg';
import icCandle3 from '../../image/icon/ic-candle-3.svg';
import icCandle4 from '../../image/icon/ic-candle-4.svg';
// import icDown from '../../image/icon/ic-select.svg';
import icDown from '../../image/icon/ic-next-silder.svg';
import icSub from '../../image/icon/ic-sub-product.svg';
import icWax from '../../image/icon/icWax.svg';
import icProduct1 from '../../image/icon/product1.svg';
import icProduct2 from '../../image/icon/product2.svg';
import icStarSelected from '../../image/icon/star-selected.svg';
import icStar from '../../image/icon/star.svg';
import { discoveryDetail, btAdd } from '../../imagev2/png';
import {
  bottleCustome, perfume, productSelect, startGrayV2, startv2, oilBurner, icCandle, icCandleOne, icCandleTwo, startOutline,
} from '../../imagev2/svg';
import {
  generaCurrency, getNameFromButtonBlock, getNameFromCommon, isRightAlignedText, scrollToTargetAdjusted, sendAddToCartClickHotjarEvent,
} from '../../Redux/Helpers';
import ItemScentSelectV2 from '../../views2/productDetailV2/itemScentSelectV2';
import ItemScentSelect from './itemScentSelect';
import ItemSelectTypePerfume from './itemSelectTypePerfume';
import HowToMixV2 from '../../views2/productDetailV2/howtomix';
import CandleColor from './candleColor';

// import icDown from '../../image/icon/ic-down.svg';

const getDataCustome = (propsState) => {
  let name;
  let imageCustome;
  let isCustome;
  if (propsState) {
    const { custome } = propsState;
    if (custome) {
      name = custome.name;
      imageCustome = custome.image;
      isCustome = true;
    }
  }
  return { name, imageCustome, isCustome };
};

class TextBlock extends Component {
  constructor(props) {
    super(props);
    const { name, isCustome } = getDataCustome(
      props.propsState,
    );
    console.log('props.type.name', props.type.name);
    this.state = {
      isViewFull: false,
      itemChoise: 0,
      nameText: name || '',
      isShowCustome: isCustome,
      dropdownOpen: false,
      dropdownHandSanitizerOpen: false,
      items: props.items,
      itemWaxSelect: props.type.name === 'hand_sanitizer' ? props.itemHandlerSanitizer : props.type.name === 'home_scents' ? props.items[0] : (_.find(props.items, x => x.is_sample === false) || props.items[0]),
      combosState: _.cloneDeep(props.combosChooseScent),
      oldCombo: props.combosChooseScent,
      pointUser: 1,
      spendUser: 1,
      isExpandedCT: false,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const {
      propsState, combosChooseScent, items, type, itemHandlerSanitizer,
    } = nextProps;
    if (propsState !== prevState.propsState) {
      const { name, isCustome } = getDataCustome(
        propsState,
      );
      const nameText = name || '';
      _.assign(objectReturn, {
        propsState,
        nameText,
        isShowCustome: isCustome,
      });
    }
    if (JSON.stringify(combosChooseScent) !== JSON.stringify(prevState.oldCombo)) {
      _.assign(objectReturn, {
        combosState: _.cloneDeep(combosChooseScent),
        oldCombo: combosChooseScent,
      });
    }
    if (items !== prevState.items) {
      _.assign(objectReturn, {
        items,
        itemWaxSelect: type.name === 'hand_sanitizer' ? itemHandlerSanitizer : type.name === 'home_scents' ? items[0] : (_.find(items, x => x.is_sample === false) || items[0]),
        isViewFull: isBrowser && !['oil_burner', 'mini_candles', 'dual_candles', 'Perfume', 'dual_crayons', 'bundle_creation', 'reed_diffuser', 'perfume_diy', 'car_diffuser', 'mini_candles'].includes(type.name),
      });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : {};
  }

  toggle = () => {
    const { dropdownOpen } = this.state;
    this.setState({
      dropdownOpen: !dropdownOpen,
    });
  }

  toggleHandSanitizer = () => {
    const { dropdownHandSanitizerOpen } = this.state;
    this.setState({
      dropdownHandSanitizerOpen: !dropdownHandSanitizerOpen,
    });
  }

  onChangeWaxPerfume = (data) => {
    this.props.updateWaxPerfume(data);
    this.setState({ itemWaxSelect: data, dropdownOpen: false });
  }

  onChangeHandSanitizer = (data) => {
    if (this.state.itemWaxSelect.id !== data.id) {
      this.props.updateWaxPerfume(data);
      this.props.onClickWaxPerfume(data);
    }
    this.setState({ dropdownHandSanitizerOpen: false });
  }

  onChange = (e) => {
    const { value } = e.target;
    if (value.length > 15) {
      return;
    }
    this.setState({ nameText: value });
    this.props.handleChangeNameBottle(value);
  }

  toggleViewFull = () => {
    const { isViewFull } = this.state;
    this.setState({ isViewFull: !isViewFull });
  }

  scrollToReviews = () => {
    const ele = document.getElementById('anchor-reviews');
    if (ele) {
      ele.scrollIntoView({ behavior: 'smooth' });
    }
  }

  getImageFromProduct = (data) => {
    const ele = _.find(data.images, d => d.type === 'unisex');
    if (ele) {
      return ele.image;
    }
    return '';
  }

  onClickRemoveScent = (data, index) => {
    const { combosState } = this.state;
    const { itemCombo } = this.props;
    delete itemCombo.id;
    // _.remove(combosState, x => x.id === data.id);
    const indexEle = _.findIndex(combosState, x => x.id === data.id);
    if (indexEle > -1) {
      combosState.splice(indexEle, 1);
    }
    this.setState({ combosState });
    this.props.updateScentSelect(combosState);
    this.props.onClickApply(undefined, index);
  }

  getPriceBottle = (isRollOn) => {
    const ele = _.find(this.props.bottlePrice || [], x => x.apply_to_sample === isRollOn);
    if (ele) {
      return generaCurrency(ele.price, false);
    }
    return '';
  }

  getHeightTextDes = () => {
    const ele = document.getElementById('id-text-des');
    if (ele) {
      // console.log('ele.offsetHeight', ele.offsetHeight);
      return ele.offsetHeight;
    }
    return 0;
  }

  getHeightTextDes1 = () => {
    const ele = document.getElementById('id-text-des-1');
    if (ele) {
      // console.log('ele.offsetHeight', ele.offsetHeight);
      return ele.offsetHeight;
    }
    return 0;
  }

  summaryWillReceive = (buttonBlocks) => {
    const whatBt = getNameFromButtonBlock(buttonBlocks, 'What you will receive');
    return (
      <div className="summary other-Tagline div-row">
        <b>{whatBt}</b>
        <ArrowDropDownIcon style={{ color: '#2c2c2c' }} />
      </div>
    );
  }

  detailWillReceive = (buttonBlocks) => {
    const completeBt = getNameFromButtonBlock(buttonBlocks, 'Complete set of all Maison’s 34 finest scents');
    return (
      <div className="detail">
        <HeaderCT type={isMobile ? 'Heading-XXS' : 'Heading-S'}>
          {completeBt}
        </HeaderCT>
        <div className="div-image">
          <img src={discoveryDetail} alt="detail" />
        </div>
      </div>
    );
  }

  gotoSelectedScent = () => {
    scrollToTargetAdjusted('anchor-select-scent');
  };

  render() {
    const {
      isViewFull, itemChoise, nameText, isShowCustome, dropdownOpen,
      itemWaxSelect, combosState, dropdownHandSanitizerOpen, pointUser, spendUser,
    } = this.state;

    const {
      nameProduct, itemCombo, scrollToSample, priceSample,
      cmsCommon, itemBottle, quantity, onChangeQuantity, onClickAddtoCart,
      combos, type, altName, nameProductCustom, descriptionCustom,
      isWaxPerfume, items, currentImg, nameBottle, idCartItem, arrayThumbs,
      dataReview, buttonBlocks, isProductDetailV2, isBestSeller, tags, profile,
    } = this.props;
    const indexItems = itemChoise < combos.length ? itemChoise : 0;
    const tryBt = getNameFromCommon(cmsCommon, 'Try_a_sample_for');
    const viewMoreBt = getNameFromCommon(cmsCommon, 'view_more');
    const viewLessBt = getNameFromCommon(cmsCommon, 'view_less');
    const addToCartBt = getNameFromCommon(cmsCommon, 'ADD_TO_CART').toUpperCase();
    const soldOutBt = getNameFromCommon(cmsCommon, 'Sold Out').toUpperCase();
    const customerBottlesBt = getNameFromCommon(cmsCommon, 'CUSTOMIZE_BOTTLE');
    const reviewBt = getNameFromButtonBlock(buttonBlocks, 'Reviews');
    const ingredient1St = getNameFromButtonBlock(buttonBlocks, '1ST INGREDIENT');
    const ingredient2ndBt = getNameFromButtonBlock(buttonBlocks, '2ND INGREDIENT');
    const ingredient3rdBt = getNameFromButtonBlock(buttonBlocks, '3RD INGREDIENT');
    const ingredient4thBt = getNameFromButtonBlock(buttonBlocks, '4TH INGREDIENT');
    const requiredBt = getNameFromButtonBlock(buttonBlocks, 'Required');
    const optionalBt = getNameFromButtonBlock(buttonBlocks, 'Optional');
    const ml30 = getNameFromButtonBlock(buttonBlocks, '30mL');
    const months36Bt = getNameFromButtonBlock(buttonBlocks, '3-6 months of wear');
    const ml5 = getNameFromButtonBlock(buttonBlocks, '5mL');
    const tryMiniBt = getNameFromButtonBlock(buttonBlocks, 'Try Mini Roll-On Perfumes');
    const monthsWearbt = getNameFromButtonBlock(buttonBlocks, 'months of wear 15mL | 1-2');
    const sageBt = getNameFromButtonBlock(buttonBlocks, 'Sage & Lavender | Orris & Vanilla');
    const chooseIngredientBt = getNameFromButtonBlock(buttonBlocks, 'CHOOSE INGREDIENT');
    const ml15 = getNameFromButtonBlock(buttonBlocks, 'ml15');
    const optionCandleBt = getNameFromButtonBlock(buttonBlocks, 'Optional Candle Holder') || '';
    const week2Bt = getNameFromButtonBlock(buttonBlocks, '2 weeks - 1 month of wear');
    const ml5x2Bt = getNameFromButtonBlock(buttonBlocks, 'ml5x2');
    const outStockBt = getNameFromButtonBlock(buttonBlocks, 'Out Of Stock');
    const buy1Bt = getNameFromButtonBlock(buttonBlocks, 'Buy 1 Gift 1');
    const select3rd = getNameFromButtonBlock(buttonBlocks, 'Select the 3rd scent');
    const select4th = getNameFromButtonBlock(buttonBlocks, 'Select the 4th scent');
    const howtoMixBt = getNameFromButtonBlock(buttonBlocks, 'How to mix');
    const createYourMix = getNameFromButtonBlock(buttonBlocks, 'Create your own');
    const QuantityBt = getNameFromButtonBlock(buttonBlocks, 'Quantity');
    const shopCreation = getNameFromButtonBlock(buttonBlocks, 'SHOP CREATION');
    const personalizeBottleBt = getNameFromButtonBlock(buttonBlocks, 'Personalize Bottle');
    const OptionalBt = getNameFromButtonBlock(buttonBlocks, 'Optional');
    const lucioleBt = getNameFromButtonBlock(buttonBlocks, 'Luciole Oil Burner');
    const bestSellersHomeBt = getNameFromButtonBlock(buttonBlocks, 'Best Sellers Home');
    const DetailsBt = getNameFromButtonBlock(buttonBlocks, 'Details');
    const IntensityBt = getNameFromButtonBlock(buttonBlocks, 'Intensity');
    const OlfactiveFamilyBt = getNameFromButtonBlock(buttonBlocks, 'Olfactive Family');
    const MoodBt = getNameFromButtonBlock(buttonBlocks, 'Mood');
    const collectionBt = getNameFromButtonBlock(buttonBlocks, 'COLLECTION');
    const isShowBtCustome = (type.name === 'Creation' && arrayThumbs && arrayThumbs.length > 0) || ((type.name === 'Perfume' || type.name === 'bundle_creation' || type.name === 'perfume_diy') && !itemCombo.isSample) || type.name === 'Elixir' || isShowCustome;
    const isShowDesGenerateCombo = (type.name === 'Perfume' || type.name === 'bundle_creation' || type.name === 'perfume_diy' || type.name === 'car_diffuser' || type.name === 'reed_diffuser') && combos && combos.length > 0;
    const isDualCandles = type.name === 'dual_candles';
    const isSingleCandle = type.name === 'single_candle';
    const isCandles = isDualCandles || isSingleCandle;
    const isMiniCandle = type.name === 'mini_candles';
    const isWaxPerfumeCreate = type.name === 'Wax_Perfume';
    const isDualCrayons = type.name === 'dual_crayons';
    const isOilBurner = type.name === 'oil_burner';

    const {
      scoreScent, scoreProduct, scoreSatisfaction, dataTable,
    } = dataReview;
    const newScore = scoreScent !== undefined && scoreProduct !== undefined && scoreSatisfaction !== undefined ? ((parseFloat(scoreScent) + parseFloat(scoreProduct) + parseFloat(scoreSatisfaction)) / 3) : 0;
    this.props.updateScentSelect(combosState);

    const heightDesLess = !isMobile767fn() && isBestSeller ? 30 : 70;

    const descriptionHtml = (
      <React.Fragment>
        <div className={classnames(isRightAlignedText() ? 'tr' : 'tj', itemBottle?.product?.description || itemWaxSelect?.description || itemBottle?.product?.short_description ? 'div-description div-col justify-start' : 'hidden')}>
          {!isProductDetailV2 && !isBestSeller && <div className="line mt-3 mb-3" />}
          {
            nameProductCustom && !isBestSeller && (
              <div className="name-description mb-3">
                {nameProduct}
              </div>
            )
          }
          {
            descriptionCustom ? (
              _.map(ReactHtmlParser(descriptionCustom).slice(0, isViewFull ? ReactHtmlParser(descriptionCustom).length : 2), d => d)
            ) : ['mini_candles', 'dual_candles', 'dual_crayons', 'home_scents', 'Wax_Perfume', 'single_candle', 'mini_candles', 'oil_burner'].includes(type.name) ? (
              <div id="id-text-des" className={classnames('text-des', isViewFull || this.getHeightTextDes() < heightDesLess ? '' : 'show-hidden')}>
                {ReactHtmlParser(itemBottle?.product?.short_description)}
                {['dual_crayons', 'home_scents', 'Wax_Perfume', 'dual_candles', 'single_candle'].includes(type.name) && <div><br /></div>}
                {ReactHtmlParser(combos[0] ? combos[0].product?.short_description : '')}
                {['home_scents', 'oil_burner', 'mini_candles', 'dual_candles'].includes(type.name) && <br />}
                {['oil_burner', 'mini_candles', 'dual_candles'].includes(type.name) && <br />}
                {ReactHtmlParser(combos[1] ? combos[1].product?.short_description : '')}
                {isMiniCandle && (<br />)}
                {isMiniCandle && (<br />)}
                {ReactHtmlParser(combos[2] ? combos[2].product?.short_description : '')}
                {isMiniCandle && (<br />)}
                {isMiniCandle && (<br />)}
                {ReactHtmlParser(combos[3] ? combos[3].product?.short_description : '')}
              </div>
            ) : (
              <span id="id-text-des-1" className={classnames(isViewFull || this.getHeightTextDes1() < heightDesLess ? '' : 'show-hidden')}>
                {isShowDesGenerateCombo
                  ? (
                    <React.Fragment>
                      {ReactHtmlParser(itemBottle?.product?.description)}
                      <br />
                      {ReactHtmlParser(combos[0]?.product?.short_description)}
                      <br />
                      {ReactHtmlParser(combos[1] ? combos[1]?.product?.short_description : '')}
                    </React.Fragment>
                  )
                  : isWaxPerfume ? ReactHtmlParser(itemWaxSelect?.description) : itemBottle
                    ? ReactHtmlParser(itemBottle?.product?.description) : ''}
              </span>
            )
          }
          {
            (
              <div className={classnames('div-row', (isBrowser || isViewFull ? '' : 'bt-hidden-mobile'))}>
                <button
                  type="button"
                  className={this.getHeightTextDes() < heightDesLess && this.getHeightTextDes1() < heightDesLess ? 'hidden' : 'mt-3'}
                  onClick={this.toggleViewFull}
                >
                  {isViewFull ? viewLessBt : viewMoreBt}
                </button>
              </div>
            )
          }

        </div>
      </React.Fragment>
    );

    const renderQuanityV2 = (
      <div className={classnames('div-quantity')}>
        <div className="div-number">
          <button
            onClick={() => onChangeQuantity({ target: { value: quantity - 1 } })}
            className={classnames('button-bg__none', quantity < 2 ? 'disabled' : '')}
            type="button"
            disabled={quantity < 2}
          >
            <img src={icSub} alt="sub" />
          </button>

          <div className="quanity">
            {quantity}
          </div>
          <button
            onClick={() => onChangeQuantity({ target: { value: quantity + 1 } })}
            className="button-bg__none"
            type="button"
          >
            <img src={icAdd} alt="sub" />
          </button>
        </div>
      </div>
    );

    if (isProductDetailV2) {
      const priceMobile = (
        <div className="price-mobile">
          <div className="text-price">
            {generaCurrency(itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : '')}
          </div>
          <ReceiveGrams isPointProductDetail buttonBlocks={buttonBlocks} price={itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : 0} />
        </div>
      );

      const addCardBlockHtml = (
        <React.Fragment>
          {
            isMobile && priceMobile
          }
          <div className="add-card-block">
            {renderQuanityV2}
            <ButtonCTV2
              name={addToCartBt}
              size={isMobile ? 'medium' : undefined}
              onClick={() => {
                if (type.name === 'hand_sanitizer') {
                  this.props.addToCartItem(itemWaxSelect);
                } else {
                  this.props.onClickAddtoCart();
                }
              }
          }
            />
          </div>
        </React.Fragment>
      );

      const summary = (
        <div className="summary-how-mix">
          <div className={classnames('other-Tagline', isMobile ? 's-size' : '')}>
            {howtoMixBt}
          </div>
        </div>
      );

      const details = (
        <div className="detail-how-mix">
          <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
            {this.props.iconsBlock?.value?.opacity}
          </div>
          {
            _.map(this.props.iconsBlock?.value?.icons || [], d => (
              <div className="item-how-mix div-row" onClick={this.gotoSelectedScent}>
                <img src={d.value?.image?.image} alt="icon" />
                <div className="div-col">
                  <span>
                    {d.value?.header?.header_text}
                  </span>
                  <span>
                    {d.value?.text}
                  </span>
                </div>
              </div>
            ))
          }
        </div>
      );

      const dropdownItemV2 = () => {
        console.log('dropdownItemV2');
        return (
          <div className="dropdown-item-v2">
            <Dropdown
              isOpen={dropdownOpen}
              toggle={this.toggle}
              className="dropdown-item-v2"
              style={{
                display: 'flex', flex: '1',
              }}
            >
              <DropdownToggle
                tag="span"
                onClick={this.toggle}
                data-toggle="dropdown"
                className="w-100"
              >
                <div
                  className="div-row items-center justify-between header-dropdown"
                >
                  <div>
                    <span className="mr-3">
                      {`${itemWaxSelect.name} ${itemWaxSelect && itemWaxSelect.variant_values && itemWaxSelect.variant_values.length > 0 ? `(${itemWaxSelect.variant_values[0]})` : ''} - `}
                      <b>
                        {generaCurrency(itemWaxSelect.price)}
                      </b>
                    </span>
                  </div>
                  <ExpandMoreIcon style={{ color: '#2c2c2c' }} />
                </div>
              </DropdownToggle>
              <DropdownMenu positionFixed>
                {
                    _.map(_.orderBy(items), d => (
                      <div
                        onClick={() => {
                          this.onChangeWaxPerfume(d);
                        }}
                      >
                        <div className="drop-down-item">
                          {`${d.name} ${d && d.variant_values && d.variant_values.length > 0 ? `(${d.variant_values[0]})` : ''} - `}
                          <b>
                            {generaCurrency(d.price)}
                          </b>
                        </div>
                      </div>
                    ))
                  }
              </DropdownMenu>
            </Dropdown>
          </div>
        );
      };

      return (
        <div className="div-text-block-v2">
          <div className="div-type div-row">
            <div className="div-image">
              <img src={this.props.dataFAQ?.parent?.icon} alt="icon" />
            </div>
            <div className="text-p">
              {this.props.dataFAQ?.parent?.name}
            </div>
          </div>

          <HeaderCT type={isMobile ? 'Heading-XS' : 'Heading-M'}>
            {nameProductCustom || nameProduct}
          </HeaderCT>

          {
            isBrowser && (
              <div className="text-price-total">
                <div className="price-total-t">
                  {generaCurrency(itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : '')}
                </div>
                {
                  ['hand_sanitizer', 'home_scents_premade', 'discovery_box', 'Creation'].includes(type.name) && (
                    <ReceiveGrams isPointProductDetail buttonBlocks={buttonBlocks} price={itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : 0} />
                  )
                }
              </div>
            )
          }

          {
            dataTable && dataTable.length > 0 && (
              <div className="div-review div-row" onClick={this.scrollToReviews}>
                <div className="text-score">
                  {newScore.toFixed(1)}
                </div>
                <div className="line-star">
                  {
                      _.map(_.range(5), (x, index) => (
                        <img src={index + 1 <= parseFloat(newScore.toFixed(1)) ? startv2 : startGrayV2} alt="star" />
                      ))
                    }
                </div>
                <button type="button" className="button-bg__none">
                  {`${dataTable.length} ${reviewBt}`}
                </button>
              </div>
            )
          }
          {
            descriptionHtml
          }
          <div className="line" />
          {
            ['discovery_box'].includes(type.name) && (
              <div className="what-will-receive">
                <AccordionCT
                  className="what-will-receive"
                  summary={this.summaryWillReceive(buttonBlocks)}
                  details={this.detailWillReceive(buttonBlocks)}
                />
              </div>
            )
          }
          {
            ['hand_sanitizer', 'home_scents_premade'].includes(type.name) && (
              dropdownItemV2()
            )
          }
          {
            ['hand_sanitizer', 'home_scents_premade', 'discovery_box', 'Creation'].includes(type.name) ? (
              addCardBlockHtml
            ) : (
              <div className="scent-how-to">
                {
                isBrowser && (
                  <div className="scent-list-choose">
                    {
                      ['Perfume', 'single_candle', 'dual_candles', 'home_scents', 'Wax_Perfume', 'dual_crayons', 'bundle_creation', 'reed_diffuser', 'perfume_diy', 'car_diffuser', 'mini_candles', 'oil_burner'].includes(type.name) && (
                        <React.Fragment>
                          {
                            type?.name === 'oil_burner' && (
                              <ItemScentSelectV2
                                data={undefined}
                                image={oilBurner}
                                title={lucioleBt}
                                description={undefined}
                                // onClickRemove={data => this.onClickRemoveScent(data, 1)}
                                // onClickChooseScent={data => this.props.onClickChooseScent(data, 1)}
                                onClickChooseScent={this.gotoSelectedScent}
                                subHeader="1x"
                              />
                            )
                          }
                          <ItemScentSelectV2
                            data={combosState && combosState.length > 0 ? combosState[0] : undefined}
                            image={combosState && combosState.length > 0 && !_.isEmpty(combosState[0]) ? this.getImageFromProduct(combosState[0].product) : ['single_candle', 'dual_candles'].includes(type?.name) ? icCandleOne : ['mini_candles'].includes(type?.name) ? icCandle : productSelect}
                            title={combosState && combosState.length > 0 && !_.isEmpty(combosState[0]) ? combosState[0].name : isWaxPerfumeCreate ? chooseIngredientBt : ingredient1St}
                            description={combosState && combosState.length > 0 && !_.isEmpty(combosState[0]) ? ingredient1St : isWaxPerfumeCreate ? undefined : requiredBt}
                            onClickRemove={data => this.onClickRemoveScent(data, 1)}
                            // onClickChooseScent={data => this.props.onClickChooseScent(data, 1)}
                            onClickChooseScent={this.gotoSelectedScent}
                            message={combosState && combosState.length > 0 && combosState[0]?.is_out_of_stock ? outStockBt : undefined}
                            imageHeader={type?.name === 'oil_burner' ? btAdd : undefined}
                          />
                          {
                            !['Wax_Perfume'].includes(type.name) && (
                              <React.Fragment>
                                <ItemScentSelectV2
                                  data={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? combosState[1] : undefined}
                                  image={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? this.getImageFromProduct(combosState[1].product) : ['single_candle', 'dual_candles'].includes(type?.name) ? icCandleTwo : ['mini_candles'].includes(type?.name) ? icCandle : productSelect}
                                  title={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? combosState[1].name : ingredient2ndBt}
                                  description={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? ingredient2ndBt : (isDualCandles || isMiniCandle) ? requiredBt : isSingleCandle ? optionCandleBt.replace('{price}', generaCurrency(itemCombo ? itemCombo.price : '')) : optionalBt}
                                  // isShowDelete={combosState && combosState.length > 1 && !_.isEmpty(combosState[1])}
                                  onClickRemove={data => this.onClickRemoveScent(data, 2)}
                                  // onClickChooseScent={data => this.props.onClickChooseScent(data, 2)}
                                  onClickChooseScent={this.gotoSelectedScent}
                                  recommendData={this.props.recommendData?.suggestions}
                                  recommendDataOfScent={this.props.recommendData?.name}
                                  onClickrecommend={this.props.onClickrecommend}
                                  buttonBlocks={this.props.buttonBlocks}
                                  message={combosState && combosState.length > 1 && combosState[1]?.is_out_of_stock ? outStockBt : undefined}
                                  imageHeader={type?.name === 'oil_burner' ? btAdd : undefined}
                                />
                                {!isProductDetailV2 && !this.props.recommendData?.suggestions && <div className="line" />}
                              </React.Fragment>
                            )
                          }
                          {
                            isMiniCandle && (
                              <React.Fragment>
                                <ItemScentSelectV2
                                  data={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? combosState[2] : undefined}
                                  image={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? this.getImageFromProduct(combosState[2].product) : icCandle}
                                  title={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? combosState[2].name : ingredient3rdBt}
                                  description={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? ingredient3rdBt : select3rd}
                                  // isShowDelete={combosState && combosState.length > 2 && !_.isEmpty(combosState[2])}
                                  onClickRemove={data => this.onClickRemoveScent(data, 3)}
                                  // onClickChooseScent={data => this.props.onClickChooseScent(data, 3)}
                                  onClickChooseScent={this.gotoSelectedScent}
                                  buttonBlocks={this.props.buttonBlocks}
                                  message={combosState && combosState.length > 2 && combosState[2]?.is_out_of_stock ? outStockBt : undefined}
                                />
                                <ItemScentSelectV2
                                  data={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? combosState[3] : undefined}
                                  image={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? this.getImageFromProduct(combosState[3].product) : icCandle}
                                  title={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? combosState[3].name : ingredient4thBt}
                                  description={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? ingredient4thBt : select4th}
                                  // isShowDelete={combosState && combosState.length > 3 && !_.isEmpty(combosState[3])}
                                  onClickRemove={data => this.onClickRemoveScent(data, 4)}
                                  // onClickChooseScent={data => this.props.onClickChooseScent(data, 4)}
                                  onClickChooseScent={this.gotoSelectedScent}
                                  buttonBlocks={this.props.buttonBlocks}
                                  message={combosState && combosState.length > 3 && combosState[3]?.is_out_of_stock ? outStockBt : undefined}
                                />
                              </React.Fragment>
                            )
                          }
                          {
                            isShowBtCustome && (
                              <ItemScentSelectV2
                                data={undefined}
                                image={currentImg || bottleCustome}
                                title={nameBottle || personalizeBottleBt}
                                description={currentImg || nameBottle ? personalizeBottleBt : OptionalBt}
                                // onClickChooseScent={this.props.toggleCustom}
                                onClickChooseScent={this.gotoSelectedScent}
                                buttonBlocks={this.props.buttonBlocks}
                              />
                            )
                          }
                        </React.Fragment>
                      )
                    }
                    {
                      type?.name !== 'oil_burner' && (
                        <ButtonCTV2 name={shopCreation} className="button-shop" onClick={this.gotoSelectedScent} />
                      )
                    }
                  </div>
                )
              }
                {
                  type?.name !== 'oil_burner' && (
                    isMobile ? (
                      <div className="how-to-mix">
                        <AccordionCT
                          className="how-mix"
                          summary={summary}
                          details={details}
                        />
                      </div>
                    ) : (
                      <HowToMixV2
                        iconsBlock={this.props.iconsBlock}
                        buttonBlocks={buttonBlocks}
                      />
                    )
                  )
                }
              </div>
            )
          }
          {
            type?.name === 'oil_burner' && (
              <div className="mt-4">
                {addCardBlockHtml}
              </div>
            )
          }
        </div>
      );
    }

    const pointUserHtml = (
      <ReceiveGrams isNotShowShipping buttonBlocks={buttonBlocks} price={itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : 0} />
    );

    const htmlReview = (
      <div className={classnames('header2 div-line-start div-row', isBrowser ? 'mt-2' : 'mt-3', !(dataTable && dataTable.length > 0) && isMobile ? 'hidden' : '')} onClick={this.scrollToReviews}>
        {
          dataTable && dataTable.length > 0 && (
            <div className="div-review div-row">
              <div className="line-star">
                {
                    _.map(_.range(5), (x, index) => (
                      <img src={index + 1 <= newScore ? icStarSelected : icStar} alt="star" />
                    ))
                  }
              </div>
              <button type="button">
                {`${dataTable?.length} ${reviewBt}`}
              </button>
            </div>
          )
        }
        {
          isBrowser && pointUserHtml
        }
      </div>
    );

    const htmlAddCartMobile = isFooter => (
      <React.Fragment>
        {
          !isFooter && (
            <ButtonCT
              dataGtmtracking="customize-bottle-step-1"
              className={`${isShowBtCustome ? 'bt-out-line mt-3' : 'hidden'}`}
              onClick={this.props.toggleCustom}
              name={customerBottlesBt}
            />
          )
        }
        <div className={classnames('div-add-cart', isFooter ? 'footer-add-cart-mobile' : 'main-add-card mt-3')}>
          <Input
            type="select"
            className={itemWaxSelect?.is_out_of_stock ? 'hidden' : 'border-checkout'}
            name="country"
            onChange={onChangeQuantity}
            value={quantity}
            style={isMobile || isTablet ? { height: '50px', borderTopRightRadius: '0px', borderBottomRightRadius: '0px' } : { height: '50px' }}
          >
            {
                _.map(new Array(99), (x, index) => (
                  <option value={index + 1}>
                    {index + 1}
                  </option>
                ))
              }
          </Input>
          <button
            type="button"
            className={classnames('bt-checkout', itemCombo && itemCombo.id ? '' : 'disable', itemWaxSelect?.is_out_of_stock ? 'out-stock' : '')}
            disabled={!itemCombo || !itemCombo.id || itemWaxSelect?.is_out_of_stock}
            onClick={() => this.props.addToCartItem(itemWaxSelect)}
            style={{ height: '50px' }}
          >
            {itemWaxSelect?.is_out_of_stock ? soldOutBt : addToCartBt}
          </button>
        </div>
      </React.Fragment>
    );

    const renderQuanity = (
      <div className={classnames(itemWaxSelect?.is_out_of_stock ? 'hidden' : 'div-quantity')}>
        <span className="header_4">
          {QuantityBt}
        </span>
        <div className="div-number">
          <button
            onClick={() => onChangeQuantity({ target: { value: quantity - 1 } })}
            className={classnames('button-bg__none', quantity < 2 ? 'disabled' : '')}
            type="button"
            disabled={quantity < 2}
          >
            <img src={icSub} alt="sub" />
          </button>

          <div className="quanity">
            {quantity}
          </div>
          <button
            onClick={() => onChangeQuantity({ target: { value: quantity + 1 } })}
            className="button-bg__none"
            type="button"
          >
            <img src={icAdd} alt="sub" />
          </button>
        </div>
      </div>
    );

    const htmlAddCartWeb = (
      <div className={classnames('div-add-cart div-row pt-3 pb-3')}>
        {
            (!isWaxPerfume || type.name === 'hand_sanitizer' || type.name === 'Wax_Perfume') ? (
              <div className="div-row w-100 justify-between items-end">
                <div className="div-row w-100">
                  <ButtonCT
                    dataGtmtracking="customize-bottle-step-1"
                    className={`${isShowBtCustome ? 'bt-out-line' : 'hidden'}`}
                    onClick={this.props.toggleCustom}
                    name={customerBottlesBt}
                  />
                  <ButtonCT
                    className={`bt-bg-cart ${!itemCombo || !itemCombo.id || itemWaxSelect?.is_out_of_stock ? 'disable' : ''}`}
                    name={itemWaxSelect?.is_out_of_stock ? soldOutBt : addToCartBt}
                    disabled={!itemCombo || !itemCombo.id || itemWaxSelect?.is_out_of_stock}
                    onClick={() => {
                      if (type.name === 'hand_sanitizer') {
                        this.props.addToCartItem(itemWaxSelect);
                      } else {
                        this.props.onClickAddtoCart();
                      }
                    }
                    }
                  />
                </div>
                {
                  renderQuanity
                }
              </div>

            ) : (
              <Dropdown
                isOpen={dropdownOpen}
                toggle={this.toggle}
                className="dropdown-region"
                style={{
                  display: 'flex', flex: '1',
                }}
              >
                <DropdownToggle
                  tag="span"
                  onClick={this.toggle}
                  data-toggle="dropdown"
                  className="w-100"
                >
                  <div
                    className="div-row items-center justify-between border-checkout"

                  >
                    <div>
                      <span className="mr-3">
                        {`${itemWaxSelect.name} ${itemWaxSelect && itemWaxSelect.variant_values && itemWaxSelect.variant_values.length > 0 ? `(${itemWaxSelect.variant_values[0]})` : ''} - `}
                        <b>
                          {generaCurrency(itemWaxSelect.price)}
                        </b>
                      </span>
                    </div>
                    <img src={icDown} alt="icDown" />
                  </div>
                </DropdownToggle>
                <DropdownMenu>
                  {
                    _.map(_.orderBy(items), d => (
                      <div
                        onClick={() => {
                          this.onChangeWaxPerfume(d);
                        }}
                      >
                        <div className="drop-down-item">
                          {`${d.name} ${d && d.variant_values && d.variant_values.length > 0 ? `(${d.variant_values[0]})` : ''} - `}
                          <b>
                            {generaCurrency(d.price)}
                          </b>
                        </div>
                      </div>
                    ))
                  }
                </DropdownMenu>
              </Dropdown>
            )
          }
      </div>
    );
    if (isBestSeller) {
      return (
        <div className="div-text-block div-col block-bestSeller">
          <div className="list-tag">
            <div className="d-tag-0">
              <img src={startOutline} alt="icon" />
              {bestSellersHomeBt}
            </div>
            {
              _.map(tags, d => (
                <div className="d-tag-1" style={{ background: d.color }}>
                  <div className="dot" />
                  {d?.name}
                  {' '}
                  {collectionBt}
                </div>
              ))
            }
            {
              this.props.actualType?.alt_name && (
                <div className="d-tag-2">
                  {this.props.actualType?.alt_name}
                </div>
              )
            }
          </div>
          <div className="name-title">
            {nameProductCustom || nameProduct}
          </div>
          <div className="list-scent">
            {
              _.map(combosState, d => (
                <div className="item-scent">
                  <img src={this.getImageFromProduct(d.product)} alt="scent" />
                  {/* {d.name} */}
                </div>
              ))
            }
          </div>
          <div className="text-des-m">
            <b>
              {MoodBt}
              :
            </b>
            {' '}
            {this.props.moodBestSeller}
          </div>
          <div className="text-des-m">
            <b>
              {OlfactiveFamilyBt}
              :
            </b>
            {' '}
            {this.props.familyBestSeller}
          </div>
          {
            profile?.intensity && (
              <div className="intensity">
                {IntensityBt}
                <div className="list-point">
                  {
                    _.map(_.range(5), d => (
                      <div className={classnames('point-i', parseInt(profile?.intensity, 10) >= (d + 1) ? 'active' : '')} />
                    ))
                  }
                </div>
              </div>
            )
          }
          {/* <div className="text-des-m">
            <b>
              Description:
            </b>
          </div> */}
          {
            descriptionHtml
          }
          <div className="line-temp" />
          <div className="div-point-price">
            <div className="price" style={isViewFull ? { marginTop: '16px' } : {}}>
              {generaCurrency(itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : '')}
            </div>
            <ReceiveGrams isPointProductDetail buttonBlocks={buttonBlocks} price={itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : 0} />
          </div>

          <div className="add-card-block">
            {renderQuanityV2}
            <ButtonCTV2
              name={addToCartBt}
              size={isMobile ? 'medium' : undefined}
              onClick={() => {
                if (type.name === 'hand_sanitizer') {
                  this.props.addToCartItem(itemWaxSelect);
                } else {
                  this.props.onClickAddtoCart();
                }
              }
          }
            />
          </div>
        </div>
      );
    }
    return (
      <div className="div-text-block div-col">
        {/* <TitleScentSelection cmsCommon={cmsCommon} /> */}
        <div className="header div-row justify-between items-end">
          <h1>
            {nameProductCustom || nameProduct}
            {isBrowser && <span className="text-gify-buy">{this.props.dataFAQ?.promo_description}</span>}
          </h1>
          <span>
            {generaCurrency(itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : '')}
          </span>
        </div>
        {
          isMobile && (
            <div className="div-row justify-between items-center">
              <span className="text-gify-buy">{this.props.dataFAQ?.promo_description}</span>
              {pointUserHtml}
            </div>
          )
        }
        {
          isBrowser && htmlReview
        }
        {
          ['Perfume', 'single_candle', 'dual_candles', 'home_scents', 'Wax_Perfume', 'dual_crayons', 'bundle_creation', 'reed_diffuser', 'perfume_diy', 'car_diffuser', 'mini_candles', 'oil_burner'].includes(type.name) && (
            <React.Fragment>
              <div className="line" />
              <ItemScentSelect
                data={combosState && combosState.length > 0 ? combosState[0] : undefined}
                image={combosState && combosState.length > 0 && !_.isEmpty(combosState[0]) ? this.getImageFromProduct(combosState[0].product) : isDualCandles || isSingleCandle || isMiniCandle ? icCandle1 : isWaxPerfumeCreate ? icWax : icProduct1}
                title={combosState && combosState.length > 0 && !_.isEmpty(combosState[0]) ? combosState[0].name : isWaxPerfumeCreate ? chooseIngredientBt : ingredient1St}
                description={combosState && combosState.length > 0 && !_.isEmpty(combosState[0]) ? ingredient1St : isWaxPerfumeCreate ? undefined : requiredBt}
                isShowDelete={combosState && combosState.length > 0 && !_.isEmpty(combosState[0])}
                onClickRemove={data => this.onClickRemoveScent(data, 1)}
                onClickChooseScent={data => this.props.onClickChooseScent(data, 1)}
                message={combosState && combosState.length > 0 && combosState[0]?.is_out_of_stock ? outStockBt : undefined}
              />
              <div className="line" />
              {
                !['Wax_Perfume'].includes(type.name) && (
                  <React.Fragment>
                    <ItemScentSelect
                      data={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? combosState[1] : undefined}
                      image={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? this.getImageFromProduct(combosState[1].product) : isDualCandles || isSingleCandle || isMiniCandle ? icCandle2 : icProduct2}
                      title={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? combosState[1].name : ingredient2ndBt}
                      description={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? ingredient2ndBt : (isDualCandles || isMiniCandle) ? requiredBt : isSingleCandle ? optionCandleBt.replace('{price}', generaCurrency(itemCombo ? itemCombo.price : '')) : optionalBt}
                      isShowDelete={combosState && combosState.length > 1 && !_.isEmpty(combosState[1])}
                      onClickRemove={data => this.onClickRemoveScent(data, 2)}
                      onClickChooseScent={data => this.props.onClickChooseScent(data, 2)}
                      recommendData={this.props.recommendData?.suggestions}
                      recommendDataOfScent={this.props.recommendData?.name}
                      onClickrecommend={this.props.onClickrecommend}
                      buttonBlocks={this.props.buttonBlocks}
                      message={combosState && combosState.length > 1 && combosState[1]?.is_out_of_stock ? outStockBt : undefined}
                    />
                    {!this.props.recommendData?.suggestions && <div className="line" />}
                  </React.Fragment>
                )
              }
              {
                isCandles && (
                  <CandleColor
                    buttonBlocks={buttonBlocks}
                    updateColorCandle={this.props.updateColorCandle}
                    variantValues={this.props.variantValues}
                    type={type.name}
                  />
                )
              }
              {
                isMiniCandle && (
                  <React.Fragment>
                    <div className="line" />
                    <ItemScentSelect
                      data={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? combosState[2] : undefined}
                      image={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? this.getImageFromProduct(combosState[2].product) : icCandle3}
                      title={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? combosState[2].name : ingredient3rdBt}
                      description={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? ingredient3rdBt : select3rd}
                      isShowDelete={combosState && combosState.length > 2 && !_.isEmpty(combosState[2])}
                      onClickRemove={data => this.onClickRemoveScent(data, 3)}
                      onClickChooseScent={data => this.props.onClickChooseScent(data, 3)}
                      buttonBlocks={this.props.buttonBlocks}
                      message={combosState && combosState.length > 2 && combosState[2]?.is_out_of_stock ? outStockBt : undefined}
                    />
                    <div className="line" />
                    <ItemScentSelect
                      data={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? combosState[3] : undefined}
                      image={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? this.getImageFromProduct(combosState[3].product) : icCandle4}
                      title={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? combosState[3].name : ingredient4thBt}
                      description={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? ingredient4thBt : select4th}
                      isShowDelete={combosState && combosState.length > 3 && !_.isEmpty(combosState[3])}
                      onClickRemove={data => this.onClickRemoveScent(data, 4)}
                      onClickChooseScent={data => this.props.onClickChooseScent(data, 4)}
                      buttonBlocks={this.props.buttonBlocks}
                      message={combosState && combosState.length > 3 && combosState[3]?.is_out_of_stock ? outStockBt : undefined}
                    />
                    <div className="line" />
                  </React.Fragment>
                )
              }
            </React.Fragment>
          )
        }
        {/* {
          ((type.name === 'Perfume' && itemCombo.isSample) || isDualCrayons) && (
            <div className="choose-type-perfume">
              <div>
                <ItemSelectTypePerfume
                  className={itemCombo.isSample || isDualCrayons ? '' : 'active'}
                  image={icBottle}
                  title={ml30}
                  des={months36Bt}
                  price={this.getPriceBottle(false)}
                  onClick={() => (isDualCrayons ? this.props.onClickGotoPerfumeProductFromCrayons(combos) : this.props.onChangeTypePerfume(false))}
                />
              </div>
              <div>
                <ItemSelectTypePerfume
                  className={itemCombo.isSample || isDualCrayons ? 'active' : ''}
                  image={isDualCrayons ? icDualCrayon : icRollon}
                  title={isDualCrayons ? ml5x2Bt : ml5}
                  des={isDualCrayons ? week2Bt : tryMiniBt}
                  price={isDualCrayons ? generaCurrency(itemWaxSelect?.price) : this.getPriceBottle(true)}
                  onClick={() => (icDualCrayon ? {} : this.props.onChangeTypePerfume(true))}
                />
              </div>
            </div>
          )
        } */}

        {
          type.name === 'hand_sanitizer' && (
            <div className="option-sanitizer">
              <Dropdown
                isOpen={dropdownHandSanitizerOpen}
                toggle={this.toggleHandSanitizer}
                className="dropdown-hand-sanitizer"
                direction="down"
              >
                <DropdownToggle
                  tag="span"
                  onClick={this.toggle}
                  data-toggle="dropdown"
                  className="w-100"
                >
                  <div className="item-hand-sanitizer">
                    <div className="title">
                      <h2>
                        {itemWaxSelect?.name}
                      </h2>
                      <span>
                        {ml15}
                      </span>
                    </div>
                    <img src={icDown} alt="icDown" />
                  </div>
                </DropdownToggle>
                <DropdownMenu>
                  {
                    _.map(items, d => (
                      <div
                        className={classnames('item-hand-sanitizer', itemWaxSelect?.id === d.id ? 'active' : '')}
                        onClick={() => {
                          this.onChangeHandSanitizer(d);
                        }}
                      >
                        <div className="title">
                          <h2>
                            {d.name}
                          </h2>
                          <span>
                            {ml15}
                          </span>
                        </div>
                      </div>
                    ))
                  }
                </DropdownMenu>
              </Dropdown>
            </div>
          )
        }
        {
          isMobile ? htmlAddCartMobile(false) : htmlAddCartWeb
        }
        {
          isMobile && htmlReview
        }
        {
          isWaxPerfume && type.name !== 'hand_sanitizer' && type.name !== 'Wax_Perfume' ? (
            <button
              type="button"
              className={classnames('bt-checkout', itemCombo && itemCombo.id ? '' : 'disable', isMobile ? 'hidden' : '', itemWaxSelect?.is_out_of_stock ? 'out-stock' : '')}
              disabled={!itemCombo || !itemCombo.id || itemWaxSelect?.is_out_of_stock}
              onClick={() => this.props.addToCartItem(itemWaxSelect)}
              style={{ height: '50px' }}
            >
              {itemWaxSelect?.is_out_of_stock ? soldOutBt : addToCartBt}
            </button>
          ) : (<div />)
        }
        {
          descriptionHtml
        }
        {isMobile && htmlAddCartMobile(true)}
      </div>
    );
  }
}

TextBlock.propTypes = {
  datas: PropTypes.arrayOf().isRequired,
  nameProduct: PropTypes.string.isRequired,
  itemCombo: PropTypes.shape().isRequired,
  priceSample: PropTypes.string.isRequired,
  scrollToSample: PropTypes.func.isRequired,
  cmsCommon: PropTypes.arrayOf().isRequired,
  itemBottle: PropTypes.shape().isRequired,
  quantity: PropTypes.number.isRequired,
  onChangeQuantity: PropTypes.func.isRequired,
  onClickAddtoCart: PropTypes.func.isRequired,
  combos: PropTypes.arrayOf().isRequired,
  arrayImgCarousel: PropTypes.arrayOf().isRequired,
  handleChangeNameBottle: PropTypes.func.isRequired,
  toggleCustom: PropTypes.func.isRequired,
  propsState: PropTypes.shape().isRequired,
  type: PropTypes.string.isRequired,
  altName: PropTypes.string.isRequired,
  handleHeightDetails: PropTypes.func.isRequired,
  isWaxPerfume: PropTypes.bool.isRequired,
  isHandSanitizer: PropTypes.bool.isRequired,
  items: PropTypes.arrayOf().isRequired,
  addToCartItem: PropTypes.func.isRequired,
  updateWaxPerfume: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  currentImg: PropTypes.string.isRequired,
  nameBottle: PropTypes.string.isRequired,
  removeCustomization: PropTypes.func.isRequired,
  idCartItem: PropTypes.string.isRequired,
};

export default TextBlock;
