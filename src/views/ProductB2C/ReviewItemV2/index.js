import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import moment from 'moment';
import classnames from 'classnames';
import { startv2, startGrayV2 } from '../../../imagev2/svg';
import './style.scss';
import { isMobile } from '../../../DetectScreen';

function ReviewItemV2(props) {
  const {
    date_completed: dataCompleted, image, date_modified: dateModified,
    comment, rating_product: scoreProduct, rating_satisfaction: scoreSatisfaction, rating_scent: scoreScent,
    buttonBlocks, user, guest, avgRating, isVersion3,
  } = props.data;

  const name = !_.isEmpty(user) ? user.name : !_.isEmpty(guest) ? guest.name : '';
  const newScore = avgRating || (parseFloat(scoreProduct) + parseFloat(scoreSatisfaction) + parseFloat(scoreScent)) / 3;
  if (props.isVersion3) {
    return (
      <div className={classnames('review-item-v2', 'version-3')}>
        <div className={classnames('des body-text-s-regular', isMobile ? '' : 'm-size')}>
          { comment }
        </div>
        <div className="line-start">
          {
            _.map(_.range(5), (x, index) => (
              <img src={index + 1 <= newScore ? startv2 : startGrayV2} alt="star" />
            ))
          }
        </div>
        <div className="other-Tagline">
          {name}
        </div>
        <div className={classnames('text-date body-text-s-regular', isMobile ? 's-size' : '')}>
          {moment(dateModified).format('DD MMM YYYY')}
        </div>
      </div>
    );
  }
  return (
    <div className={classnames('review-item-v2')}>
      <div className="line-header div-row">
        <div className="other-Tagline">
          {name}
        </div>
        <div className="line-start">
          {
            _.map(_.range(5), (x, index) => (
              <img src={index + 1 <= newScore ? startv2 : startGrayV2} alt="star" />
            ))
          }
        </div>
      </div>
      <div className={classnames('body-text-s-regular', isMobile ? 's-size' : '')}>
        {moment(dateModified).format('DD MMM YYYY')}
      </div>
      {
        comment ? (
          <div className={classnames('des body-text-s-regular', isMobile ? '' : 'm-size')}>
            { comment }
          </div>
        ) : (
          <div className={classnames('des body-text-s-regular', isMobile ? '' : 'm-size')}>
            <i>This user has no written review.</i>
          </div>
        )
      }
    </div>
  );
}

export default ReviewItemV2;
