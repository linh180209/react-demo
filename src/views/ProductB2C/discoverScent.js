import React, { Component } from 'react';
import _ from 'lodash';
import '../../styles/discover-scents.scss';
import ScentBoxProduct from '../MMO2/components/MultiStep/scentBoxProduct';
import { GET_ALL_SCENT_NOTES, GET_PRODUCT_FOR_CART } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import BottleScentV2 from './bottleScentV2';
import ScentCharacter from './scentCharacter';
import icBottle from '../../image/bottle-custome.gif';
import MultiStep from '../MMO2/components/MultiStep/multiStepProduct';
import { getNameFromButtonBlock } from '../../Redux/Helpers';
import icNone from '../../image/icon/icNone.svg';
import { isMobile } from '../../DetectScreen';

class DiscoverScent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      resultScentNotes: [],
      indexFullBox2: 0,
      dataScent: {
        scent1: {},
        scent2: {},
        product: {},
      },
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.scentNotes?.length > 0 && prevState.scentNotes?.length === 0) {
      return { resultScentNotes: nextProps.scentNotes };
    }
    return null;
  }

  componentDidMount() {
    // this.fetchScentNotes();
    if (this.props.scentNotes?.length > 0) {
      this.setState({ resultScentNotes: this.props.scentNotes });
    }
  }

  onClickShowFullBox2 = (index) => {
    console.log('onClickShowFullBox2', index);
    this.setState({ indexFullBox2: index });
  }

  generateProduct = (dataScent) => {
    console.log('dataScent', dataScent);
    if (_.isEmpty(dataScent.scent1) && _.isEmpty(dataScent.scent2)) {
      dataScent.product = {};
      this.setState({ dataScent });
      return;
    }
    if (!_.isEmpty(dataScent.scent1) && !_.isEmpty(dataScent.scent2)) {
      const options = {
        url: dataScent.scent1.id === dataScent.scent2.id ? `${GET_PRODUCT_FOR_CART}?combo=${dataScent.scent1.id}&type=scent` : `${GET_PRODUCT_FOR_CART}?combo=${dataScent.scent1.id}${`,${dataScent.scent2.id}`}&type=perfume`,
        method: 'GET',
      };
      fetchClient(options).then((result) => {
        if (result && !result.isError) {
          _.assign(dataScent.scent1, { images: [{ type: 'unisex', image: dataScent.scent1.image }] });
          _.assign(dataScent.scent2, { images: [{ type: 'unisex', image: dataScent.scent2.image }] });
          result.combos = [
            {
              id: dataScent.scent1.id,
              product: dataScent.scent1,
              name: dataScent.scent1.name,
              description: dataScent.scent1.item_description,
            },
            {
              id: dataScent.scent2.id,
              product: dataScent.scent2,
              name: dataScent.scent2.name,
              description: dataScent.scent2.item_description,
            },
          ];
          dataScent.product = result;
          this.setState({ dataScent });
          return;
        }
        throw new Error(result.message);
      }).catch((err) => {
        toastrError(err.message);
      });
    } else if (!_.isEmpty(dataScent.scent1)) {
      _.assign(dataScent.scent1, { images: [{ type: 'unisex', image: dataScent.scent1.image }] });
      _.assign(dataScent.product, {
        combos: [
          {
            id: dataScent.scent1.id,
            product: dataScent.scent1,
            name: dataScent.scent1.name,
            description: dataScent.scent1.item_description,
          },
        ],
      });
      this.setState({ dataScent: _.cloneDeep(dataScent) });
    } else if (!_.isEmpty(dataScent.scent2)) {
      _.assign(dataScent.scent2, { images: [{ type: 'unisex', image: dataScent.scent2.image }] });
      _.assign(dataScent.product, {
        combos: [
          {
            id: dataScent.scent2.id,
            product: dataScent.scent2,
            name: dataScent.scent2.name,
            description: dataScent.scent2.item_description,
          },
        ],
      });
      this.setState({ dataScent: _.cloneDeep(dataScent) });
    }
  }

  selectScent1 = (data) => {
    const { dataScent } = this.state;
    dataScent.scent1 = data;
    this.generateProduct(dataScent);
  }

  selectScent2 = (data) => {
    const { dataScent } = this.state;
    dataScent.scent2 = data;
    this.generateProduct(dataScent);
  }

  render() {
    const {
      resultScentNotes, isNextView, indexFullBox2, dataScent,
    } = this.state;
    const { buttonBlocks, cmsCommon } = this.props;
    const discoverBt = getNameFromButtonBlock(buttonBlocks, 'discover_the_scents');
    const tapOnBt = getNameFromButtonBlock(buttonBlocks, 'tap_on');
    const mainScentBt = getNameFromButtonBlock(buttonBlocks, 'main_scent');
    const chooseBt = getNameFromButtonBlock(buttonBlocks, 'CHOOSE_2nd_INGREDIENT');
    const secondaryBt = getNameFromButtonBlock(buttonBlocks, 'secondary_scent');
    const selectBt = getNameFromButtonBlock(buttonBlocks, 'Select_ingridients');
    const { combos, profile } = dataScent.product;
    if (resultScentNotes.length === 0 || !buttonBlocks || buttonBlocks.length === 0) {
      return null;
    }
    const topData = resultScentNotes
      ? _.filter(resultScentNotes, x => x.type === 'top')[0]
      : '';
    const botData = resultScentNotes
      ? _.filter(resultScentNotes, x => x.type === 'bot')[0]
      : '';
    const heartData = resultScentNotes
      ? _.filter(resultScentNotes, x => x.type === 'heart')[0]
      : '';
    console.log('dataScent', dataScent);

    const orBt = getNameFromButtonBlock(buttonBlocks, 'OR');

    return (
      <div className="div-discover-scents">
        <h3>
          {discoverBt}
        </h3>
        <span className={isMobile ? 'description' : 'hidden'}>
          {tapOnBt}
        </span>
        {
          isMobile ? (
            <React.Fragment>
              <div className="mmo2-mobile">
                <MultiStep
                  dataProduct={this.props.data}
                  buttonBlocks={buttonBlocks}
                  dataScentNote={resultScentNotes}
                  bottleGif={[{
                    value: {
                      caption: 'bottle-gif',
                      image: icBottle,
                    },
                  }]}
                  resultScentNotes={resultScentNotes}
                  video=""
                  // headerCms={}
                  headerAndParagraphBocks={this.props.headerAndParagraphBocks}
                  onCreateMix={this.onCreateMix}
                  handleDataCustome={this.handleDataCustome}
                  onClickIngredient={this.props.onClickIngredient}
                />
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div className="div-scent">
                <div className={isMobile ? 'hidden' : 'scent-box-left'}>
                  <h4 className="title-header-scent">
                    {mainScentBt}
                  </h4>
                  <ScentBoxProduct
                    dataScentNote={resultScentNotes}
                    data={heartData}
                    isDay={undefined}
                    selectScent={this.selectScent1}
                    isNextView={isNextView}
                    buttonBlocks={buttonBlocks}
                    onClickShowFullBox2={() => {}}
                    scentSelected={dataScent.scent1}
                    onClickIngredient={this.props.onClickIngredient}
                  />
                </div>
                <div className="bottle-scent">
                  <BottleScentV2 image1={dataScent.scent1.image} image2={dataScent.scent2.image} buttonBlocks={buttonBlocks} />
                </div>
                <button type="button" className={isMobile ? 'bt-choose-scent' : 'hidden'}>
                  {chooseBt}
                </button>
                <div className={isMobile ? 'hidden' : indexFullBox2 ? 'scent-box-left' : 'scent-box-right'}>
                  <h4>
                    {secondaryBt}
                  </h4>
                  {
              indexFullBox2 !== 2
              && (
                <ScentBoxProduct
                  dataScentNote={resultScentNotes}
                  data={topData}
                  isDay
                  selectScent={this.selectScent2}
                  isNextView={isNextView}
                  buttonBlocks={buttonBlocks}
                  onClickShowFullBox2={this.onClickShowFullBox2}
                  scentSelected={dataScent.scent2}
                  onClickIngredient={this.props.onClickIngredient}
                />
              )
            }
                  {
              indexFullBox2 === 0
              && (
                <h4 className="header-mix">
                  {orBt}
                </h4>
              )
            }
                  {
              indexFullBox2 !== 1
              && (
              <ScentBoxProduct
                dataScentNote={resultScentNotes}
                data={botData}
                isDay={false}
                selectScent={this.selectScent2}
                isNextView={isNextView}
                buttonBlocks={buttonBlocks}
                onClickShowFullBox2={this.onClickShowFullBox2}
                scentSelected={dataScent.scent2}
                onClickIngredient={this.props.onClickIngredient}
              />
              )
            }

                </div>
              </div>
              {
                !_.isEmpty(dataScent.scent1) || !_.isEmpty(dataScent.scent2) ? (
                  <ScentCharacter cmsCommon={cmsCommon} combos={combos || []} onClickIngredient={this.props.onClickIngredient} profile={profile} type={{ alt_name: 'Perfume', name: 'Perfume' }} />
                ) : (
                  <div className="div-scent-null">
                    <img src={icNone} alt="icon" />
                    <span>
                      {selectBt}
                    </span>
                  </div>
                )
              }
            </React.Fragment>
          )
        }
      </div>
    );
  }
}

export default DiscoverScent;
