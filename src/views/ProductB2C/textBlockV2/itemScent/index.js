import AddIcon from '@mui/icons-material/Add';
import DeleteOutlineTwoToneIcon from '@mui/icons-material/DeleteOutlineTwoTone';
import EditIcon from '@mui/icons-material/Edit';
import classnames from 'classnames';
import _ from 'lodash';
import React from 'react';
import ButtonCT from '../../../../componentsv2/buttonCT';
import { isMobile767fn } from '../../../../DetectScreen';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import { candleHolder, icMiniCandleHolder } from '../../../../imagev2/svg';
import './styles.scss';

function ItemScent(props) {
  const onClickrecommend = (data) => {
    if (props.onClickrecommend) {
      props.onClickrecommend(data);
    }
  };

  const recommendBt = getNameFromButtonBlock(props.buttonBlocks, 'Recommended scents with');
  const candleHolderBt = getNameFromButtonBlock(props.buttonBlocks, 'Complimentary candle holder');
  const miniCandleHolderBt = getNameFromButtonBlock(props.buttonBlocks, 'Complimentary petite candle holder');
  const isCustomBottle = props.currentImg || props.nameBottle;
  if (isCustomBottle) {
    return (
      <div className={classnames('item-scent', props.isDisabled ? 'disabled' : '')}>
        <img className="image-icon" src={props.icon} alt="icon" />
        <div className="block-right custome-bottle">
          <div className="text-name">
            {props.index}
            .
            {' '}
            {props.name}
          </div>
          <div className="text-custome" style={{ fontFamily: props.font }}>
            {props.nameBottle}
          </div>
          <div className="color-custome" style={{ background: props.color }} />
          <button
            className="button-bg__none bt-edit"
            type="button"
            onClick={props.onClickChooseScent}
          >
            <EditIcon />
          </button>
        </div>
      </div>
    );
  }

  return (
    <div className={classnames('item-scent', props.isDisabled ? 'disabled' : '')}>
      <img className="image-icon" src={props.icon} alt="icon" />
      <div className="block-right">
        <div className="step-scent">
          {
            !_.isEmpty(props.data) ? (
              <div className="block-select">
                <div className="block-text">
                  {
                    props.subText && (
                      <div className="tab-text">
                        {props.subText}
                      </div>
                    )
                  }

                  <div className="text-name">
                    {props.index}
                    .
                    {' '}
                    {props.name}
                  </div>
                </div>
                <button
                  className="button-bg__none bt-delete"
                  type="button"
                  onClick={() => props.onClickRemove(props.data)}
                >
                  <DeleteOutlineTwoToneIcon />
                </button>
              </div>
            ) : (
              <div className="block-select none-data">
                <div className="block-text">
                  {
                    props.subText && (
                      <div className="tab-text">
                        {props.subText}
                      </div>
                    )
                  }

                  <div className="text-name">
                    {props.index}
                    .
                    {' '}
                    {props.name}
                  </div>
                  {
                    props.description && (
                      <div className="text-des">
                        {props.description}
                      </div>
                    )
                  }
                </div>
                {
                  isMobile767fn() ? (
                    <ButtonCT
                      onClick={props.onClickChooseScent}
                      variant="outlined"
                      onlyIcon={<AddIcon style={{ color: '#2C2C22' }} />}
                      disabled={props.isDisabled}
                    />
                  ) : (
                    <ButtonCT
                      onClick={props.onClickChooseScent}
                      variant="outlined"
                      name={props.nameButton}
                      endIcon={<AddIcon style={{ color: '#2C2C22' }} />}
                      disabled={props.isDisabled}
                    />
                  )
                }
              </div>
            )
          }
        </div>
        {
          props.recommendData && (
            <div className="block-recommend">
              <div className="text">
                {recommendBt}
                {' '}
                {props.recommendDataOfScent}
                :
              </div>
              <div className="list-scent">
                {
                    _.map(props.recommendData, d => (
                      <div
                        onClick={() => {
                          onClickrecommend(d);
                        }}
                        className="item-scent-recommend div-col"
                      >
                        <div className="text-family">
                          {d.group}
                        </div>
                        <div className="image-scent">
                          <img src={d.image || _.find(d.images, x => x.type === 'suggestion')?.image} alt="icon" />
                        </div>
                      </div>
                    ))
                  }
              </div>
            </div>
          )
        }
        {
          props.isHolder && _.isEmpty(props.data) && (
            <div className="block-holder">
              <img src={props.isMiniCandle ? icMiniCandleHolder : candleHolder} alt="icon" />
              <div>
                +
                {' '}
                {props.isMiniCandle ? miniCandleHolderBt : candleHolderBt}
              </div>
            </div>
          )
        }
      </div>
    </div>
  );
}

export default ItemScent;
