/* eslint-disable react/sort-comp */
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import classnames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import {
  Dropdown, DropdownMenu, DropdownToggle,
} from 'reactstrap';
import CheckIcon from '@mui/icons-material/Check';
import '../../../../node_modules/slick-carousel/slick/slick-theme.css';
import '../../../../node_modules/slick-carousel/slick/slick.css';
import ReceiveGrams from '../../../components/receiveGrams';
import ButtonCTV2 from '../../../componentsv2/buttonCT';
import HeaderCT from '../../../componentsv2/headerCT';
import ScentCharacterV2 from '../../../componentsv2/scentCharacterV2';
import {
  isBrowser, isMobile, isMobile767fn,
} from '../../../DetectScreen';
import icAdd from '../../../image/icon/ic-add-product.svg';
import icSub from '../../../image/icon/ic-sub-product.svg';
import { discoveryDetail } from '../../../imagev2/png';
import {
  candle1,
  candle2, candleHolder, icBurner, icCarDiffUser, icMiniCandle1,
  icMiniCandle2, icMiniCandle3, icMiniCandle4, icOil1, icOil2,
  icReed, perfumeCustom, perfumeStep1, perfumeStep2, startOutline,
  emptyScent, icMiniCandleHolder, icPerfumeDiy,
} from '../../../imagev2/svg';
import {
  generaCurrency, getAltImageV2, getNameFromButtonBlock, getNameFromCommon, isRightAlignedText, scrollToTargetAdjusted,
} from '../../../Redux/Helpers';
import CandleColor from '../candleColor';
import ItemScent from './itemScent';
import './styles.scss';

// import icDown from '../../image/icon/ic-down.svg';

const getDataCustome = (propsState) => {
  let name;
  let imageCustome;
  let isCustome;
  if (propsState) {
    const { custome } = propsState;
    if (custome) {
      name = custome.name;
      imageCustome = custome.image;
      isCustome = true;
    }
  }
  return { name, imageCustome, isCustome };
};

class TextBlockV2 extends Component {
  constructor(props) {
    super(props);
    const { name, isCustome } = getDataCustome(
      props.propsState,
    );

    this.state = {
      isShowCustome: isCustome,
      dropdownOpen: false,
      dropdownHandSanitizerOpen: false,
      items: props.items,
      itemWaxSelect: props.type.name === 'hand_sanitizer' ? props.itemHandlerSanitizer : props.type.name === 'home_scents' ? props.items[0] : (_.find(props.items, x => x.is_sample === false) || props.items[0]),
      combosState: _.cloneDeep(props.combosChooseScent),
      oldCombo: props.combosChooseScent,
      subTabV3: props.isBestSeller || ['hand_sanitizer', 'discovery_box'].includes(props.type.name) ? 1 : 0,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const {
      propsState, combosChooseScent, items, type, itemHandlerSanitizer, isBestSeller,
    } = nextProps;
    if (propsState !== prevState.propsState) {
      const { name, isCustome } = getDataCustome(
        propsState,
      );
      const nameText = name || '';
      _.assign(objectReturn, {
        propsState,
        nameText,
        isShowCustome: isCustome,
      });
    }
    if (JSON.stringify(combosChooseScent) !== JSON.stringify(prevState.oldCombo)) {
      _.assign(objectReturn, {
        combosState: _.cloneDeep(combosChooseScent),
        oldCombo: combosChooseScent,
      });
    }
    if (items !== prevState.items) {
      _.assign(objectReturn, {
        items,
        itemWaxSelect: type.name === 'hand_sanitizer' ? itemHandlerSanitizer : type.name === 'home_scents' ? items[0] : (_.find(items, x => x.is_sample === false) || items[0]),
      });
    }
    if (isBestSeller !== prevState.isBestSeller) {
      _.assign(objectReturn, { isBestSeller, subTabV3: isBestSeller || ['hand_sanitizer', 'discovery_box'].includes(type.name) ? 1 : 0 });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : {};
  }

  toggle = () => {
    const { dropdownOpen } = this.state;
    this.setState({
      dropdownOpen: !dropdownOpen,
    });
  }

  toggleHandSanitizer = () => {
    const { dropdownHandSanitizerOpen } = this.state;
    this.setState({
      dropdownHandSanitizerOpen: !dropdownHandSanitizerOpen,
    });
  }

  onChangeWaxPerfume = (data) => {
    this.props.updateWaxPerfume(data);
    this.setState({ itemWaxSelect: data, dropdownOpen: false });
  }

  onChangeHandSanitizer = (data) => {
    if (this.state.itemWaxSelect.id !== data.id) {
      this.props.updateWaxPerfume(data);
      this.props.onClickWaxPerfume(data);
    }
    this.setState({ dropdownHandSanitizerOpen: false });
  }

  onChange = (e) => {
    const { value } = e.target;
    if (value.length > 15) {
      return;
    }
    this.setState({ nameText: value });
    this.props.handleChangeNameBottle(value);
  }

  scrollToReviews = () => {
    const ele = document.getElementById('anchor-reviews');
    if (ele) {
      ele.scrollIntoView({ behavior: 'smooth' });
    }
  }

  getImageFromProduct = (data) => {
    const ele = _.find(data.images, d => d.type === 'unisex');
    if (ele) {
      return ele.image;
    }
    return '';
  }

  onClickRemoveScent = (data, index) => {
    const { combosState } = this.state;
    const { itemCombo } = this.props;
    delete itemCombo.id;
    // _.remove(combosState, x => x.id === data.id);
    const indexEle = _.findIndex(combosState, x => x.id === data.id);
    console.log('indexEle', { indexEle, combosState, data });
    if (indexEle > -1) {
      combosState.splice(indexEle, 1);
    }
    this.setState({ combosState });
    this.props.updateScentSelect(combosState);
    this.props.onClickApply(undefined, index);
  }

  getPriceBottle = (isRollOn) => {
    const ele = _.find(this.props.bottlePrice || [], x => x.apply_to_sample === isRollOn);
    if (ele) {
      return generaCurrency(ele.price, false);
    }
    return '';
  }

  getHeightTextDes = () => {
    const ele = document.getElementById('id-text-des');
    if (ele) {
      console.log('ele.offsetHeight', ele.offsetHeight);
      return ele.offsetHeight;
    }
    return 0;
  }

  getHeightTextDes1 = () => {
    const ele = document.getElementById('id-text-des-1');
    if (ele) {
      console.log('ele.offsetHeight', ele.offsetHeight);
      return ele.offsetHeight;
    }
    return 0;
  }

  summaryWillReceive = (buttonBlocks) => {
    const whatBt = getNameFromButtonBlock(buttonBlocks, 'What you will receive');
    return (
      <div className="summary other-Tagline div-row">
        <b>{whatBt}</b>
        <ArrowDropDownIcon style={{ color: '#2c2c2c' }} />
      </div>
    );
  }

  detailWillReceive = (buttonBlocks) => {
    const completeBt = getNameFromButtonBlock(buttonBlocks, 'Complete set of all Maison’s 34 finest scents');
    return (
      <div className="detail">
        <HeaderCT type={isMobile ? 'Heading-XXS' : 'Heading-S'}>
          {completeBt}
        </HeaderCT>
        <div className="div-image">
          <img src={discoveryDetail} alt="detail" />
        </div>
      </div>
    );
  }

  gotoSelectedScent = () => {
    scrollToTargetAdjusted('anchor-select-scent');
  };

  render() {
    const {
      isShowCustome, dropdownOpen,
      itemWaxSelect, combosState, subTabV3,
    } = this.state;

    const {
      nameProduct, itemCombo,
      cmsCommon, itemBottle, quantity, onChangeQuantity,
      combos, type, nameProductCustom, descriptionCustom,
      isWaxPerfume, items, currentImg, nameBottle, arrayThumbs,
      buttonBlocks, isBestSeller, tags, profile,
    } = this.props;

    const addToCartBt = getNameFromCommon(cmsCommon, 'ADD_TO_CART').toUpperCase();
    const optionalBt = getNameFromButtonBlock(buttonBlocks, 'Optional');
    const outOfStockBt = getNameFromButtonBlock(buttonBlocks, 'Out Of Stock');
    const chooseIngredientBt = getNameFromButtonBlock(buttonBlocks, 'CHOOSE INGREDIENT');
    const chooseFirstCandleBt = getNameFromButtonBlock(buttonBlocks, 'choose first candle');
    const chooseSecondCandleBt = getNameFromButtonBlock(buttonBlocks, 'choose second candle');
    const chooseThirdCandleBt = getNameFromButtonBlock(buttonBlocks, 'choose third candle');
    const chooseFourthCandleBt = getNameFromButtonBlock(buttonBlocks, 'Choose fourth candle');
    const bestSellersHomeBt = getNameFromButtonBlock(buttonBlocks, 'Best Sellers Home');
    const IntensityBt = getNameFromButtonBlock(buttonBlocks, 'Intensity');
    const OlfactiveFamilyBt = getNameFromButtonBlock(buttonBlocks, 'Olfactive Family');
    const MoodBt = getNameFromButtonBlock(buttonBlocks, 'Mood');
    const collectionBt = getNameFromButtonBlock(buttonBlocks, 'COLLECTION');
    const creationBt = getNameFromButtonBlock(buttonBlocks, 'Creation');
    const scentCharacterBt = getNameFromButtonBlock(buttonBlocks, 'Scent Characteristics');
    const descriptionBt = getNameFromButtonBlock(buttonBlocks, 'Description');
    const chooseFirstBt = getNameFromButtonBlock(buttonBlocks, 'CHOOSE FIRST SCENT');
    const chooseSecondBt = getNameFromButtonBlock(buttonBlocks, 'CHOOSE SECOND SCENT');
    const designYourBottleBt = getNameFromButtonBlock(buttonBlocks, 'DESIGN YOUR BOTTLE');
    const bottleDesignBt = getNameFromButtonBlock(buttonBlocks, 'Bottle design');
    const selectScentBt = getNameFromButtonBlock(buttonBlocks, 'Select Scent');
    const designBt = getNameFromButtonBlock(buttonBlocks, 'Design');
    const createYourPersonalBt = getNameFromButtonBlock(buttonBlocks, 'CREATE YOUR PERSONALISED PERFUME IN 3 STEPS');
    const createDualCandleBt = getNameFromButtonBlock(buttonBlocks, 'create your personalised CANDLE in 3 steps');
    const chooseYourBt = getNameFromButtonBlock(buttonBlocks, 'Choose your second half moon');
    const chooseBurnerBt = getNameFromButtonBlock(buttonBlocks, 'choose two scents to go with your oil burner');
    const oilBurnerDes = getNameFromButtonBlock(buttonBlocks, 'This product comes with the oil burner');
    const reedDiffuserDes = getNameFromButtonBlock(buttonBlocks, 'This product comes with reed diffuser base');
    const carDiffuserDes = getNameFromButtonBlock(buttonBlocks, 'This product comes with car diffuser device');
    const perfumeDiyDes = getNameFromButtonBlock(buttonBlocks, 'This product comes with creation diy kit');
    const chooseScentRediffBt = getNameFromButtonBlock(buttonBlocks, 'choose two scents for your reed diffuser');
    const chooseScentCarBt = getNameFromButtonBlock(buttonBlocks, 'choose two scents for your device');
    const chooseProductBt = getNameFromButtonBlock(buttonBlocks, 'Choose product type');
    const candleMiniHolderBt = getNameFromButtonBlock(buttonBlocks, 'Complimentary petite candle holder');
    const candleHolderBt = getNameFromButtonBlock(buttonBlocks, 'Complimentary candle holder');
    const youAreEligibleBt = getNameFromButtonBlock(buttonBlocks, 'You are eligible to get a complimentary 4th candle');
    const upTo4CandleBt = getNameFromButtonBlock(buttonBlocks, 'Up to 4 candles per set. Buy 3 candles and get complimentary 4th candle');
    const noScentSelectedBt = getNameFromButtonBlock(buttonBlocks, 'No scent selected');
    const selectYourBt = getNameFromButtonBlock(buttonBlocks, 'Select your scents');

    const isShowBtCustome = (type.name === 'Creation' && arrayThumbs && arrayThumbs.length > 0) || ((type.name === 'Perfume' || type.name === 'bundle_creation' || type.name === 'perfume_diy') && !itemCombo.isSample) || type.name === 'Elixir' || isShowCustome;
    const isShowDesGenerateCombo = (type.name === 'Perfume' || type.name === 'bundle_creation' || type.name === 'perfume_diy' || type.name === 'car_diffuser' || type.name === 'reed_diffuser') && combos && combos.length > 0;
    const isDualCandles = type.name === 'dual_candles';
    const isSingleCandle = type.name === 'single_candle';
    const isCandles = isDualCandles || isSingleCandle;
    const isMiniCandle = type.name === 'mini_candles';
    const isWaxPerfumeCreate = type.name === 'Wax_Perfume';
    const isDualCrayons = type.name === 'dual_crayons';
    const isOilBurner = type.name === 'oil_burner';
    const isReedDiffuser = type.name === 'reed_diffuser';
    const isCarDiffuser = type.name === 'car_diffuser';
    const isDiscoveryBox = type.name === 'discovery_box';
    const isHandSanitizer = type.name === 'hand_sanitizer';
    const isPerfumeDiy = type.name === 'perfume_diy';
    // const {
    //   scoreScent, scoreProduct, scoreSatisfaction,
    // } = dataReview;
    // const newScore = scoreScent !== undefined && scoreProduct !== undefined && scoreSatisfaction !== undefined ? ((parseFloat(scoreScent) + parseFloat(scoreProduct) + parseFloat(scoreSatisfaction)) / 3) : 0;
    this.props.updateScentSelect(combosState);

    // const heightDesLess = isBestSeller ? (!isMobile767fn() && isBestSeller ? 30 : 70) : 400;
    const dropdownItem = () => (
      <div className="dropdown-item-wrap-v2">
        <div className="div-title">
          {chooseProductBt}
          :
        </div>
        <Dropdown
          isOpen={dropdownOpen}
          toggle={this.toggle}
          className="dropdown-item-v2"
          style={{
            display: 'flex', flex: '1',
          }}
        >
          <DropdownToggle
            tag="span"
            onClick={this.toggle}
            data-toggle="dropdown"
            className="w-100"
          >
            <div
              className="div-row items-center justify-between header-dropdown"
            >
              <div>
                <span className="mr-3">
                  {`${itemWaxSelect.name} ${itemWaxSelect && itemWaxSelect.variant_values && itemWaxSelect.variant_values.length > 0 ? `(${itemWaxSelect.variant_values[0]})` : ''} -`}
                  {' '}
                  <b>
                    {`${generaCurrency(itemWaxSelect.price)}`}
                  </b>
                </span>
              </div>
              <ExpandMoreIcon style={{ color: '#2c2c2c' }} />
            </div>
          </DropdownToggle>
          <DropdownMenu positionFixed>
            {
                  _.map(_.orderBy(_.filter(items, x => x.id !== itemWaxSelect.id)), d => (
                    <div
                      onClick={() => {
                        this.onChangeWaxPerfume(d);
                      }}
                    >
                      <div className="drop-down-item">
                        {`${d.name} ${d && d.variant_values && d.variant_values.length > 0 ? `(${d.variant_values[0]})` : ''} -`}
                        <b style={{ marginLeft: '5px' }}>
                          {`${generaCurrency(d.price)}`}
                        </b>
                      </div>
                    </div>
                  ))
                }
          </DropdownMenu>
        </Dropdown>
      </div>
    );

    const descriptionHtml = iconBlocksDescription => (
      <React.Fragment>
        <div className={classnames(isRightAlignedText() ? 'tr' : 'tj', itemBottle?.product?.description || itemWaxSelect?.description || itemBottle?.product?.short_description ? 'div-description div-col justify-start' : 'hidden')}>
          {/* {!isProductDetailV2 && !isBestSeller && <div className="line mt-3 mb-3" />} */}
          {
            nameProductCustom && !isBestSeller && (
              <div className="name-description mb-3">
                {nameProduct}
              </div>
            )
          }
          {
            isHandSanitizer && (
              dropdownItem()
            )
          }
          {
            iconBlocksDescription && (
              <div className="list-icon">
                {
                  _.map(iconBlocksDescription?.value?.icons, d => (
                    <div>
                      <img src={d.value?.image?.image} alt={getAltImageV2(d.value?.image)} />
                    </div>
                  ))
                }
              </div>
            )
          }
          {
            descriptionCustom ? (
              _.map(ReactHtmlParser(descriptionCustom), d => d)
            ) : ['mini_candles', 'dual_candles', 'dual_crayons', 'home_scents', 'Wax_Perfume', 'single_candle', 'mini_candles', 'oil_burner'].includes(type.name) ? (
              <div id="id-text-des" className={classnames('text-des')}>
                {ReactHtmlParser(itemBottle?.product?.short_description)}
                {/* {['dual_crayons', 'home_scents', 'Wax_Perfume', 'dual_candles', 'single_candle'].includes(type.name) && <div><br /></div>}
                {ReactHtmlParser(combos[0] ? combos[0].product?.short_description : '')}
                {['home_scents', 'oil_burner', 'mini_candles', 'dual_candles'].includes(type.name) && <br />}
                {['oil_burner', 'mini_candles', 'dual_candles'].includes(type.name) && <br />}
                {ReactHtmlParser(combos[1] ? combos[1].product?.short_description : '')}
                {isMiniCandle && (<br />)}
                {isMiniCandle && (<br />)}
                {ReactHtmlParser(combos[2] ? combos[2].product?.short_description : '')}
                {isMiniCandle && (<br />)}
                {isMiniCandle && (<br />)}
                {ReactHtmlParser(combos[3] ? combos[3].product?.short_description : '')} */}
              </div>
            ) : (
              <span id="id-text-des-1">
                {isShowDesGenerateCombo
                  ? (
                    <React.Fragment>
                      {ReactHtmlParser(itemBottle?.product?.description)}
                      {/* <br />
                      {ReactHtmlParser(combos[0]?.product?.short_description)}
                      <br />
                      {ReactHtmlParser(combos[1] ? combos[1]?.product?.short_description : '')} */}
                    </React.Fragment>
                  )
                  : isWaxPerfume ? ReactHtmlParser(itemWaxSelect?.description) : itemBottle
                    ? ReactHtmlParser(itemBottle?.product?.description) : ''}
              </span>
            )
          }

        </div>
      </React.Fragment>
    );

    const renderQuanityV2 = (
      <div className={classnames('div-quantity')}>
        <div className="div-number">
          <button
            onClick={() => onChangeQuantity({ target: { value: quantity - 1 } })}
            className={classnames('button-bg__none', quantity < 2 ? 'disabled' : '')}
            type="button"
            disabled={quantity < 2}
          >
            <img src={icSub} alt="sub" />
          </button>

          <div className="quanity">
            {quantity}
          </div>
          <button
            onClick={() => onChangeQuantity({ target: { value: quantity + 1 } })}
            className="button-bg__none"
            type="button"
          >
            <img src={icAdd} alt="sub" />
          </button>
        </div>
      </div>
    );

    const tabCreation = () => (
      <div className="tab-creation div-col">
        {
          (isOilBurner || isReedDiffuser || isCarDiffuser || isPerfumeDiy) && (
            <div className="title--image-text">
              <img src={isPerfumeDiy ? icPerfumeDiy : isCarDiffuser ? icCarDiffUser : isReedDiffuser ? icReed : icBurner} alt="icon" />
              <div>
                {isPerfumeDiy ? perfumeDiyDes : isCarDiffuser ? carDiffuserDes : isReedDiffuser ? reedDiffuserDes : oilBurnerDes}
              </div>
            </div>
          )
        }
        <div className="sub-header">
          {isCarDiffuser ? chooseScentCarBt : isReedDiffuser ? chooseScentRediffBt : isOilBurner ? chooseBurnerBt : isDualCandles ? createDualCandleBt : createYourPersonalBt}
          :
        </div>
        <div className="step-list div-col">
          <ItemScent
            name={combosState && combosState.length > 0 && !_.isEmpty(combosState[0]) ? combosState[0].name : (isCandles || isMiniCandle) ? chooseFirstCandleBt : isWaxPerfumeCreate ? chooseIngredientBt : chooseFirstBt}
            data={combosState && combosState.length > 0 ? combosState[0] : undefined}
            onClickChooseScent={data => this.props.onClickChooseScent(data, 1)}
            onClickRemove={data => this.onClickRemoveScent(data, 1)}
            icon={combosState && combosState.length > 0 && !_.isEmpty(combosState[0]) ? this.getImageFromProduct(combosState[0].product) : (isOilBurner || isReedDiffuser || isCarDiffuser) ? icOil1 : isCandles ? candle1 : isMiniCandle ? icMiniCandle1 : perfumeStep1}
            buttonBlocks={this.props.buttonBlocks}
            nameButton={selectScentBt}
            index={1}
          />
          {
            !isWaxPerfumeCreate && (
              <ItemScent
                subText={(combosState && combosState.length > 1 || isMiniCandle) ? undefined : optionalBt}
                name={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? combosState[1].name : (isCandles || isMiniCandle) ? chooseSecondCandleBt : chooseSecondBt}
                data={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? combosState[1] : undefined}
                icon={combosState && combosState.length > 1 && !_.isEmpty(combosState[1]) ? this.getImageFromProduct(combosState[1].product) : (isOilBurner || isReedDiffuser || isCarDiffuser) ? icOil2 : isCandles ? candle2 : isMiniCandle ? icMiniCandle2 : perfumeStep2}
                onClickChooseScent={data => this.props.onClickChooseScent(data, 2)}
                onClickRemove={data => this.onClickRemoveScent(data, 2)}
                isDisabled={(!combosState || combosState.length === 0)}
                recommendData={this.props.recommendData?.suggestions}
                recommendDataOfScent={this.props.recommendData?.name}
                buttonBlocks={this.props.buttonBlocks}
                onClickrecommend={d => this.props.onClickApply(d, 2)}
                nameButton={selectScentBt}
                index={2}
                description={isCandles ? chooseYourBt : undefined}
                isHolder={isCandles || isMiniCandle}
                isMiniCandle={isMiniCandle}
              />
            )
          }
          {
            isMiniCandle && (
              <React.Fragment>
                {
                  combosState.length > 1 && (
                    <ItemScent
                      name={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? combosState[2].name : chooseThirdCandleBt}
                      data={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? combosState[2] : undefined}
                      icon={combosState && combosState.length > 2 && !_.isEmpty(combosState[2]) ? this.getImageFromProduct(combosState[2].product) : icMiniCandle3}
                      onClickRemove={data => this.onClickRemoveScent(data, 3)}
                      onClickChooseScent={data => this.props.onClickChooseScent(data, 3)}
                      buttonBlocks={this.props.buttonBlocks}
                      index={3}
                      nameButton={selectScentBt}
                    />
                  )
                }
                {
                  combosState.length > 2 && (
                    <ItemScent
                      name={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? combosState[3].name : chooseFourthCandleBt}
                      data={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? combosState[3] : undefined}
                      icon={combosState && combosState.length > 3 && !_.isEmpty(combosState[3]) ? this.getImageFromProduct(combosState[3].product) : icMiniCandle4}
                      onClickRemove={data => this.onClickRemoveScent(data, 4)}
                      onClickChooseScent={data => this.props.onClickChooseScent(data, 4)}
                      buttonBlocks={this.props.buttonBlocks}
                      index={4}
                      nameButton={selectScentBt}
                    />
                  )
                }
              </React.Fragment>
            )
          }
          {
            isShowBtCustome && (
              <ItemScent
                subText={currentImg || nameBottle ? undefined : optionalBt}
                name={currentImg || nameBottle ? bottleDesignBt : designYourBottleBt}
                icon={currentImg || perfumeCustom}
                onClickChooseScent={this.props.toggleCustom}
                isDisabled={!combosState || combosState.length === 0}
                buttonBlocks={this.props.buttonBlocks}
                nameButton={designBt}
                index={3}
                currentImg={this.props.currentImg}
                nameBottle={this.props.nameBottle}
                font={this.props.font}
                color={this.props.color}
              />
            )
          }
          {
            isDualCandles && (
              <CandleColor
                buttonBlocks={buttonBlocks}
                updateColorCandle={this.props.updateColorCandle}
                variantValues={this.props.variantValues}
                type={type.name}
                isVersion2
              />
            )
          }
          {
            isMiniCandle && combosState.length < 4 && (
              <div className="des-mini">
                {combosState.length < 3 ? upTo4CandleBt : youAreEligibleBt}
              </div>
            )
          }
          {
            ((isMiniCandle || isDualCandles) && combosState.length > 1) && (
              <div className="block-holder-mini">
                <img src={isMiniCandle ? icMiniCandleHolder : candleHolder} alt="icon" />
                <div>
                  <CheckIcon style={{ color: '#73D13D' }} />
                  {' '}
                  {isDualCandles ? candleHolderBt : candleMiniHolderBt}
                </div>
              </div>
            )
          }
        </div>
      </div>
    );

    const tabDescription = () => (
      <div className="tab-description div-col">
        {descriptionHtml(isHandSanitizer || isDiscoveryBox || isCarDiffuser || isCandles || isOilBurner || isReedDiffuser ? this.props.iconBlocksDescription : undefined)}
      </div>
    );

    const scentCharacter = () => (combosState?.length === 0 || !combosState[0]?.product?.ingredient ? (
      <div className="div-empty">
        <img src={emptyScent} alt="empty" />
        <span>
          {noScentSelectedBt}
        </span>
        <span>
          {selectYourBt}
        </span>
      </div>
    ) : (
      <ScentCharacterV2
        datas={combosState}
        cmsCommon={this.props.cmsCommon}
        profile={this.props.profile}
        isMiniCandle={isMiniCandle}
        className={isMobile767fn() ? '' : 'text-block-info-v2'}
        isPerfumeProduct={this.props.isPerfumeProduct}
      />
    ));

    const listButtonTab = (
      <div className="list-tab">
        {
          !isBestSeller && !isDiscoveryBox && !isHandSanitizer && (
            <button
              onClick={() => this.setState({ subTabV3: 0 })}
              className={classnames('button-bg__none', subTabV3 === 0 ? 'active' : '')}
              type="button"
            >
              {creationBt}
            </button>
          )
        }
        <button
          onClick={() => this.setState({ subTabV3: 1 })}
          className={classnames('button-bg__none', subTabV3 === 1 ? 'active' : '')}
          type="button"
        >
          {descriptionBt}
        </button>
        {
            !isDiscoveryBox && !isHandSanitizer && (
              <button
                onClick={() => this.setState({ subTabV3: 2 })}
                className={classnames('button-bg__none', subTabV3 === 2 ? 'active' : '')}
                type="button"
              >
                {scentCharacterBt}
              </button>
            )
          }
      </div>
    );

    if (isBestSeller) {
      return (
        <div className="div-text-block div-col block-bestSeller text-block-v2">
          <div className="list-tag">
            {
              type.name !== 'Creation' && (
                <div className="d-tag-0">
                  <img src={startOutline} alt="icon" />
                  {bestSellersHomeBt}
                </div>
              )
            }
            {
              _.map(tags, d => (
                <div className="d-tag-1" style={{ background: d.color }}>
                  <div className="dot" />
                  {d?.name}
                  {' '}
                  {collectionBt}
                </div>
              ))
            }
            {
              this.props.actualType?.alt_name && (
                <div className="d-tag-2">
                  {this.props.actualType?.alt_name}
                </div>
              )
            }
          </div>
          <div className="name-title">
            {nameProductCustom || nameProduct}
          </div>

          {listButtonTab}
          {
            subTabV3 === 1 ? (
              <React.Fragment>
                <div className="list-scent">
                  {
                    _.map(combosState, d => (
                      <div className="item-scent">
                        <img src={this.getImageFromProduct(d.product)} alt="scent" />
                        {/* {d.name} */}
                      </div>
                    ))
                  }
                </div>
                <div className="text-des-m">
                  <b>
                    {MoodBt}
                    :
                  </b>
                  {' '}
                  {this.props.moodBestSeller}
                </div>
                <div className="text-des-m">
                  <b>
                    {OlfactiveFamilyBt}
                    :
                  </b>
                  {' '}
                  {this.props.familyBestSeller}
                </div>
                {
                  profile?.intensity && (
                    <div className="intensity">
                      {IntensityBt}
                      <div className="list-point">
                        {
                          _.map(_.range(5), d => (
                            <div className={classnames('point-i', parseInt(profile?.intensity, 10) >= (d + 1) ? 'active' : '')} />
                          ))
                        }
                      </div>
                    </div>
                  )
                }
                {
                  descriptionHtml()
                }
                <div className="line-temp" />
              </React.Fragment>
            ) : (
              <div className="content-tab">
                {scentCharacter()}
              </div>
            )
          }
          <div className="div-point-price">
            <div className="price">
              {generaCurrency(itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : '')}
            </div>
            <ReceiveGrams isPointProductDetail buttonBlocks={buttonBlocks} price={itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : 0} />
          </div>

          <div className="add-card-block">
            {renderQuanityV2}
            <ButtonCTV2
              disabled={!itemWaxSelect?.id || this.props.dataFAQ?.is_out_of_stock || itemWaxSelect?.is_out_of_stock}
              name={this.props.dataFAQ?.is_out_of_stock || itemWaxSelect?.is_out_of_stock ? outOfStockBt : addToCartBt}
              onClick={() => {
                if (type.name === 'hand_sanitizer') {
                  this.props.addToCartItem(itemWaxSelect);
                } else {
                  this.props.onClickAddtoCart();
                }
              }}
            />
          </div>
        </div>
      );
    }
    return (
      <div className="text-block-v2">
        <div className="name-product">
          {nameProductCustom || nameProduct}
          {/* {isPerfumeDiy && <img className="icon-product" src={icTitlePerfumeDiy} alt="icon" />} */}
        </div>

        {listButtonTab}

        <div className="content-tab">
          {
            subTabV3 === 0 ? (
              tabCreation()
            ) : (
              subTabV3 === 1 ? (
                tabDescription()
              ) : (
                scentCharacter()
              )
            )
          }
        </div>
        <div className="line-temp" />
        <div className="div-point-price">
          <div className="price">
            {generaCurrency(itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : '')}
          </div>
          <ReceiveGrams isPointProductDetail buttonBlocks={buttonBlocks} price={itemWaxSelect ? itemWaxSelect.price : itemCombo ? itemCombo.price : 0} />
        </div>
        <div id="id-add-card-block" className="add-card-block">
          {renderQuanityV2}
          <ButtonCTV2
            disabled={!itemWaxSelect?.id || this.props.dataFAQ?.is_out_of_stock || itemWaxSelect?.is_out_of_stock}
            name={this.props.dataFAQ?.is_out_of_stock || itemWaxSelect?.is_out_of_stock ? outOfStockBt : addToCartBt}
            onClick={() => {
              if (type.name === 'hand_sanitizer') {
                this.props.addToCartItem(itemWaxSelect);
              } else {
                this.props.onClickAddtoCart();
              }
            }}
          />
        </div>
      </div>
    );
  }
}

TextBlockV2.propTypes = {
  nameProduct: PropTypes.string.isRequired,
  itemCombo: PropTypes.shape().isRequired,
  cmsCommon: PropTypes.arrayOf().isRequired,
  itemBottle: PropTypes.shape().isRequired,
  quantity: PropTypes.number.isRequired,
  onChangeQuantity: PropTypes.func.isRequired,
  onClickAddtoCart: PropTypes.func.isRequired,
  combos: PropTypes.arrayOf().isRequired,
  handleChangeNameBottle: PropTypes.func.isRequired,
  toggleCustom: PropTypes.func.isRequired,
  propsState: PropTypes.shape().isRequired,
  type: PropTypes.string.isRequired,
  isWaxPerfume: PropTypes.bool.isRequired,
  items: PropTypes.arrayOf().isRequired,
  addToCartItem: PropTypes.func.isRequired,
  updateWaxPerfume: PropTypes.func.isRequired,
  currentImg: PropTypes.string.isRequired,
  nameBottle: PropTypes.string.isRequired,
};

export default TextBlockV2;
