/* eslint-disable react/sort-comp */
// import '../../styles/style.scss';
import _ from 'lodash';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import React, { Component } from 'react';
import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import BlockFAQV2 from '../../componentsv2/blockFAQV2';
import {
  GET_ALL_PRODUCTS_BOTTLE, GET_ALL_PRODUCTS_BOTTLE_BUNDLE, GET_ALL_PRODUCTS_BOTTLE_CAR,
  GET_ALL_PRODUCTS_BOTTLE_DIY, GET_ALL_PRODUCTS_BURNER, GET_ALL_PRODUCTS_DIFFUSER, GET_ALL_PRODUCTS_HOLDER,
  GET_ALL_PRODUCTS_METAL_CAP, GET_ALL_PRODUCTS_MINI_CANDLE_BOX, GET_ALL_PRODUCTS_SCENT, GET_ALL_SCENT_NOTES,
  GET_COMBO_URL, GET_FAQ_QUESTION, GET_PRICES_PRODUCTS, GET_PRODUCT_TYPE_LINK, GET_PRODUCT_URL, GET_SCENT_LIB,
} from '../../config';
import { addProductBasket, createBasketGuest, updateProductBasket } from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import updateIngredientsData from '../../Redux/Actions/ingredients';
import loadingPage from '../../Redux/Actions/loading';
import addScentsToStore, {
  addScentNotesToStore, addScentsCandleToStore, addScentsDiyToStore,
  addScentsDualCrayonsToStore, addScentsHomeToStore, addScentsMiniCandleToStore,
  addScentsOilBurnerToStore, addScentsReedDifffUserToStore, addScentsWaxToStore,
} from '../../Redux/Actions/scents';
import {
  fetchCMSHomepage, generateHreflang, generateUrlWeb, getLinkFromButtonBlock,
  getNameFromButtonBlock, googleAnalitycs, gotoShopHome, isNumeric,
  removeLinkHreflang, scrollTop, setPrerenderReady, trackGTMViewItem,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { fetchScentLib } from '../../Redux/Helpers/fetchAPI';
import { toastrError } from '../../Redux/Helpers/notification';
import {
  generateScriptFAQ, generateScriptProductDetail, removeScriptFAQ, removeScriptProductDetail,
} from '../../Utils/addScriptSchema';
import FooterV2 from '../../views2/footer';
import ProductDetail from './productDetail';
import Reviews from './reviews';
import ReviewsV3 from './reviewsV3';


class ProductB2C extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datas: [],
      data: {},
      match: this.props.match,
      location: this.props.location,
      reviewsData: {
        product: undefined,
        title: undefined,
        comment: undefined,
        rating: 1,
        ingredientDetail: {},
      },
      type: undefined,
      bottlePrice: [],
      isCreateProduct: false,
      dataFAQ: {},
      titleHeader: {},
    };
    this.refProductDetail = React.createRef();
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/products');

    const { match } = this.props;
    const values = queryString.parse(this.props.location.search);
    this.fetchData(match, values);
    this.getAllIngredients();
    if (this.props.scentNotes?.length > 0) {
      this.setState({ scentNotes: this.props.scentNotes });
    } else {
      this.fetchScentNotes();
    }
  }


  componentDidUpdate = () => {
    const { match, location, isUpdateUrl } = this.state;
    if (isUpdateUrl) {
      this.state.isUpdateUrl = false;
      const values = queryString.parse(location.search);
      this.fetchData(match, values);
    }
  }

  componentWillUnmount() {
    removeLinkHreflang();
    removeScriptFAQ();
    removeScriptProductDetail();
  }

  getAllIngredients = async () => {
    const { ingredients } = this.props;
    if (!ingredients || ingredients.length === 0) {
      const dataScentLib = await fetchScentLib();
      this.props.updateIngredientsData(dataScentLib);
    }
  }

  onChangeProduct = (data) => {
    const { reviewsData } = this.state;
    reviewsData.product = data.id;
    this.setState({ data, reviewsData: JSON.parse(JSON.stringify(reviewsData)) });
  }

  fetchScentNotes = () => {
    const options = {
      url: GET_ALL_SCENT_NOTES,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        this.props.addScentNotesToStore(result);
        this.setState({ scentNotes: result });
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err.message);
    });
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { match, location } = nextProps;
    if (match !== prevState.match || location !== prevState.location) {
      _.assign(objectReturn, { match, location, isUpdateUrl: true });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  arraymove = (arr, fromIndex, toIndex) => {
    const element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
  }

  generaImageForScent = data => _.map(data, x => ({ image: x, type: null }))

  fetchHandSanitizer = () => {
    const options = {
      url: GET_PRODUCT_TYPE_LINK.replace('{type}', 'hand_sanitizer'),
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchBottle = () => {
    const options = {
      url: GET_ALL_PRODUCTS_BOTTLE,
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchBottleCar = () => {
    const options = {
      url: GET_ALL_PRODUCTS_BOTTLE_CAR,
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchBottleDiy = () => {
    const options = {
      url: GET_ALL_PRODUCTS_BOTTLE_DIY,
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchDiffUser = () => {
    const options = {
      url: GET_ALL_PRODUCTS_DIFFUSER,
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchBottleBundle = () => {
    const options = {
      url: GET_ALL_PRODUCTS_BOTTLE_BUNDLE,
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchHolder = () => {
    const options = {
      url: GET_ALL_PRODUCTS_HOLDER,
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchBurner = () => {
    const options = {
      url: GET_ALL_PRODUCTS_BURNER,
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchMiniCandleBox = () => {
    const options = {
      url: GET_ALL_PRODUCTS_MINI_CANDLE_BOX,
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchMetaCap = () => {
    const options = {
      url: GET_ALL_PRODUCTS_METAL_CAP,
      method: 'GET',
    };
    return fetchClient(options);
  }

  getPrice = (type) => {
    try {
      const option = {
        url: GET_PRICES_PRODUCTS.replace('{type}', type),
        method: 'GET',
      };
      return fetchClient(option);
    } catch (error) {
      toastrError('error', error.message);
    }
  }

  fetchDataFAQ = async (type) => {
    const newType = type === 'home_scents_premade' ? 'home_scents' : type;
    const option = {
      url: GET_FAQ_QUESTION.replace('{type}', newType),
      method: 'GET',
    };
    return fetchClient(option);
  }

  sortScents = (data, type) => {
    const { sortId } = this.props.location?.state || {};
    if (sortId && sortId.length > 1) {
      const cloneCombo = _.cloneDeep(data?.combo);
      const newCombo = _.filter(cloneCombo, x => x.product?.type !== 'Scent');
      _.forEach(sortId, (d) => {
        if (['dual_candles', 'dual_crayons', 'oil_burner'].includes(type)) {
          const ele = _.find(cloneCombo, x => x.product?.combo && (x.product?.combo[0]?.product?.id === d));
          newCombo.push(ele);
        } else {
          const ele = _.find(cloneCombo, x => x.product?.id === d);
          newCombo.push(ele);
        }
      });
      _.assign(data, { combo: newCombo });
    }
  };

  fetchData = async (match, values) => {
    const { cms } = this.props;
    const { reviewsData } = this.state;
    let cmsProduct = _.find(cms, x => x.title === 'Product');
    if (!cmsProduct) {
      cmsProduct = await fetchCMSHomepage('product');
      this.props.addCmsRedux(cmsProduct);
    }
    const {
      id1: idProduct1, id2: idProduct2, type: typeParams, id: idParamas, item: itemParams, tags,
    } = match.params;
    const id1 = typeParams && isNumeric(typeParams) ? typeParams : undefined;
    const id2 = idParamas && isNumeric(idParamas) ? idParamas : undefined;
    const { type: typeValue, id: idValue, item: itemValue } = values;
    const type = (typeParams ? typeParams.replace('-', '_') : typeParams) || typeValue;
    const id = idParamas || idValue;
    const item = itemParams || itemValue;
    let itemHandlerSanitizer = {};
    const tagsFormat = tags ? tags.replace('-', '_') : '';

    if (tagsFormat) {
      this.updateReviewData({});
      this.props.loadingPage(true);
      if (tagsFormat === 'wax') {
        Promise.all([this.getPrice('Wax_Perfume'), this.fetchDataFAQ('Wax_Perfume')]).then((results) => {
          this.props.loadingPage(false);
          const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
          const datas = {
            combo: [{
              product: {
                short_description: getNameFromButtonBlock(buttonBlocks, 'des_wax'),
                description: getNameFromButtonBlock(buttonBlocks, 'des_wax'),
                // images: [],
                images: results[1].images,
                type: 'Bottle',
              },
            }],
            name: getNameFromButtonBlock(buttonBlocks, 'Solid Perfume'),
            items: [{
              price: _.find(results[0], x => !x.apply_to_sample)?.price,
            }],
            type: {
              name: 'Wax_Perfume',
            },
          };
          this.setState({
            dataFAQ: results[1],
            datas: [datas],
            data: datas,
            isCreateProduct: true,
            type: datas.type.name,
            reviewsData: {
              product: undefined,
              title: undefined,
              comment: undefined,
              rating: 1,
              ingredientDetail: {},
            },
            isBestSeller: false,
          });
          // Add script schema FAQ
          generateScriptFAQ(results[1]?.faqs);
        }).catch((err) => {
          toastrError(err);
          this.props.loadingPage(false);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      } else if (tagsFormat === 'hand_sanitizer') {
        Promise.all([this.fetchHandSanitizer()]).then((results) => {
          this.props.history.push(generateUrlWeb(`/product/hand-sanitizer/${results[0][0].product}/${results[0][0].id}`));
        }).catch((err) => {
          toastrError(err);
          this.props.loadingPage(false);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      } else if (tagsFormat === 'home_scents') {
        this.props.history.push(generateUrlWeb('/all-products'));
        // Promise.all([this.fetchBottle(), this.getPrice('home_scents'), this.fetchDataFAQ('home_scents')]).then((results) => {
        //   this.props.loadingPage(false);
        //   const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
        //   if (results && results.length > 1) {
        //     results[0][0].description = results[0][0].short_description;
        //     // results[0][0].images = results[0][0].images.concat(results[2].images);
        //     results[0][0].images = results[2].images;
        //     const datas = {
        //       combo: [{
        //         product: results[0][0],
        //       }],
        //       name: getNameFromButtonBlock(buttonBlocks, 'Home Scents'),
        //       items: [{
        //         price: results[1][0]?.price,
        //         isHomeScent: true,
        //       }],
        //       type: {
        //         name: 'home_scents',
        //       },
        //       videos: results[0][0]?.videos || undefined,
        //     };
        //     this.setState({
        //       dataFAQ: results[2],
        //       datas: [datas],
        //       data: datas,
        //       isCreateProduct: true,
        //       type: datas.type.name,
        //       reviewsData: {
        //         product: undefined,
        //         title: undefined,
        //         comment: undefined,
        //         rating: 1,
        //         ingredientDetail: {},
        //       },
        //       isBestSeller: false,
        //     });
        //     // Add script schema FAQ
        //     generateScriptFAQ(results[2]?.faqs);
        //   } else {
        //     throw new Error();
        //   }
        // }).catch((err) => {
        //   toastrError(err);
        //   this.props.loadingPage(false);
        //   // this.props.history.push(generateUrlWeb('/'));
        //   gotoShopHome();
        // });
      } else if (tagsFormat === 'mini_candles') {
        Promise.all([this.fetchMiniCandleBox(), this.getPrice('mini_candles'), this.fetchDataFAQ('mini_candles')]).then((results) => {
          this.props.loadingPage(false);
          results[0][0].type = 'Bottle';
          results[0][0].short_description = results[0][0].description;
          results[0][0].images = results[2].images;
          // results[0][0].images = results[0][0].images.concat(results[2].images);
          const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
          const datas = {
            combo: [{
              product: results[0][0],
            }],
            name: getNameFromButtonBlock(buttonBlocks, 'Mini Candle'),
            items: [{
              price: results[1][0]?.price,
            }],
            type: {
              name: 'mini_candles',
            },
            videos: results[0][0]?.videos || undefined,
          };
          this.setState({
            dataFAQ: results[2],
            datas: [datas],
            data: datas,
            isCreateProduct: true,
            type: datas.type.name,
            reviewsData: {
              product: undefined,
              title: undefined,
              comment: undefined,
              rating: 1,
              ingredientDetail: {},
            },
            isBestSeller: false,
          });
          // Add script schema FAQ
          generateScriptFAQ(results[2]?.faqs);
        }).catch((err) => {
          toastrError(err);
          this.props.loadingPage(false);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      } else if (tagsFormat === 'oil_burner') {
        Promise.all([this.fetchBurner(), this.getPrice('oil_burner'), this.fetchDataFAQ('oil_burner')]).then((results) => {
          this.props.loadingPage(false);
          results[0][0].type = 'Bottle';
          results[0][0].short_description = results[0][0].description;
          // results[0][0].images = results[0][0].images.concat(results[2].images);
          results[0][0].images = results[2].images;
          const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
          const datas = {
            combo: [{
              product: results[0][0],
            }],
            name: getNameFromButtonBlock(buttonBlocks, 'Oil Burner'),
            items: [{
              price: results[1][0]?.price,
            }],
            type: {
              name: 'oil_burner',
            },
            videos: results[0][0]?.videos || undefined,
          };
          this.setState({
            dataFAQ: results[2],
            datas: [datas],
            data: datas,
            isCreateProduct: true,
            type: datas.type.name,
            reviewsData: {
              product: undefined,
              title: undefined,
              comment: undefined,
              rating: 1,
              ingredientDetail: {},
            },
            isBestSeller: false,
          });
          // Add script schema FAQ
          generateScriptFAQ(results[2]?.faqs);
        }).catch((err) => {
          toastrError(err);
          this.props.loadingPage(false);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      } else if (tagsFormat === 'dual_candles') {
        Promise.all([this.fetchHolder(), this.getPrice('dual_candles'), this.fetchDataFAQ('dual_candles')]).then((results) => {
          this.props.loadingPage(false);
          results[0][0].type = 'Bottle';
          results[0][0].short_description = results[0][0]?.description;
          // results[0][0].product.short_description = results[0][0].product.description;
          // results[0][0].images = results[0][0].images.concat(results[2].images);
          results[0][0].images = results[2].images;
          const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
          const datas = {
            combo: [{
              product: results[0][0],
            }],
            name: getNameFromButtonBlock(buttonBlocks, 'Scented Dual Candle'),
            items: [{
              price: results[1][0]?.price,
            }],
            type: {
              name: 'dual_candles',
            },
            videos: results[0][0]?.videos || undefined,
          };
          this.setState({
            dataFAQ: results[2],
            datas: [datas],
            data: datas,
            isCreateProduct: true,
            type: datas.type.name,
            reviewsData: {
              product: undefined,
              title: undefined,
              comment: undefined,
              rating: 1,
              ingredientDetail: {},
            },
            isBestSeller: false,
          });
          // Add script schema FAQ
          generateScriptFAQ(results[2]?.faqs);
        }).catch((err) => {
          toastrError(err);
          this.props.loadingPage(false);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      } else if (tagsFormat === 'single_candle') {
        Promise.all([this.fetchHolder(), this.getPrice('single_candle'), this.fetchDataFAQ('single_candle')]).then((results) => {
          this.props.loadingPage(false);
          results[0][0].type = 'Bottle';
          results[0][0].images = results[2].images;
          results[0][0].short_description = results[0][0]?.description;
          const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
          const datas = {
            combo: [{
              product: results[0][0],
            }],
            name: getNameFromButtonBlock(buttonBlocks, 'Candle Creation'),
            items: [{
              price: results[1][0]?.price,
            }],
            type: {
              name: 'single_candle',
            },
            videos: results[0][0]?.videos || undefined,
          };
          this.setState({
            dataFAQ: results[2],
            datas: [datas],
            data: datas,
            isCreateProduct: true,
            type: datas.type.name,
            reviewsData: {
              product: undefined,
              title: undefined,
              comment: undefined,
              rating: 1,
              ingredientDetail: {},
            },
            isBestSeller: false,
          });
          // Add script schema FAQ
          generateScriptFAQ(results[2]?.faqs);
        }).catch((err) => {
          toastrError(err);
          this.props.loadingPage(false);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      } else if (tagsFormat === 'creation_perfume' || tagsFormat === 'miniature_perfume') {
        Promise.all([this.fetchBottle(), this.getPrice('perfume'), this.fetchDataFAQ('Perfume')]).then((results) => {
          this.props.loadingPage(false);
          // results[0][0].images = results[0][0].images.concat(results[2].images);
          results[0][0].images = results[2].images.concat(_.filter(results[0][0].images, x => x.type === 'suggestion'));
          const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
          if (results && results.length > 1) {
            const datas = {
              combo: [{
                product: results[0][0],
              }],
              name: getNameFromButtonBlock(buttonBlocks, 'Perfume Creation'),
              items: [{
                price: tagsFormat === 'miniature_perfume' ? _.find(results[1], x => x.apply_to_sample)?.price : _.find(results[1], x => !x.apply_to_sample)?.price,
                isSample: tagsFormat === 'miniature_perfume',
              }],
              type: {
                name: 'Perfume',
              },
              videos: results[0][0]?.videos || undefined,
            };
            this.setState({
              dataFAQ: results[2],
              datas: [datas],
              data: datas,
              bottlePrice: results[1],
              isCreateProduct: true,
              type: datas.type.name,
              reviewsData: {
                product: undefined,
                title: undefined,
                comment: undefined,
                rating: 1,
                ingredientDetail: {},
              },
              isBestSeller: false,
            });
            // Add script schema FAQ
            generateScriptFAQ(results[2]?.faqs);
          } else {
            throw new Error();
          }
        }).catch((err) => {
          this.props.loadingPage(false);
          toastrError(err);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      } else if (tagsFormat === 'car_diffuser') {
        Promise.all([this.fetchBottleCar(), this.getPrice('car_diffuser'), this.fetchDataFAQ('car_diffuser')]).then((results) => {
          this.props.loadingPage(false);
          results[0][0].type = 'Bottle';
          results[0][0].images = results[2].images;
          results[0][0].description = results[2].description;
          // results[0][0].images = results[0][0].images.concat(results[2].images);
          const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
          if (results && results.length > 1) {
            const datas = {
              combo: [{
                product: results[0][0],
              }],
              name: getNameFromButtonBlock(buttonBlocks, 'Car Diffuser Creation'),
              items: [{
                price: results[1][0]?.price,
              }],
              type: {
                name: 'car_diffuser',
              },
              videos: results[0][0]?.videos || undefined,
            };
            this.setState({
              dataFAQ: results[2],
              datas: [datas],
              data: datas,
              bottlePrice: results[1],
              isCreateProduct: true,
              type: datas.type.name,
              reviewsData: {
                product: undefined,
                title: undefined,
                comment: undefined,
                rating: 1,
                ingredientDetail: {},
              },
              isBestSeller: false,
            });
            // Add script schema FAQ
            generateScriptFAQ(results[2]?.faqs);
          } else {
            throw new Error();
          }
        }).catch((err) => {
          this.props.loadingPage(false);
          toastrError(err);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      } else if (tagsFormat === 'perfume_diy') {
        Promise.all([this.fetchBottleDiy(), this.getPrice('perfume_diy'), this.fetchDataFAQ('perfume_diy')]).then((results) => {
          this.props.loadingPage(false);
          results[0][0].type = 'Bottle';
          results[0][0].images = results[2].images;
          // results[0][0].images = results[0][0].images.concat(results[2].images);
          const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
          if (results && results.length > 1) {
            const datas = {
              combo: [{
                product: results[0][0],
              }],
              name: getNameFromButtonBlock(buttonBlocks, 'Perfume DIY Creation'),
              items: [{
                price: results[1][0]?.price,
              }],
              type: {
                name: 'perfume_diy',
              },
              videos: results[0][0]?.videos || undefined,
            };
            this.setState({
              dataFAQ: results[2],
              datas: [datas],
              data: datas,
              bottlePrice: results[1],
              isCreateProduct: true,
              type: datas.type.name,
              reviewsData: {
                product: undefined,
                title: undefined,
                comment: undefined,
                rating: 1,
                ingredientDetail: {},
              },
              isBestSeller: false,
            });
            // Add script schema FAQ
            generateScriptFAQ(results[2]?.faqs);
          } else {
            throw new Error();
          }
        }).catch((err) => {
          this.props.loadingPage(false);
          toastrError(err);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      } else if (tagsFormat === 'dual_crayons') {
        Promise.all([this.fetchMetaCap(), this.getPrice('dual_crayons'), this.getPrice('perfume'), this.fetchDataFAQ('dual_crayons')]).then((results) => {
          this.props.loadingPage(false);
          results[0][0].type = 'Bottle';
          results[0][0].images = results[3].images;
          // results[0][0].images = results[0][0].images.concat(results[3].images);
          const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
          const datas = {
            combo: [{
              product: results[0][0],
            }],
            name: getNameFromButtonBlock(buttonBlocks, 'Dual Crayon Creation'),
            items: [{
              price: results[1][0]?.price,
            }],
            type: {
              name: 'dual_crayons',
            },
            videos: results[0][0]?.videos || undefined,
          };
          this.setState({
            dataFAQ: results[3],
            datas: [datas],
            bottlePrice: results[2],
            data: datas,
            isCreateProduct: true,
            type: datas.type.name,
            reviewsData: {
              product: undefined,
              title: undefined,
              comment: undefined,
              rating: 1,
              ingredientDetail: {},
            },
            isBestSeller: false,
          });
          // Add script schema FAQ
          generateScriptFAQ(results[3]?.faqs);
        }).catch((err) => {
          toastrError(err);
          this.props.loadingPage(false);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      } else if (tagsFormat === 'bundle_creation') {
        Promise.all([this.fetchBottleBundle(), this.getPrice('bundle_creation'), this.fetchBottle(), this.fetchDataFAQ('bundle_creation')]).then((results) => {
          this.props.loadingPage(false);
          const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
          if (results && results.length > 1) {
            results[0][0].type = 'Bottle';
            // results[0][0].images = results[0][0].images.concat(_.filter(results[2][0].images, x => x.type === 'suggestion'));
            results[0][0].images = results[3].images.concat(_.filter(results[2][0].images, x => x.type === 'suggestion'));
            const datas = {
              combo: [{
                product: results[0][0],
              }],
              name: getNameFromButtonBlock(buttonBlocks, 'Bundle Creation'),
              items: [{
                price: _.find(results[1], x => !x.apply_to_sample)?.price,
              }],
              type: {
                name: 'bundle_creation',
              },
              videos: results[0][0]?.videos || undefined,
            };
            this.setState({
              dataFAQ: results[3],
              datas: [datas],
              data: datas,
              bottlePrice: results[1],
              isCreateProduct: true,
              type: datas.type.name,
              reviewsData: {
                product: undefined,
                title: undefined,
                comment: undefined,
                rating: 1,
                ingredientDetail: {},
              },
              isBestSeller: false,
            });
            // Add script schema FAQ
            generateScriptFAQ(results[3]?.faqs);
          } else {
            throw new Error();
          }
        }).catch((err) => {
          this.props.loadingPage(false);
          toastrError(err);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      } else if (tagsFormat === 'reed_diffuser') {
        // this.fetchAllScentsMmo();
        Promise.all([this.fetchDiffUser(), this.getPrice('reed_diffuser'), this.fetchDataFAQ('reed_diffuser')]).then((results) => {
          this.props.loadingPage(false);
          results[0][0].type = 'Bottle';
          results[0][0].images = results[2].images;
          // results[0][0].images = results[0][0].images.concat(results[2].images);
          const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
          if (results && results.length > 1) {
            const datas = {
              combo: [{
                product: results[0][0],
              }],
              name: getNameFromButtonBlock(buttonBlocks, 'Reed Diffuser Creation'),
              items: [{
                price: _.find(results[1], x => !x.apply_to_sample)?.price,
              }],
              type: {
                name: 'reed_diffuser',
              },
              videos: results[0][0]?.videos || undefined,
            };
            this.setState({
              dataFAQ: results[2],
              datas: [datas],
              data: datas,
              bottlePrice: results[1],
              isCreateProduct: true,
              type: datas.type.name,
              reviewsData: {
                product: undefined,
                title: undefined,
                comment: undefined,
                rating: 1,
                ingredientDetail: {},
              },
              isBestSeller: false,
            });
            // Add script schema FAQ
            generateScriptFAQ(results[2]?.faqs);
          } else {
            throw new Error();
          }
        }).catch((err) => {
          this.props.loadingPage(false);
          toastrError(err);
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        });
      }
      return;
    }
    if (id1 && id2 && isNumeric(id1) && isNumeric(id2)) {
      this.props.loadingPage(true);
      this.fetchCombo(id1, id2).then((result) => {
        this.props.history.push(generateUrlWeb(`/product/perfume_diy/${result.id}`));
      }).catch((err) => {
        this.props.loadingPage(false);
        toastrError(err);
        gotoShopHome();
      });
      return;
    }

    if (type && isNumeric(id)) {
      this.props.loadingPage(true);
      const buttonBlocks = _.filter(cmsProduct ? cmsProduct.body : [], x => x.type === 'button_block');
      let bottlePerfume;
      let holder;
      let dataFAQ;
      let miniHolder;

      this.fetchProduct(id).then(async (resultT) => {
        let typeFromProduct = type;
        if (type === 'best_seller') {
          typeFromProduct = resultT?.type?.name;
        }
        if (typeFromProduct === 'bundle_creation') {
          bottlePerfume = await this.fetchBottle();
        }
        if (typeFromProduct === 'single_candle') {
          holder = await this.fetchHolder();
        }
        if (typeFromProduct === 'mini_candles') {
          miniHolder = await this.fetchMiniCandleBox();
        }

        // get data FAQ
        if (typeFromProduct === 'Perfume' || typeFromProduct === 'Scent') {
          dataFAQ = await this.fetchDataFAQ('Perfume');
        } else if (typeFromProduct === 'wax') {
          dataFAQ = await this.fetchDataFAQ('Wax_Perfume');
        } else {
          dataFAQ = await this.fetchDataFAQ(typeFromProduct);
        }

        this.props.loadingPage(false);
        const result = _.cloneDeep(resultT);
        if (result && !result.isError) {
          // tracking gtm
          trackGTMViewItem(result);

          result.shortTitle = result.name;
          reviewsData.product = result.id;
          const productType = result.type.parent;

          const titleHeader = {
            productType: _.isNull(productType) ? result.type.name : productType.name,
            ingridient: result.name.replace('-', '+'),
          };
          this.setState({
            ...this.state,
            titleHeader,
          });

          const isHomeScentPremade = typeFromProduct === 'home_scents' && result.is_featured;
          if (isHomeScentPremade) {
            result.type.name = 'home_scents_premade';
          }

          if (typeFromProduct === 'Perfume' || typeFromProduct === 'Scent') {
            this.getPrice('perfume').then((bottlePrice) => {
              this.setState({ bottlePrice });
            });
            if (typeFromProduct === 'Scent') {
              const bottle = _.find(result.combo, x => x.product.type === 'Bottle');
              const product = _.find(result.combo, x => x.product.type === 'Scent');
              const temps = _.cloneDeep(result.samples);
              _.assign(temps[0], { isSample: true });
              result.items = temps;
              if (bottle) {
                // bottle.product.images = result.images;
                bottle.product.images = dataFAQ.images;
                bottle.product.description = product.product.description;
              } else {
                const bottleT = {
                  product: {
                    // images: _.concat(result.images, this.generaImageForScent(result.items[0].images)),
                    images: dataFAQ.images.concat(this.generaImageForScent(result.items[0].images)),
                    description: result.description,
                    type: 'Bottle',
                  },
                };
                result.combo = [
                  {
                    name: result.name,
                    id: result.items[0].id,
                    description: result.items[0].description,
                    product: {
                      name: result.name,
                      id: result.items[0].id,
                      images: result.images,
                      type: typeFromProduct,
                      ingredient: product && product.product ? product.product.ingredient : undefined,
                      profile: product && product.product ? product.product.profile : undefined,
                    },
                  },
                ];
                result.combo.push(bottleT);
              }
            } else {
              // const product = _.filter(result.combo, x => x.product.type === 'Scent');
              // _.forEach(product, (d) => {
              //   d.product.short_description = d.product.description;
              // });

              const bottle = _.find(result.combo, x => x.product.type === 'Bottle');
              if (bottle) {
                // bottle.product.images = _.concat(result.images, bottle.product.images);
                bottle.product.images = dataFAQ.images.concat(result.images).concat(_.filter(bottle.product.images, x => x.type === 'suggestion'));
              } else {
                const bottleT = {
                  product: {
                    images: result.images,
                    description: result.description,
                    type: 'Bottle',
                  },
                };
                result.combo.push(bottleT);
              }
            }
          } else if (typeFromProduct === 'perfume_diy') {
            this.getPrice('perfume_diy').then((bottlePrice) => {
              this.setState({ bottlePrice });
            });
            _.forEach(result.combo, (d) => {
              if (d.product?.type === 'scent_diy') {
                _.assign(d.product, d.product.combo[0]?.product);
              } else if (d.product?.type === 'bottle_diy') {
                d.product.type = 'Bottle';
              }
            });
            const bottle = _.find(result.combo, x => x.product.type === 'Bottle');
            if (bottle) {
              // bottle.product.images = _.concat(result.images, bottle.product.images);
              bottle.product.images = _.concat(dataFAQ.images, result.images);
            }
          } else if (typeFromProduct === 'car_diffuser') {
            this.getPrice('car_diffuser').then((bottlePrice) => {
              this.setState({ bottlePrice });
            });
            _.forEach(result.combo, (d) => {
              if (d.product?.type === 'car_diffuser_box') {
                d.product.type = 'Bottle';
                d.product.description = dataFAQ.description;
              } else if (d.product.type === 'scent_diy') {
                // description of car_diffuser from field "description"
                d.product = d.product.combo[0].product;
                d.product.short_description = d.product.description;
              }
            });
            const bottle = _.find(result.combo, x => x.product.type === 'Bottle');
            if (bottle) {
              bottle.product.images = _.concat(dataFAQ.images, result.images);
            }
          } else if (typeFromProduct === 'reed_diffuser') {
            _.forEach(result.combo, (d) => {
              if (d.product.type === 'diffuser') {
                d.product.type = 'Bottle';
                d.product.images = dataFAQ?.images;
              } else if (d.product.type === 'reed_oil') {
                // description of reed_diffuser from field "description"
                d.product = d.product.combo[0].product;
                console.log('d.product', d.product);
                d.product.short_description = d.product.description;
              }
            });
          } else if (typeFromProduct === 'wax' || typeFromProduct === 'hand_sanitizer' || isHomeScentPremade) {
            if (isHomeScentPremade) {
              const index = _.findIndex(result.items, x => parseInt(x.id, 10) === parseInt(item, 10));
              if (index > -1) {
                this.arraymove(result.items, index, 0);
              }
            }
            if (typeFromProduct === 'wax') {
              _.remove(result.items, x => x.is_sample);
            }
            if (typeFromProduct === 'hand_sanitizer') {
              result.items = _.orderBy(result.items, ['price'], ['asc']);
              itemHandlerSanitizer = _.find(result.items, x => parseInt(x.id, 10) === parseInt(item, 10));
            }
            if (result.combo && result.combo.length > 0) {
              result.combo.push({
                product: {
                  short_description: typeFromProduct === 'wax' ? getNameFromButtonBlock(buttonBlocks, 'des_wax') : '',
                  description: result.description,
                  // images: result.images,
                  images: dataFAQ?.images,
                  type: 'Bottle',
                },
              });
            } else {
              result.combo = [{
                name: result.name,
                id: result.items[0].id,
                product: {
                  name: result.name,
                  id: result.items[0].id,
                  images: result.images,
                  type: 'Scent',
                },
              },
              {
                product: {
                  description: result.description,
                  // images: result.images,
                  images: dataFAQ?.images,
                  type: 'Bottle',
                },
              },
              ];
            }
          } else if (typeFromProduct === 'elixir') {
            const bottle = _.find(result.combo, x => x.product.type === 'Bottle');
            if (bottle) {
              // bottle.product.images = _.concat(bottle.product.images, _.filter(result.images, x => x.type === 'suggestion'));
              bottle.product.images = dataFAQ?.images.concat(_.filter(result.images, x => x.type === 'suggestion'));
              bottle.product.description = result.description;
            }
            _.forEach(result.combo, (d) => {
              if (d.product.type === 'Scent') {
                // description of elixir from field "description"
                d.product.short_description = d.product.description;
              }
            });
            this.setState({ datas: [result], data: result });
          } else if (typeFromProduct === 'kit' || typeFromProduct === 'creation' || typeFromProduct === 'bundle' || typeFromProduct === 'Scent' || typeFromProduct === 'discovery_box' || typeFromProduct === 'holder' || typeFromProduct === 'mask_sanitizer' || typeFromProduct === 'toilet_dropper') {
            const bottle = _.find(result.combo, x => x.product.type === 'Bottle');
            const product = _.find(result.combo, x => x.product.type === 'Scent');
            if (bottle) {
              // bottle.product.images = type === 'creation' ? _.concat(result.images, _.filter(bottle.product.images, x => x.type === 'suggestion')) : result.images;
              bottle.product.images = dataFAQ?.images;
              bottle.product.description = typeFromProduct === 'kit' || typeFromProduct === 'creation' ? result.description : product.product.description;
              if (typeFromProduct === 'creation') {
                bottle.product.images = bottle.product.images.concat(result.images);
              }
            } else {
              if (typeFromProduct === 'bundle' && result.combo.length > 0) {
                _.forEach(result.combo, (d) => {
                  // d.product.images = [];
                  d.description = d.product.description;
                });
              }
              const imagesT = typeFromProduct === 'holder' || typeFromProduct === 'Scent' || typeFromProduct === 'discovery_box' || typeFromProduct === 'mask_sanitizer' || typeFromProduct === 'toilet_dropper' ? _.concat(result.images, this.generaImageForScent(result.items[0].images)) : result.images;
              const bottleT = {
                product: {
                  // images: imagesT,
                  images: typeFromProduct === 'creation' ? dataFAQ?.images.concat(result.images) : dataFAQ?.images,
                  description: typeFromProduct === 'holder' || typeFromProduct === 'kit' || typeFromProduct === 'creation' || typeFromProduct === 'Scent' || typeFromProduct === 'bundle' || typeFromProduct === 'discovery_box' || typeFromProduct === 'mask_sanitizer' || typeFromProduct === 'toilet_dropper' ? result.description : product ? product.product.description : '',
                  type: 'Bottle',
                },
              };
              if ((typeFromProduct === 'holder' || typeFromProduct === 'Scent' || typeFromProduct === 'discovery_box' || typeFromProduct === 'mask_sanitizer' || typeFromProduct === 'toilet_dropper') && !result.combo) {
                result.combo = [
                  {
                    name: result.name,
                    id: result.items[0].id,
                    description: result.items[0].description,
                    product: {
                      name: result.name,
                      id: result.items[0].id,
                      images: result.images,
                      type: typeFromProduct,
                      ingredient: product && product.product ? product.product.ingredient : undefined,
                      profile: product && product.product ? product.product.profile : undefined,
                    },
                  },
                ];
              }
              result.combo.push(bottleT);
            }
          } else if (typeFromProduct === 'mini_candles') {
            _.forEach(result.combo, (d) => {
              if (['mini_candle_box', 'mini_candle_holder'].includes(d.product.type)) {
                d.product.type = 'Bottle';
                d.product.short_description = d.product.description;
                d.product.images = dataFAQ?.images;
                // d.product.images = (dataFAQ?.images || []).concat(d.product.images);
              }
              if (d.product.type === 'mini_candle') {
                d.product.type = 'Scent';
                d.product.images = d.product.images.concat(d.product.combo[0].product.images);
                d.product.id = d.product.combo[0].product.id;
                d.description = d.product.combo[0].description;
                d.product.short_description = d.product.combo[0].product.description;
                d.product.ingredient = d.product.combo[0].product.ingredient;
              }
            });
            if (!_.find(result.combo, x => x.product.type === 'Bottle')) {
              const bottleT = {
                product: {
                  short_description: miniHolder[0].description,
                  images: dataFAQ?.images,
                  description: miniHolder[0].description,
                  type: 'Bottle',
                },
              };
              result.combo.push(bottleT);
            }
          } else if (typeFromProduct === 'oil_burner') {
            _.forEach(result.combo, (d) => {
              if (d.product.type === 'burner') {
                d.product.type = 'Bottle';
                d.product.short_description = d.product.description;
                // d.product.images = (dataFAQ?.images || []).concat(d.product.images);
                d.product.images = dataFAQ?.images;
              }
              if (d.product.type === 'oil_dropper') {
                console.log('oil_dropper ==', d);
                d.product.type = 'Scent';
                d.product.images = d.product.images.concat(d.product.combo[0].product.images);
                d.product.short_description = d.product.combo[0].product.description;
                d.product.ingredient = d.product.combo[0].product.ingredient;
              }
            });
          } else if (typeFromProduct === 'dual_candles') {
            _.forEach(result.combo, (d) => {
              if (d.product.type === 'holder') {
                d.product.type = 'Bottle';
                d.product.short_description = d.product.description;
                // d.product.images = (dataFAQ?.images || []).concat(d.product.images);
                d.product.images = dataFAQ?.images;
              }
              if (d.product.type === 'single_candle') {
                d.product.type = 'Scent';
                d.product.short_description = d.product.combo[0].product.description;
                d.product.images = d.product.images.concat(d.product.combo[0].product.images);
                d.description = d.product.combo[0].description;
                d.product.ingredient = d.product.combo[0].product.ingredient;
              }
            });
          } else if (typeFromProduct === 'single_candle') {
            const imageScents = _.filter(result.images, d => d.type === 'main');
            _.forEach(imageScents, (d) => {
              d.type = null;
            });

            const bottleT = {
              product: {
                short_description: holder[0]?.description,
                images: imageScents.concat(holder[0]?.images),
                description: result.description,
                type: 'Bottle',
              },
            };
            // bottleT.product.images = (dataFAQ?.images || []).concat(bottleT.product.images);
            bottleT.product.images = dataFAQ?.images;
            const scents = _.find(result.combo, x => x.product.type === 'Scent');
            if (scents) {
              scents.product.short_description = scents.product.combo[0].product.description;
            }
            result.combo.push(bottleT);
          } else if (typeFromProduct === 'home_scents') {
            const bottle = _.find(result.combo, x => x.product.type === 'Bottle');
            if (bottle) {
              bottle.product.description = bottle.product.short_description;
              // bottle.product.images = (dataFAQ?.images || []).concat(bottle.product.images);
              bottle.product.images = dataFAQ?.images;
            }
          } else if (typeFromProduct === 'gift_bundle') {
            const bottleT = {
              product: {
                images: result.images,
                // images: (dataFAQ?.images || []).concat(result.images),
                // images: dataFAQ?.images,
                description: result.description,
                type: 'Bottle',
              },
            };
            result.combo.push(bottleT);
          } else if (typeFromProduct === 'dual_crayons') {
            this.getPrice('perfume').then((bottlePrice) => {
              this.setState({ bottlePrice });
            });
            _.forEach(result.combo, (d) => {
              if (d.product.type === 'metal_cap') {
                d.product.type = 'Bottle';
                // d.product.images = (dataFAQ?.images || []).concat(d.product.images);
                d.product.images = dataFAQ?.images;
              }
              if (d.product.type === 'crayon') {
                d.product.type = 'Scent';
                // d.product.short_description = d.product.description;
                d.name = d.product.combo[0].name;
                d.description = d.product.combo[0].description;
                d.product.images = d.product.images.concat(d.product.combo[0].product.images);
                d.product.short_description = d.product.combo[0].product.short_description;
                d.product.ingredient = d.product.combo[0].product.ingredient;
              }
            });
          } else if (typeFromProduct === 'bundle_creation') {
            this.getPrice('bundle_creation').then((bottlePrice) => {
              this.setState({ bottlePrice });
            });
            const bottle = _.find(result.combo, x => x.product.type === 'bottle_bundle_creation');
            const scents = _.filter(result.combo, x => x.product.type === 'Scent');
            bottle.product.images = bottle.product.images.concat(bottlePerfume[0].images);
            if (scents && scents.length > 1) {
              bottle.product.type = 'Bottle';
              bottle.type = 'Bottle';
              // bottle.product.images = _.concat(result.images, bottle.product.images);
              bottle.product.images = dataFAQ.images;
            } else {
              const product = _.find(result.combo, x => x.product.type === 'Scent');
              bottle.product.type = 'Bottle';
              bottle.type = 'Bottle';
              // bottle.product.images = _.concat(result.images, bottle.product.images);
              bottle.product.images = dataFAQ.images;
              result.combo = [
                {
                  name: result.name,
                  id: result.items[0].id,
                  product: {
                    short_description: product?.product?.short_description,
                    name: result.name,
                    id: product.product.id,
                    images: result.images.concat(product.product.images),
                    type: typeFromProduct,
                    ingredient: product && product.product ? product.product.ingredient : undefined,
                    profile: product && product.product ? product.product.profile : undefined,
                  },
                },
              ];
              // bottle.product.images = (dataFAQ?.images || []).concat(bottle.product.images);
              result.combo.push(bottle);
            }
          }
          // change name product
          if (typeFromProduct === 'holder') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Holder') });
          } else if (result.short_description) {
            _.assign(result, { displayName: result.short_description });
          } else if (typeFromProduct === 'wax') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Solid Perfume') });
          } else if (typeFromProduct === 'home_scents') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Home Scents') });
          } else if (typeFromProduct === 'dual_candles') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Scented Dual Candle') });
          } else if (typeFromProduct === 'mini_candles') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Mini Candle') });
          } else if (typeFromProduct === 'single_candle') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Candle Creation') });
          } else if (typeFromProduct === 'Perfume' || typeFromProduct === 'Scent') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Perfume Creation') });
          } else if (typeFromProduct === 'perfume_diy') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Perfume DIY Creation') });
          } else if (typeFromProduct === 'car_diffuser') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Car Diffuser Creation') });
          } else if (typeFromProduct === 'dual_crayons') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Dual Crayon Creation') });
          } else if (typeFromProduct === 'bundle_creation') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Bundle Creation') });
          } else if (typeFromProduct === 'reed_diffuser') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Reed Diffuser Creation') });
          } else if (typeFromProduct === 'oil_burner') {
            _.assign(result, { displayName: getNameFromButtonBlock(buttonBlocks, 'Oil Burner') });
          }
          for (let i = 0; i < result?.combo?.length; i += 1) {
            const d = result?.combo[i];
            if (d.product?.type === 'Scent') {
              if (d?.child_quantity === 2) {
                d.child_quantity = 1;
                result?.combo.push(d);
              }
            }
          }

          if (type === 'best_seller') {
            const bottle = _.find(result.combo, x => x.product.type === 'Bottle');
            if (bottle) {
              bottle.product.description = result.description;
              bottle.product.short_description = result.description;
              bottle.product.images = _.concat(result.images, bottle.product.images);
            } else {
              const bottleT = {
                product: {
                  images: result.images,
                  description: result.description,
                  short_description: result.description,
                  type: 'Bottle',
                },
              };
              result.combo.push(bottleT);
            }
          }
          console.log('result ==', result);
          // sort scent
          this.sortScents(result, typeFromProduct);
          // add schema product detail
          generateScriptProductDetail(result);
          this.setState({
            dataFAQ,
            datas: [result],
            data: result,
            type: result.type.name,
            reviewsData: _.cloneDeep(reviewsData),
            isCreateProduct: false,
            itemHandlerSanitizer,
            isBestSeller: result?.items[0]?.is_best_seller || type === 'creation' || (type === 'best_seller' && result?.items[0]?.is_featured),
          });

          // Add script schema FAQ
          generateScriptFAQ(dataFAQ?.faqs);
        } else {
          throw new Error();
        }
      }).catch((err) => {
        this.props.loadingPage(false);
        console.error(err);
        toastrError(err);
        // this.props.history.push('/');
      });
      return;
    }
    const { products } = values;
    if (products) {
      const pareProducts = JSON.parse(decodeURIComponent(products));
      const pending = [];
      _.forEach(pareProducts, (d) => {
        const { ids } = d;
        pending.push(this.fetchCombo(ids[0], ids[1]));
      });
      Promise.all(pending).then((results) => {
        for (let i = 0; i < results.length; i += 1) {
          results[i].shortTitle = pareProducts[i].name;
          const { combo } = results[i];
          const bottle = _.find(combo, x => x.product.type === 'Bottle');
          if (bottle) {
            bottle.product.images = bottle.product.images.concat(pareProducts[i].images);
            results[i].name = pareProducts[i].name ? pareProducts[i].name : results[i].name;
          }
        }
        reviewsData.product = results[0].id;
        this.setState({ datas: results, data: results[0], reviewsData: _.cloneDeep(reviewsData) });
      }).catch((err) => {
        console.error(err);
        toastrError(err);
        // this.props.history.push(generateUrlWeb('/'));
        gotoShopHome();
      });
    } else {
      console.error('products null');
      toastrError('products null');
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    }
  }

  fetchProduct = (id) => {
    const options = {
      url: GET_PRODUCT_URL.replace('{id}', id),
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchCombo = (id1, id2) => {
    const options = {
      url: GET_COMBO_URL.replace('{id1}', id1)
        .replace('{id2}', id2),
      method: 'GET',
    };
    return fetchClient(options);
  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  updateReviewData = (data) => {
    if (this.refProductDetail && this.refProductDetail.current) {
      this.refProductDetail.current.setDataReview(data);
    }
  }

  onChangeTypePerfume = (isRollOn) => {
    if (this.state.isCreateProduct) {
      if (isRollOn) {
        this.props.history.push(generateUrlWeb('/product/miniature_perfume'));
      } else {
        this.props.history.push(generateUrlWeb('/product/creation_perfume'));
      }
    } else {
      let newUrl;
      if (isRollOn) {
        newUrl = window.location.pathname.replace('Perfume', 'Scent');
      } else {
        newUrl = window.location.pathname.replace('Scent', 'Perfume');
      }
      this.props.history.push(generateUrlWeb(newUrl));
    }
  }

  render() {
    const {
      data, datas, reviewsData, type,
      bottlePrice,
    } = this.state;
    const { login, basket, match } = this.props;
    const values = queryString.parse(this.props.location.search);
    if (_.isEmpty(data)) {
      return (<div />);
    }
    const { tags, type: typeParams, id } = match.params;
    const tagsFormat = tags ? tags.replace('-', '_') : '';
    const { type: typeValue } = values;
    const typeTemp = (tagsFormat ? tagsFormat.replace('-', '_') : typeParams) || typeValue;

    const { is_reviewable: isReviewable } = data;
    const samples = data && data.samples ? data.samples : [];
    const priceSample = samples && samples.length > 0 ? samples[0]?.price : undefined;
    const recommendCms = _.find(this.props.cms, x => x.title === 'Product');
    const buttonBlocks = _.filter(recommendCms ? recommendCms.body : [], x => x.type === 'button_block');
    const videoBlocks = _.filter(recommendCms ? recommendCms.body : [], x => x.type === 'imagetxt_block');
    const headerAndParagraphBocks = _.filter(recommendCms ? recommendCms.body : [], x => x.type === 'header_and_paragraph_block');
    const ele = _.find(this.props.countries, x => x.code === auth.getCountry());
    let href = `https://maison21g.com/${ele?.code}/product`;
    let sHref = '/product';
    const typeT = typeTemp?.replace('-', '_');
    const titleSeo = getNameFromButtonBlock(buttonBlocks, `seo_${typeT}`);
    let desSeo = getLinkFromButtonBlock(buttonBlocks, `seo_${typeT}`);

    if (typeT === 'dual_crayons') {
      href = `https://maison21g.com/${ele?.code}/product/dual_crayons`;
      sHref = '/product/dual_crayons';
    } else if (typeT === 'discovery_box') {
      href = `https://maison21g.com/${ele?.code}/product/discovery_box/2106`;
      sHref = '/product/discovery_box/2106';
    } else if (typeT === 'mask_sanitizer') {
      href = `https://maison21g.com/${ele?.code}/product/mask_sanitizer/3790`;
      sHref = '/product/mask_sanitizer/3790';
    } else if (typeT === 'toilet_dropper') {
      href = `https://maison21g.com/${ele?.code}/product/toilet_dropper/3791`;
      sHref = '/product/toilet_dropper/3791';
    } else if (typeT === 'wax') {
      href = `https://maison21g.com/${ele?.code}/product/wax`;
      sHref = '/product/wax';
    } else if (typeT === 'single_candle') {
      href = `https://maison21g.com/${ele?.code}/product/single_candle`;
      sHref = '/product/single_candle';
    } else if (typeT === 'dual_candles') {
      href = `https://maison21g.com/${ele?.code}/product/dual_candles`;
      sHref = '/product/dual_candles';
    } else if (typeT === 'mini_candles') {
      href = `https://maison21g.com/${ele?.code}/product/mini_candles`;
      sHref = '/product/mini_candles';
    } else if (typeT === 'home_scents') {
      href = `https://maison21g.com/${ele?.code}/product/home_scents`;
      sHref = '/product/home_scents';
    } else if (typeT === 'hand_sanitizer') {
      href = `https://maison21g.com/${ele?.code}/product/hand_sanitizer`;
      sHref = '/product/hand-sanitizer/2061';
    } else if (typeT === 'creation_perfume' || typeT === 'Perfume') {
      href = `https://maison21g.com/${ele?.code}/product/creation_perfume`;
      sHref = '/product/creation_perfume';
      desSeo = getLinkFromButtonBlock(buttonBlocks, 'seo_creation_perfume');
    } else if (typeT === 'perfume_diy') {
      href = `https://maison21g.com/${ele?.code}/product/perfume_diy`;
      sHref = '/product/perfume_diy';
    } else if (typeT === 'car_diffuser') {
      href = `https://maison21g.com/${ele?.code}/product/car_diffuser`;
      sHref = '/product/car_diffuser';
    } else if (typeT === 'miniature_perfume' || typeT === 'Scent') {
      href = `https://maison21g.com/${ele?.code}/product/miniature_perfume`;
      sHref = '/product/miniature_perfume';
      desSeo = getLinkFromButtonBlock(buttonBlocks, 'seo_miniature_perfume');
    } else if (typeT === 'dual_crayons') {
      href = `https://maison21g.com/${ele?.code}/product/dual_crayons`;
      sHref = '/product/dual_crayons';
    } else if (typeT === 'bundle_creation') {
      href = `https://maison21g.com/${ele?.code}/product/bundle_creation`;
      sHref = '/product/bundle_creation';
    } else if (typeT === 'reed_diffuser') {
      href = `https://maison21g.com/${ele?.code}/product/reed_diffuser`;
      sHref = '/product/reed_diffuser';
    } else if (typeT === 'oil_burner') {
      href = `https://maison21g.com/${ele?.code}/product/oil_burner`;
      sHref = '/product/oil_burner';
    }
    return (
      <div className="div-col" style={{ background: '#F5ECDC' }}>
        <MetaTags>
          <title>
            {titleSeo}
          </title>
          <meta name="description" content={desSeo} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {/* <link href={href} rel="canonical" /> */}
          {generateHreflang(this.props.countries, sHref)}
        </MetaTags>

        <HeaderHomePageV3 isEcommerce />

        {/* <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
        /> */}
        <ProductDetail
          ref={this.refProductDetail}
          buttonBlocks={buttonBlocks}
          priceSample={priceSample}
          type={type}
          data={data}
          datas={datas}
          createBasketGuest={this.props.createBasketGuest}
          login={login}
          basket={basket}
          addProductBasket={this.props.addProductBasket}
          updateProductBasket={this.props.updateProductBasket}
          onChangeProduct={this.onChangeProduct}
          propsState={this.props.location ? this.props.location.state : undefined}
          cms={this.props.cms}
          onClickIngredient={this.onClickIngredient}
          recommendCms={recommendCms ? recommendCms.body : []}
          videoBlocks={videoBlocks}
          headerAndParagraphBocks={headerAndParagraphBocks}
          ingredients={this.props.ingredients}
          history={this.props.history}
          scentNotes={this.state.scentNotes}
          scents={this.props.scents}
          scentsCandle={this.props.scentsCandle}
          scentsMiniCandle={this.props.scentsMiniCandle}
          scentsHome={this.props.scentsHome}
          scentsWax={this.props.scentsWax}
          scentsDiy={this.props.scentsDiy}
          scentsDualCrayon={this.props.scentsDualCrayon}
          scentsOilBurner={this.props.scentsOilBurner}
          scentsReedDiffuser={this.props.scentsReedDiffuser}
          addScentsToStore={this.props.addScentsToStore}
          addScentsWaxToStore={this.props.addScentsWaxToStore}
          addScentsCandleToStore={this.props.addScentsCandleToStore}
          addScentsMiniCandleToStore={this.props.addScentsMiniCandleToStore}
          addScentsHomeToStore={this.props.addScentsHomeToStore}
          addScentsDualCrayonsToStore={this.props.addScentsDualCrayonsToStore}
          addScentsDiyToStore={this.props.addScentsDiyToStore}
          addScentNotesToStore={this.props.addScentNotesToStore}
          addScentsOilBurnerToStore={this.props.addScentsOilBurnerToStore}
          addScentsReedDifffUserToStore={this.props.addScentsReedDifffUserToStore}
          loadingPage={this.props.loadingPage}
          bottlePrice={bottlePrice}
          onChangeTypePerfume={this.onChangeTypePerfume}
          isCreateProduct={this.state.isCreateProduct}
          itemHandlerSanitizer={this.state.itemHandlerSanitizer}
          dataFAQ={this.state.dataFAQ || {}}
          isBestSeller={this.state.isBestSeller}
        />
        <ReviewsV3
          dataFAQ={this.state.dataFAQ}
          buttonBlocks={buttonBlocks}
        />
        {/* <Reviews
          data={reviewsData}
          isAddReviews={isReviewable}
          login={login}
          cms={this.props.cms}
          buttonBlocks={buttonBlocks}
          updateReviewData={this.updateReviewData}
          itemHandlerSanitizer={this.state.itemHandlerSanitizer}
          isProductDetailV3
        /> */}
        <BlockFAQV2
          dataFAQ={this.state.dataFAQ}
          buttonBlocks={buttonBlocks}
        />
        <FooterV2 />
      </div>
    );
  }
}

ProductB2C.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id1: PropTypes.string,
      id2: PropTypes.string,
    }),
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  basket: PropTypes.arrayOf(PropTypes.object).isRequired,
  addProductBasket: PropTypes.func.isRequired,
  updateProductBasket: PropTypes.func.isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  cms: PropTypes.shape(PropTypes.object).isRequired,
};

const mapDispatchToProps = {
  createBasketGuest,
  addProductBasket,
  addCmsRedux,
  updateProductBasket,
  updateIngredientsData,
  addScentsToStore,
  addScentsWaxToStore,
  addScentsCandleToStore,
  addScentsMiniCandleToStore,
  addScentsHomeToStore,
  addScentsDualCrayonsToStore,
  loadingPage,
  addScentsDiyToStore,
  addScentNotesToStore,
  addScentsOilBurnerToStore,
  addScentsReedDifffUserToStore,
};

function mapStateToProps(state) {
  return {
    basket: state.basket,
    login: state.login,
    cms: state.cms,
    countries: state.countries,
    ingredients: state.ingredients,
    scents: state.scents,
    scentsDiy: state.scentsDiy,
    scentsWax: state.scentsWax,
    scentsCandle: state.scentsCandle,
    scentsMiniCandle: state.scentsMiniCandle,
    scentsHome: state.scentsHome,
    scentsDualCrayon: state.scentsDualCrayon,
    scentsOilBurner: state.scentsOilBurner,
    scentsReedDiffuser: state.scentsReedDiffuser,
    scentNotes: state.scentNotes,
    // showAskRegion: state.showAskRegion,
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductB2C));
