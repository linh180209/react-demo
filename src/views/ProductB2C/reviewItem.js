import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import classnames from 'classnames';
import ReviewLineStar from './reviewLineStar';
import icStar from '../../image/icon/star.svg';
import icStarSelected from '../../image/icon/star-selected.svg';
import { isBrowser } from '../../DetectScreen';
import { isRightAlignedText } from '../../Redux/Helpers';

class ReviewItem extends Component {
  render() {
    const { data } = this.props;
    const {
      date_completed: dataCompleted, image, date_modified: dateModified,
      comment, rating_product: scoreProduct, rating_satisfaction: scoreSatisfaction, rating_scent: scoreScent,
      buttonBlocks, user, guest, avgRating,
    } = data;
    const name = !_.isEmpty(user) ? user.name : !_.isEmpty(guest) ? guest.name : '';
    const newScore = avgRating || (parseFloat(scoreProduct) + parseFloat(scoreSatisfaction) + parseFloat(scoreScent)) / 3;
    return (
      <div className="div-review-item">
        <div className={`div-leff ${isBrowser ? 'div-col' : 'div-row'}`}>
          <div className="line-star">
            {
              _.map(_.range(5), (x, index) => (
                <img src={index + 1 <= newScore ? icStarSelected : icStar} alt="star" />
              ))
            }
          </div>
          <h2>
            {name}
          </h2>
          <span>
            {moment(dateModified).format('DD MMM YYYY')}
          </span>
        </div>
        <div className={classnames('div-right div-col', isRightAlignedText() ? 'tr' : 'tl')}>
          <span>
            {comment}
          </span>
          <div className="list-image">
            {
              _.map(image ? [image] : [], d => (
                <img src={d} alt="test" onClick={this.props.onClickImage} />
              ))
            }
          </div>
        </div>
      </div>
    );
  }
}

export default ReviewItem;
