import React, { Component } from 'react';
import { Col } from 'reactstrap';
import PropTypes from 'prop-types';
import _ from 'lodash';

import RecommendItem from '../../components/Products/recommendItem';
import { isTablet } from '../../DetectScreen';
import { GET_FAQ_QUESTION } from '../../config';
import { toastrError } from '../../Redux/Helpers/notification';
import fetchClient from '../../Redux/Helpers/fetch-client';

class RecommendProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      faqs: [],
    };
  }

  componentDidMount() {
    // this.fetchDataFAQ();
  }

  // fetchDataFAQ = async () => {
  //   const { type } = this.props;
  //   const newType = type === 'home_scents_premade' ? 'home_scents' : type;
  //   const option = {
  //     url: GET_FAQ_QUESTION.replace('{type}', newType),
  //     method: 'GET',
  //   };
  //   try {
  //     const data = await fetchClient(option);
  //     if (data && !data.isError) {
  //       this.setState({ faqs: data?.faqs });
  //     } else {
  //       throw new Error(data.message);
  //     }
  //   } catch (err) {
  //     toastrError(err.message);
  //   }
  // }

  render() {
    const {
      dataFAQ,
    } = this.props;
    const faqs = dataFAQ?.faqs;
    return (
      <div className="div-col" style={{ alignItems: 'center', marginTop: '30px' }}>
        <Col md={isTablet ? '12' : '8'} xs="12">
          <div style={{
            width: '100%',
            height: '1px',
            background: 'rgba(38, 38, 38, 0.1)',
            visibility: faqs?.length === 0 ? 'hidden' : 'visible',
          }}
          />
          {
            _.map(faqs || [], x => (
              <RecommendItem cmsCommon={this.props.cmsCommon} title={x.title} description={x.description} />
            ))
          }
          <div style={{ height: '50px' }} />
        </Col>
      </div>
    );
  }
}

RecommendProducts.propTypes = {
  type: PropTypes.string.isRequired,
  cmsCommon: PropTypes.shape().isRequired,
};
export default RecommendProducts;
