import React, { Component } from 'react';
import {
  Modal, Dropdown, DropdownMenu, DropdownToggle, ModalBody,
} from 'reactstrap';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classnames from 'classnames';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ImportExportIcon from '@mui/icons-material/ImportExport';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import moment from 'moment';
import '../../styles/modal-review.scss';
import '../../styles/review-product-detail.scss';
import icStar from '../../image/icon/star.svg';
import icStarSelected from '../../image/icon/star-selected.svg';
import ReviewItem from './reviewItem';
import ReviewItemV2 from './ReviewItemV2';
import {
  GET_REVIEWS_FROM_PRODUCT_URL, GET_PRODUCT_REVIEW_URL,
} from '../../config';
import {
  startv2, startGrayV2,
} from '../../imagev2/svg';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import {
  getCmsCommon, getNameFromButtonBlock, getNameFromCommon, addFontCustom,
} from '../../Redux/Helpers';
import ReviewLineStar from './reviewLineStar';
import icDownBlack from '../../image/icon/arrow-d-review.svg';
import icClose from '../../image/icon/icClose.svg';
import HeaderCT from '../../componentsv2/headerCT';
import ButtonCT from '../../componentsv2/buttonCT';

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import { isMobile, isMobile767fn } from '../../DetectScreen';

class Reviews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataTable: [],
      scoreScent: 0,
      scoreProduct: 0,
      scoreSatisfaction: 0,
      dropdownOpen: false,
      isOpenModal: false,
      sortValueIndex: 0,
      reviewClicked: undefined,
      data: props.data,
      starReviewSelected: 0,
    };
    this.sortList = [
      {
        label: 'Newest',
        value: 'Newest',
      },
      {
        label: 'Oldest',
        value: 'Oldest',
      },
    ];
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const object = {};
    const { buttonBlocks, data } = nextProps;
    if (buttonBlocks !== prevState.buttonBlocks) {
      const { dataTable } = prevState;
      _.forEach(dataTable, (x) => {
        _.assign(x, { buttonBlocks });
      });
      _.assign(object, { buttonBlocks }, { dataTable });
    }
    if (data !== prevState.data) {
      if (!data || _.isEmpty(data.product)) {
        _.assign(object, { dataTable: [] });
      }
      _.assign(object, { data });
    }
    return !_.isEmpty(object) ? object : null;
  }

  componentDidMount = () => {
    if (this.props.isWorkShops) {
      const { data } = this.props;
      _.forEach(data, (x, index) => {
        _.assign(x, { index });
        _.assign(x, { avgRating: this.getScoreWorkShop(x) });
      });
      this.setState({ dataTable: data });
    } else {
      const { product } = this.props.data;
      this.getReviewProduct(product);
    }
  };

  componentDidUpdate(prevProps, prevState) {
    const { data } = this.props;
    if (data !== prevProps.data) {
      const { product } = data;
      this.getReviewProduct(product);
    }
  }

  toggle = () => {
    const { dropdownOpen } = this.state;
    this.setState({
      dropdownOpen: !dropdownOpen,
    });
  }

  getReviewProduct = (id) => {
    if (!id) {
      return;
    }
    const { itemHandlerSanitizer } = this.props;
    const option = {
      url: GET_PRODUCT_REVIEW_URL.replace('{id}', id),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        if (result.length === 0) {
          this.props.updateReviewData({
            dataTable: [], scoreScent: 0, scoreProduct: 0, scoreSatisfaction: 0,
          });
          this.setState({ dataTable: result });
          return;
        }
        let sumScent = 0;
        let sumProduct = 0;
        let sumSatisfaction = 0;
        const resultNew = !_.isEmpty(itemHandlerSanitizer) ? _.filter(result, x => x.line_item.item === itemHandlerSanitizer.id) : result;
        const startTotal = [0, 0, 0, 0, 0];
        _.forEach(resultNew, (x, index) => {
          _.assign(x, { index });
          _.assign(x, { avgRating: this.getScore(x) });
          sumScent += x.rating_scent;
          sumProduct += x.rating_product;
          sumSatisfaction += x.rating_satisfaction;
          const avg = parseInt(this.getScore(x), 10);
          startTotal[avg - 1] = startTotal[avg - 1] + 1;
        });
        const scoreScent = (sumScent / resultNew.length).toFixed(1);
        const scoreProduct = (sumProduct / resultNew.length).toFixed(1);
        const scoreSatisfaction = (sumSatisfaction / resultNew.length).toFixed(1);
        const avgRating = (parseFloat(scoreScent) + parseFloat(scoreProduct) + parseFloat(scoreSatisfaction)) / 3;
        this.setState({
          dataTable: resultNew, scoreScent, scoreProduct, scoreSatisfaction, avgRating, startTotal,
        });
        this.props.updateReviewData({
          dataTable: resultNew, scoreScent, scoreProduct, scoreSatisfaction, avgRating,
        });
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err.message);
    });
  }

  onClickImage = (data) => {
    this.setState({ reviewClicked: data, isOpenModal: true });
  }

  itemReview = (cell, row) => (
    <ReviewItem data={row} buttonBlocks={row.buttonBlocks} onClickImage={() => this.onClickImage(row)} />
  )

  itemReviewV2 = (cell, row) => (
    <div className={classnames('div-row-data div-row', row.length < 3 ? 'justify-start' : 'justify-between')}>
      {
        _.map(row || [], d => (
          <ReviewItemV2 data={d} />
        ))
      }
    </div>
  )

  itemReviewV3 = (cell, row) => (
    <div className={classnames('div-row-data div-row', 'justify-between')}>
      {
        _.map(row || [], d => (
          <ReviewItemV2 data={d} isVersion3 />
        ))
      }
    </div>
  )

  headerFormatter = () => (
    <div className="hidden" />
  )

  toggleModal = () => {
    const { isOpenModal } = this.state;
    this.setState({ isOpenModal: !isOpenModal });
  }

  scrollToReviews = () => {
    const ele = document.getElementById('anchor-reviews');
    if (ele) {
      ele.scrollIntoView({ behavior: 'smooth' });
    }
  }

  onPageChange = () => {
    this.scrollToReviews();
  }

  getScore = (data) => {
    const newScore = (parseFloat(data?.rating_product) + parseFloat(data?.rating_satisfaction) + parseFloat(data?.rating_scent)) / 3;
    return newScore;
  };

  getScoreWorkShop = (data) => {
    const newScore = (parseFloat(data?.rating_diffusion) + parseFloat(data?.rating_fit) + parseFloat(data?.rating_guidance) + parseFloat(data?.rating_innovative) + parseFloat(data?.rating_learning) + parseFloat(data?.rating_luxury) + parseFloat(data?.rating_overall) + parseFloat(data?.rating_satisfaction) + parseFloat(data?.rating_strength)) / 9;
    return newScore;
  };

  groupData = (listData, step) => {
    const data = [];
    for (let i = 0; i < listData.length; i += step) {
      data.push(listData.slice(i, i + step));
    }
    return data;
  };

  selectReviewStar = (index) => {
    if (index === this.state.starReviewSelected) {
      this.setState({ starReviewSelected: 0 });
    } else {
      this.setState({ starReviewSelected: index });
    }
  }

  toggleSort = () => {
    const { sortValueIndex } = this.state;
    this.setState({ sortValueIndex: sortValueIndex === 0 ? 1 : 0 });
  }

  render() {
    const options = {
      sizePerPage: 5,
      hideSizePerPage: true,
      hidePageListOnlyOnePage: true,
      onPageChange: this.onPageChange,
    };
    const optionsV2 = {
      sizePerPage: 2,
      hideSizePerPage: true,
      hidePageListOnlyOnePage: true,
      onPageChange: this.onPageChange,
    };
    const { cms, buttonBlocks } = this.props;
    const {
      dataTable, scoreScent, isOpenModal, scoreProduct, scoreSatisfaction, reviewClicked, newScore,
      avgRating, startTotal, starReviewSelected,
    } = this.state;
    const cmsCommon = getCmsCommon(cms);
    const reviewBt = getNameFromCommon(cmsCommon, 'REVIEWS');
    if (dataTable.length === 0) {
      return null;
    }
    console.log('reviewClicked', dataTable);
    const settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      // autoplay: true,
      arrows: true,
      autoplaySpeed: 5000,
      pauseOnHover: false,
    };

    const modalImage = (
      <Modal
        isOpen={isOpenModal}
        toggle={this.toggleModal}
        className={`modal-picture ${addFontCustom()}`}
        centered
      >
        <ModalBody className="modal-picture-body">
          <div className="bt-close div-row justify-end">
            <button type="button" onClick={this.toggleModal}>
              <img src={icClose} alt="" />
            </button>
          </div>

          <div className="div-name div-row items-center">
            <h2>
              {reviewClicked ? !_.isEmpty(reviewClicked.user) ? reviewClicked.user.name : !_.isEmpty(reviewClicked.guest) ? reviewClicked.guest.name : '' : ''}
            </h2>
            <div className="line-star">
              {
                _.map(_.range(5), (x, index) => (
                  <img src={index + 1 <= reviewClicked?.avgRating ? icStarSelected : icStar} alt="star" />
                ))
              }
            </div>
          </div>
          <div className="div-image">
            <Slider {...settings}>
              {
              _.map(reviewClicked && reviewClicked.image ? [reviewClicked.image] : [], d => (
                <img className="image-view" src={d} alt="test" />
              ))
            }
            </Slider>
          </div>
        </ModalBody>
      </Modal>
    );
    const headerReview = (
      <div className="header-review">
        <div className="div-left">
          {/* <h2>
            {newScore ? newScore.toFixed(1) : ''}
          </h2>
          <div className="line-star">
            {
              _.map(_.range(5), (x, index) => (
                <img src={index + 1 <= newScore ? icStarSelected : icStar} alt="star" />
              ))
            }
          </div>
          <span>
            {baseOnBt}
            {' '}
            {dataTable.length}
            {' '}
            {reviewBt}
          </span> */}
          <h2 className="header_2">
            {reviewBt.charAt(0).toUpperCase() + reviewBt.slice(1)}
          </h2>
          <span className="header_4">
            {dataTable.length}
            {' '}
            {reviewBt}
          </span>
        </div>
        <div className="div-right">
          {/* <span>
            {sortBt}
          </span> */}
          <Dropdown
            isOpen={this.state.dropdownOpen}
            toggle={this.toggle}
            className="dropdown-review"
          >
            <DropdownToggle onClick={this.toggle}>
              <div className="value">
                <span>{getNameFromButtonBlock(buttonBlocks, this.sortList[this.state.sortValueIndex].label)}</span>
                <img src={icDownBlack} alt="icDown" />
              </div>
            </DropdownToggle>
            <DropdownMenu>
              {
                _.map(this.sortList, (d, index) => (
                  <div onClick={() => this.setState({ sortValueIndex: index, dropdownOpen: false })}>
                    {getNameFromButtonBlock(buttonBlocks, d.label)}
                  </div>
                ))
              }
            </DropdownMenu>
          </Dropdown>
        </div>
      </div>
    );
    let listReviews = [];
    listReviews = _.filter(dataTable, x => !!x.comment).concat(_.filter(dataTable, x => !x.comment));
    if (starReviewSelected !== 0) {
      listReviews = _.filter(listReviews, x => parseInt(x.avgRating, 10) === starReviewSelected);
    }
    if (this.state.sortValueIndex === 0) {
      listReviews.sort((a, b) => moment(b.date_modified).valueOf() - moment(a.date_modified).valueOf());
    } else {
      listReviews.sort((a, b) => moment(a.date_modified).valueOf() - moment(b.date_modified).valueOf());
    }
    const groupData = this.groupData(listReviews, isMobile767fn() ? 1 : this.props.isProductDetailV3 ? 2 : 3);
    const sortBy = getNameFromButtonBlock(buttonBlocks, 'sort by');
    const mostRecentBt = getNameFromButtonBlock(buttonBlocks, 'most recent');
    const customReviewBt = getNameFromButtonBlock(buttonBlocks, 'Customer Reviews');
    const newBt = getNameFromButtonBlock(buttonBlocks, 'Newest');
    const oldBt = getNameFromButtonBlock(buttonBlocks, 'Oldest');
    if (this.props.isProductDetailV3) {
      return (
        <div className="div-review-product review-product-v3">
          <div id="anchor-reviews" />
          {
              isMobile767fn() ? (
                <div className="div-header-review-mobile div-col">
                  <div className="title-header tc">
                    {customReviewBt}
                  </div>
                  <div className="text-sub div-row">
                    <div className="text-review">
                      {listReviews?.length}
                      {' '}
                      {reviewBt ? _.upperFirst(_.camelCase(reviewBt)) : ''}
                    </div>
                    <ButtonCT
                      color="secondary"
                      onClick={this.toggleSort}
                      name={this.state.sortValueIndex === 0 ? newBt : oldBt}
                      startIcon={<ImportExportIcon />}
                      endIcon={<ArrowDropDownIcon />}
                    />
                  </div>

                </div>
              ) : (
                <div className="div-header-review">
                  <div className="div-text-left">
                    <div className="title-header">
                      {customReviewBt}
                    </div>
                    <div className="text-review">
                      {listReviews?.length}
                      {' '}
                      {reviewBt ? _.upperFirst(_.camelCase(reviewBt)) : ''}
                    </div>
                  </div>
                  <ButtonCT
                    color="secondary"
                    onClick={this.toggleSort}
                    name={this.state.sortValueIndex === 0 ? newBt : oldBt}
                    startIcon={<ImportExportIcon />}
                    endIcon={<ArrowDropDownIcon />}
                  />
                </div>
              )
            }

          <div className="wrap-item-review">
            <BootstrapTable
              keyField="index"
              data={groupData}
              columns={[{
                dataField: 'index',
                text: '',
                formatter: this.itemReviewV3,
                headerFormatter: this.headerFormatter,
              }]}
              classes="table-react-next-custom"
              wrapperClasses="table-react-next-custom__wrapper"
              pagination={paginationFactory(optionsV2)}
            />
          </div>

        </div>
      );
    }

    if (this.props.isProductDetailV2) {
      return (
        <div className="div-col items-center wrap-review">
          <div className="div-review-product review-product-v2">
            <div id="anchor-reviews" />
            <div className="div-header div-col">
              <HeaderCT type={isMobile ? 'Heading-S' : 'Heading-M'}>
                {reviewBt}
              </HeaderCT>
              <div className="div-line-start div-row">
                <span>
                  {avgRating ? avgRating.toFixed(1) : ''}
                </span>
                <div className="line-star">
                  {
                    _.map(_.range(5), (x, index) => (
                      <img src={index + 1 <= parseFloat(avgRating.toFixed(1)) ? startv2 : startGrayV2} alt="star" />
                    ))
                  }
                </div>
              </div>
              <div className="div-sort div-row">
                <div className="title-sort div-row">
                  <div className="other-Tagline">
                    {sortBy}
                  </div>
                  <ImportExportIcon style={{ color: '#2c2c2c' }} />
                </div>
                <div className="list-button div-row">
                  <ButtonCT
                    onClick={() => this.setState({ sortValueIndex: 0 })}
                    variant="outlined"
                    name={mostRecentBt}
                    size="small"
                    className={this.state.sortValueIndex === 0 ? 'active' : ''}
                  />
                  <ButtonCT
                    onClick={() => this.setState({ sortValueIndex: 1 })}
                    variant="outlined"
                    name="oldest"
                    size="small"
                    className={this.state.sortValueIndex === 0 ? '' : 'active'}
                  />
                </div>
              </div>
              <div className="div-result-review div-row">
                {
                  _.map(startTotal, (d, index) => (
                    <div
                      onClick={() => this.selectReviewStar(5 - index)}
                      className={classnames('info-start div-row', starReviewSelected === 5 - index ? 'active' : '')}
                    >
                      <span>{5 - index}</span>
                      <img src={startv2} alt="start" />
                      <span className="text-number">
                        (
                        {`${startTotal[4 - index]}`}
                        )
                      </span>
                    </div>
                  ))
                }
              </div>

              <div className="wrap-item-review">
                <BootstrapTable
                  keyField="index"
                  data={groupData}
                  columns={[{
                    dataField: 'index',
                    text: '',
                    formatter: this.itemReviewV2,
                    headerFormatter: this.headerFormatter,
                  }]}
                  classes="table-react-next-custom"
                  wrapperClasses="table-react-next-custom__wrapper"
                  pagination={paginationFactory(optionsV2)}
                />
              </div>

            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="div-col items-center">
        <div className="div-review-product">
          <div id="anchor-reviews" />
          { modalImage }
          {/* <h3>
            {reviewBt.charAt(0).toUpperCase() + reviewBt.slice(1)}
          </h3>
          <span className={!isMobile ? 'hidden' : 'text-total'}>
            {dataTable.length}
            {' '}
            {reviewBt.charAt(0).toUpperCase() + reviewBt.slice(1)}
          </span> */}
          {headerReview}
          <hr />
          <div className="wrap-item-review">
            <BootstrapTable
              keyField="index"
              data={listReviews}
              columns={[{
                dataField: 'index',
                text: '',
                formatter: this.itemReview,
                headerFormatter: this.headerFormatter,
              }]}
              classes="table-react-next-custom"
              wrapperClasses="table-react-next-custom__wrapper"
              pagination={paginationFactory(options)}
            />
          </div>
        </div>
      </div>
    );
  }
}

Reviews.propTypes = {
  data: PropTypes.shape({
    product: PropTypes.string,
    title: PropTypes.string,
    comment: PropTypes.string,
    rating: PropTypes.number,
  }).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  cms: PropTypes.shape().isRequired,
};

export default Reviews;
