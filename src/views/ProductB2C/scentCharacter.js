import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import parseHtml from 'react-html-parser';
import classnames from 'classnames';
import Slider from 'react-slick';

import icSearch from '../../image/icon/ic-search.svg';
import icPlay from '../../image/icon/ic-play-video-1.svg';
import ProgressLine from '../ResultScentV2/progressLine';
import ProgressCircle from '../ResultScentV2/progressCircle';
import { getNameFromCommon } from '../../Redux/Helpers';
import { isBrowser, isMobile, isMobile767fn } from '../../DetectScreen';
import BlockVideoIngredient from './BlockVideoIngredient';
import ProgressCircleV2 from '../ResultScentV2/progressCircleV2';

class ScentCharacter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectItemId: (!this.props.isBestSeller && ['Perfume', 'dual_candles', 'home_scents', 'dual_crayons', 'bundle_creation', 'reed_diffuser', 'perfume_diy', 'car_diffuser', 'mini_candles', 'oil_burner'].includes(props.type.name) && props.combos.length > 1) ? 0 : (props.combos && props.combos.length > 0 ? props.combos[0].id : undefined),
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { combos, type, isBestSeller } = nextProps;
    if (combos && JSON.stringify(combos) !== JSON.stringify(prevState.combos)) {
      return ({ combos, selectItemId: !isBestSeller && ['Perfume', 'dual_candles', 'home_scents', 'dual_crayons', 'bundle_creation', 'reed_diffuser', 'perfume_diy', 'car_diffuser', 'mini_candles', 'oil_burner'].includes(type.name) && combos.length > 1 ? 0 : combos[0].id });
    }
    return null;
  }

  render() {
    const {
      cmsCommon, profile, type, ingredients,
    } = this.props;
    // console.log('ingredients', ingredients);
    const {
      selectItemId, combos,
    } = this.state;
    const viewMore = getNameFromCommon(cmsCommon, 'view_more_info');
    const strengthBt = getNameFromCommon(cmsCommon, 'STRENGTH');
    const durationBt = getNameFromCommon(cmsCommon, 'DURATION');
    const scentCharacter = getNameFromCommon(cmsCommon, 'Scent_characteristics');
    const comboScent = getNameFromCommon(cmsCommon, 'COMBO_SCENT');
    const hereAre = getNameFromCommon(cmsCommon, 'Here_are_the_mix_characteristics_for');
    const cloneCombo = _.cloneDeep(combos);
    const isBundle = type && type.name === 'bundle';
    const isPerfumeDiy = type && type.name === 'perfume_diy';
    const isDualCandles = type && type.name === 'dual_candles';
    const videosT = [];
    _.forEach(combos, (d) => {
      const ele = _.find(ingredients, x => x.ingredient.id === (isBundle ? d.product.combo[0].product?.ingredient?.id : d.product?.ingredient?.id));
      if (ele) {
        videosT.push(ele);
      }
    });
    const videos = _.filter(videosT, d => d && d.videos && d.videos.length > 0);
    if (!this.props.isBestSeller && type && ['Perfume', 'dual_candles', 'home_scents', 'gift_bundle', 'dual_crayons', 'bundle_creation', 'reed_diffuser', 'perfume_diy', 'car_diffuser', 'mini_candles', 'oil_burner'].includes(type.name) && combos.length >= 2) {
      let item;
      if (type.name === 'mini_candles' && combos.length === 4) {
        item = {
          id: 0,
          image1: _.find(combos[0].product.images || [], x => x.type === 'unisex') ? _.find(combos[0].product.images || [], x => x.type === 'unisex').image : '',
          image2: _.find(combos[1].product.images || [], x => x.type === 'unisex') ? _.find(combos[1].product.images || [], x => x.type === 'unisex').image : '',
          image3: _.find(combos[2].product.images || [], x => x.type === 'unisex') ? _.find(combos[2].product.images || [], x => x.type === 'unisex').image : '',
          image4: _.find(combos[3].product.images || [], x => x.type === 'unisex') ? _.find(combos[3].product.images || [], x => x.type === 'unisex').image : '',
          name: `${combos[0].name} & ${combos[1].name} & ${combos[2].name} & ${combos[3].name}`,
          title: comboScent,
        };
      } else if (type.name !== 'mini_candles') {
        item = {
          id: 0,
          image1: _.find(combos[0].product.images || [], x => x.type === 'unisex') ? _.find(combos[0].product.images || [], x => x.type === 'unisex').image : '',
          image2: _.find(combos[1].product.images || [], x => x.type === 'unisex') ? _.find(combos[1].product.images || [], x => x.type === 'unisex').image : '',
          name: `${combos[0].name} & ${combos[1].name}`,
          title: comboScent,
        };
      }

      cloneCombo.unshift(item);
    }
    const selectItem = _.find(cloneCombo, x => x.id === selectItemId);
    const profileSelect = selectItem && selectItem.product && selectItem.product.profile ? selectItem.product.profile : profile;
    const colorProcess = (
      <div className={classnames('div-process-line')}>
        {
          _.map(profileSelect?.accords || [], x => (
            <ProgressLine data={x} isShowPercent className={this.props.isBestSeller ? 'ver-2' : ''} />
          ))
        }
      </div>
    );
    const circleProcess = (
      <div className="div-process-circle">
        {
          this.props.isBestSeller ? (
            <React.Fragment>
              <ProgressCircleV2
                title={strengthBt}
                percent={profileSelect ? parseInt(parseFloat(profileSelect.strength) * 100, 10) : 0}
                fill="#F5ECDC"
                trail="#56564E"
              />
              <ProgressCircleV2
                title={durationBt}
                percent={profileSelect ? parseInt(parseFloat(profileSelect.duration) * 100, 10) : 0}
                fill="#F5ECDC"
                trail="#56564E"
              />
            </React.Fragment>
          ) : (
            <React.Fragment>
              <ProgressCircle title={strengthBt} percent={profileSelect ? parseInt(parseFloat(profileSelect.strength) * 100, 10) : 0} />
              <ProgressCircle title={durationBt} percent={profileSelect ? parseInt(parseFloat(profileSelect.duration) * 100, 10) : 0} />
            </React.Fragment>
          )
        }
      </div>
    );
    const listScent = (
      <div className={!cloneCombo || cloneCombo.length < 2 ? 'hidden' : 'div-button-scent'}>
        {
          _.map(cloneCombo, d => (
            <React.Fragment>
              {
                d.id === 0 ? (
                  <button
                    type="button"
                    onClick={() => this.setState({ selectItemId: d.id })}
                    className={classnames(selectItemId !== d.id ? 'none-select' : '', d.image3 ? 'multiple-4' : '')}

                  >
                    <div className="div-bt-image-combo">
                      <img
                        loading="lazy"
                        src={d.image1}
                        alt="scent"
                      />
                      <img
                        loading="lazy"
                        src={d.image2}
                        alt="scent"
                      />
                      {
                        d.image3 && (
                          <img
                            loading="lazy"
                            src={d.image3}
                            alt="scent"
                          />
                        )
                      }
                      {
                        d.image4 && (
                          <img
                            loading="lazy"
                            src={d.image4}
                            alt="scent"
                          />
                        )
                      }
                      <span>{d.title}</span>
                    </div>

                  </button>
                ) : (
                  <button
                    type="button"
                    onClick={() => this.setState({ selectItemId: d.id })}
                    className={selectItemId !== d.id ? 'none-select' : ''}

                  >
                    <div className="div-bt-image-combo">
                      <img
                        loading="lazy"
                        className="full-image"
                        src={_.find(d.product.images || [], x => x.type === 'unisex') ? _.find(d.product.images || [], x => x.type === 'unisex').image : ''}
                        alt="scent"
                      />
                    </div>

                  </button>
                )
              }
            </React.Fragment>

          ))
        }
      </div>
    );
    const scentInfo = (
      <div className="div-info-detail">
        <div className={selectItem.id === 0 ? 'text-info-combo' : 'hidden'}>
          <span>
            {hereAre}
          </span>
          <span>{selectItem.name}</span>
        </div>
        <div className={selectItem.id === 0 ? 'hidden' : 'text-info'}>
          <span>
            {selectItem ? parseHtml(isBundle ? selectItem.product.short_description : selectItem.description) : ''}
          </span>
          <div className="div-bt-view">
            <button
              type="button"
              onClick={() => this.props.onClickIngredient(isBundle ? selectItem.product.short_description : selectItem.product.ingredient)}
            >
              <img
                src={icSearch}
                alt="search"
              />
              <span>
                {viewMore}
              </span>
            </button>
          </div>
        </div>

      </div>
    );

    const relatedVideo = (
      videos.length > 0 && <BlockVideoIngredient cmsCommon={cmsCommon} videos={videos} isBestSeller={this.props.isBestSeller} />
    );
    if (this.props.isBestSeller) {
      return (
        <div className={classnames('div-scent-character', this.props.isBestSeller ? 'scent-best-seller' : '')}>
          <h3 className="header_2">
            {scentCharacter}
          </h3>
          {
            !isMobile767fn() ? (
              <React.Fragment>
                <div className="info-character div-row">
                  <div className="info-character-right">
                    {
                      selectItem.id === 0
                        ? (
                          <div className={classnames('div-image-combo', selectItem.image3 ? 'multiple-4' : '')}>
                            <img
                              loading="lazy"
                              src={selectItem.image1}
                              alt="scent"
                            />
                            <img
                              loading="lazy"
                              src={selectItem.image2}
                              alt="scent"
                            />
                            {
                              selectItem.image3 && (
                                <img
                                  loading="lazy"
                                  src={selectItem.image3}
                                  alt="scent"
                                />
                              )
                            }
                            {
                              selectItem.image4 && (
                                <img
                                  loading="lazy"
                                  src={selectItem.image4}
                                  alt="scent"
                                />
                              )
                            }
                          </div>
                        ) : (
                          <div className="div-image">
                            <img
                              loading="lazy"
                              src={selectItem ? _.find(selectItem.product.images || [], x => x.type === 'unisex') ? _.find(selectItem.product.images || [], x => x.type === 'unisex').image : '' : ''}
                              alt="scent"
                            />
                          </div>

                        )
                    }
                  </div>
                  <div className="info-character-left div-col">
                    {listScent}
                    <div className="div-info">
                      {colorProcess}
                      {circleProcess}
                    </div>
                  </div>
                </div>
                {relatedVideo}
              </React.Fragment>
            ) : (
              <React.Fragment>
                {
                  selectItem.id === 0
                    ? (
                      <div className={classnames('div-image-combo-mobile', selectItem.image3 ? 'multiple-4' : '')}>
                        <img
                          loading="lazy"
                          src={selectItem.image1}
                          alt="scent"
                        />
                        <img
                          loading="lazy"
                          src={selectItem.image2}
                          alt="scent"
                        />
                        {
                          selectItem.image3 && (
                            <img
                              loading="lazy"
                              src={selectItem.image3}
                              alt="scent"
                            />
                          )
                        }
                        {
                          selectItem.image4 && (
                            <img
                              loading="lazy"
                              src={selectItem.image4}
                              alt="scent"
                            />
                          )
                        }
                      </div>
                    ) : (
                      <img
                        loading="lazy"
                        className="img-scent-mobile"
                        src={selectItem ? _.find(selectItem.product.images || [], x => x.type === 'unisex') ? _.find(selectItem.product.images || [], x => x.type === 'unisex').image : '' : ''}
                        alt="scent"
                      />
                    )
                }
                {listScent}
                <div className="div-info">
                  {circleProcess}
                  {colorProcess}
                </div>
                {relatedVideo}
              </React.Fragment>
            )
          }
        </div>
      );
    }
    return (
      <div className={classnames('div-scent-character', this.props.isBestSeller ? 'scent-best-seller' : '')}>
        <h3 className="header_2">
          {scentCharacter}
        </h3>
        {
          isBrowser ? (
            <React.Fragment>
              <div className="info-character div-row">
                <div className="info-character-right">
                  {
                    selectItem.id === 0
                      ? (
                        <div className={classnames('div-image-combo', selectItem.image3 ? 'multiple-4' : '')}>
                          <img
                            loading="lazy"
                            src={selectItem.image1}
                            alt="scent"
                          />
                          <img
                            loading="lazy"
                            src={selectItem.image2}
                            alt="scent"
                          />
                          {
                            selectItem.image3 && (
                              <img
                                loading="lazy"
                                src={selectItem.image3}
                                alt="scent"
                              />
                            )
                          }
                          {
                            selectItem.image4 && (
                              <img
                                loading="lazy"
                                src={selectItem.image4}
                                alt="scent"
                              />
                            )
                          }
                        </div>
                      ) : (
                        <div className="div-image">
                          <img
                            loading="lazy"
                            src={selectItem ? _.find(selectItem.product.images || [], x => x.type === 'unisex') ? _.find(selectItem.product.images || [], x => x.type === 'unisex').image : '' : ''}
                            alt="scent"
                          />
                        </div>

                      )
                  }
                </div>
                <div className="info-character-left div-col">
                  {listScent}
                  <div className="div-info">
                    {colorProcess}
                    {circleProcess}
                  </div>
                </div>
              </div>
              {relatedVideo}
            </React.Fragment>
          ) : (
            <React.Fragment>
              {listScent}
              {
                selectItem.id === 0
                  ? (
                    <div className={classnames('div-image-combo-mobile', selectItem.image3 ? 'multiple-4' : '')}>
                      <img
                        loading="lazy"
                        src={selectItem.image1}
                        alt="scent"
                      />
                      <img
                        loading="lazy"
                        src={selectItem.image2}
                        alt="scent"
                      />
                      {
                        selectItem.image3 && (
                          <img
                            loading="lazy"
                            src={selectItem.image3}
                            alt="scent"
                          />
                        )
                      }
                      {
                        selectItem.image4 && (
                          <img
                            loading="lazy"
                            src={selectItem.image4}
                            alt="scent"
                          />
                        )
                      }
                    </div>
                  ) : (
                    <img
                      loading="lazy"
                      className="img-scent-mobile"
                      src={selectItem ? _.find(selectItem.product.images || [], x => x.type === 'unisex') ? _.find(selectItem.product.images || [], x => x.type === 'unisex').image : '' : ''}
                      alt="scent"
                    />
                  )
              }
              {
                selectItemId === 0
                  ? (
                    <div className="span-info-mobile">
                      <span>{hereAre}</span>
                      <span>{selectItem.name}</span>

                    </div>
                  ) : (
                    <span className="span-info-mobile">
                      {selectItem ? parseHtml(isBundle ? selectItem.product.combo[0].description : selectItem.description) : ''}
                    </span>
                  )
              }
              <div className="div-info">
                {colorProcess}
                {circleProcess}
              </div>
              <div className={selectItem.id === 0 ? 'hidden' : 'div-bt-view'}>
                <button
                  type="button"
                  onClick={() => this.props.onClickIngredient(isBundle ? selectItem.product.combo[0].product.ingredient : selectItem.product.ingredient)}
                >
                  <img
                    src={icSearch}
                    alt="search"
                  />
                  <span>
                    {viewMore}
                  </span>
                </button>
              </div>
              {relatedVideo}
            </React.Fragment>
          )
        }
      </div>
    );
  }
}

ScentCharacter.propTypes = {
  cmsCommon: PropTypes.shape().isRequired,
  ingredients: PropTypes.arrayOf().isRequired,
};

export default ScentCharacter;
