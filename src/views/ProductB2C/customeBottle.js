/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import loadImage from 'blueimp-load-image';
import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import _ from 'lodash';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import {
  Dropdown, DropdownMenu, DropdownToggle,
} from 'reactstrap';
import Dropzone from 'react-dropzone';
import { LazyLoadImage } from 'react-lazy-load-image-component';

import 'react-image-gallery/styles/css/image-gallery.css';
import { isIOS } from 'react-device-detect';
import icDown from '../../image/icon/ic-down.svg';
import icClose from '../../image/icon/ic-close-region.svg';
import icBottle from '../../image/img-bottle.png';
import icBottleNew from '../../image/bottle-new.png';
import testName from '../../image/custome-name.png';
import icUndo from '../../image/icon/ic_undo.svg';
import icCrop from '../../image/icon/ic_crop.svg';
import icUploadFile from '../../image/icon/ic-upload-file.svg';
import icUploadMore from '../../image/icon/ic-upload-more.svg';
import logoBlack from '../../image/icon/logo-custome-black.svg';
import logoWhite from '../../image/icon/logo-custome-white.svg';
import '../../styles/custome-bottle.scss';
import { getNameFromCommon, generaCurrency, getTextFromTextBlock } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';


const getDataCustome = (propsState) => {
  let name;
  let imageCustome;
  let isCustome;
  let isBlack;
  if (propsState) {
    const { custome } = propsState;
    if (custome) {
      name = custome.name;
      imageCustome = custome.image;
      isCustome = true;
      isBlack = custome.isBlack;
    }
  }
  return {
    name, imageCustome, isCustome, isBlack,
  };
};

const widthCrop = 70;
const heightCrop = 225;

class CustomeBottle extends Component {
  constructor(props) {
    super(props);
    const { name, imageCustome, isBlack } = getDataCustome(
      props.propsState,
    );
    this.state = {
      srcImage: undefined,
      isCroped: !!imageCustome,
      croppedImageUrlDone: imageCustome || props.currentImg,
      crop: {
        unit: 'px',
        width: widthCrop,
        height: heightCrop,
        aspect: widthCrop / heightCrop,
      },
      isShowText: false,
      textImageText: props.currentImg ? props.nameBottle || '' : '',
      textText: !props.currentImg ? props.nameBottle || '' : '',
      dropdownOpen: false,
      isBlack: isBlack === undefined ? props.isBlack : isBlack,
    };
    this.refCrop = React.createRef();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { propsState } = nextProps;
    if (propsState !== prevState.propsState) {
      const { name, imageCustome, isBlack } = getDataCustome(
        propsState,
      );
      const nameBottle = name || '';

      const croppedImageUrlDone = imageCustome;
      _.assign(objectReturn, {
        isBlack: isBlack === undefined ? nextProps.isBlack : isBlack,
        propsState,
        nameBottle,
        croppedImageUrlDone,
      });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : {};
  }

  toggle = () => {
    const { dropdownOpen } = this.state;
    this.setState({
      dropdownOpen: !dropdownOpen,
    });
  }

  onClickCrop = () => {
    const { croppedImageUrl } = this.state;
    this.setState({
      isCroped: true,
      croppedImageUrlDone: croppedImageUrl,
    });
    this.props.handleCropImage(croppedImageUrl);
  }

  onCLickUndo = () => {
    this.setState({
      srcImage: '',
      isCroped: true,
      croppedImageUrlDone: undefined,
      croppedImageUrl: undefined,
    });
    // document.getElementById('inputFile').value = '';
    // this.props.handleCropImage('');
  }

  onSelectFile = (files) => {
    if (files && files.length > 0) {
      loadImage(
        files[0],
        (img) => {
          const base64data = img.toDataURL('image/png');
          this.setState({
            srcImage: base64data, isCroped: false, croppedImageUrlDone: undefined, croppedImageUrl: undefined,
          });
        },
        { orientation: 1 },
      );
    }
  };

  onImageLoaded = (image) => {
    this.imageRef = image;
  };

  onCropComplete = (crop) => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop) => {
    this.setState({ crop });
  };

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        'newFile.jpeg',
      );
      this.setState({ croppedImageUrl, isCroped: false });
    }
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width * scaleX;
    canvas.height = crop.height * scaleY;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width * scaleX,
      crop.height * scaleY,
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob((blob) => {
        if (!blob) {
          return;
        }
        blob.name = fileName;
        this.fileUrl = window.URL.createObjectURL(blob);
        resolve(this.fileUrl);
      }, 'image/png');
    });
  }

  handleActiveImgOnBottle = (img) => {
    this.setState({
      isCroped: true,
      croppedImageUrlDone: img,
    });
  };

  setNameBottle = (text) => {
    this.setState({ nameBottle: text });
  }

  onChangeImageText = (e) => {
    const { value } = e.target;
    if (this.state.textImageText.length >= 15 && value.length >= 15) {
      return;
    }

    this.setState({ textImageText: value });
  }

  onChangeText = (e) => {
    const { value } = e.target;
    if (this.state.textText.length >= 15 && value.length >= 15) {
      return;
    }
    this.setState({ textText: value });
  }

  onSave = () => {
    const {
      isShowText, textText, textImageText, croppedImageUrlDone, isBlack,
    } = this.state;
    if (isShowText) {
      this.props.onSave({ isOnlyName: true, nameBottle: textText });
    } else {
      this.props.onSave({
        isOnlyName: false, nameBottle: textImageText, croppedImageUrlDone, isBlack,
      });
    }
    if (this.props.idCartItem) {
      setTimeout(() => {
        this.props.onClickUpdatetoCart();
        if (this.props.propsState) {
          this.props.propsState.custome.name = textText || textImageText;
          this.props.propsState.custome.image = croppedImageUrlDone;
          this.props.propsState.custome.isBlack = isBlack;
        }
      }, 500);
    }
    this.props.closePopUp();
  }

  htmlWeb = listImage => (
    <div className="item-slice">
      <div className="line-image">
        {
          _.map(listImage.slice(0, 5), x => (
            <div className="div-image" onClick={() => this.setState({ croppedImageUrlDone: x.image, croppedImageUrl: undefined, srcImage: undefined })}>
              <LazyLoadImage className={this.state.croppedImageUrlDone === x.image ? 'active' : ''} src={x.thumb} alt="bottle" />
              {/* <img className={this.state.croppedImageUrlDone === x.image ? 'active' : ''} src={x.thumb} alt="bottle" onClick={() => this.setState({ croppedImageUrlDone: x.image, croppedImageUrl: undefined, srcImage: undefined })} /> */}
            </div>
          ))
        }
      </div>
      <div className="line-image">
        {
          _.map(listImage.slice(5, 10), x => (
            <div className="div-image" onClick={() => this.setState({ croppedImageUrlDone: x.image, croppedImageUrl: undefined, srcImage: undefined })}>
              <LazyLoadImage className={this.state.croppedImageUrlDone === x.image ? 'active' : ''} src={x.thumb} alt="bottle" />
              {/* <img className={this.state.croppedImageUrlDone === x.image ? 'active' : ''} src={x.thumb} alt="bottle" onClick={() => this.setState({ croppedImageUrlDone: x.image, croppedImageUrl: undefined, srcImage: undefined })} /> */}
            </div>
          ))
        }
      </div>
    </div>
  );

  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      arrows: false,
      autoplaySpeed: 5000,
      pauseOnHover: false,
    };
    const {
      cmsCommon, arrayThumbs, itemBottle, cmsTextBlocks, name1, name2,
      idCartItem,
    } = this.props;
    const newArray = [];
    if (arrayThumbs) {
      for (let i = 0; i < arrayThumbs.length; i += 10) {
        newArray.push(arrayThumbs.slice(i, i + 10));
      }
    }
    const {
      isCroped, croppedImageUrlDone, croppedImageUrl, srcImage, crop, nameBottle, isShowText, textImageText, textText,
      isBlack,
    } = this.state;
    const uploadImageBt = getNameFromCommon(cmsCommon, 'Upload_Image');
    const cropBt = getNameFromCommon(cmsCommon, 'Crop');
    const undoBt = getNameFromCommon(cmsCommon, 'Undo');
    const saveBt = getNameFromCommon(cmsCommon, `${idCartItem ? 'UPDATE_TO_CART' : 'SAVE'}`);
    const cancelBt = getNameFromCommon(cmsCommon, 'CANCEL');
    const freeBt = getNameFromCommon(cmsCommon, 'FREE');
    const eauBt = getNameFromCommon(cmsCommon, 'Eau_de_parfum');
    const mlBt = getNameFromCommon(cmsCommon, '25ml');
    const ctBottle = getNameFromCommon(cmsCommon, 'CUSTOMISE_YOUR_BOTTLE');
    const nameText = getNameFromCommon(cmsCommon, 'Name');
    const nameTextText = getNameFromCommon(cmsCommon, 'IMAGE_TEXT');
    const textOnly = getNameFromCommon(cmsCommon, 'TEXT_ONLY');
    const enterAName = getNameFromCommon(cmsCommon, 'Enter_a_name');
    const premadeDesign = getNameFromCommon(cmsCommon, 'PREMADE_DESIGN');
    const reuploadImage = getNameFromCommon(cmsCommon, 'Reupload_Image');
    const profileYourFile = getNameFromCommon(cmsCommon, 'Browse_your_files');
    const addText = getNameFromCommon(cmsCommon, 'ADD_TEXT');
    const textColor = getNameFromCommon(cmsCommon, 'Text_Color');

    const htmlNew = (
      <div
        style={isMobile && isIOS ? { marginTop: '40px' } : {}}
        className="div-custome-bottle-v2 animated faster fadeIn"
      >
        <div className="div-content">
          <div className="div-sroll">
            <img className="bt-close" src={icClose} alt="bt-close" onClick={this.props.closePopUp} />
            <div className="div-header">
              <h1>
                {ctBottle}
              </h1>
            </div>

            <div className="div-image-bottle">
              <div className="div-backgroud" />

              <img
                src={icBottleNew}
                alt="icBottle"
                className={`div-bottle ${isShowText ? 'show-bottle' : 'hidden-bottle'}`}
              />

              <img
                src={icBottle}
                alt="icBottle"
                className={`div-bottle ${isShowText ? 'hidden-bottle' : 'show-bottle'}`}
              />


              {
                isShowText
                  ? (
                    <React.Fragment>
                      <input
                        type="text"
                        className="text-name-text"
                        placeholder={nameText}
                        disabled
                        value={textText}
                      />
                      <div className="ingredients-name">
                        <span>
                          {name1}
                        </span>
                        <span className={name2 ? '' : 'hidden'}>
                          X
                        </span>
                        <span>
                          {name2}
                        </span>
                      </div>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <img
                        onClick={() => {
                          if (!croppedImageUrlDone && !croppedImageUrl && this.refCrop.current) {
                            this.refCrop.current.open();
                          }
                        }}
                        src={croppedImageUrlDone || (croppedImageUrl || testName)}
                        alt="testName"
                        className="div-name animated faster fadeIn"
                      />
                      <input
                        type="text"
                        className={`text-name animated faster fadeIn ${isBlack ? 'black' : ''}`}
                        placeholder={nameText}
                        disabled
                        value={textImageText}
                      />
                      <img
                        src={isBlack ? logoBlack : logoWhite}
                        alt=""
                        className="image-logo  animated faster fadeIn"
                      />
                      <span className="text-des-custome animated faster fadeIn" style={{ color: isBlack ? '#000000' : '#ffffff' }}>
                        {eauBt}
                      </span>
                      <span className="text-size-custome animated faster fadeIn" style={{ color: isBlack ? '#000000' : '#ffffff' }}>
                        {mlBt}
                      </span>
                      {
                      srcImage && !croppedImageUrlDone
                        ? (
                          <ReactCrop
                            src={srcImage}
                            crop={crop}
                            onImageLoaded={this.onImageLoaded}
                            onComplete={this.onCropComplete}
                            onChange={this.onCropChange}
                            minWidth={widthCrop}
                            minHeight={heightCrop}
                          />
                        ) : (<div />)
                    }
                      {
                      croppedImageUrl ? (
                        <div className="div-button-image">
                          <button type="button" onClick={this.onClickCrop}>
                            <img src={icCrop} alt="crop" />
                            {cropBt}
                          </button>
                          <button type="button" onClick={this.onCLickUndo}>
                            <img src={icUndo} alt="undo" />
                            {undoBt}
                          </button>
                        </div>
                      ) : (<div />)
                    }
                    </React.Fragment>
                  )
              }
            </div>
            <div className="div-button">
              <button type="button" className={isShowText ? '' : 'active'} onClick={() => { this.setState({ isShowText: false }); }}>
                <div className="detail-bt">
                  <span>
                    {nameTextText}
                  </span>
                  <span>
                    {itemBottle && parseFloat(itemBottle.price) !== 0
                      ? `(+${generaCurrency(itemBottle.price)})`
                      : freeBt}
                  </span>
                </div>
              </button>
              <button type="button" className={isShowText ? 'active' : ''} onClick={() => { this.setState({ isShowText: true }); }}>
                <div className="detail-bt">
                  <span>
                    {textOnly}
                  </span>
                  <span>
                    {freeBt}
                  </span>
                </div>

              </button>
            </div>
            <div className="div-input-image">
              {
                isShowText ? (
                  <div className="div-input input-only-text animated fadeIn">
                    <div className="div-input-text">
                      <input type="text" onChange={this.onChangeText} value={textText} placeholder="My perfume" />
                      <span>
                        {textText.length}
                        /15
                      </span>
                    </div>
                    <span>
                      {enterAName}
                    </span>
                  </div>
                ) : (
                  <React.Fragment>
                    <div className="slice-image">
                      <div className="div-path-slice animated fadeIn">
                        <span>
                          {premadeDesign}
                        </span>
                        <Slider {...settings}>
                          {
                          _.map(newArray, d => (
                            this.htmlWeb(d)
                          ))
                        }
                        </Slider>
                      </div>
                      <div className="div-line-space animated fadeIn" />
                      <div className="div-button-crop animated fadeIn">
                        <span>
                          {uploadImageBt}
                        </span>
                        {
                          srcImage
                            ? (
                              <React.Fragment>
                                <img className="image-crop-done" src={srcImage} alt="" />
                                <Dropzone onDrop={acceptedFiles => this.onSelectFile(acceptedFiles)} accept=".jpeg,.png,.jpg" multiple={false}>
                                  {({ getRootProps, getInputProps }) => (
                                    <section className="div-section-more">
                                      <div {...getRootProps()}>
                                        <input {...getInputProps()} />
                                        <img src={icUploadMore} alt="uploadFile" />
                                        <span>
                                          {reuploadImage}
                                        </span>
                                      </div>
                                    </section>
                                  )}
                                </Dropzone>
                              </React.Fragment>
                            ) : (
                              <Dropzone ref={this.refCrop} onDrop={acceptedFiles => this.onSelectFile(acceptedFiles)} accept=".jpeg,.png,.jpg" multiple={false}>
                                {({ getRootProps, getInputProps }) => (
                                  <section>
                                    <div {...getRootProps()}>
                                      <input {...getInputProps()} />
                                      <img src={icUploadFile} alt="uploadFile" />
                                      <span>
                                        {profileYourFile}
                                      </span>
                                    </div>
                                  </section>
                                )}
                              </Dropzone>
                            )
                        }
                      </div>
                    </div>
                    <div className="div-text-color">
                      <div className="div-text animated fadeIn">
                        <span>
                          {addText}
                        </span>
                        <div className="div-input short mt-2">
                          <div className="div-input-text">
                            <input type="text" onChange={this.onChangeImageText} value={textImageText} placeholder="My perfume" />
                            <span>
                              {textImageText.length}
                              /15
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="div-color">
                        <span>
                          {textColor}
                        </span>
                        <Dropdown
                          isOpen={this.state.dropdownOpen}
                          toggle={this.toggle}
                          className="dropdown-text-color mt-2"
                        >
                          <DropdownToggle
                            tag="span"
                            onClick={this.toggle}
                            data-toggle="dropdown"
                            aria-expanded={this.state.dropdownOpen}
                          >
                            <div className="div-row items-center justify-between items-center border-checkout" style={{ height: '56px' }}>
                              <div className="drop-down-item none-border">
                                <div className={`div-square ${isBlack ? 'black' : ''}`} />
                                <span className="ml-3">
                                  {isBlack ? 'Black' : 'White'}
                                </span>
                              </div>
                              <img src={icDown} alt="icDown" />
                            </div>

                          </DropdownToggle>
                          <DropdownMenu>

                            <div
                              onClick={() => {
                                this.setState({ isBlack: true, dropdownOpen: false });
                              }}
                            >
                              <div
                                className="drop-down-item"
                              >
                                <div className="div-square black" />
                                <span className="ml-3">
                                  {/* {`${d.name}`} */}
                                  Black
                                </span>
                              </div>
                            </div>
                            <div
                              onClick={() => {
                                this.setState({ isBlack: false, dropdownOpen: false });
                              }}
                            >
                              <div className="drop-down-item">
                                <div className="div-square" />
                                <span className="ml-3">
                                  {/* {`${d.name}`} */}
                                  White
                                </span>
                              </div>
                            </div>

                          </DropdownMenu>
                        </Dropdown>
                      </div>
                    </div>
                    <span className="tc w-100" style={{ marginTop: '20px' }}>
                      {enterAName}
                    </span>

                  </React.Fragment>
                )
              }
            </div>
            <div className="div-button-bottom">
              <button type="button" className="active" onClick={this.onSave}>
                {saveBt}
              </button>
              <button type="button" onClick={this.props.closePopUp}>
                {cancelBt}
              </button>
            </div>
          </div>
        </div>
      </div>
    );

    return (
      <React.Fragment>
        { htmlNew }
      </React.Fragment>
    );
  }
}

CustomeBottle.propTypes = {
  propsState: PropTypes.shape().isRequired,
  cmsCommon: PropTypes.arrayOf().isRequired,
  handleCropImage: PropTypes.func.isRequired,
  nameBottle: PropTypes.string.isRequired,
  arrayThumbs: PropTypes.arrayOf().isRequired,
  handleActiveImgOnBottle: PropTypes.func.isRequired,
  currentImg: PropTypes.string.isRequired,
  closePopUp: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  itemBottle: PropTypes.shape().isRequired,
  cmsTextBlocks: PropTypes.arrayOf().isRequired,
  name1: PropTypes.string.isRequired,
  name2: PropTypes.string.isRequired,
  onClickUpdatetoCart: PropTypes.func.isRequired,
  idCartItem: PropTypes.string.isRequired,
};

export default CustomeBottle;
