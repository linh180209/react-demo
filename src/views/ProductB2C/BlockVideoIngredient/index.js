import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { getNameFromCommon } from '../../../Redux/Helpers';
import { isBrowser, isMobile } from '../../../DetectScreen';
import icPlay from '../../../image/icon/ic-play-video-1.svg';
import ModalVideo from '../../../components/ModalVideo';
import { useMergeState } from '../../../Utils/customHooks';
import './styles.scss';

function BlockVideoIngredient(props) {
  const [state, setState] = useMergeState({
    videoSelected: undefined,
    isOpenModalVideo: false,
  });

  const onClickOpenVideo = (d) => {
    setState({ videoSelected: d, isOpenModalVideo: true });
  };

  const onToggleModalVideo = () => {
    const { isOpenModalVideo } = state;
    setState({ isOpenModalVideo: !isOpenModalVideo });
  };

  const settings = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    pauseOnHover: false,
    centerMode: true,
  };

  const relatedVideoBt = getNameFromCommon(props.cmsCommon, 'related videos');
  return (
    <div className={classnames('video-ingredient', props.isBestSeller ? 'list-grid' : '')}>
      <ModalVideo
        onClose={onToggleModalVideo}
        isOpen={state.isOpenModalVideo}
        name={state.videoSelected?.name}
        video={state.videoSelected?.videos[0]?.video}
        poster={state.videoSelected?.videos[0]?.placeholder}
      />
      <h3>
        {relatedVideoBt}
      </h3>
      <div className={classnames('list-video', (!isMobile && props.videos.length === 1 ? 'center' : !isMobile && props.videos.length > 3 ? 'scroll-item' : ''))}>
        {
          isBrowser || props.isBestSeller ? (
            _.map(props.videos, d => (
              <div className="video-item" onClick={() => onClickOpenVideo(d)}>
                {/* <VideoAcademy url={d.videos[0].video} poster={d.videos[0].placeholder} isControl /> */}
                <div className="thumbnail-video">
                  <img loading="lazy" src={d.videos[0].placeholder} alt="thumbnail" />
                  <button type="button" className="btn btn-bg__none">
                    <img src={icPlay} alt="play" />
                  </button>

                </div>
                <span>{d.name}</span>
              </div>
            ))) : (
              <Slider
                {... settings}
              >
                {
                  _.map(props.videos, d => (
                    <div className="video-item" onClick={() => onClickOpenVideo(d)}>
                      {/* <VideoAcademy url={d.videos[0].video} poster={d.videos[0].placeholder} isControl /> */}
                      <div className="thumbnail-video">
                        <img loading="lazy" src={d.videos[0].placeholder} alt="thumbnail" />
                        <button type="button" className="btn btn-bg__none">
                          <img src={icPlay} alt="play" />
                        </button>

                      </div>
                      <div className="title">{d.name}</div>
                    </div>
                  ))
                }
              </Slider>
          )
        }
      </div>
    </div>
  );
}

export default BlockVideoIngredient;
