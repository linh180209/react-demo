/* eslint-disable react/jsx-max-props-per-line */
import React, { Component } from 'react';
import _ from 'lodash';
import icNext from '../../image/icon/next-scent.svg';
import icBack from '../../image/icon/back-scent.svg';

class TitleScentSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isSelectScent: false,
    };
  }

  renderScentSelection = () => (
    <div
      className="div-item-scent-selection"
      onClick={() => {
        this.setState({ isSelectScent: true });
      }}
    >
      <img src={testImage} alt="scent" />
      <div className="div-text">
        <h3>
          AMBER AFFAIR
        </h3>
        <span>
          1st Ingredient
        </span>
      </div>
      <button type="button" className="bt-click-more">
        <img src={icNext} alt="next" />
      </button>
    </div>
  )

  render() {
    const { isSelectScent } = this.state;
    return (
      <div className="div-scent-selection">
        <div className={isSelectScent ? 'hidden' : 'div-1 animated faster fadeInLeft'}>
          <div className="div-title">
            <h3>
              Creation Perfume
            </h3>
            <div className="div-select">
              <button type="button">
                Day Perfume
              </button>
              <button type="button" className="bt-active">
                Night Perfume
              </button>
            </div>
          </div>
          <div className="div-list-scent">
            {
              _.map(_.range(2), x => (
                this.renderScentSelection()
              ))
            }
          </div>
        </div>
        <div className={isSelectScent ? 'div-2 animated faster fadeInRight' : 'hidden'}>
          <div
            className="div-title"
            onClick={() => {
              this.setState({ isSelectScent: false });
            }}
          >
            <button type="button">
              <img src={icBack} alt="back" />
            </button>
            <h3>
              Select Your Ingredient
            </h3>
          </div>
          <div className="div-body">
            {
              _.map(_.range(3), x => (
                <button className="bt-scent" type="button">
                  <img src={testImage} alt="scent" />
                </button>
              ))
            }
          </div>
        </div>
        <div className="div-text-price">
          <button type="button">
            Try a sample for S$15.00
          </button>
          <span>
            S$ 69.00
          </span>
        </div>
      </div>
    );
  }
}

export default TitleScentSelection;
