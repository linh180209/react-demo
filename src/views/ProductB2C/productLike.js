import React, { Component } from 'react';
import { Col } from 'reactstrap';
import PropTypes from 'prop-types';

import ScentItem from '../../components/ScentItem';
import { isMobile, isTablet } from '../../DetectScreen';

class ProductLike extends Component {
  data = {
    name: 'Jasmin & Sandalwood',
    // image_urls: [test5],
    created: 'Lorem ipsum',
    variant_values: '',
    buttonName: '+ ADD TO CART',
  }

  render() {
    return (
      <div className="div-col" style={{ alignItems: 'center' }}>
        <Col md="8" sx="12" className="div-col" style={{ alignItems: 'center', padding: '0px' }}>
          <span
            style={{
              fontSize: '2rem',
              color: '#000',
            }}
          >
            You may also like
          </span>
          {
            isMobile && !isTablet ? (
              <div
                className="div-row"
                style={{
                  justifyContent: 'space-around',
                  width: '100%',
                  margin: '50px 0px 80px 0px',
                }}
              >
                <ScentItem data={this.data} isFavorite cms={this.props.cms} />
                <ScentItem data={this.data} isFavorite cms={this.props.cms} />
              </div>
            )
              : (
                <div
                  className="div-row"
                  style={{
                    justifyContent: 'space-between',
                    width: '100%',
                    margin: '50px 0px 80px 0px',
                  }}
                >
                  <ScentItem data={this.data} isFavorite cms={this.props.cms} />
                  <ScentItem data={this.data} isFavorite cms={this.props.cms} />
                  <ScentItem data={this.data} isFavorite cms={this.props.cms} />
                </div>
              )
          }

        </Col>
      </div>
    );
  }
}

ProductLike.propTypes = {
  data: PropTypes.shape(PropTypes.object).isRequired,
  cms: PropTypes.shape().isRequired,
};

export default ProductLike;
