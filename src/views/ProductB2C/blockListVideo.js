import React, { Component } from 'react';
import YouTube from 'react-youtube';
import Slider from 'react-slick';
import _ from 'lodash';
import '../../styles/block-video-product.scss';
import icLogo from '../../image/icon/icLogo.svg';
import icYoutube from '../../image/icon/icYoutube.svg';
import { getNameFromButtonBlock } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

class BlockListVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      idVideo: props.videoBlocks[0]?.value?.text,
    };
  }

  videoItem = d => (
    <button type="button" className="bt-image-video" onClick={() => this.onClickVideo(d.value.text)}>
      <img loading="lazy" src={d.value.image.image} alt="video" />
      <span>{d.value.image.caption}</span>
    </button>
  )

  openYouTube = () => {
    const { idVideo } = this.state;
    window.open(`https://www.youtube.com/watch?v=${idVideo}`);
  }

  onClickVideo = (id) => {
    this.setState({ idVideo: id });
    const ele = document.getElementById('id-block-video');
    if (ele && isMobile) {
      ele.scrollIntoView({ behavior: 'smooth' });
    }
  }

  render() {
    const { buttonBlocks, videoBlocks } = this.props;
    const { idVideo } = this.state;
    const opts = {
      width: isMobile ? window.innerWidth : `${(window.innerWidth - 220) * 0.74}`,
      height: isMobile ? window.innerWidth * 9 / 16 : `${(window.innerWidth - 220) * 0.74 * 9 / 16}`,
      playerVars: {
        autoplay: 0,
      },
    };
    const settings = {
      dots: false,
      infinite: false,
      slidesToShow: isMobile ? 2 : 4,
      slidesToScroll: 1,
      arrows: true,
      pauseOnHover: false,
    };
    console.log('videoBlocks', videoBlocks);
    const learnToBt = getNameFromButtonBlock(buttonBlocks, 'learn_to_mix');
    const channelBt = getNameFromButtonBlock(buttonBlocks, 'CHANNEL');
    const mixYourBt = getNameFromButtonBlock(buttonBlocks, 'mix_your');
    const learnHow = getNameFromButtonBlock(buttonBlocks, 'learn_how');
    const viewOnBt = getNameFromButtonBlock(buttonBlocks, 'View_On');
    return (
      <div id="id-block-video" className="div-block-list-video">
        <h3>
          {learnToBt}
        </h3>
        <div className="div-video">
          {/* <div className={isMobile ? 'div-area-temp' : 'hidden'} /> */}
          <div className="video">
            <YouTube videoId={idVideo} opts={opts} onReady={this._onReady} />
          </div>
          <div className="div-info">
            <div className="div-text">
              <div className="div-image">
                <img src={icLogo} alt="logo" />
                <span>
                  {channelBt}
                </span>
              </div>
              <h4>
                {mixYourBt}
              </h4>
              <span>
                {learnHow}
              </span>
            </div>
            <button type="button" className="bt-youtube" onClick={this.openYouTube}>
              {viewOnBt}
              <img src={icYoutube} alt="youtube" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default BlockListVideo;
