import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import iconRemove from '../../image/icon/ic-delete-scent.svg';
import iconNext from '../../image/icon/ic-next-mix.svg';
import { isMobile } from '../../DetectScreen/detectIFrame';
import { getNameFromButtonBlock } from '../../Redux/Helpers';
import icInfo from '../../image/icon/icNewInfo.svg';

function ItemScentSelect(props) {
  const onClickRemove = (e) => {
    e.stopPropagation();
    props.onClickRemove(props.data);
  };

  const onClickChooseScent = () => {
    props.onClickChooseScent(props.data);
  };

  const onClickrecommend = (data) => {
    console.log('onClickrecommend', data);
    if (props.onClickrecommend) {
      props.onClickrecommend(data);
    }
  };
  const recommendBt = getNameFromButtonBlock(props.buttonBlocks, 'RECOMMENDED');
  const recommendHtml = (
    props.recommendData && (
      <div className="recommend-data">
        <h3>{recommendBt.replace('{scent_name}', props.recommendDataOfScent)}</h3>
        <div className="list-scent">
          {_.map(props.recommendData, d => (
            <div
              className="image-content"
              onClick={(e) => {
                e.stopPropagation();
                onClickrecommend(d);
              }}
            >
              <img className="scent" src={d.image} alt="icon" />
              <img className="info" src={icInfo} alt="info" />
            </div>
          ))}
        </div>
      </div>
    )
  );

  return (
    <div
      className="item-scent-select"
      onClick={onClickChooseScent}
    >
      <div className="item-scent-select-body">
        <div className="image-scent">
          <img src={props.image} alt="scent" className={props.isShowDelete ? '' : 'icon'} />
        </div>
        <div className="name-scent div-col">
          <div className="div-row items-center">
            <h3>
              {props.title}
            </h3>
            <button
              onClick={onClickRemove}
              type="button"
              className={props.isShowDelete ? 'button-bg__none' : 'hidden'}
            >
              <img src={iconRemove} alt="remove" />
            </button>
            <span>
              {props.message}
            </span>
          </div>
          <span>
            {props.description}
          </span>
        </div>
        {/* {!isMobile && recommendHtml} */}
        <img src={iconNext} alt="next" />
      </div>
      {props.recommendData && <div className="line" style={{ marginTop: '15px' }} />}
      {recommendHtml}
    </div>
  );
}

export default ItemScentSelect;
