import React, { Component } from 'react';
import '../../styles/bottle-scent-v2.scss';
import icBottle from '../../image/bottle-custome.gif';
import { getNameFromButtonBlock } from '../../Redux/Helpers';

export default class BottleScentV2 extends Component {
  render() {
    const { image1, image2, buttonBlocks } = this.props;

    const st1Bt = getNameFromButtonBlock(buttonBlocks, '1ST');
    const nd2Bt = getNameFromButtonBlock(buttonBlocks, '2ND');

    return (
      <div className="div-bottle-scent-v2">
        <img className="ic-bottle" src={icBottle} alt="bottle" />
        <div className="selection-scent">
          <div className={`scent scent-top ${image1 ? 'none-border-full' : (!image1 && !image2) ? '' : 'none-border'}`} style={image1 ? { backgroundImage: `url(${image1})` } : {}}>
            <span className={image1 ? 'hidden' : ''}>
              {st1Bt}
            </span>
          </div>
          <div className={`scent scent-bottom ${image2 ? 'none-border-full' : 'none-border'}`} style={image2 ? { backgroundImage: `url(${image2})` } : {}}>
            <span className={image2 ? 'hidden' : ''}>
              {nd2Bt}
            </span>
          </div>
        </div>
      </div>
    );
  }
}
