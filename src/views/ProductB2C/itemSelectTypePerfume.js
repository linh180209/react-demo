import React from 'react';
import classnames from 'classnames';

function ItemSelectTypePerfume(props) {
  return (
    <div
      className={classnames('item-select-type-perfume', props.className)}
      onClick={props.onClick}
    >
      <div>
        <img loading="lazy" src={props.image} alt="bottle" />
        <div className="item-text tc">
          <h3>
            {props.title}
          </h3>
          <span>
            {props.des}
          </span>
          <h4>
            {props.price}
          </h4>
        </div>
      </div>
    </div>
  );
}

export default ItemSelectTypePerfume;
