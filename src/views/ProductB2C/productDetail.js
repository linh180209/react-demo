/* eslint-disable react/sort-comp */
import Drawer from '@mui/material/Drawer';
import classnames from 'classnames';
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { Col, Row } from 'react-bootstrap';
import { isIOS } from 'react-device-detect';
import 'react-image-crop/dist/ReactCrop.css';
import 'react-image-gallery/styles/css/image-gallery.css';
import { Modal, ModalBody } from 'reactstrap';
import CardKnowMore from '../../components/CardKnowMore';

import '../../../node_modules/slick-carousel/slick/slick-theme.css';
import '../../../node_modules/slick-carousel/slick/slick.css';
import {
  addFontCustom, generateUrlWeb, getCmsCommon, getLinkFromButtonBlock, getNameFromButtonBlock, getSearchPathName, onClickProduct, scrollTop, segmentBestSellerProductViewInitiated,
} from '../../Redux/Helpers';
import '../../styles/product-detail.scss';

import VideoAcademy from '../../components/AcademyItem/videoAcademy';
import CustomeBottleV4 from '../../componentsv2/customeBottleV4';
import PickIngredientV3 from '../../componentsv2/pickIngredientV3';
import PremadeProductV3 from '../../componentsv2/premadeProductV3';
import SelectScentV2 from '../../componentsv2/selectScentV2';
import {
  GET_ALL_SCENTS_PRODUCTS_CANDLE, GET_ALL_SCENTS_PRODUCTS_DUAL_CRAYONS, GET_ALL_SCENTS_PRODUCTS_HOME_SCENTS, GET_ALL_SCENTS_PRODUCTS_MINI_CANDLE, GET_ALL_SCENTS_PRODUCTS_OIL_BURNER, GET_ALL_SCENTS_PRODUCTS_PERFUME, GET_ALL_SCENTS_PRODUCTS_PERFUME_DIY, GET_ALL_SCENTS_PRODUCTS_REED_DIFFUSER, GET_ALL_SCENTS_PRODUCTS_WAX, GET_BEST_SELLER_PERFUME, GET_PRODUCT_FOR_CART, GET_SCENTED_CANDLES, GET_SCENTED_CAR_DIFFUSER, GET_SCENTED_DUAL_CRAYONS, GET_SCENTED_HOME, GET_SCENTED_MINI_CANDLES, GET_SCENTED_OIL_BURNER, GET_SCENTED_PERFUME_DIY, GET_SCENTED_REED_DIFFUSER,
} from '../../config';
import { COLOR_CUSTOME, FONT_CUSTOME } from '../../constants';
import {
  isBrowser, isMobile, isMobile767fn, isMobile991fn,
} from '../../DetectScreen';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import ImageBlock from './imageBlock';
import TextBlockV2 from './textBlockV2';

const preDataGroup = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { tags } = x;
    _.forEach(tags, (tag) => {
      const group = _.find(dataScents, d => d.tag === tag.name);
      if (group) {
        group.datas.push(x);
      } else {
        dataScents.push({
          tag: tag.name,
          group: tag.group,
          datas: [x],
        });
      }
    });
  });
  return dataScents;
};

const preDataAlphabet = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { name } = x;
    const firstChart = name.charAt(0);
    const group = _.find(dataScents, d => d.tag.toLowerCase() === firstChart.toLowerCase());
    if (group) {
      group.datas.push(x);
    } else {
      dataScents.push({
        tag: firstChart,
        datas: [x],
      });
    }
  });
  return dataScents;
};

const preDataScents = (scents) => {
  const dataScents = [];

  dataScents.push({
    datas: _.orderBy(preDataGroup(scents), ['tag'], ['asc']),
    title: ['Scent Family', 'Gender', 'mood', 'Scent Note'],
  });

  dataScents.push({
    datas: _.orderBy(preDataAlphabet(scents), ['tag'], ['asc']),
    title: ['Alphabet'],
  });

  dataScents.push({
    datas: [{
      tag: '',
      datas: _.orderBy(scents, ['popularity'], ['desc']),
    }],
    title: ['Popularity'],
  });
  return dataScents;
};

const addScentNoteIntoScents = (scents, scentNotes) => {
  const scentClones = _.cloneDeep(scents);
  _.forEach(scentNotes, (d) => {
    const { name, tags } = d;
    _.forEach(tags, (tag) => {
      const { products } = tag;
      _.forEach(products, (product) => {
        const ele = _.find(scentClones, x => x.id === product.id);
        if (ele) {
          _.assign(ele, product);
          ele.tags.push({
            name,
            group: 'Scent Note',
          });
        }
      });
    });
  });
  return scentClones;
};

const getDataCustome = (propsState) => {
  let name;
  let imageCustome;
  let isCustome;
  let font = FONT_CUSTOME.JOST;
  let idCartItem;
  let quantity;
  let color = COLOR_CUSTOME.BLACK;
  if (propsState) {
    const { custome } = propsState;
    if (custome) {
      name = custome.name;
      imageCustome = custome.image;
      isCustome = custome.isCustome;
      font = custome.font;
      color = custome.color;
      idCartItem = custome.idCartItem;
      quantity = custome.quantity;
    }
  }
  return {
    name, imageCustome, isCustome, font, idCartItem, quantity, color,
  };
};

class ProductDetail extends PureComponent {
  constructor(props) {
    super(props);
    this.images = [];
    const {
      name, isCustome, font, color, idCartItem, quantity,
    } = getDataCustome(
      props.propsState,
    );
    console.log('quantity', quantity, _.isEmpty(quantity));
    this.state = {
      isShowKnowMore: false,
      isShowViewFullImage: false,
      dataPopup: {
        title: '',
        content: '',
        imageKnowMore: '',
      },
      nameBottle: name,
      arrayThumbs: [],
      currentImg: '',
      isShowCustom: isCustome,
      font,
      color,
      idCartItem,
      quantity: quantity || 1,
      scentedHomes: [],
      scentedDualCrayons: [],
      scentedCandles: [],
      scentedReedDiffUser: [],
      scentedCarDiffUser: [],
      scentedMiniCandle: [],
      scentedOilBurner: [],
      scentedPerfumeDiy: [],
      bestSeller: [],
      dataReview: {},
      isShowSelectScent: false,
      dataScentSelection: {},
      type: props.type,
      indexSelect: 1,
      isChangeType: false,
      variantValues: '',
    };
    this.refCustome = React.createRef();
    this.waxPerfume = {};
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { propsState, basket, type } = nextProps;
    if (propsState && JSON.stringify(propsState) !== JSON.stringify(prevState.propsState)) {
      const {
        name, imageCustome, isCustome, font, color, idCartItem, quantity,
      } = getDataCustome(
        propsState,
      );
      const isShowCustom = isCustome;
      const nameBottle = name;
      const isCroped = true;

      _.assign(objectReturn, {
        font,
        color,
        propsState,
        isShowCustom,
        nameBottle,
        isCroped,
        currentImg: imageCustome,
        idCartItem,
        quantity: quantity || 1,
      });
      _.assign(objectReturn, { propsState });
    }

    // close pick scents when change type
    if (type !== prevState.type) {
      _.assign(objectReturn, { type, isShowSelectScent: false, isChangeType: true });
    }

    const {
      scents, scentsWax, scentsCandle, scentsHome, scentsDualCrayon,
      scentsDiy, scentNotes, scentsMiniCandle, scentsOilBurner, scentsReedDiffuser,
    } = nextProps;
    if (scents !== prevState.scents && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scents?.length > 0) {
      const dataScentsPerfume = preDataScents(addScentNoteIntoScents(scents, scentNotes));
      _.assign(objectReturn, { scents, dataScentsPerfume });
    }
    if (scentsDiy !== prevState.scentsDiy && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsDiy?.length > 0) {
      const dataScentsDiyPerfume = preDataScents(addScentNoteIntoScents(scentsDiy, scentNotes));
      _.assign(objectReturn, { scentsDiy, dataScentsDiyPerfume });
    }
    if (scentsWax !== prevState.dataScentsWax && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsWax?.length > 0) {
      const dataScentsWax = preDataScents(addScentNoteIntoScents(scentsWax, scentNotes));
      _.assign(objectReturn, { scentsWax, dataScentsWax });
    }
    if (scentsCandle !== prevState.scentsCandle && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsCandle?.length > 0) {
      const dataScentsCandle = preDataScents(addScentNoteIntoScents(scentsCandle, scentNotes));
      _.assign(objectReturn, { scentsCandle, dataScentsCandle });
    }
    if (scentsMiniCandle !== prevState.scentsMiniCandle && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsMiniCandle?.length > 0) {
      const dataScentsMiniCandle = preDataScents(addScentNoteIntoScents(scentsMiniCandle, scentNotes));
      _.assign(objectReturn, { scentsMiniCandle, dataScentsMiniCandle });
    }
    if (scentsHome !== prevState.scentsHome && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsHome?.length > 0) {
      const dataScentsHome = preDataScents(addScentNoteIntoScents(scentsHome, scentNotes));
      _.assign(objectReturn, { scentsHome, dataScentsHome });
    }
    if (scentsDualCrayon !== prevState.scentsDualCrayon && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsDualCrayon?.length > 0) {
      const dataScentsDualCrayon = preDataScents(addScentNoteIntoScents(scentsDualCrayon, scentNotes));
      _.assign(objectReturn, { scentsDualCrayon, dataScentsDualCrayon });
    }
    if (scentsOilBurner !== prevState.scentsOilBurner && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsOilBurner?.length > 0) {
      const dataScentsOilBurner = preDataScents(addScentNoteIntoScents(scentsOilBurner, scentNotes));
      _.assign(objectReturn, { scentsDualCrayon, dataScentsOilBurner });
    }
    if (scentsReedDiffuser !== prevState.scentsReedDiffuser && scentNotes !== prevState.scentNotes && scentNotes?.length > 0 && scentsReedDiffuser?.length > 0) {
      const dataScentsReedDiffuser = preDataScents(addScentNoteIntoScents(scentsReedDiffuser, scentNotes));
      _.assign(objectReturn, { scentsDualCrayon, dataScentsReedDiffuser });
    }

    return !_.isEmpty(objectReturn) ? objectReturn : {};
  }

    scrollToSample = () => {
      const element = document.getElementById('idSample');
      if (element) {
        element.scrollIntoView({ behavior: 'smooth' });
      }
    };

    updateColorCandle = (variantValues, images) => {
      if (images?.length > 0) {
        _.remove(this.images, x => x.variantImage === true);
        _.forEach(images, d => (
          this.images.unshift({
            original: d.image,
            thumbnail: d.image,
            variantImage: true,
          })
        ));
      }
      this.setState({ variantValues });
    }

    addToCartItem = (select) => {
      const index = _.findIndex(this.props.data.items, x => x.id === select.id);
      const data = _.cloneDeep(this.props.data);
      if (index > -1) {
        const d = data.items[index];
        data.name = `${d.name} ${d.variant_values && d.variant_values.length > 0 ? `(${d.variant_values[0]})` : ''}`;
      }
      this.handleSubmitAddToCart(data, index);
    }

    updateWaxPerfume = (data) => {
      this.waxPerfume = data;
      this.generateData(this.props.data);
    }

    onSaveCustomer = (data) => {
      const {
        image, name, color, font,
      } = data;
      if (_.isEmpty(image)) {
        this.setState({
          currentImg: undefined,
          nameBottle: name,
          font,
          color,
        });
        return;
      }
      this.setState({
        currentImg: image,
        nameBottle: name,
        font,
        color,
      });
    }

    setDataReview = (data) => {
      this.setState({ dataReview: data });
    }

    fetchScentedHomes = () => {
      const option = {
        url: GET_SCENTED_HOME,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedHomes: response });
      });
    }

    fetchScentedDualCrayons = () => {
      const option = {
        url: GET_SCENTED_DUAL_CRAYONS,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedDualCrayons: response });
      });
    }

    fetchScentedCanldes = () => {
      const option = {
        url: GET_SCENTED_CANDLES,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedCandles: response });
      });
    }

    fetchBestSeller = () => {
      const option = {
        url: GET_BEST_SELLER_PERFUME,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ bestSeller: response });
      });
    }

    fetchScentedReedDiffUser = () => {
      const option = {
        url: GET_SCENTED_REED_DIFFUSER,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedReedDiffUser: response });
      });
    }

    fetchScentedCarDiffUser = () => {
      const option = {
        url: GET_SCENTED_CAR_DIFFUSER,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedCarDiffUser: response });
      });
    }

    fetchScentedMiniCandle = () => {
      const option = {
        url: GET_SCENTED_MINI_CANDLES,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedMiniCandle: response });
      });
    }

    fetchScentedOilBurner = () => {
      const option = {
        url: GET_SCENTED_OIL_BURNER,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedOilBurner: response });
      });
    }

    fetchScentedPerfumeDiy = () => {
      const option = {
        url: GET_SCENTED_PERFUME_DIY,
        method: 'GET',
      };
      fetchClient(option).then((response) => {
        this.setState({ scentedPerfumeDiy: response });
      });
    }

    removeCustomization = () => {
      this.setState({
        currentImg: undefined,
        nameBottle: undefined,
        font: FONT_CUSTOME.JOST,
        color: COLOR_CUSTOME.BLACK,
      });
    }

    prepareDataItemCart = async (dataSelect, index = 0) => {
      const { pathname, search } = window.location;
      const {
        basket,
        type,
      } = this.props;
      const urlPro = pathname !== '/products-questionnaire' ? `${pathname}${search || ''}` : undefined;
      const { items, combo } = dataSelect;
      const itemCombo = items && items.length > index ? items[index] : undefined;
      const itemBottle = _.find(combo || [], x => x.product.type === 'Bottle');
      const typePro = type ? (type === 'elixir' ? 'unisex' : null) : null;
      const imageBottle = itemBottle
        ? _.find(itemBottle.product.images, x => x.type === typePro)
        : undefined;
      const bottleImage = this.state.currentImg
        ? await fetch(this.state.currentImg).then(r => r.blob())
        : undefined;
      const imagePremade = _.find(this.state.arrayThumbs, x => x.image === this.state.currentImg)?.id;
      if (itemCombo) {
        console.log('itemCombo==', itemCombo);
        const { price, id, is_featured: isFeatured } = itemCombo;
        const data = {
          id: this.state.idCartItem,
          is_featured: isFeatured,
          item: id,
          type: this.state.type,
          font: this.state.font,
          color: this.state.color,
          is_display_name: (!bottleImage && !!this.state.nameBottle) || !!this.state.nameBottle,
          is_customized: !!bottleImage || !!imagePremade,
          name: this.state.nameBottle ? this.state.nameBottle : dataSelect.name,
          price,
          quantity: this.state.quantity,
          total: bottleImage
            ? parseFloat(price) * this.state.quantity + this.itemBottle.price
            : parseFloat(price) * this.state.quantity,
          image_urls: imageBottle ? [imageBottle.image] : undefined,
          file: bottleImage
            ? new File([bottleImage], 'product.png')
            : undefined,
          imagePremade,
          meta: {
            url: urlPro,
          },
        };
        const dataTemp = {
          idCart: basket.id,
          item: data,
        };
        return dataTemp;
      }
    };

    handleSubmitAddToCart = async (dataSelect, index = 0) => {
      const {
        createBasketGuest,
        addProductBasket,
        basket,
      } = this.props;
      const { items } = dataSelect;
      if (_.isEmpty(items[0]) || !items[0].id) {
        toastrError('Please create a product');
        return;
      }
      const dataTemp = await this.prepareDataItemCart(dataSelect, index);
      if (!basket.id) {
        createBasketGuest(dataTemp);
      } else {
        addProductBasket(dataTemp);
      }
    };

    handleUpdateCart = async (dataSelect, index = 0) => {
      const {
        updateProductBasket,
      } = this.props;
      const dataTemp = await this.prepareDataItemCart(dataSelect, index);
      if (this.state.idCartItem) {
        _.assign(dataTemp, { isUpdateFromProductDetail: true });
        updateProductBasket(dataTemp);
      }
    }

    fetchScentItem = (type) => {
      if (type === 'Perfume' || type === 'bundle_creation' || type === 'car_diffuser') {
        this.fetchAllScent();
      } else if (type === 'perfume_diy') {
        this.fetchAllScentDiy();
      } else if (type === 'Wax_Perfume') {
        this.fetchAllScentWax();
      } else if (['single_candle', 'dual_candles'].includes(type)) {
        this.fetchAllScentCandle();
      } else if (type === 'mini_candles') {
        this.fetchAllScentMiniCandle();
      } else if (type === 'home_scents') {
        this.fetchAllScentHome();
      } else if (type === 'dual_crayons') {
        this.fetchAllDualCrayonScent();
      } else if (type === 'oil_burner') {
        this.fetchAllOilBurnerScent();
      } else if (type === 'reed_diffuser') {
        this.fetchAllReedDiffuserScent();
      }
    }

    componentDidMount = () => {
      const { data } = this.props;
      this.generateData(data);
      this.handleItemDelete();

      let dataScentsPerfume = [];
      let dataScentsWax = [];
      let dataScentsCandle = [];
      let dataScentsMiniCandle = [];
      let dataScentsHome = [];
      let dataScentsDualCrayon = [];
      let dataScentsDiyPerfume = [];
      let dataScentsOilBurner = [];
      let dataScentsReedDiffuser = [];
      const { type } = data;
      this.fetchScentItem(type.name);

      const {
        scents, scentsWax, scentsCandle, scentsHome, scentsDualCrayon,
        scentsDiy, scentNotes, scentsMiniCandle, scentsOilBurner, scentsReedDiffuser,
      } = this.props;
      dataScentsPerfume = preDataScents(addScentNoteIntoScents(scents, scentNotes));
      dataScentsWax = preDataScents(addScentNoteIntoScents(scentsWax, scentNotes));
      dataScentsCandle = preDataScents(addScentNoteIntoScents(scentsCandle, scentNotes));
      dataScentsMiniCandle = preDataScents(addScentNoteIntoScents(scentsMiniCandle, scentNotes));
      dataScentsHome = preDataScents(addScentNoteIntoScents(scentsHome, scentNotes));
      dataScentsDualCrayon = preDataScents(addScentNoteIntoScents(scentsDualCrayon, scentNotes));
      dataScentsDiyPerfume = preDataScents(addScentNoteIntoScents(scentsDiy, scentNotes));
      dataScentsOilBurner = preDataScents(addScentNoteIntoScents(scentsDiy, scentsOilBurner));
      dataScentsReedDiffuser = preDataScents(addScentNoteIntoScents(scentsDiy, scentsReedDiffuser));
      this.setState({
        dataScentsPerfume,
        dataScentsWax,
        dataScentsCandle,
        dataScentsHome,
        dataScentsDualCrayon,
        dataScentsDiyPerfume,
        dataScentsMiniCandle,
        dataScentsOilBurner,
        dataScentsReedDiffuser,
      });
    };

    handleItemDelete = () => {
      if (this.props.basket && this.state.idCartItem) {
        if (!_.find(this.props.basket.items, x => x.id === this.state.idCartItem)) {
          this.props.propsState.custome.idCartItem = undefined;
          this.setState({ idCartItem: undefined, isShowCustom: false });
        }
      }
    }

    componentDidUpdate(prevProps, prevState) {
      // console.log('===product Detail====');
      const { data, type } = this.props;
      const { isChangeType } = this.state;
      if (JSON.stringify(data) !== JSON.stringify(prevProps.data)) {
        this.generateData(data);
        // console.log('===product Detail==== 1');
      }
      if (isChangeType) {
        this.state.isChangeType = false;
        this.fetchScentItem(type);
        // console.log('===product Detail==== 2');
      }
    }

    onChangeQuantity = (e) => {
      const { value } = e.target;
      const quantity = parseInt(value, 10);
      this.setState({ quantity });
    };

    onClickAddtoCart = async () => {
      const { data } = this.props;
      if (this.state.variantValues) {
        const index = _.findIndex(data?.items, x => x.variant_values[0] === this.state.variantValues);
        if (index > -1) {
          this.handleSubmitAddToCart(data, index);
          return;
        }
      }
      this.handleSubmitAddToCart(data);
    };

    onClickUpdatetoCart = async () => {
      const { data } = this.props;
      this.handleUpdateCart(data);
    }

    onCloseKnowMore = () => {
      this.setState({ isShowKnowMore: false });
    };

    generateData = (data) => {
      this.thumbs = [];
      this.images = [];
      const {
        combo, name, type, items, displayName,
      } = data;
      const { videos } = this.props.dataFAQ;

      const imageType = items[0]?.is_best_seller ? _.find(items[0]?.images, x => x.type === 'main')?.image : undefined;
      if (imageType) {
        this.images.push({
          original: imageType,
          thumbnail: imageType,
        });
      }

      const bottles = _.find(combo, x => x.product.type === 'Bottle');
      const products = _.filter(combo, x => x.product.type !== 'Bottle');

      // add bottome image when product do NOT add name scent on bottle
      const creationScentInital = products.length === 0 || ['Wax_Perfume', 'dual_crayons'].includes(type.name);

      // add bottle custom
      if (type.name === 'bundle_creation') {
        // this.images.push({
        //   original: bottleBundleCreation,
        //   thumbnail: bottleBundleCreation,
        //   isBundleCreation: true,
        // });
      }
      if (!this.props.isBestSeller && (type.name === 'Perfume' || type.name === 'bundle_creation' || type.name === 'perfume_diy')) {
        if (items[0].isSample) {
          // this.images.push({
          //   original: bottleRollon,
          //   thumbnail: bottleRollon,
          //   isSample: true,
          // });
        } else {
          this.images.push({
            dataCustomBottle: {},
          });
        }
      } else if (type.name === 'dual_crayons') {
        // this.images.push({
        //   original: bottleDualCaryon,
        //   thumbnail: bottleDualCaryon,
        //   isDualCrayons: true,
        // });
      } else if (type.name === 'dual_candles') {
        // this.images.push({
        //   original: candle3,
        //   thumbnail: candle3,
        //   isDualCandles: true,
        // });
      } else if (type.name === 'single_candle') {
        // this.images.push({
        //   original: singleCandle,
        //   thumbnail: singleCandle,
        //   isDualCandles: true,
        // });
      } else if (type.name === 'Wax_Perfume') {
        // this.images.push({
        //   original: solidBottle,
        //   thumbnail: solidBottle,
        //   isWaxPerfume: true,
        // }, {
        //   original: waxImage,
        //   thumbnail: waxImage,
        // });
      } else if (!this.props.isBestSeller && type.name === 'home_scents') {
        this.images.push({
          dataCustomBottle: {},
          isHomeScent: true,
        });
      }
      // else if (type.name === 'reed_diffuser') {
      //   this.images.push({
      //     original: diffUser,
      //     thumbnail: diffUser,
      //     isReedDiffuser: true,
      //   });
      // }
      this.nameProduct = this.props.datas.length > 1 ? data.shortTitle : (displayName || name);
      const imageProduct = bottles && bottles.product.images
        ? _.filter(bottles.product.images, (x) => {
          if (x) {
            if (x.type === null) {
              return true;
            } if (x.type === 'suggestion') {
              this.thumbs.push(x);
            }
          }
          return false;
        })
        : [];
      this.setState({
        arrayThumbs: this.thumbs,
      });
      // if (['gift_bundle', 'Perfume', 'Elixir', 'dual_candles', 'single_candle', 'home_scents', 'dual_crayons', 'bundle_creation', 'reed_diffuser', 'perfume_diy'].includes(type.name)) {
      //   _.forEach(products, (d) => {
      //     const i = _.filter(d.product.images, x => x.type === null);
      //     imageProduct = imageProduct.concat(i);
      //   });
      // }

      // move image hardcode to bottom
      const imageFirst = this.images?.length ? this.images[0] : undefined;
      if (creationScentInital) {
        this.images = [];
      }
      _.forEach(imageProduct, (x) => {
        const i = {
          original: x.image,
          thumbnail: x.image,
        };
        this.images.push(i);
      });
      if (creationScentInital && imageFirst) {
        this.images.push(imageFirst);
      }

      _.forEach(videos, (x) => {
        const i = {
          original: x.placeholder,
          thumbnail: x.placeholder,
          embedUrl: x.video,
        };
        this.images.push(i);
      });


      // if wax only display in items image
      if (!_.isEmpty(this.waxPerfume)) {
        const { images } = this.waxPerfume;
        _.forEach(_.filter(images || [], d => d.type === null), (x) => {
          const i = {
            original: x.image,
            thumbnail: x.image,
          };
          this.images.push(i);
        });
      }
      this.forceUpdate();
    };

    isActive = (shortTitle) => {
      const { data } = this.props;
      return shortTitle === data.shortTitle;
    };

    handleActiveImgOnBottle = (img) => {
      // this.refCustome.current.handleActiveImgOnBottle(img);
      this.setState({ currentImg: img });
    };

    handleChangeNameBottle = (text) => {
      this.refCustome.current.setNameBottle(text);
      this.setState({ nameBottle: text });
    }

    handleCropImage = (img) => {
      this.setState({ currentImg: img });
    }

    toggleCustom = () => {
      const { isShowCustom } = this.state;
      if (!isShowCustom) {
        this.handleItemDelete();
      }
      this.setState({ isShowCustom: !isShowCustom });
    }

    addRemoveClass = (addClass, removeClass) => {
      const element = document.getElementById('div-ingredient-detail');
      if (element && removeClass) {
        element.classList.remove(removeClass);
      }
      const arr = element.className.split(' ');
      if (arr.indexOf(addClass) === -1) {
        element.className += ` ${addClass}`;
      }
    }

    onClickGotoProduct = (product) => {
      const url = `/product/best_seller/${product.product}`;
      if (url) {
        segmentBestSellerProductViewInitiated(product);
        this.props.history.push(generateUrlWeb(url));
        scrollTop();
      }
    }

    onClickChooseScent = (data, index, isOnlyShowIngredientLocal = false) => {
      const { buttonBlocks } = this.props;
      const key = index === 1 ? '1ST INGREDIENT' : index === 2 ? '2ND INGREDIENT' : index === 3 ? '3RD INGREDIENT' : '4TH INGREDIENT';
      const dataScentSelection = {
        subTitle: getNameFromButtonBlock(buttonBlocks, key),
        index,
      };
      if (data) {
        _.assign(dataScentSelection, data.product);
      }

      this.setState({
        dataScentSelection,
        isShowSelectScent: true,
        indexSelect: index,
        isOnlyShowIngredientLocal,
      });
    }

    fetchAllScentWax = () => {
      const { scentsWax } = this.props;
      if (scentsWax && scentsWax.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_WAX,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsWaxToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllDualCrayonScent = () => {
      const { scentsDualCrayon } = this.props;
      if (scentsDualCrayon && scentsDualCrayon.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_DUAL_CRAYONS,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsDualCrayonsToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllOilBurnerScent = () => {
      const { scentsOilBurner } = this.props;
      if (scentsOilBurner && scentsOilBurner.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_OIL_BURNER,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsOilBurnerToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllReedDiffuserScent = () => {
      const { scentsReedDiffuser } = this.props;
      if (scentsReedDiffuser && scentsReedDiffuser.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_REED_DIFFUSER,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsReedDifffUserToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllScent = () => {
      const { scents } = this.props;
      if (scents && scents.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_PERFUME,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllScentDiy = () => {
      const { scentsDiy } = this.props;
      if (scentsDiy && scentsDiy.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_PERFUME_DIY,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsDiyToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllScentHome = () => {
      const { scentsHome } = this.props;
      if (scentsHome && scentsHome.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_HOME_SCENTS,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsHomeToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllScentCandle = () => {
      const { scentsCandle } = this.props;
      if (scentsCandle && scentsCandle.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_CANDLE,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsCandleToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    fetchAllScentMiniCandle = () => {
      const { scentsMiniCandle } = this.props;
      if (scentsMiniCandle && scentsMiniCandle.length > 0) {
        return;
      }
      const option = {
        url: GET_ALL_SCENTS_PRODUCTS_MINI_CANDLE,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.props.addScentsMiniCandleToStore(result);
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        this.props.loadingPage(false);
        toastrError(error.message);
      });
    }

    getProduct = async (scents, type) => {
      const url = `${GET_PRODUCT_FOR_CART}?combo=${scents[0].id}${scents.length > 1 ? `,${scents[1].id}` : ''}${scents.length > 2 ? `,${scents[2].id}` : ''}${scents.length > 3 ? `,${scents[3].id}` : ''}&type=${type}`;
      const option = {
        url,
        method: 'GET',
      };
      return fetchClient(option);
    }

    getProductCart = async (scents, isShowRollOn, isSolidPerfume, isDualCandles, isShowHome, isDualCrayons, isBundleCreation, isReedDiffuser, isPerfumeDiy, isCarDiffuser, isMiniCandle, isOilBurner) => {
      try {
        const type = isOilBurner ? 'oil_burner' : isMiniCandle ? 'mini_candles' : isCarDiffuser ? 'car_diffuser' : isPerfumeDiy ? 'perfume_diy' : isReedDiffuser ? 'reed_diffuser' : isBundleCreation ? 'bundle_creation' : isDualCrayons ? 'dual_crayons' : isDualCandles ? (scents.length > 1 ? 'dual_candles' : 'single_candle') : isSolidPerfume ? 'wax_perfume' : isShowHome ? 'home_scents' : (scents.length > 1 ? 'perfume' : isShowRollOn ? 'scent' : 'perfume');
        const product = await this.getProduct(scents, type);
        return product;
      } catch (error) {
        console.error(error.mesage);
      }
    }

    generateProductCart = async (scents, type) => {
      try {
        const product = await this.getProduct(scents, type);
        return product;
      } catch (error) {
        console.error(error.mesage);
      }
    }

    onClickGotoPerfumeProductFromCrayons = async (combo) => {
      const scents = _.map(combo, d => (
        {
          id: d?.product?.combo[0]?.product?.id,
        }
      ));
      if (scents.length === 0) {
        this.props.history.push(generateUrlWeb('/product/creation_perfume'));
      } else {
        // const product = await this.getProductCart(scents, false, false, false, false);
        const product = await this.generateProductCart(scents, 'perfume');
        this.props.history.push(generateUrlWeb(`/product/Perfume/${product.id}`));
      }
    }

    onClickApply = async (data, index) => {
      let scents = [];
      const { type } = this.props.data;
      if (!data) {
        const cloneData = _.cloneDeep(this.combosState);
        scents = _.map(cloneData, x => x.product);
        // if (this.combosState && this.combosState.length > 0) {
        //   scents.push(this.combosState[0].product);
        // }
      } else if (index === 3 || index === 4) {
        if (this.combosState && this.combosState.length > 0) {
          scents.push(this.combosState[0].product);
        }
        if (this.combosState && this.combosState.length > 1) {
          scents.push(this.combosState[1].product);
        }
        if (this.combosState && this.combosState.length > 2) {
          scents.push(this.combosState[2].product);
        }
        scents.push(data);
      } else if (index === 1) {
        scents.push(data);
        if (this.combosState && this.combosState.length > 1) {
          scents.push(this.combosState[1].product);
        }
      } else if (this.combosState && this.combosState.length > 0) {
        scents.push(this.combosState[0].product);
        scents.push(data);
      } else {
        scents.push(data);
      }
      if (type.name === 'Wax_Perfume') {
        this.setState({ isShowSelectScent: false });
        if (scents.length === 0) {
          this.props.history.push(generateUrlWeb('/product/wax'));
        } else {
          this.props.loadingPage(true);
          const product = await this.generateProductCart(scents, 'wax_perfume');
          this.props.history.push(generateUrlWeb(`/product/wax/${product.id}`));
        }
      } else if (type.name === 'home_scents') {
        this.setState({ isShowSelectScent: false });
        if (scents.length === 0) {
          this.props.history.push(generateUrlWeb('/product/home_scents'));
        } else {
          this.props.loadingPage(true);
          const product = await this.generateProductCart(scents, 'home_scents');
          this.props.history.push(generateUrlWeb(`/product/home_scents/${product.id}`));
        }
      } else if (['single_candle', 'dual_candles'].includes(type.name)) {
        this.setState({ isShowSelectScent: false });
        const formatScents = _.map(scents, d => (d.combo ? d.combo[0].product : d));
        if (formatScents.length === 0) {
          this.props.history.push(generateUrlWeb(window.localStorage.pathOldProduct || '/product/dual_candles'));
        } else {
          this.props.loadingPage(true);
          const product = await this.generateProductCart(formatScents, formatScents.length > 1 ? 'dual_candles' : 'single_candle');
          if (window.location.pathname === generateUrlWeb('/product/dual-candles')
          || window.location.pathname === generateUrlWeb('/product/dual_candles')) {
            window.localStorage.pathOldProduct = '/product/dual_candles';
          } else if (window.location.pathname === generateUrlWeb('/product/single-candle')
          || window.location.pathname === generateUrlWeb('/product/single_candle')) {
            window.localStorage.pathOldProduct = '/product/single-candle';
          }
          // this.props.history.push(generateUrlWeb(formatScents.length > 1 ? `/product/dual_candles/${product.id}` : `/product/single-candle/${product.id}`));
          const { pathname, search } = getSearchPathName(generateUrlWeb(formatScents.length > 1 ? `/product/dual_candles/${product.id}` : `/product/single-candle/${product.id}`));
          this.props.history.push({
            pathname,
            search,
            state: {
              sortId: _.map(formatScents, x => x.id),
            },
          });
        }
      } else if (type.name === 'Perfume') {
        this.setState({ isShowSelectScent: false });
        const isShowRollOn = window.location.pathname.includes('Scent') || window.location.pathname.includes('miniature_perfume');
        if (scents.length === 0) {
          this.props.history.push(generateUrlWeb(isShowRollOn ? '/product/miniature_perfume' : '/product/creation_perfume'));
          return;
        }
        this.props.loadingPage(true);
        const product = await this.generateProductCart(scents, 'perfume');
        const custome = isShowRollOn ? {} : {
          image: this.state.currentImg,
          name: this.state.nameBottle,
          font: this.state.font,
          color: this.state.color,
          quantity: this.state.quantity,
        };
        const { pathname, search } = getSearchPathName(generateUrlWeb(isShowRollOn ? `/product/Scent/${product.id}` : `/product/Perfume/${product.id}`));
        this.props.history.push({
          pathname,
          search,
          state: {
            custome,
            sortId: _.map(scents, x => x.id),
          },
        });
      } else if (type.name === 'perfume_diy') {
        // const formatScents = _.map(scents, d => (d.combo ? _.find(d.combo, x => x.product.type === 'Scent')?.product : d));
        this.setState({ isShowSelectScent: false });
        if (scents.length === 0) {
          this.props.history.push(generateUrlWeb('/product/perfume_diy'));
          return;
        }
        this.props.loadingPage(true);
        const product = await this.generateProductCart(scents, 'perfume_diy');
        const custome = {
          image: this.state.currentImg,
          name: this.state.nameBottle,
          font: this.state.font,
          color: this.state.color,
          quantity: this.state.quantity,
        };

        // this.props.history.push(generateUrlWeb(`/product/perfume_diy/${product.id}`), { custome });
        const { pathname, search } = getSearchPathName(generateUrlWeb(`/product/perfume_diy/${product.id}`));
        this.props.history.push({
          pathname,
          search,
          state: {
            custome,
            sortId: _.map(scents, x => x.id),
          },
        });
      } else if (type.name === 'car_diffuser') {
        // const formatScents = _.map(scents, d => (d.combo ? _.find(d.combo, x => x.product.type === 'Scent')?.product : d));
        this.setState({ isShowSelectScent: false });
        if (scents.length === 0) {
          this.props.history.push(generateUrlWeb('/product/car_diffuser'));
          return;
        }
        this.props.loadingPage(true);
        const product = await this.generateProductCart(scents, 'car_diffuser');
        const { pathname, search } = getSearchPathName(generateUrlWeb(`/product/car_diffuser/${product.id}`));
        this.props.history.push({
          pathname,
          search,
          state: {
            sortId: _.map(scents, x => x.id),
          },
        });
      } else if (type.name === 'mini_candles') {
        this.setState({ isShowSelectScent: false });
        // const itemsComboNew = _.filter(this.props.data.combo, x => x.product.type !== 'Bottle');
        const formatScents = _.map(scents, d => (d.combo ? d.combo[0].product : d));
        if (formatScents.length === 0) {
          this.props.history.push(generateUrlWeb('/product/mini_candles'));
        } else {
          this.props.loadingPage(true);
          const product = await this.generateProductCart(formatScents, 'mini_candles');
          const { pathname, search } = getSearchPathName(generateUrlWeb(`/product/mini_candles/${product.id}`));
          this.props.history.push({
            pathname,
            search,
            state: {
              sortId: _.map(formatScents, x => x.id),
            },
          });
        }
      } else if (type.name === 'dual_crayons') {
        this.setState({ isShowSelectScent: false });
        const formatScents = _.map(scents, d => (d.combo ? d.combo[0].product : d));
        if (formatScents.length === 0) {
          this.props.history.push(generateUrlWeb('/product/dual_crayons'));
        } else {
          this.props.loadingPage(true);
          const product = await this.generateProductCart(formatScents, 'dual_crayons');
          const { pathname, search } = getSearchPathName(generateUrlWeb(`/product/dual_crayons/${product.id}`));
          this.props.history.push({
            pathname,
            search,
            state: {
              sortId: _.map(formatScents, x => x.id),
            },
          });
        }
      } else if (type.name === 'bundle_creation') {
        this.setState({ isShowSelectScent: false });
        const formatScents = _.map(scents, d => (d.combo ? d.combo[0].product : d));
        if (formatScents.length === 0) {
          this.props.history.push(generateUrlWeb('/product/bundle_creation'));
        } else {
          this.props.loadingPage(true);
          const product = await this.generateProductCart(formatScents, 'bundle_creation');
          const { pathname, search } = getSearchPathName(generateUrlWeb(`/product/bundle_creation/${product.id}`));
          this.props.history.push({
            pathname,
            search,
            state: {
              sortId: _.map(formatScents, x => x.id),
            },
          });
        }
      } else if (type.name === 'reed_diffuser') {
        this.setState({ isShowSelectScent: false });
        const formatScents = _.map(scents, d => (d.combo ? d.combo[0].product : d));
        if (formatScents.length === 0) {
          this.props.history.push(generateUrlWeb('/product/reed_diffuser'));
        } else {
          this.props.loadingPage(true);
          const product = await this.generateProductCart(formatScents, 'reed_diffuser');
          const { pathname, search } = getSearchPathName(generateUrlWeb(`/product/reed_diffuser/${product.id}`));
          this.props.history.push({
            pathname,
            search,
            state: {
              sortId: _.map(formatScents, x => x.id),
            },
          });
        }
      } else if (type.name === 'oil_burner') {
        this.setState({ isShowSelectScent: false });
        const formatScents = _.map(scents, d => (d.combo ? d.combo[0].product : d));
        if (formatScents.length === 0) {
          this.props.history.push(generateUrlWeb('/product/oil_burner'));
        } else {
          this.props.loadingPage(true);
          const product = await this.generateProductCart(formatScents, 'oil_burner');
          // this.props.history.push(generateUrlWeb(`/product/oil_burner/${product.id}`));
          const { pathname, search } = getSearchPathName(generateUrlWeb(`/product/oil_burner/${product.id}`));
          this.props.history.push({
            pathname,
            search,
            state: {
              sortId: _.map(formatScents, x => x.id),
            },
          });
        }
      }
      // this.props.loadingPage(false);
    }

    updateScentSelect = (data) => {
      this.combosState = data;
    }

    onClickClose = () => {
      this.setState({ isShowSelectScent: false });
    }

    onClickViewFullImage = () => {
      this.setState({ isShowViewFullImage: false });
    }

    onChangeTypePerfume = (data) => {
      _.assign(this.state, {
        currentImg: undefined,
        nameBottle: undefined,
        font: FONT_CUSTOME.JOST,
        color: COLOR_CUSTOME.BLACK,
      });
      this.props.onChangeTypePerfume(data);
    }

    onClickWaxPerfume = (item) => {
      const { data } = this.props;
      this.props.history.push(generateUrlWeb(`/product/hand_sanitizer/${data.id}/${item.id}`));
    }

    onClickImage = (e) => {
      const { localName } = e.target;
      if (localName === 'img') {
        this.setState({ isShowViewFullImage: true });
      }
    }

    onClickrecommend = (data, type) => {
      const scents = type?.name === 'reed_diffuser' ? this.props.scentsReedDiffuser : type?.name === 'perfume_diy' ? this.props.scentsDiy : ['Perfume', 'car_diffuser'].includes(type?.name) ? this.props.scents : this.props.scentsDualCrayon;
      const datascent = _.find(scents, x => x.id === data.id);
      if (datascent) {
        this.onClickChooseScent({ product: datascent }, 2, true);
      }
    }

    getProductIdRecommended = (type, combosChooseScent) => {
      if (['oil_burner', 'dual_crayons'].includes(type)) {
        const scent = _.find(combosChooseScent?.product?.combo, x => x.product.type === 'Scent');
        return scent?.product?.id;
      }
      return combosChooseScent?.product?.id;
    }

    gotoLink = (link) => {
      if (link.includes('http')) {
        window.location.href = link;
        return;
      }
      this.props.history.push(generateUrlWeb(link));
    };

    render() {
      const cmsCommon = getCmsCommon(this.props.cms);
      const {
        quantity, scentedHomes, scentedCandles,
        scentedDualCrayons, bestSeller, scentedReedDiffUser,
        scentedCarDiffUser, scentedMiniCandle, scentedOilBurner,
        isShowKnowMore, scentedPerfumeDiy,
        dataPopup,
        isShowCustom,
        dataScentSelection,
        dataScentsPerfume, dataScentsWax, dataScentsCandle, dataScentsHome,
        isShowSelectScent, dataReview, isShowViewFullImage, dataScentsDualCrayon,
        dataScentsDiyPerfume, dataScentsMiniCandle, dataScentsOilBurner, dataScentsReedDiffuser,
      } = this.state;
      const {
        data, datas, recommendCms, itemHandlerSanitizer, isBestSeller,
      } = this.props;
      const {
        items, combo, profile, type,
      } = data;
      const cmsTextBlocks = _.filter(recommendCms, x => x.type === 'text_block');
      const iconBlocksDescription = _.find(recommendCms || [], x => x.type === 'icons_block' && x.value?.image_background?.caption === 'image-description');
      const ctaBlock = _.find(recommendCms, x => x.type === 'cta_block' && x.value?.image?.caption === data.type.name);
      const typeVideo = isMobile767fn() ? 'phone' : isMobile991fn() ? 'tablet' : 'web';
      const dataVideo = _.find(ctaBlock?.value?.videos, x => x.value.type === typeVideo);

      const combosChooseScent = _.filter(combo, x => _.isEmpty(x) || x.product.type !== 'Bottle');
      const combos = _.filter(combo, x => !_.isEmpty(x) && x.product.type !== 'Bottle');
      this.itemBottle = _.find(combo, x => !_.isEmpty(x) && x.product.type === 'Bottle');
      const itemCombo = items && items.length > 0 ? items[0] : undefined;
      const isWaxPerfume = data.type.name === 'Wax_Perfume' || data.type.name === 'hand_sanitizer' || data.type.name === 'home_scents_premade';
      const isHandSanitizer = data.type.name === 'hand_sanitizer' || data.type.name === 'discovery_box' || data.type.name === 'mask_sanitizer' || data.type.name === 'toilet_dropper' || data.type.name === 'holder' || data.type.name === 'home_scents_premade';
      const isCuratedCandle = data.type.name === 'dual_candles' && data.is_featured;

      if (isWaxPerfume && _.isEmpty(this.waxPerfume)) {
        this.waxPerfume = !_.isEmpty(itemHandlerSanitizer) ? itemHandlerSanitizer : (_.find(items, x => !x.is_sample) ? _.find(items, x => !x.is_sample) : items[0]);
      } else {
        this.waxPerfume = {};
      }
      const preMakeBt = getNameFromButtonBlock(this.props.buttonBlocks, 'Pre_Made_Home_Scent');
      const recommendBt = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Duo Candles');
      const recommendCrayons = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Duo Crayons');
      const recommendReedDiffUser = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Reed Diffuser');
      const recommendCarDiffUser = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Car Diffuser');
      const recommendMiniCandle = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Mini Candle');
      const recommendOilBurner = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Oil Burner');
      const recommendPerfumeDiy = getNameFromButtonBlock(this.props.buttonBlocks, 'Recommended Perfume Diy');
      const bestSellersBt = getNameFromButtonBlock(this.props.buttonBlocks, 'Best Sellers');
      const bestSellersHomeBt = getNameFromButtonBlock(this.props.buttonBlocks, 'Best Sellers Home');
      const allProductBt = getNameFromButtonBlock(this.props.buttonBlocks, 'all products');
      const allProductLink = getLinkFromButtonBlock(this.props.buttonBlocks, 'all products');
      const bestSellersHomeLink = getLinkFromButtonBlock(this.props.buttonBlocks, 'Best Sellers Home');
      const homeBt = getNameFromButtonBlock(this.props.buttonBlocks, 'Home');
      const dataCustom = {
        image: this.state.currentImg,
        nameBottle: this.state.nameBottle,
        font: this.state.font,
        color: this.state.color,
        combos,
      };

      const dataScents = _.cloneDeep(data.type.name === 'reed_diffuser' ? dataScentsReedDiffuser : data.type.name === 'oil_burner' ? dataScentsOilBurner : data.type.name === 'mini_candles' ? dataScentsMiniCandle : data.type.name === 'perfume_diy' ? dataScentsDiyPerfume : data.type.name === 'dual_crayons' ? dataScentsDualCrayon : (data.type.name === 'Perfume' || data.type.name === 'bundle_creation' || data.type.name === 'reed_diffuser' || data.type.name === 'car_diffuser') ? dataScentsPerfume : (data.type.name === 'dual_candles' || data.type.name === 'single_candle') ? dataScentsCandle : data.type.name === 'home_scents' ? dataScentsHome : dataScentsWax);
      const isPerfumeProduct = ['Perfume', 'Creation', 'perfume_diy', 'Scent', 'dual_crayons'].includes(data.type.name);

      // remove scent selected
      if (['reed_diffuser', 'dual_crayons'].includes(data.type.name)) {
        _.forEach(dataScents, (k) => {
          _.remove(k.datas[0]?.datas || [], x => _.find(combos, d => (d.product.combo ? d.product.combo[0].product.id : d.product.id) === x.id));
        });
      }
      const recommendData = ['Perfume', 'dual_crayons', 'perfume_diy', 'car_diffuser'].includes(data.type.name) && this.props.ingredients?.length > 0 && combosChooseScent?.length === 1 ? _.find(this.props.ingredients, x => x.id === this.getProductIdRecommended(data.type.name, combosChooseScent[0])) : undefined;

      return (
        <div
          className={classnames('div-product-detail div-col', 'product-detail-v3')}
          // style={{ height: heightDetail }}
        >
          <CardKnowMore
            onCloseKnowMore={this.onCloseKnowMore}
            isShowKnowMore={isShowKnowMore}
            data={dataPopup}
          />
          {/* <Modal
            className={`modal-view-full-image ${addFontCustom()}`}
            isOpen={isShowViewFullImage}
            toggle={this.onClickViewFullImage}
          >
            <ModalBody>
              <ImageBlock
                images={this.images}
                datas={[]}
                isActive={this.isActive}
                onChangeProduct={this.props.onChangeProduct}
                dataCustom={dataCustom}
                buttonBlocks={this.props.buttonBlocks}
                onClose={this.onClickViewFullImage}
                className={classnames(isMobile ? 'modal-custom-mobile' : '')}
                isPopUp
                isSelectedScent={combos?.length > 0}
              />
            </ModalBody>
          </Modal> */}
          <div className="show-page-1 div-page1">
            <div className="div-route-link">
              <button
                className="button-bg__none"
                type="button"
                onClick={() => this.gotoLink(getLinkFromButtonBlock(this.props.buttonBlocks, 'Home'))}
              >
                {homeBt}
              </button>
              <span>/</span>
              <button
                className="button-bg__none"
                type="button"
                onClick={() => this.gotoLink(isBestSeller ? bestSellersHomeLink : allProductLink)}
              >
                { isBestSeller ? bestSellersHomeBt : allProductBt}
              </button>
              <span>/</span>
              <button className="button-bg__none active" type="button">
                {(isCuratedCandle ? data.short_description : undefined) || this.nameProduct}
              </button>
            </div>

            <Row className="mr-0 ml-0">
              <Col md={isMobile991fn() ? '12' : '6'} xs="12">
                <ImageBlock
                  images={this.images}
                  datas={datas}
                  isActive={this.isActive}
                  onChangeProduct={this.props.onChangeProduct}
                  dataCustom={dataCustom}
                  buttonBlocks={this.props.buttonBlocks}
                  // onClickImage={this.onClickImage}
                  isSelectedScent={combos?.length > 0}
                  className="product-detail-v2 product-detail-v3"
                />
              </Col>
              <Col md={isMobile991fn() ? '12' : '6'} xs="12">
                {
                    isMobile767fn() ? (
                      <Modal
                        isOpen={isShowSelectScent}
                        toggle={this.onClickClose}
                        className={`modal-scent-mobile-v2 ${addFontCustom()}`}
                        fade={false}
                      >
                        <SelectScentV2
                          scentNotes={dataScents}
                          dataSelection={dataScentSelection}
                          buttonBlocks={this.props.buttonBlocks}
                          onClickApply={d => this.onClickApply(d, this.state.indexSelect)}
                          onClickCancel={this.onClickClose}
                          onClickIngredient={this.props.onClickIngredient}
                          isHeaderMobile
                          isNotPreData
                          indexSelect={this.state.indexSelect}
                          isDualCandles={['single_candle', 'dual_candles', 'mini_candles'].includes(data.type.name)}
                          isShowHome={data.type.name === 'Home'}
                          isOnlyShowIngredientLocal={this.state.isOnlyShowIngredientLocal}
                          scentRecommend={recommendData}
                          recommendDataOfScent={recommendData?.name}
                        />
                      </Modal>
                    ) : (
                      <Drawer
                        anchor="right"
                        open={isShowSelectScent}
                        onClose={this.onClickClose}
                        className="drawer-pick-scent"
                      >
                        <PickIngredientV3
                        // ref={this.refPickIngredient}
                          scentRecommend={recommendData}
                          recommendDataOfScent={recommendData?.name}
                          onClickCloseScent={this.onClickClose}
                          dataSelection={dataScentSelection}
                          dataScents={dataScents}
                          onClickApply={this.onClickApply}
                          loadingPage={this.props.loadingPage}
                          onClickIngredient={this.props.onClickIngredient}
                          typeSelection={this.props.typeSelection}
                          buttonBlocks={this.props.buttonBlocks}
                          onChangeFilter={this.props.onChangeFilter}
                          onChangeSearch={this.props.onChangeSearch}
                        // shareRefFilterSearchScent={this.props.shareRefFilterSearchScent}
                          isDualCandles={['single_candle', 'dual_candles', 'mini_candles'].includes(data.type.name)}
                          isShowHome={data.type.name === 'Home'}
                          onClickChangeImageCandle={this.onClickChangeImageCandle}
                          isOnlyShowIngredientLocal={this.state.isOnlyShowIngredientLocal}
                          className="pick-product-detail-v3"
                          isPerfumeProduct={isPerfumeProduct}
                          isPickProductDetailV3
                        />
                      </Drawer>
                    )
                  }
                <TextBlockV2
                  login={this.props.login}
                  arrayThumbs={this.state.arrayThumbs}
                  nameProduct={this.nameProduct}
                  nameProductCustom={isBestSeller ? data.name : isCuratedCandle ? data.short_description : undefined}
                  descriptionCustom={isCuratedCandle ? data.description : undefined}
                  itemCombo={itemCombo}
                    // priceSample={priceSample}
                  scrollToSample={this.scrollToSample}
                  cmsCommon={cmsCommon}
                  itemBottle={this.itemBottle}
                  onChangeQuantity={this.onChangeQuantity}
                  quantity={quantity}
                  onClickAddtoCart={this.onClickAddtoCart}
                  combos={combos}
                  combosChooseScent={combosChooseScent}
                  toggleCustom={this.toggleCustom}
                  propsState={this.props.propsState}
                  type={data.type || {}}
                  tags={data.tags}
                  profile={data.profile}
                  actualType={data.actual_type}
                  altName={data && data.type ? data.type.alt_name : ''}
                  isWaxPerfume={isWaxPerfume}
                  isHandSanitizer={isHandSanitizer}
                  items={items}
                  addToCartItem={this.addToCartItem}
                  updateWaxPerfume={this.updateWaxPerfume}
                  onClickIngredient={this.props.onClickIngredient}
                  currentImg={this.state.currentImg}
                  nameBottle={this.state.nameBottle}
                  font={this.state.font}
                  color={this.state.color}
                  removeCustomization={this.removeCustomization}
                  idCartItem={this.state.idCartItem}
                  dataReview={dataReview}
                  buttonBlocks={this.props.buttonBlocks}
                  onClickChooseScent={this.onClickChooseScent}
                  updateScentSelect={this.updateScentSelect}
                  onClickApply={this.onClickApply}
                  bottlePrice={this.props.bottlePrice}
                  onChangeTypePerfume={this.onChangeTypePerfume}
                  onClickGotoPerfumeProductFromCrayons={this.onClickGotoPerfumeProductFromCrayons}
                  onClickWaxPerfume={this.onClickWaxPerfume}
                  itemHandlerSanitizer={this.props.itemHandlerSanitizer}
                  recommendData={recommendData}
                  onClickrecommend={d => this.onClickrecommend(d, type)}
                  dataFAQ={this.props.dataFAQ}
                  isBestSeller={isBestSeller}
                  updateColorCandle={this.updateColorCandle}
                  variantValues={this.state.variantValues}
                  moodBestSeller={data?.short_description}
                  familyBestSeller={data?.family}
                  iconBlocksDescription={iconBlocksDescription}
                  isPerfumeProduct={isPerfumeProduct}
                />
              </Col>
            </Row>
            {
              ctaBlock && (
                <div className="video-block">
                  <div className="text-header">
                    {ctaBlock?.value?.text}
                  </div>
                  <div className="div-video">
                    <VideoAcademy url={dataVideo?.value?.video} poster={dataVideo?.value?.placeholder} isControl />
                  </div>
                </div>
              )
            }
            {
              data.type.name === 'home_scents' && (
                <PremadeProductV3
                  buttonBlocks={this.props.buttonBlocks}
                  title={preMakeBt}
                  data={scentedHomes}
                  onClickGotoProduct={this.onClickGotoProduct}
                  fetchData={this.fetchScentedHomes}
                  isShowDots
                />
              )
            }
            {
              data.type.name === 'dual_crayons' && (
                <PremadeProductV3
                  buttonBlocks={this.props.buttonBlocks}
                  title={recommendCrayons}
                  data={scentedDualCrayons}
                  onClickGotoProduct={this.onClickGotoProduct}
                  fetchData={this.fetchScentedDualCrayons}
                  isShowDots
                />
              )
            }
            {
              (data.type.name === 'dual_candles' || (data.type.name === 'single_candle' && combos?.length === 0)) && (
                <PremadeProductV3
                  buttonBlocks={this.props.buttonBlocks}
                  title={recommendBt}
                  data={scentedCandles}
                  onClickGotoProduct={this.onClickGotoProduct}
                  fetchData={this.fetchScentedCanldes}
                  isShowDots
                />
              )
            }
            {
              (data.type.name === 'Perfume' && (combos?.length > 1 || combos?.length === 0)) && (
                <PremadeProductV3
                  buttonBlocks={this.props.buttonBlocks}
                  title={bestSellersBt}
                  data={bestSeller}
                  onClickGotoProduct={this.onClickGotoProduct}
                  fetchData={this.fetchBestSeller}
                  isShowDots
                />
              )
            }
            {
              data.type.name === 'reed_diffuser' && (
                <PremadeProductV3
                  buttonBlocks={this.props.buttonBlocks}
                  title={recommendReedDiffUser}
                  data={scentedReedDiffUser}
                  onClickGotoProduct={this.onClickGotoProduct}
                  fetchData={this.fetchScentedReedDiffUser}
                  isShowDots
                />

              )
            }
            {
              data.type.name === 'car_diffuser' && (
                <PremadeProductV3
                  buttonBlocks={this.props.buttonBlocks}
                  title={recommendCarDiffUser}
                  data={scentedCarDiffUser}
                  onClickGotoProduct={this.onClickGotoProduct}
                  fetchData={this.fetchScentedCarDiffUser}
                  isShowDots
                />
              )
            }
            {
              data.type.name === 'mini_candles' && (
                <PremadeProductV3
                  buttonBlocks={this.props.buttonBlocks}
                  title={recommendMiniCandle}
                  data={scentedMiniCandle}
                  onClickGotoProduct={this.onClickGotoProduct}
                  fetchData={this.fetchScentedMiniCandle}
                  isShowDots
                />
              )
            }
            {
              data.type.name === 'oil_burner' && (
                <PremadeProductV3
                  buttonBlocks={this.props.buttonBlocks}
                  title={recommendOilBurner}
                  data={scentedOilBurner}
                  onClickGotoProduct={this.onClickGotoProduct}
                  fetchData={this.fetchScentedOilBurner}
                  isShowDots
                />
              )
            }
            {
              data.type.name === 'perfume_diy' && (
                <PremadeProductV3
                  buttonBlocks={this.props.buttonBlocks}
                  title={recommendPerfumeDiy}
                  data={scentedPerfumeDiy}
                  onClickGotoProduct={this.onClickGotoProduct}
                  fetchData={this.fetchScentedPerfumeDiy}
                  isShowDots
                />
              )
            }
          </div>
          {
            isShowCustom
              ? (
                <CustomeBottleV4
                  ref={this.refCustome}
                  cmsCommon={cmsCommon}
                  handleCropImage={this.handleCropImage}
                  propsState={this.props.propsState}
                  arrayThumbs={this.state.arrayThumbs}
                  currentImg={this.state.currentImg}
                  nameBottle={this.state.nameBottle}
                  font={this.state.font}
                  color={this.state.color}
                  handleActiveImgOnBottle={this.handleActiveImgOnBottle}
                  closePopUp={() => this.setState({ isShowCustom: false })}
                  onSave={this.onSaveCustomer}
                  itemBottle={this.itemBottle}
                  cmsTextBlocks={cmsTextBlocks}
                  name1={combos && combos.length > 0 ? combos[0].name : ''}
                  name2={combos && combos.length > 1 ? combos[1].name : ''}
                  onClickUpdatetoCart={this.onClickUpdatetoCart}
                  idCartItem={this.state.idCartItem}
                />
              ) : (<div />)
          }
        </div>
      );
    }
}

ProductDetail.propTypes = {
  priceSample: PropTypes.number.isRequired,
  type: PropTypes.string.isRequired,
  data: PropTypes.shape(PropTypes.object).isRequired,
  datas: PropTypes.arrayOf(PropTypes.object).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  basket: PropTypes.arrayOf(PropTypes.object).isRequired,
  onChangeProduct: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  propsState: PropTypes.shape(PropTypes.object).isRequired,
  cms: PropTypes.shape().isRequired,
  recommendCms: PropTypes.arrayOf().isRequired,
  updateProductBasket: PropTypes.func.isRequired,
  ingredients: PropTypes.arrayOf().isRequired,
};

export default ProductDetail;
