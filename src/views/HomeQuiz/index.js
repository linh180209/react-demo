/* eslint-disable react/sort-comp */
/* eslint-disable no-shadow */
import React, { Component } from 'react';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import smoothscroll from 'smoothscroll-polyfill';
import {
  scrollTop, fetchCMSHomepage, getCmsCommon, googleAnalitycs, getSEOFromCms, generateHreflang, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers';
import HeaderHomePage from '../../components/HomePage/header';
import addCmsRedux from '../../Redux/Actions/cms';
import { toastrError } from '../../Redux/Helpers/notification';
import BlockVideo from '../../components/HomeAlt/BlockVideo';
import CTABlockBig from '../../components/HomeAlt/CTABlockBig';
import BlockProduct from '../../components/HomeAlt/BlockProduct';
import Testimonials from '../../components/HomeAlt/Testimonials';
import BlockStepItem from '../../components/HomeAlt/BlockStep';
import BlockWorkShop from '../../components/HomeAlt/BlockWorkShop';
import BlockIcon from '../../components/HomeAlt/BlockIcon';
import BlockSaying from '../../components/HomeAlt/BlockSayingV2';
import BlockIconHeaderV2 from '../../components/HomeAlt/BlockIconHeaderV2';
import BlockQuizStart from '../../components/HomeAlt/BlockQuizStart';

import { createBasketGuest, addProductBasket } from '../../Redux/Actions/basket';
import loadingPage from '../../Redux/Actions/loading';
import FooterV2 from '../../views2/footer';

class HomeQuiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      homePageCms: undefined,
    };
  }

  componentDidMount() {
    setPrerenderReady();
    smoothscroll.polyfill();
    googleAnalitycs('/');
    scrollTop();
    this.fetchData();
    this.props.loadingPage(false);
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchData = async () => {
    const { cms } = this.props;
    const homePageCms = _.find(cms, x => x.title === 'Home Quiz');
    if (!homePageCms) {
      try {
        const result = await fetchCMSHomepage('home-quiz');
        this.props.addCmsRedux(result);
        this.setState({ homePageCms: result });
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      this.setState({ homePageCms });
    }
  }

  scrollToTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  render() {
    const {
      history, login, basket, addProductBasket, createBasketGuest, cms,
    } = this.props;
    const { homePageCms } = this.state;
    if (!homePageCms) {
      return (
        <div />
      );
    }
    const { isShowLogin } = this.props.location;
    let isPositionRight = false;
    const cmsCommon = getCmsCommon(cms);
    const seo = getSEOFromCms(homePageCms);
    const metaHtml = (
      <MetaTags>
        <title>
          {seo ? seo.seoTitle : ''}
        </title>
        <meta name="description" content={seo ? seo.seoDescription : ''} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(this.props.countries, '/home-quiz')}
      </MetaTags>
    );
    return (
      <div className="div-col justify-center items-center">
        {metaHtml}
        <HeaderHomePage isShowLogin={isShowLogin} isSpecialMenu isDisableScrollShow isNotShowRegion />
        {
          _.map(homePageCms.body, (d, index) => {
            switch (d.type) {
              case 'cta_block':
                return <BlockQuizStart heroCms={d} history={history} index={index} />;
              case 'banner_block':
              {
                isPositionRight = !isPositionRight;
                return <BlockVideo heroCms={d} history={history} login={login} isPositionRight={isPositionRight} index={index} />;
              }
              case 'featured_apps_block':
                return <CTABlockBig heroCms={d} history={history} login={login} isLanding index={index} />;
              case 'featured_products_block':
                return <BlockProduct heroCms={d} history={history} login={login} basket={basket} createBasketGuest={createBasketGuest} addProductBasket={addProductBasket} cmsCommon={cmsCommon} index={index} />;
              case 'steps_block':
                return <BlockStepItem heroCms={d} history={history} login={login} index={index} isHidenHeader />;
              case 'feedbacks_block':
                return <BlockSaying heroCms={d} history={history} login={login} index={index} />;
              case 'atelier_list_block':
                return <BlockWorkShop heroCms={d} history={history} index={index} isHidenHeader />;
              case 'benefits_block':
                if (d.value.image_background.caption === 'block-icon-header') {
                  return <BlockIconHeaderV2 heroCms={d} isHidenHeader />;
                }
                return <BlockStepItem heroCms={d} history={history} login={login} index={index} isBenefitsBlock />;
              case 'icons_block':
                return <BlockIcon heroCms={d} />;
              case 'blogs_block':
                return <Testimonials heroCms={d} scrollToTop={this.scrollToTop} />;
              default:
                break;
            }
          })
        }

        <FooterV2 />
      </div>
    );
  }
}


HomeQuiz.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
  location: PropTypes.shape(PropTypes.object).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  basket: PropTypes.shape(PropTypes.object).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    login: state.login,
    basket: state.basket,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  addProductBasket,
  createBasketGuest,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HomeQuiz));
