import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  CardGroup,
  Card,
  Button,
  Input,
  InputGroup,
  InputGroupAddon,
} from 'reactstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { PASSWORD_RESET_COMFIRM_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import {
  generateUrlWeb, getCmsCommon, getNameFromCommon, gotoShopHome,
} from '../../Redux/Helpers';

class ResetPassWord extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: '',
      confirmPassword: '',
    };
  }

  onChange = (e) => {
    e.preventDefault();
    const { value, name } = e.target;
    if (name === 'newPassword') {
      this.setState({ newPassword: value });
    } else if (name === 'confirmPassword') {
      this.setState({ confirmPassword: value });
    }
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { newPassword, confirmPassword } = this.state;
    if (!newPassword || !confirmPassword) {
      toastrError('Please fill all the information');
      return;
    }
    if (newPassword !== confirmPassword) {
      toastrError('Your password and confirmation password do not match');
      return;
    }
    // if (newPassword.length < 8 || !this.isStrong(newPassword)) {
    //   toastrError('Password between 8 and 20 characters; one numeric digit, and one special character, but cannot contain whitespace.');
    //   return;
    // }
    this.resetPassWordAPI(newPassword, confirmPassword);
  }

  isStrong = password => password.match('^(?=.*[!@#$&*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z].*[a-z])')

  resetPassWordAPI = (newPassword, confirmPassword) => {
    const { params } = this.props.match;
    const { uidb64, token } = params;
    const body = {
      token,
      uid: uidb64,
      new_password1: newPassword,
      new_password2: confirmPassword,
    };
    const options = {
      url: PASSWORD_RESET_COMFIRM_URL,
      method: 'POST',
      body,
    };
    fetchClient(options).then((result) => {
      if (!result.isError) {
        toastrSuccess('Password has been reset with the new password');
        // this.props.history.push(generateUrlWeb('/'));
        gotoShopHome();
        return;
      }
      throw new Error(result.message);
    }).catch((e) => {
      toastrError(e.message);
    });
  }

  render() {
    const { newPassword, confirmPassword } = this.state;
    const { cms } = this.props;
    if (!cms) {
      return null;
    }
    const cmsCommon = getCmsCommon(cms);
    const resetPassword = getNameFromCommon(cmsCommon, 'RESET_PASSWORD');
    const submitN = getNameFromCommon(cmsCommon, 'SUBMIT');
    return (
      <div className="login app app-edit flex-row align-items-center">
        <Container>
          <Row className="justify-content-center" style={{ width: '100%' }}>
            <Col md="6" xs="12">
              <CardGroup className="login__card-group" style={{ width: '100%' }}>
                <Card className="p-4">
                  <h1 className="mt-4">
                    {resetPassword}
                  </h1>
                  <form onSubmit={this.onSubmit}>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <i className="icon-lock" />
                      </InputGroupAddon>
                      <Input
                        type="password"
                        placeholder="New Password"
                        name="newPassword"
                        value={newPassword}
                        onChange={this.onChange}
                      />
                    </InputGroup>

                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <i className="icon-lock" />
                      </InputGroupAddon>
                      <Input
                        type="password"
                        placeholder="Confirm New Password"
                        name="confirmPassword"
                        value={confirmPassword}
                        onChange={this.onChange}
                      />
                    </InputGroup>
                    <Row>
                      <Col xs="12">
                        <Button type="submit" className="px-4 btn--login">
                          {submitN}
                        </Button>
                      </Col>
                    </Row>
                  </form>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

ResetPassWord.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      uidb64: PropTypes.string,
      token: PropTypes.string,
    }),
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  cms: PropTypes.shape().isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
  };
}

export default withRouter(connect(mapStateToProps)(ResetPassWord));
