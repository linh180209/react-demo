import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';


import { PASSWORD_RESET_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import Header from '../../components/HomePage/header';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import {
  generateUrlWeb, getCmsCommon, getNameFromCommon, googleAnalitycs,
} from '../../Redux/Helpers';
import FooterV2 from '../../views2/footer';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };
  }

  componentDidMount = () => {
    window.scrollTo(0, 0);
    googleAnalitycs('/ForgotPassword');
  };

  onChange = (e) => {
    const { value } = e.target;
    this.setState({ email: value });
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { email } = this.state;
    if (!email || !this.validateEmail(email)) {
      toastrError('Please fill the email');
      return;
    }
    this.resetPassWord(email);
  }

  validateEmail = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  resetPassWord = (email) => {
    const body = {
      email,
    };
    const options = {
      url: PASSWORD_RESET_URL,
      body,
      method: 'POST',
    };
    fetchClient(options).then((result) => {
      if (!result.isError) {
        toastrSuccess('Please check your email');
        return;
      }
      throw new Error(result.message);
    }).catch((e) => {
      toastrError(e.message);
    });
  }

  loginClick = () => {
    this.props.history.push(generateUrlWeb('/login'));
  }

  registerClick = () => {
    this.props.history.push(generateUrlWeb('/register'));
  }

  render() {
    const { cms } = this.props;
    if (!cms) {
      return null;
    }
    const cmsCommon = getCmsCommon(cms);
    const resetPassword = getNameFromCommon(cmsCommon, 'RESET_PASSWORD');
    const resetYourPassword = getNameFromCommon(cmsCommon, 'RESET_YOUR_PASSWORD');
    const description1 = getNameFromCommon(cmsCommon, 'TYPE_IN_YOUR');
    const description2 = getNameFromCommon(cmsCommon, 'INSTRUCTIONS_ON_HOW');
    const yourEmail = getNameFromCommon(cmsCommon, 'YOUR_EMAIL');
    const { email } = this.state;
    const enterEmail = (
      <div className="div-col justify-center items-center">
        <div className="div-col items-center" style={{ marginTop: '200px', marginBottom: '200px' }}>
          <span
            style={{
              fontSize: '1.5rem',
            }}
          >
            {resetYourPassword}
          </span>
          <span className="mb-5 mt-4" style={{ textAlign: 'center' }}>
            {description1}
            <br />
            {description2}
          </span>
          <input
            type="email"
            name="email"
            placeholder={yourEmail}
            value={email}
            onChange={this.onChange}
            style={{
              width: '300px',
              border: 'none',
              borderBottom: '1px solid #000',
              paddingBottom: '5px',
              fontStyle: 'italic',
              textAlign: 'center',
            }}
          />
          <button
            type="button"
            className="button-bg__none pt-2 pb-2 pl-3 pr-3 mt-5"
            style={{
              border: '1px solid #000',
              borderRadius: '8px',
            }}
            onClick={this.onSubmit}
          >
            {resetPassword}
          </button>
        </div>
      </div>
    );
    return (
      <div className="div-col">
        <Header />
        {enterEmail}
        <FooterV2 />
      </div>
    );
  }
}

ForgotPassword.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  cms: PropTypes.shape().isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
  };
}

export default withRouter(connect(mapStateToProps)(ForgotPassword));
