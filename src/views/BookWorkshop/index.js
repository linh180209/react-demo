import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import MetaTags from 'react-meta-tags';

import moment from 'moment';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import {
  scrollTop, fetchCMSHomepage, getSEOFromCms, getNameFromButtonBlock, generateHreflang, removeLinkHreflang,
  fetchTokenSimplyBook,
  generateUrlWeb,
  fetchTokenSimplyBookAdmin,
  setPrerenderReady,
  segmentWorkshopDetailsEntered,
  gotoUrl,
} from '../../Redux/Helpers/index';
import addCmsRedux from '../../Redux/Actions/cms';
import './styles.scss';
import Step2 from './components/Step2';
import Step3 from './components/Step3';
import Step4 from './components/Step4';

import {
  getEventListSimplyBook, addAddOns, addTime, addDataForm, addDetailWorkshopBooked, updateDataForm,
} from '../../Redux/Actions/bookWorkshop';
import loadingPage from '../../Redux/Actions/loading';
import { isMobile } from '../../DetectScreen';
import Step5 from './components/Step5';
import icPrev from '../../image/icon/ic-prev-workshop.svg';
import { loginCompleted } from '../../Redux/Actions/login';


import { toastrError } from '../../Redux/Helpers/notification';
import FooterV2 from '../../views2/footer';
import auth from '../../Redux/Helpers/auth';

const getCms = (workshopCMS) => {
  const objectReturn = {
    headerBlock: '',
    textBlock: '',
    imageBlock: {},
    seo: undefined,
    paragraphBlock: '',
    ateliersBlock: [],
    faqBlocks: [],
  };
  if (!_.isEmpty(workshopCMS)) {
    const seo = getSEOFromCms(workshopCMS);
    const { body } = workshopCMS;
    if (body) {
      const headerBlock = _.find(body, x => x.type === 'header_block').value;
      const textBlock = _.find(body, x => x.type === 'text_block').value;
      const imageBlock = _.find(body, x => x.type === 'image_block').value;
      const buttonBlock = _.filter(body, x => x.type === 'button_block');
      const paragraphBlock = _.find(body, x => x.type === 'paragraph_block').value;
      const ateliersBlock = _.filter(body, x => x.type === 'ateliers_block').map(ele => ele.value).map(ele => ele[0]).map(ele => ele.value);
      const faqBlocks = _.filter(body, x => x.type === 'faq_block');
      _.assign(objectReturn, {
        headerBlock, textBlock, seo, imageBlock, paragraphBlock, ateliersBlock, buttonBlock, faqBlocks,
      });
    }
  }
  return objectReturn;
};

// const { match } = this.props;
//     const { token } = match.params;

class BookWorkshop extends Component {
  constructor(props) {
    super(props);

    this.state = {
      seo: undefined,
      progressWorkshop: {
        currentStep: 2,
        animate: 'slide-in-left',
      },
    };
  }

  componentDidMount = () => {
    setPrerenderReady();
    scrollTop();
    this.fetchDataInit();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.progressWorkshop.currentStep !== this.state.progressWorkshop.currentStep) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
    }
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    this.props.loadingPage(true);
    const workshopCMS = _.find(cms, x => x.title === 'workshop v3');
    const pending = [fetchCMSHomepage('workshop-v3')];
    this.props.addTime(null);
    fetchTokenSimplyBookAdmin();
    if (!workshopCMS) {
      const results = await Promise.all(pending);
      const cmsData = results[0];
      this.props.addCmsRedux(cmsData);
      const dataFromStore = getCms(cmsData);
      this.setState(dataFromStore);
    } else {
      const dataFromStore = getCms(workshopCMS);
      this.setState(dataFromStore);
    }

    fetchTokenSimplyBook().then((res) => {
      if (res) {
        this.props.getEventListSimplyBook();
        this.props.loadingPage(false);
      }
    }).catch(err => toastrError(err?.errors?.message));
  }

  gotoDone = () => {
    gotoUrl('https://www.maison21g.com/ae/perfume-workshop', this.props.history, true);
  }

  handleNextStep = (step) => {
    if (step === 5 && auth.getCountry() === 'ae') {
      segmentWorkshopDetailsEntered(this.props.dataTime);
      this.gotoDone();
      return;
    }
    this.setState((prevState) => {
      const nextStep = {
        progressWorkshop: {
          ...prevState.progressWorkshop,
          currentStep: step,
          animate: 'slide-in-left',
        },
      };
      // console.log(`currentStep #${step}: `, prevState);
      if (step === 4) {
        segmentWorkshopDetailsEntered(this.props.dataTime);
      }
      return nextStep;
    });
  }

  handlePrevStep = (step) => {
    this.setState(prevState => ({
      progressWorkshop: {
        ...prevState.progressWorkshop,
        currentStep: step,
        animate: 'slide-in-right',
      },
    }));
  }

  handleBackMobile = (step) => {
    this.setState(prevState => ({
      progressWorkshop: {
        ...prevState.progressWorkshop,
        currentStep: step - 1,
        animate: 'slide-in-right',
      },
    }));
  }

  switchView = () => {
    const nextText = getNameFromButtonBlock(this.state.buttonBlock, 'next');
    const EventFiltered = _.find(this.props.eventList, item => item.id === this.props.match.params.service);
    switch (this.state.progressWorkshop.currentStep) {
      case 2:
        return (
          <Step2
            history={this.props.history}
            buttonBlock={this.state.buttonBlock}
            nextText={nextText}
            eventFiltered={EventFiltered}
            nextStep={this.handleNextStep}
            prevStep={this.handlePrevStep}
            dataAddOns={this.props.dataAddOns}
            animate={this.state.progressWorkshop.animate}
            addAddOns={this.props.addAddOns}
            serviceId={this.props.match.params.service}
            count={this.props.match.params.count || 1}
            totalPrice={this.props.eventList && this.props.eventList[this.props.match.params.service] ? this.props.eventList[this.props.match.params.service].price_without_tax * this.props.match.params.count : 0}
          />
        );
      case 3:
        return (
          <Step3
            history={this.props.history}
            loadingPage={this.props.loadingPage}
            duration={EventFiltered ? EventFiltered.duration : 30}
            buttonBlock={this.state.buttonBlock}
            nextStep={this.handleNextStep}
            prevStep={this.handlePrevStep}
            animate={this.state.progressWorkshop.animate}
            addTime={this.props.addTime}
            dataTime={this.props.dataTime}
            serviceId={this.props.match.params.service}
            count={this.props.match.params.count || 1}
          />
        );
      case 4:
        return (
          <Step4
            history={this.props.history}
            buttonBlock={this.state.buttonBlock}
            nextStep={this.handleNextStep}
            loginCompleted={this.props.loginCompleted}
            prevStep={this.handlePrevStep}
            animate={this.state.progressWorkshop.animate}
            dataTime={this.props.dataTime}
            serviceId={this.props.match.params.service}
            count={this.props.match.params.count || 1}
            addDataForm={this.props.addDataForm}
            updateDataForm={this.props.updateDataForm}
            addDetailWorkshopBooked={this.props.addDetailWorkshopBooked}
          />
        );
      case 5:
        return (
          <Step5
            history={this.props.history}
            loadingPage={this.props.loadingPage}
            buttonBlock={this.state.buttonBlock}
            nextStep={this.handleNextStep}
            prevStep={this.handlePrevStep}
            animate={this.state.progressWorkshop.animate}
            dataTime={this.props.dataTime}
            serviceId={this.props.match.params.service}
            count={this.props.match.params.count || 1}
          />
        );
      default:
        return '';
    }
  };

  backFunc = () => {
    const pathName = this.props.location.pathname.split('/');
    this.props.history.replace(generateUrlWeb(`/${pathName[1]}/${pathName[2]}/${pathName[3]}`));
  }

  render() {
    const {
      buttonBlock,
      seo,
      progressWorkshop,
      ateliersBlock,
    } = this.state;
    const { dataAddOns, dataTime, eventList } = this.props;
    const menuText = this.props.match.params.workshop ? this.props.match.params.workshop : '';
    const workshopName = this.props.match.params.workshop ? this.props.match.params.workshop : '';
    const seoTitle = _.isEmpty(menuText) ? (seo ? seo.seoTitle : '') : getNameFromButtonBlock(buttonBlock, `title ${menuText}`);
    const seoDescription = _.isEmpty(menuText) ? (seo ? seo.seoKeywords : '') : getNameFromButtonBlock(buttonBlock, `description ${menuText}`);
    const addOnsText = getNameFromButtonBlock(buttonBlock, 'add_ons');
    const timeText = getNameFromButtonBlock(buttonBlock, 'time');
    const paymentText = getNameFromButtonBlock(buttonBlock, 'payment');
    const confirmationText = getNameFromButtonBlock(buttonBlock, 'confirmation');
    const dateAndTimeText = getNameFromButtonBlock(buttonBlock, 'date_and_time');
    const backText = getNameFromButtonBlock(buttonBlock, 'back');
    const peopleText = getNameFromButtonBlock(buttonBlock, 'people');

    const filterAtelierBock = ateliersBlock && ateliersBlock.length > 0 ? ateliersBlock.find(x => x.buttons.find(b => b.value.name === `simplyBookID-${this.props.match.params.service}`)) : '';
    const defaultAmountFilter = filterAtelierBock ? getNameFromButtonBlock(filterAtelierBock.buttons, 'default_amount') : '';
    const defaultAmount = defaultAmountFilter || 1;
    return (
      <React.Fragment>
        <MetaTags>
          <title>
            {seoTitle}
          </title>
          <meta name="description" content={seoDescription} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/perfume-workshop')}
        </MetaTags>
        {/* <HeaderHomePage />
        { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <HeaderHomePageV3 isRemoveMessage />

        <div className={`container-fluid ${!isMobile ? 'py-4' : 'p-0'}`} style={{ background: '#F5ECDC' }}>
          <section className="book-workshop">
            {!isMobile ? (
              <h2 className="title-step">
                {progressWorkshop.currentStep === 2 ? addOnsText : progressWorkshop.currentStep === 3 ? timeText : progressWorkshop.currentStep === 4 ? confirmationText : paymentText}
              </h2>
            ) : ''}
            <div className="wrapper-progress-bar">
              <ul className={classnames('progressbar-custom', auth.getCountry() === 'ae' ? 'sum-step-3' : '')}>
                <li className="active">
                  {eventList && eventList[this.props.match.params.service] ? eventList[this.props.match.params.service].name : workshopName}
                  <br />
                  {defaultAmount * this.props.match.params.count}
                  {' '}
                  {peopleText || 'people'}
                </li>
                <li className={`${progressWorkshop.currentStep >= 2 && 'active'} ${progressWorkshop.currentStep === 2 ? 'current-step' : ''}`}>
                  {dataAddOns.length <= 0 ? addOnsText : ''}
                  {dataAddOns.length > 0 ? `${_.map(dataAddOns, item => item.name.trim()).join(' & ')}` : ''}
                </li>
                <li className={`${progressWorkshop.currentStep >= 3 && 'active'} ${progressWorkshop.currentStep === 3 ? 'current-step' : ''}`}>
                  {!dataTime ? (
                    <div>
                      {dateAndTimeText}
                    </div>
                  ) : ''}
                  {dataTime ? `${dataTime.time} - ${(moment(dataTime.time, 'HH:mm').add('minutes', (dataTime?.arrChecked.length - 1) * 30)).format('HH:mm')}` : ''}
                  {' '}
                  {dataTime && dataTime.date ? (
                    <div>
                      {moment(new Date(dataTime.date)).format('DD-MM-YYYY')}
                    </div>
                  ) : ''}
                </li>
                {
                  auth.getCountry() !== 'ae' && (
                    <li className={`${progressWorkshop.currentStep >= 4 && 'active'} ${progressWorkshop.currentStep === 4 ? 'current-step' : ''}`}>{paymentText || 'Payment'}</li>
                  )
                }
              </ul>
            </div>
            {isMobile && (progressWorkshop.currentStep === 3 || progressWorkshop.currentStep === 4 || progressWorkshop.currentStep === 5) ? (
              <button onClick={() => this.handleBackMobile(progressWorkshop.currentStep)} className="btn-back-mobile text-uppercase" type="button">
                <img src={icPrev} alt="prev" />
                {' '}
                {backText || 'BACK'}
              </button>
            ) : isMobile && progressWorkshop.currentStep === 2 ? (
              <button onClick={() => this.backFunc()} className="btn-back-mobile text-uppercase" type="button">
                <img src={icPrev} alt="prev" />
                {' '}
                {backText || 'BACK'}
              </button>
            ) : ''}
            {isMobile ? (
              <h2 className="title-step">
                {progressWorkshop.currentStep === 2 ? addOnsText : progressWorkshop.currentStep === 3 ? timeText : progressWorkshop.currentStep === 4 ? confirmationText : paymentText}
              </h2>
            ) : ''}
            {localStorage.getItem('tokenSimplyBook') ? this.switchView() : ''}
          </section>
        </div>
        <FooterV2 />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  cms: state.cms,
  countries: state.countries,
  eventList: state?.bookWorkshop?.eventList || [],
  dataAddOns: state?.bookWorkshop?.dataAddOns || [],
  dataTime: state?.bookWorkshop?.time || null,
  // showAskRegion: state.showAskRegion,
});

const mapDispatchToProps = {
  addCmsRedux,
  getEventListSimplyBook,
  addAddOns,
  addTime,
  addDataForm,
  updateDataForm,
  addDetailWorkshopBooked,
  loadingPage,
  loginCompleted,
};

BookWorkshop.defaultProps = {
  match: {
    params: {
      workshop: '',
    },
  },
};

BookWorkshop.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      workshop: PropTypes.string,
    }),
  }),
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BookWorkshop));
