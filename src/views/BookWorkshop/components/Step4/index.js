import React, { Component } from 'react';
import './styles.scss';
import _ from 'lodash';
import { Input, Label } from 'reactstrap';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import moment from 'moment';
import { connect } from 'react-redux';
import JsonRpcClient from 'react-jsonrpc-client';
import PhoneInput, { isPossiblePhoneNumber, isValidPhoneNumber } from 'react-phone-number-input';
import { isMobile } from '../../../../DetectScreen';
import facebook from '../../../../image/facebook.png';
import google from '../../../../image/google.png';
import icPrev from '../../../../image/icon/ic-prev-workshop.svg';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import 'react-phone-number-input/style.css';
import {
  validateEmail, trackGTMSignUp, trackGTMLogin, postPointRedeem,
  getNameFromButtonBlock,
  isCheckNull,
  segmentWorkshopPersonalDetailsEntered,
} from '../../../../Redux/Helpers';
import auth from '../../../../Redux/Helpers/auth';
import { toastrError, toastrSuccess } from '../../../../Redux/Helpers/notification';
import {
  LOGIN_FACEBOOK_URL, LOGIN_GOOGLE_URL, SIMPLY_BOOK_URL, PUT_OUTCOME_URL, getCompanySimplyBook,
} from '../../../../config';

class Step4 extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();

    this.api = new JsonRpcClient({
      endpoint: SIMPLY_BOOK_URL,
      headers: {
        'X-Company-Login': getCompanySimplyBook(),
        'X-Token': localStorage.getItem('tokenSimplyBook'),
      },
    });
  }

  componentDidMount() {
    const loginData = auth.getLogin();
    if (loginData) {
      this.props.updateDataForm({
        field: 'name', value: loginData.user.first_name ? loginData.user.first_name : '',
      });
      this.props.updateDataForm({
        field: 'email', value: loginData.user.email ? loginData.user.email : '',
      });
    }
  }

  responseFacebook = (response) => {
    console.log(response);
    const { accessToken } = response;
    if (accessToken) {
      // const urlAvatar = handleDownloadFile('http://graph.facebook.com/108326343695312/picture?width=256&height=256&access_token=EAAD7DYx6D6sBANOrmEyGtVcer2r1RZBN4N3Tj6IihCo0TZA3qQVErbCVeslwTEkWUETdxiO22GHZAjBSAWLT0mzdOZA5ZC0joN1E0OscwoXyzNhlZCqvLErzhYGXz6ZBrUEU0s50ZBmiaejZCmZAoxolB0ZBD4v9stXFoz86ci3cOrBuWPGRpQRhrfZC5UZCqYB9z9qQ2RYC49yCPNWmiCrnG1pfJ');
      // auth.setUrlAvatar(urlAvatar);
      this.loginSocial(accessToken, LOGIN_FACEBOOK_URL);
    }
  }

  responseGoogle = (response) => {
    console.log(response);
    const { accessToken } = response;
    if (accessToken) {
      this.loginSocial(accessToken, LOGIN_GOOGLE_URL);
    }
  }

  loginSocial = (accessToken, url) => {
    const options = {
      url,
      method: 'POST',
      body: {
        access_token: accessToken,
      },
    };
    fetchClient(options).then((result) => {
      console.log(result);
      if (!result.isError) {
        this.props.loginCompleted(result);
        // toastrSuccess('You are login successfully');
        // this.props.onClose();

        // tracking GTM
        if (url === LOGIN_GOOGLE_URL) {
          if (moment().valueOf() - moment(result.user.date_created).valueOf() < 5 * 60 * 1000) {
            trackGTMSignUp('google', result.user.id);
          } else {
            trackGTMLogin('google', result.user.id);
          }
        } else if (url === LOGIN_FACEBOOK_URL) {
          if (moment().valueOf() - moment(result.user.date_created).valueOf() < 5 * 60 * 1000) {
            trackGTMSignUp('facebook', result.user.id);
          } else {
            trackGTMLogin('facebook', result.user.id);
          }
        }

        if (!_.isEmpty(this.props.refPoint)) {
          postPointRedeem(this.props.refPoint);
        }
        this.handleAfterLogin(result);
      } else {
        throw new Error(result.message);
      }
    }).catch((e) => {
      toastrError(e.message);
    });
  }

  updateOutComeUser = (user, outcome) => {
    if (isCheckNull(outcome)) {
      return;
    }
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', outcome),
      method: 'PUT',
      body: {
        user: parseInt(user, 10),
      },
    };
    fetchClient(options);
  }

  handleAfterLogin = (result) => {
    auth.login(result);
    const outcome = auth.getOutComeId();
    if (outcome && !result.user.outcome) {
      result.user.outcome = outcome;
      this.updateOutComeUser(result.user.id, outcome);
    }

    this.props.loginCompleted(result);
    if (result.user) {
      this.props.updateDataForm({
        field: 'name', value: result.user.first_name ? result.user.first_name : '',
      });
      this.props.updateDataForm({
        field: 'phone', value: result.user.phone ? result.user.phone : '',
      });
      this.props.updateDataForm({
        field: 'email', value: result.user.email ? result.user.email : '',
      });
    }
  }

  handleChange = (event) => {
    // this.setState(prevState => ({ dataForm: { ...prevState.dataForm, [event.target.name]: event.target.value } }));
    this.props.updateDataForm({ field: event.target.name, value: event.target.value });
  }

  validateForm = () => {
    const { dataForm } = this.props;
    // const re = /^\+65(6|8|9)\d{7}|(6|8|9)\d{7}$/;
    if (!dataForm.name) {
      toastrError('Name is required');
      return false;
    } if (!dataForm.name) {
      toastrError('Name is required');
      return false;
    } if (!dataForm.email || !validateEmail(dataForm.email)) {
      toastrError('Please enter a valid email address');
      return false;
    } if (!dataForm.phone || !isPossiblePhoneNumber(dataForm.phone) || !isValidPhoneNumber(dataForm.phone)) {
      toastrError('Please enter a valid phone number');
      return false;
    } return true;
  }

  handleConfirm = () => {
    const validate = this.validateForm();
    const ServiceId = this.props.serviceId;
    const {
      addDataForm, nextStep, addDetailWorkshopBooked,
    } = this.props;
    const { dataForm } = this.props;
    const date = new Date(this.props.bookWorkshop.time.date);
    const dataProduct = [];
    _.map(this.props.bookWorkshop.dataAddOns, (item) => {
      dataProduct.push({ id: item.id, qty: 1 });
    });
    if (validate) {
      addDataForm(dataForm);

      this.api.request('book', parseInt(ServiceId, 10),
        parseInt(this.props.bookWorkshop.time.store.id, 10),
        date ? moment(date).format('YYYY-MM-DD') : '',
        `${this.props.bookWorkshop.time.time}:00`,
        {
          name: dataForm.name,
          email: dataForm.email,
          phone: dataForm.phone,
          // client_time_offset": 180,
        },
        {
          location_id: 4,
          products: dataProduct,
          handle_invoice: true,
          subscribe: dataForm.isPromoLetters,
        },
        parseInt(this.props.count, 10),
        '')
        .then((res) => {
          if (res) {
            addDetailWorkshopBooked(res);
            segmentWorkshopPersonalDetailsEntered(
              {
                name: dataForm.name,
                email: dataForm.email,
                phone: dataForm.phone,
              },
            );
            nextStep(5);
          }
        }).catch((err) => {
          if (err?.message?.message) {
            toastrError(err?.message?.message);
          }
        });
    }
  }

  onChangeCheckbox = (e) => {
    const { checked } = e.target;
    this.props.updateDataForm({ field: 'isPromoLetters', value: checked });
  }

  onChangePhoneNumber = (phoneNumber) => {
    this.props.updateDataForm({ field: 'phone', value: phoneNumber || '' });
  }

  render() {
    const {
      animate, dataTime, prevStep, user,
    } = this.props;
    const { dataForm, buttonBlock } = this.props;
    const backText = getNameFromButtonBlock(buttonBlock, 'back');
    const nextText = getNameFromButtonBlock(buttonBlock, 'next');
    const detailsText = getNameFromButtonBlock(buttonBlock, 'details');
    const summaryText = getNameFromButtonBlock(buttonBlock, 'summary');
    const dateText = getNameFromButtonBlock(buttonBlock, 'date');
    const workshopText = getNameFromButtonBlock(buttonBlock, 'workshop-time');
    const boutiqueText = getNameFromButtonBlock(buttonBlock, 'boutique');
    const termText = getNameFromButtonBlock(buttonBlock, 'term');
    const doneText = getNameFromButtonBlock(buttonBlock, 'Done');
    return (
      <section className={`wrapper-step step-4 ${animate || 'slide-in-left'}`}>
        {!isMobile ? (
          <button type="button" onClick={() => prevStep(3)} className="btn-back text-uppercase">
            <img src={icPrev} alt="prev" />
            {' '}
            {backText || 'BACK'}
          </button>
        ) : ''}
        <div className="wrapper-content">
          <div className="detail-form">
            <div className="form-group" ref={this.myRef}>
              <h4>{detailsText || 'Details'}</h4>
              <input value={dataForm && Object.prototype.hasOwnProperty.call(dataForm, 'name') ? dataForm.name : user ? user.first_name + user.last_name : ''} type="text" name="name" placeholder="Name*" onChange={this.handleChange} />
              <input value={dataForm && Object.prototype.hasOwnProperty.call(dataForm, 'email') ? dataForm.email : user ? user.email : ''} type="text" name="email" placeholder="Email*" onChange={this.handleChange} />
              {/* <input value={dataForm && Object.prototype.hasOwnProperty.call(dataForm, 'phone') ? dataForm.phone : user ? user.phone : ''} type="text" name="phone" placeholder="Phone*" onChange={this.handleChange} /> */}
              <PhoneInput
                placeholder="Enter phone number"
                defaultCountry={_.upperCase(auth.getCountry())}
                international
                countryCallingCodeEditable={false}
                countrySelectProps={{ unicodeFlags: true }}
                value={dataForm && Object.prototype.hasOwnProperty.call(dataForm, 'phone') ? dataForm.phone : user ? user.phone : ''}
                onChange={this.onChangePhoneNumber}
                error={dataForm && Object.prototype.hasOwnProperty.call(dataForm, 'phone') ? (isValidPhoneNumber(dataForm.phone) ? undefined : 'Invalid phone number') : 'Phone number required'}
              />
            </div>
            {!auth.getLogin() ? (
              <div className="login-with-social">
                {!isMobile ? <hr /> : ''}
                <p>Or connect with</p>
                <div className="d-flex justify-content-center">
                  <FacebookLogin
                    appId="276035609956267"
                    callback={this.responseFacebook}
                    render={renderProps => (
                      <div className="icon-social facebook" onClick={renderProps.onClick}>
                        <a
                          href
                          onClick={(e) => {
                            e.preventDefault();
                            renderProps.onClick();
                          }}
                        >
                          <img src={facebook} alt="" />
                        </a>
                      </div>
                    )}
                  />
                  <GoogleLogin
                    clientId="1051429470092-1ktl08tfm23q3b7i2au9ivv6pnil51g1.apps.googleusercontent.com"
                    onSuccess={this.responseGoogle}
                    onFailure={this.onFailure}
                    render={renderProps => (
                      <div className="icon-social" onClick={renderProps.onClick}>
                        <a
                          href
                          onClick={(e) => {
                            e.preventDefault();
                            renderProps.onClick();
                          }}
                        >
                          <img src={google} alt="" />
                        </a>
                      </div>
                    )}
                  />
                </div>
              </div>
            ) : ''}
          </div>
          <div className="summary-box">
            <div className="wrapper-summary">
              <div className="wrapper-info">
                <h4>{summaryText || 'Summary'}</h4>
                <div className="info-group">
                  <Label>
                    {dateText || 'Date'}
                    :
                  </Label>
                  <span>{dataTime && dataTime.date ? moment(new Date(dataTime.date)).format('DD-MM-YYYY') : ''}</span>
                </div>
                <div className="info-group">
                  <Label>
                    {workshopText || 'Workshop Time'}
                    :
                  </Label>
                  <span>{dataTime ? `${dataTime.time} - ${(moment(dataTime.time, 'HH:mm').add('minutes', (dataTime?.arrChecked.length - 1) * 30)).format('HH:mm')}` : ''}</span>
                </div>
                <div className="info-group">
                  <Label>
                    {boutiqueText || 'Boutique'}
                    :
                  </Label>
                  <span>{dataTime && dataTime.store ? dataTime?.store?.name : ''}</span>
                </div>
              </div>
              <div className="foot-info">
                <Label style={{ cursor: 'pointer' }}>
                  <Input
                    name="create_account"
                    id="idCheckbox"
                    type="checkbox"
                    className="custom-input-filter__checkbox"
                    style={{ height: '42px' }}
                    checked={dataForm && Object.prototype.hasOwnProperty.call(dataForm, 'isPromoLetters') ? dataForm.isPromoLetters : false}
                    onChange={this.onChangeCheckbox}
                  />
                  <Label
                    for="idCheckbox"
                    style={{ pointerEvents: 'none' }}
                  />
                  {termText || 'I wish to receive promotions and relevant information from Maison21G.'}
                </Label>
              </div>
            </div>
            <button type="button" onClick={() => this.handleConfirm()}>{auth.getCountry() === 'ae' ? (doneText || 'DONE') : (nextText || 'NEXT')}</button>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  user: state.login && state.login.user ? state.login.user : null,
  bookWorkshop: state.bookWorkshop,
  dataForm: state.bookWorkshop.dataForm,
});

export default connect(mapStateToProps, null)(Step4);
