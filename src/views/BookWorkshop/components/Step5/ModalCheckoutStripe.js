import React, { Component } from 'react';
import './styles.scss';
import _ from 'lodash';
import { Modal } from 'reactstrap';
import classnames from 'classnames';

import StoreCheckout from '../StoreCheckout';
import logoParis from '../../../../image/logo-paris.svg';
import icPrev from '../../../../image/icon/ic-prev-workshop.svg';
import { addFontCustom } from '../../../../Redux/Helpers';

class ModalCheckoutStripe extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  render() {
    return (
      <div>
        <Modal isOpen={this.props.isOpen} centered toggle={this.props.toggle} className={classnames('modal-checkout-stripe', addFontCustom())} backdrop keyboard>
          <button type="button" onClick={() => this.props.toggle()} className="btn-back"><img src={icPrev} alt="prev" /></button>
          <img alt="logo" src={logoParis} className="logo" />
          <h3 className="email-title">{this.props.email}</h3>
          <StoreCheckout toggle={this.props.toggle} loadingPage={this.props.loadingPage} payStripeText={this.props.payStripeText} />
        </Modal>
      </div>
    );
  }
}


export default ModalCheckoutStripe;
