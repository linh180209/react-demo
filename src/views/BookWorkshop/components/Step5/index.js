import React, { Component } from 'react';
import './styles.scss';
import _ from 'lodash';
import JsonRpcClient from 'react-jsonrpc-client';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import paypal from '../../../../image/paypal-logo-preview.svg';
import visa from '../../../../image/visa-card.svg';
import mastercard from '../../../../image/master-card.svg';
import icPrev from '../../../../image/icon/ic-prev-workshop.svg';
import {
  getCompanySimplyBook,
  SIMPLY_BOOK_URL,
} from '../../../../config';
import RadioButton from '../../../../components/Input/radioButton';
import {
  generaCurrency, generateUrlWeb, getNameFromButtonBlock, gotoUrl,
} from '../../../../Redux/Helpers';
import { addDetailWorkshopBooked } from '../../../../Redux/Actions/bookWorkshop';
import ModalCheckoutStripe from './ModalCheckoutStripe';
import { toastrError, toastrSuccess } from '../../../../Redux/Helpers/notification';
import { isMobile } from '../../../../DetectScreen';
import auth from '../../../../Redux/Helpers/auth';

const PLUSONEFREE = 'PLUSONEFREE';
const specialPromoMapping = {
  1: 'PLUSONEFREEPCA', // Perfume Creation Atelier
  3: 'PLUSONEFREELA', // Love Atelier
  8: 'PLUSONEFREEFA', // Family Atelier
  11: 'PLUSONEFREEAA', // Advanced Atelier
  18: 'PLUSONEFREEBA', // Bridal Atelier
  22: 'PLUSONEFREETBA', // Team Building Atelier
};


class Step5 extends Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
    this.state = {
      paymentMethod: 'stripe',
      detailWorkshopData: null,
      promoCode: '',
      isOpen: false,
      promocodeApplied: [],
    };

    this.api = new JsonRpcClient({
      endpoint: SIMPLY_BOOK_URL,
      headers: {
        'X-Company-Login': getCompanySimplyBook(),
        'X-Token': localStorage.getItem('tokenSimplyBook'),
      },
    });
  }

  onChange = (value) => {
    this.setState(prevState => ({ ...prevState, paymentMethod: value }));
  };

  transformPromoCode = (serviceId, count, code) => {
    if (code === PLUSONEFREE && Object.keys(specialPromoMapping).includes(serviceId) && count > 1) {
      return specialPromoMapping[serviceId];
    }
    return code;
  }

  onchangePromocode = (e) => {
    console.log(e);
    this.setState(prevState => (
      {
        ...prevState,
        promoCode: e.target.value,
        transformedPromoCode: this.transformPromoCode(
          this.props.serviceId, this.props.count, e.target.value,
        ),
      }
    ));
  }

  handleApplyPromocode = () => {
    const apiAdmin = new JsonRpcClient({
      endpoint: `${SIMPLY_BOOK_URL}admin`,
      headers: {
        'X-Company-Login': getCompanySimplyBook(),
        'X-User-Token': localStorage.getItem('tokenSimplyBookAdminToken') || '',
      },
    });
    apiAdmin.request(
      'applyPromoCode',
      this.props.bookWorkshop?.detailWorkshop?.invoice?.id,
      this.state.transformedPromoCode,
    ).then((res) => {
      if (res) {
        console.log(res, 'apply promocode');
        this.props.addDetailWorkshopBooked(res);
        toastrSuccess('Your promo code was successfully applied!');
        this.setState(prevState => ({ ...prevState, detailWorkshopData: res, promocodeApplied: res.promotion_instances }));
      }
    }).catch((err) => {
      if (err.response?.data?.message === 'Token Expired') {
        this.props.history.push(generateUrlWeb('/perfume-workshop'));
        toastrError(err.response?.data?.message);
        this.props.loadingPage(false);
        return;
      }
      toastrError('Could not find promotion code');
    });
  }

  postPaySuccessFull = () => {
    const idInvoice = this.props.detailWorkshop.invoice?.id || this.props.detailWorkshop.id;
    const { history } = this.props;
    const api = new JsonRpcClient({
      endpoint: `${SIMPLY_BOOK_URL}admin`,
      headers: {
        'X-Company-Login': getCompanySimplyBook(),
        'X-User-Token': localStorage.getItem('tokenSimplyBookAdminToken'),
      },
    });
    api.request(
      'confirmInvoice', idInvoice, 'stripe',
    ).then((resInfo) => {
      if (resInfo.status === 'paid') {
        history.push(generateUrlWeb(`/perfume-workshop/stripe/success/${idInvoice}`));
      } else {
        history.push(generateUrlWeb(`/perfume-workshop/stripe/cancel/${idInvoice}`));
      }
    }).catch((err) => {
      if (err.response?.data?.message === 'Token Expired') {
        this.props.history.push(generateUrlWeb('/perfume-workshop'));
        toastrError(err.response?.data?.message);
        return;
      }
      console.log(err?.message?.message);
    });
  };

  openModalCheckout = () => {
    const amout = this.props.detailWorkshop?.invoice?.amount || this.props.detailWorkshop?.amount;
    if (amout === 0) {
      this.postPaySuccessFull();
      return;
    }
    this.setState(prevState => ({ ...prevState, isOpen: true }));
  }

  gotoDone = () => {
    gotoUrl('https://www.maison21g.com/ae/perfume-workshop', this.props.history, true);
  }

  toggleCheckoutModal = () => this.setState(prevState => ({ ...prevState, isOpen: !prevState.isOpen }));

  groupBy = (a, keyFunction) => {
    const groups = {};
    a.forEach((el) => {
      const key = keyFunction(el);
      if (key in groups === false) {
        groups[key] = [];
      }
      groups[key].push(el);
    });
    return groups;
  }

  renderGroupProduct = (arr) => {
    const filtered = _.filter(arr, item => item.type === 'product');

    const byName = this.groupBy(filtered.filter(it => it.product.name), it => it.product.name);

    /* console.log(byName); */
    const countsExtended = Object.keys(byName).map((name) => {
      const byZone = this.groupBy(byName[name], it => it.product.name);
      const sum = byName[name].reduce((acc, it) => acc + it.product.price, 0);
      return {
        name,
        count: byName[name] ? byName[name].length : 1,
        valueSum: sum,
      };
    });

    return (
      countsExtended
        ? (
          _.map(countsExtended, item => (
            <div className="info">
              <p>{item?.count}</p>
              <p className="name">{item.name}</p>
              <p className="text-right" style={{ whiteSpace: 'nowrap' }}>
                {generaCurrency(item?.valueSum)}
              </p>
            </div>
          ))

        ) : <div />
    );
  }

  urlRedirect = (status, idInvoice) => {
    const baseURL = window.location.origin;
    const urlGenerate = generateUrlWeb(`/perfume-workshop/paypal/${status}/${idInvoice}`);
    const url = `${baseURL}${urlGenerate}`;
    return url;
  }

  render() {
    const {
      animate, prevStep, buttonBlock,
    } = this.props;
    const detailWorkshopDataA = this.props.bookWorkshop.detailWorkshop.invoice?.lines || this.props.bookWorkshop.detailWorkshop.lines;
    const {
      paymentMethod, promoCode, isOpen, promocodeApplied,
    } = this.state;
    const backText = getNameFromButtonBlock(buttonBlock, 'back');
    const breakdownText = getNameFromButtonBlock(buttonBlock, 'breakdown');
    const qtyText = getNameFromButtonBlock(buttonBlock, 'qty');
    const nameText = getNameFromButtonBlock(buttonBlock, 'name');
    const amountText = getNameFromButtonBlock(buttonBlock, 'amount');
    const methodText = getNameFromButtonBlock(buttonBlock, 'method');
    const payStripeText = getNameFromButtonBlock(buttonBlock, 'pay_stripe');
    const doneText = getNameFromButtonBlock(buttonBlock, 'Done');
    const isSingapore = auth.getCountry() === 'sg';

    return (
      <section className={`wrapper-step step-5 ${animate || 'slide-in-left'}`}>
        {!isMobile ? (
          <button type="button" onClick={() => prevStep(4)} className="btn-back">
            <img src={icPrev} alt="prev" />
            {' '}
            {backText || 'BACK'}
          </button>
        ) : ''}
        {/* <StripeCheckout /> */}
        <div className="wrapper-content">
          <div className="detail-form">
            <div className="form-group" ref={this.myRef}>
              <h4>{breakdownText || 'Breakdown'}</h4>
              <div className="total-info">
                <div className="info">
                  <p>{qtyText || 'Qty'}</p>
                  <p className="name">{nameText || 'Name'}</p>
                  <p className="text-right" style={{ whiteSpace: 'nowrap' }}>{amountText || 'Amount'}</p>
                </div>
                {detailWorkshopDataA ? _.map(detailWorkshopDataA, item => (item.type === 'booking'
                  ? (
                    <div className="info">
                      <p>{item?.qty}</p>
                      <p className="name">{item?.name ? item?.name.replace(/ *\([^)]*\) */g, '') : ''}</p>
                      <p className="text-right" style={{ whiteSpace: 'nowrap' }}>
                        {generaCurrency(item?.amount)}
                      </p>
                    </div>
                  ) : '')) : ''}
                {detailWorkshopDataA && _.find(detailWorkshopDataA, item => item.type === 'product') ? this.renderGroupProduct(detailWorkshopDataA) : ''}
                {/*

                <div className="info">
                                    <p>2</p>
                                    <p className="name">Free flow Prosseco</p>
                                    <p className="text-right">$80</p>
                                </div> */}
              </div>
              <div className="promo-code-box">
                <input value={promoCode} type="text" name="promoCode" placeholder="Promo Code" onChange={this.onchangePromocode} />
                <button type="button" onClick={this.handleApplyPromocode}>Apply</button>
              </div>
              {/* <div className="promo-code-apply">
                {promocodeApplied.length > 0 ? _.map(promocodeApplied, item => <span>{item.code}</span>) : ''}
              </div> */}
              <div className="foot-info">
                <span>Total</span>
                <span>
                  {generaCurrency(this.props.bookWorkshop.detailWorkshop?.invoice?.amount || this.props.bookWorkshop.detailWorkshop.amount)}
                </span>
              </div>
            </div>
          </div>
          <div className="summary-box">
            {
              isSingapore ? (
                <div className="wrapper-info">
                  <h4>{methodText || 'Method'}</h4>
                  <div className="wrapper-method">
                    {/* <div className={`payment-box ${paymentMethod === 'paypal' ? 'active' : ''}`} onClick={() => this.onChange('paypal')}>
                      <RadioButton
                        name="paymentMethod"
                        label="PayPal"
                        value="paypal"
                        selected={paymentMethod === 'paypal'}
                        onChange={this.onChange}
                      />
                      <img src={paypal} className="logo-paypal" alt="logo-paypal" />
                    </div> */}
                    <div className={`payment-box ${paymentMethod === 'stripe' ? 'active' : ''}`} onClick={() => this.onChange('stripe')}>
                      <RadioButton
                        name="paymentMethod"
                        label="Stripe"
                        value="stripe"
                        selected={paymentMethod === 'stripe'}
                        onChange={this.onChange}
                      />
                      <div className="d-flex align-items-center">
                        <img src={visa} alt="logo-visa" />
                        <img src={mastercard} alt="logo-mastercard" />
                      </div>
                    </div>
                  </div>
                </div>
              ) : (
                <div className="wrapper-info">
                  <h4>{methodText || 'Method'}</h4>
                  <div>
                    Service paid in the boutique
                  </div>
                </div>
              )
            }


            <div className="foot-info pt-2" style={{ borderTop: 'unset' }}>
              {paymentMethod === 'paypal' ? (
                <form action="https://www.paypal.com/cgi-bin/webscr" method="POST" id="ps_form" target="_top">

                  <input type="hidden" name="upload" value="1" />
                  <input type="hidden" name="cmd" value="_cart" />
                  <input type="hidden" name="business" value="dylan@maison21g.com" />

                  {detailWorkshopDataA ? _.map(detailWorkshopDataA, (item, index) => (
                    <div>
                      <input type="hidden" name={`item_name_${index + 1}`} value={item?.name} />
                      <input type="hidden" name={`quantity_${index + 1}`} value={item?.qty} />
                      <input type="hidden" name={`amount_${index + 1}`} value={item.final_price ? item.final_price : item.price} />
                    </div>
                  )) : ''}

                  <input type="hidden" name="currency_code" value={this.props.bookWorkshop.detailWorkshop?.invoice?.currency || this.props.bookWorkshop.detailWorkshop.currency} />
                  <input type="hidden" name="no_shipping" value="1" />
                  <input type="hidden" name="no_note" value="1" />

                  <input type="hidden" name="shopping_url" value="https://maison21g.simplybook.asia/v2/" />
                  <input type="hidden" name="cancel_return" value={this.urlRedirect('cancel', this.props.bookWorkshop?.detailWorkshop?.invoice?.id || this.props.bookWorkshop?.detailWorkshop?.id)} />
                  <input type="hidden" name="notify_url" value={`https://maison21g.simplybook.asia/v2/ext/invoice-payment/confirm/system/paypal/invoice_id/${this.props.bookWorkshop?.detailWorkshop?.invoice?.id || this.props.bookWorkshop?.detailWorkshop?.id}`} />
                  <input type="hidden" name="return" value={this.urlRedirect('success', this.props.bookWorkshop?.detailWorkshop?.invoice?.id || this.props.bookWorkshop?.detailWorkshop?.id)} />

                  <input type="hidden" name="lc" value="en" />

                  <input type="hidden" name="first_name" value={this.props.bookWorkshop?.dataForm?.name || ''} />
                  <input type="hidden" name="last_name" value="" />
                  <input type="hidden" name="address1" value="" />
                  <input type="hidden" name="address2" value="" />
                  <input type="hidden" name="city" value="" />
                  <input type="hidden" name="country" value="" />
                  <input type="hidden" name="zip" value="" />
                  <input type="hidden" name="phone" value={this.props.bookWorkshop?.dataForm?.phone || ''} />

                  <input type="hidden" name="email" value={this.props.bookWorkshop?.dataForm?.email || ''} />
                  <input type="hidden" name="custom" value="" />

                  <input type="hidden" name="charset" value="UTF-8" />

                  <input type="submit" value="NEXT" id="pay_btn" className="btn-confirm" />
                </form>
              )
                : (
                  isSingapore ? (
                    <button
                      type="button"
                      className="btn-confirm"
                      onClick={() => this.openModalCheckout()}
                    >
                      NEXT
                    </button>
                  ) : (
                    <button
                      type="button"
                      className="btn-confirm"
                      onClick={this.gotoDone}
                    >
                      {doneText}
                    </button>
                  )
                )}
              <ModalCheckoutStripe loadingPage={this.props.loadingPage} isOpen={isOpen} toggle={this.toggleCheckoutModal} email={this.props.bookWorkshop?.dataForm?.email || ''} payStripeText={payStripeText} />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  user: state.login && state.login.user ? state.login.user : null,
  bookWorkshop: state.bookWorkshop,
  detailWorkshop: state.bookWorkshop.detailWorkshop || null,
});

const mapDispatchToProps = {
  addDetailWorkshopBooked,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Step5));
