/* eslint-disable no-return-await */
/* eslint-disable react/sort-comp */
import Axios from 'axios';
import _ from 'lodash';
import moment from 'moment';
import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import JsonRpcClient from 'react-jsonrpc-client';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import {
  getCompanySimplyBook, SIMPLY_BOOK_URL,
} from '../../../../config';
import { isMobile } from '../../../../DetectScreen';
import icNext from '../../../../image/icon/ic-next-workshop.svg';
import icPrev from '../../../../image/icon/ic-prev-workshop.svg';
import TimelineItem from './TimelineItem';
import './styles.scss';


import { fetchTokenSimplyBookAdmin, getNameFromButtonBlock, generateUrlWeb } from '../../../../Redux/Helpers/index';
import { toastrError } from '../../../../Redux/Helpers/notification';


const baseUrl = 'https://user-api-v2.simplybook.me/admin';
class Step3 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTimecell: null,
      dataStep3: null,
      currentDate: null,
      timeLine: [],
      timeRange: [],
      timeSlotAvaible: [],
    };
    this.api = new JsonRpcClient({
      endpoint: SIMPLY_BOOK_URL,
      headers: {
        'X-Company-Login': getCompanySimplyBook(),
        'X-Token': localStorage.getItem('tokenSimplyBook'),
      },
    });
  }

  componentDidMount = () => {
    this.props.loadingPage(true);
    if (localStorage.getItem('tokenSimplyBookAdminToken')) {
      this.fetchData();
    } else {
      fetchTokenSimplyBookAdmin().then((res) => {
        if (res) {
          this.fetchData();
        }
      }).catch(err => (err ? toastrError('Get token err. Please try again') : ''));
    }
    this.getTimeRange();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.currentDate !== this.state.currentDate) {
      if (this.state.currentDate) {
        this.props.loadingPage(true);
      }
    }
  }

  getTimeRange() {
    const toInt = time => ((h, m) => h * 2 + m / 30)(...time.split(':').map(parseFloat));
    const toTime = int => [Math.floor(int / 2), int % 2 ? '30' : '00'].join(':');
    const range = (from, to) => Array(to - from + 1).fill().map((item, i) => from + i);
    const eachHalfHour = (t1, t2) => range(...[t1, t2].map(toInt)).map(toTime);
    const timeLineArr = eachHalfHour('11:00', '20:00');

    this.setState(prevState => ({ ...prevState, timeRange: timeLineArr }));
  }

  updateTimeAvailable = (newDate) => {
    if (newDate) this.handleGetBookingTime(moment(newDate).format('YYYY-MM-DD'), this.state.dataStep3);
  }

  hoverTimeCell = (index, stt) => {
    if (stt === 'enter') {
      this.setState({ activeTimecell: index });
    } else {
      this.setState({ activeTimecell: null });
    }
  }

  onChangeDate = (e) => {
    this.setState({ currentDate: e });
    console.log('currentDate:', e);
    this.updateTimeAvailable(e);
  }

  handleNextPrevDatePicker = (stt, currentDate) => {
    const myDate = new Date(currentDate);
    if (stt === 'next') {
      this.setState({ currentDate: new Date(myDate.setDate(myDate.getDate() + 1)) });
      this.updateTimeAvailable(new Date(myDate.setDate(myDate.getDate())));
    } else {
      this.setState({ currentDate: new Date(myDate.setDate(myDate.getDate() - 1)) });
      this.updateTimeAvailable(new Date(myDate.setDate(myDate.getDate())));
    }
  }

  addFullSlot = (data, booksExist) => {
    const { duration } = this.props;
    const newData = _.filter(data, (d) => {
      const startSlot = moment(d?.id);
      const endSlot = moment(d?.id).add(duration, 'minutes');
      return _.isEmpty(_.find(booksExist, x => moment(x.start_datetime) < endSlot && startSlot < moment(x.end_datetime)));
    });
    const step = (duration ? parseInt(duration, 10) : 60) / 30;
    const result = [];
    _.forEach(newData, (d) => {
      const ele = moment(d?.time, 'HH:mm:ss');
      result.push(ele.format('HH:mm'));
      for (let i = 0; i < step - 1; i += 1) {
        result.push(ele.add(30, 'minutes').format('HH:mm'));
      }
    });
    return result;
  }

  handleGetBookingTime = async (date, units) => {
    Object.keys(units).map(async (keyUnit) => {
      const tokenAdmin = localStorage.getItem('tokenSimplyBookAdminToken');
      const model = {
        date,
        serviceId: this.props.serviceId,
        providerId: keyUnit,
        token: tokenAdmin,
      };
      try {
        const responses = await Promise.all([this.fetchAvaileTime(model), this.fetchBookslist(model)]);
        const booksExist = _.filter(responses[1].data?.data, x => x.status !== 'canceled');
        const slotAvaiables = this.addFullSlot(responses[0]?.data, booksExist);

        this.setState(prevStates => ({ timeSlotAvaible: { ...prevStates.timeSlotAvaible, [keyUnit]: slotAvaiables } }));
      } catch (err) {
        console.log('handleGetBookingTime', err);
        if (err.response?.data?.message === 'Token Expired') {
          this.props.history.push(generateUrlWeb('/perfume-workshop'));
          this.props.loadingPage(false);
        }
        toastrError(err.response?.data?.message);
      }
    });
  }

  fetchData() {
    this.api.request('getUnitList').then((res) => {
      if (res) {
        this.setState({ dataStep3: res });

        this.api.request('getFirstWorkingDay', { event_id: this.props.serviceId }).then((resDate) => {
          if (resDate) {
            this.setState({ currentDate: this.props.dataTime && this.props.dataTime.date ? new Date(this.props.dataTime.date) : new Date(resDate) });

            const date = this.props.dataTime && this.props.dataTime.date ? moment(new Date(this.props.dataTime.date)).format('YYYY-MM-DD') : moment(new Date(resDate)).format('YYYY-MM-DD');

            // Map all provider to get all enable time & disable time
            this.handleGetBookingTime(date, res);
          }
        });
      }
    }).catch((err) => { console.log(err?.response?.data?.message); });
  }

  fetchAvaileTime = ({
    date, providerId, serviceId, token,
  }) => Axios.get(`${baseUrl}/schedule/available-slots?date=${date}&provider_id=${providerId}&service_id=${serviceId}`, {
    headers: {
      'Content-Type': 'application / json',
      'X-Company-Login': getCompanySimplyBook(),
      'X-Token': token || '',
    },
  });

  fetchBookslist = ({
    date, providerId, token,
  }) => Axios.get(`${baseUrl}/bookings?on_page=100&filter[providers]=${providerId}&filter[date]=${date}`, {
    headers: {
      'Content-Type': 'application / json',
      'X-Company-Login': getCompanySimplyBook(),
      'X-Token': token || '',
    },
  });

  render() {
    const {
      nextStep, prevStep, animate, buttonBlock, dataTime, duration,
    } = this.props;

    const settings = {
      infinite: false,
      swipeToSlide: false,
      speed: 500,
      arrows: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: true,
    };

    const {
      timeRange, dataStep3, currentDate, timeSlotAvaible,
    } = this.state;

    const backText = getNameFromButtonBlock(buttonBlock, 'back');
    const availableText = getNameFromButtonBlock(buttonBlock, 'available');
    const unavailableText = getNameFromButtonBlock(buttonBlock, 'unavailable');
    return (
      <section className={`step-3 ${animate || 'slide-in-left'}`}>
        {!isMobile ? (
          <button onClick={() => prevStep(2)} className="btn-back text-uppercase" type="button">
            <img src={icPrev} alt="prev" />
            {' '}
            {backText || 'BACK'}
          </button>
        ) : ''}
        <div className="time-select-box">
          <button
            type="button"
            onClick={() => {
              if (currentDate > new Date()) {
                this.handleNextPrevDatePicker('prev', currentDate);
              }
            }}
            className={`${currentDate > new Date() ? '' : 'disable-button'}`}
          >
            <img src={icPrev} alt="prev" />
            {' '}
            {!isMobile ? ' Prev date' : ''}
          </button>
          <DatePicker
            dateFormat="dd - MM - yyyy"
            minDate={new Date()}
            className="border-checkout w-100 mt-1"
            onChange={this.onChangeDate}
            selected={currentDate}
          />
          <button type="button" onClick={() => { this.handleNextPrevDatePicker('next', currentDate); }}>
            {!isMobile ? 'Next date ' : ''}
            <img src={icNext} alt="next" />
          </button>
        </div>
        <div className="wrapper-timeline-box">

          <Slider {...settings}>
            {dataStep3 ? Object.keys(dataStep3).map((key, index) => (dataStep3[key]
              ? (
                <TimelineItem
                  timeSlotAvaible={timeSlotAvaible[key]}
                  loadingPage={this.props.loadingPage}
                  duration={duration}
                  key={index}
                  currentDate={currentDate}
                  nextStep={nextStep}
                  timeLineArr={timeRange}
                  name={dataStep3[key].name}
                  picture_path={dataStep3[key].picture_path}
                  storeInfo={dataStep3[key]}
                  addTime={this.props.addTime}
                  dataTime={dataTime}
                />
              ) : '')) : ''}
          </Slider>

        </div>

        <div className="note-select-box">
          <div className="wrapper-note available">
            -
            {' '}
            {availableText || 'Available'}
          </div>
          <div className="wrapper-note unavailable">
            -
            {' '}
            {unavailableText || 'Unavailable'}
          </div>
        </div>
        {/* <div className="footer-step" >
                    <button className="btn-next-step" onClick={() => prevStep(2)}>PREV</button>
                </div> */}
      </section>
    );
  }
}

export default (Step3);
