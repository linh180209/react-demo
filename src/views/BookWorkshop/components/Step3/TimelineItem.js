import React, { Component, memo } from 'react';
import _ from 'lodash';
import 'react-datepicker/dist/react-datepicker.css';
import './styles.scss';
import moment from 'moment';

class TimelineItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTimecell: null,
    };
  }

  hoverTimeCell = (arrIndex, stt) => {
    if (stt === 'enter') {
      this.setState({ activeTimecell: arrIndex });
    } else {
      this.setState({ activeTimecell: [] });
    }
  }

  handleSelectTimeLine = (index, time, dataDetail) => {
    this.setState({ activeTimecell: index });
    this.props.addTime({
      date: this.props.currentDate,
      time: time || '',
      store: this.props.storeInfo,
      arrChecked: dataDetail.arrChecked,
    });
    setTimeout(() => {
      this.props.nextStep(4);
    }, 1000);
  }

  compareTime = (str1, str2, str3) => {
    const beginningTime = moment(str2, 'hh:mm:ss');
    const mainTime = moment(`${str1}:00`, 'hh:mm:ss');
    // if ((mainTime.isAfter(beginningTime) || mainTime.isSame(beginningTime)) && mainTime.isBefore(endTime)) {
    if (mainTime.isSame(beginningTime)) {
      return 1;
    }
    return 0;
  }

  compareTimeDuration = (str1, str2) => {
    const durationTime = moment(str2, 'hh:mm:ss').add(30, 'minutes');
    const mainTime = moment(`${str1}:00`, 'hh:mm:ss');
    // // if ((mainTime.isAfter(beginningTime) || mainTime.isSame(beginningTime)) && mainTime.isBefore(endTime)) {

    if (mainTime.isSame(durationTime)) {
      return 1;
    }
    return 0;
  }

  getIndexSelected = (data, index, listIndex) => {
    if (!data[index].checked) {
      return [];
    }
    const ele = _.find(listIndex, d => d.slice(0, d.length - 1).includes(index));
    return ele || [];
  }

  generateListIndex = (index, step) => _.map(_.range(0, 10), x => _.range(index + step * x, index + step * x + step + 1))

  cookData = (timeSlotAvaible, duration) => {
    const {
      timeLineArr, dataTime, currentDate, storeInfo,
    } = this.props;
    console.log('datass', {
      timeLineArr, dataTime, currentDate, storeInfo, timeSlotAvaible,
    });
    const step = (duration ? parseInt(duration, 10) : 60) / 30;
    const { activeTimecell } = this.state;
    const arrUnActiveTimeT = _.map(timeLineArr, d => (
      {
        time: d,
        checked: _.includes(timeSlotAvaible, d),
      }
    ));
    let indexFirstSlot = -1;
    const arrUnActiveTime = _.map(arrUnActiveTimeT, (d, index) => {
      if (d.checked && indexFirstSlot === -1) {
        indexFirstSlot = index;
      } else if (!d.checked) {
        indexFirstSlot = -1;
      }
      const listIndex = indexFirstSlot > -1 ? this.generateListIndex(indexFirstSlot, step) : [];
      return {
        ...d,
        arrChecked: this.getIndexSelected(arrUnActiveTimeT, index, listIndex),
      };
    });
    this.props.loadingPage(false);
    return _.map(arrUnActiveTime, (item, index) => (
      <div
        onClick={() => (item.arrChecked.length > 0 ? this.handleSelectTimeLine(index, arrUnActiveTime[item.arrChecked[0]].time, item) : '')}
        className={`
        provider-item
        ${item.checked ? '' : 'disabled-item'}
        ${activeTimecell && activeTimecell.length > 0 && (activeTimecell.includes(index)) ? 'active-time-cell' : ''}
        ${dataTime && dataTime.date && currentDate && this.isSameDate(currentDate, dataTime?.date) && storeInfo.id === dataTime.store.id && dataTime.arrChecked && dataTime.arrChecked.length > 0 && (dataTime.arrChecked.includes(index)) ? 'active-time-cell' : ''}
        
        `}
        key={index}
        onMouseEnter={() => this.hoverTimeCell(item.arrChecked, 'enter')}
        onMouseLeave={() => this.hoverTimeCell(item.arrChecked, 'leave')}
      >
        <span className="time-cell">{item.time}</span>
      </div>
    ));
  };

  isSameDate = (d1, d2) => {
    if (moment(d1).isSame(moment(d2))) {
      return true;
    }
    return false;
  }

  render() {
    const {
      name, duration, timeSlotAvaible,
    } = this.props;

    return (
      <div className="timeline-box">
        <div className="wrapper-image" style={{ background: `url('https://maison21g.simplybook.me${this.props.picture_path ? this.props.picture_path : ''}')` }} />
        <h4>{name}</h4>
        <div className="timeline-selection">
          {timeSlotAvaible && duration
            ? this.cookData(timeSlotAvaible, duration) : ''}
        </div>
      </div>
    );
  }
}

export default memo(TimelineItem);
