import React from 'react';
import { CardElement } from 'react-stripe-elements';
import { Label } from 'reactstrap';

class CardSection extends React.Component {
  render() {
    return (
      <Label>
        <CardElement style={{ base: { fontSize: '14px' } }} hidePostalCode />
      </Label>
    );
  }
}

export default CardSection;
