import React from 'react';
import { injectStripe } from 'react-stripe-elements';
import axios from 'axios';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import JsonRpcClient from 'react-jsonrpc-client';
import CardSection from './CardSection';
import { toastrError } from '../../../../Redux/Helpers/notification';
import { generateUrlWeb } from '../../../../Redux/Helpers/index';
import { getCompanySimplyBook, SIMPLY_BOOK_URL } from '../../../../config';
import { MESSAGE_SHOW } from '../../../../components/Loading';

class InjectedCheckoutForm extends React.Component {
  constructor(props) {
    super(props);
    MESSAGE_SHOW.data = 'Your payment is being processed. Please do not refresh or close this page.';
  }

  componentWillUnmount() {
    MESSAGE_SHOW.data = undefined;
  }

  handleSubmit = (ev) => {
    // We don't want to let default form submission happen here, which would refresh the page.
    this.props.loadingPage(true);
    ev.preventDefault();

    // Use Elements to get a reference to the Card Element mounted somewhere
    // in your <Elements> tree. Elements will know how to find your Card Element
    // because only one is allowed.
    // See our getElement documentation for more:
    // https://stripe.com/docs/stripe-js/reference#elements-get-element
    const cardElement = this.props.elements.getElement('card');

    // From here we can call createPaymentMethod to create a PaymentMethod
    // See our createPaymentMethod documentation for more:
    // https://stripe.com/docs/stripe-js/reference#stripe-create-payment-method
    if (localStorage.getItem('tokenSimplyBookAdminToken')) {
      this.props.stripe.createPaymentMethod({
        type: 'card',
        card: cardElement,
        billing_details: {
          name: this.props.dataUser?.name || '-',
          email: this.props.dataUser?.email || '-',
          phone: this.props.dataUser?.phone || '-',
        },
      })
        .then((resPaymentMethod) => {
          console.log('Received Stripe PaymentMethod:', resPaymentMethod, resPaymentMethod);
          if (resPaymentMethod.paymentMethod) {
            const detailWorkshopData = this.props.detailWorkshop.invoice?.lines || this.props.detailWorkshop.lines;
            const idInvoice = this.props.detailWorkshop.invoice?.id || this.props.detailWorkshop.id;
            // console.log(idInvoice, 'idInvoice');
            const params = new URLSearchParams({});
            const name = this.props.dataUser?.name;
            if (detailWorkshopData) {
              detailWorkshopData.forEach((item, index) => {
                params.append(`metadata[${index + 1}]`, item.description_string);
              });
            }
            axios.post('https://api.stripe.com/v1/payment_intents',
              `${new URLSearchParams({
                amount: (this.props.detailWorkshop?.invoice?.amount || this.props.detailWorkshop?.amount) * 100,
                currency: 'SGD',
                'payment_method_types[]': 'card',
              })}&${params}${name ? `&description="Client name: ${name}"` : ''}`,
              {
                headers: {
                  'Content-type': 'application/x-www-form-urlencoded',
                  Authorization: 'Bearer sk_live_xBZGxVpecV9zM8AuQ1cz8sdp',
                },
              })
              .then((res) => {
                if (res) {
                  const secrectClientID = res.data.client_secret;
                  this.props.stripe.confirmCardPayment(secrectClientID, {
                    payment_method: {
                      card: cardElement,
                      billing_details: {
                        name: this.props.dataUser?.name || '-',
                        email: this.props.dataUser?.email || '-',
                        phone: this.props.dataUser?.phone || '-',
                      },
                    },
                  }).then((resStatus) => {
                    if (resStatus.paymentIntent && resStatus.paymentIntent.status === 'succeeded') {
                      const { history } = this.props;
                      const api = new JsonRpcClient({
                        endpoint: `${SIMPLY_BOOK_URL}admin`,
                        headers: {
                          'X-Company-Login': getCompanySimplyBook(),
                          'X-User-Token': localStorage.getItem('tokenSimplyBookAdminToken'),
                        },
                      });
                      api.request(
                        'confirmInvoice', idInvoice, 'stripe',
                      ).then((resInfo) => {
                        this.props.loadingPage(false);
                        if (resInfo.status === 'paid') {
                          history.push(generateUrlWeb(`/perfume-workshop/stripe/success/${idInvoice}`));
                        } else {
                          history.push(generateUrlWeb(`/perfume-workshop/stripe/cancel/${idInvoice}`));
                        }
                      }).catch((err) => {
                        if (err.response?.data?.message === 'Token Expired') {
                          history.push(generateUrlWeb('/perfume-workshop'));
                          toastrError(err.response?.data?.message);
                          return;
                        }
                        console.log(err?.message?.message); this.props.loadingPage(false);
                      });
                    } else {
                      const { history } = this.props;
                      this.props.loadingPage(false);
                      toastrError(resStatus.error.message);
                      setTimeout(() => {
                        history.push(generateUrlWeb(`/perfume-workshop/stripe/cancel/${idInvoice}`));
                      }, 1000);
                    }
                  }).catch(err => console.log(err));
                  // You can also use confirmCardSetup with the SetupIntents API.
                  // See our confirmCardSetup documentation for more:
                  // https://stripe.com/docs/stripe-js/reference#stripe-confirm-card-setup
                  axios.post('https://api.stripe.com/v1/setup_intents',
                    new URLSearchParams({
                      'payment_method_types[]': 'card',
                    }),
                    {
                      headers: {
                        'Content-type': 'application/x-www-form-urlencoded',
                        Authorization: 'Bearer sk_live_xBZGxVpecV9zM8AuQ1cz8sdp',
                      },
                    })
                    .then((resp) => {
                      if (resp) {
                        const secrectClientSetupID = resp.data.client_secret;
                        this.props.stripe.confirmCardSetup(secrectClientSetupID, {
                          payment_method: {
                            card: cardElement,
                          },
                        });
                        // this.props.stripe.retrievePaymentIntent(secrectClientID).then((response) => {
                        //   if (response.paymentIntent && response.paymentIntent.status === 'succeeded') {

                        //   } else {
                        //     history.push(`/sg/perfume-workshop/stripe/cancel/${detailWorkshop?.invoice.id}`);
                        //   }
                        // });
                      }
                    });

                  // You can also use createToken to create tokens.
                  // See our tokens documentation for more:
                  // https://stripe.com/docs/stripe-js/reference#stripe-create-token
                  // With createToken, you will not need to pass in the reference to
                  // the Element. It will be inferred automatically.
                  this.props.stripe.createToken({
                    type: 'card',
                    name: this.props.dataUser?.name || '-',
                    email: this.props.dataUser?.email || '-',
                    phone: this.props.dataUser?.phone || '-',
                  });
                  // token type can optionally be inferred if there is only one Element
                  // with which to create tokens
                  // this.props.stripe.createToken({name: 'Jenny Rosen'});

                  // You can also use createSource to create Sources.
                  // See our Sources documentation for more:
                  // https://stripe.com/docs/stripe-js/reference#stripe-create-source
                  // With createSource, you will not need to pass in the reference to
                  // the Element. It will be inferred automatically.
                  this.props.stripe.createSource({
                    type: 'card',
                    owner: {
                      name: this.props.dataUser?.name || '-',
                      email: this.props.dataUser?.email || '-',
                      phone: this.props.dataUser?.phone || '-',
                    },
                  });
                }
              }).catch((err) => { console.log(err); this.props.loadingPage(false); });
          } else if (resPaymentMethod.error) { toastrError(resPaymentMethod.error.message); this.props.loadingPage(false); }
        }).catch((err) => { console.log(err); this.props.loadingPage(false); });
    } else {
      toastrError('Payment error');
    }
  };

  render() {
    return (
      <React.Fragment>
        <form onSubmit={this.handleSubmit}>
          {/* <AddressSection /> */}
          <CardSection />
          <button name="btnSubmit" className="btn-pay" type="submit">
            {this.props.payStripeText || 'PAY SGD'}
            {' '}
            {this.props.detailWorkshop?.invoice?.amount || this.props.detailWorkshop?.amount || ''}
          </button>
        </form>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  dataUser: state.bookWorkshop.dataForm || null,
  detailWorkshop: state.bookWorkshop.detailWorkshop || null,
});

export default withRouter(connect(mapStateToProps, null)(injectStripe(InjectedCheckoutForm)));
