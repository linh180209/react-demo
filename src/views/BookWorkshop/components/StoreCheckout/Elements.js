import React from 'react';
import { Elements } from 'react-stripe-elements';
import InjectedCheckoutForm from './InjectedCheckoutForm';

class ElementsComponent extends React.Component {
  render() {
    return (
      <Elements>
        <InjectedCheckoutForm loadingPage={this.props.loadingPage} payStripeText={this.props.payStripeText} />
      </Elements>
    );
  }
}

export default ElementsComponent;
