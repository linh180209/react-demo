import React from 'react';
import { StripeProvider } from 'react-stripe-elements';

import StripeScriptLoader from 'react-stripe-script-loader';
import ElementsComponent from './Elements';

class StoreCheckout extends React.Component {
  render() {
    return (
      <StripeScriptLoader
        uniqueId="stripe-js"
        script="https://js.stripe.com/v3/"
        loader="Loading..."
      >
        <StripeProvider apiKey="pk_live_9ERNmOrnzyWFu5QXuNctGDBI">
          <ElementsComponent loadingPage={this.props.loadingPage} payStripeText={this.props.payStripeText} />
        </StripeProvider>
      </StripeScriptLoader>
    );
  }
}

export default StoreCheckout;
