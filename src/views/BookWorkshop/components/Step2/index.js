import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import JsonRpcClient from 'react-jsonrpc-client';
import checkedIc from '../../../../image/icon/checked-workshop-ic.svg';
import { getCompanySimplyBook, SIMPLY_BOOK_URL } from '../../../../config';
import './styles.scss';
import {
  generaCurrency, getNameFromButtonBlock, segementWorkshopAddOn, segmentWorkshopCheckOutInitiated,
} from '../../../../Redux/Helpers/index';
import { isMobile } from '../../../../DetectScreen';
import icPrev from '../../../../image/icon/ic-prev-workshop.svg';
import auth from '../../../../Redux/Helpers/auth';

class Step2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataAddons: null,
      dataSelected: [],
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { eventFiltered, totalPrice } = nextProps;
    if (eventFiltered && eventFiltered !== prevState.eventFiltered) {
      segmentWorkshopCheckOutInitiated(eventFiltered, totalPrice);
      return { eventFiltered };
    }
    return null;
  }

  componentDidMount = () => {
    const token = localStorage.getItem('tokenSimplyBook');
    const api = new JsonRpcClient({
      endpoint: SIMPLY_BOOK_URL,
      headers: {
        'X-Company-Login': getCompanySimplyBook(),
        'X-Token': token,
      },
    });
    api.request(
      'getProductList', { service_id: this.props.serviceId },
    ).then((res) => {
      if (res?.length > 0) {
        const serviceAddOns = _.filter(res, (item) => {
          if (item?.services?.length > 0) {
            const isExist = _.find(item.services, element => element?.service_id == this.props.serviceId);
            if (isExist) return item;
          }
        });
        this.setState({ dataAddons: !serviceAddOns?.length ? res : serviceAddOns });
      }
    }).catch(err => console.log(err));
  }

  handleSelectProduct = (item) => {
    const cloneArr = [...this.props.dataAddOns];
    const index = cloneArr.findIndex(i => i.id === item.id);
    if (index !== -1) {
      cloneArr.splice(index, 1);
      this.setState({ dataSelected: cloneArr });
    } else {
      cloneArr.push(item);
      this.setState({
        dataSelected: cloneArr,
      });
    }

    this.props.addAddOns(cloneArr);
  }

  backFunc = () => {
    if (auth.getCountry() === 'ae') {
      window.location.href = 'https://www.maison21g.com/ae/perfume-workshop';
    } else {
      window.location.href = 'https://www.maison21g.com/sg/perfume-workshop';
    }
  }

  render() {
    const {
      nextStep, animate, totalPrice, count, buttonBlock, dataAddOns,
    } = this.props;
    const { dataAddons, dataSelected } = this.state;
    const totalPriceExtra = _.reduce(
      this.state.dataSelected,
      (accumulator, currentValue) => accumulator + currentValue?.price,
      0,
    ) || 0;
    const totalText = getNameFromButtonBlock(buttonBlock, 'total');
    const backText = getNameFromButtonBlock(buttonBlock, 'back');
    const addonsUnitText = getNameFromButtonBlock(buttonBlock, 'addons_unit');
    return (
      <section className={`wrapper-step step-2 ${animate || 'slide-in-left'}`}>
        {!isMobile ? (
          <button onClick={() => this.backFunc()} className="btn-back text-uppercase" type="button">
            <img src={icPrev} alt="prev" />
            {' '}
            {backText || 'BACK'}
          </button>
        ) : ''}
        {/* <button className="btn-back"><img src={icPrev} alt='prev' /> BACK</button> */}
        {dataAddons ? _.map(dataAddons, (item, index) => (
          <div className="list-type">
            <div className={`item-type-workshop ${dataAddOns.find(i => i.id === item.id) ? 'active' : ''}`} active onClick={() => this.handleSelectProduct(item)} key={index}>
              <div className="square-check">
                <img src={checkedIc} alt="checked" />
              </div>
              <div className="info">
                <h4>{item?.name}</h4>
                <p className="m-0">
                  {generaCurrency(item?.price)}
                  {' '}
                  {addonsUnitText}
                </p>
              </div>
            </div>
          </div>
        )) : ''}
        <div className="footer-step">
          <div className="info-step1">
            <span>{totalText || 'Total'}</span>
            <span className="total-price">
              {generaCurrency(totalPrice + (totalPriceExtra * count))}
            </span>
          </div>
          <button
            className="btn-next-step"
            onClick={() => {
              nextStep(3);
              segementWorkshopAddOn(this.state.dataSelected);
            }}
            type="button"
          >
            {this.props.nextText || 'NEXT'}

          </button>
        </div>
      </section>
    );
  }
}


export default withRouter(Step2);
