import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Row, Col } from 'reactstrap';
import {
  Player, ControlBar, Shortcut, BigPlayButton,
} from 'video-react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import windowSize from 'react-window-size';

import Title from '../../components/StepQuestion/title';
import ButtonYesNo from '../../components/StepQuestion/buttonYesNo';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { GET_BENEFIT_CHOISE_URL } from '../../config';
import { addProductsRequest } from '../../Redux/Actions/products';
import { toastrError } from '../../Redux/Helpers/notification';
import BackGroundVideo from '../../components/backgroundVideo';
import { generateUrlWeb } from '../../Redux/Helpers';

class Benefits extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      isEnd: false,
      data: [],
      isCreateData: false,
      urlBenifit: '',
    };
  }

  componentDidMount = () => {
    if (this.props.products.length !== 2) {
      this.props.history.push(generateUrlWeb('/quiz'));
      return;
    }
    const { questions } = this.props;
    const benifit = _.find(questions, x => x.type === 'BENEFIT');
    this.setState({ urlBenifit: benifit.video_urls[0], urlImage: benifit.image_urls[0] });
    this.focusInput();
    if (this.state.data) {
      const data = this.createData(this.props.benefits);
      this.setState({ data });
    }
    // this.refs.player.subscribeToStateChange(this.handleStateChange);
  }

  componentDidUpdate = () => {
    if (this.state.isEnd) {
      // this.refs.player.subscribeToStateChange(this.handleStateChange);
    }
    if (this.state.isCreateData) {
      const { benefits } = this.state;
      const data = this.createData(benefits);
      this.setState({ data });
      this.state.isCreateData = false;
    }
  }

  componentWillUnmount = () => {
    this.cancelFocus();
  }

  onChange = (value) => {
    this.setState({ value });
    const choise = _.find(this.state.data, x => x.answer === value);
    if (choise) {
      this.getResult(choise);
    }
  }

  onKeyPress = (e) => {
    const d = _.find(this.data, x => x.type.toUpperCase() === e.key.toUpperCase());
    if (d) {
      this.onChange(d.answer);
    }
  }

  // onEnd = () => {
  //   this.props.history.push('/letsguideyou/results');
  // }

  getResult = (choise) => {
    const { gender } = this.props.match.params;
    const options = {
      // url: `${GET_BENEFIT_CHOISE_URL}?gender=${gender}&benefit=${choise.id}`,
      url: `${GET_BENEFIT_CHOISE_URL}?gender=${gender}`,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && result.length > 0) {
        const benefitChoose = _.find(result, x => x.benefit.title === choise.title);
        this.props.addProductsRequest(benefitChoose);
        this.props.addProductsRequest(result);
        this.setState({ isEnd: true });
        setTimeout(() => {
          this.props.history.push(generateUrlWeb('/quiz/outcome'));
        }, 3000);
      } else {
        toastrError('No the result');
      }
    }).catch((e) => {
      toastrError(e.message);
    });
  }

  static getDerivedStateFromProps = (nextProps, prevState) => {
    const { benefits } = nextProps;
    const object = {};
    if (benefits !== prevState.benefits) {
      _.assign(object, { isCreateData: true, benefits });
    }
    return !_.isEmpty(object) ? object : null;
  }

  createData = (benefits) => {
    const data = [];
    _.forEach(benefits, (item) => {
      const d = {
        id: item.id,
        answer: item.title,
        title: item.title,
        type: item.title.charAt(0).toUpperCase(),
        url: item.video_urls && item.video_urls.length > 0 ? item.video_urls[0] : undefined,
      };
      data.push(d);
    });
    return data;
  }

  // handleStateChange = () => {
  //   const { player } = this.refs.player.getState();
  //   if (player.ended) {
  //     this.props.history.push('/letsguideyou/results');
  //   }
  // }

  focusInput = () => {
    document.addEventListener('keydown', this.onKeyPress, true);
  }

  cancelFocus = () => {
    document.removeEventListener('keydown', this.onKeyPress, true);
  }

  render() {
    const { no } = this.props;
    const {
      value, isEnd, data, urlBenifit, urlImage,
    } = this.state;
    // const { windowWidth, windowHeight } = this.props;
    // const isSreenWeb = windowWidth > windowHeight;
    return (
      <div>
        {
          isEnd
            ? (
              <div>
                {/* <BackGroundVideo url={urlEnd} isVideo loop={false} /> */}
                {/* <Player
                  ref="player"
                  playsinline
                  autoPlay
                  muted
                  autobuffer
                  aspectRatio={isSreenWeb ? '16:9' : 'auto'}
                  src={urlEnd}
                >
                  <ControlBar disableCompletely disableDefaultControls />
                  <Shortcut dblclickable />
                  <BigPlayButton />
                </Player> */}
              </div>
            )
            : (
              <div>
                {/* <div className="background--letsguideyou" /> */}
                <BackGroundVideo url={urlBenifit} urlImage={urlImage} loop={false} />
                <div className="animated fadeIn">
                  <Row className="bg-benenifits">
                    <Col md="3" />
                    <Col md="6">
                      <div id={this.props.id} className="text__container_wrap">
                        <Title no={no} title="why do you want to wear perfume?" />
                        <div style={{ marginTop: '32px', display: 'flex', flexDirection: 'column' }}>
                          {
                  _.map(data, (item) => {
                    const {
                      answer, type, url,
                    } = item;
                    return (
                      <ButtonYesNo
                        isCheck={value === answer}
                        title={item.title}
                        type={type}
                        onChange={this.onChange}
                        url={url}
                        className="mb-2"
                        isBackgroundVideo
                      />
                    );
                  })
                }

                        </div>
                      </div>
                    </Col>
                    <Col md="3" />
                  </Row>
                </div>
              </div>
            )
        }

      </div>
    );
  }
}

Benefits.defaultProps = {
  no: '11',
  benefits: [],
  products: [],
  match: {
    params: {
      gender: '',
    },
  },
};

Benefits.propTypes = {
  id: PropTypes.string.isRequired,
  no: PropTypes.string,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  benefits: PropTypes.arrayOf(PropTypes.object),
  products: PropTypes.arrayOf(PropTypes.object),
  match: PropTypes.shape({
    params: PropTypes.shape({
      gender: PropTypes.string,
    }),
  }),
  addProductsRequest: PropTypes.func.isRequired,
  questions: PropTypes.arrayOf(PropTypes.object).isRequired,
  // windowWidth: PropTypes.number.isRequired,
  // windowHeight: PropTypes.number.isRequired,
};

function mapStateToProps(state) {
  return {
    products: state.products,
    benefits: state.benefits,
    questions: state.questions,
  };
}

const mapDispatchToProps = {
  addProductsRequest,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(windowSize(Benefits)));
