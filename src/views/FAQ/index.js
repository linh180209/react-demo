import React, { Component } from 'react';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import ReactHtmlParser from 'react-html-parser';

import './styles.scss';
import TextBlock from '../../components/TextBlock';
import {
  convertHeaderToSize, scrollTop, fetchCMSHomepage, getSEOFromCms, googleAnalitycs, generateHreflang, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers';
// import Header from '../../components/HomePage/header';
import addCmsRedux from '../../Redux/Actions/cms';
import CollapseItem from './collapseItem';
import BlockIconLine from '../../components/HomeAlt/BlockIconLine';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';

const getCms = (faqBlock) => {
  if (faqBlock) {
    const seo = getSEOFromCms(faqBlock);
    const { header_text: headerText, body } = faqBlock;
    const faqBlocks = _.filter(body, x => x.type === 'faq_block');
    const headerBlock = _.find(body, x => x.type === 'header_and_paragraph_block');
    const iconBlock = _.find(body, x => x.type === 'icons_block');
    return {
      headerText, seo, faqBlocks, headerBlock, iconBlock,
    };
  }
  return {
    headerText: '', seo: undefined, faqBlocks: [], headerBlock: undefined,
  };
};
class FAQPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headerText: '',
      faqBlocks: [],
      seo: undefined,
      headerBlock: undefined,
      iconBlock: undefined,
    };
  }


  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/faq');

    this.fetchDataInit();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const faqBlock = _.find(cms, x => x.title === 'FAQ-v2');
    if (!faqBlock) {
      const cmsData = await fetchCMSHomepage('faq');
      this.props.addCmsRedux(cmsData);
      const dataCms = getCms(cmsData);
      this.setState(dataCms);
    } else {
      const dataCms = getCms(faqBlock);
      this.setState(dataCms);
    }
  }

  render() {
    const {
      headerText, headerBlock, seo, faqBlocks, iconBlock,
    } = this.state;
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/faq')}
        </MetaTags>
        {/* <Header />
        { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <HeaderHomePageV3 />
        <div className="div-faq">
          <h1>{headerText}</h1>
          <div className="content-faq">
            {
              _.map(faqBlocks, d => (
                <CollapseItem
                  headerText={d.value?.header?.header_text}
                >
                  {
                    _.map(d.value?.faq, x => (
                      <CollapseItem
                        headerText={x.value?.header?.header_text}
                        className="sub-item"
                      >
                        <div className="text-answer">
                          {ReactHtmlParser(x.value?.paragraph)}
                        </div>
                      </CollapseItem>
                    ))
                  }
                </CollapseItem>
              ))
            }
            {
              headerBlock && (
                <div className="div-need-more">
                  <h2>
                    {headerBlock?.value?.header?.header_text}
                  </h2>
                  <div className="des-need-more">
                    {ReactHtmlParser(headerBlock?.value?.paragraph)}
                  </div>
                </div>
              )
            }
          </div>
          {
            iconBlock && (
              <BlockIconLine data={iconBlock} />
            )
          }
        </div>
        <FooterV2 />
      </div>
    );
  }
}

FAQPage.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
};
function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};

export default connect(mapStateToProps, mapDispatchToProps)(FAQPage);
