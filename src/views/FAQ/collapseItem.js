import React, { useState, useEffect } from 'react';
import { Collapse } from 'react-collapse';
import classnames from 'classnames';

import icCollapseOpen from '../../image/icon/collapse-open.svg';
import icCollapseClose from '../../image/icon/collapse-close.svg';

function CollapseItem(props) {
  const [isShow, setIsShow] = useState(false);
  return (
    <div className={classnames('collapse-item', props.className)}>
      <button type="button" onClick={() => setIsShow(!isShow)}>
        <img src={isShow ? icCollapseOpen : icCollapseClose} alt="icon" />
        <span>{props.headerText}</span>
      </button>
      <Collapse
        isOpened={isShow}
      >
        {props.children}
      </Collapse>
    </div>
  );
}

export default CollapseItem;
