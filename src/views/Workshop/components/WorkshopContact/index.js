/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './styles.scss';

class WorkshopContact extends Component {
  render() {
    const { paragraphBlock } = this.props;
    return (
      <div className="workshop__contact">
        <div className="container-fluid custom">
          <div className="row">
            <div className="col-12 text-center" dangerouslySetInnerHTML={{ __html: paragraphBlock }}>
              {/* {paragraphBlock} */}
              {/* <h4>
                Interested in a custom atelier tailored for
                <br />
                you? No worries
                {' '}
                <Link to="/">
                  send us an email.
                </Link>
              </h4>
              <p>
                workshop@maison21g.com
                <br />
                +65 6226 2677
              </p> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default WorkshopContact;
