import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import MetaTags from 'react-meta-tags';

import HeaderHomePage from '../../components/HomePage/header';
import {
  scrollTop, fetchCMSHomepage, getSEOFromCms, googleAnalitycs, getNameFromButtonBlock, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers/index';
import addCmsRedux from '../../Redux/Actions/cms';
import WorkshopDetail from './components/WorkshopDetail';
import WorkshopDetailMobile from './components/WorkshopDetailMobile';
import WorkshopList from './components/WorkshopList';
import WorkshopContact from './components/WorkshopContact';
import WorkshopBanner from './components/WorkshopBanner';
import { isMobile } from '../../DetectScreen';
import FooterV2 from '../../views2/footer';

const getCms = (workshopCMS) => {
  const objectReturn = {
    headerBlock: '',
    textBlock: '',
    imageBlock: {},
    seo: undefined,
    paragraphBlock: '',
    ateliersBlock: [],
  };
  if (!_.isEmpty(workshopCMS)) {
    const seo = getSEOFromCms(workshopCMS);
    const { body } = workshopCMS;
    if (body) {
      const headerBlock = _.find(body, x => x.type === 'header_block').value;
      const textBlock = _.find(body, x => x.type === 'text_block').value;
      const imageBlock = _.find(body, x => x.type === 'image_block').value;
      const buttonBlock = _.filter(body, x => x.type === 'button_block');
      const paragraphBlock = _.find(body, x => x.type === 'paragraph_block').value;
      const ateliersBlock = _.filter(body, x => x.type === 'ateliers_block').map(ele => ele.value).map(ele => ele[0]).map(ele => ele.value);
      _.assign(objectReturn, {
        headerBlock, textBlock, seo, imageBlock, paragraphBlock, ateliersBlock, buttonBlock,
      });
    }
  }
  return objectReturn;
};

class Workshop extends Component {
  constructor(props) {
    super(props);

    this.state = {
      seo: undefined,
      animate: {},
      minHeight: window.innerWidth * 0.64,
      showContactAndFooter: true,
      visibleDetailMobile: false,
      headerBlock: '',
      textBlock: '',
      imageBlock: {},
      paragraphBlock: '',
      ateliersBlock: [],
      buttonBlock: [],

      currentWorkshopIndex: 0,
      detailInit: true,
      currentMenuText: '',
      showingDetail: false,
    };
  }

  componentDidMount = () => {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/perfume-workshop');
    this.fetchDataInit();
  }

  componentDidUpdate(prevProps) {
    const { match } = prevProps;
    if (match && match.params && match.params.workshop && match.params.workshop !== this.state.workshop) {
      googleAnalitycs(window.location.pathname);
    }
    if (match.params.workshop !== this.state.workshop) {
      this.setState({ workshop: match.params.workshop });
    }
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;

    const workshopCMS = _.find(cms, x => x.title === 'workshop v2');
    if (!workshopCMS) {
      const cmsData = await fetchCMSHomepage('workshop-v2');
      this.props.addCmsRedux(cmsData);

      const dataFromStore = getCms(cmsData);
      if (dataFromStore.ateliersBlock.length > 5) {
        _.assign(dataFromStore, { minHeight: window.innerWidth * 0.64 });
      } else {
        _.assign(dataFromStore, { minHeight: window.innerWidth * 0.34 });
      }
      this.setState(dataFromStore);
    } else {
      const dataFromStore = getCms(workshopCMS);
      if (dataFromStore.ateliersBlock.length > 5) {
        _.assign(dataFromStore, { minHeight: window.innerWidth * 0.64 });
      } else {
        _.assign(dataFromStore, { minHeight: window.innerWidth * 0.34 });
      }
      this.setState(dataFromStore);
    }
  }

  gotoWithUrl = (id) => {
    const ele = this.state.ateliersBlock[id];
    const url = ele.header.header_text.toLowerCase().replace(/ /g, '-');
    const pathName = this.props.location.pathname ? this.props.location.pathname.split('/') : [];
    console.log(pathName, pathName.length);
    if (pathName.length >= 3 && pathName[1] === 'sg' && pathName[2] === 'perfume-workshop') {
      this.props.history.push(generateUrlWeb(`/perfume-workshop/${url}`));
    } else {
      this.props.history.push(generateUrlWeb(`/perfume-workshop/${url}`));
    }
  }

  gotoBack = () => {
    this.props.history.push(generateUrlWeb('/perfume-workshop'));
  }

  showDetails = (id = 0) => {
    console.log('showDetails', id);
    if (!isMobile) {
      this.setState({
        animate: {
          list: 'slide-out-right',
          detail: 'slide-in-left',
        },
        minHeight: 1400,
        currentWorkshopIndex: id,
      });
    } else {
      this.setState({
        animate: {
          detailMobie: ['scale-in-bl'],
        },
        showContactAndFooter: false,
        visibleDetailMobile: true,
        currentWorkshopIndex: id,
      });
    }
  }

  hideDetails = () => {
    const { ateliersBlock } = this.state;
    if (!isMobile) {
      this.setState({
        animate: {
          list: 'slide-in-right',
          detail: 'slide-out-left',
        },
        minHeight: ateliersBlock.length > 4 ? window.innerWidth * 0.64 : window.innerWidth * 0.34,
      });
    } else {
      this.setState({
        animate: {
          banner: 'scale-in-tr',
          list: '',
        },
        showContactAndFooter: true,
        visibleDetailMobile: false,

      });
    }
  }

  render() {
    const {
      headerBlock,
      textBlock,
      imageBlock,
      paragraphBlock,
      ateliersBlock,
      buttonBlock,
      showContactAndFooter,
      seo,
      animate,
      minHeight,
      visibleDetailMobile,
      currentWorkshopIndex,
      detailInit,
      currentMenuText,
      showingDetail,
    } = this.state;
    const menuText = this.props.match.params.workshop ? this.props.match.params.workshop : '';
    const menuIndex = ateliersBlock.length ? ateliersBlock.findIndex(atelier => atelier.header.header_text.replace('The ', '').replace(' & ', ' ').replace(' ', '-').toLowerCase() === menuText.toLowerCase()) : -1;
    const seoTitle = _.isEmpty(menuText) ? (seo ? seo.seoTitle : '') : getNameFromButtonBlock(buttonBlock, `title ${menuText}`);
    const seoKeywords = _.isEmpty(menuText) ? (seo ? seo.seoKeywords : '') : getNameFromButtonBlock(buttonBlock, `keywords ${menuText}`);
    const seoDescription = _.isEmpty(menuText) ? (seo ? seo.seoKeywords : '') : getNameFromButtonBlock(buttonBlock, `description ${menuText}`);
    if (menuText) {
      if (currentMenuText && (menuText !== currentMenuText)) {
        this.setState({
          detailInit: true,
        });
      }

      if (detailInit && (menuIndex !== -1) && ateliersBlock.length) {
        this.hideDetails();
        this.setState({
          detailInit: false,
          currentMenuText: menuText,
        });
        if (menuIndex !== -1) {
          this.setState({
            showingDetail: true,
          });
          this.showDetails(menuIndex);
        }
      }
    } else if (showingDetail) {
      this.setState({
        showingDetail: false,
        currentMenuText: '',
        detailInit: true,
      });
      this.hideDetails();
    }
    console.log('ateliersBlock', ateliersBlock);
    const htmlWeb = (
      <div className="workshop">
        {!visibleDetailMobile ? (
          <React.Fragment>
            <WorkshopBanner animate={animate} imageBlock={imageBlock} headerText={headerBlock ? headerBlock.header_text : ''} textBlock={textBlock} />
            <div className="container-fluid custom">
              <div className="workshop__parent" style={{ minHeight }}>
                <WorkshopList animate={animate} items={ateliersBlock} showDetails={this.gotoWithUrl} />
                {!isMobile && (
                  <div className="workshop__child">
                    <WorkshopDetail animate={animate} item={ateliersBlock[currentWorkshopIndex]} hideDetails={this.gotoBack} />
                  </div>
                )}
              </div>
            </div>
          </React.Fragment>
        ) : (
          isMobile && <WorkshopDetailMobile item={ateliersBlock[currentWorkshopIndex]} animate={animate} hideDetails={this.gotoBack} />
        )}
        {showContactAndFooter && <WorkshopContact paragraphBlock={paragraphBlock} />}
      </div>
    );
    return (
      <React.Fragment>
        <MetaTags>
          <title>
            {seoTitle}
          </title>
          <meta name="description" content={seoDescription} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/perfume-workshop')}
        </MetaTags>
        <HeaderHomePage />
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        {htmlWeb}
        {showContactAndFooter && <FooterV2 />}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  cms: state.cms,
  countries: state.countries,
  // showAskRegion: state.showAskRegion,
});

const mapDispatchToProps = {
  addCmsRedux,
};

Workshop.defaultProps = {
  match: {
    params: {
      workshop: '',
    },
  },
};

Workshop.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      workshop: PropTypes.string,
    }),
  }),
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Workshop));
