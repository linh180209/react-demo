
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Label, Input } from 'reactstrap';
import '../../styles/result-scent.scss';
import { getNameFromButtonBlock, isCheckNull, segmentTrackPersonalityQuizStep, segmentTrackPersonalityQuizStep13Completed, trackGTMSubmitEmail } from '../../Redux/Helpers';
import icBack from '../../image/icon/arrow-grey.svg';
import BackGroundVideo from '../../components/backgroundVideo';
import { isMobileOnly, isMobile } from '../../DetectScreen';
import icEmail from '../../image/icon/ic-email-input.svg';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import { GET_BASKET_URL, GUEST_CREATE_BASKET_URL, PUT_OUTCOME_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import auth from '../../Redux/Helpers/auth';
import loadingPage from '../../Redux/Actions/loading';
import { loadAllBasket } from '../../Redux/Actions/basket';

class EnterEmailQuiz extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      isCheckEmail: true,
    };
    this.refVideo = React.createRef();
  }

  componentDidUpdate(prevProps) {
    const { openVideo } = prevProps;
    if (openVideo !== this.props.openVideo && this.props.openVideo) {
      if (this.refVideo && !this.interval) {
        this.refVideo.current.handlePlayVideoManual();
      }
    }
  }

  assignEmailToCart = () => {
    const { email } = this.state;
    const { basket } = this.props;
    if (!basket.id) {
      const options = {
        url: GUEST_CREATE_BASKET_URL,
        method: 'POST',
        body: {
          subtotal: 0,
          total: 0,
          email,
        },
      };
      fetchClient(options).then((result) => {
        if (result && !result.isError) {
          this.props.loadAllBasket(result);
        }
      });
      return;
    }
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basket.id),
      method: 'PUT',
      body: {
        email,
      },
    };
    fetchClient(option);
  }

  isValidateValue = (value) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(value).toLowerCase());
  }

  onClickSend = () => {
    console.log('onClickSend');
    const { email } = this.state;
    if (!this.isValidateValue(email)) {
      toastrError('Please enter a valid email address.');
      return;
    }
    if (isCheckNull(this.props.idOutCome)) {
      return;
    }
    this.assignEmailToCart();
    this.props.onClickNext();
    const option = {
      url: PUT_OUTCOME_URL.replace('{id}', this.props.idOutCome),
      method: 'PUT',
      body: {
        email,
        send_email: true,
      },
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        auth.setEmail(email);
        if (this.state.isCheckEmail) trackGTMSubmitEmail(email);

        segmentTrackPersonalityQuizStep13Completed(email,
          this.state.isCheckEmail ? true : false);
       
        toastrSuccess(`An email has been sent to ${email}.`);
        return;
      }
      throw new Error(`Failed to send email.`);
    }).catch((err) => {
      toastrError(err.message);
    });
  }

  onChangeCheckboxEmail = (e) => {
    const { checked } = e.target;
    this.setState({ isCheckEmail: checked });
  }

  render() {
    const {
      buttonBlocks, videoBlock, openVideo, namePath,
    } = this.props;
    console.log('buttonBlocks, videoBlock', window.innerHeight);
    const backbt = getNameFromButtonBlock(buttonBlocks, 'Back');
    const almostDontBt = getNameFromButtonBlock(buttonBlocks, 'Almost_done');
    const wellUseBt = getNameFromButtonBlock(buttonBlocks, 'We’ll_use_your_email');
    const continuteBt = getNameFromButtonBlock(buttonBlocks, 'Continue_without_saving');
    const unlockPhoneBoutiqueBt = getNameFromButtonBlock(buttonBlocks, 'UNLOCK_YOUR_FULL_PROFILE_BOUTIQUE');
    const unlockPhoneBt = getNameFromButtonBlock(buttonBlocks, 'UNLOCK_YOUR_FULL_PROFILE');
    const unlockWebBt = getNameFromButtonBlock(buttonBlocks, 'unlock_your_full_profile_and_scent_recommendations');
    const enterEmailBt = getNameFromButtonBlock(buttonBlocks, 'Enter_your_email');
    const sendBt = getNameFromButtonBlock(buttonBlocks, 'Send_Email');
    const iAgreeBt = getNameFromButtonBlock(buttonBlocks, 'I agree to recieve e-mails from Maison 21G');
    return (
      <div
        style={{ height: ['sofitel'].includes(namePath) ? `${window.innerHeight}px` : `${window.innerHeight - 104}px` }}
        className={`save-result-email ${['sofitel'].includes(namePath) ? 'full-screen' : ''} div-col justify-between items-center ${openVideo ? 'open-video' : ''}`}
      >
        <div className="div-content">
          <div className="header div-col items-center tc">
            <h2>
              {almostDontBt}
            </h2>
            <span>
              {wellUseBt}
            </span>
          </div>
          <div className="video">
            <BackGroundVideo
              ref={this.refVideo}
              url={videoBlock && videoBlock.value ? videoBlock.value.video : ''}
              placeholder={videoBlock && videoBlock.value ? videoBlock.value.placeholder : ''}
              styleVideo="contain"
            />
          </div>
          <div className="div-input-email div-col items-center">
            <span className="tc text-title-input">
              {
                namePath === 'boutique-quiz' ? unlockPhoneBoutiqueBt : (isMobile ? unlockPhoneBt : unlockWebBt)
              }
            </span>
            <Label check style={{ cursor: 'pointer' }}>
              <Input
                name="isCheckEmail"
                id="id-email"
                type="checkbox"
                defaultValue
                className="custom-input-filter__checkbox"
                checked={this.state.isCheckEmail}
                onChange={this.onChangeCheckboxEmail}
              />
              {' '}
              <Label
                for="id-email"
                style={{ pointerEvents: 'none' }}
              />
              {iAgreeBt}
            </Label>
            <div className={isMobile ? 'div-input div-col' : 'div-input div-row'}>
              <div className="div-email div-row">
                <img src={icEmail} alt="email" />
                <input
                  type="email"
                  name="email"
                  value={this.state.email}
                  placeholder={enterEmailBt}
                  onChange={e => this.setState({ email: e.target.value })}
                />
              </div>
              <button
                type="button"
                onClick={this.onClickSend}
              >
                {sendBt}
              </button>
            </div>
            <span
              data-gtmtracking="funnel-1-step-11-skip-quizz-result-by-mail"
              className={namePath === 'boutique-quiz' ? 'hidden' : 'tc text-continue'}
              onClick={() => this.props.onClickNext()}
            >
              {continuteBt}
            </span>
            {
            namePath === 'boutique-quiz' && (<div style={isMobile ? { height: '10px', marginBottom: '70px' } : { height: '30px' }} />)
          }
          </div>
        </div>
      </div>
    );
  }
}

EnterEmailQuiz.propTypes = {
  buttonBlocks: PropTypes.arrayOf().isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    basket: state.basket,
    cms: state.cms,
  };
}

const mapDispatchToProps = {
  loadingPage,
  loadAllBasket,
};

export default connect(mapStateToProps, mapDispatchToProps)(EnterEmailQuiz);
