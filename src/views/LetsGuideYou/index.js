/* eslint-disable react/sort-comp */
import classnames from 'classnames';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { isIOS, withOrientationChange } from 'react-device-detect';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import ReactHtmlParser from 'react-html-parser';
import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Col, Row } from 'reactstrap';
import smoothscroll from 'smoothscroll-polyfill';
import AccessAlarmsIcon from '@mui/icons-material/AccessAlarms';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import RangeBar from '../../components/RangeBar';
import ChooseCountry from '../../components/StepQuestion/chooseCountry';
import Favorite from '../../components/StepQuestion/favorite';
import FavoriteDislike from '../../components/StepQuestion/favoriteDislike';
import QuestionGender from '../../components/StepQuestion/questionGender';
import QuestionSkinTone from '../../components/StepQuestion/questionSkinTone';
import QuestionStrength from '../../components/StepQuestion/questionStrength';
import QuestionYesNo from '../../components/StepQuestion/questionYesNo';
import {
  CLEAR_OUTCOME_URL, DISLIKES_OUTCOME_URL, LIKED_OUTCOME_URL, POST_OUTCOMES_URL, PUT_OUTCOME_URL, RESULT_OUTCOME_URL,
} from '../../config';
import { isBrowser, isMobileOnly } from '../../DetectScreen';
import { isMobile } from '../../DetectScreen/detectIFrame';
import icBMW1 from '../../image/icon/ic-bmw-1.svg';
import icBMW from '../../image/icon/ic-bmw.svg';
import icProcess from '../../image/icon/ic-bt-process.svg';
import icWatch from '../../image/icon/ic-watch.svg';
import urlStep1 from '../../image/icon/s-bottle-1.svg';
import urlStep10 from '../../image/icon/s-bottle-10.svg';
import urlStep12 from '../../image/icon/s-bottle-12.svg';
import urlStep2 from '../../image/icon/s-bottle-2.svg';
import urlStep3 from '../../image/icon/s-bottle-3.svg';
import urlStep4 from '../../image/icon/s-bottle-4.svg';
import urlStep5 from '../../image/icon/s-bottle-5.svg';
import urlStep6 from '../../image/icon/s-bottle-6.svg';
import urlStep7 from '../../image/icon/s-bottle-7.svg';
import urlStep8 from '../../image/icon/s-bottle-8.svg';
import urlStep9 from '../../image/icon/s-bottle-9.svg';
import { logoGoldText } from '../../imagev2/svg';
import updateAnswersData from '../../Redux/Actions/answers';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import { loginUpdateOutCome } from '../../Redux/Actions/login';
import updateOutComeComplete from '../../Redux/Actions/outcome';
import { clearAllProducts } from '../../Redux/Actions/products';
import updateQuestionsData from '../../Redux/Actions/questions';
import {
  fetchAnswers, fetchAnswersAlwaysUS, fetchCMSHomepage, fetchQuestions, fetchQuestionsAlwaysUS, generateHreflang, generateUrlWeb, getCmsCommon, getNameFromButtonBlock, getNameFromCommon, getSearchPathName, getSEOFromCms, googleAnalitycs, gotoShopHome, isCheckNull, removeLinkHreflang, scrollTop, segmentTrackPersonalityQuizStep, setPrerenderReady,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import '../../styles/personality.scss';
import '../../styles/style.scss';
import SelectionPerfume from '../SelectionPerfume';
import BlockVideoQuiz from './blockVideoQuiz';
import EnterEmailQuiz from './enterEmailQuiz';
import PopUpScentDNA from './popupScentDNA';
import WrapComponent from './wrapComponent';
import ButtonCT from '../../componentsv2/buttonCT';
import LandingQuiz from '../Quiz/LandingQuiz';


const questionFree = [
  {
    type: 'qtGender',
    id: 'qt1',
    no: 1,
    title: 'What is your gender ?',
    name: 'gender',
    answer1: 'Male',
    answer2: 'Female',
    answer3: 'Unisex',
  },
  {
    type: 'qtAge',
    id: 'qt2',
    no: 2,
    title: 'What is your age ?',
    name: 'age',
    addMarginBottom: false,
  },
  {
    type: 'qtSkin',
    id: 'qt3',
    no: 3,
    title: 'What is your skin-tone ?',
    name: 'skin-tone',
  },
];

function getPathNamePage(path) {
  if (path.includes('/boutique-quiz')) {
    return 'boutique-quiz';
  } if (path.includes('/partnership-quiz')) {
    return 'partnership-quiz';
  } if (path.includes('/krisshop')) {
    return 'krisshop';
  } if (path.includes('/quiz') || path.includes('/quizv2') || path.includes('/quiz-diy') || path.includes('/quizv2-diy')) {
    return 'quiz';
  } if (path.includes('/sephora')) {
    return 'sephora';
  } if (path.includes('/sofitel')) {
    return 'sofitel';
  } if (path.includes('/bmw')) {
    return 'bmw';
  }
  return null;
}

class LetsGuideYou extends Component {
  constructor(props) {
    super(props);
    this.refBegin = React.createRef();
    this.currentElement = questionFree[0];
    this.currentElement.ref = this.refBegin;
    const persionality = _.find(this.props.questions, x => x.type === 'PERSONALITY');
    const urlBackground = persionality ? persionality.video_urls[0] : undefined;
    const { infoGift, isNoStartPage } = this.props.location.state ? this.props.location.state : {};
    this.namePath = getPathNamePage(window.location.pathname);

    // // skip question skin-tone
    if (this.namePath === 'sephora') {
      _.remove(questionFree, x => x.no === 3);
    }

    this.state = {
      listQuestion: [this.currentElement],
      urlBackground,
      description: '',
      isStart: this.namePath !== 'quiz',
      stepQuestion: 0,
      isShowFavorite: false,
      persionaltilyData: {},
      selectLikes: [],
      selectDislikes: [],
      isLogined: false,
      isShowIngredient: false,
      ingredientDetail: {},
      isClickPrev: false,
      listLanguage: [],
      dataPartnerShip: {},
      infoGift,
      isShowVideo: false,
      isShowEnterEmail: false,
      isShowButtonNextDob: false,
    };
    this.yourPerfumeData = undefined;
    this.result = [];
    this.currentElement = undefined;
    this.currentResult = [];
    this.currentItem = [];
    this.gender = '';
    this.changeMood = [];
    this.idOutCome = undefined;
    this.isClickSelect = false;
    this.isPreventCallAPI = false;
    this.oldBody = {};
    this.selectLikesNON_STATE = '';
    this.englishQuestions = [];
    this.englishAnswers = [];
    this.actualAnswers = [];
  }

  generateOutComeUrlForQuiz = () => {
    if (window.location.pathname.includes('/quiz-diy')) {
      return '/quiz-diy/outcome/';
    } if (window.location.pathname.includes('/quizv2-diy')) {
      return '/quizv2-diy/outcome/';
    } if (window.location.pathname.includes('/quizv2')) {
      return '/quizv2/outcome/';
    } if (window.location.pathname.includes('/quiz')) {
      return '/quiz/outcome/';
    }
  }

  componentDidMount() {
    console.log('componentDidMount');
    setPrerenderReady();

    if (this.namePath === 'krisshop') {
      // this.fetchDataPartnerShip();
      this.slug = 'krisshop';
    } else if (this.namePath === 'partnership-quiz') {
      const { match } = this.props;
      const { slug } = match.params;
      this.slug = slug;
    }
    this.fetchData();
    this.props.clearAllProducts();
    smoothscroll.polyfill();
    scrollTop();
    this.disableScrollBody();

    googleAnalitycs(this.namePath === 'quiz' ? '/quiz' : this.namePath === 'boutique-quiz' ? '/boutique-quiz' : this.namePath === 'sephora' ? '/sephora' : this.namePath === 'sofitel' ? '/sofitel' : this.namePath === 'bmw' ? '/bmw' : this.namePath === 'partnership-quiz' ? '/partnership-quiz' : '/krisshop/');

    const { login } = this.props;
    const { infoGift } = this.state;
    if (login && login.user && login.user.outcome && !['boutique-quiz', 'krisshop', 'sephora'].includes(this.namePath)) {
      this.idOutCome = login.user.outcome || undefined;
    }
    const idOutCome = login && login.user && login.user.outcome || auth.getOutComeId();
    if (this.namePath === 'quiz' && idOutCome && !(this.props.location && this.props.location.state && this.props.location.state.isRetake)) {
      const { pathname, search } = getSearchPathName(generateUrlWeb(`${this.generateOutComeUrlForQuiz()}${idOutCome}${!_.isEmpty(infoGift) ? `/?codes=${infoGift.codes}` : ''}`));
      this.props.history.push({
        pathname,
        search,
        state: { stepInstore: 2, infoGift },
      });
    }
    // else if (!this.idOutCome) {
    //   this.initOutCome();
    // }
  }

  componentWillUnmount = () => {
    this.enableScrollBody();
    removeLinkHreflang();
  }

  disableScrollBody = () => {
    const name = 'disable-sroll';
    const element = document.getElementById('body');
    const arr = element.className.split(' ');
    if (arr.indexOf(name) === -1) {
      element.className += ` ${name}`;
      if (!isBrowser && isIOS) {
        document.body.style.top = `-${window.scrollY}px`;
      }
    }
  }

  enableScrollBody = () => {
    const name = 'disable-sroll';
    const element = document.getElementById('body');
    const arr = element.className.split(' ');
    if (arr.indexOf(name) !== -1) {
      element.classList.remove('disable-sroll');
    }
  }

  initOutCome = async () => {
    try {
      const option = {
        url: POST_OUTCOMES_URL,
        method: 'POST',
        body: {
          is_boutique: this.namePath === 'boutique-quiz',
          partnership: ['sofitel', 'sephora', 'bmw'].includes(this.namePath) ? this.namePath : this.slug,
        },
      };
      const result = await fetchClient(option, true);
      this.idOutCome = result.id;
    } catch (error) {
      console.log('error', error);
    }
  }

  componentDidUpdate() {
    const { isLogined, login } = this.state;
    if (isLogined) {
      if (login && login.user && login.user.outcome && !['boutique-quiz', 'krisshop', 'sephora'].includes(this.namePath)) {
        this.idOutCome = login.user.outcome;
      }
      this.setState({ isLogined: false });
    }
    scrollTop();
  }


  onChangeAge = (value, name, no, id, idAnswer) => {
    this.dataAge = {
      value,
      name,
      no,
      id,
      idAnswer,
    };
    if (this.namePath === 'boutique-quiz') {
      const momentValue = moment(value, 'DD/MM/YYYY');
      console.log('momentValue', momentValue);
      if (momentValue && value.length === 10
        && momentValue.isValid()
      && momentValue < moment()
      && momentValue > moment('01/01/1900')) {
        this.setState({ isShowButtonNextDob: true, isShowErrorDob: false });
      } else if (value.length === 10) {
        this.setState({ isShowButtonNextDob: false, isShowErrorDob: true });
      } else {
        this.setState({ isShowButtonNextDob: false, isShowErrorDob: false });
      }
    }
  }

  onChangeOccasion = (value, name, no, id, idAnswer) => {
    this.dataOccasion = {
      value,
      name,
      no,
      id,
      idAnswer,
    };
  }

  onChange = (value, name, no, id, idAnswer) => {
    // if (!this.idOutCome) {
    //   return;
    // }
    if (no >= 8) {
      if (no === 8) {
        this.addResult(value, name, no - 1, id, idAnswer, false);
        // if (this.namePath !== 'quiz') {
        this.onClickNext(no);
        // }
        this.setState({ selectLikes: [] });
        // if (this.namePath === 'quiz') {
        //   return;
        // }
      } if (no === 9) {
        this.setState({ selectDislikes: [] });
      }
      if (no === 8) {
        const occasionQuestion = _.find(this.props.questions, x => x.type === 'OCCASION');
        const cmsCommon = getCmsCommon(this.props.cms);
        const whydoyouwear = getNameFromCommon(cmsCommon, 'Why do you wear perfume');
        const workBt = getNameFromCommon(cmsCommon, 'WORK & SOCIAL');
        const seductiveBt = getNameFromCommon(cmsCommon, 'SEDUCTIVE');
        const specialBt = getNameFromCommon(cmsCommon, 'SPECIAL OCCASION');
        const chillBt = getNameFromCommon(cmsCommon, 'CHILL & RELAX');
        const elegantBt = getNameFromCommon(cmsCommon, 'Elegant & Confident');
        const attractiveBt = getNameFromCommon(cmsCommon, 'Attractive & Sexy');
        const standoutBt = getNameFromCommon(cmsCommon, 'Stand out & Trendy');
        const freshBt = getNameFromCommon(cmsCommon, 'Fresh & Conforting');
        const freshEleBt = getNameFromCommon(cmsCommon, 'FRESH & ELEGANT');
        const seductiveAttBt = getNameFromCommon(cmsCommon, 'SEDUCTIVE & ATTRACTIVE');
        const dayPerfumebt = getNameFromCommon(cmsCommon, 'Day Perfume');
        const nightPerfumebt = getNameFromCommon(cmsCommon, 'Night Perfume');
        const isExist = _.find(this.state.listQuestion, x => x.no === 11);
        if (occasionQuestion && !isExist) {
          // if (['boutique-quiz', 'quiz'].includes(this.namePath)) {
          if (this.namePath !== 'krisshop') {
            this.state.listQuestion.push({
              no: 11,
              type: 'occasion',
              name: 'occasion',
              title: whydoyouwear,
              idAnswer1: '2',
              idAnswer2: '3',
              idAnswer3: '4',
              idAnswer4: '1',
              answer1: workBt,
              answer2: seductiveBt,
              answer3: specialBt,
              answer4: chillBt,
              subAnswer1: elegantBt,
              subAnswer2: attractiveBt,
              subAnswer3: standoutBt,
              subAnswer4: freshBt,
            });
          } else {
            this.state.listQuestion.push({
              no: 11,
              type: 'occasion',
              name: 'occasion',
              title: occasionQuestion.title,
              idAnswer1: 'day',
              idAnswer2: 'night',
              dataMap: [{
                title: freshEleBt,
                value: 'day',
              }, {
                title: seductiveAttBt,
                value: 'night',
              }],
              answer1: freshEleBt,
              answer2: seductiveAttBt,
              subAnswer1: dayPerfumebt,
              subAnswer2: nightPerfumebt,
            });
          }
        }
      } else if (no === 11) {
        // if (['boutique-quiz', 'quiz'].includes(this.namePath)) {
        if (this.namePath !== 'krisshop') {
          this.state.selectDislikes.length = 0;
          const isExist = _.find(this.state.listQuestion, x => x.no === 13);
          const cmsCommon = getCmsCommon(this.props.cms);
          const strongBt = getNameFromCommon(cmsCommon, 'How strong do you like your perfume');
          if (!isExist) {
            this.state.listQuestion.push({
              no: 13,
              type: 'strength',
              name: 'strength',
              title: strongBt,
            });
          }
          this.onChangeOccasion(value, name, no, id, idAnswer);
          this.addResult(value, name, no - 1, id, idAnswer, false);
          this.onClickNext(11);
        } else {
          this.state.selectDislikes.length = 0;
          this.onChangeOccasion(value, name, no, id, idAnswer);
          this.addResult(value, name, no - 1, id, idAnswer, true);
        }
      } else if (no === 12) {
        this.addResult(value, name, no - 1, id, idAnswer, true);
        this.addItemListQuestion(questionFree[1]);
        this.onClickNext(12);
      } else if (no === 13) {
        const favorite = _.find(this.state.listQuestion, x => x.type === 'favorite');
        if (!favorite) {
          this.state.listQuestion.push({
            no: 9,
            type: 'favorite',
          });
        }
        this.addResult(value, name, no - 1, id, idAnswer, false);
        this.onClickNext(13);
      }
      return;
    }

    this.addResult(value, name, no - 1, id, idAnswer);
    if (no === 1 && this.namePath === 'boutique-quiz') {
      const cmsCommon = getCmsCommon(this.props.cms);
      const whereAreBt = getNameFromCommon(cmsCommon, 'Where_are_you_from');
      if (!_.find(this.state.listQuestion, x => x.type === 'country')) {
        this.state.listQuestion.push({
          no: 12,
          type: 'country',
          name: 'country',
          title: whereAreBt,
        });
      }
    } else if (no < questionFree.length) {
      this.addItemListQuestion(questionFree[no]);
    } else {
      const object = this.getQuestion(no);
      if (object) {
        this.addItemListQuestion(object);
      } else {
        toastrError('No the data');
      }
    }
    if (no !== 2) {
      this.onClickNext(no);
    }
  }

  onChangeYourPefume = (data) => {
    this.yourPerfumeData = data;
    const index = 14;
    segmentTrackPersonalityQuizStep(index, [{
      id: index,
      no: index,
      title: 'Which perfume do you like?',
      name: 'perfume-quiz',
    }], [{
      name: 'perfume-quiz',
      id: index,
      value: data?.name,
    }], false, this.props?.cms);
    this.makeBodyPost(this.result, true, true);
  }

  getValueDefault = (question) => {
    if (!question && this.result.length === 0) {
      return '';
    }
    switch (question.type) {
      case 'qtAge':
        return _.find(this.result, x => x.name === 'age') ? _.find(this.result, x => x.name === 'age').value : (this.namePath === 'boutique-quiz' ? undefined : 30);
      case 'qtSkin':
        return _.find(this.result, x => x.name === 'skin-tone') ? _.find(this.result, x => x.name === 'skin-tone').value : '';
      case 'qtGender':
        return _.find(this.result, x => x.name === 'gender') ? _.find(this.result, x => x.name === 'gender').value : '';
      case 'country':
        return _.find(this.result, x => x.name === 'country') ? _.find(this.result, x => x.name === 'country').value : '';
      case 'occasion':
        console.log('find data', _.find(this.result, x => x.name === 'occasion'));
        return _.find(this.result, x => x.name === 'occasion') ? _.find(this.result, x => x.name === 'occasion').idAnswer : '';
      case 'strength':
        return _.find(this.result, x => x.name === 'strength') ? _.find(this.result, x => x.name === 'strength').idAnswer : '';
      default:
        // return this.result[question.no - 1] ? this.result[question.no - 1].idAnswer : '';
        return _.find(this.result, x => x.no === question.no - 1) ? _.find(this.result, x => x.no === question.no - 1).idAnswer : '';
    }
  }

  getImageProcess = (index) => {
    switch (index) {
      case 1:
        return urlStep1;
      case 2:
        return urlStep2;
      case 3:
        return urlStep3;
      case 4:
        return urlStep4;
      case 5:
        return urlStep5;
      case 6:
        return urlStep6;
      case 7:
        return urlStep7;
      case 8:
        return urlStep8;
      case 9:
        return urlStep9;
      case 10:
        return urlStep10;
      case 11:
        return urlStep12;
      case 13:
        return urlStep12;
      case 14:
        return urlStep10;
      case 12:
        return urlStep2;
      default:
        break;
    }
  }
  // eslint-disable-next-line lines-between-class-members
  getTextProcess = (no) => {
    switch (no) {
      case 1:
        return '0G of 21G';
      case 2:
        return '2G of 21G';
      case 3:
        return '4G of 21G';
      case 4:
        return '6G of 21G';
      case 5:
        return '8G of 21G';
      case 6:
        return '10G of 21G';
      case 7:
        return '12G of 21G';
      case 8:
        return '14G of 21G';
      case 9:
        return '18G of 21G';
      case 10:
        return '19G of 21G';
      case 11:
        return '16G of 21G';
      case 13:
        return '17G of 21G';
      case 14:
        return '21G of 21G';
      case 12:
        return '2G of 21G';
      default:
        break;
    }
  }

  updateSelectionLike = (ele, englishResults = []) => {
    console.log('selectedLike:', ele);

    this.isClickSelect = true;
    const { selectLikes } = this.state;
    const isExist = _.find(selectLikes, x => x.id === ele.id);
    if (isExist) {
      this.setState({ selectLikes: [] });
      return;
    }
    this.setState({ selectLikes: [ele] });
    this.selectLikesNON_STATE = englishResults.find(x => x.id === ele.id);
  }

  updateSelectionDislike = (ele, index) => {
    this.isClickSelect = true;
    const { selectDislikes } = this.state;
    const isExist = _.find(selectDislikes, x => x.id === ele.id);
    if (isExist) {
      _.remove(selectDislikes, ele);
      this.setState({ selectDislikes });
      return;
    }
    if (index < 2) {
      selectDislikes.push(ele);
      this.setState({ selectDislikes });
    } else {
      this.setState({ selectDislikes: [ele] });
    }
  }

  setShowFavorite = () => {
    if (this.timeOut) {
      clearTimeout(this.timeOut);
    }
    this.timeOut = setTimeout(() => {
      this.isClickSelect = false;
      this.timeOut = undefined;
    }, 200);
  }

  getImage = (type, d) => (_.find(d ? d.images : [], x => x.type === type) ? _.find(d ? d.images : [], x => x.type === type).image : '')

  onClickNext = (index) => {
    console.log('onClickNext', index);
    if (index !== 2 && index !== 9 && index !== 14) { // corner cases on 'gender' and 'favorite'
      segmentTrackPersonalityQuizStep(index, _.cloneDeep(this.state.listQuestion),
        _.cloneDeep(this.result), false, this.props?.cms,
        _.cloneDeep(this.englishQuestions), _.cloneDeep(this.englishAnswers),
        _.cloneDeep(this.props.questions), _.cloneDeep(this.actualAnswers));
    }

    if (index === 8) {
      setTimeout(() => {
        this.setState({ stepQuestion: 10, isClickPrev: false });
      }, 300);
      return;
    }
    if (index === 11) {
      setTimeout(() => {
        this.setState({ stepQuestion: 12, isClickPrev: false });
      }, 300);
      return;
    }
    if (index === 12) {
      setTimeout(() => {
        this.setState({ stepQuestion: 1, isClickPrev: false });
      }, 300);
      return;
    }
    if (index === 13) {
      setTimeout(() => {
        this.setState({ stepQuestion: 8, isClickPrev: false });
      }, 300);
      return;
    }
    if (index === 9) {
      if (!['boutique-quiz', 'sephora'].includes(this.namePath)) {
        const isExist = _.find(this.state.listQuestion, x => x.no === 14);
        if (!isExist) {
          this.state.listQuestion.push({
            no: 14,
            type: 'perfume-quiz',
            name: 'perfume-quiz',
          });
        }
        setTimeout(() => {
          this.setState({ stepQuestion: 13, isClickPrev: false });
        }, 300);
      } else {
        this.makeBodyPost(this.result, true, true);
      }
      segmentTrackPersonalityQuizStep(index, [{
        id: index,
        no: index,
        title: 'Any favorite perfume family?',
        name: 'favorite',
      }], [{
        name: 'favorite',
        id: index,
        value: this.selectLikesNON_STATE.name,
      }], false, this.props?.cms);
      return;
    }
    if (index === 2) {
      const {
        value, name, no, id, idAnswer,
      } = this.dataAge;
      this.onChange(value, name, no, id, idAnswer);
      segmentTrackPersonalityQuizStep(index,
        _.cloneDeep(this.state.listQuestion),
        _.cloneDeep(this.result), false, this.props?.cms);
    }
    if (index === 10) {
      this.postCompletedTest();
      return;
    }

    // skip question skin-tone
    // if (index >= this.state.listQuestion.length) {
    //   console.log('returning, since index is greater than listQuestion.length');
    //   return;
    // }

    // skip question skin
    if (index === 2 && this.namePath === 'sephora') {
      setTimeout(() => {
        this.setState({ stepQuestion: 3, isClickPrev: false });
      }, 300);
      return;
    }

    setTimeout(() => {
      this.setState({ stepQuestion: index, isClickPrev: false });
    }, 300);
  }

  onClickPrev = (index) => {
    console.log('onClickPrev', index);

    // skip question skin-tone
    if (index === 2 && this.namePath === 'sephora') {
      this.setState({ stepQuestion: 1, isClickPrev: true });
      return;
    }

    if (index === 0 && this.namePath === 'boutique-quiz') {
      this.setState({ stepQuestion: 11, isClickPrev: true });
      return;
    }

    // if (index === 11 && ['boutique-quiz', 'quiz'].includes(this.namePath)) {
    if (index === 11 && this.namePath !== 'krisshop') {
      this.setState({ stepQuestion: 10, isClickPrev: true });
      return;
    }

    if (index === 10 && this.namePath === 'boutique-quiz') {
      this.setState({ stepQuestion: 0, isClickPrev: true });
      return;
    }

    // if (index === 9 && ['boutique-quiz', 'quiz'].includes(this.namePath)) {
    if (index === 9 && this.namePath !== 'krisshop') {
      this.setState({ stepQuestion: 7, isClickPrev: true });
      return;
    }

    // if (index === 7 && ['boutique-quiz', 'quiz'].includes(this.namePath)) {
    if (index === 7 && this.namePath !== 'krisshop') {
      this.setState({ stepQuestion: 12, isClickPrev: true });
      return;
    }

    // if (index === 12 && this.namePath === 'quiz') {
    if (index === 12 && !['krisshop', 'boutique-quiz'].includes(this.namePath)) {
      this.setState({ stepQuestion: 8, isClickPrev: true });
      return;
    }

    if (index === -1) {
      this.setState({ isStart: true, isClickPrev: false });
      return;
    }
    // if ((this.namePath === 'quiz') && index === 8) {
    if ((!['krisshop', 'boutique-quiz'].includes(this.namePath)) && index === 8) {
      this.setState({ stepQuestion: 7, selectDislikes: [], isClickPrev: true });
      return;
    }
    // if ((this.namePath !== 'quiz') && index === 8) {
    if (['krisshop', 'boutique-quiz'].includes(this.namePath) && index === 8) {
      this.setState({ stepQuestion: 10, isClickPrev: true });
      return;
    }
    // if ((this.namePath !== 'quiz') && index === 9) {
    if (['krisshop', 'boutique-quiz'].includes(this.namePath) && index === 9) {
      this.setState({ stepQuestion: 7, isClickPrev: true });
      return;
    }

    this.setState({ stepQuestion: index, isClickPrev: true });
  }

  onClickStart = async () => {
    this.setState({ isStart: false });
  }

  postCompletedTest = () => {
    console.log('postCompletedTest');
    if (['krisshop'].includes(this.namePath)) {
      if (this.isPreventCallAPI) {
        return;
      }
      if (!this.idOutCome) {
        // this.props.history.push(generateUrlWeb('/'));
        gotoShopHome();
        return;
      }
      this.isPreventCallAPI = true;
      const { selectDislikes } = this.state;
      const URL = RESULT_OUTCOME_URL.replace('{id}', this.idOutCome);
      const url1 = selectDislikes.length === 0 ? URL : `${URL}?disliked=${selectDislikes[0].id}${selectDislikes.length > 1 ? `,${selectDislikes[1].id}` : ''}`;
      const option = {
        url: url1,
        method: 'GET',
      };
      this.props.loadingPage(true);
      fetchClient(option).then((result) => {
        this.isPreventCallAPI = false;
        this.props.loadingPage(false);
        if (result && !result.isError) {
          const isHasEmail = this.props.login && this.props.login.user ? this.props.login.user.email : this.props.basket ? this.props.basket.email : undefined;
          if (isHasEmail || ['sephora', 'krisshop'].includes(this.namePath)) {
            this.setState({ isShowVideo: true });
          } else {
            this.setState({ isShowEnterEmail: true });
          }
          return;
        }
        throw new Error();
      }).catch((err) => {
        this.props.loadingPage(false);
        this.isPreventCallAPI = false;
        console.error('err', err.message);
      });
    } else {
      const isHasEmail = this.props.login && this.props.login.user ? this.props.login.user.email : this.props.basket ? this.props.basket.email : undefined;

      // if (isHasEmail || ['sephora', 'krisshop'].includes(this.namePath)) {
      if (isHasEmail || !['quiz', 'partnership-quiz', 'sofitel', 'bmw', 'boutique-quiz'].includes(this.namePath)) {
        if (['boutique-quiz', 'sephora'].includes(this.namePath)) {
          this.endVideo();
        } else {
          this.setState({ isShowVideo: true });
        }
      } else {
        this.setState({ isShowEnterEmail: true });
      }
    }
  }

  static getDerivedStateFromProps = (nextProps, prevState) => {
    const objectReturn = {};
    const { questions, login } = nextProps;
    if (login !== prevState.login) {
      _.assign(objectReturn, { login, isLogined: true });
    }
    if (questions !== prevState.questions) {
      const persionality = _.find(questions, x => x.type === 'PERSONALITY');
      const urlBackground = persionality ? persionality.video_urls[0] : undefined;
      const urlBgImage = persionality ? persionality.image_urls[0] : undefined;
      _.assign(objectReturn, { urlBackground, urlBgImage });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  getQuestion = (nTotal) => {
    const { answers } = this.props;

    // skip question skin-tone
    // const no = noTotal - questionFree.length;
    const no = (this.namePath !== 'sephora' || (this.namePath === 'sephora' && nTotal === 2)) ? nTotal - questionFree.length : nTotal - questionFree.length - 1;
    const noTotal = (this.namePath === 'sephora' && nTotal === 2) ? nTotal + 1 : nTotal;

    let parent;
    if (no === 0) {
      parent = answers;
    } else {
      let temp = answers;
      for (let i = 0; i < no; i += 1) {
        temp = _.find(temp, x => x.title === this.currentResult[i]).children;
      }
      parent = temp;
    }
    if (parent) {
      this.currentItem = parent;
      const object = {
        idAnswer1: parent[0].id,
        idAnswer2: parent[1].id,
        type: 'qtYesNo',
        id: `qt${noTotal + 1}`,
        no: noTotal + 1,
        title: parent[0].question,
        name: `persional${no + 1}`,
        answer1: parent[0].title,
        persionalId1: parent[0].personality ? parent[0].personality.id : '',
        type1: parent[0].title.charAt(0).toUpperCase(),
        answer2: parent[1].title,
        type2: parent[1].title.charAt(0).toUpperCase(),
        persionalId2: parent[1].personality ? parent[1].personality.id : '',
      };
      return object;
    }
    return null;
  }

  fetchData = async () => {
    const {
      cms, answers, questions,
    } = this.props;
    const cmsPersonal = _.find(cms, x => x.title === 'Personality');
    if (!cmsPersonal || answers.length === 0 || questions.length === 0) {
      this.props.loadingPage(true);
      const pending = [fetchAnswers(), fetchQuestions()];
      if (!cmsPersonal) {
        pending.push(fetchCMSHomepage('personality'));
      }
      try {
        const results = await Promise.all(pending);

        this.props.updateAnswersData(results[0]);
        this.props.updateQuestionsData(results[1]);


        if (results.length > 2) {
          this.props.addCmsRedux(results[2]);
        }

        const extraResults = await Promise.all([fetchAnswersAlwaysUS(),
          fetchQuestionsAlwaysUS()]);


        let spreadExtraResults = [...extraResults[0]];
        let currentChildrenArray = extraResults[0]?.[0]?.children;
        while (currentChildrenArray && currentChildrenArray?.length > 0) {
          spreadExtraResults = [...spreadExtraResults, ...currentChildrenArray];
          currentChildrenArray = currentChildrenArray[0].children;
        }

        let spreadResults = [...results[0]];
        let currentChildrenArray_2 = results[0]?.[0]?.children;
        while (currentChildrenArray_2 && currentChildrenArray_2?.length > 0) {
          spreadResults = [...spreadResults, ...currentChildrenArray_2];
          currentChildrenArray_2 = currentChildrenArray_2[0].children;
        }

        this.englishAnswers = spreadExtraResults;
        this.actualAnswers = spreadResults;
        this.englishQuestions = extraResults[1];

        this.props.loadingPage(false);
      } catch (error) {
        toastrError(error.message);
        this.props.loadingPage(false);
      }
    }
  }

  addItemListQuestion = (data) => {
    const ref = React.createRef();
    data.ref = ref;
    this.currentElement = data;
    this.state.listQuestion.push(data);
  }

  skipLikeDislikeQuestion = async () => {
    try {
      if (['krisshop'].includes(this.namePath)) {
        this.props.loadingPage(true);
        const options = {
          url: LIKED_OUTCOME_URL.replace('{id}', this.idOutCome),
          method: 'GET',
        };
        await fetchClient(options);
        const options1 = {
          url: DISLIKES_OUTCOME_URL.replace('{id}', this.idOutCome),
          method: 'GET',
        };
        await fetchClient(options1);
        this.postCompletedTest();
        this.props.loadingPage(false);
      } else {
        this.postCompletedTest();
      }
    } catch (error) {
      toastrError(error.message);
    }
  }

  addResult = (value, name, no, id, idAnswer, isAddNext) => {
    const item = _.find(this.result, x => x.name === name);
    if (item) {
      _.assign(item, {
        value, name, no, id, idAnswer,
      });
    } else {
      this.result.push({
        value, name, no, id, idAnswer,
      });
    }
    if (no < this.state.listQuestion.length - 1 && this.namePath !== 'boutique-quiz') {
      this.state.listQuestion = _.filter(this.state.listQuestion, (x, index) => index <= no);
    } else if (no < this.state.listQuestion.length - 2 && this.namePath === 'boutique-quiz') {
      this.state.listQuestion = _.filter(this.state.listQuestion, (x, index) => index <= no);
    }

    if (no >= questionFree.length) {
      const n = no - questionFree.length;
      if (n < this.currentResult.length) {
        this.currentResult[n] = value;
        this.currentResult = _.filter(this.currentResult, (x, index) => index <= n);
      } else {
        this.currentResult.push(value);
      }
    } else {
      this.currentResult = [];
    }
    this.makeBodyPost(this.result, isAddNext);
  }

  makeBodyPost = (datas, isAddNext = false, isAddLoading = false) => {
    const { selectLikes } = this.state;
    const answers = [];
    console.log('datas', datas);
    const age = _.find(datas, x => x.name === 'age') ? _.find(datas, x => x.name === 'age').value : 30;
    const skinTone = _.find(datas, x => x.name === 'skin-tone');
    const gender = _.find(datas, x => x.name === 'gender');
    const occation = _.find(datas, x => x.name === 'occasion');
    const country = _.find(datas, x => x.name === 'country');
    const strength = _.find(datas, x => x.name === 'strength');
    const family = selectLikes && selectLikes.length > 0 ? selectLikes[0].id : undefined;
    if (this.namePath === 'boutique-quiz') {
      for (let i = 4; i < datas.length; i += 1) {
        if (datas[i].idAnswer && datas[i].no < 8) {
          answers.push(datas[i].idAnswer);
        }
      }
    } else {
      for (let i = questionFree.length; i < datas.length; i += 1) {
        if (datas[i].idAnswer && datas[i].no < 8) {
          answers.push(datas[i].idAnswer);
        }
      }
    }
    this.sendRealTimeData({
      age: this.namePath === 'boutique-quiz' ? undefined : age,
      date_of_birth: this.namePath === 'boutique-quiz' ? parseInt(moment(age, 'MM/DD/YYYY').valueOf() / 1000, 10) : undefined,
      skin_tone: skinTone ? skinTone.value : undefined,
      gender: gender ? (gender.value.toLowerCase() === 'male' ? 1 : gender.value.toLowerCase() === 'female' ? 2 : 3) : undefined,
      country: country ? country.value.code : undefined,
      answers: answers.length > 0 ? answers : undefined,
      external_product: this.yourPerfumeData?.id,
      is_boutique: this.namePath === 'boutique-quiz',
      partnership: ['sofitel', 'sephora', 'bmw'].includes(this.namePath) ? this.namePath : this.slug,
      occasion: (!['boutique-quiz', 'quiz'].includes(this.namePath) && occation) ? occation.idAnswer : undefined,
      mixes: (['boutique-quiz', 'quiz'].includes(this.namePath) && occation) ? [parseInt(occation.idAnswer, 10)] : undefined,
      strength: strength ? strength.idAnswer : undefined,
      family,
    }, isAddNext, isAddLoading);
  }

  sendRealTimeData = async (body, isAddNext, isAddLoading) => {
    if (isCheckNull(this.idOutCome)) {
      await this.initOutCome();
    }
    // console.log('body', body);
    // console.log('body 1', isAddNext);
    // console.log('body 2', this.idOutCome);
    // prevent multile click
    if (!isAddNext && JSON.stringify(this.oldBody) === JSON.stringify(body)) {
      return;
    }
    this.oldBody = _.cloneDeep(body);

    if (!isCheckNull(this.idOutCome)) {
      const options = {
        url: PUT_OUTCOME_URL.replace('{id}', this.idOutCome),
        method: 'PUT',
        body,
      };
      if (isAddLoading) {
        this.props.loadingPage(true);
      }
      fetchClient(options, true).then((result) => {
        if (isAddLoading) {
          this.props.loadingPage(false);
        }
        if (!result || result.isError) {
          return;
        }
        const objectState = {};
        const { personality_choice: persionaltilyData } = result;
        _.assign(objectState, { persionaltilyData });
        if (isAddNext && result && !result.isError) {
          // if (['krisshop', 'quiz', 'boutique-quiz', 'sofitel', 'sephora', 'bmw'].includes(this.namePath)) {
          if (result.answers.length === 5) {
            this.skipLikeDislikeQuestion();
          }
          // }
        }
        this.setState(objectState);
      });
    }
  }

  clearOutCome = (outComeId) => {
    const options = {
      url: CLEAR_OUTCOME_URL.replace('{id}', outComeId),
      method: 'Delete',
    };
    return fetchClient(options);
  }

  handlePostOutComeDone = (id) => {
    console.log('handlePostOutComeDone', id, this.namePath);
    if (this.namePath === 'quiz') {
      const { login, infoGift } = this.state;
      const outCome = {};
      outCome.id = id;
      outCome.changeMood = this.changeMood;
      outCome.personalityChoise = this.persionaltily;
      outCome.productsPersonal = this.productsPersonal;
      this.props.updateOutComeComplete(outCome);
      if (login && login.user && login.user.id) {
        this.props.loginUpdateOutCome(id);
      }
      auth.setOutComeId(id);
      const { pathname, search } = getSearchPathName(generateUrlWeb(`${this.generateOutComeUrlForQuiz()}${id}${!_.isEmpty(infoGift) ? `/?codes=${infoGift.codes}` : ''}`));
      this.props.history.push({
        pathname,
        search,
        // search: window.location.search,
        state: { infoGift },
      });
    } else if (this.namePath === 'boutique-quiz') {
      this.props.history.push(generateUrlWeb(`/boutique-quiz/outcome/${id}`));
    } else if (this.namePath === 'krisshop') {
      this.props.history.push(generateUrlWeb(`/krisshop/outcome/${id}`));
    } else if (this.namePath === 'partnership-quiz') {
      console.log('data route: ', generateUrlWeb(`/partnership-quiz/outcome/${id}/${this.slug}`));
      this.props.history.push(generateUrlWeb(`/partnership-quiz/outcome/${id}/${this.slug}`));
    } else if (this.namePath === 'sephora') {
      this.props.history.push(generateUrlWeb(`/sephora/outcome/${id}`));
    } else if (this.namePath === 'sofitel') {
      this.props.history.push(generateUrlWeb(`/sofitel/outcome/${id}`));
    } else if (this.namePath === 'bmw') {
      this.props.history.push(generateUrlWeb(`/bmw/outcome/${id}`));
    }
  }

  tranlateQuestion = () => {
    const cmsCommon = getCmsCommon(this.props.cms);
    const whatIsYourNameBt = getNameFromCommon(cmsCommon, this.namePath === 'boutique-quiz' ? 'When is your birthday' : 'What_is_your_age');
    // const yearsBt = getNameFromCommon(cmsCommon, 'years');
    const whatIsYourSkinToneBt = getNameFromCommon(cmsCommon, 'What_is_your_skin-tone');
    const whatIsYourGenderBt = getNameFromCommon(cmsCommon, 'What_is_your_gender');
    const maleBt = getNameFromCommon(cmsCommon, 'Male');
    const femaleBt = getNameFromCommon(cmsCommon, 'Female');
    const genderFreeBt = getNameFromCommon(cmsCommon, 'Gender-free');
    questionFree[1].title = whatIsYourNameBt;

    // skip question skin-tone
    if (questionFree.length > 2) {
      questionFree[2].title = whatIsYourSkinToneBt;
    }

    questionFree[0].title = whatIsYourGenderBt;
    questionFree[0].answer1 = maleBt;
    questionFree[0].answer2 = femaleBt;
    questionFree[0].answer3 = genderFreeBt;
  }

  onClickIngredient = (ingredientDetail) => {
    console.log('onClickIngredient', ingredientDetail);
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  updateDataLike = (data) => {
    console.log('updateDataLike');
    this.setState({ dataLike: data });
  }

  updateDataDisLike = (data) => {
    console.log('dataDisLike');
    this.setState({ dataDisLike: data });
  }

  endVideo = () => {
    this.setState({ isShowVideo: false });
    this.handlePostOutComeDone(this.idOutCome);
  }

  render() {
    const {
      cms, answers, questions, isLandscape,
    } = this.props;
    const { dataPartnerShip, dataLike, dataDisLike } = this.state;
    const cmsPage = _.find(cms, x => x.title === 'Personality');
    const buttonBlocks = _.filter(cmsPage ? cmsPage.body : [], x => x.type === 'button_block');
    const videoBlocks = _.filter(cmsPage ? cmsPage.body : [], x => x.type === 'video_block');
    const ctaBlock = _.find(cmsPage ? cmsPage.body : [], x => x.type === 'cta_block');
    if (!cmsPage || _.isEmpty(answers) || _.isEmpty(questions)) {
      return (<div />);
    }
    const cmsCommon = getCmsCommon(cms);
    const nextBt = getNameFromCommon(cmsCommon, 'NEXT');
    const durationBt = getNameFromCommon(cmsCommon, 'Duration');
    const bt23Bt = getNameFromCommon(cmsCommon, '2-3_Minutes');
    const scentDnaBt = getNameFromCommon(cmsCommon, 'SCENT_DNA');
    const noDataBt = getNameFromCommon(cmsCommon, 'NO_DATA');
    const backBt = getNameFromCommon(cmsCommon, 'BACK');
    this.tranlateQuestion();

    const seo = getSEOFromCms(cmsPage);
    const {
      listQuestion, urlBackground,
      description, urlBgImage, isStart, stepQuestion, isShowFavorite,
      persionaltilyData, selectLikes, selectDislikes, isClickPrev, isShowVideo,
      isShowEnterEmail,
    } = this.state;

    const cmsPersonal = _.find(this.props.cms, x => x.title === 'Personality');
    const paragraphBlock = cmsPersonal && cmsPersonal.body ? _.filter(cmsPersonal.body, x => x.type === 'paragraph_block') : undefined;
    const arrayDescription = description.split('\r\n');
    _.remove(arrayDescription, x => x === '');
    const startHtml = (
      <LandingQuiz
        cmsCommon={getCmsCommon(cms)}
        buttonBlocks={buttonBlocks}
        imageBackground={cmsPersonal?.image_background}
        headerText={cmsPersonal?.header_text}
        namePath={this.namePath}
        paragraphBlock={paragraphBlock}
        onClickStart={this.onClickStart}
      />
      // <div
      //   className={`div-start div-col justify-center items-center ${['sofitel', 'krisshop'].includes(this.namePath) ? 'full-screen' : ''}`}
      //   style={{ backgroundImage: `url(${cmsPersonal ? cmsPersonal.image_background : ''})`, backgroundRepeat: 'no-repeat' }}
      // >
      //   <div className="logo-maison">
      //     <img src={logoGoldText} alt="logo" />
      //   </div>
      //   <div className={classnames('content-personality div-col', this.namePath === 'bmw' ? 'style-bmw' : '')} style={isMobile && !isLandscape && isIOS ? { paddingBottom: '100px' } : {}}>
      //     <h1 className="tx-header mt-3 mb-3">
      //       {cmsPersonal && cmsPersonal.header_text ? cmsPersonal.header_text : ''}
      //     </h1>
      //     {
      //         this.namePath === 'bmw' ? (
      //           <div className="div-des">
      //             <div className="image">
      //               <img src={icBMW1} alt="icon" />
      //               <img src={icBMW} alt="icon" />
      //             </div>
      //             <span className="description tc">
      //               {ReactHtmlParser(paragraphBlock && paragraphBlock.length > 0 ? paragraphBlock[0].value : '')}
      //             </span>
      //           </div>
      //         ) : (
      //           <span className="description tc">
      //             {ReactHtmlParser(paragraphBlock && paragraphBlock.length > 0 ? paragraphBlock[0].value : '')}
      //           </span>
      //         )
      //       }
      //     <div className="div-bottom div-col">
      //       <div className="div-time div-col">
      //         <div className="div-row div-watch">
      //           <AccessAlarmsIcon style={{ color: '#EBD8B8', fontSize: '24px', marginRight: '8px' }} />
      //           <span>
      //             {bt23Bt}
      //           </span>
      //         </div>
      //       </div>
      //       <ButtonCT
      //         onClick={this.onClickStart}
      //         name={getNameFromButtonBlock(buttonBlocks, 'take quiz')}
      //         endIcon={<ArrowForwardIcon style={{ color: '#EBD8B8' }} />}
      //       />
      //     </div>
      //   </div>
      // </div>
    );

    const questionView = _.find(listQuestion, x => x.no === stepQuestion + 1) || listQuestion[listQuestion.length - 1];
    const defaultValue = this.getValueDefault(questionView);
    const processPercent = Math.floor((questionView.no === 12 ? 2 : questionView.no) / 11 * 100);
    const widthProgress = {
      width: `${processPercent}%`,
    };
    const gender = _.find(this.result, x => x.name === 'gender');
    const isHasEmail = (this.namePath === 'quiz' || this.namePath === 'boutique-quiz' || ['sofitel', 'sephora', 'bmw'].includes(this.namePath) || this.namePath === 'krisshop') && this.props.login && this.props.login.user ? this.props.login.user.email : this.props.basket ? this.props.basket.email : undefined;

    // const imageBlock = _.find((cmsPage.body || []), x => x.type === 'image_block');
    const questionHtml = (
      <div>
        <div className="animated fadeIn" style={{ overflow: 'hidden' }}>
          {
            !['sephora', 'boutique-quiz'].includes(this.namePath) && (
              <BlockVideoQuiz
                openVideo={isShowVideo}
                endVideo={this.endVideo}
                buttonBlocks={buttonBlocks}
                videoBlocks={videoBlocks}
              />
            )
          }

          {
            !isHasEmail && (
              <EnterEmailQuiz
                onClickNext={() => {
                  if (this.namePath === 'boutique-quiz') {
                    this.endVideo();
                  } else {
                    this.setState({ isShowVideo: true });
                  }
                }}
                openVideo={isShowEnterEmail}
                buttonBlocks={buttonBlocks}
                videoBlock={ctaBlock ? ctaBlock.value.videos[0] : {}}
                idOutCome={this.idOutCome}
                namePath={this.namePath}
              />
            )
          }

          <Row>
            <Col
              md="12"
              xs="12"
              className="height__fix image-backgroud relative"
              style={{ background: '#F5ECDC', overflow: 'hidden', marginTop: ['sofitel', 'krisshop'].includes(this.namePath) ? 0 : -72 }}
            >
              <div className={isShowVideo || ((isMobile && !isIOS) || (isMobile && !isLandscape)) || isMobileOnly ? 'hidden' : 'div-process-bottle'}>
                <div className="div-image-bottle div-col justify-center items-center">
                  <div className="circle" />
                  <img className="active" src={this.getImageProcess(stepQuestion + 1)} alt="bottle" />
                  <span>
                    {this.getTextProcess(stepQuestion + 1)}
                  </span>
                </div>
              </div>
              {
                _.map(listQuestion, (question, index) => {
                  if (index - stepQuestion > 3) {
                    return <div />;
                  }
                  return (
                    <WrapComponent
                      isClickPrev={isClickPrev}
                      namePath={this.namePath}
                      isView={question.no === questionView.no}
                      isHidden={question.no >= questionView.no + 1 || question.no <= questionView.no - 1}
                      url={this.getImageProcess(question.no)}
                      onClickPrev={() => this.onClickPrev(question.no - 2)}
                      onClickNext={() => (question.no === 14 ? this.onChangeYourPefume() : this.onClickNext(question.no))}
                      cmsCommon={cmsCommon}
                      // idDisableBack={this.namePath === 'partnership-quiz'}
                      question={question}
                      name={this.getTextProcess(question.no)}
                      selectLikes={question.no === 9 ? selectLikes : selectDislikes}
                      gender={gender ? gender.value.toLowerCase() : ''}
                      getImage={this.getImage}
                      isHiddenSkipButton={(question.no === 9 && (!dataLike || dataLike.length === 0)) || (question === 10 && (!dataDisLike || dataDisLike.length === 0))}
                      isShowFavorite={question.no === 9 || question.no === 10 || question.no === 14}
                      buttonBlocks={buttonBlocks}
                      isLandscape={isLandscape}
                      className={question.name === 'country' && isMobile ? 'scroll-div' : ''}
                      isShowButtonNextDob={this.namePath !== 'boutique-quiz' || this.state.isShowButtonNextDob}

                    >
                      {
                      (question.type === 'qtSkin'
                        ? (
                          <QuestionSkinTone
                            ref={question.ref}
                            id={question.id}
                            no={question.no}
                            title={question.title}
                            onChange={this.onChange}
                            name={question.name}
                            defaultValue={defaultValue}
                            cmsCommon={cmsCommon}
                            isLandscape={isLandscape}
                          />
                        )
                        : (question.type === 'qtAge' ? (
                          <RangeBar
                            ref={question.ref}
                            id={question.id}
                            no={question.no}
                            title={question.title}
                            onChange={this.onChangeAge}
                            name={question.name}
                            nameBt={nextBt}
                            defaultValue={this.getValueDefault({ type: 'qtAge' })}
                            onClickNext={() => this.onClickNext(question.no)}
                            isShowErrorDob={this.state.isShowErrorDob}
                            isDob={this.namePath === 'boutique-quiz'}
                          />
                        ) : (question.type === 'qtGender' ? (
                          <QuestionGender
                            ref={question.ref}
                            id={question.id}
                            idAnswer1={question.idAnswer1}
                            idAnswer2={question.idAnswer2}
                            idAnswer3={question.idAnswer3}
                            name={question.name}
                            no={question.no}
                            title={question.title}
                            answer1={question.answer1}
                            persionalId1={question.persionalId1}
                            answer2={question.answer2}
                            persionalId2={question.persionalId2}
                            answer3={question.answer3}
                            persionalId3={question.persionalId3}
                            onChange={this.onChange}
                            defaultValue={defaultValue}
                            isLandscape={isLandscape}
                          />
                        ) : (question.type === 'favorite' ? (
                          <Favorite
                            updateSelectionLike={this.updateSelectionLike}
                            selectLikes={selectLikes}
                            cmsCommon={cmsCommon}
                            cmsPersonal={cmsPersonal}
                            gender={gender ? gender.value.toLowerCase() : ''}
                            getImage={this.getImage}
                            onChange={this.onChange}
                            no={question.no}
                            isView={questionView.no === question.no}
                            onClickNext={() => this.onClickNext(question.no)}
                            history={this.props.history}
                            cms={this.props.cms}
                            addCmsRedux={this.props.addCmsRedux}
                            onClickIngredient={this.onClickIngredient}
                            idOutCome={this.idOutCome}
                            slugPartnerShip={this.slug}
                            updateDataLike={this.updateDataLike}
                            buttonBlocks={buttonBlocks}
                            isLandscape={isLandscape}
                            loadingPage={this.props.loadingPage}
                            namePath={this.namePath}
                          />
                        ) : (
                          question.no === 10 ? (
                            <FavoriteDislike
                              updateSelectionDislike={this.updateSelectionDislike}
                              selectLikes={selectLikes}
                              selectDislikes={selectDislikes}
                              cmsCommon={cmsCommon}
                              cmsPersonal={cmsPersonal}
                              getImage={this.getImage}
                              onChange={this.onChange}
                              no={question.no}
                              idOutCome={this.idOutCome}
                              history={this.props.history}
                              onClickNext={() => this.onClickNext(question.no)}
                              onClickIngredient={this.onClickIngredient}
                              slugPartnerShip={this.slug}
                              buttonBlocks={buttonBlocks}
                              isLandscape={isLandscape}
                              namePath={this.namePath}
                              updateDataDisLike={this.updateDataDisLike}
                            />
                          ) : (
                            question.no === 12 ? (
                              <ChooseCountry
                                no={question.no}
                                title={question.title}
                                onChange={this.onChange}
                                name={question.name}
                                buttonBlocks={buttonBlocks}
                                cmsCommon={cmsCommon}
                              />
                            )
                              : (
                                question.type === 'strength' ? (
                                  <QuestionStrength
                                    title={question.title}
                                    name={question.name}
                                    no={question.no}
                                    id={question.id}
                                    onChange={this.onChange}
                                  />
                                ) : question.type === 'perfume-quiz' ? (
                                  <SelectionPerfume
                                    isQuizScreen
                                    onChangeYourPefume={this.onChangeYourPefume}
                                    dataQuizSelect={this.yourPerfumeData}
                                  />
                                ) : (
                                  <QuestionYesNo
                                    ref={question.ref}
                                    id={question.id}
                                    idAnswer1={question.idAnswer1}
                                    idAnswer2={question.idAnswer2}
                                    idAnswer3={question.idAnswer3}
                                    idAnswer4={question.idAnswer4}
                                    name={question.name}
                                    no={question.no}
                                    title={question.title}
                                    answer1={question.answer1}
                                    type1={question.type1}
                                    persionalId1={question.persionalId1}
                                    answer2={question.answer2}
                                    type2={question.type2}
                                    persionalId2={question.persionalId2}
                                    answer3={question.answer3}
                                    answer4={question.answer4}
                                    onChange={this.onChange}
                                    defaultValue={defaultValue}
                                    subAnswer1={question.subAnswer1}
                                    subAnswer2={question.subAnswer2}
                                    subAnswer3={question.subAnswer3}
                                    subAnswer4={question.subAnswer4}
                                    isLandscape={isLandscape}
                                  />
                                ))
                          )
                        )))))
                  }
                    </WrapComponent>
                  );
                })
              }
            </Col>
          </Row>


        </div>
        <PopUpScentDNA
          question={questionView}
          setToggle={() => this.setState({ isShowFavorite: !isShowFavorite })}
          icProcess={icProcess}
          widthProgress={widthProgress}
          scentDnaBt={scentDnaBt}
          selectLikes={questionView.no === 10 ? selectLikes : selectDislikes}
          noDataBt={noDataBt}
          isShowFavorite={isShowFavorite}
          getTextProcess={this.getTextProcess}
          getImage={this.getImage}
          setShowFavorite={this.setShowFavorite}
          cmsCommon={cmsCommon}
          isLandscape={isLandscape}
        />
      </div>
    );
    const formHlmt = (
      <div style={{ overflowY: 'auto' }}>
        {isStart ? startHtml : questionHtml}
      </div>
    );
    const { isShowIngredient, ingredientDetail } = this.state;
    return (
      <div className="container_full div-personality">
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/quiz')}
        </MetaTags>
        {
          !isStart && !['krisshop', 'sofitel'].includes(this.namePath) && (<HeaderHomePageV3 isRemoveMessage />)
        }
        <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
          className="min-top-height"
          isKrisshop={this.namePath === 'krisshop'}
        />
        {/* <BackGroundVideo url={urlBackground} urlImage={urlBgImage} isVideo={false} /> */}
        {
          formHlmt
        }
      </div>
    );
  }
}

LetsGuideYou.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      slug: PropTypes.string,
    }),
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  answers: PropTypes.arrayOf(PropTypes.object).isRequired,
  questions: PropTypes.arrayOf(PropTypes.object).isRequired,
  clearAllProducts: PropTypes.func.isRequired,
  loginUpdateOutCome: PropTypes.func.isRequired,
  login: PropTypes.shape({
    token: PropTypes.string,
  }).isRequired,
  updateOutComeComplete: PropTypes.func.isRequired,
  cms: PropTypes.shape(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  updateAnswersData: PropTypes.func.isRequired,
  updateQuestionsData: PropTypes.func.isRequired,
  isFunnel: PropTypes.bool.isRequired,
  isLandscape: PropTypes.bool.isRequired,
};


function mapStateToProps(state) {
  return {
    answers: state.answers,
    questions: state.questions,
    login: state.login,
    cms: state.cms,
    basket: state.basket,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  clearAllProducts,
  updateOutComeComplete,
  loginUpdateOutCome,
  updateAnswersData,
  updateQuestionsData,
  addCmsRedux,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(withOrientationChange(LetsGuideYou)));
