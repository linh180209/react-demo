import React, { Component } from 'react';
import '../../styles/style.scss';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { MetaTags } from 'react-meta-tags';
import ReactHtmlParser from 'react-html-parser';
import AccessAlarmsIcon from '@mui/icons-material/AccessAlarms';
import classnames from 'classnames';
import { isMobile, isIOS, withOrientationChange } from 'react-device-detect';
import '../../styles/personality.scss';
import { toastrError } from '../../Redux/Helpers/notification';
import {
  fetchCMSHomepage, getSEOFromCms, getCmsCommon, getNameFromCommon, generateHreflang, generateUrlWeb, getNameFromButtonBlock, removeLinkHreflang, setPrerenderReady, segmentTrackPersonalityQuizInitiated, getSearchPathName,
} from '../../Redux/Helpers';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import icWatch from '../../image/icon/ic-watch.svg';
import icBMW from '../../image/icon/ic-bmw.svg';
import icBMW1 from '../../image/icon/ic-bmw-1.svg';
import { isLandscape } from '../../DetectScreen';

class LandingQuiz extends Component {
  componentDidMount() {
    setPrerenderReady();
    this.fetchData();
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    removeLinkHreflang();
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize = () => {
    this.forceUpdate();
  }

  fetchData = async () => {
    const { cms } = this.props;
    const cmsPersonal = _.find(cms, x => x.title === 'Personality');
    const pending = [];
    if (!cmsPersonal) {
      this.props.loadingPage(true);
      pending.push(fetchCMSHomepage('personality'));
    }
    try {
      const results = await Promise.all(pending);
      this.props.addCmsRedux(results[0]);
      this.props.loadingPage(false);
    } catch (error) {
      toastrError(error.message);
      this.props.loadingPage(false);
    }
  }

  onClickStart = async () => {
    segmentTrackPersonalityQuizInitiated();
    // console.log(`pushing:`, window.location.search);
    console.log(this.props.history);
    const { pathname, search } = getSearchPathName(generateUrlWeb('/quiz'));
    this.props.history.push({
      pathname,
      search,
      // search: window.location.search,
      state: {
        isNoStartPage: true,
      },

    });
  }

  render() {
    const { cms } = this.props;
    const cmsPage = _.find(cms, x => x.title === 'Personality');
    if (!cmsPage) {
      return (<div />);
    }
    const cmsPersonal = _.find(this.props.cms, x => x.title === 'Personality');
    const seo = getSEOFromCms(cmsPage);
    const buttonBlocks = _.filter(cmsPage ? cmsPage.body : [], x => x.type === 'button_block');
    const paragraphBlock = cmsPersonal && cmsPersonal.body ? _.filter(cmsPersonal.body, x => x.type === 'paragraph_block') : undefined;
    const cmsCommon = getCmsCommon(cms);
    const bt23Bt = getNameFromCommon(cmsCommon, '2-3_Minutes');
    const heightScreen = window.innerHeight - 72;
    return (
      <div className="container_full div-personality">
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/landingquiz')}
        </MetaTags>
        {/* <HeaderHomePage visible={false} isSpecialMenu isDisableScrollShow isNotShowRegion /> */}
        <HeaderHomePageV3 isRemoveMessage />

        <div
          className="div-start div-col justify-center items-center"
          style={{
            backgroundImage: `url(${cmsPersonal ? cmsPersonal.image_background : ''})`,
            height: `${heightScreen}px`,
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
          }}
        >
          <div className={classnames('content-personality div-col', this.namePath === 'bmw' ? 'style-bmw' : '')} style={isMobile && !isLandscape && isIOS ? { paddingBottom: '100px' } : {}}>
            <h1 className="tx-header mt-3 mb-3">
              {cmsPersonal && cmsPersonal.header_text ? cmsPersonal.header_text : ''}
            </h1>
            {
              this.namePath === 'bmw' ? (
                <div className="div-des">
                  <div className="image">
                    <img src={icBMW} alt="icon" />
                    <img src={icBMW1} alt="icon" />
                  </div>
                  <span className="description tc">
                    {ReactHtmlParser(paragraphBlock && paragraphBlock.length > 0 ? paragraphBlock[0].value : '')}
                  </span>
                </div>
              ) : (
                <span className="description tc">
                  {ReactHtmlParser(paragraphBlock && paragraphBlock.length > 0 ? paragraphBlock[0].value : '')}
                </span>
              )
            }
            <div className="div-bottom div-col">
              <div className="div-time div-col">
                {/* <span className="w-100 tc">
                {durationBt}
              </span> */}
                <div className="div-row div-watch">
                  <AccessAlarmsIcon style={{ color: '#EBD8B8', fontSize: '24px', marginRight: '8px' }} />
                  <span>
                    {bt23Bt}
                  </span>
                </div>
              </div>
              <div className="button-animal">
                <button
                  type="button"
                  data-gtmtracking="funnel-1-step-02-start-quizz"
                  onClick={this.onClickStart}
                >
                  {/* {cmsPersonal.link} */}
                  {getNameFromButtonBlock(buttonBlocks, 'quiz')}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(withOrientationChange(LandingQuiz)));
