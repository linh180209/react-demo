import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classnames from 'classnames';
import {
  isMobile, isiOS, isTablet,
} from '../../DetectScreen/detectIFrame';
import ProgressLine from '../ResultScentV2/progressLine';
import ProgressCircle from '../ResultScentV2/progressCircle';
import icBack from '../../image/icon/ic-back.svg';
import icNext from '../../image/icon/ic-next-quiz.svg';
import icNone from '../../image/icon/ic-none-like.svg';

import { getNameFromCommon, getNameFromButtonBlock } from '../../Redux/Helpers';

class WrapComponent extends Component {
  scentProfile = (data) => {
    const { ingredient, profile, name } = data;
    const strengthBt = 'STRENGTH';
    const durationBt = 'DURATION';
    return (
      <div className="div-scent-profile animated faster fadeInRight">
        <h3>
          {name}
        </h3>
        <span>
          {ingredient && ingredient.title ? ingredient.title : ''}
        </span>
        <div className="div-progress">
          <div className="div-process-line">
            {
              _.map(profile ? profile.accords : [], x => (
                <ProgressLine data={x} isShowPercent />
              ))
            }
          </div>
          <div className="div-process-circle">
            <ProgressCircle title={strengthBt} percent={profile ? parseInt(parseFloat(profile.strength) * 100, 10) : 0} />
            <ProgressCircle title={durationBt} percent={profile ? parseInt(parseFloat(profile.duration) * 100, 10) : 0} />
          </div>
        </div>
      </div>
    );
  };

  render() {
    const {
      idDisableBack, url, children, onClickPrev, onClickNext, cmsCommon, question, selectLikes, getImage, gender, name,
      isView, isClickPrev, isHidden, namePath, isHiddenSkipButton, isShowFavorite, buttonBlocks, isLandscape, className,
      isShowButtonNextDob,
    } = this.props;
    const backBt = getNameFromCommon(cmsCommon, 'BACK');
    const nextBt = getNameFromCommon(cmsCommon, 'NEXT');
    const skipBt = getNameFromCommon(cmsCommon, 'SKIP');
    const scentDnaBt = getNameFromCommon(cmsCommon, 'SCENT_DNA');
    const noDataBt = getNameFromCommon(cmsCommon, 'NO_DATA');
    const ingredientInfoBt = getNameFromButtonBlock(buttonBlocks, 'Ingredient_info');
    const selectBt = getNameFromButtonBlock(buttonBlocks, 'Select_ingridients_to_view_their_characters_here');
    console.log('isClickPrev', isClickPrev, isView);
    return (
      <div
        // style={{ height: ['sofitel'].includes(namePath) ? `${window.innerHeight}px` : `${window.innerHeight - 70}px` }}
        className={`wrap-component-persional ${['krisshop', 'sofitel'].includes(namePath) ? 'full-screen' : ''} div-col justify-center items-center animated faster ${isHidden ? 'hidden' : ''} ${isView ? (isClickPrev ? 'fadeInLeft' : 'fadeInRight') : (isClickPrev ? 'fadeOutRight' : 'fadeOutLeft')}`}
      >
        <div className={((isMobile && !isiOS) || (isMobile && !isLandscape)) ? 'hidden' : 'div-process div-col justify-center items-center'}>
          {
            idDisableBack
              ? (<div className="bt-back" />)
              : (
                <button
                  type="button"
                  className="bt-back"
                  onClick={onClickPrev}
                >
                  <img src={icBack} alt="ic-back" className="mr-2" />
                  {backBt}
                </button>
              )
          }
          {/* <div className="div-image-bottle div-col justify-center items-center">
            <div className="circle" />
            <img className="active" src={url} alt={getAltImage(url)} />
            <span>
              {name}
            </span>
          </div> */}
        </div>
        <div className={classnames('div-children div-col items-center', className)}>
          <div className={(!isMobile || (isTablet && isLandscape)) || idDisableBack ? 'hidden' : 'button-back-mobile'}>
            <button
              type="button"
              className="bt-back"
              onClick={onClickPrev}
            >
              <img src={icBack} alt="ic-back" className="mr-2" />
              {backBt}
            </button>
            <button
              data-gtmtracking={selectLikes && selectLikes.length > 0 ? (question.no === 9 ? 'funnel-1-step-12-favorite-ingredient-next' : 'funnel-1-step-13-favorite-ingredient-next') : (question.no === 9 ? 'funnel-1-step-12-favorite-ingredient-skip' : 'funnel-1-step-13-favorite-ingredient-skip')}
              type="button"
              className={!isShowFavorite || isHiddenSkipButton || (selectLikes && selectLikes.length > 0) ? 'hidden' : selectLikes && selectLikes.length > 0 ? 'bt-next' : 'bt-skip'}
              onClick={onClickNext}
            >
              {selectLikes && selectLikes.length > 0 ? nextBt : skipBt}
              <img
                data-gtmtracking={selectLikes && selectLikes.length > 0 ? (question.no === 9 ? 'funnel-1-step-12-favorite-ingredient-next' : 'funnel-1-step-13-favorite-ingredient-next') : (question.no === 9 ? 'funnel-1-step-12-favorite-ingredient-skip' : 'funnel-1-step-13-favorite-ingredient-skip')}
                src={icNext}
                alt="next"
                className={selectLikes && selectLikes.length > 0 ? '' : 'hidden'}
              />
            </button>
          </div>
          {children}
          <div className={question.no === 2 ? 'div-bt-next' : 'hidden'}>
            {
              isShowButtonNextDob ? (
                <button
                  data-gtmtracking="funnel-1-step-4-age"
                  type="button"
                  onClick={onClickNext}
                >
                  {nextBt}
                </button>
              ) : (
                <div className="div-temp" />
              )
            }

          </div>
        </div>
        {/* <div
          className={question.no > 8 && question.no < 11 && (!isMobile || (isTablet && isLandscape)) ? `div-right div-col items-center${(namePath === 'boutique-quiz' || namePath === 'partnership-quiz') && isTablet ? 'top-full-screen' : ''}` : 'hidden'}
        >
          <span className="text-title">
            {ingredientInfoBt}
          </span>
          {
            !selectLikes || selectLikes.length === 0 ? (
              <div className="div-none-like">
                <img src={icNone} alt="icon" />
                <span>
                  {selectBt}
                </span>
              </div>
            ) : (
              <React.Fragment>
                {selectLikes && selectLikes.length > 0 ? this.scentProfile(selectLikes[0]) : null}
                {selectLikes && selectLikes.length > 1 ? this.scentProfile(selectLikes[1]) : null}
              </React.Fragment>
            )
          }
        </div> */}
      </div>
    );
  }
}
WrapComponent.propTypes = {
  url: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  children: PropTypes.shape().isRequired,
  cmsCommon: PropTypes.shape().isRequired,
  onClickPrev: PropTypes.func.isRequired,
  onClickNext: PropTypes.func.isRequired,
  getImage: PropTypes.func.isRequired,
  idDisableBack: PropTypes.bool.isRequired,
  question: PropTypes.shape().isRequired,
  selectLikes: PropTypes.arrayOf().isRequired,
  isHidden: PropTypes.bool.isRequired,
  namePath: PropTypes.string.isRequired,
  isLandscape: PropTypes.bool.isRequired,
};

export default WrapComponent;
