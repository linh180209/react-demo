import React, { Component } from 'react';
import PropTypes from 'prop-types';
import onClickOutside from 'react-onclickoutside';
import {
  isMobile, isTablet,
} from '../../DetectScreen/detectIFrame';
import { getNameFromCommon } from '../../Redux/Helpers';
import icInfo from '../../image/icon/ic-info-skin.svg';

class PopUpScentDNA extends Component {
  handleClickOutside = () => {
    this.props.setShowFavorite(false);
  }

  render() {
    const {
      question, setToggle, icProcess, isShowFavorite, scentDnaBt, selectLikes, noDataBt,
      widthProgress, getTextProcess, getImage, cmsCommon, isLandscape,
    } = this.props;
    const note = getNameFromCommon(cmsCommon, 'The_level_of_Melanin');
    return (
      <div className={isMobile && !isLandscape ? 'footer' : 'hidden'}>
        <div className="footer__container">
          {/* <button
            className={question.no > 9 && isMobileOnly ? 'bt-more' : 'hidden'}
            type="button"
            onClick={setToggle}
          >
            <img src={icProcess} alt="icProcess" />
          </button> */}
          <div className={(!isMobile || (isTablet && isLandscape)) || question.type !== 'qtSkin' ? 'hidden' : 'div-note div-row justify-center items-center'}>
            <img src={icInfo} alt="icInfo" />
            <span>
              {note}
            </span>
          </div>
          <div className="bg_progressbar">
            <div className="active yellow" style={widthProgress} />
          </div>
          <div className="text tc">
            {getTextProcess(question.no)}
          </div>
          {/* <div className={`${question.no > 9 && isShowFavorite ? 'div-favorite-mobile mt-2 mb-2 show' : 'div-favorite-mobile not-show'} div-col justify-between w-100`}>
            <div className={question.no > 9 && isShowFavorite ? 'div-item div-col justify-center items-center mt-3' : 'hidden'}>
              <div className="div-image div-col justify-center items-center">
                {
                selectLikes && selectLikes.length > 0
                  ? (
                    <img src={getImage('dna', selectLikes[0])} alt="dna" />
                  ) : (
                    <span>
                      {noDataBt}
                    </span>
                  )
              }
              </div>
            </div>
            <div className={question.no > 9 && isShowFavorite ? 'div-item div-col justify-center items-center mt-3' : 'hidden'}>
              <div className="div-image div-col justify-center items-center mt-3">
                {
                selectLikes && selectLikes.length > 1
                  ? (
                    <img src={getImage('dna', selectLikes[1])} alt="dna" />
                  ) : (
                    <span>
                      {noDataBt}
                    </span>
                  )
              }
              </div>
            </div>
          </div> */}
        </div>
      </div>
    );
  }
}

PopUpScentDNA.propTypes = {
  question: PropTypes.shape().isRequired,
  setToggle: PropTypes.func.isRequired,
  icProcess: PropTypes.string.isRequired,
  widthProgress: PropTypes.number.isRequired,
  scentDnaBt: PropTypes.string.isRequired,
  selectLikes: PropTypes.arrayOf().isRequired,
  noDataBt: PropTypes.string.isRequired,
  isShowFavorite: PropTypes.bool.isRequired,
  getTextProcess: PropTypes.func.isRequired,
  getImage: PropTypes.func.isRequired,
  setShowFavorite: PropTypes.func.isRequired,
  isLandscape: PropTypes.bool.isRequired,
};

export default onClickOutside(PopUpScentDNA);
