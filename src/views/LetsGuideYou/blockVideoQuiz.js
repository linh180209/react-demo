import React, { Component } from 'react';
import _ from 'lodash';
import {
  isMobile, isBrowser, isTablet, isiOS,
} from '../../DetectScreen/detectIFrame';
import BackGroundVideo from '../../components/backgroundVideo';
import { getNameFromButtonBlock } from '../../Redux/Helpers';

export default class BlockVideoQuiz extends Component {
  constructor(props) {
    super(props);
    this.listText = [];
    this.state = {
      isShowMix: false,
      indexTextDisplay: 0,
      durationVideo: 8,
    };
    this.refVideo = React.createRef();
    this.getListText(props.buttonBlocks);
  }

  componentDidUpdate(prevProps) {
    const { openVideo } = prevProps;
    if (openVideo !== this.props.openVideo && this.props.openVideo) {
      if (this.refVideo && !this.interval) {
        this.refVideo.current.handlePlayVideoManual();
        // this.interval = setInterval(() => (
        //   this.setState({ indexTextDisplay: this.state.indexTextDisplay + 1 })
        // ), 8000 / 7);
      }
    }
    if (this.listText.length === 0) {
      this.getListText(this.props.buttonBlocks);
    }
  }

  getListText = (buttonBlocks) => {
    if (buttonBlocks && buttonBlocks.length > 0 && this.listText.length === 0) {
      this.listText.push(getNameFromButtonBlock(buttonBlocks, 'CLEAN_SCENTS'));
      this.listText.push(getNameFromButtonBlock(buttonBlocks, 'CRUELTY-FREE'));
      this.listText.push(getNameFromButtonBlock(buttonBlocks, 'EAU_DE_PARFUM_ONLY'));
      this.listText.push(getNameFromButtonBlock(buttonBlocks, 'HIGHT_QUALITY_NATURAL'));
      this.listText.push(getNameFromButtonBlock(buttonBlocks, 'PERSIONALITY-BASED'));
      this.listText.push(getNameFromButtonBlock(buttonBlocks, 'FREE_RETURN'));
      this.listText.push(getNameFromButtonBlock(buttonBlocks, 'ENJOY_YOUR_CREATION'));
    }
  }

  getCurrentTime = (sec, duration) => {
    this.setState({ indexTextDisplay: parseInt(sec, 10), durationVideo: duration - 1 });
    if (sec > 0 && !this.props.openVideo) {
      if (this.refVideo) {
        this.refVideo.current.handlePauseVideoManual();
      }
    }
  }

  componentWillUnmount = () => {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  onEnd = () => {
    console.log('onEnd');
    this.setState({ isShowMix: true });
    setTimeout(() => {
      this.props.endVideo();
    }, 2000);
  }

  render() {
    const {
      isShowMix, indexTextDisplay, durationVideo,
    } = this.state;
    const { openVideo, buttonBlocks, videoBlocks } = this.props;
    const yourMixBt = getNameFromButtonBlock(buttonBlocks, 'YOUR_MIX_IS_READY');
    const skipBt = getNameFromButtonBlock(buttonBlocks, 'skip_to_result');
    const urlVideo = _.find(videoBlocks, x => x.value.type === (isBrowser ? 'web' : isTablet ? 'tablet' : 'phone'));
    const countDown = parseInt(durationVideo - indexTextDisplay, 10);
    return (
      <div className={`block-video-quiz ${openVideo ? 'open-video' : ''}`}>
        {
            isShowMix ? (
              <div className="div-text-mix">
                <span>
                  {yourMixBt}
                </span>
              </div>
            ) : (
              <React.Fragment>
                <div className="div-video">
                  <BackGroundVideo
                    ref={this.refVideo}
                    getCurrentTime={this.getCurrentTime}
                    loop={false}
                    autoPlay={false}
                    onEnd={this.onEnd}
                    url={urlVideo ? urlVideo.value.video : ''}
                    placeholder={urlVideo ? urlVideo.value.placeholder : ''}
                  />
                </div>
                <div className="div-text" style={isMobile && isiOS ? { bottom: '70px' } : {}}>
                  <span className={indexTextDisplay % 2 === 0 ? 'animated faster fadeIn' : 'animated fadeIn'}>
                    {this.listText.length > indexTextDisplay ? this.listText[indexTextDisplay] : ''}
                  </span>
                </div>
                <div className="div-button-next">
                  <button
                    data-gtmtracking="funnel-1-step-14-speed-up-result"
                    type="button"
                    onClick={this.props.endVideo}
                  >
                    {`(${countDown}) ${skipBt}`}
                  </button>
                </div>
              </React.Fragment>
            )
          }
      </div>
    );
  }
}
