import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classnames from 'classnames';
import HeaderItem from './headerItems';
import icPaypalHeader from '../../image/icon/ic-paypal.svg';

import {
  getAltImageV2, getCmsCommon, getNameFromCommon, trackGTMCheckOut,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { isBrowser, isMobile } from '../../DetectScreen';
import ButtonCT from '../../components/ButtonCT';

class PaymentItem extends Component {
  componentDidMount() {
    trackGTMCheckOut(7, this.props.basket);
  }

  render() {
    const {
      stepCheckOut, isStepDone, onClickHeader, noteText, changeNote, payMethod,
      onClickStripe, onClickPaypal, onClickPaypalLate, onClickZaloPay, onClickMomo,
      onClickBankTransfer, onClickAtmCard, onClickIntCard, onClickCod,
      onCheckOutFunc, isOnlyGift, textBlock, shipInfo, imageBlockIcon, iconBlock,
    } = this.props;
    const icCredit = _.filter(imageBlockIcon, x => x.value.caption === 'Credit/Debit');
    const icPaypal = _.filter(imageBlockIcon, x => x.value.caption === 'paypal');
    const icpayLate = _.filter(imageBlockIcon, x => x.value.caption === 'payLate');
    const icZaloPay = _.filter(imageBlockIcon, x => x.value.caption.toLowerCase() === 'zalo_pay');
    const icMomo = _.filter(imageBlockIcon, x => x.value.caption.toLowerCase() === 'momo');
    const icBankTransfer = _.filter(imageBlockIcon, x => x.value.caption.toLowerCase() === 'bank_transfer');
    const icAtmCard = _.filter(imageBlockIcon, x => x.value.caption.toLowerCase() === 'atm_card');
    const icIntCard = _.filter(imageBlockIcon, x => x.value.caption.toLowerCase() === 'int_card');
    const icCod = _.filter(imageBlockIcon, x => x.value.caption.toLowerCase() === 'cod');

    const cmsCommon = getCmsCommon(this.props.cms);
    const addNoteBt = getNameFromCommon(cmsCommon, 'ADD_A_NOTE');
    const payBt = getNameFromCommon(cmsCommon, 'PAYMENT_METHOD');
    const creditBt = getNameFromCommon(cmsCommon, 'Credit/Debit_card');
    const placeBt = getNameFromCommon(cmsCommon, 'PLACE_ORDER');
    const paymentBt = getNameFromCommon(cmsCommon, 'PAYMENT');
    const payLateBt = getNameFromCommon(cmsCommon, 'Pay_Later');

    const zaloPayBt = getNameFromCommon(cmsCommon, 'ZALO_PAY');
    const momoBt = getNameFromCommon(cmsCommon, 'MOMO');
    const bankTransferBt = getNameFromCommon(cmsCommon, 'BANK_TRANSFER');
    const atmCardBt = getNameFromCommon(cmsCommon, 'ATM_CARD');
    const intCardBt = getNameFromCommon(cmsCommon, 'INT_CARD');
    const codBt = getNameFromCommon(cmsCommon, 'COD');

    const zaloPayNoteBt = getNameFromCommon(cmsCommon, 'ZALO_PAY_NOTE');
    const momoNoteBt = getNameFromCommon(cmsCommon, 'MOMO_NOTE');
    const bankTransferNoteBt = getNameFromCommon(cmsCommon, 'BANK_TRANSFER_NOTE');
    const atmCardNoteBt = getNameFromCommon(cmsCommon, 'ATM_CARD_NOTE');
    const intCardNoteBt = getNameFromCommon(cmsCommon, 'INT_CARD_NOTE');
    const codNoteBt = getNameFromCommon(cmsCommon, 'COD_NOTE');

    const continueBt = getNameFromCommon(cmsCommon, 'continueBt');
    const isSingapore = shipInfo?.country && shipInfo?.country.toLowerCase() === 'sg';
    const isVietNam = shipInfo?.country && shipInfo?.country.toLowerCase() === 'vn';

    const htmlCenter = (
      <div className="h-100 div-col justify-center ml-5">
        <span />
      </div>
    );
    const paymentMain = (
      <div className="div-col div-grid">
        <div className={`div-option-checkout top div-row ${payMethod === 'stripe' ? 'active' : ''}`} onClick={onClickStripe}>
          <div className="div-button div-col justify-center items-center" style={{ width: '38px', margin: 'auto' }}>
            <div className={classnames('div-col justify-center items-center', payMethod === 'stripe' ? 'selected' : '')}>
              <button
                type="button"
                className={payMethod === 'stripe' ? 'bt-choose' : ''}
              />
            </div>
          </div>
          <div className="div-name div-col justify-center name-payment">
            <span style={{ marginLeft: '5px' }}>
              {creditBt}
            </span>
          </div>
          <div className="div-card div-row justify-end items-center" style={{ flex: '1' }}>
            {
              _.map(icCredit, x => (
                <img loading="lazy" src={x.value.image} alt="icMaster" className="mr-3" />
              ))
            }
          </div>
        </div>

        <div className="div-option-checkout-wrap" onClick={onClickPaypal}>
          <div className={`div-option-checkout div-row ${payMethod === 'paypal' ? 'active' : ''}`}>
            <div className="div-button div-col justify-center items-center" style={{ width: '38px', margin: 'auto' }}>
              <div className={classnames('div-col justify-center items-center', payMethod === 'paypal' ? 'selected' : '')}>
                <button
                  type="button"
                  // className={payMethod === 'paypal' ? 'bt-choose' : ''}
                />
              </div>
            </div>
            <div className="div-name div-col justify-center name-payment">
              <img src={icPaypalHeader} alt="icPaypal" style={{ width: '80px' }} />
            </div>
            <div className="div-card div-row justify-end items-center" style={{ flex: '1' }}>
              {
                _.map(icPaypal, x => (
                  <img loading="lazy" src={x.value.image} alt="icMaster" className="mr-3" />
                ))
              }
            </div>
          </div>
          <div className={payMethod === 'paypal' ? 'div-description-text' : 'hidden'}>

            <div>
              {textBlock && textBlock.length > 0 ? textBlock[0].value : ''}
            </div>
          </div>
        </div>

        <div className="div-option-checkout-wrap" onClick={onClickPaypalLate}>
          <div className={`div-option-checkout div-row ${payMethod === 'stripe_pay_later' ? 'active' : 'bottom'}`}>
            <div className="div-button div-col justify-center items-center" style={{ width: '38px', margin: 'auto' }}>
              <div className={classnames('div-col justify-center items-center', payMethod === 'stripe_pay_later' ? 'selected' : '')}>
                <button
                  type="button"
                  className={payMethod === 'stripe_pay_later' ? 'bt-choose' : ''}
                />
              </div>
            </div>
            <div className="div-name div-col justify-center name-payment">
              <span>
                {payLateBt}
              </span>
            </div>
            <div className="div-card div-row justify-end items-center" style={{ flex: '1' }}>
              {
                _.map(icpayLate, x => (
                  <img loading="lazy" src={x.value.image} alt="icMaster" className="mr-3" />
                ))
              }
            </div>
          </div>
          <div className={payMethod === 'stripe_pay_later' ? 'div-description-text bottom-border' : 'hidden'}>
            <div className="div-list-icon">
              {
                _.map(iconBlock ? iconBlock.icons : [], (x, index) => (
                  <div className="div-item-icon">
                    <img loading="lazy" src={x.value.image.image} alt={getAltImageV2(x.value.image)} />
                    <span>{`${index + 1}. ${x.value.text}`}</span>
                  </div>
                ))
              }
            </div>
            <div className="text-1">
              {textBlock && textBlock.length > 1 ? textBlock[1].value : ''}
            </div>
            <div className="text-2">
              {textBlock && textBlock.length > 2 ? textBlock[2].value : ''}
            </div>
          </div>
        </div>
      </div>
    );
    const paymentVN = (
      <div className={`list-payment-method vnpays  ${!isVietNam ? 'hidden' : ''}`}>
        {zaloPayBt && (
          <div className="div-option-checkout-wrap zalopay" onClick={onClickZaloPay}>
            <div className={`div-option-checkout div-row ${payMethod === 'zalopay' ? 'active' : ''}`}>
              <div className="div-button div-col justify-center items-center" style={{ width: '38px', margin: 'auto' }}>
                <div className={classnames('div-col justify-center items-center', payMethod === 'zalopay' ? 'selected' : '')}>
                  <button
                    type="button"
                    className={payMethod === 'zalopay' ? 'bt-choose' : ''}
                  />
                </div>
              </div>
              <div className="div-name div-col justify-center name-payment">
                <b>{zaloPayBt}</b>
                {zaloPayNoteBt && (<span className="note note-zalopay">{zaloPayNoteBt}</span>)}
              </div>
              <div className="div-card div-row justify-end items-center" style={{ width: '80px' }}>
                {
                  _.map(icZaloPay, x => (
                    <img loading="lazy" src={x.value.image} alt="icZaloPay" className="mr-3" />
                  ))
                }
              </div>
            </div>
          </div>
        )}

        {momoBt && (
          <div className="div-option-checkout-wrap momo" onClick={onClickMomo}>
            <div className={`div-option-checkout div-row ${payMethod === 'momo' ? 'active' : ''}`}>
              <div className="div-button div-col justify-center items-center" style={{ width: '38px', margin: 'auto' }}>
                <div className={classnames('div-col justify-center items-center', payMethod === 'momo' ? 'selected' : '')}>
                  <button
                    type="button"
                    className={payMethod === 'momo' ? 'bt-choose' : ''}
                  />
                </div>
              </div>
              <div className="div-name div-col justify-center name-payment">
                <b>{momoBt}</b>
                {momoNoteBt && (<span className="note note-momo">{momoNoteBt}</span>)}
              </div>
              <div className="div-card div-row justify-end items-center" style={{ width: '80px' }}>
                {
                  _.map(icMomo, x => (
                    <img loading="lazy" src={x.value.image} alt="icMomo" className="mr-3" />
                  ))
                }
              </div>
            </div>
          </div>
        )}

        {bankTransferBt && (
          <div className="div-option-checkout-wrap bank_transfer" onClick={onClickBankTransfer}>
            <div className={`div-option-checkout div-row ${payMethod === 'bank_transfer' ? 'active' : ''}`}>
              <div className="div-button div-col justify-center items-center" style={{ width: '38px', margin: 'auto' }}>
                <div className={classnames('div-col justify-center items-center', payMethod === 'bank_transfer' ? 'selected' : '')}>
                  <button
                    type="button"
                    className={payMethod === 'bank_transfer' ? 'bt-choose' : ''}
                  />
                </div>
              </div>
              <div className="div-name div-col justify-center name-payment">
                <b>{bankTransferBt || 'Bank Transfer'}</b>
                {bankTransferNoteBt && (<span className="note note-bank-transfer">{bankTransferNoteBt}</span>)}
              </div>
              <div className="div-card div-row justify-end items-center" style={{ width: '80px' }}>
                {
                  _.map(icBankTransfer, x => (
                    <img loading="lazy" src={x.value.image} alt="icBankTransfer" className="mr-3" />
                  ))
                }
              </div>
            </div>
          </div>
        )}

        {atmCardBt && (
          <div className="div-option-checkout-wrap atm_card" onClick={onClickAtmCard}>
            <div className={`div-option-checkout div-row ${payMethod === 'atm_card' ? 'active' : ''}`}>
              <div className="div-button div-col justify-center items-center" style={{ width: '38px', margin: 'auto' }}>
                <div className={classnames('div-col justify-center items-center', payMethod === 'atm_card' ? 'selected' : '')}>
                  <button
                    type="button"
                    className={payMethod === 'atm_card' ? 'bt-choose' : ''}
                  />
                </div>
              </div>
              <div className="div-name div-col justify-center name-payment">
                <b>{atmCardBt || 'ATM Card/Internet banking'}</b>
                {atmCardNoteBt && (<span className="note note-atm-card">{atmCardNoteBt}</span>)}
              </div>
              <div className="div-card div-row justify-end items-center" style={{ width: '80px' }}>
                {
                  _.map(icAtmCard, x => (
                    <img loading="lazy" src={x.value.image} alt="icAtmCard" className="mr-3" />
                  ))
                }
              </div>
            </div>
          </div>
        )}

        {intCardBt && (
          <div className="div-option-checkout-wrap int_card" onClick={onClickIntCard}>
            <div className={`div-option-checkout div-row ${payMethod === 'int_card' ? 'active' : ''}`}>
              <div className="div-button div-col justify-center items-center" style={{ width: '38px', margin: 'auto' }}>
                <div className={classnames('div-col justify-center items-center', payMethod === 'int_card' ? 'selected' : '')}>
                  <button
                    type="button"
                    className={payMethod === 'int_card' ? 'bt-choose' : ''}
                  />
                </div>
              </div>
              <div className="div-name div-col justify-center name-payment">
                <b>{intCardBt || 'Visa/Master/JCB'}</b>
                {intCardNoteBt && (<span className="note note-int-card">{intCardNoteBt}</span>)}
              </div>
              <div className="div-card div-row justify-end items-center" style={{ width: '80px' }}>
                {
                  _.map(icIntCard, x => (
                    <img loading="lazy" src={x.value.image} alt="icIntCard" className="mr-3" />
                  ))
                }
              </div>
            </div>
          </div>
        )}

        {codBt && (
          <div className="div-option-checkout-wrap cod" onClick={onClickCod}>
            <div className={`div-option-checkout div-row ${payMethod === 'cod' ? 'active' : ''}`}>
              <div className="div-button div-col justify-center items-center" style={{ width: '38px', margin: 'auto' }}>
                <div className={classnames('div-col justify-center items-center', payMethod === 'cod' ? 'selected' : '')}>
                  <button
                    type="button"
                    className={payMethod === 'cod' ? 'bt-choose' : ''}
                  />
                </div>
              </div>
              <div className="div-name div-col justify-center name-payment">
                <b>{codBt || 'Cash On Delivery'}</b>
                {codNoteBt && (<span className="note note-cod">{codNoteBt}</span>)}
              </div>
              <div className="div-card div-row justify-end items-center" style={{ width: '80px' }}>
                {
                  _.map(icCod, x => (
                    <img loading="lazy" src={x.value.image} alt="icCod" className="mr-3" />
                  ))
                }
              </div>
            </div>
          </div>
        )}
      </div>
    );
    return (
      <React.Fragment>
        <div id="anchor-step4" />
        <HeaderItem
          cmsCommon={cmsCommon}
          stepCheckOut={stepCheckOut}
          isStepDone={isStepDone}
          step={this.props.step}
          title={paymentBt}
          onClick={onClickHeader}
          htmlCenter={htmlCenter}
        />
        {
          this.props.stepCheckOut === this.props.step && !this.props.isStepDone && (
            <div>
              <React.Fragment>
                <div className="div-col div-header-mini">
                  <span>
                    {payBt}
                  </span>
                </div>
                {
                  isVietNam ? paymentVN : paymentMain
                }
              </React.Fragment>
              {
                isBrowser && (
                  <div className="div-row justify-end">
                    <ButtonCT
                      name={placeBt}
                      className="bt-next bt-place-order"
                      onClick={this.props.onCheckOutFunc}
                    />
                  </div>
                )
              }
            </div>
          )
        }

      </React.Fragment>
    );
  }
}

PaymentItem.propTypes = {
  stepCheckOut: PropTypes.number.isRequired,
  isStepDone: PropTypes.bool.isRequired,
  onClickHeader: PropTypes.func.isRequired,
  changeNote: PropTypes.func.isRequired,
  onCheckOutFunc: PropTypes.func.isRequired,
  noteText: PropTypes.string.isRequired,
  payMethod: PropTypes.string.isRequired,
  onClickStripe: PropTypes.func.isRequired,
  onClickPaypal: PropTypes.func.isRequired,
  onClickPaypalLate: PropTypes.func.isRequired,
  onClickZaloPay: PropTypes.func.isRequired,
  onClickMomo: PropTypes.func.isRequired,
  onClickBankTransfer: PropTypes.func.isRequired,
  onClickAtmCard: PropTypes.func.isRequired,
  onClickIntCard: PropTypes.func.isRequired,
  onClickCod: PropTypes.func.isRequired,
  cms: PropTypes.shape().isRequired,
  isOnlyGift: PropTypes.bool.isRequired,
  textBlock: PropTypes.arrayOf().isRequired,
  shipInfo: PropTypes.shape.isRequired,
  imageBlockIcon: PropTypes.arrayOf().isRequired,
};
export default PaymentItem;
