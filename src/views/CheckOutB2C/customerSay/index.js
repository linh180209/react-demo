import _ from 'lodash';
import moment from 'moment';
import React, { useCallback } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { isMobile } from '../../../DetectScreen';
import icStarSelected from '../../../image/icon/star-selected.svg';
import icStar from '../../../image/icon/star.svg';
import { getAltImageV2 } from '../../../Redux/Helpers';
import './styles.scss';

function CustomerSay(props) {
  const itemSay = useCallback(d => (
    <div className="item-say div-col">
      <div className="line-star div-row">
        {
          _.map(_.range(5), (x, index) => (
            <img src={index + 1 <= parseInt(d.value?.instagram, 10) ? icStarSelected : icStar} alt="star" />
          ))
        }
      </div>
      <div className="des">
        {d.value?.text}
      </div>
      <div className="account-say div-row">
        <img src={d.value?.avatar?.image} alt={getAltImageV2(d?.value?.avatar)} />
        <div className="name-time div-col">
          <h4>
            {d.value?.name}
          </h4>
          <span>
            {moment(d?.value?.date).format('DD MMM YYYY')}
          </span>
        </div>
      </div>
    </div>
  ), [props.data]);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: isMobile ? 1 : props.isQuizPage ? 3 : 2,
    slidesToScroll: isMobile ? 1 : props.isQuizPage ? 3 : 2,
    arrows: true,
    autoplaySpeed: 5000,
    pauseOnHover: false,
  };
  return (
    <div className="customer-say div-col">
      <div className="line-header div-row">
        <div className="line" />
        <h3>{props.data?.value?.header?.header_text}</h3>
        <div className="line" />
      </div>
      <div className="content-slider">
        <Slider {...settings}>
          {
          _.map(props.data?.value?.feedbacks, d => (
            itemSay(d)
          ))
        }
        </Slider>
      </div>
    </div>
  );
}

export default CustomerSay;
