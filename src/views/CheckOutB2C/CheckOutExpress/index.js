import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import React from 'react';
import { isIOS, isSafari } from 'react-device-detect';
import { KEY_STRIPE_APPLE, POST_CHECK_OUT_EXPRESS } from '../../../config';
import icPaypal from '../../../image/icon/ic-paypal-express.svg';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import auth from '../../../Redux/Helpers/auth';
import CheckoutForm from './checkoutForm';
import './styles.scss';


const stripePromise = loadStripe(KEY_STRIPE_APPLE);

function CheckOutExpress(props) {
  const onClickPaypal = () => {
    const option = {
      url: POST_CHECK_OUT_EXPRESS.replace('{id}', props.basket?.id),
      method: 'POST',
      body: {
        payment_method: 'paypal',
        paypal_express: true,
        promos: props.promotion?.id ? [props.promotion?.id] : undefined,
      },
    };
    fetchClient(option).then((result) => {
      if (result.url) {
        window.location.href = result.url;
      }
    }).catch((error) => {
      toastrError(error.message);
    });
  };

  const expressBt = getNameFromButtonBlock(props.buttonBlock, 'Express Checkout');
  const orBt = getNameFromButtonBlock(props.buttonBlock, 'OR');
  return (
    <div className="div-checkout-express">
      <div className="content">
        <div className="title-checkout">
          {expressBt}
        </div>
        <button
          className="bt-express"
          data-gtmtracking="checkout-paypal-express"
          type="button"
          onClick={onClickPaypal}
        >
          <img src={icPaypal} alt="paypal" />
        </button>
        {
          ['sg'].includes(auth.getCountry()) && isSafari && props.basket?.total && parseFloat(props.basket?.total, 10) > 0 && (
            <Elements stripe={stripePromise}>
              <CheckoutForm
                basket={props.basket}
                createAddressFromApplePay={props.createAddressFromApplePay}
                listShipping={props.listShipping}
                preCheckOutApplePay={props.preCheckOutApplePay}
              />
            </Elements>
          )
        }
      </div>
      <div className="line-or">
        <div>
          {orBt}
        </div>
      </div>
    </div>
  );
}

export default CheckOutExpress;
