import React, { useState, useEffect, useRef } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import _ from 'lodash';
import { PaymentRequestButtonElement, useStripe } from '@stripe/react-stripe-js';
import { CHECKOUT_FUNCTION_URL, GET_BASKET_URL } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import { generateUrlWeb, trackGTMCheckOut } from '../../../Redux/Helpers';
import { clearBasket } from '../../../Redux/Actions/basket';
import loadingPage from '../../../Redux/Actions/loading';
import { ZERO_CURRENCIES } from '../../../constants';
import { isMobile } from '../../../DetectScreen';

const CheckoutForm = (props) => {
  const stripe = useStripe();
  const history = useHistory();
  const dispatch = useDispatch();
  const login = useSelector(state => state.login);
  const [paymentRequest, setPaymentRequest] = useState(null);
  const isPaymentRequest = useRef(false);
  const funcUpdateShipping = useRef();

  const gotoSuccessPage = (order) => {
    history.push(generateUrlWeb(`/pay/success/${order}`));
    dispatch(clearBasket());
  };

  const getTotal = (total, country) => {
    if (ZERO_CURRENCIES.includes(country)) {
      return parseInt(parseFloat(total), 10);
    }
    return parseInt(parseFloat(total) * 100, 10);
  };

  const assignBasketToUser = (basketId, userId) => {
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basketId),
      method: 'PUT',
      body: {
        user: userId,
      },
    };
    return fetchClient(option);
  };
  useEffect(() => {
    if (stripe && props.basket?.id && props.listShipping?.length > 0) {
      const shippingSelection = _.find(props.listShipping, x => (props.basket?.meta?.country?.toUpperCase() === x.country.code.toUpperCase()));
      const amount = props.basket?.total ? getTotal(props.basket?.total, props.basket?.meta?.country) : undefined;
      const isFreeShipping = amount && shippingSelection ? amount > parseFloat(shippingSelection?.threshold) : false;
      if (!paymentRequest) {
        const pr = stripe.paymentRequest({
          country: props.basket?.meta?.country.toUpperCase(),
          currency: props.basket?.meta?.currency,
          total: {
            label: 'Total',
            amount,
          },
          requestPayerName: true,
          requestPayerEmail: true,
          requestPayerPhone: true,
          requestShipping: true,
          shippingOptions: [
            {
              id: 'free-shipping',
              label: 'Time shipping',
              detail: 'Arrives in 3 to 8 days',
              amount: isFreeShipping ? 0 : (getTotal(shippingSelection?.shipping, props.basket?.meta?.country) || 0),
            },
          ],
        });

        // Check the availability of the Payment Request API.
        pr.canMakePayment().then((result) => {
          if (result && !isPaymentRequest.current) {
            setPaymentRequest(pr);
            isPaymentRequest.current = true;
            if (props.updateIsShowAppleExpress) {
              props.updateIsShowAppleExpress(true);
            }
          }
        });
      } else if (!funcUpdateShipping.current) {
        paymentRequest.update({
          total: {
            label: 'Total',
            amount,
          },
        });
      }
    }
  }, [stripe, props.basket, props.listShipping, paymentRequest]);

  const postAddress = (idAddress, idAddressBilling, email, name) => {
    // tracking GMT
    trackGTMCheckOut(8, props.basket);
    const isLogined = !login?.user?.id;

    // assign user to cart, if basket id !== login cart
    if (isLogined && props.basket.id !== login?.id) {
      assignBasketToUser(props.basket.id, login.id);
    }

    const options = {
      url: CHECKOUT_FUNCTION_URL.replace('{id}', props.basket?.id),
      method: 'POST',
      body: {
        shipping_address: idAddress,
        billing_address: idAddressBilling,
        payment_method: 'apple_pay',
        meta: {},
      },
    };
    if (!login?.user?.id) {
      options.body.meta = {
        guest: {
          email,
          name,
        },
      };
    }
    return fetchClient(options, true);
  };

  useEffect(() => {
    if (paymentRequest) {
      paymentRequest.on('paymentmethod', async (ev) => {
        console.log('paymentmethod shippingaddresschange', ev);
        const {
          shippingAddress, payerEmail, payerName, paymentMethod,
        } = ev;
        try {
          dispatch(loadingPage(true));
          const body = {
            street1: shippingAddress?.addressLine?.length > 0 ? shippingAddress?.addressLine[0] : undefined,
            city: shippingAddress?.city,
            country: shippingAddress?.country,
            state: shippingAddress?.region,
            postal_code: shippingAddress?.postalCode,
            phone: shippingAddress?.phone,
            name: payerName,
            email: payerEmail,
          };
          const bodyShipping = {
            street1: paymentMethod?.billing_details?.address?.line1,
            street2: paymentMethod?.billing_details?.address?.line2,
            city: paymentMethod?.billing_details?.address?.city,
            country: paymentMethod?.billing_details?.address?.country,
            state: paymentMethod?.billing_details?.address?.state,
            postal_code: paymentMethod?.billing_details?.address?.postal_code,
            phone: paymentMethod?.billing_details?.phone,
            name: paymentMethod?.billing_details?.name,
            email: paymentMethod?.billing_details?.email,
          };
          const address = await props.createAddressFromApplePay(body);
          const addressBilling = await props.createAddressFromApplePay(bodyShipping);
          if (!address?.id || !addressBilling?.id) {
            ev.complete('invalid_shipping_address');
          }

          const resultCheckOut = await postAddress(address?.id, addressBilling?.id, payerEmail, payerName);

          // Confirm the PaymentIntent without handling potential next actions (yet).
          const { paymentIntent, error: confirmError } = await stripe.confirmCardPayment(
            resultCheckOut?.secret,
            { payment_method: ev.paymentMethod.id },
            { handleActions: false },
          );

          if (confirmError) {
            // Report to the browser that the payment failed, prompting it to
            // re-show the payment interface, or show an error message and close
            // the payment interface.
            ev.complete('fail');
          } else {
            // Report to the browser that the confirmation was successful, prompting
            // it to close the browser payment method collection interface.
            ev.complete('success');
            // Check if the PaymentIntent requires any actions and if so let Stripe.js
            // handle the flow. If using an API version older than "2019-02-11"
            // instead check for: `paymentIntent.status === "requires_source_action"`.
            if (paymentIntent.status === 'requires_action') {
              // Let Stripe.js handle the rest of the payment flow.
              const { error } = await stripe.confirmCardPayment(props.clientSecret);
              if (error) {
                // toastrError(error.message);
                // The payment failed -- ask your customer for a new payment method.
              } else {
                // The payment has succeeded.
                gotoSuccessPage(resultCheckOut?.order_id);
              }
            } else {
              // The payment has succeeded.
              gotoSuccessPage(resultCheckOut?.order_id);
            }
          }
        } catch (error) {
          // toastrError(error.message);
          ev.complete('fail');
        }
        dispatch(loadingPage(false));
      });

      paymentRequest.on('shippingaddresschange', (ev) => {
        console.log('CheckoutForm shippingaddresschange', ev.shippingAddress);
        const shippingSelection = _.find(props.listShipping, x => (ev.shippingAddress?.country?.toUpperCase() === x.country.code.toUpperCase()));
        if (shippingSelection) {
          props.preCheckOutApplePay(shippingSelection?.country.code);
          funcUpdateShipping.current = ev;
          // ev.updateWith({
          //   status: 'success',
          // });
        } else {
          ev.updateWith({ status: 'invalid_shipping_address' });
        }
      });

      paymentRequest.on('cancel', () => {
        funcUpdateShipping.current = undefined;
      });
    }
  }, [paymentRequest]);
  useEffect(() => {
    if (funcUpdateShipping.current) {
      const amount = props.basket?.total ? getTotal(props.basket?.total, props.basket?.meta?.country) : undefined;
      funcUpdateShipping.current.updateWith({
        status: 'success',
        total: {
          label: 'Total',
          amount,
        },
      });
    }
  }, [props.basket]);

  if (paymentRequest) {
    return (
      <div className="bt-apple-pay">
        <PaymentRequestButtonElement options={{
          paymentRequest,
          style: {
            paymentRequestButton: {
              type: 'default',
              // One of 'default', 'book', 'buy', or 'donate'
              // Defaults to 'default'

              theme: 'dark',
              // One of 'dark', 'light', or 'light-outline'
              // Defaults to 'dark'

              height: !props.isCheckOutV2 ? '31px' : isMobile ? '48px' : '64px',
              // Defaults to '40px'. The width is always '100%'.
              width: '100%',
              borderRadius: !props.isCheckOutV2 ? '3px' : isMobile ? '10px' : '20px',
            },
          },
        }}
        />
      </div>
    );
  }

  // Use a traditional checkout form.
  return null;
};

export default CheckoutForm;
