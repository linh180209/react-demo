import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Row, Col, Label, Input,
} from 'reactstrap';
import _ from 'lodash';
import classnames from 'classnames';
import Select from 'react-select';
import InputMask from 'react-input-mask';
import PhoneInput from 'react-phone-number-input';
import HeaderItem from './headerItems';
import emitter from '../../Redux/Helpers/eventEmitter';
import {
  getNameFromCommon, getCmsCommon, getNameFromButtonBlock, trackGTMCheckOut,
} from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';
import RadioButton from '../../components/Input/radioButton';
import ButtonCT from '../../components/ButtonCT';
import icNext from '../../image/icon/ic-next.svg';
import icDown from '../../image/icon/ic-drop-grey.svg';
import CheckboxInput from '../../components/CheckboxInput';
import CheckOutExpress from './CheckOutExpress';
import 'react-phone-number-input/style.css';
import auth from '../../Redux/Helpers/auth';


class CustomerItemV2 extends Component {
  componentDidMount() {
    trackGTMCheckOut(2, this.props.basket);
  }

  generateSelectInputComponents = () => {
    const inputComponents = {
      DropdownIndicator: selectProps => (
        <img style={{ marginBottom: '0px', marginRight: '10px' }} src={icDown} alt="Arrow down icon" />
      ),
    };
    return inputComponents;
  };

  render() {
    const {
      stepCheckOut, isStepDone, shipInfo, onChange, buttonBlock, login, listError, isCheckOutClub, isOnlyGift, isHasFreeGift,
    } = this.props;
    const isLogined = login && login.user && login.user.id;
    const cmsCommon = getCmsCommon(this.props.cms);
    const alreadyBt = getNameFromCommon(cmsCommon, 'Already_have_an_account');
    const loginBt = getNameFromCommon(cmsCommon, 'Log_in');
    const customerBt = getNameFromCommon(cmsCommon, 'CUSTOMER');
    const firstNameBt = getNameFromCommon(cmsCommon, 'FIRST_NAME');
    const lastNameBt = getNameFromCommon(cmsCommon, 'LAST_NAME');
    const emailBt = getNameFromCommon(cmsCommon, 'Email');
    const thisInfoBt = getNameFromButtonBlock(buttonBlock, 'This_information');
    const continuteShippingBt = getNameFromButtonBlock(buttonBlock, 'CONTINUE TO SHIPPING');
    const continuteGiftFreeBt = getNameFromButtonBlock(buttonBlock, 'CONTINUE TO FREE GIFT');
    const continutePaymentBt = getNameFromButtonBlock(buttonBlock, 'CONTINUE TO PAYMENT');
    const keepBt = getNameFromButtonBlock(buttonBlock, 'Keep me up to date on news and offers');
    const birthdateBt = getNameFromCommon(cmsCommon, 'Birth_Date');
    const genderBt = getNameFromCommon(cmsCommon, 'Gender');
    const maleBt = getNameFromCommon(cmsCommon, 'MALE');
    const femaleBt = getNameFromCommon(cmsCommon, 'FEMALE');
    const weLoveBt = getNameFromCommon(cmsCommon, 'We’d love to surprise you on your Birthday');
    const optionalBt = getNameFromCommon(cmsCommon, 'Optional');
    const mrBt = getNameFromCommon(cmsCommon, 'mr');
    const mrsBt = getNameFromCommon(cmsCommon, 'mrs');
    const continueBt = getNameFromCommon(cmsCommon, 'CONTINUE');
    const phoneBt = getNameFromCommon(cmsCommon, 'PHONE');
    console.log('shipInfo', shipInfo);
    const htmlBottom = (
      <div className="h-100 div-col justify-center ml-5">
        <span className="tx-more-infor">
          {shipInfo.email}
        </span>
      </div>
    );
    const logAccount = (
      <div className={`${isMobile ? 'div-col login-mobile' : 'div-row justify-end login-web'}`}>
        <div>
          <span style={{ fontSize: '14px', color: '#545454', fontWeight: '400' }}>
            {alreadyBt}
          </span>
          <button
            type="button"
            className="button-bg__none"
            style={{ color: '#0B5EC2', textDecoration: 'underline', fontSize: '14px' }}
            onClick={() => emitter.emit('eventOpenLogin')}
          >
            {loginBt}
          </button>
        </div>
        <span className={isCheckOutClub ? '' : 'hidden'}>
          <i>{`* ${thisInfoBt}`}</i>
        </span>
      </div>
    );
    const listGender = [
      {
        label: mrBt,
        value: 'Male',
      },
      {
        label: mrsBt,
        value: 'Female',
      },
    ];
    console.log('shipInfo.is_get_newsletters', shipInfo.is_get_newsletters);
    return (
      <React.Fragment>
        <div id="anchor-step1" />
        {
          !['kr', 'id', 'my', 'sa'].includes(auth.getCountry()) && (
            <CheckOutExpress
              promotion={this.props.promotion}
              basket={this.props.basket}
              buttonBlock={buttonBlock}
              createAddressFromApplePay={this.props.createAddressFromApplePay}
              listShipping={this.props.listShipping}
              preCheckOutApplePay={this.props.preCheckOutApplePay}
            />
          )
        }

        <div className="header-title">
          <h3>{customerBt}</h3>
          {!isLogined ? logAccount : <div />}
        </div>
        {/* <HeaderItem
          cmsCommon={cmsCommon}
          stepCheckOut={stepCheckOut}
          isStepDone={isStepDone}
          step={1}
          title={customerBt}
          onClick={this.props.onClickHeader}
          htmlCenter={!isLogined && !isStepDone ? logAccount : undefined}
          htmlBottom={isStepDone ? htmlBottom : undefined}
        /> */}

        <div className="body-step">
          <React.Fragment>
            <div className="div-content div-col">
              <Row className={isTablet ? { margin: '0px' } : {}}>
                {/* <Col md={isTablet ? '12' : '4'} xs="12" style={isMobile ? { padding: '0px', marginTop: '0px' } : { paddingRight: '7px', marginTop: '0px' }}>
                  <Select
                    value={_.find(listGender, x => x.value === shipInfo.gender)}
                    onChange={this.props.onChangeGenderSelect}
                    options={listGender}
                    className={classnames('input-text input-select')}
                    components={this.generateSelectInputComponents()}
                  />
                </Col> */}
                <Col md={isTablet ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px' } : { paddingRight: '7px', marginTop: '0px' }}>
                  <input
                    type="text"
                    className={`border-checkout w-100 ${listError.includes('firstName') ? 'error' : ''}`}
                    placeholder={`${firstNameBt}*`}
                    name="firstName"
                    value={shipInfo.firstName}
                    onChange={onChange}
                  />
                </Col>
                <Col md={isTablet ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px', marginTop: '25px' } : { paddingLeft: '7px', marginTop: '0px' }}>
                  <input
                    type="text"
                    className={`border-checkout w-100 ${listError.includes('lastName') ? 'error' : ''}`}
                    placeholder={`${lastNameBt}*`}
                    name="lastName"
                    value={shipInfo.lastName}
                    onChange={onChange}
                  />
                </Col>
              </Row>
              <Row className={isTablet ? { margin: '0px' } : {}}>
                <Col md="12" style={isMobile ? { padding: '0px' } : { paddingRight: '15px' }}>
                  <input
                    type="email"
                    className={`border-checkout w-100 ${listError.includes('email') ? 'error' : ''}`}
                    placeholder={`${emailBt}*`}
                    name="email"
                    value={shipInfo.email}
                    onChange={onChange}
                  />
                </Col>
              </Row>
              <CheckboxInput
                className="checkbox-offers"
                id="checkbox-offers"
                name="is_get_newsletters"
                data={{
                  label: keepBt,
                  value: shipInfo.is_get_newsletters,
                }}
                checked={shipInfo.is_get_newsletters}
                onChange={() => onChange({ target: { name: 'is_get_newsletters', value: !shipInfo.is_get_newsletters } })}
              />
              {/* <Label className="checkbox-offers">
                <Input
                  name="create_address"
                  id="idCheckbox"
                  type="checkbox"
                  checked
                  className="custom-input-filter__checkbox"
                  // onChange={onClickCreateAddress}
                  // checked={shipInfo.create_address}
                />
                {' '}
                <Label
                  for="idCheckbox"
                  style={{ pointerEvents: 'none' }}
                />
                Keep me up to date on news and offers
              </Label> */}
              <Row className={isTablet ? { margin: '0px' } : {}}>
                <Col md={isTablet ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px' } : { paddingRight: '7px' }}>
                  <InputMask
                    className={`border-checkout w-100 mb-2 ${listError.includes('dob') ? 'error' : ''}`}
                    type="text"
                    name="dob"
                    placeholder={`${birthdateBt} (MM/DD/YYYY)`}
                    mask="99/99/9999"
                    maskChar={null}
                    defaultValue={shipInfo.dob}
                    value={shipInfo.dob}
                    onChange={onChange}
                  />
                  <span className="des-birth">{weLoveBt}</span>
                  <span className={isCheckOutClub ? 'hidden' : 'option-birth'}>{`(${optionalBt})`}</span>

                </Col>
                <Col className="relative" md={isTablet ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px' } : { paddingLeft: '7px' }}>

                  <PhoneInput
                    international
                    defaultCountry="SG"
                    countryCallingCodeEditable={false}
                    countrySelectProps={{ unicodeFlags: true }}
                    className={`border-checkout w-100 ${listError.includes('phone') ? 'error' : ''}`}
                    placeholder={`${phoneBt}*`}
                    value={shipInfo.phone}
                    name="phone"
                    onChange={value => onChange({ target: { name: 'phone', value } })}
                  />
                </Col>
              </Row>
            </div>
          </React.Fragment>
          <div className="div-row justify-end">
            <ButtonCT
              name={!isOnlyGift ? continuteShippingBt : (isHasFreeGift ? continuteGiftFreeBt : continutePaymentBt)}
              className="bt-next"
              // iconRight={icNext}
              onClick={this.props.onClickContinute}
            />
          </div>
        </div>

      </React.Fragment>
    );
  }
}

CustomerItemV2.propTypes = {
  stepCheckOut: PropTypes.number.isRequired,
  isStepDone: PropTypes.bool.isRequired,
  onClickHeader: PropTypes.func.isRequired,
  shipInfo: PropTypes.shape().isRequired,
  login: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  onClickContinute: PropTypes.func.isRequired,
  cms: PropTypes.shape().isRequired,
  listError: PropTypes.arrayOf().isRequired,
  isCheckOutClub: PropTypes.bool.isRequired,
};

export default CustomerItemV2;
