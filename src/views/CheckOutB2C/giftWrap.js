import React, { useState, useEffect } from 'react';
import { Input, Label } from 'reactstrap';
import { GET_PRODUCT_TYPE_LINK } from '../../config';
import { generaCurrency, getNameFromButtonBlock } from '../../Redux/Helpers';
import fetchClient from '../../Redux/Helpers/fetch-client';

function GiftWrap(props) {
  const [giftWrap, setGiftWrap] = useState();
  const [isGift, setIsGift] = useState(false);

  const fetchGiftWrap = () => {
    const options = {
      url: GET_PRODUCT_TYPE_LINK.replace('{type}', 'gift_wrap'),
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && result.length > 0) {
        setGiftWrap(result[0]);
      }
    }).catch((error) => {
      console.log('error', error);
    });
  };

  const onChange = (e) => {
    const { checked } = e.target;
    setIsGift(checked);
    props.selectGiftWrap(checked);
  };

  useEffect(() => {
    fetchGiftWrap();
  }, []);

  const giftWrapBt = getNameFromButtonBlock(props.buttonBlock, 'Gift_Wrap');
  const addABt = getNameFromButtonBlock(props.buttonBlock, 'Add_a_M21G_Gift_wrapping_to_your_purchase');
  return (
    <div className="gift-wrap">
      <Label check style={{ cursor: 'pointer' }}>
        <Input
          id="idCheckbox"
          type="checkbox"
          className="custom-input-filter__checkbox"
          style={{ height: '42px' }}
          onChange={onChange}
          value={isGift}
        />
        {' '}
        <Label
          for="idCheckbox"
          style={{ pointerEvents: 'none' }}
        />
      </Label>
      <div className="text">
        <h3>
          {giftWrapBt}
        </h3>
        <span>
          {addABt}
        </span>
      </div>
      <div className="price">
        {giftWrap ? generaCurrency(giftWrap.price) : ''}
      </div>
    </div>
  );
}

export default GiftWrap;
