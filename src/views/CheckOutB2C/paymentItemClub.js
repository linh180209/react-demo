import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Input } from 'reactstrap';
import _ from 'lodash';
import classnames from 'classnames';
import InputMask from 'react-input-mask';
import HeaderItem from './headerItems';

import icCard from '../../image/icon/icCard.svg';
import { getNameFromCommon, getNameFromButtonBlock } from '../../Redux/Helpers';
import { isBrowser } from '../../DetectScreen';
import ButtonCT from '../../components/ButtonCT';

class PaymentItemClub extends Component {
  render() {
    const {
      cmsCommon, imageBlockIcon, listError, cardInfo, onChange,
      stepCheckOut, isStepDone, isOnlyGift, imageTxtBlock, buttonBlock,
    } = this.props;
    const payBt = getNameFromCommon(cmsCommon, 'PAYMENT_METHOD');
    const paymentBt = getNameFromCommon(cmsCommon, 'PAYMENT');
    const creditBt = getNameFromCommon(cmsCommon, 'Credit/Debit_card');
    const placeBt = getNameFromCommon(cmsCommon, 'PLACE_ORDER');
    const willBt = getNameFromButtonBlock(buttonBlock, 'what_you_will_receive');
    const cardNameBt = getNameFromButtonBlock(buttonBlock, 'Cardholder_name');
    const icCredit = _.filter(imageBlockIcon, x => x.value.caption === 'Credit/Debit');
    return (
      <React.Fragment>
        <HeaderItem
          cmsCommon={cmsCommon}
          stepCheckOut={stepCheckOut}
          isStepDone={isStepDone}
          step={this.props.step}
          title={paymentBt}
          onClick={() => {}}
        />
        {
          this.props.stepCheckOut === this.props.step && !this.props.isStepDone && (
            <div className="div-payment-club">
              <div className="div-col div-header-mini">
                <span>
                  {payBt}
                </span>
              </div>
              <div className="div-col div-grid">
                <div className="div-option-checkout top div-row active">
                  <div className="div-button div-col justify-center items-center" style={{ width: '38px', margin: 'auto' }}>
                    <div className={classnames('div-col justify-center items-center selected')}>
                      <button
                        type="button"
                        className="bt-choose"
                      />
                    </div>
                  </div>
                  <div className="div-name div-col justify-center name-payment">
                    <span style={{ marginLeft: '5px' }}>
                      {creditBt}
                    </span>
                  </div>
                  <div className="div-card div-row justify-end items-center" style={{ flex: '1' }}>
                    {
                      _.map(icCredit, x => (
                        <img loading="lazy" src={x.value.image} alt="icMaster" className="mr-3" />
                      ))
                    }
                  </div>
                </div>
              </div>
              {
                  isBrowser && (
                    <div className="div-row justify-end">
                      <ButtonCT
                        name={placeBt}
                        className="bt-next bt-place-order"
                        onClick={this.props.onCheckOutFunc}
                      />
                    </div>
                  )
                }
            </div>
          )
        }
      </React.Fragment>
    );
  }
}

PaymentItemClub.propTypes = {
  cmsCommon: PropTypes.shape().isRequired,
  cardInfo: PropTypes.shape().isRequired,
  imageBlockIcon: PropTypes.arrayOf().isRequired,
  imageTxtBlock: PropTypes.arrayOf().isRequired,
  listError: PropTypes.arrayOf().isRequired,
  onChange: PropTypes.func.isRequired,
};
export default PaymentItemClub;
