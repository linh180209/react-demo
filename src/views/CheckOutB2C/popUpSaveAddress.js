import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';

import '../../styles/popup_address.scss';
import { Row, Col, Input } from 'reactstrap';
import icClose from '../../image/icon/close.svg';
import { getNameFromButtonBlock } from '../../Redux/Helpers';
import { GET_SHIPPING_URL, CREATE_ADDRESS_URL, UPDATE_ADDRESS_GUEST_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import { isMobile } from '../../DetectScreen';

class ItemAdress extends Component {
  render() {
    const {
      data, currentAddress, onChangeAddress, closeShowAddress, buttonBlock,
    } = this.props;
    const deliverToAddressBt = getNameFromButtonBlock(buttonBlock, 'deliver_to_this_address');
    const {
      name, street1, phone, postal_code, city, state, country,
    } = data;
    return (
      <div className="div-item-address">
        <div className="div-info-text">
          {/* <div className={isMobile ? 'div-button-choose' : 'hidden'}> */}
          <div className="hidden">
            <button type="button" />
          </div>
          <div className="div-info">
            <h4>
              {name}
            </h4>
            <span>
              {`${street1 ? ` ${street1}` : ''}${city ? ` ${city}` : ''}${state ? ` ${state}` : ''}${country ? ` ${country.name}` : ''}${postal_code ? ` ${postal_code}` : ''}`}
            </span>
            <span>
              {phone}
            </span>
          </div>

        </div>
        <button
          className={currentAddress && currentAddress.id === data.id ? 'choosed' : ''}
          type="button"
          onClick={() => { onChangeAddress(data); closeShowAddress(); }}
        >
          {deliverToAddressBt}
        </button>
      </div>
    );
  }
}

class PopUpSaveAdsress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSlide: 1,
      isAddNewAddress: false,
      isEditAddress: false,
      shipInfo: props.defaultShipInfo || { country: 'sg' },
      listError: [],
    };
    this.refSilder = React.createRef();
  }

  componentDidMount() {
    this.fetchShippingInfo();
  }

  fetchShippingInfo = () => {
    const options = {
      url: GET_SHIPPING_URL,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      _.forEach(result, (x) => {
        x.name = x.country.name;
      });
      const listShipping = _.orderBy(result, 'name', 'asc');
      this.setState({ listShipping });
    }).catch((err) => {
      console.log('error', err);
    });
  }

  addressSlider = d => (
    <div className="div-address-slider">
      <div className="div-line-address">
        {
          _.map(d.slice(0, 3), x => (
            <ItemAdress
              data={x}
              currentAddress={this.props.currentAddress}
              onChangeAddress={this.props.onChangeAddress}
              closeShowAddress={this.props.closeShowAddress}
              buttonBlock={this.props.buttonBlock}
            />
          ))
        }
      </div>
      <div className="div-line-address mt-5">
        {
          _.map(d.slice(3, 6), x => (
            <ItemAdress
              data={x}
              currentAddress={this.props.currentAddress}
              onChangeAddress={this.props.onChangeAddress}
              closeShowAddress={this.props.closeShowAddress}
              buttonBlock={this.props.buttonBlock}
            />
          ))
        }
      </div>
    </div>
  );

  onChange = (e) => {
    const { shipInfo } = this.state;
    const { name, value } = e.target;
    _.assign(shipInfo, { [name]: value });
    this.setState({ shipInfo, listError: [] });
  }

  checkValidAddress = () => {
    const fieldError = [];
    const listError = [];
    const { shipInfo } = this.state;
    if (!shipInfo.street1) {
      fieldError.push('address');
      listError.push('street1');
    }
    if (!shipInfo.postal_code) {
      fieldError.push('zip code');
      listError.push('postal_code');
    }
    if (!shipInfo.city) {
      fieldError.push('city');
      listError.push('city');
    }
    if (!shipInfo.phone) {
      fieldError.push('phone');
      listError.push('phone');
    }
    if (fieldError.length > 0) {
      toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
    }
    return listError;
  }

  onClickEditAddress = () => {
    const { shipInfo } = this.state;
    const option = {
      url: UPDATE_ADDRESS_GUEST_URL.replace('{id}', shipInfo.id),
      method: 'PUT',
      body: shipInfo,
    };
    fetchClient(option, true).then((result) => {
      this.props.onChangeAddress(result);
      this.props.closeShowAddress();
    }).catch((error) => {
      toastrError(error);
    });
  }

  onSaveAddress = () => {
    const { shipInfo } = this.state;
    const { login } = this.props;
    const body = _.cloneDeep(shipInfo);
    if (login && login.user && login.user.id) {
      _.assign(body, {
        user: login.user.id,
        email: login.user.email,
        name: `${login.user.first_name} ${login.user.last_name}`,
      });
    }
    const listError = this.checkValidAddress();
    if (listError.length > 0) {
      this.setState({ listError });
    }
    const option = {
      url: CREATE_ADDRESS_URL,
      method: 'POST',
      body,
    };
    fetchClient(option, true).then((result) => {
      this.props.onChangeAddress(result);
      this.props.closeShowAddress();
    }).catch((err) => {
      toastrError(err.message);
    });
  }

  editAddress = () => {
    const addressClone = _.cloneDeep(this.props.currentAddress);
    _.assign(addressClone, { country: addressClone.country.code });
    this.setState({ shipInfo: addressClone, isEditAddress: true });
  }

  render() {
    const {
      listAddress, currentAddress, onChangeAddress, closeShowAddress, buttonBlock, isShowNewAddress,
    } = this.props;
    const chooseAShippingBt = getNameFromButtonBlock(buttonBlock, 'choose_a_shipping_address');
    const addressBt = getNameFromButtonBlock(buttonBlock, 'address');
    const phoneBt = getNameFromButtonBlock(buttonBlock, 'phone');
    const countryBt = getNameFromButtonBlock(buttonBlock, 'country');
    const cityBt = getNameFromButtonBlock(buttonBlock, 'city');
    const zipCodeBt = getNameFromButtonBlock(buttonBlock, 'zipcode');
    const stateBt = getNameFromButtonBlock(buttonBlock, 'state');
    const addNewAddressBt = getNameFromButtonBlock(buttonBlock, 'ADD_NEW_ADDRESS');
    const saveNewAddressBt = getNameFromButtonBlock(buttonBlock, 'save_new_address');
    const saveAddressBt = getNameFromButtonBlock(buttonBlock, 'save_address');
    const editOrBt = getNameFromButtonBlock(buttonBlock, 'Edit_or_delete_addresses');
    const {
      currentSlide, listError, shipInfo, isAddNewAddress, listShipping, isEditAddress,
    } = this.state;
    const newListAddress = [];
    for (let i = 0; i < listAddress.length; i += 5) {
      newListAddress.push(listAddress.slice(i, i + 6));
    }
    const settings = {
      dots: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnHover: false,
      afterChange: (slide) => {
        this.setState({ currentSlide: slide + 1 });
      },
    };

    const htmlNewAddress = (
      <div className="content-new-address div-col">
        <input
          type="text"
          className={`border-checkout mt-3 ${listError.includes('street1') ? 'error' : ''}`}
          placeholder={`${addressBt}*`}
          name="street1"
          value={shipInfo.street1}
          onChange={this.onChange}
        />
        <input
          type="text"
          className={`border-checkout w-100 mt-3 ${listError.includes('phone') ? 'error' : ''}`}
          placeholder={`${phoneBt}*`}
          name="phone"
          value={shipInfo.phone}
          onChange={this.onChange}
        />

        <Row className="mt-3">
          <Col md={isMobile ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px' } : { paddingRight: '7px' }}>
            <Input
              type="select"
              className="border-checkout w-100"
              placeholder={countryBt}
              name="country"
              onChange={this.onChange}
              style={{ height: '50px' }}
            >
              {
                  _.map(listShipping || [], x => (
                    <option value={x.country.code} selected={(shipInfo.country ? shipInfo.country.toUpperCase() : '') === x.country.code.toUpperCase()}>
                      {x.country.name}
                    </option>
                  ))
                }
            </Input>
          </Col>
          <Col md={isMobile ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px', marginTop: '1rem' } : { paddingLeft: '7px' }}>
            <input
              type="text"
              className={`border-checkout w-100 ${listError.includes('city') ? 'error' : ''}`}
              placeholder={`${cityBt}*`}
              name="city"
              value={shipInfo.city}
              onChange={this.onChange}
              style={{ height: '50px' }}
            />
          </Col>
        </Row>

        <Row className="mt-3">
          <Col md="6" xs="6" style={isMobile ? { padding: '0px', paddingRight: '7px' } : { paddingRight: '7px' }}>
            <input
              type="text"
              className={`border-checkout w-100 ${listError.includes('postal_code') ? 'error' : ''}`}
              placeholder={`${zipCodeBt}*`}
              name="postal_code"
              value={shipInfo.postal_code}
              onChange={this.onChange}
            />

          </Col>
          <Col md="6" xs="6" style={isMobile ? { padding: '0px' } : { paddingRight: '15px', paddingLeft: '7px' }}>
            <input
              type="text"
              className={`border-checkout w-100 ${listError.includes('state') ? 'error' : ''}`}
              placeholder={`${stateBt}`}
              name="state"
              value={shipInfo.state}
              onChange={this.onChange}
            />
          </Col>
        </Row>
      </div>
    );

    return (
      <div className="div-popup-save-address animated faster fadeIn">
        <div className="div-content">
          <button
            type="button"
            className="button-close button-bg__none"
            onClick={this.props.closeShowAddress}
          >
            <img src={icClose} alt="back" />
          </button>
          <div className="div-header">
            <h3>
              {isAddNewAddress ? addNewAddressBt : isEditAddress ? saveAddressBt : chooseAShippingBt}
            </h3>
          </div>
          {
            isAddNewAddress || isEditAddress ? (
              htmlNewAddress
            ) : (
              <React.Fragment>
                <div className="div-list-address">
                  {
              isMobile ? (
                <div className="div-line-address-mobile">
                  {
                    _.map(listAddress, x => (
                      <ItemAdress data={x} currentAddress={currentAddress} onChangeAddress={onChangeAddress} closeShowAddress={closeShowAddress} buttonBlock={buttonBlock} />
                    ))
                  }
                </div>
              ) : (
                <Slider ref={this.refSilder} {...settings}>
                  {
                    _.map(newListAddress, d => (
                      this.addressSlider(d)
                    ))
                  }
                </Slider>
              )
            }
                </div>
                <div className={newListAddress && newListAddress.length > 1 ? 'div-control-page' : 'hidden'}>
                  <button type="button" className="button-bg__none" onClick={() => this.refSilder.current.slickPrev()}>
                    <i className="fa fa-chevron-left" />
                  </button>
                  <span>
                    {`${currentSlide} OF ${newListAddress.length}`}
                  </span>
                  <button type="button" className="button-bg__none" onClick={() => this.refSilder.current.slickNext()}>
                    <i className="fa fa-chevron-right" />
                  </button>
                </div>
              </React.Fragment>
            )
          }
          {
            isShowNewAddress && (
              <div className="button-add-address">
                <button type="button" onClick={() => (isAddNewAddress ? this.onSaveAddress() : isEditAddress ? this.onClickEditAddress() : this.setState({ isAddNewAddress: true }))}>
                  {isAddNewAddress ? saveNewAddressBt : isEditAddress ? saveAddressBt : `+ ${addNewAddressBt}`}
                </button>
                {
                  !isAddNewAddress && !isEditAddress && (
                    <button type="button" className="button-bg__none link-button" onClick={this.editAddress}>
                      {editOrBt}
                    </button>
                  )
                }

              </div>
            )
          }
        </div>
      </div>
    );
  }
}

export default PopUpSaveAdsress;
