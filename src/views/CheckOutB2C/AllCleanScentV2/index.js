import _ from 'lodash';
import React from 'react';
import { useHistory } from 'react-router-dom';
import {
  generateUrlWeb, getAltImageV2, getLinkFromButtonBlock, getNameFromButtonBlock,
} from '../../../Redux/Helpers';
import './styles.scss';

function AllCleanScentV2(props) {
  const history = useHistory();
  const itemRender = d => (
    <div className="item-clean div-row">
      <img src={d?.value?.image?.image} alt={getAltImageV2(d?.value?.image)} />
      <div className="text-name div-col">
        <h4>{d.value?.header?.header_text}</h4>
        <span>{d.value?.text}</span>
      </div>
    </div>
  );
  console.log('props.data', props.data);
  const termBt = getNameFromButtonBlock(props.buttonBlock, 'Terms and Conditions');
  const linkTermBt = getLinkFromButtonBlock(props.buttonBlock, 'Terms and Conditions');
  console.log('linkTermBt', linkTermBt);
  return (
    <div className="all-clean-scent-v2 div-col">
      <div className="content-all">
        <h3>
          {props.data?.opacity}
        </h3>
        <div className="list-event">
          {
            _.map(props.data?.icons, d => (
              itemRender(d)
            ))
          }
        </div>
      </div>
      <button type="button" className="button-bg__none bt-tearm" onClick={() => history.push(generateUrlWeb(linkTermBt))}>
        <span>{termBt}</span>
      </button>
    </div>
  );
}

export default AllCleanScentV2;
