import React, { Component } from 'react';
import PropTypes from 'prop-types';
import onClickOutside from 'react-onclickoutside';
import '../../styles/button-info.scss';
import { getNameFromCommon } from '../../Redux/Helpers';

class ButtonInfo extends Component {
  state = {
    isShow: false,
  };

  handleClickOutside = () => {
    this.setState({ isShow: false });
  }

  render() {
    const { isShow } = this.state;
    const { cmsCommon } = this.props;
    const btWhyOpen = getNameFromCommon(cmsCommon, 'Why_open_an_account');
    const btViewPrevious = getNameFromCommon(cmsCommon, 'View_previous_orders');
    const noHassle = getNameFromCommon(cmsCommon, 'No_hassle_of_ever');
    const takePart = getNameFromCommon(cmsCommon, 'Take_part_in_reward_programs');
    const earnCredits = getNameFromCommon(cmsCommon, 'Earn_credits');
    const learnMore = getNameFromCommon(cmsCommon, 'Learn_more');
    return (
      <div className="div-button-info">
        <button
          type="button"
          className="button-underline"
          onClick={() => { this.setState({ isShow: !isShow }); }}
        >
          {learnMore}
          {/* <img style={{ height: '1rem' }} src={icInfo} alt="icInfo" /> */}
        </button>
        <div className={isShow ? 'card-info' : 'hidden'}>
          <div className="header">
            {btWhyOpen}
          </div>
          <div className="div-line" />
          <div className="div-content div-col">
            <div className="div-item div-row items-center">
              <div className="point-info" />
              <span>
                {btViewPrevious}
              </span>
            </div>
            <div className="div-item div-row items-center">
              <div className="point-info" />
              <span>
                {noHassle}
              </span>
            </div>
            <div className="div-item div-row items-center">
              <div className="point-info" />
              <span>
                {takePart}
              </span>
            </div>
            <div className="div-item div-row items-center">
              <div className="point-info" />
              <span>
                {earnCredits}
              </span>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

ButtonInfo.propTypes = {
  cmsCommon: PropTypes.shape().isRequired,
};

export default onClickOutside(ButtonInfo);
