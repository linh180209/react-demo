import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { isMobile } from '../../DetectScreen';
import { getNameFromCommon } from '../../Redux/Helpers';

class HeaderItem extends Component {
  render() {
    const {
      stepCheckOut, isStepDone, step, title, className, isHiddenButton, htmlCenter, htmlBottom, cmsCommon,
    } = this.props;
    const editBt = getNameFromCommon(cmsCommon, 'EDIT');
    return (
      <div className={classnames('div-col div-header', step !== stepCheckOut ? 'border-mobile' : '')}>
        <div className={`${className} header-text div-row items-center`}>
          <button
            type="button"
            className="bt-circle active"
          >
            <span style={{ color: '#ffffff' }}>
              {step}
            </span>
          </button>
          <div className="div-content-header w-100">
            <span className="tx-header">
              {title}
            </span>
            <div className="html-right">
              {htmlCenter}
            </div>
          </div>
          <div className={classnames('div-row justify-between items-center relative h-100', isHiddenButton || !isStepDone ? 'hidden' : '')}>
            <button
              type="button"
              className="border-checkout bt-edit"
              onClick={this.props.onClick}
            >
              {editBt}
            </button>
          </div>
        </div>
        {htmlBottom}
      </div>

    );
  }
}

HeaderItem.defaultProps = {
  htmlBottom: undefined,
  htmlCenter: undefined,
  isHiddenButton: false,
  className: '',
  onClick: () => {},
};

HeaderItem.propTypes = {
  stepCheckOut: PropTypes.number.isRequired,
  isStepDone: PropTypes.bool.isRequired,
  isHiddenButton: PropTypes.bool,
  step: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  className: PropTypes.string,
  htmlCenter: PropTypes.string,
  htmlBottom: PropTypes.string,
};

export default HeaderItem;
