import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import '../../styles/footer-checkout-mobile.scss';
import icPlus from '../../image/icon/icPlus.svg';
import { useMergeState } from '../../Utils/customHooks';
import icClose from '../../image/icon/ic-close-w.svg';
import icInfo from '../../image/icon/ic-info-promo.svg';
import { generaCurrency, getNameFromButtonBlock } from '../../Redux/Helpers';

function FooterCheckoutMobile(props) {
  const [state, setState] = useMergeState({
    isAddPromotion: false,
  });

  const thisPromoBt = getNameFromButtonBlock(props.buttonBlock, 'This_promo_code');
  const addPromoCodeBt = getNameFromButtonBlock(props.buttonBlock, 'ADD_PROMO_CODE');
  const applyBt = getNameFromButtonBlock(props.buttonBlock, 'Apply');
  const totalPayBt = getNameFromButtonBlock(props.buttonBlock, 'TOTAL_PAYMENT');
  const placeOrderBt = getNameFromButtonBlock(props.buttonBlock, 'PLACE_ORDER');

  return (
    <div className={`footer-checkout-mobile div-col ${props.isShowFooter ? 'show-div' : 'hide-div'}`}>
      {
        props.isPromoErrorMobile && (
          <div className="div-card-error div-row items-center animated fadeIn">
            <img src={icInfo} alt="info" />
            <span>
              {thisPromoBt}
            </span>
          </div>
        )
      }
      {
        !props.isDisablePromotion && (
          <div className="add-promotion div-row justify-between items-center">
            {
          state.isAddPromotion ? (
            <div className="div-input-add div-row items-center">
              {
                !_.isEmpty(props.promotion) ? (
                  <span>
                    {props.promotion.code}
                  </span>
                ) : (
                  <input type="text" onChange={props.onChangeTex} value={props.valuePromo} />
                )
              }

              <button
                type="button"
                className={
                classnames((!_.isEmpty(props.promotion) ? 'div-relative' : 'div-ab'), 'has-bg', (!_.isEmpty(props.promotion) ? 'bg-red' : ''), (props.valuePromo ? '' : 'hidden'))
                }
                onClick={_.isEmpty(props.promotion) ? props.clearTextPromo : props.removePromotion}
              >
                <img src={icClose} alt="close" />
              </button>
            </div>
          ) : (
            <span>
              {addPromoCodeBt}
            </span>
          )
        }
            {
          state.isAddPromotion ? (
            <button
              type="button"
              className={classnames('bg-normal', (!props.valuePromo ? 'disabled' : ''), (!_.isEmpty(props.promotion) ? 'hidden' : ''))}
              disabled={!props.valuePromo}
              onClick={props.onCheckPromo}
            >
              {applyBt}
            </button>
          ) : (
            <button type="button" className="button-bg__none" onClick={() => setState({ isAddPromotion: true })}>
              <img src={icPlus} alt="plus" />
            </button>
          )
        }
          </div>
        )
      }
      <div className="total div-row">
        <div className="text-total div-col justify-between">
          <h4>
            {totalPayBt}
          </h4>
          <span>
            {generaCurrency(props.newPrice === -1 ? props.subtotal : props.newPrice)}
            <span className={props.newPrice === -1 ? 'hidden' : 'old-price'}>
              {generaCurrency(props.subtotal)}
            </span>
          </span>
        </div>
        <button type="button" onClick={props.onCheckOutFunc}>
          {placeOrderBt}
        </button>
      </div>
    </div>
  );
}

export default FooterCheckoutMobile;
