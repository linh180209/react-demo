import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Input } from 'reactstrap';
import _ from 'lodash';
import HeaderItem from './headerItems';
import { getNameFromCommon, getCmsCommon } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

class BillItems extends Component {
  render() {
    const {
      stepCheckOut, isStepDone, shipInfo, onChange, onClickContinute, onClickHeader, isHiddenButton,
      listShipping,
    } = this.props;
    const cmsCommon = getCmsCommon(this.props.cms);
    const shippingAddressBt = getNameFromCommon(cmsCommon, 'SHIPPING_ADDRESS');
    const billBt = getNameFromCommon(cmsCommon, 'BILLING');
    const addressBt = getNameFromCommon(cmsCommon, 'Address');
    const zipCodeBt = getNameFromCommon(cmsCommon, 'ZIP_CODE');
    const countryBt = getNameFromCommon(cmsCommon, 'COUNTRY');
    const phoneBt = getNameFromCommon(cmsCommon, 'PHONE');
    const continueBt = getNameFromCommon(cmsCommon, 'CONTINUE');

    const htmlCenter = (
      <div className="h-100 div-col justify-center ml-5" style={isMobile ? { marginBottom: '30px' } : {}}>
        <span className="tx-more-infor">
          {shipInfo.billStreet1}
        </span>
        <span className="tx-more-infor">
          {shipInfo.billPostalCode}
        </span>
        <span className="tx-more-infor">
          {shipInfo.billCountry}
        </span>
        <span className="tx-more-infor">
          {shipInfo.billPhone}
        </span>
      </div>
    );
    return (
      <React.Fragment>
        <HeaderItem
          cmsCommon={cmsCommon}
          stepCheckOut={stepCheckOut}
          isStepDone={isStepDone}
          step={3}
          title={billBt}
          onClick={onClickHeader}
          isHiddenButton={isHiddenButton}
          htmlCenter={htmlCenter}
        />
        {
                stepCheckOut === 3
                  ? (
                    <React.Fragment>
                      <div className="div-col" style={{ paddingLeft: '50px' }}>
                        <div className="div-col mt-3 mb-3">
                          <span style={{ color: '#0D0D0D' }}>
                            {shippingAddressBt}
                          </span>
                        </div>
                        <div className="div-col">
                          <input
                            type="text"
                            className="border-checkout mt-2 mb-2"
                            placeholder={addressBt}
                            name="billStreet1"
                            value={shipInfo.billStreet1}
                            onChange={onChange}
                          />
                          <Row>
                            <Col md="6" xs="12" style={isMobile ? { padding: '0px' } : {}}>
                              <input
                                type="text"
                                className="border-checkout w-100"
                                placeholder={zipCodeBt}
                                name="billPostalCode"
                                value={shipInfo.billPostalCode}
                                onChange={onChange}
                              />
                            </Col>
                            <Col md="6" xs="12" style={isMobile ? { padding: '0px', marginTop: '0.5rem' } : {}}>
                              <Input
                                type="select"
                                className="border-checkout w-100"
                                placeholder={countryBt}
                                name="billCountry"
                                onChange={onChange}
                                style={{ height: '42px' }}
                              >
                                {
                              _.map(listShipping, x => (
                                <option value={x.country.code}>
                                  {x.country.name}
                                </option>
                              ))
                            }
                              </Input>
                            </Col>
                          </Row>
                          <div className="div-row mt-2 mb-3">
                            <input
                              type="text"
                              className="border-checkout w-100"
                              placeholder={phoneBt}
                              name="billPhone"
                              onChange={onChange}
                              value={shipInfo.billPhone}
                            />
                          </div>
                          <div className="mb-5">
                            <button
                              type="button"
                              className="bt-checkout"
                              onClick={onClickContinute}
                            >
                              {continueBt}
                            </button>
                          </div>
                        </div>
                      </div>
                    </React.Fragment>
                  ) : (<div />)
              }

        <div style={{ background: '#0D0D0D', height: '1px', width: '100%' }} />
      </React.Fragment>
    );
  }
}

BillItems.propTypes = {
  stepCheckOut: PropTypes.number.isRequired,
  isStepDone: PropTypes.bool.isRequired,
  isHiddenButton: PropTypes.bool.isRequired,
  onClickHeader: PropTypes.func.isRequired,
  shipInfo: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  onClickContinute: PropTypes.func.isRequired,
  listShipping: PropTypes.arrayOf().isRequired,
  cms: PropTypes.shape().isRequired,
};

export default BillItems;
