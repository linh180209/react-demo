import React, { useState, useEffect } from 'react';
import {
  Modal, ModalBody,
} from 'reactstrap';
import classnames from 'classnames';
import { addFontCustom } from '../../../Redux/Helpers';
import './styles.scss';

function ModalCheckoutMobile(props) {
  return (
    <Modal className={classnames('modal-full-screen', addFontCustom())} isOpen={props.isOpen}>
      <ModalBody>
        {props.children}
      </ModalBody>
    </Modal>
  );
}

export default ModalCheckoutMobile;
