/* eslint-disable react/sort-comp */
import classnames from 'classnames';
import * as EmailValidator from 'email-validator';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import MetaTags from 'react-meta-tags';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Input } from 'reactstrap';
import smoothscroll from 'smoothscroll-polyfill';
import CardItem from '../../components/B2B/CardItem';
import BuyMoreGiftShipping from '../../components/BuyMoreGiftShipping';
import IconBlockCheckOut from '../../components/IconBlockCheckOut';
import ReceiveGrams from '../../components/receiveGrams';
import {
  CHECKOUT_FUNCTION_URL, CHECK_EMAIL_LOGIN_URL, CHECK_PROMOTION_CODE_V2, CREATE_ADDRESS_URL, GET_BASKET_URL, GET_GIFT_FREE, GET_LIST_ADDRESS_URL, GET_SHIPPING_EXPRESS_URL, GET_SHIPPING_URL, PAYPAL_GENERATE_URL, PUT_OUTCOME_URL, STRIPE_GENERATE_URL, UPDATE_ADDRESS_GUEST_URL,
} from '../../config';
import { isBrowser, isMobile, isTablet } from '../../DetectScreen';
import icClose from '../../image/icon/ic-close-region.svg';
import icPlus from '../../image/icon/ic-plus.svg';
import icRemove from '../../image/icon/ic-remove-promo.svg';
import icSub from '../../image/icon/ic-sub.svg';
import icError from '../../image/icon/info-error.svg';
import icSuccess from '../../image/icon/info-success.svg';
import {
  addProductGiftBasket, clearBasket, clearProductBasket, createBasketGuest, deleteProductBasket, preCheckOutBasket, updateProductArrayBasket, updateProductBasket,
} from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import {
  fetchCMSHomepage, generaCurrency, generateHreflang, generateUrlWeb, getCmsCommon, getNameFromButtonBlock, getNameFromCommon, getSEOFromCms, gotoShopHome, hiddenChatIcon, isAllGift, isCheckNull,
  objectEquals, removeLinkHreflang, trackGTMCheckOut, visibilityChatIcon,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import '../../styles/checkout.scss';
import '../../styles/footer-checkout-mobile.scss';
import { addScriptGoogleAPI } from '../../Utils/addScriptGoogle';
import { addScriptStripe } from '../../Utils/addStripe';
import AllCleanScentV2 from './AllCleanScentV2';
import CustomerItemV2 from './customerItemV2';
import CustomerSay from './customerSay';
import ModalCheckoutMobile from './modalCheckOutMobile';
import PaymentItemV2 from './paymentItemV2';
import PopUpSaveAdsress from './popUpSaveAddress';
import PromotionGiftV2 from './promotionGiftV2';
import ShippingItemV2 from './shippingItemsV2';
import StepInfo from './StepInfo';


const getCms = (cms) => {
  if (!cms) {
    return { seo: {} };
  }
  const seo = getSEOFromCms(cms);
  const { body } = cms;
  const textBlock = _.filter(body, x => x.type === 'text_block');
  const feedbackBlock = _.find(body, x => x.type === 'feedbacks_block');
  const buttonBlock = _.filter(body, x => x.type === 'button_block');
  const imageBlockIcon = _.filter(body, x => x.type === 'image_block');
  const iconBlock = _.find(body, x => x.type === 'icons_block' && !['icon-scent', 'icon-top'].includes(x.value?.image_background?.caption))?.value;
  const iconBlockTop = _.find(body, x => x.type === 'icons_block' && x.value?.image_background?.caption === 'icon-top')?.value;
  const iconBlockScent = _.find(body, x => x.type === 'icons_block' && x.value?.image_background?.caption === 'icon-scent')?.value;
  const blogBlock = _.find(body, x => x.type === 'blogs_block').value;
  return {
    seo, textBlock, imageBlockIcon, buttonBlock, iconBlock, blogBlock, iconBlockTop, feedbackBlock, iconBlockScent,
  };
};
const listExpressTimeslot = ['8AM - 12PM', '12PM - 6PM', '6PM - 9PM'];
const addressDefaul = {
  street1: undefined,
  city: undefined,
  state: undefined,
  country: undefined,
  postal_code: undefined,
  pan_card: undefined,
};
class CheckOutB2CPage extends Component {
  constructor(props) {
    super(props);
    this.discountCode = auth.getDiscountCode();
    const { basket } = props;
    this.state = {
      promotionCode: basket?.promo?.code ? basket?.promo?.code : (this.discountCode ? this.discountCode : ''),
      noteText: '',
      promotion: basket?.promo?.id ? basket?.promo : {},
      payMethod: 'stripe',
      bankCode: '',
      stepCheckOut: 1,
      isStep1Done: false,
      isStep2Done: false,
      isStep3Done: false,
      isStep4Done: false,
      isSelfCollect: false,
      isExpress: false,
      expressTimeslot: listExpressTimeslot[0],
      timeDelivery: 'Any Time',
      isBillAddressSame: true,
      seo: {},
      textBlock: [],
      imageBlockIcon: [],
      iconBlock: undefined,
      blogBlock: undefined,
      isShowCreateAccount: false,
      listError: [],
      buttonBlock: [],
      iconBlockTop: [],
      feedbackBlock: {},
      iconBlockScent: {},
      isOpenNote: false,
      isOPenPromo: false,
      isShowChoiseAddress: false,
      listAddress: [],
      isShowOptionAddress: false,
      giftFrees: [],
      isShowFooter: true,
    };
    this.shipping = 0;
    this.giftWrap = false;
    this.shipInfo = {};
    this.billingInfo = _.cloneDeep(addressDefaul);
    this.timeoutShipInfo = undefined;
    this.checkOutData = undefined;
    this.isCheckOutClicked = false;
    this.optionTime = [
      'Any Time',
      '9.00 am to 12.00 pm',
      '12.00 pm to 3.00 pm',
      '3.00 pm to 6.00 pm',
      '6.00 pm to 10.00 pm',
    ];
    this.currentAddress = {};
    this.isCheckCartUpdate = false;
  }

  componentDidMount() {
    smoothscroll.polyfill();

    addScriptStripe();
    addScriptGoogleAPI();

    if (isMobile) {
      document.addEventListener('scroll', this.trackScrolling);
      hiddenChatIcon();
    }
    // document.body.appendChild(script);
    this.fetchCmsCheckOut();
    this.fetchShippingInfo();
    this.fetchFreeGift();
  }

  componentWillUnmount() {
    removeLinkHreflang();
    this.saveDataCheckOut();
    if (isMobile) {
      document.removeEventListener('scroll', this.trackScrolling);
      visibilityChatIcon();
    }
  }

  saveDataCheckOut = () => {
    const stateData = _.cloneDeep(this.state);
    delete stateData.stepCheckOut;
    delete stateData.isStep1Done;
    delete stateData.isStep2Done;
    delete stateData.isStep3Done;
    delete stateData.isStep4Done;
    delete stateData.seo;
    delete stateData.textBlock;
    delete stateData.imageBlockIcon;
    delete stateData.iconBlock;
    delete stateData.blogBlock;
    delete stateData.listError;
    delete stateData.buttonBlock;
    delete stateData.isOpenNote;
    delete stateData.isOPenPromo;
    delete stateData.isShowChoiseAddress;
    delete stateData.listAddress;
    delete stateData.isShowOptionAddress;
    delete stateData.giftFrees;
    delete stateData.isShowFooter;
    delete stateData.iconBlockTop;
    delete stateData.feedbackBlock;
    delete stateData.iconBlockScent;
    delete this.shipInfo.idAddress;

    const dataCheckOut = {
      stateData,
      shipInfo: this.shipInfo,
      billingInfo: this.billingInfo,
      currentAddress: this.currentAddress,
    };
    auth.saveDataCheckOut(JSON.stringify(dataCheckOut));
  }

  getDataCheckOut = () => {
    try {
      const sDataCheckOut = auth.getDataCheckOut();
      if (sDataCheckOut) {
        const dataCheckOut = JSON.parse(sDataCheckOut);
        if (!_.isEmpty(dataCheckOut.shipInfo)) {
          this.shipInfo = dataCheckOut.shipInfo;
          this.billingInfo = dataCheckOut.billingInfo || _.cloneDeep(addressDefaul);
          this.shipInfo.country = isCheckNull(this.shipInfo?.country) ? auth.getCountry() : this.shipInfo?.country;
          this.billingInfo.country = isCheckNull(this.billingInfo?.country) ? auth.getCountry() : this.billingInfo?.country;
          this.currentAddress = this.currentAddress || dataCheckOut.currentAddress;
          if (this.state.promotionCode) {
            delete dataCheckOut.stateData?.promotionCode;
            delete dataCheckOut.stateData?.promotion;
          }
          this.setState(dataCheckOut.stateData, () => { this.preCheckOut(); });
        }
      }
    } catch (error) {
      console.log('error', error);
    }
  }

  isBottom = el => (
    el.getBoundingClientRect().bottom <= window.innerHeight
  )

  trackScrolling = () => {
    const wrappedElement = document.getElementById('id-promotion');
    if (this.isBottom(wrappedElement) && this.state.isShowFooter) {
      this.setState({ isShowFooter: false });
    } else if (!this.isBottom(wrappedElement) && !this.state.isShowFooter) {
      this.setState({ isShowFooter: true });
    }
  };

  fetchFreeGift = () => {
    const options = {
      url: GET_GIFT_FREE,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      this.setState({ giftFrees: result });
    }).catch((err) => {
      console.log('err', err);
    });
  }

  isCheckEqualBasket = (basket1, basket2) => {
    const clone1 = _.cloneDeep(basket1);
    const clone2 = _.cloneDeep(basket2);
    _.forEach(clone1.items, (x) => {
      delete x.id;
    });
    delete clone1.meta;
    delete clone1.store_discount;
    delete clone2.meta;
    delete clone2.store_discount;
    _.forEach(clone2.items, (x) => {
      delete x.id;
    });
    return objectEquals(clone1, clone2);
  }

  componentDidUpdate = (prevProps) => {
    const { basket, login } = this.props;
    delete basket.date_modified;
    if (!this.isCheckCartUpdate && basket && !this.isCheckEqualBasket(basket, prevProps.basket)) {
      this.isCheckCartUpdate = true;
      if (!this.state.promotionCode && !_.isEmpty(basket.promo) && basket.promo.code) {
        this.state.promotionCode = basket.promo.code;
        if (basket?.promo?.id) {
          this.setState({ promotion: basket?.promo });
        } else {
          this.onCheckPromo();
        }
      } else if (this.state.promotionCode) {
        this.onCheckPromo('NOT_SHOW_TOAST');
      } else {
        this.preCheckOut();
      }
    } else {
      this.isCheckCartUpdate = false;
    }
    if (login !== prevProps.login) {
      if (login && login.user && login.user.id) {
        this.shipInfo.name = `${login.user.first_name} ${login.user.last_name}`;
        this.shipInfo.firstName = login.user.first_name;
        this.shipInfo.lastName = login.user.last_name;
        this.shipInfo.email = login.user.email;
        this.shipInfo.phone = login.user.phone;
        this.shipInfo.dob = login.user.date_of_birth ? moment(login.user.date_of_birth).format('MM/DD/YYYY') : '';
        this.shipInfo.gender = login.user.gender ? (login.user.gender || 'Male') : 'Male';
        this.forceUpdate();
      }
    }
  }

  fetchCmsCheckOut = () => {
    const { cms } = this.props;
    const cmsCheckOut = _.find(cms, x => x.title === 'CheckOut');
    if (!cmsCheckOut) {
      fetchCMSHomepage('checkout').then((result) => {
        const cmsData = result;
        this.props.addCmsRedux(cmsData);
        const dataCms = getCms(cmsData);
        this.setState(dataCms);
      });
    } else {
      const dataCms = getCms(cmsCheckOut);
      this.setState(dataCms);
    }
  }

  onChangePromo = (e) => {
    const { value } = e.target;
    this.setState({ promotionCode: value, isPromoError: false });
  }

  onChangeGenderSelect = (value, name) => {
    this.shipInfo.gender = value?.value;
    this.forceUpdate();
  };

  onChangeBillingInfo = (e) => {
    const { value, name } = e.target;
    _.assign(this.billingInfo, { [name]: value });
    this.forceUpdate();
  };

  onChange = (e) => {
    const { value, name } = e.target;
    this.shipInfo[name] = value;
    this.forceUpdate();
    this.setState({ listError: [] });
    if (name === 'firstName' || name === 'lastName') {
      this.shipInfo.name = `${this.shipInfo.firstName} ${this.shipInfo.lastName}`;
    }
    if (name === 'email') {
      if (this.timeOutEmail) {
        clearTimeout(this.timeOutEmail);
        this.timeOutEmail = null;
      }
      this.timeOutEmail = setTimeout(this.checkEmailExist, 2000);
    }
    if (name === 'country') {
      // handle if change country but this country don't support express
      const { isExpress } = this.state;
      if (isExpress) {
        const ele = _.find(this.listShipping, x => x.country.code.toLowerCase() === value.toLowerCase());
        if (ele && !ele.is_express) {
          this.setState({ isExpress: false }, () => {
            this.preCheckOut();
            this.forceUpdate();
          });
          return;
        }
      }
      this.preCheckOut();
      this.forceUpdate();
    }
  }

  removePromotion = () => {
    this.setState({ promotion: undefined, promotionCode: '' }, () => {
      delete this.props.basket.promo;
      this.preCheckOut();
    });
  }

  onCheckPromo = (isNotShowToast) => {
    const { login, basket } = this.props;
    if (isNotShowToast !== 'NOT_SHOW_TOAST') {
      trackGTMCheckOut(6, basket);
    }

    const cmsCommon = getCmsCommon(this.props.cms);
    const proCodeNotAvaiBt = getNameFromCommon(cmsCommon, 'Promotion_code_is_not_avaialble.');
    const proCodeAvaiBt = getNameFromCommon(cmsCommon, 'Promotion_code_is_avaialble.');

    const { id } = basket;
    if (!id) {
      toastrError(proCodeNotAvaiBt);
      return;
    }

    const { promotionCode } = this.state;
    const options = {
      url: `${CHECK_PROMOTION_CODE_V2.replace('{id}', id)}`,
      method: 'POST',
      body: {
        code: promotionCode,
      },
    };
    fetchClient(options, true).then((result) => {
      const { isError } = result;
      if (isError) {
        if (isMobile && this.state.isShowFooter) {
          this.setState({ isPromoErrorMobile: true });
          setTimeout(() => {
            this.setState({ isPromoErrorMobile: false });
          }, 2000);
        } else if (isNotShowToast !== 'NOT_SHOW_TOAST') {
          toastrError(proCodeNotAvaiBt);
        }
        this.setState({ promotion: '', isPromoError: true }, () => {
          this.preCheckOut(true);
        });
      } else {
        this.setState({
          promotion: result, promotionCode: result.code, isOPenPromo: false, isPromoError: false,
        }, () => {
          this.preCheckOut();
          this.saveDataCheckOut();
        });
        if (isNotShowToast !== 'NOT_SHOW_TOAST') {
          toastrSuccess(proCodeAvaiBt);
        }
      }
    }).catch((err) => {
      if (isNotShowToast !== 'NOT_SHOW_TOAST') {
        toastrError(err.message);
      }
    });
  }


  assignEmailToBasket = (basketId, email) => {
    if (!basketId || !email) {
      return;
    }
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basketId),
      method: 'PUT',
      body: {
        email,
      },
    };
    return fetchClient(option);
  }

  assignBasketToUser = (basketId, userId) => {
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basketId),
      method: 'PUT',
      body: {
        user: userId,
      },
    };
    return fetchClient(option);
  }

  createAddressFromApplePay = async (body) => {
    const { login } = this.props;
    const isLogined = login && login.user && login.user.id;
    try {
      const address = isLogined ? await this.createAddressWithUser(body) : await this.createAddress(body);
      return address;
    } catch (err) {
      return undefined;
    }
  }

  onCheckOutFunc = async () => {
    if (this.isCheckOutClicked) {
      return;
    }

    const { basket, login, loadingPage } = this.props;

    // tracking GMT
    trackGTMCheckOut(8, basket);

    this.saveDataCheckOut();

    if (!basket || !basket.id) {
      toastrError('Checkout is failed');
      return;
    }
    const isOnlyGift = isAllGift(basket.items);
    const isLogined = login && login.user && login.user.id;

    // assign user to cart, if basket id !== login cart
    if (isLogined && basket.id !== login.id) {
      this.assignBasketToUser(basket.id, login.id);
    }

    if (!isOnlyGift) {
      if (!this.checkContinuteStep1('payment') || !this.checkContinuteStep2('payment')) {
        return;
      }
    } else if (!this.checkContinuteStep1('payment')) {
      return;
    }

    if (!document.querySelector('.div-option-checkout.active')) {
      toastrError('Please select a payment method');
      return;
    }

    const {
      payMethod, noteText, isSelfCollect, isExpress, promotion,
      expressTimeslot, bankCode,
    } = this.state;
    if (!this.shipInfo.idAddress) {
      const address = isLogined && this.shipInfo.create_address ? await this.createAddressWithUser(this.shipInfo) : await this.createAddress(this.shipInfo);
      if (!address) {
        toastrError('Check Out Error');
        return;
      }
      this.shipInfo.idAddress = address.id;
    }

    let idbillingAddress;
    if (this.state.isBillAddressSame) {
      const resultAddress = await this.createAddress(this.shipInfo);
      idbillingAddress = resultAddress?.id;
    } else {
      const resultAddress = await this.createAddress(_.assign(this.shipInfo, this.billingInfo));
      idbillingAddress = resultAddress?.id;
    }

    const ele = _.find(this.listShipping, x => x.country.code.toLowerCase() === this.shipInfo.country.toLowerCase());
    const options = {
      url: CHECKOUT_FUNCTION_URL.replace('{id}', basket.id),
      method: 'POST',
      body: {
        payment_method: ['atm_card', 'int_card'].indexOf(payMethod) >= 0 ? 'zalopay' : payMethod,
        bank_code: ['zalopayapp', 'momo', 'atm_card', 'int_card'].indexOf(payMethod) ? bankCode : '',
        shipping_address: (isSelfCollect || isOnlyGift) ? undefined : this.shipInfo.idAddress,
        billing_address: idbillingAddress,
        note: noteText,
        shipping: this.shipping,
        is_self_collect: isSelfCollect,
        is_express: isExpress && ele && ele.is_express,
        express_timeslot: isExpress && ele && ele.is_express ? expressTimeslot : undefined,
        personality_choice: this.idPersonalityChoice,
        // delivery_slot: timeDelivery,
        packaging: this.packaging,
        is_gift_wrapped: this.giftWrap,
        gender: this.shipInfo.gender,
        date_of_birth: this.shipInfo.dob ? moment(this.shipInfo.dob, 'MM/DD/YYYY').valueOf() / 1000 : undefined,
        is_get_newsletters: this.shipInfo.is_get_newsletters,
      },
    };
    if (promotion && promotion.id) {
      options.body.promos = [promotion.id];
    }
    if (this.props.refPoint !== '') {
      options.body.ref = this.props.refPoint;
    }
    if (!isLogined) {
      options.body.meta = {
        guest: {
          name: this.shipInfo.name,
          email: this.shipInfo.email,
          cart: basket.id,
          phone: this.shipInfo.phone,
          create_account: isOnlyGift ? false : this.shipInfo.create_account,
        },
      };
    }
    this.isCheckOutClicked = true;
    loadingPage(true);
    fetchClient(options, true).then((result) => {
      const { isError, order } = result;
      this.isCheckOutClicked = false;
      loadingPage(false);
      if (!isError) {
        if (order) {
          this.props.history.push(generateUrlWeb(`/pay/success/${order}`));
          this.props.clearBasket();
          return;
        }
        if (payMethod === 'paypal') {
          const htmlProcess = `${result.substring(0, 6)}id='id-pay'${result.substring(6, result.length)}`;
          const s = document.getElementById('div-button');
          s.innerHTML = htmlProcess;
          const idForm = document.getElementById('id-pay');
          idForm.submit();
        } else if (
          [
            'zalopay',
            'momo',
            'bank_transfer',
            'atm_card',
            'int_card',
            'cod',
          ].indexOf(payMethod) >= 0
        ) {
          if (result.status) {
            window.location.href = result.data.url;
            return;
          }
          toastrError(result.message);
        } else {
          this.redirectStripe(result);
        }
      }
    }).catch((err) => {
      toastrError(err);
      this.isCheckOutClicked = false;
      loadingPage(false);
    });
  }

  updateAddress = (id, data) => {
    if (!id) {
      return;
    }
    const options = {
      url: UPDATE_ADDRESS_GUEST_URL.replace('{id}', id),
      method: 'PUT',
      body: data,
    };
    fetchClient(options, true).then((result) => {
      console.log('result', result);
    });
  }

  paypalGenerate = (order, total) => {
    const defaultHeaders = {
      'Content-Type': 'application/json',
    };
    const options = {
      method: 'POST',
      body: JSON.stringify({
        order,
        total,
      }),
      headers: defaultHeaders,
    };
    fetch(new Request(PAYPAL_GENERATE_URL, options))
      .then(result => result.text()).then((html) => {
        const htmlProcess = `${html.substring(0, 6)}id='id-pay'${html.substring(6, html.length)}`;
        const s = document.getElementById('div-button');
        s.innerHTML = htmlProcess;
        const idForm = document.getElementById('id-pay');
        idForm.submit();
        // this.setState({ formPay: htmlProcess, isShowFormPay: true });
      }).catch((err) => {
        toastrError(err);
      });
  }

  stripeGenerate = (order, total) => {
    const options = {
      url: STRIPE_GENERATE_URL,
      method: 'POST',
      body: {
        order,
        total,
      },
    };
    fetchClient(options)
      .then((result) => {
        // const s = document.createElement('div');
        // s.innerHTML = html;
        // this.instance.appendChild(s);
        this.redirectStripe(result);
        // this.setState({ formPay: result, isShowFormPay: true });
      }).catch((err) => {
        toastrError(err);
      });
  }

  redirectStripe = (formPay) => {
    const { key, session } = formPay;
    const stripe = window.Stripe(key);
    stripe.redirectToCheckout({
      // Make the id field from the Checkout Session creation API response
      // available to this file, so you can provide it as parameter here
      // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
      sessionId: session,
    }).then((result) => {
      console.log('result', result);
    });
  }

  initalData = (address) => {
    const { login } = this.props;
    const defaults = {
      firstName: login && login.user ? `${login.user.first_name}` : '',
      lastName: login && login.user ? `${login.user.last_name}` : '',
      name: login && login.user ? `${login.user.first_name} ${login.user.last_name}` : '',
      dob: login && login.user && login.user.date_of_birth ? moment(login.user.date_of_birth).format('MM/DD/YYYY') : '',
      gender: login && login.user && login.user.gender ? login.user.gender : 'Male',
      street1: address ? address.street1 : '',
      street2: address ? address.street2 : '',
      apartment: address ? address.apartment : '',
      state: address ? address.state : '',
      pan_card: '',
      city: address ? address.city : '',
      postal_code: address ? address.postal_code : '',
      country: address?.country?.code || auth.getCountry(),
      email: login && login.user ? login.user.email : auth.getEmail(),
      phone: address ? address.phone : (this.shipInfo?.phone || ''), // choose new address maybe clear phone
      billStreet1: '',
      billPostalCode: '',
      billPhone: '',
      billCountry: auth.getCountry(),
      create_account: true,
      create_address: true,
      is_get_newsletters: true,
    };
    this.shipInfo = defaults;
    this.preCheckOut();
    this.forceUpdate();
  }

  isAllGift = (data) => {
    const ele = _.find(data, d => d.item.product.type.name !== 'Gift');
    return !ele;
  }

  preCheckOutApplePay = (country) => {
    this.shipInfo.country = country;
    this.preCheckOut();
  }

  preCheckOut = (isNotCheckPromo = false) => {
    const { country, email } = this.shipInfo;
    const { id, items, promo } = this.props.basket;
    const { isSelfCollect, isExpress, promotion } = this.state;
    if (!items || items.length === 0) {
      if (!id) {
        return;
      }
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
      return;
    }
    const isOnlyGift = this.isAllGift(items);
    const eleCountry = _.find(this.listShipping, x => x.country?.code?.toLowerCase() === country?.toLowerCase());
    if (id && eleCountry) {
      const data = {
        id,
        body: isSelfCollect || isOnlyGift ? {} : {
          shipping_address: {
            country: country ? country?.toLowerCase() : '',
          },
          is_express: !!(isExpress && eleCountry?.is_express),
          is_gift_wrapped: this.giftWrap,
          promos: undefined,
        },
      };

      // check discount code
      if (!isCheckNull(this.discountCode)) {
        this.state.promotionCode = this.discountCode;
        this.discountCode = '';
        auth.setDiscountCode('');
        this.onCheckPromo();
        return;
      }

      if (promotion && promotion.id) {
        data.body.promos = [promotion.id];
      } else if (promo && promo.code && !isNotCheckPromo) {
        this.state.promotionCode = promo.code;
        this.onCheckPromo();
        return;
      }
      this.props.preCheckOutBasket(data);
    }
  }

  isCheckCountry = (country) => {
    const shipMoney = _.find(this.listShipping, d => d.country.code === country);
    return !!shipMoney;
  }

  updateShipping = (country) => {
    const shipMoney = _.find(this.listShipping, d => d.country.code === country);
    let shipping = 0;
    if (shipMoney) {
      const {
        subtotal,
      } = this.props.basket;

      if (subtotal >= shipMoney.threshold) {
        shipping = 0;
      } else {
        shipping = shipMoney.shipping;
      }
    } else {
      shipping = 0;
    }
    return shipping;
  }

  fetchOutCome = () => {
    if (isCheckNull(auth.getOutComeId())) {
      return;
    }
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', auth.getOutComeId()),
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        const { personality_choice: personalityChoice } = result;
        this.idPersonalityChoice = personalityChoice ? personalityChoice.id : undefined;
      }
    }).catch((err) => {
      console.log('err', err);
    });
  }

  fetchShippingInfo = () => {
    const optionExpress = {
      url: GET_SHIPPING_EXPRESS_URL,
      method: 'GET',
    };
    const options = {
      url: GET_SHIPPING_URL,
      method: 'GET',
    };
    Promise.all([fetchClient(options), fetchClient(optionExpress)]).then((results) => {
      _.forEach(results[1], (x) => {
        const ele = _.find(results[0], d => d.country.code === x.country.code);
        if (ele) {
          ele.is_express = x.is_express;
          ele.shippingExpress = x.shipping;
        }
      });
      _.forEach(results[0], (x) => {
        x.name = x.country.name;
      });
      this.listShipping = _.orderBy(results[0], 'name', 'asc');
      this.fetchAdress();
    }).catch((err) => {
      console.log('err', err);
    });
  }

  fetchAdress = () => {
    const dataUser = _.isEmpty(this.props.login) ? auth.getLogin() : this.props.login;
    if (dataUser && dataUser.user && dataUser.user.id) {
      const option = {
        url: GET_LIST_ADDRESS_URL,
        method: 'GET',
      };
      fetchClient(option, true).then((result) => {
        if (result && !result.isError) {
          this.setState({ listAddress: result, isShowOptionAddress: result && result.length > 0 });
          const address = _.find((result), x => x.is_default_baddress);
          if (!address && result.length > 0) {
            this.initalData(result[result.length - 1]);
            this.currentAddress = result[result.length - 1];
            this.getDataCheckOut();
            return;
          }
          this.initalData(address);
          this.currentAddress = address;

          // get data checkout before
          this.getDataCheckOut();

          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        console.log('error: ', error);
      });
    } else {
      this.setState({ isShowOptionAddress: false });
      this.initalData();

      // get data checkout before
      this.getDataCheckOut();
    }
  }

  // fetchAdress = () => {
  //   if (!this.props.login) {
  //     return;
  //   }
  //   const { user } = this.props.login;
  //   if (user && user.id) {
  //     const options = {
  //       url: GET_ADDRESS_URL.replace('{user_pk}', user.id),
  //       method: 'GET',
  //     };
  //     fetchClient(options, true).then((result) => {
  //       const address = _.find((result), x => x.is_default_baddress);
  //       if (!address && result.length > 0) {
  //         this.initalData(result[result.length - 1]);
  //         this.currentAddress = result[result.length - 1];
  //         return;
  //       }
  //       this.initalData(address);
  //       this.currentAddress = address;
  //     }).catch((err) => {
  //       toastrError(err);
  //     });
  //   } else {
  //     this.initalData();
  //   }
  // }

  isAddressSame = () => {
    if (_.isEmpty(this.currentAddress)) {
      return false;
    }
    const {
      country, city, email, postal_code, phone, street1, state,
    } = this.currentAddress;
    if (this.shipInfo.country !== (country ? country.code : undefined)) {
      return false;
    }
    if (this.shipInfo.city !== city && !_.isEmpty(this.shipInfo.city && !_.isEmpty(city))) {
      return false;
    }
    if (this.shipInfo.email !== email) {
      return false;
    }
    if (this.shipInfo.postal_code !== postal_code) {
      return false;
    }
    if (this.shipInfo.phone !== phone) {
      return false;
    }
    if (this.shipInfo.street1 !== street1) {
      return false;
    }
    if (this.shipInfo.state !== state && !_.isEmpty(this.shipInfo.state && !_.isEmpty(state))) {
      return false;
    }
    return true;
  }

  validityAddress = (data) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const key in data) {
      if (!data[key] && key !== 'idAddress') {
        return false;
      }
    }
    return true;
  }

  createAddressWithUser = async (body) => {
    const { user } = this.props.login;
    if (user && user.id) {
      _.assign(body, { is_active: this.shipInfo.create_address });
      const options = {
        url: GET_LIST_ADDRESS_URL,
        method: 'POST',
        body,
      };
      try {
        const address = await fetchClient(options, true);
        return address;
      } catch (error) {
        console.log('error', error);
      }
      return null;
    }
  }

  createAddress = async (body) => {
    const options = {
      url: CREATE_ADDRESS_URL,
      method: 'POST',
      body,
    };
    try {
      const address = await fetchClient(options);
      return address;
    } catch (error) {
      console.log('error', error);
    }
    return null;
  }

  onChangeTimeDelivery = (e) => {
    const { value } = e.target;
    this.setState({ timeDelivery: value });
  }

  onChangeExpressTimeSlot = (d) => {
    this.setState({ expressTimeslot: d });
  }

  changeNote = (e) => {
    const { value } = e.target;
    this.setState({ noteText: value });
  }

  onClickCreateAccount = (e) => {
    const { checked } = e.target;
    this.shipInfo.create_account = checked;
    this.forceUpdate();
  }

  onClickCreateAddress = (e) => {
    const { checked } = e.target;
    this.shipInfo.create_address = checked;
    this.forceUpdate();
  }

  handleUpdatePromoItem = (promo) => {
    const { items } = this.props.basket;
    const { id, gift } = promo;
    const itemPromos = _.filter(items, x => x.item.product.type.toLowerCase() === gift.type.toLowerCase());
    const listItemUpdate = [];
    let quantityPromo = gift.quantity;
    _.forEach(itemPromos, (d) => {
      if (quantityPromo > 0) {
        listItemUpdate.push({
          idCart: this.props.basket.id,
          item: {
            promo: id,
            quantity: d.quantity,
            total: (d.quantity - quantityPromo < 0 ? 0 : (d.quantity - quantityPromo) * d.price),
            id: d.id,
          },
        });
        quantityPromo -= d.quantity;
      }
    });
    this.props.updateProductArrayBasket(listItemUpdate);
  }

  gotoAnchor = (id) => {
    const ele = document.getElementById(id);
    if (ele && (isMobile || isTablet)) {
      ele.scrollIntoView({ behavior: 'smooth' });
      // window.scrollBy(0, offset);
    }
  }

  checkEmailExist = () => {
    const { login } = this.props;
    const isLogined = login && login.user && login.user.id;
    if (isLogined) {
      this.setState({ isShowCreateAccount: false });
      this.shipInfo.create_account = false;
      return;
    }
    const { email } = this.shipInfo;
    if (!email) {
      this.setState({ isShowCreateAccount: true });
      return;
    }
    const option = {
      url: CHECK_EMAIL_LOGIN_URL.replace('{email}', email),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.shipInfo.create_account = false;
        this.setState({ isShowCreateAccount: false });
        return;
      }
      throw new Error();
    }).catch(() => {
      this.shipInfo.create_account = true;
      this.setState({ isShowCreateAccount: true });
    });
  }

  checkContinuteStep1 = (status) => {
    // const { name, email, phone } = this.shipInfo;
    const {
      email, firstName, lastName, phone,
    } = this.shipInfo;
    const { listError } = this.state;
    const fieldError = [];
    const isPhoneValid = phone?.length && phone?.length > 5;
    this.saveDataCheckOut();
    if (!firstName || !email || !lastName || !isPhoneValid) {
      if (!email) {
        fieldError.push('email');
        listError.push('email');
      }
      if (!firstName) {
        fieldError.push('first name');
        listError.push('firstName');
      }
      if (!lastName) {
        fieldError.push('last name');
        listError.push('lastName');
      }
      if (!isPhoneValid) {
        fieldError.push('phone');
        listError.push('phone');
      }
      toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }

    // check '@' include first name, last name
    if (firstName.includes('@')) {
      fieldError.push('First name');
      listError.push('firstName');
    }
    if (lastName.includes('@')) {
      fieldError.push('Last name');
      listError.push('lastName');
    }
    if (listError.length > 0) {
      toastrError('@ is an invalid character for name');
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }

    if (!EmailValidator.validate(this.shipInfo.email)) {
      listError.push('email');
      toastrError('The email is not available');
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }
    this.checkEmailExist();
    if (status !== 'payment') {
      this.setState({ stepCheckOut: 2, isStep1Done: true, isStep2Done: false });
    }
    // this.gotoAnchor('anchor-step1');

    // assign email into basket
    const { basket } = this.props;
    if (basket?.email !== email) {
      this.assignEmailToBasket(basket?.id, email);
    }
    return true;
  }

  checkContinuteStep2 = (status) => {
    const { isSelfCollect, isBillAddressSame, listError } = this.state;
    const {
      street1, postal_code: postalCode, country, pan_card: panCard,
    } = this.shipInfo;
    const fieldError = [];
    const isPanCardError = ['in', 'id'].includes(country) && !panCard;
    this.saveDataCheckOut();
    if (!isSelfCollect && (!street1 || !postalCode || isPanCardError)) {
      if (!street1) {
        fieldError.push('address');
        listError.push('street1');
      }
      if (!postalCode) {
        fieldError.push('zip code');
        listError.push('postal_code');
      }
      if (isPanCardError) {
        fieldError.push('pan_card');
        listError.push('pan_card');
      }
      toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
      this.setState({ stepCheckOut: 2, isStep2Done: false, listError });
      return false;
    }
    if (!isBillAddressSame) {
      const {
        street1: billStreet1, postal_code: billPostalCode, country: billCountry, pan_card: billPanCard,
      } = this.billingInfo;
      const isBillPanCardError = ['in', 'id'].includes(billCountry) && !billPanCard;
      if (!billStreet1 || !billPostalCode || isBillPanCardError) {
        if (!billStreet1) {
          fieldError.push('address');
          listError.push('billStreet1');
        }
        if (!billPostalCode) {
          fieldError.push('zip code');
          listError.push('billPostalCode');
        }
        if (isBillPanCardError) {
          fieldError.push('pan_card');
          listError.push('billPanCard');
        }
        toastrError(`Please enter your billing ${_.join(fieldError, ', ')}`);
        this.setState({ stepCheckOut: 2, isStep2Done: false, listError });
        return false;
      }
    }
    if (status !== 'payment') {
      this.setState({ stepCheckOut: 3, isStep2Done: true, isStep3Done: false });
    }
    // setTimeout(() => {
    //   this.gotoAnchor('anchor-step3');
    // }, 300);

    return true;
  }

  checkContinuteStep3 = (status) => {
    const {
      items,
    } = this.props.basket;
    const { giftFrees } = this.state;
    const isOnlyGift = this.isAllGift(items);
    const isHasFreeGift = giftFrees && giftFrees.length > 0;
    if (status !== 'payment') {
      this.setState({ stepCheckOut: isOnlyGift ? 3 : isHasFreeGift ? 4 : 3, isStep3Done: true });
    }
    // setTimeout(() => {
    //   this.gotoAnchor(isHasFreeGift ? 'anchor-step4' : 'anchor-step3');
    // }, 300);
  }

  prevStepCheckOut = (step) => {
    this.setState({ stepCheckOut: step });
  }
  // checkContinuteStep3 = () => {
  //   const { isBillAddressSame } = this.state;
  //   const { billStreet1, billPostalCode, billPhone } = this.shipInfo;
  //   if (!isBillAddressSame && (!billStreet1 || !billPostalCode || !billPhone)) {
  //     toastrError('Please enter all the information');
  //     this.setState({ stepCheckOut: 3, isStep3Done: false });
  //     return false;
  //   }
  //   this.setState({ stepCheckOut: 4, isStep3Done: true });
  //   return true;
  // }

  isShowPackagingOption = (basket) => {
    const { items } = basket;
    let flags = false;
    _.forEach(items, (d) => {
      const { item } = d;
      if (item) {
        const type = item.product.type.name;
        const countrySupport = ['sg', 'au'];
        if ((item.is_sample === false) && (type === 'Perfume' || type === 'Creation' || type === 'Scent' || type === 'Elixir') && (this.shipInfo.country ? countrySupport.includes(this.shipInfo.country.toLowerCase()) : false)) {
          flags = true;
        }
      }
    });
    return flags;
  }

  selectPackagingOption = (id) => {
    this.packaging = id;
  };

  selectGiftWrap = (value) => {
    this.giftWrap = value;
    this.preCheckOut();
  }

  openShowAddress = () => {
    this.setState({ isShowChoiseAddress: true });
  }

  closeShowAddress = () => {
    this.setState({ isShowChoiseAddress: false });
  }

  onChooseNewAddress = (flag) => {
    if (flag) {
      this.initalData();
    } else {
      this.initalData(this.currentAddress);
    }
  }

  onChangeAddress = (address) => {
    this.currentAddress = address;
    this.initalData(address);
  }

  generateListStep = (isOnlyGift, isHasFreeGift, cmsCommon, buttonBlock) => {
    const customerBt = getNameFromCommon(cmsCommon, 'CUSTOMER');
    const shippingBt = getNameFromCommon(cmsCommon, 'Shipping');
    const promotionBt = getNameFromButtonBlock(buttonBlock, 'PROMOTION');
    const paymentBt = getNameFromCommon(cmsCommon, 'PAYMENT');
    const listStep = [
      {
        name: customerBt,
        step: 1,
      },
    ];
    if (!isOnlyGift) {
      listStep.push({
        name: shippingBt,
        step: 2,
      });
    }
    if (isHasFreeGift && !isOnlyGift) {
      listStep.push({
        name: promotionBt,
        step: 3,
      });
    }
    listStep.push({
      name: paymentBt,
      step: isOnlyGift ? 2 : isHasFreeGift ? 4 : 3,
    });
    return listStep;
  }

  render() {
    const { basket, login, addProductGiftBasket } = this.props;
    const {
      payMethod, noteText, isSelfCollect, timeDelivery, isExpress, bankCode,
      stepCheckOut, isStep1Done, isStep2Done, isStep3Done, isStep4Done, isBillAddressSame, seo,
      isShowCreateAccount, listError, isOpenNote, isOPenPromo, textBlock, imageBlockIcon, buttonBlock,
      iconBlock, blogBlock, isShowChoiseAddress, listAddress, isShowOptionAddress, giftFrees, isPromoError,
      promotion, promotionCode, iconBlockTop, feedbackBlock, iconBlockScent,
    } = this.state;
    if (!basket) {
      return (<div />);
    }
    const {
      id, total, shipping, subtotal, items, discount,
    } = basket;
    const newPrice = parseFloat(discount) > 0 ? parseFloat(subtotal) - parseFloat(discount) : -1;
    const finalPrice = newPrice === -1 ? parseFloat(subtotal) : newPrice;
    const giftFreeProduct = _.filter(items, x => x.is_free_gift);

    const isOnlyGift = this.isAllGift(items);
    const listProduct = basket && basket.items ? basket.items : [];
    const totalProduct = basket && basket.items ? basket.items.length : 0;

    const cmsCommon = getCmsCommon(this.props.cms);
    const subtotalBt = getNameFromCommon(cmsCommon, 'Subtotal');
    const shippingBt = getNameFromCommon(cmsCommon, 'Shipping');
    const discountBt = getNameFromCommon(cmsCommon, 'Discount');
    const promoGiftBt = getNameFromCommon(cmsCommon, 'Promo/Gift_Certificate');
    const applyBt = getNameFromCommon(cmsCommon, 'APPLY');
    const totalBt = getNameFromCommon(cmsCommon, 'TOTAL');
    const orderBt = getNameFromCommon(cmsCommon, 'ORDER_SUMMARY');
    const checkOutBt = getNameFromCommon(cmsCommon, 'CHECKOUT');
    const addNoteBt = getNameFromCommon(cmsCommon, 'ADD_A_NOTE');
    const placeBt = getNameFromCommon(cmsCommon, 'PLACE_ORDER');
    const showBt = getNameFromCommon(cmsCommon, 'Show Details');

    const freeShipping = getNameFromButtonBlock(buttonBlock, 'Free Shipping');
    const moreTo = getNameFromButtonBlock(buttonBlock, 'more to qualify');

    const isShowPackagingOption = this.isShowPackagingOption(basket);
    const isLogined = login && login.user && login.user.id;
    if (!isShowPackagingOption) {
      this.packaging = undefined;
    }
    const isHasFreeGift = giftFrees && giftFrees.length > 0;
    const shippingSelection = _.find(this.listShipping, x => (this.shipInfo && this.shipInfo.country ? this.shipInfo.country.toUpperCase() : '') === x.country.code.toUpperCase());
    const offsetShipping = shippingSelection && parseFloat(shipping) > 0 ? parseFloat(shippingSelection?.threshold) - parseFloat(subtotal) : -1;

    const htmlItems = (
      <div className="div-col div-infor">
        <div className="div-customer div-col">

          <StepInfo
            listStep={this.generateListStep(isOnlyGift, isHasFreeGift, cmsCommon, buttonBlock)}
            stepCheckOut={stepCheckOut}
            prevStepCheckOut={this.prevStepCheckOut}
          />
          {
            stepCheckOut === 1 && (
              <CustomerItemV2
                step={1}
                buttonBlock={buttonBlock}
                stepCheckOut={stepCheckOut}
                isStepDone={isStep1Done}
                onClickHeader={() => this.setState({ stepCheckOut: 1, isStep1Done: false })}
                shipInfo={this.shipInfo}
                onChange={this.onChange}
                onClickContinute={this.checkContinuteStep1}
                login={login}
                cms={this.props.cms}
                listError={listError}
                onChangeGenderSelect={this.onChangeGenderSelect}
                isOnlyGift={isOnlyGift}
                isHasFreeGift={isHasFreeGift}
                basket={basket}
                promotion={promotion}
                createAddressFromApplePay={this.createAddressFromApplePay}
                listShipping={this.listShipping}
                preCheckOutApplePay={this.preCheckOutApplePay}
              />
            )
          }

          {
            stepCheckOut === 2 && !isOnlyGift ? (
              <ShippingItemV2
                step={2}
                login={login}
                isLogined={isLogined}
                currentAddress={this.currentAddress}
                openShowAddress={this.openShowAddress}
                closeShowAddress={this.closeShowAddress}
                onChooseNewAddress={this.onChooseNewAddress}
                onChangeAddress={this.onChangeAddress}
                listAddress={listAddress}
                isShowOptionAddress={isShowOptionAddress}
                buttonBlock={buttonBlock}
                stepCheckOut={stepCheckOut}
                isStepDone={isStep2Done}
                isExpress={isExpress}
                isSelfCollect={isSelfCollect}
                // onClickHeader={() => { this.setState({ stepCheckOut: 2 }); this.checkEmailExist(); }}
                onClickHeader={() => this.setState({ stepCheckOut: 2, isStep2Done: false })}
                onClickSelfCollect={() => this.setState({ isSelfCollect: true, isExpress: false, isBillAddressSame: false }, () => { this.preCheckOut(); })}
                onClickStandard={() => this.setState({ isSelfCollect: false, isExpress: false }, () => { this.preCheckOut(); })}
                onClickExpress={() => this.setState({ isSelfCollect: false, isExpress: true }, () => { this.preCheckOut(); })}
                shipInfo={this.shipInfo}
                billingInfo={this.billingInfo}
                onChange={this.onChange}
                onChangeBillingInfo={this.onChangeBillingInfo}
                onClickContinute={this.checkContinuteStep2}
                listShipping={this.listShipping}
                optionTime={this.optionTime}
                onChangeTimeDelivery={this.onChangeTimeDelivery}
                isBillAddressSame={isBillAddressSame}
                onClickCheckBillAdress={e => this.setState({ isBillAddressSame: e })}
                shipping={shipping}
                timeDelivery={timeDelivery}
                cms={this.props.cms}
                isShowPackagingOption={isShowPackagingOption}
                selectPackagingOption={this.selectPackagingOption}
                isShowCreateAccount={isShowCreateAccount}
                onClickCreateAccount={this.onClickCreateAccount}
                onClickCreateAddress={this.onClickCreateAddress}
                listError={listError}
                basket={basket}
                selectGiftWrap={this.selectGiftWrap}
                listExpressTimeslot={listExpressTimeslot}
                expressTimeslot={this.state.expressTimeslot}
                onChangeExpressTimeSlot={this.onChangeExpressTimeSlot}
                isHasFreeGift={isHasFreeGift}
                prevStepCheckOut={this.prevStepCheckOut}
              />
            ) : (<div />)
          }
          {
            stepCheckOut === 3 && isHasFreeGift && !isOnlyGift && (
              <PromotionGiftV2
                step={3}
                stepCheckOut={stepCheckOut}
                isStepDone={isStep3Done}
                onClickHeader={() => this.setState({ stepCheckOut: isOnlyGift ? 2 : 3, isStep3Done: false })}
                giftFrees={giftFrees}
                finalPrice={finalPrice}
                giftFreeProduct={giftFreeProduct}
                basket={basket}
                buttonBlockCard={buttonBlock}
                gotoAnchor={this.gotoAnchor}
                addProductGiftBasket={addProductGiftBasket}
                cms={this.props.cms}
                onClickContinute={this.checkContinuteStep3}
                prevStepCheckOut={this.prevStepCheckOut}
              />
            )
          }
          {
            stepCheckOut === (isOnlyGift ? 2 : isHasFreeGift ? 4 : 3) && (
              <PaymentItemV2
                step={isOnlyGift ? 2 : isHasFreeGift ? 4 : 3}
                stepCheckOut={stepCheckOut}
                isStepDone={isStep4Done}
                onClickHeader={() => this.setState({ stepCheckOut: (isOnlyGift ? 2 : isHasFreeGift ? 4 : 3), isStep4Done: false })}
                changeNote={this.changeNote}
                onCheckOutFunc={this.onCheckOutFunc}
                onClickContinute={this.onCheckOutFunc}
                noteText={noteText}
                payMethod={payMethod}
                onClickStripe={() => { this.setState({ payMethod: 'stripe' }); }}
                onClickPaypal={() => { this.setState({ payMethod: 'paypal' }); }}
                onClickPaypalLate={() => { this.setState({ payMethod: 'stripe_pay_later' }); }}
                onClickZaloPay={() => { this.setState({ payMethod: 'zalopay', bankCode: 'zalopayapp' }); }}
                onClickMomo={() => { this.setState({ payMethod: 'momo', bankCode: 'payUrl' }); }}
                onClickBankTransfer={() => { this.setState({ payMethod: 'bank_transfer', bankCode: '' }); }}
                onClickAtmCard={() => { this.setState({ payMethod: 'atm_card', bankCode: 'ATM' }); }}
                onClickIntCard={() => { this.setState({ payMethod: 'int_card', bankCode: 'CC' }); }}
                onClickCod={() => { this.setState({ payMethod: 'cod', bankCode: '' }); }}
                cms={this.props.cms}
                isOnlyGift={isOnlyGift}
                textBlock={textBlock}
                shipInfo={this.shipInfo}
                imageBlockIcon={imageBlockIcon}
                iconBlock={iconBlock}
                isHasFreeGift={giftFrees && giftFrees.length > 0}
                buttonBlock={buttonBlock}
                prevStepCheckOut={this.prevStepCheckOut}
                basket={basket}
              />
            )
          }
          {
            stepCheckOut !== (isOnlyGift ? 2 : isHasFreeGift ? 4 : 3) && (
              <React.Fragment>
                <CustomerSay data={feedbackBlock} />
                <AllCleanScentV2 data={iconBlockScent} buttonBlock={buttonBlock} />
              </React.Fragment>
            )
          }
        </div>
      </div>
    );

    const summaryCheckOut = (
      <div className="div-col div-summary">
        <div className="div-row justify-between items-center mt-3">
          <span>
            {subtotalBt}
          </span>
          {
            isBrowser ? (
              <span className="tx-price">
                {generaCurrency(newPrice === -1 ? subtotal : newPrice, true)}
              </span>
            ) : (
              newPrice === -1
                ? (
                  <span className="tx-price">
                    {generaCurrency(subtotal, true)}
                  </span>
                ) : (
                  <span>
                    <span className="tx-price strike">
                      {generaCurrency(subtotal, true)}
                    </span>
                    <span className="tx-price ml-2">
                      {generaCurrency(newPrice, true)}
                    </span>
                  </span>
                )
            )
          }
        </div>
        <div className="div-row justify-between items-center mt-2 mb-2">
          <span>
            {shippingBt}
          </span>
          <span className="tx-price">
            {generaCurrency(shipping, true)}
          </span>
        </div>
        {
          offsetShipping > 0 && (
            <div className="div-row justify-between items-center mt-2 mb-2 free-shipping">
              <span>
                {freeShipping}
              </span>
              <span className="tx-price text-yellow">
                {`${generaCurrency(offsetShipping)} ${moreTo}`}
              </span>
            </div>
          )
        }
        <div className="div-row justify-between items-center">
          <span>
            {discountBt}
          </span>
          <span className="tx-price">
            {generaCurrency(discount, true)}
          </span>
        </div>
        <hr style={{
          width: '100%', color: '#E2E2E2', margin: '0px', marginTop: '16px',
        }}
        />
        <div className={`div-expand ${isOpenNote ? 'open-expand' : 'close-expand'}`}>
          <div
            className="div-header-expand"
            onClick={() => {
              this.setState({ isOpenNote: !isOpenNote });
            }
            }
          >
            <span>
              {addNoteBt}
            </span>
            <button
              type="button"
              onClick={() => {
                this.setState({ isOpenNote: !isOpenNote });
              }
            }
            >
              <img src={isOpenNote ? icSub : icPlus} alt="icon" />
            </button>
          </div>
          <div className="div-content-expand">
            <Input
              type="textarea"
              className="border-checkout mb-3"
              style={{ height: '4rem' }}
              value={noteText}
              onChange={this.changeNote}
              onClick={() => trackGTMCheckOut(5, basket)}
            />
          </div>
        </div>
        <hr style={{ width: '100%', color: '#E2E2E2', margin: '0px' }} />
        <div className={`div-expand ${isOPenPromo ? 'open-expand' : 'close-expand'}`}>
          <div className={classnames('div-header-expand', isOPenPromo ? 'hidden' : '')} onClick={() => (!_.isEmpty(this.state.promotion) ? {} : this.setState({ isOPenPromo: !isOPenPromo }))}>
            {
              !_.isEmpty(this.state.promotion) ? (
                <div className="div-promo-apply">
                  <span>{this.state.promotionCode}</span>
                  <button type="button" className="button-bg__none" onClick={this.removePromotion}>
                    <img src={icRemove} alt="remove" />
                  </button>
                </div>
              ) : (
                <React.Fragment>
                  <span>
                    {promoGiftBt}
                  </span>
                  <button
                    type="button"
                    onClick={() => this.setState({ isOPenPromo: !isOPenPromo })}
                  >
                    <img src={isOPenPromo ? icSub : icPlus} alt="icon" />
                  </button>
                </React.Fragment>

              )
            }
          </div>
          <div id="id-promotion" className="div-content-expand">
            <div className="div-row mb-3 mt-3 div-promo">
              <div className="group">
                <input
                  className={classnames('input-material input_underline border-checkout', promotionCode ? 'input-text-bottom' : '', isPromoError ? 'error' : '', !_.isEmpty(promotion) && !isPromoError ? 'success' : '')}
                  type="text"
                  value={this.state.promotionCode}
                  onChange={this.onChangePromo}
                  required
                />
                <label className={classnames('label-material', promotionCode ? 'has-text' : '')}>
                  {promoGiftBt}
                </label>
                <img src={isPromoError ? icError : icSuccess} className={classnames(isPromoError || !_.isEmpty(promotion) && !isPromoError ? 'icon-show' : 'hidden')} alt="icon" />
              </div>
              <button
                type="button"
                disabled={!this.state.promotionCode}
                className={`${isMobile && !isTablet ? 'w-30 border-checkout' : ''} ${!this.state.promotionCode ? 'disabled' : ''}`}
                onClick={this.onCheckPromo}
              >
                {applyBt}
              </button>
            </div>
          </div>
        </div>

        {/* <div className="div-col mt-3">
          <span>
            {promoGiftBt}
          </span>
          <div className="div-row">
            <input
              type="text"
              className={`${isMobile && !isTablet ? 'w-70 border-checkout' : 'w-80 border-checkout'}`}
              onChange={this.onChangePromo}
            />
            <button
              type="button"
              className={`${isMobile && !isTablet ? 'w-30 border-checkout' : 'w-30 border-checkout'}`}
              style={{ marginLeft: '40px' }}
              onClick={this.onCheckPromo}
            >
              {applyBt}
            </button>
          </div>
        </div> */}
        <div style={{ width: '100%', height: '1px', background: '#E2E2E2' }} />
        <div className="div-total div-row justify-between">
          <span>
            {totalBt}
          </span>

          <div className="div-row div-right">
            {
              isBrowser && parseFloat(discount) > 0 && (
                <React.Fragment>
                  <div className="coupon__tag">
                    {/* <img src={icSaleOff} alt="icon" /> */}
                    <span>{generaCurrency(discount, true)}</span>
                  </div>
                  <span className="tx-price strike ml-2">
                    {generaCurrency((parseFloat(total) + parseFloat(discount)), true)}
                  </span>
                </React.Fragment>
              )
            }
            <span className="ml-2 total-price">
              {generaCurrency(total, true)}
            </span>
          </div>
        </div>
        <ReceiveGrams
          shippingValue={shipping}
          countryName={shippingSelection?.country?.name}
          buttonBlocks={buttonBlock}
          price={total}
        />
        {
          !isBrowser && (
            <React.Fragment>
              <div className="line-black" style={{ width: '100%', height: '1px', background: '#0D0D0D' }} />
              <button
                type="button"
                className="bt-checkout mt-3"
                onClick={this.onCheckOutFunc}
              >
                {placeBt}
              </button>
            </React.Fragment>
          )
        }
      </div>
    );
    const htmlInformation = (
      <div
        className="div-order div-col"
      >
        {
          isBrowser && (
            <div
              className="div-header-order div-row"
            >
              <span>{orderBt}</span>
            </div>
          )
        }

        <BuyMoreGiftShipping
          buttonBlock={buttonBlock}
          isShowBottle
          totalPerfumeExlixir={subtotal}
          giftFrees={giftFrees}
          shippingSelection={shippingSelection}
          giftFreeProduct={giftFreeProduct || []}
        />

        <div className="scroll-item">
          {_.map(listProduct, d => (
            <CardItem
                // isCheckOut
              isB2C
              idCart={id}
              data={d}
              updateProductBasket={this.props.updateProductBasket}
              deleteProductBasket={this.props.deleteProductBasket}
              // isCheckOut={!isMobile}
              cms={this.props.cms}
              style={{ borderBottomColor: '#E2E2E2' }}
            />
          ))}
        </div>
        {isMobile || isTablet ? <div /> : summaryCheckOut}
      </div>
    );
    const cardTotalItem = (
      <div className="footer-checkout-mobile card-total-item" onClick={() => this.setState({ isOpenListItemMobile: true })}>
        <div className="div-content">
          <div className="left-card">
            <div className="div-image">
              <div className="img-detail">
                {
                  _.map(listProduct, (d, index) => (
                    index === listProduct.length - 1
                      ? (
                        <img
                          src={d.image_display}
                          alt="icon"
                          style={{ top: `-${1 * 2}px`, left: `-${1 * 2}px` }}
                        />
                      ) : (<div className="img-temp" />)
                  ))
                }
              </div>
            </div>
            <div className="title-item-card">
              <h2>{`${listProduct.length} ITEMS`}</h2>
              <span>{showBt}</span>
            </div>
          </div>
          <h3>{generaCurrency(total, true)}</h3>
        </div>

      </div>

    );

    return (
      <div className="div-col justify-center div-checkout" style={isTablet ? { marginLeft: '40px', marginRight: '40px' } : {}}>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/checkout')}
        </MetaTags>
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}

        <IconBlockCheckOut data={iconBlockTop} />

        <div className="div-body">
          {
            isMobile || isTablet ? (
              <React.Fragment>
                {htmlItems}
                {stepCheckOut === (isOnlyGift ? 3 : isHasFreeGift ? 4 : 3) ? summaryCheckOut : undefined}
                {cardTotalItem}
              </React.Fragment>
            ) : (
              <React.Fragment>
                {htmlItems}
                {htmlInformation}
              </React.Fragment>

            )
          }
        </div>
        {/* <AllCleanScent blogBlock={blogBlock} /> */}
        {
          isShowChoiseAddress ? (
            <PopUpSaveAdsress
              closeShowAddress={this.closeShowAddress}
              listAddress={listAddress}
              currentAddress={this.currentAddress}
              onChangeAddress={this.onChangeAddress}
              buttonBlock={buttonBlock}
            />
          ) : (null)
        }

        <ModalCheckoutMobile isOpen={this.state.isOpenListItemMobile}>
          <div className="body-item-mobile" style={isMobile ? { height: `${window.innerHeight}px` } : {}}>
            <div className="div-header-modal">
              <h2>{orderBt}</h2>
              <button className="button-bg__none" type="button" onClick={() => this.setState({ isOpenListItemMobile: false })}>
                <img src={icClose} alt="icon" />
              </button>
            </div>
            {htmlInformation}
            {summaryCheckOut}
          </div>
        </ModalCheckoutMobile>

        {/* {
          isMobile && (
          <FooterCheckoutMobile
            onChangeTex={this.onChangePromo}
            valuePromo={this.state.promotionCode}
            isApplied={false}
            clearTextPromo={() => this.setState({ promotionCode: '' })}
            onCheckPromo={this.onCheckPromo}
            removePromotion={this.removePromotion}
            isPromoErrorMobile={this.state.isPromoErrorMobile}
            promotion={this.state.promotion}
            newPrice={newPrice}
            subtotal={subtotal}
            onCheckOutFunc={this.onCheckOutFunc}
            buttonBlock={buttonBlock}
            isShowFooter={this.state.isShowFooter}
          />
          )
        } */}

        <div id="div-button" className="hidden" />
      </div>
    );
  }
}

CheckOutB2CPage.propTypes = {
  login: PropTypes.shape(PropTypes.object).isRequired,
  basket: PropTypes.shape(PropTypes.object).isRequired,
  updateProductBasket: PropTypes.func.isRequired,
  deleteProductBasket: PropTypes.func.isRequired,
  updateProductArrayBasket: PropTypes.func.isRequired,
  refPoint: PropTypes.string.isRequired,
  clearBasket: PropTypes.func.isRequired,
  clearProductBasket: PropTypes.func.isRequired,
  preCheckOutBasket: PropTypes.func.isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  cms: PropTypes.shape().isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  loadingPage: PropTypes.func.isRequired,
  addProductGiftBasket: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    basket: state.basket,
    refPoint: state.refPoint,
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  deleteProductBasket,
  updateProductBasket,
  updateProductArrayBasket,
  clearBasket,
  clearProductBasket,
  preCheckOutBasket,
  createBasketGuest,
  addCmsRedux,
  loadingPage,
  addProductGiftBasket,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CheckOutB2CPage));
