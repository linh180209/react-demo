import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';
import { isMobile } from '../../../DetectScreen';

function AllCleanScent(props) {
  const { blogBlock } = props;
  return (
    <div className="div-all-clean-scent">
      <h3>
        {blogBlock ? blogBlock.button.name : ''}
      </h3>
      <div className="div-list-item">
        {
        _.map(blogBlock ? blogBlock.blogs : [], x => (
          <div className="div-item-all-clean-scent">
            <img src={x.value.image.image} alt="icon" />
            <h4>
              {x.value.image.caption}
            </h4>
            <span className={isMobile ? 'hidden' : ''}>
              {ReactHtmlParser(x.value.description)}
            </span>
          </div>
        ))
        }
      </div>
    </div>
  );
}

export default AllCleanScent;
