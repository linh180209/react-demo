import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect } from 'react';
import icDone from '../../../image/icon/ic-done.svg';
import icNext from '../../../image/icon/ic-next-checkout.svg';
import { scrollTop } from '../../../Redux/Helpers';
import './styles.scss';

function StepInfo(props) {
  // scroll to top when change stepCheckOut
  useEffect(() => {
    scrollTop();
  }, [props.stepCheckOut]);

  const stepRender = (d, index) => (
    <div
      onClick={() => (props.stepCheckOut > index ? props.prevStepCheckOut(d.step) : () => {})}
      className={classnames('step-item div-row', props.stepCheckOut > index ? 'click-able' : '')}
    >
      {index !== 1 && <img className="ic-next" src={icNext} alt="next" />}
      {props.stepCheckOut > index && <img className="ic-done" src={icDone} alt="done" />}
      <span className={props.stepCheckOut === index ? 'active' : ''}>{d.name}</span>
    </div>
  );

  return (
    <div className="step-info div-row">
      {
        _.map(props.listStep, (d, index) => (
          stepRender(d, index + 1)
        ))
      }
    </div>
  );
}

export default StepInfo;
