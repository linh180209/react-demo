import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import GiftFree from '../../components/HomePage/giftFree';
import HeaderItem from './headerItems';
import {
  getCmsCommon, getNameFromButtonBlock, getNameFromCommon, trackGTMCheckOut,
} from '../../Redux/Helpers';
import ButtonCT from '../../components/ButtonCT';
import icNext from '../../image/icon/ic-next.svg';

function PromotionGift(props) {
  useEffect(() => {
    trackGTMCheckOut(4, props.basket);
  }, []);

  const cmsCommon = getCmsCommon(props.cms);
  const continueBt = getNameFromCommon(cmsCommon, 'CONTINUE');
  const freeGiftBt = getNameFromButtonBlock(props.buttonBlockCard, 'Free_gifts_selected');
  const htmlBottom = (
    <div className="h-100 div-col justify-center ml-5">
      <span className="tx-more-infor">
        {props.giftFreeProduct.length}
        /
        {props.giftFrees.length}
        {' '}
        {freeGiftBt}
      </span>
    </div>
  );
  return (
    <React.Fragment>
      <div id="anchor-step3" />
      <HeaderItem
        cmsCommon={cmsCommon}
        stepCheckOut={props.stepCheckOut}
        isStepDone={props.isStepDone}
        step={props.step}
        title={getNameFromButtonBlock(props.buttonBlockCard, 'PROMOTION')}
        onClick={props.onClickHeader}
        htmlBottom={props.isStepDone ? htmlBottom : undefined}
      />
      {
        props.stepCheckOut === props.step && !props.isStepDone
        && (
          <div className="body-step">
            <div className="div-promotion-gift">
              <GiftFree
                amount={props.giftFrees && (props.giftFrees.length > 0) ? props.giftFrees[0].amount : 0}
                giftFrees={props.giftFrees}
                price={props.finalPrice}
                addProductGiftBasket={props.addProductGiftBasket}
                basket={props.basket}
                giftFreeProduct={props.giftFreeProduct}
                buttonBlocks={props.buttonBlockCard}
                isCollapse
                isShow
              />
            </div>
            <div className="div-row justify-end">
              <ButtonCT
                name={continueBt}
                className="bt-next"
                iconRight={icNext}
                onClick={props.onClickContinute}
              />
            </div>
          </div>
        )
      }
    </React.Fragment>
  );
}

PromotionGift.propTypes = {
  stepCheckOut: PropTypes.number,
  isStepDone: PropTypes.bool,
  giftFrees: PropTypes.shape({}).isRequired,
  basket: PropTypes.shape({}).isRequired,
  giftFreeProduct: PropTypes.shape({}).isRequired,
  buttonBlockCard: PropTypes.arrayOf().isRequired,
  finalPrice: PropTypes.number.isRequired,
  addProductGiftBasket: PropTypes.func.isRequired,
};

export default PromotionGift;
