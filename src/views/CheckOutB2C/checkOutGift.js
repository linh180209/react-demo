/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import {
  Col, Row, Input,
} from 'reactstrap';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as EmailValidator from 'email-validator';
import MetaTags from 'react-meta-tags';
import smoothscroll from 'smoothscroll-polyfill';
import moment from 'moment';
import HeaderHomePage from '../../components/HomePage/header';
import fetchClient, { fetchClientFormData } from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import {
  CHECK_PROMOTION_CODE, GET_ADDRESS_URL, CREATE_ADDRESS_URL, CHECKOUT_FUNCTION_URL,
  UPDATE_ADDRESS_GUEST_URL, PAYPAL_GENERATE_URL, STRIPE_GENERATE_URL, GET_SHIPPING_URL,
  CHECK_EMAIL_LOGIN_URL, PUT_OUTCOME_URL, GET_SHIPPING_EXPRESS_URL, GET_BASKET_URL,
  GUEST_CREATE_BASKET_URL, ADD_BASKET_URL, PRECHECKOUT_BASKET_URL, EDIT_DELETE_BASKET_URL,
  GET_LIST_ADDRESS_URL,
  CHECK_PROMOTION_CODE_V2,
} from '../../config';
import {
  getNameFromCommon, getCmsCommon, fetchCMSHomepage, getSEOFromCms, generaCurrency, isCheckNull, hiddenChatIcon, visibilityChatIcon, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady, trackGTMCheckOut, gotoShopHome,
} from '../../Redux/Helpers';
import '../../styles/checkout.scss';
import CardItem from '../../components/B2B/CardItem';
import CustomerItem from './customerItem';
import ShippingItem from './shippingItems';
import BillItems from './billItems';
import PaymentItem from './paymentItem';
import auth from '../../Redux/Helpers/auth';
import addCmsRedux from '../../Redux/Actions/cms';
import icSub from '../../image/icon/ic-sub.svg';
import icPlus from '../../image/icon/ic-plus.svg';
import loadingPage from '../../Redux/Actions/loading';
import AllCleanScent from './AllCleanScent';
import iconCheck from '../../image/icon/ic-check-green.svg';
import { generateBodyItemCart } from '../../Redux/Sagas/basket';
import PopUpSaveAdsress from './popUpSaveAddress';
import FooterCheckoutMobile from './footerCheckOutMobile';
import { isMobile, isTablet } from '../../DetectScreen';
import FooterV2 from '../../views2/footer';
import { addScriptStripe } from '../../Utils/addStripe';
import { addScriptGoogleAPI } from '../../Utils/addScriptGoogle';

const getCms = (cms) => {
  if (!cms) {
    return { seo: {} };
  }
  const seo = getSEOFromCms(cms);
  const { body } = cms;
  const textBlock = _.filter(body, x => x.type === 'text_block');
  const buttonBlock = _.filter(body, x => x.type === 'button_block');
  const imageBlockIcon = _.filter(body, x => x.type === 'image_block');
  const iconBlock = _.find(body, x => x.type === 'icons_block').value;
  const blogBlock = _.find(body, x => x.type === 'blogs_block').value;
  return {
    seo, textBlock, imageBlockIcon, buttonBlock, iconBlock, blogBlock,
  };
};

const addressDefaul = {
  street1: undefined,
  city: undefined,
  state: undefined,
  country: undefined,
  postal_code: undefined,
  pan_card: undefined,
};

class CheckOutGift extends Component {
  constructor(props) {
    super(props);
    const { codes, item, customeBottle } = this.props.location.state ? this.props.location.state : {};

    this.state = {
      promotionCode: '',
      noteText: '',
      promotion: {},
      payMethod: 'stripe',
      stepCheckOut: 1,
      isStep1Done: false,
      isStep2Done: false,
      isStep3Done: false,
      isStep4Done: false,
      isSelfCollect: false,
      isExpress: false,
      timeDelivery: 'Any Time',
      isBillAddressSame: true,
      seo: {},
      textBlock: [],
      imageBlockIcon: [],
      iconBlock: undefined,
      blogBlock: undefined,
      isShowCreateAccount: false,
      listError: [],
      buttonBlock: [],
      isOpenNote: false,
      isOPenPromo: false,
      redeemCode: codes,
      item,
      customeBottle,
      isShowChoiseAddress: false,
      listAddress: [],
      isShowOptionAddress: false,
      isShowFooter: true,
    };
    this.shipping = 0;
    this.shipInfo = {};
    this.billingInfo = _.cloneDeep(addressDefaul);
    this.timeoutShipInfo = undefined;
    this.checkOutData = undefined;
    this.isCheckOutClicked = false;
    this.optionTime = [
      'Any Time',
      '9.00 am to 12.00 pm',
      '12.00 pm to 3.00 pm',
      '3.00 pm to 6.00 pm',
      '6.00 pm to 10.00 pm',
    ];
    this.currentAddress = {};
  }

  componentDidMount() {
    setPrerenderReady();
    const { redeemCode, item } = this.state;
    if (!redeemCode || !item) {
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
      return;
    }
    smoothscroll.polyfill();
    addScriptStripe();
    addScriptGoogleAPI();

    if (isMobile) {
      document.addEventListener('scroll', this.trackScrolling);
      hiddenChatIcon();
    }
    // document.body.appendChild(script);
    this.fetchCmsCheckOut();
    this.fetchShippingInfo();
    this.generateCardGift();
  }

  componentWillUnmount() {
    removeLinkHreflang();
    if (isMobile) {
      document.removeEventListener('scroll', this.trackScrolling);
      visibilityChatIcon();
    }
  }

  isBottom = el => (
    el.getBoundingClientRect().bottom <= window.innerHeight
  )

  trackScrolling = () => {
    const wrappedElement = document.getElementById('id-promotion');
    if (this.isBottom(wrappedElement) && this.state.isShowFooter) {
      this.setState({ isShowFooter: false });
    } else if (!this.isBottom(wrappedElement) && !this.state.isShowFooter) {
      this.setState({ isShowFooter: true });
    }
  };

  componentDidUpdate = (prevProps) => {
    const { login } = this.props;
    if (login !== prevProps.login) {
      if (login && login.user && login.user.id) {
        this.shipInfo.name = `${login.user.first_name} ${login.user.last_name}`;
        this.shipInfo.firstName = login.user.first_name;
        this.shipInfo.lastName = login.user.last_name;
        this.shipInfo.email = login.user.email;
        this.shipInfo.phone = login.user.phone;
        this.shipInfo.dob = login.user.date_of_birth ? moment(login.user.date_of_birth).format('MM/DD/YYYY') : '';
        this.shipInfo.gender = login.user.gender ? (login.user.gender || 'Male') : 'Male';
        this.forceUpdate();
      }
    }
  }

  generateCardGift = async () => {
    try {
      this.props.loadingPage(true);
      const { redeemCode, item, customeBottle } = this.state;
      const basketCreate = await this.createCardGift();
      const basket = await this.addCardGift(basketCreate.id, redeemCode, item, customeBottle);
      this.setState({ basket }, () => {
        this.onCheckPromo();
      });
      this.props.loadingPage(false);
    } catch (error) {
      console.log('error', error.message);
      toastrError(error.message);
      this.props.loadingPage(false);
    }
  }

  createCardGift = () => {
    const { user } = auth.getLogin() || {};
    const options = {
      url: GUEST_CREATE_BASKET_URL,
      method: 'POST',
      body: {
        user: !_.isEmpty(user) ? user.id : undefined,
        is_gift: true,
      },
    };
    return fetchClient(options);
  };

  addCardGift = async (idCart, redeemCode, item, customeBottle) => {
    const bottleImage = customeBottle.currentImg
      ? await fetch(customeBottle.currentImg).then(r => r.blob())
      : undefined;
    const data = {
      item: item.id,
      price: item.price,
      is_black: customeBottle.isBlack,
      color: customeBottle.color,
      font: customeBottle.font,
      imagePremade: customeBottle.imagePremade,
      is_display_name: (!bottleImage && !!customeBottle.nameBottle) || !!customeBottle.nameBottle,
      is_customized: !!bottleImage || !!customeBottle.imagePremade,
      file: bottleImage
        ? new File([bottleImage], 'product.png')
        : undefined,
      quantity: 1,
      name: customeBottle.nameBottle || item.name,
    };
    const dataItem = {
      idCart,
      item: data,
    };
    if (!idCart) {
      return null;
    }
    const url = ADD_BASKET_URL.replace('{cartPk}', idCart);
    if (!data.file) {
      const options = generateBodyItemCart(dataItem, url, 'POST');
      return fetchClient(options, true);
    }
    const options = generateBodyItemCart(dataItem, url, 'POST');
    return fetchClientFormData(options, true);
  }

  fetchCmsCheckOut = () => {
    const { cms } = this.props;
    const cmsCheckOut = _.find(cms, x => x.title === 'CheckOut');
    if (!cmsCheckOut) {
      fetchCMSHomepage('checkout').then((result) => {
        const cmsData = result;
        this.props.addCmsRedux(cmsData);
        const {
          seo, textBlock, imageBlockIcon, buttonBlock, iconBlock, blogBlock,
        } = getCms(cmsData);
        this.setState({
          seo, textBlock, imageBlockIcon, buttonBlock, iconBlock, blogBlock,
        });
      });
    } else {
      const {
        seo, textBlock, imageBlockIcon, buttonBlock, iconBlock, blogBlock,
      } = getCms(cmsCheckOut);
      this.setState({
        seo, textBlock, imageBlockIcon, buttonBlock, iconBlock, blogBlock,
      });
    }
  }

  onChangePromo = (e) => {
    const { value } = e.target;
    this.setState({ promotionCode: value });
  }

  onChangeGenderSelect = (value, name) => {
    this.shipInfo.gender = value;
    this.forceUpdate();
  }

  onChangeBillingInfo = (e) => {
    const { value, name } = e.target;
    _.assign(this.billingInfo, { [name]: value });
    this.forceUpdate();
  };

  onChange = (e) => {
    const { value, name } = e.target;
    this.shipInfo[name] = value;
    this.forceUpdate();
    this.setState({ listError: [] });
    if (name === 'firstName' || name === 'lastName') {
      this.shipInfo.name = `${this.shipInfo.firstName} ${this.shipInfo.lastName}`;
    }
    if (name === 'email') {
      if (this.timeOutEmail) {
        clearTimeout(this.timeOutEmail);
        this.timeOutEmail = null;
      }
      this.timeOutEmail = setTimeout(this.checkEmailExist, 2000);
    }
    if (name === 'country') {
      // handle if change country but this country don't support express
      const { isExpress } = this.state;
      if (isExpress) {
        const ele = _.find(this.listShipping, x => x.country.code.toLowerCase() === value.toLowerCase());
        if (ele && !ele.is_express) {
          this.setState({ isExpress: false }, () => {
            this.preCheckOut();
            this.forceUpdate();
          });
          return;
        }
      }
      this.preCheckOut();
      this.forceUpdate();
    }
  }

  onCheckPromo = () => {
    const { basket } = this.state;
    const { id } = basket;
    const cmsCommon = getCmsCommon(this.props.cms);
    const proCodeNotAvaiBt = getNameFromCommon(cmsCommon, 'Promotion_code_is_not_avaialble.');
    const proCodeAvaiBt = getNameFromCommon(cmsCommon, 'Promotion_code_is_avaialble.');

    if (!id) {
      toastrError(proCodeNotAvaiBt);
      return;
    }

    const { redeemCode } = this.state;
    const options = {
      url: `${CHECK_PROMOTION_CODE_V2.replace('{id}', id)}`,
      method: 'POST',
      body: {
        code: redeemCode,
      },
    };

    fetchClient(options, true).then((result) => {
      const { isError } = result;
      if (isError) {
        toastrError(proCodeNotAvaiBt);
        this.setState({ promotion: undefined });
      } else {
        this.setState({ promotion: result, promotionCode: result.code }, () => {
          this.preCheckOut();
        });
        // toastrSuccess(proCodeAvaiBt);
      }
    }).catch((err) => {
      console.log('error', err.message);
      toastrError(err.message);
    });
  }

  assignBasketToUser = (basketId, userId) => {
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basketId),
      method: 'PUT',
      body: {
        user: userId,
      },
    };
    return fetchClient(option);
  }

  onCheckOutFunc = async () => {
    if (this.isCheckOutClicked) {
      return;
    }
    const { basket } = this.state;
    trackGTMCheckOut(8, basket);

    const { login, loadingPage } = this.props;
    const isOnlyGift = this.isAllGift(basket.items);
    const isLogined = login && login.user && login.user.id;

    if (!basket || !basket.id) {
      toastrError('Checkout is failed');
      return;
    }

    // assign user to cart, if basket id === login cart
    if (isLogined && basket.id !== login.id) {
      this.assignBasketToUser(basket.id, login.id);
    }

    if (!isOnlyGift) {
      if (!this.checkContinuteStep1() || !this.checkContinuteStep2()) {
        return;
      }
    } else if (!this.checkContinuteStep1()) {
      return;
    }
    const {
      payMethod, noteText, isSelfCollect, isExpress, promotion,
    } = this.state;
    if (!this.shipInfo.idAddress) {
      const address = isLogined && this.shipInfo.create_address ? await this.createAddressWithUser(this.shipInfo) : await this.createAddress(this.shipInfo);
      if (!address) {
        toastrError('Check Out Error');
        return;
      }
      this.shipInfo.idAddress = address.id;
    }

    let idbillingAddress;
    if (this.state.isBillAddressSame) {
      const resultAddress = await this.createAddress(this.shipInfo);
      idbillingAddress = resultAddress?.id;
    } else {
      const resultAddress = await this.createAddress(_.assign(this.shipInfo, this.billingInfo));
      idbillingAddress = resultAddress?.id;
    }

    const options = {
      url: CHECKOUT_FUNCTION_URL.replace('{id}', basket.id),
      method: 'POST',
      body: {
        payment_method: payMethod,
        shipping_address: (isSelfCollect || isOnlyGift) ? undefined : this.shipInfo.idAddress,
        billing_address: idbillingAddress,
        note: noteText,
        shipping: this.shipping,
        is_self_collect: isSelfCollect,
        is_express: isExpress,
        personality_choice: this.idPersonalityChoice,
        // delivery_slot: timeDelivery,
        packaging: this.packaging,
        date_of_birth: this.shipInfo.dob ? moment(this.shipInfo.dob, 'MM/DD/YYYY').valueOf() / 1000 : undefined,
      },
    };
    if (promotion && promotion.id) {
      options.body.promos = [promotion.id];
    }
    if (this.props.refPoint !== '') {
      options.body.ref = this.props.refPoint;
    }
    if (!isLogined) {
      options.body.meta = {
        guest: {
          name: this.shipInfo.name,
          email: this.shipInfo.email,
          cart: basket.id,
          phone: this.shipInfo.phone,
          create_account: isOnlyGift ? false : this.shipInfo.create_account,
        },
      };
    }
    this.isCheckOutClicked = true;
    loadingPage(true);
    fetchClient(options, true).then((result) => {
      const { isError, order, message } = result;
      this.isCheckOutClicked = false;
      loadingPage(false);
      if (!isError) {
        if (order) {
          this.props.history.push(generateUrlWeb(`/pay/success/${order}`));
          return;
        }
        if (payMethod === 'paypal') {
          const htmlProcess = `${result.substring(0, 6)}id='id-pay'${result.substring(6, result.length)}`;
          const s = document.getElementById('div-button');
          s.innerHTML = htmlProcess;
          const idForm = document.getElementById('id-pay');
          idForm.submit();
        } else {
          this.redirectStripe(result);
        }
      } else {
        throw (message);
      }
    }).catch((err) => {
      console.log('error', err.message);
      toastrError(err);
      this.isCheckOutClicked = false;
      loadingPage(false);
    });
  }

  paypalGenerate = (order, total) => {
    const defaultHeaders = {
      'Content-Type': 'application/json',
    };
    const options = {
      method: 'POST',
      body: JSON.stringify({
        order,
        total,
      }),
      headers: defaultHeaders,
    };
    fetch(new Request(PAYPAL_GENERATE_URL, options))
      .then(result => result.text()).then((html) => {
        const htmlProcess = `${html.substring(0, 6)}id='id-pay'${html.substring(6, html.length)}`;
        const s = document.getElementById('div-button');
        s.innerHTML = htmlProcess;
        const idForm = document.getElementById('id-pay');
        idForm.submit();
        // this.setState({ formPay: htmlProcess, isShowFormPay: true });
      }).catch((err) => {
        console.log('err', err.message);
        toastrError(err);
      });
  }

  stripeGenerate = (order, total) => {
    const options = {
      url: STRIPE_GENERATE_URL,
      method: 'POST',
      body: {
        order,
        total,
      },
    };
    fetchClient(options)
      .then((result) => {
        // const s = document.createElement('div');
        // s.innerHTML = html;
        // this.instance.appendChild(s);
        this.redirectStripe(result);
        // this.setState({ formPay: result, isShowFormPay: true });
      }).catch((err) => {
        console.log('err', err.message);
        toastrError(err);
      });
  }

  redirectStripe = (formPay) => {
    const { key, session } = formPay;
    const stripe = window.Stripe(key);
    stripe.redirectToCheckout({
      // Make the id field from the Checkout Session creation API response
      // available to this file, so you can provide it as parameter here
      // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
      sessionId: session,
    }).then((result) => {
      console.log('result', result);
    });
  }

  initalData = (address) => {
    const { login } = this.props;
    const defaults = {
      // idAddress: address ? address.id : undefined,
      firstName: login && login.user ? `${login.user.first_name}` : '',
      lastName: login && login.user ? `${login.user.last_name}` : '',
      name: login && login.user ? `${login.user.first_name} ${login.user.last_name}` : '',
      dob: login && login.user && login.user.date_of_birth ? moment(login.user.date_of_birth).format('MM/DD/YYYY') : '',
      gender: login && login.user && login.user.gender ? login.user.gender : 'Male',
      street1: address ? address.street1 : '',
      street2: address ? address.street2 : '',
      apartment: address ? address.apartment : '',
      state: address ? address.state : '',
      city: address ? address.city : '',
      postal_code: address ? address.postal_code : '',
      country: address ? address.country.code : auth.getCountry(),
      email: login && login.user ? login.user.email : auth.getEmail(),
      phone: address ? address.phone : '',
      billStreet1: '',
      billPostalCode: '',
      billPhone: '',
      billCountry: auth.getCountry(),
      create_account: true,
      create_address: true,
    };
    this.shipInfo = defaults;
    this.billingInfo.country = this.shipInfo.country;
    this.preCheckOut();
    this.forceUpdate();
  }

  isAllGift = (data) => {
    const ele = _.find(data, d => d.item.product.type.name !== 'Gift');
    return !ele;
  }

  preCheckOut = () => {
    const { country, email } = this.shipInfo;
    const { id, items } = this.state.basket || {};
    const { isSelfCollect, isExpress, promotion } = this.state;
    if (!items || items.length === 0) {
      if (!id) {
        return;
      }
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
      return;
    }
    if (id) {
      const data = {
        id,
        body: isSelfCollect ? {} : {
          shipping_address: {
            country: country ? country.toLowerCase() : '',
          },
          is_express: isExpress,
        },
      };
      if (promotion && promotion.id) {
        data.body.promos = [promotion.id];
      }
      this.preCheckOutBasket(data);
    }
  }

  preCheckOutBasket = (data) => {
    const { id, body } = data;
    const options = {
      url: PRECHECKOUT_BASKET_URL
        .replace('{cartPk}', id),
      method: 'POST',
      body,
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        this.setState({ basket: result });
      }
    }).catch((err) => {
      console.log('err', err);
    });
  };

  isCheckCountry = (country) => {
    const shipMoney = _.find(this.listShipping, d => d.country.code === country);
    return !!shipMoney;
  }

  updateShipping = (country) => {
    const shipMoney = _.find(this.listShipping, d => d.country.code === country);
    let shipping = 0;
    if (shipMoney) {
      const {
        subtotal,
      } = this.state.basket;

      if (subtotal >= shipMoney.threshold) {
        shipping = 0;
      } else {
        shipping = shipMoney.shipping;
      }
    } else {
      shipping = 0;
    }
    return shipping;
  }

  fetchOutCome = () => {
    if (isCheckNull(auth.getOutComeId())) {
      return;
    }
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', auth.getOutComeId()),
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        const { personality_choice: personalityChoice } = result;
        this.idPersonalityChoice = personalityChoice ? personalityChoice.id : undefined;
      }
    }).catch((err) => {
      console.log('err', err);
    });
  }

  fetchShippingInfo = () => {
    const optionExpress = {
      url: GET_SHIPPING_EXPRESS_URL,
      method: 'GET',
    };
    const options = {
      url: GET_SHIPPING_URL,
      method: 'GET',
    };
    Promise.all([fetchClient(options), fetchClient(optionExpress)]).then((results) => {
      _.forEach(results[1], (x) => {
        const ele = _.find(results[0], d => d.country.code === x.country.code);
        if (ele) {
          ele.is_express = x.is_express;
          ele.shippingExpress = x.shipping;
        }
      });
      _.forEach(results[0], (x) => {
        x.name = x.country.name;
      });
      this.listShipping = _.orderBy(results[0], 'name', 'asc');
      this.fetchAdress();
    }).catch((err) => {
      console.log('err', err);
    });
    // fetchClient(options).then((results) => {
    //   console.log('results', results);
    //   if (results && !results.isError) {
    //     _.forEach(results, (x) => {
    //       x.name = x.country.name;
    //     });
    //     this.listShipping = _.orderBy(results, 'name', 'asc');
    //     this.fetchAdress();
    //   } else {
    //     toastrError('Get list shipping');
    //   }
    // }).catch((err) => {
    //   console.log('err', err);
    //   toastrError('Get list shipping');
    // });
  }

  fetchAdress = () => {
    const dataUser = _.isEmpty(this.props.login) ? auth.getLogin() : this.props.login;
    if (dataUser && dataUser.user && dataUser.user.id) {
      const option = {
        url: GET_LIST_ADDRESS_URL,
        method: 'GET',
      };
      fetchClient(option, true).then((result) => {
        if (result && !result.isError) {
          this.setState({ listAddress: result, isShowOptionAddress: result && result.length > 0 });
          const address = _.find((result), x => x.is_default_baddress);
          if (!address && result.length > 0) {
            this.initalData(result[result.length - 1]);
            this.currentAddress = result[result.length - 1];
            return;
          }
          this.initalData(address);
          this.currentAddress = address;
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        console.log('error: ', error);
      });
    } else {
      this.setState({ isShowOptionAddress: false });
      this.initalData();
    }
  }

  isAddressSame = () => {
    if (_.isEmpty(this.currentAddress)) {
      return false;
    }
    const {
      country, city, email, postal_code, phone, street1, state,
    } = this.currentAddress;
    if (this.shipInfo.country !== (country ? country.code : undefined)) {
      return false;
    }
    if (this.shipInfo.city !== city && !_.isEmpty(this.shipInfo.city && !_.isEmpty(city))) {
      return false;
    }
    if (this.shipInfo.email !== email) {
      return false;
    }
    if (this.shipInfo.postal_code !== postal_code) {
      return false;
    }
    if (this.shipInfo.phone !== phone) {
      return false;
    }
    if (this.shipInfo.street1 !== street1) {
      return false;
    }
    if (this.shipInfo.state !== state && !_.isEmpty(this.shipInfo.state && !_.isEmpty(state))) {
      return false;
    }
    return true;
  }

  validityAddress = (data) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const key in data) {
      if (!data[key] && key !== 'idAddress') {
        return false;
      }
    }
    return true;
  }

  createAddressWithUser = async (body) => {
    const { user } = this.props.login;
    if (user && user.id) {
      _.assign(body, { is_active: this.shipInfo.create_address });
      const options = {
        url: GET_LIST_ADDRESS_URL,
        method: 'POST',
        body,
      };
      try {
        const address = await fetchClient(options, true);
        return address;
      } catch (error) {
        console.log('error', error);
      }
      return null;
    }
  }

  createAddress = async (body) => {
    const options = {
      url: CREATE_ADDRESS_URL,
      method: 'POST',
      body,
    };
    try {
      const address = await fetchClient(options);
      return address;
    } catch (error) {
      console.log('error', error);
    }
    return null;
  }

  onChangeTimeDelivery = (e) => {
    const { value } = e.target;
    this.setState({ timeDelivery: value });
  }

  changeNote = (e) => {
    const { value } = e.target;
    this.setState({ noteText: value });
  }

  onClickCreateAccount = (e) => {
    const { checked } = e.target;
    this.shipInfo.create_account = checked;
    this.forceUpdate();
  }


  gotoAnchor = (id) => {
    const ele = document.getElementById(id);
    if (ele && (isMobile || isTablet)) {
      ele.scrollIntoView({ behavior: 'smooth' });
      // window.scrollBy(0, offset);
    }
  }

  checkEmailExist = () => {
    const { login } = this.props;
    const isLogined = login && login.user && login.user.id;
    if (isLogined) {
      this.setState({ isShowCreateAccount: false });
      this.shipInfo.create_account = false;
      return;
    }
    const { email } = this.shipInfo;
    if (!email) {
      this.setState({ isShowCreateAccount: true });
      return;
    }
    const option = {
      url: CHECK_EMAIL_LOGIN_URL.replace('{email}', email),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.setState({ isShowCreateAccount: false });
        this.shipInfo.create_account = false;
        return;
      }
      throw new Error();
    }).catch(() => {
      this.shipInfo.create_account = true;
      this.setState({ isShowCreateAccount: true });
    });
  }

  checkContinuteStep1 = () => {
    // const { name, email, phone } = this.shipInfo;
    const { email, firstName, lastName } = this.shipInfo;
    const { listError } = this.state;
    const fieldError = [];
    if (!firstName || !email || !lastName) {
      if (!email) {
        fieldError.push('email');
        listError.push('email');
      }
      if (!firstName) {
        fieldError.push('first name');
        listError.push('firstName');
      }
      if (!lastName) {
        fieldError.push('last name');
        listError.push('lastName');
      }
      toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }

    // check '@' include first name, last name
    if (firstName.includes('@')) {
      fieldError.push('First name');
      listError.push('firstName');
    }
    if (lastName.includes('@')) {
      fieldError.push('Last name');
      listError.push('lastName');
    }
    if (listError.length > 0) {
      toastrError('@ is an invalid character for name');
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }

    if (!EmailValidator.validate(this.shipInfo.email)) {
      listError.push('email');
      toastrError('The email is not available');
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }
    this.checkEmailExist();
    this.setState({ stepCheckOut: 2, isStep1Done: true });
    this.gotoAnchor('anchor-step1');
    return true;
  }

  checkContinuteStep2 = () => {
    const { isSelfCollect, isBillAddressSame, listError } = this.state;
    const {
      street1, postal_code, phone, country, pan_card: panCard,
    } = this.shipInfo;
    const fieldError = [];
    const isPanCardError = ['in', 'id'].includes(country) && !panCard;
    if (!isSelfCollect && (!street1 || !postal_code || !phone || isPanCardError)) {
      if (!street1) {
        fieldError.push('address');
        listError.push('street1');
      }
      if (!postal_code) {
        fieldError.push('zip code');
        listError.push('postal_code');
      }
      if (!phone) {
        fieldError.push('phone');
        listError.push('phone');
      }
      if (isPanCardError) {
        fieldError.push('pan_card');
        listError.push('pan_card');
      }
      toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
      this.setState({ stepCheckOut: 2, isStep2Done: false, listError });
      return false;
    }

    if (!isBillAddressSame) {
      const {
        street1: billStreet1, postal_code: billPostalCode, country: billCountry, pan_card: billPanCard,
      } = this.billingInfo;
      const isBillPanCardError = ['in', 'id'].includes(billCountry) && !billPanCard;
      if (!billStreet1 || !billPostalCode || isBillPanCardError) {
        if (!billStreet1) {
          fieldError.push('address');
          listError.push('billStreet1');
        }
        if (!billPostalCode) {
          fieldError.push('zip code');
          listError.push('billPostalCode');
        }
        if (isBillPanCardError) {
          fieldError.push('pan_card');
          listError.push('billPanCard');
        }
        toastrError(`Please enter your billing ${_.join(fieldError, ', ')}`);
        this.setState({ stepCheckOut: 2, isStep2Done: false, listError });
        return false;
      }
    }
    this.gotoAnchor('anchor-step2', 0);
    this.setState({ stepCheckOut: 3, isStep2Done: true, isStep3Done: false });
    // setTimeout(() => {
    //   this.gotoAnchor('anchor-step3');
    // },300);

    return true;
  }

  checkContinuteStep3 = () => {
    const { isBillAddressSame } = this.state;
    const { billStreet1, billPostalCode, billPhone } = this.shipInfo;
    if (!isBillAddressSame && (!billStreet1 || !billPostalCode || !billPhone)) {
      toastrError('Please enter all the information');
      this.setState({ stepCheckOut: 3, isStep3Done: false });
      return false;
    }
    this.setState({ stepCheckOut: 4, isStep3Done: true });
    return true;
  }

  isShowPackagingOption = (basket) => {
    const { items } = basket;
    let flags = false;
    _.forEach(items, (d) => {
      const { item } = d;
      if (item) {
        const type = item.product.type.name;
        const countrySupport = ['sg', 'au'];
        if ((item.is_sample === false) && (type === 'Perfume' || type === 'Creation' || type === 'Scent' || type === 'Elixir') && (this.shipInfo.country ? countrySupport.includes(this.shipInfo.country.toLowerCase()) : false)) {
          flags = true;
        }
      }
    });
    return flags;
  }

  selectPackagingOption = (id) => {
    this.packaging = id;
  };

  updateProductBasket = (data) => {
    const { id: idCart } = this.state.basket;
    const { item } = data;
    const url = EDIT_DELETE_BASKET_URL.replace('{cartPk}', idCart).replace('{id}', item.id);
    const {
      file, imagePremade,
    } = item;
    this.props.loadingPage(true);
    if (!file || imagePremade) {
      const options = generateBodyItemCart({ idCart, item }, url, 'PUT');
      fetchClient(options, true).then((result) => {
        this.props.loadingPage(false);
        if (result && !result.isError) {
          this.setState({ basket: result }, () => {
            this.preCheckOut();
          });
        }
      }).catch((err) => {
        this.props.loadingPage(false);
        console.log('error', err);
      });
      return;
    }
    const options = generateBodyItemCart({ idCart, item }, url, 'PUT');
    console.log('options 1', options);
    fetchClientFormData(options, true).then((result) => {
      this.props.loadingPage(false);
      if (result && !result.isError) {
        this.setState({ basket: result }, () => {
          this.preCheckOut();
        });
      }
    }).catch(() => {
      this.props.loadingPage(false);
    });
  }

  deleteProductBasket = (data) => {
    const { id: idCart } = this.state.basket;
    const { item } = data;
    if (!item.id || !idCart) {
      return;
    }
    const options = {
      url: EDIT_DELETE_BASKET_URL
        .replace('{cartPk}', idCart)
        .replace('{id}', item.id),
      method: 'DELETE',
    };
    this.props.loadingPage(true);
    fetchClient(options, true).then((result) => {
      this.props.loadingPage(false);
      if (result && !result.isError) {
        this.setState({ basket: result });
      }
    }).catch((err) => {
      console.log('errr', err);
    });
  }

  openShowAddress = () => {
    this.setState({ isShowChoiseAddress: true });
  }

  closeShowAddress = () => {
    this.setState({ isShowChoiseAddress: false });
  }

  onChooseNewAddress = (flag) => {
    if (flag) {
      this.initalData();
    } else {
      this.initalData(this.currentAddress);
    }
  }

  onChangeAddress = (address) => {
    this.currentAddress = address;
    this.initalData(address);
  }

  render() {
    const { login } = this.props;
    const {
      payMethod, noteText, isSelfCollect, timeDelivery, isExpress,
      stepCheckOut, isStep1Done, isStep2Done, isStep3Done, isStep4Done, isBillAddressSame, seo,
      isShowCreateAccount, listError, isOpenNote, isOPenPromo, textBlock, imageBlockIcon, buttonBlock,
      iconBlock, blogBlock, redeemCode, basket, isShowChoiseAddress, listAddress, isShowOptionAddress,
    } = this.state;
    if (!basket) {
      return (<div />);
    }
    const {
      id, total, shipping, subtotal, items, discount,
    } = basket;
    const newPrice = parseFloat(discount) > 0 ? parseFloat(subtotal) - parseFloat(discount) : -1;
    const isOnlyGift = this.isAllGift(items);
    const listProduct = basket && basket.items ? basket.items : [];
    const totalProduct = basket && basket.items ? basket.items.length : 0;

    const cmsCommon = getCmsCommon(this.props.cms);
    const subtotalBt = getNameFromCommon(cmsCommon, 'Subtotal');
    const shippingBt = getNameFromCommon(cmsCommon, 'Shipping');
    const discountBt = getNameFromCommon(cmsCommon, 'Discount');
    const promoGiftBt = getNameFromCommon(cmsCommon, 'Promo/Gift_Certificate');
    const applyBt = getNameFromCommon(cmsCommon, 'APPLY');
    const totalBt = getNameFromCommon(cmsCommon, 'TOTAL');
    const orderBt = getNameFromCommon(cmsCommon, 'ORDER_SUMMARY');
    const checkOutBt = getNameFromCommon(cmsCommon, 'CHECKOUT');
    const addNoteBt = getNameFromCommon(cmsCommon, 'ADD_A_NOTE');
    const placeBt = getNameFromCommon(cmsCommon, 'PLACE_ORDER');
    const giftCodeBt = getNameFromCommon(cmsCommon, 'GIFT_CODE_APPLIED');
    const isShowPackagingOption = this.isShowPackagingOption(basket);
    const isLogined = login && login.user && login.user.id;
    if (!isShowPackagingOption) {
      this.packaging = undefined;
    }
    const htmlItems = (
      <Col md={isTablet ? '12' : '6'} xs="12" className="div-col div-infor" style={isMobile ? { paddingLeft: '10px', paddingRight: '10px', marginTop: '30px' } : {}}>
        <div className="div-customer div-col">
          <CustomerItem
            stepCheckOut={stepCheckOut}
            isStepDone={isStep1Done}
            onClickHeader={() => this.setState({ stepCheckOut: 1 })}
            shipInfo={this.shipInfo}
            onChange={this.onChange}
            onClickContinute={this.checkContinuteStep1}
            login={login}
            cms={this.props.cms}
            listError={listError}
            onChangeGenderSelect={this.onChangeGenderSelect}
          />
          {
            !isOnlyGift ? (
              <ShippingItem
                isLogined={isLogined}
                currentAddress={this.currentAddress}
                openShowAddress={this.openShowAddress}
                closeShowAddress={this.closeShowAddress}
                onChooseNewAddress={this.onChooseNewAddress}
                onChangeAddress={this.onChangeAddress}
                listAddress={listAddress}
                isShowOptionAddress={isShowOptionAddress}
                buttonBlock={buttonBlock}
                stepCheckOut={stepCheckOut}
                isStepDone={isStep2Done}
                isExpress={isExpress}
                isSelfCollect={isSelfCollect}
                onClickHeader={() => { this.setState({ stepCheckOut: 2 }); this.checkEmailExist(); }}
                onClickSelfCollect={() => this.setState({ isSelfCollect: true, isExpress: false, isBillAddressSame: false }, () => { this.preCheckOut(); })}
                onClickStandard={() => this.setState({ isSelfCollect: false, isExpress: false }, () => { this.preCheckOut(); })}
                onClickExpress={() => this.setState({ isSelfCollect: false, isExpress: true }, () => { this.preCheckOut(); })}
                shipInfo={this.shipInfo}
                onChange={this.onChange}
                onClickContinute={this.checkContinuteStep2}
                listShipping={this.listShipping}
                optionTime={this.optionTime}
                onChangeTimeDelivery={this.onChangeTimeDelivery}
                isBillAddressSame={isBillAddressSame}
                shipping={shipping}
                timeDelivery={timeDelivery}
                cms={this.props.cms}
                isShowPackagingOption={isShowPackagingOption}
                selectPackagingOption={this.selectPackagingOption}
                isShowCreateAccount={isShowCreateAccount}
                onClickCreateAccount={this.onClickCreateAccount}
                listError={listError}
                isCheckOutGift
                billingInfo={this.billingInfo}
                onChangeBillingInfo={this.onChangeBillingInfo}
                onClickCheckBillAdress={e => this.setState({ isBillAddressSame: e })}
              />
            ) : (<div />)
          }

          {/* <BillItems
            stepCheckOut={stepCheckOut}
            isStepDone={isStep3Done}
            isHiddenButton={isBillAddressSame}
            onClickHeader={() => this.setState({ stepCheckOut: 3 })}
            shipInfo={this.shipInfo}
            onChange={this.onChange}
            onClickContinute={this.checkContinuteStep3}
            listShipping={this.listShipping}
            cms={this.props.cms}
          /> */}
          {/* <PaymentItem
            stepCheckOut={stepCheckOut}
            isStepDone={isOnlyGift ? isStep2Done : isStep3Done}
            onClickHeader={() => { this.setState({ stepCheckOut: isOnlyGift ? 2 : 3 }); this.checkEmailExist(); }}
            changeNote={this.changeNote}
            onCheckOutFunc={this.onCheckOutFunc}
            noteText={noteText}
            payMethod={payMethod}
            onClickStripe={() => { this.setState({ payMethod: 'stripe' }); }}
            onClickPaypal={() => { this.setState({ payMethod: 'paypal' }); }}
            onClickPaypalLate={() => { this.setState({ payMethod: 'stripe_pay_later' }); }}
            cms={this.props.cms}
            isOnlyGift={isOnlyGift}
            textBlock={textBlock}
            shipInfo={this.shipInfo}
            imageBlockIcon={imageBlockIcon}
            iconBlock={iconBlock}
            basket={basket}
          /> */}
        </div>
      </Col>
    );

    const summaryCheckOut = (
      <div className="div-col" style={isMobile || isTablet ? { width: '100%', margin: isTablet ? '0 10px' : '0 20px' } : {}}>
        <div className="div-row justify-between mt-3">
          <span>
            <b>
              {subtotalBt}
            </b>
          </span>
          {
            newPrice === -1
              ? (
                <span>
                  <b>
                    {generaCurrency(subtotal, true)}
                  </b>
                </span>
              ) : (
                <span>
                  <span className="strike">
                    {generaCurrency(subtotal, true)}
                  </span>
                  <span className="ml-2">
                    <b>
                      {generaCurrency(newPrice, true)}
                    </b>
                  </span>
                </span>
              )
          }
        </div>
        <div className="div-row justify-between mt-2 mb-2">
          <span>
            {shippingBt}
          </span>
          <span>
            {generaCurrency(shipping, true)}
          </span>
        </div>
        <div className="div-row justify-between">
          <span>
            {discountBt}
          </span>
          <span>
            {generaCurrency(discount, true)}
          </span>
        </div>
        <hr style={{
          width: '100%', color: '#E2E2E2', margin: '0px', marginTop: '16px',
        }}
        />
        <div id="id-promotion" className={`div-expand ${isOpenNote ? 'open-expand' : 'close-expand'}`}>
          <div
            className="div-header-expand"
            onClick={() => {
              this.setState({ isOpenNote: !isOpenNote });
            }
            }
          >
            <span>
              {addNoteBt}
            </span>
            <button
              type="button"
              onClick={() => {
                this.setState({ isOpenNote: !isOpenNote });
              }
            }
            >
              <img src={isOpenNote ? icSub : icPlus} alt="icon" />
            </button>
          </div>
          <div className="div-content-expand">
            <Input
              type="textarea"
              className="border-checkout mb-3"
              style={{ height: '4rem' }}
              value={noteText}
              onChange={this.changeNote}
              onClick={() => trackGTMCheckOut(5, basket)}
            />
          </div>
        </div>
        <hr style={{ width: '100%', color: '#E2E2E2', margin: '0px' }} />
        <div className="div-expand close-expand">
          <div className="div-header-expand">
            <div className="div-left-gift">
              <span className={isMobile ? 'hidden' : 'title mr-5'}>
                {redeemCode}
              </span>
              <img src={iconCheck} alt="icon" className="mr-3" />
              <span className="text-green">
                {giftCodeBt}
              </span>

            </div>

            <button type="button">
              <img src={icSub} alt="icon" />
            </button>
          </div>
        </div>
        {/* <div className="div-col mt-3">
          <span>
            {promoGiftBt}
          </span>
          <div className="div-row">
            <input
              type="text"
              className={`${isMobile && !isTablet ? 'w-70 border-checkout' : 'w-80 border-checkout'}`}
              onChange={this.onChangePromo}
            />
            <button
              type="button"
              className={`${isMobile && !isTablet ? 'w-30 border-checkout' : 'w-30 border-checkout'}`}
              style={{ marginLeft: '40px' }}
              onClick={this.onCheckPromo}
            >
              {applyBt}
            </button>
          </div>
        </div> */}
        <div style={{ width: '100%', height: '1px', background: '#0D0D0D' }} />
        <div className="div-row justify-between mt-3 mb-3">
          <span style={{ fontSize: '20px', color: '#0D0D0D', fontWeight: '600' }}>
            {totalBt}
          </span>
          <span style={{ fontSize: '20px', color: '#0D0D0D', fontWeight: '600' }}>
            {generaCurrency(total, true)}
          </span>
        </div>
        <div style={{ width: '100%', height: '1px', background: '#0D0D0D' }} />
        <button
          type="button"
          className="bt-checkout mb-5 mt-3"
          style={{ padding: '1rem 0px' }}
          onClick={this.onCheckOutFunc}
        >
          {placeBt}
        </button>
      </div>
    );
    const htmlInformation = (
      <Col md={isTablet ? '12' : '6'} xs="12" className="div-order div-col" style={isMobile ? { marginTop: '0px', paddingRight: '10px', paddingLeft: '10px' } : { marginTop: '40px', paddingRight: '40px' }}>
        <div className="div-header div-row justify-between pb-2" style={isMobile || isTablet ? { visibility: 'hidden' } : {}}>
          <span>
            {orderBt}
          </span>
          {/* <button type="button" className="button-bg__none pa0">
              Edit Cart
              </button> */}
        </div>
        {
              _.map(listProduct, d => (
                <CardItem
                  // isCheckOut
                  isB2C
                  idCart={id}
                  data={d}
                  updateProductBasket={this.updateProductBasket}
                  deleteProductBasket={this.deleteProductBasket}
                  isCheckOut={!isMobile}
                  cms={this.props.cms}
                  style={{ borderBottomColor: '#E2E2E2' }}
                  isCheckOutGift
                />
              ))
            }
        {isMobile || isTablet ? <div /> : summaryCheckOut}
      </Col>
    );
    return (
      <div className="div-col justify-center div-checkout" style={isTablet ? { marginLeft: '40px', marginRight: '40px' } : {}}>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/checkout-gift')}
        </MetaTags>
        <HeaderHomePage />
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}

        <h1
          className="tx-header tc"
          style={isMobile || isTablet ? {
            textTransform: 'uppercase', letterSpacing: '6px', marginTop: '10px', marginBottom: '0px',
          } : {
            marginTop: '30px', marginBottom: '30px',
          }}
        >
          {/* {checkOutBt.charAt(0).toUpperCase() + checkOutBt.slice(1)} */}
        </h1>
        <Row className="div-body">
          {
            isMobile || isTablet ? (
              <React.Fragment>
                {htmlInformation}
                {htmlItems}
                {summaryCheckOut}
              </React.Fragment>
            ) : (
              <React.Fragment>
                {htmlItems}
                {htmlInformation}
              </React.Fragment>

            )
          }
        </Row>
        <AllCleanScent blogBlock={blogBlock} />
        {
          isShowChoiseAddress ? (
            <PopUpSaveAdsress
              closeShowAddress={this.closeShowAddress}
              listAddress={listAddress}
              currentAddress={this.currentAddress}
              onChangeAddress={this.onChangeAddress}
              buttonBlock={buttonBlock}
            />
          ) : (null)
        }
        {
          isMobile && (
          <FooterCheckoutMobile
            isDisablePromotion
            newPrice={newPrice}
            subtotal={subtotal}
            onCheckOutFunc={this.onCheckOutFunc}
            buttonBlock={buttonBlock}
            isShowFooter={this.state.isShowFooter}
          />
          )
        }
        <div id="div-button" className="hidden" />
        <FooterV2 />
      </div>
    );
  }
}

CheckOutGift.propTypes = {
  login: PropTypes.shape(PropTypes.object).isRequired,
  refPoint: PropTypes.string.isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  cms: PropTypes.shape().isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  loadingPage: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    refPoint: state.refPoint,
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CheckOutGift));
