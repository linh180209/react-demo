import React, { useRef, useEffect } from 'react';
import classnames from 'classnames';
import {
  Col, Row, Input, Label, UncontrolledPopover, PopoverBody, PopoverHeader,
} from 'reactstrap';
import _ from 'lodash';

import { isMobile } from '../../DetectScreen';
import AutoCompleteInput from '../../components/Input/autoCompleteInput';
import icInfo from '../../image/icon/ic-info.svg';
import { getNameFromButtonBlock, getNameFromCommon } from '../../Redux/Helpers';
import useForceRerender from '../../Utils/useForceRerender';

function BillingAddress(props) {
  const billingRef = useRef();
  const forceRerender = useForceRerender();

  const onChangeInputAutoComplete = (value, name) => {
    props.onChange({
      target: {
        value,
        name,
      },
    });
  };

  const onChangeAutoComplete = async (data, isSearchCity) => {
    const {
      country: autoCompleteCountry,
      address: autoCompleteAddress,
      city: autoCompleteCity,
      state: autoCompleteState,
      zip: autoCompleteZip,
    } = data;
    if (!isSearchCity) {
      props.billingInfo.street1 = autoCompleteAddress;
    }
    props.billingInfo.city = autoCompleteCity;
    props.billingInfo.state = autoCompleteState?.name;
    props.billingInfo.postal_code = autoCompleteZip;
    if (autoCompleteCountry) {
      props.onChange({
        target: {
          value: autoCompleteCountry?.code?.toLowerCase(),
          name: 'country',
        },
      });
    }
  };

  const clearAutoComplete = (isSearchCity) => {
    props.billingInfo.city = '';
    props.billingInfo.state = '';
    props.billingInfo.postal_code = '';
    if (!isSearchCity) {
      props.billingInfo.street1 = '';
    }
    props.onChange({
      target: {
        value: props.billingInfo.street1,
        name: 'street1',
      },
    });
  };

  useEffect(() => {
    if (!props.isBillAddressSame) {
      props.billingInfo.country = props.shipInfo?.country;
      clearAutoComplete();
      forceRerender();
    }
  }, [props.isBillAddressSame]);

  const addressBt = getNameFromCommon(props.cmsCommon, 'Address');
  const countryBt = getNameFromCommon(props.cmsCommon, 'COUNTRY');
  const cityBt = getNameFromCommon(props.cmsCommon, 'city');
  const zipCodeBt = getNameFromCommon(props.cmsCommon, 'ZIP_CODE');
  const stateBt = getNameFromCommon(props.cmsCommon, 'State');
  const panCardBt = getNameFromCommon(props.cmsCommon, 'PAN/Aadhar Number');
  const panCardIndoBt = getNameFromCommon(props.cmsCommon, 'PAN/Aadhar Number Indonesia');
  const thisInfoBt = getNameFromButtonBlock(props.buttonBlock, 'This information will be used for custom clearance only');
  const thisInfoIndonesiaBt = getNameFromButtonBlock(props.buttonBlock, 'This information of Indonesia');
  const bilingAdress = getNameFromButtonBlock(props.buttonBlock, 'Billing address');
  const defaultBt = getNameFromButtonBlock(props.buttonBlock, 'Default');
  const sameAsBt = getNameFromButtonBlock(props.buttonBlock, 'Same as shipping address');
  const newBt = getNameFromButtonBlock(props.buttonBlock, 'New');
  const addAnBt = getNameFromButtonBlock(props.buttonBlock, 'Add an alternative billing address');

  return (
    <div>
      <div className="div-col div-header-mini">
        <span>
          {bilingAdress}
        </span>
      </div>
      <div className="div-col div-grid">
        <div
          onClick={() => props.onClickCheckBillAdress(true)}
          className="div-option-checkout top div-row"
        >
          <div className="div-button div-col justify-center items-center" style={isMobile ? { width: '38px', margin: 'auto' } : {}}>
            <div className={classnames('div-col justify-center items-center', props.isBillAddressSame ? 'selected' : '')}>
              <button
                type="button"
                className={props.isBillAddressSame ? 'bt-choose' : ''}
              />
            </div>
          </div>
          <div className="div-name div-col justify-center">
            <span>
              {defaultBt}
            </span>
          </div>
          <div className="div-info div-col justify-center">
            <span>
              {sameAsBt}
            </span>
          </div>
        </div>
        <div
          onClick={() => props.onClickCheckBillAdress(false)}
          className="div-option-checkout bottom div-row"
        >
          <div className="div-button div-col justify-center items-center" style={isMobile ? { width: '38px', margin: 'auto' } : {}}>
            <div className={classnames('div-col justify-center items-center', !props.isBillAddressSame ? 'selected' : '')}>
              <button
                type="button"
                className={!props.isBillAddressSame ? 'bt-choose' : ''}
              />
            </div>
          </div>
          <div className="div-name div-col justify-center">
            <span>
              {newBt}
            </span>
          </div>
          <div className="div-info div-col justify-center">
            <span>
              {addAnBt}
            </span>
          </div>
        </div>
      </div>

      <div className={props.isBillAddressSame ? 'hidden' : 'div-col div-shipping-address mt-3'}>
        <AutoCompleteInput
          ref={billingRef}
          className={`border-checkout w-100 padding-0 ${props.listError.includes('billStreet1') ? 'error' : ''}`}
          isSearchCity={false}
          name="street1"
          id="street1"
          onKeyDown={() => {}}
          label=""
          placeholder={`${addressBt}*`}
          country={props.billingInfo?.country || 'sg'}
          value={props.billingInfo?.street1}
          onChangeInput={onChangeInputAutoComplete}
          onChange={onChangeAutoComplete}
          eventClear={clearAutoComplete}
          isError={props.listError.includes('billStreet1')}
          errorMessage=""
        />
        <Row>
          <Col md={isMobile ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px' } : { paddingRight: '7px' }}>
            <Input
              type="select"
              className="border-checkout w-100"
              placeholder={countryBt}
              name="country"
              onChange={props.onChange}
            >
              {
                  _.map(props.listShipping, x => (
                    <option value={x.country.code} selected={(props.billingInfo?.country ? props.billingInfo?.country.toUpperCase() : '') === x.country.code.toUpperCase()}>
                      {x.country.name}
                    </option>
                  ))
                }
            </Input>
          </Col>
          <Col md={isMobile ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px' } : { paddingLeft: '7px' }}>
            <input
              type="text"
              className="border-checkout w-100"
              placeholder={cityBt}
              name="city"
              value={props.billingInfo?.city}
              onChange={props.onChange}
            />
          </Col>
        </Row>

        <Row>
          <Col md="6" xs="6" style={isMobile ? { padding: '0px', paddingRight: '7px' } : { paddingRight: '7px' }}>
            <input
              type="text"
              className={`border-checkout w-100 ${props.listError.includes('billPostalCode') ? 'error' : ''}`}
              placeholder={`${zipCodeBt}*`}
              name="postal_code"
              value={props.billingInfo?.postal_code}
              onChange={props.onChange}
            />

          </Col>
          {
            props.billingInfo?.country === 'us'
              ? (
                <Col md="6" xs="6" style={isMobile ? { padding: '0px' } : { paddingRight: '15px', paddingLeft: '7px' }}>
                  <input
                    type="text"
                    className="border-checkout w-100"
                    placeholder={`${stateBt}`}
                    name="state"
                    value={props.billingInfo?.state}
                    onChange={props.onChange}
                  />
                </Col>
              ) : ['in', 'id'].includes(props.billingInfo?.country) ? (
                <Col md="6" xs="6" style={isMobile ? { padding: '0px' } : { paddingRight: '15px', paddingLeft: '7px' }}>
                  <input
                    type="text"
                    className={`border-checkout w-100 ${props.listError.includes('billPanCard') ? 'error' : ''}`}
                    placeholder={`${props.billingInfo?.country === 'id' ? panCardIndoBt : panCardBt}*`}
                    name="pan_card"
                    value={props.billingInfo?.pan_card}
                    onChange={props.onChange}
                  />
                  <button
                    style={{
                      position: 'absolute',
                      top: '50%',
                      transform: 'translateY(-50%)',
                      right: '30px',
                    }}
                    id="id-pan-card"
                    className="button-bg__none bt-info"
                    type="button"
                  >
                    <img style={{ width: '15px' }} src={icInfo} alt="info" />
                  </button>
                  <UncontrolledPopover trigger="focus" placement="bottom" target="id-pan-card">
                    <PopoverBody className="popover-checkout">{props.billingInfo?.country === 'id' ? thisInfoIndonesiaBt : thisInfoBt}</PopoverBody>
                  </UncontrolledPopover>
                </Col>
              ) : undefined
          }
        </Row>
      </div>

    </div>
  );
}

export default BillingAddress;
