import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Col, Row, Input, Label, UncontrolledPopover, PopoverBody, PopoverHeader,
} from 'reactstrap';
import _ from 'lodash';
import classnames from 'classnames';
import HeaderItem from './headerItems';
import auth from '../../Redux/Helpers/auth';
import PackagingOption from './packagingOption';
import emitter from '../../Redux/Helpers/eventEmitter';
import {
  getCmsCommon, getNameFromCommon, generaCurrency, getNameFromButtonBlock, trackGTMCheckOut,
} from '../../Redux/Helpers';
import ButtonInfo from './buttonInfo';
import { isBrowser, isMobile } from '../../DetectScreen';
import icInfo from '../../image/icon/ic-info.svg';
import icNext from '../../image/icon/ic-next.svg';
import ButtonCT from '../../components/ButtonCT';
import AutoCompleteInput from '../../components/Input/autoCompleteInput';
import BillingAddress from './billingAddress';

class ShippingItemV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isNewAddress: false,
      isShowOptionAddress: props.isShowOptionAddress,
    };
    this.addressRef = React.createRef(null);
  }

  componentDidMount() {
    trackGTMCheckOut(3, this.props.basket);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { isShowOptionAddress } = nextProps;
    if (isShowOptionAddress !== prevState.isShowOptionAddress) {
      return ({ isShowOptionAddress });
    }
    return null;
  }

  onChangeInputAutoComplete = (value, name) => {
    this.props.onChange({
      target: {
        value,
        name,
      },
    });
  };

  onChangeAutoComplete = async (data, isSearchCity) => {
    const {
      country: autoCompleteCountry,
      address: autoCompleteAddress,
      city: autoCompleteCity,
      state: autoCompleteState,
      zip: autoCompleteZip,
    } = data;
    if (!isSearchCity) {
      this.props.shipInfo.street1 = autoCompleteAddress;
    }
    this.props.shipInfo.city = autoCompleteCity;
    this.props.shipInfo.state = autoCompleteState?.name;
    this.props.shipInfo.postal_code = autoCompleteZip;
    if (autoCompleteCountry) {
      this.props.onChange({
        target: {
          value: autoCompleteCountry?.code?.toLowerCase(),
          name: 'country',
        },
      });
    }
  };


  clearAutoComplete = (isSearchCity) => {
    this.props.shipInfo.city = '';
    this.props.shipInfo.state = '';
    this.props.shipInfo.postal_code = '';
    if (!isSearchCity) {
      this.props.shipInfo.street1 = '';
    }
    this.props.onChange({
      target: {
        value: this.props.shipInfo.street1,
        name: 'street1',
      },
    });
  };

  render() {
    const { isNewAddress, isShowOptionAddress } = this.state;
    const {
      stepCheckOut, isStepDone, shipInfo, onChange, onClickContinute, isSelfCollect, onClickSelfCollect, onClickStandard,
      onChangeTimeDelivery, listShipping, optionTime, isBillAddressSame, onClickCheckBillAdress, shipping, timeDelivery,
      isShowPackagingOption, selectPackagingOption, isShowCreateAccount, onClickCreateAccount, listError,
      isCheckOutClub, buttonBlock, isExpress, onClickExpress, isCheckOutGift, openShowAddress, closeShowAddress,
      onClickCreateAddress, listAddress, currentAddress, isLogined, basket, expressTimeslot, onChangeBillingInfo, billingInfo,
      isHasFreeGift,
    } = this.props;

    const cmsCommon = getCmsCommon(this.props.cms);
    const shippingBt = getNameFromCommon(cmsCommon, 'Shipping');
    const selfCollectBt = getNameFromCommon(cmsCommon, 'Self_Collect_Free');
    const standBt = getNameFromCommon(cmsCommon, 'Standard');
    const deliveryBt = getNameFromCommon(cmsCommon, 'Delivery_Time');
    const shippingMethodyBt = getNameFromCommon(cmsCommon, 'shipping_method');
    const selfBt = getNameFromCommon(cmsCommon, 'Self_Collect');
    const pickupBt = getNameFromCommon(cmsCommon, 'Pick_up_your_perfume');
    const duxtonBt = getNameFromCommon(cmsCommon, '77_duxton');
    const freeBt = getNameFromCommon(cmsCommon, 'FREE');
    const daysBt = getNameFromCommon(cmsCommon, '2-3_days');
    const shippingAddressBt = getNameFromCommon(cmsCommon, 'SHIPPING_ADDRESS');
    const addressBt = getNameFromCommon(cmsCommon, 'Address');
    const zipCodeBt = getNameFromCommon(cmsCommon, 'ZIP_CODE');
    const countryBt = getNameFromCommon(cmsCommon, 'COUNTRY');
    const phoneBt = getNameFromCommon(cmsCommon, 'PHONE');
    const stateBt = getNameFromCommon(cmsCommon, 'State');
    const panCardBt = getNameFromCommon(cmsCommon, 'PAN/Aadhar Number');
    const panCardIndoBt = getNameFromCommon(cmsCommon, 'PAN/Aadhar Number Indonesia');
    const createAccount = getNameFromCommon(cmsCommon, 'Create_an_account');
    const cityBt = getNameFromCommon(cmsCommon, 'city');
    const continueBt = getNameFromCommon(cmsCommon, 'CONTINUE');
    const messageSABt = getNameFromCommon(cmsCommon, 'message_Saudi_Arabia');
    const requireBt = getNameFromButtonBlock(buttonBlock, 'Required_Field');
    const expressBt = getNameFromButtonBlock(buttonBlock, 'Express');
    const samedayBt = getNameFromButtonBlock(buttonBlock, 'Same_day_delivery');
    const thisOptionBt = getNameFromButtonBlock(buttonBlock, 'This_option');
    const saveAddressBt = getNameFromButtonBlock(buttonBlock, 'Saved_Address');
    const changeBt = getNameFromButtonBlock(buttonBlock, 'Change');
    const newAddressBt = getNameFromButtonBlock(buttonBlock, 'New_Address');
    const addNewAddressBt = getNameFromButtonBlock(buttonBlock, 'Add_new_address_for_shipping');
    const requiredFieldBt = getNameFromButtonBlock(buttonBlock, 'Required_Field');
    const saveAddressForFutureBt = getNameFromButtonBlock(buttonBlock, 'Save_address_for_future_transaction');
    const thisInfoBt = getNameFromButtonBlock(buttonBlock, 'This information will be used for custom clearance only');
    const thisInfoIndonesiaBt = getNameFromButtonBlock(buttonBlock, 'This information of Indonesia');
    const freeShipping = getNameFromButtonBlock(buttonBlock, 'Free shipping for orders above');
    const billingAddress = getNameFromButtonBlock(buttonBlock, 'Billing address');
    const continuteGiftBt = getNameFromButtonBlock(buttonBlock, 'CONTINUE TO FREE GIFT');
    const continutePaymentBt = getNameFromButtonBlock(buttonBlock, 'CONTINUE TO PAYMENT');
    const returnToCustomerBt = getNameFromButtonBlock(buttonBlock, 'RETURN TO CUSTOMER');
    const alreadyBt = getNameFromCommon(cmsCommon, 'Already_have_an_account');
    const loginBt = getNameFromCommon(cmsCommon, 'Log_in');

    const shippingSelection = _.find(listShipping, x => (shipInfo && shipInfo.country ? shipInfo.country.toUpperCase() : '') === x.country.code.toUpperCase());
    const shippingCurrency = _.find(listShipping, x => (currentAddress && currentAddress.country ? currentAddress.country.code.toUpperCase() : '') === x.country.code.toUpperCase());
    const isExpressCountry = shippingSelection ? shippingSelection.is_express : false;
    const isSelfCollectCountry = auth.getCountry() && ['vn', 'sg'].includes(auth.getCountry().toLowerCase());

    const isFreeDelivery = basket && shippingSelection ? parseFloat(shippingSelection.threshold) < parseFloat(basket.subtotal) : false;
    if (shipInfo.country !== 'us') {
      shipInfo.state = '';
    }
    const htmlCenter = (
      <div className="h-100 div-col justify-center ml-5">
        <span className="tx-more-infor">
          {shipInfo.street1}
          {' '}
          {shipInfo.postal_code}
          {' '}
          {shipInfo.country}
        </span>
        <span className="tx-more-infor">
          {`${shippingBt}: ${isSelfCollect ? selfCollectBt : `${standBt} ${generaCurrency(shipping)}`}`}
        </span>
        <span className="tx-more-infor">
          {`${deliveryBt}: ${timeDelivery}`}
        </span>
        <span className="tx-more-infor">
          {billingAddress}
          :
          {' '}
          {isBillAddressSame ? shipInfo.street1 : billingInfo.street1}
          {' '}
          {isBillAddressSame ? shipInfo.postal_code : billingInfo.postal_code}
          {' '}
          {isBillAddressSame ? shipInfo.country : billingInfo.country}
        </span>
      </div>
    );
    const inputAddress = (
      <React.Fragment>
        <div className="div-row">
          <div className="div-header-mini">
            <span style={{ color: '#0D0D0D' }}>
              {shippingAddressBt}
            </span>
          </div>
          <span className={isCheckOutClub ? '' : 'hidden'}>
            <i>{`* ${requireBt}`}</i>
          </span>
        </div>

        <div className={isShowOptionAddress ? 'div-address-choose div-grid' : 'hidden'}>
          <div
            className="div-option-checkout top div-row items-center active"
            onClick={() => { this.setState({ isNewAddress: false }); this.props.onChooseNewAddress(false); }
                  }
          >
            <div className="div-button div-col justify-center items-center" style={isMobile ? { width: '38px', margin: 'auto' } : {}}>
              <div className={classnames('div-col justify-center items-center', isNewAddress ? '' : 'selected')}>
                <button
                  type="button"
                  className={isNewAddress ? '' : 'bt-choose'}
                />
              </div>
            </div>
            <div className="div-name div-col justify-center" style={isMobile ? { width: 'auto' } : {}}>
              <span>
                {saveAddressBt}
              </span>
              <span className={isMobile ? 'text-address' : 'hidden'}>
                {`${currentAddress && currentAddress.street1 ? currentAddress.street1 : ''}${currentAddress && currentAddress.city ? ` ${currentAddress.city}` : ''}${currentAddress && currentAddress.state ? ` ${currentAddress.state}` : ''}${shippingCurrency ? ` ${shippingCurrency.name}` : ''}${currentAddress && currentAddress.postal_code ? ` ${currentAddress.postal_code}` : ''}`}
              </span>
            </div>
            <div className={isMobile ? 'hidden' : 'div-info div-col justify-center'}>
              <span className={isMobile ? 'ml-3 hidden' : 'pr-3'}>
                {`${currentAddress && currentAddress.street1 ? currentAddress.street1 : ''}${currentAddress && currentAddress.city ? ` ${currentAddress.city}` : ''}${currentAddress && currentAddress.state ? ` ${currentAddress.state}` : ''}${shippingCurrency ? ` ${shippingCurrency.name}` : ''}${currentAddress && currentAddress.postal_code ? ` ${currentAddress.postal_code}` : ''}`}
              </span>
            </div>
            <div className="div-button-change div-col items-end mr-3">
              <button type="button" className={listAddress && listAddress.length > 1 ? 'button-bg__none' : 'hidden'} onClick={openShowAddress}>
                {changeBt}
              </button>
            </div>
          </div>
          <div className="div-option-checkout bottom div-row items-center" onClick={() => { this.setState({ isNewAddress: true }); this.props.onChooseNewAddress(true); }}>
            <div className="div-button div-col justify-center items-center" style={isMobile ? { width: '38px', margin: 'auto' } : {}}>
              <div className={classnames('div-col justify-center items-center', isNewAddress ? 'selected' : '')}>
                <button
                  type="button"
                  className={isNewAddress ? 'bt-choose' : ''}
                />
              </div>
            </div>
            <div className="div-name div-col justify-center" style={isMobile ? { width: 'auto' } : {}}>
              <span>
                {newAddressBt}
              </span>
            </div>
            <div className="div-info div-col justify-center">
              <span className={isMobile ? 'hidden' : ''}>
                {addNewAddressBt}
              </span>
            </div>
          </div>
        </div>
        <div className={isNewAddress ? 'note-require' : 'hidden'}>
          <span>
            <i>{`* ${requiredFieldBt}`}</i>
          </span>
        </div>
        <div className={isNewAddress || !isShowOptionAddress ? 'div-col div-shipping-address' : 'hidden'}>
          <AutoCompleteInput
            ref={this.addressRef}
            className={`border-checkout w-100 padding-0 ${listError.includes('street1') ? 'error' : ''}`}
            isSearchCity={false}
            name="street1"
            id="street1"
            onKeyDown={() => {}}
            label=""
            placeholder={`${addressBt}*`}
            country={shipInfo.country || 'sg'}
            value={shipInfo.street1}
            onChangeInput={this.onChangeInputAutoComplete}
            onChange={this.onChangeAutoComplete}
            eventClear={this.clearAutoComplete}
            isError={listError.includes('street1')}
            errorMessage=""
          />
          <Row>
            <Col md={isMobile ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px' } : { paddingRight: '7px' }}>
              <Input
                type="select"
                className="border-checkout w-100"
                placeholder={countryBt}
                name="country"
                onChange={onChange}
              >
                {
                  _.map(listShipping, x => (
                    <option value={x.country.code} selected={(shipInfo.country ? shipInfo.country.toUpperCase() : '') === x.country.code.toUpperCase()}>
                      {x.country.name}
                    </option>
                  ))
                }
              </Input>
            </Col>
            <Col md={isMobile ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px' } : { paddingLeft: '7px' }}>
              <input
                type="text"
                className={`border-checkout w-100 ${listError.includes('city') ? 'error' : ''}`}
                placeholder={cityBt}
                name="city"
                value={shipInfo.city}
                onChange={onChange}
              />
            </Col>
          </Row>

          <Row>
            <Col md="6" xs="6" style={isMobile ? { padding: '0px', paddingRight: '7px' } : { paddingRight: '7px' }}>
              <input
                type="text"
                className={`border-checkout w-100 ${listError.includes('postal_code') ? 'error' : ''}`}
                placeholder={`${zipCodeBt}*`}
                name="postal_code"
                value={shipInfo.postal_code}
                onChange={onChange}
              />

            </Col>
            {
              shipInfo.country === 'us'
                ? (
                  <Col md="6" xs="6" style={isMobile ? { padding: '0px' } : { paddingRight: '15px', paddingLeft: '7px' }}>
                    <input
                      type="text"
                      className={`border-checkout w-100 ${listError.includes('state') ? 'error' : ''}`}
                      placeholder={`${stateBt}`}
                      name="state"
                      value={shipInfo.state}
                      onChange={onChange}
                    />
                  </Col>
                ) : ['in', 'id'].includes(shipInfo.country) ? (
                  <Col md="6" xs="6" style={isMobile ? { padding: '0px' } : { paddingRight: '15px', paddingLeft: '7px' }}>
                    <input
                      type="text"
                      className={`border-checkout w-100 ${listError.includes('pan_card') ? 'error' : ''}`}
                      placeholder={`${shipInfo.country === 'id' ? panCardIndoBt : panCardBt}*`}
                      name="pan_card"
                      value={shipInfo.pan_card}
                      onChange={onChange}
                    />
                    <button
                      style={{
                        position: 'absolute',
                        top: '50%',
                        transform: 'translateY(-50%)',
                        right: '30px',
                      }}
                      id="id-pan-card"
                      className="button-bg__none bt-info"
                      type="button"
                    >
                      <img style={{ width: '15px' }} src={icInfo} alt="info" />
                    </button>
                    <UncontrolledPopover trigger="focus" placement="bottom" target="id-pan-card">
                      <PopoverBody className="popover-checkout">{shipInfo.country === 'id' ? thisInfoIndonesiaBt : thisInfoBt}</PopoverBody>
                    </UncontrolledPopover>
                  </Col>
                ) : shipInfo.country === 'sa' ? (
                  <Col md="6" xs="6" style={isMobile ? { padding: '0px' } : { paddingRight: '15px', paddingLeft: '7px' }}>
                    <span className="info-sa">{messageSABt}</span>
                  </Col>
                ) : undefined
            }
          </Row>
        </div>
        <Label className={(isNewAddress || !isShowOptionAddress) && isLogined ? '' : 'hidden'} check style={{ cursor: 'pointer', marginTop: '18px', marginLeft: '15px' }}>
          <Input
            name="create_address"
            id="idCheckbox"
            type="checkbox"
            className="custom-input-filter__checkbox"
            onChange={onClickCreateAddress}
            checked={shipInfo.create_address}
          />
          {' '}
          <Label
            for="idCheckbox"
            style={{ pointerEvents: 'none' }}
          />
          {saveAddressForFutureBt}
        </Label>
      </React.Fragment>
    );

    const logAccount = (
      <div className={`${isMobile ? 'div-col login-mobile' : 'div-row justify-end login-web'}`}>
        <div>
          <span style={{ fontSize: '14px', color: '#545454', fontWeight: '400' }}>
            {alreadyBt}
          </span>
          <button
            type="button"
            className="button-bg__none"
            style={{ color: '#0B5EC2', textDecoration: 'underline', fontSize: '14px' }}
            onClick={() => emitter.emit('eventOpenLogin')}
          >
            {loginBt}
          </button>
        </div>
        <span className={isCheckOutClub ? '' : 'hidden'}>
          <i>{`* ${thisInfoBt}`}</i>
        </span>
      </div>
    );

    return (
      <div className="step-shipping">
        <div id="anchor-step2" />
        <div className="header-title mb-0">
          <h3>{shippingBt}</h3>
          {!isLogined ? logAccount : <div />}
        </div>
        {/* <HeaderItem
          cmsCommon={cmsCommon}
          stepCheckOut={stepCheckOut}
          isStepDone={isStepDone}
          step={this.props.step}
          title={shippingBt ? shippingBt.toUpperCase() : ''}
          onClick={this.props.onClickHeader}
          htmlBottom={isStepDone ? htmlCenter : undefined}
        /> */}

        <React.Fragment>
          <div className="body-step div-col">
            <div className="div-col div-header-mini">
              <span>
                {shippingMethodyBt}
              </span>
            </div>
            <div className="div-col div-grid">
              {
                isMobile
                  ? (
                    <div
                      onClick={onClickSelfCollect}
                      className={isCheckOutClub || !isSelfCollectCountry ? 'hidden' : 'div-option-checkout top div-col'}
                      style={{ height: 'auto' }}
                    >
                      <div className="div-button div-row  items-center" style={{ height: '41px', width: '100%' }}>
                        <div className={classnames('div-row justify-center items-center', isSelfCollect ? 'selected' : '')} style={{ marginLeft: '8px' }}>
                          <button
                            type="button"
                            className={`${isSelfCollect ? 'bt-choose' : ''}`}
                          />
                        </div>
                        <div className="div-name div-col justify-center" style={{ marginLeft: '8px' }}>
                          <span>
                            {selfBt}
                          </span>
                        </div>
                        <div style={{
                          flex: '1', justifyContent: 'flex-end', display: 'flex', marginRight: '16px',
                        }}
                        >
                          <span>
                            {isCheckOutGift ? '' : freeBt.charAt(0).toUpperCase() + freeBt.slice(1)}
                          </span>
                        </div>

                      </div>
                      <div className="div-info div-col justify-center" style={{ marginLeft: '35px', marginBottom: '8px' }}>
                        <span>
                          {pickupBt}
                        </span>
                        <span>
                          {duxtonBt}
                        </span>
                      </div>
                    </div>
                  ) : (
                    <div
                      onClick={onClickSelfCollect}
                      className={isCheckOutClub || !isSelfCollectCountry ? 'hidden' : 'div-option-checkout top div-row'}
                    >
                      <div className="div-button div-col justify-center items-center" style={isMobile ? { width: '38px', margin: 'auto' } : {}}>
                        <div className={classnames('div-col justify-center items-center', isSelfCollect ? 'selected' : '')}>
                          <button
                            type="button"
                            className={`${isSelfCollect ? 'bt-choose' : ''}`}
                          />
                        </div>
                      </div>
                      <div className="div-name div-col justify-center">
                        <span>
                          {selfBt}
                        </span>
                      </div>
                      <div className="div-info div-col justify-center">
                        <span>
                          {pickupBt}
                        </span>
                        <span>
                          {duxtonBt}
                        </span>
                      </div>
                      <div className="div-price div-col justify-center items-end mr-3">
                        <span>
                          {isCheckOutGift ? '' : freeBt.charAt(0).toUpperCase() + freeBt.slice(1)}
                        </span>
                      </div>
                    </div>
                  )
              }
              <div
                onClick={onClickStandard}
                className={`div-option-checkout ${isCheckOutClub || !isSelfCollectCountry ? (isExpressCountry ? 'top' : 'full-border') : (isExpressCountry ? 'mid' : 'bottom')} div-col active`}
              >
                <div className="div-button div-row justify-center items-center" style={{ width: '100%' }}>
                  <div className={classnames('div-col justify-center items-center', (isMobile ? 'ml-2 mr-2' : 'ml-3 mr-3'), !isSelfCollect && !isExpress ? 'selected' : '')}>
                    <button
                      type="button"
                      className={`${!isSelfCollect && !isExpress ? 'bt-choose' : ''}`}
                    />
                  </div>
                  <div className="div-name div-col justify-center">
                    <span>
                      {standBt}
                    </span>
                  </div>
                  <div className="div-info div-row items-center">
                    <span className={isMobile ? 'ml-3' : ''}>
                      {daysBt}
                    </span>
                    {
                      isBrowser && shippingSelection && parseFloat(shippingSelection?.threshold) > 0 && !isFreeDelivery && (
                        <span className="ml-3 text-yellow">
                          {`${freeShipping} ${generaCurrency(shippingSelection?.threshold)}`}
                        </span>
                      )
                    }
                  </div>
                  <div className="div-price div-col justify-center items-end mr-3">
                    <span>
                      {isCheckOutClub || isCheckOutGift ? '' : isFreeDelivery ? generaCurrency('0.00') : generaCurrency(shippingSelection ? shippingSelection.shipping : '')}
                    </span>
                  </div>
                </div>
                {
                  isMobile && shippingSelection && parseFloat(shippingSelection?.threshold) > 0 && (
                    <span className="text-yellow">
                      {`${freeShipping} ${generaCurrency(shippingSelection?.threshold)}`}
                    </span>
                  )
                }
              </div>
              <div
                onClick={onClickExpress}
                className={classnames('div-col', !isExpressCountry ? 'hidden' : 'bottom-sub')}
                style={{ height: 'auto' }}
              >
                <div className={classnames('div-option-checkout mid div-row items-center', !isSelfCollect && isExpress ? 'bottom-extend' : 'bottom')}>
                  <div className="div-button div-col justify-center items-center" style={isMobile ? { width: '38px', margin: 'auto' } : {}}>
                    <div className={classnames('div-col justify-center items-center', !isSelfCollect && isExpress ? 'selected' : '')}>
                      <button
                        type="button"
                        className={`${!isSelfCollect && isExpress ? 'bt-choose' : ''}`}
                      />
                    </div>
                  </div>
                  <div className="div-name div-col justify-center" style={isMobile ? { width: 'auto' } : {}}>
                    <span>
                      {expressBt}
                    </span>
                  </div>
                  <div className="div-info div-col justify-center">
                    <span className={isMobile ? 'ml-3' : ''}>
                      {samedayBt}
                    </span>
                  </div>
                  <div className="div-price div-col justify-center items-end mr-3">
                    <span>
                      {generaCurrency(shippingSelection ? shippingSelection.shippingExpress : '')}
                    </span>
                  </div>
                </div>
                <div className={classnames('text-extend', !isSelfCollect && isExpress ? '' : 'hidden')}>
                  {
                    _.map(this.props.listExpressTimeslot, d => (
                      <button
                        onClick={(e) => {
                          e.stopPropagation();
                          this.props.onChangeExpressTimeSlot(d);
                        }}
                        type="button"
                        className={classnames('bt-express-option', expressTimeslot === d ? 'active' : '')}
                      >
                        {d}
                      </button>
                    ))
                  }
                </div>
              </div>

            </div>
            {
              !isSelfCollect ? (
                (inputAddress)
              ) : (<div />)
            }
            {
              isShowCreateAccount
                ? (
                  <div className={isMobile ? 'mt-25 div-row items-center' : 'mt-25 div-row items-center'} style={{ marginLeft: '20px' }}>
                    <Label check style={{ cursor: 'pointer' }}>
                      <Input
                        name="create_account"
                        id="idCheckbox"
                        type="checkbox"
                        className="custom-input-filter__checkbox"
                        // style={{ height: '42px' }}
                        onChange={onClickCreateAccount}
                        checked={shipInfo.create_account}
                      />
                      {' '}
                      <Label
                        for="idCheckbox"
                        style={{ pointerEvents: 'none' }}
                      />
                      {createAccount}
                    </Label>
                    <ButtonInfo cmsCommon={cmsCommon} />
                  </div>
                ) : (<div />)
            }
            <BillingAddress
              listError={listError}
              cmsCommon={cmsCommon}
              buttonBlock={buttonBlock}
              onChange={onChangeBillingInfo}
              billingInfo={billingInfo}
              shipInfo={shipInfo}
              listShipping={listShipping}
              isBillAddressSame={isBillAddressSame}
              onClickCheckBillAdress={onClickCheckBillAdress}
            />
            {
              isShowPackagingOption ? <PackagingOption selectPackagingOption={selectPackagingOption} cmsCommon={cmsCommon} selectGiftWrap={this.props.selectGiftWrap} buttonBlock={buttonBlock} /> : (<div />)
            }
            <div className="div-list-button">
              <ButtonCT
                name={returnToCustomerBt}
                className="bt-text"
                onClick={() => this.props.prevStepCheckOut(this.props.step - 1)}
              />
              <ButtonCT
                name={isHasFreeGift ? continuteGiftBt : continutePaymentBt}
                className="bt-next"
                onClick={onClickContinute}
              />
            </div>
          </div>
        </React.Fragment>
      </div>
    );
  }
}

ShippingItemV2.propTypes = {
  stepCheckOut: PropTypes.number.isRequired,
  isStepDone: PropTypes.bool.isRequired,
  isSelfCollect: PropTypes.bool.isRequired,
  onClickHeader: PropTypes.func.isRequired,
  onClickSelfCollect: PropTypes.func.isRequired,
  onClickStandard: PropTypes.func.isRequired,
  shipInfo: PropTypes.shape().isRequired,
  onChange: PropTypes.func.isRequired,
  onClickContinute: PropTypes.func.isRequired,
  listShipping: PropTypes.arrayOf().isRequired,
  optionTime: PropTypes.arrayOf().isRequired,
  onChangeTimeDelivery: PropTypes.func.isRequired,
  isBillAddressSame: PropTypes.bool.isRequired,
  onClickCheckBillAdress: PropTypes.func.isRequired,
  shipping: PropTypes.string.isRequired,
  timeDelivery: PropTypes.string.isRequired,
  cms: PropTypes.shape().isRequired,
  isShowPackagingOption: PropTypes.bool.isRequired,
  selectPackagingOption: PropTypes.func.isRequired,
  isShowCreateAccount: PropTypes.bool.isRequired,
  onClickCreateAccount: PropTypes.func.isRequired,
  listError: PropTypes.arrayOf().isRequired,
  isCheckOutClub: PropTypes.bool.isRequired,
  buttonBlock: PropTypes.arrayOf().isRequired,
  onClickExpress: PropTypes.func.isRequired,
};

export default ShippingItemV2;
