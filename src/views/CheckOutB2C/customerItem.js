import React, { Component, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Row, Col, Label, Input,
} from 'reactstrap';
import InputMask from 'react-input-mask';
import HeaderItem from './headerItems';
import emitter from '../../Redux/Helpers/eventEmitter';
import {
  getNameFromCommon, getCmsCommon, getNameFromButtonBlock, trackGTMCheckOut,
} from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';
import RadioButton from '../../components/Input/radioButton';
import ButtonCT from '../../components/ButtonCT';
import icNext from '../../image/icon/ic-next.svg';


function CustomerItem(props) {
  useEffect(() => {
    trackGTMCheckOut(2, props.basket);
  }, []);

  useEffect(() => {
    console.log('isStepDone', props.isStepDone);
  }, [props.isStepDone]);

  const {
    stepCheckOut, isStepDone, shipInfo, onChange, buttonBlock, login, listError, isCheckOutClub,
  } = props;
  const isLogined = login && login.user && login.user.id;
  const cmsCommon = getCmsCommon(props.cms);
  const alreadyBt = getNameFromCommon(cmsCommon, 'Already_have_an_account');
  const loginBt = getNameFromCommon(cmsCommon, 'Log_in');
  const customerBt = getNameFromCommon(cmsCommon, 'CUSTOMER');
  const firstNameBt = getNameFromCommon(cmsCommon, 'FIRST_NAME');
  const lastNameBt = getNameFromCommon(cmsCommon, 'LAST_NAME');
  const thisInfoBt = getNameFromButtonBlock(buttonBlock, 'This_information');
  const birthdateBt = getNameFromCommon(cmsCommon, 'Birth_Date');
  const genderBt = getNameFromCommon(cmsCommon, 'Gender');
  const maleBt = getNameFromCommon(cmsCommon, 'MALE');
  const femaleBt = getNameFromCommon(cmsCommon, 'FEMALE');
  const weLoveBt = getNameFromCommon(cmsCommon, 'We’d love to surprise you on your Birthday');
  const optionalBt = getNameFromCommon(cmsCommon, 'Optional');
  const mrBt = getNameFromCommon(cmsCommon, 'mr');
  const mrsBt = getNameFromCommon(cmsCommon, 'mrs');
  const continueBt = getNameFromCommon(cmsCommon, 'CONTINUE');
  const phoneBt = getNameFromCommon(cmsCommon, 'PHONE');

  const htmlBottom = (
    <div className="h-100 div-col justify-center ml-5">
      <span className="tx-more-infor">
        {shipInfo.email}
      </span>
    </div>
  );
  const logAccount = (
    <div className={`${isMobile ? 'div-col login-mobile' : 'div-row justify-end'}`}>
      <div>
        <span style={{ fontSize: '14px', color: '#545454', fontWeight: '400' }}>
          {alreadyBt}
        </span>
        <button
          type="button"
          className="button-bg__none"
          style={{ color: '#0B5EC2', textDecoration: 'underline', fontSize: '14px' }}
          onClick={() => emitter.emit('eventOpenLogin')}
        >
          {loginBt}
        </button>
      </div>
      <span className={isCheckOutClub ? '' : 'hidden'}>
        <i>{`* ${thisInfoBt}`}</i>
      </span>
    </div>
  );
  return (
    <React.Fragment>
      <div id="anchor-step1" />
      <HeaderItem
        cmsCommon={cmsCommon}
        stepCheckOut={stepCheckOut}
        isStepDone={isStepDone}
        step={1}
        title={customerBt}
        onClick={props.onClickHeader}
        htmlCenter={!isLogined && !isStepDone ? logAccount : undefined}
        htmlBottom={isStepDone ? htmlBottom : undefined}
      />
      {
          !isStepDone && (
            <div className="body-step">
              <React.Fragment>
                <div className="div-content div-col">
                  <div className="div-male div-row mt-3">
                    <RadioButton
                      name="gender"
                      label={mrBt}
                      value="Male"
                      selected={shipInfo.gender === 'Male'}
                      onChange={props.onChangeGenderSelect}
                    />
                    <RadioButton
                      name="gender"
                      label={mrsBt}
                      selected={shipInfo.gender === 'Female'}
                      className="ml-5"
                      value="Female"
                      onChange={props.onChangeGenderSelect}
                    />
                  </div>
                  <Row className={isTablet ? { margin: '0px' } : {}}>
                    <Col md={isTablet ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px' } : { paddingRight: '7px' }}>
                      <input
                        type="text"
                        className={`border-checkout w-100 ${listError.includes('firstName') ? 'error' : ''}`}
                        placeholder={`${firstNameBt}*`}
                        name="firstName"
                        value={shipInfo.firstName}
                        onChange={onChange}
                      />
                    </Col>
                    <Col md={isTablet ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px', marginTop: '25px' } : { paddingLeft: '7px' }}>
                      <input
                        type="text"
                        className={`border-checkout w-100 ${listError.includes('lastName') ? 'error' : ''}`}
                        placeholder={`${lastNameBt}*`}
                        name="lastName"
                        value={shipInfo.lastName}
                        onChange={onChange}
                      />
                    </Col>
                  </Row>
                  <Row className={isTablet ? { margin: '0px' } : {}}>
                    <Col md="12" style={isMobile ? { padding: '0px' } : { paddingRight: '15px' }}>
                      <input
                        type="email"
                        className={`border-checkout w-100 ${listError.includes('email') ? 'error' : ''}`}
                        placeholder="Email*"
                        name="email"
                        value={shipInfo.email}
                        onChange={onChange}
                      />
                    </Col>
                  </Row>
                  <Row className={isTablet ? { margin: '0px' } : {}}>
                    <Col md={isTablet ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px' } : { paddingRight: '7px' }}>
                      <InputMask
                        className={`border-checkout w-100 mb-2 ${listError.includes('dob') ? 'error' : ''}`}
                        type="text"
                        name="dob"
                        placeholder={`${birthdateBt} (MM/DD/YYYY)`}
                        mask="99/99/9999"
                        maskChar={null}
                        defaultValue={shipInfo.dob}
                        value={shipInfo.dob}
                        onChange={onChange}
                      />
                      <span className="des-birth">{weLoveBt}</span>
                      <span className={isCheckOutClub ? 'hidden' : 'option-birth'}>{`(${optionalBt})`}</span>

                    </Col>
                    <Col className="relative" md={isTablet ? '12' : '6'} xs="12" style={isMobile ? { padding: '0px' } : { paddingLeft: '7px' }}>
                      <input
                        type="text"
                        className={`border-checkout w-100 ${listError.includes('phone') ? 'error' : ''}`}
                        placeholder={`${phoneBt}*`}
                        name="phone"
                        value={shipInfo.phone}
                        onChange={onChange}
                      />
                    </Col>
                  </Row>
                </div>
              </React.Fragment>
              <div className="div-row justify-end">
                <ButtonCT
                  name={continueBt}
                  className="bt-next"
                  iconRight={icNext}
                  onClick={props.onClickContinute}
                />
              </div>
            </div>
          )
        }
    </React.Fragment>
  );
}

export default CustomerItem;
