import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import {
  getImageDisplay, getUrlGotoProduct, getAltImage, generaCurrency, generateUrlWeb,
} from '../../Redux/Helpers/index';
import auth from '../../Redux/Helpers/auth';
import { isMobile, isTablet } from '../../DetectScreen';

class CheckOutB2CItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: this.props.data.quantity,
      oldQuantity: this.props.data.quantity,
    };
    this.timeOutCall = undefined;
  }

  onClickDelete = () => {
    if (this.props.deleteProductBasket) {
      const { idCart, data } = this.props;
      this.props.deleteProductBasket({
        idCart,
        item: data,
      });
    }
  }

  onChangeQuantity = (e) => {
    const { value } = e.target;
    this.setState({ quantity: value });
    if (this.timeOutCall) {
      clearTimeout(this.timeOutCall);
      this.timeOutCall = undefined;
    }
    this.timeOutCall = setTimeout(() => {
      this.updateItem(value);
    }, 1000);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { data } = nextProps;
    const objectReturn = {};
    if (data && data.quantity !== prevState.oldQuantity) {
      _.assign(objectReturn, { quantity: data.quantity, oldQuantity: data.quantity });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  gotoProduct = (e) => {
    const { data } = this.props;
    const dataPro = getUrlGotoProduct(data);
    let urlPro = dataPro.url;
    const { meta } = data;
    if (meta && meta.url) {
      urlPro = meta.url;
    }
    if (urlPro) {
      const { image, name, item } = data;
      const custome = dataPro.isPerfume ? { image, name: name !== item.name ? name : undefined } : undefined;
      this.props.history.push(generateUrlWeb(urlPro), { custome });
    }
  }

  updateItem = (quantity) => {
    const { idCart } = this.props;
    const data = _.assign({}, this.props.data);
    data.quantity = parseFloat(quantity, 10);
    data.total = parseFloat(data.quantity, 10) * parseFloat(data.price, 10);
    if (this.props.updateProductBasket) {
      this.props.updateProductBasket({
        idCart,
        item: data,
      });
    }
  }

  render() {
    const {
      name, item, price, total, image, is_display_name: isDisplayName,
    } = this.props.data;
    const isPerfume = item.product.type.name === 'Perfume';
    const { quantity } = this.state;
    const imgDisplay = getImageDisplay(this.props.data);
    // const imgDisplay = imageBottle ? imageBottle.image : '';
    return (
      <div className="div-col pt-5 pb-5 bb">
        <div className="div-row">
          <div className="div-row w-60">
            <button
              onClick={this.gotoProduct}
              type="button"
              style={
            {
              width: isMobile && !isTablet ? '54px' : '120px',
              display: 'flex',
              flexDirection: 'column',
            }}
              className="button-bg__none"
            >
              <img loading="lazy" src={imgDisplay} alt={getAltImage(imgDisplay)} />
            </button>
            <div className="div-col ml-3 mr-3 items-start" style={{ flex: '1' }}>
              <span style={{ color: '#000' }}>
                {name}
              </span>
              <span
                style={{ color: '#c4c4c4' }}
                className="mt-3"
              >
                {image ? 'Bottle personalization' : ''}
              </span>
              <span
                style={{ color: '#c4c4c4' }}
                className="mb-3"
              >
                {name && name !== item.name ? 'Perfume Name' : ''}
              </span>
            </div>
          </div>
          <div className="w-10 mr-2">
            <span>
              {generaCurrency(price)}
            </span>
          </div>
          <div className="w-10">
            <input
              type="number"
              value={quantity}
              style={{ width: '40px' }}
              onChange={this.onChangeQuantity}
            />
          </div>
          <div className="w-20 div-row justify-end">
            <span>
              {generaCurrency(total)}
            </span>
          </div>
        </div>
        <div className="div-row ml-3">
          <div style={{ width: isMobile && !isTablet ? '54px' : '120px' }} />
          <button
            type="button"
            className="underline button-bg__none pa0"
            onClick={this.onClickDelete}
          >
            Remove
          </button>
          <button
            type="button"
            className={isPerfume ? 'underline button-bg__none pa0 ml-3' : 'hidden'}
            onClick={e => this.gotoProduct(e)}
          >
            Customize bottle
          </button>
        </div>
      </div>
    );
  }
}

CheckOutB2CItem.propTypes = {
  data: PropTypes.shape(PropTypes.object).isRequired,
  idCart: PropTypes.number.isRequired,
  deleteProductBasket: PropTypes.func.isRequired,
  updateProductBasket: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default withRouter(CheckOutB2CItem);
