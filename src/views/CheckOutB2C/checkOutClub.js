/* eslint-disable consistent-return */
/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import {
  Col, Row, Input,
} from 'reactstrap';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as EmailValidator from 'email-validator';
import MetaTags from 'react-meta-tags';
import smoothscroll from 'smoothscroll-polyfill';

import moment from 'moment';
import HeaderHomePage from '../../components/HomePage/header';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import {
  CHECK_PROMOTION_CODE, GET_ADDRESS_URL, CREATE_ADDRESS_URL, CHECKOUT_FUNCTION_URL,
  UPDATE_ADDRESS_GUEST_URL, PAYPAL_GENERATE_URL, STRIPE_GENERATE_URL, GET_SHIPPING_URL,
  CHECK_EMAIL_LOGIN_URL, GET_BASKET_URL, PRODUCTS_V2_URL, GET_LIST_ADDRESS_URL,
} from '../../config';
import {
  preCheckOutBasketSubscription,
} from '../../Redux/Actions/basket';
import { createBasketSubscription } from '../../Redux/Actions/basketSubscription';

import {
  getNameFromCommon, getCmsCommon, fetchCMSHomepage, getSEOFromCms, generaCurrency, isCheckNull,
  googleAnalitycs, scrollTop, hiddenChatIcon, visibilityChatIcon, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady, trackGTMCheckOut, gotoShopHome,
  segmentClub21GSubscriptionCheckoutStarted,
  segmentClub21GShippingInfoEntered,
  segmentClub21GPaymentInfoEntered,
  segmentClub21GCheckoutCustomerDetailsEntered,
} from '../../Redux/Helpers';
import '../../styles/checkout.scss';
import CardItem from '../../components/B2B/CardItem';
import CustomerItem from './customerItem';
import ShippingItem from './shippingItems';
import PaymentItem from './paymentItem';
import PaymentItemClub from './paymentItemClub';
import auth from '../../Redux/Helpers/auth';
import addCmsRedux from '../../Redux/Actions/cms';
import icSub from '../../image/icon/ic-sub.svg';
import icPlus from '../../image/icon/ic-plus.svg';
import PopUpSaveAdsress from './popUpSaveAddress';
import FooterCheckoutMobile from './footerCheckOutMobile';
import { isBrowser, isMobile, isTablet } from '../../DetectScreen';
import ModalCheckoutMobile from './modalCheckOutMobile';
import icClose from '../../image/icon/ic-close-region.svg';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';
import { addScriptStripe } from '../../Utils/addStripe';
import { addScriptGoogleAPI } from '../../Utils/addScriptGoogle';

const getCms = (cms) => {
  if (!cms) {
    return { seo: {} };
  }
  const seo = getSEOFromCms(cms);
  const { body } = cms;
  const textBlock = _.filter(body, x => x.type === 'text_block');
  const imageBlockIcon = _.filter(body, x => x.type === 'image_block');
  const buttonBlock = _.filter(body, x => x.type === 'button_block');
  const imageTxtBlock = _.filter(body, x => x.type === 'imagetxt_block');
  return {
    seo, textBlock, imageBlockIcon, buttonBlock, imageTxtBlock,
  };
};

const addressDefaul = {
  street1: undefined,
  city: undefined,
  state: undefined,
  country: undefined,
  postal_code: undefined,
  pan_card: undefined,
};

class CheckOutClub extends Component {
  constructor(props) {
    super(props);
    this.state = {
      noteText: '',
      promotion: {},
      payMethod: 'stripe',
      stepCheckOut: 1,
      isStep1Done: false,
      isStep2Done: false,
      isSelfCollect: false,
      timeDelivery: 'Any Time',
      isBillAddressSame: true,
      seo: {},
      imageBlockIcon: [],
      buttonBlock: [],
      imageTxtBlock: [],
      isShowCreateAccount: false,
      listError: [],
      isOpenNote: false,
      isOPenPromo: false,
      isShowChoiseAddress: false,
      listAddress: [],
      isShowOptionAddress: false,
      isShowFooter: true,
    };
    this.shipping = 0;
    this.shipInfo = {};
    this.billingInfo = _.cloneDeep(addressDefaul);
    this.cardInfo = {
      name: '',
      serial: '',
      expireDate: '',
      security: '',
    };
    this.timeoutShipInfo = undefined;
    this.checkOutData = undefined;
    this.isCheckOutClicked = false;
    this.optionTime = [
      'Any Time',
      '9.00 am to 12.00 pm',
      '12.00 pm to 3.00 pm',
      '3.00 pm to 6.00 pm',
      '6.00 pm to 10.00 pm',
    ];
    this.currentAddress = {};
  }

  componentDidMount() {
    setPrerenderReady();
    smoothscroll.polyfill();
    scrollTop();
    addScriptStripe();
    addScriptGoogleAPI();
    if (isMobile) {
      document.addEventListener('scroll', this.trackScrolling);
      hiddenChatIcon();
    }
    // document.body.appendChild(script);
    this.fetchCmsCheckOut();
    this.fetchShippingInfo();
    const { login } = this.props;
    // if (login && login.user && login.user.id) {
    this.fetchDataForSubscription(login?.user?.id);
    segmentClub21GSubscriptionCheckoutStarted();
    // }
  }

  componentWillUnmount() {
    removeLinkHreflang();
    if (isMobile) {
      document.removeEventListener('scroll', this.trackScrolling);
      visibilityChatIcon();
    }
  }

  isBottom = el => (
    el.getBoundingClientRect().bottom <= window.innerHeight
  )

  trackScrolling = () => {
    const wrappedElement = document.getElementById('id-promotion');
    if (this.isBottom(wrappedElement) && this.state.isShowFooter) {
      this.setState({ isShowFooter: false });
    } else if (!this.isBottom(wrappedElement) && !this.state.isShowFooter) {
      this.setState({ isShowFooter: true });
    }
  };

  componentDidUpdate = (prevProps) => {
    const { basketSubscription, login } = this.props;
    delete basketSubscription.date_modified;
    if (JSON.stringify(basketSubscription) !== JSON.stringify(prevProps.basketSubscription)) {
      this.preCheckOut();
    }
    if (login !== prevProps.login) {
      if (login && login.user && login.user.id) {
        this.fetchDataForSubscription(login.user.id);
        // console.log('moment(login.user.date_of_birth)', moment(login.user.date_of_birth).format('MM/DD/YYYY'));
        this.shipInfo.name = `${login.user.first_name} ${login.user.last_name}`;
        this.shipInfo.firstName = login.user.first_name;
        this.shipInfo.lastName = login.user.last_name;
        this.shipInfo.email = login.user.email;
        this.shipInfo.phone = login.user.phone;
        this.shipInfo.dob = login.user.date_of_birth ? moment(login.user.date_of_birth).format('MM/DD/YYYY') : '';
        this.shipInfo.gender = login.user.gender ? login.user.gender || 'Male' : 'Male';
        this.forceUpdate();
      }
    }
  }

  fetchCmsCheckOut = () => {
    const { cms } = this.props;
    const cmsCheckOut = _.find(cms, x => x.title === 'CheckOut');
    if (!cmsCheckOut) {
      fetchCMSHomepage('checkout').then((result) => {
        const cmsData = result;
        this.props.addCmsRedux(cmsData);
        const {
          seo, textBlock, imageBlockIcon, buttonBlock, imageTxtBlock,
        } = getCms(cmsData);
        this.setState({
          seo, textBlock, imageBlockIcon, buttonBlock, imageTxtBlock,
        });
      });
    } else {
      const {
        seo, textBlock, imageBlockIcon, buttonBlock, imageTxtBlock,
      } = getCms(cmsCheckOut);
      this.setState({
        seo, textBlock, imageBlockIcon, buttonBlock, imageTxtBlock,
      });
    }
  }

  onChangePromo = (e) => {
    const { value } = e.target;
    this.setState({ promotionCode: value });
  }

  onChangeGenderSelect = (value, name) => {
    this.shipInfo.gender = value;
    this.forceUpdate();
  }

  onChangeBillingInfo = (e) => {
    const { value, name } = e.target;
    _.assign(this.billingInfo, { [name]: value });
    this.forceUpdate();
  };

  onChange = (e) => {
    const { value, name } = e.target;
    this.shipInfo[name] = value;
    this.forceUpdate();
    this.setState({ listError: [] });
    if (name === 'firstName' || name === 'lastName') {
      this.shipInfo.name = `${this.shipInfo.firstName} ${this.shipInfo.lastName}`;
    }
    if (name === 'email') {
      if (this.timeOutEmail) {
        clearTimeout(this.timeOutEmail);
        this.timeOutEmail = null;
      }
      this.timeOutEmail = setTimeout(this.checkEmailExist, 2000);
    }
    if (name === 'country') {
      this.preCheckOut();
      this.forceUpdate();
    }
  }

  // onCheckPromo = () => {
  //   const { login, basketSubscription: basket } = this.props;
  //   const isLogined = login && login.user && login.user.id;
  //   const { total } = basket;
  //   const cmsCommon = getCmsCommon(this.props.cms);
  //   const proCodeNotAvaiBt = getNameFromCommon(cmsCommon, 'Promotion_code_is_not_avaialble.');
  //   const proCodeAvaiBt = getNameFromCommon(cmsCommon, 'Promotion_code_is_avaialble.');

  //   const { promotionCode } = this.state;
  //   const options = {
  //     url: `${CHECK_PROMOTION_CODE}?code=${promotionCode}&total=${total}&email=${this.shipInfo.email}`,
  //     method: 'GET',
  //   };
  //   fetchClient(options, true).then((result) => {
  //     const { isError } = result;
  //     if (isError) {
  //       toastrError(proCodeNotAvaiBt);
  //       this.setState({ promotion: undefined });
  //     } else {
  //       this.setState({ promotion: result, promotionCode: result.code }, () => {
  //         this.preCheckOut();
  //       });
  //       toastrSuccess(proCodeAvaiBt);
  //     }
  //   }).catch((err) => {
  //     toastrError(err.message);
  //   });
  // }

  assignUserInBasket = async () => {
    const { login, basketSubscription } = this.props;
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basketSubscription.id),
      method: 'PUT',
      body: {
        user: login.user.id,
      },
    };
    return fetchClient(option);
  };

  onCheckOutFunc = async () => {
    const { basketSubscription: basket, login } = this.props;
    if (this.isCheckOutClicked || !basket.id) {
      return;
    }

    // tracking GTM
    trackGTMCheckOut(8, basket);
    const isLogined = login && login.user && login.user.id;
    if (isLogined) {
      try {
        const result = await this.assignUserInBasket();
        if (result && result.isError) {
          toastrError(result.message);
          return;
        }
      } catch (error) {
        toastrError(error.message);
        return;
      }
    }
    const isOnlyGift = this.isAllGift(basket.items);
    if (!isOnlyGift) {
      if (!this.checkContinuteStep1() || !this.checkContinuteStep2()) {
        return;
      }
    } else if (!this.checkContinuteStep1()) {
      return;
    }
    const {
      payMethod, noteText, isSelfCollect, promotion, isBillAddressSame,
    } = this.state;
    if (!this.shipInfo.idAddress) {
      const address = isLogined && this.shipInfo.create_address ? await this.createAddressWithUser(this.shipInfo) : await this.createAddress(this.shipInfo);
      if (!address) {
        toastrError('Check Out Error');
        return;
      }
      this.shipInfo.idAddress = address.id;
    }

    let idbillingAddress;
    if (this.state.isBillAddressSame) {
      const resultAddress = await this.createAddress(this.shipInfo);
      idbillingAddress = resultAddress?.id;
    } else {
      const resultAddress = await this.createAddress(_.assign(this.shipInfo, this.billingInfo));
      idbillingAddress = resultAddress?.id;
    }

    segmentClub21GPaymentInfoEntered(2, this.props.basketSubscription, this.getShippingMethod(), this.state.payMethod);

    const options = {
      url: CHECKOUT_FUNCTION_URL.replace('{id}', basket.id),
      method: 'POST',
      body: {
        payment_method: payMethod,
        shipping_address: this.shipInfo.idAddress,
        note: noteText,
        billing_address: idbillingAddress,
        shipping: this.shipping,
        is_self_collect: isSelfCollect,
        personality_choice: this.idPersonalityChoice,
        packaging: this.packaging,
        gender: this.shipInfo.gender,
      },
    };
    if (promotion && promotion.id) {
      options.body.promos = [promotion.id];
    }
    if (this.props.refPoint !== '') {
      options.body.ref = this.props.refPoint;
    }
    if (!isLogined) {
      options.body.meta = {
        guest: {
          name: this.shipInfo.name,
          email: this.shipInfo.email,
          cart: basket.id,
          create_account: true,
        },
      };
    }
    this.isCheckOutClicked = true;
    fetchClient(options, true).then((result) => {
      const { isError, order, message } = result;
      this.isCheckOutClicked = false;
      if (!isError) {
        if (order) {
          this.props.history.push(generateUrlWeb(`/pay/success/${order}`));
          return;
        }
        if (payMethod === 'paypal') {
          const htmlProcess = `${result.substring(0, 6)}id='id-pay'${result.substring(6, result.length)}`;
          const s = document.getElementById('div-button');
          s.innerHTML = htmlProcess;
          const idForm = document.getElementById('id-pay');
          idForm.submit();
        } else {
          this.redirectStripe(result);
        }
      } else {
        throw (message);
      }
    }).catch((err) => {
      toastrError(err);
      this.isCheckOutClicked = false;
    });
  }

  updateAddress = (id, data) => {
    if (!id) {
      return;
    }
    const options = {
      url: UPDATE_ADDRESS_GUEST_URL.replace('{id}', id),
      method: 'PUT',
      body: data,
    };
    fetchClient(options, true).then((result) => {
      console.log('result', result);
    });
  }

  paypalGenerate = (order, total) => {
    const defaultHeaders = {
      'Content-Type': 'application/json',
    };
    const options = {
      method: 'POST',
      body: JSON.stringify({
        order,
        total,
      }),
      headers: defaultHeaders,
    };
    fetch(new Request(PAYPAL_GENERATE_URL, options))
      .then(result => result.text()).then((html) => {
        const htmlProcess = `${html.substring(0, 6)}id='id-pay'${html.substring(6, html.length)}`;
        const s = document.getElementById('div-button');
        s.innerHTML = htmlProcess;
        const idForm = document.getElementById('id-pay');
        idForm.submit();
        // this.setState({ formPay: htmlProcess, isShowFormPay: true });
      }).catch((err) => {
        toastrError(err);
      });
  }

  stripeGenerate = (order, total) => {
    const options = {
      url: STRIPE_GENERATE_URL,
      method: 'POST',
      body: {
        order,
        total,
      },
    };
    fetchClient(options)
      .then((result) => {
        // const s = document.createElement('div');
        // s.innerHTML = html;
        // this.instance.appendChild(s);
        this.redirectStripe(result);
        // this.setState({ formPay: result, isShowFormPay: true });
      }).catch((err) => {
        toastrError(err);
      });
  }

  redirectStripe = (formPay) => {
    const { key, session } = formPay;
    const stripe = window.Stripe(key);
    stripe.redirectToCheckout({
      // Make the id field from the Checkout Session creation API response
      // available to this file, so you can provide it as parameter here
      // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
      sessionId: session,
    }).then((result) => {
      console.log('result', result);
    });
  }

  initalData = (address) => {
    const { login } = this.props;
    const defaults = {
      // idAddress: address ? address.id : undefined,
      firstName: login && login.user ? `${login.user.first_name}` : '',
      lastName: login && login.user ? `${login.user.last_name}` : '',
      dob: login && login.user && login.user.date_of_birth ? moment(login.user.date_of_birth).format('MM/DD/YYYY') : '',
      gender: login && login.user && login.user.gender ? login.user.gender : 'Male',
      name: login && login.user ? `${login.user.first_name} ${login.user.last_name}` : '',
      street1: address ? address.street1 : '',
      street2: address ? address.street2 : '',
      apartment: address ? address.apartment : '',
      state: address ? address.state : '',
      city: address ? address.city : '',
      postal_code: address ? address.postal_code : '',
      country: address ? address.country.code : auth.getCountry(),
      email: login && login.user ? login.user.email : auth.getEmail(),
      phone: address ? address.phone : '',
      billStreet1: '',
      billPostalCode: '',
      billPhone: '',
      billCountry: auth.getCountry(),
      create_account: true,
      create_address: true,
    };
    this.shipInfo = defaults;
    this.billingInfo.country = this.shipInfo.country;
    this.preCheckOut();
    this.forceUpdate();
    this.fetchSubscriptionItem();
  }

  isAllGift = (data) => {
    const ele = _.find(data, d => d.item.product.type.name !== 'Gift');
    return !ele;
  }

  preCheckOut = () => {
    const { country, email } = this.shipInfo;
    const { id, items } = this.props.basketSubscription;
    const { isSelfCollect, promotion } = this.state;
    if (!items || items.length === 0) {
      if (!id) {
        return;
      }
      return;
    }
    const isOnlyGift = this.isAllGift(items);
    if (id) {
      const data = {
        id,
        body: isSelfCollect || isOnlyGift ? {} : {
          shipping_address: {
            country: country ? country.toLowerCase() : '',
          },
        },
      };
      if (promotion && promotion.id) {
        data.body.promos = [promotion.id];
      }
      this.props.preCheckOutBasketSubscription(data);
    }
  }

  isCheckCountry = (country) => {
    const shipMoney = _.find(this.listShipping, d => d.country.code === country);
    return !!shipMoney;
  }

  updateShipping = (country) => {
    const shipMoney = _.find(this.listShipping, d => d.country.code === country);
    let shipping = 0;
    if (shipMoney) {
      const {
        subtotal,
      } = this.props.basketSubscription;

      if (subtotal >= shipMoney.threshold) {
        shipping = 0;
      } else {
        shipping = shipMoney.shipping;
      }
    } else {
      shipping = 0;
    }
    return shipping;
  }

  fetchSubscriptionItem = () => {
    const options = {
      url: PRODUCTS_V2_URL.replace('{type}', 'subscription'),
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchDataForSubscription = async (userId) => {
    const { basketSubscription } = this.props;
    if (basketSubscription && basketSubscription.id) {
      return;
    }
    try {
      const data = {
        userId,
      };
      await this.props.createBasketSubscription(data);
    } catch (error) {
      toastrError(error.message);
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    }
  }

  fetchShippingInfo = () => {
    const options = {
      url: GET_SHIPPING_URL,
      method: 'GET',
    };
    fetchClient(options).then((results) => {
      if (results && !results.isError) {
        _.forEach(results, (x) => {
          x.name = x.country.name;
        });
        this.listShipping = _.orderBy(results, 'name', 'asc');
        this.fetchAdress();
      } else {
        toastrError('Get list shipping');
      }
    }).catch((err) => {
      console.log('err', err);
      toastrError('Get list shipping');
    });
  }

  // fetchAdress = () => {
  //   if (!this.props.login) {
  //     return;
  //   }
  //   const { user } = this.props.login;
  //   if (user && user.id) {
  //     const options = {
  //       url: GET_ADDRESS_URL.replace('{user_pk}', user.id),
  //       method: 'GET',
  //     };
  //     fetchClient(options, true).then((result) => {
  //       const address = _.find((result), x => x.is_default_baddress);
  //       if (!address && result.length > 0) {
  //         this.initalData(result[result.length - 1]);
  //         this.currentAddress = result[result.length - 1];
  //         return;
  //       }
  //       this.initalData(address);
  //       this.currentAddress = address;
  //     }).catch((err) => {
  //       toastrError(err);
  //     });
  //   } else {
  //     this.initalData();
  //   }
  // }

  fetchAdress = () => {
    const dataUser = _.isEmpty(this.props.login) ? auth.getLogin() : this.props.login;
    if (dataUser && dataUser.user && dataUser.user.id) {
      const option = {
        url: GET_LIST_ADDRESS_URL,
        method: 'GET',
      };
      fetchClient(option, true).then((result) => {
        if (result && !result.isError) {
          this.setState({ listAddress: result, isShowOptionAddress: result && result.length > 0 });
          const address = _.find((result), x => x.is_default_baddress);
          if (!address && result.length > 0) {
            this.initalData(result[result.length - 1]);
            this.currentAddress = result[result.length - 1];
            return;
          }
          this.initalData(address);
          this.currentAddress = address;
          return;
        }
        throw new Error(result.message);
      }).catch((error) => {
        console.log('error: ', error);
      });
    } else {
      this.setState({ isShowOptionAddress: false });
      this.initalData();
    }
  }

  isAddressSame = () => {
    if (_.isEmpty(this.currentAddress)) {
      return false;
    }
    const {
      country, city, email, postal_code, phone, street1, state,
    } = this.currentAddress;
    if (this.shipInfo.country !== (country ? country.code : undefined)) {
      return false;
    }
    if (this.shipInfo.city !== city && !_.isEmpty(this.shipInfo.city && !_.isEmpty(city))) {
      return false;
    }
    if (this.shipInfo.email !== email) {
      return false;
    }
    if (this.shipInfo.postal_code !== postal_code) {
      return false;
    }
    if (this.shipInfo.phone !== phone) {
      return false;
    }
    if (this.shipInfo.street1 !== street1) {
      return false;
    }
    if (this.shipInfo.state !== state && !_.isEmpty(this.shipInfo.state && !_.isEmpty(state))) {
      return false;
    }
    return true;
  }

  validityAddress = (data) => {
    // eslint-disable-next-line no-restricted-syntax
    for (const key in data) {
      if (!data[key] && key !== 'idAddress') {
        return false;
      }
    }
    return true;
  }

  createAddressWithUser = async (body) => {
    const { user } = this.props.login;
    if (user && user.id) {
      _.assign(body, { is_active: this.shipInfo.create_address });
      const options = {
        url: GET_LIST_ADDRESS_URL,
        method: 'POST',
        body,
      };
      try {
        const address = await fetchClient(options, true);
        return address;
      } catch (error) {
        console.log('error', error);
      }
      return null;
    }
  }

  createAddress = async (body) => {
    const options = {
      url: CREATE_ADDRESS_URL,
      method: 'POST',
      body,
    };
    try {
      const address = await fetchClient(options);
      return address;
    } catch (error) {
      console.log('error', error);
    }
    return null;
  }

  onChangeSelfCollect = () => {
    const { isSelfCollect } = this.state;
    this.setState({ isSelfCollect: !isSelfCollect });
  }

  onChangeTimeDelivery = (e) => {
    const { value } = e.target;
    this.setState({ timeDelivery: value });
  }

  changeNote = (e) => {
    const { value } = e.target;
    this.setState({ noteText: value });
  }

  onClickCreateAccount = (e) => {
    const { checked } = e.target;
    this.shipInfo.create_account = checked;
    this.forceUpdate();
  }


  gotoAnchor = (id) => {
    const ele = document.getElementById(id);
    if (ele && (isMobile || isTablet)) {
      ele.scrollIntoView({ behavior: 'smooth' });
      // window.scrollBy(0, offset);
    }
  }

  checkEmailExist = () => {
    const { login } = this.props;
    const isLogined = login && login.user && login.user.id;
    if (isLogined) {
      this.setState({ isShowCreateAccount: false });
      return;
    }
    const { email } = this.shipInfo;
    if (!email) {
      this.setState({ isShowCreateAccount: true });
      return;
    }
    const option = {
      url: CHECK_EMAIL_LOGIN_URL.replace('{email}', email),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.setState({ isShowCreateAccount: false });
        return;
      }
      throw new Error();
    }).catch(() => {
      this.setState({ isShowCreateAccount: true });
    });
  }

  getShippingMethod = () => {
    const { isSelfCollect, isExpress } = this.state;
    if (isSelfCollect) {
      return 'Self Collect';
    } if (isExpress) {
      return 'Express';
    }
    return 'Standard';
  }

  getShippingAddress = () => {
    const {
      street1, city, state, postal_code, country,
    } = this.shipInfo;
    return `${street1}, ${city}, ${state}, ${postal_code}, ${country}`;
  }

  checkContinuteStep1 = () => {
    // const { name, email, phone } = this.shipInfo;
    const {
      email, firstName, lastName, dob, phone,
    } = this.shipInfo;
    const { listError } = this.state;
    const fieldError = [];
    const isPhoneValid = phone?.length && phone?.length > 5;
    const isDobAvaiable = dob && moment(dob, 'MM/DD/YYY').isValid();
    if (!firstName || !email || !lastName || !isPhoneValid) {
      if (!email) {
        fieldError.push('email');
        listError.push('email');
      }
      if (!firstName) {
        fieldError.push('first name');
        listError.push('firstName');
      }
      if (!lastName) {
        fieldError.push('last name');
        listError.push('lastName');
      }
      if (!isPhoneValid) {
        fieldError.push('phone');
        listError.push('phone');
      }
      toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }

    // check '@' include first name, last name
    if (firstName.includes('@')) {
      fieldError.push('First name');
      listError.push('firstName');
    }
    if (lastName.includes('@')) {
      fieldError.push('Last name');
      listError.push('lastName');
    }
    if (listError.length > 0) {
      toastrError('@ is an invalid character for name');
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }

    if (!EmailValidator.validate(this.shipInfo.email)) {
      listError.push('email');
      toastrError('The email is not available');
      this.setState({ stepCheckOut: 1, isStep1Done: false, listError });
      return false;
    }
    this.checkEmailExist();
    this.setState({ stepCheckOut: 2, isStep1Done: true });
    this.gotoAnchor('anchor-step1');
    segmentClub21GCheckoutCustomerDetailsEntered(this.props.basketSubscription, this.shipInfo);
    return true;
  }

  checkContinuteStep2 = () => {
    const { isSelfCollect, isBillAddressSame, listError } = this.state;
    const {
      street1, postal_code: postalCode, country, pan_card: panCard,
    } = this.shipInfo;
    const fieldError = [];
    const isPanCardError = ['in', 'id'].includes(country) && !panCard;
    if (!isSelfCollect && (!street1 || !postalCode || isPanCardError)) {
      if (!street1) {
        fieldError.push('address');
        listError.push('street1');
      }
      if (!postalCode) {
        fieldError.push('zip code');
        listError.push('postal_code');
      }
      if (isPanCardError) {
        fieldError.push('pan_card');
        listError.push('pan_card');
      }
      toastrError(`Please enter your ${_.join(fieldError, ', ')}`);
      this.setState({ stepCheckOut: 2, isStep2Done: false, listError });
      return false;
    }

    if (!isBillAddressSame) {
      const {
        street1: billStreet1, postal_code: billPostalCode, country: billCountry, pan_card: billPanCard,
      } = this.billingInfo;
      const isBillPanCardError = ['in', 'id'].includes(billCountry) && !billPanCard;
      if (!billStreet1 || !billPostalCode || isBillPanCardError) {
        if (!billStreet1) {
          fieldError.push('address');
          listError.push('billStreet1');
        }
        if (!billPostalCode) {
          fieldError.push('zip code');
          listError.push('billPostalCode');
        }
        if (isBillPanCardError) {
          fieldError.push('pan_card');
          listError.push('billPanCard');
        }
        toastrError(`Please enter your billing ${_.join(fieldError, ', ')}`);
        this.setState({ stepCheckOut: 2, isStep2Done: false, listError });
        return false;
      }
    }
    this.gotoAnchor('anchor-step2', 0);
    this.setState({ stepCheckOut: 3, isStep2Done: true, isStep3Done: false });
    segmentClub21GShippingInfoEntered(this.props.basketSubscription,
      this.getShippingMethod(),
      this.getShippingAddress(),
      this.shipInfo);
    // setTimeout(() => {
    //   this.gotoAnchor('anchor-step3');
    // },300);

    return true;
  }

  checkContinuteStep3 = () => {
    const { isBillAddressSame } = this.state;
    const { billStreet1, billPostalCode, billPhone } = this.shipInfo;
    if (!isBillAddressSame && (!billStreet1 || !billPostalCode || !billPhone)) {
      toastrError('Please enter all the information');
      this.setState({ stepCheckOut: 3, isStep3Done: false });
      return false;
    }
    this.setState({ stepCheckOut: 4, isStep3Done: true });
    return true;
  }

  isShowPackagingOption = (basket) => {
    const { items } = basket;
    let flags = false;
    _.forEach(items, (d) => {
      const { item } = d;
      if (item) {
        const type = item.product.type.name;
        const countrySupport = ['sg', 'au'];
        if ((item.is_sample === false) && (type === 'Perfume' || type === 'Creation' || type === 'Scent' || type === 'Elixir') && (this.shipInfo.country ? countrySupport.includes(this.shipInfo.country.toLowerCase()) : false)) {
          flags = true;
        }
      }
    });
    return flags;
  }

  selectPackagingOption = (id) => {
    this.packaging = id;
  };

  openShowAddress = () => {
    this.setState({ isShowChoiseAddress: true });
  }

  closeShowAddress = () => {
    this.setState({ isShowChoiseAddress: false });
  }

  onChooseNewAddress = (flag) => {
    if (flag) {
      this.initalData();
    } else {
      this.initalData(this.currentAddress);
    }
  }

  onChangeAddress = (address) => {
    this.currentAddress = address;
    this.initalData(address);
  }

  render() {
    const { basketSubscription: basket, login } = this.props;
    if (!basket) {
      return;
    }
    const {
      noteText, isSelfCollect, timeDelivery,
      stepCheckOut, isStep1Done, isStep2Done, isBillAddressSame, seo,
      isShowCreateAccount, listError, isOpenNote, isOPenPromo, imageBlockIcon,
      buttonBlock, imageTxtBlock, isShowChoiseAddress, listAddress, isShowOptionAddress,
    } = this.state;
    const {
      id, total, shipping, subtotal, items, discount,
    } = basket;

    const newPrice = parseFloat(discount) > 0 ? parseFloat(subtotal) - parseFloat(discount) : -1;
    const isOnlyGift = this.isAllGift(items);
    const listProduct = basket && basket.items ? basket.items : [];
    const cmsCommon = getCmsCommon(this.props.cms);
    const subtotalBt = getNameFromCommon(cmsCommon, 'Subtotal');
    const shippingBt = getNameFromCommon(cmsCommon, 'Shipping');
    const discountBt = getNameFromCommon(cmsCommon, 'Discount');
    const promoGiftBt = getNameFromCommon(cmsCommon, 'Promo/Gift_Certificate');
    const applyBt = getNameFromCommon(cmsCommon, 'APPLY');
    const totalBt = getNameFromCommon(cmsCommon, 'TOTAL');
    const orderBt = getNameFromCommon(cmsCommon, 'ORDER_SUMMARY');
    const checkOutBt = getNameFromCommon(cmsCommon, 'CHECKOUT');
    const addNoteBt = getNameFromCommon(cmsCommon, 'ADD_A_NOTE');
    const placeBt = getNameFromCommon(cmsCommon, 'PLACE_ORDER');
    const mouthBt = getNameFromCommon(cmsCommon, 'Month');
    const billedBt = getNameFromCommon(cmsCommon, 'Billed_Monthly');
    const showBt = getNameFromCommon(cmsCommon, 'Show Details');

    const isLogined = login && login.user && login.user.id;
    const isShowPackagingOption = this.isShowPackagingOption(basket);
    if (!isShowPackagingOption) {
      this.packaging = undefined;
    }
    const htmlItems = (
      <div className="div-col div-infor">
        <div className="div-customer div-col">
          <CustomerItem
            isCheckOutClub
            step={1}
            buttonBlock={buttonBlock}
            stepCheckOut={stepCheckOut}
            isStepDone={isStep1Done}
            onClickHeader={() => this.setState({ stepCheckOut: 1, isStep1Done: false })}
            shipInfo={this.shipInfo}
            onChange={this.onChange}
            onClickContinute={this.checkContinuteStep1}
            login={login}
            cms={this.props.cms}
            listError={listError}
            onChangeGenderSelect={this.onChangeGenderSelect}
            genderSelect={this.shipInfo.gender}
            basket={basket}
          />
          {
            !isOnlyGift ? (
              <ShippingItem
                isCheckOutClub
                step={2}
                isLogined={isLogined}
                currentAddress={this.currentAddress}
                openShowAddress={this.openShowAddress}
                closeShowAddress={this.closeShowAddress}
                onChooseNewAddress={this.onChooseNewAddress}
                onChangeAddress={this.onChangeAddress}
                listAddress={listAddress}
                isShowOptionAddress={isShowOptionAddress}
                buttonBlock={buttonBlock}
                stepCheckOut={stepCheckOut}
                isStepDone={isStep2Done}
                isSelfCollect={isSelfCollect}
                onClickHeader={() => this.setState({ stepCheckOut: 2, isStep2Done: false })}
                onClickSelfCollect={() => this.setState({ isSelfCollect: true, isBillAddressSame: false }, () => { this.preCheckOut(); })}
                onClickStandard={() => this.setState({ isSelfCollect: false }, () => { this.preCheckOut(); })}
                onClickExpress={() => this.setState({ isSelfCollect: false }, () => { this.preCheckOut(); })}
                shipInfo={this.shipInfo}
                onChange={this.onChange}
                onClickContinute={this.checkContinuteStep2}
                listShipping={this.listShipping}
                optionTime={this.optionTime}
                onChangeTimeDelivery={this.onChangeTimeDelivery}
                isBillAddressSame={isBillAddressSame}
                shipping={shipping}
                timeDelivery={timeDelivery}
                cms={this.props.cms}
                isShowPackagingOption={isShowPackagingOption}
                selectPackagingOption={this.selectPackagingOption}
                isShowCreateAccount={isShowCreateAccount}
                onClickCreateAccount={this.onClickCreateAccount}
                listError={listError}
                billingInfo={this.billingInfo}
                onChangeBillingInfo={this.onChangeBillingInfo}
                onClickCheckBillAdress={e => this.setState({ isBillAddressSame: e })}
              />
            ) : (<div />)
          }
          <PaymentItemClub
            step={3}
            stepCheckOut={stepCheckOut}
            cmsCommon={cmsCommon}
            cardInfo={this.cardInfo}
            imageBlockIcon={imageBlockIcon}
            listError={listError}
            onChange={() => {}}
            isOnlyGift={isOnlyGift}
            imageTxtBlock={imageTxtBlock}
            buttonBlock={buttonBlock}
            onCheckOutFunc={this.onCheckOutFunc}
          />
          {/* <PaymentItem
            stepCheckOut={stepCheckOut}
            isStepDone={isOnlyGift ? isStep2Done : isStep3Done}
            onClickHeader={() => { this.setState({ stepCheckOut: isOnlyGift ? 2 : 3 }); this.checkEmailExist(); }}
            changeNote={this.changeNote}
            onCheckOutFunc={this.onCheckOutFunc}
            noteText={noteText}
            payMethod={payMethod}
            onClickStripe={() => { this.setState({ payMethod: 'stripe' }); }}
            onClickPaypal={() => { this.setState({ payMethod: 'paypal' }); }}
            onClickPaypalLate={() => { this.setState({ payMethod: 'stripe_pay_later' }); }}
            cms={this.props.cms}
            isOnlyGift={isOnlyGift}
            textBlock={textBlock}
            shipInfo={this.shipInfo}
            imageBlockIcon={imageBlockIcon}
            basket={basket}
          /> */}
        </div>
      </div>
    );

    const summaryCheckOut = (
      <div className="div-col div-summary">
        <div className="div-row justify-between items-center mt-3">
          <span>
            {subtotalBt}
          </span>
          {
            isBrowser ? (
              <span className="tx-price">
                {generaCurrency(newPrice === -1 ? subtotal : newPrice, true)}
              </span>
            ) : (
              newPrice === -1
                ? (
                  <span className="tx-price">
                    {generaCurrency(subtotal, true)}
                  </span>
                ) : (
                  <span>
                    <span className="tx-price strike">
                      {generaCurrency(subtotal, true)}
                    </span>
                    <span className="tx-price ml-2">
                      {generaCurrency(newPrice, true)}
                    </span>
                  </span>
                )
            )
          }
        </div>
        <div className="div-row justify-between items-center mt-2 mb-2">
          <span>
            {shippingBt}
          </span>
          <span className="tx-price">
            {generaCurrency(shipping, true)}
          </span>
        </div>
        {
          !isBrowser && (
            <div className="div-row justify-between items-center pb-2">
              <span>
                {discountBt}
              </span>
              <span className="tx-price">
                {generaCurrency(discount, true)}
              </span>
            </div>
          )
        }
        <hr style={{
          width: '100%', color: 'rgba(38, 38, 38, 0.1)', margin: '0px', marginTop: '16px',
        }}
        />
        <div id="id-promotion" className={`div-expand ${isOpenNote ? 'open-expand' : 'close-expand'}`}>
          <div
            className="div-header-expand"
            onClick={() => {
              this.setState({ isOpenNote: !isOpenNote });
            }
            }
          >
            <span>
              {addNoteBt}
            </span>
            <button
              onClick={() => {
                this.setState({ isOpenNote: !isOpenNote });
              }}
              type="button"
            >
              <img src={isOpenNote ? icSub : icPlus} alt="icon" />
            </button>
          </div>
          <div className="div-content-expand">
            <Input
              type="textarea"
              className="border-checkout mb-3"
              style={{ height: '4rem' }}
              value={noteText}
              onChange={this.changeNote}
              onClick={() => trackGTMCheckOut(5, basket)}
            />
          </div>
        </div>

        <div style={{ width: '100%', height: '1px', background: 'rgba(38, 38, 38, 0.1)' }} />
        <div className="div-row justify-between  items-center mt-3 mb-3">
          <span style={{ fontSize: '16px', color: '#0D0D0D', fontWeight: '400' }}>
            {totalBt}
          </span>
          <div className="div-col items-end">
            <span style={{ fontSize: '20px', color: '#0D0D0D', fontWeight: '400' }}>
              {`${generaCurrency(total, true)}/${mouthBt}`}
            </span>
            <span style={{ fontSize: '12px', color: '#0D0D0D' }}>
              {billedBt}
            </span>
          </div>
        </div>
        {
          !isBrowser && (
            <React.Fragment>
              <div className="line-black" style={{ width: '100%', height: '1px', background: '#0D0D0D' }} />
              <button
                type="button"
                className="bt-checkout mt-3"
                onClick={this.onCheckOutFunc}
              >
                {placeBt}
              </button>
            </React.Fragment>
          )
        }
      </div>
    );
    const htmlInformation = (
      <div className="div-order div-col">
        {
          isBrowser && (
            <div
              className="div-header-order div-row justify-center"
            >
              <span>{orderBt}</span>
            </div>
          )
        }
        {
              _.map(listProduct, d => (
                <CardItem
                  isCheckOutClub
                  isB2C
                  idCart={id}
                  data={d}
                  isCheckOut={!isMobile}
                  cms={this.props.cms}
                  style={{ borderBottomColor: 'rgba(38, 38, 38, 0.1)' }}
                />
              ))
            }
        {isMobile || isTablet ? <div /> : summaryCheckOut}
      </div>
    );

    const cardTotalItem = (
      <div className="footer-checkout-mobile card-total-item" onClick={() => this.setState({ isOpenListItemMobile: true })}>
        <div className="div-content">
          <div className="left-card">
            <div className="div-image">
              <div className="img-detail">
                {
                  _.map(listProduct, (d, index) => (
                    index === listProduct.length - 1
                      ? (
                        <img
                          src={d.image_display}
                          alt="icon"
                          style={{ top: `-${1 * 2}px`, left: `-${1 * 2}px` }}
                        />
                      ) : (<div className="img-temp" />)
                  ))
                }
              </div>
            </div>
            <div className="title-item-card">
              <h2>{`${listProduct.length} ITEMS`}</h2>
              <span>{showBt}</span>
            </div>
          </div>
          <h3>{generaCurrency(total, true)}</h3>
        </div>

      </div>
    );

    return (
      <div className="div-col justify-center div-checkout" style={isTablet ? { marginLeft: '40px', marginRight: '40px' } : {}}>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/checkout-club')}
        </MetaTags>
        {/* <HeaderHomePage isProductPage isSpecialMenu isDisableScrollShow isNotShowRegion /> */}
        <HeaderHomePageV3 />
        <Row className="div-body">
          {
            isMobile || isTablet ? (
              <React.Fragment>
                {htmlItems}
                {stepCheckOut === 3 ? summaryCheckOut : undefined}
                {cardTotalItem}
              </React.Fragment>
            ) : (
              <React.Fragment>
                {htmlItems}
                {htmlInformation}
              </React.Fragment>

            )
          }
        </Row>
        {
          isShowChoiseAddress ? (
            <PopUpSaveAdsress
              closeShowAddress={this.closeShowAddress}
              listAddress={listAddress}
              currentAddress={this.currentAddress}
              onChangeAddress={this.onChangeAddress}
              buttonBlock={buttonBlock}
            />
          ) : (null)
        }

        <ModalCheckoutMobile isOpen={this.state.isOpenListItemMobile}>
          <div className="body-item-mobile">
            <div className="div-header-modal">
              <h2>{orderBt}</h2>
              <button className="button-bg__none" type="button" onClick={() => this.setState({ isOpenListItemMobile: false })}>
                <img src={icClose} alt="icon" />
              </button>
            </div>
            {htmlInformation}
            {summaryCheckOut}
          </div>
        </ModalCheckoutMobile>

        <div id="div-button" className="hidden" />
        <FooterV2 />
      </div>
    );
  }
}

CheckOutClub.propTypes = {
  login: PropTypes.shape(PropTypes.object).isRequired,
  basketSubscription: PropTypes.shape(PropTypes.object).isRequired,
  refPoint: PropTypes.string.isRequired,
  preCheckOutBasketSubscription: PropTypes.func.isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  cms: PropTypes.shape().isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    basketSubscription: state.basketSubscription,
    refPoint: state.refPoint,
    cms: state.cms,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  preCheckOutBasketSubscription,
  addCmsRedux,
  createBasketSubscription,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CheckOutClub));
