import React, { Component } from 'react';
import _ from 'lodash';
import CardItem from '../../../../components/B2B/CardItem';
import PurchaseSuccess from '../../../../image/icon/purchase-success.svg';
import { getNameFromCommon } from '../../../../Redux/Helpers';
import BankTransfer from '../bankTransfer';

class OrderDetail extends Component {
  render() {
    const { resPayment, renderPriceString, cmsCommon } = this.props;
    const thankYouBt = getNameFromCommon(cmsCommon, 'Thank_you_for_you_purchase');
    const aConfirmEmailBt = getNameFromCommon(cmsCommon, 'A_confirmation_email_has_been_sent_two');
    const oderBt = getNameFromCommon(cmsCommon, 'Order');
    const returnToStore = getNameFromCommon(cmsCommon, 'return_to_store');
    const orderSummaryBt = getNameFromCommon(cmsCommon, 'ORDER_SUMMARY');
    const subtotalBt = getNameFromCommon(cmsCommon, 'Subtotal');
    const shippingBt = getNameFromCommon(cmsCommon, 'Shipping');
    const discountBt = getNameFromCommon(cmsCommon, 'DISCOUNT');
    const paidBt = getNameFromCommon(cmsCommon, 'Paid');

    const bankTransferGuide = getNameFromCommon(cmsCommon, 'BANK_TRANSFER_INFO_GUIDE');
    const bankTransferAccountNumber = getNameFromCommon(cmsCommon, 'BANK_TRANSFER_INFO_ACCOUNT_NUMBER');
    const bankTransferAccountName = getNameFromCommon(cmsCommon, 'BANK_TRANSFER_INFO_ACCOUNT_NAME');
    const bankTransferBankName = getNameFromCommon(cmsCommon, 'BANK_TRANSFER_INFO_BANK_NAME');
    const bankTransferBankBranch = getNameFromCommon(cmsCommon, 'BANK_TRANSFER_INFO_BANK_BRANCH');
    const bankTransferContent = getNameFromCommon(cmsCommon, 'BANK_TRANSFER_INFO_CONTENT');
    const bankTransferNote = getNameFromCommon(cmsCommon, 'BANK_TRANSFER_INFO_NOTE');

    const isBankTransferPayment = resPayment && resPayment.payment_method === 'bank_transfer';

    const confirmEmailBlock = (
      <p className="__information-email text-center">
        {aConfirmEmailBt}
        <br />
        <span className="__information-email text-center font-weight-bold">
          {resPayment.meta && resPayment.meta.guest ? resPayment.meta.guest.email : (resPayment.meta && resPayment.meta.sender ? resPayment.meta.sender.email : (resPayment.shipping_address ? resPayment.shipping_address.email : ''))}
        </span>
      </p>
    );

    const orderIdBlock = (
      <p className="__information-orderId text-center">
        {oderBt}
        :
        {' '}
        <span className="font-weight-bold">
          {resPayment && resPayment.id}
        </span>
      </p>
    );

    const returnToStoreBlock = (
      <div className="__return d-flex justify-content-center">
        <a type="button" href="/all-products">
          {returnToStore}
        </a>
      </div>
    );

    return (
      <div className="order-detail">
        <div className="row">
          <div className="col-6">
            <div className="order-overview">
              <div className="__image d-flex justify-content-center">
                <img loading="lazy" src={PurchaseSuccess} alt="" className="img-fluid" />
              </div>
              <div className="__information">
                <h3 className="__information-thankyou text-center">
                  {thankYouBt}
                </h3>
                {isBankTransferPayment && (
                  <BankTransfer
                    bankTransferAccountNumber={bankTransferAccountNumber}
                    bankTransferGuide={bankTransferGuide}
                    bankTransferAccountName={bankTransferAccountName}
                    bankTransferBankName={bankTransferBankName}
                    bankTransferBankBranch={bankTransferBankBranch}
                    bankTransferContent={bankTransferContent}
                    bankTransferNote={bankTransferNote}
                    customerName={resPayment.shipping_address.name || ''}
                    customerPhone={resPayment.shipping_address.phone || ''}
                    orderId={resPayment.id || ''}
                  />
                )}
                {!isBankTransferPayment && confirmEmailBlock}
                {!isBankTransferPayment && orderIdBlock}
              </div>
              {!isBankTransferPayment && returnToStoreBlock}
            </div>
          </div>
          <div className="col-6">
            <div className="order-summary">
              <div className="__header">
                {orderSummaryBt}
              </div>
              {resPayment.line_items && (
                <div className="__list">
                  <ul>
                    {
                    _.map(resPayment.line_items, (d, index) => (
                      <li className="cd-cart__product" key={index}>
                        <CardItem
                          updateProductBasket={this.props.updateProductBasket}
                          deleteProductBasket={this.props.deleteProductBasket}
                          data={d}
                          // idCart={id}
                          isB2C
                          onCloseCard={this.onCloseCard}
                          cms={this.props.cms}
                          listCountry={this.props.countries}
                          onOpenCustomBottle={() => {}}
                          onCloseCustomBottle={() => {}}
                          isPaySuccess
                        />
                      </li>
                    ))
                  }
                  </ul>
                  {/* {resPayment.line_items.map(item => (
                    <div className="__list-item d-flex justify-content-between">
                      <div className="row">
                        <div className="col-3">
                          <div className="__item-image">
                            <img alt="" className="img-fluid" src={item.image_display} />
                          </div>
                        </div>
                        <div className="col-6 d-flex align-items-center">
                          <div className="__item-information">
                            <span>
                              {item.name}
                              {' '}
                              x
                              {' '}
                              {item.quantity}
                            </span>
                          </div>
                        </div>
                        <div className="col-3 d-flex justify-content-end align-items-center">
                          <div className="__item-price">
                            <span className="font-weight-bold">
                              {renderPriceString(item.total)}
                            </span>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))} */}
                </div>
              )}
              {resPayment.currency && (
                <div className="__price">
                  <div className="__price-item d-flex justify-content-between">
                    <span className="__price-tag font-weight-bold">
                      {subtotalBt}
                    </span>
                    <span className="__price-number font-weight-bold">
                      {renderPriceString(resPayment.subtotal)}
                    </span>
                  </div>
                  <div className="__price-item d-flex justify-content-between">
                    <span className="__price-tag">
                      {shippingBt}
                    </span>
                    <span className="__price-number">
                      {renderPriceString(resPayment.shipping)}
                    </span>
                  </div>
                  <div className="__price-item d-flex justify-content-between">
                    <span className="__price-tag">
                      {discountBt}
                    </span>
                    <span className="__price-number">
                      {renderPriceString(resPayment.discount)}
                    </span>
                  </div>
                </div>
              )}
              {resPayment.currency && (
                <div className="__total d-flex justify-content-between">
                  <span className="__total-tag font-weight-bold">
                    {paidBt}
                  </span>
                  <span className="__total-number">
                    {renderPriceString(resPayment.total)}
                  </span>
                </div>
              )}
            </div>
          </div>
        </div>
        {isBankTransferPayment && (
          <div className="col-12">
            <div className="order-overview">
              <div className="__information">
                {confirmEmailBlock}
                {orderIdBlock}
              </div>
              {returnToStoreBlock}
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default OrderDetail;
