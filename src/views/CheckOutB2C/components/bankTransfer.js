import React, { Component } from 'react';

class BankTransfer extends Component {
  render() {
    const {
      bankTransferGuide,
      bankTransferAccountName,
      bankTransferAccountNumber,
      bankTransferBankName,
      bankTransferBankBranch,
      bankTransferContent,
      bankTransferNote,
      customerName,
      customerPhone,
      orderId,
    } = this.props;

    const transferContent = bankTransferContent
      .replace('{{CUSTOMER_NAME}}', customerName.toUpperCase())
      .replace('{{CUSTOMER_PHONE}}', customerPhone)
      .replace('{{ORDER_ID}}', orderId);

    return (
      <div className="__information-bankTransfer text-left">
        {bankTransferGuide && (<div className="transfer-guide">{bankTransferGuide}</div>)}
        <div className="transfer-info">
          {bankTransferAccountName && (<p className="account-name">{bankTransferAccountName}</p>)}
          {bankTransferAccountNumber && (<p className="account-number">{bankTransferAccountNumber}</p>)}
          {bankTransferBankName && (<p className="bank-name">{bankTransferBankName}</p>)}
          {bankTransferBankBranch && (<p className="bank-branch">{bankTransferBankBranch}</p>)}
          {transferContent && (<p className="content">{transferContent}</p>)}
        </div>
        {bankTransferNote && (<div className="transfer-note">{bankTransferNote}</div>)}
      </div>
    );
  }
}

export default BankTransfer;
