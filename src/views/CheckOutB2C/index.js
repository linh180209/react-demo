import React, { Component } from 'react';
import '../../styles/style.scss';
import HeaderHomePage from '../../components/HomePage/header';
import CheckOutB2CPage from './checkOutB2CPage';
import { scrollTop, googleAnalitycs, setPrerenderReady } from '../../Redux/Helpers';
import FooterV2 from '../../views2/footer';

class CheckOutB2C extends Component {
  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/checkout');
  }

  render() {
    return (
      <div className="div-col">
        <HeaderHomePage isProductPage isSpecialMenu isDisableScrollShow />
        <CheckOutB2CPage />
        <FooterV2 />
      </div>
    );
  }
}

export default CheckOutB2C;
