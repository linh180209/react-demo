import React, { Component } from 'react';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Modal, ModalBody,
} from 'reactstrap';
import classnames from 'classnames';

// import ReactGA from 'react-ga';
import _ from 'lodash';
import '../../styles/thankyou.scss';
import '../../styles/popup-reward.scss';
import Header from '../../components/HomePage/header';
import {
  getCmsCommon, getNameFromCommon, googleAnalitycs, trackGTMPurchase, trackGTMEventPurchase, generateUrlWeb, setPrerenderReady, addFontCustom, generaCurrency, addOrderCartNamogoo,
} from '../../Redux/Helpers';
import { GET_ORDER_URL, GET_USER_ACTIONS, TOKEN_REFRESH } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import OrderDetail from './components/OrderDetail';
import OrderDetailMobile from './components/OrderDetailMobile';
import auth from '../../Redux/Helpers/auth';
import { loadAllBasketSubscription } from '../../Redux/Actions/basketSubscription';
import icClose from '../../image/icon/ic-close-menu.svg';
import logoMoney from '../../image/logo-money.png';
import { isMobileOnly } from '../../DetectScreen';
import initialState from '../../Redux/Reducers/initialState';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';

class PaySuccess extends Component {
  constructor(props) {
    super(props);

    this.state = {
      resPayment: {},
      totalPoint: undefined,
    };
  }

  componentDidMount() {
    setPrerenderReady();
    const { match } = this.props;
    const { params } = match;
    const { orderId } = params;
    if (orderId) {
      this.fetchData(orderId);
    }
    googleAnalitycs('/pay/success');
  }

  onClosePopUp = () => {
    this.setState({ totalPoint: undefined });
  }

  popupCongratulations = () => {
    const cmsCommon = getCmsCommon(this.props.cms);
    const congratulationsBt = getNameFromCommon(cmsCommon, 'congratulations!');
    const GramsBt = getNameFromCommon(cmsCommon, 'Grams!');
    const youhaveBt = getNameFromCommon(cmsCommon, 'You_have_just');
    const GramsEarnedBt = getNameFromCommon(cmsCommon, 'Grams_Earned');
    const myTotalBt = getNameFromCommon(cmsCommon, 'My_Total_Grams');
    const closeBt = getNameFromCommon(cmsCommon, 'CLOSE');
    const myRewardsBt = getNameFromCommon(cmsCommon, 'MY_REWARDS');
    return (
      <Modal className={classnames('modal-full-screen', addFontCustom())} isOpen={!!this.state.totalPoint}>
        <ModalBody className="flex">
          <div className="popup-history congratulation">
            <button className="button-bg__none bt-x" type="button" onClick={this.onClosePopUp}>
              <img src={icClose} alt="close" />
            </button>
            <h1>
              {congratulationsBt}
            </h1>
            <img className="logo-money" src={logoMoney} alt="logo" />
            <h2 className="header-point-1">
              +
              {parseInt(this.state.totalPoint, 10)}
              {' '}
              {GramsBt}
            </h2>
            <span className="des">
              {youhaveBt}
            </span>
            <div className="div-text">
              <h4>
                {GramsEarnedBt}
              </h4>
              <span>
                +
                {parseInt(this.state.totalPoint, 10)}
              </span>
            </div>
            <div className="div-text">
              <h4>
                {myTotalBt}
              </h4>
              <span>
                {this.props.login && this.props.login.user ? this.props.login.user.point : 0}
              </span>
            </div>
            <div className="div-button">
              <button className="bt-close outline-bt" type="button" onClick={this.onClosePopUp}>
                {closeBt}
              </button>
              <button
                onClick={() => this.props.history.push(generateUrlWeb('/account/my-rewards'))}
                className="bt-close"
                type="button"
              >
                {myRewardsBt}
              </button>
            </div>
          </div>
        </ModalBody>
      </Modal>
    );
  };

  handleShowPoint = (total) => {
    const options = {
      url: GET_USER_ACTIONS,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        const ele = _.find(result, x => x.type === 'buy');
        if (ele) {
          this.setState({ totalPoint: ele.point * total / ele.spend });
        }
      }
    }).catch((err) => {
      console.log(err.mesage);
    });
  }

  renderPriceString = amount => generaCurrency(amount, true);

  exchangeRateCalculate = (price, rate) => {
    if (!price) {
      return '0.00';
    }
    const exchangeRate = rate || 1;
    return `${(parseFloat(price, 10) / exchangeRate).toFixed(2)}`;
  }

  fetchData = (id) => {
    const options = {
      url: GET_ORDER_URL.replace('{id}', id),
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        const login = auth.getLogin();
        if (login && login.user && login.user.id) {
          this.handleShowPoint(result.total);
        } else if (result.cart === this.props.basket.id) {
          this.props.loadAllBasketSubscription(initialState.basket);
          localStorage.removeItem('basketId');
        }

        this.setState({
          resPayment: result,
        });

        const {
          line_items: lineItems, current_timestamp: currentTimestamp, transaction_timestamp: transactionTimestamp, exchange_rate: exchangeRate,
          promo,
        } = result;
        const items = _.map(lineItems, d => (
          {
            id: `${d.item.id}`,
            name: d.item.name,
            price: this.exchangeRateCalculate(d.item.price, exchangeRate),
            quantity: d.quantity,
            category: d.item.product.type.name,
            total: this.exchangeRateCalculate(d.total, exchangeRate),
            currency: 'sgd',
          }
        ));
        const transaction = {
          id: `${result.id}`,
          revenue: this.exchangeRateCalculate(result.total, exchangeRate),
          shipping: this.exchangeRateCalculate(result.shipping, exchangeRate),
          currency: 'sgd',
          coupon: promo ? promo.code : '',
        };
        const ecommerce = {
          currencyCode: 'sgd',
          purchase: {
            actionField: transaction,
            products: items,
          },
        };
        const dataNamogoo = {
          orderTotalBeforeDiscount: this.exchangeRateCalculate(result.subtotal, exchangeRate),
          orderTotalAfterDiscount: this.exchangeRateCalculate(parseFloat(result.subtotal) - parseFloat(result.discount), exchangeRate),
          couponCode: promo ? promo.code : '',
          numOfItems: items.length,
          currency: 'sgd',
        };
        addOrderCartNamogoo(dataNamogoo);
        trackGTMPurchase(ecommerce);
        trackGTMEventPurchase(transactionTimestamp, currentTimestamp);
      }
    });
  }

  render() {
    const cmsCommon = getCmsCommon(this.props.cms);
    const shipAddressbt = getNameFromCommon(cmsCommon, 'SHIPPING_ADDRESS');
    const continuteShoppingBt = getNameFromCommon(cmsCommon, 'CONTINUE_SHOPPING');

    const { resPayment } = this.state;
    console.log('STATE:', this.state);
    return (
      <div className="div-col">
        {this.popupCongratulations()}
        {/* <Header /> */}
        <HeaderHomePageV3 />
        <div className="thankyou">
          {!isMobileOnly ? <OrderDetail resPayment={resPayment || {}} renderPriceString={this.renderPriceString} cmsCommon={cmsCommon} countries={this.props.countries} />
            : <OrderDetailMobile resPayment={resPayment || {}} renderPriceString={this.renderPriceString} cmsCommon={cmsCommon} /> }
          <div className="shipping">
            <div className="row">
              <div className="col-12">
                <h4>
                  {shipAddressbt}
                </h4>
                {resPayment.shipping_address && (
                  <div className="__information">
                    <div className="__information-name">
                      <span className="font-weight-bold">
                        {resPayment.shipping_address.name}
                      </span>
                    </div>
                    <div className="__information-address">
                      <span>
                        {resPayment.shipping_address.street1}
                        <br />
                        {resPayment.shipping_address.postal_code}
                        {' '}
                        {resPayment.shipping_address.country.name}
                      </span>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className="continue d-flex justify-content-center">
                <a type="button" href="/#">
                  {continuteShoppingBt}
                </a>
              </div>
            </div>
          </div>
        </div>
        <FooterV2 />
      </div>
    );
  }
}

PaySuccess.propTypes = {
  cms: PropTypes.shape().isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      orderId: PropTypes.string,
    }),
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
    login: state.login,
    basket: state.basket,
  };
}

const mapDispatchToProps = {
  loadAllBasketSubscription,
};

export default connect(mapStateToProps, mapDispatchToProps)(PaySuccess);
// export default PaySuccess;
