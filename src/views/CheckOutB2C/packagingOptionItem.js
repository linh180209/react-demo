import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getAltImage } from '../../Redux/Helpers';
import icChecked from '../../image/icon/ic-checked.svg';
import RadioButton from '../../components/Input/radioButton';

class PackagingOptionItem extends Component {
  onClick = () => {
    if (this.props.onChange) {
      this.props.onChange(this.props.data.id);
    }
  }

  render() {
    const { isActive, data } = this.props;
    const { name, image } = data;
    return (
      <div
        className="div-packaing-option-item"
        onClick={this.onClick}
      >
        <RadioButton
          name={name}
          label={name}
          selected={isActive}
        />
        <img className="div-image" src={image} alt={getAltImage(image)} />
        {/* <div className={`div-button ${isActive ? 'active' : ''}`}>
          <img className="div-image" src={image} alt={getAltImage(image)} onClick={this.onClick} />
          <div className="div-checked">
            <img className="img-checked" src={icChecked} alt="ic-checked" />
          </div>

        </div>

        <span className="mt-2 mb-2">
          {name}
        </span> */}

      </div>
    );
  }
}

PackagingOptionItem.propTypes = {
  data: PropTypes.number.isRequired,
  isActive: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default PackagingOptionItem;
