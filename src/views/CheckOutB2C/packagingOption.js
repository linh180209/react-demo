import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

import PackagingOptionItem from './packagingOptionItem';

import { GET_PACKAGING_OPTIONS_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { getNameFromCommon } from '../../Redux/Helpers';
import GiftWrap from './giftWrap';
import { isBrowser } from '../../DetectScreen';

class PackagingOption extends Component {
  state={
    idChoise: 0,
    data: [],
  }

  componentDidMount() {
    this.fetchData();
  }

  onChange = (id) => {
    this.setState({ idChoise: id });
    this.props.selectPackagingOption(id);
  }

  fetchData = () => {
    const options = {
      url: GET_PACKAGING_OPTIONS_URL,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      this.setState({ data: result });
    });
  }

  render() {
    const { idChoise, data } = this.state;
    const packagingBt = getNameFromCommon(this.props.cmsCommon, 'PACKAGING_OPTION');
    return (
      <div className="div-packaging">
        <h4>
          {packagingBt}
        </h4>
        <div
          className="div-option justify-between"
        >
          {
            _.map(data, d => (
              <PackagingOptionItem
                data={d}
                isActive={idChoise === d.id}
                onChange={this.onChange}
              />
            ))
          }

        </div>
        {/* <GiftWrap
          buttonBlock={this.props.buttonBlock}
          selectGiftWrap={this.props.selectGiftWrap}
        /> */}
      </div>
    );
  }
}

PackagingOption.propTypes = {
  selectPackagingOption: PropTypes.func.isRequired,
  cmsCommon: PropTypes.arrayOf().isRequired,
};

export default PackagingOption;
