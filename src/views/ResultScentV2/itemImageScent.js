/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import bottleResult from '../../image/bottle-new.png';
import icInfo from '../../image/icon/icNewInfo.svg';
import bottleDualCrayon from '../../image/bottle_dual-crayon.png';
import icSearchInfo from '../../image/icon/ic-search-info.svg';
import BottleCustom from '../../components/B2B/CardItem/bottleCustom';
import { getNameFromCommon } from '../../Redux/Helpers';

class ItemImageScent extends Component {
  render() {
    const {
      isRollOn, disableInfo, data, onClickIngredient, onClickImage, isNightProd, customeBottle,
      cmsCommon, isDisableNameScent, isDisableImage,
    } = this.props;
    const eauBt = getNameFromCommon(cmsCommon, 'Eau_de_parfum');
    const mlBt = getNameFromCommon(cmsCommon, '25ml');
    const viewInfobt = getNameFromCommon(cmsCommon, 'view info');
    const {
      currentImg, nameBottle, isBlack, font, color,
    } = customeBottle || {};
    const { combo } = data || {};
    const products = _.filter(combo || [], x => x.product.type === 'Scent');
    const image1 = products && products.length > 0 ? _.find(products[0].product.images, x => x.type === 'unisex') : undefined;
    const ingredient1 = products && products.length > 0 ? products[0].product.ingredient : undefined;
    const image2 = products && products.length > 1 ? _.find(products[1].product.images, x => x.type === 'unisex') : undefined;
    const ingredient2 = products && products.length > 1 ? products[1].product.ingredient : undefined;
    return (
      <div className="div-item-image-scent">
        {
          isRollOn ? (
            <div className="scent-image">
              {
                !isDisableImage && (
                  <React.Fragment>
                    <img className="img-right img-roll-on animated fadeIn" src={image1 ? image1.image : ''} alt="scent" onClick={() => onClickImage && onClickImage(isNightProd, false, true)} />
                    <img className="img-left img-roll-on animated fadeIn" src={image2 ? image2.image : ''} alt="scent" onClick={() => onClickImage && onClickImage(isNightProd, false, true)} />
                  </React.Fragment>
                )
              }
              <img className="img-bottle img-bottle-rollon animated fadeIn" src={isRollOn ? bottleDualCrayon : bottleResult} alt="bottle" onClick={() => onClickImage && onClickImage(isNightProd, false, true)} />
              <div className={isRollOn ? 'hidden' : 'div-text-scent'}>
                <span>
                  {products && products.length > 0 ? products[0].name : ''}
                </span>
                <span className={products && products.length > 1 ? '' : 'hidden'}>
                  X
                </span>
                <span>
                  {products && products.length > 1 ? products[1].name : ''}
                </span>
              </div>
              {
                disableInfo ? (
                  <div />
                ) : (
                  this.props.isNewInfo ? (
                    <React.Fragment>
                      <div className="div-info left-info" onClick={() => onClickIngredient(ingredient2)}>
                        <img src={icSearchInfo} alt="info" />
                        <span>{viewInfobt}</span>
                      </div>
                      <div className="div-info right-info" onClick={() => onClickIngredient(ingredient1)}>
                        <img src={icSearchInfo} alt="info" />
                        <span>{viewInfobt}</span>
                      </div>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <img className="img-left-info" src={icInfo} alt="info" onClick={() => onClickIngredient(ingredient1)} />
                      <img className="img-right-info" src={icInfo} alt="info" onClick={() => onClickIngredient(ingredient2)} />
                    </React.Fragment>
                  )

                )
              }
            </div>
          ) : (
            <div className="scent-image animated fadeIn">
              {
                !isDisableImage && (
                  <React.Fragment>
                    <img
                      className="img-right"
                      src={image1 ? image1.image : ''}
                      alt="scent"
                      onClick={() => onClickIngredient(ingredient1)}
                    />
                    <img
                      className="img-left"
                      src={image2 ? image2.image : ''}
                      alt="scent"
                      onClick={() => onClickIngredient(ingredient2)}
                    />
                  </React.Fragment>
                )
              }

              <div className="div-img-bottle">
                <BottleCustom
                  isDisableNameScent={isDisableNameScent}
                  isPersonalResult
                  isImageText={!!currentImg}
                  onGotoProduct={() => onClickImage && onClickImage(data)}
                  image={currentImg}
                  isBlack={isBlack}
                  font={font}
                  color={color}
                  eauBt={eauBt}
                  mlBt={mlBt}
                  isDisplayName={!!nameBottle}
                  name={nameBottle}
                  combos={products}
                  classWrap={`custom-wrap ${disableInfo ? 'small' : ''}`}
                  classCustomOnly={`name-only ${disableInfo ? 'small' : ''}`}
                  classTextIngredients={`${nameBottle ? 'text-ingredients-name' : 'text-ingredients'} ${disableInfo ? 'small' : ''}`}
                  classImageName={`custom-image-name ${disableInfo ? 'small' : ''}`}
                  classImageLogo={`custom-image-logo ${disableInfo ? 'small' : ''}`}
                  classDesCustom={`custom-des ${disableInfo ? 'small' : ''}`}
                  classSizeCustom={`custom-size ${disableInfo ? 'small' : ''}`}
                  classNameCustom={`custom-text-name ${disableInfo ? 'small' : ''}`}
                />
              </div>
              {
                disableInfo ? (
                  <div />
                ) : (
                  this.props.isNewInfo ? (
                    <React.Fragment>
                      <div className="div-info left-info" onClick={() => onClickIngredient(ingredient2)}>
                        <img src={icSearchInfo} alt="info" />
                        <span>{viewInfobt}</span>
                      </div>
                      <div className="div-info right-info" onClick={() => onClickIngredient(ingredient1)}>
                        <img src={icSearchInfo} alt="info" />
                        <span>{viewInfobt}</span>
                      </div>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <img className="img-right-info" src={icInfo} alt="info" onClick={() => onClickIngredient(ingredient1)} />
                      <img className="img-left-info" src={icInfo} alt="info" onClick={() => onClickIngredient(ingredient2)} />
                    </React.Fragment>
                  )
                )
              }
            </div>
          )
        }
      </div>
    );
  }
}

ItemImageScent.defaultProps = {
  isRollOn: false,
  disableInfo: false,
};


ItemImageScent.propTypes = {
  isRollOn: PropTypes.bool,
  disableInfo: PropTypes.bool,
  data: PropTypes.shape().isRequired,
  onClickImage: PropTypes.func.isRequired,
  isNightProd: PropTypes.bool.isRequired,
};

export default ItemImageScent;
