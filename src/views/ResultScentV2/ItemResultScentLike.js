import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classnames from 'classnames';
import '../../styles/result-scent.scss';
import icUploadFile from '../../image/icon/ic-upload-file.svg';
import icSwitch from '../../image/icon/icSwitch.png';
import ItemImageScent from './itemImageScent';
import ProgressLine from './progressLine';
import ProgressCircle from './progressCircle';
import { getNameFromCommon, generaCurrency, getNameFromButtonBlock } from '../../Redux/Helpers';
import icLike from '../../image/icon/ic-like.svg';
import icDislike from '../../image/icon/ic-dislike.svg';
import icLiked from '../../image/icon/ic-liked.svg';
import icDisliked from '../../image/icon/ic-disliked.svg';
import { PUT_OUTCOME_URL, GET_ALL_SCENT_NOTES, GET_BOUTIQUE_SCENT_NOTES } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import ic130 from '../../image/icon/ic-13-0.svg';
import ic130Selected from '../../image/icon/ic-13-0-selected.svg';
import ic131 from '../../image/icon/ic-13-1.svg';
import ic131Selected from '../../image/icon/ic-13-1-selected.svg';
import { toastrError } from '../../Redux/Helpers/notification';
import { isMobile } from '../../DetectScreen';

class ItemResultScentLike extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRollOn: false,
      isChangeScent: props.isChangeScent,
      isChangeRollOn: false,
      listLike: props.scentsLiked || [],
      listDislike: props.scentsDisliked || [],
      isNightProd: props.isNightProd,

      topScent: [],
      botScent: [],
      heartScent: [],
    };
    this.refSlider = React.createRef();
  }

  componentDidMount() {
    this.fetchScentNotes();
  }

  fetchScentNotes = () => {
    const options = {
      url: GET_BOUTIQUE_SCENT_NOTES.replace('{id}', this.props.idOutCome),
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        const { bot_scents: botScent, heart_scents: heartScent, top_scents: topScent } = result;
        this.setState({ topScent, botScent, heartScent });
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err.message);
    });
  };

  onClickChillRelax = () => {
    this.setState({ isChangeScent: false, isNightProd: false });
    this.props.onSwitchIndex(false, 0);
  }

  onClickSeducation = () => {
    this.setState({ isChangeScent: false, isNightProd: true });
    this.props.onSwitchIndex(true, 0);
  }

  onClickSpecialOccasion = () => {
    this.setState({ isChangeScent: true, isNightProd: true });
    this.props.onSwitchIndex(true, 1);
  }

  onClickWorkSocial = () => {
    this.setState({ isChangeScent: true, isNightProd: false });
    this.props.onSwitchIndex(false, 1);
  }

  onChangeScent = () => {
    const { isChangeScent } = this.state;
    this.setState({ isChangeScent: !isChangeScent });
  }

  onChangeRollOn = () => {
    const { isChangeRollOn } = this.state;
    this.setState({ isChangeRollOn: !isChangeRollOn });
  }

  addListLike = (id) => {
    if (!id) {
      return;
    }
    const { listLike, listDislike } = this.state;
    const { namePath } = this.props;
    if (_.includes(listLike, id)) {
      _.remove(listLike, x => x === id);
    } else {
      _.remove(listDislike, x => x === id);
      listLike.push(id);
      // if (namePath !== 'sephora') {
      //   setTimeout(() => {
      //     this.changeScentWhenDislike(id);
      //   }, 1000);
      // }
    }
    this.setState({ listLike, listDislike });
  }

  addListDisLike = (id) => {
    if (!id) {
      return;
    }
    const { listDislike, listLike } = this.state;
    if (_.includes(listDislike, id)) {
      _.remove(listDislike, x => x === id);
    } else {
      _.remove(listLike, x => x === id);
      listDislike.push(id);

      setTimeout(() => {
        this.changeScentWhenDislike(id);
      }, 1000);
    }
    this.setState({ listDislike, listLike });
  }

  getScentReplace = (id, scents) => {
    const index = _.findIndex(scents, x => x.id === id);
    let scentSelected;
    if (index > -1) {
      _.forEach(scents, (x, i) => {
        if (!x.isDisliked && i !== index && !scentSelected) {
          _.assign(x, { isDisliked: true });
          scentSelected = x;
        }
      });
    }
    // if (index > -1) {
    //   if (index === scents.length - 1) {
    //     return scents[0];
    //   }
    //   return scents[index + 1];
    // }
    return scentSelected;
  }

  doChangeScent = (id, scentReplace) => {
    const { isNightProd } = this.state;
    const { dataDay, dataNight } = this.props;
    const datas = isNightProd ? dataNight : dataDay;
    const { data } = datas;
    const { combo } = data;
    const ele = _.find(combo, x => x.product.id === id);
    _.assign(ele.product, scentReplace);
    // ele.product.images = [{ image: scentReplace.image, type: 'unisex' }];
    this.props.callUpdateScentAll(datas);
  }

  changeScentWhenDislike = (id) => {
    const { topScent, botScent, heartScent } = this.state;
    const scentTop = this.getScentReplace(id, topScent);
    if (scentTop) {
      this.doChangeScent(id, scentTop);
      return;
    }
    const scentBot = this.getScentReplace(id, botScent);
    if (scentBot) {
      this.doChangeScent(id, scentBot);
      return;
    }
    const scentHeart = this.getScentReplace(id, heartScent);
    if (scentHeart) {
      this.doChangeScent(id, scentHeart);
    }
  }

  updateLikeData = () => {
    const { listDislike, listLike } = this.state;
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', this.props.idOutCome),
      method: 'PUT',
      body: {
        scents_liked: listLike,
        scents_disliked: listDislike,
      },
    };
    fetchClient(options);
  }

  createCustomMix = () => {
    const { isNightProd, listDislike } = this.state;
    const { dataDay, dataNight } = this.props;
    this.updateLikeData();
    this.props.gotoCustomMix(isNightProd ? dataNight : dataDay);
  }

  render() {
    const {
      isSmall, isRight, dataDay, dataNight, cmsCommon, onClickAddToCart,
      onClickIngredient, onClickImage, isHiddenAddCart,
      onClickCustomBottle, customeBottle, classButton, onSwitchIndex,
      isDisableSwitchScent, isOnlyPerfume, isGift, isDisableNameScent,
      buttonBlocks, isLandscape, namePath,
    } = this.props;
    const {
      isRollOn, isChangeScent, isChangeRollOn, listLike, listDislike, isNightProd,
    } = this.state;
    console.log('isChangeScent', isChangeScent, isNightProd);
    const datas = isNightProd ? dataNight : dataDay;
    this.props.dataUpdateProduct(datas);
    const { data, subTitle, title } = datas;
    const { items, profile } = data;
    const { combo } = data;
    const products = _.filter(combo || [], x => x.product.type === 'Scent');
    const itemRollOn = _.find(items, x => x.is_sample) || {};
    const itemFull = _.find(items, x => !x.is_sample) || {};
    const currentItem = isRollOn ? itemRollOn : itemFull;
    const recommentItem = isRollOn ? itemFull : itemRollOn;

    const addToCart = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
    const customeBt = getNameFromCommon(cmsCommon, 'CUSTOMIZE_BOTTLE');
    const tryOnBt = getNameFromCommon(cmsCommon, 'Try_Roll_on_sample_for');
    const getFullBottleBt = getNameFromCommon(cmsCommon, 'Get_the_Full_Bottle_for');
    const strengthBt = getNameFromCommon(cmsCommon, 'STRENGTH');
    const durationBt = getNameFromCommon(cmsCommon, 'DURATION');
    const alternativeBt = getNameFromCommon(cmsCommon, 'Alternative_Scent');
    const choosePerfumeBt = getNameFromCommon(cmsCommon, 'choose_perfume');
    const createMixBt = getNameFromButtonBlock(buttonBlocks, 'CREATE_CUSTOM_MIX');

    const chillBt = getNameFromCommon(cmsCommon, 'CHILL & RELAX');
    const seductionBt = getNameFromCommon(cmsCommon, 'seduction');
    const specialBt = getNameFromCommon(cmsCommon, 'SPECIAL OCCASION');
    const workBt = getNameFromCommon(cmsCommon, 'WORK & SOCIAL');
    const elegantBt = getNameFromCommon(cmsCommon, 'Elegant & Confident');
    const attractiveBt = getNameFromCommon(cmsCommon, 'Attractive & Sexy');
    const standoutBt = getNameFromCommon(cmsCommon, 'Stand out & Trendy');
    const freshBt = getNameFromCommon(cmsCommon, 'Fresh & Conforting');

    const titleQuiz = !isChangeScent && !isNightProd ? chillBt
      : !isChangeScent && isNightProd ? seductionBt
        : isChangeScent && isNightProd ? specialBt
          : isChangeScent && !isNightProd ? workBt : '';

    const subTitleQuiz = !isChangeScent && !isNightProd ? freshBt
      : !isChangeScent && isNightProd ? attractiveBt
        : isChangeScent && isNightProd ? standoutBt
          : isChangeScent && !isNightProd ? elegantBt : '';

    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      pauseOnHover: false,
    };
    const iconButton1 = (
      <div
        className={`div-icon-button animated ${isMobile && !isLandscape ? 'fadeInLeft' : 'fadeInUp'}`}
      >
        <div
          className={isHiddenAddCart ? 'hidden' : 'div-item-icon'}
          // onClick={() => onClickImage(isNightProd, true)}
          onClick={onClickCustomBottle}
        >
          <img src={icUploadFile} alt="icon" />
          <span>{customeBt}</span>
        </div>
        <hr className={isDisableSwitchScent ? 'hidden' : ''} />
        <div className={isDisableSwitchScent ? 'hidden' : 'div-item-icon'} onClick={() => { onSwitchIndex(isNightProd); this.onChangeScent(); }}>
          <img src={icSwitch} alt="icon" className={isChangeScent ? 'roate-animate' : ''} />
          <span>{alternativeBt}</span>
        </div>
      </div>
    );
    const iconButton2 = (
      <div className={`div-icon-button animated ${isMobile && !isLandscape ? 'fadeInRight' : 'fadeInDown'}`}>
        {/* <div className="div-item-icon" onClick={this.onChangeRollOn}>
          <img src={icSwitch} alt="icon" className={isChangeRollOn ? 'roate-animate' : ''} />
          <span>Customize Bottle</span>
        </div> */}
        <div className={isDisableSwitchScent ? 'hidden' : 'div-item-icon'} onClick={() => { onSwitchIndex(isNightProd); this.onChangeScent(); }}>
          <img src={icSwitch} alt="icon" className={isChangeScent ? 'roate-animate' : ''} />
          <span>{alternativeBt}</span>
        </div>
      </div>
    );

    const slideScent = (
      <div className="div-slide-scent">
        <div
          className="div-button-func-like div-left"
        >
          <button type="button" onClick={() => this.addListLike(products && products.length > 1 ? products[1].product.id : '')}>
            <img src={_.includes(listLike, products && products.length > 1 ? products[1].product.id : '') ? icLiked : icLike} alt="like" />
          </button>
          <hr />
          <button type="button" onClick={() => this.addListDisLike(products && products.length > 1 ? products[1].product.id : '')}>
            <img src={_.includes(listDislike, products && products.length > 1 ? products[1].product.id : '') ? icDisliked : icDislike} alt="disLike" />
          </button>
        </div>
        <div className="div-scent-image">
          <ItemImageScent
            isRollOn={isRollOn}
            data={data}
            onClickIngredient={onClickIngredient}
            onClickImage={onClickImage}
            isNightProd={isNightProd}
            customeBottle={customeBottle}
            cmsCommon={cmsCommon}
            isNewInfo={this.props.isNewInfo}
          />
        </div>
        <div className="div-button-func-like div-right">
          <button type="button" onClick={() => this.addListLike(products && products.length > 0 ? products[0].product.id : '')}>
            <img src={_.includes(listLike, products && products.length > 0 ? products[0].product.id : '') ? icLiked : icLike} alt="like" />
          </button>
          <hr />
          <button type="button" onClick={() => this.addListDisLike(products && products.length > 0 ? products[0].product.id : '')}>
            <img src={_.includes(listDislike, products && products.length > 0 ? products[0].product.id : '') ? icDisliked : icDislike} alt="disLike" />
          </button>
        </div>
      </div>
    );

    const sildeScentMobile = (
      <div className="div-slide-scent-mobile">
        <div
          className="div-button-func-like div-left"
        >
          <button type="button" onClick={() => this.addListLike(products && products.length > 1 ? products[1].product.id : '')}>
            <img src={_.includes(listLike, products && products.length > 1 ? products[1].product.id : '') ? icLiked : icLike} alt="like" />
          </button>
          <hr />
          <button type="button" onClick={() => this.addListDisLike(products && products.length > 1 ? products[1].product.id : '')}>
            <img src={_.includes(listDislike, products && products.length > 1 ? products[1].product.id : '') ? icDisliked : icDislike} alt="disLike" />
          </button>
        </div>
        <div className="div-scent-image-mobile">
          <ItemImageScent
            isRollOn={isRollOn}
            data={data}
            onClickIngredient={onClickIngredient}
            onClickImage={onClickImage}
            isNightProd={isNightProd}
            customeBottle={customeBottle}
            cmsCommon={cmsCommon}
            isNewInfo={this.props.isNewInfo}
          />
        </div>
        <div className="div-button-func-like div-right">
          <button type="button" onClick={() => this.addListLike(products && products.length > 0 ? products[0].product.id : '')}>
            <img src={_.includes(listLike, products && products.length > 0 ? products[0].product.id : '') ? icLiked : icLike} alt="like" />
          </button>
          <hr />
          <button type="button" onClick={() => this.addListDisLike(products && products.length > 0 ? products[0].product.id : '')}>
            <img src={_.includes(listDislike, products && products.length > 0 ? products[0].product.id : '') ? icDisliked : icDislike} alt="disLike" />
          </button>
        </div>
      </div>
    );

    const slideColor = (
      <div className="div-slide-color">
        <div className="div-process-line">
          {
            _.map(profile ? profile.accords : [], x => (
              <ProgressLine data={x} />
            ))
          }
        </div>
        <div className="div-process-circle">
          <ProgressCircle title={strengthBt} percent={parseInt(profile ? profile.strength * 100 : 0, 10)} />
          <div className="mt-4" />
          <ProgressCircle title={durationBt} percent={parseInt(profile ? profile.duration * 100 : 0, 10)} />
        </div>
      </div>
    );

    const webHtml = (
      <div className={`div-item-result-scent-boutique-v2 ${isRight ? 'right-margin' : ''} ${['boutique-quiz'].includes(namePath) ? 'boutique-quiz-item' : ''}`}>
        {
          ['boutique-quiz'].includes(namePath) ? (
            <div className="header-result header-boutique">
              <div className="header">
                <h1>
                  {titleQuiz}
                </h1>
                <span>
                  {subTitleQuiz}
                </span>
              </div>
            </div>
          ) : (
            <div className="header-result">
              <div className="header">
                <h1>
                  {title}
                </h1>
                <span>
                  {subTitle}
                </span>
              </div>
              <div className="div-bt-change">
                <button type="button" className={`button-bg__none ${isNightProd ? '' : 'active'}`} onClick={() => this.setState({ isNightProd: false })}>
                  <img src={isNightProd ? ic130 : ic130Selected} alt="icon" />
                  Day
                </button>
                <button type="button" className={`button-bg__none ${isNightProd ? 'active' : ''}`} onClick={() => this.setState({ isNightProd: true })}>
                  <img src={isNightProd ? ic131Selected : ic131} alt="icon" />
                  Night
                </button>
              </div>
            </div>
          )
        }
        {
          isSmall ? (
            <React.Fragment>
              <div className="div-content-small mt-3">
                <div className="div-image-small">
                  <ItemImageScent
                    isRollOn={isRollOn}
                    data={data}
                    onClickIngredient={onClickIngredient}
                    onClickImage={onClickImage}
                    isNightProd={isNightProd}
                    customeBottle={customeBottle}
                    cmsCommon={cmsCommon}
                    isDisableNameScent={isDisableNameScent}
                    isNewInfo={this.props.isNewInfo}
                  />
                </div>
                <div className="div-text-small animated fadeIn">
                  <div className="text-price">
                    <span className="vol mr-2">
                      25ml
                    </span>
                    <span className="price">
                      {generaCurrency(currentItem ? currentItem.price : '')}
                    </span>
                  </div>
                  <button type="button" className={isHiddenAddCart ? 'hidden' : `bt-add-cart ${classButton}`} onClick={() => onClickAddToCart(currentItem, subTitle, customeBottle)}>
                    {addToCart}
                  </button>
                  <button
                    type="button"
                    className="div-link-rollon"
                    onClick={() => {
                      this.setState({ isRollOn: !isRollOn });
                    }}
                  >
                    {`${isRollOn ? getFullBottleBt : tryOnBt} ${generaCurrency(recommentItem ? recommentItem.price : '')}`}
                  </button>
                </div>
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div className="div-content-item">
                <Slider
                  {...settings}
                >
                  {slideScent}
                  {slideColor}
                </Slider>
              </div>

              <div className={isDisableSwitchScent ? 'hidden' : 'div-icon-button'} onClick={() => { onSwitchIndex(isNightProd); this.onChangeScent(); }}>
                <img src={icSwitch} alt="icon" className={isChangeScent ? 'roate-animate' : ''} />
                <span>{alternativeBt}</span>
              </div>
              <button type="button" className="bt-create-custome-mix" onClick={this.createCustomMix}>
                {createMixBt}
              </button>
            </React.Fragment>
          )
        }
      </div>
    );
    const mobileHtml = (
      <div className={classnames('div-item-result-scent-v2-mobile result-boutique', ['boutique-quiz'].includes(namePath) ? 'boutique-quiz-item-mobile' : '')}>
        {
          ['boutique-quiz'].includes(namePath) ? (
            <div className="header-result header-boutique">
              <div className="header">
                <h1>
                  {titleQuiz}
                </h1>
                <span>
                  {subTitleQuiz}
                </span>
              </div>
            </div>
          ) : (
            <div className="div-name-price">
              <div className="div-header">
                <span className="name">
                  {title}
                </span>
                <span className="price">
                  {/* {generaCurrency(currentItem ? currentItem.price : '')} */}
                  {subTitle}
                </span>
              </div>
              <div className="div-bt-change">
                <button type="button" className={`button-bg__none ${isNightProd ? '' : 'active'}`} onClick={() => this.setState({ isNightProd: false })}>
                  <img src={isNightProd ? ic130 : ic130Selected} alt="icon" />
                  Day
                </button>
                <button type="button" className={`button-bg__none ${isNightProd ? 'active' : ''}`} onClick={() => this.setState({ isNightProd: true })}>
                  <img src={isNightProd ? ic131Selected : ic131} alt="icon" />
                  Night
                </button>
              </div>
            </div>
          )
        }
        <div className="div-content-slide">
          <Slider
            {...settings}
          >
            {sildeScentMobile}
            {slideColor}
          </Slider>
        </div>
        {/* <div className="div-button-func" style={isHiddenAddCart ? { marginTop: '10px' } : {}}>
          {iconButton2}
        </div> */}
        <div className={isDisableSwitchScent ? 'hidden' : 'div-icon-button'} onClick={() => { onSwitchIndex(isNightProd); this.onChangeScent(); }}>
          <img src={icSwitch} alt="icon" className={isChangeScent ? 'roate-animate' : ''} />
          <span>{alternativeBt}</span>
        </div>
        <button type="button" className="bt-create-custome-mix" onClick={this.createCustomMix}>
          {createMixBt}
        </button>
      </div>
    );
    return (
      <div className="div-wrap-item-result-scent">
        {isMobile && !isLandscape ? mobileHtml : webHtml}
      </div>
    );
  }
}

ItemResultScentLike.defaultProps = {
  isSmall: false,
  isRight: false,
  isNightProd: false,
  isHiddenAddCart: false,
  isDisableSwitchScent: false,
};

ItemResultScentLike.propTypes = {
  isSmall: PropTypes.bool,
  isRight: PropTypes.bool,
  data: PropTypes.shape().isRequired,
  isNightProd: PropTypes.bool,
  isHiddenAddCart: PropTypes.bool,
  isDisableSwitchScent: PropTypes.bool,
  customeBottle: PropTypes.shape().isRequired,
  onSwitchIndex: PropTypes.func.isRequired,
};


export default ItemResultScentLike;
