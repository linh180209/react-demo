import React, { Component } from 'react';
import Slider from 'react-slick';
import PropTypes from 'prop-types';
import _ from 'lodash';
import '../../styles/result-scent.scss';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { isTablet, isMobile } from '../../DetectScreen/detectIFrame';
import icUploadFile from '../../image/icon/ic-upload-file.svg';
import icSwitch from '../../image/icon/icSwitch.png';
import ItemImageScent from './itemImageScent';
import ProgressLine from './progressLine';
import ProgressCircle from './progressCircle';
import { getNameFromCommon, generaCurrency, getNameFromButtonBlock } from '../../Redux/Helpers';

class ItemResultScentKrisshop extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRollOn: false,
      isChangeScent: false,
      isChangeRollOn: false,
    };
    this.refSlider = React.createRef();
  }

  onChangeScent = () => {
    const { isChangeScent } = this.state;
    this.setState({ isChangeScent: !isChangeScent });
  }

  onChangeRollOn = () => {
    const { isChangeRollOn } = this.state;
    this.setState({ isChangeRollOn: !isChangeRollOn });
  }

  onClickGotoProductKrisshop = (url) => {
    window.parent.postMessage({ openLink: url }, '*');
  }

  render() {
    const {
      isSmall, isRight, data: datas, cmsCommon,
      onClickIngredient, onClickImage, isNightProd, isHiddenAddCart,
      onClickCustomBottle, customeBottle, classButton, onSwitchIndex,
      isDisableSwitchScent, isOnlyPerfume, isGift, isDisableNameScent,
      isLandscape, buttonBlocks,
    } = this.props;
    const { isRollOn, isChangeScent, isChangeRollOn } = this.state;
    const { data, subTitle, title } = datas;
    const { items, profile } = data;
    console.log('items', items);
    const itemRollOn = _.find(items, x => x.is_sample) || {};
    const itemFull = _.find(items, x => !x.is_sample) || {};
    const currentItem = isRollOn ? itemRollOn : itemFull;
    const recommentItem = isRollOn ? itemFull : itemRollOn;

    const strengthBt = getNameFromCommon(cmsCommon, 'STRENGTH');
    const durationBt = getNameFromCommon(cmsCommon, 'DURATION');
    const alternativeBt = getNameFromCommon(cmsCommon, 'Alternative_Scent');
    const viewProductBt = getNameFromButtonBlock(buttonBlocks, 'view_product');
    console.log('currentItem', currentItem);
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      pauseOnHover: false,
    };
    const iconButton1 = (
      <div
        className={`div-icon-button animated ${isMobile && !isLandscape ? 'fadeInLeft' : 'fadeInUp'}`}
      >
        {/* <div
          className={isHiddenAddCart ? 'hidden' : 'div-item-icon'}
          // onClick={() => onClickImage(isNightProd, true)}
          onClick={onClickCustomBottle}
        >
          <img src={icUploadFile} alt="icon" />
          <span>{customeBt}</span>
        </div>
        <hr className={isDisableSwitchScent ? 'hidden' : ''} /> */}
        <div className={isDisableSwitchScent ? 'hidden' : 'div-item-icon'} onClick={() => { onSwitchIndex(isNightProd); this.onChangeScent(); }}>
          <img src={icSwitch} alt="icon" className={isChangeScent ? 'roate-animate' : ''} />
          <span>{alternativeBt}</span>
        </div>
      </div>
    );
    const iconButton2 = (
      <div className={`div-icon-button animated ${isMobile && !isLandscape ? 'fadeInRight' : 'fadeInDown'}`}>
        {/* <div className="div-item-icon" onClick={this.onChangeRollOn}>
          <img src={icSwitch} alt="icon" className={isChangeRollOn ? 'roate-animate' : ''} />
          <span>Customize Bottle</span>
        </div> */}
        <div className={isDisableSwitchScent ? 'hidden' : 'div-item-icon'} onClick={() => { onSwitchIndex(isNightProd); this.onChangeScent(); }}>
          <img src={icSwitch} alt="icon" className={isChangeScent ? 'roate-animate' : ''} />
          <span>{alternativeBt}</span>
        </div>
      </div>
    );

    const slideScent = (
      <div className="div-slide-scent">
        <div
          className="div-button-func div-left"
        >
          {isRollOn ? iconButton2 : iconButton1}
        </div>
        <div className="div-scent-image">
          <ItemImageScent
            isRollOn={isRollOn}
            data={data}
            onClickIngredient={onClickIngredient}
            onClickImage={onClickImage}
            isNightProd={isNightProd}
            customeBottle={customeBottle}
            cmsCommon={cmsCommon}
          />
        </div>
        <div className="div-button-func div-right" />
      </div>
    );

    const sildeScentMobile = (
      <div className="div-slide-scent-mobile">
        <div className="div-scent-image-mobile">
          <ItemImageScent
            isRollOn={isRollOn}
            data={data}
            onClickIngredient={onClickIngredient}
            onClickImage={onClickImage}
            isNightProd={isNightProd}
            customeBottle={customeBottle}
            cmsCommon={cmsCommon}
          />
        </div>
      </div>
    );

    const slideColor = (
      <div className="div-slide-color">
        <div className="div-process-line">
          {
            _.map(profile ? profile.accords : [], x => (
              <ProgressLine data={x} />
            ))
          }
        </div>
        <div className="div-process-circle">
          <ProgressCircle title={strengthBt} percent={parseInt(profile ? profile.strength * 100 : 0, 10)} />
          <div className="mt-4" />
          <ProgressCircle title={durationBt} percent={parseInt(profile ? profile.duration * 100 : 0, 10)} />
        </div>
      </div>
    );

    const webHtml = (
      <div className={`div-item-result-scent-krisshop-v2 ${isRight ? 'right-margin' : ''}`}>
        <h1>
          {title}
        </h1>
        <span>
          {subTitle}
        </span>
        {
          isSmall ? (
            <React.Fragment>
              <div className="div-content-small mt-3">
                <div className="div-image-small">
                  <ItemImageScent
                    isRollOn={isRollOn}
                    data={data}
                    onClickIngredient={onClickIngredient}
                    onClickImage={onClickImage}
                    isNightProd={isNightProd}
                    customeBottle={customeBottle}
                    cmsCommon={cmsCommon}
                    isDisableNameScent={isDisableNameScent}
                  />
                </div>
                <div className="div-text-small animated fadeIn">
                  <div className="text-price">
                    <span className="vol mr-2">
                      30ml
                    </span>
                  </div>
                  <button
                    type="button"
                    className={isHiddenAddCart ? 'hidden' : `bt-add-cart ${classButton}`}
                    onClick={() => this.onClickGotoProductKrisshop(currentItem.krisshop)}
                  >
                    {viewProductBt}
                  </button>
                </div>
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div className="div-content-item">
                {/* <div className="div-version-1"> */}
                <Slider
                  {...settings}
                >
                  {slideScent}
                  {slideColor}
                </Slider>
                {/* </div> */}
                {/* <div className="div-version-2" /> */}
              </div>

              <div className={`div-price-result ${isOnlyPerfume ? 'div-center' : ''}`}>

                <div className="div-left">
                  <div className="text-price">
                    <span className="vol">
                      30ml
                    </span>
                  </div>
                </div>
                <button
                  type="button"
                  className={isHiddenAddCart ? 'hidden' : `bt-add-cart ${classButton}`}
                  onClick={() => this.onClickGotoProductKrisshop(currentItem.krisshop)}
                >
                  {viewProductBt}
                </button>
              </div>
            </React.Fragment>
          )
        }
      </div>
    );
    const mobileHtml = (
      <div className="div-item-result-scent-v2-mobile">
        <div className="div-name-price">
          <div className="div-header">
            <span className="name">
              {title}
            </span>
            {/* <span className="price">
              {generaCurrency(currentItem ? currentItem.price : '')}
            </span> */}
          </div>
          <div className="div-des">
            <span>
              {subTitle}
            </span>
            <span>
              25ml
            </span>
          </div>
        </div>
        <div className="div-content-slide">
          <Slider
            {...settings}
          >
            {sildeScentMobile}
            {slideColor}
          </Slider>
        </div>
        <div className="div-button-func" style={isHiddenAddCart ? { marginTop: '10px' } : {}}>
          {isRollOn ? iconButton2 : iconButton1}
        </div>

        <button
          className={isHiddenAddCart ? 'hidden' : 'button-add-cart-mobile'}
          type="button"
          onClick={() => this.onClickGotoProductKrisshop(currentItem.krisshop)}
        >
          {viewProductBt}
        </button>
      </div>
    );
    return (
      <div className="div-wrap-item-result-scent">
        {isMobile && !isLandscape ? mobileHtml : webHtml}
      </div>
    );
  }
}

ItemResultScentKrisshop.defaultProps = {
  isSmall: false,
  isRight: false,
  isNightProd: false,
  isHiddenAddCart: false,
  isDisableSwitchScent: false,
};

ItemResultScentKrisshop.propTypes = {
  isSmall: PropTypes.bool,
  isRight: PropTypes.bool,
  data: PropTypes.shape().isRequired,
  isNightProd: PropTypes.bool,
  isHiddenAddCart: PropTypes.bool,
  isDisableSwitchScent: PropTypes.bool,
  customeBottle: PropTypes.shape().isRequired,
  onSwitchIndex: PropTypes.func.isRequired,
};

export default ItemResultScentKrisshop;
