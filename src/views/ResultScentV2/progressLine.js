import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import '../../styles/result-scent.scss';

class ProgressLine extends Component {
  render() {
    const { data, isShowPercent } = this.props;
    const { name, color, weight } = data;
    return (
      <div className={classnames('progressLine', this.props.className)}>
        <div className="div-text">
          <span>
            {name}
          </span>
          <span>
            {isShowPercent ? `${weight}%` : ''}
          </span>
        </div>
        <div className="div-progress">
          <div className="bg-progress" />
          <div className="tint-color" style={{ backgroundColor: color, width: `${weight}%` }} />
        </div>
      </div>
    );
  }
}

ProgressLine.propTypes = {
  data: PropTypes.shape().isRequired,
  isShowPercent: PropTypes.isRequired,
};

export default ProgressLine;
