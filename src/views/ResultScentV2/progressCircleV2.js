import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import './progressCircle.scss';

class ProgressCircleV2 extends Component {
  render() {
    const { percent, title } = this.props;
    return (
      <div className={classnames('progress-circle-version-2')}>
        <div className="div-circle mt-2">
          <CircularProgressbar
            percentage={percent}
            text={`${percent}%`}
            background
            backgroundPadding={10}
            styles={{
              text: {
                fontSize: '12px',
              },
              trail: {
                strokeWidth: 14,
                stroke: '#EBD8B8',
              },
              path: {
                strokeWidth: 7,
                stroke: this.props.trail || '#C37B53',
              },
              background: {
                fill: this.props.fill || '#F1E4CD',
              },
              strokeLinecap: 'butt',
            }}
          />
        </div>

        <span className="title">
          {title}
        </span>

      </div>
    );
  }
}

ProgressCircleV2.defaultProps = {
  percent: 50,
};
ProgressCircleV2.propTypes = {
  percent: PropTypes.number,
  title: PropTypes.string.isRequired,
};
export default ProgressCircleV2;
