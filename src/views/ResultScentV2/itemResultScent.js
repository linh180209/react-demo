import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import PropTypes from 'prop-types';
import _ from 'lodash';
import '../../styles/result-scent.scss';
import icUploadFile from '../../image/icon/ic-upload-file.svg';
import icSwitch from '../../image/icon/icSwitch.png';
import icEdit from '../../image/icon/ic-edit.svg';
import ItemImageScent from './itemImageScent';
import ProgressLine from './progressLine';
import ProgressCircle from './progressCircle';
import { getNameFromCommon, generaCurrency } from '../../Redux/Helpers';
import { isBrowser, isMobile, isMobileOnly } from '../../DetectScreen';
import { GET_PRICES_PRODUCTS } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';

class ItemResultScent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRollOn: false,
      isChangeScent: false,
      isChangeRollOn: false,
      priceDualCrayons: undefined,
    };
    this.refSlider = React.createRef();
  }

  getPrice = (type) => {
    try {
      const option = {
        url: GET_PRICES_PRODUCTS.replace('{type}', type),
        method: 'GET',
      };
      return fetchClient(option);
    } catch (error) {
      toastrError('error', error.message);
    }
  }

  componentDidMount = () => {
    this.getPrice('dual_crayons').then((result) => {
      this.setState({ priceDualCrayons: result[0].price });
    });
  }

  onChangeScent = () => {
    const { isChangeScent } = this.state;
    this.setState({ isChangeScent: !isChangeScent });
  }

  onChangeRollOn = () => {
    const { isChangeRollOn } = this.state;
    this.setState({ isChangeRollOn: !isChangeRollOn });
  }

  onClickChillRelax = () => {
    this.setState({ isChangeScent: false });
    this.props.onSwitchIndex(false, 0);
  }

  onClickSeducation = () => {
    this.setState({ isChangeScent: true });
    this.props.onSwitchIndex(true, 0);
  }

  onClickSpecialOccasion = () => {
    this.setState({ isChangeScent: false });
    this.props.onSwitchIndex(true, 1);
  }

  onClickWorkSocial = () => {
    this.setState({ isChangeScent: true });
    this.props.onSwitchIndex(false, 1);
  }


  render() {
    const {
      isSmall, isRight, data: datas, cmsCommon, onClickAddToCart,
      onClickIngredient, onClickImage, isNightProd, isHiddenAddCart,
      onClickCustomBottle, customeBottle, classButton, onSwitchIndex,
      isDisableSwitchScent, isOnlyPerfume, isGift, isDisableNameScent,
      isLandscape, isPerfumeCombinations, isResultAccount, isResultQuiz,
      isDisableButtonSwitchCustom, isDisableButtonRightSwitchCustom, titleQuiz, subTitleQuiz,
    } = this.props;
    const { isRollOn, isChangeScent, priceDualCrayons } = this.state;
    const {
      data, subTitle, title, price, ml,
    } = datas;
    const { items, profile } = data;
    const itemRollOn = _.find(items, x => x.is_sample) || {};
    const itemFull = _.find(items, x => !x.is_sample) || {};
    const currentItem = isRollOn ? itemRollOn : itemFull;
    const recommentItem = isRollOn ? itemFull : itemRollOn;

    const addToCart = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
    const customeBt = getNameFromCommon(cmsCommon, 'CUSTOMIZE_BOTTLE');
    const tryOnBt = getNameFromCommon(cmsCommon, 'Try_Roll_on_sample_for');
    const getFullBottleBt = getNameFromCommon(cmsCommon, 'Get_the_Full_Bottle_for');
    const ml25 = getNameFromCommon(cmsCommon, 'ml_25');
    const ml5 = getNameFromCommon(cmsCommon, 'ml_5x2');
    const strengthBt = getNameFromCommon(cmsCommon, 'STRENGTH');
    const durationBt = getNameFromCommon(cmsCommon, 'DURATION');
    const alternativeBt = getNameFromCommon(cmsCommon, 'Alternative_Scent');
    const choosePerfumeBt = getNameFromCommon(cmsCommon, 'choose_perfume');
    const viewDetailBt = getNameFromCommon(cmsCommon, 'view detail');

    const settings = {
      dots: true,
      infinite: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      pauseOnHover: false,
    };
    const iconButton1 = (
      <div
        className={`div-icon-button animated ${(isMobile && !isLandscape || isMobileOnly) ? 'fadeInLeft' : 'fadeInUp'}`}
      >
        <div
          className={isHiddenAddCart ? 'hidden' : 'div-item-icon'}
          // onClick={() => onClickImage(isNightProd, true)}
          onClick={onClickCustomBottle}
          data-gtmtracking="customize-bottle-step-1"
        >
          <img src={icUploadFile} alt="icon" data-gtmtracking="customize-bottle-step-1" />
          <span data-gtmtracking="customize-bottle-step-1">{customeBt}</span>
        </div>
        <hr className={isDisableSwitchScent || isBrowser ? 'hidden' : ''} />
        <div className={isDisableSwitchScent || isBrowser ? 'hidden' : 'div-item-icon'} onClick={() => { onSwitchIndex(isNightProd); this.onChangeScent(); }}>
          <img src={icSwitch} alt="icon" className={isChangeScent ? 'roate-animate' : ''} />
          <span>{alternativeBt}</span>
        </div>
      </div>
    );
    const iconButton2 = (
      <div className={`div-icon-button animated ${(isMobile && !isLandscape || isMobileOnly) ? 'fadeInRight' : 'fadeInDown'}`}>
        {/* <div className="div-item-icon" onClick={this.onChangeRollOn}>
          <img src={icSwitch} alt="icon" className={isChangeRollOn ? 'roate-animate' : ''} />
          <span>Customize Bottle</span>
        </div> */}
        <div className={isDisableSwitchScent ? 'hidden' : 'div-item-icon'} onClick={() => { onSwitchIndex(isNightProd); this.onChangeScent(); }}>
          <img src={icSwitch} alt="icon" className={isChangeScent ? 'roate-animate' : ''} />
          <span>{alternativeBt}</span>
        </div>
      </div>
    );

    const slideScent = (
      <div className="div-slide-scent">
        <div
          className="div-button-func div-left"
        >
          {isRollOn || isDisableButtonSwitchCustom ? null : iconButton1}
        </div>
        <div className="div-scent-image">
          <ItemImageScent
            isRollOn={isRollOn}
            data={data}
            onClickIngredient={onClickIngredient}
            // onClickImage={onClickImage}
            onClickImage={() => onClickImage(isNightProd, isRollOn)}
            isNightProd={isNightProd}
            customeBottle={customeBottle}
            cmsCommon={cmsCommon}
            isDisableNameScent={this.props.isDisableNameScent}
            isNewInfo={this.props.isNewInfo}
          />
        </div>
        <div className="div-button-func div-right">
          {isDisableButtonSwitchCustom || isDisableButtonRightSwitchCustom ? null : iconButton2}
        </div>
      </div>
    );

    const sildeScentMobile = (
      <div className="div-slide-scent-mobile">
        <div className="div-scent-image-mobile">
          <ItemImageScent
            isRollOn={isRollOn}
            data={data}
            onClickIngredient={onClickIngredient}
            // onClickImage={onClickImage}
            onClickImage={() => onClickImage(isNightProd, isRollOn)}
            isNightProd={isNightProd}
            customeBottle={customeBottle}
            cmsCommon={cmsCommon}
            isNewInfo={this.props.isNewInfo}
          />
        </div>
      </div>
    );

    const slideColor = (
      <div className="div-slide-color">
        <div className="div-process-line">
          {
            _.map(profile ? profile.accords : [], x => (
              <ProgressLine data={x} />
            ))
          }
        </div>
        <div className="div-process-circle">
          <ProgressCircle title={strengthBt} percent={parseInt(profile ? profile.strength * 100 : 0, 10)} />
          <div className="mt-4" />
          <ProgressCircle title={durationBt} percent={parseInt(profile ? profile.duration * 100 : 0, 10)} />
        </div>
      </div>
    );

    const webHtml = (
      <div className={`div-item-result-scent-v2 ${isRight ? 'right-margin' : ''}`}>
        {
          isPerfumeCombinations ? (
            <div className="div-header-combinations">
              <div className="div-top">
                <span>{title}</span>
                {!isResultAccount && <h2>{generaCurrency(isRollOn ? priceDualCrayons : currentItem?.price)}</h2>}
              </div>
              <div className="div-bottom">
                <h2>
                  {subTitle}
                </h2>
                {!isResultAccount && (<span>{isRollOn ? ml5 : ml25}</span>)}
              </div>
            </div>
          ) : isResultQuiz ? (
            <div className="div-header-quiz">
              <div className="header-text">
                <h4>{titleQuiz}</h4>
                <span>{subTitleQuiz}</span>
              </div>
              <div className="div-price">
                <h4>{generaCurrency(isRollOn ? priceDualCrayons : currentItem?.price)}</h4>
                <span>
                  {isRollOn ? ml5 : ml25}
                </span>
              </div>
            </div>
          ) : (
            <React.Fragment>
              <h1>
                {title}
              </h1>
              <span>
                {subTitle}
              </span>
            </React.Fragment>
          )
        }

        {
          isSmall ? (
            <React.Fragment>
              <div className="div-content-small mt-3">
                <div className="div-image-small">
                  <ItemImageScent
                    isRollOn={isRollOn}
                    data={data}
                    onClickIngredient={onClickIngredient}
                    // onClickImage={onClickImage}
                    onClickImage={() => onClickImage(isNightProd, isRollOn)}
                    isNightProd={isNightProd}
                    customeBottle={customeBottle}
                    cmsCommon={cmsCommon}
                    isDisableNameScent={isDisableNameScent}
                  />
                </div>
                <div className="div-text-small animated fadeIn">
                  <div className="text-price">
                    <span className="vol mr-2">
                      {ml25}
                    </span>
                    <span className="price">
                      {generaCurrency(isRollOn ? priceDualCrayons : currentItem.price)}
                    </span>
                  </div>
                  <button
                    data-gtmtracking={this.props.dataGtmtracking}
                    type="button"
                    className={isHiddenAddCart ? 'hidden' : `bt-add-cart ${classButton}`}
                    onClick={() => onClickAddToCart(currentItem, subTitle, customeBottle, data)}
                  >
                    {addToCart}
                  </button>
                  <button
                    type="button"
                    className="div-link-rollon"
                    onClick={() => {
                      this.setState({ isRollOn: !isRollOn });
                    }}
                  >
                    {`${isRollOn ? getFullBottleBt : tryOnBt} ${generaCurrency(!isRollOn ? priceDualCrayons : recommentItem?.price)}`}
                  </button>
                </div>
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div className="div-content-item">
                {/* <div className="div-version-1"> */}
                <Slider
                  {...settings}
                >
                  {slideScent}
                  {slideColor}
                </Slider>
                {/* </div> */}
                {/* <div className="div-version-2" /> */}
              </div>

              <div className={`div-price-result ${isOnlyPerfume ? 'div-center' : ''}`}>
                {
                  isPerfumeCombinations ? (
                    <div className="div-bottom-button">
                      <div className="line-button">
                        <button
                          data-gtmtracking={this.props.dataGtmtracking}
                          onClick={() => onClickImage(isNightProd, isRollOn)}
                          type="button"
                          className="bt-add-cart bt-outline"
                        >
                          {viewDetailBt}
                        </button>
                        <button
                          data-gtmtracking={this.props.dataGtmtracking}
                          onClick={() => (this.props.onClickAddToCart(currentItem, subTitle, customeBottle, data))}
                          type="button"
                          className="bt-add-cart bt-black"
                        >
                          {addToCart}
                        </button>
                      </div>
                      <button
                        type="button"
                        className="div-link-rollon mt-2"
                        onClick={() => {
                          this.setState({ isRollOn: !isRollOn });
                        }}
                      >
                        {`${isRollOn ? getFullBottleBt : tryOnBt} ${generaCurrency(!isRollOn ? priceDualCrayons : recommentItem?.price)}`}
                      </button>
                    </div>
                  ) : isOnlyPerfume ? (
                    <div className="div-button-add">
                      <button
                        data-gtmtracking={this.props.dataGtmtracking}
                        onClick={() => (isGift ? this.props.onClickCheckOutGift(currentItem, subTitle, customeBottle) : this.props.onClickAddToCart(currentItem, subTitle, customeBottle, data))}
                        type="button"
                        className="bt-add-cart bt-black mt-4"
                      >
                        {isGift ? choosePerfumeBt : addToCart}
                      </button>
                    </div>
                  ) : isResultQuiz ? (
                    <div className="bottom-quiz">
                      <div className="list-bt">
                        {/* {
                          !isRollOn && ( */}
                        <button
                          type="button"
                          // onClick={onClickCustomBottle}
                          onClick={() => onClickImage(isNightProd, isRollOn)}
                          data-gtmtracking="customize-bottle-step-1"
                          className={isHiddenAddCart ? 'hidden' : 'bt-add-cart bt-outline'}
                        >
                          {/* <img src={icEdit} alt="upload" /> */}
                          {viewDetailBt}
                        </button>
                        {/* )
                        } */}
                        <button
                          data-gtmtracking={this.props.dataGtmtracking}
                          type="button"
                          className={isHiddenAddCart ? 'hidden' : `bt-add-cart ${classButton}`}
                          onClick={() => onClickAddToCart(currentItem, subTitle, customeBottle, data)}
                        >
                          {addToCart}
                        </button>
                      </div>
                      <button
                        type="button"
                        className="div-link-rollon"
                        onClick={() => {
                          this.setState({ isRollOn: !isRollOn });
                        }}
                      >
                        {`${isRollOn ? getFullBottleBt : tryOnBt} ${generaCurrency(!isRollOn ? priceDualCrayons : recommentItem?.price)}`}
                      </button>
                    </div>
                  ) : (
                    <React.Fragment>
                      <div className="div-left">
                        <div className="text-price">
                          <span className="price">
                            {generaCurrency(isRollOn ? priceDualCrayons : currentItem.price)}
                          </span>
                          <span className="vol">
                            {isRollOn ? ml5 : ml25}
                          </span>
                        </div>
                        <button
                          type="button"
                          className="div-link-rollon"
                          onClick={() => {
                            this.setState({ isRollOn: !isRollOn });
                          }}
                        >
                          {`${isRollOn ? getFullBottleBt : tryOnBt} ${generaCurrency(!isRollOn ? priceDualCrayons : recommentItem?.price)}`}
                        </button>
                      </div>
                      <button
                        data-gtmtracking={this.props.dataGtmtracking}
                        type="button"
                        className={isHiddenAddCart ? 'hidden' : `bt-add-cart ${classButton}`}
                        onClick={() => onClickAddToCart(currentItem, subTitle, customeBottle, data)}
                      >
                        {addToCart}
                      </button>
                    </React.Fragment>
                  )
                }
              </div>
            </React.Fragment>
          )
        }
      </div>
    );
    const mobileHtml = (
      <div className="div-item-result-scent-v2-mobile">
        <div className="div-name-price">
          <div className="div-header">
            <span className="name">
              {isPerfumeCombinations ? title : titleQuiz}
            </span>
            <span className="price">
              {generaCurrency(isRollOn ? priceDualCrayons : currentItem?.price)}
            </span>
          </div>
          <div className="div-des">
            <span>
              {isPerfumeCombinations ? subTitle : subTitleQuiz}
            </span>
            <span>
              {ml25}
            </span>
          </div>
        </div>
        <div className="div-content-slide">
          <Slider
            {...settings}
          >
            {sildeScentMobile}
            {slideColor}
          </Slider>
        </div>
        {
          isPerfumeCombinations && (
            <div className="line-button">
              <button
                data-gtmtracking={this.props.dataGtmtracking}
                onClick={() => onClickImage(isNightProd, isRollOn)}
                type="button"
                className="bt-add-cart bt-outline"
              >
                {viewDetailBt}
              </button>
              <button
                data-gtmtracking={this.props.dataGtmtracking}
                onClick={() => (this.props.onClickAddToCart(currentItem, subTitle, customeBottle, data))}
                type="button"
                className="bt-add-cart bt-black"
              >
                {addToCart}
              </button>
            </div>
          )
        }
        <div className={isDisableButtonSwitchCustom ? 'hidden' : 'div-button-func'} style={isHiddenAddCart ? { marginTop: '10px' } : {}}>
          {isRollOn ? iconButton2 : iconButton1}
        </div>
        {
          isOnlyPerfume ? (
            <button
              data-gtmtracking={this.props.dataGtmtracking}
              onClick={() => (isGift ? this.props.onClickCheckOutGift(currentItem, subTitle, customeBottle) : this.props.onClickAddToCart(currentItem, subTitle, customeBottle, data))}
              type="button"
              className="bt-add-cart bt-black mt-4"
            >
              {isGift ? choosePerfumeBt : addToCart}
            </button>
          ) : isResultQuiz ? (
            <div className="bottom-quiz-mobile">
              {
                !isRollOn && (
                  <button
                    type="button"
                    onClick={onClickCustomBottle}
                    data-gtmtracking="customize-bottle-step-1"
                    className={isHiddenAddCart ? 'hidden' : `bt-cutome ${classButton}`}
                  >
                    <img src={icEdit} alt="upload" />
                    {customeBt}
                  </button>
                )
              }
              <button
                data-gtmtracking={this.props.dataGtmtracking}
                className={isHiddenAddCart ? 'hidden' : 'button-add-cart-mobile'}
                type="button"
                onClick={() => onClickAddToCart(currentItem, subTitle, customeBottle, data)}
              >
                {addToCart}
              </button>
              <button
                className="button-link-mobile"
                type="button"
                onClick={() => {
                  this.setState({ isRollOn: !isRollOn });
                }}
              >
                {`${isRollOn ? getFullBottleBt : tryOnBt} ${generaCurrency(!isRollOn ? priceDualCrayons : recommentItem?.price)}`}
              </button>
            </div>
          ) : (
            <React.Fragment>
              <button
                data-gtmtracking={this.props.dataGtmtracking}
                className={isHiddenAddCart || isPerfumeCombinations ? 'hidden' : 'button-add-cart-mobile'}
                type="button"
                onClick={() => onClickAddToCart(currentItem, subTitle, customeBottle, data)}
              >
                {addToCart}
              </button>
              <button
                className="button-link-mobile mt-2"
                type="button"
                onClick={() => {
                  this.setState({ isRollOn: !isRollOn });
                }}
              >
                {`${isRollOn ? getFullBottleBt : tryOnBt} ${generaCurrency(!isRollOn ? priceDualCrayons : recommentItem?.price)}`}
              </button>
            </React.Fragment>
          )
        }
      </div>
    );
    return (
      <div className="div-wrap-item-result-scent">
        {(isMobile && !isLandscape) || isMobileOnly ? (isResultAccount ? webHtml : mobileHtml) : webHtml}
      </div>
    );
  }
}

ItemResultScent.defaultProps = {
  isSmall: false,
  isRight: false,
  isResultAccount: false,
  isNightProd: false,
  isHiddenAddCart: false,
  isDisableSwitchScent: false,
};

ItemResultScent.propTypes = {
  isSmall: PropTypes.bool,
  isResultAccount: PropTypes.bool,
  isRight: PropTypes.bool,
  data: PropTypes.shape().isRequired,
  isNightProd: PropTypes.bool,
  isHiddenAddCart: PropTypes.bool,
  isDisableSwitchScent: PropTypes.bool,
  customeBottle: PropTypes.shape().isRequired,
  onSwitchIndex: PropTypes.func.isRequired,
};

export default ItemResultScent;
