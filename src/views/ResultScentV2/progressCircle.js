import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import './progressCircle.scss';

class ProgressCircle extends Component {
  render() {
    const { percent, title } = this.props;
    return (
      <div className={classnames('progress-circle')}>

        <span>
          {title}
        </span>
        <div className="div-circle mt-2">
          <CircularProgressbar
            percentage={percent}
            text={`${percent}%`}
            styles={{
              text: {
                fontSize: '25px',
              },
              strokeLinecap: 'butt',
            }}
          />
        </div>

      </div>
    );
  }
}

ProgressCircle.defaultProps = {
  percent: 50,
};
ProgressCircle.propTypes = {
  percent: PropTypes.number,
  title: PropTypes.string.isRequired,
};
export default ProgressCircle;
