import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import CircularProgressbar from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import './progressCircle.scss';

class ProgressCircleV3 extends Component {
  convertPercent = (percent) => {
    if (percent % 10 === 0) {
      return parseInt(percent / 10, 10);
    }
    return `${parseInt(percent / 10, 10)}.${percent % 10}`;
  }

  render() {
    const { percent } = this.props;
    return (
      <div className={classnames('progress-circle-version-3')}>
        <div className="div-circle">
          <CircularProgressbar
            percentage={percent}
            text={`${this.convertPercent(parseInt(percent, 10))}h`}
            background
            backgroundPadding={10}
            styles={{
              text: {
                fontSize: '18px',
                fill: this.props.textColor || this.props.trail,
              },
              trail: {
                strokeWidth: 14,
                stroke: this.props.stroke || '#EBD8B8',
              },
              path: {
                strokeWidth: 7,
                stroke: this.props.trail || '#C37B53',
              },
              background: {
                fill: this.props.fill || '#F1E4CD',
              },
              strokeLinecap: 'butt',
            }}
          />
        </div>
      </div>
    );
  }
}

ProgressCircleV3.defaultProps = {
  percent: 50,
};
ProgressCircleV3.propTypes = {
  percent: PropTypes.number,
};
export default ProgressCircleV3;
