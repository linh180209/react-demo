import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import BottomScrollListener from 'react-bottom-scroll-listener';
import _ from 'lodash';
import classnames from 'classnames';
import queryString from 'query-string';
// import { isMobileOnly } from 'react-device-detect';
import {
  isMobile, isBrowser, isTablet, isMobileOnly,
} from '../../DetectScreen/detectIFrame';
import '../../styles/result-scent.scss';

import icRetake from '../../image/icon/ic-retake.svg';
import icBack from '../../image/icon/ic-back.svg';
import ItemResultScent from './itemResultScent';
import icExtendFooter from '../../image/icon/icExtendFooter.svg';
import ic2Bottle from '../../image/icon/ic2Bottle.svg';
import ItemImageScent from './itemImageScent';
import CustomeBottleV3 from '../../components/CustomeBottleV3';
import icRetakeGray from '../../image/icon/ic-retake-gray.svg';
import icSearch from '../../image/icon/ic-search-black.svg';
import ic120 from '../../image/icon/ic-12-0.svg';
import ic120Selected from '../../image/icon/ic-12-0-selected.svg';
import ic121 from '../../image/icon/ic-12-1.svg';
import ic121Selected from '../../image/icon/ic-12-1-selected.svg';
import ic122 from '../../image/icon/ic-12-2.svg';
import ic122Selected from '../../image/icon/ic-12-2-selected.svg';
import ic123 from '../../image/icon/ic-12-3.svg';
import ic123Selected from '../../image/icon/ic-12-3-selected.svg';
import {
  generaCurrency, getNameFromCommon, scrollTop, googleAnalitycs, getNameFromButtonBlock,
  hiddenChatIcon, visibilityChatIcon,
} from '../../Redux/Helpers';
import { FONT_CUSTOME, COLOR_CUSTOME } from '../../constants';
import ItemResultScentLike from './ItemResultScentLike';
import ItemResultScentKrisshop from './itemResultScentKrisshop';
import BuyMoreDiscounts from '../../components/BuyMoreDiscounts';

class ResultScentV2 extends Component {
  constructor(props) {
    super(props);
    const { infoGift, customeBottle } = this.props;
    this.state = {
      isExtendFooter: false,
      isOpenPopUpMobile: false,
      isDayCustom: true,
      arrayThumbs: [],
      isShowDescription: false,
      dataCustomDay: {
        currentImg: !_.isEmpty(infoGift) ? infoGift.image : customeBottle ? customeBottle.currentImg : undefined,
        nameBottle: !_.isEmpty(infoGift) && infoGift.isDisplayName ? infoGift.name : customeBottle ? customeBottle.nameBottle : '',
        font: !_.isEmpty(infoGift) ? infoGift.font : customeBottle ? customeBottle.font : FONT_CUSTOME.JOST,
        color: !_.isEmpty(infoGift) ? infoGift.color : customeBottle ? customeBottle.color : COLOR_CUSTOME.BLACK,
      },
      dataCustomNight: {
        currentImg: !_.isEmpty(infoGift) ? infoGift.image : customeBottle ? customeBottle.currentImg : undefined,
        nameBottle: !_.isEmpty(infoGift) && infoGift.isDisplayName ? infoGift.name : customeBottle ? customeBottle.nameBottle : '',
        font: !_.isEmpty(infoGift) ? infoGift.font : customeBottle ? customeBottle.font : FONT_CUSTOME.JOST,
        color: !_.isEmpty(infoGift) ? infoGift.color : customeBottle ? customeBottle.color : COLOR_CUSTOME.BLACK,
      },
      redeemCode: infoGift ? infoGift.codes : '',
      isShowCustom: false,
      isChillRelax: props.mixes === 1,
      isSeducation: props.mixes === 3,
      isSpecialOccasion: props.mixes === 4,
      isWorkSocial: props.mixes === 2,
    };
    this.refItemResultScentLike = React.createRef();
  }

  componentDidMount() {
    scrollTop();
    googleAnalitycs(this.props.namePath === 'quiz' ? '/quiz/outcome/scent' : this.props.namePath === 'krisshop' ? '/krisshop/outcome/scent' : this.props.namePath === 'partnership-quiz' ? '/partnership-quiz/outcome' : this.props.namePath === 'sephora' ? '/sephora/outcome/scent' : this.props.namePath === 'sofitel' ? '/sofitel/outcome/scent' : this.props.namePath === 'bmw' ? '/bmw/outcome/scent' : '/boutique-quiz/outcome/scent');
    if (isBrowser || (isTablet && this.props.isLandscape)) {
      document.getElementById('myDIV').addEventListener('wheel', this.myFunction);
    }
    if (isMobile && !this.props.isLandscape) {
      hiddenChatIcon();
      setTimeout(() => {
        this.scrollToActive();
      }, 200);
    }
  }

  componentWillUnmount() {
    if (isBrowser || (isTablet && this.props.isLandscape)) {
      document.getElementById('myDIV').removeEventListener('wheel', this.myFunction);
    }
    if ((isMobile && !this.props.isLandscape) || isMobileOnly) {
      visibilityChatIcon();
    }
  }

  scrollToActive = () => {
    const elediv = document.getElementById('list-option');
    const ele = document.getElementById('id-active');
    if (elediv && ele) {
      const value = ele.getBoundingClientRect().left - 20;
      elediv.scrollLeft = value < 0 ? 0 : value;
    }
  }

  myFunction = (event) => {
    if (this.props.namePath !== 'quiz' || this.state.redeemCode) {
      return;
    }
    const { deltaY } = event;
    const { isExtendFooter } = this.state;
    if (deltaY > 10 && !isExtendFooter) {
      this.setState({ isExtendFooter: true });
    }
    if (deltaY < -10 && isExtendFooter) {
      this.setState({ isExtendFooter: false });
    }
  }

  onBottomDetect = () => {
    const { isOpenPopUpMobile } = this.state;
    if ((isMobile && !this.props.isLandscape || isMobileOnly) && !isOpenPopUpMobile) {
      this.setState({ isExtendFooter: true, isOpenPopUpMobile: true });
    }
  }

  generateDataCustomeBottome = (data) => {
    const {
      combo,
    } = data;
    const bottles = _.find(combo, x => x.product.type === 'Bottle');
    const arrayThumbs = _.filter(bottles.product.images, x => x.type === 'suggestion');
    return arrayThumbs;
  }

  handleCropImage = (img) => {
    const { isDayCustom, dataCustomDay, dataCustomNight } = this.state;
    if (isDayCustom) {
      dataCustomDay.currentImg = img;
      this.setState({ dataCustomDay });
    } else {
      dataCustomNight.currentImg = img;
      this.setState({ dataCustomNight });
    }
  };

  onSaveCustomer = (data) => {
    const { isDayCustom, dataCustomDay, dataCustomNight } = this.state;
    const dataCustom = isDayCustom ? dataCustomDay : dataCustomNight;
    const {
      image, name, color, font,
    } = data;
    dataCustom.color = color;
    dataCustom.font = font;
    if (_.isEmpty(image)) {
      dataCustom.currentImg = undefined;
      dataCustom.nameBottle = name;
      this.forceUpdate();
      return;
    }
    dataCustom.currentImg = image;
    dataCustom.nameBottle = name;
    const imagePremade = _.find(this.state.arrayThumbs, x => x.image === dataCustom.currentImg) ? _.find(this.state.arrayThumbs, x => x.image === dataCustom.currentImg).id : undefined;
    _.assign(dataCustom, { imagePremade });
    this.forceUpdate();
  }

  onClickCustomBottle = (isDayCustom) => {
    const arrayThumbs = this.generateDataCustomeBottome(isDayCustom ? this.props.dataDay.data : this.props.dataNight.data);
    this.setState({ isDayCustom, arrayThumbs, isShowCustom: true });
  }

  onChangeOption = (id) => {
    if (this.props.isViewExternalProduct) {
      this.props.onChangeExternalProduct();
    }
    switch (id) {
      case 1:
        this.setState({
          isChillRelax: true,
          isSeducation: false,
          isSpecialOccasion: false,
          isWorkSocial: false,
        });
        this.refItemResultScentLike.current.onClickChillRelax();
        break;
      case 2:
        this.setState({
          isChillRelax: false,
          isSeducation: true,
          isSpecialOccasion: false,
          isWorkSocial: false,
        });
        this.refItemResultScentLike.current.onClickSeducation();
        break;
      case 3:
        this.setState({
          isChillRelax: false,
          isSeducation: false,
          isSpecialOccasion: true,
          isWorkSocial: false,
        });
        this.refItemResultScentLike.current.onClickSpecialOccasion();
        break;
      case 4:
        this.setState({
          isChillRelax: false,
          isSeducation: false,
          isSpecialOccasion: false,
          isWorkSocial: true,
        });
        this.refItemResultScentLike.current.onClickWorkSocial();
        break;
      default:
        break;
    }
  }

  renderOption = d => (
    <div
      onClick={() => this.onChangeOption(d.id)}
      className={classnames('div-option', d.selected && !this.props.isViewExternalProduct ? 'active' : '')}
      id={d.selected && !this.props.isViewExternalProduct ? 'id-active' : ''}
    >
      <img src={d.image} alt="img" />
      <span>
        {d.name}
      </span>
    </div>
  )

  render() {
    const {
      isExtendFooter, arrayThumbs, isDayCustom, dataCustomDay, dataCustomNight,
      isShowCustom, redeemCode, isShowDescription, isChillRelax, isSeducation, isSpecialOccasion, isWorkSocial,
    } = this.state;
    const {
      dataDay, dataNight, cmsCommon, promoPerfume, personalityChoice, onBack,
      headerText, onClickAddToCart, addToCartBoth, onClickIngredient, onClickImage, namePath,
      onClickRetake, textBlock, onSwitchIndex, dataPersionality, buttonBlocks,
      occasion, scentsDisliked, scentsLiked, isLandscape, mixes, isAccountOutcome,
      basket, externalProduct, isViewExternalProduct, isLimitMixes,
    } = this.props;
    const totalPerfumeExlixir = _.filter(basket?.items || [], x => (x.item.product.type.name === 'Perfume' || x.item.product.type.name === 'Elixir') && !x.item.is_sample);
    const dayNightPerfumebt = getNameFromCommon(cmsCommon, 'Day_Night_Perfumes');
    const addComboBt = getNameFromCommon(cmsCommon, 'add_combo_to_cart');
    const backBt = getNameFromCommon(cmsCommon, 'BACK');
    const retakeBt = getNameFromCommon(cmsCommon, 'RETAKE_QUIZ');
    const chillBt = getNameFromCommon(cmsCommon, 'CHILL & RELAX');
    const seductionBt = getNameFromCommon(cmsCommon, 'seduction');
    const specialBt = getNameFromCommon(cmsCommon, 'SPECIAL OCCASION');
    const workBt = getNameFromCommon(cmsCommon, 'WORK & SOCIAL');

    const elegantBt = getNameFromCommon(cmsCommon, 'Elegant & Confident');
    const attractiveBt = getNameFromCommon(cmsCommon, 'Attractive & Sexy');
    const standoutBt = getNameFromCommon(cmsCommon, 'Stand out & Trendy');
    const freshBt = getNameFromCommon(cmsCommon, 'Fresh & Conforting');

    const browserPerfumeBt = getNameFromCommon(cmsCommon, 'BROWSE_PERFUME');

    const moreBt = getNameFromButtonBlock(buttonBlocks, 'more_personality_info');
    const lifeBt = getNameFromButtonBlock(buttonBlocks, 'Life_is_an');
    const youAreBt = getNameFromButtonBlock(buttonBlocks, 'You_are');
    const redoBt = getNameFromButtonBlock(buttonBlocks, 'redo_questionaire');
    const basedOnBt = getNameFromButtonBlock(buttonBlocks, 'Based on perfume by');
    const titleQuiz = isViewExternalProduct ? basedOnBt : isChillRelax ? chillBt : isSeducation ? seductionBt : isSpecialOccasion ? specialBt : isWorkSocial ? workBt : '';
    const subTitleQuiz = isViewExternalProduct ? externalProduct?.name : isChillRelax ? freshBt : isSeducation ? attractiveBt : isSpecialOccasion ? standoutBt : isWorkSocial ? elegantBt : '';

    const dataCustom = isDayCustom ? dataCustomDay : dataCustomNight;
    const itemBottle = _.find(isDayCustom ? dataDay.data.combo : dataNight.data.combo, x => x.product.type === 'Bottle');
    const scents = _.filter(isDayCustom ? dataDay.data.combo : dataNight.data.combo, x => x.product.type !== 'Bottle');
    // this.dataMix = occasion === 'day' ? dataDay : dataNight;
    if (externalProduct) {
      _.assign(externalProduct, { data: externalProduct.product });
    }

    let dataOption;
    // new version 4. have 2 option Seducation, WorkSocial
    if (isLimitMixes) {
      dataOption = [
        {
          image: ic120, name: workBt, selected: isWorkSocial, id: 4,
        },
        {
          image: ic121, name: seductionBt, selected: isSeducation, id: 2,
        },
      ];
    } else {
      //
      dataOption = [
        {
          image: ic123, name: chillBt, selected: isChillRelax, id: 1,
        },
        {
          image: ic121, name: seductionBt, selected: isSeducation, id: 2,
        },
      ];
      if (!this.props.isDisableSwitchScentNight) {
        dataOption.push(
          {
            image: ic122, name: specialBt, selected: isSpecialOccasion, id: 3,
          },
        );
      }
      if (!this.props.isDisableSwitchScentDay) {
        dataOption.push(
          {
            image: ic120, name: workBt, selected: isWorkSocial, id: 4,
          },
        );
      }
    }

    const externalProductHtml = (
      externalProduct ? (
        <div className={classnames('div-extend-product', isViewExternalProduct ? 'active' : '')} onClick={() => this.props.onChangeExternalProduct()}>
          <div className="div-text">
            <h1>
              {basedOnBt}
            </h1>
            <span>
              {externalProduct?.name}
            </span>
          </div>
          <div className="div-image">
            <img src={externalProduct?.image} alt="perfume" />
          </div>
        </div>
      ) : (null)
    );
    const externalProductHtmlMobile = (
      externalProduct ? (
        <div className={classnames('div-extend-product', isViewExternalProduct ? 'active' : '')} onClick={() => this.props.onChangeExternalProduct()}>
          <div className="div-image">
            <img src={externalProduct?.image} alt="perfume" />
          </div>
          <div className="div-text">
            <h1>
              {basedOnBt}
            </h1>
            <span>
              {externalProduct?.name}
            </span>
          </div>

        </div>
      ) : (null)
    );
    return (
      <div>
        {
          isShowCustom
            ? (
              <CustomeBottleV3
                cmsCommon={cmsCommon}
                arrayThumbs={arrayThumbs}
                handleCropImage={this.handleCropImage}
                currentImg={dataCustom.currentImg}
                nameBottle={dataCustom.nameBottle}
                font={dataCustom.font}
                color={dataCustom.color}
                closePopUp={() => this.setState({ isShowCustom: false })}
                onSave={this.onSaveCustomer}
                itemBottle={itemBottle}
                cmsTextBlocks={textBlock}
                name1={scents && scents.length > 0 ? scents[0].name : ''}
                name2={scents && scents.length > 1 ? scents[1].name : ''}
              />
            ) : (<div />)
        }

        <div id="myDIV" ref={this.paneDidMount} className={`div-result-scent ${(isMobile && !isLandscape) || isMobileOnly ? '' : 'animated fadeInRight'} ${['krisshop', 'sofitel'].includes(namePath) ? 'full-height' : ''}`}>
          <div className="div-result-content">
            <button className={this.props.namePath === 'quiz' && !isAccountOutcome ? 'bt-back-result' : 'hidden'} type="button" onClick={onClickRetake}>
              {/* <img src={icBack} alt="icBack" />
              {backBt} */}
              <img src={icRetake} alt="icRetake" />
              {retakeBt}
            </button>
            {/* <button className={this.props.namePath === 'quiz' ? 'bt-retake-quiz-result' : 'hidden'} type="button" onClick={onClickRetake}>
              <img src={icRetake} alt="icRetake" />
              {retakeBt}
            </button> */}
            <div className="div-header-text">
              <div
                className="text"
                onMouseEnter={isBrowser && !isAccountOutcome ? () => this.setState({ isShowDescription: true }) : null}
                onMouseLeave={isBrowser && !isAccountOutcome ? () => this.setState({ isShowDescription: false }) : null}
              >
                <img src={dataPersionality.image} alt="icon" />
                <div className="div-text-name">
                  <div className={`des-text-tx ${isShowDescription ? 'show' : ''}`} style={{ color: dataPersionality.color }}>
                    {personalityChoice ? personalityChoice.personality.description : ''}
                  </div>
                  <span className={`title-tx ${!isShowDescription ? 'show' : ''}`} style={{ color: dataPersionality.color }}>
                    {personalityChoice ? `${personalityChoice.personality.title}` : ''}
                  </span>
                </div>
              </div>
              {
                (isMobile && !isLandscape) || isMobileOnly || isAccountOutcome ? (
                  <span className="des-text" style={{ color: dataPersionality.color }}>
                    {personalityChoice ? personalityChoice.personality.description : ''}
                  </span>
                ) : (null)
              }

              <button
                data-gtmtracking="funnel-1-step-15-more-personality-info"
                type="button"
                onClick={this.props.showMoreInfo}
              >
                <img
                  data-gtmtracking="funnel-1-step-15-more-personality-info"
                  src={icSearch}
                  alt="search"
                />
                {moreBt}
              </button>
            </div>
            {/* <h1>
              {headerText}
            </h1> */}
            <div id="list-option" className={['boutique-quiz', 'quiz'].includes(namePath) ? 'list-option' : 'hidden'}>
              {
                _.map(dataOption, d => (
                  this.renderOption(d)
                ))
              }
            </div>
            <Row className="row-content" style={namePath !== 'quiz' && isMobile ? { marginBottom: '20px' } : {}}>
              {
                namePath === 'boutique-quiz' || namePath === 'partnership-quiz' || ['sofitel', 'sephora', 'bmw', 'lazada'].includes(namePath) ? (
                  <Col sm="12" md="12" className="div-col-result-boutique">
                    <ItemResultScentLike
                      ref={this.refItemResultScentLike}
                      isSmall={isExtendFooter}
                      dataDay={dataDay}
                      dataNight={dataNight}
                      cmsCommon={cmsCommon}
                      onClickAddToCart={onClickAddToCart}
                      onClickIngredient={onClickIngredient}
                      onClickImage={() => {}}
                      onClickCustomBottle={() => this.onClickCustomBottle(true)}
                      isHiddenAddCart={namePath === 'boutique-quiz' || namePath === 'partnership-quiz' || ['sofitel', 'sephora', 'bmw', 'lazada'].includes(namePath)}
                      // classButton={namePath === 'partnership-quiz' ? 'bt-black' : ''}
                      customeBottle={dataCustomDay}
                      onSwitchIndex={onSwitchIndex}
                      isDisableSwitchScent={this.props.isDisableSwitchScentDay || ['boutique-quiz', 'quiz'].includes(namePath)}
                      isNewInfo={['boutique-quiz', 'quiz'].includes(namePath)}
                      isOnlyPerfume={redeemCode}
                      onClickCheckOutGift={this.props.onClickCheckOutGift}
                      gotoCustomMix={data => this.props.gotoCustomMix(data)}
                      isNightProd={(namePath === 'boutique-quiz' && [3, 4].includes(mixes)) || (namePath !== 'boutique-quiz' && occasion !== 'day')}
                      isChangeScent={[2, 4].includes(mixes)}
                      idOutCome={this.props.idOutCome}
                      scentsDisliked={scentsDisliked}
                      scentsLiked={scentsLiked}
                      buttonBlocks={buttonBlocks}
                      isLandscape={isLandscape}
                      callUpdateScentAll={this.props.callUpdateScentAll}
                      namePath={namePath}
                      dataUpdateProduct={this.props.dataUpdateProduct}
                    />
                  </Col>
                ) : namePath === 'krisshop' ? (
                  <Col sm="12" md="12" className="div-col-result-boutique">
                    <ItemResultScentKrisshop
                      isSmall={isExtendFooter}
                      data={occasion !== 'day' ? dataNight : dataDay}
                      cmsCommon={cmsCommon}
                      onClickIngredient={onClickIngredient}
                      onClickImage={() => {}}
                      onClickCustomBottle={() => {}}
                      classButton="bt-black"
                      customeBottle={dataCustomDay}
                      isNightProd={occasion !== 'day'}
                      onSwitchIndex={onSwitchIndex}
                      isDisableSwitchScent={this.props.isDisableSwitchScentDay}
                      isOnlyPerfume={redeemCode}
                      onClickCheckOutGift={this.props.onClickCheckOutGift}
                      isLandscape={isLandscape}
                      buttonBlocks={buttonBlocks}
                    />
                  </Col>
                ) : (
                  <React.Fragment>
                    <Col sm="12" md="12" className="div-col-result">
                      <ItemResultScent
                        // isSmall={isExtendFooter}
                        ref={this.refItemResultScentLike}
                        data={isViewExternalProduct ? externalProduct : (isChillRelax || isWorkSocial) ? dataDay : dataNight}
                        cmsCommon={cmsCommon}
                        onClickAddToCart={onClickAddToCart}
                        onClickIngredient={onClickIngredient}
                        onClickImage={(isNightProd, isRollOn) => onClickImage(isNightProd, isRollOn, dataCustom)}
                        onClickCustomBottle={() => this.onClickCustomBottle(true)}
                        isHiddenAddCart={namePath === 'boutique-quiz' || namePath === 'partnership-quiz' || ['sofitel', 'sephora', 'bmw', 'lazada'].includes(namePath)}
                        classButton={namePath === 'partnership-quiz' ? 'bt-black' : ''}
                        customeBottle={dataCustomDay}
                        onSwitchIndex={onSwitchIndex}
                        isDisableSwitchScent={this.props.isDisableSwitchScentDay}
                        isOnlyPerfume={redeemCode}
                        onClickCheckOutGift={this.props.onClickCheckOutGift}
                        isLandscape={isLandscape}
                        isResultQuiz
                        // isDisableButtonSwitchCustom
                        isDisableButtonRightSwitchCustom
                        titleQuiz={titleQuiz}
                        subTitleQuiz={subTitleQuiz}
                        isNightProd={isSeducation || isSpecialOccasion}
                        isNewInfo
                      />
                    </Col>
                  </React.Fragment>
                )
              }
              {isBrowser && externalProductHtml}
              {isMobile && externalProductHtmlMobile}
              {
                !isAccountOutcome && (
                  <button className="bt-retake-quiz-result-mobile" type="button" onClick={onClickRetake}>
                    <img src={icRetake} alt="icRetake" />
                    {retakeBt}
                  </button>
                )
              }

            </Row>
            <BottomScrollListener onBottom={this.onBottomDetect} />
          </div>
          {
            redeemCode ? (
              <div />
            ) : namePath !== 'quiz' ? (
              <button className={(isBrowser || (isTablet && isLandscape)) ? 'bt-retake-boutique' : 'hidden'} type="button" onClick={onClickRetake}>
                <img src={icRetake} alt="icon" />
                {redoBt}
              </button>
            ) : (null)
          }
        </div>
      </div>
    );
  }
}

ResultScentV2.defaultProps = {
  isAccountOutcome: false,
  externalProduct: undefined,
  isViewExternalProduct: false,
};

ResultScentV2.propTypes = {
  cmsCommon: PropTypes.shape().isRequired,
  dataDay: PropTypes.shape().isRequired,
  dataNight: PropTypes.shape().isRequired,
  onClickAddToCart: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  promoPerfume: PropTypes.shape().isRequired,
  personalityChoice: PropTypes.shape().isRequired,
  onBack: PropTypes.func.isRequired,
  headerText: PropTypes.string.isRequired,
  addToCartBoth: PropTypes.func.isRequired,
  onClickImage: PropTypes.func.isRequired,
  namePath: PropTypes.string.isRequired,
  onSwitchIndex: PropTypes.func.isRequired,
  isDisableSwitchScentDay: PropTypes.bool.isRequired,
  isDisableSwitchScentNight: PropTypes.bool.isRequired,
  isAccountOutcome: PropTypes.bool,
  basket: PropTypes.shape().isRequired,
  externalProduct: PropTypes.shape(),
  isViewExternalProduct: PropTypes.bool,
};

export default ResultScentV2;
