import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import '../../styles/image-scent-mix.scss';
import bottleResult from '../../image/bottle-new.png';

function ItemImageScentMix(props) {
  const { listScent } = props.data;
  console.log('listScent', listScent);
  const newList = _.filter(listScent, x => !_.isEmpty(x));
  return (
    <div className="div-image-scent-mix">
      <img src={bottleResult} alt="bottle" />
      <div className={classnames('list-scent', (newList.length === 4 ? 'scents-4' : newList.length === 5 ? 'scents-5' : ''))}>
        {
          newList.length === 5 ? (
            <React.Fragment>
              <div className="line-scent">
                {
                  _.map(newList.slice(0, 3), d => (
                    <img src={d.image} alt="scent" />
                  ))
                }
              </div>
              <div className="line-scent line-2">
                {
                  _.map(newList.slice(3, 5), d => (
                    <img src={d.image} alt="scent" />
                  ))
                }
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              {
                _.map(newList, d => (
                  <img src={d.image} alt="scent" />
                ))
              }
            </React.Fragment>
          )
        }


      </div>
    </div>
  );
}

export default ItemImageScentMix;
