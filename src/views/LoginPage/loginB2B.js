import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import logo from '../../image/logo_21.png';

import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import { LOGIN_URL, GET_BASKET_URL, CREATE_BASKET_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import loadingPage from '../../Redux/Actions/loading';
import getAllRequest from '../../Redux/Actions/getall';
import { loginCompleted } from '../../Redux/Actions/login';
import { loadAllBasket } from '../../Redux/Actions/basket';
import BackGroundVideo from '../../components/backgroundVideo';
import bg from '../../image/bg-loginb2b.jpg';
import { generateUrlWeb, gotoShopHome } from '../../Redux/Helpers';

class LoginB2B extends Component {
  constructor(props) {
    super(props);
    this.data = {
      email: '',
      password: '',
    };
  }

  componentDidMount = () => {
    this.props.loadingPage(false);
  }

  onChange = (e) => {
    const { value, name } = e.target;
    console.log('name, value', name, value);
    this.data[name] = value;
  }

  getBasket = (id) => {
    const options = {
      url: GET_BASKET_URL.replace('{cartPk}', id),
      method: 'GET',
    };
    return fetchClient(options, true);
  }

  handleBack = () => {
    // this.props.history.push(generateUrlWeb('/'));
    gotoShopHome();
  }

  createBasket = (user) => {
    const { id } = user;
    const options = {
      url: CREATE_BASKET_URL.replace('{userId}', id),
      method: 'POST',
      body: {},
    };
    return fetchClient(options, true);
  }

  handleLoginSuccessful = (basket) => {
    toastrSuccess('You are login successfully');
    this.props.loadAllBasket(basket);
    this.props.history.push(generateUrlWeb('/b2b/landing'));
    this.props.loadingPage(false);
  }

  loginApi = (email, password) => {
    this.props.loadingPage(true);
    const body = {
      email,
      password,
    };
    const options = {
      url: LOGIN_URL,
      method: 'POST',
      body,
    };
    fetchClient(options).then((result) => {
      this.props.loadingPage(false);
      if (!result.isError && !result.message) {
        if (result.user.is_b2b) {
          this.props.loginCompleted(result);
          const { user } = result;
          if (user.cart) {
            this.getBasket(user.cart).then((basket) => {
              this.handleLoginSuccessful(basket);
            });
            return;
          }
          this.createBasket(user).then((basket) => {
            this.handleLoginSuccessful(basket);
          });
        } else {
          toastrError('You are not a partner');
          this.props.loadingPage(false);
        }
        return;
      }
      throw new Error(result.message);
    }).catch((e) => {
      toastrError(e.message);
      this.props.loadingPage(false);
    });
  }

  submit = (e) => {
    e.preventDefault();
    const { email, password } = this.data;
    if (!email || !password) {
      toastrError('Please fill all the information!');
      return;
    }
    this.loginApi(email, password);
  }

  render() {
    return (
      <div className="login">
        <BackGroundVideo isVideo={false} urlImage={bg} />
        <div className="content">
          <img src={logo} alt="logo" />
          <form className="login-form">
            <span className="mt-4 login-span">
              Email
            </span>
            <input
              type="email"
              className="mt-2 login-input"
              name="email"
              onChange={this.onChange}
            />
            <span className="mt-2 login-span">
              Password
            </span>
            <input
              type="password"
              className="mt-2 login-input"
              name="password"
              onChange={this.onChange}
            />
            <button
              type="submit"
              className="mt-4 login-submit"
              onClick={this.submit}
            >
              Submit
            </button>
          </form>
          <span className="mt-2">
            Become a partner?
          </span>
          <Link
            to="#"
          >
            Contact Us
          </Link>
        </div>
        <div className="login-back">
          <button
            type="button"
            onClick={this.handleBack}
          >
            <div className="button-context">
              <i className="fa fa-caret-left" />
              Back to the website
            </div>
          </button>
        </div>
      </div>
    );
  }
}

LoginB2B.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  loadingPage: PropTypes.func.isRequired,
  loginCompleted: PropTypes.func.isRequired,
  getAllRequest: PropTypes.func.isRequired,
  loadAllBasket: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  loadingPage,
  loginCompleted,
  getAllRequest,
  loadAllBasket,
};

export default withRouter(connect(
  null,
  mapDispatchToProps,
)(LoginB2B));
