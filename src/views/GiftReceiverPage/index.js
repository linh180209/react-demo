import React, { Component } from 'react';
import '../../styles/style.scss';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import MetaTags from 'react-meta-tags';
import queryString from 'query-string';

import '../../styles/gift-receiver.scss';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import {
  fetchCMSHomepage, getSEOFromCms, getCmsCommon, getNameFromCommon, getNameFromButtonBlock, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady, getSearchPathName,
} from '../../Redux/Helpers';
import AboutItem from '../AboutUs/AboutItem/index';
import BlockIconHeaderV2 from '../../components/HomeAlt/BlockIconHeaderV2';
import BlockIcon from '../../components/HomeAlt/BlockIcon';
import BottleCustom from '../../components/B2B/CardItem/bottleCustom';
import { CHECK_PROMOTION_CODE } from '../../config';
import { toastrError } from '../../Redux/Helpers/notification';
import fetchClient from '../../Redux/Helpers/fetch-client';
import auth from '../../Redux/Helpers/auth';
import FooterV2 from '../../views2/footer';

const getCMS = (data) => {
  if (data) {
    const { body, image_background: imageBg } = data;
    const seo = getSEOFromCms(data);
    const imageTxtBlock = _.filter(body, x => x.type === 'imagetxt_block').map(e => e.value);
    const benefitsBlock = _.find(data.body, x => x.type === 'benefits_block');
    const iconBlock = _.find(data.body, x => x.type === 'icons_block');
    const imgBlock = _.find(data.body, x => x.type === 'image_block').value;
    const buttonBlocks = _.filter(data.body, x => x.type === 'button_block');
    return {
      seo, imageTxtBlock, benefitsBlock, iconBlock, imageBg, imgBlock, buttonBlocks,
    };
  }
  return {
    seo: undefined,
    imageTxtBlock: [],
    benefitsBlock: undefined,
    iconBlock: undefined,
    imageBg: '',
    imgBlock: '',
    buttonBlocks: [],
  };
};

// http://localhost:3003/gift/receiver/?codes=JVEALLME&is_customized=1&name=gift%20test&is_black=0&image=https://s.scentdesigner.stage.23c.se/media/images/custom/product_2SIMf3V.jpg
class GiftReceiverPage extends Component {
  constructor(props) {
    super(props);
    const infoGift = this.props.location?.search ? queryString.parse(this.props.location.search) : undefined;
    this.state = {
      seo: undefined,
      imageTxtBlock: [],
      benefitsBlock: undefined,
      iconBlock: undefined,
      imageBg: '',
      infoGift: infoGift || {},
      redeemCode: infoGift ? infoGift.codes : '',
      imgBlock: '',
      buttonBlocks: [],
      dataGift: {},
    };
  }

  componentDidMount() {
    setPrerenderReady();
    this.fetchCMS();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchCMS = async () => {
    const { cms } = this.props;
    const cmsBlock = _.find(cms, x => x.title === 'Gift Receiver');
    if (!cmsBlock) {
      const cmsData = await fetchCMSHomepage('gift-receiver');
      this.props.addCmsRedux(cmsData);
      const dataCms = getCMS(cmsData);
      this.setState(dataCms);
    } else {
      const dataCms = getCMS(cmsBlock);
      this.setState(dataCms);
    }
  }

  onChangeCode = (e) => {
    const { value } = e.target;
    this.setState({ redeemCode: value });
  }

  fetchData = () => {
    const { redeemCode } = this.state;
    if (!redeemCode) {
      toastrError('Please enter your code');
      return;
    }
    const option = {
      url: `${CHECK_PROMOTION_CODE}?code=${redeemCode}`,
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        if (!result.is_gift_generated) {
          throw new Error('Your code invalid');
        }
        auth.setDiscountCode(redeemCode);
        this.props.loadingPage(false);
        this.setState({ dataGift: result });
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      this.props.loadingPage(false);
      toastrError(err.message);
    });
  }

  gotoFunnel = () => {
    const { redeemCode, dataGift } = this.state;
    const { meta } = dataGift;
    const { line_item: lineItem, sender, receiver } = meta || {};
    const {
      is_customized: isCustomized, name, is_black: isBlack, image, is_display_name: isDisplayName,
      color, font,
    } = lineItem || {};
    const { pathname, search } = getSearchPathName(generateUrlWeb(`/funnel/?codes=${redeemCode}`));
    this.props.history.push({
      pathname,
      search,
      state: {
        infoGift: {
          codes: redeemCode,
          image,
          name,
          isBlack,
          color,
          font,
          sender,
          isDisplayName,
          isCustomized,
        },
      },
    });
  }

  render() {
    const cmsCommon = getCmsCommon(this.props.cms);
    const eauBt = getNameFromCommon(cmsCommon, 'Eau_de_parfum');
    const mlBt = getNameFromCommon(cmsCommon, '25ml');

    const {
      seo, imageTxtBlock, benefitsBlock, iconBlock, imageBg, infoGift,
      sRedeem, imgBlock, buttonBlocks, redeemCode, dataGift,
    } = this.state;
    const { meta } = dataGift || {};
    const { line_item: lineItem, sender, receiver } = meta || {};
    const {
      is_customized: isCustomized, name, is_black: isBlack, image, is_display_name: isDisplayName,
    } = lineItem || {};
    const redeemBt = getNameFromButtonBlock(buttonBlocks, 'redeem_a_maison_21g_gift');
    const enterCodeBt = getNameFromButtonBlock(buttonBlocks, 'Enter_your_code_to_redeem_your_gift');
    const redeenNowBt = getNameFromButtonBlock(buttonBlocks, 'REDEEM_NOW');
    const congurationBt = getNameFromButtonBlock(buttonBlocks, 'CONGRATULATIONS');
    const youHaveBt = getNameFromButtonBlock(buttonBlocks, 'You_have_been_gifted_a');
    const fromBt = getNameFromButtonBlock(buttonBlocks, 'From');
    const messageBt = getNameFromButtonBlock(buttonBlocks, 'Message');
    const startYourCreateBt = getNameFromButtonBlock(buttonBlocks, 'START_YOUR_CREATION');
    const perfumeCreateBt = getNameFromButtonBlock(buttonBlocks, 'Perfume_Creation');

    return (
      <div>
        <MetaTags>
          <title>
            {seo?.seoTitle}
          </title>
          <meta name="description" content={seo?.seoDescription} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/gift/receiver')}
        </MetaTags>
        <HeaderHomePageV3 />
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}

        <div className="div-gift-receiver" style={{ backgroundImage: `url(${imageBg})` }}>
          <h1>
            {redeemBt}
          </h1>
          <div className="div-card-body">
            <div className="div-image">
              {
                !_.isEmpty(dataGift)
                  ? (
                    <BottleCustom
                      classWrap="custome-gift-receiver"
                      isImageText={isCustomized}
                      onGotoProduct={() => {}}
                      image={image}
                      isBlack={isBlack}
                      eauBt={eauBt}
                      mlBt={mlBt}
                      isDisplayName={isDisplayName}
                      name={name}
                      combos={[]}
                    />
                  ) : (
                    <img loading="lazy" src={imgBlock.image} alt="product" />
                  )
              }
            </div>
            {
              _.isEmpty(dataGift) ? (
                <div className="div-enter-redeem">
                  <span>
                    {enterCodeBt}
                    :
                  </span>
                  <input type="text" value={redeemCode} onChange={this.onChangeCode} />
                  <button className="button-bg" type="button" onClick={this.fetchData}>
                    {redeenNowBt}
                  </button>
                </div>
              ) : (
                <div className="div-redeem">
                  <h3>
                    {congurationBt}
                    !
                  </h3>
                  <div className="div-info have-message">
                    <span>
                      {youHaveBt}
                      {' '}
                      <b>{perfumeCreateBt}</b>
                    </span>
                    <span className="mt-2">
                      {fromBt}
                      :
                      {' '}
                      <b>{sender ? sender.name : ''}</b>
                      {' '}
                      -
                      {' '}
                      {sender ? sender.email : ''}
                    </span>
                  </div>
                  <div className={receiver && receiver.note ? 'message' : 'hidden'}>
                    <span>
                      {messageBt}
                      :
                    </span>
                    <span className="mt-3">
                      {receiver ? receiver.note : ''}
                    </span>
                  </div>
                  <button className="button-bg" type="button" onClick={this.gotoFunnel}>
                    {startYourCreateBt}
                  </button>
                </div>
              )
            }
          </div>
        </div>
        <AboutItem items={imageTxtBlock} />
        <BlockIconHeaderV2 heroCms={benefitsBlock} />
        <BlockIcon heroCms={iconBlock} />
        <FooterV2 />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  loadingPage,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(GiftReceiverPage));
