import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';

import { VERIFICATION_EMAIL_URL, GET_BASKET_URL, ASSIGN_BASKET_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import { loginCompleted } from '../../Redux/Actions/login';
import { loadAllBasket } from '../../Redux/Actions/basket';
import auth from '../../Redux/Helpers/auth';
import {
  generateUrlWeb, gotoShopHome, segmentSignUp, segmentVerifycation,
} from '../../Redux/Helpers';

class VerificationPage extends Component {
  componentWillMount() {
    const { match } = this.props;
    const { token } = match.params;
    this.sendVerification(token);
  }

  getBasket = (id) => {
    const options = {
      url: GET_BASKET_URL.replace('{cartPk}', id),
      method: 'GET',
    };
    return fetchClient(options, false);
  }

  assignBasketId = (cart, basket) => {
    const items = [];
    _.forEach(basket.items, (x) => {
      items.push(x.id);
    });
    const options = {
      url: ASSIGN_BASKET_URL.replace('{id}', cart),
      method: 'POST',
      body: {
        items,
      },
    };
    return fetchClient(options, true);
  }

  assignBasket = async (user) => {
    const basketId = auth.getBasketId();
    if (!_.isEmpty(basketId)) {
      try {
        const basket = await this.getBasket(basketId);
        if (basket && basket.items.length > 0) {
          const newBasket = await this.assignBasketId(user.cart, basket);
          this.props.loadAllBasket(newBasket);
        }
      } catch (error) {
        console.log('error', error);
      }
    }
  }

  sendVerification = (token) => {
    const options = {
      url: VERIFICATION_EMAIL_URL,
      method: 'POST',
      body: {
        verify_token: token,
      },
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        this.props.loginCompleted(result);
        this.assignBasket(result.user);
        segmentVerifycation(result.user);
        segmentSignUp(result.user);
        toastrSuccess('E-mail confirmation successful');
        // this.props.history.push(generateUrlWeb('/'));
        gotoShopHome();
      } else {
        toastrError('Verification Email');
      }
    }).catch(() => {
      toastrError('Verification Email');
    });
  }

  render() {
    return (
      <div />
    );
  }
}

VerificationPage.propTypes = {
  loginCompleted: PropTypes.func.isRequired,
  loadAllBasket: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      token: PropTypes.string,
    }),
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};
const mapDispatchToProps = {
  loginCompleted,
  loadAllBasket,
};
export default withRouter(connect(null, mapDispatchToProps)(VerificationPage));
