import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { getAltImage } from '../../Redux/Helpers';

class RecommendItem extends Component {
  render() {
    const { data, onClickAddSuggest } = this.props;
    const { image } = data;
    return (
      <div
        data-gtmtracking={this.props.dataGtmtracking}
        className="div-recommend-item"
        onClick={() => onClickAddSuggest(data.id)}
      >
        <div className="div-image" data-gtmtracking={this.props.dataGtmtracking}>
          <img loading="lazy" src={image} alt={getAltImage(image)} data-gtmtracking={this.props.dataGtmtracking} />
        </div>
        <div className="div-text" data-gtmtracking={this.props.dataGtmtracking}>
          <h3 data-gtmtracking={this.props.dataGtmtracking}>
            {data.name}
          </h3>
          <span data-gtmtracking={this.props.dataGtmtracking}>{data.family}</span>
        </div>
      </div>
    );
  }
}

RecommendItem.propTypes = {
  data: PropTypes.shape().isRequired,
  gender: PropTypes.string.isRequired,
  onClickAddSuggest: PropTypes.func.isRequired,
};

export default RecommendItem;
