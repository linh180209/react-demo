/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import {
  Player, ControlBar, Shortcut, BigPlayButton,
} from 'video-react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  isSafari, isEdge,
} from 'react-device-detect';
import MetaTags from 'react-meta-tags';


import ScentItem from '../../components/ScentItem';
import refresh from '../../image/icon/refresh.svg';
import heart from '../../image/heart.svg';
import HeaderHomePage from '../../components/HomePage/header';
import SelectionScent from '../../components/MMO/selectionScent';
import { GET_ALL_PRODUCTS_SCENT, CREATE_PRODUCT_URL } from '../../config';
import fetchClient, { fetchClientFormData } from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import loadingPage from '../../Redux/Actions/loading';
import {
  scrollTop, getSEOFromCms, getNameFromCommon, getCmsCommon, fetchCMSHomepage, googleAnalitycs, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers';
import ScentMmoMobile from '../../components/ScentItem/scentMmoMobile';
import addCmsRedux from '../../Redux/Actions/cms';
import updateMmosData from '../../Redux/Actions/mmos';
import RecommendMmo from './recommendMmo';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import { isMobile, isTablet } from '../../DetectScreen';
import FooterV2 from '../../views2/footer';

const getListTimeStamp = (datas) => {
  console.log('datas', datas);
  const list = [0];
  _.forEach(datas, (d) => {
    list.push(d.value.timestamp);
  });
  return list;
};

const prePareCms = (mmoCms) => {
  if (mmoCms) {
    const seo = getSEOFromCms(mmoCms);
    const { body } = mmoCms;
    const videoBlocks = _.filter(body, x => x.type === 'video_block');
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    const headerBlocks = _.find(body, x => x.type === 'header_block');
    const subTextBlocks = _.filter(body, x => x.type === 'text_block');
    const valueFindVideo = isMobile ? 'phone' : (isTablet ? 'tablet' : 'web');
    const videoBlock = _.find(videoBlocks, x => x.value.type === valueFindVideo);

    const stepBlock = _.filter(body, x => x.type === 'timestamp_block');
    const listTimeStamp = stepBlock ? getListTimeStamp(stepBlock) : [];
    const urlVideo = videoBlock ? videoBlock.value.video : undefined;
    const placeholder = videoBlock ? videoBlock.value.placeholder : undefined;
    const headerCms = headerBlocks && headerBlocks.value ? headerBlocks.value.header_text : '';
    const buttonCms = _.map(buttonBlocks, d => d.value);
    return {
      listTimeStamp, urlVideo, placeholder, seo, headerCms, buttonCms, subTextCms: subTextBlocks,
    };
  }
  return {
    listTimeStamp: [], urlVideo: '', seo: undefined, headerCms: '', subTextCms: [], buttonCms: [],
  };
};

class MMO extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowSelect: false,
      data1: undefined,
      data2: undefined,
      isCreated: false,
      isRandom: false,
      products: [],
      listTimeStamp: [],
      seo: undefined,
      urlVideo: '',
      headerCms: '',
      subTextCms: '',
      buttonCms: [],
      isFetchData: false,
      isShowIngredient: false,
      ingredientDetail: undefined,
    };
    this.listDataLeft = [];
    this.listDataRight = [];
    this.getTime = undefined;
    this.step = 0;
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/mmo');
    this.fetchDataInit();
    this.getTime = setInterval(this.handleVideo, 500);
    // this.refs.player.play();
    // setTimeout(() => {
    //   this.refs.player.pause();
    // }, 500);
  }

  componentDidUpdate(prevProps, prevState) {
    const { isFetchData } = this.state;
    if (isFetchData) {
      this.setState({ isFetchData: false });
      this.fetchDataInit();
    }
    if (prevState.isRandom !== this.state.isRandom && this.state.isRandom) {
      const symbolContainerLeft = document.getElementById('random-left');
      const symbolContainerRight = document.getElementById('random-right');
      if (symbolContainerLeft && !isSafari) {
        const animationLeft = symbolContainerLeft.animate(
          [
            { transform: 'none', filter: 'blur(0)' },
            { filter: 'blur(2px)', offset: 0.5 },
            {
              transform: `translateY(-${80.8}%)`,
              filter: 'blur(0)',
            },
          ],
          {
            duration: 1 * 1000,
            easing: 'ease-in-out',
          },
        );
        const animationPromise = new Promise(
          resolve => (animationLeft.onfinish = resolve),
        );
        animationLeft.play();
        animationPromise.then(() => {
          const data1 = _.cloneDeep(this.listDataLeft[this.listDataLeft.length - 1]);
          this.setState({ isRandom: false, data1 });
          this.setStep(true, true);
        });
      }
      if (symbolContainerRight && !isSafari) {
        const animationRight = symbolContainerRight.animate(
          [
            { transform: 'none', filter: 'blur(0)' },
            { filter: 'blur(2px)', offset: 0.5 },
            {
              transform: `translateY(${80.8}%)`,
              filter: 'blur(0)',
            },
          ],
          {
            duration: 1 * 1000,
            easing: 'ease-in-out',
          },
        );
        const animationPromise = new Promise(
          resolve => (animationRight.onfinish = resolve),
        );
        animationRight.play();
        animationPromise.then(() => {
          const data2 = _.cloneDeep(this.listDataRight[0]);
          this.setState({ isRandom: false, data2 });
        });
      }
    }
    if (prevState.login !== this.state.login && this.state.results) {
      const products = this.createData(this.state.results);
      this.setState({ products });
    }
  }

  componentWillUnmount() {
    removeLinkHreflang();
    if (this.getTime) {
      clearInterval(this.getTime);
    }
  }

  onOpenSelectionData1 = () => {
    this.selection = 'data1';
    this.setState({ isShowSelect: true });
  }

  onOpenSelectionData2 = () => {
    this.selection = 'data2';
    this.setState({ isShowSelect: true });
  }

  onCloseSelection = () => {
    this.setState({ isShowSelect: false });
  }

  onClickAddMix = (d) => {
    const cmsCommon = getCmsCommon(this.props.cms);
    const removeBt = getNameFromCommon(cmsCommon, 'Remove');

    const data = _.assign({}, d);
    data.buttonName = `- ${removeBt ? removeBt.toUpperCase() : ''}`;
    data.titleTop = undefined;
    if (this.selection === 'data1') {
      this.setState({ isShowSelect: false, data1: data });
      this.setStep(true, this.state.data2);
    } else {
      this.setState({ isShowSelect: false, data2: data });
      this.setStep(this.state.data1, true);
    }
  }

  onClickRemoveScentData1 = () => {
    this.setState({ data1: undefined });
    this.setStep(false, this.state.data2);
  }

  onClickRemoveScentData2 = () => {
    this.setState({ data2: undefined });
    this.setStep(this.state.data1, false);
  }

  onCreateMix = () => {
    const { data1, data2 } = this.state;
    const { login } = this.props;
    if (login && login.user) {
      this.createProduct(data1.id, data2.id);
    } else {
      this.props.history.push(generateUrlWeb(`/products/${data1.id}/${data2.id}`));
    }
    this.setState({ isCreated: true });
  }

  onClickRandom = () => {
    this.listDataLeft = this.randomListAnimation(this.state.products);
    this.listDataRight = this.randomListAnimation(this.state.products, this.listDataLeft[0]);
    if (isMobile || isSafari || isEdge) {
      const data1 = _.cloneDeep(this.listDataLeft[0]);
      const data2 = _.cloneDeep(this.listDataRight[this.listDataRight.length - 1]);
      this.refs.player.seek(0);
      this.setStep(true, true);
      this.setState({ data1, data2 });
    } else {
      this.refs.player.seek(0);
      this.setState({ isRandom: true });
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { login, mmos } = nextProps;
    if (login !== prevState.login) {
      _.assign(objectReturn, { login });
    }
    if (mmos && mmos !== prevState.mmos) {
      _.assign(objectReturn, { mmos });
    }
    console.log('objectReturn', objectReturn);
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  setStep = (data1, data2) => {
    if (!data1 && !data2) {
      this.step = 0;
      this.refs.player.seek(0);
    } else if (data1 && !data2 || !data1 && data2) {
      this.step = 1;
      this.refs.player.seek(0);
    } else if (data1 && data2) {
      this.step = 2;
      // this.refs.player.seek(this.state.listTimeStamp[1]);
    }
  }

  createProduct = (id1, id2) => {
    const formData = new FormData();
    formData.append('combo', JSON.stringify([id1, id2]));
    const option = {
      url: CREATE_PRODUCT_URL,
      method: 'POST',
      body: formData,
    };
    fetchClientFormData(option, true).then((result) => {
      if (result) {
        this.props.history.push(generateUrlWeb(`/products/${id1}/${id2}`));
      }
    }).catch((err) => {
      toastrError(err);
    });
  }

  fetchAllScents = () => {
    const options = {
      url: GET_ALL_PRODUCTS_SCENT,
      method: 'GET',
    };
    return fetchClient(options);
  }

  fetchDataInit = async () => {
    const { cms, mmos } = this.props;
    const mmoCms = _.find(cms, x => x.title === 'MMO');
    if (!mmoCms || _.isEmpty(mmos)) {
      this.props.loadingPage(true);
      const pending = [fetchCMSHomepage('mmo'), this.fetchAllScents()];
      try {
        const results = await Promise.all(pending);
        const cmsData = results[0];
        const result = results[1];
        this.props.addCmsRedux(cmsData);
        this.props.updateMmosData(result);
        const {
          listTimeStamp, urlVideo, seo, placeholder, headerCms, subTextCms, buttonCms,
        } = prePareCms(cmsData);
        const products = this.createData(result);
        this.setState({
          products, results: result, listTimeStamp, urlVideo, seo, placeholder, headerCms, subTextCms, buttonCms,
        });
        this.props.loadingPage(false);
      } catch (error) {
        toastrError(error.message);
        this.props.loadingPage(false);
      }
    } else {
      const { mmos: results, cms: cmsPage } = this.props;
      const cmsT = _.find(cmsPage, x => x.title === 'MMO');
      const {
        listTimeStamp, urlVideo, seo, headerCms, subTextCms, buttonCms,
      } = prePareCms(cmsT);
      const products = this.createData(results);
      this.setState({
        products, results: mmos, listTimeStamp, urlVideo, seo, headerCms, subTextCms, buttonCms,
      });
    }
  }

  createData = (results) => {
    const products = [];
    _.forEach(results, (d) => {
      const item = {
        name: d.name,
        image_urls: d ? [d.image] : '',
        description: d.description,
        ingredient: d.ingredient,
        id: d.id,
        content2: d.family,
        header2: 'Family',
        suggestions: d.suggestions,
      };
      products.push(item);
    });
    return _.orderBy(products, 'name', 'asc');
  }

  randomListAnimation = (datas, dExist) => {
    const cmsCommon = getCmsCommon(this.props.cms);
    const removeBt = getNameFromCommon(cmsCommon, 'Remove');

    const results = [];
    _.forEach(Array(5), () => {
      const list = _.filter(datas, d => !results.includes(d));
      let data;
      if (datas.length > 9) {
        while (data && data.id === (dExist ? dExist.id : undefined) || !data) {
          data = list[Math.floor(Math.random() * list.length)];
        }
      } else {
        data = list[Math.floor(Math.random() * list.length)];
      }
      data.titleTop = undefined;
      data.buttonName = `- ${removeBt ? removeBt.toUpperCase() : ''}`;
      results.push(data);
    });
    return results;
  }

  handleVideo = () => {
    if (this.refs && this.refs.player) {
      const { player } = this.refs.player.getState();
      const { currentTime, paused } = player;
      const timeStop = this.state.listTimeStamp[this.step];
      if (currentTime < timeStop && paused) {
        this.refs.player.play();
      } else if (currentTime >= timeStop && !paused) {
        this.refs.player.pause();
      }
    }
  }

  onClickAddSuggest1 = (id) => {
    const { products } = this.state;
    const ele = _.find(products, x => x.id === id);
    if (ele) {
      this.selection = 'data1';
      this.onClickAddMix(ele);
    }
  }

  onClickAddSuggest2 = (id) => {
    const { products } = this.state;
    const ele = _.find(products, x => x.id === id);
    if (ele) {
      this.selection = 'data2';
      this.onClickAddMix(ele);
    }
  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  render() {
    const {
      isShowSelect, data1, data2, isCreated, isRandom,
      products, urlVideo, placeholder, seo, headerCms, buttonCms, subTextCms,
      isShowIngredient, ingredientDetail,
    } = this.state;
    if (!products || products.length === 0) {
      return null;
    }
    const { login } = this.props;
    const gender = login && login.user && login.user.gender ? login.user.gender : 'unisex';
    // this.setStep(data1, data2);
    console.log('placeholder', placeholder, gender);
    const htmlWeb = (
      <div className="div-mmo">
        <div className="div-title">
          <h1 className="title">
            {headerCms}
          </h1>
          <span className="description">
            {subTextCms && subTextCms.length > 0 ? subTextCms[0].value : ''}
          </span>
        </div>
        <div className="content">
          <div className="div-object">
            <div className="div-title-block">
              <h5>
                {subTextCms && subTextCms.length > 1 ? subTextCms[1].value : ''}
              </h5>
              <span>
                {!data1 && data2 ? `${subTextCms && subTextCms.length > 0 ? subTextCms[3].value : ''}` : ''}
              </span>
            </div>
            <div className="mmo-block1 add-boder">
              {
                    isRandom
                      ? (
                        <div
                          style={{
                            height: '385px',
                            overflowY: 'hidden',
                          }}
                        >
                          <div id="random-left">
                            {
                              _.map(this.listDataLeft, d => (
                                <div>
                                  <ScentItem
                                    data={d}
                                    onClickIngredient={this.onClickIngredient}
                                    onClickRightButton={this.onClickRemoveScentData1}
                                    isMMO
                                    cms={this.props.cms}
                                  />
                                  <div style={{ marginBottom: '20px' }} />
                                </div>
                              ))
                            }
                          </div>
                        </div>
                      )
                      : (data1
                        ? (
                          <ScentItem
                            // className="scent-mmo"
                            data={data1}
                            onClickIngredient={this.onClickIngredient}
                            onClickRightButton={this.onClickRemoveScentData1}
                            isMMO
                            cms={this.props.cms}
                          />
                        )
                        : (data2 ? (
                          <RecommendMmo
                            dataGtmtracking="funnel-3-step-03-scent-1-added-to-mix"
                            dataGtmtrackingSelect="funnel-3-step-02-select-scent-1"
                            data={data2.suggestions}
                            openSelection={this.onOpenSelectionData1}
                            onClickAddSuggest={this.onClickAddSuggest1}
                          />
                        ) : (
                          <button
                            type="button"
                            data-gtmtracking="funnel-3-step-02-select-scent-1"
                            onClick={this.onOpenSelectionData1}
                          >
                            <div data-gtmtracking="funnel-3-step-02-select-scent-1">
                              <span data-gtmtracking="funnel-3-step-02-select-scent-1">
                                {buttonCms.length > 0 ? buttonCms[0].text : ''}
                              </span>
                              <i data-gtmtracking="funnel-3-step-02-select-scent-1" className="fa fa-plus" />
                            </div>
                          </button>
                        )))
                  }
            </div>
          </div>

          <div className="div-object">
            <div className="mmo-block2">
              <div className="disable-button video div-col justify-center">
                <Player
                  ref="player"
                  playsInline
                  loop
                  muted
                  autobuffer
                  disablePauseOnClick
                  src={urlVideo}
                  poster={placeholder}
                >
                  <ControlBar disableCompletely disableDefaultControls />
                  <Shortcut dblclickable clickable={false} />
                  <BigPlayButton />
                </Player>
              </div>
              <div className="button-bl1">
                <button
                  data-gtmtracking="funnel-3-step-02-bis-trending-mix"
                  type="button"
                  onClick={this.onClickRandom}
                >
                  <img data-gtmtracking="funnel-3-step-02-bis-trending-mix" src={refresh} alt="refresh" />
                  {buttonCms.length > 1 ? buttonCms[1].text : ''}
                </button>
                <button
                  type="button"
                >
                  <img className="hidden" src={heart} alt="heart" />
                  {/* FAVOURITES */}
                </button>
              </div>
              <div className="button-bl2 mt-3">
                <button
                  type="button"
                  data-gtmtracking="funnel-3-step-06-create"
                  onClick={this.onCreateMix}
                  disabled={!data1 || !data2}
                >
                  {buttonCms.length > 2 ? buttonCms[2].text : ''}
                </button>
              </div>
            </div>
          </div>

          <div className="div-object">
            <div className="div-title-block">
              <h5>
                {subTextCms && subTextCms.length > 0 ? subTextCms[2].value : ''}
              </h5>
              <span>
                {data1 && !data2 ? `${subTextCms && subTextCms.length > 3 ? subTextCms[3].value : ''}` : ''}
              </span>
            </div>
            <div className="mmo-block3 add-boder">
              {
                    isRandom
                      ? (
                        <div
                          style={{
                            height: '385px',
                            overflowY: 'hidden',
                          }}
                        >
                          <div
                            id="random-right"
                            style={{
                              marginTop: '-623%',
                            }}
                          >
                            {
                              _.map(this.listDataRight, d => (
                                <div>
                                  <ScentItem
                                    data={d}
                                    onClickIngredient={this.onClickIngredient}
                                    onClickRightButton={this.onClickRemoveScentData1}
                                    isMMO
                                    cms={this.props.cms}
                                  />
                                  <div style={{ marginBottom: '20px' }} />
                                </div>
                              ))
                            }
                          </div>
                        </div>
                      )
                      : (data2
                        ? (
                          <ScentItem
                            // className="scent-mmo"
                            data={data2}
                            onClickIngredient={this.onClickIngredient}
                            onClickRightButton={this.onClickRemoveScentData2}
                            isMMO
                            cms={this.props.cms}
                          />
                        )
                        : (data1 ? (
                          <RecommendMmo
                            dataGtmtracking="funnel-3-step-05-scent-1-added-to-mix"
                            dataGtmtrackingSelect="funnel-3-step-02-select-scent-2"
                            data={data1.suggestions}
                            openSelection={this.onOpenSelectionData2}
                            onClickAddSuggest={this.onClickAddSuggest2}
                          />
                        ) : (
                          <button
                            type="button"
                            data-gtmtracking="funnel-3-step-02-select-scent-2"
                            onClick={this.onOpenSelectionData2}
                          >
                            <div data-gtmtracking="funnel-3-step-02-select-scent-2">
                              <span data-gtmtracking="funnel-3-step-02-select-scent-2">
                                {buttonCms.length > 0 ? buttonCms[0].text : ''}
                              </span>
                              <i data-gtmtracking="funnel-3-step-02-select-scent-2" className="fa fa-plus" />
                            </div>
                          </button>

                        )))
                  }
            </div>
          </div>
        </div>
      </div>
    );


    const htmlMobile = (
      <div className="div-mmo-mobile div-col items-center">
        <div className="div-title">
          <h1 className="title" style={{ marginTop: '10px' }}>
            {headerCms}
          </h1>
          <span className="description" style={{ marginBottom: '0px' }}>
            {subTextCms && subTextCms.length > 0 ? subTextCms[0].value : ''}
          </span>
        </div>
        <div className="content" style={isTablet ? { width: '60%' } : { width: '100%' }}>
          <div className="mmo-block2">
            <div className="disable-button video div-col justify-center">
              <Player
                ref="player"
                playsInline
                loop
                muted
                autobuffer
                disablePauseOnClick
                src={urlVideo}
                poster={placeholder}
              >
                <ControlBar disableCompletely disableDefaultControls />
                <Shortcut dblclickable clickable={false} />
                <BigPlayButton />
              </Player>
            </div>
            <div className="div-col justify-around items-center mb-4">
              <button
                type="button"
                data-gtmtracking="funnel-3-step-02-bis-trending-mix"
                className="button-bg__none"
                onClick={this.onClickRandom}
              >
                <img
                  data-gtmtracking="funnel-3-step-02-bis-trending-mix"
                  src={refresh}
                  alt="refresh"
                  style={{
                    width: '24px', marginRight: '0.5rem', textTransform: 'uppercase', letterSpacing: '0.2rem',
                  }}
                />
                {buttonCms.length > 1 ? buttonCms[1].text : ''}
              </button>
            </div>
            <div className="div-row justify-between mt-3" style={isMobile ? { margin: '0px 15px 0px 15px' } : {}}>
              {
                data1 ? (
                  <ScentMmoMobile
                    data={data1}
                    onClickRightButton={this.onClickRemoveScentData1}
                  />
                ) : (
                  <button
                    data-gtmtracking="funnel-3-step-02-select-scent-1"
                    type="button"
                    style={{
                      width: '156px',
                      height: '44px',
                      border: '1px solid #0D0D0D',
                      borderRadius: '6px',
                      background: '#0D0D0D',
                      color: '#ffffff',
                    }}
                    onClick={this.onOpenSelectionData1}
                  >
                    +
                    {' '}
                    {buttonCms.length > 0 ? buttonCms[0].text : ''}
                  </button>
                )
              }
              {
                data2 ? (
                  <ScentMmoMobile
                    data={data2}
                    onClickRightButton={this.onClickRemoveScentData2}
                  />
                ) : (
                  <button
                    data-gtmtracking="funnel-3-step-02-select-scent-2"
                    type="button"
                    style={{
                      width: '156px',
                      height: '44px',
                      border: '1px solid #0D0D0D',
                      borderRadius: '6px',
                      background: '#0D0D0D',
                      color: '#ffffff',
                    }}
                    onClick={this.onOpenSelectionData2}
                  >
                    +
                    {' '}
                    {buttonCms.length > 0 ? buttonCms[0].text : ''}
                  </button>
                )
              }
            </div>
            <div className="div-col justify-around items-center mt-4 mb-5" style={{ margin: '0px 15px' }}>

              <button
                type="button"
                style={{
                  width: '100%',
                  height: '44px',
                  borderRadius: '6px',
                  border: data1 && data2 ? '1px solid #0D0D0D' : '1px solid #8D8D8D',
                  background: data1 && data2 ? '#0D0D0D' : '#ffffff',
                  color: data1 && data2 ? '#ffffff' : '#8D8D8D',

                }}
                data-gtmtracking="funnel-3-step-06-create"
                onClick={this.onCreateMix}
                disabled={!data1 || !data2}
              >
                {buttonCms.length > 2 ? buttonCms[2].text : ''}
              </button>
              {/* <button
                type="button"
                className="button-bg__none"
              >
                <div style={{ width: '24px' }} />
              </button> */}
            </div>
          </div>
        </div>
      </div>
    );
    const listSuggest = data1 && !data2 ? data1.suggestions : (!data1 && data2 ? data2.suggestions : []);
    const suggestIds = _.map(listSuggest, x => x.id);
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/mmo')}
        </MetaTags>
        <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
        />
        {
          isShowSelect
            ? (
              <SelectionScent
                dataGtmtracking={this.selection === 'data1' ? 'funnel-3-step-03-scent-1-added-to-mix' : 'funnel-3-step-05-scent-1-added-to-mix'}
                datas={_.filter(products, x => (x.id !== (data1 ? data1.id : undefined) && x.id !== (data2 ? data2.id : undefined)))}
                close={this.onCloseSelection}
                onClickIngredient={this.onClickIngredient}
                onClickRightButton={this.onClickAddMix}
                cms={this.props.cms}
                suggestIds={suggestIds}
              />
            ) : (<div />)
        }
        <HeaderHomePage />
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <div>
          {isMobile ? htmlMobile : htmlWeb}
        </div>
        <FooterV2 />
      </div>
    );
  }
}

MMO.propTypes = {
  loadingPage: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  mmos: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  updateMmosData: PropTypes.func.isRequired,

};

function mapStateToProps(state) {
  return {
    login: state.login,
    cms: state.cms,
    mmos: state.mmos,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  loadingPage,
  addCmsRedux,
  updateMmosData,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MMO));
