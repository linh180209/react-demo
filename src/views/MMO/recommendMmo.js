import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import RecommendItem from './recommendItem';

class RecommendMmo extends Component {
  render() {
    const {
      data, gender, openSelection, onClickAddSuggest,
    } = this.props;
    return (
      <div className="div-recommend-mmo">
        {
          _.map(data || [], d => (
            <RecommendItem dataGtmtracking={this.props.dataGtmtracking} data={d} gender={gender} onClickAddSuggest={onClickAddSuggest} />
          ))
        }
        <div className="line" />
        <div className="div-wrap-button">
          <button
            data-gtmtracking={this.props.dataGtmtrackingSelect}
            type="button"
            onClick={openSelection}
          >
            + SELECT SCENT
          </button>
        </div>
      </div>
    );
  }
}

RecommendMmo.propTypes = {
  data: PropTypes.arrayOf().isRequired,
  gender: PropTypes.string.isRequired,
  openSelection: PropTypes.func.isRequired,
  onClickAddSuggest: PropTypes.func.isRequired,
};

export default RecommendMmo;
