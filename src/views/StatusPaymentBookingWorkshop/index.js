import React, { Component, memo } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import MetaTags from 'react-meta-tags';

import { Link } from 'react-router-dom/cjs/react-router-dom.min';
import Axios from 'axios';
import JsonRpcClient from 'react-jsonrpc-client';
import HeaderHomePage from '../../components/HomePage/header';
import {
  scrollTop, fetchCMSHomepage, getSEOFromCms, getNameFromButtonBlock, generateHreflang, removeLinkHreflang,
  fetchTokenSimplyBook, fetchTokenSimplyBookAdmin, generaCurrency, setPrerenderReady, segmentWorkshopBookingCompleted,
} from '../../Redux/Helpers/index';
import addCmsRedux from '../../Redux/Actions/cms';
import './styles.scss';

import { getEventListSimplyBook } from '../../Redux/Actions/bookWorkshop';
import { isMobile } from '../../DetectScreen';
import tick from '../../image/tick-gold.svg';
import { getCompanySimplyBook, SIMPLY_BOOK_URL } from '../../config';
import { toastrError } from '../../Redux/Helpers/notification';
import loadingPage from '../../Redux/Actions/loading';
import FooterV2 from '../../views2/footer';

const getCms = (workshopCMS) => {
  const objectReturn = {
    headerBlock: '',
    textBlock: '',
    imageBlock: {},
    seo: undefined,
    paragraphBlock: '',
    ateliersBlock: [],
    faqBlocks: [],
  };
  if (!_.isEmpty(workshopCMS)) {
    const seo = getSEOFromCms(workshopCMS);
    const { body } = workshopCMS;
    if (body) {
      const headerBlock = _.find(body, x => x.type === 'header_block').value;
      const textBlock = _.find(body, x => x.type === 'text_block').value;
      const imageBlock = _.find(body, x => x.type === 'image_block').value;
      const buttonBlock = _.filter(body, x => x.type === 'button_block');
      const paragraphBlock = _.find(body, x => x.type === 'paragraph_block').value;
      const ateliersBlock = _.filter(body, x => x.type === 'ateliers_block').map(ele => ele.value).map(ele => ele[0]).map(ele => ele.value);
      const faqBlocks = _.filter(body, x => x.type === 'faq_block');
      _.assign(objectReturn, {
        headerBlock, textBlock, seo, imageBlock, paragraphBlock, ateliersBlock, buttonBlock, faqBlocks,
      });
    }
  }
  return objectReturn;
};

// const { match } = this.props;
//     const { token } = match.params;

class StatusPaymentBookingWorkshop extends Component {
  constructor(props) {
    super(props);

    this.state = {
      seo: undefined,
      detailWorkshopData: {},
      unitList: {},
      statusUpdated: false,
    };
  }

  componentDidMount = () => {
    setPrerenderReady();
    scrollTop();
    // if (this.state.statusUpdated) {
    this.fetchDataInit();
    // }
  }

  // componentDidUpdate(prevProps) {
  //   const { match } = prevProps;
  //   if (match && match.params && match.params.workshop && match.params.workshop !== this.state.workshop) {
  //     googleAnalitycs(window.location.pathname);
  //   }
  //   if (match.params.workshop !== this.state.workshop) {
  //     this.setState({ workshop: match.params.workshop });
  //   }
  // }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    this.props.loadingPage(true);
    const workshopCMS = _.find(cms, x => x.title === 'workshop');
    const pending = [fetchTokenSimplyBook(), fetchCMSHomepage('workshop-v2')];
    if (!workshopCMS) {
      const results = await Promise.all(pending);
      const cmsData = results[1];

      if (this.props.match.params.idInvoice && this.props.match.params.payment) {
        fetchTokenSimplyBookAdmin().then(
          (res) => {
            const api = new JsonRpcClient({
              endpoint: SIMPLY_BOOK_URL,
              headers: {
                'X-Company-Login': getCompanySimplyBook(),
                'X-Token': localStorage.getItem('tokenSimplyBook'),
              },
            });
            const invoicePromise = Axios.get(`https://user-api-v2.simplybook.me/admin/invoices/${this.props.match.params.idInvoice}`, {
              headers: {
                'Content-Type': 'application / json',
                'X-Company-Login': getCompanySimplyBook(),
                'X-Token': res,
              },
            });
            Promise.all([invoicePromise, api.request('getUnitList')])
              .then((resp) => {
                console.log('Workshop Response : ', resp[0]);
                segmentWorkshopBookingCompleted(resp[0].data, resp[1]);
                this.setState(prevState => ({ ...prevState, detailWorkshopData: resp[0].data }));
                this.props.loadingPage(false);
              // api.request('confirmBookingPayment', )
              }).catch((err) => {
                toastrError(err?.errors?.message);
                console.log('Error : ', err);
              });
          },
        );
      }
      if (results[0]) {
        const api = new JsonRpcClient({
          endpoint: SIMPLY_BOOK_URL,
          headers: {
            'X-Company-Login': getCompanySimplyBook(),
            'X-Token': localStorage.getItem('tokenSimplyBook'),
          },
        });
        api.request(
          'getUnitList',
        ).then((res) => {
          if (res) {
            this.setState({ unitList: res });
          }
        }).catch((err) => { console.log(err?.message?.message); });
      }


      // const pending = [fetchCMSHomepage('refer-a-friend')];
      //   const results = await Promise.all(pending);

      this.props.addCmsRedux(cmsData);

      const dataFromStore = getCms(cmsData);
      this.setState(dataFromStore);
    } else {
      const dataFromStore = getCms(workshopCMS);
      this.setState(dataFromStore);
    }
    // localStorage.getItem('tokenSimplyBook') ? fetchTokenSimplyBook().then(res => res ? getEventListSimplyBook() : '') : getEventListSimplyBook();
  }

  groupBy = (a, keyFunction) => {
    const groups = {};
    a.forEach((el) => {
      const key = keyFunction(el);
      if (key in groups === false) {
        groups[key] = [];
      }
      groups[key].push(el);
    });
    return groups;
  }

  renderGroupProduct = (arr) => {
    const filtered = _.filter(arr, item => item.type === 'product');
    const byName = this.groupBy(filtered.filter(it => it.product.name), it => it.product.name);

    /* console.log(byName); */
    const countsExtended = Object.keys(byName).map((name) => {
      const byZone = this.groupBy(byName[name], it => it.product.name);

      const sum = byName[name].reduce((acc, it) => acc + it.product.price, 0);
      return {
        name,
        count: byName[name] ? byName[name].length : 1,
        valueSum: sum,
      };
    });

    return (
      countsExtended
        ? (
          _.map(countsExtended, item => (
            <div className="info">
              <p>{item?.count}</p>
              <p className="name">{item.name}</p>
              <p className="text-right">
                {generaCurrency(item?.valueSum)}
              </p>
            </div>
          ))

        ) : <div />
    );
  }

  render() {
    const {
      buttonBlock,
      seo,
      detailWorkshopData,
      unitList,
    } = this.state;
    const menuText = this.props.match.params.workshop ? this.props.match.params.workshop : '';
    const seoTitle = _.isEmpty(menuText) ? (seo ? seo.seoTitle : '') : getNameFromButtonBlock(buttonBlock, `title ${menuText}`);
    const seoDescription = _.isEmpty(menuText) ? (seo ? seo.seoKeywords : '') : getNameFromButtonBlock(buttonBlock, `description ${menuText}`);
    const filterBooking = detailWorkshopData ? _.find(detailWorkshopData?.lines, e => e.type === 'booking') : '';
    const providerID = filterBooking && filterBooking?.bookings[0] ? filterBooking.bookings[0].provider_id : '';
    const paymentCanceltext = getNameFromButtonBlock(buttonBlock, 'payment_cancel');
    const paymentSuccesstext = getNameFromButtonBlock(buttonBlock, 'payment_success');
    const paymentNotSuccesstext = getNameFromButtonBlock(buttonBlock, 'payment_not_success');
    const paymentErrtext = getNameFromButtonBlock(buttonBlock, 'payment_err');
    const confirmEmailText = getNameFromButtonBlock(buttonBlock, 'confirm_email');
    const orderText = getNameFromButtonBlock(buttonBlock, 'order');
    const boutiqueText = getNameFromButtonBlock(buttonBlock, 'boutique');
    const backToHometext = getNameFromButtonBlock(buttonBlock, 'back_to_home');
    const breakdownText = getNameFromButtonBlock(buttonBlock, 'breakdown');
    const qtyText = getNameFromButtonBlock(buttonBlock, 'qty');
    const nameText = getNameFromButtonBlock(buttonBlock, 'name');
    const amountText = getNameFromButtonBlock(buttonBlock, 'amount');
    const totalText = getNameFromButtonBlock(buttonBlock, 'total');
    // .bookings[0].provider_id;
    return (
      <React.Fragment>
        <MetaTags>
          <title>
            {seoTitle}
          </title>
          <meta name="description" content={seoDescription} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/perfume-workshop')}
        </MetaTags>
        <HeaderHomePage />
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}

        <div className={`container-fluid ${!isMobile ? 'py-4' : 'p-0'}`} style={{ background: '#f8f8f8' }}>
          <section className="book-workshop-res">
            <div className="wrapper-content">
              <div className="left">
                <img src={tick} alt="checked-icon" />
                {detailWorkshopData.status === 'cancelled' ? <h3>{paymentCanceltext || 'Payment Cancelled!'}</h3>
                  : detailWorkshopData.status === 'paid' ? <h3>{paymentSuccesstext || 'Thank you for your purchase!'}</h3> : detailWorkshopData.status === 'error' ? detailWorkshopData.status === 'pending' ? <h3>{paymentNotSuccesstext || 'Your payment not success!'}</h3> : <h3>{paymentErrtext || 'Err! Please try again'}</h3> : ''}
                <div className="info">
                  {detailWorkshopData.status === 'paid' ? (
                    <p>
                      {confirmEmailText || 'A confirmation email has been sent to'}
                      {' '}
                      <strong>{detailWorkshopData?.client?.email}</strong>
                    </p>
                  ) : ''}
                  <p>
                    {orderText || 'Order:'}
                    {' '}
                    {detailWorkshopData?.number}
                  </p>
                  <p>
                    {boutiqueText || 'Boutique:'}
                    {' '}
                    {providerID && unitList && unitList[providerID] ? unitList[providerID].name : ''}
                  </p>
                </div>
                {!isMobile ? <Link to="/sg" className="btn-backhome">{backToHometext || 'Back to home'}</Link> : ''}
              </div>
              <div className="right">
                <div className="breakdown">
                  <h4>{breakdownText || 'Breakdown'}</h4>
                  <div className="total-info">
                    <div className="info">
                      <p>{qtyText || 'Qty'}</p>
                      <p className="name">{nameText || 'Name'}</p>
                      <p className="text-right">{amountText || 'Amount'}</p>
                    </div>
                    {detailWorkshopData ? _.map(detailWorkshopData?.lines, item => (item.type === 'booking'
                      ? (
                        <div className="info">
                          <p>{item?.qty}</p>
                          <p className="name">{item?.name ? item?.name.replace(/ *\([^)]*\) */g, '') : ''}</p>
                          <p className="text-right">
                            {generaCurrency(item?.amount)}
                          </p>
                        </div>
                      ) : '')) : ''}
                    {detailWorkshopData && _.find(detailWorkshopData?.lines, item => item.type === 'product') ? this.renderGroupProduct(detailWorkshopData?.lines) : ''}
                  </div>

                  <div className="foot-info">
                    <span>{totalText || 'Total'}</span>
                    <span>
                      {generaCurrency(detailWorkshopData?.amount)}
                    </span>
                  </div>
                </div>
              </div>
              {isMobile ? <Link to="/sg" className="btn-backhome">{backToHometext || 'Back to home'}</Link> : ''}
            </div>
          </section>
        </div>
        <div style={{ display: 'none' }}>
          {this.props.match.params.status === 'cancel' && this.props.match.params.payment
            ? <iframe title="payment" src={`https://maison21g.simplybook.asia/v2/ext/invoice-payment/cancel/system/${this.props.match.params.payment}/invoice_id/${this.props.match.params.idInvoice}`} width="540" height="450" />
            : this.props.match.params.status === 'success' && this.props.match.params.payment
              ? <iframe title="payment" src={`https://maison21g.simplybook.asia/v2/ext/invoice-payment/return/system/${this.props.match.params.payment}/invoice_id/${this.props.match.params.idInvoice}`} width="540" height="450" /> : ''}
          {/* https://witcompany.simplybook.asia/v2/ext/invoice-payment/return/system/paypal/invoice_id/69 */}
        </div>
        <FooterV2 />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  cms: state.cms,
  countries: state.countries,
  eventList: state?.bookWorkshop?.eventList || [],
  dataAddOns: state?.bookWorkshop?.dataAddOns || [],
  dataTime: state?.bookWorkshop?.time || null,
  // showAskRegion: state.showAskRegion,
});

const mapDispatchToProps = {
  addCmsRedux,
  getEventListSimplyBook,
  loadingPage,
};

StatusPaymentBookingWorkshop.defaultProps = {
  match: {
    params: {
      workshop: '',
    },
  },
};

StatusPaymentBookingWorkshop.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      workshop: PropTypes.string,
    }),
  }),
};

export default memo(withRouter(connect(mapStateToProps, mapDispatchToProps)(StatusPaymentBookingWorkshop)));
