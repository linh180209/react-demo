import React, { Component } from 'react';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import queryString from 'query-string';
import '../../styles/product_block_v2.scss';
import '../../styles/mmo-gift.scss';

import Header from '../../components/HomePage/header';
import icBack from '../../image/icon/back-gift.svg';
import CreatePerfumeAdapt from '../../components/ProductAllV2/ProductBlock/createPerfumeAdapt';
import addCmsRedux from '../../Redux/Actions/cms';
import { createBasketGuest, addProductBasket } from '../../Redux/Actions/basket';
import loadingPage from '../../Redux/Actions/loading';
import icSearchBlack from '../../image/icon/search-product-black.svg';
import {
  GET_ALL_SCENTS_PRODUCTS, GET_PRICES_PRODUCTS, GET_BOTTLE_PRODUCTS, GET_DROPDOWN_WAX_PERFUME,
} from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import addScentsToStore from '../../Redux/Actions/scents';
import {
  getSEOFromCms, fetchCMSHomepage, scrollTop, googleAnalitycs, getNameFromButtonBlock, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady, getSearchPathName,
} from '../../Redux/Helpers';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import { isBrowser, isMobile } from '../../DetectScreen';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';

const preDataGroup = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { tags } = x;
    _.forEach(tags, (tag) => {
      const group = _.find(dataScents, d => d.tag === tag.name);
      if (group) {
        group.datas.push(x);
      } else {
        dataScents.push({
          tag: tag.name,
          group: tag.group,
          datas: [x],
        });
      }
    });
  });
  return dataScents;
};

const preDataAlphabet = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { name } = x;
    const firstChart = name.charAt(0);
    const group = _.find(dataScents, d => d.tag.toLowerCase() === firstChart.toLowerCase());
    if (group) {
      group.datas.push(x);
    } else {
      dataScents.push({
        tag: firstChart,
        datas: [x],
      });
    }
  });
  return dataScents;
};

const preDataScents = (scents) => {
  const dataScents = [];

  dataScents.push({
    datas: _.orderBy(preDataGroup(scents), ['tag'], ['asc']),
    title: ['Scent Family', 'Gender', 'mood'],
  });

  dataScents.push({
    datas: _.orderBy(preDataAlphabet(scents), ['tag'], ['asc']),
    title: ['Alphabet'],
  });

  dataScents.push({
    datas: [{
      tag: '',
      datas: _.orderBy(scents, ['popularity'], ['desc']),
    }],
    title: ['Popularity'],
  });
  console.log('dataScents', dataScents);
  return dataScents;
};

const getCms = (cms) => {
  const objectReturn = {
    seo: undefined,
    buttonBlocks: [],
  };
  if (!_.isEmpty(cms)) {
    const seo = getSEOFromCms(cms);
    const { body, header_text: headerText } = cms;
    if (body) {
      const buttonBlocks = _.filter(body, x => x.type === 'button_block');
      _.assign(objectReturn, {
        seo,
        headerText,
        buttonBlocks,
      });
    }
  }
  return objectReturn;
};

class MmoGift extends Component {
  constructor(props) {
    super(props);
    const { infoGift } = this.props.location.state ? this.props.location.state : {};
    this.state = {
      seo: undefined,
      headerText: '',
      buttonBlocks: [],
      isShowPopUpSearchMobile: false,
      isShowPopupFiltermobile: false,
      bottlePerfume: {},
      pricesPerfume: [],
      infoGift,
      redeemCode: infoGift ? infoGift.codes : '',
    };
    this.valueFilters = [
      {
        title: 'Family',
        value: 'Scent Family',
      },
      {
        title: 'Popularity',
        value: 'Popularity',
      },
      {
        title: 'Alphabet (A-Z)',
        value: 'Alphabet',
      },
      {
        title: 'Mood',
        value: 'Mood',
      },
    ];
    this.refInput = React.createRef();
  }

  componentDidMount = () => {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/mmo-gift');
    this.fetchCMS();
    this.handleForPerfume();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const returnObject = {};
    const { scents } = nextProps;
    if (scents !== prevState.scents) {
      const dataScents = preDataScents(scents);
      _.assign(returnObject, { scents, dataScents });
    }
    return !_.isEmpty(returnObject) ? returnObject : null;
  }

  fetchCMS = async () => {
    const { cms } = this.props;
    const mmoGift = _.find(cms, x => x.title === 'MMO GIFT');
    if (!mmoGift) {
      const cmsData = await fetchCMSHomepage('mmo-gift');
      this.props.addCmsRedux(cmsData);
      const dataFromStore = getCms(cmsData);
      this.setState(dataFromStore);
    } else {
      const dataFromStore = getCms(mmoGift);
      this.setState(dataFromStore);
    }
  }

  handleForPerfume = () => {
    this.fetchAllScent();
    const pending = [this.getPrice('perfume'), this.getBottle()];
    Promise.all(pending).then((results) => {
      this.setState({ pricesPerfume: results[0], bottlePerfume: results[1][0] });
    }).catch((err) => {
      console.log('err', err);
    });
  }

  fetchAllScent = () => {
    const { scents } = this.props;
    if (scents && scents.length > 0) {
      return;
    }
    const option = {
      url: GET_ALL_SCENTS_PRODUCTS,
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      this.props.loadingPage(false);
      if (result && !result.isError) {
        this.props.addScentsToStore(result);
        return;
      }
      throw new Error(result.message);
    }).catch((error) => {
      this.props.loadingPage(false);
      toastrError(error.message);
    });
  }

  getPrice = async (type) => {
    try {
      const option = {
        url: GET_PRICES_PRODUCTS.replace('{type}', type),
        method: 'GET',
      };
      const result = await fetchClient(option);
      return result;
    } catch (error) {
      toastrError('error', error.message);
    }
  }

  getBottle = async () => {
    try {
      const option = {
        url: GET_BOTTLE_PRODUCTS,
        method: 'GET',
      };
      const result = await fetchClient(option);
      return result;
    } catch (error) {
      toastrError('error', error.message);
    }
  }

  onCloseProductDetail = () => {
    console.log('onCloseProductDetail');
  }

  onClickGoBack = () => {
    this.props.history.goBack();
  }

  onClickChoiseGift = (item, customeBottle) => {
    const { infoGift } = this.state;
    const { pathname, search } = getSearchPathName(generateUrlWeb(`/checkout-gift/?codes=${infoGift.codes}`));
    this.props.history.push({
      pathname,
      search,
      state: {
        codes: infoGift.codes,
        item,
        customeBottle,
      },
    });
  }

  onClickApply = () => {

  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  onChangeFilter = (value) => {
    this.setState({ isShowPopupFiltermobile: value, isShowPopUpSearchMobile: false });
  }

  onChangeSearch = (value) => {
    this.setState({ isShowPopupFiltermobile: false, isShowPopUpSearchMobile: value });
    if (value) {
      setTimeout(() => {
        this.refInput.current.focus();
      }, 500);
    }
  }

  onChangeValueSearch = () => {
    this.refFilterSearchScent.current.onChangeValueSearchFilter({ value: this.valueInputSearch, type: 'Search' });
    this.setState({ isShowPopUpSearchMobile: false });
  }

  shareRefFilterSearchScent = (ref) => {
    this.refFilterSearchScent = ref;
  }

  onChangeValueFilter = (value) => {
    this.refFilterSearchScent.current.onChangeValueSearchFilter({ value, type: 'Filter' });
    this.setState({ isShowPopupFiltermobile: false });
  }

  onChangeInputSearch = (e) => {
    const { value } = e.target;
    this.valueInputSearch = value;
  }

  render() {
    const {
      isShowPopUpSearchMobile, isShowPopupFiltermobile, pricesPerfume, bottlePerfume,
      dataScents, buttonBlocks, headerText, seo, isShowIngredient, ingredientDetail,
      infoGift,
    } = this.state;
    const searchBt = getNameFromButtonBlock(buttonBlocks, 'SEARCH');
    const backBt = getNameFromButtonBlock(buttonBlocks, 'Back');
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/mmo-gift')}
        </MetaTags>
        {/* <Header />
        { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <HeaderHomePageV3 />
        <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
        />
        <div className="div-product-block-v2 div-mmo-gift">
          <div className="div-header-mmo-gift">
            <button
              onClick={this.onClickGoBack}
              type="button"
              className={isMobile ? 'hidden' : 'button-back'}
            >
              <img src={icBack} alt="back" />
              {backBt}
            </button>
            <h1>
              {headerText}
            </h1>
          </div>
          <div className="div-card-mmo-gift">
            <div className="div-card ">
              <div className="div-wrap">
                <div className="div-left">
                  <CreatePerfumeAdapt
                    onCloseProductDetail={this.onCloseProductDetail}
                    dataScents={dataScents}
                    loadingPage={this.props.loadingPage}
                    onClickIngredient={this.onClickIngredient}
                    pricesPerfume={pricesPerfume}
                    pricesSolid={[]}
                    valueDropdownWax={[]}
                    bottlePerfume={bottlePerfume}
                    cms={this.props.cms}
                    typeSelection="creation_perfume"
                    basket={this.props.basket}
                    addProductBasket={this.props.addProductBasket}
                    createBasketGuest={this.props.createBasketGuest}
                    buttonBlocks={buttonBlocks}
                    onChangeFilter={this.onChangeFilter}
                    onChangeSearch={this.onChangeSearch}
                    shareRefFilterSearchScent={this.shareRefFilterSearchScent}
                    isMmoGift
                    isHiddenBack={isBrowser}
                    infoGift={infoGift || {}}
                    onClickChoiseGift={this.onClickChoiseGift}
                  />
                </div>
              </div>
            </div>
          </div>
          {/* can't use on Filter Search Scent */}
          <div
            className={isShowPopUpSearchMobile || isShowPopupFiltermobile ? 'div-popup-filter-search-mobile animated faster fadeIn' : 'hidden'}
          >
            <div className="bg-close" onClick={() => this.setState({ isShowPopUpSearchMobile: false, isShowPopupFiltermobile: false })} />
            <div className={isShowPopupFiltermobile ? 'div-popup-filter' : 'hidden'}>
              {
              _.map(this.valueFilters, d => (
                <button
                  type="button"
                  onClick={() => {
                    this.onChangeValueFilter(d.value);
                  }}
                >
                  {d.title}
                </button>
              ))
            }
            </div>
            <div className={isShowPopUpSearchMobile ? 'div-popup-search' : 'hidden'}>
              <div className="div-input">
                <img src={icSearchBlack} alt="search" />
                <input onChange={this.onChangeInputSearch} ref={this.refInput} type="text" />
              </div>
              <button type="button" onClick={this.onChangeValueSearch}>
                {searchBt}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
    basket: state.basket,
    scents: state.scents,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  createBasketGuest,
  addProductBasket,
  loadingPage,
  addScentsToStore,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MmoGift));
