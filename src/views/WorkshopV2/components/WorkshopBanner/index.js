import React, { Component } from 'react';
import './styles.scss';

class WorkshopBanner extends Component {
  render() {
    const {
      animate,
      headerText,
      textBlock,
      imageBlock,
    } = this.props;

    return (
      <div className={`workshop__banner ${animate.banner}`} style={{ backgroundImage: imageBlock ? `url(${imageBlock.image})` : '' }}>
        <div className="container-fluid custom" style={{ marginTop: '40px' }}>
          <div className="row">
            <div className="col-12 text-center">
              <h1>
                {headerText}
              </h1>
              <p>
                {textBlock}
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default WorkshopBanner;
