import React, { Component } from 'react';
import { isMobileOnly } from 'react-device-detect';
import _ from 'lodash';
import classnames from 'classnames';
import './styles.scss';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { isBrowser, isMobile } from '../../../../DetectScreen';
import icNext from '../../../../image/icon/ic-next-white.svg';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import LazyImage from '../../../../components/LazyImage';

class WorkshopList extends Component {
  handleClick = (id) => {
    const { showDetails } = this.props;
    showDetails(id);
  }

  itemRender = (index, item) => (
    <div className={`item${index + 1} item-list`}>
      <div key={index} className="workshop__list__item" onClick={() => { this.handleClick(index); }}>
        <div className="__image">
          <LazyImage src={item ? item.videos[0].value.placeholder : ''} alt="workshop_item" className="img-fluid" />
          {/* <img src={item ? item.videos[0].value.placeholder : ''} alt="workshop_item" className="img-fluid" /> */}
        </div>
        <div className={classnames(this.props.isHomePage ? 'hover-text' : 'hiden', (index !== 0 ? 'small' : ''))}>
          <div className="text">
            <span>
              {getNameFromButtonBlock(this.props.buttonBlocks, 'book_now')}
            </span>
            <img src={icNext} alt="icon" />
          </div>
        </div>
        {
            index === 0 && !this.props.isHomePage && isBrowser ? (
              <div className="__text-des div-col item-first">
                <div className="bg-text" />
                <div className="div-row justify-between">
                  <div className="__title">
                    {item ? item.header.header_text : ''}
                  </div>
                  <div className="__price">
                    {item.short_price_text}
                  </div>
                </div>
                <div className="__description">
                  {item.short_text}
                </div>


              </div>
            ) : (
              <div className="__text-des div-col">
                <div className="bg-text" />
                <div className="div-row justify-between">
                  <div className="__title">
                    {item ? item.header.header_text : ''}
                  </div>
                  <div className="__price">
                    {item.short_price_text}
                  </div>
                </div>
                <div className="__description">
                  {item.short_text}
                </div>

              </div>
            )
          }
      </div>
    </div>
  )

  render() {
    const {
      animate, items, isHomePage,
    } = this.props;
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      // autoplay: true,
      arrows: !isMobile,
      autoplaySpeed: 6000,
      pauseOnHover: false,
    };
    return (
      <div className={`workshop__list_v2 ${animate ? animate.list : ''} ${isHomePage && isMobile ? 'workshop-slider' : ''}`}>
        {
          isMobile && isHomePage ? (
            <div className="slider-item">
              <Slider {...settings}>
                {
                items.map((item, index) => (
                  this.itemRender(index, item)
                ))
              }
              </Slider>
            </div>
          ) : (
            <div className="grid-container">
              {items.map((item, index) => (
                this.itemRender(index, item)
              ))}
            </div>
          )
        }
      </div>
    );
  }
}

export default WorkshopList;
