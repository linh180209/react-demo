import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';
import { Input } from 'reactstrap';
import { connect } from 'react-redux';
import { getAltImageV2, getNameFromButtonBlock } from '../../../../Redux/Helpers';
import './styles.scss';
import amountPersonIC from '../../../../image/amount-person.svg';
import WorkshopListV2 from '../WorkshopListV2';
import RecommendItem from '../../../../components/Products/recommendItem';
import { GET_REVIEW_WORK_SHOPS } from '../../../../config';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../../Redux/Helpers/notification';
import Reviews from '../../../ProductB2C/reviews';

class WorkshopDetailMobileV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectData: {},
      quantity: 1,
      listReviews: [],
    };
    this.onChangeQuantity = this.onChangeQuantity.bind(this);
    this.isFetchReviewWorkshop = false;
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    const { item } = this.props;
    if (item && item.videos[0]) {
      this.setState({ selectData: item.videos[0] });
    } else if (item) {
      this.setState({ selectData: item.images[0] });
    }
  }

  componentDidUpdate() {
    if (!this.isFetchReviewWorkshop && this.props.eventList && this.props.item) {
      this.isFetchReviewWorkshop = true;
      this.getReviewWorkShops();
    }
  }

  onChangeQuantity(e) {
    this.setState({ quantity: e.target.value });
  }

  getReviewWorkShops = () => {
    const { item } = this.props;
    const filteredButton = item?.buttons.find(x => x?.value?.name.includes('simplyBookID'));
    const id = filteredButton ? filteredButton.value.name.split('-')[1] : 0;
    const eventListFiltered = id && this.props.eventList ? this.props.eventList[id] : [];
    if (eventListFiltered) {
      const option = {
        url: GET_REVIEW_WORK_SHOPS.replace('{id}', eventListFiltered?.id),
        method: 'GET',
      };
      fetchClient(option).then((result) => {
        console.log('getReviewWorkShops', result);
        this.setState({ listReviews: result });
      }).catch((err) => {
        toastrError(err.mesage);
      });
    }
  };

  createElements = (number) => {
    const elements = [];
    for (let i = 1; i <= number; i += 1) {
      elements.push(<option value={i}>{i}</option>);
    }
    return elements;
  }

  render() {
    const {
      animate, item, faqBlock,
    } = this.props;
    const { selectData } = this.state;
    let listImageVideo = [];
    if (item && item.videos[0]) {
      listImageVideo.push(item.videos[0]);
    }
    listImageVideo = listImageVideo.concat(item.images);
    listImageVideo = _.filter(listImageVideo, x => x.id !== selectData.id);
    const settings = {
      dots: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      arrows: false,
      autoplaySpeed: 5000,
      pauseOnHover: false,
    };
    const { quantity } = this.state;
    // const filterServiceEvent = item.buttons[1]
    const filteredButton = item?.buttons.find(x => x?.value?.name.includes('simplyBookID'));
    const id = filteredButton ? filteredButton.value.name.split('-')[1] : 0;
    const eventListFiltered = id && this.props.eventList ? this.props.eventList[id] : [];
    const bookAWorkshopText = getNameFromButtonBlock(item?.buttons, 'sg');
    const faqBlockFiltered = _.find(faqBlock, itemFAQ => itemFAQ.value.header && itemFAQ.value.header.header_text === `simplyBookID-${id}`);
    return (
      <div className="mobi--workshop__detail">
        <WorkshopListV2 animate={this.props.animate} items={this.props.ateliersBlock} showDetails={this.props.showDetails} currentMenuText={this.props.currentMenuText} />
        <div className={`mobi--workshop__detail__banner ${animate.detailMobie[0]}`}>
          <Slider {...settings}>
            {item && item.videos && item.videos.length > 0 && item.videos[0].value ? (
              <div className="workshop__detail__images__item">
                <video
                  style={{
                    objectFit: 'cover',
                    width: '100%',
                  }}
                  muted="true"
                  playsinline="true"
                  preload="auto"
                  loop
                  src={item ? item.videos[0].value.video : ''}
                  poster={item ? item.videos[0].value.placeholder : ''}
                  controls
                >
                  {/* <source src={item ? item.videos[0].value.video : ''} /> */}
                </video>
              </div>
            ) : ''}
            {item && item.images && item.images.map(ele => ele.value).map(image => (
              <div className="workshop__detail__images__item">
                <img loading="lazy" src={image.image} alt={getAltImageV2(image)} className="img-fluid" />
              </div>
            ))}
          </Slider>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-12">
              <div className={`mobi--workshop__detail__info mt-2 mb-2 ${animate.detailMobie[0]}`}>
                <div className="information__header">
                  <div className="information__header__title">
                    {/* {item ? item.header.header_text : ''} */}
                    {eventListFiltered ? eventListFiltered?.name : ''}
                  </div>
                  <div className="information__header__price">
                    <p style={{ fontSize: 18 }}>{item ? item.price : ''}</p>
                    <p style={{ fontSize: 15 }}>{item ? item.price_text : ''}</p>
                  </div>
                </div>
              </div>
              <div className="workshop__detail__about__content" dangerouslySetInnerHTML={{ __html: item ? item.text : '' }} />
              <div className="mobi--workshop__detail__bookNow col-12">
                {item && (
                  <div className="d-flex flex-rows justify-content-between w-100 p-0">
                    {eventListFiltered && eventListFiltered.bookings_limit > 1
                      ? (
                        <div className="wrapper-select">
                          <img src={amountPersonIC} alt="icon" />
                          <Input
                            type="select"
                            className="select-amount-person"
                            name="country"
                            onChange={this.onChangeQuantity}
                            value={quantity || 1}
                          >
                            {this.createElements(eventListFiltered.bookings_limit)}
                          </Input>
                        </div>
                      ) : ''}
                    <button type="button" className="d-flex justify-content-center align-items-center booking__btn" onClick={() => this.props.directBookingPage(1, eventListFiltered.id, quantity || 1)}>
                      {bookAWorkshopText ? bookAWorkshopText.toUpperCase() : 'BOOK A WORKSHOP'}
                    </button>
                    {/* {item.buttons.length === 2 ? (
                      <React.Fragment>
                        <a href={item.buttons[0].value.link}>
                          <button type="button" className="d-flex justify-content-between align-items-center booking__btn" onClick={this.onClick}>
                            {item.buttons[0].value.text.toUpperCase()}
                          </button>
                        </a>

                      </React.Fragment>
                    ) : (
                      <button type="button" className="d-flex justify-content-center align-items-center booking__btn" onClick={() => this.props.directBookingPage(1, eventListFiltered.id, quantity || 1)}>
                        {item.buttons[0].value.text.toUpperCase()}
                      </button>
                    )
                    } */}
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="row mb-5">
            <div className="col-12">
              <div className="mobi--workshop__detail__benefit">
                <div className="mobi--workshop__detail__benefit__title">
                  {this.props.youJourneyText || 'Your workshop journey'}
                </div>
                {item && (
                  <div className="d-flex flex-rows justify-content-between wrapper-list" style={{ gap: 16, overflowX: 'auto', marginRight: -16 }}>
                    {item.what_you_gets.map(ele => ele.value).map((offer, index) => (
                      <div key={index}>
                        <div className="mobi--workshop__detail__benefit__item">
                          <div className="mobi--workshop__detail__benefit__item__icon">
                            <img src={offer.image.image} alt="" className="img-fluid" />
                          </div>
                          <div className="mobi--workshop__detail__benefit__item__title">
                            {offer.title}
                          </div>
                          <div className="mobi--workshop__detail__benefit__item__content">
                            {offer.text}
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                )}
              </div>
              {faqBlockFiltered ? (
                <div className="div-faq">
                  <div className="div-content-faq">
                    <div
                      style={{
                        width: '100%',
                        height: '1px',
                        background: '#e2e2e2',
                      }}
                    />
                    {
                      _.map(faqBlockFiltered.value ? faqBlockFiltered.value.faq : [], d => (
                        <RecommendItem title={d.value.header.header_text} description={d.value.paragraph} />
                      ))
                    }
                  </div>
                </div>
              ) : ''}
              {
                this.state.listReviews?.length > 0 && (
                  <Reviews
                    isWorkShops
                    data={this.state.listReviews}
                    cms={this.props.cms}
                    buttonBlocks={this.props.buttonBlock}
                    updateReviewData={() => {}}
                  />
                )
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  eventList: state.bookWorkshop.eventList,
  cms: state.cms,
});


export default connect(mapStateToProps, null)(WorkshopDetailMobileV2);
