/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import {
  Player, BigPlayButton,
} from 'video-react';
import _ from 'lodash';
import './styles.scss';
import {
  Input,
} from 'reactstrap';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { connect } from 'react-redux';
import { getAltImageV2, getNameFromButtonBlock } from '../../../../Redux/Helpers';
import amountPersonIC from '../../../../image/amount-person.svg';
import nextIC from '../../../../image/icon/next-ic-slick.svg';
import PrevIC from '../../../../image/icon/prev-ic-slick.svg';
import RecommendItem from '../../../../components/Products/recommendItem';
import { isMobile } from '../../../../DetectScreen';
import { GET_REVIEW_WORK_SHOPS, POST_HISTORY_REVIEW_WORKSHOP_ID_URL } from '../../../../config';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../../Redux/Helpers/notification';
import Reviews from '../../../ProductB2C/reviews';

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style }}
      onClick={onClick}
    >
      <img src={nextIC} alt="next-ic" />
    </div>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style }}
      onClick={onClick}
    >
      <img src={PrevIC} alt="prev-ic" />
    </div>
  );
}
class WorkshopDetailV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 1,
      listReviews: [],
    };
    this.onChangeQuantity = this.onChangeQuantity.bind(this);
    this.isFetchReviewWorkshop = false;
  }

  componentDidUpdate() {
    if (!this.isFetchReviewWorkshop && this.props.eventList && this.props.item) {
      this.isFetchReviewWorkshop = true;
      this.getReviewWorkShops();
    }
  }

  onChangeQuantity(e) {
    this.setState({ quantity: e.target.value });
  }

  getReviewWorkShops = () => {
    const { item } = this.props;
    const filteredButton = item?.buttons.find(x => x?.value?.name.includes('simplyBookID'));
    const id = filteredButton ? filteredButton.value.name.split('-')[1] : 0;
    const eventListFiltered = id && this.props.eventList ? this.props.eventList[id] : [];
    if (eventListFiltered) {
      const option = {
        url: GET_REVIEW_WORK_SHOPS.replace('{id}', eventListFiltered?.id),
        method: 'GET',
      };
      fetchClient(option).then((result) => {
        console.log('getReviewWorkShops', result);
        this.setState({ listReviews: result });
      }).catch((err) => {
        toastrError(err.mesage);
      });
    }
  };

  createElements = (number, event) => {
    const elements = [];
    const min = event.min_group_booking || 1;
    for (let i = parseInt(min, 10); i <= number; i += 1) {
      elements.push(<option value={i}>{i}</option>);
    }
    return elements;
  }

  render() {
    const {
      animate, item, faqBlock,
    } = this.props;
    const settings = {
      dots: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      autoplaySpeed: 5000,
      pauseOnHover: false,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
    };

    const { quantity } = this.state;
    // const filterServiceEvent = item.buttons[1]
    const filteredButton = item?.buttons.find(x => x?.value?.name.includes('simplyBookID'));
    const bookAWorkshopText = getNameFromButtonBlock(item?.buttons, 'sg');
    const id = filteredButton ? filteredButton.value.name.split('-')[1] : 0;
    const eventListFiltered = id && this.props.eventList ? this.props.eventList[id] : [];
    const faqBlockFiltered = _.find(faqBlock, itemFAQ => itemFAQ.value.header && itemFAQ.value.header.header_text === `simplyBookID-${id}`);
    return (
      <div className={`workshop__detail_V2 ${animate.detail}`}>
        <div className="workshop__detail__layout pb-5">
          <div className="row">
            <div className="col-7">
              <Slider {...settings}>
                {item && item.videos && item.videos.length > 0 && item.videos[0].value ? (
                  <div>
                    <div className="workshop__detail__video">
                      <Player
                        playsInline
                        loop
                        autobuffer
                        disablePauseOnClick
                        src={item ? item.videos[0].value.video : ''}
                        poster={item && item.videos[0].value.placeholder ? item.videos[0].value.placeholder : ''}
                      >
                        <BigPlayButton position="center" />
                      </Player>
                    </div>
                  </div>
                ) : ''}
                {item && item.images && item.images.map(ele => ele.value).map(image => (
                  <div className="workshop__detail__images__item">
                    <img loading="lazy" src={image.image} alt={getAltImageV2(image)} className="img-fluid" />
                  </div>
                ))}
              </Slider>
            </div>
            <div className="col-5">
              <div className="workshop__detail__information">
                <div className="information__header">
                  <div className="information__header__title">
                    {/* {item ? item.header.header_text : ''} */}
                    {eventListFiltered ? eventListFiltered?.name : ''}
                  </div>
                  <div className="information__header__price">
                    <p style={{ fontSize: 20 }}>{item ? item.price : ''}</p>
                    <p style={{ fontSize: 17 }}>{item ? item.price_text : ''}</p>
                  </div>
                </div>
                <div className="workshop__detail__about__content" dangerouslySetInnerHTML={{ __html: item ? item.text : '' }} />
                <div className="information__body">
                  <div className="information__body__booking d-flex flex-column justify-content-between">
                    {item && (
                      <div className="d-flex flex-rows justify-content-between">
                        {eventListFiltered && eventListFiltered.bookings_limit > 1
                          ? (
                            <div className="wrapper-select">
                              <img src={amountPersonIC} alt="icon" />
                              <Input
                                type="select"
                                className="select-amount-person"
                                name="country"
                                onChange={this.onChangeQuantity}
                                value={quantity || 1}
                              >
                                {this.createElements(eventListFiltered.bookings_limit, eventListFiltered)}
                              </Input>
                            </div>
                          ) : ''}
                        <button type="button" className="d-flex justify-content-center align-items-center booking__btn" onClick={() => this.props.directBookingPage(1, eventListFiltered.id, quantity || 1)}>
                          {bookAWorkshopText ? bookAWorkshopText.toUpperCase() : 'BOOK A WORKSHOP'}
                        </button>
                        {/* <button type="button" className="d-flex justify-content-center align-items-center booking__btn" onClick={() => window.open('https://maison21g.simplybook.asia/v2/#book')}>
                          {bookAWorkshopText ? bookAWorkshopText.toUpperCase() : 'BOOK A WORKSHOP'}
                        </button> */}
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="workshop__detail__benefit">
            <div className="workshop__detail__benefit__title">
              {this.props.youJourneyText || 'Your workshop journey'}
            </div>
            {item && (
              <div className="d-flex flex-rows justify-content-between workshop__detail__benefit_faq" style={{ marginRight: isMobile ? 8 : 0 }}>
                {item.what_you_gets.map(ele => ele.value).map(offer => (
                  <div className="d-flex flex-column workshop__detail__benefit__item">
                    <div className="workshop__detail__benefit__item__icon">
                      <img loading="lazy" src={offer.image.image} alt="" className="img-fluid" />
                    </div>
                    <div className="workshop__detail__benefit__item__title">
                      {offer.title}
                    </div>
                    <div className="workshop__detail__benefit__item__content">
                      {offer.text}
                    </div>
                  </div>
                ))}
              </div>
            )}
          </div>
          {faqBlockFiltered ? (
            <div className="div-faq">
              <div className="div-content-faq">
                <div
                  style={{
                    width: '100%',
                    height: '1px',
                    background: '#e2e2e2',
                  }}
                />
                {
                  _.map(faqBlockFiltered.value ? faqBlockFiltered.value.faq : [], d => (
                    <RecommendItem title={d.value.header.header_text} description={d.value.paragraph} />
                  ))
                }
                {/* <div style={{ height: isBrowser ? '200px' : '120px' }} /> */}
              </div>
            </div>
          ) : ''}

        </div>
        {
          this.state.listReviews?.length > 0 && (
            <Reviews
              isWorkShops
              data={this.state.listReviews}
              cms={this.props.cms}
              buttonBlocks={this.props.buttonBlock}
              updateReviewData={() => {}}
            />
          )
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  eventList: state.bookWorkshop.eventList,
  cms: state.cms,
});

export default connect(mapStateToProps, null)(WorkshopDetailV2);
