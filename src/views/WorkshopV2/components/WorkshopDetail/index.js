/* eslint-disable react/no-danger */
import React, { Component } from 'react';
import {
  Player, BigPlayButton,
} from 'video-react';
import './styles.scss';
import { Link } from 'react-router-dom';
import { getAltImageV2 } from '../../../../Redux/Helpers';

class WorkshopDetail extends Component {
  render() {
    const { animate, hideDetails, item } = this.props;

    return (
      <div className={`workshop__detail ${animate.detail}`}>
        <div className="workshop__detail__back" onClick={hideDetails}>
          <i className="fa fa-arrow-left mr-2 img-fluid" />
          {/* <img src="https://s.scentdesigner.stage.23c.se/media/images/arrow.original.png" alt="" className="mr-2 img-fluid" /> */}
          Back
        </div>
        <div className="workshop__detail__layout pb-5">
          <div className="row">
            <div className="col-7">
              <div className="workshop__detail__video">
                {/* <video
                  style={{
                    width: '100%',
                    objectFit: 'cover',
                  }}
                  muted="true"
                  playsinline="true"
                  preload="auto"
                  loop
                  poster={item ? item.videos[0].value.placeholder : ''}
                  controls
                >
                  <source src={item ? item.videos[0].value.video : ''} />
                </video> */}
                <Player
                  playsInline
                  loop
                  muted
                  autobuffer
                  disablePauseOnClick
                  src={item ? item.videos[0].value.video : ''}
                  poster={item ? item.videos[0].value.placeholder : ''}
                >
                  <BigPlayButton position="center" />
                </Player>
              </div>
            </div>
            <div className="col-5">
              <div className="workshop__detail__information">
                <div className="information__header d-flex flex-rows justify-content-between align-items-end">
                  <div className="information__header__title">
                    {item ? item.header.header_text : ''}
                  </div>
                  <div className="information__header__price">
                    {item ? item.price : ''}
                  </div>
                </div>
                <div className="information__body">
                  <div className="information__body__price">
                    Price:
                  </div>
                  <div className="information__body__content">
                    <strong>
                      {item ? item.price : ''}
                    </strong>
                    <br />
                    {item ? item.price_text : ''}
                  </div>
                  <div className="information__body__booking d-flex flex-column justify-content-between">
                    <p>
                      Book now:
                    </p>
                    {item && (
                      <div className="d-flex flex-rows justify-content-between">
                        {item.buttons.length == 2 ? (
                          <React.Fragment>
                            <a href={item.buttons[0].value.link}>
                              <button type="button" className="d-flex justify-content-between align-items-center booking__btn" onClick={this.onClick}>
                                <img loading="lazy" src="https://s.scentdesigner.stage.23c.se/media/images/singapore.original.png" alt="" className="img-fluid" />
                                <div className="d-flex justify-content-center">
                                  {item.buttons[0].value.text.toUpperCase()}
                                </div>
                              </button>
                            </a>
                            <a href={item.buttons[1].value.link}>
                              <button type="button" className="d-flex justify-content-between align-items-center booking__btn" onClick={this.onClick}>
                                <img loading="lazy" src="https://s.scentdesigner.stage.23c.se/media/images/australia.original.png" alt="" className="img-fluid" />
                                <div className="d-flex justify-content-center">
                                  {item.buttons[1].value.text.toUpperCase()}
                                </div>
                              </button>
                            </a>

                          </React.Fragment>
                        ) : (
                          <a href={item.buttons[0].value.link}>
                            <button type="button" className="d-flex justify-content-between align-items-center booking__btn" onClick={this.onClick}>
                              <img loading="lazy" src="https://s.scentdesigner.stage.23c.se/media/images/singapore.original.png" alt="" className="img-fluid" />
                              <div className="d-flex justify-content-center">
                                {item.buttons[0].value.text.toUpperCase()}
                              </div>
                            </button>
                          </a>
                        )
                        }
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row align-items-end mt-5">
            <div className="col-7">
              <div className="ml-5 pr-3">
                <div className="workshop__detail__about">
                  <div className="workshop__detail__about__title">
                    About Atelier:
                  </div>
                  <div className="workshop__detail__about__content" dangerouslySetInnerHTML={{ __html: item ? item.text : '' }}>
                    {/* <strong>
                      A unique sensorial experience to tighten team spirit and strengthen your corporate image!
                    </strong>
                    <br />
                    <br />
                    Dare to make the scent of your company with the creativity of your employees
                    at the venue of your choice. You can bring your team to the beautiful
                    Maison 21G Atelier in Duxton or we can come with all our creative tools to
                    the place of your choice. An exceptional experience guaranteed! */}
                  </div>
                </div>
                <div className="workshop__detail__contact">
                  <div className="workshop__detail__contact__title">
                    Contact:
                  </div>
                  <div className="workshop__detail__contact__content">
                    {item ? item.email : ''}
                    {' '}
                    |
                    {' '}
                    {item ? item.phone : ''}
                  </div>
                </div>
                <div className="workshop__detail__benefit">
                  <div className="workshop__detail__benefit__title">
                    What you get:
                  </div>
                  {item && (
                    <div className="d-flex flex-rows justify-content-between">
                      {item.what_you_gets.map(ele => ele.value).map(offer => (
                        <div className="d-flex flex-column justify-content-between workshop__detail__benefit__item">
                          <div className="workshop__detail__benefit__item__icon">
                            <img loading="lazy" src={offer.image.image} alt="" className="img-fluid" />
                          </div>
                          <div className="workshop__detail__benefit__item__title">
                            {offer.title}
                          </div>
                          <div className="workshop__detail__benefit__item__content">
                            {offer.text}
                          </div>
                        </div>
                      ))}
                    </div>
                  )}
                </div>
              </div>
            </div>
            <div className="col-5">
              {item && (
                <div className="workshop__detail__images mr-3">
                  {item.images.map(ele => ele.value).map(image => (
                    <div className="workshop__detail__images__item">
                      <img loading="lazy" src={image.image} alt={getAltImageV2(image)} className="img-fluid" />
                    </div>
                  ))}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default WorkshopDetail;
