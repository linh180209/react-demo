import React, { Component } from 'react';
import classnames from 'classnames';
import './styles.scss';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import icNext from '../../../../image/icon/ic-next-white.svg';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import LazyImage from '../../../../components/LazyImage';
import { isMobile } from '../../../../DetectScreen';

class WorkshopListV2 extends Component {
  handleClick = (id) => {
    const { showDetails } = this.props;
    showDetails(id);
  }

  itemRender = (index, item) => (
    <div className={`item${index + 1} item-list ${index === this.props.currentMenuText ? 'active-item' : ''}`}>
      <div key={index} className="workshop__list__item" onClick={() => { this.handleClick(index); }}>
        <div className="__image">
          {item && item.videos && item.videos.length > 0
            ? <LazyImage src={item && item.videos && item.videos.length > 0 ? item.videos[0].value.placeholder : ''} alt="workshop_item" className="img-fluid" />
            : item.images && item.images.length > 0
              ? <LazyImage src={item.images[0].value.image} alt="workshop_item" className="img-fluid" />
              : ''}

          {/* <img src={item ? item.videos[0].value.placeholder : ''} alt="workshop_item" className="img-fluid" /> */}
        </div>
        <div className={classnames(this.props.isHomePage ? 'hover-text' : 'hiden', (index !== 0 ? 'small' : ''))}>
          <div className="text">
            <span>
              {getNameFromButtonBlock(this.props.buttonBlocks, 'book_now')}
            </span>
            <img src={icNext} alt="icon" />
          </div>
        </div>
        {
          !this.props.isHomePage && (
            <div className="__text-des div-col">
              <div className="bg-text" />
              <div className="__title">
                {item ? item.short_text : ''}
              </div>
            </div>
          )
        }
      </div>
    </div>
  )

  render() {
    const {
      animate, items, isHomePage,
    } = this.props;
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      // autoplay: true,
      arrows: !isMobile,
      autoplaySpeed: 6000,
      pauseOnHover: false,
    };
    return (
      <div className={`workshop__list_v3 ${animate && isMobile ? animate.list : ''} ${isHomePage && isMobile ? 'workshop-slider' : ''}`}>
        {
          isMobile && isHomePage ? (
            <div className="slider-item">
              <Slider {...settings}>
                {
                  items.map((item, index) => (
                    this.itemRender(index, item)
                  ))
                }
              </Slider>
            </div>
          ) : (
            <div className="grid-container-v2">
              {items.map((item, index) => (
                this.itemRender(index, item)
              ))}
            </div>
          )
        }
      </div>
    );
  }
}

export default WorkshopListV2;
