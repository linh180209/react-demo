import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';
import {
  Player, BigPlayButton,
} from 'video-react';
import { getAltImageV2 } from '../../../../Redux/Helpers';
import './styles.scss';
import icPlay from '../../../../image/icon/icPlay.png';

class WorkshopDetailMobile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectData: {},
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    const { item } = this.props;
    if (item && item.videos[0]) {
      this.setState({ selectData: item.videos[0] });
    } else if (item) {
      this.setState({ selectData: item.images[0] });
    }
  }

  render() {
    const { animate, hideDetails, item } = this.props;
    const { selectData } = this.state;
    const imageSliderSettings = {
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 3,
      arrows: false,
    };

    const featureSliderSettings = {
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
    };
    let listImageVideo = [];
    if (item && item.videos[0]) {
      listImageVideo.push(item.videos[0]);
    }
    listImageVideo = listImageVideo.concat(item.images);
    listImageVideo = _.filter(listImageVideo, x => x.id !== selectData.id);
    console.log('listImageVideo', listImageVideo);
    return (
      <div className="mobi--workshop__detail">
        <div className="mobi--workshop__detail__back" onClick={hideDetails}>
          <i className="fa fa-arrow-left mr-2 img-fluid" />
          {/* <img src="https://s.scentdesigner.stage.23c.se/media/images/arrow.original.png" alt="" className="mr-2 img-fluid" /> */}
          Back
        </div>
        <div className={`mobi--workshop__detail__banner ${animate.detailMobie[0]}`}>
          {
            !_.isEmpty(selectData) && selectData.type === 'video' ? (
              <video
                style={{
                  width: '100%',
                  objectFit: 'cover',
                }}
                muted="true"
                playsinline="true"
                preload="auto"
                loop
                poster={selectData.value.placeholder}
                controls
              >
                <source src={selectData.value.video} />
              </video>
            ) : (!_.isEmpty(selectData) ? (
              <img loading="lazy" src={selectData.value.image} alt={getAltImageV2(selectData.value)} />
            ) : null)
          }

        </div>
        <div className="container">
          <div className="row mt-2">
            <div className="col-12">
              {item && (
                <div className="slider-images">
                  <Slider {...imageSliderSettings}>
                    {_.map(listImageVideo, (d, index) => (
                      <div className="p-1" onClick={() => this.setState({ selectData: d })}>
                        <img src={d.type === 'video' ? d.value.placeholder : d.value.image} alt={getAltImageV2(d.value)} className="img-fluid" />
                        <img className={d.type === 'video' ? 'play-icon' : 'hidden'} src={icPlay} alt="play" />
                      </div>
                    ))}
                    {/* {item.images.map(ele => ele.value).map(image => (
                      <div className="p-1">
                        <img src={image.image} alt="" className="img-fluid" />
                      </div>
                    ))} */}
                  </Slider>
                </div>
              )}
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <div className={`mobi--workshop__detail__info mt-3 mb-5 ${animate.detailMobie[0]}`}>
                <h3>
                  {item ? item.header.header_text : ''}
                </h3>
                <p className="__price">
                  {item ? item.price : ''}
                </p>
                <p className="__content">
                  {item ? item.price_text : ''}
                </p>
              </div>
              <div className="mobi--workshop__detail__about mb-5">
                <h4>about atelier:</h4>
                <div className="__content" dangerouslySetInnerHTML={{ __html: item ? item.text : '' }}>
                  {/* Dare to make the scent of your company with the creativity of your employees at the venue of your choice. You can bring your team to the beautiful Maison 21G Atelier in Duxton or we can come with all our creative tools to the place of your choice. An exceptional experience guaranteed! */}
                </div>
              </div>
              <div className="mobi--workshop__detail__contact">
                <h4>contact us at:</h4>
                <p className="__content">
                  {item ? item.email : ''}
                  {' '}
                  |
                  {' '}
                  {item ? item.phone : ''}
                </p>
              </div>
            </div>
          </div>
          <div className="row mb-5">
            <div className="col-12">
              <div className="mobi--workshop__detail__benefit">
                <div className="mobi--workshop__detail__benefit__title">
                  What you get:
                </div>
                {item && (
                  <Slider {...featureSliderSettings} infinite={item.what_you_gets.length > 2}>
                    {item.what_you_gets.map(ele => ele.value).map((offer, index) => (
                      <div key={index}>
                        <div className="mobi--workshop__detail__benefit__item">
                          <div className="mobi--workshop__detail__benefit__item__icon">
                            <img src={offer.image.image} alt="" className="img-fluid" />
                          </div>
                          <div className="mobi--workshop__detail__benefit__item__title">
                            {offer.title}
                          </div>
                          <div className="mobi--workshop__detail__benefit__item__content">
                            {offer.text}
                          </div>
                        </div>
                      </div>
                    ))}
                  </Slider>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="mobi--workshop__detail__bookNow">
          <div className="container">
            <div className="row">
              <div className="col-12">
                <p className="mb-1">BOOK NOW:</p>
                {item && (
                  <div className="d-flex flex-rows justify-content-between">
                    {item.buttons.length == 2 ? (
                      <React.Fragment>
                        <a href={item.buttons[0].value.link}>
                          <button type="button" className="btn">
                            <img loading="lazy" src="https://s.scentdesigner.stage.23c.se/media/images/singapore.original.png" alt="" className="img-fluid mr-2" />
                            <span>Singapore</span>
                          </button>
                        </a>
                        <a href={item.buttons[1].value.link}>
                          <button type="button" className="btn mr-5">
                            <img loading="lazy" src="https://s.scentdesigner.stage.23c.se/media/images/australia.original.png" alt="" className="img-fluid mr-2" />
                            <span>
                              {item.buttons[1].value.text.toUpperCase()}
                            </span>
                          </button>
                        </a>
                      </React.Fragment>
                    ) : (
                      <a href={item.buttons[0].value.link}>
                        <button type="button" className="btn">
                          <img loading="lazy" src="https://s.scentdesigner.stage.23c.se/media/images/singapore.original.png" alt="" className="img-fluid mr-2" />
                          <span>
                            {item.buttons[0].value.text.toUpperCase()}
                          </span>
                        </button>
                      </a>
                    )
                    }
                  </div>
                )}
                {/* <div className="d-flex align-items-center justify-content-start">
                  <button type="button" className="btn">
                    <img src="https://s.scentdesigner.stage.23c.se/media/images/singapore.original.png" alt="" className="img-fluid mr-2" />
                    <span>Singapore</span>
                  </button>
                  <button type="button" className="btn ml-2">
                    <img src="https://s.scentdesigner.stage.23c.se/media/images/australia.original.png" alt="" className="img-fluid mr-2" />
                    <span>Sydney</span>
                  </button>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default WorkshopDetailMobile;
