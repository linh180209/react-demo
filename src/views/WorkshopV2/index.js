import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import MetaTags from 'react-meta-tags';

import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import {
  scrollTop, fetchCMSHomepage, getSEOFromCms, googleAnalitycs, getNameFromButtonBlock, generateHreflang, generateUrlWeb, removeLinkHreflang,
  fetchTokenSimplyBook,
  setPrerenderReady,
} from '../../Redux/Helpers/index';
import addCmsRedux from '../../Redux/Actions/cms';
import WorkshopDetailV2 from './components/WorkshopDetailV2';
import WorkshopDetailMobileV2 from './components/WorkshopDetailMobileV2';
import { getEventListSimplyBook } from '../../Redux/Actions/bookWorkshop';
import loadingPage from '../../Redux/Actions/loading';

import { isMobile } from '../../DetectScreen';
import WorkshopListV2 from './components/WorkshopListV2';
import FooterV2 from '../../views2/footer';
import auth from '../../Redux/Helpers/auth';

const getCms = (workshopCMS) => {
  const objectReturn = {
    headerBlock: '',
    textBlock: '',
    imageBlock: {},
    seo: undefined,
    paragraphBlock: '',
    ateliersBlock: [],
    faqBlocks: [],
  };
  if (!_.isEmpty(workshopCMS)) {
    const seo = getSEOFromCms(workshopCMS);
    const { body } = workshopCMS;
    if (body) {
      const headerBlock = _.find(body, x => x.type === 'header_block').value;
      const textBlock = _.find(body, x => x.type === 'text_block').value;
      const imageBlock = _.find(body, x => x.type === 'image_block').value;
      const buttonBlock = _.filter(body, x => x.type === 'button_block');
      const paragraphBlock = _.find(body, x => x.type === 'paragraph_block').value;
      const ateliersBlock = _.filter(body, x => x.type === 'ateliers_block').map(ele => ele.value).map(ele => ele[0]).map(ele => ele.value);
      const faqBlocks = _.filter(body, x => x.type === 'faq_block');
      _.assign(objectReturn, {
        headerBlock, textBlock, seo, imageBlock, paragraphBlock, ateliersBlock, buttonBlock, faqBlocks,
      });
    }
  }
  return objectReturn;
};

class WorkshopV2 extends Component {
  constructor(props) {
    super(props);

    this.state = {
      seo: undefined,
      animate: {},
      minHeight: window.innerWidth * 0.64,
      showContactAndFooter: true,
      visibleDetailMobile: false,
      headerBlock: '',
      textBlock: '',
      imageBlock: {},
      paragraphBlock: '',
      ateliersBlock: [],
      buttonBlock: [],
      faqBlocks: [],
      currentWorkshopIndex: 0,
      detailInit: true,
      currentMenuText: '',
      showingDetail: false,
    };
  }

  componentDidMount = () => {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/perfume-workshop');
    // fetchTokenSimplyBook();
    this.fetchDataInit();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { match } = nextProps;
    if (match && match.params && match.params.workshop && match.params.workshop !== prevState.workshop) {
      googleAnalitycs(window.location.pathname);
    }
    if (match.params.workshop !== prevState.workshop) {
      return { ...prevState, workshop: match.params.workshop };
    }
    return { ...prevState };
  }

  fetchDataInit = async () => {
    fetchTokenSimplyBook().then(res => (res ? this.fetchDataEventList() : ''));
    const { cms } = this.props;
    const workshopCMS = _.find(cms, x => x.title === 'workshop v3');
    if (!workshopCMS) {
      this.props.loadingPage(true);
      const cmsData = await fetchCMSHomepage('workshop-v3');
      this.props.addCmsRedux(cmsData);
      const dataFromStore = getCms(cmsData);
      if (dataFromStore.ateliersBlock.length > 5) {
        _.assign(dataFromStore, { minHeight: window.innerWidth * 0.64 });
      } else {
        _.assign(dataFromStore, { minHeight: window.innerWidth * 0.34 });
      }
      this.setState(dataFromStore);
    } else {
      const dataFromStore = getCms(workshopCMS);
      if (dataFromStore.ateliersBlock.length > 5) {
        _.assign(dataFromStore, { minHeight: window.innerWidth * 0.64 });
      } else {
        _.assign(dataFromStore, { minHeight: window.innerWidth * 0.34 });
      }
      this.setState(dataFromStore);
    }
  }

  fetchDataEventList = async () => {
    this.props.getEventListSimplyBook();
    this.props.loadingPage(false);
  }

  gotoWithUrl = (id) => {
    const ele = this.state.ateliersBlock[id];
    const url = ele.header.header_text.toLowerCase().replace(/ /g, '-');
    this.props.history.push(generateUrlWeb(`/perfume-workshop/${url}`));
  }

  gotoBack = () => {
    this.props.history.push(generateUrlWeb('/perfume-workshop'));
  }

  showDetails = (id = 0) => {
    if (!isMobile) {
      this.setState({
        animate: {
          list: 'slide-out-right',
          detail: 'slide-in-left',
        },
        minHeight: 1200,
        currentWorkshopIndex: id,
      });
    } else {
      this.setState({
        animate: {
          detailMobie: ['scale-in-bl'],
        },
        showContactAndFooter: false,
        visibleDetailMobile: true,
        currentWorkshopIndex: id,
      });
    }
  }

  hideDetails = () => {
    const { ateliersBlock } = this.state;
    if (!isMobile) {
      this.setState({
        animate: {
          list: 'slide-in-right',
          detail: 'slide-out-left',
        },
        minHeight: ateliersBlock.length > 4 ? window.innerWidth * 0.64 : window.innerWidth * 0.34,
      });
    } else {
      this.setState({
        animate: {
          banner: 'scale-in-tr',
          list: '',
        },
        showContactAndFooter: true,
        visibleDetailMobile: false,

      });
    }
  }

  directBookingPage = (id, idService, count) => {
    const url = generateUrlWeb(`${this.props.location.pathname.replace(/\/$/, '')}`);
    if (!idService) {
      this.props.history.push(url);
    } else { this.props.history.push(`${url}/service/${idService}/count/${count}`); }
  }

  renderHtmlWeb() {
    const {
      ateliersBlock,
      animate,
      minHeight,
      visibleDetailMobile,
      currentWorkshopIndex,
      detailInit,
      currentMenuText,
      faqBlocks,
      showingDetail,
      buttonBlock,
    } = this.state;
    const youJourneyText = getNameFromButtonBlock(buttonBlock, 'your_workshop_journey');
    const menuText = this.props.match.params.workshop ? this.props.match.params.workshop : '';
    const menuIndex = ateliersBlock.length ? ateliersBlock.findIndex(atelier => atelier.header.header_text.replace('The ', '').replace(' & ', ' ').replace(' ', '-').toLowerCase() === menuText.toLowerCase()) : -1;
    if (menuText) {
      if (currentMenuText && (menuText !== currentMenuText)) {
        this.setState({
          detailInit: true,
        });
      }

      if (detailInit && (menuIndex !== -1) && ateliersBlock.length) {
        this.hideDetails();
        this.setState({
          detailInit: false,
          currentMenuText: menuText,
        });
        if (menuIndex !== -1) {
          this.setState({
            showingDetail: true,
          });
          this.showDetails(menuIndex);
        }
      }
    } else if (showingDetail) {
      this.setState({
        showingDetail: false,
        currentMenuText: '',
        detailInit: true,
      });
      this.hideDetails();
    }
    return (
      <div className="workshop-v3 workshop">
        {!visibleDetailMobile ? (
          <React.Fragment>
            {/* <WorkshopBanner animate={animate} imageBlock={imageBlock} headerText={headerBlock ? headerBlock.header_text : ''} textBlock={textBlock} /> */}
            <div className="container-fluid custom">
              <div className="workshop__parent2" style={{ minHeight }}>
                <WorkshopListV2 animate={animate} items={ateliersBlock} showDetails={this.gotoWithUrl} currentMenuText={menuIndex} />
                {!isMobile && ateliersBlock.length > 0 && (
                  <div className="workshop__child2">
                    <WorkshopDetailV2
                      buttonBlock={buttonBlock}
                      youJourneyText={youJourneyText}
                      animate={animate}
                      directBookingPage={this.directBookingPage}
                      item={ateliersBlock[currentWorkshopIndex]}
                      hideDetails={this.gotoBack}
                      faqBlock={faqBlocks || []}
                    />
                  </div>
                )}
              </div>
            </div>
          </React.Fragment>
        ) : (
          isMobile && (
          <WorkshopDetailMobileV2
            buttonBlock={buttonBlock}
            youJourneyText={youJourneyText}
            item={ateliersBlock[currentWorkshopIndex]}
            directBookingPage={this.directBookingPage}
            animate={animate}
            hideDetails={this.gotoBack}
            ateliersBlock={ateliersBlock}
            showDetails={this.gotoWithUrl}
            faqBlock={faqBlocks || []}
            currentMenuText={menuIndex}
          />
          )
        )}
        {/* {showContactAndFooter && <WorkshopContact paragraphBlock={paragraphBlock} />} */}
      </div>
    );
  }

  render() {
    const {
      buttonBlock,
      showContactAndFooter,
      seo,
    } = this.state;

    const menuText = this.props.match.params.workshop ? this.props.match.params.workshop : '';
    const seoWorkshop = menuText ? getNameFromButtonBlock(buttonBlock, `title ${menuText}`) : '';
    const seoTitle = _.isEmpty(menuText) ? (seo ? seo.seoTitle : '') : `Masterclass with perfume experts | Maison 21G ${auth.getCountryName()}`;
    const seoDescription = _.isEmpty(menuText) ? (seo ? seo.seoKeywords : '') : getNameFromButtonBlock(buttonBlock, `description ${menuText}`);
    return (
      <React.Fragment>
        <MetaTags>
          <title>
            {seoTitle}
          </title>
          <meta name="description" content={seoDescription} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, `/perfume-workshop/${menuText}`)}
        </MetaTags>
        {/* <HeaderHomePage /> */}
        <HeaderHomePageV3 />
        {/* { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        {this.renderHtmlWeb()}
        {showContactAndFooter && <FooterV2 />}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  cms: state.cms,
  countries: state.countries,
  // showAskRegion: state.showAskRegion,
});

const mapDispatchToProps = {
  addCmsRedux,
  getEventListSimplyBook,
  loadingPage,
};

WorkshopV2.defaultProps = {
  match: {
    params: {
      workshop: '',
    },
  },
};

WorkshopV2.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      workshop: PropTypes.string,
    }),
  }),
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WorkshopV2));
