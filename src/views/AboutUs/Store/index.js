/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import _ from 'lodash';
// import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import ImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css';
import '../../../styles/address-store.scss';

import icLocation from '../../../image/icon/icLocation.svg';
import icEmail from '../../../image/icon/icEmail.svg';
import icPhone from '../../../image/icon/icPhone.svg';
import icTime from '../../../image/icon/icTime.svg';
// import addCmsRedux from '../../../Redux/Actions/cms';
import {
  fetchCMSHomepage, scrollTop, getSEOFromCms, googleAnalitycs,
} from '../../../Redux/Helpers';
import { isMobile, isTablet } from '../../../DetectScreen';

// const getCms = (data) => {
//   if (data) {
//     const storeBlock = _.filter(data.body, x => x.type === 'stores_block');
//     const labelTextBlock = _.filter(data.body, x => x.type === 'label_and_text_block');
//     const seo = getSEOFromCms(data);
//     return { storeBlock, seo, labelTextBlock };
//   }
//   return [];
// };

class AddressStore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // storeBlock: [],
      addressSelection: undefined,
      // labelTextBlock: [],
    };
  }

  componentDidMount() {
    // scrollTop();
    googleAnalitycs(window.location.pathname);
    // this.fetchDataInit();
  }

  // fetchDataInit = async () => {
  //   const { cms } = this.props;
  //   const storeCms = _.find(cms, x => x.title === 'Store location');
  //   if (!storeCms) {
  //     const cmsData = await fetchCMSHomepage('store-location');
  //     this.props.addCmsRedux(cmsData);
  //     const { storeBlock, seo, labelTextBlock } = getCms(cmsData);
  //     this.setState({ storeBlock, seo, labelTextBlock });
  //   } else {
  //     const { storeBlock, seo, labelTextBlock } = getCms(storeCms);
  //     this.setState({ storeBlock, seo, labelTextBlock });
  //   }
  // }

  onClickStore = (d) => {
    // if (d.name.toLowerCase() === 'singapore') {
    //   this.props.history.push('/store/Singapore');
    // } else {
    //   this.props.history.push('/store/Australia');
    // }
    this.setState({ addressSelection: d });
  };

  render() {
    const {
      addressSelection,
    } = this.state;
    const { storeBlock } = this.props;
    if (storeBlock.length === 0) {
      return null;
    }
    const listAddress = _.map(storeBlock, x => ({
      name: x.country,
      images: _.map(x.images, d => (
        {
          original: d.value.image,
          thumbnail: d.value.image,
        }
      )),
      imageBg: x.image_background.image,
      nameStore: x.name,
      address: x.address,
      email: x.email,
      phone: x.phone,
      time: x.open_hour,
    }));

    // const isAtSingapore = !window.location.pathname.includes('Australia');
    // const titleSeo = isAtSingapore ? _.find(labelTextBlock, x => x.value.label === 'seo-title-singapore') : _.find(labelTextBlock, x => x.value.label === 'seo-title-australia');
    // const descriptionSeo = isAtSingapore ? _.find(labelTextBlock, x => x.value.label === 'seo-search-singapore') : _.find(labelTextBlock, x => x.value.label === 'seo-search-australia');
    const addressChoose = addressSelection || listAddress[0];
    const htmlWeb = (
      <div className="div-address-store" id="our-stores">
        <div className="div-map">
          <img loading="lazy" src={addressChoose.imageBg} alt="img2" />
        </div>
        <div className="div-info">
          <div className="div-cart div-col">
            <div className="div-header div-row">
              {
                _.map(listAddress, d => (
                  <button
                    type="button"
                    className={`${addressChoose.name === d.name ? 'active' : ''}`}
                    style={{ width: `${100 / listAddress.length}%` }}
                    onClick={() => this.onClickStore(d)}
                  >
                    {d.name}
                  </button>
                ))
              }
            </div>
            <div className="div-image">
              <ImageGallery
                items={addressChoose.images}
                showPlayButton={false}
                showFullscreenButton={false}
                showThumbnails={false}
                autoPlay
                slideInterval={5000}
              />
            </div>
            <div className="div-text div-col justify-between">
              <div className="div-location div-row">
                <div className="div-image div-col justify-center items-center">
                  <img loading="lazy" src={icLocation} alt="location" style={{ width: '25px' }} />
                </div>
                <div className="div-col">
                  <b>
                    {addressChoose.nameStore}
                  </b>
                  <span>
                    {addressChoose.address}
                  </span>
                </div>
              </div>
              <div className="div-row">
                <div className="div-image div-col justify-center items-center">
                  <img src={icEmail} alt="email" />
                </div>
                <a href={`mailto:${addressChoose.email}`}>
                  {addressChoose.email}
                </a>
                {/* <span>
                  {addressChoose.email}
                </span> */}
              </div>
              <div className="div-row">
                <div className="div-image div-col justify-center items-center">
                  <img src={icPhone} alt="phone" />
                </div>
                <a href={`tel:${addressChoose.phone}`}>
                  {addressChoose.phone}
                </a>
                {/* <span>
                  {addressChoose.phone}
                </span> */}
              </div>
              <div className="div-row">
                <div className="div-image div-col justify-center items-center">
                  <img src={icTime} alt="time" />
                </div>

                <span>
                  {addressChoose.time}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );

    const htmlMobile = (
      <div className="div-address-store-mobile p-0">
        <div className="div-map">
          <img loading="lazy" src={addressChoose.imageBg} alt="img2" />
        </div>
        <div className="div-info">
          <div className="div-cart div-col">
            <div className="div-header div-row">
              {
                _.map(listAddress, d => (
                  <button
                    type="button"
                    className={`${addressChoose.name === d.name ? 'active' : ''}`}
                    style={{ width: `${100 / listAddress.length}%` }}
                    onClick={() => this.onClickStore(d)}
                  >
                    {d.name}
                  </button>
                ))
              }
            </div>
            <div className="div-image">
              <ImageGallery
                items={addressChoose.images}
                showPlayButton={false}
                showFullscreenButton={false}
                showThumbnails={false}
                autoPlay
                slideInterval={5000}
              />
            </div>
            <div className="div-text div-col justify-between">
              <div className="div-location div-row">
                <div className="div-image div-col justify-center items-center">
                  <img src={icLocation} alt="location" style={{ width: '20px' }} />
                </div>
                <div className="div-col">
                  <b>
                    {addressChoose.nameStore}
                  </b>
                  <span>
                    {addressChoose.address}
                  </span>
                </div>
              </div>
              <div className="div-row">
                <div className="div-image div-col justify-center items-center">
                  <img src={icEmail} alt="email" />
                </div>
                <a href={`mailto:${addressChoose.email}`}>
                  {addressChoose.email}
                </a>
              </div>
              <div className="div-row">
                <div className="div-image div-col justify-center items-center">
                  <img src={icPhone} alt="phone" />
                </div>
                <span>
                  {addressChoose.phone}
                </span>
              </div>
              <div className="div-row">
                <div className="div-image div-col justify-center items-center">
                  <img src={icTime} alt="time" />
                </div>

                <span>
                  {addressChoose.time}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );

    return (
      // <div>ABCDEF</div>
      <div>
        {isMobile || isTablet ? htmlMobile : htmlWeb}
      </div>
    );
  }
}

// AddressStore.propTypes = {
//   cms: PropTypes.arrayOf(PropTypes.object).isRequired,
//   addCmsRedux: PropTypes.func.isRequired,
//   history: PropTypes.shape({
//     push: PropTypes.func,
//   }).isRequired,
// };
// function mapStateToProps(state) {
//   return {
//     cms: state.cms,
//   };
// }

// const mapDispatchToProps = {
//   addCmsRedux,
// };

// export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AddressStore));

export default withRouter(AddressStore);
