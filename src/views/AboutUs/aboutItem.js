import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactHtmlParser from 'react-html-parser';
import _ from 'lodash';
import { getAltImageV2 } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

class AboutItem extends Component {
  render() {
    const { isCenter, data } = this.props;
    const { value } = data;
    const { videos } = value || { videos: [] };
    const isCol = isTablet && isCenter;
    const type = isTablet ? 'tablet' : (isMobile ? 'phone' : 'web');
    const videoObject = _.find(videos, x => x.value.type === type);
    const isVideo = !!videoObject;
    const htmlMobile = (
      <div className="div-col justify-center  items-center relative">
        <img
          loading="lazy"
          style={{
            width: '100vw',
            height: '222px',
            objectFit: 'cover',
          }}
          src={value ? value.image.image : ''}
          alt={getAltImageV2(value ? value.image : undefined)}
        />
        <div
          style={{
            position: 'absolute',
            width: '80%',
            top: '0px',
            marginTop: '30px',
            zIndex: '1',
          }}
        >
          {isVideo && videoObject
            ? (
              <video
                id="myVideo"
                style={{
                  objectFit: 'cover',
                  width: '100%',
                  height: '100%',
                }}
                muted
                playsinline="true"
                autoPlay
                loop
                preload="auto"
                src={videoObject.value.video}
                poster={videoObject.value.placeholder}
              />
            ) : (<div />)
          }
        </div>
        <div style={{
          marginLeft: '10px',
          marginRight: '10px',
          marginBottom: '50px',
          textAlign: 'center',
          marginTop: '20px',
        }}
        >
          {
            ReactHtmlParser(value ? value.description : '')
          }
        </div>
      </div>
    );
    const mobileHtmlVideo = (
      <div className="div-col justify-center  items-center relative">
        <img
          loading="lazy"
          style={{
            position: 'absolute',
            width: '100vw',
            objectFit: 'cover',
            top: '0px',
            zIndex: '-1',
          }}
          src={value ? value.image.image : ''}
          alt={getAltImageV2(value ? value.image : undefined)}
        />
        <div
          style={{
            width: '80%',
            // height: '216px',
            marginTop: '30px',
          }}
        >
          {isVideo && videoObject
            ? (
              <video
                id="myVideo"
                style={{
                  objectFit: 'cover',
                  width: '100%',
                  height: '100%',
                }}
                muted
                playsinline="true"
                autoPlay
                loop
                preload="auto"
                src={videoObject.value.video}
                poster={videoObject.value.placeholder}
              />
            ) : (<div />)
          }
        </div>

        <div style={{
          marginLeft: '10px',
          marginRight: '10px',
          marginBottom: '50px',
          textAlign: 'center',
          marginTop: '20px',
        }}
        >
          {
            ReactHtmlParser(value ? value.description : '')
          }
        </div>
      </div>
    );

    if (isMobile && !isTablet) {
      return (
        <div>
          {/* {isCenter ? mobileHtmlVideo : htmlMobile} */}
          {htmlMobile}
        </div>
      );
    }
    return (
      <div
        className={isCol ? 'div-col justify-center  items-center relative' : 'div-row justify-center items-center relative w-100'}
      >
        <img
          loading="lazy"
          style={{
            width: '100vw',
            objectFit: 'cover',
            position: 'absolute',
            bottom: '0px',
            right: '0px',
            height: '100%',
          }}
          src={value ? value.image.image : ''}
          alt={getAltImageV2(value ? value.image : undefined)}
        />
        <div style={{
          flex: isCenter ? '0' : '1',
          position: 'relative',
        }}
        />
        <div
          style={{
            position: 'relative',
            flex: '1',
            marginTop: '100px',
            marginBottom: isCol ? (isVideo ? '20px' : '300px') : '100px',
          }}
          className="div-col justify-center  items-center"
        >
          <div style={{ width: '70%', textAlign: 'center' }} className="div-col justify-center  items-center">
            {
            ReactHtmlParser(value ? value.description : '')
          }
          </div>
          {/* <span style={{
            fontSize: '1.5rem',
            color: '#000000',
          }}
          >
          Why a “Maison”?
          </span>

          <span style={{
            color: '#3D3D3D',
          }}
          >
          21 grams, that’s the weight of your soul!
          </span>
          <span style={{
            color: '#000000',
            marginTop: '30px',
            marginLeft: '20%',
            marginRight: '20%',
          }}
          >
          A “maison” is the French word for a “house”, and we wish to be a House of Scent Designers. Everyone can feel at home and welcome with Maison 21G, whether you’re in our boutiques, with our partners, or online, so long as you’re willing to be bold and design with us. Here, anything is possible! And for the first time, no one is in the middle. It’s straight from us to you, with total transparency! We’re handing over control of the finest ingredients, tools, and our expert knowledge to allow you craft your signatures. Your scent is freshly made by you, for you, always!
          People cater the wine they drink, the clothes they wear, and the way they act for different occasions. Perfume should be the same. There’s just one rule in our house, and it’s very simple: be free, be independent, and release your creative spirit. Then, you can express who you truly are to the world!
          </span> */}
        </div>
        {
          isCenter ? (
            <div style={{
              position: 'relative',
              flex: '1',
              marginBottom: isCol ? '100px' : '0px',
            }}
            >
              {isVideo ? (
                <div
                  style={{
                    width: '80%',
                    // height: '216px',
                  }}
                >
                  <video
                    id="myVideo"
                    style={{
                      objectFit: 'cover',
                      width: '100%',
                      height: '100%',
                    }}
                    muted
                    playsinline="true"
                    autoPlay
                    loop
                    preload="auto"
                    src={videoObject.value.video}
                    poster={videoObject.value.placeholder}
                  />
                </div>
              ) : (
                <div style={{
                  position: 'relative',
                  flex: '1',
                  marginBottom: isCol ? '100px' : '0px',
                }}
                />
              )}
            </div>
          ) : (
            <div />
          )
        }

      </div>
    );
  }
}

AboutItem.propTypes = {
  isCenter: PropTypes.bool.isRequired,
  data: PropTypes.object.isRequired,
};

export default AboutItem;
