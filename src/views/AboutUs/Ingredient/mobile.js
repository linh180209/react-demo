import React, { Component } from 'react';
import _ from 'lodash';
import slugify from 'react-slugify';
import { Accordion } from 'react-bootstrap';
import './styles.scss';

const RenderItem = (props) => {
  const {
    ingredient, eventKey, activeItemIndex, onClickItem,
  } = props;
  return (
    <React.Fragment>
      <Accordion.Toggle as="div" variant="link" eventKey={eventKey} onClick={() => { onClickItem(eventKey); }}>
        <div className={`__item ${eventKey === activeItemIndex ? 'active' : ''}`}>
          <div className="d-flex justify-content-between align-items-center">
            <span>
              {ingredient.title}
            </span>
            {eventKey === activeItemIndex ? <i className="fa fa-times" /> : <i className="fa fa-plus" />}
          </div>
        </div>
      </Accordion.Toggle>
      <Accordion.Collapse eventKey={eventKey}>
        <div className="__item-collapse">
          <div dangerouslySetInnerHTML={{ __html: ingredient.text }} />
        </div>
      </Accordion.Collapse>
    </React.Fragment>
  );
};

class Ingredient extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeItemIndex: -1,
    };
  }

  onClickItem = (index) => {
    if (index !== this.state.activeItemIndex) {
      this.setState({
        activeItemIndex: index,
      });
    } else {
      this.setState({
        activeItemIndex: -1,
      });
    }
  }


  render() {
    const { data } = this.props;
    const { activeItemIndex } = this.state;
    return (
      <div
        className="about-ingredient"
        id={data.header ? slugify(data.header.header_text) : ''}
      >
        <h3>
          {data.header ? data.header.header_text : ''}
        </h3>
        <div className="py-5">
          <div>
            <img loading="lazy" src={data.image ? data.image.image : ''} className="img-fluid w-100" alt="" />
          </div>
          <div className="about-ingredient-list">
            <Accordion defaultActiveKey="0" className="d-flex flex-column justify-content-between h-100">
              <div className="about-ingredient-list d-flex flex-column justify-content-between">
                {data.ingredients && (
                  data.ingredients.map(e => e.value).map((ingredient, index) => (
                    <RenderItem ingredient={ingredient} eventKey={index} activeItemIndex={activeItemIndex} onClickItem={this.onClickItem} />
                  ))
                )}
              </div>
            </Accordion>
          </div>
        </div>
      </div>
    );
  }
}

export default Ingredient;
