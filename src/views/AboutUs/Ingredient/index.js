import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import IngredientWeb from './web';
import IngredientMobile from './mobile';
import { isMobileOnly } from '../../../DetectScreen';

class Ingredient extends Component {
  render() {
    const { data } = this.props;
    if (isMobileOnly) {
      return <IngredientMobile data={data} />;
    }
    return <IngredientWeb data={data} />;
  }
}

export default Ingredient;
