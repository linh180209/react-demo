import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import InstagramMobile from './mobile';
import InstagramWeb from './web';
import { isMobileOnly } from '../../../DetectScreen';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { API_INSTAGRAM } from '../../../config';

class Instagram extends Component {
  constructor(props) {
    super(props);

    this.state = {
      instagramData: [],
    };
  }

  componentDidMount() {
    fetch(API_INSTAGRAM)
      .then(response => response.json())
      .then((data) => {
        const instagramData = data.graphql.user.edge_owner_to_timeline_media.edges.map(item => item.node);
        this.setState({
          instagramData,
        });
      });
  }

  render() {
    const { data } = this.props;
    const { instagramData } = this.state;
    console.log('instagramData', instagramData);
    if (isMobileOnly) {
      return <InstagramMobile data={data} instagramData={instagramData} />;
    }
    return <InstagramWeb data={data} instagramData={instagramData} />;
  }
}

export default Instagram;
