import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { isMobileOnly } from '../../../DetectScreen';


const sliderSetting = {
  speed: 500,
  slidesToShow: !isMobileOnly ? 3 : 1,
  slidesToScroll: 1,
  centerMode: true,
};

const RenderItem = (props) => {
  const { item } = props;
  return (
    <div>
      <div className="__item">
        <a target="_blank" rel="noopener noreferrer" href={`https://www.instagram.com/p/${item.shortcode}/`}>
          <img src={item.thumbnail_src} alt="" className="img-fluid" />
        </a>
      </div>
    </div>
  );
};

class Instagram extends Component {
  render() {
    const { data, instagramData } = this.props;
    return (
      <div
        className="about-instagram"
      >
        <div className="row">
          <div className="col-3 d-flex align-items-center">
            <div className="__header">
              {data.link && (
                <a href={data.link.link}>
                  {data.link.text}
                </a>
              )}
              {data.header && (
                <h4>
                  {data.header.header_text}
                </h4>
              )}
            </div>
          </div>
          <div className="col-9">
            {instagramData.length !== 0 && (
              <div className="__slider">
                <Slider {...sliderSetting}>
                  {instagramData.map(item => (
                    <RenderItem item={item || {}} />
                  ))}
                </Slider>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Instagram;
