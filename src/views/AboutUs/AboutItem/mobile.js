import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';

class AboutItemMobile extends Component {
  render() {
    const { items } = this.props;
    return (
      <React.Fragment>
        {items.map((item, index) => (
          <div className="about-item" id="our-story">
            <div>
              <img loading="lazy" className="img-fluid w-100" src={item.image ? item.image.image : ''} alt="" />
            </div>
            <div className="__content">
              <h3>
                {item.text || ''}
              </h3>
              <div dangerouslySetInnerHTML={{ __html: item.description || '' }} />
            </div>
          </div>
        ))}
      </React.Fragment>
    );
  }
}

export default AboutItemMobile;
