import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';

class AboutItemWeb extends Component {
  render() {
    const { items } = this.props;
    return (
      <React.Fragment>
        {items.map((item, index) => (
          <div className="about-item" id="our-story">
            <div className={`row ${index % 2 === 1 ? 'flex-row-reverse' : ''}`}>
              <div className="col-6 p-0">
                <div
                  className="__image"
                  style={{
                    backgroundImage: `url(${item.image ? item.image.image : ''})`,
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    minHeight: '100%',
                  }}
                />
              </div>
              <div className="col-6 p-0">
                <div className="__content">
                  <h3>
                    {item.text || ''}
                  </h3>
                  <div dangerouslySetInnerHTML={{ __html: item.description || '' }} />
                </div>
              </div>
            </div>
          </div>
        ))}
      </React.Fragment>
    );
  }
}

export default AboutItemWeb;
