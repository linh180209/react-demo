import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import AboutItemMobile from './mobile';
import AboutItemWeb from './web';
import { isMobileOnly } from '../../../DetectScreen';

class AboutItem extends Component {
  render() {
    const { items } = this.props;
    if (isMobileOnly) {
      return <AboutItemMobile items={items} />;
    }
    return <AboutItemWeb items={items} />;
  }
}

export default AboutItem;
