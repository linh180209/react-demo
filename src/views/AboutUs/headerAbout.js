import React, { Component } from 'react';
import PropTypes from 'prop-types';
import smoothscroll from 'smoothscroll-polyfill';

import ReactHtmlParser from 'react-html-parser';
import logo from '../../image/logo-dark.png';

class HeaderAbout extends Component {
  onClickScoll = () => {
    const element = document.getElementById('id-scroll');
    if (element) {
      element.scrollIntoView({ behavior: 'smooth' });
      smoothscroll.polyfill();
    }
  }

  render() {
    const { imgBg, textBlock } = this.props;
    return (
      <div
        style={{
          width: '100vw',
          marginTop: '70px',
        }}
        className="height_home_page div-col justify-center items-center"
      >
        <div
          className="div-col justify-center items-center"
        >
          <img
            loading="lazy"
            src={imgBg}
            style={{
              height: 'calc(100vh-70px)',
              width: '100vw',
              position: 'absolute',
              zIndex: '-1',
              objectFit: 'cover',
              top: '0px',
            }}
            className="height_home_page"
            alt="bg"
          />
          <img
            loading="lazy"
            style={{
              width: '100px',
              height: '100px',
            }}
            src={logo}
            alt="logo"
          />
          <div className="text-about">
            {ReactHtmlParser(textBlock ? textBlock.value : '')}
          </div>
          <div style={{
            height: '1rem',
            width: '1px',
            background: '#000000',
          }}
          />
          <button
            type="button"
            className="button-bg__none mt-5"
            onClick={this.onClickScoll}
          >
            SCROLL
          </button>
        </div>
      </div>
    );
  }
}

HeaderAbout.propTypes = {
  imgBg: PropTypes.string.isRequired,
  textBlock: PropTypes.shape(PropTypes.object).isRequired,
};

export default HeaderAbout;
