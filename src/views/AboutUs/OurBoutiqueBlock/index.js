import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import ReactHtmlParser from 'react-html-parser';
import '../../../styles/our-boutique-block.scss';
import ItemBoutique from '../../OurBoutique/itemBoutique';

function OurBoutiqueBlock(props) {
  const [tab, setTab] = useState(0);

  const setAnimation = (index) => {
    const ele = document.getElementById('line-active');
    const tabList = document.getElementById('list-tab');
    const bt = document.getElementById(`bt-${index}`);
    if (bt && ele && tabList) {
      ele.style.left = `${bt.getBoundingClientRect().x - tabList.getBoundingClientRect().x}px`;
      ele.style.width = `${bt.getBoundingClientRect().width}px`;
    }
  };

  const onClickTab = (index) => {
    setTab(index);
    setAnimation(index);
  };

  useEffect(() => {
    setAnimation(0);
  }, []);

  return (
    <div className={classnames('our-boutique-block', props.className)}>
      {/* <h1>
        {props.data.value.header.header_text}
      </h1> */}
      {
        props.data.value.stores?.length > 1 && (
          <div id="list-tab" className="list-tab">
            {
              _.map(props.data.value.stores, (d, index) => (
                <button
                  id={`bt-${index}`}
                  onClick={() => onClickTab(index)}
                  className={classnames('button-bg__none', index === tab ? 'active' : '')}
                  type="button"
                >
                  {d.value.country}
                </button>
              ))
            }
            <div id="line-active" className="line-active" />
          </div>
        )
      }
      <ItemBoutique data={props.data.value.stores[tab]} />
      <h2 className="header_2">
        {props.data.value.header.header_text}
      </h2>
      {
        props.data.value.stores[tab].value.text && (
          <div className="header_4">
            {
              ReactHtmlParser(props.data.value.stores[tab].value.text)
            }
          </div>
        )
      }
    </div>
  );
}

export default OurBoutiqueBlock;
