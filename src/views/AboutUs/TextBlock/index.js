import React, { useState, useEffect } from 'react';
import ReactHtmlParser from 'react-html-parser';
import '../../../styles/text-block-about-us.scss';

function TextBlockAboutUs(props) {
  return (
    <div className="text-block-about-us">
      <h1>
        {props.data.value.header.header_text}
      </h1>
      <div>
        {ReactHtmlParser(props.data.value.paragraph)}
      </div>
    </div>
  );
}

export default TextBlockAboutUs;
