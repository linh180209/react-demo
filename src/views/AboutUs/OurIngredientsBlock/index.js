import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';
import classnames from 'classnames';
import '../../../styles/our-ingredients-block.scss';
import { getAltImageV2, isRightAlignedText } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';

function ItemBlock(props) {
  return (
    <div className={classnames('item-block', props.right && !isMobile ? 'right-item' : '')}>
      <div className="image-text">
        <img loading="lazy" src={props.data.value.images[0].value.image} alt={getAltImageV2(props.data.value.images[0].value)} />
        <div className={classnames('text-info', isRightAlignedText() ? 'tr' : '')}>
          <h2 className="header_2">
            {props.data.value.header.header_text}
          </h2>
          <span className={classnames('header_4', isRightAlignedText() ? 'tr' : '')}>
            {ReactHtmlParser(props.data.value.text)}
          </span>
        </div>
      </div>
      <div className={classnames(props.hiddenLine ? 'hidden' : 'line-contact', props.right && isMobile ? 'line-right-mobile' : '')} />
    </div>
  );
}

function OurIngredientsBlock(props) {
  return (
    <div className="our-ingredients-block div-col items-center">
      <h2 className="header_1">
        {props.data.value.header.header_text}
      </h2>
      {
        _.map(props.data.value.ateliers, (d, index) => (
          <ItemBlock data={d} right={index % 2 === 1} hiddenLine={index === props.data.value.ateliers.length - 1} />
        ))
      }
    </div>
  );
}

export default OurIngredientsBlock;
