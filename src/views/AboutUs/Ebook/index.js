import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import classnames from 'classnames';
// import ReactGA from 'react-ga';
import icCheck from '../../../image/icon/check-about.svg';
import logo from '../../../image/icon/logo-about.svg';
import { toastrError, toastrSuccess } from '../../../Redux/Helpers/notification';
import { SUBSCRIBER_NEWLETTER_URL, GUEST_CREATE_BASKET_URL, GET_BASKET_URL } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import {
  validateEmail, getNameFromButtonBlock, isRightAlignedText,
} from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';
import { isBrowser } from '../../../DetectScreen';
import ButtonCT from '../../../components/ButtonCT';

class Ebook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };
  }

  onChangeEmail = (e) => {
    const { value } = e.target;
    this.setState({ email: value });
  }

  assignEmailToCart = (email) => {
    const { basket } = this.props;
    if (!basket.id) {
      const options = {
        url: GUEST_CREATE_BASKET_URL,
        method: 'POST',
        body: {
          subtotal: 0,
          total: 0,
          email,
        },
      };
      fetchClient(options).then((result) => {
        if (result && !result.isError) {
          this.props.loadAllBasket(result);
        }
      });
      return;
    }
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basket.id),
      method: 'PUT',
      body: {
        email,
      },
    };
    fetchClient(option);
  }

  postNewLetter = (email) => {
    if (!validateEmail(email)) {
      toastrError('Email is not avaiable');
      return;
    }
    // ReactGA.event({
    //   category: 'Newsletter',
    //   action: 'Submit',
    // });

    this.assignEmailToCart(email);

    const { login } = this.props;
    const options = {
      url: SUBSCRIBER_NEWLETTER_URL,
      method: 'POST',
      body: {
        download: true,
        email,
        user: login && login.user ? login.user.id : undefined,
      },
    };
    fetchClient(options).then((result) => {
      // console.log('result', result);
      if (result.isError) {
        if (result.email) {
          toastrError('You already have registered');
        } else {
          toastrError('You have already subscribed to our newsletter');
        }
        return;
      }
      auth.setEmail(email);
      toastrSuccess('Email sent successfully');
      if (options.body.user) {
        this.props.loginUpdateUserInfo({ is_get_newsletters: true });
      }
    }).catch((err) => {
      toastrError(err);
    });
  }

  render() {
    const { email } = this.state;
    const { data } = this.props;
    const { buttons, image_background: imageBlock } = data;
    const header1 = getNameFromButtonBlock(buttons, 'header_1');
    const des1 = getNameFromButtonBlock(buttons, 'des_1');
    const header2 = getNameFromButtonBlock(buttons, 'header_2');
    const des21 = getNameFromButtonBlock(buttons, 'des_2_1');
    const des22 = getNameFromButtonBlock(buttons, 'des_2_2');
    const des23 = getNameFromButtonBlock(buttons, 'des_2_3');
    const des24 = getNameFromButtonBlock(buttons, 'des_2_4');
    const des25 = getNameFromButtonBlock(buttons, 'des_2_5');
    const emailbt = getNameFromButtonBlock(buttons, 'Email');
    const downloadFreeBt = getNameFromButtonBlock(buttons, 'download_free');
    const buttonList = _.filter(buttons || [], x => x.value.link === 'list');
    return (
      <React.Fragment>
        {
          isBrowser ? (
            <div className="div-ebook" id="our-ebook">
              <div className="div-bg">
                <div className="div-bg-1">
                  <div className={classnames('div-body', isRightAlignedText() ? 'block-text-right' : '')}>
                    <h2 className="header_1">
                      {header2}
                    </h2>
                    <h3 className="des-1 header_3">
                      {des21}
                    </h3>
                    {
                      _.map(buttonList, d => (
                        <div className="div-line-check">
                          <img src={icCheck} alt="icCheck" />
                          <span className="header_4">
                            {d.value.text}
                          </span>
                        </div>
                      ))
                    }
                    <span className="des-2 header_4">
                      {des25}
                    </span>
                  </div>
                </div>
                <div className="div-bg-2">
                  <div className={classnames('div-body', isRightAlignedText() ? 'block-text-right' : '')}>
                    <input type="text" placeholder={emailbt} onChange={this.onChangeEmail} />
                    <div>
                      <ButtonCT
                        className="button-black"
                        name={downloadFreeBt}
                        disabled={_.isEmpty(email)}
                        onClick={() => this.postNewLetter(email)}
                      />
                      {/* <button
                        type="button"
                        className="button-homepage-new-6 text-black animated-hover"
                        onClick={() => this.postNewLetter(email)}
                        disabled={_.isEmpty(email)}
                      >
                        {downloadFreeBt}
                      </button> */}
                    </div>
                  </div>
                </div>
              </div>
              <div className="div-image">
                <div className="div-image-body">
                  <img loading="lazy" src={imageBlock ? imageBlock.image : ''} alt="bg" />
                  <div className={this.props.isVersion2 ? 'hidden' : 'div-text-footer'}>
                    <h4>
                      {header1}
                    </h4>
                    <span>
                      {des1}
                    </span>
                    <img src={logo} alt="logo" />
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div className="div-ebook-mobile" id="our-ebook">
              <div className="div-image">
                <div className="div-image-body">
                  <img src={imageBlock ? imageBlock.image : ''} alt="bg" />
                  <div className={this.props.isVersion2 ? 'hidden' : 'div-text-footer'}>
                    <h4>
                      {header1}
                    </h4>
                    <span>
                      {des1}
                    </span>
                    <img src={logo} alt="logo" />
                  </div>
                </div>
              </div>
              <div className="div-body">
                <h3 className="header_2">
                  {header2}
                </h3>
                <span className="des-1 header_4">
                  {des21}
                </span>
                {
                _.map(buttonList, d => (
                  <div className="div-line-check">
                    <img src={icCheck} alt="icCheck" />
                    <span className="header_4">
                      {d.value.text}
                    </span>
                  </div>
                ))
              }
                <span className="des-2 header_4">
                  {des25}
                </span>
              </div>

              <div className="div-bg-2">
                <div className="div-body-email">
                  <input type="text" placeholder={emailbt} onChange={this.onChangeEmail} />
                  <div>
                    <ButtonCT
                      className="button-black"
                      name={downloadFreeBt}
                      disabled={_.isEmpty(email)}
                      onClick={() => this.postNewLetter(email)}
                    />
                    {/* <button
                      type="button"
                      onClick={() => this.postNewLetter(email)}
                      disabled={_.isEmpty(email)}
                    >
                      {downloadFreeBt}
                    </button> */}
                  </div>
                </div>
              </div>
            </div>
          )
        }
      </React.Fragment>

    );
  }
}

export default Ebook;
