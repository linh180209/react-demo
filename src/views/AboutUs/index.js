import React, { Component } from 'react';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import '../../styles/about.scss';

import {
  getSEOFromCms, fetchCMSHomepage, scrollTop, googleAnalitycs, generateHreflang, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers';
import Header from '../../components/HomePage/header';
import AboutBanner from './AboutBanner';
import AboutItem from './AboutItem/index';
import Commitment from './Commitment';
import Ingredient from './Ingredient';
import Store from './Store';
import Ebook from './Ebook';
import Instagram from './Instagram';
import Event from './Event';
import BlockStepItem from '../../components/HomeAlt/BlockStep';

import addCmsRedux from '../../Redux/Actions/cms';
import { loadAllBasket } from '../../Redux/Actions/basket';
import { loginUpdateUserInfo } from '../../Redux/Actions/login';
import FooterV2 from '../../views2/footer';

const getCms = (aboutUs) => {
  const objectReturn = {
    bannerBlock: [],
    textBlock: '',
    imageTxtBlock: [],
    commitmentBlock: {},
    ingredientBlock: {},
    storeBlock: {},
    instagramBlock: {},
    featureBlock: {},
    seo: undefined,
    stepBlock: {},
    benefitsBlock: {},
  };

  if (!_.isEmpty(aboutUs)) {
    const seo = getSEOFromCms(aboutUs);
    const { body } = aboutUs;
    if (body) {
      const bannerBlock = _.filter(body, x => x.type === 'banner_block').map(e => e.value);
      const textBlock = _.find(body, x => x.type === 'text_block').value;
      const imageTxtBlock = _.filter(body, x => x.type === 'imagetxt_block').map(e => e.value);
      const commitmentBlock = _.find(body, x => x.type === 'commitments_block').value;
      const ingredientBlock = _.find(body, x => x.type === 'ingredients_block').value;
      const storeBlock = _.find(body, x => x.type === 'stores_block').value;
      const instagramBlock = _.find(body, x => x.type === 'instagram_block').value;
      const featureBlock = _.find(body, x => x.type === 'icons_block').value;
      const stepBlock = _.find(body, x => x.type === 'steps_block').value;
      const benefitsBlock = _.find(body, x => x.type === 'benefits_block');
      _.assign(objectReturn, {
        bannerBlock,
        textBlock,
        imageTxtBlock,
        commitmentBlock,
        ingredientBlock,
        storeBlock,
        instagramBlock,
        featureBlock,
        seo,
        stepBlock,
        benefitsBlock,
      });
    }
  }
  return objectReturn;
};
class AboutUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bannerBlock: [],
      textBlock: '',
      imageTxtBlock: [],
      commitmentBlock: {},
      ingredientBlock: {},
      storeBlock: {},
      instagramBlock: {},
      featureBlock: {},
      stepBlock: {},
      seo: undefined,
      benefitsBlock: {},
    };
  }


  componentDidMount() {
    // console.log('HASH:', this.props.location.hash);
    setPrerenderReady();
    if (!this.props.location.hash) {
      scrollTop();
    }
    googleAnalitycs('/about-us');
    this.fetchDataInit();
    this.scrollToHashId();
  }

  componentDidUpdate(prevProps, prevState) {
    this.scrollToHashId();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const aboutUs = _.find(cms, x => x.title === 'about us v2');
    if (!aboutUs) {
      const cmsData = await fetchCMSHomepage('about-us-v2');
      this.props.addCmsRedux(cmsData);
      const dataFromStore = getCms(cmsData);
      this.setState(dataFromStore);
    } else {
      const dataFromStore = getCms(aboutUs);
      this.setState(dataFromStore);
    }
  }

  scrollToHashId = () => {
    const { removeHash } = this;
    // get URL hash (minus the hash mark)
    const hash = this.props.location.hash.substring(1);

    // if there's a hash, scroll to that ID
    if (hash && hash !== '#null') {
      // setTimeout and requestAnimationFrame help ensure a true DOM repaint/reflow before we try to scroll
      // - reference: http://stackoverflow.com/a/34999925
      setTimeout(
        window.requestAnimationFrame(() => {
          const el = document.getElementById(hash);
          if (el) {
            el.scrollIntoView();
          }
          // clean up the hash, so we don't scroll on every prop update
          removeHash();
        }),
        0,
      );
    }
  }

  // borrowed from http://stackoverflow.com/questions/1397329/how-to-remove-the-hash-from-window-location-with-javascript-without-page-refresh/5298684#5298684
  removeHash = () => {
    const loc = window.location;
    const hist = window.history;

    // use modern browser history API
    if (hist && 'pushState' in hist) {
      hist.replaceState('', document.title, loc.pathname + loc.search);
      // fallback for older browsers
    } else {
      // prevent scrolling by storing the page's current scroll offset
      const scrollV = document.body.scrollTop;
      const scrollH = document.body.scrollLeft;

      loc.hash = '';
      // restore the scroll offset, should be flicker free
      document.body.scrollTop = scrollV;
      document.body.scrollLeft = scrollH;
    }
  }

  render() {
    const {
      bannerBlock, textBlock, imageTxtBlock, commitmentBlock, ingredientBlock,
      storeBlock, instagramBlock, featureBlock, seo, stepBlock, benefitsBlock,
    } = this.state;
    const { history, login, countries } = this.props;
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(countries, '/about-us')}
        </MetaTags>
        <Header />
        <div className="about">
          <AboutBanner banner={bannerBlock.length ? bannerBlock[0] : {}} text={textBlock || ''} />
          <AboutItem items={imageTxtBlock} />
          <Commitment data={commitmentBlock} />
          <Ingredient data={ingredientBlock} />
          <Store storeBlock={storeBlock.stores ? storeBlock.stores.map(e => e.value) : []} />
          <Ebook
            basket={this.props.basket}
            login={this.props.login}
            loginUpdateUserInfo={this.props.loginUpdateUserInfo}
            loadAllBasket={this.props.loadAllBasket}
            data={stepBlock}
          />
          <BlockStepItem heroCms={benefitsBlock} history={history} login={login} index={1} isBenefitsBlock />
          {/* <Instagram data={instagramBlock} /> */}
          <Event banner={bannerBlock.length ? bannerBlock[1] : {}} features={featureBlock} />
        </div>
        <FooterV2 />
      </div>
    );
  }
}

AboutUs.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    login: state.login,
    basket: state.basket,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  loginUpdateUserInfo,
  loadAllBasket,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutUs));
