import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import { ParallaxBanner } from 'react-scroll-parallax';
import { Fade } from 'react-reveal';
import BackgroundVideo from '../../../components/backgroundVideo';
import { isMobileOnly, isTablet } from '../../../DetectScreen';

const RenderBanner = (props) => {
  const { banner, text } = props;
  return (
    <Fade duration={2000} left>
      <div className="__content d-flex flex-column position-relative justify-content-center align-items-center">
        <img loading="lazy" src={banner.logo ? banner.logo.image : ''} alt="logo" />
        <div className="pt-4">
          <div dangerouslySetInnerHTML={{ __html: banner.description || '' }} />
        </div>
      </div>
    </Fade>
  );
};

const RenderVideo = (props) => {
  const { videos } = props;
  if (isMobileOnly) {
    return <BackgroundVideo url={videos.length ? videos.find(video => video.type === 'phone').video : ''} placeholder={videos.length ? videos.find(video => video.type === 'phone').placeholder : ''} />;
  }
  if (isTablet) {
    return <BackgroundVideo url={videos.length ? videos.find(video => video.type === 'tablet').video : ''} placeholder={videos.length ? videos.find(video => video.type === 'tablet').placeholder : ''} />;
  }
  return <BackgroundVideo url={videos.length ? videos.find(video => video.type === 'web').video : ''} placeholder={videos.length ? videos.find(video => video.type === 'web').placeholder : ''} />;
};

class AboutBanner extends Component {
  render() {
    const { banner, text } = this.props;
    const videos = banner.videos ? banner.videos.map(e => e.value) : [];
    return (
      <React.Fragment>
        {banner && (
          <div
            className="about-banner d-flex justify-content-center align-items-center"
          >
            {videos.length && (
              <ParallaxBanner
                className="d-flex justify-content-center align-items-center"
                layers={[{
                  children: <RenderVideo videos={videos} />,
                  amount: 0.4,
                }]}
                style={{
                  height: '70vh',
                }}
              >
                {banner.logo && <RenderBanner banner={banner} text={text} />}
              </ParallaxBanner>
            )}
          </div>
        )}
        {text && (
          <div className="about-banner-text position-relative">
            {text}
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default AboutBanner;
