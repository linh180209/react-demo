import React, { useState, useEffect } from 'react';
import '../../styles/style.scss';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { MetaTags } from 'react-meta-tags';
import '../../styles/about-us-v3.scss';
import '../../styles/our-boutique.scss';
import addCmsRedux from '../../Redux/Actions/cms';
import {
  fetchCMSHomepage, generateHreflang, getSEOFromCms, removeLinkHreflang, setPrerenderReady, setScriptIntoDocument,
} from '../../Redux/Helpers';
import { toastrError } from '../../Redux/Helpers/notification';
import { loadAllBasket } from '../../Redux/Actions/basket';
import { loginUpdateUserInfo } from '../../Redux/Actions/login';

import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import BlockColumn from '../../components/HomeAlt/BlockColumn';
// import TextBlockAboutUs from './TextBlock';
// import BlockBanner from '../../components/HomeAlt/BlockBanner';
import BlockIconHeaderV2 from '../../components/HomeAlt/BlockIconHeaderV2';
import OurIngredientsBlock from './OurIngredientsBlock';
import OurBoutiqueBlock from './OurBoutiqueBlock';
import Ebook from './Ebook';
// import Instagram from './Instagram';
// import { SCHEMA_MARKUP_ID } from '../../constants';
import BlockBannerV2 from '../../components/HomeAlt/BlockBannerV2';
import CTABlockOurTeam from '../OurTeam/ctaBlockOurTeam';
import FooterV2 from '../../views2/footer';

function AboutUsV3(props) {
  const [aboutCms, setAboutCms] = useState({});

  const fetchCMS = async () => {
    const { cms } = props;
    const aboutCmsFind = _.find(cms, x => x.title === 'About Us V3');
    if (!aboutCmsFind) {
      try {
        const result = await fetchCMSHomepage('about-us-v3');
        props.addCmsRedux(result);
        setAboutCms(result);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      setAboutCms(aboutCmsFind);
    }
  };

  // const setScript = () => {
  //   setScriptIntoDocument(SCHEMA_MARKUP_ID, `
  //   {
  //     "@context": "https://schema.org/",
  //     "@type": "Recipe",
  //     "name": "Party Coffee Cake",
  //     "author": {
  //       "@type": "Person",
  //       "name": "Mary Stone"
  //     },
  //     "datePublished": "2018-03-10",
  //     "description": "This coffee cake is awesome and perfect for parties.",
  //     "prepTime": "PT20M"
  //   }
  //   `);
  // };

  useEffect(() => {
    setPrerenderReady();
    // setScript();
    fetchCMS();
    return (() => {
      removeLinkHreflang();
    });
  }, []);

  const seo = getSEOFromCms(aboutCms);
  const metaData = (
    <MetaTags>
      <title>
        {seo ? seo.seoTitle : ''}
      </title>
      <meta name="description" content={seo ? seo.seoDescription : ''} />
      <meta name="robots" content="index, follow" />
      <meta name="revisit-after" content="3 month" />
      {generateHreflang(props.countries, '/about-us')}
    </MetaTags>
  );
  return (
    <div>
      {metaData}
      {/* <HeaderHomePage />
      { props.showAskRegion && (<div className="div-temp-region" />) } */}
      <HeaderHomePageV3 />

      <div className="about-us-v3">
        {
        _.map(aboutCms.body, (d, index) => {
          switch (d.type) {
            case 'columns_block':
              return <BlockColumn data={d} />;
            case 'cta_block':
              return <CTABlockOurTeam data={d} />;
            case 'banner_block':
              return <BlockBannerV2 id={`banner_block-${index}`} data={d} />;
            case 'benefits_block':
              return <BlockIconHeaderV2 heroCms={d} classSub1="header_3" classSub2="header_4" isSlider />;
            case 'atelier_list_block':
              return <OurIngredientsBlock data={d} />;
            case 'stores_block':
              return <OurBoutiqueBlock data={d} className="store-block" />;
            case 'steps_block':
              return (
                <Ebook
                  basket={props.basket}
                  login={props.login}
                  loginUpdateUserInfo={props.loginUpdateUserInfo}
                  loadAllBasket={props.loadAllBasket}
                  data={d.value}
                  isVersion2
                />
              );
            // case 'instagram_block':
            //   return (
            //     <Instagram data={d.value} />
            //   );
            default:
              return null;
          }
        })
      }
      </div>
      <FooterV2 />
    </div>
  );
}

AboutUsV3.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
    basket: state.basket,
    login: state.login,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  loginUpdateUserInfo,
  loadAllBasket,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AboutUsV3));
