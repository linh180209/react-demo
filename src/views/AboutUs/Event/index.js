import React, { Component } from 'react';
import _ from 'lodash';
import slugify from 'react-slugify';
import { Fade } from 'react-reveal';
import { ParallaxBanner, withController } from 'react-scroll-parallax';
import './styles.scss';
import { isMobileOnly } from '../../../DetectScreen';

const RenderItem = (props) => {
  const { feature, index } = props;
  return (
    <Fade top delay={(index + 1) * 300}>
      <div className="__item">
        <div className="__item-icon d-flex justify-content-center">
          <img loading="lazy" src={feature.image ? feature.image.image : ''} alt="" className="img-fluid" />
        </div>
        <div className="__item-title text-center">
          {feature.text}
        </div>
      </div>
    </Fade>
  );
};

class Event extends Component {
  componentDidUpdate(prevProps, prevState, snapshot) {
    this.props.parallaxController.update();
  }

  render() {
    this.props.parallaxController.update();
    const { banner, features } = this.props;
    return (
      <React.Fragment>
        {banner && (
          <ParallaxBanner
            layers={[
              {
                image: banner.image ? banner.image.image : '',
                amount: 0.3,
              },
            ]}
            style={{
              height: '100%',
            }}
          >
            <div
              className="about-event d-flex flex-column justify-content-center align-items-center"
              id={banner.header ? slugify(banner.header.header_text) : ''}
            >
              <h3>
                {banner.header ? banner.header.header_text : ''}
              </h3>
              <div dangerouslySetInnerHTML={{
                __html: banner.description || '',
              }}
              />
              <a href={banner.button ? banner.button.link : ''}>
                {banner.button ? banner.button.text : ''}
              </a>
            </div>
            <div style={{
              position: 'absolute',
              top: '0',
              left: '0',
              width: '100%',
              height: '100%',
              backgroundColor: '#B79B53',
              opacity: '0.4',
            }}
            />
          </ParallaxBanner>
        )}
        {features && (
          <div
            style={{
              backgroundImage: `url(${features.image_background ? features.image_background.image : ''})`,
              backgroundPosition: 'center',
              backgroundSize: 'cover',
            }}
          >
            {features.icons && (
              <div
                className={`about-features d-flex ${isMobileOnly ? 'flex-column' : ''} justify-content-between`}
              >
                {features.icons.map(e => e.value).map((feature, index) => (
                  <RenderItem feature={feature} index={index} />
                ))}
              </div>
            )}
          </div>
        )}
      </React.Fragment>
    );
  }
}
export default withController(Event);
