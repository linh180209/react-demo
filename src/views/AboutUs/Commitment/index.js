import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import slugify from 'react-slugify';
import { Fade } from 'react-reveal';
import { ParallaxBanner, withController } from 'react-scroll-parallax';
import { isMobileOnly } from '../../../DetectScreen';

const RenderItem = (props) => {
  const { commitment, index } = props;
  return (
    <Fade top delay={(index + 1) * 300}>
      <div className="__item d-flex flex-column align-items-center">
        <div className="__logo">
          <img loading="lazy" src={commitment.image ? commitment.image.image : ''} alt={commitment.image} />
        </div>
        <div className="__content">
          <div className="__content-title">
            {commitment.header ? commitment.header.header_text : ''}
          </div>
          <div className="__content-description">
            {commitment.text || ''}
          </div>
        </div>
      </div>
    </Fade>
  );
};

class Commitment extends Component {
  componentDidUpdate(prevProps, prevState, snapshot) {
    this.props.parallaxController.update();
  }

  render() {
    const { data } = this.props;
    this.props.parallaxController.update();
    return data && (
      <ParallaxBanner
        layers={[
          {
            image: data.image_background ? data.image_background.image : '',
            amount: 0.4,
          },
        ]}
        style={{
          height: '100%',
        }}
      >
        {data && (
          <div
            className="about-commitment d-flex flex-column justify-content-center align-items-center"
            id={data.header ? slugify(data.header.header_text) : ''}
            style={{
              backgroundColor: '#181817',
              opacity: '0.9',
            }}
          >
            <h3>
              {data.header ? data.header.header_text : ''}
            </h3>
            {data.commitments && (
              <div className={`about-commitment-list ${isMobileOnly ? 'flex-column' : ''} d-flex justify-content-around`}>
                {
                  data.commitments.map(e => e.value).map((commitment, index) => (
                    <RenderItem commitment={commitment} index={index} />
                  ))
                }
              </div>
            )}
          </div>
        )}
      </ParallaxBanner>
    );
  }
}

export default withController(Commitment);
