import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import InputMask from 'react-input-mask';
import _ from 'lodash';
import './styles.scss';

class ReviewBanner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSlide: 0,
      pageGo: 1,
    };
    this.slider = React.createRef();
  }

  onChange = (e) => {
    const { value } = e.target;
    this.setState({ pageGo: value });
  }

  gotoPage = () => {
    const { pageGo } = this.state;
    if (this.slider) {
      this.slider.slickGoTo(pageGo - 1);
    }
  }

  render() {
    const { items } = this.props;
    const isDisplayDot = (item, index, maxLength) => {
      const { currentSlide } = this.state;
      console.log('item', item);
      if (index === 0 || index === maxLength - 1) {
        return (<li className={item.props.className} key={index}>{item.props.children}</li>);
      } if (index === 1 && currentSlide > 5) {
        return (<li className="disable" disabled key={index}>...</li>);
      } if (index === maxLength - 2 && currentSlide < maxLength - 5) {
        return (<li className="disable" disabled key={index}>...</li>);
      } if (Math.abs(currentSlide - index) < 3) {
        return (<li className={item.props.className} key={index}>{item.props.children}</li>);
      }
      return (<li className="hidden" key={index}>{item.props.children}</li>);
    };
    const sliderSetting = {
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      infinite: false,
      beforeChange: (prev, next) => {
        this.setState({ currentSlide: next, pageGo: next + 1 });
      },
      appendDots: dots => (
        <div>
          <ul>
            <li onClick={() => { this.slider.slickPrev(); }} style={{ opacity: '1' }}>
              <div
                className="page-number"
              >
                <i className="fa fa-chevron-left" />
              </div>
            </li>
            {dots.map((item, index) => (
              isDisplayDot(item, index, dots.length)
            ))}
            <li onClick={() => { this.slider.slickNext(); }} style={{ opacity: '1' }}>
              <div
                className="page-number"
              >
                <i className="fa fa-chevron-right" />
              </div>
            </li>
          </ul>
        </div>
      ),
      customPaging: i => (
        <div
          className="page-number"
        >
          {i + 1}
        </div>
      ),
    };

    return (
      <div className="review-banner">
        <div className="row" style={{ position: 'relative' }}>
          <div className="col-12">
            {items && (
              <Slider ref={slider => (this.slider = slider)} {...sliderSetting}>
                {items.map(ele => ele.value).map((item, index) => (
                  <div key={index}>
                    <div className="__item">
                      <div className="row">
                        <div className="col-7">
                          <div className="banner-image">
                            <img src={item.image ? item.image.image : ''} alt="" />
                          </div>
                        </div>
                        <div className="col-5">
                          <div className="banner-content">
                            <h3>
                              {item.header ? item.header.header_text : ''}
                            </h3>
                            <div dangerouslySetInnerHTML={{
                              __html: item.description,
                            }}
                            />
                            <div className="d-flex justify-content-between align-items-center">
                              <img src={item.logo.image} alt="" className="img-fluid" />
                              <a type="button" href={item ? item.button.link : ''}>
                                {item.button ? item.button.text : ''}
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </Slider>
            )}
          </div>
          <div className="div-go-page">
            <span>
              go to page
            </span>
            <InputMask
              className="input-page"
              type="text"
              name="page"
              mask="99"
              maskChar={null}
              value={this.state.pageGo}
              onChange={this.onChange}
            />
            <span>
              of
              {' '}
              {items.length}
            </span>
            <button type="button" className="bt-go" onClick={this.gotoPage}>
              Go
              <i className="fa fa-chevron-right" />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ReviewBanner;
