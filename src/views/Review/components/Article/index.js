import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import moment from 'moment';
import './styles.scss';
import { isTablet } from '../../../../DetectScreen';

class Article extends Component {
  render() {
    const { items } = this.props;
    const sliderSetting = {
      speed: 500,
      slidesToShow: !isTablet ? 2 : 1,
      slidesToScroll: 1,
      centerMode: true,
      infinite: false,
    };

    return (
      <div className="review-article">
        <div className="row justify-content-center">
          {items && (
            <div className="__title d-flex flex-column justify-content-center">
              <h3>
                {items.header ? items.header.header_text : ''}
              </h3>
              <p>
                {items.text}
              </p>
            </div>
          )}
        </div>
        <div className="article-slider">
          <div className="row">
            <div className="col-12">
              {items.feedbacks && (
                <Slider {...sliderSetting}>
                  {items.feedbacks.map(ele => ele.value).map((item, index) => (
                    <div key={index}>
                      <div className="__item">
                        <div className="cover">
                          <img src={item.avatar.image} alt={item.avatar.caption} className="img-fluid" />
                        </div>
                        <div className="content">
                          <div className="row pt-3">
                            <div className="col-8">
                              <p className="__item-name">
                                {item.name}
                              </p>
                              <p className="__item-content">
                                {item.text}
                              </p>
                              <p className="__item-date">
                                {moment(item.date).format('MMMM Do YYYY')}
                              </p>
                            </div>
                            <div className="col-4">
                              <div className="d-flex align-items-center justify-content-end h-100">
                                <a href={item.link}>
                                  READ NOW
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </Slider>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Article;
