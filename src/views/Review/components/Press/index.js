import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { isMobileOnly, isTablet } from '../../../../DetectScreen';
import './styles.scss';

class Press extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentIndex: 0,
    };
  }


  render() {
    const { items } = this.props;
    const { currentIndex } = this.state;
    const sliderSetting = {
      speed: 500,
      slidesToShow: isMobileOnly || isTablet ? 1 : 3,
      slidesToScroll: 1,
      dots: !isMobileOnly,
      centerMode: true,
      arrows: false,
    };

    return (
      <div className="review-press">
        <div className="row justify-content-center">
          {items && (
            <div className="__title d-flex flex-column justify-content-center">
              <h3>
                {items.header ? items.header.header_text : ''}
              </h3>
              <p>
                {items.text}
              </p>
            </div>
          )}
        </div>
        <div className="row">
          <div className="col-12 justify-content-center">
            {items.feedbacks && (
              <div className="press-slider">
                <Slider
                  {...sliderSetting}
                  afterChange={(index) => {
                    this.setState({
                      currentIndex: index,
                    });
                  }}
                >
                  {items.feedbacks.map(ele => ele.value).map((item, index) => (
                    <div key={index}>
                      <div className={`__item d-flex ${isMobileOnly ? 'flex-column-reverse' : 'flex-column'} justify-content-center`}>
                        {!isMobileOnly && (
                          <div className="quote">
                            <p>
                              {item.text}
                            </p>
                          </div>
                        )}
                        <div className="logo">
                          <img className="img-fluid" src={item.avatar.image} alt={item.avatar.caption} />
                        </div>
                      </div>
                    </div>
                  ))}
                </Slider>
                {isMobileOnly && items.feedbacks && (
                  <div className="__quote">
                    <p>
                      {items.feedbacks.map(ele => ele.value)[currentIndex].text}
                    </p>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Press;
