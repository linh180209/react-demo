import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';
import './styles.scss';
import { isMobileOnly, isTablet } from '../../../../DetectScreen';

class Influencer extends Component {
  render() {
    const { items } = this.props;
    const sliderSetting = {
      speed: 500,
      slidesToShow: !(isMobileOnly || isTablet) ? 5 : 1,
      slidesToScroll: 3,
      centerMode: true,
      dots: isMobileOnly,
      arrows: !isMobileOnly,
      infinite: !isMobileOnly,
    };


    return (
      <div className="review-influencer">
        <div className="row justify-content-center">
          {items && (
            <div className="__title d-flex flex-column justify-content-center">
              <h3>
                {items.header ? items.header.header_text : ''}
              </h3>
              <p>
                {items.text}
              </p>
            </div>
          )}
        </div>
        <div className="row">
          <div className="col-12">
            {items.feedbacks && (
              <div className="influencer-slider">
                <Slider {...sliderSetting}>
                  {items.feedbacks.map(ele => ele.value).map((item, index) => (
                    <div key={index}>
                      <div className="__item" href={item.link}>
                        <a href={item.link}>
                          <div className="instagram">
                            <img src={item.avatar.image} alt={item.avatar.caption} className="img-fuild" />
                            <p className="link">
                              {item.instagram}
                            </p>
                          </div>
                          <div className="quote">
                            <p>
                              {item.text}
                            </p>
                          </div>
                        </a>
                      </div>
                    </div>
                  ))}
                </Slider>
              </div>
            )}
          </div>
        </div>
        {/* <div className="row justify-content-center">
          <div className="influencer-all">
            <button type="button" className="__all-btn">
              see all review
              </button>
          </div>
        </div> */}
      </div>
    );
  }
}

export default Influencer;
