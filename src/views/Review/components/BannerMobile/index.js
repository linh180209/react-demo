import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';
import './styles.scss';

class BannerMobile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentIndex: 0,
      currentSlide: 0,
    };
  }

  render() {
    const { items } = this.props;
    const { currentIndex } = this.state;
    const isDisplayDot = (item, index, maxLength) => {
      const { currentSlide } = this.state;
      console.log('item', item);
      if (index === 0 || index === maxLength - 1) {
        return (<li className={item.props.className} key={index}>{item.props.children}</li>);
      } if (index === 1 && currentSlide > 5) {
        return (<li className="disable" disabled key={index}>...</li>);
      } if (index === maxLength - 2 && currentSlide < maxLength - 5) {
        return (<li className="disable" disabled key={index}>...</li>);
      } if (Math.abs(currentSlide - index) < 3) {
        return (<li className={item.props.className} key={index}>{item.props.children}</li>);
      }
      return (<li className="hidden" key={index}>{item.props.children}</li>);
    };
    const sliderSetting = {
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      dots: true,
      infinite: false,
      beforeChange: (prev, next) => {
        this.setState({ currentSlide: next });
      },
      appendDots: dots => (
        <div>
          <ul>
            <li onClick={() => { this.slider.slickPrev(); }} style={{ opacity: '1' }}>
              <div
                className="page-number"
              >
                <i className="fa fa-chevron-left" />
              </div>
            </li>
            {dots.map((item, index) => (
              isDisplayDot(item, index, dots.length)
            ))}
            <li onClick={() => { this.slider.slickNext(); }} style={{ opacity: '1' }}>
              <div
                className="page-number"
              >
                <i className="fa fa-chevron-right" />
              </div>
            </li>
          </ul>
        </div>
      ),
      customPaging: i => (
        <div
          className="page-number"
        >
          {i + 1}
        </div>
      ),
    };

    return (
      <div className="mobi--review-banner">
        <div className="row">
          <div className="col-12">
            {items && (
              <React.Fragment>
                <Slider
                  ref={slider => (this.slider = slider)}
                  {...sliderSetting}
                  afterChange={(index) => {
                    this.setState({
                      currentIndex: index,
                    });
                  }}
                >
                  {items.map(ele => ele.value).map((item, index) => (
                    <div key={index}>
                      <div className="mobi__item">
                        <div className="banner-image">
                          <img src={item.image ? item.image.image : ''} alt="" />
                        </div>
                        <div className="banner-content">
                          <h3 className="text-center">
                            {item.header ? item.header.header_text : ''}
                          </h3>
                          <div dangerouslySetInnerHTML={{
                            __html: item.description,
                          }}
                          />
                          <div className="banner-logo d-flex justify-content-center">
                            <img src={item.logo.image} alt="" />
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </Slider>
                <div className="banner-link d-flex justify-content-center">
                  <a type="button" className="readBtn" href={items[currentIndex] ? items[currentIndex].value.button.link : ''}>
                    {items[currentIndex] ? items[currentIndex].value.button.text : ''}
                  </a>
                </div>
                <div
                  className="down-btn d-flex justify-content-center"
                  onClick={() => {
                    window.scrollTo(0, 600);
                  }}
                >
                  <i className="fa fa-chevron-down mr-2 img-fluid" />
                </div>
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default BannerMobile;
