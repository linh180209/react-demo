import React, { Component } from 'react';
import moment from 'moment';
import _ from 'lodash';
import './styles.scss';

class ArticleMobile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: 1,
      totalPage: 0,
      pageSize: 2,
    };
  }

  componentDidUpdate(previousProps, previousState) {
    const { items } = this.props;
    if ((previousProps.items !== this.props.items) && items.feedbacks) {
      this.setState({
        totalPage: items.feedbacks.length % 2 === 0 ? parseInt(items.feedbacks.length / 2, 10) : parseInt(items.feedbacks.length / 2, 10) + 1,
      });
    }
  }

  handlePrev = () => {
    if (this.state.currentPage !== 1) {
      const { currentPage } = this.state;
      this.setState({
        currentPage: currentPage - 1,
      });
    }
  }

  handleNext = () => {
    if (this.state.currentPage !== this.state.totalPage) {
      const { currentPage } = this.state;
      this.setState({
        currentPage: currentPage + 1,
      });
    }
  }

  changePage = (pageNumber) => {
    if (pageNumber !== this.state.currentPage) {
      this.setState({
        currentPage: pageNumber,
      });
    }
  }


  render() {
    const { items } = this.props;
    const { currentPage, totalPage, pageSize } = this.state;

    return (
      <div className="mobi--review-article">
        <div className="row justify-content-center">
          {items && (
            <div className="__title d-flex flex-column justify-content-center">
              <h3>
                {items.header ? items.header.header_text : ''}
              </h3>
              <p>
                {items.text}
              </p>
            </div>
          )}
        </div>
        <div className="mobi--item-list">
          {items.feedbacks && _.slice(items.feedbacks.map(ele => ele.value), pageSize * (currentPage - 1), pageSize * (currentPage - 1) + 2).map(item => (
            <a className="__item" href={item.link}>
              <div className="cover">
                <img src={item.avatar.image} alt={item.avatar.caption} className="img-fluid" />
              </div>
              <div className="content d-flex flex-column justify-content-start">
                <p className="__item-name">
                  {item.name}
                </p>
                <p className="__item-content">
                  {item.text}
                </p>
                <p className="__item-date">
                  {moment(item.date).format('MMMM Do YYYY')}
                </p>
              </div>
            </a>
          ))}
          {items.feedbacks && (
            <div className="pagination d-flex flex-rows justify-content-center">
              <button type="button" className="__navBtn" onClick={this.handlePrev}>
                Prev
              </button>
              {_.times(totalPage, Number).map(index => (
                <button type="button" className={`pageBtn ${index === currentPage - 1 ? 'active' : ''}`} onClick={() => { this.changePage(index + 1); }}>
                  {index + 1}
                </button>
              ))}
              <button type="button" className="__navBtn" onClick={this.handleNext}>
                Next
              </button>
            </div>
          )}
        </div>
      </div >
    );
  }
}

export default ArticleMobile;
