import React, { Component } from 'react';
import '../../styles/style.scss';
import PropTypes, { element } from 'prop-types';
import { connect } from 'react-redux';

import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import '../../styles/review.scss';

// import HeaderHomePage from '../../components/HomePage/header';
import {
  scrollTop, fetchCMSHomepage, getSEOFromCms, googleAnalitycs, generateHreflang, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers/index';
import addCmsRedux from '../../Redux/Actions/cms';
import Press from './components/Press';
import Influencer from './components/Influencer';
import ReviewBanner from './components/ReviewBanner';
import BannerMobile from './components/BannerMobile';
import Article from './components/Article';
import ArticleMobile from './components/ArticleMobile';
import { isMobileOnly, isTablet } from '../../DetectScreen';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';


const getCms = (reviewCMS) => {
  const objectReturn = {
    carouselBlock: [],
    feedbackBlock: [],
  };
  if (!_.isEmpty(reviewCMS)) {
    const seo = getSEOFromCms(reviewCMS);
    const { body } = reviewCMS;
    if (body) {
      const carouselBlock = _.find(body, x => x.type === 'carousel_block').value;
      const feedbackBlock = _.filter(body, x => x.type === 'feedbacks_block').map(ele => ele.value);
      _.assign(objectReturn, { carouselBlock, feedbackBlock, seo });
    }
  }
  return objectReturn;
};

class Review extends Component {
  constructor(props) {
    super(props);

    this.influencerRef = React.createRef();

    this.state = {
      seo: undefined,
      minHeight: window.innerWidth <= 1366 ? 520 : 700,
      carouselBlock: [],
      feedbackBlock: [],
      visibleDetailMobile: false,
    };
  }

  componentDidMount = () => {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/reviews');
    this.fetchDataInit();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;

    const reviewCMS = _.find(cms, x => x.title === 'review');
    if (!reviewCMS) {
      const cmsData = await fetchCMSHomepage('review');
      this.props.addCmsRedux(cmsData);

      const dataFromStore = getCms(cmsData);
      this.setState(dataFromStore);
    } else {
      const dataFromStore = getCms(reviewCMS);
      this.setState(dataFromStore);
    }
  }

  render() {
    const {
      carouselBlock,
      feedbackBlock,
      seo,
      minHeight,
      visibleDetailMobile,
    } = this.state;


    const htmlWeb = (
      <div className="review">
        <React.Fragment>
          {isMobileOnly || isTablet
            ? <BannerMobile items={carouselBlock.length ? carouselBlock : []} />
            : <ReviewBanner items={carouselBlock.length ? carouselBlock : []} />
          }
          <Influencer items={feedbackBlock.length ? feedbackBlock[0] : {}} />
          {/* <Press items={feedbackBlock.length ? feedbackBlock[1] : {}} /> */}
          {/* {isMobileOnly || isTablet
            ? <ArticleMobile items={feedbackBlock.length ? feedbackBlock[2] : {}} />
            : <Article items={feedbackBlock.length ? feedbackBlock[2] : {}} />
          } */}
        </React.Fragment>
      </div>
    );
    return (
      <React.Fragment>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/reviews')}
        </MetaTags>
        {/* <HeaderHomePage />
        { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <HeaderHomePageV3 />
        {htmlWeb}
        <FooterV2 />
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  cms: state.cms,
  countries: state.countries,
  // showAskRegion: state.showAskRegion,
});

const mapDispatchToProps = {
  addCmsRedux,
};

Review.defaultProps = {
  match: {
    params: {
      review: '',
    },
  },
};

Review.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      review: PropTypes.string,
    }),
  }),
};

export default connect(mapStateToProps, mapDispatchToProps)(Review);
