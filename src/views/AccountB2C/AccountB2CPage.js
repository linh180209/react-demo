/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import _ from 'lodash';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import PropTypes from 'prop-types';
import MetaTags from 'react-meta-tags';

import addCmsRedux from '../../Redux/Actions/cms';
import AccountB2C from '../../components/Account/accountB2C';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import {
  scrollTop, getCmsCommon, fetchCMSHomepage, getSEOFromCms, googleAnalitycs, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers';
import IngredientPopUp from '../../components/IngredientDetail/ingredientPopUp';
import auth from '../../Redux/Helpers/auth';
import FooterV2 from '../../views2/footer';

const getCMS = (cms) => {
  if (!cms) {
    return { seo: {} };
  }
  const seo = getSEOFromCms(cms);
  return { seo };
};
class AccountB2CPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowIngredient: false,
      ingredientDetail: undefined,
      match: props.match,
    };
    const login = auth.getLogin();
    if (!login || !login.user || !login.user.id) {
      this.props.history.push(generateUrlWeb('/login'));
    }
  }

  state = {
    seo: {},
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const { match } = nextProps;
    if (match && match.params && match.params !== prevState.params) {
      return ({ params: match.params });
    }
    return null;
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/account');
    this.fetchCms();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchCms = () => {
    const { cms } = this.props;
    const cmsAccountPage = _.find(cms, x => x.title === 'AccountPage');
    if (!cmsAccountPage) {
      fetchCMSHomepage('accountpage').then((result) => {
        const cmsData = result;
        this.props.addCmsRedux(cmsData);
        const { seo } = getCMS(cmsData);
        this.setState({ seo });
      });
    } else {
      const { seo } = getCMS(cmsAccountPage);
      this.setState({ seo });
    }
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  render() {
    const { cms, countries } = this.props;
    const { seo, ingredientDetail, isShowIngredient } = this.state;
    const cmsCommon = getCmsCommon(cms);
    if (_.isEmpty(cmsCommon)) {
      return null;
    }
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(countries, '/account')}
        </MetaTags>
        {/* <HeaderHomePage />
        { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <HeaderHomePageV3 />

        <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
        />
        <AccountB2C
          cmsCommon={cmsCommon}
          onClickIngredient={this.onClickIngredient}
          addCmsRedux={this.props.addCmsRedux}
          cms={cms}
          params={this.state.params || {}}
        />
        <FooterV2 />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};

AccountB2CPage.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  history: PropTypes.shape().isRequired,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AccountB2CPage));
