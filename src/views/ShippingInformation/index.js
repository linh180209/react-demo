import React, { Component } from 'react';
import '../../styles/style.scss';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';

import MetaTags from 'react-meta-tags';

import TextBlock from '../../components/TextBlock';
import {
  convertHeaderToSize, scrollTop, getSEOFromCms, fetchCMSHomepage, googleAnalitycs, generateHreflang, removeLinkHreflang, setPrerenderReady,
} from '../../Redux/Helpers';
import Header from '../../components/HomePage/header';
import addCmsRedux from '../../Redux/Actions/cms';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';

const getCms = (faqBlock) => {
  if (faqBlock) {
    const seo = getSEOFromCms(faqBlock);
    const { header_text: headerText, header_size: headerSize, body } = faqBlock;
    const styleHeader = {
      fontSize: headerSize ? convertHeaderToSize(headerSize) : undefined,
    };
    return {
      headerText, styleHeader, blockTexts: body, seo,
    };
  }
  return {
    headerText: '', styleHeader: {}, blockTexts: [], seo: undefined,
  };
};
class ShippingInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      headerText: '',
      styleHeader: {},
      blockTexts: [],
      seo: undefined,
    };
  }


  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/shipping-infomation');

    this.fetchDataInit();
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const shipBlock = _.find(cms, x => x.title === 'Shipping Information');
    if (!shipBlock) {
      const cmsData = await fetchCMSHomepage('shipping-information');
      this.props.addCmsRedux(cmsData);
      const {
        headerText, styleHeader, blockTexts, seo,
      } = getCms(cmsData);
      this.setState({
        headerText, styleHeader, blockTexts, seo,
      });
    } else {
      const {
        headerText, styleHeader, blockTexts, seo,
      } = getCms(shipBlock);
      this.setState({
        headerText, styleHeader, blockTexts, seo,
      });
    }
  }

  render() {
    const {
      headerText, styleHeader, blockTexts, seo,
    } = this.state;
    return (
      <div>
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/shipping-infomation')}
        </MetaTags>
        {/* <Header />
        { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <HeaderHomePageV3 />
        <TextBlock headerText={headerText} styleHeader={styleHeader} blockTexts={blockTexts} />
        <FooterV2 />
      </div>
    );
  }
}

ShippingInformation.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
    // showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};

export default connect(mapStateToProps, mapDispatchToProps)(ShippingInformation);
