import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import HeaderHomePage from '../../components/HomePage/header';
import { generateUrlWeb } from '../../Redux/Helpers';
import LoginRegister from './loginRegister';
import './styles.scss';

function LoginPage(props) {
  const history = useHistory();
  const showAskRegion = useSelector(state => state.showAskRegion);
  const login = useSelector(state => state.login);

  useEffect(() => {
    if (login?.user?.id) {
      history.push(generateUrlWeb('/account'));
    }
  }, [login]);

  return (
    <div>
      <HeaderHomePage isProductPage isShowAskRegion isHiddenAccount />
      { showAskRegion && (<div className="div-temp-region" />) }
      <div className="div-login-page">
        <LoginRegister isPageLogin history={history} />
      </div>
    </div>
  );
}

export default LoginPage;
