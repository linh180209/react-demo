import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import { Card, CardBody } from 'reactstrap';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';
import { connect } from 'react-redux';
import _ from 'lodash';
import { SwipeableDrawer } from '@material-ui/core';

// import ReactGA from 'react-ga';
import moment from 'moment';
import icClose from '../../image/icon/ic-close-menu.svg';
import icCloseW from '../../image/icon/ic-close-menu-w.svg';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import {
  validateEmail, postPointRedeem, isCheckNull, trackGTMLogin, trackGTMSignUp, fetchCMSHomepage, getSEOFromCms, getNameFromButtonBlock, handleDownloadFile, generateUrlWeb, gotoShopHome,
} from '../../Redux/Helpers';
import {
  SIGN_UP_URL, LOGIN_URL, LOGIN_FACEBOOK_URL, LOGIN_GOOGLE_URL,
  CREATE_BASKET_URL, GET_BASKET_URL, PUT_OUTCOME_URL, ASSIGN_BASKET_URL,
} from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import loadingPage from '../../Redux/Actions/loading';
import { loginCompleted } from '../../Redux/Actions/login';
import { loadAllBasket } from '../../Redux/Actions/basket';
import auth from '../../Redux/Helpers/auth';
import '../../styles/_login.scss';
import facebook from '../../image/facebook.png';
import google from '../../image/google.png';
import addCmsRedux from '../../Redux/Actions/cms';
import { isMobile } from '../../DetectScreen';

const getCMS = (cms) => {
  if (!cms) {
    return { seo: {}, buttonBlock: [] };
  }
  const seo = getSEOFromCms(cms);
  const { body } = cms;
  const buttonBlock = _.filter(body, x => x.type === 'button_block');
  return {
    seo, buttonBlock,
  };
};

class LoginRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoginComponent: true,
      buttonBlock: [],
      isShown: false,
    };

    this.data = {
      email: '',
      password: '',
      firtName: '',
      lastName: '',
      rEmail: '',
      rPassword1: '',
      rPassword2: '',
    };
  }

  componentDidMount() {
    this.fetchCms();
    const { showRegister } = this.props;
    if (showRegister) {
      this.setState({
        isLoginComponent: false,
      });
    }
  }

  fetchCms = () => {
    const { cms } = this.props;
    const cmsLoginPage = _.find(cms, x => x.title === 'Login Page');
    if (!cmsLoginPage) {
      fetchCMSHomepage('login-page').then((result) => {
        this.props.addCmsRedux(result);
        const cmsData = getCMS(result);
        this.setState(cmsData);
      });
    } else {
      const cmsData = getCMS(cmsLoginPage);
      this.setState(cmsData);
    }
  }

  onSubmit = (e) => {
    console.log('onSubmit');
    e.preventDefault();
    if (this.isValid(this.data)) {
      const { email: userName, password: passWord } = this.data;
      this.loginApi(userName, passWord);
    }
  }

  onSubmitRegister = (e) => {
    console.log('onSubmitRegister');
    e.preventDefault();
    if (this.isValidRegister(this.data)) {
      this.signUp(this.data);
    }
  }

  onChange = (e) => {
    const { value, name } = e.target;
    this.data[name] = value;
    this.forceUpdate();
  };

  getBasket = (id) => {
    const options = {
      url: GET_BASKET_URL.replace('{cartPk}', id),
      method: 'GET',
    };
    return fetchClient(options, true);
  }

  assignBasketId = (cart, basket) => {
    const items = [];
    _.forEach(basket.items, (x) => {
      items.push(x.id);
    });
    const options = {
      url: ASSIGN_BASKET_URL.replace('{id}', cart),
      method: 'POST',
      body: {
        items,
      },
    };
    return fetchClient(options, true);
  }

  updateOutComeUser = (user, outcome) => {
    if (isCheckNull(outcome)) {
      return;
    }
    const options = {
      url: PUT_OUTCOME_URL.replace('{id}', outcome),
      method: 'PUT',
      body: {
        user: parseInt(user, 10),
      },
    };
    fetchClient(options);
  }

  gotoForgotPassword = () => {
    this.props.history.push(generateUrlWeb('/forgotPassword'));
  }

  isValid = (data) => {
    const { buttonBlock } = this.state;
    const { email, password } = data;
    if (!email || !password) {
      toastrError(getNameFromButtonBlock(buttonBlock, 'Please_fill_all_information'));
      return false;
    }
    if (!validateEmail(email)) {
      toastrError(getNameFromButtonBlock(buttonBlock, 'Please_enter_a_valid_email_address'));
      return false;
    }
    return true;
  }

  isValidRegister = (data) => {
    const { buttonBlock } = this.state;
    const {
      firtName, lastName, rEmail, rPassword1, rPassword2,
    } = data;
    if (!firtName || !lastName || !rEmail || !rPassword1 || !rPassword2) {
      toastrError(getNameFromButtonBlock(buttonBlock, 'Please_fill_all_information'));
      return false;
    }
    if (!validateEmail(rEmail)) {
      toastrError(getNameFromButtonBlock(buttonBlock, 'Please_enter_a_valid_email_address'));
      return false;
    }
    if (rPassword1 !== rPassword2) {
      toastrError(getNameFromButtonBlock(buttonBlock, 'Your_password_and_repeat_password_do_not_match'));
      return false;
    }
    return true;
  }

  createBasket = (user) => {
    const { id } = user;
    const options = {
      url: CREATE_BASKET_URL.replace('{userId}', id),
      method: 'POST',
      body: {},
    };
    return fetchClient(options, true);
  }

  handleAfterLogin = (result) => {
    auth.login(result);
    const outcome = auth.getOutComeId();
    if (outcome && !result.user.outcome) {
      result.user.outcome = outcome;
      this.updateOutComeUser(result.user.id, outcome);
    }

    this.props.loginCompleted(result);
    const { user } = result;
    if (this.props.basket && this.props.basket.id && this.props.basket.items && this.props.basket.items.length > 0) {
      this.assignBasketId(result.user.cart, this.props.basket).then((res) => {
        if (res) {
          this.handleLoginSuccessful(res, user);
        } else {
          throw new Error();
        }
      }).catch((err) => {
        toastrError(err.message);
        this.props.loadingPage(false);
      });
      return;
    }
    if (user.cart) {
      this.getBasket(user.cart).then((basket) => {
        this.handleLoginSuccessful(basket, user);
      });
      return;
    }
    this.createBasket(user).then((basket) => {
      this.handleLoginSuccessful(basket, user);
    });
  }

  handleLoginSuccessful = (basket, user) => {
    const { buttonBlock } = this.state;
    toastrSuccess(getNameFromButtonBlock(buttonBlock, 'You_are_login_successfully'));
    this.props.loadAllBasket(basket);
    if (user.is_b2b) {
      this.props.history.push(generateUrlWeb('/b2b/landing'));
    } else if (this.props.isPageLogin) {
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    } else {
      this.props.onClose();
    }
    this.props.loadingPage(false);
  }

  loginApi = (email, passWord) => {
    console.log('loginApi', email, passWord);
    this.props.loadingPage(true);
    const body = {
      email,
      password: passWord,
    };
    const options = {
      url: LOGIN_URL,
      method: 'POST',
      body,
    };
    fetchClient(options).then((result) => {
      console.log('result---', result);
      if (!result.isError && !result.message) {
        // tracking GTM
        trackGTMLogin('normal', result.user.id);

        if (!result.user.is_b2b) {
          this.handleAfterLogin(result);
        } else {
          toastrError('You are a partner, you can not login there');
          this.props.loadingPage(false);
        }
      } else {
        console.log('result', result.message);
        throw new Error(result.message);
      }
    }).catch((e) => {
      console.log('erro', e);
      toastrError(e.message);
      this.props.loadingPage(false);
    });
  }

  responseFacebook = (response) => {
    console.log(response);
    const { accessToken } = response;
    if (accessToken) {
      // const urlAvatar = handleDownloadFile('http://graph.facebook.com/108326343695312/picture?width=256&height=256&access_token=EAAD7DYx6D6sBANOrmEyGtVcer2r1RZBN4N3Tj6IihCo0TZA3qQVErbCVeslwTEkWUETdxiO22GHZAjBSAWLT0mzdOZA5ZC0joN1E0OscwoXyzNhlZCqvLErzhYGXz6ZBrUEU0s50ZBmiaejZCmZAoxolB0ZBD4v9stXFoz86ci3cOrBuWPGRpQRhrfZC5UZCqYB9z9qQ2RYC49yCPNWmiCrnG1pfJ');
      // auth.setUrlAvatar(urlAvatar);
      this.loginSocial(accessToken, LOGIN_FACEBOOK_URL);
    }
  }

  responseGoogle = (response) => {
    console.log(response);
    const { accessToken } = response;
    if (accessToken) {
      this.loginSocial(accessToken, LOGIN_GOOGLE_URL);
    }
  }

  loginSocial = (accessToken, url) => {
    const options = {
      url,
      method: 'POST',
      body: {
        access_token: accessToken,
      },
    };
    fetchClient(options).then((result) => {
      if (!result.isError) {
        // this.props.loginCompleted(result);
        // toastrSuccess('You are login successfully');
        // this.props.onClose();

        // tracking GTM
        if (url === LOGIN_GOOGLE_URL) {
          if (moment().valueOf() - moment(result.user.date_created).valueOf() < 5 * 60 * 1000) {
            trackGTMSignUp('google', result.user.id);
          } else {
            trackGTMLogin('google', result.user.id);
          }
        } else if (url === LOGIN_FACEBOOK_URL) {
          if (moment().valueOf() - moment(result.user.date_created).valueOf() < 5 * 60 * 1000) {
            trackGTMSignUp('facebook', result.user.id);
          } else {
            trackGTMLogin('facebook', result.user.id);
          }
        }

        if (!_.isEmpty(this.props.refPoint)) {
          postPointRedeem(this.props.refPoint);
        }
        this.handleAfterLogin(result);
      } else {
        throw new Error(result.message);
      }
    }).catch((e) => {
      toastrError(e.message);
    });
  }

  toggleButton = () => {
    const { isLoginComponent } = this.state;
    this.setState({ isLoginComponent: !isLoginComponent });
  }

  signUp = (data) => {
    const {
      firtName, lastName, rEmail, rPassword1,
    } = data;
    const options = {
      url: SIGN_UP_URL,
      method: 'POST',
      body: {
        first_name: firtName,
        last_name: lastName,
        email: rEmail,
        password: rPassword1,
      },
    };
    fetchClient(options).then((result) => {
      console.log('result', result);
      if (!result.isError) {
        // this.loginApi(rEmail, rPassword1);
        // tracking GTM
        trackGTMSignUp('normal', result.user_id);

        toastrSuccess('Please, verification email!');
        if (!_.isEmpty(this.props.refPoint)) {
          postPointRedeem(this.props.refPoint);
        }
        if (this.props.isPageLogin) {
          // this.props.history.push(generateUrlWeb('/'));
          gotoShopHome();
        } else {
          this.props.onClose();
        }
        return;
      }
      throw (new Error(result.message));
    }).catch((e) => {
      toastrError(e.message);
    });
  }

  render() {
    const { isLoginComponent, buttonBlock } = this.state;
    const { urlRedirect } = this.props;
    const {
      email, password, firtName, lastName, rEmail, rPassword1, rPassword2,
    } = this.data;
    const emailBt = getNameFromButtonBlock(buttonBlock, 'Email');
    const passBt = getNameFromButtonBlock(buttonBlock, 'Password');
    const forgotBt = getNameFromButtonBlock(buttonBlock, 'Forgot_your_password');
    const loginBt = getNameFromButtonBlock(buttonBlock, 'LOGIN');
    const orConnectBt = getNameFromButtonBlock(buttonBlock, 'or_connect_with');
    const firstNameBt = getNameFromButtonBlock(buttonBlock, 'First_Name');
    const lastNameBt = getNameFromButtonBlock(buttonBlock, 'Last_Name');
    const emailAddressBt = getNameFromButtonBlock(buttonBlock, 'Email_Address');
    const confirmPassBt = getNameFromButtonBlock(buttonBlock, 'Confirm_Password');
    const createAccountBt = getNameFromButtonBlock(buttonBlock, 'CREATE_ACCOUNT');
    const BonjourBt = getNameFromButtonBlock(buttonBlock, 'Bonjour');
    const joinBt = getNameFromButtonBlock(buttonBlock, 'Join_the_Family');
    const dontAccountBt = getNameFromButtonBlock(buttonBlock, 'Dont_have_an_Account');
    const registerBt = getNameFromButtonBlock(buttonBlock, 'Register');
    const alreadyHaveAccountBt = getNameFromButtonBlock(buttonBlock, 'Already_have_an_Account');
    const joinNowBt = getNameFromButtonBlock(buttonBlock, 'JOIN_NOW');
    const loginBt1 = getNameFromButtonBlock(buttonBlock, 'Login');
    const logintoYouBt = getNameFromButtonBlock(buttonBlock, 'Login_to_your_account');
    const registerAnAccountBt = getNameFromButtonBlock(buttonBlock, 'Register_an_Account');
    const loginHtml = (
      <form onSubmit={this.onSubmit}>
        <div className="login-content">
          <div className="group">
            <input
              className="input-material input_underline"
              type="text"
              name="email"
              value={email}
              onChange={this.onChange}
              required
            />
            <label className="label-material">
              {emailBt}
            </label>
          </div>
          <div className="group">
            <input
              className="input-material input_underline"
              type="password"
              name="password"
              value={password}
              onChange={this.onChange}
              required
            />
            <label className="label-material">
              {passBt}
            </label>
          </div>

          <div className="div-bt-login">
            <button
              type="submit"
              onClick={this.onSubmit}
            >
              {loginBt}
            </button>
          </div>

          <div className="text-register">
            <span>
              {dontAccountBt}
            </span>
            <button
              onClick={this.toggleButton}
              type="button"
              className="button-bg__none"
            >
              {registerBt}
            </button>
          </div>
          <div className="forgot_pwd">
            <Link
              to={generateUrlWeb('/forgotPassword')}
            >
              {forgotBt}
            </Link>
          </div>
          <hr />
          <div className="button-login">
            <div className="d-flex justify-content-between">
              <div className="text-uppercase login-social">
                {orConnectBt}
              </div>
            </div>
            <div className="d-flex justify-content-between">
              <FacebookLogin
                appId="276035609956267"
                callback={this.responseFacebook}
                render={renderProps => (
                  <div className="icon-social facebook" onClick={renderProps.onClick}>
                    <a
                      href
                      onClick={(e) => {
                        e.preventDefault();
                        renderProps.onClick();
                      }}
                    >
                      <img src={facebook} alt="" />
                    </a>
                  </div>
                )}
              />
              <GoogleLogin
                clientId="1051429470092-1ktl08tfm23q3b7i2au9ivv6pnil51g1.apps.googleusercontent.com"
                onSuccess={this.responseGoogle}
                onFailure={this.onFailure}
                render={renderProps => (
                  <div className="icon-social" onClick={renderProps.onClick}>
                    <a
                      href
                      onClick={(e) => {
                        e.preventDefault();
                        renderProps.onClick();
                      }}
                    >
                      <img src={google} alt="" />
                    </a>
                  </div>
                )}
              />
            </div>
          </div>
        </div>
      </form>
    );
    const registerHtml = (
      <form onSubmit={this.onSubmitRegister}>
        <div className="register-content">
          <div className="group">
            <input
              className="input-material input_underline"
              type="text"
              name="firtName"
              value={firtName}
              onChange={this.onChange}
              required
            />
            <label className="label-material">
              {firstNameBt}
            </label>
          </div>
          <div className="group">
            <input
              className="input-material input_underline"
              type="text"
              name="lastName"
              value={lastName}
              onChange={this.onChange}
              required
            />
            <label className="label-material">
              {lastNameBt}
            </label>
          </div>
          <div className="group">
            <input
              className="input-material input_underline"
              type="text"
              name="rEmail"
              value={rEmail}
              onChange={this.onChange}
              required
            />
            <label className="label-material">
              {emailAddressBt}
            </label>
          </div>
          <div className="group">
            <input
              className="input-material input_underline"
              type="password"
              name="rPassword1"
              value={rPassword1}
              onChange={this.onChange}
              required
            />
            <label className="label-material">
              {passBt}
            </label>
          </div>
          <div className="group">
            <input
              className="input-material input_underline"
              type="password"
              name="rPassword2"
              value={rPassword2}
              onChange={this.onChange}
              required
            />
            <label className="label-material">
              {confirmPassBt}
            </label>
          </div>
          <div className="div-bt-login">
            <button
              type="submit"
              onClick={this.onSubmitRegister}
            >
              {createAccountBt}
            </button>
          </div>

          <div className="text-register">
            <span>
              {alreadyHaveAccountBt}
            </span>
            <button
              onClick={this.toggleButton}
              type="button"
              className="button-bg__none"
            >
              {loginBt1}
            </button>
          </div>
          <div className="forgot_pwd">
            <Link
              to={generateUrlWeb('/forgotPassword')}
            >
              {forgotBt}
            </Link>
          </div>
          <hr />

          <div className="button-login">
            <div className="d-flex justify-content-between">
              <div className="text-uppercase login-social">
                {orConnectBt}
              </div>
            </div>
            <div className="d-flex justify-content-between">
              <FacebookLogin
                appId="276035609956267"
                callback={this.responseFacebook}
                render={renderProps => (
                  <div className="icon-social facebook" onClick={renderProps.onClick}>
                    <img src={facebook} alt="" />
                  </div>
                )}
              />
              <GoogleLogin
                clientId="1051429470092-1ktl08tfm23q3b7i2au9ivv6pnil51g1.apps.googleusercontent.com"
                onSuccess={this.responseGoogle}
                onFailure={this.onFailure}
                render={renderProps => (
                  <div className="icon-social" onClick={renderProps.onClick}>
                    <img src={google} alt="" />
                  </div>
                )}
              />
            </div>
          </div>
        </div>
      </form>
    );
    const contentHtml = (
      <div
        className="div-drawer-login"
      >
        <div className="login_header">
          {isLoginComponent ? BonjourBt : joinBt}
        </div>
        <div className="sub-header">
          {isLoginComponent ? logintoYouBt : registerAnAccountBt}
        </div>
        <button
          onMouseEnter={() => !isMobile && this.setState({ isShown: true })}
          onMouseLeave={() => !isMobile && this.setState({ isShown: false })}
          className="button-close button-bg__none"
          type="button"
          onClick={this.props.onClose}
        >
          <img src={this.state.isShown ? icCloseW : icClose} alt="icon" />
        </button>
        <div className="w-100 content-form">
          {isLoginComponent ? loginHtml : registerHtml}
        </div>
      </div>
    );
    if (this.props.isPageLogin) {
      return (
        contentHtml
      );
    }
    return (
      <SwipeableDrawer
        anchor="right"
        open={this.props.isOpen}
        onClose={() => this.props.onClose()}
      >
        {contentHtml}
      </SwipeableDrawer>
    );
  }
}

const mapDispatchToProps = {
  loginCompleted,
  loadingPage,
  loadAllBasket,
  addCmsRedux,
};

function mapStateToProps(state) {
  return {
    refPoint: state.refPoint,
    cms: state.cms,
    basket: state.basket,
  };
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginRegister));
