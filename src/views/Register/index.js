import React, { Component } from 'react';


import Register from '../../components/HomePage/register';
import HeaderHomePage from '../../components/HomePage/header';
import { googleAnalitycs } from '../../Redux/Helpers';

class RegisterPage extends Component {
  componentDidMount = () => {
    googleAnalitycs('/register');
  };

  onOpenMenu = () => {
  }

  render() {
    return (
      <div>
        <div className="div-homepage">
          <HeaderHomePage onOpen={this.onOpenMenu} />
          <Register />
        </div>
      </div>
    );
  }
}

export default RegisterPage;
