import _ from 'lodash';
import moment from 'moment';
import {
  GET_BASKET_URL, GUEST_CREATE_BASKET_URL, POST_OUTCOMES_URL, PUT_OUTCOME_URL,
} from '../../config';
import AppFlowActions from '../../constants';
import {
  fetchAnswers, fetchAnswersFerrari, fetchCMSHomepage, fetchQuestions, fetchQuestionsFerrari, getSEOFromCms, trackGTMSubmitEmail,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import emitter from '../../Redux/Helpers/eventEmitter';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';

export const fetchDataInit = () => {
  const pending = [fetchAnswers(), fetchQuestions(), fetchCMSHomepage('personality')];
  return Promise.all(pending);
};

export const fetchDataInitV2 = () => {
  const pending = [fetchAnswers(), fetchQuestions(), fetchCMSHomepage('personality-v2')];
  return Promise.all(pending);
};

export const fetchDataInitFerrari = () => {
  const pending = [fetchAnswersFerrari(), fetchQuestionsFerrari(), fetchCMSHomepage('personality-ferrari')];
  return Promise.all(pending);
};

export const handleCMSPage = (cmsPage) => {
  const seo = getSEOFromCms(cmsPage);
  const buttonBlocks = _.filter(cmsPage?.body || [], x => x.type === 'button_block');
  const videoBlocks = _.filter(cmsPage?.body || [], x => x.type === 'video_block');
  const ctaBlock = _.find(cmsPage?.body || [], x => x.type === 'cta_block');
  const headerAndParagraphBocks = _.filter(cmsPage?.body || [], x => x.type === 'header_and_paragraph_block');
  const ctaBlocks = _.filter(cmsPage?.body || [], x => x.type === 'cta_block');
  const paragraphBlock = _.filter(cmsPage?.body, x => x.type === 'paragraph_block');
  const imagetxtBlock = _.find(cmsPage?.body, x => x.type === 'imagetxt_block');
  const bannerBlocks = _.filter(cmsPage?.body, x => x.type === 'banner_block');
  const imagetxtBlocks = _.filter(cmsPage?.body, x => x.type === 'imagetxt_block');
  return {
    seo,
    buttonBlocks,
    videoBlocks,
    ctaBlock,
    imageBackground: cmsPage?.image_background,
    headerText: cmsPage?.header_text,
    paragraphBlock,
    imagetxtBlock,
    bannerBlocks,
    ctaBlocks,
    imagetxtBlocks,
    headerAndParagraphBocks,
  };
};

export const getPathNamePage = (path) => {
  if (path.includes('/boutique-quiz') || path.includes('/boutique-quiz-v2')) {
    return 'boutique-quiz';
  } if (path.includes('/partnership-quiz')) {
    return 'partnership-quiz';
  } if (path.includes('/krisshop')) {
    return 'krisshop';
  } if (path.includes('/sephora')) {
    return 'sephora';
  } if (path.includes('/sofitel')) {
    return 'sofitel';
  } if (path.includes('/bmw')) {
    return 'bmw';
  } if (path.includes('/lazada')) {
    return 'lazada';
  } if (path.includes('/ae-quiz') || path.includes('/quiz') || path.includes('/quizv2') || path.includes('/quiz-diy') || path.includes('/quizv2-diy') || path.includes('/quizv3') || path.includes('/quizv4') || path.includes('/quiz-ferrari')) {
    return 'quiz';
  }
  return null;
};

export const getSlug = (namePath) => {
  if (namePath === 'krisshop') {
    return 'krisshop';
  }
  if (namePath === 'partnership-quiz') {
    const { match } = this.props;
    const { slug } = match.params;
    return slug;
  }
};

export const getLinkGoogleAnalitycs = namePath => (namePath === 'quiz' ? '/quiz' : namePath === 'boutique-quiz' ? '/boutique-quiz' : namePath === 'sephora' ? '/sephora' : namePath === 'sofitel' ? '/sofitel' : namePath === 'bmw' ? '/bmw' : namePath === 'partnership-quiz' ? '/partnership-quiz' : '/krisshop/');

export const initOutCome = async (namePath, slug) => {
  try {
    const option = {
      url: POST_OUTCOMES_URL,
      method: 'POST',
      body: {
        is_boutique: namePath === 'boutique-quiz',
        partnership: ['sofitel', 'sephora', 'bmw', 'lazada'].includes(namePath) ? namePath : slug,
      },
    };
    const result = await fetchClient(option, true);
    // this.idOutCome = result.id;
    return result;
  } catch (error) {
    toastrError(error.message);
  }
};

export const geneateBody = (dataAll, namePath, slug) => ({
  is_boutique: namePath === 'boutique-quiz',
  partnership: ['sofitel', 'sephora', 'bmw', 'lazada'].includes(namePath) ? namePath : slug,
  age: dataAll?.age,
  date_of_birth: dataAll?.dob ? parseInt(moment(dataAll?.dob, 'MM/DD/YYYY').valueOf() / 1000, 10) : undefined,
  skin_tone: dataAll?.skinTone,
  gender: dataAll?.gender ? parseInt(dataAll?.gender, 10) : undefined,
  answers: dataAll?.resultYesNo,
  external_products: _.map(dataAll?.externalProducts || [], d => d?.id),
  mixes: _.map(dataAll?.mixes || [], d => parseInt(d?.id, 10)),
  strength: dataAll?.strength,
  families: _.map(dataAll?.families || [], d => d?.id),
});

export const assignEmailToCart = (email, basket, loadAllBasket) => {
  if (!basket.id) {
    const options = {
      url: GUEST_CREATE_BASKET_URL,
      method: 'POST',
      body: {
        subtotal: 0,
        total: 0,
        email,
      },
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        loadAllBasket(result);
      }
    });
    return;
  }
  const option = {
    url: GET_BASKET_URL.replace('{cartPk}', basket.id),
    method: 'PUT',
    body: {
      email,
    },
  };
  fetchClient(option);
};

export const updateEmailToOutcome = (idOutCome, email) => {
  const option = {
    url: PUT_OUTCOME_URL.replace('{id}', idOutCome),
    method: 'PUT',
    body: {
      email,
      send_email: true,
    },
  };
  fetchClient(option).then((result) => {
    if (result && !result.isError) {
      auth.setEmail(email);
      trackGTMSubmitEmail(email);
      toastrSuccess('Sending Email is successful!');
      return;
    }
    throw new Error('Sending Email is failed!');
  }).catch((err) => {
    toastrError(err.message);
  });
};

export const generateOutComeUrlForQuiz = () => {
  console.log('window.location.pathname', window.location.pathname);
  if (window.location.pathname.includes('/quiz-diy')) {
    return '/quiz-diy/outcome/';
  } if (window.location.pathname.includes('/quizv2-diy')) {
    return '/quizv2-diy/outcome/';
  } if (window.location.pathname.includes('/quizv2')) {
    return '/quizv2/outcome/';
  } if (window.location.pathname.includes('/quizv3')) {
    return '/quizv3/outcome/';
  } if (window.location.pathname.includes('/quizv4')) {
    return '/quizv4/outcome/';
  } if (window.location.pathname.includes('/quiz') || window.location.pathname.includes('/ae-quiz') || window.location.pathname.includes('/quiz-ferrari')) {
    return '/quiz/outcome/';
  }
};

export const emitChangeQuestion = (index) => {
  emitter.emit(AppFlowActions.CHANGE_QUESTIONS, index);
};
