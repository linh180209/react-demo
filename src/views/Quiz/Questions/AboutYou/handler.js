import _ from 'lodash';
import icQ10 from '../../../../image/icon/ic-q-1-0.svg';
import icQ11 from '../../../../image/icon/ic-q-1-1.svg';
import icQ20 from '../../../../image/icon/ic-q-2-0.svg';
import icQ21 from '../../../../image/icon/ic-q-2-1.svg';
import icQ30 from '../../../../image/icon/ic-q-3-0.svg';
import icQ31 from '../../../../image/icon/ic-q-3-1.svg';
import icQ40 from '../../../../image/icon/ic-q-4-0.svg';
import icQ41 from '../../../../image/icon/ic-q-4-1.svg';
import icQ50 from '../../../../image/icon/ic-q-5-0.svg';
import icQ51 from '../../../../image/icon/ic-q-5-1.svg';

import ic10 from '../../../../imagev2/svg/ic-question-1-0.svg';
import ic11 from '../../../../imagev2/svg/ic-question-1-1.svg';
import ic20 from '../../../../imagev2/svg/ic-question-2-0.svg';
import ic21 from '../../../../imagev2/svg/ic-question-2-1.svg';
import ic30 from '../../../../imagev2/svg/ic-question-3-0.svg';
import ic31 from '../../../../imagev2/svg/ic-question-3-1.svg';
import ic40 from '../../../../imagev2/svg/ic-question-4-0.svg';
import ic41 from '../../../../imagev2/svg/ic-question-4-1.svg';
import ic50 from '../../../../imagev2/svg/ic-question-5-0.svg';
import ic51 from '../../../../imagev2/svg/ic-question-5-1.svg';

const gtm = [
  'funnel-1-step-06-secure-or-adventurous',
  'funnel-1-step-07-care-or-lead',
  'funnel-1-step-08-tradition-novelty',
  'funnel-1-step-09-fun-success',
  'funnel-1-step-10-organize-surpise',
];

const listIconV4 = [[
  ic10,
  ic20,
  ic30,
  ic40,
  ic50,
], [
  ic11,
  ic21,
  ic31,
  ic41,
  ic51,
]];

const listIcon = [[
  icQ10,
  icQ20,
  icQ30,
  icQ40,
  icQ50,
], [
  icQ11,
  icQ21,
  icQ31,
  icQ41,
  icQ51,
]];

export const formatQuestion = (parent, index, isQuizv4) => ({
  name: index,
  title: parent[0].question,
  type: 'Question-choose',
  idIndex: `id-question-choose-${index}`,
  numberQuestion: `${4 + index}/12`,
  answer1: parent[0].title,
  answer2: parent[1].title,
  idAnswer1: parent[0].id,
  idAnswer2: parent[1].id,
  icon1: isQuizv4 ? listIconV4[0][index] : listIcon[0][index],
  icon2: isQuizv4 ? listIconV4[1][index] : listIcon[1][index],
  idIndexNext: index === 4 ? 'id-scent-preferences' : `id-question-choose-${index + 1}`,
  dataGtmtracking: gtm[index],
});

export const generateQuestions = (answers, resultYesNo, no, isQuizv4) => {
  console.log('onClickYesNo', {
    answers, resultYesNo, no, isQuizv4,
  });
  // clear result when chose again
  _.forEach(resultYesNo, (d, index) => {
    if (index >= no) {
      _.remove(resultYesNo, d);
    }
  });

  const listQuestion = [];
  let temp = answers;
  listQuestion.push(formatQuestion(temp, 0, isQuizv4));
  temp = { children: temp };
  for (let i = 0; i < 4; i += 1) {
    if (no > i) {
      temp = _.find(temp.children, x => x.id === resultYesNo[i]);
    } else {
      temp = temp.children[0];
    }
    listQuestion.push(formatQuestion(temp.children, i + 1, isQuizv4));
  }
  return listQuestion;
};
