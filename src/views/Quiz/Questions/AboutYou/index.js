import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { useSelector, useDispatch } from 'react-redux';
import { useMergeState } from '../../../../Utils/customHooks';
import QuestionType from '../QuestionType';
import { getNameFromButtonBlock, getNameFromCommon, scrollToTargetAdjusted } from '../../../../Redux/Helpers';
import { generateQuestions } from './handler';
import { emitChangeQuestion } from '../../handler';

function AboutYou(props) {
  const answers = useSelector(state => state.answers);
  const [state, setState] = useMergeState({
    gender: undefined,
    resultYesNo: [],
  });
  const [listQuestion, setListQuestion] = useState([]);

  const scrollToNext = (id) => {
    setTimeout(() => {
      scrollToTargetAdjusted(id);
    }, 200);
  };

  const updateStepQuestion = (step) => {
    if (step === 7 || step < props.stateParent?.stepQuestion) {
      props.updateStepQuestion(step);
    }
    // increate index question
    _.assign(props.stateParent, { stepQuestion: step + 1 });
    emitChangeQuestion(step + 1);
  };

  const onClick = (name, value, idIndex) => {
    console.log('onClick', name, value, idIndex);

    // increate index question
    updateStepQuestion(idIndex);

    setState({ [name]: value });
    _.assign(props.dataAll, { [name]: value });

    scrollToNext(`step-${idIndex + 1}`);

    // update data
    props.updateRealTime();
  };

  const onClickYesNo = (name, value, idIndex) => {
    if (state.resultYesNo.length > parseInt(name, 10)) {
      state.resultYesNo[parseInt(name, 10)] = value;
    } else {
      state.resultYesNo.push(value);
    }

    // generate again question
    const list = generateQuestions(answers, state.resultYesNo, parseInt(name, 10) + 1);
    setListQuestion(list);

    // increate index question
    updateStepQuestion(idIndex);

    setState({ resultYesNo: state.resultYesNo });
    _.assign(props.dataAll, { resultYesNo: state.resultYesNo });

    if (idIndex === 7) {
      scrollToNext('id-header-scent-preferences');
    } else {
      scrollToNext(`step-${idIndex + 1}`);
    }

    // update data
    props.updateRealTime();
  };

  useEffect(() => {
    if (answers?.length > 0) {
      const list = generateQuestions(answers, state.resultYesNo, 0);
      setListQuestion(list);
    }
  }, [answers]);

  const { stepQuestion } = props.stateParent || {};
  const aboutYouBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'ABOUT YOU');
  const pleaseSelectOneBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'PLEASE SELECT ONE');
  const whatIsYourGenderBt = getNameFromCommon(props.cmsCommon, 'What_is_your_gender');
  const whatIsYourSkinToneBt = getNameFromCommon(props.cmsCommon, 'What_is_your_skin-tone');
  const whatIsYourNameBt = getNameFromCommon(props.cmsCommon, 'When is your birthday');
  return (
    <div id="id-about-you" className="about-you">
      <div id="id-header-about-you" className="index-step" />
      <div className="header-question-item">
        {aboutYouBt}
      </div>
      <QuestionType
        buttonBlocks={props.cmsState?.buttonBlocks}
        cmsCommon={props.cmsCommon}
        type="Gender"
        name="gender"
        onClick={onClick}
        dataSelected={state.gender}
        idIndex={0}
        title={whatIsYourGenderBt}
        numberQuestion="1/12"
        isShow
        isCurrentQuestion={stepQuestion === 0}
      />
      <QuestionType
        buttonBlocks={props.cmsState?.buttonBlocks}
        cmsCommon={props.cmsCommon}
        type="Skin-tone"
        name="skinTone"
        idIndex={1}
        dataSelected={state.skinTone}
        onClick={onClick}
        title={whatIsYourSkinToneBt}
        subTitle={pleaseSelectOneBt}
        numberQuestion="2/12"
        isShow={stepQuestion > 0}
        isCurrentQuestion={stepQuestion === 1}
      />
      <QuestionType
        buttonBlocks={props.cmsState?.buttonBlocks}
        cmsCommon={props.cmsCommon}
        type="Birthday"
        name="dob"
        idIndex={2}
        dataSelected={state.dob}
        onClick={onClick}
        title={whatIsYourNameBt}
        numberQuestion="3/12"
        isShow={stepQuestion > 1}
        isCurrentQuestion={stepQuestion === 2}
      />
      {
        _.map(listQuestion, (d, index) => (
          <QuestionType
            type="Question-choose"
            title={d.title}
            idIndex={3 + index}
            name={d.name}
            numberQuestion={d.numberQuestion}
            answer1={d.answer1}
            answer2={d.answer2}
            idAnswer1={d.idAnswer1}
            idAnswer2={d.idAnswer2}
            icon1={d.icon1}
            icon2={d.icon2}
            onClick={onClickYesNo}
            dataSelected={index < state.resultYesNo.length ? state.resultYesNo[index] : undefined}
            isShow={stepQuestion > 2 + index}
            dataGtmtracking={d.dataGtmtracking}
            isCurrentQuestion={stepQuestion === 3 + index}
          />
        ))
      }
    </div>
  );
}

export default AboutYou;
