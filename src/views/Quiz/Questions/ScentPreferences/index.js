import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import QuestionType from '../QuestionType';
import { useMergeState } from '../../../../Utils/customHooks';
import { getNameFromButtonBlock, getNameFromCommon, scrollToTargetAdjusted } from '../../../../Redux/Helpers';
import { emitChangeQuestion } from '../../handler';

function ScentPreferences(props) {
  const [state, setState] = useMergeState({});

  const scrollToNext = (id) => {
    setTimeout(() => {
      scrollToTargetAdjusted(id);
    }, 200);
  };

  const updateStepQuestion = (step) => {
    if (step === 10 || step < props.stateParent?.stepQuestion) {
      props.updateStepQuestion(step);
    }

    // increate index question
    _.assign(props.stateParent, { stepQuestion: step + 1 });
    emitChangeQuestion(step + 1);
  };

  const onClick = (name, value, idIndex) => {
    console.log('onClick', name, value, idIndex);

    // increate index question
    updateStepQuestion(idIndex);

    setState({ [name]: value });
    _.assign(props.dataAll, { [name]: value });

    if (idIndex === 10) {
      scrollToNext('id-header-write-create');
    } else {
      scrollToNext(`step-${idIndex + 1}`);
    }

    // update data
    props.updateRealTime();
  };

  const { stepQuestion } = props.stateParent || {};
  const scentPreferencesBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'SCENT PREFERENCES');
  const select1Bt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'SELECT 1 OR MORE (OPTIONAL)');
  const whichPerfumeLikeBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Which perfumes do you like');
  const searchBelowBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'SEARCH BELOW TO ADD YOUR PERFUMES (OPTIONAL)');
  const strongBt = getNameFromCommon(props.cmsCommon, 'How strong do you like your perfume');
  const favoriteBt = getNameFromCommon(props.cmsCommon, 'Any favorite perfume family');
  return (
    <div id="id-scent-preferences" className={classnames('scent-preferences', stepQuestion > 7 ? '' : 'hidden')}>
      <div id="id-header-scent-preferences" className="index-step" />
      <div className="header-question-item">
        {scentPreferencesBt}
      </div>
      <QuestionType
        type="Strength"
        name="strength"
        idIndex={8}
        title={strongBt}
        numberQuestion="9/12"
        onClick={onClick}
        dataSelected={state.strength}
        isShow={stepQuestion > 7}
        buttonBlocks={props.cmsState?.buttonBlocks}
        isCurrentQuestion={stepQuestion === 8}
      />
      <QuestionType
        type="Perfume-family"
        name="families"
        idIndex={9}
        title={favoriteBt}
        subTitle={select1Bt}
        numberQuestion="10/12"
        onClick={onClick}
        dataSelected={state.families}
        isShow={stepQuestion > 8}
        buttonBlocks={props.cmsState?.buttonBlocks}
        isCurrentQuestion={stepQuestion === 9}
      />
      <QuestionType
        type="Perfume-like"
        name="externalProducts"
        idIndex={10}
        title={whichPerfumeLikeBt}
        subTitle={searchBelowBt}
        numberQuestion="11/12"
        onClick={onClick}
        dataSelected={state.externalProducts}
        isShow={stepQuestion > 9}
        buttonBlocks={props.cmsState?.buttonBlocks}
        isCurrentQuestion={stepQuestion === 10}
      />
    </div>
  );
}

export default ScentPreferences;
