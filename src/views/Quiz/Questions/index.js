import React, { useRef } from 'react';
import { isBrowser } from '../../../DetectScreen/detectIFrame';
import AboutYou from './AboutYou';
import Banner from './Banner';
import BottleStep from './BottleStep';
import HeaderQuestion from './HeaderQuestion';
import ScentPreferences from './ScentPreferences';
import WhyCreate from './WhyCreate';
import './styles.scss';
import { useMergeState } from '../../../Utils/customHooks';

function Questions(props) {
  const [state, setState] = useMergeState({
    stepQuestion: 0,
  });
  const dataAll = useRef({
    isAgreeEmail: true,
  });

  const updateRealTime = async () => {
    props.updateRealTime(dataAll.current);
  };

  const updateStepQuestion = (step) => {
    setState({ stepQuestion: step + 1 });
  };

  return (
    <div className="div-questions">
      <HeaderQuestion total={12} cmsState={props.cmsState} />
      {isBrowser && <BottleStep />}
      <div className="content-list">
        <Banner data={props.cmsState?.imagetxtBlock} />
        <AboutYou
          updateRealTime={updateRealTime}
          dataAll={dataAll.current}
          stateParent={state}
          updateStepQuestion={updateStepQuestion}
          cmsState={props.cmsState}
          cmsCommon={props.cmsCommon}
        />
        <ScentPreferences
          updateRealTime={updateRealTime}
          dataAll={dataAll.current}
          stateParent={state}
          updateStepQuestion={updateStepQuestion}
          cmsState={props.cmsState}
          cmsCommon={props.cmsCommon}
        />
        <WhyCreate
          updateRealTime={updateRealTime}
          dataAll={dataAll.current}
          stateParent={state}
          updateStepQuestion={updateStepQuestion}
          onClikcDiscover={() => props.onClikcDiscover(dataAll.current)}
          onClickSkip={() => props.onClickSkip(dataAll.current)}
          cmsState={props.cmsState}
          cmsCommon={props.cmsCommon}
        />
      </div>
    </div>
  );
}

export default Questions;
