import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import AppFlowActions from '../../../../constants';
import { getNameFromButtonBlock, scrollToTargetAdjusted } from '../../../../Redux/Helpers';
import { useMergeState } from '../../../../Utils/customHooks';
import useEmitter from '../../../../Utils/useEmitter';
import useWindowEvent from '../../../../Utils/useWindowEvent';
import './styles.scss';

function HeaderQuestion(props) {
  const [state, setState] = useMergeState({
    percent: 0,
  });
  const [tab, setTab] = useState();
  const [isFixPositionHeader, setIsFixPositionHeader] = useState(false);

  const gotoId = (id) => {
    scrollToTargetAdjusted(id);
  };

  const isIncludeScreen = (top, bottom) => top <= 1 && bottom >= 1;

  const hanleScroll = () => {
    const aboutus = document.getElementById('id-about-you');
    const scentPreferences = document.getElementById('id-scent-preferences');
    const writeCreate = document.getElementById('id-write-create');
    const headerQuestion = document.getElementById('id-header-question');
    let aboutusTop = 0;
    let aboutusBottom = 0;
    let scentPreferencesTop = 0;
    let scentPreferencesBottom = 0;
    let writeCreateTop = 0;
    let writeCreateBottom = 0;
    if (aboutus) {
      aboutusTop = aboutus.getBoundingClientRect().top - 100;
      aboutusBottom = aboutus.getBoundingClientRect().bottom - 100;
    }
    if (scentPreferences) {
      scentPreferencesTop = scentPreferences.getBoundingClientRect().top - 100;
      scentPreferencesBottom = scentPreferences.getBoundingClientRect().bottom - 100;
    }
    if (writeCreate) {
      writeCreateTop = writeCreate.getBoundingClientRect().top - 100;
      writeCreateBottom = writeCreate.getBoundingClientRect().bottom - 100;
    }
    if (headerQuestion) {
      const headerQuestionTop = headerQuestion.getBoundingClientRect().top;
      console.log('document.body.scrollTop', window.pageYOffset, headerQuestionTop);
      if (window.pageYOffset < 105) {
        setIsFixPositionHeader(false);
      } else if (headerQuestionTop < 1) {
        setIsFixPositionHeader(true);
      }
    }

    if (isIncludeScreen(aboutusTop, aboutusBottom) && tab !== 'about-you') {
      setTab('about-you');
    } else if (isIncludeScreen(scentPreferencesTop, scentPreferencesBottom) && tab !== 'scent-preferences') {
      setTab('scent-preferences');
    } else if (isIncludeScreen(writeCreateTop, writeCreateBottom) && tab !== 'write-create') {
      setTab('write-create');
    }
  };

  useWindowEvent('scroll', hanleScroll, window);

  useEmitter(AppFlowActions.CHANGE_QUESTIONS, (step) => {
    setState({ percent: step / props.total * 100 });
  }, []);

  const aboutUsBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'ABOUT YOU');
  const scentPreferencesBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'SCENT PREFERENCES');
  const whyCreateBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'WHY CREATE');
  return (
    <div id="id-header-question" className={classnames('header-question', isFixPositionHeader ? 'fix-position' : '')}>
      <button
        type="button"
        className={classnames('button-bg__none', tab === 'about-you' ? 'active' : '')}
        onClick={() => gotoId('id-header-about-you')}
      >
        {aboutUsBt}
      </button>
      <button
        type="button"
        className={classnames('button-bg__none', tab === 'scent-preferences' ? 'active' : '')}
        onClick={() => gotoId('id-header-scent-preferences')}
      >
        {scentPreferencesBt}
      </button>
      <button
        type="button"
        className={classnames('button-bg__none', tab === 'write-create' ? 'active' : '')}
        onClick={() => gotoId('id-header-write-create')}
      >
        {whyCreateBt}
      </button>
      <div className="bar-process" style={{ width: `${state.percent}%` }} />
    </div>
  );
}

export default HeaderQuestion;
