import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import QuestionType from '../QuestionType';
import { useMergeState } from '../../../../Utils/customHooks';
import { getNameFromButtonBlock, getNameFromCommon, scrollToTargetAdjusted } from '../../../../Redux/Helpers';
import BlockEmail from '../QuestionType/CustomBottleQuestion/blockEmail';
import { emitChangeQuestion } from '../../handler';

function WhyCreate(props) {
  const [state, setState] = useMergeState({});

  const scrollToNext = (id) => {
    setTimeout(() => {
      scrollToTargetAdjusted(id);
    }, 200);
  };

  const updateStepQuestion = (step) => {
    // increate index question
    _.assign(props.stateParent, { stepQuestion: step + 1 });
    emitChangeQuestion(step + 1);
    // if (step === 10) {
    //   props.updateStepQuestion(step);
    // }
  };

  const onClick = (name, value, idIndex) => {
    console.log('onClick', name, value, idIndex);

    // increate index question
    updateStepQuestion(idIndex);

    setState({ [name]: value });
    _.assign(props.dataAll, { [name]: value });

    scrollToNext(`step-${idIndex + 1}`);

    // update data
    props.updateRealTime();
  };

  const { stepQuestion } = props.stateParent || {};
  const whyCreateBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'WHY CREATE');
  const select1OrBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'SELECT 1 OR MORE');
  const designAndBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Design and engrave your perfume bottle');
  const chooseFromBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'CHOOSE FROM OUR PRESETS OR UPLOAD YOUR OWN');
  const whydoyouwear = getNameFromCommon(props.cmsCommon, 'Why do you wear perfume');
  return (
    <div id="id-write-create" className={classnames('write-create', stepQuestion > 10 ? '' : 'hidden')}>
      <div id="id-header-write-create" className="index-step" />
      <div className="header-question-item">
        {whyCreateBt}
      </div>
      <QuestionType
        type="Wear-perfume"
        name="mixes"
        idIndex={11}
        title={whydoyouwear}
        subTitle={select1OrBt}
        numberQuestion="12/12"
        onClick={onClick}
        dataSelected={state.mixes}
        isShow={stepQuestion > 10}
        cmsCommon={props.cmsCommon}
        buttonBlocks={props.cmsState?.buttonBlocks}
        isCurrentQuestion={stepQuestion === 11}
      />
      <QuestionType
        type="Design-bottle"
        name="customeBottle"
        title={designAndBt}
        subTitle={chooseFromBt}
        idIndex={12}
        onClick={onClick}
        dataSelected={state.customeBottle}
        isShow={stepQuestion > 11}
        cmsCommon={props.cmsCommon}
        buttonBlocks={props.cmsState?.buttonBlocks}
        isCurrentQuestion={stepQuestion === 12}
      />
      <BlockEmail
        idIndex={13}
        isShow={stepQuestion > 12}
        onClikcDiscover={props.onClikcDiscover}
        onClickSkip={props.onClickSkip}
        dataAll={props.dataAll}
        cmsState={props.cmsState}
        isCurrentQuestion={stepQuestion === 13}
      />
    </div>
  );
}

export default WhyCreate;
