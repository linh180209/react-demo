import React, { useState, useEffect } from 'react';
import ReactHtmlParser from 'react-html-parser';
import icScroll from '../../../../image/icon/ic-scroll-down.svg';
import testImage from '../../../../image/test/4.png';
import { getAltImageV2, scrollToTargetAdjusted } from '../../../../Redux/Helpers';
import './styles.scss';

function Banner(props) {
  const onClickStart = () => {
    scrollToTargetAdjusted('id-header-about-you');
  };
  return (
    <div className="div-banner">
      <img className="image-bg" src={props.data?.value?.image?.image} alt={getAltImageV2(props.data?.value?.image)} />
      <h1>
        {props.data?.value?.text}
      </h1>
      <div className="des">
        {
          ReactHtmlParser(props.data?.value?.description)
        }
      </div>
      <button
        data-gtmtracking="funnel-1-step-02-start-quizz"
        type="button"
        className="button-bg__none"
        onClick={onClickStart}
      >
        <img src={icScroll} alt="icon" />
      </button>
    </div>
  );
}

export default Banner;
