import React, { useState, useEffect } from 'react';
import icEclip from '../../../../image/icon/ellipse.png';
import urlStep1 from '../../../../image/icon/s-bottle-1.svg';
import urlStep2 from '../../../../image/icon/s-bottle-2.svg';
import urlStep3 from '../../../../image/icon/s-bottle-3.svg';
import urlStep4 from '../../../../image/icon/s-bottle-4.svg';
import urlStep5 from '../../../../image/icon/s-bottle-5.svg';
import urlStep6 from '../../../../image/icon/s-bottle-6.svg';
import urlStep7 from '../../../../image/icon/s-bottle-7.svg';
import urlStep8 from '../../../../image/icon/s-bottle-8.svg';
import urlStep9 from '../../../../image/icon/s-bottle-12.svg';
import urlStep10 from '../../../../image/icon/s-bottle-9.svg';
import urlStep11 from '../../../../image/icon/s-bottle-10.svg';
import urlStep12 from '../../../../image/icon/s-bottle-11.svg';
import './styles.scss';
import useEmitter from '../../../../Utils/useEmitter';
import AppFlowActions from '../../../../constants';
import { useMergeState } from '../../../../Utils/customHooks';

const getIcon = (step) => {
  switch (step) {
    case 0:
    case 1:
      return {
        icon: urlStep1,
        name: 0,
      };
    case 2:
      return {
        icon: urlStep2,
        name: 2,
      };
    case 3:
      return {
        icon: urlStep3,
        name: 4,
      };
    case 4:
      return {
        icon: urlStep4,
        name: 6,
      };
    case 5:
      return {
        icon: urlStep5,
        name: 8,
      };
    case 6:
      return {
        icon: urlStep6,
        name: 10,
      };
    case 7:
      return {
        icon: urlStep7,
        name: 12,
      };
    case 8:
      return {
        icon: urlStep8,
        name: 14,
      };
    case 9:
      return {
        icon: urlStep9,
        name: 16,
      };
    case 10:
      return {
        icon: urlStep10,
        name: 18,
      };
    case 11:
      return {
        icon: urlStep11,
        name: 20,
      };
    case 12:
      return {
        icon: urlStep12,
        name: 21,
      };
    default:
      return {
        icon: urlStep12,
        name: 21,
      };
  }
};

function BottleStep(props) {
  const [state, setState] = useMergeState({
    step: 1,
  });

  useEmitter(AppFlowActions.CHANGE_QUESTIONS, (step) => {
    setState({ step });
  }, []);

  return (
    <div className="bottle-step">
      <img className="image-bg" src={icEclip} alt="icon" />
      <img className="bottle" src={getIcon(state.step).icon} alt="bottle" />
      <div className="text">
        {`${getIcon(state.step).name}G OF 21G`}
      </div>
    </div>
  );
}

export default BottleStep;
