import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import BottleCustom from '../../../../../components/B2B/CardItem/bottleCustom';
import bottle1 from '../../../../../image/icon/bottle-1.png';
import bottle2 from '../../../../../image/icon/bottle-2.png';
import './styles.scss';
import CustomeBottleV3 from '../../../../../components/CustomeBottleV3';
import { useMergeState } from '../../../../../Utils/customHooks';
import { COLOR_CUSTOME, FONT_CUSTOME } from '../../../../../constants';
import { GET_ALL_PRODUCTS_BOTTLE } from '../../../../../config';
import fetchClient from '../../../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../../../Redux/Helpers/notification';
import { getNameFromButtonBlock } from '../../../../../Redux/Helpers';

const defaultDataCustom = {
  currentImg: '',
  nameBottle: '',
  imagePremade: undefined,
  font: FONT_CUSTOME.JOST,
  color: COLOR_CUSTOME.BLACK,
};

function CustomBottleQuestion(props) {
  const [state, setState] = useMergeState({
    dataCustom: props.value || _.cloneDeep(defaultDataCustom),
    isOpenCustomeBottle: false,
    arrayThumbs: [],
  });

  const onSaveCustomer = (data) => {
    const { dataCustom } = state;
    const {
      image, name, color, font, imagePremade,
    } = data;
    dataCustom.color = color;
    dataCustom.font = font;
    dataCustom.imagePremade = imagePremade;
    if (_.isEmpty(image)) {
      dataCustom.currentImg = undefined;
      dataCustom.nameBottle = name;
      setState({ dataCustom });
      return;
    }
    dataCustom.currentImg = image;
    dataCustom.nameBottle = name;
    setState({ dataCustom });

    props.onClick(props.name, dataCustom, props.idIndex);
  };

  const onClickSkip = () => {
    setState({ dataCustom: _.cloneDeep(defaultDataCustom) });
    props.onClick(props.name, _.cloneDeep(defaultDataCustom), props.idIndex);
  };

  const handleCropImage = (img) => {
    state.dataCustom.currentImg = img;
    setState({ dataCustom: state.dataCustom });
  };

  const fetchBottle = async () => {
    const options = {
      url: GET_ALL_PRODUCTS_BOTTLE,
      method: 'GET',
    };
    try {
      const result = await fetchClient(options);
      if (result && !result.isError && result.length > 0) {
        const thumbs = [];
        _.forEach(result[0]?.images, (x) => {
          if (x.type === 'suggestion') {
            thumbs.push(x);
          }
        });
        setState({ arrayThumbs: thumbs });
      }
    } catch (err) {
      toastrError(err.message);
    }
  };

  useEffect(() => {
    fetchBottle();
  }, []);

  const skipBt = getNameFromButtonBlock(props.buttonBlocks, 'SKIP');
  const CUSTOMISEBt = getNameFromButtonBlock(props.buttonBlocks, 'CUSTOMISE');

  return (
    <div className="custom-bottle-question">
      {
        state.isOpenCustomeBottle && (
          <CustomeBottleV3
            cmsCommon={props.cmsCommon}
            arrayThumbs={state.arrayThumbs}
            handleCropImage={handleCropImage}
            currentImg={state.dataCustom.currentImg}
            nameBottle={state.dataCustom.nameBottle}
            font={state.dataCustom.font}
            color={state.dataCustom.color}
            closePopUp={() => {
              setState({ isOpenCustomeBottle: false });
            }
            }
            onSave={onSaveCustomer}
            dataGtmtracking="funnel-1-step-15-save-customize"
            dataGtmtrackingClose="funnel-1-step-15-close-customize"

            // itemBottle={{
            //   price: this.bottle ? this.bottle.items[0].price : '',
            // }}
            // cmsTextBlocks={textBlock}
            // name1={combos && combos.length > 0 ? combos[0].name : ''}
            // name2={combos && combos.length > 1 ? combos[1].name : ''}
            name1=""
          />
        )
      }
      <div className="div-bottle">
        {!state.dataCustom.currentImg && <img src={bottle1} alt="bottle" className="image-temp" />}
        <BottleCustom
          isImageText={!!state.dataCustom.currentImg}
          image={state.dataCustom.currentImg}
          isBlack
          isDisplayName
          font={state.dataCustom.font}
          color={state.dataCustom.color}
          // eauBt={eauBt}
          eauBt="Eau_de_parfum"
          // mlBt={mlBt}
          mlBt="25ml"
          name={state.dataCustom.nameBottle}
        />
        {!state.dataCustom.currentImg && <img src={bottle2} alt="bottle" className="image-temp" />}
      </div>
      <button
        data-gtmtracking="funnel-1-step-15-customize"
        type="button"
        className="bt-next-question bt-black"
        onClick={() => setState({ isOpenCustomeBottle: true })}
      >
        {CUSTOMISEBt}
      </button>
      <button
        data-gtmtracking="funnel-1-step-15-customize-skip"
        onClick={onClickSkip}
        type="button"
        className="bt-skip-question"
      >
        {skipBt}
      </button>
      {/* <BlockEmail /> */}
    </div>
  );
}

export default CustomBottleQuestion;
