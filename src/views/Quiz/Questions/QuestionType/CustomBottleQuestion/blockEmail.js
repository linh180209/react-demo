import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import icEmail from '../../../../../image/icon/ic-email-input.svg';
import { useMergeState } from '../../../../../Utils/customHooks';
import icCheck from '../../../../../image/icon/ic-check-green-bg.svg';
import icCheckGrey from '../../../../../image/icon/ic-check-grey.svg';
import { getNameFromButtonBlock } from '../../../../../Redux/Helpers';

function BlockEmail(props) {
  // redux
  const login = useSelector(state => state.login);
  const basket = useSelector(state => state.basket);

  const [state, setState] = useMergeState({
    email: props.dataAll?.email,
    isAgreeEmail: props.dataAll?.isAgreeEmail,
  });

  const onClikcDiscover = () => {
    props.onClikcDiscover();
  };

  const onClickSkip = () => {
    props.onClickSkip();
  };

  const onChangeEmail = (e) => {
    const { value } = e.target;
    setState({ email: value });
    _.assign(props.dataAll, { email: value });
  };

  const onCheckBox = () => {
    setState({ isAgreeEmail: !state.isAgreeEmail });
    _.assign(props.dataAll, { isAgreeEmail: !state.isAgreeEmail });
  };

  useEffect(() => {
    if (!props.dataAll?.email) {
      const value = login?.user?.email || basket?.email;
      onChangeEmail({
        target: { value },
      });
    }
  }, [login, basket]);
  const saveYourBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Save your results and receive');
  const yourPersionalBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Your personality decoding');
  const yourRelationBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Your relation to perfume');
  const yourFragranceBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Your fragrance style');
  const yourBespokeBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Your bespoke scents creation recommendation');
  const offYourFirstBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, '10% off your first purchase');
  const enterEmailBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Enter_your_email');
  const iAgreeBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'I agree to recieve e-mails from Maison 21G');
  const discoverBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'DISCOVER MY PERFUME CREATIONS');
  const continuteBt = getNameFromButtonBlock(props.cmsState?.buttonBlocks, 'Continue_without_saving');
  return (
    <div className={classnames('block-email', props.isShow ? '' : 'hidden')}>
      <div id={`step-${props.idIndex}`} className="index-step" />
      <h3>{saveYourBt}</h3>
      <div className="div-info">
        <div className="info-line">
          <img src={icCheck} alt="icon" />
          <span>{yourPersionalBt}</span>
        </div>
        <div className="info-line">
          <img src={icCheck} alt="icon" />
          <span>{yourRelationBt}</span>
        </div>
        <div className="info-line">
          <img src={icCheck} alt="icon" />
          <span>{yourFragranceBt}</span>
        </div>
        <div className="info-line">
          <img src={icCheck} alt="icon" />
          <span>{yourBespokeBt}</span>
        </div>
        <div className="info-line">
          <img src={icCheck} alt="icon" />
          <span>{offYourFirstBt}</span>
        </div>
      </div>
      <div className="div-email div-row">
        <img src={icEmail} alt="email" />
        <input
          type="email"
          name="email"
          value={state.email}
          placeholder={enterEmailBt}
          onChange={onChangeEmail}
        />
      </div>
      <div
        onClick={onCheckBox}
        className={classnames('div-checkbox', state.isAgreeEmail ? 'active' : '')}
      >
        <div className="checkbox">
          <img src={icCheckGrey} alt="icon" />
        </div>
        <span>{iAgreeBt}</span>
      </div>
      <button
        data-gtmtracking="funnel-1-step-16-send-email-next"
        type="button"
        className="bt-next-question bt-yellow"
        onClick={onClikcDiscover}
      >
        {discoverBt}
      </button>
      <button
        data-gtmtracking="funnel-1-step-16-send-email-skip"
        type="button"
        className="bt-skip-question bt-grey"
        onClick={onClickSkip}
      >
        {continuteBt}
      </button>
    </div>
  );
}

export default BlockEmail;
