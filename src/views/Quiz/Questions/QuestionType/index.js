import React, { useState, useEffect } from 'react';
import InputMask from 'react-input-mask';
import classnames from 'classnames';
import icMaleSelected from '../../../../image/icon/ic-male-selected-white.svg';
import icMale from '../../../../image/icon/ic-male.svg';
import icFemaleSelected from '../../../../image/icon/ic-female-selected-white.svg';
import icFemale from '../../../../image/icon/ic-female.svg';
import icUnsexSelected from '../../../../image/icon/ic-unsex-selected-white.svg';
import icUnsex from '../../../../image/icon/ic-unsex.svg';

import icPale from '../../../../image/icon/ic-pale.svg';
import icTanned from '../../../../image/icon/ic-tanned.svg';
import icBrown from '../../../../image/icon/ic-brown.svg';
import icBlack from '../../../../image/icon/ic-black.svg';
import icInfo from '../../../../image/icon/ic-info-skin.svg';

import ItemQuestion from './itemQuestion';
import PerfumeFamily from './PerfumeFamily';
import PerfumeLike from './PerfumeLike';
import WearPerfume from './WearPerfume';

import './styles.scss';
import CustomBottleQuestion from './CustomBottleQuestion';
import ItemInputDate from './itemInputDate';
import StrengthPerfume from './StrengthPerfume';
import { getNameFromButtonBlock, getNameFromCommon } from '../../../../Redux/Helpers';

function QuestionType(props) {
  const [currentQuestion, setCurrentQuestion] = useState(false);

  useEffect(() => {
    if (props.isCurrentQuestion) {
      setCurrentQuestion(true);
    } else {
      setTimeout(() => {
        setCurrentQuestion(false);
      }, 150);
    }
  }, [props.isCurrentQuestion]);

  return (
    <div className={classnames('question-type', props.isShow ? '' : 'hidden', currentQuestion ? 'current-question' : '')}>
      <div id={`step-${props.idIndex}`} className="index-step" />
      <span className="index-question">{props.numberQuestion}</span>
      <h2 className="title-question">{props.title}</h2>
      {
        props.subTitle && (
          <span className="sub-title-question">
            {props.subTitle}
          </span>
        )
      }
      {
        props.type === 'Gender' ? (
          <div className="list-item-question">
            <ItemQuestion
              title={getNameFromCommon(props.cmsCommon, 'Male')}
              name={props.name}
              value="1"
              className={classnames('item-gender', props.dataSelected === '1' ? 'div-active' : '')}
              icon={props.dataSelected === '1' ? icMaleSelected : icMale}
              onClick={props.onClick}
              idIndex={props.idIndex}
              dataGtmtracking="funnel-1-step-03-gender"
            />
            <ItemQuestion
              title={getNameFromCommon(props.cmsCommon, 'Female')}
              name={props.name}
              value="2"
              className={classnames('item-gender female', props.dataSelected === '2' ? 'div-active' : '')}
              icon={props.dataSelected === '2' ? icFemaleSelected : icFemale}
              onClick={props.onClick}
              idIndex={props.idIndex}
              dataGtmtracking="funnel-1-step-03-gender"
            />
            <ItemQuestion
              title={getNameFromCommon(props.cmsCommon, 'Gender-free')}
              name={props.name}
              value="3"
              className={classnames('item-gender unisex', props.dataSelected === '3' ? 'div-active' : '')}
              icon={props.dataSelected === '3' ? icUnsexSelected : icUnsex}
              onClick={props.onClick}
              idIndex={props.idIndex}
              dataGtmtracking="funnel-1-step-03-gender"
            />
          </div>
        ) : props.type === 'Skin-tone' ? (
          <div className="list-item-question list-skin-tone">
            <ItemQuestion
              title={getNameFromButtonBlock(props.buttonBlocks, 'PALE')}
              name={props.name}
              value="#fff4f2"
              className={classnames('item-skin-tone', props.dataSelected === '#fff4f2' ? 'div-active' : '')}
              icon={icPale}
              onClick={props.onClick}
              idIndex={props.idIndex}
              dataGtmtracking="funnel-1-step-04-skin-color"
            />
            <ItemQuestion
              title={getNameFromButtonBlock(props.buttonBlocks, 'TANNED')}
              name={props.name}
              value="#d4a093"
              className={classnames('item-skin-tone', props.dataSelected === '#d4a093' ? 'div-active' : '')}
              icon={icTanned}
              onClick={props.onClick}
              idIndex={props.idIndex}
              dataGtmtracking="funnel-1-step-04-skin-color"
            />
            <ItemQuestion
              title={getNameFromButtonBlock(props.buttonBlocks, 'BROWN')}
              name={props.name}
              value="#a36a5f"
              className={classnames('item-skin-tone', props.dataSelected === '#a36a5f' ? 'div-active' : '')}
              icon={icBrown}
              onClick={props.onClick}
              idIndex={props.idIndex}
              dataGtmtracking="funnel-1-step-04-skin-color"
            />
            <ItemQuestion
              title={getNameFromButtonBlock(props.buttonBlocks, 'BLACK')}
              name={props.name}
              value="#4e2f2a"
              className={classnames('item-skin-tone', props.dataSelected === '#4e2f2a' ? 'div-active' : '')}
              icon={icBlack}
              onClick={props.onClick}
              idIndex={props.idIndex}
              dataGtmtracking="funnel-1-step-04-skin-color"
            />
          </div>
        ) : props.type === 'Birthday' ? (
          <div className="list-item-question list-skin-tone">
            <ItemInputDate
              value={props.dataSelected}
              onClick={props.onClick}
              idIndex={props.idIndex}
              name={props.name}
              buttonBlocks={props.buttonBlocks}
              dataGtmtracking="funnel-1-step-05-birthday"
            />
          </div>
        ) : props.type === 'Question-choose' ? (
          <div className="list-item-question">
            <ItemQuestion
              name={props.name}
              title={props.answer1}
              value={props.idAnswer1}
              onClick={props.onClick}
              idIndex={props.idIndex}
              icon={props.icon1}
              className={classnames(props.dataSelected === props.idAnswer1 ? 'div-active' : '')}
              dataGtmtracking={props.dataGtmtracking}
            />
            <ItemQuestion
              name={props.name}
              title={props.answer2}
              value={props.idAnswer2}
              onClick={props.onClick}
              idIndex={props.idIndex}
              icon={props.icon2}
              className={classnames(props.dataSelected === props.idAnswer2 ? 'div-active' : '')}
              dataGtmtracking={props.dataGtmtracking}
            />
          </div>
        ) : props.type === 'Strength' ? (
          <div className="list-item-question div-col">
            <StrengthPerfume
              name={props.name}
              idIndex={props.idIndex}
              value={props.dataSelected}
              onClick={props.onClick}
              buttonBlocks={props.buttonBlocks}
              dataGtmtracking="funnel-1-step-11-perfume-strength"
            />
          </div>
        ) : props.type === 'Perfume-family' ? (
          <div className="list-item-question">
            <PerfumeFamily
              name={props.name}
              idIndex={props.idIndex}
              value={props.dataSelected}
              onClick={props.onClick}
              buttonBlocks={props.buttonBlocks}
            />
          </div>
        ) : props.type === 'Perfume-like' ? (
          <div className="list-item-question">
            <PerfumeLike
              name={props.name}
              idIndex={props.idIndex}
              value={props.dataSelected}
              onClick={props.onClick}
              buttonBlocks={props.buttonBlocks}
            />
          </div>
        ) : props.type === 'Wear-perfume' ? (
          <WearPerfume
            name={props.name}
            idIndex={props.idIndex}
            value={props.dataSelected}
            onClick={props.onClick}
            buttonBlocks={props.buttonBlocks}
            cmsCommon={props.cmsCommon}
          />
        ) : props.type === 'Design-bottle' ? (
          <CustomBottleQuestion
            name={props.name}
            idIndex={props.idIndex}
            value={props.dataSelected}
            onClick={props.onClick}
            buttonBlocks={props.buttonBlocks}
          />
        ) : (
          <div className="list-item-question" />
        )
      }
      {
        props.type === 'Skin-tone' && (
          <div className="info-skin-tone">
            <img src={icInfo} alt="info" />
            <span>{getNameFromButtonBlock(props.buttonBlocks, 'The level of Melanin affects the reaction of the scents on your skin')}</span>
          </div>
        )
      }
    </div>
  );
}

export default QuestionType;
