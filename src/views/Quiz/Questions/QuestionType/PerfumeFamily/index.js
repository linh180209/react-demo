/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import { GET_ALL_SCENT_FAMILY } from '../../../../../config';
import { isLandscape, isMobile } from '../../../../../DetectScreen/detectIFrame';
import fetchClient from '../../../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../../../Redux/Helpers/notification';
import icChecked from '../../../../../image/icon/ic-checked-quiz.svg';
import './styles.scss';
import { getNameFromButtonBlock, scrollToTargetAdjusted } from '../../../../../Redux/Helpers';

function PerfumeFamily(props) {
  const [dataProduct, setDataProduct] = useState([]);
  const [value, setValue] = useState(props.value || []);

  const fetchDataFamily = () => {
    const options = {
      url: GET_ALL_SCENT_FAMILY,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        setDataProduct(result);
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      console.log('err', err);
      toastrError(err.message);
    });
  };

  const isExistItem = d => _.find(value, x => x.id === d.id);

  const onSelection = (d) => {
    if (isExistItem(d)) {
      _.remove(value, x => x.id === d.id);
    } else {
      value.push(d);
      scrollToTargetAdjusted('id-bt-next-family');
    }
    setValue([...value]);
  };

  const onClickNext = () => {
    props.onClick(props.name, value, props.idIndex);
  };

  const onClickSkip = () => {
    setValue([]);
    props.onClick(props.name, [], props.idIndex);
  };

  const itemFamily = d => (
    <div
      onClick={() => onSelection(d)}
      className={`item-favorite ${isMobile && !isLandscape ? '' : 'enable_hover'} ${isExistItem(d) ? 'active' : ''}`}
    >
      <div className="div-image">
        <img
          loading="lazy"
          className="ingredient"
          src={d.image}
          alt="scent"
          // onClick={() => onSelection(d)}
        />
        <span>
          {d.name}
        </span>
        <div
          className={`div-bg ${isExistItem(d) ? 'show' : ''}`}
          // onClick={() => onSelection(d)}
        />
      </div>
      <img
        className="checked"
        src={icChecked}
        alt="checked"
        // onClick={() => onSelection(d)}
      />
    </div>
  );

  useEffect(() => {
    fetchDataFamily();
  }, []);
  const nextBt = getNameFromButtonBlock(props.buttonBlocks, 'NEXT');
  const skipBt = getNameFromButtonBlock(props.buttonBlocks, 'SKIP');
  return (
    <div className="perfume-family-question">
      <div className="list-item-favorite">
        {
          _.map(dataProduct, d => (
            itemFamily(d)
          ))
        }
      </div>
      <div id="id-bt-next-family" />
      <button
        data-gtmtracking="funnel-1-step-12-favorite-ingredient-next"
        onClick={onClickNext}
        type="button"
        className={classnames('bt-next-question', value.length === 0 ? 'disabled' : '')}
        disabled={value.length === 0}
      >
        {nextBt}
      </button>
      <button
        data-gtmtracking="funnel-1-step-12-favorite-ingredient-skip"
        onClick={onClickSkip}
        type="button"
        className="bt-skip-question"
      >
        {skipBt}
      </button>
    </div>
  );
}

export default PerfumeFamily;
