import React, { useState, useEffect } from 'react';
import classnames from 'classnames';

function ItemQuestion(props) {
  const onClick = () => {
    props.onClick(props.name, props.value, props.idIndex);
  };

  return (
    <div
      data-gtmtracking={props.dataGtmtracking}
      className={classnames('item-question', props.className)}
      onClick={onClick}
    >
      <div className="div-image">
        <div className="active" />
        <img src={props.icon} alt="icon" />
      </div>
      <div className="name">{props.title}</div>
    </div>
  );
}

export default ItemQuestion;
