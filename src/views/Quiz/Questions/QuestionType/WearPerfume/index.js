import _ from 'lodash';
import React, { useState, useRef } from 'react';
import classnames from 'classnames';
import { isBrowser } from '../../../../../DetectScreen/detectIFrame';
import icCheck from '../../../../../image/icon/ic-check-yellow.svg';
import './styles.scss';
import { getNameFromButtonBlock, getNameFromCommon } from '../../../../../Redux/Helpers';

function WearPerfume(props) {
  const [value, setValue] = useState(props.value || []);
  const dataList = useRef([
    {
      title: getNameFromCommon(props.cmsCommon, 'WORK & SOCIAL'),
      subtitle: getNameFromCommon(props.cmsCommon, 'Elegant & Confident'),
      id: '2',
    }, {
      title: getNameFromCommon(props.cmsCommon, 'seduction'),
      subtitle: getNameFromCommon(props.cmsCommon, 'Attractive & Sexy'),
      id: '3',
    }, {
      title: getNameFromCommon(props.cmsCommon, 'SPECIAL OCCASION'),
      subtitle: getNameFromCommon(props.cmsCommon, 'Stand out & Trendy'),
      id: '4',
    }, {
      title: getNameFromCommon(props.cmsCommon, 'CHILL & RELAX'),
      subtitle: getNameFromCommon(props.cmsCommon, 'Fresh & Conforting'),
      id: '1',
    },
  ]);

  const isExistItem = d => _.find(value, x => x.id === d.id);

  const onClickSelect = (d) => {
    if (isExistItem(d)) {
      _.remove(value, x => x.id === d.id);
    } else {
      value.push(d);
    }
    setValue([...value]);
  };

  const onClick = () => {
    props.onClick(props.name, value, props.idIndex);
  };

  const itemWearPerfume = d => (
    <div
      className={classnames('item-wear-perfume', isExistItem(d) ? 'active' : '')}
      onClick={() => onClickSelect(d)}
    >
      <div className="div-checkbox">
        <div className="checkbox">
          <img src={icCheck} alt="icob" />
        </div>
        {
          isBrowser && (
            <span className="des-1">
              {d.title}
            </span>
          )
        }

      </div>
      <div className="des">
        {
          !isBrowser && (
            <span className="des-1">
              {d.title}
            </span>
          )
        }

        <span className="des-2">
          {d.subtitle}
        </span>
      </div>
    </div>
  );

  const nextBt = getNameFromButtonBlock(props.buttonBlocks, 'NEXT');
  return (
    <div className="wear-perfume">
      {
        _.map(dataList.current, d => (
          itemWearPerfume(d)
        ))
      }
      <button
        data-gtmtracking="funnel-1-step-14-occasion"
        type="button"
        className={classnames('bt-next-question', value.length === 0 ? 'disabled' : '')}
        disabled={value.length === 0}
        onClick={onClick}
      >
        {nextBt}
      </button>
    </div>
  );
}

export default WearPerfume;
