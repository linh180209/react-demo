import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import icStrength from '../../../../../image/icon/ic-strength-v2.png';
import { getNameFromButtonBlock } from '../../../../../Redux/Helpers';

function StrengthPerfume(props) {
  const [valueSeleted, setValueSeleted] = useState(props.value);

  const onClick = (value) => {
    setValueSeleted(value);
    setTimeout(() => {
      props.onClick(props.name, value, props.idIndex);
    }, 800);
  };
  const SUBTLEBt = getNameFromButtonBlock(props.buttonBlocks, 'SUBTLE');
  const BALANCEDBt = getNameFromButtonBlock(props.buttonBlocks, 'BALANCED');
  const STRONGBt = getNameFromButtonBlock(props.buttonBlocks, 'STRONG');
  return (
    <div>
      <div className="question-strength">
        <div className="wrap-strength">
          <div className="bg-white" />
          <div className={classnames('bg-color', valueSeleted === 0 ? 'level-1' : valueSeleted === 1 ? 'level-2' : valueSeleted === 2 ? 'level-3' : '')} />
          <img src={icStrength} alt="strength" />
          <div className="user-touch">
            <div data-gtmtracking={props.dataGtmtracking} className="touch-1" onClick={() => onClick(0)} />
            <div data-gtmtracking={props.dataGtmtracking} className="touch-2" onClick={() => onClick(1)} />
            <div data-gtmtracking={props.dataGtmtracking} className="touch-3" onClick={() => onClick(2)} />
          </div>
        </div>
      </div>
      <div className="list-text">
        <span>{SUBTLEBt}</span>
        <span>{BALANCEDBt}</span>
        <span>{STRONGBt}</span>
      </div>
    </div>
  );
}

export default StrengthPerfume;
