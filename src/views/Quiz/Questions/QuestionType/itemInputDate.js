import React, { useState, useEffect } from 'react';
import InputMask from 'react-input-mask';
import classnames from 'classnames';
import { getNameFromButtonBlock, isDobValid } from '../../../../Redux/Helpers';

function ItemInputDate(props) {
  const [value, setValue] = useState(props.value);
  const [isEnable, setIsEnable] = useState(false);

  const onClick = () => {
    props.onClick(props.name, value, props.idIndex);
  };

  const onClickSkip = () => {
    props.onClick(props.name, '', props.idIndex);
  };

  useEffect(() => {
    const isValid = isDobValid(value);
    setIsEnable(isValid);
  }, [value]);

  const nextBt = getNameFromButtonBlock(props.buttonBlocks, 'NEXT');
  const skipBt = getNameFromButtonBlock(props.buttonBlocks, 'SKIP');
  return (
    <div className="div-col items-center">
      <InputMask
        className="input-dob"
        type="text"
        placeholder="DD/MM/YYYY"
        mask="99/99/9999"
        maskChar={null}
        value={value}
        onChange={e => setValue(e.target.value)}
      />
      <button
        data-gtmtracking={props.dataGtmtracking}
        type="button"
        className={classnames('bt-next-question', isEnable ? '' : 'disabled')}
        onClick={onClick}
        disabled={!isEnable}
      >
        {nextBt}
      </button>
      <button
        data-gtmtracking={props.dataGtmtracking}
        onClick={onClickSkip}
        type="button"
        className="bt-skip-question"
      >
        {skipBt}
      </button>
    </div>
  );
}

export default ItemInputDate;
