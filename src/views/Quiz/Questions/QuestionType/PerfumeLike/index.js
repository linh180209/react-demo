import React, { useState, useEffect, useRef } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import icRemove from '../../../../../image/icon/ic-remove-quiz.svg';
import { useMergeState } from '../../../../../Utils/customHooks';
import ItemPerfumeSelection from '../../../../SelectionPerfume/itemPerfumeSelection';
import './styles.scss';
import { getNameFromButtonBlock } from '../../../../../Redux/Helpers';

function PerfumeLike(props) {
  const [state, setState] = useMergeState({
    dataQuizSelect: undefined,
    listValue: props.value || [],
  });
  const dataA = useRef();

  const isExistItem = d => _.find(state.listValue, x => x.id === d.id);

  const onClickPerfume = (data) => {
    if (!isExistItem(data)) {
      state.listValue.push(data);
    }
    setState({ listValue: [...state.listValue] });
    // setState({ dataQuizSelect: data });
  };

  const onClickRemove = (d) => {
    _.remove(state.listValue, x => x.id === d.id);
    setState({ listValue: [...state.listValue] });
  };

  const clearPerfume = () => {
    setState({ dataQuizSelect: undefined });
  };

  const onClickNext = () => {
    props.onClick(props.name, state.listValue, props.idIndex);
  };

  const onClickSkip = () => {
    clearPerfume();
    props.onClick(props.name, [], props.idIndex);
  };

  const updateDataA = (data) => {
    dataA.current = data;
  };
  const nextBt = getNameFromButtonBlock(props.buttonBlocks, 'NEXT');
  const skipBt = getNameFromButtonBlock(props.buttonBlocks, 'SKIP');
  const yourbt = getNameFromButtonBlock(props.buttonBlocks, 'YOUR PERFUMES');
  const typeBt = getNameFromButtonBlock(props.buttonBlocks, 'Type your perfume here');
  return (
    <div className="perfume-like">
      {
        state.listValue.length > 0 && (
          <div className="list-added">
            <h4>
              {yourbt.replace('{total}', state.listValue.length)}
            </h4>
            {
              _.map(state.listValue, k => (
                <div className="item-added div-row">
                  <div className="item-left div-row">
                    <img src={k.image} alt="logo" />
                    <span>
                      {k.name}
                    </span>
                  </div>
                  <button type="button" className="button-bg__none" onClick={() => onClickRemove(k)}>
                    <img src={icRemove} alt="icon" />
                  </button>
                </div>
              ))
            }
          </div>
        )
      }
      <div className="div-list-item center-item">
        <ItemPerfumeSelection
          dataGtmtracking="funnel-2-step-02-favorite-perfume-1"
          onClickPerfume={onClickPerfume}
          clearPerfume={clearPerfume}
          placeholder1={typeBt}
          placeholder2={typeBt}
          isSearchFunc
          updateDataA={updateDataA}
          dataA={dataA.current}
          updateCenterScreen={() => {}}
          dataQuizSelect={state.dataQuizSelect}
          clearDataQuiz={() => setState({ dataQuizSelect: undefined })}
          cmsCommon={props.cmsCommon}
          isNotScrollTop
        />
      </div>
      <button
        data-gtmtracking="funnel-1-step-13-favorite-perfume-next"
        onClick={onClickNext}
        type="button"
        className={classnames('bt-next-question', state.listValue.length === 0 ? 'disabled' : '')}
        disabled={state.listValue.length === 0}
      >
        {nextBt}
      </button>
      <button
        data-gtmtracking="funnel-1-step-13-favorite-perfume-skip"
        onClick={onClickSkip}
        type="button"
        className="bt-skip-question"
      >
        {skipBt}
      </button>
    </div>
  );
}

export default PerfumeLike;
