import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import _ from 'lodash';
import MetaTags from 'react-meta-tags';
import {
  generateHreflang, generateUrlWeb, getCmsCommon, getSearchPathName, removeLinkHreflang, setPrerenderReady,
} from '../../../Redux/Helpers';
import addCmsRedux from '../../../Redux/Actions/cms';
import loadingPage from '../../../Redux/Actions/loading';
import updateAnswersData from '../../../Redux/Actions/answers';
import updateQuestionsData from '../../../Redux/Actions/questions';
import { fetchDataInit, handleCMSPage } from '../handler';
import { toastrError } from '../../../Redux/Helpers/notification';
import HeaderHomePageV3 from '../../../components/HomePage/HeaderHomePageV3';
import LandingQuiz from './index';

function LandingQuizPage(props) {
  // history
  const history = useHistory();

  // redux
  const dispatch = useDispatch();
  const countries = useSelector(state => state.countries);
  const cms = useSelector(state => state.cms);
  const answers = useSelector(state => state.answers);
  const questions = useSelector(state => state.questions);

  // state
  const [cmsState, setCmsState] = useState({});

  const fetchData = async () => {
    const cmsPersonal = _.find(cms, x => x.title === 'Personality');
    if (!cmsPersonal || answers.length === 0 || questions.length === 0) {
      dispatch(loadingPage(true));
      try {
        const results = await fetchDataInit();
        dispatch(updateAnswersData(results[0]));
        dispatch(updateQuestionsData(results[1]));
        dispatch(addCmsRedux(results[2]));
        const cmsData = handleCMSPage(results[2]);
        setCmsState(cmsData);
      } catch (error) {
        toastrError(error.message);
      } finally {
        dispatch(loadingPage(false));
      }
    } else {
      const cmsData = handleCMSPage(cmsPersonal);
      setCmsState(cmsData);
    }
  };

  const onClickStart = () => {
    const { pathname, search } = getSearchPathName(generateUrlWeb('/quizv3'));
    history.push({
      pathname,
      search,
      state: {
        isNoStartPage: true,
      },
    });
  };

  useEffect(() => {
    setPrerenderReady();
    fetchData();
    return (() => {
      removeLinkHreflang();
    });
  }, []);

  return (
    <div>
      <MetaTags>
        <title>
          {cmsState?.seo?.seoTitle}
        </title>
        <meta name="description" content={cmsState?.seo?.seoDescription} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/quizv3')}
      </MetaTags>
      {/* <HeaderHomePage visible={false} isSpecialMenu isDisableScrollShow isNotShowRegion /> */}
      <HeaderHomePageV3 isRemoveMessage />

      <LandingQuiz
        cmsCommon={getCmsCommon(cms)}
        buttonBlocks={cmsState.buttonBlocks}
        imageBackground={cmsState.imageBackground}
        headerText={cmsState.headerText}
        namePath="quiz"
        paragraphBlock={cmsState.paragraphBlock}
        onClickStart={onClickStart}
      />
    </div>
  );
}

export default LandingQuizPage;
