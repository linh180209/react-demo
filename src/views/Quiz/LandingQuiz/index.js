import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import ReactHtmlParser from 'react-html-parser';
import AccessAlarmsIcon from '@mui/icons-material/AccessAlarms';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import icBMW from '../../../image/icon/ic-bmw.svg';
import icBMW1 from '../../../image/icon/ic-bmw-1.svg';
import { logoGoldText } from '../../../imagev2/svg';
import { getNameFromButtonBlock, getNameFromCommon } from '../../../Redux/Helpers';
import './styles.scss';
import ButtonCT from '../../../componentsv2/buttonCT';

function LandingQuiz(props) {
  const bt23Bt = getNameFromCommon(props.cmsCommon, '2-3_Minutes');
  const quizBt = getNameFromButtonBlock(props.buttonBlocks, 'take quiz');
  return (
    <div
      className={`div-landing-quiz div-col justify-center items-center ${['sofitel', 'krisshop'].includes(props.namePath) ? 'full-screen' : ''}`}
      style={{ backgroundImage: `url(${props?.imageBackground})` }}
    >
      <div className="logo-maison">
        <img src={logoGoldText} alt="logo" />
      </div>
      <div className={classnames('content-personality div-col', props.namePath === 'bmw' ? 'style-bmw' : '')}>
        <h1 className="tx-header mt-3 mb-3">
          {props?.headerText}
        </h1>
        {
            props.namePath === 'bmw' ? (
              <div className="div-des">
                <div className="image">
                  <img src={icBMW1} alt="icon" />
                  <img src={icBMW} alt="icon" />
                </div>
                <span className="description tc">
                  {ReactHtmlParser(props.paragraphBlock?.length > 0 ? props.paragraphBlock[0].value : '')}
                </span>
              </div>
            ) : (
              <span className="description tc">
                {ReactHtmlParser(props.paragraphBlock?.length > 0 ? props.paragraphBlock[0].value : '')}
              </span>
            )
          }
        <div className="div-bottom div-col">
          <div className="div-time div-col">
            <div className="div-row div-watch">
              <AccessAlarmsIcon style={{ color: '#EBD8B8', fontSize: '24px', marginRight: '8px' }} />
              <span>
                {bt23Bt}
              </span>
            </div>
          </div>
          <ButtonCT
            onClick={props.onClickStart}
            name={quizBt}
            endIcon={<ArrowForwardIcon style={{ color: '#EBD8B8' }} />}
          />
          {/* <div className="button-animal">
            <button
              type="button"
              data-gtmtracking="funnel-1-step-02-start-quizz"
              onClick={props.onClickStart}
            >
              {quizBt}
            </button>
          </div> */}
        </div>
      </div>
    </div>
  );
}

export default LandingQuiz;
