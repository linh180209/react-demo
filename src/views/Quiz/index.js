import _ from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import MetaTags from 'react-meta-tags';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import smoothscroll from 'smoothscroll-polyfill';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import { PUT_OUTCOME_URL } from '../../config';
import updateAnswersData from '../../Redux/Actions/answers';
import { loadAllBasket } from '../../Redux/Actions/basket';
import addCmsRedux from '../../Redux/Actions/cms';
import loadingPage from '../../Redux/Actions/loading';
import { loginUpdateOutCome } from '../../Redux/Actions/login';
import updateQuestionsData from '../../Redux/Actions/questions';
import {
  generateHreflang, generateUrlWeb, getCmsCommon, googleAnalitycs, isCheckNull, removeLinkHreflang, scrollTop, isValidateValue, getSearchPathName,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import { useMergeState } from '../../Utils/customHooks';
import {
  assignEmailToCart,
  fetchDataInit,
  geneateBody,
  generateOutComeUrlForQuiz, getLinkGoogleAnalitycs, getPathNamePage, getSlug, handleCMSPage, initOutCome, updateEmailToOutcome,
} from './handler';
import LandingQuiz from './LandingQuiz';
import Questions from './Questions';
import './styles.scss';

function Quiz(props) {
  // history
  const history = useHistory();

  // redux
  const dispatch = useDispatch();
  const cms = useSelector(state => state.cms);
  const countries = useSelector(state => state.countries);
  const basket = useSelector(state => state.basket);
  const login = useSelector(state => state.login);
  const answers = useSelector(state => state.answers);
  const questions = useSelector(state => state.questions);

  // ref
  const slug = useRef();
  const namePath = useRef(getPathNamePage(window.location.pathname));
  const idOutCome = useRef();
  const oldBody = useRef();

  // state
  const [state, setState] = useMergeState({
    infoGift: props.location?.state || {},
    isStart: namePath.current !== 'quiz',
  });
  const [cmsState, setCmsState] = useState({});

  const fetchData = async () => {
    const cmsPersonal = _.find(cms, x => x.title === 'Personality');
    if (!cmsPersonal || answers.length === 0 || questions.length === 0) {
      dispatch(loadingPage(true));
      try {
        const results = await fetchDataInit();
        dispatch(updateAnswersData(results[0]));
        dispatch(updateQuestionsData(results[1]));
        dispatch(addCmsRedux(results[2]));
        const cmsData = handleCMSPage(results[2]);
        setCmsState(cmsData);
      } catch (error) {
        toastrError(error.message);
      } finally {
        dispatch(loadingPage(false));
      }
    } else {
      const cmsData = handleCMSPage(cmsPersonal);
      setCmsState(cmsData);
    }
  };

  const gotoResultPage = (customeBottle) => {
    if (namePath.current === 'quiz') {
      if (login?.user?.id) {
        dispatch(loginUpdateOutCome(idOutCome.current));
      }
      auth.setOutComeId(idOutCome.current);
      const { pathname, search } = getSearchPathName(generateUrlWeb(`${generateOutComeUrlForQuiz()}${idOutCome.current}${!_.isEmpty(state.infoGift?.codes) ? `/?codes=${state.infoGift?.codes}` : ''}`));
      history.push({
        pathname,
        search,
        state: { infoGift: state.infoGift, customeBottle },
      });
    } else if (namePath.current === 'boutique-quiz') {
      if (window.location.pathname.includes('/boutique-quiz-v2')) {
        const { pathname, search } = getSearchPathName(generateUrlWeb(`/boutique-quiz-v2/outcome/${idOutCome.current}`));
        history.push({
          pathname,
          search,
          state: { customeBottle },
        });
      } else {
        history.push(generateUrlWeb(`/boutique-quiz/outcome/${idOutCome.current}`));
      }
    }
  };

  const updateRealTime = async (dataAll) => {
    console.log('updateRealTime 0', dataAll);
    const body = geneateBody(_.cloneDeep(dataAll));
    console.log('updateRealTime 1', body);
    // prevent click send multiple
    if (JSON.stringify(body) === JSON.stringify(oldBody.current)) {
      return;
    }
    oldBody.current = body;

    console.log('updateRealTime body', body);
    if (isCheckNull(idOutCome.current)) {
      const result = await initOutCome(namePath.current, slug.current);
      idOutCome.current = result?.id;
    }
    console.log('updateRealTime 2', idOutCome.current);
    if (!isCheckNull(idOutCome.current)) {
      const options = {
        url: PUT_OUTCOME_URL.replace('{id}', idOutCome.current),
        method: 'PUT',
        body,
      };
      fetchClient(options, true);
    }
  };

  const onClickDiscover = (dataAll) => {
    console.log('dataAll', dataAll);
    if (!dataAll?.email || !isValidateValue(dataAll?.email)) {
      toastrError('Please enter the email!');
      return;
    }
    if (isCheckNull(idOutCome.current)) {
      return;
    }
    assignEmailToCart(dataAll?.email, basket, (result) => {
      dispatch(loadAllBasket(result));
    });
    updateEmailToOutcome(idOutCome.current, dataAll?.email);

    // go to result
    gotoResultPage(dataAll?.customeBottle);
  };

  const onClickSkip = (dataAll) => {
    // go to result
    gotoResultPage(dataAll?.customeBottle);
  };

  useEffect(() => {
    if (login?.user?.outcome && !['boutique-quiz', 'krisshop', 'sephora'].includes(namePath.current)) {
      idOutCome.current = login?.user?.outcome || undefined;
    }
  }, [login]);

  useEffect(() => {
    // fetch Data
    fetchData();

    // scroll top
    smoothscroll.polyfill();
    scrollTop();

    // google analitycs
    googleAnalitycs(getLinkGoogleAnalitycs(namePath.current));

    // get slug
    slug.current = getSlug(namePath.current);

    // redirect to result if done before
    const idOutComeTemp = login?.user?.outcome || auth.getOutComeId();
    if (namePath.current === 'quiz' && idOutComeTemp && !(props.location?.state?.isRetake)) {
      const { pathname, search } = getSearchPathName(generateUrlWeb(`${generateOutComeUrlForQuiz()}${idOutComeTemp}${!_.isEmpty(state.infoGift) ? `/?codes=${state.infoGift?.codes}` : ''}`));
      history.push({
        pathname,
        search,
        state: { stepInstore: 2, infoGift: state.infoGift },
      });
    }
    return (() => {
      removeLinkHreflang();
    });
  }, []);

  if (_.isEmpty(cmsState) || _.isEmpty(answers) || _.isEmpty(questions)) {
    return (<div />);
  }
  return (
    <div className="div-quiz-v2">
      <MetaTags>
        <title>
          {cmsState?.seo?.seoTitle}
        </title>
        <meta name="description" content={cmsState?.seo?.seoDescription} />
        <meta name="robots" content="index, follow" />
        <meta name="revisit-after" content="3 month" />
        {generateHreflang(countries, '/quiz')}
      </MetaTags>
      {/* <HeaderHomePage
        visible={false}
        isSpecialMenu
        isDisableScrollShow
        isNotShowRegion
      /> */}
      <HeaderHomePageV3 isRemoveMessage />
      {
        state.isStart ? (
          <LandingQuiz
            cmsCommon={getCmsCommon(cms)}
            buttonBlocks={cmsState.buttonBlocks}
            imageBackground={cmsState.imageBackground}
            headerText={cmsState.headerText}
            namePath={namePath.current}
            paragraphBlock={cmsState.paragraphBlock}
            onClickStart={() => { setState({ isStart: false }); }}
          />
        ) : (
          <Questions
            cmsCommon={getCmsCommon(cms)}
            cmsState={cmsState}
            updateRealTime={updateRealTime}
            onClikcDiscover={onClickDiscover}
            onClickSkip={onClickSkip}
          />
        )
      }
      {/* {!state.isStart && <Footer />} */}
    </div>
  );
}

export default Quiz;
