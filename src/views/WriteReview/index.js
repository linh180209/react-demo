/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import '../../styles/style.scss';
import moment from 'moment';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import MetaTags from 'react-meta-tags';
import Dropzone from 'react-dropzone';
import loadImage from 'blueimp-load-image';
import queryString from 'query-string';
import '../../styles/write-review-page.scss';
import fetchClient, { fetchClientFormData } from '../../Redux/Helpers/fetch-client';

// import Header from '../../components/HomePage/header';
import icUploadFile from '../../image/icon/ic-upload-file.svg';
import icDone from '../../image/icon/checkReview.svg';
import {
  getNameFromCommon, fetchCMSHomepage, getSEOFromCms, getNameFromButtonBlock, getLinkFromButtonBlock, scrollTop,
  getUrlProductReview, googleAnalitycs, generateHreflang, generateUrlWeb, removeLinkHreflang, setPrerenderReady, fetchTokenSimplyBook, gotoShopHome, segmentProductReviewed,
} from '../../Redux/Helpers';
import addCmsRedux from '../../Redux/Actions/cms';
import { toastrError } from '../../Redux/Helpers/notification';
import {
  GET_LINE_ITEM_URL, GET_HISTORY_REVIEW_URL, GET_HISTORY_REVIEW_WORKSHOP_URL, POST_HISTORY_REVIEW_WORKSHOP_URL, POST_HISTORY_REVIEW_WORKSHOP_ID_URL,
} from '../../config';
import loadingPage from '../../Redux/Actions/loading';
import { getEventListSimplyBook } from '../../Redux/Actions/bookWorkshop';
import ItemLike from './itemLike';
import CheckboxInput from '../../components/CheckboxInput';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import FooterV2 from '../../views2/footer';


const prePareCms = (cms) => {
  if (cms) {
    const seo = getSEOFromCms(cms);
    const { body, header_text: headerText } = cms;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    return {
      seo, buttonBlocks, headerText,
    };
  }
  return { seo: undefined, buttonBlocks: [], headerText: '' };
};

const handleGetDataWorkshop = async (bookWorkshop, dataQuery) => {
  const { workshop, booking, user } = dataQuery;
  const objectReturn = { booking, user, workshop };
  const ele = _.find(bookWorkshop?.eventList, x => x.id === workshop);
  _.assign(objectReturn, { itemReview: ele, data: ele });
  const option = {
    url: GET_HISTORY_REVIEW_WORKSHOP_URL.replace('{id}', user),
    method: 'GET',
  };
  let resultComment;
  try {
    resultComment = await fetchClient(option);
    if (resultComment?.isError) {
      throw new Error(resultComment?.message);
    } else {
      const review = resultComment[0];
      _.assign(objectReturn, {
        ratingOverall: review ? review.rating_overall : 5,
        ratingLearning: review ? review.rating_learning : 5,
        ratingGuidance: review ? review.rating_guidance : 5,
        ratingSatisfaction: review ? review.rating_satisfaction : 5,
        ratingInnovative: review ? review.rating_innovative : 5,
        ratingLuxury: review ? review.rating_Luxury : 5,
        ratingStrength: review ? review.rating_strength : 5,
        ratingDiffusion: review ? review.rating_diffusion : 5,
        ratingFit: review ? review.rating_fit : 5,
        isRecommended: review ? review.is_recommended : false,
        comment: review ? review.comment : undefined,
        imageUpload: review ? review.image : undefined,
      });
    }
  } catch (error) {
    toastrError(error.message);
  }
  return objectReturn;
};
class WriteViewPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonBlocks: [],
      headerText: '',
      seo: undefined,
      comment: '',
      imageUpload: undefined,
      data: undefined,
      isSent: false,
      dataQuery: queryString.parse(props.location.search),
      rateScent: 0,
      rateProduct: 0,
      rateSatisfaction: 0,

      itemReview: undefined,
      ratingOverall: 0,
      ratingLearning: 0,
      ratingGuidance: 0,
      ratingSatisfaction: 0,
      ratingInnovative: 0,
      ratingLuxury: 0,
      ratingStrength: 0,
      ratingDiffusion: 0,
      ratingFit: 0,
      isRecommended: false,
    };
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/write-review');
    this.fetchCMS();
    this.fetchLineItem(this.state.dataQuery);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { bookWorkshop } = nextProps;
    if (bookWorkshop?.eventList && bookWorkshop !== prevState.bookWorkshop) {
      _.assign(objectReturn, { bookWorkshop, isGetData: true });
    }
    console.log('objectReturn', objectReturn);
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  componentDidUpdate() {
    const { bookWorkshop, dataQuery } = this.state;
    if (this.state.isGetData) {
      this.state.isGetData = false;
      handleGetDataWorkshop(bookWorkshop, dataQuery).then((result) => {
        this.setState(result);
      });
    }
  }

  componentWillUnmount() {
    removeLinkHreflang();
  }


  onChange = (e) => {
    const { value } = e.target;
    this.setState({ comment: value });
  }

  fetchWriteReviewForWorkshop = (workshop, booking, user) => {
    const { bookWorkshop } = this.props;
    if (!bookWorkshop?.eventList) {
      fetchTokenSimplyBook().then((res) => {
        if (res) {
          this.props.getEventListSimplyBook();
        }
      }).catch(err => toastrError(err?.errors?.message));
    } else {
      const objecReturn = handleGetDataWorkshop(bookWorkshop, this.state.dataQuery);
      this.setState(objecReturn);
    }
  }

  fetchLineItem = (data) => {
    if (!data) {
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    }
    const {
      line_item: lineItem, booking, workshop, user,
    } = data;
    if (workshop && booking && user) {
      this.fetchWriteReviewForWorkshop(workshop, booking, user);
      return;
    }
    if (!lineItem) {
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    }
    const options = {
      url: GET_LINE_ITEM_URL.replace('{id}', lineItem),
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      const { review } = result;
      this.setState({
        data: result,
        rateScent: review ? review.rating_scent : 5,
        rateProduct: review ? review.rating_product : 5,
        rateSatisfaction: review ? review.rating_satisfaction : 5,
        comment: review ? review.comment : undefined,
        imageUpload: review ? review.image : undefined,
      });
    }).catch((error) => {
      toastrError(error.message);
      // this.props.history.push(generateUrlWeb('/'));
      gotoShopHome();
    });
  };

  onSelectFile = (files) => {
    if (files && files.length > 0) {
      loadImage(
        files[0],
        (img) => {
          const base64data = img.toDataURL('image/png');
          this.setState({ imageUpload: base64data });
          this.fileImage = files[0];
        },
        { orientation: 1 },
      );
    }
  };

  fetchCMS = async () => {
    const { cms } = this.props;
    const myOrder = _.find(cms, x => x.title === 'Write Review');
    if (_.isEmpty(myOrder)) {
      try {
        const cmsData = await fetchCMSHomepage('write-review');
        this.props.addCmsRedux(cmsData);
        const dataCms = prePareCms(cmsData);
        this.setState(dataCms);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const dataCms = prePareCms(myOrder);
      this.setState(dataCms);
    }
  }

  postData = () => {
    const {
      line_item: id, user, email, name, workshop, booking,
    } = this.state.dataQuery;
    const {
      rateProduct, rateScent, rateSatisfaction, comment, data,
      ratingOverall, ratingLearning, ratingGuidance,
      ratingSatisfaction, ratingInnovative, ratingLuxury, ratingStrength, ratingDiffusion,
      ratingFit, isRecommended, itemReview,
    } = this.state;
    const formData = new FormData();
    if (this.fileImage) {
      formData.append('image', this.fileImage);
    }
    if (comment) {
      formData.append('comment', comment);
    }
    if (workshop) {
      formData.append('booking', booking);
      formData.append('rating_overall', ratingOverall);
      formData.append('rating_learning', ratingLearning);
      formData.append('rating_guidance', ratingGuidance);
      formData.append('rating_satisfaction', ratingSatisfaction);
      formData.append('rating_innovative', ratingInnovative);
      formData.append('rating_luxury', ratingLuxury);
      formData.append('rating_strength', ratingStrength);
      formData.append('rating_diffusion', ratingDiffusion);
      formData.append('rating_fit', ratingFit);
      formData.append('is_recommended', isRecommended);
    } else {
      formData.append('line_item', id);
      formData.append('rating_scent', rateScent);
      formData.append('rating_product', rateProduct);
      formData.append('rating_satisfaction', rateSatisfaction);
    }
    if (user) {
      formData.append('user', user);
    }
    if (email && name) {
      formData.append('guest', JSON.stringify({
        email,
        name,
      }));
    }
    const options = {
      url: workshop ? (itemReview?.id ? POST_HISTORY_REVIEW_WORKSHOP_ID_URL.replace('{id}', itemReview?.id) : POST_HISTORY_REVIEW_WORKSHOP_URL)
        : (data?.review ? `${GET_HISTORY_REVIEW_URL}${data?.review?.id}/` : GET_HISTORY_REVIEW_URL),
      method: (data?.review || itemReview?.id) ? 'PUT' : 'POST',
      body: formData,
    };
    this.props.loadingPage(true);
    fetchClientFormData(options, true).then((result) => {
      console.log('result', result);
      this.props.loadingPage(false);
      if (result && !result.isError) {
        this.setState({ isSent: true });
        const rate = workshop ? _.mean([ratingOverall,
          ratingLearning,
          ratingGuidance,
          ratingSatisfaction,
          ratingInnovative,
          ratingLuxury,
          ratingStrength,
          ratingDiffusion,
          ratingFit])
          : _.mean([rateScent,
            rateProduct,
            rateSatisfaction]);
        const dataSegment = {
          product_id: result?.line_item?.id,
          product_name: result?.line_item?.name,
          review_id: result?.id,
          review_body: result?.comment,
          satisfaction_rating: rateSatisfaction,
          scent_rating: rateScent,
          product_rating: rateProduct,
          average_rating: rate,
          review_image_url: result?.image,
        };
        segmentProductReviewed(dataSegment);
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err.message);
      this.props.loadingPage(false);
    });
  }

  onClickGotoProductWorkshop = (link) => {
    if (link) {
      this.props.history.push(generateUrlWeb(`/${link}`));
    }
  };

  onClickGotoProduct = (data) => {
    console.log('data', data);
    const {
      product, item, combo, type,
    } = data;
    const urlPro = getUrlProductReview(product, type, combo, item).url;
    if (urlPro) {
      this.props.history.push(generateUrlWeb(urlPro));
    }
  }

  onChangeLike = (name, value) => {
    this.state[name] = value;
    this.forceUpdate();
  }

  render() {
    const {
      seo, buttonBlocks, headerText,
      rateScent, rateProduct, rateSatisfaction, comment, imageUpload,
      data, isSent, dataQuery, ratingOverall, ratingLearning, ratingGuidance,
      ratingSatisfaction, ratingInnovative, ratingLuxury, ratingStrength, ratingDiffusion,
      ratingFit, isRecommended, workshop,
    } = this.state;

    const deliveredBt = getNameFromButtonBlock(buttonBlocks, 'Delivered_on');
    const rateBt = getNameFromButtonBlock(buttonBlocks, 'rate_review_product');
    const reviewDetailBt = getNameFromButtonBlock(buttonBlocks, 'Review_Detail');
    const scentBlendBt = getNameFromButtonBlock(buttonBlocks, 'Scent_Blend');
    const productBt = getNameFromButtonBlock(buttonBlocks, 'Product');
    const satisfactionBt = getNameFromButtonBlock(buttonBlocks, 'Satisfaction');
    const uploadPhotoBt = getNameFromButtonBlock(buttonBlocks, 'Upload_Photo');
    const submitBt = getNameFromButtonBlock(buttonBlocks, 'submit');
    const thankReviewBt = getNameFromButtonBlock(buttonBlocks, 'Thank_you_for_your_review');
    const myAccountBt = getNameFromButtonBlock(buttonBlocks, 'my_account');
    const backToShopBt = getNameFromButtonBlock(buttonBlocks, 'back_to_shop');
    const ratingOverallBt = getNameFromButtonBlock(buttonBlocks, 'rating_overall');
    const ratingLearningBt = getNameFromButtonBlock(buttonBlocks, 'rating_learning');
    const ratingGuidanceBt = getNameFromButtonBlock(buttonBlocks, 'rating_guidance');
    const ratingSatisfactionBt = getNameFromButtonBlock(buttonBlocks, 'rating_satisfaction');
    const ratingInnovativeBt = getNameFromButtonBlock(buttonBlocks, 'rating_innovative');
    const ratingLuxuryBt = getNameFromButtonBlock(buttonBlocks, 'rating_luxury');
    const ratingStrengthBt = getNameFromButtonBlock(buttonBlocks, 'rating_strength');
    const ratingDiffusionBt = getNameFromButtonBlock(buttonBlocks, 'rating_diffusion');
    const ratingFitBt = getNameFromButtonBlock(buttonBlocks, 'rating_fit');
    const recommendedBt = getNameFromButtonBlock(buttonBlocks, 'recommended');

    return (
      <div>
        {/* <Header />
        { this.props.showAskRegion && (<div className="div-temp-region" />) } */}
        <HeaderHomePageV3 />
        <MetaTags>
          <title>
            {seo ? seo.seoTitle : ''}
          </title>
          <meta name="description" content={seo ? seo.seoDescription : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/write-review')}
        </MetaTags>
        <div className="div-write-review-page">

          <h3>
            {headerText}
          </h3>
          <img className={!isSent ? 'hidden' : 'img-done'} src={icDone} alt="done" />
          <span className={!isSent ? 'hidden' : 'text-des'}>{thankReviewBt}</span>
          <div className="div-item-history">
            <div className="div-body-item">
              <div className="div-left">
                <h5>
                  {`${deliveredBt} ${data ? moment(data.date_created).format('d MMM YYYY') : ''}`}
                </h5>
                <span className="title">
                  {rateBt}
                </span>
                <div className="div-product">
                  {
                    workshop ? (
                      <img
                        src={`https://maison21g.simplybook.asia${data.picture_path}`}
                        alt="product"
                        onClick={() => this.onClickGotoProductWorkshop(data?.seo_url)}
                      />
                    ) : (
                      <img src={data ? data.image : ''} alt="product" onClick={() => this.onClickGotoProduct(data)} />
                    )
                  }
                  <div className="text-info">
                    <span>
                      {data ? data?.type?.name : ''}
                    </span>
                    <span>
                      {data ? data?.name : ''}
                    </span>
                  </div>
                </div>
              </div>
              {
                dataQuery?.workshop ? (
                  <div className="div-right">
                    <ItemLike
                      title={ratingOverallBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="ratingOverall"
                      rate={ratingOverall}
                    />
                    <ItemLike
                      title={ratingLearningBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="ratingLearning"
                      rate={ratingLearning}
                    />
                    <ItemLike
                      title={ratingGuidanceBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="ratingGuidance"
                      rate={ratingGuidance}
                    />
                    <ItemLike
                      title={ratingSatisfactionBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="ratingSatisfaction"
                      rate={ratingSatisfaction}
                    />
                    <ItemLike
                      title={ratingInnovativeBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="ratingInnovative"
                      rate={ratingInnovative}
                    />
                    <ItemLike
                      title={ratingLuxuryBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="ratingLuxury"
                      rate={ratingLuxury}
                    />
                    <ItemLike
                      title={ratingStrengthBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="ratingStrength"
                      rate={ratingStrength}
                    />
                    <ItemLike
                      title={ratingDiffusionBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="ratingDiffusion"
                      rate={ratingDiffusion}
                    />
                    <ItemLike
                      title={ratingFitBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="ratingFit"
                      rate={ratingFit}
                    />
                    <CheckboxInput
                      className="checkbox-review"
                      id="isRecommended"
                      name="isRecommended"
                      data={{
                        label: recommendedBt,
                        value: isRecommended,
                      }}
                      checked={isRecommended}
                      onChange={() => this.onChangeLike('isRecommended', !isRecommended)}
                    />
                  </div>
                ) : (
                  <div className="div-right">
                    <ItemLike
                      title={scentBlendBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="rateScent"
                      rate={rateScent}
                    />
                    <ItemLike
                      title={productBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="rateProduct"
                      rate={rateProduct}
                    />
                    <ItemLike
                      title={satisfactionBt}
                      disabled={isSent}
                      onChange={this.onChangeLike}
                      name="rateSatisfaction"
                      rate={rateSatisfaction}
                    />
                  </div>
                )
              }

            </div>

            <span className="title mt-4">
              {reviewDetailBt}
            </span>
            {
              isSent ? (
                <span className="comment">
                  {comment}
                </span>
              ) : (
                <textarea value={comment} className="comment" onChange={this.onChange} />
              )
            }
            <div className="div-list-post-image">
              {
              imageUpload ? (
                <img className="img-up" src={imageUpload} alt="post" />
              ) : (
                <div />
              )
            }
              <div className={isSent ? 'hidden' : 'div-upload-image'}>
                <Dropzone ref={this.refCrop} onDrop={acceptedFiles => this.onSelectFile(acceptedFiles)} accept=".jpeg,.png,.jpg" multiple={false}>
                  {({ getRootProps, getInputProps }) => (
                    <section>
                      <div {...getRootProps()}>
                        <input {...getInputProps()} />
                        <img src={icUploadFile} alt="uploadFile" />
                        <span>
                          {uploadPhotoBt}
                        </span>
                      </div>
                    </section>
                  )}
                </Dropzone>
              </div>
            </div>
            <div className="button-submit">
              {
                isSent ? (
                  <React.Fragment>
                    <button type="button" className="bg-whilte" onClick={() => this.props.history.push(generateUrlWeb('/account'))}>
                      {myAccountBt}
                    </button>
                    <button
                      type="button"
                      onClick={() => {
                        // this.props.history.push(generateUrlWeb('/'))
                        gotoShopHome();
                      }}
                    >
                      {backToShopBt}
                    </button>
                  </React.Fragment>

                ) : (
                  <button type="button" onClick={this.postData}>
                    {submitBt}
                  </button>
                )
              }


            </div>
          </div>
        </div>
        <FooterV2 />
      </div>

    );
  }
}

WriteViewPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
};

const mapDispatchToProps = {
  addCmsRedux,
  loadingPage,
  getEventListSimplyBook,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
    bookWorkshop: state.bookWorkshop,
    // showAskRegion: state.showAskRegion,
  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(WriteViewPage));
