import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import icStar from '../../image/icon/star.svg';
import icStarSelected from '../../image/icon/star-selected.svg';

function ItemLike(props) {
  return (
    <div className="div-star">
      <span>
        {props.title}
      </span>
      <div className="div-list-star">
        {
          _.map(_.range(5), (x, index) => (
            <button
              type="button"
              disabled={props.disabled}
              onClick={() => props.onChange(props.name, index + 1)}
            >
              <img src={index + 1 <= props.rate ? icStarSelected : icStar} alt="star" />
            </button>
          ))
        }
      </div>
    </div>
  );
}

export default ItemLike;
