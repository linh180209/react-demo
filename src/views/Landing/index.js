import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import {
  Container,
  Row,
  Col,
  Button,
} from 'reactstrap';
import BackGroundVideo from '../../components/backgroundVideo';
import { generateUrlWeb } from '../../Redux/Helpers';

class Landing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      urlBackground: '',
      urlBgImage: '',
      questions: [],
    };
  }


  componentDidMount() {
    const { questions } = this.props;
    console.log('questions sas', questions);
    const persionality = _.find(questions, x => x.type === 'PERSONALITY');
    const urlBackground = persionality ? persionality.video_urls[0] : undefined;
    const urlBgImage = persionality ? persionality.image_urls[0] : undefined;
    this.setState({ urlBackground, urlBgImage });
  }

  static getDerivedStateFromProps = (nextProps, prevState) => {
    const objectReturn = {};
    const { questions } = nextProps;

    if (questions !== prevState.questions) {
      console.log('questions change', questions);
      const persionality = _.find(questions, x => x.type === 'PERSONALITY');
      const urlBackground = persionality ? persionality.video_urls[0] : undefined;
      const urlBgImage = persionality ? persionality.image_urls[0] : undefined;
      _.assign(objectReturn, { urlBackground, urlBgImage });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  handleMood = () => {
    const { history } = this.props;
    history.push(generateUrlWeb('/occasion'));
  }

  handleLetsGuide = () => {
    const { history } = this.props;
    history.push(generateUrlWeb('/quiz'));
  }

  render() {
    const { urlBackground, urlBgImage } = this.state;
    console.log('urlBackground', urlBackground, urlBgImage);
    return (
      <div className="container__landing">
        <BackGroundVideo url={urlBackground} urlImage={urlBgImage} />
        <div className="container--body">
          <div className="container text-center">
            <div className="container container--body_item">
              <span className="text--header">
                Design your bespoke fragrance
              </span>
              <Container className="container--body_button">
                <Row>
                  <Col md="4">
                    <Button
                      className="btn__landing"
                      onClick={this.handleLetsGuide}
                    >
                      Lets Guide
                    </Button>
                  </Col>
                  <Col md="4">
                    <Button
                      className="btn__landing"
                      onClick={this.handleMood}
                    >
                      <span>
                        Mood
                      </span>
                      <br />
                      <span style={{ fontSize: '0.8rem' }}>
                        occasion
                      </span>
                    </Button>
                  </Col>
                  <Col md="4">
                    <Button
                      className="btn__landing"
                    >
                      Do your own
                    </Button>
                  </Col>
                </Row>
              </Container>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Landing.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  questions: PropTypes.arrayOf(PropTypes.object).isRequired,
};

function mapStateToProps(state) {
  return {
    questions: state.questions,
  };
}

export default withRouter(connect(mapStateToProps, null)(Landing));
