import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import Slider from 'react-slick';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import _ from 'lodash';
import { GET_VIEW_COUNT } from '../../../../config';
import './styles.scss';
import { Row, Col, Container } from 'reactstrap';
import { withRouter, Link } from 'react-router-dom';
import { generateUrlWeb } from '../../../../Redux/Helpers';
// import { generaCurrency } from '../../../../Redux/Helpers';

class WhatTrending extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataTrendingStories: [],
      dataViewCount: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.data !== this.props.data
      && nextProps.dataArticles !== this.props.dataArticles
    ) {
      const dataFilterTrendingStories = nextProps.data && nextProps.dataArticles
        ? nextProps.dataArticles.posts.filter(
          elem => nextProps.data.find(({ value }) => elem.slug === value)
                && elem.slug,
        )
        : '';
      this.setState({
        dataTrendingStories: dataFilterTrendingStories,
      });
      if (dataFilterTrendingStories.length < 5) {
        this.fetchViewCount(5 - dataFilterTrendingStories.length);
        // console.log("asds");
      }
    }
  }

  filterViewCount = (num, dataViewCount, dataStories, dataTrendingStories) => {
    const dataFilter = dataStories.filter(
      ({ slug }) => !dataTrendingStories.some(x => x.slug == slug),
    );
    const keysSorted = Object.keys(dataViewCount).sort((a, b) => dataViewCount[b].views_per_week - dataViewCount[a].views_per_week);
    const arrNew = [...this.state.dataTrendingStories];
    for (let i = 0; i < keysSorted.length; i++) {
      //  console.log(keysSorted[i])
      for (let j = 0; j < dataFilter.length; j++) {
        // console.log("hello", dataTopStories.find(keysSorted[i]));
        if (keysSorted[i] === dataFilter[j].slug && j < num) {
          arrNew.push(dataFilter[j]);
        }
      }
    }
    return arrNew;
  };

  fetchViewCount = (num) => {
    const option = {
      url: GET_VIEW_COUNT,
      method: 'GET',
    };

    const pending = [fetchClient(option, true)];

    Promise.all(pending)
      .then((result) => {
        if (result && result.length > 0) {
          const rsv = this.filterViewCount(
            num,
            result[0],
            this.props.dataArticles.posts,
            this.state.dataTrendingStories,
          );
          this.setState({ dataTrendingStories: rsv });
          return;
        }
        throw new Error(result.message);
      })
      .catch((err) => {
        toastrError(err.message);
      });
  };

  handleRoute = (slug) => {
    this.props.history.push(generateUrlWeb(`/blog/${slug}`));
    window.location.reload();
  };

  render() {
    const { labelBlocks } = this.props;
    const { dataTrendingStories } = this.state;
    const whatTrending = _.filter(
      labelBlocks,
      x => x.value.name === 'what_trending',
    );

    return (
      <div className="what-trending-section">
        <div className="title-center">
          <h3>{whatTrending[0] ? whatTrending[0].value.label : 'what trending'}</h3>
        </div>
        <div className="articles-box">
          {dataTrendingStories
            ? dataTrendingStories.slice(0, 5).map(x => (
              <div className="article">
                <div
                  className="image-article"
                  onClick={() => this.handleRoute(x.slug)}
                >
                  <div
                    className="image"
                    style={{
                      backgroundImage: `url(${x.image ? x.image : ''})`,
                    }}
                  />
                </div>
                <div className="info-article">
                  <div className="info-title">
                    <span className="cat">
                      {x.categories
                        ? _.map(x.categories, (x, i) => (
                          <Link to={generateUrlWeb(`/blog/categories/${x.split(' ').join('-')}`)} key={i}>
                            {i === 0 ? x : `, ${x}`}
                          </Link>
                        ))
                        : ''}
                    </span>
                  </div>
                  <Link to={generateUrlWeb(`/blog/${x.slug}`)} className="title">
                    {x.title ? x.title : ''}
                  </Link>
                </div>
              </div>
            ))
            : ''}
        </div>
      </div>
    );
  }
}

export default withRouter(WhatTrending);
