import React, { Component } from 'react';
import './styles.scss';
import _ from 'lodash';
import moment from 'moment';
import { Link, withRouter } from 'react-router-dom';
import icSeeMore from '../../../../image/icon/ic-see-more.svg';
import icDefault from '../../../../image/icon/default-image.png';
import { generateUrlWeb } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';

class RecentStories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataRecentStories: [],
      num: 6,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.dataArticles !== this.props.dataArticles) {
      const dataFilterRecentStories = nextProps.dataArticles
        ? nextProps.dataArticles.posts.filter(
          elem => new Date(
            moment(elem.first_published_at).format('YYYY-MM-DD')
                  < new Date(),
          ),
        )
        : '';
      this.setState({
        dataRecentStories: _.orderBy(dataFilterRecentStories, ['first_published_at'], ['desc']),
      });
    }
  }

  handleLoadMore = () => {
    if (this.state.dataRecentStories.length > this.state.num) {
      this.setState({ num: this.state.num + 6 });
    }
  };

  handleRoute = (slug) => {
    this.props.history.push(generateUrlWeb(`/blog/${slug}`));
  };

  render() {
    const { dataArticles, isArticlePage, labelBlocks } = this.props;
    const { dataRecentStories, num } = this.state;
    const recentStories = _.filter(
      labelBlocks,
      x => x.value.name === 'recents_stories',
    );
    const loadMore = _.filter(
      labelBlocks,
      x => x.value.name === 'load_more',
    );

    return (
      <div className="recent-stories-section">
        <div className="hr-title-left">
          <span>{recentStories[0] ? recentStories[0].value.label : 'recent stories'}</span>
          <div className="hr" />
        </div>
        <div className="articles-box">
          {dataRecentStories
            ? dataRecentStories.slice(0, num).map((x, index) => (
              <div className="article animated faster fadeIn" key={index}>
                <div
                  className="wrapper-image-article"
                  onClick={() => this.handleRoute(x.slug)}
                >
                  <img loading="lazy" src={x.image ? x.image : icDefault} alt="icon" />
                </div>
                <div className="content-article">
                  {isMobile ? (
                    <div className="info-title">
                      <span className="cat">
                        {x.categories
                          ? _.map(x.categories, (x, i) => (
                            <Link to={generateUrlWeb(`/blog/categories/${x.split(' ').join('-')}`)} key={i}>
                              {i === 0 ? x : `, ${x}`}
                            </Link>
                          ))
                          : ''}
                      </span>
                      <span
                        className="date"
                        onClick={() => this.handleRoute(x.slug)}
                      >
                        {x.first_published_at
                          ? moment(x.first_published_at).format(
                            'MMMM DD, YYYY',
                          )
                          : ''}
                      </span>
                    </div>
                  ) : (
                    <span className="cat">
                      {x.categories
                        ? _.map(x.categories, (x, i) => (
                          <Link to={generateUrlWeb(`/blog/categories/${x.split(' ').join('-')}`)} key={i}>
                            {i === 0 ? x : `, ${x}`}
                          </Link>
                        ))
                        : ''}
                    </span>
                  )}
                  <Link to={generateUrlWeb(`/blog/${x.slug}`)} className="title-article">
                    {x.title ? x.title : ''}
                  </Link>
                  {!isMobile && (
                  <span
                    className="date"
                    onClick={() => this.handleRoute(x.slug)}
                  >
                    {x.first_published_at
                          && moment(x.first_published_at).format(
                            'ddd, DD MMMM YYYY',
                          )}
                  </span>
                  )}
                </div>
              </div>
            ))
            : ''}
        </div>
        {isArticlePage && isMobile ? (
          <div
            className="paging-mobile"
            style={{
              display: dataRecentStories.length > num ? 'block' : 'none',
            }}
          >
            <div className="overlay" />
            <div className="wrap-btn">
              <button
                type="button"
                onClick={() => this.handleLoadMore()}
                disbled={!dataRecentStories.length > 6}
                style={{ opacity: dataRecentStories.length > 6 ? 1 : 0.2 }}
              >
                See more
                <img src={icSeeMore} alt="icon" />
              </button>
            </div>
          </div>
        ) : (
          <div className="paging">
            <p>
              Showing
              {' '}
              {dataRecentStories.length > num ? num : dataRecentStories.length}
              {' '}
              of
              {' '}
              {dataRecentStories.length}
              {' '}
              articles
            </p>
            <button
              type="button"
              onClick={() => this.handleLoadMore()}
              disbled={!dataRecentStories.length > 6}
              style={{ opacity: dataRecentStories.length > 6 ? 1 : 0.2 }}
            >
              {loadMore && loadMore[0] ? loadMore[0].value.label : 'load more'}
            </button>
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(RecentStories);
