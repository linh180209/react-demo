import _ from 'lodash';
import React, { Component } from 'react';
import { SUBSCRIBER_NEWLETTER_URL } from '../../../../config';
import { isMobile } from '../../../../DetectScreen';
import { validateEmail } from '../../../../Redux/Helpers';
import auth from '../../../../Redux/Helpers/auth';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import {
  toastrError,
  toastrSuccess
} from '../../../../Redux/Helpers/notification';
import './styles.scss';

class SubscribeMail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: undefined,
    };
  }

  onChangeEmail = (e) => {
    const { value } = e.target;
    this.setState({ email: value });
  };

  postNewLetter = (email) => {
    if (!validateEmail(email)) {
      toastrError('Email is not avaiable');
      return;
    }

    const { login } = this.props;
    const options = {
      url: SUBSCRIBER_NEWLETTER_URL,
      method: 'POST',
      body: {
        email,
        user: login && login.user ? login.user.id : undefined,
      },
    };
    fetchClient(options)
      .then((result) => {
        if (result.isError) {
          if (result.email) {
            toastrError('You already have registered');
          } else {
            toastrError('You have already subscribed to our newsletter');
          }
          return;
        }
        auth.setEmail(email);
        toastrSuccess('Email sent successfully');
        if (options.body.user) {
          this.props.loginUpdateUserInfo({ is_get_newsletters: true });
        }
      })
      .catch((err) => {
        toastrError(err);
      });
  };

  render() {
    const { data, labelBlocks } = this.props;
    const { email } = this.state;
    const subscribe = _.find(
      labelBlocks,
      x => x.value.name === 'subscribe',
    );
    const joinTheMaisonFamily = _.find(
      labelBlocks,
      x => x.value.name === 'Join_the_maison_family',
    );
    return (
      <div className="subscribe-mail-section">
        <div className="image-sc">
          <div
            className="image"
            style={{
              backgroundImage: `url(${
                data && data.image ? data.image.image : ''
              })`,
            }}
          />
        </div>
        <div className="info-sc-mail">
          {isMobile && <h4>{joinTheMaisonFamily ? joinTheMaisonFamily.value.label : 'Join the Maison 21G Family'}</h4>}
          <p className="title">{data && data.text ? data.text : ''}</p>
          <div className="ip-sc">
            <input
              type="email"
              name="title"
              aria-label="Your Email"
              aria-describedby="basic-addon2"
              placeholder={data && data.button ? data.button.placeholder : ''}
              onChange={this.onChangeEmail}
            />
            <button
              className="sc-btn"
              onClick={() => this.postNewLetter(email)}
              disabled={_.isEmpty(email)}
            >
              {data && data.button ? data.button.text : ''}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default SubscribeMail;
