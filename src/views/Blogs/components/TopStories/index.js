import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import _ from 'lodash';
import { Row, Col, Container } from 'reactstrap';
import moment from 'moment';
import { Link, withRouter } from 'react-router-dom';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import { GET_VIEW_COUNT } from '../../../../config';
import './styles.scss';

import { generateUrlWeb, isNumeric } from '../../../../Redux/Helpers';

// import { generaCurrency } from '../../../../Redux/Helpers';

class TopStories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataTopStories: [],
      dataViewCount: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.data !== this.props.data
      && nextProps.dataArticles !== this.props.dataArticles
    ) {
      const dataFilterTopStories = nextProps.data && nextProps.dataArticles
        ? nextProps.dataArticles.posts.filter(
          elem => nextProps.data.find(({ value }) => elem.slug === value)
                && elem.slug,
        )
        : '';
      this.setState({
        dataTopStories:
          dataFilterTopStories.length > 4
            ? dataFilterTopStories.slice(0, 4)
            : dataFilterTopStories,
      });
      if (dataFilterTopStories.length < 4) {
        this.fetchViewCount(4 - dataFilterTopStories.length);
      }
    }
  }

  filterViewCount = (num, dataViewCount, dataStories, dataTopStories) => {
    const dataFilter = dataStories.filter(
      ({ slug }) => !dataTopStories.some(x => x.slug == slug),
    );
    const keysSorted = Object.keys(dataViewCount).sort((a, b) => dataViewCount[b].views_per_day - dataViewCount[a].views_per_day);
    const arrNew = [...this.state.dataTopStories];
    for (let i = 0; i < keysSorted.length; i++) {
      for (let j = 0; j < dataFilter.length; j++) {
        if (keysSorted[i] === dataFilter[j].slug && j < num) {
          // console.log(dataFilter[j]);
          arrNew.push(dataFilter[j]);
        }
      }
    }
    return arrNew;
  };

  fetchViewCount = (num) => {
    const option = {
      url: GET_VIEW_COUNT,
      method: 'GET',
    };

    const pending = [fetchClient(option, true)];

    Promise.all(pending)
      .then((result) => {
        if (result && result.length > 0) {
          const rsv = this.filterViewCount(
            num,
            result[0],
            this.props.dataArticles.posts,
            this.state.dataTopStories,
          );
          this.setState({ dataTopStories: rsv });
          return;
        }
        throw new Error(result.message);
      })
      .catch((err) => {
        toastrError(err.message);
      });
  };

  handleRoute = (slug) => {
    this.props.history.push(generateUrlWeb(`/blog/${slug}`));
  };

  render() {
    const { data, dataArticles, labelBlocks } = this.props;
    const { dataViewCount, dataTopStories } = this.state;
    const today_top_stories = _.filter(
      labelBlocks,
      x => x.value.name === 'today_top_stories',
    );
    return (
      <div className="top-stories-section">
        <div className="hr-title-center">
          <div className="hr" />
          <span>
            {today_top_stories[0]
              ? today_top_stories[0].value.label
              : 'today’s top storiess'}
          </span>
          <div className="hr" />
        </div>
        <Row>
          {_.map(_.orderBy(dataTopStories, ['first_published_at'], ['desc']), x => (
            <Col md="3">
              <div className="article">
                <div className="image-article">
                  <div
                    className="image"
                    onClick={() => this.handleRoute(x.slug)}
                    style={{
                      backgroundImage: `url(${x.image ? x.image : ''})`,
                    }}
                  />
                  <div className="hover-l" onClick={() => this.handleRoute(x.slug)} />
                  <div className="tag">{x.tags[0] && x.tags[0]}</div>
                </div>
                <div className="info-article">
                  <div className="info-title">
                    <Link
                      className="cat"
                      to={generateUrlWeb(`/blog/categories/${x.categories[0].split(' ').join('-')}`)}
                    >
                      {x.categories[0] ? x.categories[0] : ''}
                    </Link>
                    <span
                      className="date"
                      onClick={() => this.handleRoute(x.slug)}
                    >
                      {x.first_published_at
                        ? moment().subtract(7, 'd').format('YYYY-MM-DD')
                          < moment(x.first_published_at).format('YYYY-MM-DD')
                          ? moment(x.first_published_at).fromNow()
                          : moment(x.first_published_at).format('DD/MM/YYYY')
                        : ''}
                    </span>
                  </div>
                  <Link to={generateUrlWeb(`/blog/${x.slug}`)} className="title">
                    {x.title ? x.title : ''}
                  </Link>
                  <div className="des" onClick={() => this.handleRoute(x.slug)}>
                    {htmlPare(x.description ? x.description : '')}
                  </div>
                </div>
              </div>
            </Col>
          ))}
        </Row>
      </div>
    );
  }
}

export default withRouter(TopStories);
