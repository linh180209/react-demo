import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import Slider from 'react-slick';
import './styles.scss';
import { Row, Col, Container } from 'reactstrap';
import { Link, withRouter } from 'react-router-dom';
import _ from 'lodash';
import icExit from '../../../../image/icon/ic-exit-black.svg';
import { generateUrlWeb } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';

class SearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActiveSearch: false,
      value: '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.keyPress = this.keyPress.bind(this);
  }

  toggleSearchbar = () => {
    this.setState({ isActiveSearch: !this.state.isActiveSearch });
  };

  handleRoute = (cat) => {
    window.location.reload();
    this.props.history.push(generateUrlWeb(`/blog/categories/${cat.split(' ').join('-')}`));
  };

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  keyPress(e) {
    if (e.keyCode == 13) {
      if (e.target.value.trim().length > 0) {
        this.props.history.push(generateUrlWeb(`/blog/search/${e.target.value.trim()}`));
        window.location.reload();
      }
    }
  }

  render() {
    const { isActiveSearch } = this.state;
    const { data } = this.props;
    return (
      <div className="search-bar-section">
        {isMobile ? (
          <div
            className={` container-search-blog-mobile ${
              isActiveSearch ? 'active' : ''
            }`}
          >
            <i
              className="fa fa-search"
              onClick={() => this.toggleSearchbar()}
            />
            <input
              type="search"
              placeholder=""
              onChange={this.handleChange}
              onKeyDown={this.keyPress}
            />
            <img
              style={{ width: '14px', marginLeft: '40px', cursor: 'pointer' }}
              src={icExit}
              alt="icExit"
              onClick={() => {
                this.toggleSearchbar();
              }}
            />
          </div>
        ) : (
          ''
        )}
        <ul className={`wrapper-menu ${isActiveSearch ? 'active' : ''}`}>
          {_.map(data && data.categories ? data.categories : [], (x, i) => (
            <li>
              <Link to={generateUrlWeb(`/blog/categories/${x.name.split(' ').join('-')}`)} key={i}>
                {x.name}
              </Link>
            </li>
          ))}
        </ul>
        {!isMobile ? (
          <div
            className={` container-search-blog ${
              isActiveSearch ? 'active' : ''
            }`}
          >
            <i
              className="fa fa-search"
              onClick={() => this.toggleSearchbar()}
            />
            <input
              type="search"
              placeholder={`${!isMobile ? 'Search Maison 21G Blog' : ''}`}
              onChange={this.handleChange}
              onKeyDown={this.keyPress}
            />
            <img
              style={{ width: '14px', marginLeft: '40px', cursor: 'pointer' }}
              src={icExit}
              alt="icExit"
              onClick={() => {
                this.toggleSearchbar();
              }}
            />
          </div>
        ) : (
          ''
        )}
      </div>
    );
  }
}

export default withRouter(SearchBar);
