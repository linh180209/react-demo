import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import './styles.scss';
import _ from 'lodash';
import { Row, Col, Container } from 'reactstrap';
import {
  Player, ControlBar, Shortcut, BigPlayButton,
} from 'video-react';
import { Link } from 'react-router-dom';
import { generateUrlWeb } from '../../../../Redux/Helpers';

class Video extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataVideoStories: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.data !== this.props.data
      && nextProps.dataArticles !== this.props.dataArticles
    ) {
      const dataFilterVideoStories = nextProps.data && nextProps.dataArticles
        ? nextProps.dataArticles.posts.filter(
          elem => elem.slug == nextProps.data.post,
        )[0]
        : '';
      this.setState({
        dataVideoStories: dataFilterVideoStories,
      });
    }
  }

  render() {
    const { data, labelBlocks } = this.props;
    const { dataVideoStories } = this.state;
    const urlVideo = data && data.videos ? data.videos[0].value.video : '';
    const posterVideo = data && data.videos ? data.videos[0].value.placeholder : '';
    const clickHere = _.filter(
      labelBlocks,
      x => x.value.name === 'click_here_to_read_the_story',
    );
    const brandLogoText = _.find(
      labelBlocks,
      x => x.value.name === 'brand_logo_video',
    );
    return (
      <div className="video-section">
        <div className="wrapper-video">
          <Player
            playsInline
            muted
            autobuffer
            src={urlVideo}
            poster={posterVideo || ''}
            fluid={false}
            width="100%"
            height="100%"
            playIcon={<button>Play</button>}
          >
            <ControlBar />
            <Shortcut dblclickable />
            <BigPlayButton position="center" />
          </Player>
        </div>
        <div className="wrapper-content">
          <div className="content-video">
            <h3>
              Maision 21G
              {' '}
              <span>{brandLogoText ? brandLogoText.value.label : 'channel'}</span>
            </h3>
            <p className="cat">
              {' '}
              {dataVideoStories?.categories
                ? _.join(dataVideoStories?.categories, ', ')
                : ''}
            </p>
            <h4 className="title">
              {dataVideoStories?.title ? dataVideoStories?.title : ''}
            </h4>
            <div className="info">
              {htmlPare(
                dataVideoStories?.description ? dataVideoStories?.description : '',
              )}
            </div>
            <Link
              to={generateUrlWeb(`/blog/${
                dataVideoStories?.slug ? dataVideoStories?.slug : '#'
              }`)}
            >
              {clickHere[0]
                ? clickHere[0].value.label
                : 'click here to read the story'}
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Video;
