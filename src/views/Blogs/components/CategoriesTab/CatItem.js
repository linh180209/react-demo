import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import icDefault from '../../../../image/icon/default-image.png';
import moment from 'moment';
import './styles.scss';
import { withRouter, Link } from 'react-router-dom';

// import { Row, Col, Container } from "reactstrap";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col,
} from 'reactstrap';
import { generateUrlWeb } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';

class CatItem extends Component {
  handleRoute = (slug) => {
    this.props.history.push(generateUrlWeb(`/blog/${slug}`));
  };

  render() {
    const { dataFilterCat, activeTabName } = this.props;

    return (
      <div className="categories-tab-section">
        <Row
          className="animated faster fadeIn"
          style={{ margin: isMobile && 0 }}
        >
          {dataFilterCat
            ? dataFilterCat.slice(0, 8).map(x => (
              <Col
                sm="3"
                className="animated faster fadeIn"
                style={{ padding: isMobile && 0 }}
              >
                <div className="article">
                  <div
                    className="image-article"
                    onClick={() => this.handleRoute(x.slug)}
                  >
                    <div
                      className="image"
                      style={{
                        backgroundImage: `url(${
                          x.image ? x.image : icDefault
                        })`,
                      }}
                    />
                    {/* <div className="tag">New</div> */}
                  </div>
                  <div className="info-article">
                    <div className="info-title">
                      <span className="cat">
                        {/* {x.categories ? _.join(x.categories, ", ") : ""} */}
                        {/* x.categories ? (
                            _.map(x.categories, (x, i) => (
                              <Link to={`/blog/categories/${x}`} key={i}>
                                {i == 0 ? x : `, ${x}`}
                              </Link>
                            ))
                          ) : '' */}
                        {activeTabName ? (
                          <Link to={generateUrlWeb(`/blog/categories/${activeTabName.split(' ').join('-')}`)}>
                            {activeTabName}
                          </Link>
                        ) : (
                          ''
                        )}
                      </span>
                      <span
                        className="date"
                        onClick={() => this.handleRoute(x.slug)}
                      >
                        {x.first_published_at
                          ? moment().subtract(7, 'd').format('YYYY-MM-DD')
                              < moment(x.first_published_at).format('YYYY-MM-DD')
                            ? moment(x.first_published_at).fromNow()
                            : moment(x.first_published_at).format(
                              'DD/MM/YYYY',
                            )
                          : ''}
                      </span>
                    </div>
                    <Link to={generateUrlWeb(`/blog/${x.slug}`)} className="title-article">
                      {x.title ? x.title : ''}
                    </Link>
                  </div>
                </div>
              </Col>
            ))
            : ''}
        </Row>
      </div>
    );
  }
}

export default withRouter(CatItem);
