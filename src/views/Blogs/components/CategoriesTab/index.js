import React, { Component } from 'react';
import htmlPare from 'react-html-parser';
import moment from 'moment';
import './styles.scss';
import { withRouter, Link } from 'react-router-dom';
import Tabs from 'react-responsive-tabs';

// IMPORTANT you need to include the default styles
import 'react-responsive-tabs/styles.css';

// import { Row, Col, Container } from "reactstrap";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col,
} from 'reactstrap';
import classnames from 'classnames';

import CatItem from './CatItem';
import icDefault from '../../../../image/icon/default-image.png';
import { generateUrlWeb } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';

class CategoriesTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: '0',
      dataFilterCat: null,
      activeTabName: '',
    };
  }

  getTabs = () => (
    this.props.dataArticles
      && this.props.dataArticles.categories.map((i, index) => ({
        key: i.name, // Optional. Equals to tab index if this property is omitted
        tabClassName: 'tab-custom', // Optional
        panelClassName: 'panel-custom', // Optional
        title: i.name,
        // getContent: () => (),
      }))
  );

  toggle = (tab, cat) => {
    if (this.state.activeTab !== tab) this.setState({ activeTab: tab });
    const dataFil = this.props.dataArticles.posts.filter(items => items.categories.find(e => e == cat));
    this.setState({
      dataFilterCat: dataFil,
      activeTabName: cat,
    });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.dataArticles !== this.props.dataArticles) {
      const dataFilter = nextProps.dataArticles
        ? nextProps.dataArticles.posts.filter(items => items.categories.find(
          e => e == nextProps.dataArticles.categories[0].name,
        ))
        : '';
      this.setState({
        dataFilterCat: dataFilter,
        activeTab: '0',
      });
    }
  }

  handleRoute = (slug) => {
    this.props.history.push(generateUrlWeb(`/blog/${slug}`));
  };

  render() {
    const { dataArticles } = this.props;
    const { activeTab, dataFilterCat, activeTabName } = this.state;

    return (
      <div className="categories-tab-section" style={{ marginTop: !isMobile ? '4rem' : '1rem' }}>
        {/* {!isMobile ? (
          <Tabs
            items={this.getTabs()}
            showInkBar={true}
            onChange={(key) => {
              this.toggle(0, key);
            }}
          />
        ) : ( */}
        <div style={{ position: 'relative' }}>
          <ul className="Tabs">
            {dataArticles
              ? dataArticles.categories.map((x, index) => (
                <li
                  className={classnames('Tabs__tab', 'Tab', {
                    active: activeTab == index,
                  })}
                  onClick={() => {
                    this.toggle(index, x.name);
                  }}
                >
                  {x.name}
                </li>
              ))
              : ''}
            <li
              className={`Tabs__presentation-slider active-tab-${activeTab}`}
              role="presentation"
              style={{
                width: `${
                  dataArticles ? 100 / dataArticles.categories.length : 1
                }%`,
                left: `${
                  dataArticles
                    ? (100 / dataArticles.categories.length) * activeTab
                    : 1
                }%`,
              }}
            >
              <div className="line" />
            </li>
          </ul>
          <hr />
        </div>
        {/* )} */}

        <CatItem dataFilterCat={dataFilterCat} activeTabName={activeTabName || (dataArticles ? dataArticles.categories[0].name : '')} />
        {/* <Row className="animated faster fadeIn" style={{margin: isMobile && 0 }}>
          {dataFilterCat
            ? dataFilterCat.slice(0, 8).map((x) => (
                <Col sm="3" className="animated faster fadeIn" style={{padding: isMobile && 0 }}>
                  <div className="article" onClick={()=>this.handleRoute(x.slug)}>
                    <div className="image-article">
                      <div
                        className="image"
                        style={{
                          backgroundImage: `url(${
                            x.image ? x.image : icDefault
                          })`,
                        }}
                      ></div>
                    </div>
                    <div className="info-article">
                      <div className="info-title">
                        <span className="cat">
                          {x.categories ? _.join(x.categories, ", ") : ""}
                        </span>
                        <span className="date">
                          {x.first_published_at &&
                            moment(x.first_published_at).format(
                              "ddd, DD MMMM YYYY"
                            )}
                        </span>
                      </div>
                      <Link to={`/blog/${x.slug}`} className="title-article">
                        {x.title ? x.title : ""}
                      </Link>
                    </div>
                  </div>
                </Col>
              ))
            : ""}
        </Row> */}
        <div className="link-cat animated faster fadeIn">
          <Link
            to={
              dataArticles
                ? generateUrlWeb(`/blog/categories/${dataArticles.categories[activeTab].name.split(' ').join('-')}`)
                : generateUrlWeb('#')
            }
          >
            more
            {' '}
            {dataArticles ? dataArticles.categories[activeTab].name : ''}
          </Link>
        </div>
      </div>
    );
  }
}

export default withRouter(CategoriesTab);
