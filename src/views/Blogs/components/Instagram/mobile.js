import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const sliderSetting = {
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  dots: false,
  centerMode: true,
};

const RenderItem = (props) => {
  const { item } = props;
  return (
    <div>
      <div className="__item">
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={`https://www.instagram.com/p/${item.shortcode}/`}
        >
          <img loading="lazy" src={item.thumbnail_src} alt="" className="img-fluid" />
        </a>
      </div>
    </div>
  );
};

class Instagram extends Component {
  render() {
    const { data, instagramData, labelBlocks } = this.props;
    const onTheGram = _.filter(
      labelBlocks,
      x => x.value.name === 'on_the_gram',
    );
    return (
      <div className="about-instagram-blogs">
        <div className="hr-title-center">
          <div className="hr" />
          <span>{onTheGram[0] ? onTheGram[0].value.label : 'on the ‘gram'}</span>
          <div className="hr" />
        </div>
        {instagramData.length !== 0 && (
          <div className="__slider mt-4">
            <Slider {...sliderSetting}>
              {instagramData.map(item => (
                <RenderItem item={item || {}} />
              ))}
            </Slider>
          </div>
        )}
      </div>
    );
  }
}

export default Instagram;
