import React, { Component } from 'react';
import _ from 'lodash';
import './styles.scss';
import InstagramMobile from './mobile';
import InstagramWeb from './web';
import { isMobileOnly } from '../../../../DetectScreen';
import { API_INSTAGRAM } from '../../../../config';

class Instagram extends Component {
  constructor(props) {
    super(props);

    this.state = {
      instagramData: [],
    };
  }

  componentDidMount() {
    fetch(
      API_INSTAGRAM,
    )
      .then(response => response.json())
      .then((data) => {
        const instagramData = data.graphql.user.edge_owner_to_timeline_media.edges.map(
          item => item.node,
        );
        this.setState({
          instagramData,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  render() {
    const { data, labelBlocks } = this.props;
    const { instagramData } = this.state;
    if (isMobileOnly) {
      return <InstagramMobile data={data} instagramData={instagramData} labelBlocks={labelBlocks} />;
    }
    return <InstagramWeb data={data} instagramData={instagramData} labelBlocks={labelBlocks} />;
  }
}

export default Instagram;
