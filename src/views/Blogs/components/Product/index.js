import _ from 'lodash';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import bottleDefault from '../../../../image/bottle-new.png';
import { generaCurrency, generateUrlWeb } from '../../../../Redux/Helpers';
import './styles.scss';

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataBestSeller: [],
    };
  }

  componentDidMount() {
    // this.fetchDataBestSellerProduct();
  }

  // fetchDataBestSellerProduct = () => {
  //   const option = {
  //     url: GET_PRODUCT_TYPE_LINK.replace("{type}", `best_seller`),
  //     method: "GET",
  //   };

  //   const pending = [fetchClient(option, true)];

  //   Promise.all(pending)
  //     .then((result) => {
  //       if (result && result.length > 0) {
  //         console.log("best seller", result);
  //         this.setState({ dataBestSeller: result });
  //         return;
  //       }
  //       throw new Error(result.message);
  //     })
  //     .catch((err) => {
  //       toastrError(err.message);
  //     });
  // };

  onClickProduct = (product) => {
    const { type } = product;
    let url;
    if (type.name === 'Wax_Perfume') {
      url = `/product/wax/${product.product}`;
    } else if (type.name === 'Elixir') {
      url = `/product/elixir/${product.product}`;
    } else if (type.name === 'Kit') {
      url = `/product/kit/${product.product}`;
    } else if (type.name === 'bundle') {
      url = `/product/bundle/${product.product}`;
    } else if (type.name === 'Scent') {
      url = `/product/Scent/${product.product}`;
    } else if (type.name === 'hand_sanitizer') {
      url = `/product/hand-sanitizer/${product.product}/${product.id}`;
    } else if (type.name === 'home_scents') {
      url = `/product/home-scents/${product.product}/${product.id}`;
    } else if (type.name === 'Perfume') {
      url = `/product/Perfume/${product.product}`;
    } else if (type.name === 'dual_candles') {
      url = `/product/dual-candles/${product.product}`;
    } else if (type.name === 'single_candle') {
      url = `/product/single-candle/${product.product}`;
    } else if (type.name === 'holder') {
      url = `/product/holder/${product.product}`;
    } else if (type.name === 'gift_bundle') {
      url = `/product/gift-bundle/${product.product}`;
    } else if (type.name === 'mini_candles') {
      url = `/product/mini_candles/${product.product}`;
    }
    this.props.history.push(generateUrlWeb(url));
  }

  render() {
    const { data, labelBlocks } = this.props;
    const { dataBestSeller } = this.state;
    const shopMaison21g = _.find(
      labelBlocks,
      x => x.value.name === 'shop_maison_21g',
    );
    const shopAtMaison21g = _.find(
      labelBlocks,
      x => x.value.name === 'shop_maison_at_21g',
    );
    return (
      <div className="product-section">
        <div className="title-center">
          <h1>
            {shopMaison21g ? shopMaison21g.value.label : 'shop maison 21g'}
          </h1>
        </div>
        <div className="products-box">
          {data && data.items
            ? data.items.slice(0, 4).map(x => (
              <div className="product">
                <div className="image-product">
                  <div
                    className="image"
                    onClick={() => this.onClickProduct(x)}
                    style={{
                      backgroundImage: `url(${
                        x.image ? x.image : bottleDefault
                      })`,
                    }}
                  />
                </div>
                <div className="info-product">
                  <h4 className="title">{x.name ? x.name : ''}</h4>
                  <div className="info-title">
                    <p className="info">{x.short_description ? x.short_description : ''}</p>
                    <p className="price">
                      {' '}
                      {generaCurrency(x.price ? x.price : '')}
                    </p>
                  </div>
                </div>
              </div>

            ))
            : ''}
        </div>
        <Link to={generateUrlWeb('/all-products')} className="link-shop">
          {shopAtMaison21g ? shopAtMaison21g.value.label : 'SHOP AT MAISON 21G'}
        </Link>
      </div>
    );
  }
}

export default Product;
