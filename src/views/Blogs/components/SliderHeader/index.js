import React, { Component } from 'react';
import _ from 'lodash';
import htmlPare from 'react-html-parser';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './styles.scss';
import { Row, Col, Container } from 'reactstrap';
import { Link } from 'react-router-dom';
import { generateUrlWeb } from '../../../../Redux/Helpers';
import { isMobile } from '../../../../DetectScreen';
// import { generaCurrency } from '../../../../Redux/Helpers';

class SliderHeader extends Component {
  render() {
    const { data } = this.props;
    const sliderSetting = {
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: false,
      arrows: !isMobile,
      infinite: true,
      dots: true,
      autoplay: true,
      autoplaySpeed: 5000,
      variableWidth: true,
      appendDots: dots => (
        <div
          style={{
            padding: '10px',
          }}
        >
          <ul style={{ margin: '0px' }}>{dots}</ul>
        </div>
      ),
      customPaging: i => (
        <div>
          <div className="dot">
            <div className="s-dot" />
          </div>
        </div>
      ),
    };
    return data ? (
      <div
        className="slider-header"
        id="slider-header"
        //   d-flex justify-content-center align-items-center
      >
        <Slider {...sliderSetting}>
          {_.map(data, x => (
            <div>
              <a
                className="wrap-a"
                href={x.value.button.link ? x.value.button.link : '#'}
              >
                <Row noGutters style={{ width: '100vw' }}>
                  <Col md="6">
                    <img
                      src={x.value.image.image ? x.value.image.image : ''}
                      alt="banner-header"
                    />
                    <div className="tag-slider">New</div>
                  </Col>
                  <Col md="6">
                    <div
                      className="wrapper-content-sli"
                      style={{
                        backgroundColor: x.value.background_color
                          ? x.value.background_color
                          : '#0d0d0d',
                      }}
                    >
                      <div>
                        <h3
                          style={{
                            color:
                              x.value.background_color != '#0d0d0d'
                                ? '#ffffff'
                                : '',
                          }}
                        >
                          {x.value.header ? x.value.header.header_text : ''}
                        </h3>
                        <div className="d-flex justify-content-center align-items-center" style={{ margin: '1.5em 0 2em' }}>
                          <h2>
                            {x.value.header2 ? x.value.header2.header_text : ''}
                          </h2>
                        </div>
                        <Link
                          to={
                            x.value.button.link ? generateUrlWeb(x.value.button.link) : generateUrlWeb('/blog')
                          }
                        >
                          {x.value.button ? x.value.button.text : 'read more'}
                        </Link>
                      </div>
                    </div>
                  </Col>
                </Row>
              </a>
            </div>
          ))}
        </Slider>
      </div>
    ) : (
      ''
    );
  }
}

export default SliderHeader;
