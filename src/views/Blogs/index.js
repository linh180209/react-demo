/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import '../../styles/style.scss';
import {
  Player, ControlBar, Shortcut, BigPlayButton,
} from 'video-react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import MetaTags from 'react-meta-tags';

import { Container, Row, Col } from 'reactstrap';
import HeaderHomePageV3 from '../../components/HomePage/HeaderHomePageV3';
import { toastrError } from '../../Redux/Helpers/notification';
import loadingPage from '../../Redux/Actions/loading';
import icBackToTop from '../../image/icon/ic-back-to-top.svg';
import icBackToTopMobile from '../../image/icon/ic-scroll-top-mobile.svg';
import { loginUpdateUserInfo } from '../../Redux/Actions/login';
import {
  scrollTop,
  getSEOFromCms,
  getNameFromCommon,
  getCmsCommon,
  fetchCMSHomepage,
  googleAnalitycs,
  generateHreflang,
  removeLinkHreflang,
  setPrerenderReady,
} from '../../Redux/Helpers';
import fetchClient, {
  fetchClientFormData,
} from '../../Redux/Helpers/fetch-client';
import './styles.scss';
import addCmsRedux from '../../Redux/Actions/cms';
import SearchBar from './components/SearchBar';
import SliderHeader from './components/SliderHeader';
import TopStories from './components/TopStories';
import RecentStories from './components/RecentStories';
import WhatTrending from './components/WhatTrending';
import SubscribeMail from './components/SubscribeMail';
import Product from './components/Product';
import CategoriesTab from './components/CategoriesTab';
import Video from './components/Video';
import Instagram from './components/Instagram';
import { isMobile } from '../../DetectScreen';
import auth from '../../Redux/Helpers/auth';
import FooterV2 from '../../views2/footer';
import { replaceCountryInMetaTags } from '../../Utils/replaceCountryInMetaTags';

const prePareCms = (mmoCms) => {
  if (mmoCms) {
    const seo = getSEOFromCms(mmoCms);
    // const { body } = mmoCms;
    return {
      seo,
    };
  }
  return {
    seo: undefined,
  };
};

const prePareDataArticle = (data) => {
  const { body } = data;
  if (data) {
    const carouselBlocks = _.filter(body, x => x.type === 'carousel_block')[0]
      .value;
    const topStoriesBlocks = _.filter(
      body,
      x => x.type === 'top_stories_block',
    )[0].value;
    const whatTrendingBlocks = _.filter(
      body,
      x => x.type === 'trending_block',
    )[0].value;
    const featuredVideoBlock = _.filter(
      body,
      x => x.type === 'featured_video_block',
    )[0].value;
    const newLetterBlock = _.filter(
      body,
      x => x.type === 'newsletter_block',
    )[0].value;
    const labelBlocks = _.filter(body, x => x.type === 'label_block');
    return {
      carouselBlocks,
      topStoriesBlocks,
      whatTrendingBlocks,
      featuredVideoBlock,
      newLetterBlock,
      labelBlocks,
    };
  }
  return {
    carouselBlocks: [],
    topStoriesBlocks: [],
    whatTrendingBlocks: [],
    featuredVideoBlock: [],
    newLetterBlock: [],
    labelBlocks: [],
  };
};

class Blogs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seo: undefined,
    };
  }

  componentDidMount() {
    setPrerenderReady();
    scrollTop();
    googleAnalitycs('/blog');
    this.fetchDataInit();
    window.addEventListener('scroll', this.handleScroll);
  }

  gotoAnchor = (id) => {
    const ele = document.getElementById(id);
    if (ele) {
      ele.scrollIntoView({ behavior: 'smooth' });
      ele.scrollTop -= 300;
    }
  };

  componentDidUpdate() {
    scrollTop();
    if (this.props.match && this.props.match.params && this.props.match.params.anchor) {
      const { anchor } = this.props.match.params;
      setTimeout(() => {
        this.gotoAnchor(anchor);
      }, 1000);
    }
  }

  componentWillUnmount() {
    window.addEventListener('scroll', this.handleScroll);
    removeLinkHreflang();
  }

  handleScroll = () => {
    const ele = document.getElementById('scroll-top-btn');
    if (!ele) {
      return;
    }
    if (
      document.body.scrollTop > 600
      || document.documentElement.scrollTop > 600
    ) {
      ele.style.opacity = '1';
    } else {
      ele.style.opacity = '0';
    }
  };

  handleScrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  // fetchArticles = () => {
  //   const options = {
  //     url: GET_ALL_ARTICLES,
  //     method: 'GET',
  //   };
  //   return fetchClient(options);
  // };

  fetchDataInit = async () => {
    const { cms, mmos } = this.props;
    const { dataArticles } = this.state;
    const mmoCms = _.find(cms, x => x.title === 'blog');
    if (!mmoCms || _.isEmpty(mmos) || _.isEmpty(dataArticles)) {
      this.props.loadingPage(true);
      const pending = [fetchCMSHomepage('blog')];
      try {
        const results = await Promise.all(pending);
        const cmsData = results[0];
        const resultDataArticles = results[0];
        this.props.addCmsRedux(cmsData);
        const { seo } = prePareCms(cmsData);
        const {
          carouselBlocks,
          topStoriesBlocks,
          whatTrendingBlocks,
          featuredVideoBlock,
          newLetterBlock,
          labelBlocks,
        } = prePareDataArticle(resultDataArticles);
        this.setState({
          dataArticles: resultDataArticles,
          seo,
          carouselBlocks,
          topStoriesBlocks,
          whatTrendingBlocks,
          featuredVideoBlock,
          newLetterBlock,
          labelBlocks,
        });
        this.props.loadingPage(false);
      } catch (error) {
        toastrError(error.message);
        this.props.loadingPage(false);
      }
    } else {
      const { cms: cmsPage } = this.props;
      const cmsT = _.find(cmsPage, x => x.title === 'blog');
      const { seo } = prePareCms(cmsT);
      const products = this.createData(results);
      this.setState({
        seo,
        dataArticles,
        carouselBlocks,
        topStoriesBlocks,
        whatTrendingBlocks,
        featuredVideoBlock,
        newLetterBlock,
        labelBlocks,
      });
    }
  };

  // window.onscroll = function() {scrollFunction()};
  // // khai báo hàm scrollFunction

  render() {
    const {
      seo,
      carouselBlocks,
      topStoriesBlocks,
      whatTrendingBlocks,
      dataArticles,
      featuredVideoBlock,
      newLetterBlock,
      labelBlocks,
    } = this.state;

    const { login } = this.props;

    const gender = login && login.user && login.user.gender ? login.user.gender : 'unisex';
    const countryName = auth.getCountryName();

    return (
      <div>
        <MetaTags>
          <title>{seo ? replaceCountryInMetaTags(countryName, seo?.seoTitle) : ''}</title>
          <meta name="description" content={seo ? replaceCountryInMetaTags(seo?.seoDescription) : ''} />
          <meta name="robots" content="index, follow" />
          <meta name="revisit-after" content="3 month" />
          {generateHreflang(this.props.countries, '/blog')}
        </MetaTags>
        {/* <HeaderHomePage isBgBlack isSpecialMenu isDisableScrollShow isNotShowRegion /> */}
        <HeaderHomePageV3 isRemoveMessage />
        <div className="wrapper-blogs">
          <SearchBar data={dataArticles} />
          <SliderHeader data={carouselBlocks} />
          <div id="aa" />
          <Container className="container-lg relative">
            <div className="anchor" id="top-stories" />
            <TopStories
              data={topStoriesBlocks}
              dataArticles={dataArticles}
              labelBlocks={labelBlocks}
            />
            <Row>
              <Col md="8" className="relative">
                <div className="anchor" id="recent-stories" />
                <RecentStories
                  dataArticles={dataArticles}
                  isArticlePage={false}
                  labelBlocks={labelBlocks}
                />
              </Col>
              <Col md="4" className="relative">
                <div className="anchor" id="what-trending" />
                {!isMobile && (
                  <WhatTrending
                    data={whatTrendingBlocks}
                    dataArticles={dataArticles}
                    labelBlocks={labelBlocks}
                  />
                )}
                <SubscribeMail
                  data={newLetterBlock}
                  login={login}
                  loginUpdateUserInfo={this.props.loginUpdateUserInfo}
                  labelBlocks={labelBlocks}
                />
                <Product data={dataArticles} labelBlocks={labelBlocks} />
              </Col>
            </Row>
            <Row>
              <Col md="12">
                <CategoriesTab dataArticles={dataArticles} />
              </Col>
            </Row>
            <Row>
              <Col md="12">
                <Video
                  data={featuredVideoBlock}
                  dataArticles={dataArticles}
                  labelBlocks={labelBlocks}
                />
              </Col>
            </Row>
            {/* <Row>
              <Col md="12">
                <Instagram labelBlocks={labelBlocks} />
              </Col>
            </Row> */}
          </Container>
          <div
            className="scroll-top-btn"
            id="scroll-top-btn"
            onClick={() => this.handleScrollTop()}
            // style={{display: 'none'}}
          >
            <img src={isMobile ? icBackToTopMobile : icBackToTop} />
            <span style={{ display: isMobile ? 'none' : 'block' }}>
              back to the top
            </span>
          </div>
          <FooterV2 />
        </div>
      </div>
    );
  }
}

Blogs.propTypes = {
  loadingPage: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  loginUpdateUserInfo: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    cms: state.cms,
    mmos: state.mmos,
    basket: state.basket,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  loadingPage,
  addCmsRedux,
  loginUpdateUserInfo,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Blogs));
