import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Fade } from 'react-reveal';
import _ from 'lodash';
import '../../styles/dropdown.scss';

const DropDownMenu = (props) => {
  const [isShowSubMenu, setisShowSubMenu] = useState(false);

  const toggleHover = () => {
    setisShowSubMenu(true);
  };

  const toggleLeave = () => {
    setisShowSubMenu(false);
  };

  const onClickMenu = () => {
    props.onClickMenu(props.data);
  };

  const onClickSubMenu = (link) => {
    setisShowSubMenu(false);
    props.onClickLink(link);
  };

  return (
    <div
      onMouseEnter={toggleHover}
      onMouseLeave={toggleLeave}
    >
      <button
        type="button"
        className="bt-menu hvr-underline-from-left"
        name={props.data.value.text}
        onClick={onClickMenu}
        onMouseEnter={toggleHover}
      >
        {props.data.value.text.includes('...') ? '...' : props.data.value.text}
        {/* <div className={props.data.value.submenu.length > 0 ? 'details-menu' : ''} /> */}
      </button>
      {props.data.value.submenu.length > 0 ? (
        <div
          onMouseEnter={toggleHover}
          onMouseLeave={toggleLeave}
          className="cd-dropdown"
        >
          <div
            className={isShowSubMenu ? 'fadeIn' : 'fadeOut'}
          >
            <ul onMouseEnter={toggleHover}>
              {
                _.map(props.data.value.submenu, (d, index) => (
                  <li
                    onClick={() => onClickSubMenu(d.value.link)}
                  >
                    {d.value.text}
                  </li>
                ))
              }
            </ul>
          </div>
        </div>
      ) : <div />
      }
    </div>
  );
};

DropDownMenu.propTypes = {
  data: PropTypes.shape({
    value: PropTypes.shape(PropTypes.object),
  }).isRequired,
  onClickLink: PropTypes.func.isRequired,
  onClickMenu: PropTypes.func.isRequired,
};
export default DropDownMenu;
