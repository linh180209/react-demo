import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { isMobile } from '../../DetectScreen';

class CarouselText extends Component {
  constructor(props) {
    super(props);

    this.state = { activeIndex: 0, isNext: false };
  }

  componentWillMount = () => {
    const { data, defaultValue } = this.props;
    const index = _.findIndex(data, x => x.title === defaultValue.title);
    this.setState({ activeIndex: index });
  }

  onExiting = () => {
    this.animating = true;
  }

  onExited = () => {
    if (isMobile) {
      setTimeout(() => {
        this.animating = false;
      }, 1000);
    } else {
      this.animating = false;
    }
  }

  onAnimation = (animation) => {
    const el = document.getElementById('span-text');
    if (el.classList.contains('animation-right')) {
      el.classList.remove('animation-right');
    }
    if (el.classList.contains('animation-left')) {
      el.classList.remove('animation-left');
    }
    // eslint-disable-next-line no-unused-expressions
    el.offsetWidth;
    el.classList.add(animation);
  }

  next = () => {
    const { activeIndex } = this.state;
    const { data } = this.props;
    const nextIndex = activeIndex === data.length - 1 ? 0 : activeIndex + 1;
    this.setState({ activeIndex: nextIndex, isNext: true });
    this.props.onChange(data[nextIndex], nextIndex + 1);
    this.onAnimation('animation-right');
  }

  previous = () => {
    const { activeIndex } = this.state;
    const { data } = this.props;
    const nextIndex = activeIndex === 0 ? data.length - 1 : activeIndex - 1;
    this.setState({ activeIndex: nextIndex, isNext: false });
    this.props.onChange(data[nextIndex], nextIndex + 1);
    this.onAnimation('animation-left');
  }

  render() {
    const { activeIndex, isNext } = this.state;
    const { data } = this.props;
    const item = data[activeIndex];
    return (
      <div className="div-carousel">
        <button
          type="button"
          onClick={this.previous}
        >
          <i className="fa fa-chevron-left" />
        </button>
        <span id="span-text" className={isNext ? 'animation-right' : 'animation-left'}>
          {' '}
          {item.title}
          {' '}
        </span>
        <button
          type="button"
          onClick={this.next}
        >
          <i className="fa fa-chevron-right" />
        </button>
      </div>

    );
  }
}

CarouselText.defaultProps = {
  defaultValue: {},
};

CarouselText.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  onChange: PropTypes.func.isRequired,
  defaultValue: PropTypes.shape({
    title: PropTypes.string,
  }),
};

export default CarouselText;
