import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import logo from '../../image/icon/logo-loading.gif';

export const MESSAGE_SHOW = {
  data: '',
};
class Loading extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isShow: true,
    };
  }

  static getDerivedStateFromProps = (nextProps, prevState) => {
    const { loading } = nextProps;
    // console.log('loading', loading);
    if (loading !== prevState.isShow) {
      return ({ isShow: loading });
    }
    return null;
  }

  render() {
    const { isShow } = this.state;
    return isShow ? (
      <div className="loading-page">
        <div className="sweet-loading">
          <img loading="lazy" src={logo} alt="logo" style={{ width: '150px' }} />
        </div>
        {
          MESSAGE_SHOW.data && (
            <div className="message-bottom">
              {MESSAGE_SHOW.data}
            </div>
          )
        }

      </div>
    ) : (<div />);
  }
}

Loading.propTypes = {
  loading: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return {
    loading: state.loading,
  };
}

export default connect(mapStateToProps)(Loading);
