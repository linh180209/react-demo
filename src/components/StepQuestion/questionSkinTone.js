import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { isMobile } from '../../DetectScreen/detectIFrame';
import Title from './title';
import icInfo from '../../image/icon/ic-info-skin.svg';
import { getNameFromCommon } from '../../Redux/Helpers';
import icChecked from '../../image/icon/ic-checked-quiz.svg';

class ItemSkinTone extends Component {
  render() {
    const { data, colorActive } = this.props;
    return (
      <div className={`item-skin-tone ${colorActive === data.color ? 'choosed animated faster fadeIn' : ''}`}>
        <div className="circle">
          <button
            data-gtmtracking="funnel-1-step-05-skin-color"
            onClick={() => this.props.onClick(data)}
            type="button"
            style={{ background: data.color }}
          >
            <img data-gtmtracking="funnel-1-step-05-skin-color" src={icChecked} alt="circle" />
          </button>
        </div>
        {/* <span>
          {data.title}
        </span> */}
      </div>
    );
  }
}
class QuestionSkinTone extends Component {
  state = {
    listColor: [
      {
        title: 'Very Fair',
        color: '#fff4f2',
      },
      {
        title: 'Fair',
        color: '#f2d4ca',
      },
      {
        title: 'Olive',
        color: '#e0afa8',
      },
      {
        title: 'Light Brown',
        color: '#d4a093',
      },
      {
        title: 'Brown',
        color: '#c68d82',
      },
      {
        title: 'Dark Brown',
        color: '#a36a5f',
      },
      {
        title: 'Black Brown',
        color: '#905c4f',
      },
      {
        title: 'Darker Brown',
        color: '#653d35',
      },
      {
        title: 'Black',
        color: '#4e2f2a',
      },
      {
        title: 'Dark Black',
        color: '#281b19',
      },
    ],
    colorActive: this.props.defaultValue,
  }

  // componentDidMount() {
  //   const { name, no, id } = this.props;
  //   const { colorActive } = this.state;
  //   this.props.onChange(colorActive, name, no, id);
  // }

  onClick = (color) => {
    this.setState({ colorActive: color });
    const { name, no, id } = this.props;
    this.props.onChange(color, name, no, id);
  }

  focusInput = () => {
    console.log('focusInput');
  }

  cancelFocus = () => {
    console.log('cancelFocus');
  }

  onClickItem = (data) => {
    const { color } = data;
    this.setState({ colorActive: color });
    setTimeout(() => {
      const { name, no, id } = this.props;
      this.props.onChange(color, name, no, id);
    }, 300);
  }

  render() {
    const {
      no, title, cmsCommon, isLandscape,
    } = this.props;
    const { listColor, colorActive } = this.state;
    const note = getNameFromCommon(cmsCommon, 'The_level_of_Melanin');
    return (
      <div id={this.props.id} className="text__container justify-between">
        <Title no={no} title={title} />
        <div
          className="container-skin"
        >
          {/* {
            _.map(listColor, item => (
              item === colorActive
                ? (
                  <button type="button" onClick={() => this.onClick(item)} className="skin skin_active" style={{ backgroundColor: item, '&:hover': { backgroundColor: '#777' } }} />
                )
                : (
                  <button type="button" onClick={() => this.onClick(item)} className="skin" style={{ backgroundColor: item, '&:hover': { backgroundColor: '#777' } }} />
                )
            ))
          } */}
          <div className="list-item-skintone">
            {
              _.map(listColor, item => (
                <ItemSkinTone data={item} onClick={this.onClickItem} colorActive={colorActive} />
              ))
            }
          </div>
        </div>
        <div className={isMobile && !isLandscape ? 'hidden' : 'div-note div-row justify-center items-center'}>
          <img src={icInfo} alt="icInfo" />
          <span>
            {note}
          </span>
        </div>
      </div>
    );
  }
}

QuestionSkinTone.propTypes = {
  no: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  defaultValue: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  cmsCommon: PropTypes.shape().isRequired,
};

export default QuestionSkinTone;
