import React, { Component } from 'react';
import { isMobile } from '../../DetectScreen';
import icChecked from '../../image/icon/ic-checked-quiz.svg';

class ItemChooseQuiz extends Component {
  onClick = () => {
    const { title } = this.props;
    this.props.onChange(title);
  }

  render() {
    const {
      className, url, title, widthIcon, subAnswer, dataGtmtracking, isAddChecked,
    } = this.props;
    return (
      <div
        data-gtmtracking={dataGtmtracking}
        className={`item-choose-quiz ${className} ${className && className.includes('choosed') ? 'animated faster fadeIn' : ''}`}
        onClick={this.onClick}
      >
        {/* <div className="temp-top" /> */}
        <div className="div-image" data-gtmtracking={dataGtmtracking}>
          <img
            data-gtmtracking={dataGtmtracking}
            className="img-icon"
            src={url}
            alt="icon"
            style={widthIcon && isMobile ? { width: widthIcon } : {}}
          />
        </div>
        <span data-gtmtracking={dataGtmtracking}>
          {title}
        </span>
        {subAnswer ? (
          <span className="sub-title" data-gtmtracking={dataGtmtracking}>
            {subAnswer}
          </span>
        ) : (
          <img className="img-checked" src={icChecked} alt="checked" data-gtmtracking={dataGtmtracking} />
        )}
        {isAddChecked && <img className="img-checked" src={icChecked} alt="checked" data-gtmtracking={dataGtmtracking} />}
      </div>
    );
  }
}

export default ItemChooseQuiz;
