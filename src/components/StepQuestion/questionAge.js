import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Title from './title';
import ButtonYesNo from './buttonYesNo';

class QuestionAge extends Component {
  state = {
    value: '',
  }

  onChange = (title) => {
    // const isCheck = title === this.props.answer1;
    if (title === this.props.answer1) {
      this.props.onChange(title, this.props.name, this.props.no - 1);
    } else if (title === this.props.answer2) {
      this.props.onChange(title, this.props.name, this.props.no - 1);
    } else if (title === this.props.answer3) {
      this.props.onChange(title, this.props.name, this.props.no - 1);
    } else if (title === this.props.answer4) {
      this.props.onChange(title, this.props.name, this.props.no - 1);
    }
    this.setState({ value: title });
    this.cancelFocus();
  };

  focusInput = () => {

  }

  cancelFocus = () => {

  }

  render() {
    const {
      id, no, answer1, answer2, answer3, answer4, title,
      type1, type2, type3, type4, url1, url2, url3, url4,
    } = this.props;
    const { value } = this.state;
    return (
      <div id={id} className="text__container padding-top-50">
        <Title no={no} title={title} />
        <div style={{ marginTop: '32px', display: 'flex', flexDirection: 'column' }}>
          <ButtonYesNo
            isCheck={value === answer1}
            title={answer1}
            type={type1}
            onChange={this.onChange}
            url={url1}
            isSizeSmall
          />
          <ButtonYesNo
            className="mt-2"
            isCheck={value === answer2}
            title={answer2}
            type={type2}
            onChange={this.onChange}
            url={url2}
            isSizeSmall
          />
          <ButtonYesNo
            className="mt-2"
            isCheck={value === answer3}
            title={answer3}
            type={type3}
            onChange={this.onChange}
            url={url3}
            isSizeSmall
          />
          <ButtonYesNo
            className="mt-2"
            isCheck={value === answer4}
            title={answer4}
            type={type4}
            onChange={this.onChange}
            url={url4}
            isSizeSmall
          />
        </div>
      </div>
    );
  }
}

QuestionAge.defaultProps = {
  persionalId1: '',
  persionalId2: '',
  url1: undefined,
  url2: undefined,
  url3: undefined,
  url4: undefined,
};

QuestionAge.propTypes = {
  answer1: PropTypes.string.isRequired,
  answer2: PropTypes.string.isRequired,
  answer3: PropTypes.string.isRequired,
  answer4: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  no: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  persionalId1: PropTypes.string,
  persionalId2: PropTypes.string,
  type1: PropTypes.string.isRequired,
  type2: PropTypes.string.isRequired,
  type3: PropTypes.string.isRequired,
  type4: PropTypes.string.isRequired,
  url1: PropTypes.string,
  url2: PropTypes.string,
  url3: PropTypes.string,
  url4: PropTypes.string,
};

export default QuestionAge;
