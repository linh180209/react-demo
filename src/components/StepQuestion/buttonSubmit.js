import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ButtonSubmit extends Component {
  onClick = () => {
    this.props.onSubmit();
  }

  onKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.onClick();
    }
  }

  focusInput = () => {
    document.addEventListener('keydown', this.onKeyPress, true);
  }

  cancelFocus = () => {
    document.removeEventListener('keydown', this.onKeyPress, true);
  }

  render() {
    return (
      <div id={this.props.id} className="mt-5 bt-submit-container">
        <button
          className="bt-submit"
          type="button"
          onClick={this.onClick}
          onKeyPress={this._handleKeyPress}
        >
          Submit
        </button>
        <span className="text__text">
          press
          {' '}
          <strong>
            ENTER
          </strong>
        </span>
      </div>
    );
  }
}

ButtonSubmit.propTypes = {
  id: PropTypes.PropTypes.string.isRequired,
  onSubmit: PropTypes.func.isRequired,
};

export default ButtonSubmit;
