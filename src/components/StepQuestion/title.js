import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Title extends Component {
  render() {
    return (
      <div className="div-col justify-center items-center">
        <span className="text__title--question">
          {this.props.title}
        </span>
      </div>
    );
  }
}

Title.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Title;
