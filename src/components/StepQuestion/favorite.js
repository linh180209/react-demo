/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/sort-comp */
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { GET_ALL_SCENT_FAMILY, GET_ALL_SCENT_FAMILY_V4_PARTNER } from '../../config';
import {
  isMobile,
} from '../../DetectScreen/detectIFrame';
import icChecked from '../../image/icon/ic-checked-quiz.svg';
import icBackQuiz from '../../image/icon/icBackQuiz.svg';
import icNextQuiz from '../../image/icon/icNextQuiz.svg';
import { getNameFromButtonBlock, getNameFromCommon } from '../../Redux/Helpers';
import fetchClient, { fetchClientAlwaysUS } from '../../Redux/Helpers/fetch-client';
import '../../styles/favorite.scss';
import ProgressCircle from '../../views/ResultScentV2/progressCircle';
import ProgressLine from '../../views/ResultScentV2/progressLine';

class Favorite extends Component {
  constructor(props) {
    super(props);
    this.refSilder = React.createRef();
    this.lengthSelected = 0;
    this.englishResults = [];
  }

  state = {
    dataProduct: [],
    currentSlider: 0,
  }

  componentDidMount() {
    this.props.updateDataLike([]);
    this.fetchDataLiked();
  }

  fetchDataLiked = () => {
    const url = this.props.namePath === 'sephora' ? GET_ALL_SCENT_FAMILY_V4_PARTNER.replace('{partner}', 'sephora') : GET_ALL_SCENT_FAMILY;
    const options = {
      url,
      method: 'GET',
    };
    this.props.loadingPage(true);
    const promises = [fetchClient(options), fetchClientAlwaysUS(options)];
    Promise.all(promises).then((result) => {
      console.log(result);
      if (result?.[0] && !result?.[0]?.isError) {
        this.props.loadingPage(false);
        this.setState({ dataProduct: result[0] });
        this.props.updateDataLike(result[0]);
        if (result[1]) {
          this.englishResults = result[1];
        }
        return;
      }
      throw new Error(result?.[0]?.message);
    }).catch((err) => {
      this.props.loadingPage(false);
      console.log('err', err);
    });
  }

  onSelection = (d) => {
    const { updateSelectionLike } = this.props;
    updateSelectionLike(d, this.englishResults);
    setTimeout(() => {
      this.props.onClickNext();
    }, 500);
  }

  componentDidUpdate(prevProps) {
    const { selectLikes } = prevProps;
    if (this.lengthSelected !== selectLikes.length && selectLikes.length > 0) {
      setTimeout(() => {
        if (this.refSilder && this.refSilder.current) {
          this.refSilder.current.slickGoTo(selectLikes.length - 1);
        }
      }, 200);
    }
  }

  scentProfile = (data, buttonBlocks) => {
    const {
      ingredient, profile, name,
    } = data;
    const strengthBt = getNameFromButtonBlock(buttonBlocks, 'STRENGTH');
    const durationBt = getNameFromButtonBlock(buttonBlocks, 'DURATION');
    return (
      <div className="div-scent-profile">
        <h3>
          {name}
        </h3>
        <h4>
          {ingredient && ingredient.title ? ingredient.title : ''}
        </h4>
        <div className="div-progress">
          <div className="div-process-line">
            {
              _.map(profile ? profile.accords : [], x => (
                <ProgressLine data={x} isShowPercent />
              ))
            }
          </div>
          <div className="div-process-circle">
            <ProgressCircle title={strengthBt} percent={profile ? parseInt(parseFloat(profile.strength) * 100, 10) : 0} />
            <ProgressCircle title={durationBt} percent={profile ? parseInt(parseFloat(profile.duration) * 100, 10) : 0} />
          </div>
        </div>
      </div>
    );
  };

  render() {
    const {
      cmsCommon, selectLikes, isLandscape,
    } = this.props;
    const { dataProduct, currentSlider } = this.state;
    const nextBt = getNameFromCommon(cmsCommon, 'NEXT');
    const skipBt = getNameFromCommon(cmsCommon, 'SKIP');
    const favoriteBt = getNameFromCommon(cmsCommon, 'Any favorite perfume family');

    return (
      <div className="div-favorite">
        <div id="id-scroll" className="id-scroll" />
        <div className="div-header div-col justify-center items-center w-100">
          <span className="text__title--question">
            {favoriteBt}
          </span>
        </div>

        {
          isMobile ? (
            <div className="div-ingridients">
              <div className="content">
                {
                  _.map(dataProduct, d => (
                    <div
                      className={`item-favorite ${isMobile && !isLandscape ? '' : 'enable_hover'} animated faster zoomIn ${_.find(selectLikes, x => x.id === d.id) ? 'active' : ''}`}

                    >
                      <div className="div-image" data-gtmtracking="funnel-3-Family">
                        <img
                          loading="lazy"
                          className="ingredient"
                          src={d.image}
                          alt="scent"
                          onClick={() => this.onSelection(d)}
                          data-gtmtracking="funnel-3-Family"
                        />
                        <span data-gtmtracking="funnel-3-Family" onClick={() => this.onSelection(d)}>{d.name}</span>
                        <div data-gtmtracking="funnel-3-Family" className={`div-bg ${_.find(selectLikes, x => x.id === d.id) ? 'show' : ''}`} onClick={() => this.onSelection(d)} />
                      </div>
                      <img className="checked" src={icChecked} alt="checked" onClick={() => this.onSelection(d)} />
                    </div>
                  ))
                }
              </div>
            </div>
          ) : (
            <div className="div-ingridients">
              <div className="content">
                {
                  _.map(dataProduct.slice(0, 4), d => (
                    <div
                      className={`item-favorite ${isMobile && !isLandscape ? '' : 'enable_hover'} animated faster zoomIn ${_.find(selectLikes, x => x.id === d.id) ? 'active' : ''}`}

                    >
                      <div className="div-image" data-gtmtracking="funnel-3-Family">
                        <img
                          loading="lazy"
                          className="ingredient"
                          src={d.image}
                          alt="scent"
                          onClick={() => this.onSelection(d)}
                          data-gtmtracking="funnel-3-Family"
                        />
                        <span data-gtmtracking="funnel-3-Family" onClick={() => this.onSelection(d)}>{d.name}</span>
                        <div data-gtmtracking="funnel-3-Family" className={`div-bg ${_.find(selectLikes, x => x.id === d.id) ? 'show' : ''}`} onClick={() => this.onSelection(d)} />
                      </div>
                      <img className="checked" src={icChecked} alt="checked" onClick={() => this.onSelection(d)} />
                    </div>
                  ))
                }
              </div>
              <div className="content">
                {
                  _.map(dataProduct.slice(4, 7), d => (
                    <div
                      className={`item-favorite ${isMobile && !isLandscape ? '' : 'enable_hover'} animated faster zoomIn ${_.find(selectLikes, x => x.id === d.id) ? 'active' : ''}`}

                    >
                      <div className="div-image">
                        <img
                          loading="lazy"
                          className="ingredient"
                          src={d.image}
                          alt="scent"
                          onClick={() => this.onSelection(d)}
                        />
                        <span onClick={() => this.onSelection(d)}>{d.name}</span>
                        <div className={`div-bg ${_.find(selectLikes, x => x.id === d.id) ? 'show' : ''}`} onClick={() => this.onSelection(d)} />
                      </div>
                      <img className="checked" src={icChecked} alt="checked" onClick={() => this.onSelection(d)} />
                    </div>
                  ))
                }
              </div>
            </div>
          )
        }

        <div className={isMobile && !isLandscape ? 'hidden' : 'div-button div-row justify-center items-center'}>
          <div className="div-bt-next">
            <button
              type="button"
              data-gtmtracking={selectLikes && selectLikes.length > 0 ? 'funnel-1-step-12-favorite-ingredient-next' : 'funnel-1-step-12-favorite-ingredient-skip'}
              className={(dataProduct.length === 0 || (selectLikes && selectLikes.length > 0)) ? 'hidden' : (selectLikes && selectLikes.length > 0 ? '' : 'bt-skip')}
              onClick={this.props.onClickNext}
            >
              {selectLikes && selectLikes.length > 0 ? nextBt : skipBt}
            </button>
          </div>
        </div>
        <div className={isMobile && !isLandscape ? 'div-slider-like' : 'hidden'}>
          <div className={isMobile && !isLandscape && selectLikes && selectLikes.length > 1 ? 'div-bt-quiz' : 'hidden'}>
            {
              currentSlider === 1
                ? (
                  <button type="button" className="button-bg__none" onClick={() => this.refSilder.current.slickPrev()}>
                    <img src={icBackQuiz} alt="back" className="mr-1" />
                    {selectLikes && selectLikes.length > 0 ? selectLikes[0].name : ''}
                  </button>
                ) : (<div />)
            }
            {
              currentSlider === 0 ? (
                <button type="button" className="button-bg__none" onClick={() => this.refSilder.current.slickNext()}>
                  {selectLikes && selectLikes.length > 1 ? selectLikes[1].name : ''}
                  <img src={icNextQuiz} alt="next" className="ml-1" />
                </button>
              ) : (
                <div />
              )
            }
          </div>

        </div>

      </div>
    );
  }
}
Favorite.propTypes = {
  cmsCommon: PropTypes.shape().isRequired,
  updateSelectionLike: PropTypes.func.isRequired,
  onClickNext: PropTypes.func.isRequired,
  selectLikes: PropTypes.arrayOf().isRequired,
};

export default Favorite;
