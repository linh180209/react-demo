import React, { useState, useEffect, useRef } from 'react';
import _ from 'lodash';
import Select from 'react-select';
import { connect } from 'react-redux';
import { isMobile } from '../../DetectScreen';
import Title from './title';
import icCheck from '../../image/icon/checkCountry.png';
import icSearchYellow from '../../image/icon/icSearchYellow.svg';
import { useMergeState } from '../../Utils/customHooks';
import fSingapore from '../../image/icon/flag-singapore.png';
import fAustralia from '../../image/icon/flag-australia.png';
import fChina from '../../image/icon/flag-china.png';
import fFrance from '../../image/icon/flag-france.png';
import fMalaysia from '../../image/icon/flag-malaysia.png';
import fUk from '../../image/icon/flag-uk.png';
import fUs from '../../image/icon/flag-us.png';
import { getNameFromButtonBlock, getNameFromCommon } from '../../Redux/Helpers';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { GET_ALL_COUNTRY_QUIZ } from '../../config';

function ChooseCountry(props) {
  const [state, setState] = useMergeState({
    selectCountry: {},
    isSearchCountry: true,
    isSearch: false,
    dataSearch: [],
  });
  const [countries, setCountries] = useState([]);

  const refInput = useRef(null);
  const listCountry = useRef([
    {
      name: 'Singapore',
      icon: fSingapore,
      code: 'sg',
    },
    {
      name: 'France',
      icon: fFrance,
      code: 'fr',
    },
    {
      name: 'Australia',
      icon: fAustralia,
      code: 'au',
    },
    {
      name: 'Malaysia',
      icon: fMalaysia,
      code: 'my',
    },
    {
      name: 'China',
      icon: fChina,
      code: 'cn',
    },
    {
      name: 'United States',
      icon: fUs,
      code: 'us',
    },
    {
      name: 'United Kingdom',
      icon: fUk,
      code: 'gb',
    },
  ]);

  const fetchCountry = async () => {
    try {
      const option = {
        url: GET_ALL_COUNTRY_QUIZ,
        method: 'GET',
      };
      const result = await fetchClient(option);
      if (result && !result.isError) {
        setCountries(result);
        setState({ dataSearch: result });
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchCountry();
  }, []);

  const onChangeInput = (e) => {
    const { value } = e.target;
    let data = countries;
    if (value) {
      data = _.filter(countries, x => x.name.toLowerCase().includes(value.trim().toLowerCase()));
    }
    setState({ dataSearch: data });
  };

  const onBlur = () => {
    setTimeout(() => {
      setState({ isSearch: false, dataSearch: countries });
    }, 100);
  };

  const onClickCountryDirect = (data) => {
    setState({ selectCountry: data });
    props.onChange(data, props.name, props.no);
  };

  const onClickCountry = (data) => {
    setState({ selectCountry: data });
  };

  const onClickSearch = () => {
    setState({ isSearch: true });
  };

  useEffect(() => {
    if (state.isSearch && refInput) {
      refInput.current.focus();
    }
  }, [state.isSearch]);
  const typeBt = getNameFromButtonBlock(props.buttonBlocks, 'type_in_below');
  const typeYourBt = getNameFromButtonBlock(props.buttonBlocks, 'Type_your_country_here');
  const canFindBt = getNameFromButtonBlock(props.buttonBlocks, 'Cant_find_your_Country');
  const searchBt = getNameFromButtonBlock(props.buttonBlocks, 'search_a_country');
  const nextBt = getNameFromCommon(props.cmsCommon, 'NEXT');
  return (
    <div id={props.id} className={`text__container ${isMobile ? 'padding-top-70' : ''}`}>
      <Title no={props.no} title={props.title} />
      {
        state.isSearchCountry ? (
          <React.Fragment>
            <h4 className="sub-header-country">
              {typeBt}
            </h4>
            <div className="list-country">
              <div className={`div-search ${state.isSearch ? 'result' : ''}`}>
                <div
                  onClick={onClickSearch}
                  className="div-input"
                >
                  <img
                    className={state.isSearch || !_.isEmpty(state.selectCountry) ? 'hidden' : 'icon-search'}
                    src={icSearchYellow}
                    alt="search"
                  />
                  {
                    state.isSearch ? (
                      <input ref={refInput} type="text" placeholder={state.selectCountry.name || typeYourBt} onChange={onChangeInput} onBlur={onBlur} />
                    ) : (
                      <span>
                        {
                          state.selectCountry.name || typeYourBt
                        }
                      </span>
                    )
                  }
                </div>
                <div className={state.isSearch ? 'div-list-search' : 'hidden'}>
                  {
                  _.map(state.dataSearch, x => (
                    <div
                      className="item-perfume"
                      onClick={() => { onClickCountry(x); }}
                    >
                      <span>
                        {x.name}
                      </span>
                    </div>
                  ))
                }
                </div>
              </div>
              {
                !state.isSearch && !_.isEmpty(state.selectCountry) && (
                  <button type="button" className="button-next" onClick={() => onClickCountryDirect(state.selectCountry)}>
                    {nextBt}
                  </button>
                )
              }

            </div>
          </React.Fragment>

        ) : (
          <React.Fragment>
            <div className="div-choose-country">
              <div className="line-country">
                {
                  _.map(listCountry.current.slice(0, 3), d => (
                    <button className="item-country button-bg__none" type="button" onClick={() => onClickCountryDirect(d)}>
                      <img src={d.icon} alt="country" />
                      <span>
                        {d.name}
                      </span>
                      {
                        state.selectCountry.name === d.name && (
                          <div className="div-check">
                            <img src={icCheck} alt="checked" />
                          </div>
                        )
                      }

                    </button>
                  ))
                }
              </div>
              <div className="line-country">
                {
                  _.map(listCountry.current.slice(3, 7), d => (
                    <button className="item-country button-bg__none" type="button" onClick={() => onClickCountryDirect(d)}>
                      <img src={d.icon} alt="country" />
                      <span>
                        {d.name}
                      </span>
                      {
                        state.selectCountry.name === d.name && (
                          <div className="div-check">
                            <img src={icCheck} alt="checked" />
                          </div>
                        )
                      }
                    </button>
                  ))
                }
              </div>
            </div>
            <div className="search-country">
              <span>
                {canFindBt}
              </span>
              <button type="button" onClick={() => setState({ isSearchCountry: true })}>
                {searchBt}
              </button>
            </div>
          </React.Fragment>
        )
      }
    </div>
  );
}

export default ChooseCountry;
