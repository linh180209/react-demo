/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isMobile, isTablet } from '../../DetectScreen/detectIFrame';
import Title from './title';
import ButtonYesNo from './buttonYesNo';
import ItemChooseQuiz from './itemChooseQuiz';
import ic40 from '../../image/icon/ic-4-0.svg';
import ic40Selected from '../../image/icon/ic-4-0-selected.svg';
import ic41 from '../../image/icon/ic-4-1.svg';
import ic41Selected from '../../image/icon/ic-4-1-selected.svg';
import ic50 from '../../image/icon/ic-5-0.svg';
import ic50Selected from '../../image/icon/ic-5-0-selected.svg';
import ic51 from '../../image/icon/ic-5-1.svg';
import ic51Selected from '../../image/icon/ic-5-1-selected.svg';
import ic60 from '../../image/icon/ic-6-0.svg';
import ic60Selected from '../../image/icon/ic-6-0-selected.svg';
import ic61 from '../../image/icon/ic-6-1.svg';
import ic61Selected from '../../image/icon/ic-6-1-selected.svg';
import ic70 from '../../image/icon/ic-7-0.svg';
import ic70Selected from '../../image/icon/ic-7-0-selected.svg';
import ic71 from '../../image/icon/ic-7-1.svg';
import ic71Selected from '../../image/icon/ic-7-1-selected.svg';
import ic80 from '../../image/icon/ic-8-0.svg';
import ic80Selected from '../../image/icon/ic-8-0-selected.svg';
import ic81 from '../../image/icon/ic-8-1.svg';
import ic81Selected from '../../image/icon/ic-8-1-selected.svg';
import ic120 from '../../image/icon/ic-12-0.svg';
import ic120Selected from '../../image/icon/ic-12-0-selected.svg';
import ic121 from '../../image/icon/ic-12-1.svg';
import ic121Selected from '../../image/icon/ic-12-1-selected.svg';
import ic122 from '../../image/icon/ic-12-2.svg';
import ic122Selected from '../../image/icon/ic-12-2-selected.svg';
import ic123 from '../../image/icon/ic-12-3.svg';
import ic123Selected from '../../image/icon/ic-12-3-selected.svg';
import ic130 from '../../image/icon/ic-13-0.svg';
import ic130Selected from '../../image/icon/ic-13-0-selected.svg';
import ic131 from '../../image/icon/ic-13-1.svg';
import ic131Selected from '../../image/icon/ic-13-1-selected.svg';

class QuestionYesNo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.defaultValue,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { defaultValue } = nextProps;
    if (defaultValue !== prevState.defaultValue) {
      return { defaultValue, value: defaultValue, isUpdate: true };
    }
    return null;
  }


  onChange = (title, idAnswer) => {
    if (idAnswer === this.props.idAnswer1) {
      this.props.onChange(title, this.props.name, this.props.no, this.props.persionalId1, this.props.idAnswer1);
    } else if (idAnswer === this.props.idAnswer2) {
      this.props.onChange(title, this.props.name, this.props.no, this.props.persionalId2, this.props.idAnswer2);
    } else if (idAnswer === this.props.idAnswer3) {
      this.props.onChange(title, this.props.name, this.props.no, this.props.persionalId3, this.props.idAnswer3);
    } else if (idAnswer === this.props.idAnswer4) {
      this.props.onChange(title, this.props.name, this.props.no, this.props.persionalId4, this.props.idAnswer4);
    }
    this.setState({ value: idAnswer });
  };

  generateUrl = (index, selected, isMultiAnswer) => {
    const { no, isSubQuiz } = this.props;
    switch (no) {
      case (isSubQuiz ? 1 : 4):
        return selected ? (index === 0 ? ic40Selected : ic41Selected) : (index === 0 ? ic40 : ic41);
      case (isSubQuiz ? 2 : 5):
        return selected ? (index === 0 ? ic50Selected : ic51Selected) : (index === 0 ? ic50 : ic51);
      case (isSubQuiz ? 3 : 6):
        return selected ? (index === 0 ? ic60Selected : ic61Selected) : (index === 0 ? ic60 : ic61);
      case (isSubQuiz ? 4 : 7):
        return selected ? (index === 0 ? ic70Selected : ic71Selected) : (index === 0 ? ic70 : ic71);
      case (isSubQuiz ? 5 : 8):
        return selected ? (index === 0 ? ic80Selected : ic81Selected) : (index === 0 ? ic80 : ic81);
      case (isSubQuiz ? 6 : 11):
        return selected ? (index === 0 ? (isMultiAnswer ? ic120Selected : ic130Selected) : index === 1 ? (isMultiAnswer ? ic121Selected : ic131Selected) : index === 2 ? ic122Selected : ic123Selected) : (index === 0 ? (isMultiAnswer ? ic120 : ic130) : index === 1 ? (isMultiAnswer ? ic121 : ic131) : index === 2 ? ic122 : ic123);
      default:
        break;
    }
  }

  getIconWithLeft = (no) => {
    const { isLandscape, isSubQuiz } = this.props;
    switch (no) {
      case (isSubQuiz ? 1 : 4):
        return isTablet && !isLandscape ? '104px' : isMobile && !isLandscape ? '85px' : '';
      case (isSubQuiz ? 2 : 5):
        return isTablet && !isLandscape ? '136px' : isMobile && !isLandscape ? '110px' : '';
      case (isSubQuiz ? 3 : 6):
        return isTablet && !isLandscape ? '131px' : isMobile && !isLandscape ? '106px' : '';
      case (isSubQuiz ? 4 : 7):
        return isTablet && !isLandscape ? '123px' : isMobile && !isLandscape ? '100px' : '';
      case (isSubQuiz ? 5 : 8):
        return isTablet && !isLandscape ? '147px' : isMobile && !isLandscape ? '121px' : '';
      case (isSubQuiz ? 6 : 11):
        return isTablet && !isLandscape ? '121px' : isMobile && !isLandscape ? '103px' : '';
      default:
        return undefined;
    }
  }

  getIconWithRight = (no) => {
    const { isLandscape, isSubQuiz } = this.props;
    switch (no) {
      case (isSubQuiz ? 1 : 4):
        return isTablet && !isLandscape ? '127px' : isMobile && !isLandscape ? '103px' : '';
      case (isSubQuiz ? 2 : 5):
        return isTablet && !isLandscape ? '117px' : isMobile && !isLandscape ? '95px' : '';
      case (isSubQuiz ? 3 : 6):
        return isTablet && !isLandscape ? '96px' : isMobile && !isLandscape ? '78px' : '';
      case (isSubQuiz ? 4 : 7):
        return isTablet && !isLandscape ? '131px' : isMobile && !isLandscape ? '106px' : '';
      case (isSubQuiz ? 5 : 8):
        return isTablet && !isLandscape ? '137px' : isMobile && !isLandscape ? '111px' : '';
      case (isSubQuiz ? 6 : 11):
        return isTablet && !isLandscape ? '125px' : isMobile && !isLandscape ? '103px' : '';
      default:
        return undefined;
    }
  }

  generateDataGTM = (no) => {
    const { isSubQuiz } = this.props;
    switch (no) {
      case (isSubQuiz ? 1 : 4):
        return 'funnel-1-step-06-secure-or-adventurous';
      case (isSubQuiz ? 2 : 5):
        return 'funnel-1-step-07-care-or-lead';
      case (isSubQuiz ? 3 : 6):
        return 'funnel-1-step-08-tradition-novelty';
      case (isSubQuiz ? 4 : 7):
        return 'funnel-1-step-09-fun-success';
      case (isSubQuiz ? 5 : 8):
        return 'funnel-1-step-10-organize-surpise';
      case (isSubQuiz ? 6 : 11):
        return '';
      case (13):
        return 'funnel-2-occasion';
      default:
        return undefined;
    }
  }

  render() {
    const { value } = this.state;
    const {
      no, answer1, answer2, answer3, answer4, subAnswer1, subAnswer2, subAnswer3, subAnswer4, title, idAnswer1, idAnswer2, idAnswer3, idAnswer4,
    } = this.props;
    return (
      <div id={this.props.id} className={`text__container ${isMobile ? 'padding-top-30' : ''}`}>
        <Title no={no} title={title} />
        {
          answer4 ? (
            <div className="scroll-able">
              <div className="content-multile">
                <div className="div-content-body">
                  <ItemChooseQuiz
                    dataGtmtracking={this.generateDataGTM(no)}
                    widthIcon={this.getIconWithLeft(no)}
                    className={`${value === idAnswer1 ? 'choosed' : ''} ${isMobile ? 'w-50' : ''} border-right`}
                    url={this.generateUrl(0, value === idAnswer1, true)}
                    title={answer1}
                    onChange={() => this.onChange(answer1, idAnswer1)}
                    subAnswer={subAnswer1}
                    isAddChecked
                  />
                  <ItemChooseQuiz
                    dataGtmtracking={this.generateDataGTM(no)}
                    widthIcon={this.getIconWithRight(no)}
                    className={`${value === idAnswer2 ? 'choosed' : ''} ${isMobile ? 'w-50' : ''}`}
                    url={this.generateUrl(1, value === idAnswer2, true)}
                    title={answer2}
                    onChange={() => this.onChange(answer2, idAnswer2)}
                    subAnswer={subAnswer2}
                    isAddChecked
                  />
                </div>
                <div className="div-content-body">
                  <ItemChooseQuiz
                    dataGtmtracking={this.generateDataGTM(no)}
                    widthIcon={this.getIconWithLeft(no)}
                    className={`${value === idAnswer3 ? 'choosed' : ''} ${isMobile ? 'w-50' : ''} border-right`}
                    url={this.generateUrl(2, value === idAnswer3, true)}
                    title={answer3}
                    onChange={() => this.onChange(answer3, idAnswer3)}
                    subAnswer={subAnswer3}
                    isAddChecked
                  />
                  <ItemChooseQuiz
                    dataGtmtracking={this.generateDataGTM(no)}
                    widthIcon={this.getIconWithRight(no)}
                    className={`${value === idAnswer4 ? 'choosed' : ''} ${isMobile ? 'w-50' : ''}`}
                    url={this.generateUrl(3, value === idAnswer4, true)}
                    title={answer4}
                    onChange={() => this.onChange(answer4, idAnswer4)}
                    subAnswer={subAnswer4}
                    isAddChecked
                  />
                </div>
              </div>
            </div>
          ) : (
            <div className="div-content-body">
              <ItemChooseQuiz
                dataGtmtracking={this.generateDataGTM(no)}
                widthIcon={this.getIconWithLeft(no)}
                className={`${value === idAnswer1 ? 'choosed' : ''} ${isMobile ? 'w-50' : ''} border-right`}
                url={this.generateUrl(0, value === idAnswer1)}
                title={answer1}
                onChange={() => this.onChange(answer1, idAnswer1)}
                subAnswer={subAnswer1}
              />
              <ItemChooseQuiz
                dataGtmtracking={this.generateDataGTM(no)}
                widthIcon={this.getIconWithRight(no)}
                className={`${value === idAnswer2 ? 'choosed' : ''} ${isMobile ? 'w-50' : ''}`}
                url={this.generateUrl(1, value === idAnswer2)}
                title={answer2}
                onChange={() => this.onChange(answer2, idAnswer2)}
                subAnswer={subAnswer2}
              />
            </div>
          )
        }
      </div>
    );
  }
}

QuestionYesNo.defaultProps = {
  url1: '',
  url2: '',
  persionalId1: '',
  persionalId2: '',
  idAnswer1: undefined,
  idAnswer2: undefined,
};

QuestionYesNo.propTypes = {
  no: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  type1: PropTypes.string.isRequired,
  type2: PropTypes.string.isRequired,
  answer1: PropTypes.string.isRequired,
  answer2: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  url1: PropTypes.string,
  url2: PropTypes.string,
  persionalId1: PropTypes.string,
  persionalId2: PropTypes.string,
  idAnswer1: PropTypes.number,
  idAnswer2: PropTypes.number,
  defaultValue: PropTypes.string.isRequired,
};

export default QuestionYesNo;
