import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isIOS } from 'react-device-detect';
import Title from './title';
import checked from '../../image/checked.svg';
import { isMobile } from '../../DetectScreen';

class QuestionText extends Component {
  constructor(props) {
    super(props);
    this.refInput = React.createRef();
  }

  onSubmit = () => {
    if (this.text) {
      this.props.onChange(this.text, this.props.name, this.props.no - 1);
    }
  }

  onChangeText = (e) => {
    const { value } = e.target;
    this.text = value;
  }

  focusInput = () => {
    this.refInput.current.focus();
    this.refInput.current.value = '';
  }

  cancelFocus = () => {
    this.refInput.current.blur();
  }

  _handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.onSubmit();
    }
  }


  render() {
    const {
      title, no, typeInput, addMarginBottom,
    } = this.props;
    return (
      <div id={this.props.id} className="text__container">
        <div style={isMobile && isIOS && addMarginBottom ? { marginBottom: '30vh' } : {}}>
          <Title no={no} title={title} />
          <div className="text__input--div">
            <input
              ref={this.refInput}
              onKeyPress={this._handleKeyPress}
              className="text__input--text"
              type={typeInput}
              onChange={this.onChangeText}
              placeholder="Type your answer here ..."
            />
          </div>
          <div className="text__button--div-root">
            <div className="text__button--div">
              <button
                className="text__button--bt"
                type="button"
                onClick={this.onSubmit}
              >
                <div>
                  <span>
                    OK
                  </span>
                  <img className="text__button--check" src={checked} alt="check" />
                </div>
              </button>
            </div>
            {/* <span className="text__text">
                press
              <strong>
                {' '}
                  ENTER
              </strong>
            </span> */}
          </div>
        </div>
      </div>
    );
  }
}
QuestionText.defaultProps = {
  type: 'qtText',
  typeInput: 'text',
  addMarginBottom: false,
};

QuestionText.propTypes = {
  no: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  type: PropTypes.string,
  typeInput: PropTypes.string,
  addMarginBottom: PropTypes.bool,
  defaultValue: PropTypes.string.isRequired,
};

export default QuestionText;
