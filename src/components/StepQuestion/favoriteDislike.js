/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import {
  isMobile, isBrowser, isTablet,
} from '../../DetectScreen/detectIFrame';
// import '../../styles/favorite.scss';
import {
  getNameFromCommon, getNameFromButtonBlock, generateUrlWeb, gotoShopHome,
} from '../../Redux/Helpers';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { DISLIKES_OUTCOME_URL, DISLIKES_PARTNERSHIP_OUTCOME_URL, LIKED_OUTCOME_URL } from '../../config';
import icInfo from '../../image/icon/icNewInfo.svg';
import icChecked from '../../image/icon/ic-checked-quiz.svg';
import ProgressLine from '../../views/ResultScentV2/progressLine';
import ProgressCircle from '../../views/ResultScentV2/progressCircle';
import icNone from '../../image/icon/ic-none-like.svg';
import icBackQuiz from '../../image/icon/icBackQuiz.svg';
import icNextQuiz from '../../image/icon/icNextQuiz.svg';

class FavoriteDislike extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      currentSlider: 0,
    };
    this.refSilder = React.createRef();
    this.lengthSelected = 0;
  }

  componentDidMount() {
    this.fetchDataWithLikeAPI();
    // if (this.props.namePath === 'quiz') {
    //   this.fetchDataWithLikeAPI();
    // } else {
    //   this.fetchData();
    // }
  }

  fetchDataWithLikeAPI = async () => {
    this.props.updateDataDisLike(undefined);
    const { idOutCome } = this.props;
    const options = {
      url: LIKED_OUTCOME_URL.replace('{id}', idOutCome),
      method: 'GET',
    };
    await fetchClient(options);
    this.fetchData();
  }

  fetchData = () => {
    const {
      selectLikes, idOutCome, history,
    } = this.props;
    if (!idOutCome) {
      // history.push(generateUrlWeb('/'));
      gotoShopHome();
    }
    const URL = DISLIKES_OUTCOME_URL.replace('{id}', idOutCome);
    const option = {
      url: selectLikes.length === 0 ? URL : `${URL}?liked=${selectLikes[0].id}${selectLikes.length > 1 ? `,${selectLikes[1].id}` : ''}`,
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.setState({ data: result });
        this.props.updateDataDisLike(result);
        setTimeout(() => {
          const ele = document.getElementById('id-scroll');
          if (ele) {
            ele.scrollIntoView({ behavior: 'smooth' });
          }
        }, 500);
        return;
      }
      throw new Error();
    }).catch((err) => {
      console.error('error', err);
    });
  }

  onSelection = (d) => {
    const { selectDislikes, updateSelectionDislike } = this.props;
    const index = selectDislikes.length;
    updateSelectionDislike(d, index);
  }

  componentDidUpdate(prevProps) {
    const { selectLikes } = prevProps;
    if (this.lengthSelected !== selectLikes.length && selectLikes.length > 0) {
      setTimeout(() => {
        if (this.refSilder && this.refSilder.current) {
          this.refSilder.current.slickGoTo(selectLikes.length - 1);
        }
      }, 200);
    }
  }

  scentProfile = (data) => {
    const { ingredient, profile, name } = data;
    const strengthBt = 'STRENGTH';
    const durationBt = 'DURATION';
    return (
      <div className="div-scent-profile">
        <h3>
          {name}
        </h3>
        <h4>
          {ingredient && ingredient.title ? ingredient.title : ''}
        </h4>
        <div className="div-progress">
          <div className="div-process-line">
            {
              _.map(profile ? profile.accords : [], x => (
                <ProgressLine data={x} isShowPercent />
              ))
            }
          </div>
          <div className="div-process-circle">
            <ProgressCircle title={strengthBt} percent={profile ? parseInt(parseFloat(profile.strength) * 100, 10) : 0} />
            <ProgressCircle title={durationBt} percent={profile ? parseInt(parseFloat(profile.duration) * 100, 10) : 0} />
          </div>
        </div>
      </div>
    );
  };

  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      pauseOnHover: false,
      beforeChange: (current, next) => {
        this.setState({ currentSlider: next });
      },
    };
    const {
      cmsCommon, cmsPersonal, selectDislikes, getImage,
      onClickNext, buttonBlocks, isLandscape,
    } = this.props;
    const { data, currentSlider } = this.state;
    const nextBt = getNameFromCommon(cmsCommon, 'NEXT');
    const skipBt = getNameFromCommon(cmsCommon, 'SKIP');
    const selectBt = getNameFromButtonBlock(buttonBlocks, 'Select_ingridients_to_view_their_characters_here');

    const textBlock = _.filter(cmsPersonal.body, x => x.type === 'text_block');

    return (
      <div className="div-favorite">
        <div id="id-scroll" className="id-scroll" />
        <div className="div-header div-col justify-center items-center">
          <span className={isMobile && !isLandscape ? 'text-header tc' : 'text-header mt-3 tc'}>
            {textBlock && textBlock.length > 4 ? textBlock[4].value : ''}
          </span>
          <span className={isMobile && !isLandscape ? 'description mb-2 tc' : 'description mb-5 tc'}>
            {textBlock && textBlock.length > 5 ? textBlock[5].value : ''}
          </span>
        </div>

        <div className="div-ingridients">
          <div className="content">
            {
              _.map(data, d => (
                <div
                  className={`item-favorite ${isMobile && !isLandscape ? '' : 'enable_hover'} animated faster zoomIn ${_.find(selectDislikes, x => x.id === d.id) ? 'active' : ''}`}

                >
                  <div className="div-image">
                    <img
                      loading="lazy"
                      className="ingredient"
                      src={getImage('ingredient', d)}
                      alt="scent"
                      onClick={() => this.onSelection(d)}
                    />
                    <span>{d.ingredient && (isBrowser || (isTablet && isLandscape)) ? d.ingredient.title : ''}</span>
                    <div className={`div-bg ${_.find(selectDislikes, x => x.id === d.id) ? 'show' : ''}`} onClick={() => this.onSelection(d)} />
                    <button type="button" className="bt-info" onClick={() => this.props.onClickIngredient(d.ingredient)}>
                      <img src={icInfo} alt="icInfo" />
                    </button>
                  </div>
                  <img className="checked" src={icChecked} alt="checked" onClick={() => this.onSelection(d)} />
                </div>
              ))
            }
          </div>
        </div>
        <div className={isMobile && !isLandscape ? 'hidden' : 'div-button div-row justify-center items-center'}>
          <div className="div-bt-next">
            <button
              type="button"
              data-gtmtracking={selectDislikes && selectDislikes.length > 0 ? 'funnel-1-step-13-dislike-ingredient-next' : 'funnel-1-step-13-disliked-ingredient-skip'}
              className={data.length === 0 ? 'hidden' : (selectDislikes && selectDislikes.length > 0 ? '' : 'bt-skip')}
              onClick={this.props.onClickNext}
            >
              {selectDislikes && selectDislikes.length > 0 ? nextBt : skipBt}
            </button>
          </div>
        </div>
        <div className={isMobile && !isLandscape ? 'div-slider-like' : 'hidden'}>
          {
            !selectDislikes || selectDislikes.length === 0 ? (
              <div className="div-none-like">
                <img src={icNone} alt="icon" />
                <span>
                  {selectBt}
                </span>
              </div>
            ) : (
              <Slider
                ref={this.refSilder}
                {... settings}
              >
                {
              _.map(selectDislikes || [], d => (
                this.scentProfile(d)
              ))
            }
              </Slider>
            )
          }
          <div className={isMobile && !isLandscape && selectDislikes && selectDislikes.length > 1 ? 'div-bt-quiz' : 'hidden'}>
            {
              currentSlider === 1
                ? (
                  <button type="button" className="button-bg__none" onClick={() => this.refSilder.current.slickPrev()}>
                    <img src={icBackQuiz} alt="back" className="mr-1" />
                    {selectDislikes && selectDislikes.length > 0 ? selectDislikes[0].name : ''}
                  </button>
                ) : (<div />)
            }
            {
              currentSlider === 0 ? (
                <button type="button" className="button-bg__none" onClick={() => this.refSilder.current.slickNext()}>
                  {selectDislikes && selectDislikes.length > 1 ? selectDislikes[1].name : ''}
                  <img src={icNextQuiz} alt="next" className="ml-1" />
                </button>
              ) : (
                <div />
              )
            }
          </div>
        </div>
      </div>
    );
  }
}
FavoriteDislike.propTypes = {
  cmsCommon: PropTypes.shape().isRequired,
  cmsPersonal: PropTypes.shape().isRequired,
  persionaltily: PropTypes.shape().isRequired,
  updateSelectionDislike: PropTypes.func.isRequired,
  selectDislikes: PropTypes.arrayOf().isRequired,
  getImage: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  selectLikes: PropTypes.arrayOf().isRequired,
  no: PropTypes.number.isRequired,
  idOutCome: PropTypes.number.isRequired,
  onClickNext: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  history: PropTypes.func.isRequired,
  slugPartnerShip: PropTypes.string.isRequired,
};

export default FavoriteDislike;
