import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isMobile, isTablet } from '../../DetectScreen/detectIFrame';
import Title from './title';
import ButtonYesNo from './buttonYesNo';
import ItemChooseQuiz from './itemChooseQuiz';
import icMale from '../../image/icon/ic-male.svg';
import icMaleChoosed from '../../image/icon/ic-male-selected.svg';
import icFemale from '../../image/icon/ic-female.svg';
import icFemaleChoosed from '../../image/icon/ic-female-selected.svg';
import icUnsex from '../../image/icon/ic-unsex.svg';
import icUnsexChoosed from '../../image/icon/ic-unsex-selected.svg';

class QuestionGender extends Component {
  state = {
    value: this.props.defaultValue,
  }

  onChange = (title) => {
    // const isCheck = title === this.props.answer1;

    if (title === this.props.answer1) {
      this.props.onChange(title, this.props.name, this.props.no, this.props.persionalId1, this.props.idAnswer1);
    } else if (title === this.props.answer2) {
      this.props.onChange(title, this.props.name, this.props.no, this.props.persionalId2, this.props.idAnswer2);
    } else if (title === this.props.answer3) {
      this.props.onChange(title, this.props.name, this.props.no, this.props.persionalId3, this.props.idAnswer3);
    }
    this.setState({ value: title });
    this.cancelFocus();
  };

  focusInput = () => {
    this.setState({ value: '' });
  }

  cancelFocus = () => {
    console.log('cancelFocus');
  }

  render() {
    const { value } = this.state;
    const {
      no, answer1, answer2, answer3, title, isButtonYesNo,
    } = this.props;
    return (
      <div id={this.props.id} className={`text__container ${isMobile ? 'padding-top-30' : ''}`}>
        <Title no={no} title={title} />
        {
          isButtonYesNo ? (
            <div className="div-content-gender div-col items-center justify-center">
              <ButtonYesNo
                isCheck={value === answer1}
                title={answer1}
                onChange={this.onChange}
              />
              <ButtonYesNo
                className={isMobile ? 'mt-1' : 'mt-2'}
                isCheck={value === answer2}
                title={answer2}
                onChange={this.onChange}
              />
              <ButtonYesNo
                className={isMobile ? 'mt-1' : 'mt-2'}
                isCheck={value === answer3}
                title={answer3}
                onChange={this.onChange}
              />

            </div>
          ) : (
            <div className="div-content-gender div-row items-center justify-center">
              <ItemChooseQuiz
                dataGtmtracking="funnel-1-step-03-gender"
                url={value === answer1 ? icMaleChoosed : icMale}
                title={answer1}
                onChange={this.onChange}
                className={value === answer1 ? 'choosed' : ''}
                widthIcon={isTablet ? '126px' : isMobile ? '72px' : '141px'}
              />
              <ItemChooseQuiz
                dataGtmtracking="funnel-1-step-03-gender"
                url={value === answer2 ? icFemaleChoosed : icFemale}
                title={answer2}
                onChange={this.onChange}
                className={`border-left border-right ${value === answer2 ? 'choosed' : ''}`}
                widthIcon={isTablet ? '86px' : isMobile ? '49px' : '96px'}
              />
              <ItemChooseQuiz
                dataGtmtracking="funnel-1-step-03-gender"
                url={value === answer3 ? icUnsexChoosed : icUnsex}
                widthIcon={isTablet ? '106px' : isMobile ? '58px' : '102px'}
                title={answer3}
                onChange={this.onChange}
                className={value === answer3 ? 'choosed' : ''}
              />
            </div>
          )
        }
      </div>
    );
  }
}

QuestionGender.defaultProps = {
  url1: '',
  url2: '',
  persionalId1: '',
  persionalId2: '',
  idAnswer1: undefined,
  idAnswer2: undefined,
};

QuestionGender.propTypes = {
  no: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  type1: PropTypes.string.isRequired,
  type2: PropTypes.string.isRequired,
  type3: PropTypes.string.isRequired,
  answer1: PropTypes.string.isRequired,
  answer2: PropTypes.string.isRequired,
  answer3: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  url1: PropTypes.string,
  url2: PropTypes.string,
  url3: PropTypes.string,
  persionalId1: PropTypes.string,
  persionalId2: PropTypes.string,
  persionalId3: PropTypes.string,
  idAnswer1: PropTypes.number,
  idAnswer2: PropTypes.number,
  idAnswer3: PropTypes.number,
  defaultValue: PropTypes.string,
};

export default QuestionGender;
