import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ButtonYesNo extends Component {
  onChange = () => {
    const { title } = this.props;
    this.props.onChange(title);
  }

  render() {
    const {
      isCheck, title,
    } = this.props;
    return (
      <button
        onClick={this.onChange}
        className={isCheck ? 'bt-yes-no active' : 'bt-yes-no'}
        type="button"
      >
        <div className="div-content">
          {/* <div className="circle" /> */}
          <span>
            {title}
          </span>
        </div>
      </button>
    // <div
    //   className={nameClass}
    //   onClick={this.onChange}
    //   style={{ cursor: 'pointer' }}
    // >
    //   <img
    //     style={{
    //       width: '100%',
    //       height: '100%',
    //       position: 'absolute',
    //       zIndex: '-1',
    //     }}
    //     src={bg10}
    //     alt="bg10"
    //   />
    //   <div
    //     className="container_absolute"
    //   >
    //     <div className="container_text">
    //       <span className="question__container--Yes" style={{ width: `${width}px` }}>
    //         {title}
    //       </span>
    //     </div>
    //   </div>
    // </div>
    );
  }
}

ButtonYesNo.defaultProps = {
  className: '',
  isBackgroundVideo: false,
  isSizeSmall: false,
};

ButtonYesNo.propTypes = {
  isCheck: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  className: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  width: PropTypes.number.isRequired,
  url: PropTypes.string.isRequired,
  isBackgroundVideo: PropTypes.bool,
  isSizeSmall: PropTypes.bool,
};

export default ButtonYesNo;
