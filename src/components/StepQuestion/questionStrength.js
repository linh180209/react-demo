/* eslint-disable react/jsx-no-comment-textnodes */
import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import Title from './title';
import icStrength from '../../image/icon/ic-strength.png';

function QuestionStrength(props) {
  const [valueChose, setValue] = useState();

  const onChange = (value) => {
    setValue(value);
    setTimeout(() => {
      props.onChange(value, props.name, props.no, props.id, value);
    }, 500);
  };

  return (
    <div id={props.id} className="text__container padding-top-50">
      <Title no={props.no} title={props.title} />
      <div className="question-strength">
        <div className="wrap-strength">
          <div className={classnames('bg-color', valueChose === 0 ? 'level-1' : valueChose === 1 ? 'level-2' : valueChose === 2 ? 'level-3' : '')} />
          <img src={icStrength} alt="strength" />
          <div className="user-touch">
            <div data-gtmtracking="funnel-1-power-of-perfume" className="touch-1" onClick={() => onChange(0)} />
            <div data-gtmtracking="funnel-1-power-of-perfume" className="touch-2" onClick={() => onChange(1)} />
            <div data-gtmtracking="funnel-1-power-of-perfume" className="touch-3" onClick={() => onChange(2)} />
          </div>
        </div>
      </div>
    </div>

  );
}

export default QuestionStrength;
