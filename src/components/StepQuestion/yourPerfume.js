/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import '../../styles/your_perfume.scss';
import icClose from '../../image/icon/ic-close-region.svg';
import icSearch from '../../image/icon/ic-search.svg';
import icLoadingBlack from '../../image/icon/loading-black.svg';
import { getNameFromCommon, getAltImage } from '../../Redux/Helpers';
import { GET_YOUR_PERFUME_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';

class YourPerfume extends Component {
  constructor(props) {
    super(props);
    this.state = {
      idChoise: undefined,
      listData: [],
      isForcus: false,
      isLoading: false,
    };
    this.search = '';
    this.offset = 1;
    this.searchFnc('a');
  }

  onClickDiscover = () => {
    this.props.onClickNext();
  }

  onChange = (e) => {
    const { value } = e.target;
    this.search = value;
    if (_.isEmpty(value)) {
      this.setState({ listData: [] });
      this.searchFnc('a');
    } else {
      this.searchFnc(value);
    }
  }

  searchFnc = (value) => {
    if (this.timeOutSearching) {
      clearTimeout(this.timeOutSearching);
      this.timeOutSearching = undefined;
    }
    this.timeOutSearching = setTimeout(() => { this.fetchData(value); }, 500);
    this.setState({ isLoading: true });
  }

  fetchData = (value, offset = 1) => {
    if (value === 'a' && this.dataA) {
      this.setState({ listData: this.dataA, isLoading: false });
      return;
    }
    const option = {
      url: GET_YOUR_PERFUME_URL.replace('{search}', value).replace('{offset}', offset),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        const dataSearch = _.orderBy(result, 'name', 'asc');
        this.setState({ listData: dataSearch, isLoading: false });
        if (value === 'a') {
          this.dataA = dataSearch;
        }
        // this.offset = this.offset + result.length;
      }
    });
  }

  onFocus = () => {
    if (_.isEmpty(this.search)) {
      this.searchFnc('a');
    }
    this.setState({ isForcus: true });
  }

  onBlur = () => {
    setTimeout(() => {
      this.setState({ isForcus: false });
    }, 300);
  }

  render() {
    const {
      idChoise, listData, isForcus, isLoading,
    } = this.state;
    const { cmsCommon, cmsPersonal, onChangeYourPefume } = this.props;
    const orBt = getNameFromCommon(cmsCommon, 'OR');
    const discoverBt = getNameFromCommon(cmsCommon, 'DISCOVER_ADVENTUROUS_BLENDS');
    const showmeBt = getNameFromCommon(cmsCommon, 'SHOW_ME_ELEVATED_SCENTS');
    const skipBt = getNameFromCommon(cmsCommon, 'SKIP');
    const nextBt = getNameFromCommon(cmsCommon, 'NEXT');
    const textBlock = _.filter(cmsPersonal.body, x => x.type === 'text_block');
    const isSearch = listData && listData.length > 0 && isForcus;
    return (
      <div className="div-your-perfume">
        <h2>
          {textBlock && textBlock.length > 6 ? textBlock[6].value : ''}
        </h2>
        <span className="subtitle">
          {textBlock && textBlock.length > 7 ? textBlock[7].value : ''}
        </span>
        <div className="div-content">
          <div className="div-search">
            {
            idChoise ? (
              <div className="div-result">
                <div className="div-name">
                  <span>
                    {textBlock && textBlock.length > 8 ? textBlock[8].value : ''}
                  </span>
                  <div
                    className="item-perfume"
                  >
                    <img loading="lazy" src={idChoise.image} alt={getAltImage(idChoise.image)} />
                    <span>
                      {idChoise.name}
                    </span>
                  </div>
                </div>
                <button
                  type="button"
                  className="button-bg__none"

                  onClick={() => { this.setState({ idChoise: undefined }); onChangeYourPefume(undefined); }
                  }
                >
                  <img src={icClose} alt="ic-close" />
                </button>
              </div>
            ) : (
              <div className="div-input-search">
                <input
                  type="text"
                  defaultValue={this.search}
                  placeholder={textBlock && textBlock.length > 8 ? textBlock[8].value : ''}
                  onChange={this.onChange}
                  onFocus={this.onFocus}
                  onBlur={this.onBlur}
                />
                <button
                  type="button"
                  className="button-bg__none"
                >
                  <img src={isLoading ? icLoadingBlack : icSearch} alt={icSearch} className={isLoading ? 'animate-loading ' : ''} />
                </button>
              </div>
            )
          }
            <div className={`${idChoise || !isSearch ? 'hidden' : 'div-list-perfume'}`}>
              {
                _.map(listData, x => (
                  <div
                    className="item-perfume"
                    onClick={() => {
                      onChangeYourPefume(x.id);
                      this.setState({ idChoise: x });
                    }}
                  >
                    <img loading="lazy" src={x.image} alt={getAltImage(x.image)} />
                    <span>
                      {x.name}
                    </span>
                  </div>
                ))
              }
            </div>
          </div>
          {
          idChoise || !_.isEmpty(this.search) ? (
            <div className="div-button">
              {/* <button
                type="button"
                className="bt-1"
                onClick={this.onClickDiscover}
              >
                {nextBt}
              </button>
              <div className="line-or">
                <div className="line" />
                <span>
                  {orBt}
                </span>
                <div className="line" />
              </div> */}
              <button
                type="button"
                className="bt-1"
                data-gtmtracking="funnel-1-step-11-favorite-perfume-next"
                onClick={this.onClickDiscover}
              >
                {nextBt}
              </button>
            </div>
          ) : (
            <div className="div-bt-next">
              <button
                data-gtmtracking="funnel-1-step-11-favorite-perfume-skip"
                type="button"
                onClick={this.onClickDiscover}
              >
                {skipBt}
              </button>
            </div>
          )
        }
        </div>

      </div>
    );
  }
}

YourPerfume.propTypes = {
  onClickNext: PropTypes.func.isRequired,
  onChangeYourPefume: PropTypes.func.isRequired,
  cmsCommon: PropTypes.shape().isRequired,
  cmsPersonal: PropTypes.shape().isRequired,
  no: PropTypes.number.isRequired,
};

export default YourPerfume;
