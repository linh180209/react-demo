import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';
import { isBrowser, isLargerScreen } from '../../DetectScreen';

class TextBlock extends Component {
  render() {
    const { headerText, styleHeader, blockTexts } = this.props;
    const styleHD = {
      fontSize: '2rem',
      textAlign: 'center',
    };
    _.assign(styleHD, styleHeader);
    return (
      <div style={isBrowser ? {
        marginLeft: '10%', marginRight: '10%', minHeight: '100vh', height: 'fit-content',
      } : {
        minHeight: '100vh', height: 'fit-content',
      }}
      >
        <div style={isBrowser ? { marginLeft: '10%', marginRight: '10%' } : { marginLeft: '30px', marginRight: '30px' }}>
          <div className="w-100 div-col justify-center items-center">
            <h1
              className="w-100 mt-5 mb-5"
              style={styleHD}
            >
              {headerText}

            </h1>
          </div>
          <div className="mt-3" />
          {
            blockTexts && blockTexts.length > 0
              ? (
                <div>
                  {ReactHtmlParser(blockTexts[0].value)}
                </div>
              ) : (<div />)
          }
        </div>
      </div>
    );
  }
}
TextBlock.propTypes = {
  headerText: PropTypes.string.isRequired,
  styleHeader: PropTypes.objectOf(PropTypes.object).isRequired,
  blockTexts: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default TextBlock;
