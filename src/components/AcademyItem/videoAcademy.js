import React, { Component } from 'react';
import PropTypes from 'prop-types';
import YouTube from 'react-youtube';
import {
  Player, ControlBar, Shortcut, BigPlayButton,
} from 'video-react';
import '../../../node_modules/video-react/dist/video-react.css';
import { addClassIntoElemenetID, removeClassIntoElemenetID } from '../../Redux/Helpers';

class VideoAcademy extends Component {
  constructor(props) {
    super(props);
    this.addedClass = false;
    this.state = {
      widthVideo: 640,
      heightVideo: 360,
    };
    this.eventVideo = null;
  }

  componentDidMount() {
    if (this.props.isChangeFullScreen) {
      this.handleFullScreenForMobileiOS();
    }
    const ele = document.getElementById('id-video-frame');
    if (ele) {
      this.setState({
        widthVideo: ele.offsetWidth,
        heightVideo: ele.offsetHeight,
      });
    }
  }

  handleFullScreenForMobileiOS = () => {
    this.player.subscribeToStateChange((current) => {
      if (current?.isFullscreen && !this.addedClass) {
        this.addedClass = true;
        addClassIntoElemenetID('id-info-scent', 'full-screen-scent');
        addClassIntoElemenetID('id-add-card-block', 'hidden');
      } else if (!current.isFullscreen && this.addedClass) {
        this.addedClass = false;
        removeClassIntoElemenetID('id-info-scent', 'full-screen-scent');
        removeClassIntoElemenetID('id-add-card-block', 'hidden');
      }
    });
  }

  onMouseEnter = () => {
    if (this.props.url.includes('youtube')) {
      this.eventVideo.target.playVideo();
      return;
    }
    if (this.player && !this.player.hasStarted) {
      this.player.play();
    }
  }

  _onReady = (event) => {
    this.eventVideo = event;
  };

  getIdYoutube = (url) => {
    if (!url) {
      return url;
    }
    const arr = url.split('v=');
    if (arr.length > 1) {
      return arr[1];
    }
    return url;
  }

  render() {
    const {
      url, poster, autoPlay, isControl,
    } = this.props;
    const opts = {
      width: this.state.widthVideo,
      height: this.state.heightVideo,
      playerVars: {
        autoplay: 0,
      },
    };
    return (
      <div
        onMouseEnter={this.onMouseEnter}
        style={{
          width: '100%', height: '100%', objectFit: 'cover',
        }}
        id="id-video-frame"
      >
        {
          url?.includes('youtube') ? (
            <YouTube videoId={this.getIdYoutube(url)} opts={opts} onReady={this._onReady} />
          ) : (
            <Player
              playsInline
              loop
              muted
              autobuffer
              autoPlay={autoPlay}
              disablePauseOnClick
              src={url}
              poster={poster}
              ref={(player) => {
                this.player = player;
              }}
            >
              {
                isControl ? (
                  <ControlBar />
                ) : (
                  <ControlBar disableCompletely disableDefaultControls />
                )
              }
              {/* <Shortcut dblclickable clickable={false} /> */}
              <BigPlayButton position="center" />
            </Player>
          )
        }
      </div>
    );
  }
}
VideoAcademy.propTypes = {
  url: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default VideoAcademy;
