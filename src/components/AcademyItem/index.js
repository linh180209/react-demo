import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';
import VideoAcademy from './videoAcademy';
import { generateUrlWeb, getAltImageV2 } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

const gotoAnchor = (id) => {
  const ele = document.getElementById(id);
  if (ele) {
    ele.scrollIntoView({ behavior: 'smooth' });
  }
};

class AcademyItem extends Component {
  state={
    isShow: false,
  }

  onToggle = () => {
    const { isShow } = this.state;
    this.setState({ isShow: !isShow });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { workshop, data, index } = nextProps;
    const { banner } = data || {};
    if (workshop && workshop !== prevState.workshop && workshop === banner.text) {
      setTimeout(() => {
        gotoAnchor(`workshop-${index}`);
      }, 300);
      return ({ workshop, isShow: true });
    }
    return null;
  }

  render() {
    const { width, height } = this.props;
    const { isShow } = this.state;
    const isPhone = isMobile && !isTablet;
    const { data, index } = this.props;
    const {
      banner, button, description, image_background: imgBg, texts, videos,
    } = data;
    const typeVideo = isMobile ? 'phone' : (isTablet ? 'tablet' : 'web');
    const video = _.find(videos, d => d.value.type === typeVideo);
    return (
      <div
        className="div-academy-item div-col"
        style={{
          marginTop: isMobile ? '20px' : '40px',
          marginBottom: isMobile ? '20px' : '40px',
          position: 'relative',
        }}
      >
        <div id={`workshop-${index}`} style={{ position: 'absolute', top: '-120px' }} />
        <img
          loading="lazy"
          src={imgBg ? imgBg.image : ''}
          alt={getAltImageV2(imgBg)}
          style={{
            height: `${height + 80}px`,
          }}
        />
        <div
          style={{ width, height, cursor: 'pointer' }}
          className="div-button-dp div-col justify-center items-center"
          onClick={this.onToggle}
        >
          <img
            loading="lazy"
            style={{ borderRadius: '20px' }}
            src={banner && banner.image ? banner.image.image : ''}
            alt={getAltImageV2(banner ? banner.image : undefined)}
          />
          <div
            className="div-over"
            style={{
              background: '#00000000',
            }}
          />
          <span className="description" style={isPhone ? { fontSize: '0.8rem' } : {}}>
            {banner ? banner.text : ''}
          </span>
          <span className="price" style={isPhone ? { fontSize: '0.6rem' } : {}}>
            {banner ? ReactHtmlParser(banner.description) : ''}
          </span>
          <button
            type="button"
            className="button-bg__none"
            style={isPhone ? { fontSize: '1rem' } : {}}
          >
            {isShow ? '-' : '+' }
          </button>
        </div>
        <div style={{ width }} className={isShow ? 'div-extend' : 'hidden'}>
          <div className="div-video">
            <VideoAcademy url={video ? video.value.video : ''} poster={video ? video.value.placeholder : ''} />
          </div>
          <div className="mt-4" />
          {ReactHtmlParser(description)}
          <div className="mb-4" />
          <div
            className="div-info div-col justify-center items-center w-100"
          >
            <div className={isMobile ? 'div-col justify-center items-center' : 'div-row justify-center items-center'}>
              <span
                style={{
                  fontSize: isPhone ? '0.6rem' : '0.8rem',
                }}
              >
                {texts && texts.length > 0 ? texts[0].value : ''}
              </span>
              <span style={{
                fontSize: isPhone ? '0.6rem' : '0.8rem',
                textAlign: 'center',
                width: isMobile ? '80%' : '',
                marginLeft: '0.3rem',
              }}
              >
                {texts && texts.length > 1 ? texts[1].value : ''}
              </span>
            </div>
            <div className={isMobile ? 'div-col justify-center items-center' : 'div-row justify-center items-center'}>
              <span style={{
                color: '#3D3D3D',
                fontSize: isPhone ? '0.6rem' : '0.8rem',
              }}
              >
                {texts && texts.length > 2 ? texts[2].value : ''}

              </span>
              <span className="ml-3 mr-3" style={{ fontSize: isPhone ? '0.6rem' : '0.8rem' }}>
                {texts && texts.length > 3 ? texts[3].value : ''}
              </span>
              <span style={{ fontSize: isPhone ? '0.6rem' : '0.8rem' }}>
                {texts && texts.length > 4 ? texts[4].value : ''}

              </span>
            </div>
            <span style={{
              fontSize: isPhone ? '0.6rem' : '0.8rem',
              background: '#00000010',
            }}
            >
              {texts && texts.length > 5 ? texts[5].value : ''}

            </span>
          </div>
          <button
            type="button"
            className="button-bg__none mt-5"
            onClick={() => { this.props.history.push(generateUrlWeb(button.link)); }}
          >
            {button ? button.text : ''}
          </button>
        </div>
      </div>
    );
  }
}

AcademyItem.propTypes = {
  width: PropTypes.string.isRequired,
  height: PropTypes.string.isRequired,
  data: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape(PropTypes.shape({
    push: PropTypes.func,
  })).isRequired,
  index: PropTypes.number.isRequired,
};

export default withRouter(AcademyItem);
