/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { trackWindowScroll, LazyLoadImage } from 'react-lazy-load-image-component';
import { getAltImage } from '../../Redux/Helpers';

class LazyImage extends Component {
  onClick = () => {
    if (this.props.onClick) {
      this.props.onClick();
    }
  }

  render() {
    const {
      src, className, scrollPosition, alt,
    } = this.props;
    return (
      <React.Fragment>
        <LazyLoadImage
          alt={alt}
          className={className}
          scrollPosition={scrollPosition}
          src={src}
          wrapperClassName="gallery-img-wrapper"
          onClick={this.onClick}
        />
        {/* <img className={className} loading="lazy" src={src} alt={alt} onClick={this.onClick} /> */}
      </React.Fragment>
    );
  }
}

LazyImage.propTypes = {
  src: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
};

export default trackWindowScroll(LazyImage);
