import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import iconClose from '../../image/icon_close.svg';
import { isMobile } from '../../DetectScreen';
import '../../styles/product-all.scss';

class HeaderSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttons: [
        {
          nameDisplay: 'FILTER NUMBER 2',
          name: '1',
          enable: true,
        },
        {
          nameDisplay: 'FILTER NUMBER 2',
          name: '2',
          enable: false,
        },
        {
          nameDisplay: 'FILTER NUMBER 2',
          name: '3',
          enable: true,
        },
        {
          nameDisplay: 'CITRUS',
          name: '4',
          enable: true,
        },
        {
          nameDisplay: 'FILTER NUMBER 2',
          name: '5',
          enable: false,
        },
        {
          nameDisplay: 'CITRUS',
          name: '6',
          enable: false,
        },
        {
          nameDisplay: 'FILTER NUMBER 2',
          name: '7',
          enable: false,
        },
      ],
      isShowExpend: false,
    };
  }

  onClick = (name, value) => {
    console.log({ name, value });
  }

  onClickFilter = () => {
    this.setState(prev => ({ isShowExpend: !prev.isShowExpend }));
  }

  render() {
    const { buttons, isShowExpend } = this.state;
    const { close } = this.props;
    return (
      <div className="div-headerSelection" style={isMobile ? { width: '100%' } : {}}>
        <div className="header">
          <button
            type="button"
            onClick={close}
          >
            <img loading="lazy" src={iconClose} alt="iconClose" />
          </button>
          <span>
            Select a scent
          </span>
          <button
            type="button"
            onClick={this.onClickFilter}
            // className={isMobile ? 'hidden' : ''}
            className="hidden"
          >
            <i className={isShowExpend ? 'fa fa-chevron-up' : 'fa fa-chevron-down'} />
            {' '}
            CLOSE FILTERS (3)
          </button>
        </div>
        <div className={isShowExpend ? 'div-expend' : 'hiden'}>
          <div className="header-expend">
            {
            _.map(buttons, d => (
              <button
                type="button"
                className={d.enable ? 'bt-filter bt-filter-enable' : 'bt-filter bt-filter-disable'}
                onClick={() => this.onClick(d.name, !d.enable)}
              >
                {d.nameDisplay}
              </button>
            ))
          }
          </div>
          <div className={isMobile ? 'hidden' : 'div-bt-clear'}>
            <button
              type="button"
            >
              CLEAR ALL
            </button>
          </div>
        </div>
      </div>
    );
  }
}

HeaderSelection.propTypes = {
  close: PropTypes.func.isRequired,
};

export default HeaderSelection;
