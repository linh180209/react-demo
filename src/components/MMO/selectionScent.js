import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  Card, CardBody, Row, Col,
} from 'reactstrap';
import HeaderSelection from './headerSelection';
import ScentItem from '../ScentItem';
import ScentItemMobileSelection from '../ScentItem/scentMmoMobileSelection';
import { getCmsCommon, getNameFromCommon } from '../../Redux/Helpers';
import { isBrowser, isMobile, isTablet } from '../../DetectScreen';

class SelectionScent extends Component {
  constructor(props) {
    super(props);
    const { datas, cms } = this.props;
    const cmsCommon = getCmsCommon(cms);
    const addMixBt = getNameFromCommon(cmsCommon, 'ADD_TO_MIX');
    _.forEach(datas, (d) => {
      d.buttonName = `+ ${addMixBt}`;
    });
  }

  onClickRightButton = (data) => {
    if (this.props.onClickRightButton) {
      this.props.onClickRightButton(data);
    }
  }

  render() {
    const {
      close, datas, suggestIds, onClickIngredient, dataGtmtracking, cms,
    } = this.props;
    const cmsCommon = getCmsCommon(cms);
    const outTopBt = getNameFromCommon(cmsCommon, 'Our_top_3_Recommendations');
    const listSuggest = _.filter(datas || [], x => (suggestIds || []).includes(x.id));
    return (
      <Card className={isMobile ? 'div-selectionScent-mobile div-selectionScent' : 'div-selectionScent'}>
        <CardBody className="select-body">
          <HeaderSelection close={close} />
          {
            isMobile && listSuggest.length > 0 ? (
              <div className="div-recommend-mobile">
                <h6 className="header">
                  {outTopBt}
                </h6>
                <Row style={isBrowser ? { margin: '75px 40px 20px 40px' } : {}}>
                  {
                  _.map(listSuggest, x => (
                    <Col md={isTablet ? '6' : '3'} xs="12" className="justify-center items-center flex">
                      <ScentItemMobileSelection
                        cms={this.props.cms}
                        dataGtmtracking={dataGtmtracking}
                        className="scent-selection-mobile"
                        data={x}
                        onClickIngredient={onClickIngredient}
                        onClickRightButton={this.onClickRightButton}
                        isMMO
                      />
                    </Col>
                  ))
                }
                </Row>
                <hr />
              </div>
            ) : (<div />)
          }

          <div className="content">
            <Row style={isMobile && listSuggest.length > 0 ? { marginTop: '0px' } : {}}>
              {
                datas && datas.length > 0 ? (
                  _.map(datas, d => (
                    <Col md={isTablet ? '6' : '3'} xs="12">
                      {
                        isMobile
                          ? (
                            <ScentItemMobileSelection
                              cms={this.props.cms}
                              dataGtmtracking={dataGtmtracking}
                              className="scent-selection-mobile"
                              data={d}
                              onClickIngredient={onClickIngredient}
                              onClickRightButton={this.onClickRightButton}
                              isMMO
                            />
                          )
                          : (
                            <ScentItem
                              dataGtmtracking={dataGtmtracking}
                              className="scent-selection"
                              data={d}
                              onClickIngredient={onClickIngredient}
                              onClickRightButton={this.onClickRightButton}
                              isMMO
                              cms={this.props.cms}
                            />
                          )
                      }

                    </Col>
                  ))
                ) : (<div />)
              }
            </Row>
          </div>
        </CardBody>
      </Card>
    );
  }
}

SelectionScent.propTypes = {
  close: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  onClickRightButton: PropTypes.func.isRequired,
  datas: PropTypes.arrayOf(PropTypes.object).isRequired,
  cms: PropTypes.shape().isRequired,
  suggestIds: PropTypes.arrayOf.isRequired,
};

export default SelectionScent;
