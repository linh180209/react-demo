import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classnames from 'classnames';
import { getAltImage } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

class ItemMenu extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     isShowSubMenu: false,
  //   };
  // }

  onClickMenu = (nameSubActive) => {
    // const { isShowSubMenu } = this.state;
    // this.setState({ isShowSubMenu: !isShowSubMenu });
    const subItem = _.find(this.props.subMenu, x => x.name === nameSubActive);
    this.props.onClick({
      nameActive: this.props.name,
      link: this.props.link,
      content: subItem ? subItem.content : this.props.content,
      nameSubActive,
      subMenuLink: subItem ? subItem.link : '',
    });
  }

  render() {
    const {
      urlIcon, name, subMenu, nameActive, nameSubActive, content,
    } = this.props;
    // const { isShowSubMenu } = this.state;
    const isActive = nameActive === name;
    const htmlWeb = (
      <div className={classnames('item-menu', name === 'LOGOUT' ? '' : 'boder-bottom', isActive ? 'active' : '')}>
        <div className="menu-man">
          <img loading="lazy" src={urlIcon} alt={getAltImage(urlIcon)} />
          <button
            type="button"
            className="bt-boder-none ml-3 mr-3"
            onClick={() => { this.onClickMenu(subMenu ? subMenu[0].name : undefined); }}
          >
            {name}
          </button>
          {/* {
            isActive ? <div className="point" /> : (<div />)
          } */}

        </div>
        {
          subMenu && isActive
            ? (
              <div className="submenu-account">
                {
            _.map(subMenu, x => (
              <button
                type="button"
                className={nameSubActive === x.name ? 'bt-boder-none button-sub bt-menu-active' : 'bt-boder-none button-sub'}
                onClick={() => { this.onClickMenu(x.name); }}
              >
                {x.name}
              </button>
            ))
          }

              </div>
            ) : (<div />)
        }

      </div>
    );

    const htmlMobile = (
      <div className={name === 'LOGOUT' ? 'item-menu-mobile' : 'item-menu-mobile boder-bottom'}>
        <button
          onClick={() => { this.onClickMenu(subMenu ? subMenu[0].name : undefined); }}
          type="button"
          className="menu-man button-bg__none"
        >
          <div>
            <img loading="lazy" src={urlIcon} alt={getAltImage(urlIcon)} />
            <div
              style={{ fontSize: '0.8rem', minWidth: '100px', textAlign: 'initial' }}
              className={isActive ? 'bt-boder-none ml-2 mr-2 pr-3 bt-menu-active' : 'bt-boder-none ml-2 mr-2 pr-3'}
            >
              {name}
            </div>
          </div>
          {
            isActive ? <div className="point" /> : (<div />)
          }

        </button>
        {
          subMenu && isActive
            ? (
              <div className="content-submenu">
                <div className="submenu-account">
                  {
                  _.map(subMenu, x => (
                    <button
                      type="button"
                      className={nameSubActive === x.name ? 'bt-boder-none button-sub bt-menu-active boder-bottom' : 'bt-boder-none button-sub'}
                      onClick={() => { this.onClickMenu(x.name); }}
                    >
                      {x.name}
                    </button>
                  ))
                }
                </div>
                {isActive && _.find(subMenu, x => x.name === nameSubActive) ? _.find(subMenu, x => x.name === nameSubActive).content : {}}
              </div>
            ) : (<div />)
        }
        {isActive ? content : <div />}
      </div>
    );
    return (
      <div>
        {
          isMobile
            ? (
              htmlMobile
            ) : (
              htmlWeb
            )
        }
      </div>
    );
  }
}

ItemMenu.propTypes = {
  urlIcon: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  subMenu: PropTypes.arrayOf(PropTypes.object).isRequired,
  nameSubActive: PropTypes.string.isRequired,
  nameActive: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  content: PropTypes.object.isRequired,
};

export default ItemMenu;
