import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ContactPreferencesItem from './contactPerferencesItem';
import newActive from '../../../image/icon/new-active.png';
import newIcon from '../../../image/icon/new-disable.png';
import emailNotiActive from '../../../image/icon/email-noti-active.png';
import emailNoti from '../../../image/icon/email-noti-disable.png';
import { loginUpdateUserInfo } from '../../../Redux/Actions/login';
import { getNameFromCommon } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';


class ContactPreferences extends Component {
  render() {
    const { login, loginUpdateUserInfo, cmsCommon } = this.props;
    const contactPrefernces = getNameFromCommon(cmsCommon, 'CONTACT_PREFERENCES').toLowerCase();
    const youWhatwouldBt = getNameFromCommon(cmsCommon, 'What_would_you_like');
    const userYourSocialBt = getNameFromCommon(cmsCommon, 'Use_your_social_media');

    return (
      <div className={isMobile ? 'contact-preferences div-col items-center' : 'contact-preferences div-col'}>
        <span className="header-account">
          {contactPrefernces.charAt(0).toUpperCase() + contactPrefernces.slice(1)}
        </span>
        <span
          className={isMobile ? 'mt-3 mb-3' : 'mt-5 mb-5'}
          style={{
            color: '#3d3d3d',
          }}
        >
          {youWhatwouldBt}
          <br />
          {userYourSocialBt}
        </span>
        <div className="div-col">
          <ContactPreferencesItem isNew iconActive={newActive} iconDisable={newIcon} idUser={login.user.id} isActive={login.user.is_get_newsletters} loginUpdateUserInfo={loginUpdateUserInfo} cmsCommon={cmsCommon} />
          <div className="mt-3" />
          <ContactPreferencesItem iconActive={emailNotiActive} iconDisable={emailNoti} idUser={login.user.id} isActive={login.user.is_get_promotional} loginUpdateUserInfo={loginUpdateUserInfo} cmsCommon={cmsCommon} />
          <div className="mt-3" />
        </div>
      </div>
    );
  }
}

ContactPreferences.propTypes = {
  login: PropTypes.shape({
    user: PropTypes.shape(PropTypes.object),
  }).isRequired,
  loginUpdateUserInfo: PropTypes.func.isRequired,
  cmsCommon: PropTypes.arrayOf().isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

const mapDispatchToProps = {
  loginUpdateUserInfo,
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactPreferences);
