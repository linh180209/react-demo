import React, { Component } from 'react';
import PropTypes from 'prop-types';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { UPDATE_USER_URL } from '../../../config';
import { getNameFromCommon } from '../../../Redux/Helpers';
import { isMobile, isTablet } from '../../../DetectScreen';


class ContactPreferencesItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isActive: this.props.isActive,
    };
  }

  onClick = () => {
    const { isActive } = this.state;
    const { isNew } = this.props;
    this.setState({ isActive: !isActive });
    const body = isNew ? { is_get_newsletters: !isActive } : { is_get_promotional: !isActive };
    this.updateUser(body);
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { isActive } = nextProps;
    if (isActive !== prevState.oldisActive) {
      _.assign(objectReturn, { isActive, oldisActive: isActive });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  updateUser = (body) => {
    const options = {
      url: UPDATE_USER_URL.replace('{id}', this.props.idUser),
      method: 'PUT',
      body,
    };
    fetchClient(options, true).then((result) => {
      this.props.loginUpdateUserInfo(body);
    });
  }

  render() {
    const { isActive } = this.state;
    const { iconActive, iconDisable, cmsCommon } = this.props;
    const specialOffersBt = getNameFromCommon(cmsCommon, 'Special_Offers');
    const makeMeBt = getNameFromCommon(cmsCommon, 'Make_me_a_perfume_that_smells_of_love');
    const emailBt = getNameFromCommon(cmsCommon, 'EMAIL').toUpperCase();

    const styleCheck = isActive ? {
      width: '16px',
      height: '16px',
      borderRadius: '4px',
      border: '1px solid #000',
      background: '#000',
    } : {
      width: '16px',
      height: '16px',
      borderRadius: '4px',
      border: '1px solid #cecece',
    };

    const htmlWeb = (
      <div style={{
        width: '100%',
        padding: '40px 80px 40px 80px',
        border: isActive ? '1px solid #000' : '1px solid #cecece',
        borderRadius: '20px',
        display: 'flex',
        flexDirection: 'row',
      }}
      >
        <div style={{
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          width: '70%',
        }}
        >
          <img
            loading="lazy"
            style={{
              width: '57px',
              height: '57px',
            }}
            src={isActive ? iconActive : iconDisable}
            alt="special"
          />
          <div
            className="ml-5"
            style={{
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'space-between',
              height: '100%',
            }}
          >
            <span
              style={{
                fontSize: '1.1rem',
                color: '#000',
              }}
            >
              {specialOffersBt}
            </span>
            <span
              style={{
                color: '#3d3d3d',
              }}
            >
              {makeMeBt}
            </span>
          </div>
        </div>
        <div style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          width: '30%',
        }}
        >
          <span
            style={{
              color: '#000',
              fontSize: '0.9rem',
            }}
          >
            {emailBt}
          </span>
          <button
            type="button"
            className="ml-4 button-bg__none"
            onClick={this.onClick}
          >
            <div style={styleCheck} />
          </button>
        </div>
      </div>
    );

    const htmlMobile = (
      <div
        className="div-col"
        style={{
          width: '100%',
          padding: '10px 20px 10px 20px',
          border: isActive ? '1px solid #000' : '1px solid #cecece',
          borderRadius: '14px',
        }}
      >
        <div className="div-row justify-between">
          <div className="div-row">
            <img
              loading="lazy"
              style={{
                width: '24px',
                height: '24px',
              }}
              src={isActive ? iconActive : iconDisable}
              alt="special"
            />
            <span
              style={{
                fontSize: '1.1rem',
                color: '#000',
                marginLeft: '10px',
              }}
            >
              {specialOffersBt}
            </span>
          </div>
          <div style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: '30%',
          }}
          >
            <span
              style={{
                color: '#000',
                fontSize: '0.9rem',
              }}
            >
              {emailBt}
            </span>
            <button
              type="button"
              className="ml-3 button-bg__none"
              onClick={this.onClick}
            >
              <div style={styleCheck} />
            </button>
          </div>
        </div>
        <div className="mt-3">
          <span
            style={{
              color: '#3d3d3d',
            }}
          >
            {makeMeBt}
          </span>
        </div>
      </div>
    );
    return (
      <div>
        {
          isMobile && !isTablet
            ? (
              htmlMobile
            )
            : (
              htmlWeb
            )
        }
      </div>
    );
  }
}

ContactPreferencesItem.defaultProps = {
  isNew: false,
};

ContactPreferencesItem.propTypes = {
  isActive: PropTypes.bool.isRequired,
  isNew: PropTypes.bool,
  idUser: PropTypes.number.isRequired,
  iconActive: PropTypes.string.isRequired,
  iconDisable: PropTypes.string.isRequired,
  loginUpdateUserInfo: PropTypes.func.isRequired,
  cmsCommon: PropTypes.shape().isRequired,
};

export default ContactPreferencesItem;
