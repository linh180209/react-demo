import React, { Component } from 'react';
import _ from 'lodash';
import ChooseBox from './chooseBox';
import './style.scss';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';

class CardPayment extends Component {
  render() {
    const { selected, images, buttonBlocks } = this.props;
    const addBt = getNameFromButtonBlock(buttonBlocks, 'Add_a_new_card');
    return (
      <div className="div-card-payment selected">
        <div className="card-payment-left">
          <ChooseBox selected={selected} onClick={this.props.onChange} />
          <span>{addBt}</span>
        </div>
        <div className="card-image">
          {
            _.map(images, x => (
              <img loading="lazy" src={x} alt="master" />
            ))
          }
        </div>
      </div>
    );
  }
}

export default CardPayment;
