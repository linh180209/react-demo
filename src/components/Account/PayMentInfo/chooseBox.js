import React, { Component } from 'react';
import './style.scss';

class ChooseBox extends Component {
  onClick = () => {
    if (this.props.onClick) {
      this.props.onClick();
    }
  }

  render() {
    const { selected } = this.props;
    return (
      <div className={`choose-box ${selected ? 'selected' : ''}`} onClick={this.onClick}>
        <div className="div-select" />
      </div>
    );
  }
}

export default ChooseBox;
