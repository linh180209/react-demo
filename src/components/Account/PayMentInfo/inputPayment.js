import React, { Component } from 'react';
import InputMask from 'react-input-mask';
import { Label, Input } from 'reactstrap';
import icSerial from '../../../image/icon/icSerial.svg';
import './style.scss';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';

class InputPayMent extends Component {
  // setupElements = () => {
  //   stripe = window.Stripe(publishableKey);
  //   const elements = stripe.elements();
  //   const style = {
  //     base: {
  //       iconColor: '#666EE8',
  //       color: '#0d0d0d',
  //       fontSize: '18px',
  //       '::placeholder': {
  //         color: '#8D8D8D',
  //       },
  //     },
  //   };

  //   const cardNumberElement = elements.create('cardNumber', {
  //     style,
  //     placeholder: '1111 2222 3333 4444',
  //   });
  //   cardNumberElement.mount('#card-number-element');

  //   const cardExpiryElement = elements.create('cardExpiry', {
  //     style,
  //     placeholder: 'MM/YY',
  //   });
  //   cardExpiryElement.mount('#card-expiry-element');

  //   const cardCvcElement = elements.create('cardCvc', {
  //     style,
  //     placeholder: '123',
  //   });
  //   cardCvcElement.mount('#card-cvc-element');
  //   cardNumberElement.on('change', (event) => {
  //     console.log('cardNumberElement', cardNumberElement);
  //   });

  //   cardExpiryElement.on('change', (event) => {
  //     console.log('cardExpiryElement', event);
  //   });

  //   cardCvcElement.on('change', (event) => {
  //     console.log('cardCvcElement', event);
  //   });
  // }

  render() {
    const { buttonBlocks, listError } = this.props;
    const saveBt = getNameFromButtonBlock(buttonBlocks, 'Save_Credit');
    const cardHolderBt = getNameFromButtonBlock(buttonBlocks, 'Cardholder_name');
    return (
      <div className="div-input-payment">
        <input
          type="text"
          name="name"
          placeholder={cardHolderBt}
          onChange={e => this.props.onChange(e)}
          className={`border-checkout w-100 ${listError.includes('name') ? 'error' : ''}`}
        />
        <div className={`border-checkout div-input-serial ${listError.includes('number') ? 'error' : ''}`}>
          <img loading="lazy" src={icSerial} alt="serial" />
          {/* <div id="card-number-element" /> */}
          <InputMask
            style={{ border: 'none' }}
            type="text"
            name="number"
            placeholder="1111 2222 3333 4444"
            mask="9999 9999 9999 9999"
            maskChar={null}
            // defaultValue={cardInfo.serial}
            onChange={e => this.props.onChange(e)}
          />
        </div>
        <div className="div-input-security">
          {/* <div id="card-expiry-element" className="border-checkout" />
           */}
          <InputMask
            className={`border-checkout w-100 ${listError.includes('expMonth') ? 'error' : ''}`}
            type="text"
            name="expMonth"
            placeholder="MM/YY"
            mask="99/99"
            maskChar={null}
            // defaultValue={cardInfo.expireDate}
            onChange={e => this.props.onChange(e)}
          />
          {/* <div id="card-cvc-element" className="border-checkout" /> */}
          <InputMask
            className={`border-checkout w-100 ${listError.includes('cvc') ? 'error' : ''}`}
            type="text"
            name="cvc"
            placeholder="123"
            mask="999"
            maskChar={null}
            onChange={e => this.props.onChange(e)}
          />
        </div>
        {/* <Label check style={{ cursor: 'pointer' }}>
          <Input
            name="create_account"
            id="idCheckbox"
            type="checkbox"
            className="custom-input-filter__checkbox"
            style={{ height: '42px' }}
            onChange={this.onChangeCheckBox}
          />
          {' '}
          <Label
            for="idCheckbox"
            style={{ pointerEvents: 'none' }}
          />
          {saveBt}
        </Label> */}
      </div>
    );
  }
}

export default InputPayMent;
