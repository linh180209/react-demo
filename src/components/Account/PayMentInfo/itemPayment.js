import React, { Component } from 'react';
import ChooseBox from './chooseBox';
import icVisa from '../../../image/icon/visa.svg';
import icMaster from '../../../image/icon/masterColor.svg';
import './style.scss';
import { isMobile } from '../../../DetectScreen';

class ItemPayment extends Component {
  render() {
    const { data, idDefault } = this.props;
    const {
      id, brand, last4, name, exp_month: expMonth, exp_year: expYear,
    } = data;
    return (
      <div className="div-item-payment">
        <div className="div-header">
          <ChooseBox selected={idDefault === id} onClick={this.props.onChange} />
          <img
            loading="lazy"
            src={brand === 'visa' ? icVisa : icMaster}
            alt="visa"
            style={{
              height: brand === 'visa' ? isMobile ? '15px' : '19px' : isMobile ? '23px' : '32px',
            }}
          />
        </div>
        <span className="serial">
          {`**** ${last4}`}
        </span>
        <div className="div-text-name">
          <span>
            {name}
          </span>
          <span>
            {`${expMonth}/${expYear.substring(2, 4)}`}
          </span>
        </div>
      </div>
    );
  }
}

export default ItemPayment;
