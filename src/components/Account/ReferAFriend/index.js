import React, { Component } from 'react';
import { Col } from 'reactstrap';
import _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import InputRefer from './inputRefer';
import HistoryPurchaseItem from './historyPurchaseItem';
import RewardProgressBar from './rewardProgressBar';
import {
  toastrSuccess,
  toastrError,
} from '../../../Redux/Helpers/notification';
import {
  getNameFromCommon,
  fetchCMSHomepage,
  getSEOFromCms,
  scrollTop,
  getNameFromButtonBlock,
  segmentAccountDashboardReferAFriendCopyButtonClicked,
} from '../../../Redux/Helpers';

import {
  GET_SUBSCRIPTION_ORDER,
  GET_USER_OUTCOME,
  GET_SHARED_PROMO,
} from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { isBrowser, isMobile } from '../../../DetectScreen';

const prePareCms = (cms) => {
  if (cms) {
    const seo = getSEOFromCms(cms);
    const { body } = cms;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    return {
      seo,
      buttonBlocks,
    };
  }
  return { seo: undefined, buttonBlocks: [] };
};
class ReferAFriend extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seo: {},
      buttonBlocks: [],
      sharedPromoData: undefined,
      copied: false,
    };
  }

  componentDidMount() {
    const { login } = this.props;
    if (login.user) {
      this.fetchSubscription();
    }
    this.fetchDataInit();
  }

  fetchSubscription = () => {
    const { login } = this.props;
    const option = {
      url: GET_SHARED_PROMO,
      method: 'GET',
    };

    const pending = [fetchClient(option, true)];
    this.props.loadingPage(true);
    Promise.all(pending)
      .then((result) => {
        if (result && result.length > 0) {
          this.setState({ sharedPromoData: result });
          this.props.loadingPage(false);
          return;
        }
        throw new Error(result.message);
      })
      .catch((err) => {
        toastrError(err.message);
        this.props.loadingPage(false);
      });
  };

  fetchDataInit = async () => {
    const { cms } = this.props;
    const subCms = _.find(cms, x => x.title === 'Refer a friend');
    if (_.isEmpty(subCms)) {
      const pending = [fetchCMSHomepage('refer-a-friend')];
      try {
        const results = await Promise.all(pending);
        const cmsData = results[0];
        this.props.addCmsRedux(cmsData);
        const dataCms = prePareCms(cmsData);
        this.setState(dataCms);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const dataCms = prePareCms(subCms);
      this.setState(dataCms);
    }
  };

  onClickCopy = (value) => {
    const textField = document.createElement('textarea');
    textField.innerText = value;
    segmentAccountDashboardReferAFriendCopyButtonClicked(value);
    document.body.appendChild(textField);
    textField.select();
    document.execCommand('copy');
    textField.remove();
    toastrSuccess('Copy to Clipboard');
    isMobile ? this.setState({ copied: true }) : '';
  };

  render() {
    const {
      seo, copied, buttonBlocks, sharedPromoData,
    } = this.state;
    const code = sharedPromoData ? sharedPromoData[0].code : undefined;
    const progressPoint = sharedPromoData
      ? sharedPromoData[0].steps
      : undefined;

    const orderHistory = sharedPromoData
      ? sharedPromoData[0].orders
      : undefined;

    const referAFriend = getNameFromCommon(
      this.props.cmsCommon,
      'REFER_A_FRIEND',
    ).toUpperCase();
    const purchaseHistory = getNameFromButtonBlock(
      buttonBlocks,
      'Purchase_history',
    );
    const referalAndCollect = getNameFromButtonBlock(
      buttonBlocks,
      'Referal_and_collect',
    );
    const reward_progress = getNameFromButtonBlock(
      buttonBlocks,
      'Reward_progress',
    ).toUpperCase();
    const referral_bonus_earned = getNameFromButtonBlock(
      buttonBlocks,
      'Referral_bonus_earned',
    ).toUpperCase();

    return (
      <div
        className={
          isMobile
            ? 'div-col items-center div-refer-a-friend'
            : 'div-col div-refer-a-friend'
        }
      >
        <span className="header-account" style={{ marginTop: isMobile ? '30px' : '' }}>
          {referAFriend.charAt(0).toUpperCase() + referAFriend.slice(1)}
        </span>
        <span
          className="mt-5 mb-3"
          style={{
            color: '#3d3d3d',
            textAlign: isBrowser ? 'initial' : 'center',
          }}
        >
          {referalAndCollect}
        </span>
        {/* <span
          className="header-account"
          style={{
            fontSize: '1.1rem',
          }}
        >
        Send an invitation link
        </span>
        <div className="mt-4" />
        <Col
          md="5"
          xs="12"
          style={{
            padding: '0px',
          }}
        >
          <InputRefer title="Friends email" buttonName="SEND INVITE" />
        </Col>
        <div className="mb-5" /> */}
        {/* <span
          className="header-account"
          style={{
            fontSize: '1.1rem',
          }}
        >
          {shareBt}
        </span> */}
        <div className="mt-5" />
        <Col
          md="6"
          xs="12"
          style={{
            padding: '0px',
          }}
        >
          <InputRefer
            id="id-ref"
            value={code}
            copied={copied}
            buttonName="COPY"
            onClick={this.onClickCopy}
          />
        </Col>
        <div className="mb-5" />
        {/* <span
          className="header-account"
          style={{
            fontSize: '1.1rem',
          }}
        >
        More ways to invite your friends
        </span>
        <div className="mt-4" />
        <div className="div-row">
          <button
            type="button"
            style={{
              padding: '8px 40px 8px 40px',
              border: 'none',
              borderRadius: '10px',
              background: '#5F84E5',
              color: '#fff',
            }}
          >
            SHARE ON FACEBOOK
          </button>
          <div className="ml-4" />
          <button
            type="button"
            style={{
              padding: '8px 40px 8px 40px',
              border: 'none',
              borderRadius: '10px',
              background: '#F16161',
              color: '#fff',
            }}
          >
            INVITE FROM GMAIL
          </button>
        </div>
        <div className="mb-5" /> */}
        <span
          className="header-account"
          style={{
            fontSize: '1.2rem',
            letterSpacing: '0.1rem',
            margin: isMobile ? '14px 0 20px' : '',
          }}
        >
          {isMobile ? referral_bonus_earned : reward_progress}
        </span>
        <div className="ml-4 mb-4" />
        <Col
          md="6"
          xs="12"
          style={{
            padding: '0px',
          }}
        >
          <RewardProgressBar step={progressPoint} />
        </Col>
        <div
          className="w-100"
          style={{
            marginTop: '4rem',
          }}
        >
          <span
            style={{
              fontSize: '1rem',
              fontStyle: 'italic',
            }}
          >
            {purchaseHistory}
            :
          </span>
        </div>
        <div className="mb-2" />
        <Col
          md="6"
          xs="12"
          style={{
            padding: '0px',
          }}
        >
          {!orderHistory
            ? null
            : _.map(orderHistory, (x, index) => (
              <HistoryPurchaseItem
                orderHistoryData={x}
                key={index}
                num={orderHistory.length - index}
              />
            ))}
        </Col>
        <div className="mb-4" />
      </div>
    );
  }
}

ReferAFriend.propTypes = {
  login: PropTypes.shape({
    user: PropTypes.shape(PropTypes.object),
  }).isRequired,
  cmsCommon: PropTypes.shape().isRequired,
  loadingPage: PropTypes.func.isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  cms: PropTypes.array.isRequired,
  login: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}
export default connect(mapStateToProps)(ReferAFriend);
