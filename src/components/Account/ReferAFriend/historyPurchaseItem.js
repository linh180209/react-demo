import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Col } from 'reactstrap';
import moment from 'moment';

class HistoryPurchaseItem extends Component {
  render() {
    const { orderHistoryData, num } = this.props;
    const { name, date_created } = orderHistoryData;
    return (
      <div className="div-row">
        <Col
          md="1"
          xs="1"
          style={{
            padding: '0px',
          }}
          className="d-flex align-items-center"
        >
          <span
            style={{
              fontSize: '1rem',
              color: '#8D8D8D',
            }}
          >
            #
            {num < 10 ? `0${num.toString()}` : num.toString()}
          </span>
        </Col>
        <Col
          md="9"
          xs="12"
          style={{
            padding: '0px',
            paddingLeft: '16px',
          }}
        >
          <p
            className="m-0"
            style={{
              fontSize: '0.8rem',
              fontStyle: 'italic',
            }}
          >
            {name}
          </p>
          <p
            className="m-0"
            style={{
              fontSize: '0.8rem',
              fontStyle: 'italic',
            }}
          >
            Placed on
            {' '}
            {date_created
              ? moment(date_created).format('DD MMMM YYYY h:mm:ss')
              : ''}
          </p>
        </Col>
      </div>
    );
  }
}

HistoryPurchaseItem.defaultProps = {
  orderHistoryData: undefined,
  num: undefined,
};

HistoryPurchaseItem.propTypes = {
  orderHistoryData: PropTypes.shape(PropTypes.object),
  num: PropTypes.number,
};

export default HistoryPurchaseItem;
