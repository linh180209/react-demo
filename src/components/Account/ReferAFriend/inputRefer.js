import React, { Component } from 'react';
import PropTypes from 'prop-types';
import icCopyMobile from '../../../image/icon/ic-copy-mobile.svg';
import { isMobile } from '../../../DetectScreen';

class InputRefer extends Component {
  render() {
    const {
      title, buttonName, value, id, copied,
    } = this.props;
    const htmlMobile = (
      <div
        style={{
          border: '1px solid',
          color: 'black',
          borderRadius: '4px',
          display: 'flex',
          justifyContent: 'space-between',
        }}
      >
        <input
          id={id}
          type="text"
          disabled
          style={{
            flex: '3',
            border: 'none',
            padding: '16px 16px 14px 16px',
            background: 'transparent',
            textAlign: 'center',
          }}
          defaultValue={value}
        />
        <button
          type="button"
          className="bt-boder-none"
          style={{
            padding: '16px 16px 14px 16px',
            marginRight: '8px',
            borderRadius: '8px',
            position: 'absolute',
            right: '-10px',
            top: copied ? '0' : '-3px',
          }}
          onClick={() => this.props.onClick(value)}
        >
          {copied ? (
            <span style={{ color: '#34C01E' }}>copied</span>
          ) : (
            <img
              loading="lazy"
              src={icCopyMobile}
              alt={icCopyMobile}
              style={{
                width: '30px',
              }}
            />
          )}
        </button>
      </div>
    );
    const htmlWeb = (
      <div
        className="div-row"
        style={{
          paddingBottom: '8px',
        }}
      >
        <input
          id={id}
          type="text"
          disabled
          style={{
            background: 'transparent',
            flex: '3',
            padding: '12px 16px 10px',
            marginRight: '8px',
            border: '1px solid',
            fontSize: '0.8rem',
            color: 'black',
            borderRadius: '4px',
          }}
          defaultValue={value}
        />
        <button
          type="button"
          className="bt-boder-none"
          style={{
            background: '#0d0d0d',
            flex: '1',
            padding: '12px 16px 10px',
            marginRight: '8px',
            border: '1px solid',
            color: '#fff',
            borderRadius: '8px',
          }}
          onClick={() => this.props.onClick(value)}
        >
          {buttonName}
        </button>
      </div>
    );
    return (
      <div className="div-col">
        <span
          style={{
            fontStyle: 'italic',
            color: 'rgba(0, 0, 0, 0.25)',
          }}
        >
          {title}
        </span>
        {isMobile ? htmlMobile : htmlWeb}
      </div>
    );
  }
}

InputRefer.defaultProps = {
  title: '',
  copied: false,
};

InputRefer.propTypes = {
  title: PropTypes.string,
  buttonName: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  copied: PropTypes.bool,
};

export default InputRefer;
