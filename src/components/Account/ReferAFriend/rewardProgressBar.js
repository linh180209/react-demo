import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Col } from 'reactstrap';
import classNames from 'classnames';
import icRewardProgress1 from '../../../image/icon/ic-reward-progress.svg';
import icRewardProgress2 from '../../../image/icon/ic-reward-progress2.svg';
import { isMobile } from '../../../DetectScreen';

class RewardProgressBar extends Component {
  render() {
    const { step } = this.props;
    const htmlWeb = (
      <div className="div-row reward-progress">
        <Col
          md="4"
          xs="3"
          style={{
            padding: '0px',
          }}
        >
          <span
            style={{
              fontSize: '0.8rem',
            }}
          >
            1st
          </span>
          <div className={classNames('step', { active: step >= 1 })} />
        </Col>
        <Col
          md="4"
          xs="3"
          style={{
            padding: '0px',
          }}
        >
          <span
            style={{
              fontSize: '0.8rem',
            }}
          >
            2nd
          </span>
          <div className={classNames('step', { active: step >= 2 })} />
        </Col>
        <Col
          md="4"
          xs="3"
          style={{
            padding: '0px',
          }}
        >
          <span
            style={{
              fontSize: '0.8rem',
            }}
          >
            3rd
          </span>
          <div className={classNames('step', { active: step >= 3 })} />
        </Col>
        <Col
          md="4"
          xs="3"
          style={{
            padding: '0px',
          }}
        >
          <img
            loading="lazy"
            src={step >= 3 ? icRewardProgress2 : icRewardProgress1}
            alt={step >= 3 ? icRewardProgress2 : icRewardProgress1}
            style={{
              position: 'absolute',
              top: '-4%',
              left: '24px',
              width: '40px',
            }}
          />
        </Col>
      </div>
    );
    const htmlMobile = (
      <div>
        <div className="div-row reward-progress">
          <Col
            xs="12"
            style={{
              padding: '0px',
              textAlign: 'center',
            }}
          >
            <img
              loading="lazy"
              src={step >= 3 ? icRewardProgress2 : icRewardProgress1}
              alt={step >= 3 ? icRewardProgress2 : icRewardProgress1}
              style={{ width: '24%' }}
            />
          </Col>
        </div>
        <div className="div-row reward-progress mt-3">
          <Col
            xs="4"
            style={{
              padding: '0px',
            }}
          >
            <span
              style={{
                fontSize: '0.8rem',
              }}
            >
              1st
            </span>
            <div className={classNames('step', { active: step >= 1 })} />
          </Col>
          <Col
            xs="4"
            style={{
              padding: '0px',
            }}
          >
            <span
              style={{
                fontSize: '0.8rem',
              }}
            >
              2nd
            </span>
            <div className={classNames('step', { active: step >= 2 })} />
          </Col>
          <Col
            xs="4"
            style={{
              padding: '0px',
            }}
          >
            <span
              style={{
                fontSize: '0.8rem',
              }}
            >
              3rd
            </span>
            <div className={classNames('step', { active: step >= 3 })} />
          </Col>
        </div>
      </div>
    );
    return isMobile ? htmlMobile : htmlWeb;
  }
}

RewardProgressBar.defaultProps = {
  step: 0,
};

RewardProgressBar.propTypes = {
  step: PropTypes.number,
};

export default RewardProgressBar;
