import React, { Component } from 'react';
import moment from 'moment';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import icBack from '../../../image/icon/back-order-mobile.svg';
import icAddress from '../../../image/icon/icAddress.svg';
import ProcessOrder from './processOrder';
import { getNameFromButtonBlock, getLinkFromButtonBlock, scrollToTargetAdjusted } from '../../../Redux/Helpers';
import tracking1 from '../../../image/icon/tracking_1.svg';
import tracking2 from '../../../image/icon/tracking_2.svg';
import tracking3 from '../../../image/icon/tracking_3.svg';
import tracking4 from '../../../image/icon/tracking_4.svg';

const listProcessStatus = ['paid', 'processed', 'shipped', 'completed'];

class OrderTrackingMobile extends Component {
  componentDidMount() {
    setTimeout(() => {
      scrollToTargetAdjusted('div-tracking-mobile');
    }, 200);
  }

  formatDate = (date) => {
    if (!date) {
      return '';
    }
    if (moment(date).isValid) {
      return moment(date).format('DD MMM YYYY');
    }
    return '';
  };

  getProcess = (status) => {
    const index = _.findIndex(listProcessStatus, x => x === status);
    if (index === -1) {
      return 3;
    }
    return index;
  }

  getImage = (index) => {
    switch (index) {
      case 0:
        return tracking1;
      case 1:
        return tracking2;
      case 2:
        return tracking3;
      case 3:
        return tracking4;
      default:
        return 3;
    }
  }

  render() {
    const { buttonBlocks, onClickBackTracking, data } = this.props;
    const {
      id, total, date_created: dataCreate, line_items: lineItems,
      subtotal, shipping, shipping_address: shippingAddress, discount, currency,
      status, trackings, tracking_number: trackingNumber,
    } = this.props.data;
    const indexProcess = this.getProcess(status);
    const orderTrackingsBt = getNameFromButtonBlock(buttonBlocks, 'delivery_details');
    const trackingNumberBt = getNameFromButtonBlock(buttonBlocks, 'Tracking_Number');
    const receiveBt = getNameFromButtonBlock(buttonBlocks, 'Receiver');
    const orderBt = getNameFromButtonBlock(buttonBlocks, 'Order');

    const orderReceiveBt = getNameFromButtonBlock(buttonBlocks, 'order_received');
    const linkOrderReceiveBt = getLinkFromButtonBlock(buttonBlocks, 'order_received');
    const processingBt = getNameFromButtonBlock(buttonBlocks, 'processing');
    const linkProcessingbt = getLinkFromButtonBlock(buttonBlocks, 'processing');
    const shippedBt = getNameFromButtonBlock(buttonBlocks, 'shipped');
    const linkShippeBt = getLinkFromButtonBlock(buttonBlocks, 'shipped');
    const deliveredBt = getNameFromButtonBlock(buttonBlocks, 'delivered');
    const linkDelivereBt = getLinkFromButtonBlock(buttonBlocks, 'delivered');

    const dataName = [
      { name: orderReceiveBt, text: linkOrderReceiveBt },
      { name: processingBt, text: linkProcessingbt },
      { name: shippedBt, text: linkShippeBt },
      { name: deliveredBt, text: linkDelivereBt }];

    return (
      <div className="div-tracking-mobile animated faster fadeInRight">
        <div id="div-tracking-mobile" className="anchor-scroll" />
        <button type="button" className="bt-back" onClick={onClickBackTracking}>
          <img loading="lazy" src={icBack} alt="back" />
        </button>
        <h3>
          {orderTrackingsBt}
        </h3>
        <div className="div-tracking-detail">
          <span>
            {`${dataName[indexProcess].name} on ${this.formatDate(dataCreate)}`}
          </span>
          <img loading="lazy" src={this.getImage(indexProcess)} alt="tracking" />
        </div>
        <span className="tracking-number">
          {`${trackingNumberBt} - ${trackingNumber}`}
        </span>
        <div className="div-address">
          <img loading="lazy" src={icAddress} alt="address" />
          <div className="address">
            <span>
              {`${receiveBt}: ${shippingAddress ? shippingAddress.name : ''}`}
            </span>
            <span>
              {shippingAddress ? `${shippingAddress.street1} ${shippingAddress.country.name} ${shippingAddress.postal_code}` : ''}
            </span>
          </div>
        </div>
        <hr />
        <ProcessOrder
          buttonBlocks={buttonBlocks}
          status={status}
          trackings={trackings}
        />
        <div className="div-order">
          {orderBt}
          <Link to="#" className="ml-2">
            {id}
          </Link>
        </div>
      </div>
    );
  }
}

export default OrderTrackingMobile;
