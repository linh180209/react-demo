/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  generaCurrency, generateUrlWeb, getNameFromButtonBlock, getUrlGotoProductOderPage, segmentAccountDashboardReorderItemClicked,
} from '../../../Redux/Helpers';
import { createBasketGuest, addProductBasket, reorderProductBasket } from '../../../Redux/Actions/basket';
import loadingPage from '../../../Redux/Actions/loading';
import { isMobile } from '../../../DetectScreen';
import BottleCustom from '../../B2B/CardItem/bottleCustom';
import bottleHomeScent from '../../../image/bottle_home_card.png';
import { ADD_BASKET_URL } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';

class ItemOrderNew extends Component {
  formatDate = (date) => {
    if (!date) {
      return '';
    }
    if (moment(date).isValid) {
      return moment(date).format('DD MMM YYYY HH:mm:ss');
    }
    return '';
  };

  onClickDetail = () => {
    const { data } = this.props;
    if (this.props.onClickDetail) {
      this.props.onClickDetail(data);
    }
  }

  onClickProduct = (data) => {
    let urlPro = getUrlGotoProductOderPage(data).url;
    const { meta } = data;
    if (meta && meta.url) {
      urlPro = meta.url;
    }
    if (urlPro) {
      this.props.history.push(generateUrlWeb(urlPro));
    }
  }

  onClickReview = (data) => {
    const { item } = data;
    this.props.history.push(generateUrlWeb(`/account/reviews/${item.product.id}`));
  }

  addToCart = async (data) => {
    const { basket } = this.props;
    segmentAccountDashboardReorderItemClicked(data);
    const {
      id, item,
    } = data;

    const dataT = {
      idCart: basket.id,
      idItem: item?.id,
      idLineItem: id,
    };
    this.props.reorderProductBasket(dataT);
  }

  render() {
    const {
      isAll, isShip, data, buttonBlocks, isDetail, isBoutiqueTab,
    } = this.props;
    const { line_items: lineItems, status, tracking_number: trackingNumber } = data;
    const managerBt = getNameFromButtonBlock(buttonBlocks, 'MANAGE');
    const orderBt = getNameFromButtonBlock(buttonBlocks, 'Order');
    const QTYBt = getNameFromButtonBlock(buttonBlocks, 'QTY');
    const trackingNumberBt = getNameFromButtonBlock(buttonBlocks, 'Tracking_number');
    const writeAReviewBt = getNameFromButtonBlock(buttonBlocks, 'write_a_review');
    const placeOrderBt = getNameFromButtonBlock(buttonBlocks, 'Placed_on');
    const reorderBt = getNameFromButtonBlock(buttonBlocks, 'reorder');
    const eauBt = getNameFromButtonBlock(buttonBlocks, 'Eau de parfum');
    const ml25Bt = getNameFromButtonBlock(buttonBlocks, 'ml25');
    return (
      <div className="div-item-order-new">
        <div className={isDetail ? 'hidden' : 'div-header'}>
          <div className="div-left">
            <div className="div-order">
              {orderBt}
              <Link to="#" className="ml-2" onClick={this.onClickDetail}>
                {data.id}
              </Link>
            </div>
            <span>
              {`${placeOrderBt} ${this.formatDate(data.date_created)}`}
            </span>
          </div>
          <div className="div-right">
            {
              isMobile ? (
                <div />
                // <div className="div-status" onClick={this.onClickDetail}>
                //   <span>
                //     {`${data.status} >`}
                //   </span>
                // </div>
              ) : isAll ? (
                <button type="button" onClick={this.onClickDetail}>
                  {managerBt}
                </button>
              ) : (
                <span className="text-name">
                  {data.status}
                </span>
              )
            }
          </div>
        </div>
        {
          _.map(lineItems, (d) => {
            const {
              item, combo, image,
              image_display: imgDisplay, meta, name, is_customized: isCustomized, weights,
              is_display_name: isDisplayName, is_black: isBlack, font, color, is_free_gift: isFree,
            } = d;
            console.log('weights == ', d);
            const combos = combo ? _.filter(combo, x => x.product.type === 'Scent') : [];
            const isShowBottle = ((item?.product?.type?.name === 'Creation' && isCustomized) || item?.product?.type?.name === 'Perfume' || item?.product?.type?.name === 'Elixir' || (item?.product?.type?.name === 'Gift' && isCustomized)) && item?.is_sample === false;
            const isHomeScent = item?.product?.type?.name === 'home_scents' && combo && combo.length > 0;
            const isImageText = !!image;
            return (
              <div className="div-order">
                <div className="div-left">
                  <div className="div-image-product">
                    {
                      isShowBottle || isHomeScent ? (
                        <BottleCustom
                          isImageText={isImageText}
                          onGotoProduct={() => this.onGotoProduct(false)}
                          image={image}
                          isBlack={isBlack}
                          font={font}
                          color={color}
                          eauBt={eauBt}
                          mlBt={ml25Bt}
                          isDisplayName={isDisplayName}
                          name={name}
                          combos={combos}
                          bottleImage={isHomeScent ? bottleHomeScent : undefined}
                        />
                      ) : (
                        <img
                          className="img-product"
                          loading="lazy"
                          src={d.image_display}
                          alt="scent"
                        // style={isDetail ? { cursor: 'pointer' } : {}}
                          style={{ cursor: 'pointer' }}
                          onClick={() => this.onClickProduct(d)}
                        />
                      )
                    }
                  </div>
                  <div className="div-text">
                    <span className="text-type">
                      {d.item.product.type.alt_name}
                    </span>
                    {
                    isDetail ? (
                      <div className="div-text-bottom">
                        <span className="text-name">
                          {d.name}
                        </span>
                      </div>
                    ) : (
                      <span className="text-name">
                        {d.name}
                      </span>
                    )
                  }
                    {
                    isMobile ? (
                      <span className="text-quantity">
                        {`X${d.quantity}`}
                      </span>
                    ) : (null)
                  }
                    {
                    weights?.length > 0 && (
                      <span className="text-weights">
                        {
                          _.map(weights, (x, index) => `${x.name}: ${x.weight}${index !== weights.length - 1 ? ' - ' : ''}`)
                        }
                      </span>
                    )
                  }
                  </div>
                </div>
                <div className={isMobile ? 'hidden' : 'div-mid'}>
                  <span className="text-type">
                    {QTYBt}
                  </span>
                  <span className="text-name">
                    {d.quantity}
                  </span>
                </div>
                <div className="div-right">
                  {
                  isMobile ? (
                    <div className="div-info-mobile">
                      <div className="div-status" onClick={this.onClickDetail}>
                        <span>
                          {`${data.status} >`}
                        </span>
                      </div>
                      <button
                        onClick={() => this.addToCart(d)}
                        type="button"
                        className="bt-reorder"
                      >
                        {reorderBt}
                      </button>
                    </div>

                  ) : isAll ? (
                    <div className="div-status">
                      <span>
                        {status}
                      </span>
                      <button
                        onClick={() => this.addToCart(d)}
                        type="button"
                        className="bt-reorder"
                      >
                        {reorderBt}
                      </button>
                    </div>
                  ) : isShip ? (
                    <React.Fragment>
                      <span className="text-type">
                        {`${trackingNumberBt}:`}
                      </span>
                      <span className="text-name">
                        {trackingNumber}
                      </span>
                    </React.Fragment>
                  ) : (
                    <button
                      onClick={() => this.onClickReview(d)}
                      type="button"
                      className={`bt-review ${isDetail ? 'detail' : ''} ${isBoutiqueTab ? 'hidden' : ''}`}
                    >
                      {writeAReviewBt}
                    </button>
                  )
                }
                </div>
              </div>
            );
          })
        }
      </div>
    );
  }
}

ItemOrderNew.defaultProps = {
  isAll: false,
  isShip: false,
  isReceive: false,
};

ItemOrderNew.propTypes = {
  isAll: PropTypes.bool,
  isShip: PropTypes.bool,
  isReceive: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    basket: state.basket,
  };
}

const mapDispatchToProps = {
  loadingPage,
  addProductBasket,
  createBasketGuest,
  reorderProductBasket,
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemOrderNew);
