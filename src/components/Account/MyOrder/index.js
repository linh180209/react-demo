import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import _ from 'lodash';
import MyOrderItem from './myOrderItem';

import { GET_MYORDER_URL, GET_ORDER_URL } from '../../../config';
import random from '../../../image/icon/random.svg';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import {
  getNameFromCommon, fetchCMSHomepage, getSEOFromCms, getNameFromButtonBlock, generateUrlWeb, gotoShopHome,
} from '../../../Redux/Helpers';
import DropDownOrder from './dropDownOrder';
import ItemOrderNew from './itemOrderNew';
import OrderDetailNew from './orderDetailNew';
import OrderTrackingMobile from './orderTrackingMobile';
import OrderDetailNewMobile from './orderDetailNewMobile';
import { isMobile } from '../../../DetectScreen';
import auth from '../../../Redux/Helpers/auth';

const prePareCms = (cms) => {
  if (cms) {
    const seo = getSEOFromCms(cms);
    const { body, header_text: headerText } = cms;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    return {
      seo, buttonBlocks, headerText,
    };
  }
  return { seo: undefined, buttonBlocks: [], headerText: '' };
};
class MyOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataMyOrder: [],
      seo: undefined,
      buttonBlocks: [],
      headerText: '',
      indexTabs: 0,
      dataDetail: undefined,
      isDetailTracking: false,
    };
  }

  componentDidMount() {
    this.fetchCMS();
    const { user } = auth.getLogin();
    if (user) {
      this.fetchMyOrder(user.id);
    }
  }

  fetchCMS = async () => {
    const { cms } = this.props;
    const myOrder = _.find(cms, x => x.title === 'My Order');
    if (_.isEmpty(myOrder)) {
      try {
        const cmsData = await fetchCMSHomepage('my-order');
        this.props.addCmsRedux(cmsData);
        const dataCms = prePareCms(cmsData);
        this.setState(dataCms);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const dataCms = prePareCms(myOrder);
      this.setState(dataCms);
    }
  }

  fetchMyOrder = (id) => {
    const options = {
      url: `${GET_MYORDER_URL.replace('{user_pk}', id)}?query={id,line_items{id, weights, image_display,item{name,id,product{id,type},is_sample},name,price,total,quantity,color,font,image,is_black,is_customized,is_display_name},type,total,date_created,status,is_express,tracking_number}`,
      method: 'GET',
    };
    fetchClient(options, true).then((result) => {
      if (result && !result.isError) {
        this.setState({ dataMyOrder: result });
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err);
    });
  }

  gotoLanding = () => {
    // this.props.history.push(generateUrlWeb('/'));
    gotoShopHome();
  }

  onClickBack = () => {
    this.setState({ dataDetail: undefined });
  }

  onClickTracking = () => {
    this.setState({ isDetailTracking: true });
  }

  onClickBackTracking = () => {
    this.setState({ isDetailTracking: false, isBackTracking: true });
  }

  onClickDetail = (data) => {
    const { id } = data;
    this.fetchDataDetail(id);
  }

  fetchDataDetail = (id) => {
    this.props.loadingPage(true);
    const options = {
      url: `${GET_ORDER_URL.replace('{id}', id)}`,
      method: 'GET',
    };
    fetchClient(options, true).then((result) => {
      this.props.loadingPage(false);
      if (result && !result.isError) {
        this.setState({ dataDetail: result, isBackTracking: false });
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err);
      this.props.loadingPage(false);
    });
  }

  render() {
    const {
      login, history, basket, loadingPage,
    } = this.props;
    const {
      dataDetail, buttonBlocks, isDetailTracking, isBackTracking,
    } = this.state;
    const { dataMyOrder, indexTabs } = this.state;
    const dataToShip = _.filter(dataMyOrder, x => x.status === 'processed' && x.type === 'standard');
    const dataToReceive = _.filter(dataMyOrder, x => x.status === 'completed' && x.type === 'standard');
    const dataToBoutique = _.filter(dataMyOrder, x => x.status === 'completed' && x.type === 'pos');
    const myOrder = getNameFromCommon(this.props.cmsCommon, 'MY_ORDER').toLowerCase();
    const allBt = getNameFromButtonBlock(buttonBlocks, 'All');
    const toShipBt = getNameFromButtonBlock(buttonBlocks, 'To_Ship');
    const toReceiveBt = getNameFromButtonBlock(buttonBlocks, 'To_Receive');
    const boutiqueBt = getNameFromButtonBlock(buttonBlocks, 'Boutique');
    const allTab = (
      <div className="div-all-tab">
        {/* <div className="mt-3">
          <DropDownOrder />
        </div> */}
        {
          _.map(dataMyOrder, x => (
            <ItemOrderNew
              buttonBlocks={buttonBlocks}
              isAll
              data={x}
              onClickDetail={this.onClickDetail}
              history={history}
            />
          ))
        }
      </div>
    );
    const shipTab = (
      <div className="div-ship-tab">
        {/* <div className="mt-3">
          <DropDownOrder />
        </div> */}
        {
          _.map(dataToShip, x => (
            <ItemOrderNew
              buttonBlocks={buttonBlocks}
              isShip
              data={x}
              onClickDetail={this.onClickDetail}
              history={history}
            />
          ))
        }
      </div>
    );
    const boutiqueTab = (
      <div className="div-receivetab">
        {/* <div className="mt-3">
          <DropDownOrder />
        </div> */}
        {
          _.map(dataToBoutique, x => (
            <ItemOrderNew
              buttonBlocks={buttonBlocks}
              isReceive
              isBoutiqueTab
              data={x}
              onClickDetail={this.onClickDetail}
              history={history}
            />
          ))
        }
      </div>
    );

    const receive = (
      <div className="div-receivetab">
        {/* <div className="mt-3">
          <DropDownOrder />
        </div> */}
        {
          _.map(dataToReceive, x => (
            <ItemOrderNew
              buttonBlocks={buttonBlocks}
              isReceive
              data={x}
              onClickDetail={this.onClickDetail}
              history={history}
            />
          ))
        }
      </div>
    );

    const subTab = indexTabs === 0 ? allTab : indexTabs === 1 ? shipTab : indexTabs === 2 ? receive : indexTabs === 3 ? boutiqueTab : allTab;
    const orderPage = (
      <div className="div-my-order-new animated faster fadeInLeft">
        <h3>
          {myOrder}
        </h3>
        <div className="div-button-tab">
          <div
            className={`div-tab ${indexTabs === 0 ? 'active' : ''}`}
            onClick={() => this.setState({ indexTabs: 0 })}
          >
            <button
              type="button"
            >
              {isMobile ? allBt : `${allBt} (${dataMyOrder.length})`}
              <div className="number-item">
                <span>{dataMyOrder.length}</span>
              </div>
            </button>
          </div>
          <div
            className={`div-tab ${indexTabs === 1 ? 'active' : ''}`}
            onClick={() => this.setState({ indexTabs: 1 })}
          >
            <button
              type="button"
            >
              {isMobile ? toShipBt : `${toShipBt} (${dataToShip.length})`}
              <div className="number-item">
                <span>{dataToShip.length}</span>
              </div>
            </button>
          </div>
          <div
            className={`div-tab ${indexTabs === 2 ? 'active' : ''}`}
            onClick={() => this.setState({ indexTabs: 2 })}
          >
            <button
              type="button"
            >
              {isMobile ? toReceiveBt : `${toReceiveBt} (${dataToReceive.length})`}
              <div className="number-item">
                <span>{dataToReceive.length}</span>
              </div>
            </button>
          </div>
          <div
            className={`div-tab ${indexTabs === 3 ? 'active' : ''}`}
            onClick={() => this.setState({ indexTabs: 3 })}
          >
            <button
              type="button"
            >
              {isMobile ? boutiqueBt : `${boutiqueBt} (${dataToBoutique.length})`}
              <div className="number-item">
                <span>{dataToBoutique.length}</span>
              </div>
            </button>
          </div>
        </div>
        {
          subTab
        }
      </div>
    );
    return (
      <div>
        { dataDetail
          ? (
            isMobile ? (
              isDetailTracking ? (
                <OrderTrackingMobile
                  data={this.state.dataDetail}
                  buttonBlocks={buttonBlocks}
                  onClickBackTracking={this.onClickBackTracking}
                />
              ) : (
                <OrderDetailNewMobile
                  data={this.state.dataDetail}
                  login={login}
                  history={history}
                  basket={basket}
                  onClickBack={this.onClickBack}
                  buttonBlocks={buttonBlocks}
                  onClickTracking={this.onClickTracking}
                  isBackTracking={isBackTracking}
                />
              )
            ) : (
              <OrderDetailNew
                data={this.state.dataDetail}
                login={login}
                history={history}
                basket={basket}
                onClickBack={this.onClickBack}
                buttonBlocks={buttonBlocks}
              />
            )
          ) : (orderPage)
          }
      </div>
      // <div className={isMobile ? 'div-col justify-center items-center div-my-order' : 'div-col div-my-order'}>
      //   <span className={isMobile ? 'header-account mb-3' : 'header-account mb-5'}>
      //     {myOrder.charAt(0).toUpperCase() + myOrder.slice(1)}
      //   </span>
      //   {
      //     data.length !== 0
      //       ? (
      //         <div className="div-col w-100">
      //           {
      //         _.map(data, d => (
      //           <MyOrderItem
      //             data={d}
      //             login={login}
      //             history={history}
      //             basket={basket}
      //             createBasketGuest={this.props.createBasketGuest}
      //             addProductBasket={this.props.addProductBasket}
      //             cmsCommon={this.props.cmsCommon}
      //             loadingPage={loadingPage}
      //           />
      //         ))
      //       }
      //           <div className={isShowLoadMore ? 'div-row justify-center items-center' : 'hiden'}>
      //             <button
      //               type="button"
      //               className="button-bg__none"
      //               style={{
      //                 marginTop: '20px',
      //                 marginBottom: '60px',
      //               }}
      //               onClick={() => { this.setState({ isLoadMore: true }); }}
      //             >
      //               <div className="div-row">
      //                 <img src={random} alt="random" className="icon-size" />
      //                 <span className="ml-2" style={{ fontSize: '1.1rem' }}>
      //                   {loadMore}
      //                 </span>
      //               </div>
      //             </button>
      //           </div>
      //           <div className="mb-4" />
      //         </div>
      //       ) : (
      //         <div className="no-product-overview">
      //           <span className="mt-5 mb-5">
      //             {subtitle}
      //           </span>
      //           <div>
      //             <button
      //               type="button"
      //               className="mt-5 mb-5"
      //               style={{
      //                 background: 'transparent',
      //                 border: '1px solid gray',
      //                 borderRadius: '10px',
      //                 padding: '10px 40px 10px 40px',
      //               }}
      //               onClick={this.gotoLanding}
      //             >
      //               {findMyScent}
      //             </button>
      //           </div>
      //         </div>
      //       )
      //   }
      // </div>
    );
  }
}

MyOrder.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  basket: PropTypes.shape.isRequired,
  cmsCommon: PropTypes.arrayOf().isRequired,
  cms: PropTypes.arrayOf().isRequired,
  addCmsRedux: PropTypes.arrayOf().isRequired,
};

export default withRouter(MyOrder);
