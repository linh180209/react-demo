import React, { Component } from 'react';
import icBack from '../../../image/icon/back-order-mobile.svg';
import ItemOrderNew from './itemOrderNew';
import icTracking from '../../../image/icon/icTracking.svg';
import { getNameFromButtonBlock, scrollToTargetAdjusted } from '../../../Redux/Helpers';

class OerderDetailNewMobile extends Component {
  componentDidMount() {
    setTimeout(() => {
      scrollToTargetAdjusted('div-order-detail-new-mobile');
    }, 200);
  }

  render() {
    const { buttonBlocks, data, isBackTracking } = this.props;
    const {
      id, total, date_created: dataCreate, line_items: lineItems,
      subtotal, shipping, shipping_address: shippingAddress, discount, currency,
      status, trackings,
    } = this.props.data;

    const orderDetailsBt = getNameFromButtonBlock(buttonBlocks, 'order_details');
    const shippingAddressBt = getNameFromButtonBlock(buttonBlocks, 'shipping_address');
    const reviewBt = getNameFromButtonBlock(buttonBlocks, 'review');
    const subTotalBt = getNameFromButtonBlock(buttonBlocks, 'Subtotal');
    const shippingBt = getNameFromButtonBlock(buttonBlocks, 'Shipping');
    const discountBt = getNameFromButtonBlock(buttonBlocks, 'Discount');
    const totalBt = getNameFromButtonBlock(buttonBlocks, 'Total');
    const billedMonthlyBt = getNameFromButtonBlock(buttonBlocks, 'Billed_Monthly');
    const trackingPackageBt = getNameFromButtonBlock(buttonBlocks, 'Track_Package');
    const returnBt = getNameFromButtonBlock(buttonBlocks, 'return/refund');
    return (
      <div className={`div-order-detail-new-mobile animated faster ${isBackTracking ? 'fadeInLeft' : 'fadeInRight'}`}>
        <div id="div-order-detail-new-mobile" className="anchor-scroll" />
        <button type="button" className="bt-back" onClick={this.props.onClickBack}>
          <img loading="lazy" src={icBack} alt="back" />
        </button>
        <h3>
          {orderDetailsBt}
        </h3>
        <div className="div-address">
          <h4>
            {shippingAddressBt}
          </h4>
          <span>
            {shippingAddress ? shippingAddress.name : ''}
          </span>
          <span>
            {shippingAddress ? shippingAddress.phone : ''}
          </span>
          <span className="address-title">
            HOME
          </span>
          <span className="address">
            {shippingAddress ? `${shippingAddress.street1} ${shippingAddress.country.name} ${shippingAddress.postal_code}` : ''}
          </span>
        </div>
        <hr />
        <div className="div-items">
          <ItemOrderNew
            buttonBlocks={buttonBlocks}
            isDetail
            data={data}
            history={this.props.history}
          />
          {/* <div className="div-list-button">
            <button type="button">
              {returnBt}
            </button>
            <button type="button">
              {reviewBt}
            </button>
          </div> */}
        </div>
        <hr />
        <div className="div-tracking">
          <button type="button" onClick={this.props.onClickTracking}>
            <img loading="lazy" src={icTracking} alt="tracking" />
            {trackingPackageBt}
          </button>
        </div>
        <hr />
        <div className="div-total">
          <div className="div-total-item">
            <span>{subTotalBt}</span>
            <span>
              {currency ? currency.symbol : ''}
              {subtotal}
            </span>
          </div>
          <div className="div-total-item">
            <span>{shippingBt}</span>
            <span>
              {currency ? currency.symbol : ''}
              {shipping}
            </span>
          </div>
          {/* <div className="div-total-item">
            <span>GST</span>
            <span>$21</span>
          </div> */}
          <div className="div-total-item" style={{ margin: '0px' }}>
            <span>{discountBt}</span>
            <span>
              {currency ? currency.symbol : ''}
              {discount}
            </span>
          </div>
          <hr />
          <div className="div-total-sum">
            <span>
              {totalBt}
            </span>
            <div className="div-price">
              <span>
                <b>
                  {currency ? currency.symbol : ''}
                  {total}
                </b>

              </span>
              {/* <span>{billedMonthlyBt}</span> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default OerderDetailNewMobile;
