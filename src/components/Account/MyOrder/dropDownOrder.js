import React, { Component } from 'react';
import {
  Dropdown, DropdownMenu, DropdownToggle,
} from 'reactstrap';
import _ from 'lodash';
import icDown from '../../../image/icon/ic-down-order.svg';

class DropDownOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
    };
    this.options = [
      'Last 5 orders',
      'Last 10 orders',
      'Last 100 orders',
    ];
  }

  toggle = () => {
    const { dropdownOpen } = this.state;
    this.setState({
      dropdownOpen: !dropdownOpen,
    });
  }

  render() {
    return (
      <Dropdown
        isOpen={this.state.dropdownOpen}
        toggle={this.toggle}
        className="dropdown-my-order"
      >
        <DropdownToggle
          tag="span"
          onClick={this.toggle}
          data-toggle="dropdown"
          aria-expanded={this.state.dropdownOpen}
        >
          <div className="div-display-order">
            <span>
              Last 5 orders
            </span>
            <img loading="lazy" src={icDown} alt="icDown" />
          </div>
        </DropdownToggle>
        <DropdownMenu>
          {
            _.map(this.options, d => (
              <div
                onClick={() => {
                  console.log('d', d);
                }}
              >
                <div className="drop-down-item-order">
                  <span className="ml-3">
                    {d}
                  </span>
                </div>
              </div>
            ))
          }
        </DropdownMenu>
      </Dropdown>
    );
  }
}

export default DropDownOrder;
