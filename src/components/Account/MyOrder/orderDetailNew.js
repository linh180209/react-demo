import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import ProcessOrder from './processOrder';
import ItemOrderNew from './itemOrderNew';
import icBack from '../../../image/icon/back-order-mobile.svg';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';

class OrderDetailNew extends Component {
  formatDate = (date) => {
    if (!date) {
      return '';
    }
    if (moment(date).isValid) {
      return moment(date).format('DD MMM YYYY HH:mm:ss');
    }
    return '';
  };

  render() {
    const { buttonBlocks, data } = this.props;
    const {
      id, total, date_created: dataCreate, line_items: lineItems,
      subtotal, shipping, shipping_address: shippingAddress, discount, currency,
      status, trackings,
    } = this.props.data;
    const orderDetailsBt = getNameFromButtonBlock(buttonBlocks, 'order_details');
    const orderBt = getNameFromButtonBlock(buttonBlocks, 'Order');
    const placeOrderBt = getNameFromButtonBlock(buttonBlocks, 'Placed_on');
    const totalBt = getNameFromButtonBlock(buttonBlocks, 'Total');
    const shippingAddressBt = getNameFromButtonBlock(buttonBlocks, 'shipping_address');
    const totalSummaryBt = getNameFromButtonBlock(buttonBlocks, 'total_summary');
    const subTotalBt = getNameFromButtonBlock(buttonBlocks, 'Subtotal');
    const shippingBt = getNameFromButtonBlock(buttonBlocks, 'Shipping');
    const discountBt = getNameFromButtonBlock(buttonBlocks, 'Discount');
    const billedMonthlyBt = getNameFromButtonBlock(buttonBlocks, 'Billed_Monthly');
    return (
      <div className="div-order-detail-new animated faster fadeInRight">
        <div className="div-header-order">
          <button className="bt-back" type="button" onClick={this.props.onClickBack}>
            <img loading="lazy" src={icBack} alt="icon" />
          </button>
          <h3>
            {orderDetailsBt}
          </h3>
        </div>
        <div className="div-header">
          <div className="div-left">
            <div className="div-order">
              {orderBt}
              <Link to="#" className="ml-2">
                {id}
              </Link>
            </div>
            <span>
              {`${placeOrderBt} ${this.formatDate(dataCreate)}`}
            </span>
          </div>
          <div className="div-right">
            <span className="text-name">
              {`${totalBt}: `}
              <b>
                {currency ? currency.symbol : ''}
                {total}
              </b>
            </span>
          </div>
        </div>
        <ProcessOrder
          buttonBlocks={buttonBlocks}
          status={status}
          trackings={trackings}
        />
        <ItemOrderNew
          buttonBlocks={buttonBlocks}
          isDetail
          data={data}
          history={this.props.history}
        />
        <div className="div-info">
          <div className="div-address">
            <h3>
              {shippingAddressBt}
            </h3>
            <span className="name">
              {shippingAddress ? shippingAddress.name : ''}
            </span>
            <div className="address-text">
              <span className="address-type">
                Home
              </span>
              <div className="address">
                <span>
                  {shippingAddress ? `${shippingAddress.street1} ${shippingAddress.country.name} ${shippingAddress.postal_code}` : ''}
                </span>
                <span>
                  {shippingAddress ? shippingAddress.shippingAddress : ''}
                </span>
              </div>
            </div>
          </div>
          <div className="div-total">
            <h3>{totalSummaryBt}</h3>
            <div className="div-total-item">
              <span>{subTotalBt}</span>
              <span>
                {currency ? currency.symbol : ''}
                {subtotal}
              </span>
            </div>
            <div className="div-total-item">
              <span>{shippingBt}</span>
              <span>
                {currency ? currency.symbol : ''}
                {shipping}
              </span>
            </div>
            {/* <div className="div-total-item">
              <span>GST</span>
              <span>$21</span>
            </div> */}
            <div className="div-total-item">
              <span>{discountBt}</span>
              <span>
                {currency ? currency.symbol : ''}
                {discount}
              </span>
            </div>
            <hr />
            <div className="div-total-sum">
              <span>
                {totalBt}
              </span>
              <div className="div-price">
                <span>
                  <b>
                    {currency ? currency.symbol : ''}
                    {total}
                  </b>

                </span>
                {/* <span>{billedMonthlyBt}</span> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default OrderDetailNew;
