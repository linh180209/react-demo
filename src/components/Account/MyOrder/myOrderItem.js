import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import moment from 'moment';
import _ from 'lodash';
import ScentItemOverlap from '../../ScentItem/scentItemOverLap';
import {
  getImageDisplay, getUrlGotoProductOderPage, getAltImage, getNameFromCommon, generaCurrency, generateUrlWeb,
} from '../../../Redux/Helpers/index';
import auth from '../../../Redux/Helpers/auth';
import { GET_ORDER_URL } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import OrderDetail from './orderDetail';
import { isMobile, isTablet } from '../../../DetectScreen';

class MyOrderItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowDetail: false,
      dataOrder: {},
    };
  }

  onClickDetail = () => {
    this.fetchDataDetail();
  }

  fetchDataDetail = () => {
    this.props.loadingPage(true);
    const { id } = this.props.data;
    const options = {
      url: `${GET_ORDER_URL.replace('{id}', id)}`,
      method: 'GET',
    };
    fetchClient(options, true).then((result) => {
      this.setState({ dataOrder: result, isShowDetail: true });
      this.props.loadingPage(false);
    }).catch((err) => {
      toastrError(err);
      this.props.loadingPage(false);
    });
  }

  onClickRightButton = (d) => {
    const { perfume } = d;
    this.onClickAddtoCart(perfume);
  }

  onClickLeftButton = (d) => {
    this.onClickImage(d);
  }

  onClickImage = (d) => {
    const { perfume } = d;
    let urlPro = getUrlGotoProductOderPage(perfume).url;
    const { meta } = perfume;
    if (meta && meta.url) {
      urlPro = meta.url;
    }
    if (urlPro) {
      this.props.history.push(generateUrlWeb(urlPro));
    }
  }

  onClickAddtoCart = async (perfume) => {
    const { basket } = this.props;
    const { item, combo } = perfume;
    const { id, name, price } = item;
    const images = getImageDisplay(perfume);
    if (item) {
      const data = {
        item: id,
        name,
        price,
        is_featured: item.is_featured,
        quantity: 1,
        total: parseFloat(price) * 1,
        image_urls: [images],
        combo,
      };
      const dataTemp = {
        idCart: basket.id,
        item: data,
      };
      if (!basket.id) {
        this.props.createBasketGuest(dataTemp);
      } else {
        this.props.addProductBasket(dataTemp);
      }
    }
  }

  formatDate = (date) => {
    if (!date) {
      return '';
    }
    if (moment(date).isValid) {
      return moment(date).format('DD MMM YYYY');
    }
    return '';
  };

  render() {
    const {
      id, status, total, date_created: dataCreate, date_shipped: dataShipped, line_items: lineItems,
    } = this.props.data;

    const { cmsCommon } = this.props;
    const { isShowDetail, dataOrder } = this.state;
    const urls = _.map(lineItems, x => x.image_display);
    const totalBt = getNameFromCommon(cmsCommon, 'TOTAL').toUpperCase();
    const orderNumberBt = getNameFromCommon(cmsCommon, 'ORDER_NUMBER');
    const dateBt = getNameFromCommon(cmsCommon, 'DATE');
    const statusBt = getNameFromCommon(cmsCommon, 'STATUS');
    const trackBt = getNameFromCommon(cmsCommon, 'TRACK_ORDER');
    const shippedBt = getNameFromCommon(cmsCommon, 'Shipped_date');

    const htmlMobile = (
      <button
        type="button"
        className="div-row pa0"
        style={{ border: '1px solid #CECECE', borderRadius: '14px', background: isShowDetail ? '#f4f4f4' : '#ffffff' }}
        onClick={this.onClickDetail}
      >
        <div className="div-col w-25">
          <img
            loading="lazy"
            src={urls && urls.length > 0 ? urls[0] : ''}
            alt={getAltImage(urls && urls.length > 0 ? urls[0] : undefined)}
            style={{
              width: '100%', height: '65px', borderTopLeftRadius: '14px', objectFit: 'cover',
            }}
          />
          <div style={{ position: 'relative', width: '100%', height: '65px' }}>
            <img
              loading="lazy"
              src={urls && urls.length > 1 ? urls[1] : ''}
              alt={getAltImage(urls && urls.length > 1 ? urls[1] : undefined)}
              style={{
                width: '100%',
                height: '100%',
                borderBottomLeftRadius: '14px',
                position: 'absolute',
                objectFit: 'cover',
                top: '0px',
                left: '0px',
              }}
            />
          </div>
        </div>
        <div className="div-col justify-around pl-2" style={{ height: '130px' }}>
          <div className="div-row">
            <span className="title-myorder-gray">
              {orderNumberBt}
              :
            </span>
            <span className="value-myorder-black ml-2">
              {id}
            </span>
          </div>

          <div className="div-row">
            <span className="title-myorder-gray">
              {dateBt}
              :
            </span>
            <span className="value-myorder-gray ml-2">
              {this.formatDate(dataCreate)}
            </span>
          </div>

          <div className="div-row">
            <span className="title-myorder-gray">
              {totalBt}
              :
            </span>
            <span className="value-myorder-gray ml-2">
              {generaCurrency(total)}
            </span>
          </div>

          <div className="div-row">
            <span className="title-myorder-gray">
              {statusBt}
              :
            </span>
            <span className="value-myorder-gray ml-2">
              {status}
            </span>
          </div>
        </div>
      </button>
    );
    const htmlWeb = (
      <button
        type="button"
        className="div-row pt-3 pr-2 pb-3 pl-2 br4 mt-4 items-center w-100"
        style={{
          border: '1px solid #CECECE',
          background: isShowDetail ? '#f4f4f4' : '#ffffff',
        }}
        onClick={this.onClickDetail}
      >
        <div className="div-col justify-around items-start" style={{ width: '18%' }}>
          <span className="title-myorder-gray">
            {orderNumberBt}
            :
          </span>
          <span className="value-myorder-black">
            {id}
          </span>
        </div>
        <div className="div-col justify-around items-start" style={{ width: '10%' }}>
          <span className="title-myorder-gray">
            {dateBt}
            :
          </span>
          <span className="value-myorder-gray">
            {this.formatDate(dataCreate)}
          </span>
        </div>
        <div className="div-row justify-center items-center" style={{ width: '23%' }}>
          <img
            loading="lazy"
            src={urls && urls.length > 0 ? urls[0] : ''}
            alt={getAltImage(urls && urls.length > 0 ? urls[0] : undefined)}
            style={{
              width: '65px', height: '65px', borderRadius: '14px', marginRight: '5px', objectFit: 'cover',
            }}
          />
          <div style={{ position: 'relative', width: '65px', height: '65px' }}>
            <img
              loading="lazy"
              src={urls && urls.length > 1 ? urls[1] : ''}
              alt={getAltImage(urls && urls.length > 1 ? urls[1] : undefined)}
              style={{
                width: '65px',
                height: '65px',
                borderRadius: '14px',
                position: 'absolute',
                marginLeft: '5px',
                objectFit: 'cover',
                top: '0px',
                left: '0px',
              }}
            />
          </div>
        </div>
        <div className="div-row justify-around items-start" style={{ width: '50%' }}>
          <div className="div-col justify-around">
            <span className="title-myorder-gray">
              {totalBt}
              :
            </span>
            <span className="value-myorder-gray">
              {generaCurrency(total)}
            </span>
          </div>
          <div className="div-col justify-around">
            <span className="title-myorder-gray">
              {statusBt}
              :
            </span>
            <span className="value-myorder-gray">
              {status}
            </span>
          </div>
          <div className={dataShipped ? 'div-col justify-around items-start' : 'hiden'}>
            <span className="title-myorder-black">
              {trackBt}
              :
            </span>
            <span className="value-myorder-gray items-start">
              {shippedBt}
              :
              {' '}
              {this.formatDate(dataShipped)}
            </span>
          </div>
        </div>
      </button>
    );
    return (
      <div>
        <div>
          {
          isMobile && !isTablet ? (
            htmlMobile
          ) : (
            htmlWeb
          )
        }
        </div>
        <div>
          {isShowDetail ? (
            <OrderDetail
              data={dataOrder}
              onClickRightButton={this.onClickRightButton}
              onClickImage={this.onClickImage}
              onClickLeftButton={this.onClickLeftButton}
              cmsCommon={cmsCommon}
            />
          ) : (<div />)}
        </div>
      </div>
    );
  }
}

MyOrderItem.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string,
    line_items: PropTypes.arrayOf(PropTypes.object),
    status: PropTypes.string,
    total: PropTypes.string,
    date_created: PropTypes.string,
    date_modified: PropTypes.string,
    date_shipped: PropTypes.string,
    date_delivery: PropTypes.string,
    subtotal: PropTypes.string,
    shipping: PropTypes.string,
    shipping_address: PropTypes.shape(PropTypes.object),
    discount: PropTypes.string,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  basket: PropTypes.shape.isRequired,
  cmsCommon: PropTypes.arrayOf().isRequired,
};

export default MyOrderItem;
