import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import moment from 'moment';
import _ from 'lodash';

import ScentItemOverlap from '../../ScentItem/scentItemOverLap';
import {
  getNameFromCommon,
} from '../../../Redux/Helpers/index';
import { isMobile, isTablet } from '../../../DetectScreen';

class OrderDetail extends Component {
  formatDate = (date) => {
    if (!date) {
      return '';
    }
    if (moment(date).isValid) {
      return moment(date).format('DD MMM YYYY');
    }
    return '';
  };

  filterAllImage = (lineItems) => {
    const addToCartBt = getNameFromCommon(this.props.cmsCommon, 'ADD_TO_CART').toUpperCase();
    const viewProduct = getNameFromCommon(this.props.cmsCommon, 'VIEW_PRODUCT');
    const itemProducts = [];
    _.forEach(lineItems, (p) => {
      const {
        item, name,
      } = p;
      const { product } = item;
      if (product.type === 'Perfume') {
        const productData = {};
        const {
          price, image, image_display: imageDisplay,
        } = p;
        _.assign(productData, {
          name,
          price,
          description: `Bottle customisation:${image ? 'Yes' : 'No'}`,
          perfume: p,
          buttonName: `+ ${addToCartBt}`,
          image,
          buttonNameLeft: viewProduct,
          isSampleProduct: true,
        });
        const imgUrls = [imageDisplay];
        productData.image_urls = imgUrls;
        itemProducts.push(productData);
      } else if (product.type === 'Wax_Perfume' || product.type === 'Elixir' || product.type === 'Scent' || product.type === 'Kit') {
        const { image_display: imageDisplay } = p;
        const productData = {};
        const images = imageDisplay;
        const {
          id, price,
        } = item;
        _.assign(productData, {
          name,
          price,
          perfume: p,
          buttonName: `+ ${addToCartBt}`,
          id,
          image_urls: [images],
          isSampleProduct: true,
          buttonNameLeft: product.type !== 'Scent' ? viewProduct : undefined,
        });
        itemProducts.push(productData);
      }
    });
    return itemProducts;
  }

  render() {
    const {
      data, onClickRightButton, onClickImage, onClickLeftButton,
    } = this.props;
    if (!data) {
      return null;
    }
    const {
      id, total, date_created: dataCreate, line_items: lineItems,
      subtotal, shipping, shipping_address: shippingAddress, discount, currency,
    } = this.props.data;
    const {
      city, country, phone, postal_code: postalCode, street1, email,
    } = shippingAddress || {
      city: '', country: { name: '' }, phone: '', postal_code: '', street1: '', email: '',
    };
    const { cmsCommon } = this.props;
    const itemProducts = this.filterAllImage(lineItems);
    const styleTitleDetail = {
      fontSize: '1.2rem',
      color: 'black',
      fontWeight: '600',
    };
    const styleLine = {
      width: '100%',
      height: '1px',
      background: '#cecece',
      marginTop: '10px',
    };
    const orderDetailBt = getNameFromCommon(cmsCommon, 'ORDER_DETAILS');
    const thankBt = getNameFromCommon(cmsCommon, 'Thanks_for_you_order');
    const wesentBt = getNameFromCommon(cmsCommon, 'WE_HAVE_SENT_IT');
    const orderSentBt = getNameFromCommon(cmsCommon, 'Your_order_has_been_sent');
    const orderNoBt = getNameFromCommon(cmsCommon, 'ORDER_NO');
    const orderDateBt = getNameFromCommon(cmsCommon, 'ORDER_DATE');
    const orderTotalBt = getNameFromCommon(cmsCommon, 'ORDER_TOTAL');
    const subTotalBt = getNameFromCommon(cmsCommon, 'SUB-TOTAL');
    const discountBt = getNameFromCommon(cmsCommon, 'DISCOUNT');
    const deliveryBt = getNameFromCommon(cmsCommon, 'DELIVERY');
    const totalBt = getNameFromCommon(cmsCommon, 'TOTAL').toUpperCase();
    const deliveryDetailsBt = getNameFromCommon(cmsCommon, 'DELIVERY_DETAILS');
    const deliveryAddressBt = getNameFromCommon(cmsCommon, 'DELIVERY_ADDRESS');

    return (
      <div
        className="div-col w-100 mt-3"
        style={isMobile && !isTablet ? {} : {
          padding: '20px 10px 20px 10px',
          border: '1px solid #CECECE',
          borderRadius: '14px',
          background: '#f4f4f4',
        }}
      >
        <div className="div-col">
          <span style={styleTitleDetail}>
            {orderDetailBt}
          </span>
          <span>
            {thankBt}
          </span>
        </div>
        <div className="div-col mt-4">
          <span style={styleTitleDetail}>
            {wesentBt}
          </span>
          <span>
            {orderSentBt}
          </span>
        </div>
        <div className="div-col mt-4">
          <div className={isMobile && !isTablet ? 'div-col mb-3' : 'div-row mb-3'}>
            <span className={isMobile && !isTablet ? 'w-100' : 'w-50'}>
              <strong>
                {orderNoBt}
                :
              </strong>
              {' '}
              {id}
            </span>
            <span className={isMobile && !isTablet ? 'w-100' : 'w-50'}>
              <strong>
                {orderDateBt}
                :
              </strong>
              {' '}
              {this.formatDate(dataCreate)}
            </span>
          </div>
          <Row className="w-100">
            {
              _.map(itemProducts, d => (
                <Col className="mt-2" style={isMobile && !isTablet ? { paddingLeft: '0px', paddingRight: '0px' } : {}} md={isMobile ? '6' : '4'} xs="6">
                  <ScentItemOverlap
                    data={d}
                    isMyOrder
                    onClickImage={onClickImage}
                    onClickRightButton={onClickRightButton}
                    onClickLeftButton={onClickLeftButton}
                  />
                </Col>
              ))
            }
          </Row>
        </div>
        <div className="div-col mt-4 mb-3">
          <span style={styleTitleDetail}>
            {orderTotalBt}
          </span>
          <div style={styleLine} />
          <div className="div-row justify-between mt-3">
            <span>
              <strong>
                {subTotalBt}
                :
              </strong>
            </span>
            <span>
              {currency ? currency.symbol : ''}
              {subtotal}
            </span>
          </div>
          <div className="div-row justify-between mt-3">
            <span>
              <strong>
                {discountBt}
                :
              </strong>
            </span>
            <span>
              {currency ? currency.symbol : ''}
              {discount}
            </span>
          </div>
          <div className="div-row justify-between mt-3">
            <span>
              <strong>
                {deliveryBt}
                :
              </strong>
            </span>
            <span>
              {currency ? currency.symbol : ''}
              {shipping}
            </span>
          </div>
          <div style={styleLine} />
          <div className="div-row justify-between mt-3">
            <span>
              <strong>
                {totalBt}
                :
              </strong>
            </span>
            <span>
              <strong>
                {currency ? currency.symbol : ''}
                {total}
              </strong>
            </span>
          </div>
        </div>
        <div className="div-col mt-4">
          <span style={styleTitleDetail}>
            {deliveryDetailsBt}
          </span>
          <div style={styleLine} />
          <span
            className="mt-2 mb-2"
            style={{
              fontWeight: '600',
            }}
          >
            {deliveryAddressBt}
            :
          </span>
          <div>
            <Row>
              <Col md="6">
                <span>
                  {street1}
                </span>
              </Col>
              <Col md="6">
                <span>
                  {city}
                </span>
              </Col>
              <Col md="6">
                <span>
                  {postalCode}
                </span>
              </Col>
              <Col md="6">
                <span>
                  {country ? country.name : ''}
                </span>
              </Col>
              <Col md="6">
                <span>
                  {phone}
                </span>
              </Col>
              <Col md="6">
                <span>
                  {email}
                </span>
              </Col>
            </Row>
          </div>
        </div>
      </div>
    );
  }
}

OrderDetail.propTypes = {
  data: PropTypes.shape().isRequired,
  onClickRightButton: PropTypes.func.isRequired,
  onClickImage: PropTypes.func.isRequired,
  onClickLeftButton: PropTypes.func.isRequired,
  cmsCommon: PropTypes.shape().isRequired,
};

export default OrderDetail;
