import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import icCheck from '../../../image/icon/checkOrder.svg';
import { getNameFromButtonBlock, getLinkFromButtonBlock } from '../../../Redux/Helpers';
import { isBrowser, isMobile } from '../../../DetectScreen';

const listProcessStatus = ['paid', 'processed', 'shipped', 'completed'];
class ProcessOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      indexSelect: undefined,
      heightLine: 0,
    };
  }

  componentDidMount() {
    this.calcHeightLine();
  }

  componentDidUpdate() {
    this.calcHeightLine();
  }

  calcHeightLine = () => {
    let heightLine = 0;
    const ele = document.getElementById('item-3');
    const eleProcess = document.getElementById('id-process');
    if (ele && eleProcess) {
      heightLine = eleProcess.clientHeight - 10 - 40;
      heightLine -= ele.clientHeight;
    }
    if (heightLine !== this.state.heightLine) {
      this.setState({ heightLine });
    }
  }

  onMouseEnter = (index) => {
    if (isMobile) {
      return;
    }
    this.setState({ indexSelect: index });
  }

  onMouseLeave = (index) => {
    if (isMobile) {
      return;
    }
    this.setState({ indexSelect: undefined });
  }

  getProcess = (status) => {
    const index = _.findIndex(listProcessStatus, x => x === status);
    if (index === -1 && status === 'on_hold') {
      return 0;
    }
    return index;
  }

  getDateTracking = (trackings, status) => {
    const ele = _.find(trackings, x => x.new === status);
    if (ele) {
      return moment(ele).format('DD MMM YYYY - mm:ss');
    }
    return '';
  }

  render() {
    const { indexSelect, heightLine } = this.state;
    const { buttonBlocks, status, trackings } = this.props;
    const processStatus = this.getProcess(status);
    const orderReceiveBt = getNameFromButtonBlock(buttonBlocks, 'order_received');
    const linkOrderReceiveBt = getLinkFromButtonBlock(buttonBlocks, 'order_received');
    const processingBt = getNameFromButtonBlock(buttonBlocks, 'processing');
    const linkProcessingbt = getLinkFromButtonBlock(buttonBlocks, 'processing');
    const shippedBt = getNameFromButtonBlock(buttonBlocks, 'shipped');
    const linkShippeBt = getLinkFromButtonBlock(buttonBlocks, 'shipped');
    const deliveredBt = getNameFromButtonBlock(buttonBlocks, 'delivered');
    const linkDelivereBt = getLinkFromButtonBlock(buttonBlocks, 'delivered');

    const dataName = [
      { name: orderReceiveBt, text: linkOrderReceiveBt },
      { name: processingBt, text: linkProcessingbt },
      { name: shippedBt, text: linkShippeBt },
      { name: deliveredBt, text: linkDelivereBt }];
    // let heightLine = 0;
    // const ele = document.getElementById('item-3');
    // const eleProcess = document.getElementById('id-process');
    // if (ele && eleProcess) {
    //   heightLine = eleProcess.clientHeight - 10 - 40;
    //   heightLine -= ele.clientHeight;
    // }
    return (
      <div className="wrap-process-order">
        <div className="div-process-order" id="id-process">
          <div className="line-bg" style={isMobile ? { height: `${heightLine}px` } : {}} />
          {
          _.map(dataName, (x, index) => (
            <div
              id={`item-${index}`}
              className="item-order"
              onMouseEnter={() => { if (index <= processStatus) this.onMouseEnter(index); }}
              onMouseLeave={() => { if (index <= processStatus) this.onMouseLeave(index); }}
            >
              <div className={`circle-point ${index <= processStatus ? 'active' : ''}`}>
                <img loading="lazy" src={icCheck} alt="checked" />
              </div>
              {
                isBrowser ? (
                  <span>
                    {x.name}
                  </span>
                ) : (
                  <div className="div-text-mobile">
                    <div className="div-left">
                      <h4 style={this.getDateTracking(trackings, listProcessStatus[index]) ? {} : { marginTop: '10px' }}>
                        {x.name}
                      </h4>
                      <span>
                        {this.getDateTracking(trackings, listProcessStatus[index])}
                      </span>
                    </div>
                    <div className="div-right">
                      <span>
                        {x.text}
                      </span>
                    </div>
                  </div>
                )
              }
            </div>
          ))
        }
        </div>
        <div className={indexSelect !== undefined && isBrowser ? 'div-note' : 'hidden'}>
          <div className="wrap-arrow">
            <div className="arrow-up" style={indexSelect !== undefined && isBrowser ? { left: `${indexSelect * 25.5 + 10.5}%` } : {}} />
          </div>
          <span className="div-date">
            {indexSelect !== undefined ? this.getDateTracking(trackings, listProcessStatus[indexSelect]) : ''}
          </span>
          <span className="div-text">
            {indexSelect !== undefined ? dataName[indexSelect].text : ''}
          </span>
        </div>
      </div>

    );
  }
}

export default ProcessOrder;
