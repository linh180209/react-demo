import React, { Component } from 'react';
import { Modal, ModalBody } from 'reactstrap';
import _ from 'lodash';
import moment from 'moment';
import classnames from 'classnames';
import '../../../styles/modal-payment.scss';
import icClose from '../../../image/icon/ic-close-region.svg';
import PayMentInfo from '../PayMentInfo/cardPayment';
import ItemPayment from '../PayMentInfo/itemPayment';
import InputPayMent from '../PayMentInfo/inputPayment';
import { getNameFromButtonBlock, addFontCustom } from '../../../Redux/Helpers';
import icVisa from '../../../image/icon/visa.svg';
import icMaster from '../../../image/icon/master.svg';
import { GET_STRIP_CARDS } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrSuccess, toastrError } from '../../../Redux/Helpers/notification';
import { isBrowser, isMobile } from '../../../DetectScreen';

class ModalChangePayment extends Component {
  constructor(props) {
    super(props);
    this.cardInfo = {
      name: '',
      number: '',
      expMonth: '',
      cvc: '',
    };
    this.state = {
      listError: [],
      idDefault: props.idDefault,
    };
  }

  onChangeDefault = (id) => {
    this.setState({ idDefault: id });
  };

  onClickConfirm = () => {
    const { idDefault } = this.state;
    if (idDefault) {
      this.updateDefault(idDefault);
      return;
    }
    const listError = this.onCheckInfo();
    if (listError.length > 0) {
      this.setState({ listError });
      return;
    }
    const {
      name, number, expMonth, cvc,
    } = this.cardInfo;
    const { month, year } = this.getMonthYear(expMonth);
    const body = {
      name,
      number,
      exp_month: month,
      exp_year: year,
      cvc,
      is_default: true,
    };
    this.postAddCard(body);
  }

  getMonthYear = (data) => {
    const exps = data.split('/');
    if (!exps || exps.length !== 2) {
      return { month: 0, year: 0 };
    }
    const year = `${moment().format('YYYY').substring(0, 2)}${exps[1]}`;
    return { month: exps[0], year };
  }

  onCheckInfo = () => {
    const listError = [];
    const {
      name, number, cvc, expMonth,
    } = this.cardInfo;
    const { month, year } = this.getMonthYear(expMonth);
    if (!name) {
      listError.push('name');
    }
    if (!number || number.length !== 19) {
      listError.push('number');
    }
    if (!cvc || cvc.length !== 3) {
      listError.push('cvc');
    }
    if (!expMonth || expMonth.length !== 5 || parseInt(month, 10) > 12 || parseInt(year, 10) < parseInt(moment().format('YYYY'), 10)) {
      listError.push('expMonth');
    }
    return listError;
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.cardInfo[name] = value;
    if (this.state.listError.length > 0) {
      this.setState({ listError: [] });
    }
  }

  updateDefault = (id) => {
    this.props.loadingPage(true);
    const option = {
      url: `${GET_STRIP_CARDS}${id}`,
      method: 'PUT',
      body: {
        is_default: true,
      },
    };
    fetchClient(option, true).then((result) => {
      if (result && result.error !== -1 && !result.isError) {
        this.props.loadingPage(false);
        toastrSuccess('Update cart is success');
        this.props.updateStripCards();
        this.props.onClose();
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      this.props.loadingPage(false);
      toastrError(err.message);
    });
  }

  postAddCard = (body) => {
    const options = {
      url: GET_STRIP_CARDS,
      method: 'POST',
      body,
    };
    this.props.loadingPage(true);
    fetchClient(options, true).then((result) => {
      if (result && result.error !== -1 && !result.isError) {
        this.props.loadingPage(false);
        toastrSuccess('Add new cart is success');
        this.props.updateStripCards();
        this.props.onClose();
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      this.props.loadingPage(false);
      toastrError(err.message);
      if (err.message === 'Your card number is incorrect.') {
        this.setState({ listError: 'number' });
      }
    });
  }

  render() {
    const { onClose, buttonBlocks, stripCards } = this.props;
    const { listError, idDefault } = this.state;
    const changeBt = getNameFromButtonBlock(buttonBlocks, 'change_payment_method');
    const confirmBt = getNameFromButtonBlock(buttonBlocks, 'Confirm');
    const cancelBt = getNameFromButtonBlock(buttonBlocks, 'Cancel');
    return (
      <Modal className={classnames('modal-full-screen', addFontCustom())} isOpen>
        <ModalBody>
          <div className="div-modal-payment">
            <div className="div-content">
              <div className="div-content-scroll">
                <button
                  onClick={onClose}
                  className={isMobile ? 'hidden' : 'bt-close button-bg__none'}
                  type="button"
                >
                  <img loading="lazy" src={icClose} alt="close" />
                </button>
                <div className="div-content-payment">
                  {
                    isMobile ? (
                      <div className="div-header-mobile">
                        <h3>
                          {changeBt}
                        </h3>
                        <button
                          className="bt-close button-bg__none"
                          type="button"
                          onClick={onClose}
                        >
                          <img loading="lazy" src={icClose} alt="close" />
                        </button>
                      </div>
                    ) : (
                      <h3>
                        {changeBt}
                      </h3>
                    )
                  }

                  <div className="list-payment-save">
                    {
                      _.map(stripCards, d => (
                        <ItemPayment idDefault={idDefault} data={d} onChange={() => this.onChangeDefault(d.id)} />
                      ))
                    }
                  </div>
                  <div className="grid-payment">
                    <div className="item">
                      <PayMentInfo buttonBlocks={buttonBlocks} selected={idDefault === undefined} images={[icMaster, icVisa]} onChange={() => this.onChangeDefault(undefined)} />
                      <InputPayMent buttonBlocks={buttonBlocks} cardInfo={this.cardInfo} listError={listError} onChange={this.onChange} />
                    </div>
                  </div>
                </div>
                {isBrowser && <hr />}
                <button
                  onClick={this.onClickConfirm}
                  type="button"
                  className="bt-confirm"
                >
                  {confirmBt}
                </button>
                <button
                  onClick={onClose}
                  type="button"
                  className="bt-cancel button-bg__none"
                >
                  {cancelBt}
                </button>
              </div>
            </div>
          </div>
        </ModalBody>
      </Modal>
    );
  }
}

export default ModalChangePayment;
