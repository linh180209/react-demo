import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import moment from 'moment';
import timelineImg from '../../../views/Club21G/assets/timeline-image.png';
import timelineComingSoonImg from '../../../views/Club21G/assets/timeline-coming-soon.png';
import timelineBg from '../../../views/Club21G/assets/timeline-bg.png';
import { getAltImageV2, getNameFromButtonBlock } from '../../../Redux/Helpers';

class SubscriptionGoal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeClass: [true, true, true, true],
    };
  }

  toggleActive = (toggleIndex) => {
    const { activeClass } = this.state;
    const newActiveClass = activeClass.map((ele, index) => {
      if (index === toggleIndex) {
        return !ele;
      }
      return true;
    });
    this.setState({
      activeClass: newActiveClass,
    });
  }

  render() {
    const { activeClass } = this.state;
    const { buttonBlocks, subscriptionData } = this.props;
    const your = getNameFromButtonBlock(buttonBlocks, 'Your');
    const subscription = getNameFromButtonBlock(buttonBlocks, 'Subscription');
    const goals = getNameFromButtonBlock(buttonBlocks, 'Goals');
    const commingSoon = getNameFromButtonBlock(buttonBlocks, 'coming_soon');
    const dataItems = subscriptionData.slice(0, 4);
    const dataDisplays = _.map(dataItems, x => ({
      name: x.line_items[0].name,
      mounth: moment(x.date_created).format('MMM'),
      image: _.find(x.line_items[0].item.images, d => d.type === 'subscription'),
      imageHover: _.find(x.line_items[0].item.images, d => d.type === 'hover'),
    }));
    return (
      <div className="div-subscription-goal">
        <span className="title">
          {your}
          {' '}
          <b>{`2020 ${subscription}`}</b>
          {' '}
          {goals}
        </span>
        <div className="__content position-relative">
          <div className="__content-bar position-absolute" />
          <div className="__content-list">
            {
              _.map(dataDisplays, (x, index) => (
                <div className="item margin-space">
                  <div className="__image position-relative" onMouseLeave={() => { this.toggleActive(index); }} onMouseEnter={() => { this.toggleActive(index); }}>
                    <img loading="lazy" src={x.image ? x.image.image : ''} className={`front img-fluid position-absolute faster ${activeClass[index] === true ? 'fadeIn' : 'fadeOut'}`} alt={getAltImageV2(x.image)} />
                    <img loading="lazy" src={x.imageHover ? x.imageHover.image : ''} className="back img-fluid" alt={getAltImageV2(x.imageHover)} />
                  </div>
                  <div className="__milestone d-flex flex-column align-items-center">
                    <span className="__milestone-checkmark d-flex justify-content-center align-items-center active">
                      <i className="fa fa-check" />
                    </span>
                    <p className="__milestone-title">
                      {x.name}
                    </p>
                    <p className="__milestone-month">
                      {getNameFromButtonBlock(buttonBlocks, x.mounth)}
                    </p>
                  </div>
                </div>
              ))
            }
            <div className="item--coming">
              <div className="__image  d-flex justify-content-center align-items-center position-relative">
                <img loading="lazy" src={timelineComingSoonImg} className="img-fluid" alt="" />
              </div>
              <div className="__milestone d-flex flex-column align-items-center">
                <span className="__milestone-checkmark d-flex justify-content-center align-items-center">
                  <i className="fa fa-check" />
                </span>
                <p className="__milestone-title">
                  {commingSoon}
                </p>
                <p className="__milestone-month">
                  {getNameFromButtonBlock(buttonBlocks, moment().add(1, 'months').format('MMM'))}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

SubscriptionGoal.propTypes = {
  buttonBlocks: PropTypes.arrayOf().isRequired,
};

export default SubscriptionGoal;
