import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import PropTypes from 'prop-types';

import { generateUrlWeb, getNameFromButtonBlock, getSearchPathName } from '../../../Redux/Helpers';

class PersonalityQuiz extends Component {
  onClickButton = () => {
    const { pathname, search } = getSearchPathName(generateUrlWeb('/quiz'));
    const objectLinkBack = {
      pathname,
      search,
      state: { isRetake: true },
    };
    this.props.history.push(generateUrlWeb(objectLinkBack));
  }

  render() {
    const { buttonBlocks, personalityChoice } = this.props;
    const dataPersionality = { image: (personalityChoice && personalityChoice.personality) ? personalityChoice.personality.image : '', color: (personalityChoice && personalityChoice.personality) ? personalityChoice.personality.color : '' };
    const personality = getNameFromButtonBlock(buttonBlocks, 'personality_quiz');
    const changePersonality = getNameFromButtonBlock(buttonBlocks, 'Change_Personality_Quiz_Answers');
    const updateAnswers = getNameFromButtonBlock(buttonBlocks, 'update_answers');
    const saveChange = getNameFromButtonBlock(buttonBlocks, 'save_changes');
    const takeQuiz = getNameFromButtonBlock(buttonBlocks, 'take_quiz_now');
    return (
      <div className="div-persionality-quiz">
        <span className="title-quiz">
          {personality}
        </span>
        <Row className="div-outcome-quiz">
          <Col md="6" xs="12" className={personalityChoice ? 'div-image' : 'hidden'}>
            <img loading="lazy" src={dataPersionality.image} alt="outcome" />
            <span className="span-text" style={{ color: dataPersionality.color }}>
              {personalityChoice ? personalityChoice.personality.title : ''}
            </span>
          </Col>

          <Col md="6" xs="12" className="div-change-quiz">
            <span>
              {changePersonality}
            </span>
            <button type="button" onClick={this.onClickButton}>
              {personalityChoice ? updateAnswers : takeQuiz}
            </button>
          </Col>
        </Row>
        {/* <div className="div-button">
          <button type="button">
            {saveChange}
          </button>
        </div> */}
      </div>
    );
  }
}

PersonalityQuiz.propTypes = {
  buttonBlocks: PropTypes.arrayOf().isRequired,
  history: PropTypes.shape().isRequired,
};

export default PersonalityQuiz;
