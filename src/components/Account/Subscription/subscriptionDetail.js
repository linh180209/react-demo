import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import PropTypes from 'prop-types';
import moment from 'moment';
import _, { add } from 'lodash';
import icInfo from '../../../image/icon/icInfoWhilte.svg';
import ModalChangePayment from './modalChangePayment';
import InputAccoutDetail from '../AccountDetail/inputAccoutDetail';
import { getNameFromButtonBlock, generaCurrency } from '../../../Redux/Helpers';
import {
  GET_STRIP_CARDS, UPDATE_ADDRESS_GUEST_URL, GET_ADDRESS_SUBSCRIPTION_URL, CREATE_ADDRESS_URL,
} from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import PopUpSaveAdsress from '../../../views/CheckOutB2C/popUpSaveAddress';
import auth from '../../../Redux/Helpers/auth';
import { toastrError } from '../../../Redux/Helpers/notification';
import { isMobile, isTablet } from '../../../DetectScreen';

class SubcriptionDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowAddPayment: false,
      stripCards: [],
      idDefault: undefined,
      isShowChoiseAddress: false,
      currentAddress: undefined,
    };
  }

  componentDidMount() {
    this.fetchStripCards();
    this.fetchCurrentAddress();
    this.fetchAllAdress();
  }

  updateStripCards = () => {
    this.fetchStripCards();
  }

  fetchStripCards = () => {
    const option = {
      url: GET_STRIP_CARDS,
      method: 'GET',
    };
    fetchClient(option, true).then((result) => {
      const ele = _.find(result || [], x => x.is_default === true);
      this.setState({ stripCards: result, idDefault: ele ? ele.id : undefined });
    });
  }

  openShowAddress = () => {
    this.setState({ isShowChoiseAddress: true });
  }

  closeShowAddress = () => {
    this.setState({ isShowChoiseAddress: false });
  }

  onChangeAddress = (address) => {
    const { listAddress } = this.state;
    const ele = _.find(listAddress, x => x.id === address.id);
    if (!ele) {
      listAddress.unshift(address);
    } else {
      _.assign(ele, address);
    }

    this.props.loadingPage(true);
    const { currentAddress } = this.state;
    const options = {
      url: UPDATE_ADDRESS_GUEST_URL.replace('{id}', address.id),
      method: 'PUT',
      body: {
        is_subscription: true,
      },
    };
    const options1 = {
      url: UPDATE_ADDRESS_GUEST_URL.replace('{id}', currentAddress.id),
      method: 'PUT',
      body: {
        is_subscription: false,
      },
    };
    const pending = [
      fetchClient(options, true), fetchClient(options1, true),
    ];
    Promise.all(pending).then((result) => {
      this.props.loadingPage(false);
      this.setState({ currentAddress: result[0] });
    }).catch((error) => {
      toastrError(error.message);
      this.props.loadingPage(false);
    });
  }

  fetchAllAdress = () => {
    const dataUser = _.isEmpty(this.props.login) ? auth.getLogin() : this.props.login;
    if (dataUser && dataUser.user && dataUser.user.id) {
      const option = {
        url: CREATE_ADDRESS_URL,
        method: 'GET',
      };
      fetchClient(option, true).then((result) => {
        if (result && !result.isError) {
          this.setState({ listAddress: result });
        }
        throw new Error(result.message);
      }).catch((error) => {
        console.log('error: ', error);
      });
    }
  }

  fetchCurrentAddress = () => {
    const options = {
      url: GET_ADDRESS_SUBSCRIPTION_URL,
      method: 'GET',
    };
    fetchClient(options, true).then((result) => {
      this.setState({ currentAddress: result });
    });
  }

  render() {
    const {
      isShowAddPayment, stripCards, idDefault, isShowChoiseAddress, listAddress, currentAddress,
    } = this.state;
    const {
      buttonBlocks, currentSubscription, login, openPopUp,
    } = this.props;
    const { shipping_address: shippingAddress } = currentSubscription;
    const isShowPaymentError = currentSubscription ? currentSubscription.status === 'failed' : false;
    const title = getNameFromButtonBlock(buttonBlocks, 'Subscription_details');
    const memberSince = getNameFromButtonBlock(buttonBlocks, 'Member_Since');
    const currentPlan = getNameFromButtonBlock(buttonBlocks, 'Current_Plan');
    const payment = getNameFromButtonBlock(buttonBlocks, 'Payment');
    const renevalDate = getNameFromButtonBlock(buttonBlocks, 'Renewal_Date');
    const unSubscribe = getNameFromButtonBlock(buttonBlocks, 'UNSUBSCRIBE');
    const autoRenewsOn = getNameFromButtonBlock(buttonBlocks, 'Auto_Renews_on');
    const updatePaymentBt = getNameFromButtonBlock(buttonBlocks, 'update_payment_method');
    const thereBt = getNameFromButtonBlock(buttonBlocks, 'There_was_an_error');
    const editAddressBt = getNameFromButtonBlock(buttonBlocks, 'Edit_Address');
    return (
      <div className="div-subscription-detail">
        {isShowAddPayment && (
        <ModalChangePayment
          updateStripCards={this.updateStripCards}
          buttonBlocks={buttonBlocks}
          onClose={() => this.setState({ isShowAddPayment: false })}
          stripCards={stripCards}
          idDefault={idDefault}
          loadingPage={this.props.loadingPage}
        />
        )}
        {
          isShowChoiseAddress ? (
            <PopUpSaveAdsress
              closeShowAddress={this.closeShowAddress}
              listAddress={listAddress}
              currentAddress={currentAddress}
              onChangeAddress={this.onChangeAddress}
              buttonBlock={buttonBlocks}
              isShowNewAddress
              login={login}
            />
          ) : (null)
        }
        <span className="title-subscription">
          {title}
        </span>
        <Row className="info">
          <Col md="3" xs="6" className="info-item">
            <span>
              {memberSince}
            </span>
            <span>
              {moment(login.user.date_subscription).format('MMM DD, YYYY')}
            </span>
          </Col>
          {/* <Col md="3" xs="6" className="info-item">
            <span>
              {currentPlan}
            </span>
            <span>
              Discovery Box
            </span>
          </Col> */}
          <Col md="3" xs="6" className="info-item">
            <span>
              {payment}
            </span>
            <span>
              {generaCurrency(currentSubscription.total)}
            </span>
          </Col>
          <Col md="3" xs="6" className="info-item">
            <span>
              {renevalDate}
            </span>
            <span>
              {`${autoRenewsOn} ${moment(login.user.date_subscription).add(1, 'month').format('MMM DD, YYYY')}`}
            </span>
          </Col>
        </Row>
        {
          isMobile ? (
            <div className="bt-update-payment">
              <button type="button" className="button-bg__none" onClick={() => this.setState({ isShowAddPayment: true })}>
                {updatePaymentBt}
              </button>
            </div>
          ) : (
            <div className="div-update-payment">
              <div className="bt-update-payment">
                <button type="button" className="button-bg__none" onClick={() => this.setState({ isShowAddPayment: true })}>
                  {updatePaymentBt}
                </button>
              </div>
              {
                isShowPaymentError && (
                  <div className="div-info-payment-error">
                    <img loading="lazy" src={icInfo} alt="info" />
                    <span>
                      {thereBt}
                    </span>
                  </div>
                )
              }
            </div>
          )
        }

        <hr />
        <Row className="info-personal">
          <Col md="6" xs="12" className="mt-3" style={isMobile && !isTablet ? { paddingLeft: '0px', paddingRight: '0px' } : {}}>
            <InputAccoutDetail
              title="First Name"
              defaultValue={login.user.first_name}
              name="firstName"
              onChange={this.onChange}
              disabled
            />
          </Col>
          <Col md="6" xs="12" className="mt-3" style={isMobile && !isTablet ? { paddingLeft: '0px', paddingRight: '0px' } : {}}>
            <InputAccoutDetail
              title="Last Name"
              defaultValue={login.user.last_name}
              name="lastName"
              onChange={this.onChange}
              disabled
            />
          </Col>
          <Col md="6" xs="12" className="mt-3" style={isMobile && !isTablet ? { paddingLeft: '0px', paddingRight: '0px' } : {}}>
            <InputAccoutDetail
              title="Shipping Address"
              defaultValue={currentAddress ? `${currentAddress.street1 || ''} ${currentAddress.city || ''} ${currentAddress ? currentAddress.country.code : ''} ${currentAddress ? currentAddress.country.name : ''}` : ''}
              name="shipAddress"
              onChange={this.onChange}
              disabled
            />
          </Col>
          <Col md="6" xs="12" className="mt-3" style={isMobile && !isTablet ? { paddingLeft: '0px', paddingRight: '0px' } : {}}>
            <InputAccoutDetail
              title="Phone Number"
              defaultValue={currentAddress ? currentAddress.phone : ''}
              name="phoneNumber"
              onChange={this.onChange}
              disabled
            />
          </Col>
        </Row>
        <div className="div-button">
          <button type="button" onClick={openPopUp}>
            {unSubscribe}
          </button>
          <button type="button" className="bg-background" onClick={this.openShowAddress}>
            {editAddressBt}
          </button>
        </div>
      </div>
    );
  }
}

SubcriptionDetail.propTypes = {
  buttonBlocks: PropTypes.arrayOf().isRequired,
  currentSubscription: PropTypes.shape().isRequired,
  login: PropTypes.shape().isRequired,
  openPopUp: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
};

export default SubcriptionDetail;
