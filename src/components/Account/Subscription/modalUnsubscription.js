import React, { Component } from 'react';
import PropTypes from 'prop-types';
import reactHtmlPare from 'react-html-parser';

import moment from 'moment';
import icClose from '../../../image/icon/ic-close-region.svg';
import { getNameFromButtonBlock, generaCurrency } from '../../../Redux/Helpers';
import { UPDATE_USER_URL } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';

class ModalUnsubcription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isUnsubcriptionDone: false,
    };
  }

  onClickButton = () => {
    const { isUnsubcriptionDone } = this.state;
    if (isUnsubcriptionDone) {
      this.props.onClose();
    } else {
      this.unsubscribeUser();
    }
  }

  unsubscribeUser = () => {
    const { login } = this.props;
    const option = {
      url: UPDATE_USER_URL.replace('{id}', login.user.id),
      method: 'PUT',
      body: {
        is_club_member: false,
      },
    };
    fetchClient(option, true).then((result) => {
      if (result && !result.isError) {
        this.setState({ isUnsubcriptionDone: true });
        this.props.unsubscriptionSuccess();
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err.message);
    });
  }

  render() {
    const { isUnsubcriptionDone } = this.state;
    const {
      buttonBlocks, currentSubscription, onClose, login, paragraphBlock,
    } = this.props;
    const sayGoogbye = getNameFromButtonBlock(buttonBlocks, 'Saying_Goodbye?');
    const comeBack = getNameFromButtonBlock(buttonBlocks, 'Come_back_soon');
    const subscriptionPlan = getNameFromButtonBlock(buttonBlocks, 'subscription_plan');
    const discoveryBox = getNameFromButtonBlock(buttonBlocks, 'Discovery_Box');
    const nextPayment = getNameFromButtonBlock(buttonBlocks, 'next_payment');
    const youWill = getNameFromButtonBlock(buttonBlocks, 'You_will_be_immedietly');
    const UNSUBSCRIBE = getNameFromButtonBlock(buttonBlocks, 'UNSUBSCRIBE');
    const iDont = getNameFromButtonBlock(buttonBlocks, 'I_don’t_want_to_leave_yet');
    const continuteShop = getNameFromButtonBlock(buttonBlocks, 'continue_shopping');
    const ceo = getNameFromButtonBlock(buttonBlocks, 'CEO_Founder');
    const Monange = getNameFromButtonBlock(buttonBlocks, 'Johanna_Monange');
    return (
      <div className="div-modal-unsubcription animated fadeIn">
        <div className="div-background" onClick={onClose} />
        <div className="div-body-modal">
          <div className="div-content-modal">
            <button type="button" className="button-close" onClick={onClose}>
              <img loading="lazy" src={icClose} alt="icClose" />
            </button>
            <span className="div-header">
              {isUnsubcriptionDone ? comeBack : sayGoogbye}
            </span>
            <div className="div-content">
              {
                isUnsubcriptionDone ? (
                  <React.Fragment>
                    <div className="div-text">
                      {reactHtmlPare(paragraphBlock && paragraphBlock.length > 0 ? paragraphBlock[0].value : '')}
                    </div>
                    <span className="title-ceo">
                      <i>{ceo}</i>
                    </span>
                    <span className="title-name">
                      <b>{Monange}</b>
                    </span>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <span className="header-1">
                      {subscriptionPlan}
                    </span>
                    <span>
                      <b>{discoveryBox}</b>
                    </span>
                    <span>
                      {generaCurrency(currentSubscription ? currentSubscription.total : '')}
                    </span>
                    <span className="header-1 mt-3">
                      {nextPayment}
                    </span>
                    <span>
                      <b>{moment(login.user.date_subscription).add(1, 'month').format('MMM DD, YYYY')}</b>
                    </span>
                  </React.Fragment>
                )
              }
            </div>
            <hr />
            <div className="div-footer-modal">
              <span className={isUnsubcriptionDone ? 'hidden' : 'tc'}>
                {youWill}
              </span>
              <button type="button" className={isUnsubcriptionDone ? 'mt-5 mb-4 button-normal' : 'button-normal'} onClick={this.onClickButton}>
                {isUnsubcriptionDone ? continuteShop : UNSUBSCRIBE}
              </button>
              <button type="button" className={isUnsubcriptionDone ? 'hidden' : 'button-link'} onClick={onClose}>
                {iDont}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ModalUnsubcription.propTypes = {
  buttonBlocks: PropTypes.shape().isRequired,
  onClose: PropTypes.func.isRequired,
  unsubscriptionSuccess: PropTypes.func.isRequired,
  currentSubscription: PropTypes.shape().isRequired,
  login: PropTypes.shape().isRequired,
  paragraphBlock: PropTypes.arrayOf().isRequired,
};

export default ModalUnsubcription;
