import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { getNameFromButtonBlock, getLinkFromButtonBlock, generateUrlWeb } from '../../../Redux/Helpers';

class SubscriptionDetailNone extends Component {
  render() {
    const { buttonBlocks, dateUnsubscription } = this.props;
    const title = getNameFromButtonBlock(buttonBlocks, 'Subscription_details');
    const description = getNameFromButtonBlock(buttonBlocks, 'You_currently');
    const joinBt = getNameFromButtonBlock(buttonBlocks, dateUnsubscription ? 'resubscribe_to_club_21' : 'JOIN_CLUB_21');
    const linkBt = getLinkFromButtonBlock(buttonBlocks, dateUnsubscription ? 'resubscribe_to_club_21' : 'JOIN_CLUB_21');
    const youHaveEnded = getNameFromButtonBlock(buttonBlocks, 'you_have_ended');
    const youCanEasily = getNameFromButtonBlock(buttonBlocks, 'you_can_easily');
    return (
      <div className="div-subscription-detail none-text">
        <span className="title-subscription">
          {title}
        </span>
        <span className="description-none">
          {dateUnsubscription ? `${youHaveEnded} ${moment(dateUnsubscription).format('MMM DD, YYYY')}. ${youCanEasily}` : description}
        </span>
        <div className="div-button-dark">
          <Link to={generateUrlWeb(linkBt)}>
            {joinBt}
          </Link>
        </div>
      </div>
    );
  }
}

SubscriptionDetailNone.propTypes = {
  buttonBlocks: PropTypes.arrayOf().isRequired,
  lastDay: PropTypes.string.isRequired,
};

export default SubscriptionDetailNone;
