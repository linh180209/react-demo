import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

import moment from 'moment';
import '../../../styles/subscription-account.scss';
import StepTracking from './stepTracking';
import SubscriptionGoal from './subscriptionGoal';
import SubscriptionDetail from './subscriptionDetail';
import PersonalityQuiz from './personalityQuiz';
import SubscriptionDetailNone from './subscriptionDetailNone';
import {
  fetchCMSHomepage, getSEOFromCms, scrollTop, getNameFromButtonBlock,
} from '../../../Redux/Helpers';
import { toastrError } from '../../../Redux/Helpers/notification';
import { GET_SUBSCRIPTION_ORDER, GET_USER_OUTCOME } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import ModalUnsubcription from './modalUnsubscription';
import icInfo from '../../../image/icon/icInfoWhilte.svg';
import { isBrowser, isMobile } from '../../../DetectScreen';
import auth from '../../../Redux/Helpers/auth';

const prePareCms = (cms) => {
  if (cms) {
    const seo = getSEOFromCms(cms);
    const { body, image_background: imageBg } = cms;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    const paragraphBlock = _.filter(body, x => x.type === 'paragraph_block');
    return {
      seo, buttonBlocks, imageBg, paragraphBlock,
    };
  }
  return { seo: undefined, buttonBlocks: [], imageBg: '' };
};
class Subscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      seo: {},
      buttonBlocks: [],
      paragraphBlock: [],
      imageBg: '',
      subscriptionData: undefined,
      isShowPopup: false,
      outcome: undefined,
    };
  }

  componentDidMount() {
    const login = auth.getLogin();
    if (login && login.user && login.user.is_club_member) {
      this.fetchSubscription();
    }
    this.fetchDataInit();
  }

  fetchSubscription = () => {
    const login = auth.getLogin();
    const option = {
      url: GET_SUBSCRIPTION_ORDER.replace('{user}', login.user.id),
      method: 'GET',
    };
    const optionsCome = {
      url: GET_USER_OUTCOME.replace('{user}', login.user.id),
      method: 'GET',

    };
    const pending = [fetchClient(option), fetchClient(optionsCome, true)];
    this.props.loadingPage(true);
    Promise.all(pending).then((result) => {
      if (result && result.length > 1) {
        this.setState({ subscriptionData: _.orderBy(result[0], ['date_created'], ['asc']), outcome: result[1] && result[1].length > 0 ? result[1][0] : undefined });
        this.props.loadingPage(false);
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err.message);
      this.props.loadingPage(false);
    });
  };

  fetchDataInit = async () => {
    const { cms } = this.props;
    const subCms = _.find(cms, x => x.title === 'My Subscription');
    if (_.isEmpty(subCms)) {
      const pending = [fetchCMSHomepage('my-subscription')];
      try {
        const results = await Promise.all(pending);
        const cmsData = results[0];
        this.props.addCmsRedux(cmsData);
        const dataCms = prePareCms(cmsData);
        this.setState(dataCms);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const dataCms = prePareCms(subCms);
      this.setState(dataCms);
    }
  }

  closePopUp = () => {
    this.setState({ isShowPopup: false });
  }

  openPopUp = () => {
    this.setState({ isShowPopup: true });
  }

  unsubscriptionSuccess = () => {
    scrollTop();
    this.setState({ subscriptionData: undefined });
  }

  render() {
    const {
      seo, buttonBlocks, imageBg, subscriptionData, paragraphBlock, isShowPopup, outcome,
    } = this.state;
    const { login, history } = this.props;
    const currentSubscription = subscriptionData && subscriptionData.length > 0 ? subscriptionData[subscriptionData.length - 1] : undefined;
    const dateUnsubscription = currentSubscription ? currentSubscription.date_unsubscription : undefined;
    const isShowPaymentError = currentSubscription ? currentSubscription.status === 'failed' : false;
    const thereBt = getNameFromButtonBlock(buttonBlocks, 'There_was_an_error');

    // const isHasSubsciption = lastDay && moment(lastDay).format('MM') === moment().format('MM');
    return (
      <div className="div-subscription-account">
        {
          isShowPopup ? <ModalUnsubcription buttonBlocks={buttonBlocks} onClose={this.closePopUp} currentSubscription={currentSubscription} login={login} paragraphBlock={paragraphBlock} unsubscriptionSuccess={this.unsubscriptionSuccess} /> : (<div />)
        }
        {
          login && login.user && login.user.is_club_member && subscriptionData ? (
            <React.Fragment>
              {
                isShowPaymentError && (
                  <div className={isMobile ? 'div-info-payment-error' : 'hidden'}>
                    <img loading="lazy" src={icInfo} alt="info" />
                    <span>
                      {thereBt}
                    </span>
                  </div>
                )
              }
              <StepTracking buttonBlocks={buttonBlocks} status={currentSubscription ? currentSubscription.status : ''} />
              <div className="div-content-sub" style={isBrowser ? { backgroundImage: `url(${imageBg})` } : {}}>
                {/* <SubscriptionDetailNone buttonBlocks={buttonBlocks} /> */}
                <SubscriptionGoal buttonBlocks={buttonBlocks} subscriptionData={subscriptionData} />
                <SubscriptionDetail buttonBlocks={buttonBlocks} currentSubscription={currentSubscription} login={login} openPopUp={this.openPopUp} loadingPage={this.props.loadingPage} />
                <PersonalityQuiz buttonBlocks={buttonBlocks} personalityChoice={outcome ? outcome.personality_choice : ''} history={history} />
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div className="div-content-sub" style={isBrowser ? { backgroundImage: `url(${imageBg})` } : {}}>
                <SubscriptionDetailNone buttonBlocks={buttonBlocks} dateUnsubscription={dateUnsubscription} />
              </div>
            </React.Fragment>
          )
        }
      </div>
    );
  }
}

Subscription.propTypes = {
  cmsCommon: PropTypes.shape().isRequired,
  loadingPage: PropTypes.func.isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  cms: PropTypes.shape().isRequired,
  login: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
};

export default Subscription;
