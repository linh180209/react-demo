import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import moment from 'moment';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';

class StepTracking extends Component {
  render() {
    const { buttonBlocks, status } = this.props;
    const step1 = getNameFromButtonBlock(buttonBlocks, 'PACKED');
    const step2 = getNameFromButtonBlock(buttonBlocks, 'shipped');
    const step3 = getNameFromButtonBlock(buttonBlocks, 'delivered');
    const your = getNameFromButtonBlock(buttonBlocks, 'Your');
    const box = getNameFromButtonBlock(buttonBlocks, 'Box');
    const tracking = getNameFromButtonBlock(buttonBlocks, 'Tracking');
    const dataSteps = [step1, step2, step3];
    const statusRecommend = ['processed', 'shipped', 'completed'];
    const indexStep = _.findIndex(statusRecommend, x => x.toLowerCase() === status.toLowerCase());
    return (
      <div className="div-tracking-box">
        <span className="title">
          {your}
          {' '}
          <b>
            {`${moment().format('MMM')} ${box}`}
            {' '}
          </b>
          {' '}
          {tracking}
        </span>
        <div className="div-step">
          <hr />
          {
              _.map(dataSteps, (x, index) => (
                <div className={`item-step ${indexStep >= index ? 'active' : ''}`}>
                  <div className="bg-circle">
                    {index}
                  </div>
                  <span>
                    {x}
                  </span>
                </div>
              ))
            }
        </div>
      </div>
    );
  }
}

StepTracking.propTypes = {
  buttonBlocks: PropTypes.arrayOf().isRequired,
  status: PropTypes.string.isRequired,
};
export default StepTracking;
