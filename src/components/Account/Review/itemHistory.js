/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import Dropzone from 'react-dropzone';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import icStar from '../../../image/icon/star.svg';
import icStarSelected from '../../../image/icon/star-selected.svg';
import { isBrowser, isMobile } from '../../../DetectScreen';

class ItemHistory extends Component {
  onSelectFile = (files) => {
    if (files && files.length > 0) {
      console.log('object');
    }
  };

  render() {
    const { data, buttonBlocks } = this.props;
    const {
      comment, rating_product: rateProduct, rating_satisfaction: rateSatisfaction, rating_scent: rateScent, line_item: lineItem,
      image,
    } = data;
    const {
      date_completed: dataCompleted, name, type,
    } = lineItem;
    const deliveredBt = getNameFromButtonBlock(buttonBlocks, 'Delivered_on');
    const rateBt = getNameFromButtonBlock(buttonBlocks, 'rate_review_product');
    const reviewDetailBt = getNameFromButtonBlock(buttonBlocks, 'Review_Detail');
    const scentBlendBt = getNameFromButtonBlock(buttonBlocks, 'Scent_Blend');
    const productBt = getNameFromButtonBlock(buttonBlocks, 'Product');
    const satisfactionBt = getNameFromButtonBlock(buttonBlocks, 'Satisfaction');
    const uploadPhotoBt = getNameFromButtonBlock(buttonBlocks, 'Upload_Photo');
    const editbt = getNameFromButtonBlock(buttonBlocks, 'Edit');
    return (
      <div className="div-item-history">
        <h5>
          {`${deliveredBt} ${moment(dataCompleted).format('d MMM YYYY')}`}
        </h5>
        <div className="div-body-item">
          <div className="div-left">
            <span className={isMobile ? 'hidden' : 'title'}>
              {reviewDetailBt}
            </span>
            <div className="div-product">
              <img loading="lazy" src={lineItem.image} alt="product" onClick={() => this.props.onClickGotoProduct(lineItem)} />
              <div className="text-info">
                <span>
                  {type.name}
                </span>
                <span>
                  {name}
                </span>
              </div>
            </div>
            {
              isBrowser ? (
                <React.Fragment>
                  <span className="title mt-4">
                    {reviewDetailBt}
                  </span>
                  <span className="comment">
                    {comment}
                  </span>
                </React.Fragment>
              ) : (<div />)
            }

          </div>
          <div className="div-right">
            <div className="div-star">
              <span>
                {scentBlendBt}
              </span>
              <div className="div-list-star">
                {
                  _.map(_.range(5), (x, index) => (
                    <img loading="lazy" src={index + 1 <= rateScent ? icStarSelected : icStar} alt="star" />
                  ))
                }
              </div>
            </div>
            <div className="div-star">
              <span>
                {productBt}
              </span>
              <div className="div-list-star">
                {
                  _.map(_.range(5), (x, index) => (
                    <img loading="lazy" src={index + 1 <= rateProduct ? icStarSelected : icStar} alt="star" />
                  ))
                }
              </div>
            </div>
            <div className="div-star">
              <span>
                {satisfactionBt}
              </span>
              <div className="div-list-star">
                {
                  _.map(_.range(5), (x, index) => (
                    <img loading="lazy" src={index + 1 <= rateSatisfaction ? icStarSelected : icStar} alt="star" />
                  ))
                }
              </div>
            </div>
          </div>
        </div>
        {
          isMobile ? (
            <span className="comment">
              {comment}
            </span>
          ) : (<div />)
        }

        <div className={image ? 'div-list-post-image' : 'hidden'}>
          <img loading="lazy" className="img-up" src={image} alt="post" />
        </div>
        <div className="button-edit-save">
          <button type="button" onClick={() => this.props.onClickEditReview(data)}>
            {editbt}
          </button>
        </div>
      </div>
    );
  }
}

export default ItemHistory;
