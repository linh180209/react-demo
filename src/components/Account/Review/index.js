import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import { connect } from 'react-redux';
import '../../../styles/account-review.scss';
import ReviewItem from './reviewItem';
import icBack from '../../../image/icon/back-order-mobile.svg';
import { POST_REVIEWS_URL, GET_HISTORY_REVIEW_URL, GET_TO_REVIEW_URL } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import ItemReviewed from './itemReviewed';
import ItemHistory from './itemHistory';
import Writereview from './writeReview';
import {
  getNameFromCommon, fetchCMSHomepage, getSEOFromCms, getNameFromButtonBlock, getLinkFromButtonBlock, scrollTop,
  getUrlProductReview,
  generateUrlWeb,
  segmentAccountDashboardWriteReviewInitiated,
} from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';

const prePareCms = (cms) => {
  if (cms) {
    const seo = getSEOFromCms(cms);
    const { body, header_text: headerText } = cms;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    return {
      seo, buttonBlocks, headerText,
    };
  }
  return { seo: undefined, buttonBlocks: [], headerText: '' };
};

class Reviews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonBlocks: [],
      itemDetail: undefined,
      indexTabs: 0,
      dataHistorys: undefined,
      dataReviews: undefined,
      dataEditReview: {},
    };
  }

  componentDidMount() {
    scrollTop();
    const login = auth.getLogin();
    if (login && login.user) {
      const { id } = login.user;
      this.fetchReviews(id);
    }
    this.fetchCMS();
  }

  fetchCMS = async () => {
    const { cms } = this.props;
    const myOrder = _.find(cms, x => x.title === 'My Review');
    if (_.isEmpty(myOrder)) {
      try {
        const cmsData = await fetchCMSHomepage('my-review');
        this.props.addCmsRedux(cmsData);
        const dataCms = prePareCms(cmsData);
        this.setState(dataCms);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const dataCms = prePareCms(myOrder);
      this.setState(dataCms);
    }
  }

  removeHash = () => {
    this.props.history.push(generateUrlWeb('/account/'));
  }

  fetchReviews = () => {
    this.props.loadingPage(true);
    const optionsHistory = {
      url: GET_HISTORY_REVIEW_URL,
      method: 'GET',
    };
    const optionsReview = {
      url: GET_TO_REVIEW_URL,
      method: 'GET',
    };
    Promise.all([fetchClient(optionsHistory, true), fetchClient(optionsReview, true)]).then((results) => {
      this.props.loadingPage(false);
      const object = {
        dataHistorys: results[0],
        dataReviews: results[1],
      };
      if (this.props.idItem) {
        const ele = _.find(results[1], x => Number(x.product) === Number(this.props.idItem));
        const ele1 = _.find(results[0], x => Number(x.line_item.product) === Number(this.props.idItem));
        if (ele) {
          _.assign(object, { itemDetail: ele });
        } else if (ele1) {
          const { line_item: lineItem } = ele1;
          _.assign(object, { itemDetail: lineItem, dataEditReview: ele1 });
        }
        this.removeHash();
      }
      this.setState(object);
    }).catch((err) => {
      this.props.loadingPage(false);
      toastrError(err.message);
    });
  }

  onClickWriteReview = (data) => {
    this.setState({ itemDetail: data }, () => {
      segmentAccountDashboardWriteReviewInitiated(data);
      scrollTop();
    });
  }

  onClickBack = () => {
    this.setState({ itemDetail: undefined, dataEditReview: {} });
  }

  updateHistory = (data) => {
    const { dataHistorys, dataReviews } = this.state;
    const ele = _.find(dataHistorys, x => x.id === data.id);
    if (ele) {
      _.assign(ele, data);
    } else {
      dataHistorys.push(data);
    }
    _.remove(dataReviews, x => x.id === data.line_item.id);
    this.setState({ dataHistorys });
  }

  onClickEditReview = (dataReview) => {
    const { line_item: lineItem } = dataReview;
    this.setState({ itemDetail: lineItem, dataEditReview: dataReview }, () => {
      scrollTop();
    });
  };

  onClickGotoProduct = (data) => {
    const {
      product, item, combo, type,
    } = data;
    const urlPro = getUrlProductReview(product, type, combo, item).url;
    if (urlPro) {
      this.props.history.push(generateUrlWeb(urlPro));
    }
  }

  render() {
    const {
      dataHistorys, dataReviews, itemDetail, indexTabs, buttonBlocks, dataEditReview,
    } = this.state;
    const myReviewBt = getNameFromButtonBlock(buttonBlocks, 'my_reviews');
    const toReceiveBt = getNameFromButtonBlock(buttonBlocks, 'to_be_reviewed');
    const historyBt = getNameFromButtonBlock(buttonBlocks, 'history');
    const youdontBt = getNameFromButtonBlock(buttonBlocks, 'you_dont_have_anythiing_to_review');
    const shopNowBt = getNameFromButtonBlock(buttonBlocks, 'shop_now');
    const linkShowBt = getLinkFromButtonBlock(buttonBlocks, 'shop_now');
    const reviewN = getNameFromCommon(this.props.cmsCommon, 'REVIEWS').toLowerCase();

    const reviewedTab = (
      <div className="div-sub-tab">
        {
          !dataReviews ? null
            : dataReviews.length > 0 ? (
              _.map(dataReviews, x => (
                <ItemReviewed
                  buttonBlocks={buttonBlocks}
                  data={x}
                  onClick={this.onClickWriteReview}
                  onClickGotoProduct={this.onClickGotoProduct}
                />
              ))
            ) : (
              <div className="div-none-item">
                <h4>
                  {youdontBt}
                </h4>
                <div>
                  <button
                    className="div-button-shop"
                    type="button"
                    onClick={() => this.props.history.push(generateUrlWeb(linkShowBt))}
                  >
                    {shopNowBt}
                  </button>
                </div>
              </div>
            )
        }
      </div>
    );

    const historyItemTab = (
      <div className="div-sub-tab">
        {
          !dataHistorys ? null
            : _.map(dataHistorys, x => (
              <ItemHistory
                buttonBlocks={buttonBlocks}
                data={x}
                onClickEditReview={this.onClickEditReview}
                onClickGotoProduct={this.onClickGotoProduct}
              />
            ))
        }
      </div>
    );

    const subTab = indexTabs === 0 ? reviewedTab : historyItemTab;

    return (
      <div className="div-wrap-review">
        {
          itemDetail ? (
            <Writereview
              data={itemDetail}
              onBack={this.onClickBack}
              buttonBlocks={buttonBlocks}
              loadingPage={this.props.loadingPage}
              updateHistory={this.updateHistory}
              dataEditReview={dataEditReview}
              onClickGotoProduct={this.onClickGotoProduct}
            />
          ) : (
            <div className="div-account-review animated faster fadeInLeft">
              <h3>
                {myReviewBt}
              </h3>
              <div className="div-button-tab">
                <div
                  className={`div-tab ${indexTabs === 0 ? 'active' : ''}`}
                >
                  <button
                    type="button"
                    onClick={() => this.setState({ indexTabs: 0 })}
                  >
                    {`${toReceiveBt} ${dataReviews ? `(${dataReviews.length})` : '(0)'}`}
                  </button>
                </div>
                <div
                  className={`div-tab ${indexTabs === 1 ? 'active' : ''}`}
                >
                  <button
                    type="button"
                    onClick={() => this.setState({ indexTabs: 1 })}
                  >
                    {`${historyBt} ${dataHistorys ? `(${dataHistorys.length})` : '(0)'}`}
                  </button>
                </div>
              </div>
              {
          subTab
        }
            </div>
          )
        }

      </div>
    );
  }
}

Reviews.propTypes = {
  login: PropTypes.shape({
    user: PropTypes.shape(PropTypes.object),
  }).isRequired,
  cmsCommon: PropTypes.arrayOf().isRequired,
  history: PropTypes.shape().isRequired,
};


export default Reviews;
