/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import _ from 'lodash';
import moment from 'moment';
import Dropzone from 'react-dropzone';
import loadImage from 'blueimp-load-image';
import icBack from '../../../image/icon/back-order-mobile.svg';
import icStar from '../../../image/icon/star.svg';
import icStarSelected from '../../../image/icon/star-selected.svg';
import icUploadFile from '../../../image/icon/ic-upload-file.svg';
import { getNameFromButtonBlock, segmentProductReviewed } from '../../../Redux/Helpers';
import { fetchClientFormData } from '../../../Redux/Helpers/fetch-client';
import { GET_HISTORY_REVIEW_URL } from '../../../config';
import { toastrError, toastrSuccess } from '../../../Redux/Helpers/notification';
import { isBrowser, isMobile } from '../../../DetectScreen';

class Writereview extends Component {
  constructor(props) {
    super(props);
    const { dataEditReview } = props;
    const {
      comment, rating_product: rateProduct, rating_satisfaction: rateSatisfaction, rating_scent: rateScent, image,
    } = dataEditReview;
    this.state = {
      rateScent: rateScent || 5,
      rateProduct: rateProduct || 5,
      rateSatisfaction: rateSatisfaction || 5,
      comment: comment || '',
      imageUpload: image,
    };
  }

  onChange = (e) => {
    const { value } = e.target;
    this.setState({ comment: value });
  }

  onSelectFile = (files) => {
    if (files && files.length > 0) {
      loadImage(
        files[0],
        (img) => {
          const base64data = img.toDataURL('image/png');
          this.setState({ imageUpload: base64data });
          this.fileImage = files[0];
        },
        { orientation: 1 },
      );
    }
  };

  postData = () => {
    const { dataEditReview } = this.props;
    const { id } = this.props.data;
    const {
      rateProduct, rateScent, rateSatisfaction, comment,
    } = this.state;
    const formData = new FormData();
    formData.append('line_item', id);
    if (this.fileImage) {
      formData.append('image', this.fileImage);
    }
    if (comment) {
      formData.append('comment', comment);
    }
    formData.append('rating_scent', rateScent);
    formData.append('rating_product', rateProduct);
    formData.append('rating_satisfaction', rateSatisfaction);
    const options = {
      url: _.isEmpty(dataEditReview) ? GET_HISTORY_REVIEW_URL : `${GET_HISTORY_REVIEW_URL}${dataEditReview.id}`,
      method: _.isEmpty(dataEditReview) ? 'POST' : 'PUT',
      body: formData,
    };
    this.props.loadingPage(true);
    fetchClientFormData(options, true).then((result) => {
      this.props.loadingPage(false);
      if (result && !result.isError) {
        toastrSuccess('Write review is success');
        //console.log("result", result);
        const rate = _.mean([rateScent, rateProduct, rateSatisfaction]);
        const dataSegment = {
          product_id: result?.line_item?.id,
          product_name: result?.line_item?.name,
          //review_id: result?.id,
          //review_body: result?.comment,
          satisfaction_rating: rateSatisfaction,
          scent_rating: rateScent,
          product_rating: rateProduct,
          //average_rating: rate,
          //review_image_url: result?.image,
        };
        segmentProductReviewed(dataSegment);
        this.props.updateHistory(result);
        this.props.onBack();
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err.message);
      this.props.loadingPage(false);
    });
  }


  render() {
    const {
      rateScent, rateProduct, rateSatisfaction, comment, imageUpload,
    } = this.state;
    const { buttonBlocks, data } = this.props;
    const {
      date_completed: dataCompleted, image, name, type,
    } = data;
    const writeReviewBt = getNameFromButtonBlock(buttonBlocks, 'write_review');
    const deliveredBt = getNameFromButtonBlock(buttonBlocks, 'Delivered_on');
    const rateBt = getNameFromButtonBlock(buttonBlocks, 'rate_review_product');
    const reviewDetailBt = getNameFromButtonBlock(buttonBlocks, 'Review_Detail');
    const scentBlendBt = getNameFromButtonBlock(buttonBlocks, 'Scent_Blend');
    const productBt = getNameFromButtonBlock(buttonBlocks, 'Product');
    const satisfactionBt = getNameFromButtonBlock(buttonBlocks, 'Satisfaction');
    const uploadPhotoBt = getNameFromButtonBlock(buttonBlocks, 'Upload_Photo');
    const submitBt = getNameFromButtonBlock(buttonBlocks, 'submit');

    return (
      <div className="div-write-review animated faster fadeInRight">
        <div className="div-header-order">
          <button
            type="button"
            className="bt-back"
            onClick={this.props.onBack}
          >
            <img loading="lazy" src={icBack} alt="back" />
          </button>
          <h3>
            {writeReviewBt}
          </h3>
        </div>

        <div className="div-item-history">
          <h5>
            {`${deliveredBt} ${moment(dataCompleted).format('d MMM YYYY')}`}
          </h5>
          <div className="div-body-item">
            <div className="div-left">
              <span className="title">
                {rateBt}
              </span>
              <div className="div-product">
                <img loading="lazy" src={image} alt="product" onClick={() => this.props.onClickGotoProduct(data)} />
                <div className="text-info">
                  <span>
                    {type.name}
                  </span>
                  <span>
                    {name}
                  </span>
                </div>
              </div>
              {
                isBrowser ? (
                  <React.Fragment>
                    <span className="title mt-4">
                      {reviewDetailBt}
                    </span>
                    <textarea value={comment} className="comment" onChange={this.onChange} />
                  </React.Fragment>
                ) : <div />
              }

            </div>
            <div className="div-right">
              {
                isMobile ? <hr /> : <div />
              }
              <div className="div-star">
                <span>
                  {scentBlendBt}
                </span>
                <div className="div-list-star">
                  {
                  _.map(_.range(5), (x, index) => (
                    <button
                      type="button"
                      onClick={() => this.setState({ rateScent: index + 1 })
                    }
                    >
                      <img loading="lazy" src={index + 1 <= rateScent ? icStarSelected : icStar} alt="star" />
                    </button>
                  ))
                }
                </div>
              </div>
              <div className="div-star">
                <span>
                  {productBt}
                </span>
                <div className="div-list-star">
                  {
                  _.map(_.range(5), (x, index) => (
                    <button
                      type="button"
                      onClick={() => this.setState({ rateProduct: index + 1 })}
                    >
                      <img loading="lazy" src={index + 1 <= rateProduct ? icStarSelected : icStar} alt="star" />
                    </button>
                  ))
                }
                </div>
              </div>
              <div className="div-star">
                <span>
                  {satisfactionBt}
                </span>
                <div className="div-list-star">
                  {
                  _.map(_.range(5), (x, index) => (
                    <button
                      type="button"
                      onClick={() => this.setState({ rateSatisfaction: index + 1 })}
                    >
                      <img loading="lazy" src={index + 1 <= rateSatisfaction ? icStarSelected : icStar} alt="star" />
                    </button>
                  ))
                }
                </div>
              </div>
            </div>
          </div>
          {
            isMobile ? (
              <React.Fragment>
                <span className="title mt-4">
                  {reviewDetailBt}
                </span>
                <textarea value={comment} className="comment" onChange={this.onChange} />
              </React.Fragment>
            ) : (<div />)
          }
          <div className="div-list-post-image">
            {
              imageUpload ? (
                <img loading="lazy" className="img-up" src={imageUpload} alt="post" />
              ) : (
                <div />
              )
            }
            <div className="div-upload-image">
              <Dropzone ref={this.refCrop} onDrop={acceptedFiles => this.onSelectFile(acceptedFiles)} accept=".jpeg,.png,.jpg" multiple={false}>
                {({ getRootProps, getInputProps }) => (
                  <section>
                    <div {...getRootProps()}>
                      <input {...getInputProps()} />
                      <img loading="lazy" src={icUploadFile} alt="uploadFile" />
                      <span>
                        {uploadPhotoBt}
                      </span>
                    </div>
                  </section>
                )}
              </Dropzone>
            </div>
          </div>
          <div className="button-submit">
            <button type="button" onClick={this.postData}>
              {submitBt}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Writereview;
