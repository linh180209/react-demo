/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import moment from 'moment';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';

class ItemReviewed extends Component {
  render() {
    const { buttonBlocks, data } = this.props;
    const {
      date_completed: dataCompleted, image, name, type,
    } = data;
    const purchasedBt = getNameFromButtonBlock(buttonBlocks, 'Purchased_on');
    const viewBt = getNameFromButtonBlock(buttonBlocks, 'review');
    return (
      <div className="div-item-reviewed">
        <h5>
          {`${purchasedBt} ${moment(dataCompleted).format('d MMM YYYY')}`}
        </h5>
        <div className="div-item">
          <div className="div-left">
            <img loading="lazy" src={image} alt="product" onClick={() => this.props.onClickGotoProduct(data)} />
            <div className="div-text">
              <span>
                {type.name}
              </span>
              <span>
                {name}
              </span>
            </div>
          </div>
          <div className="div-button">
            <button
              type="button"
              onClick={() => this.props.onClick(data)}
            >
              {viewBt}
            </button>
          </div>
        </div>
      </div>
    );
  }
}
export default ItemReviewed;
