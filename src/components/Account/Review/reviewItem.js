import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import like from '../../../image/icon/like.png';
import likeRed from '../../../image/icon/like_red.png';
import pen from '../../../image/icon/edit.png';
import { UPDATE_REVIEWS_URL } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrSuccess, toastrError } from '../../../Redux/Helpers/notification';
import { generateUrlWeb, getAltImage, getNameFromCommon } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';

class ReviewItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false,
    };
  }

  onChangeRating = (value) => {
    this.props.data.rating = value;
    this.forceUpdate();
  }

  onChangeText = (e) => {
    const { value, name } = e.target;
    this.props.data[name] = value;
  }

  clickEdit = () => {
    const { isEdit } = this.state;
    this.setState({ isEdit: !isEdit });
    if (isEdit) {
      this.updateReview(this.props.data);
    }
  }

  updateReview = (data) => {
    const {
      id, title, comment, rating,
    } = data;
    const options = {
      url: UPDATE_REVIEWS_URL.replace('{id}', id),
      method: 'PUT',
      body: {
        title,
        comment,
        rating,
      },
    };
    fetchClient(options, true).then((result) => {
      if (!result.isError) {
        toastrSuccess('Updated');
      }
    }).catch((err) => {
      toastrError(err);
    });
  }

  gotoProduct = () => {
    const { data } = this.props;
    const { combo } = data.product;
    const products = _.filter(combo, x => x.product.type === 'Scent');
    if (products && products.length > 1) {
      this.props.history.push(generateUrlWeb(`/products/${products[0].product.id}/${products[1].product.id}`));
    }
  }

  render() {
    const { isEdit } = this.state;
    const { data, cmsCommon } = this.props;
    const {
      title, comment, rating, product,
    } = data;
    const combo = product ? product.combo : undefined;
    const products = combo ? _.filter(combo, x => x.product.type === 'Scent') : [];
    const images = [];
    _.forEach(products, (d) => {
      const image = _.find(d.product.images, x => x.type.toLowerCase() === 'unisex');
      images.push(image);
    });

    const loveItBt = getNameFromCommon(cmsCommon, 'Love_it');
    const saveReview = getNameFromCommon(cmsCommon, 'SAVE_REVIEW');
    const editReview = getNameFromCommon(cmsCommon, 'EDIT_REVIEW');
    const saveBt = getNameFromCommon(cmsCommon, 'SAVE');
    const editBt = getNameFromCommon(cmsCommon, 'EDIT');

    const htmlWeb = (
      <div style={{ maxHeight: '154px', marginBottom: '30px' }} className="div-row justify-center items-center ba br4 pt-3 pr-5 pb-3 pl-5">
        <button
          type="button"
          style={{
            width: '120px',
            height: '120px',
            position: 'relative',
            padding: '0px',
          }}
          className="div-row justify-center button-bg__none"
          onClick={this.gotoProduct}
        >
          <img
            loading="lazy"
            style={{
              position: 'relative',
              width: '50px',
              height: '100px',
              objectFit: 'cover',
              objectPosition: 'center',
              borderTopLeftRadius: '14px',
              borderBottomLeftRadius: '14px',
            }}
            src={images && images.length > 0 ? images[0].image : ''}
            alt={getAltImage(images && images.length > 0 ? images[0].image : undefined)}
          />
          <img
            loading="lazy"
            style={{
              position: 'relative',
              width: '50px',
              height: '100px',
              objectFit: 'cover',
              objectPosition: 'center',
              borderTopRightRadius: '14px',
              borderBottomRightRadius: '14px',
            }}
            src={images && images.length > 1 ? images[1].image : ''}
            alt={getAltImage(images && images.length > 1 ? images[1].image : undefined)}
          />
        </button>
        <div className="div-col pl-3 pr-4" style={{ flex: '1' }}>
          {
            isEdit
              ? (
                <input
                  type="text"
                  name="title"
                  defaultValue={title}
                  onChange={this.onChangeText}
                  style={{
                    border: 'none',
                    borderBottom: '1px solid',
                    width: '50%',
                    fontSize: '1.1rem',
                    color: '#000',
                  }}
                />
              )
              : (
                <span style={{
                  fontSize: '1.1rem',
                  color: '#000',
                }}
                >
                  {title}
                </span>
              )
          }

          <div className="div-row justify-between" style={{ width: '180px' }}>
            <span style={{ color: '#3D3D3D' }}>
              {loveItBt}
            </span>
            <div className="div-row">
              <button
                type="button"
                className="button-bg__none pa0"
                disabled={!isEdit}
                onClick={() => { this.onChangeRating(1); }}
              >
                <img loading="lazy" src={rating >= 1 ? likeRed : like} alt="like1" className="icon_size" />
              </button>
              <button
                type="button"
                disabled={!isEdit}
                className="button-bg__none pa0"
                onClick={() => { this.onChangeRating(2); }}
              >
                <img
                  loading="lazy"
                  src={rating >= 2 ? likeRed : like}
                  alt="like2"
                  className="icon_size"
                />
              </button>
              <button
                type="button"
                disabled={!isEdit}
                className="button-bg__none pa0"
                onClick={() => { this.onChangeRating(3); }}
              >
                <img loading="lazy" src={rating >= 3 ? likeRed : like} alt="like3" className="icon_size" />
              </button>
            </div>
          </div>
          <div style={{ color: '#3D3D3D', marginTop: '5px' }}>
            {isEdit
              ? (
                <textarea
                  type="text"
                  name="comment"
                  defaultValue={comment}
                  onChange={this.onChangeText}
                  style={{
                    width: '100%',
                    height: '40px',
                  }}
                />
              )
              : (
                <span>
                  {comment}
                </span>
              )
              }

          </div>
        </div>
        <div className="div-row">
          <button
            type="button"
            style={{
              width: '125px',
            }}
            className="button-bg__none pa0"
            onClick={this.clickEdit}
          >
            <img loading="lazy" src={pen} alt="pen" style={{ width: '16px', height: '16px', marginRight: '5px' }} />
            <span>
              {isEdit ? saveReview : editReview}
            </span>
          </button>
        </div>
      </div>
    );
    const htmlMobile = (
      <div className="div-row mb-3" style={{ border: '1px solid #CECECE', borderRadius: '14px' }}>
        <div className="div-col, w-25">
          <button
            type="button"
            style={{
              width: '100%',
              height: '130px',
              position: 'relative',
              padding: '0px',
            }}
            className="div-col justify-center button-bg__none"
            onClick={this.gotoProduct}
          >
            <img
              loading="lazy"
              style={{
                position: 'relative',
                width: '100%',
                height: '65px',
                objectFit: 'cover',
                objectPosition: 'center',
                borderTopLeftRadius: '14px',
              }}
              src={images && images.length > 0 ? images[0].image : ''}
              alt={getAltImage(images && images.length > 0 ? images[0].image : undefined)}
            />
            <img
              loading="lazy"
              style={{
                position: 'relative',
                width: '100%',
                height: '65px',
                objectFit: 'cover',
                objectPosition: 'center',
                borderBottomLeftRadius: '14px',
              }}
              src={images && images.length > 1 ? images[1].image : ''}
              alt={getAltImage(images && images.length > 1 ? images[1].image : undefined)}
            />
          </button>
        </div>
        <div className="div-col pl-2 pr-2 justify-around" style={{ flex: '1' }}>
          {
            isEdit
              ? (
                <input
                  type="text"
                  name="title"
                  defaultValue={title}
                  onChange={this.onChangeText}
                  style={{
                    border: 'none',
                    borderBottom: '1px solid',
                    width: '50%',
                    fontSize: '0.8rem',
                    color: '#000',
                  }}
                />
              )
              : (
                <span style={{
                  fontSize: '0.8rem',
                  color: '#000',
                }}
                >
                  {title}
                </span>
              )
          }

          <div className="div-row justify-between">
            <span style={{ color: '#3D3D3D', fontSize: '0.8rem' }}>
              {loveItBt}
            </span>
            <div className="div-row">
              <button
                type="button"
                className="button-bg__none pa0"
                disabled={!isEdit}
                onClick={() => { this.onChangeRating(1); }}
              >
                <img src={rating >= 1 ? likeRed : like} alt="like1" className="icon_size" />
              </button>
              <button
                type="button"
                disabled={!isEdit}
                className="button-bg__none pa0"
                onClick={() => { this.onChangeRating(2); }}
              >
                <img
                  loading="lazy"
                  src={rating >= 2 ? likeRed : like}
                  alt="like2"
                  className="icon_size"
                />
              </button>
              <button
                type="button"
                disabled={!isEdit}
                style={{ fontSize: '0.8rem' }}
                className="button-bg__none pa0"
                onClick={() => { this.onChangeRating(3); }}
              >
                <img loading="lazy" src={rating >= 3 ? likeRed : like} alt="like3" className="icon_size" />
              </button>
            </div>
          </div>
          <div style={{ color: '#3D3D3D', marginTop: '5px' }}>
            {isEdit
              ? (
                <textarea
                  type="text"
                  name="comment"
                  defaultValue={comment}
                  onChange={this.onChangeText}
                  style={{
                    width: '100%',
                    height: '40px',
                  }}
                />
              )
              : (
                <span>
                  {comment}
                </span>
              )
              }

          </div>
        </div>

        <div className="div-row">
          <button
            type="button"
            style={{
              width: '70px',
              height: 'fit-content',
              marginTop: '5px',
            }}
            className="button-bg__none pa0"
            onClick={this.clickEdit}
          >
            <img loading="lazy" src={pen} alt="pen" style={{ width: '12px', height: '12px', marginRight: '5px' }} />
            <span>
              {isEdit ? saveBt : editBt}
            </span>
          </button>
        </div>

      </div>
    );
    return (
      <div>
        {
          isMobile ? (htmlMobile) : (htmlWeb)
        }
      </div>
    );
  }
}

ReviewItem.propTypes = {
  data: PropTypes.shape({
    comment: PropTypes.string,
    rating: PropTypes.number,
    title: PropTypes,
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  cmsCommon: PropTypes.shape().isRequired,
};

export default withRouter(ReviewItem);
