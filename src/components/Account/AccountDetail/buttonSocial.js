import React, { Component } from 'react';
import GoogleLogin from 'react-google-login';
// import FacebookLogin from 'react-facebook-login';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';

import PropTypes from 'prop-types';
import add from '../../../image/icon/add.png';
import sub from '../../../image/icon/sub.png';
import { getNameFromCommon } from '../../../Redux/Helpers';

class ButtonSocial extends Component {
  onClick = () => {
    const { isConnected } = this.props;
    this.props.onChange(!isConnected);
  }

  responseSocial = (response) => {
    // console.log(response);
    const { accessToken } = response;
    if (accessToken) {
      this.props.onChangeToken(accessToken);
    }
  }

  // responseGoogle = (response) => {
  //   console.log(response);
  //   const { accessToken } = response;
  //   if (accessToken) {
  //     this.props.onChangeToken(accessToken);
  //   }
  // }

  renderbutton = (renderProps, title, colorSub, isConnected, name, colorText) => {
    const { cmsCommon } = this.props;
    const notConnectBt = getNameFromCommon(cmsCommon, 'Not_Connected');
    const disconnectBt = getNameFromCommon(cmsCommon, 'DISCONNECT');
    const connectBt = getNameFromCommon(cmsCommon, 'CONNECT');

    return (
      <div>
        <div
          className="div-row"
          style={{
            padding: '35px 20px 35px 20px',
            background: '#5F84E5',
            borderRadius: '14px',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
        >
          <div className="div-col">
            <span className="header-account" style={{ fontSize: '1.2rem', color: colorText }}>
              {title}
            </span>
            <span style={{ color: colorSub }}>
              {isConnected ? name : notConnectBt}
            </span>
          </div>
          <button
            type="button"
            className="button-bg__none"
            onClick={renderProps}
            style={{
              height: '100%',
              color: colorText,
              display: 'inline-flex',
              alignItems: 'center',
            }}
          >
            <img loading="lazy" style={{ width: '12px', marginRight: '5px' }} src={isConnected ? sub : add} alt="icon" />
            { isConnected ? disconnectBt : connectBt}
          </button>
        </div>
      </div>
    );
  }

  render() {
    const { isConnected } = this.props;
    const { title, name } = this.props;
    const isFacebook = title === 'Facebook';
    const colorText = isConnected ? '#fff' : '#000';
    const colorSub = isConnected ? '#fff' : '#00000077';

    return (
      <div>
        {
          isConnected
            ? (this.renderbutton(this.onClick, title, colorSub, isConnected, name, colorText))
            : (
              isFacebook ? (
                <FacebookLogin
                  appId="276035609956267"
                  autoLoad={false}
                  fields="name,email,picture"
                  callback={this.responseSocial}
                  render={renderProps => (
                    this.renderbutton(renderProps.onClick, title, colorSub, isConnected, name, colorText)
                  )}
                />
              ) : (
                <GoogleLogin
                  clientId="1051429470092-1ktl08tfm23q3b7i2au9ivv6pnil51g1.apps.googleusercontent.com"
                  onSuccess={this.responseSocial}
                  onFailure={this.onFailure}
                  render={renderProps => (
                    this.renderbutton(renderProps.onClick, title, colorSub, isConnected, name, colorText)
                  )}
                />
              )
            )
        }
      </div>
    );
  }
}

ButtonSocial.defaultProps = {
  isConnected: false,
};

ButtonSocial.propTypes = {
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  isConnected: PropTypes.bool,
  onChangeToken: PropTypes.func.isRequired,
  cmsCommon: PropTypes.shape().isRequired,
};

export default ButtonSocial;
