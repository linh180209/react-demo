import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { getNameFromCommon } from '../../../Redux/Helpers';
import deleteIcon from '../../../image/icon/delete.png';
import editIcon from '../../../image/icon/edit.png';
import { GET_SHIPPING_URL } from '../../../config';
// import COUNTRIES from '../../../constants/countries';
import { toastrError } from '../../../Redux/Helpers/notification';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import auth from '../../../Redux/Helpers/auth';
import { isMobile } from '../../../DetectScreen';

class AddressBookItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false,
    };
    this.listCountry = [];
  }

  componentDidMount = () => {
    const { isEdit } = this.props;
    this.setState({ isEdit });
    this.fetchShippingInfo();
  };

  onDelete = () => {
    const { data } = this.props;
    this.props.onDelete(data);
  }

  onSave = () => {
    const { data } = this.props;
    if (!this.isValidData(data)) {
      const message = getNameFromCommon(this.props.cmsCommon, 'Please_fill_all_the_information');
      toastrError(message);
      return;
    }
    this.props.onSave(data);
    this.setState({ isEdit: false });
  }

  onEdit = () => {
    // console.log('onEdit');
    this.setState({ isEdit: true });
  }

  onChange = (e) => {
    const { value, name } = e.target;
    if (name === 'country') {
      const valueCountry = _.find(this.listCountry, x => x.code === value);
      this.props.data.country = valueCountry;
      return;
    }
    this.props.data[name] = value;
  }

  onChangeBillAddress = () => {
    if (!this.state.isEdit) {
      return;
    }
    const { is_default_baddress: isDefault } = this.props.data;
    this.props.data.is_default_baddress = !isDefault;
    this.forceUpdate();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { isEdit } = nextProps;
    if (isEdit !== prevState.isEditOld) {
      _.assign(objectReturn, { isEdit, isEditOld: isEdit });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  fetchShippingInfo = () => {
    const options = {
      url: GET_SHIPPING_URL,
      method: 'GET',
    };
    fetchClient(options).then((results) => {
      if (results && !results.isError) {
        this.listCountry = _.map(results, x => x.country);
        this.forceUpdate();
      } else {
        toastrError('Get list shipping');
      }
    }).catch((err) => {
      console.log('err', err);
      toastrError('Get list shipping');
    });
  }

  isValidData = (data) => {
    const {
      country, postal_code: code, street1, phone,
    } = data;
    if (!country || !code || !street1 || !phone) {
      return false;
    }
    return true;
  }

  render() {
    const {
      data, isAddButton, cmsCommon,
    } = this.props;
    const { isEdit } = this.state;
    // console.log('isEdit', isEdit);
    const addNewAddress = getNameFromCommon(cmsCommon, 'ADD_NEW_ADDRESS');
    const nameN = getNameFromCommon(cmsCommon, 'NAME');
    const streetN = getNameFromCommon(cmsCommon, 'STREET');
    const countryN = getNameFromCommon(cmsCommon, 'COUNTRY');
    const zipCodeN = getNameFromCommon(cmsCommon, 'ZIP_CODE');
    const phoneN = getNameFromCommon(cmsCommon, 'PHONE');
    const deleteN = getNameFromCommon(cmsCommon, 'DELETE');
    const saveN = getNameFromCommon(cmsCommon, 'SAVE');
    const editN = getNameFromCommon(cmsCommon, 'EDIT');
    const thisIs = getNameFromCommon(cmsCommon, 'THIS_IS_YOUR_DEFAULT_BILLING_ADDRESS');
    const setAddress = getNameFromCommon(cmsCommon, 'SET_AS_YOUR_DEFAULT_BILLING_ADDRESS');

    const buttonCreate = (
      <div className={isMobile ? 'addressboook-item-mobile' : 'addressboook-item'} style={{ border: '1px dashed gray' }}>
        <button
          type="button"
          className="button-add-new"
          onClick={this.props.clickAddAddress}
        >
          <div>
            <span>
              {addNewAddress}
            </span>
            <i className="fa fa-plus" />
          </div>
        </button>
      </div>
    );
    if (isAddButton) {
      return buttonCreate;
    }
    const {
      name, street1, country, postal_code: code, phone, is_default_baddress: isDefault,
    } = data;
    // console.log('country', country);
    const valueCountry = _.find(this.listCountry, x => x.code === (country ? country.code : ''));
    if (!valueCountry) {
      this.props.data.country = _.find(this.listCountry, x => x.code.toLowerCase() === auth.getCountry());
    }
    console.log('this.props.data.country', this.props.data.country);
    return (
      <div className={isMobile ? 'addressboook-item-mobile' : 'addressboook-item'} style={isDefault ? { border: '1px solid #000' } : {}}>
        <div className="address-content" style={isEdit ? { paddingTop: '20px' } : {}}>
          <div className={isEdit ? 'address-text' : 'address-text-disable'}>
            <input
              type="text"
              placeholder={nameN}
              defaultValue={name}
              name="name"
              disabled={!isEdit}
              onChange={this.onChange}
            />
            <input
              type="text"
              defaultValue={street1}
              placeholder={streetN}
              name="street1"
              disabled={!isEdit}
              onChange={this.onChange}
            />
            {
              isEdit
                ? (
                  <select
                    disabled={!isEdit}
                    name="country"
                    onChange={this.onChange}
                  >
                    {
                  _.map(this.listCountry, d => (
                    <option value={d.code} selected={d.code === country.code}>
                      {d.name}
                    </option>
                  ))
                  }
                  </select>
                ) : (
                  <input
                    type="text"
                    defaultValue={country ? country.name : ''}
                    placeholder={countryN}
                    name="country"
                    disabled={!isEdit}
                  />
                )
            }

            <input
              type="text"
              name="postal_code"
              placeholder={zipCodeN}
              defaultValue={code}
              disabled={!isEdit}
              onChange={this.onChange}
            />
            <input
              type="text"
              name="phone"
              placeholder={phoneN}
              defaultValue={phone}
              disabled={!isEdit}
              onChange={this.onChange}
            />

          </div>
          <div className="address-button">
            <button
              type="button"
              onClick={this.onDelete}
              disabled={isDefault}
              className={isDefault ? 'disabled' : ''}
            >
              <img loading="lazy" src={deleteIcon} alt="deleteIcon" />
              {deleteN}
            </button>
            <button
              type="button"
              className="mt-2"
              onClick={isEdit ? this.onSave : this.onEdit}
            >
              <img loading="lazy" src={editIcon} alt="deleteIcon" />
              {isEdit ? saveN : editN }
            </button>
          </div>
        </div>
        <div className="address-button-bottom">
          <button
            type="button"
            onClick={this.onChangeBillAddress}
          >
            <div>
              <div className={isDefault ? 'point-active' : 'point'} />
              <span>
                {
                  isDefault ? thisIs : setAddress
                }
              </span>
            </div>
          </button>
        </div>
      </div>
    );
  }
}
AddressBookItem.defaultProps = {
  clickAddAddress: undefined,
  isEdit: false,
};

AddressBookItem.propTypes = {
  data: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    street1: PropTypes.string.isRequired,
    country: PropTypes.string.isRequired,
    postal_code: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    is_default_baddress: PropTypes.bool,
  }).isRequired,
  isAddButton: PropTypes.bool.isRequired,
  clickAddAddress: PropTypes.func,
  isEdit: PropTypes.bool,
  onDelete: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  cmsCommon: PropTypes.arrayOf().isRequired,
};

export default AddressBookItem;
