import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import AddressBookItem from './addressBookItem';
import { GET_ADDRESS_URL, DELETE_ADDRESS_URL, UPDATE_ADDRESS_URL } from '../../../config';
import loadingPage from '../../../Redux/Actions/loading';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import { getNameFromCommon } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';

class AddressBook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listAddress: [],
      newAddress: undefined,
    };
  }

  componentDidMount = () => {
    this.fetchAddress();
  };

  onDelete = (data) => {
    const { id } = data;
    if (id) {
      this.deleteAddress(id);
    } else {
      this.setState({ newAddress: undefined });
    }
  }

  onSave = (data) => {
    const { id, is_default_baddress: isDefault } = data;
    const { listAddress } = this.state;
    if (isDefault) {
      _.forEach(listAddress, (x) => {
        if (x.id !== id) {
          x.is_default_baddress = false;
        }
      });
    }
    if (listAddress.length === 0) {
      data.is_default_baddress = true;
    }
    if (!_.find(listAddress, x => x.id === id)) {
      const dataTemp = _.assign({}, data);
      this.postAddress(dataTemp);
    } else {
      this.updateAddress(data);
    }
    this.setState({ listAddress, newAddress: undefined });
  }

  fetchAddress = () => {
    const { user } = this.props.login;
    if (user && user.id) {
      const options = {
        url: GET_ADDRESS_URL.replace('{user_pk}', user.id),
        method: 'GET',
      };
      fetchClient(options, true).then((result) => {
        // console.log('result', result);
        this.setState({ listAddress: result });
      }).catch((err) => {
        toastrError(err);
      });
    }
  }

  postAddress = (data) => {
    const { user } = this.props.login;
    if (user && user.id) {
      const options = {
        url: GET_ADDRESS_URL.replace('{user_pk}', user.id),
        method: 'POST',
        body: data,
      };
      fetchClient(options, true).then((result) => {
        const { listAddress } = this.state;
        listAddress.push(result);
        this.setState({ listAddress });
      });
    }
  }

  updateAddress = (data) => {
    const { user } = this.props.login;
    if (user && user.id) {
      const options = {
        url: UPDATE_ADDRESS_URL
          .replace('{user_pk}', user.id)
          .replace('{id}', data.id),
        method: 'PUT',
        body: data,
      };
      fetchClient(options, true);
    }
  }

  deleteAddress = (id) => {
    const { user } = this.props.login;
    if (user && user.id) {
      const options = {
        url: DELETE_ADDRESS_URL
          .replace('{user_pk}', user.id)
          .replace('{id}', id),
        method: 'DELETE',
      };
      fetchClient(options, true).then(() => {
        const { listAddress } = this.state;
        _.remove(listAddress, x => x.id === id);
        this.setState({ listAddress });
      });
    }
  }

  clickAddAddress = () => {
    const newAddress = {
      country: {
        code: undefined,
        name: undefined,
      },
      postal_code: undefined,
      street1: undefined,
      phone: undefined,
      is_default_baddress: false,
    };
    this.setState({ newAddress });
  }

  render() {
    const { listAddress, newAddress } = this.state;
    const addressBook = getNameFromCommon(this.props.cmsCommon, 'ADDRESS_BOOK');
    return (
      <div className={isMobile ? 'addressbook div-col justify-center items-center' : 'addressbook'}>
        <span>
          {addressBook}
        </span>
        <div className="mt-5 mb-5">
          <Row>
            {
              _.map(listAddress, d => (
                <Col md="6" xs="12" className="mt-3">
                  <AddressBookItem
                    data={d}
                    onDelete={this.onDelete}
                    onSave={this.onSave}
                    cmsCommon={this.props.cmsCommon}
                  />
                </Col>
              ))
            }
            <Col md="6" xs="12" className="mt-3">
              {
                newAddress
                  ? (
                    <AddressBookItem
                      data={newAddress}
                      isEdit
                      onDelete={this.onDelete}
                      onSave={this.onSave}
                      cmsCommon={this.props.cmsCommon}
                    />
                  ) : (
                    <AddressBookItem
                      isAddButton
                      clickAddAddress={this.clickAddAddress}
                      onDelete={this.onDelete}
                      onSave={this.onSave}
                      cmsCommon={this.props.cmsCommon}
                    />
                  )
              }

            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

AddressBook.propTypes = {
  login: PropTypes.shape({
    user: PropTypes.shape(PropTypes.object),
  }).isRequired,
  cmsCommon: PropTypes.shape().isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

const mapDispatchToProps = {
  loadingPage,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddressBook);
