import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import moment from 'moment';
import _ from 'lodash';

import { getNameFromCommon } from '../../../Redux/Helpers';
import InputAccoutDetail from './inputAccoutDetail';
import ButtonSocial from './buttonSocial';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { loginUpdateUserInfo } from '../../../Redux/Actions/login';
import {
  UPDATE_USER_URL, GET_SOCIAL_ACCOUNT_URL, SOCIAL_DISCONNECT_URL, GOOGLE_CONNECT_URL, FACEBOOK_CONNECT_URL,
} from '../../../config';
import { toastrError, toastrSuccess } from '../../../Redux/Helpers/notification';
import { isMobile, isTablet } from '../../../DetectScreen';

const generateDays = (mounth) => {
  let maxday = 30;
  switch (parseInt(mounth, 10)) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      maxday = 31;
      break;
    case 2:
      maxday = 29;
      break;
    default:
      maxday = 30;
      break;
  }
  const dayOptions = [];
  _.forEach(Array(maxday), (i, index) => {
    dayOptions.push({
      name: index + 1,
      value: index + 1,
    });
  });
  return dayOptions;
};

class ChangeDetail extends Component {
  constructor(props) {
    super(props);
    const january = getNameFromCommon(props.cmsCommon, 'JANUARY');
    const february = getNameFromCommon(props.cmsCommon, 'FEBRUARY');
    const march = getNameFromCommon(props.cmsCommon, 'MARCH');
    const april = getNameFromCommon(props.cmsCommon, 'APRIL');
    const may = getNameFromCommon(props.cmsCommon, 'MAY');
    const june = getNameFromCommon(props.cmsCommon, 'JUNE');
    const july = getNameFromCommon(props.cmsCommon, 'JULY');
    const august = getNameFromCommon(props.cmsCommon, 'AUGUST');
    const september = getNameFromCommon(props.cmsCommon, 'SEPTEMBER');
    const october = getNameFromCommon(props.cmsCommon, 'OCTOBER');
    const november = getNameFromCommon(props.cmsCommon, 'NOVEMBER');
    const december = getNameFromCommon(props.cmsCommon, 'DECEMBER');
    const months = [
      {
        name: january,
        value: 1,
      },
      {
        name: february,
        value: 2,
      },
      {
        name: march,
        value: 3,
      },
      {
        name: april,
        value: 4,
      },
      {
        name: may,
        value: 5,
      },
      {
        name: june,
        value: 6,
      },
      {
        name: july,
        value: 7,
      },
      {
        name: august,
        value: 8,
      },
      {
        name: september,
        value: 9,
      },
      {
        name: october,
        value: 10,
      },
      {
        name: november,
        value: 11,
      },
      {
        name: december,
        value: 12,
      },
    ];
    this.state = {
      dayOptions: [],
      mounthOptions: months,
      yearOptions: [],
      data: {},
      // currentPassword: '',
      newPassword: '',
      repeatNewPassword: '',
      isConnectFacebook: false,
      isConnectGoogle: false,
    };
    this.dataGoogle = undefined;
    this.dataFacebook = undefined;
  }

  componentDidMount = () => {
    // let maxday = 30;
    // switch (parseInt(this.state.mounth, 10)) {
    //   case 1:
    //   case 3:
    //   case 5:
    //   case 7:
    //   case 8:
    //   case 10:
    //   case 12:
    //     maxday = 31;
    //     break;
    //   case 2:
    //     maxday = 29;
    //     break;
    //   default:
    //     maxday = 30;
    //     break;
    // }
    const { mounth } = this.state;
    const dayOptions = generateDays(mounth);
    // _.forEach(Array(maxday), (i, index) => {
    //   dayOptions.push({
    //     name: index + 1,
    //     value: index + 1,
    //   });
    // });
    const yearOptions = [];
    for (let i = 1900; i < parseInt(new Date().getFullYear(), 10); i += 1) {
      yearOptions.push({
        name: i,
        value: i,
      });
    }
    const data = this.initalData(this.props.login.user);
    this.fetchSocialAccount();
    this.setState({ dayOptions, yearOptions, data });
  };

  componentDidUpdate(prevProps, prevState) {
    const { login } = prevProps;
    const { oldLogin } = this.state;
    if (login !== oldLogin) {
      const data = this.initalData(oldLogin?.user);
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ data });
    }
  }

  onChange = (value, name) => {
    // console.log({ value, name });
    this.state.data[name] = value;
    if (name === 'mounth') {
      const dayOptions = generateDays(value);
      this.setState({ dayOptions });
    }
  }

  onSave = () => {
    const { data } = this.state;
    // console.log('data', data);
    this.saveUser(data);
  }

  onClickFemale = (gender) => {
    const { data } = this.state;
    data.gender = gender;
    this.setState({ data });
  }

  onClickMale = (gender) => {
    const { data } = this.state;
    data.gender = gender;
    this.setState({ data });
  }

  onChangePassword = (value, name) => {
    this.state[name] = value;
  }

  onChangeFacebook = (isConnected) => {
    if (!isConnected) {
      if (!this.dataFacebook) {
        return;
      }
      const { id } = this.dataFacebook;
      this.callDisconnectSocial(id, true);
    }
  }

  onChangeGoogle = (isConnected) => {
    if (!isConnected) {
      if (!this.dataGoogle) {
        return;
      }
      const { id } = this.dataGoogle;
      this.callDisconnectSocial(id, false);
    }
  }

  onChangeTokenFaceBook = (token) => {
    this.connectSocial(token, true);
  }

  onChangeTokenGoogle = (token) => {
    this.connectSocial(token, false);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { login } = nextProps;
    if (login !== prevState.oldLogin) {
      _.assign(objectReturn, { oldLogin: login });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  callDisconnectSocial = (id, isFaceBook) => {
    const options = {
      url: SOCIAL_DISCONNECT_URL.replace('{id}', id),
      method: 'POST',
    };

    fetchClient(options, true).then((result) => {
      // console.log('result', result);
      const { isError } = result;
      if (!isError) {
        toastrSuccess('Disconnected Social');
        if (isFaceBook) {
          this.setState({ isConnectFacebook: false });
        } else {
          this.setState({ isConnectGoogle: false });
        }
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err);
    });
  }

  fetchSocialAccount = () => {
    const options = {
      url: GET_SOCIAL_ACCOUNT_URL,
      method: 'GET',
    };
    fetchClient(options, true).then((result) => {
      // console.log('result', result);
      if (result && result.length > 0) {
        this.dataFacebook = _.find(result, x => x.provider === 'facebook');
        this.dataGoogle = _.find(result, x => x.provider === 'google');
        this.setState({
          isConnectFacebook: !_.isEmpty(this.dataFacebook),
          isConnectGoogle: !_.isEmpty(this.dataGoogle),
        });
      }
      // const account = _.find(result, result.)
    });
  }

  initalData = (user) => {
    const {
      date_of_birth: dob, email, first_name: firstName, last_name: lastName, gender,
    } = user || {};
    const momentDob = dob && moment(dob).isValid ? moment(dob) : moment().year(1990).month(1).date(1);
    const data = {
      date: momentDob.date(),
      mounth: momentDob.month(),
      year: momentDob.year(),
      email,
      firstName,
      lastName,
      gender,
    };
    return data;
  }

  saveUser = (data) => {
    const {
      firstName, lastName, email, date, mounth, year, gender,
    } = data;
    const { newPassword, repeatNewPassword } = this.state;
    if ((newPassword || repeatNewPassword) && newPassword !== repeatNewPassword) {
      toastrError('Password and Confirm password does not match!');
      return;
    }
    const epochDob = moment().year(year).month(mounth).date(date);
    const body = {
      first_name: firstName || undefined,
      last_name: lastName || undefined,
      email,
      date_of_birth: parseInt(epochDob.valueOf() / 1000, 10),
      gender: gender || undefined,
      password: newPassword || undefined,
    };
    const options = {
      url: UPDATE_USER_URL.replace('{id}', this.props.login.user.id),
      method: 'PUT',
      body,
    };
    fetchClient(options, true).then((result) => {
      const { isError } = result;
      if (!isError) {
        toastrSuccess('Save');
        body.date_of_birth = epochDob.toISOString();
        this.props.loginUpdateUserInfo(body);
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err.message);
    });
  }

  connectSocial = (token, isFaceBook) => {
    const options = {
      url: isFaceBook ? FACEBOOK_CONNECT_URL : GOOGLE_CONNECT_URL,
      method: 'POST',
      body: {
        access_token: token,
      },
    };
    fetchClient(options, true).then((result) => {
      const { isError } = result;
      if (!isError) {
        toastrSuccess('');
        if (isFaceBook) {
          this.setState({ isConnectFacebook: true });
        } else {
          this.setState({ isConnectGoogle: true });
        }
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err);
    });
  }

  render() {
    const {
      dayOptions, mounthOptions, yearOptions, data, isConnectFacebook, isConnectGoogle,
    } = this.state;
    const { login, cmsCommon } = this.props;
    const { user } = login;
    if (!user || !data) {
      return (<div />);
    }
    // console.log('data', data);
    const userName = `${login.user.first_name} ${login.user.last_name}`;

    const FirstName = getNameFromCommon(cmsCommon, 'FIRST_NAME');
    const LastName = getNameFromCommon(cmsCommon, 'LAST_NAME');
    const Email = getNameFromCommon(cmsCommon, 'EMAIL');
    const Female = getNameFromCommon(cmsCommon, 'Female');
    const Male = getNameFromCommon(cmsCommon, 'Male');
    // const CurrentPassword = getNameFromCommon(cmsCommon, 'CURRENT_PASSWORD');
    const NewPassword = getNameFromCommon(cmsCommon, 'NEW_PASSWORD');
    const RepeatNewPassword = getNameFromCommon(cmsCommon, 'REPEAT_NEW_PASSWORD');
    const ChangeDetails = getNameFromCommon(cmsCommon, 'CHANGE_DETAILS').toLowerCase();
    const ChangePassword = getNameFromCommon(cmsCommon, 'CHANGE_PASSWORD').toLowerCase();
    const Socialaccounts = getNameFromCommon(cmsCommon, 'SOCIAL_ACCOUNTS').toLowerCase();
    const saveChangeBt = getNameFromCommon(cmsCommon, 'SAVE_CHANGES');

    return (
      <div className="change-detail" style={isMobile ? { alignItems: 'center', display: 'flex', flexDirection: 'column' } : {}}>
        <span className="header-account">
          {ChangeDetails.charAt(0).toUpperCase() + ChangeDetails.slice(1)}
        </span>
        <div className="mt-4">
          <Row>
            <Col md={isTablet ? '6' : '5'} xs="12" className="mt-3" style={isMobile && !isTablet ? { paddingLeft: '0px', paddingRight: '0px' } : {}}>
              <InputAccoutDetail
                title={FirstName}
                defaultValue={data.firstName}
                name="firstName"
                onChange={this.onChange}
              />
            </Col>
            <Col md={isTablet ? '6' : '5'} xs="12" className="mt-3" style={isMobile && !isTablet ? { paddingLeft: '0px', paddingRight: '0px' } : {}}>
              <InputAccoutDetail
                title={LastName}
                defaultValue={data.lastName}
                name="lastName"
                onChange={this.onChange}
              />
            </Col>
            <Col md={isTablet ? '6' : '5'} xs="12" className="mt-3" style={isMobile && !isTablet ? { paddingLeft: '0px', paddingRight: '0px' } : {}}>
              <InputAccoutDetail
                title={Email}
                defaultValue={data.email}
                name="email"
                onChange={this.onChange}
                disabled
              />
            </Col>
            <Col md={isTablet ? '6' : '5'} xs="12" className="mt-3" style={isMobile && !isTablet ? { paddingLeft: '0px', paddingRight: '0px' } : {}}>
              <Row>
                <Col md="4" xs="4">
                  <InputAccoutDetail
                    title={isMobile && !isTablet ? '' : 'Date of birth'}
                    defaultValue={data.date}
                    name="date"
                    isTypeSelect
                    options={dayOptions}
                    onChange={this.onChange}
                  />
                </Col>
                <Col md="4" xs="4">
                  <InputAccoutDetail
                    title=""
                    defaultValue={data.mounth}
                    name="mounth"
                    isTypeSelect
                    options={mounthOptions}
                    onChange={this.onChange}
                  />
                </Col>
                <Col md="4" xs="4">
                  <InputAccoutDetail
                    title=""
                    defaultValue={data.year}
                    name="year"
                    isTypeSelect
                    options={yearOptions}
                    onChange={this.onChange}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
        <div className="div-row mt-5 mb-5">
          <button
            type="button"
            className="button-bg__none"
            onClick={() => this.onClickFemale(Female)}
          >
            <div className="div-row div-row_v-center">
              <div className={data.gender === Female ? 'point-active' : 'point'} />
              <span className="header-account" style={{ fontSize: '1rem' }}>
                {Female}
              </span>
            </div>
          </button>
          <button
            type="button"
            className="button-bg__none ml-3"
            onClick={() => this.onClickMale(Male)}
          >
            <div className="div-row div-row_v-center">
              <div className={data.gender === Male ? 'point-active' : 'point'} />
              <span className="header-account" style={{ fontSize: '1rem' }}>
                {Male}
              </span>
            </div>
          </button>
        </div>
        <div className="div-col" style={isMobile ? { width: '100%', justifyContent: 'center', alignItems: 'center' } : {}}>
          <span className="header-account">
            {ChangePassword.charAt(0).toUpperCase() + ChangePassword.slice(1)}
          </span>
          {
            isMobile ? (
              <Row className="w-100 mt-3">
                {/* <Col md="6" className="pl-0">
                  <InputAccoutDetail
                    title={CurrentPassword}
                    type="password"
                    defaultValue=""
                    name="currentPassword"
                    onChange={this.onChangePassword}
                  />
                </Col> */}
                <Col md="6" className="pl-0 mt-2">
                  <InputAccoutDetail
                    title={NewPassword}
                    type="password"
                    defaultValue=""
                    name="newPassword"
                    onChange={this.onChangePassword}
                  />
                </Col>
                <Col md="6" className="pl-0 mt-2">
                  <InputAccoutDetail
                    title={RepeatNewPassword}
                    type="password"
                    defaultValue=""
                    name="repeatNewPassword"
                    onChange={this.onChangePassword}
                  />
                </Col>
              </Row>
            )
              : (
                <Col md="5" xs="12" style={{ padding: '0px' }}>
                  <div className="mt-5" />
                  {/* <InputAccoutDetail
                    title={CurrentPassword}
                    type="password"
                    defaultValue=""
                    name="currentPassword"
                    onChange={this.onChangePassword}
                  />
                  <div className="mt-4" /> */}
                  <InputAccoutDetail
                    title={NewPassword}
                    type="password"
                    defaultValue=""
                    name="newPassword"
                    onChange={this.onChangePassword}
                  />
                  <div className="mt-4" />
                  <InputAccoutDetail
                    title={RepeatNewPassword}
                    type="password"
                    defaultValue=""
                    name="repeatNewPassword"
                    onChange={this.onChangePassword}
                  />

                </Col>
              )
          }

        </div>
        <div className="div-col" style={isMobile ? { alignItems: 'center', justifyContent: 'center', width: '100%' } : {}}>
          <span className="header-account mt-5 mb-5">
            {Socialaccounts.charAt(0).toUpperCase() + Socialaccounts.slice(1)}
          </span>
          <Row style={isTablet ? { width: '100%', justifyContent: 'center', alignItems: 'center' } : {}}>
            <Col md="5" xs="12" style={isMobile ? { paddingLeft: '0px', paddingRight: '0px' } : {}}>
              <ButtonSocial
                title="Facebook"
                name={userName}
                onChange={this.onChangeFacebook}
                isConnected={isConnectFacebook}
                onChangeToken={this.onChangeTokenFaceBook}
                cmsCommon={cmsCommon}
              />
            </Col>
            <Col md="5" xs="12" style={isMobile && !isTablet ? { paddingLeft: '0px', paddingRight: '0px', marginTop: '15px' } : {}}>
              <ButtonSocial
                title="Google"
                name={userName}
                onChange={this.onChangeGoogle}
                isConnected={isConnectGoogle}
                onChangeToken={this.onChangeTokenGoogle}
                cmsCommon={cmsCommon}
              />
            </Col>
          </Row>
        </div>
        <button
          type="button"
          className="button-bg__none mt-5 mb-5"
          style={{
            border: '1px solid #00000033',
            padding: '10px 30px 10px 30px',
            borderRadius: '10px',
          }}
          onClick={this.onSave}
        >
          {saveChangeBt}
        </button>
      </div>
    );
  }
}

ChangeDetail.propTypes = {
  login: PropTypes.shape({
    user: PropTypes.shape({
      date_of_birth: PropTypes.string,
      email: PropTypes.string,
      first_name: PropTypes.string,
      last_name: PropTypes.string,
      id: PropTypes.string,
    }),
  }).isRequired,
  cmsCommon: PropTypes.shape().isRequired,
  loginUpdateUserInfo: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

const mapDispatchToProps = {
  loginUpdateUserInfo,
};
export default connect(mapStateToProps, mapDispatchToProps)(ChangeDetail);
