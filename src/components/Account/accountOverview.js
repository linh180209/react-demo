import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';

import { withRouter } from 'react-router-dom';
import '../../../node_modules/slick-carousel/slick/slick-theme.css';
import '../../../node_modules/slick-carousel/slick/slick.css';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { GET_USER_OUTCOME } from '../../config';
import { getNameFromCommon, generateUrlWeb, getSearchPathName } from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import IngredientPopUp from '../IngredientDetail/ingredientPopUp';
import ResultNew from '../../views/ResultScent/resultNew';
import { toastrError } from '../../Redux/Helpers/notification';

class AccountOverview extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isTested: false,
      description: '',
      titlePersonality: '',
      isShowKnowMore: false,
      personalityChoice: {},
      dataPopup: {
        title: '',
        content: '',
        imageKnowMore: '',
      },
      outComeData: undefined,
    };
  }

  componentDidMount = () => {
    const { user } = auth.getLogin();
    if (user) {
      this.fetchOutCome(user.id);
    }
  };

  onClickImage = (isClickNightPerfume) => {
    const { cmsCommon } = this.props;
    const dayPerfumebt = getNameFromCommon(cmsCommon, 'Day_perfume');
    const nightPerfumebt = getNameFromCommon(cmsCommon, 'Night_perfume');
    const { outComeData } = this.state;
    const productDay = _.filter(outComeData[0].combo, x => x.product.type === 'Scent') || [];
    const productNight = _.filter(outComeData[1].combo, x => x.product.type === 'Scent') || [];
    const dayPro = {
      name: dayPerfumebt,
      ids: [productDay[0].product.id, productDay[1].product.id],
      images: [],
    };
    const nightPro = {
      name: nightPerfumebt,
      ids: [productNight[0].product.id, productNight[1].product.id],
      images: [],
    };
    const queryProducts = isClickNightPerfume ? [nightPro, dayPro] : [dayPro, nightPro];
    const queryString = encodeURIComponent(JSON.stringify(queryProducts));
    this.props.history.push(
      generateUrlWeb(`/products-questionnaire?products=${queryString}`),
    );
  };

  onClickAddToCart = (product, currentItem) => {
    const { basket } = this.props;
    const { name } = product;
    const data = {
      item: currentItem.id,
      price: currentItem.price,
      is_featured: currentItem.is_featured,
      quantity: 1,
      name,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    if (!basket.id) {
      this.props.createBasketGuest(dataTemp);
    } else {
      this.props.addProductBasket(dataTemp);
    }
  }

  onOpenKnowMore = (title, content, imageKnowMore) => {
    this.setState({
      isShowKnowMore: true,
      dataPopup: { title, content, imageKnowMore },
    });
  };

  onCloseKnowMore = () => {
    this.setState({ isShowKnowMore: false });
  }

  gotoTest = () => {
    const { pathname, search } = getSearchPathName(generateUrlWeb('/quiz'));
    this.props.history.push({
      pathname,
      search,
      state: { isRetake: true },
    });
  };

  fetchOutCome = (userId) => {
    this.props.loadingPage(true);
    const options = {
      url: GET_USER_OUTCOME.replace('{user}', userId),
      method: 'GET',

    };
    fetchClient(options, true).then((result) => {
      if (result && !result.isError) {
        const { products_day: prodDays } = result;
        if (_.isEmpty(prodDays)) {
          this.setState({ isTested: false });
          this.props.loadingPage(false);
          return;
        }
      }
      this.setState({ outComeData: result, isTested: true });
      this.props.loadingPage(false);
      // if (results && results.length > 0) {
      //   this.props.loadingPage(false);
      //   const lastResult = results[results.length - 1];
      //   console.log('lastResult', lastResult);
      //   const { products_day: prodDays, products_night: prodNights, personality_choice: personalityChoice } = lastResult;

      //   if (prodDays && prodNights && prodDays.length > 0 && prodNights.length > 0) {
      //     const description = personalityChoice ? personalityChoice.personality.description : '';
      //     const titlePersonality = personalityChoice ? personalityChoice.personality.title : '';
      //     this.setState({
      //       outComeData: [prodDays[0], prodNights[0]], personalityChoice, description, titlePersonality, isTested: true,
      //     });
      //   } else {
      //     this.setState({ isTested: false });
      //   }
      // } else {
      //   this.setState({ isTested: false });
      //   this.props.loadingPage(false);
      // }
    }).catch((err) => {
      toastrError(err);
      this.props.loadingPage(false);
    });
  }

  onClickIngredient = (ingredientDetail) => {
    this.setState({ ingredientDetail, isShowIngredient: true });
  }

  onCloseIngredient = () => {
    this.setState({ isShowIngredient: false });
  }

  render() {
    const {
      isTested, outComeData, description, titlePersonality,
      isShowIngredient, ingredientDetail,
    } = this.state;
    const settings = {
      infinite: true,
      swipeToSlide: true,
      speed: 500,
      arrows: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      initialSlide: 1,
      centerMode: true,
      centerPadding: '0px',
    };
    const accountBt = getNameFromCommon(this.props.cmsCommon, 'Account_overview');
    const testAgainBt = getNameFromCommon(this.props.cmsCommon, 'TEST_AGAIN');
    const recommendBt = getNameFromCommon(this.props.cmsCommon, 'Recommended_for_you');
    const youDoneCompleteBt = getNameFromCommon(this.props.cmsCommon, 'You_have_not_completed');
    const youPerfectBt = getNameFromCommon(this.props.cmsCommon, 'your_perferct_scent');
    const findBt = getNameFromCommon(this.props.cmsCommon, 'FIND_MY_SCENT');
    const dayPerfumebt = getNameFromCommon(this.props.cmsCommon, 'Day_perfume');
    const nightPerfumebt = getNameFromCommon(this.props.cmsCommon, 'Night_perfume');
    const freshbt = getNameFromCommon(this.props.cmsCommon, 'FRESH_AND_ELEGANT');
    const seducetivebt = getNameFromCommon(this.props.cmsCommon, 'SEDUCTIVE_AND_ATTRACTIVE');
    const yourPersonalityBt = getNameFromCommon(this.props.cmsCommon, 'YOUR_PERSONALITY');
    const yourScentBt = getNameFromCommon(this.props.cmsCommon, 'YOUR_SCENT_STYLE');
    return (
      <div className="account-overview">
        <IngredientPopUp
          isShow={isShowIngredient}
          data={ingredientDetail || {}}
          onCloseIngredient={this.onCloseIngredient}
          history={this.props.history}
        />
        <h1 className="tc">
          {accountBt}
        </h1>
        {
          isTested
            ? (
              <ResultNew outComeData={outComeData} isAccountOutcome />
            ) : (
              <div className="no-product-overview">
                <span className="mt-5 mb-5">
                  {recommendBt}
                </span>
                <span>
                  {youDoneCompleteBt}
                  <br />
                  {youPerfectBt}
                </span>
                <div>
                  <button
                    type="button"
                    className="mt-5 mb-5"
                    onClick={this.gotoTest}
                  >
                    {findBt}
                  </button>
                </div>
              </div>
            )
        }
      </div>
    );
  }
}

AccountOverview.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  loadingPage: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  basket: PropTypes.shape(PropTypes.object).isRequired,
  cmsCommon: PropTypes.shape(PropTypes.object).isRequired,
  cms: PropTypes.shape(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
};

export default withRouter(AccountOverview);
