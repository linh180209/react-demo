import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import AvatarEditor from 'react-avatar-editor';

import avatar from '../../image/img-profile.png';
import editAvatar from '../../image/icon/pen.svg';
import account from '../../image/icon/account.svg';
import gear from '../../image/icon/gear.svg';
import icSwitch from '../../image/icon/icSwitchAcc.svg';
import heart from '../../image/icon/heart.svg';
import vector from '../../image/icon/vector.svg';
import reward from '../../image/icon/reward.svg';
import link from '../../image/icon/link.svg';
import logout from '../../image/icon/logout.svg';
import icVip from '../../image/icon/ic-vip.svg';
import loadingPage from '../../Redux/Actions/loading';
import ItemMenu from './itemMenu';
import AccountOverview from './accountOverview';
import { logoutRequest, loginUpdateUserInfo } from '../../Redux/Actions/login';
import AddressBook from './AccountDetail/addressBook';
import ContactPreferences from './ContactPreferences';
import ChangeDetail from './AccountDetail/changeDetail';
import ReferAFriend from './ReferAFriend';
import MyOrder from './MyOrder';
import Reviews from './Review';
import Subscription from './Subscription';
import ReWards from './ReWards';
import { UPDATE_USER_URL } from '../../config';
import { fetchClientFormData } from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import { createBasketGuest, addProductBasket } from '../../Redux/Actions/basket';
import {
  generateUrlWeb, getAltImage, getNameFromCommon, gotoShopHome, segmentAccountDashboardTabSelected, trackGTMLogout,
} from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import menu from '../HomePage/menu';
import { isMobile, isTablet } from '../../DetectScreen';

class AccountB2C extends Component {
  constructor(props) {
    super(props);

    this.state = {
      avatarCropUrl: undefined,
      isEditAvatar: false,
      nameActive: undefined,
      nameSubActive: '',
      content: undefined,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { params } = nextProps;
    if (params && params !== prevState.params) {
      return ({ params, isGotoPage: true });
    }
    return null;
  }

  // componentDidMount() {
  //   const { params } = this.props;
  //   if (params && params.type) {
  //     const { type, idItem } = params;
  //    // this.gotoSubMenu(type, idItem);
  //   }
  // }

  componentDidUpdate() {
    const { params } = this.props;
    // if (!_.isEmpty(params) && (JSON.stringify(prevProps.params) !== JSON.stringify(params) || !this.state.nameActive)) {
    if (this.state.isGotoPage) {
      const { type, idItem } = params;
      this.gotoSubMenu(type, idItem);
      this.state.isGotoPage = false;
    }
  }


  gotoSubMenu = (type, idItem) => {
    const { cmsCommon } = this.props;
    const accOverview = getNameFromCommon(cmsCommon, 'ACCOUNT_OVERVIEW');
    const accDetails = getNameFromCommon(cmsCommon, 'ACCOUNT_DETAILS');
    const accReview = getNameFromCommon(cmsCommon, 'REVIEWS');
    const accAddressBook = getNameFromCommon(cmsCommon, 'ADDRESS_BOOK');
    const accRewards = getNameFromCommon(cmsCommon, 'MY_REWARDS');
    const accChangeDetails = getNameFromCommon(cmsCommon, 'CHANGE_DETAILS');
    const accMyOder = getNameFromCommon(cmsCommon, 'MY_ORDER');
    const accReferAFriend = getNameFromCommon(cmsCommon, 'REFER_A_FRIEND');
    const mySubscriptionBt = getNameFromCommon(cmsCommon, 'my_subscription');
    const listMenu = this.generateComponent(cmsCommon);
    if (type === 'account-overview') {
      
      this.setState({
        nameActive: accOverview,
        nameSubActive: undefined,
        content: _.find(listMenu, x => x.name === accOverview).content,
      });
    } else if (type === 'change-details') {
      this.setState({
        nameActive: accDetails,
        nameSubActive: accChangeDetails,
        subMenu: _.find(listMenu, x => x.name === accDetails).subMenu,
        content: (<ChangeDetail cmsCommon={cmsCommon} />),
      });
    } else if (type === 'address-book') {
      this.setState({
        nameActive: accDetails,
        nameSubActive: accAddressBook,
        subMenu: _.find(listMenu, x => x.name === accDetails).subMenu,
        content: (<AddressBook cmsCommon={cmsCommon} />),
      });
    } else if (type === 'my-order') {
      this.setState({
        nameActive: accMyOder,
        nameSubActive: undefined,
        content: _.find(listMenu, x => x.name === accMyOder).content,
      });
    } else if (type === 'reviews') {
      this.setState({
        nameActive: accReview,
        nameSubActive: undefined,
        content: _.find(listMenu, x => x.name === accReview).content,
      });
    } else if (type === 'refer-a-friend') {
      this.setState({
        nameSubActive: undefined,
        nameActive: accReferAFriend,
        content: _.find(listMenu, x => x.name === accReferAFriend).content,
      });
    } else if (type === 'my-rewards') {
      this.setState({
        nameSubActive: undefined,
        nameActive: accRewards,
        content: _.find(listMenu, x => x.name === accRewards).content,
      });
    } else if (type === 'my-subscription') {
      this.setState({
        nameSubActive: undefined,
        nameActive: mySubscriptionBt,
        content: _.find(listMenu, x => x.name === mySubscriptionBt).content,
      });
    }
    segmentAccountDashboardTabSelected({account_dashboard_tab_name:type});
  };

  generateComponent = (cmsCommon) => {
    const accOverview = getNameFromCommon(cmsCommon, 'ACCOUNT_OVERVIEW');
    const accDetails = getNameFromCommon(cmsCommon, 'ACCOUNT_DETAILS');
    const accChangeDetails = getNameFromCommon(cmsCommon, 'CHANGE_DETAILS');
    const accAddressBook = getNameFromCommon(cmsCommon, 'ADDRESS_BOOK');
    const accMyOder = getNameFromCommon(cmsCommon, 'MY_ORDER');
    const accReview = getNameFromCommon(cmsCommon, 'REVIEWS');
    const accRewards = getNameFromCommon(cmsCommon, 'MY_REWARDS');
    const accContactPre = getNameFromCommon(cmsCommon, 'CONTACT_PREFERENCES');
    const accReferAFriend = getNameFromCommon(cmsCommon, 'REFER_A_FRIEND');
    const mySubscriptionBt = getNameFromCommon(cmsCommon, 'my_subscription');
    this.accLogout = getNameFromCommon(cmsCommon, 'LOGOUT');
    return [
      {
        urlIcon: account,
        name: accOverview,
        link: '/account/account-overview',
        content: (<AccountOverview
          loadingPage={this.props.loadingPage}
          login={this.props.login}
          createBasketGuest={this.props.createBasketGuest}
          addProductBasket={this.props.addProductBasket}
          basket={this.props.basket}
          cmsCommon={cmsCommon}
          onClickIngredient={this.props.onClickIngredient}
          addCmsRedux={this.props.addCmsRedux}
        />),
      },
      {
        urlIcon: gear,
        name: accDetails,
        link: '/account/account-details',
        subMenu: [
          {
            name: accChangeDetails,
            content: (<ChangeDetail cmsCommon={cmsCommon} />),
            link: '/account/change-details',
          },
          // {
          //   name: 'PAYMENT METHODS',
          //   content: (<div />),
          // },
          {
            name: accAddressBook,
            content: (<AddressBook cmsCommon={cmsCommon} />),
            link: '/account/address-book',
          },
        ],
      },
      // {
      //   urlIcon: promote,
      //   name: 'PROMOTE MY CREATE',
      // },
      // {
      //   urlIcon: heart,
      //   name: 'MY FAVOURITES DETAILS',
      // },
      {
        urlIcon: vector,
        name: accMyOder,
        link: '/account/my-order',
        content: (<MyOrder
          login={this.props.login}
          createBasketGuest={this.props.createBasketGuest}
          addProductBasket={this.props.addProductBasket}
          basket={this.props.basket}
          cmsCommon={cmsCommon}
          loadingPage={this.props.loadingPage}
          cms={this.props.cms}
          addCmsRedux={this.props.addCmsRedux}

        />),
      },
      {
        urlIcon: icSwitch,
        name: mySubscriptionBt,
        link: '/account/my-subscription',
        content: (<Subscription
          cmsCommon={cmsCommon}
          loadingPage={this.props.loadingPage}
          addCmsRedux={this.props.addCmsRedux}
          cms={this.props.cms}
          login={this.props.login}
          history={this.props.history}

        />),
      },
      {
        urlIcon: heart,
        name: accReview,
        link: '/account/reviews',
        content: (<Reviews
          cmsCommon={cmsCommon}
          loadingPage={this.props.loadingPage}
          cms={this.props.cms}
          addCmsRedux={this.props.addCmsRedux}
          login={this.props.login}
          history={this.props.history}

        />),
      },
      {
        urlIcon: reward,
        name: accRewards,
        link: '/account/my-rewards',
        content: (<ReWards
          login={this.props.login}
          loadingPage={this.props.loadingPage}
          cms={this.props.cms}
          addCmsRedux={this.props.addCmsRedux}

        />),
      },
      // {
      //   urlIcon: contact,
      //   name: accContactPre,
      //   content: (<ContactPreferences cmsCommon={cmsCommon} />),
      // },
      // {
      //   urlIcon: gift,
      //   name: 'GIFT CARD AND VOUCHERS',
      // },
      {
        urlIcon: link,
        name: accReferAFriend,
        link: '/account/refer-a-friend',
        content: (<ReferAFriend
          cmsCommon={cmsCommon}
          loadingPage={this.props.loadingPage}
          cms={this.props.cms}
          addCmsRedux={this.props.addCmsRedux}
          login={this.props.login}
          history={this.props.history}
        />),
      },
      {
        urlIcon: logout,
        name: this.accLogout,
      },
    ];
  };

  onClick = (e) => {
    const {
      nameActive, nameSubActive, content, link: nLink, subMenuLink,
    } = e;
    if (nameActive === this.accLogout) {
      const { login } = this.props;
      const { user } = login;
      trackGTMLogout(user.id);

      this.props.logoutRequest();
      gotoShopHome();
      return;
    }

    this.props.history.push(generateUrlWeb(subMenuLink || nLink));
    // this.setState({ nameActive, nameSubActive, content });
  }

  onSelectFile = (e) => {
    if (e.target.files && e.target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => this.setState({ avatarCropUrl: reader.result, isEditAvatar: true }));
      reader.readAsDataURL(e.target.files[0]);
    }
  }

  onClickAvatar = async () => {
    const { isEditAvatar, avatarCropUrl } = this.state;
    const { user } = this.props.login;
    if (isEditAvatar) {
      const avatarData = avatarCropUrl ? await fetch(avatarCropUrl).then(r => r.blob()) : undefined;
      const formData = new FormData();
      formData.append('avatar', new File([avatarData], 'avatar.png'));
      const options = {
        url: UPDATE_USER_URL.replace('{id}', user.id),
        method: 'PUT',
        body: formData,
      };
      fetchClientFormData(options, true).then((result) => {
        if (result && !result.isError) {
          toastrSuccess('Update avatar');
          this.props.loginUpdateUserInfo({ avatar: result.avatar });
          this.setState({ isEditAvatar: false });
        } else {
          toastrError('Update avatar');
        }
      }).catch((err) => {
        toastrError(err.message);
      });
    } else {
      const element = document.getElementById('inputFile');
      if (element) {
        element.click();
      }
    }
  }

  render() {
    const {
      nameActive: nameActiveT, nameSubActive, content: contentT, avatarCropUrl, isEditAvatar,
    } = this.state;
    const menus = this.generateComponent(this.props.cmsCommon);
    const content = contentT || (menus && menus.length > 0 ? menus[0].content : undefined);
    const nameActive = nameActiveT || (menus && menus.length > 0 ? menus[0].name : '');
    const dataUser = _.isEmpty(this.props.login) ? auth.getLogin() : this.props.login;
    if (!dataUser) {
      return <div />;
    }
    const { user } = dataUser;
    const nameDisplay = `${user.first_name ? user.first_name : ''} ${user.last_name ? user.last_name : ''}`;
    // const imgAvatar = auth.getUrlAvatar() ? auth.getUrlAvatar() : user && user.avatar ? user.avatar : user && user.avatar_social ? user.avatar_social : avatar;
    const imgAvatar = user && user.avatar ? user.avatar : user && user.avatar_social ? user.avatar_social : avatar;
    const getRewards = getNameFromCommon(this.props.cmsCommon, 'GET_REWARDS');
    const getPointer = getNameFromCommon(this.props.cmsCommon, 'GET_MORE_POINTS');
    const htmlWeb = (
      <div className="account-b2c">
        <div className="menu">
          <div className="information pb-3">
            <div className="avatar" style={{ position: 'relative' }}>
              <img loading="lazy" src={imgAvatar} alt={getAltImage(imgAvatar)} />
              {
                isEditAvatar
                  ? (
                    <div style={{
                      position: 'absolute', top: '-50%', width: '200px', height: '200px', right: '-10px',
                    }}
                    >
                      <AvatarEditor
                        image={avatarCropUrl}
                        width={100}
                        height={100}
                        border={50}
                        color={[255, 255, 255, 0.6]} // RGBA
                        scale={1}
                        rotate={0}
                        borderRadius={50}
                        className="account-avatar"
                      />
                    </div>
                  ) : (<div />)
              }
              <button
                type="button"
                className="bt-boder-none button-edit"
                onClick={this.onClickAvatar}
              >
                <input id="inputFile" type="file" onChange={this.onSelectFile} style={{ display: 'none' }} />
                {
                  isEditAvatar ? (<i className="far fa-save" style={{ fontSize: '1.5rem', color: 'gray' }} />) : (<img src={editAvatar} alt="editAvatar" />)
                }
              </button>
            </div>
            <div className="text ml-3">
              <span className="name">
                {nameDisplay}
                {
                  user?.is_vip && (
                    <div className="icon-vip">
                      <span>VIP</span>
                      <img src={icVip} alt="vip" />
                    </div>
                  )
                }
              </span>

              {/* <div className="point">
                <img src={money} alt="money" />
                <span className="ml-2 mr-2">
                  <strong>
                    {user ? user.points : ''}
                  </strong>
                </span>
                <span>
                  POINTs
                </span>
              </div> */}
              <div className="button div-row justify-between">
                <button
                  type="button"
                  className="bt-boder-none hidden"
                >
                  { getRewards }
                </button>
                <button
                  type="button"
                  className="bt-boder-none hidden"
                >
                  {getPointer}
                </button>
              </div>
            </div>
          </div>
          <div className="list-menu">
            {
              _.map(menus, m => (
                <ItemMenu
                  urlIcon={m.urlIcon}
                  name={m.name}
                  subMenu={m.subMenu}
                  onClick={this.onClick}
                  nameActive={nameActive}
                  nameSubActive={nameSubActive}
                  content={m.content}
                  link={m.link}
                />
              ))
            }
            <div style={{
              height: '100px',
              background: '#F5ECDC',
            }}
            />
          </div>
        </div>
        <div className="content">
          {content}
        </div>
      </div>
    );
    const htmlMobile = (
      <div className="account-b2c-mobile">
        <div className="menu">
          <div className={isTablet ? 'information pb-4 pl-4 pr-4 pt-4' : 'information pb-3 pl-2 pr-2 pt-3'}>
            <div className="avatar" style={isTablet ? { position: 'relative', width: '140px' } : { position: 'relative' }}>
              <img loading="lazy" src={imgAvatar} alt={getAltImage(imgAvatar)} style={isTablet ? { width: '100px', height: '100px' } : {}} />
              {
                isEditAvatar
                  ? (
                    <div style={isTablet
                      ? {
                        position: 'absolute', top: '-50%', width: '200px', height: '200px', right: '-10px',
                      } : {
                        position: 'absolute', top: '-70%', width: '150px', height: '150px', right: '9px',
                      }}
                    >
                      <AvatarEditor
                        image={avatarCropUrl}
                        width={isTablet ? 100 : 80}
                        height={isTablet ? 100 : 80}
                        border={50}
                        color={[255, 255, 255, 0.6]} // RGBA
                        scale={1}
                        rotate={0}
                        borderRadius={50}
                        className="account-avatar"
                      />
                    </div>
                  ) : (<div />)
              }
              <button
                type="button"
                className="bt-boder-none button-edit"
                onClick={this.onClickAvatar}
                style={isTablet ? {
                  width: '45px',
                  height: '45px',
                } : {}}
              >
                <input id="inputFile" type="file" onChange={this.onSelectFile} style={{ display: 'none' }} />
                {
                  isEditAvatar ? (<i className="far fa-save" style={{ fontSize: '1.5rem', color: 'gray' }} />) : (<img src={editAvatar} alt="editAvatar" />)
                }
              </button>
            </div>
            <div className="text ml-3" style={isTablet ? { height: '100px' } : {}}>
              <span className="name" style={isTablet ? { fontSize: '1.2rem' } : {}}>
                {nameDisplay}
                {
                  user?.is_vip && (
                    <div className="icon-vip">
                      <span>VIP</span>
                      <img src={icVip} alt="vip" />
                    </div>
                  )
                }
              </span>
              {/* <div className="point">
                <img src={money} alt="money" />
                <span className="ml-2 mr-2" style={isTablet ? { fontSize: '1.1rem' } : {}}>
                  <strong>
                    {user ? user.points : ''}
                  </strong>
                </span>
                <span style={isTablet ? { fontSize: '1.1rem' } : {}}>
                  POINTs
                </span>
              </div> */}
              <div className="button div-row justify-between" style={isTablet ? { width: '280px' } : {}}>
                <button
                  type="button"
                  className="bt-boder-none hidden"
                  style={isTablet ? { fontSize: '1rem' } : {}}
                >
                  {getRewards}
                </button>
                <button
                  type="button"
                  className="bt-boder-none hidden"
                  style={isTablet ? { fontSize: '1rem' } : {}}
                >
                  {getPointer}
                </button>
              </div>
            </div>
          </div>
          <div className="list-menu">
            {
              _.map(menus, m => (
                <div>
                  <ItemMenu
                    urlIcon={m.urlIcon}
                    name={m.name}
                    subMenu={m.subMenu}
                    onClick={this.onClick}
                    nameActive={nameActive}
                    nameSubActive={nameSubActive}
                    content={m.content}
                    link={m.link}
                  />
                  {/* {content} */}
                </div>
              ))
            }
            <div style={{
              height: '100px',
              background: '#f8f8f8',
            }}
            />
          </div>
        </div>
      </div>
    );
    return (
      <div>
        {isMobile ? htmlMobile : htmlWeb}
      </div>
    );
  }
}
AccountB2C.propTypes = {
  loadingPage: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  cmsCommon: PropTypes.shape(PropTypes.object).isRequired,
  cms: PropTypes.shape(PropTypes.object).isRequired,
  logoutRequest: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  loginUpdateUserInfo: PropTypes.func.isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  basket: PropTypes.shape(PropTypes.object).isRequired,
  onClickIngredient: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    basket: state.basket,
  };
}

const mapDispatchToProps = {
  loadingPage,
  logoutRequest,
  loginUpdateUserInfo,
  addProductBasket,
  createBasketGuest,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AccountB2C));
