import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import moment from 'moment';
import classnames from 'classnames';
import icPoint from '../../../image/icon/icPoint.svg';
import icPointDisabled from '../../../image/icon/ic-point-disabled.svg';
import { toastrSuccess } from '../../../Redux/Helpers/notification';
import { isMobile } from '../../../DetectScreen';
import { generaCurrency, getNameFromButtonBlock } from '../../../Redux/Helpers';

function ItemRewardPoint(props) {
  const fallbackCopyTextToClipboard = (text) => {
    const textArea = document.createElement('textarea');
    textArea.value = text;

    // Avoid scrolling to bottom
    textArea.style.top = '0';
    textArea.style.left = '0';
    textArea.style.position = 'fixed';

    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    try {
      const successful = document.execCommand('copy');
      const msg = successful ? 'successful' : 'unsuccessful';
    } catch (err) {
      console.error('Fallback: Oops, unable to copy', err);
    }

    document.body.removeChild(textArea);
  };

  const copyTextToClipboard = (text) => {
    if (!navigator.clipboard) {
      fallbackCopyTextToClipboard(text);
      return;
    }
    navigator.clipboard.writeText(text).then(() => {
      toastrSuccess('Copying to clipboard was successful!');
    }, (err) => {
      console.error('Async: Could not copy text: ', err);
    });
  };

  const onCreateVoucher = (id) => {
    props.onClickCreateVoucher(id);
  };

  const usedBt = getNameFromButtonBlock(props.buttonBlocks, 'Used');
  const minimalSpendingBt = getNameFromButtonBlock(props.buttonBlocks, 'Minimal_Spending');
  const expireOnBt = getNameFromButtonBlock(props.buttonBlocks, 'Expires_on');
  const issuedonBt = getNameFromButtonBlock(props.buttonBlocks, 'Issued_on');
  const gBt = getNameFromButtonBlock(props.buttonBlocks, 'G');
  const isDiableCreateVoucher = (props.totalPoint < props.data.point) && !props.isUserVoucher;
  return (
    <div
      onClick={() => {
        if (isMobile && !isDiableCreateVoucher && !props.data.is_redeemed) {
          onCreateVoucher(props.data.id);
        }
      }}
      className={classnames('item-reward-point', (isMobile && (isDiableCreateVoucher || !!props.data.is_redeemed) ? 'disabled' : ''))}
    >
      {
        isMobile ? (
          <React.Fragment>
            <div className="div-left-mobile">
              <img loading="lazy" src={props.isUserVoucher ? (props.data && props.data.voucher ? props.data.voucher.image : '') : (props.data ? props.data.image : '')} alt="icon" />
            </div>
            <div className="div-right-mobile">
              <h3>
                {props.isUserVoucher ? props.data.voucher.name : props.data.name}
              </h3>
              {
                props.data.is_redeemed && (
                  <div className="text-used">
                    {usedBt}
                  </div>
                )
              }

              {
                props.isUserVoucher ? (
                  <div className="text-info">
                    <span>
                      {minimalSpendingBt}
                      {' '}
                      {generaCurrency(props.data.threshold)}
                    </span>
                    <button id="id-button" disabled={props.data.is_redeemed} className={classnames('button-bg__none', (props.data.is_redeemed ? 'inactive' : ''))} type="button" onClick={() => copyTextToClipboard(props.data.code)}>
                      {props.data.code}
                    </button>
                  </div>
                ) : (
                  <div
                    className={classnames('card-point', (isDiableCreateVoucher ? 'disable' : ''))}
                    onClick={() => {
                      if (!isDiableCreateVoucher) {
                        onCreateVoucher(props.data.id);
                      }
                    }}
                  >
                    <img loading="lazy" src={icPoint} alt="point" />
                    <span>
                      {props.data.point}
                      {gBt}
                    </span>
                  </div>
                )
              }
            </div>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <div className="div-left">
              <img loading="lazy" src={props.isUserVoucher ? (props.data && props.data.voucher ? props.data.voucher.image : '') : (props.data ? props.data.image : '')} alt="icon" />
              <div className="div-text">
                <h3>
                  {props.isUserVoucher ? props.data.voucher.name : props.data.name}
                </h3>
                {
                props.isUserVoucher && (
                  <React.Fragment>
                    <span>
                      {expireOnBt}
                      {' '}
                      -
                      {' '}
                      {props.data.valid_until ? moment(props.data.valid_until).format('DD.MM.YYYY') : ''}
                    </span>
                    <span>
                      {issuedonBt}
                      {' '}
                      -
                      {' '}
                      {
                        moment(props.data.date_created).format('DD.MM.YYYY')
                      }
                    </span>
                  </React.Fragment>
                )
              }

              </div>
            </div>
            <div className="div-right">
              {
              props.isUserVoucher ? (
                <div className="div-button">
                  {
                    props.data.is_redeemed && (
                      <div>
                        {usedBt}
                      </div>
                    )
                  }
                  <button id="id-button" disabled={props.data.is_redeemed} className={classnames('button-bg__none', (props.data.is_redeemed ? 'inactive' : ''))} type="button" onClick={() => copyTextToClipboard(props.data.code)}>
                    {props.data.code}
                  </button>
                </div>
              ) : (
                <div
                  className={classnames('card-point', (isDiableCreateVoucher ? 'disable' : ''))}
                  onClick={() => {
                    if (!isDiableCreateVoucher) {
                      onCreateVoucher(props.data.id);
                    }
                  }}
                >
                  <img loading="lazy" src={isDiableCreateVoucher ? icPointDisabled : icPoint} alt="point" />
                  <span>
                    {props.data.point}
                    {gBt}
                  </span>
                </div>
              )
            }
            </div>
          </React.Fragment>
        )
      }
    </div>
  );
}

ItemRewardPoint.defaultProps = {
  totalPoint: 0,
  onClickCreateVoucher: () => {},
};

ItemRewardPoint.propTypes = {
  totalPoint: PropTypes.number,
  onClickCreateVoucher: PropTypes.func,
};

export default ItemRewardPoint;
