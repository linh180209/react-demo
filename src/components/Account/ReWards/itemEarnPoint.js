import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import icMoney from '../../../image/icon/ic-money.svg';
import { generaCurrency, getNameFromButtonBlock } from '../../../Redux/Helpers';

function ItemEarnPoint(props) {
  if (!props.data) {
    return null;
  }
  const gBt = getNameFromButtonBlock(props.buttonBlocks, 'G');
  return (
    <div className="item-earn-point">
      <div className="div-left">
        <img loading="lazy" src={props.icon} alt="im" />
        <span>
          {props.title}
        </span>
      </div>
      <div className="div-right">
        <img loading="lazy" src={icMoney} alt="money" />
        <h3>
          {props.data.point}
          {gBt}
        </h3>

        <span className={props.data.type === 'buy' ? '' : 'transparent-text'}>
          /
          {generaCurrency(props.data.spend, true)}
        </span>
      </div>

    </div>
  );
}

export default ItemEarnPoint;
