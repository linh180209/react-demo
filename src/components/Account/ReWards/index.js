import classnames from 'classnames';
import _ from 'lodash';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import {
  Modal, ModalBody,
} from 'reactstrap';
import {
  GET_USER_ACTIONS, GET_USER_POINTS, GET_USER_VOUCHERS, GET_VOUCHERS, TOKEN_REFRESH,
} from '../../../config';
import icClose from '../../../image/icon/ic-close-menu.svg';
import iconAction from '../../../image/icon/icon-action.svg';
import iconPromo from '../../../image/icon/icon-promo.svg';
import iconPurchase from '../../../image/icon/icon-purchase.svg';
import icPoint from '../../../image/icon/icPoint.svg';
import icTimer from '../../../image/icon/icTimer.svg';
import { addFontCustom, fetchCMSHomepage, getNameFromButtonBlock } from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import '../../../styles/div-rewards.scss';
import '../../../styles/popup-reward.scss';
import { useMergeState } from '../../../Utils/customHooks';
import ItemEarnPoint from './itemEarnPoint';
import ItemRewardPoint from './itemRewardPoint';

const prePareCms = (cms) => {
  if (cms) {
    const { body, header_text: headerText } = cms;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    return {
      buttonBlocks, headerText,
    };
  }
  return { buttonBlocks: [], headerText: '' };
};

function ReWards(props) {
  const [tabs, setTabs] = useState(1);
  const [isShowPopupHistory, setIsShowPopupHistory] = useState(false);
  const [isShowPopupConfirm, setIsShowPopupConfirm] = useState(false);
  const [state, setState] = useMergeState({
    action: [],
    vourcher: [],
    userVoucher: [],
    historyUser: [],
    isShowEarned: true,
    buttonBlocks: [],
    headerText: '',
    idCreateVoucher: '',
  });

  const apiGetUserAction = () => {
    const option = {
      url: GET_USER_ACTIONS,
      method: 'GET',
    };
    return fetchClient(option, true);
  };

  const getVourcher = () => {
    const option = {
      url: GET_VOUCHERS,
      method: 'GET',
    };
    return fetchClient(option, true);
  };

  const getUserVoucher = () => {
    const option = {
      url: GET_USER_VOUCHERS,
      method: 'GET',
    };
    return fetchClient(option, true);
  };

  const getHistory = () => {
    const option = {
      url: GET_USER_POINTS,
      method: 'GET',
    };
    return fetchClient(option, true);
  };

  const fetchData = async () => {
    props.loadingPage(true);
    const pending = [apiGetUserAction(), getVourcher(), getUserVoucher(), getHistory()];
    try {
      const result = await Promise.all(pending);
      setState({
        action: result[0],
        vourcher: result[1],
        userVoucher: result[2],
        historyUser: result[3],
      });
      props.loadingPage(false);
    } catch (error) {
      toastrError(error.message);
      props.loadingPage(false);
    }
  };

  const fetchCMS = async () => {
    const { cms } = props;
    const rewards = _.find(cms, x => x.title === 'ReWards');
    if (_.isEmpty(rewards)) {
      try {
        const cmsData = await fetchCMSHomepage('rewards');
        props.addCmsRedux(cmsData);
        const dataCms = prePareCms(cmsData);
        setState(dataCms);
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const dataCms = prePareCms(rewards);
      setState(dataCms);
    }
  };

  const createVoucher = (id) => {
    const options = {
      url: GET_USER_VOUCHERS,
      method: 'POST',
      body: {
        voucher: id,
      },
    };
    return fetchClient(options, true);
  };

  const resetToken = () => {
    const options = {
      url: TOKEN_REFRESH,
      method: 'POST',
      body: {
        refresh_token: auth.tokenRefresh(),
        client_id: 'scentdesigner',
      },
    };
    return fetchClient(options);
  };

  const confirmCreateVoucer = (id) => {
    setState({ idCreateVoucher: id });
    setIsShowPopupConfirm(true);
  };

  const onClickCreateVoucher = async (id) => {
    setIsShowPopupConfirm(false);
    try {
      props.loadingPage(true);
      const result = await createVoucher(id);
      const loginData = await resetToken();
      _.assign(props.login, loginData);
      auth.login(props.login);
      onChangeTabs(3);
      if (!_.find(state.userVoucher, x => x.id === result.id)) {
        state.userVoucher.push(result);
        setState({ userVoucher: state.userVoucher });
      }
      props.loadingPage(false);
    } catch (error) {
      toastrError(error.message);
      props.loadingPage(false);
    }
  };

  useEffect(() => {
    fetchData();
    fetchCMS();
  }, []);

  const actionPonit = (
    <div className="content-point">
      <h3>
        {getNameFromButtonBlock(state.buttonBlocks, 'Activites_that')}
      </h3>
      <ItemEarnPoint icon={iconAction} data={_.find(state.action, x => x.type === 'review')} title={getNameFromButtonBlock(state.buttonBlocks, 'make_a_product')} buttonBlocks={state.buttonBlocks} />
      <ItemEarnPoint icon={iconPurchase} data={_.find(state.action, x => x.type === 'buy')} title={getNameFromButtonBlock(state.buttonBlocks, 'Purchase_a_Product')} buttonBlocks={state.buttonBlocks} />
      <ItemEarnPoint icon={iconPromo} data={_.find(state.action, x => x.type === 'ref_buy')} title={getNameFromButtonBlock(state.buttonBlocks, 'For_anytime_your')} buttonBlocks={state.buttonBlocks} />
    </div>
  );

  const voucher = (
    <div className="content-point">
      {
        _.map(state.vourcher, x => (
          <ItemRewardPoint
            buttonBlocks={state.buttonBlocks}
            data={x}
            totalPoint={props.login && props.login.user ? props.login.user.point : 0}
            onClickCreateVoucher={confirmCreateVoucer}
          />
        ))
      }
    </div>
  );
  const userVoucher = (
    <div className="content-point">
      {
        _.map(state.userVoucher, x => (
          <ItemRewardPoint data={x} isUserVoucher buttonBlocks={state.buttonBlocks} />
        ))
      }
    </div>
  );

  const setAnimation = (index) => {
    const ele = document.getElementById('line-active');
    const tabList = document.getElementById('list-tab');
    const bt = document.getElementById(`bt-${index}`);
    if (bt && ele && tabList) {
      ele.style.left = `${bt.getBoundingClientRect().x - tabList.getBoundingClientRect().x}px`;
      ele.style.width = `${bt.getBoundingClientRect().width}px`;
    }
  };

  const onChangeTabs = (tabs) => {
    setTabs(tabs);
    setAnimation(tabs);
  };

  const onClosePopUp = () => {
    setIsShowPopupHistory(false);
  };

  const onOpenPopUp = () => {
    setIsShowPopupHistory(true);
  };

  const onClosePopUpConfirm = () => {
    setIsShowPopupConfirm(false);
  };

  const confirmPurchasing = () => {
    const ConfirmBt = getNameFromButtonBlock(state.buttonBlocks, 'Confirm');
    const CancelBt = getNameFromButtonBlock(state.buttonBlocks, 'Cancel');
    const message = getNameFromButtonBlock(state.buttonBlocks, 'Do you want to purchase this item with your point');
    return (
      <Modal className={classnames('modal-full-screen', addFontCustom())} isOpen={isShowPopupConfirm}>
        <ModalBody className="flex">
          <div className="popup-history popup-confirm">
            <button className="button-bg__none bt-x" type="button" onClick={onClosePopUpConfirm}>
              <img loading="lazy" src={icClose} alt="close" />
            </button>
            <div className="div-des">
              <h3>
                {message}
              </h3>
            </div>
            <div className="list-bt div-row">
              <button className="bt-close outline-bt" type="button" onClick={onClosePopUpConfirm}>
                {CancelBt}
              </button>
              <button className="bt-close" type="button" onClick={() => onClickCreateVoucher(state.idCreateVoucher)}>
                {ConfirmBt}
              </button>
            </div>
          </div>
        </ModalBody>
      </Modal>
    );
  };

  const popupHistory = () => {
    const activityHistoryBt = getNameFromButtonBlock(state.buttonBlocks, 'activity_history');
    const myPointBt = getNameFromButtonBlock(state.buttonBlocks, 'My_Points');
    const earnedBt = getNameFromButtonBlock(state.buttonBlocks, 'Earned');
    const usedBt = getNameFromButtonBlock(state.buttonBlocks, 'Used');
    const closeBt = getNameFromButtonBlock(state.buttonBlocks, 'CLOSE');
    return (
      <Modal className={classnames('modal-full-screen', addFontCustom())} isOpen={isShowPopupHistory}>
        <ModalBody className="flex">
          <div className="popup-history">
            <button className="button-bg__none bt-x" type="button" onClick={onClosePopUp}>
              <img loading="lazy" src={icClose} alt="close" />
            </button>
            <h1>
              {activityHistoryBt}
            </h1>
            <div className="header-point">
              <h2>
                {myPointBt}
              </h2>
              <span>
                {props.login && props.login.user ? props.login.user.point : ''}
              </span>
            </div>
            <span className="sub-header">
              <button
                onClick={() => setState({ isShowEarned: true })}
                type="button"
                className={classnames('button-bg__none', (state.isShowEarned ? 'active' : ''))}
              >
                {earnedBt}
                {' '}
                {_.reduce(state.historyUser, (sum, x) => sum + x.point, 0)}
              </button>
              <span>/</span>
              <button
                onClick={() => setState({ isShowEarned: false })}
                type="button"
                className={classnames('button-bg__none', (!state.isShowEarned ? 'active' : ''))}
              >
                {usedBt}
                {' '}
                {/* {_.reduce(_.filter(state.userVoucher, x => x.is_redeemed), (sum, x) => sum + x.point, 0)} */}
                {_.reduce(state.userVoucher, (sum, x) => sum + x.point, 0)}
              </button>
            </span>
            <div className="div-list-point">
              {
              _.map(state.isShowEarned ? state.historyUser : _.filter(state.userVoucher, x => x.is_redeemed), d => (
                <div className="item-list-point">
                  <div className="line-1">
                    <span>
                      {moment(d.date_created).format('DD.MM.YYYY hh.mm.ss')}
                    </span>
                    <span>
                      {state.isShowEarned ? earnedBt : usedBt}
                    </span>
                  </div>
                  <div className="line-2">
                    <span>
                      {state.isShowEarned ? d.action.name : d.voucher.name}
                    </span>
                    <span>
                      {state.isShowEarned ? `+${d.point}` : `-${d.point}`}
                    </span>
                  </div>
                </div>
              ))
            }
            </div>
            <button className="bt-close" type="button" onClick={onClosePopUp}>
              {closeBt}
            </button>
          </div>
        </ModalBody>
      </Modal>
    );
  };

  const welcomeBt = getNameFromButtonBlock(state.buttonBlocks, 'Welcome_to');
  const gbt = getNameFromButtonBlock(state.buttonBlocks, 'G');
  const seeHistoryBt = getNameFromButtonBlock(state.buttonBlocks, 'See_History');
  const earnPointsBt = getNameFromButtonBlock(state.buttonBlocks, 'Earn_points');
  const userPointBt = getNameFromButtonBlock(state.buttonBlocks, 'Use_points');
  const rewardsBt = getNameFromButtonBlock(state.buttonBlocks, 'Rewards');

  return (
    <div className="div-rewards">
      {popupHistory()}
      {confirmPurchasing()}
      {/* {popupCongratulations()} */}
      <h3>
        {state.headerText}
      </h3>
      <span className="des">
        {welcomeBt}
      </span>
      <div className="points">
        <img loading="lazy" src={icPoint} alt="icon" />
        <span className="price">
          {props.login && props.login.user ? props.login.user.point : ''}
          {gbt}
        </span>
      </div>
      <div className="see-history">
        <button className="button-bg__none" type="button" onClick={onOpenPopUp}>
          <img loading="lazy" src={icTimer} alt="timer" />
          {seeHistoryBt}
        </button>
      </div>
      <div id="list-tab" className="tabs">
        <button
          id="bt-1"
          type="button"
          className={classnames('button-bg__none', tabs === 1 ? 'active' : '')}
          onClick={() => onChangeTabs(1)}
        >
          {earnPointsBt}
        </button>
        <button
          id="bt-2"
          type="button"
          className={classnames('button-bg__none', tabs === 2 ? 'active' : '')}
          onClick={() => onChangeTabs(2)}
        >
          {userPointBt}
        </button>
        <button
          id="bt-3"
          type="button"
          className={classnames('button-bg__none', tabs === 3 ? 'active' : '')}
          onClick={() => onChangeTabs(3)}
        >
          {rewardsBt}
        </button>
        <div id="line-active" className="line-active" />
      </div>
      <div className="tab-content">
        {
          tabs === 1 ? (
            actionPonit
          ) : tabs === 2 ? (
            voucher
          ) : (userVoucher)
        }
      </div>
    </div>
  );
}

export default ReWards;
