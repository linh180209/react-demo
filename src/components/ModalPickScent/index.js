import React, { useState, useEffect } from 'react';
import {
  Modal, ModalBody,
} from 'reactstrap';
import _ from 'lodash';
import classnames from 'classnames';
import { useSelector, useDispatch } from 'react-redux';
import './styles.scss';
import icClose from '../../image/icon/ic-prev-silder.svg';
import icCloseWhite from '../../image/icon/ic-close-white.svg';
import icDropDown from '../../image/icon/ic-dropdown-thin.svg';
import FilterSearchScent from '../ProductAllV2/ProductBlock/filterSearchScent';
import ScentLine from '../ProductAllV2/ProductBlock/scentLine';
import { addFontCustom, getNameFromButtonBlock, preDataScents } from '../../Redux/Helpers';
import { applyFilter, applySearch, fetchIngredient } from './handler';
import IngredientPopUp from '../IngredientDetail/ingredientPopUp';
import loadingPage from '../../Redux/Actions/loading';
import { toastrError } from '../../Redux/Helpers/notification';
import ButtonCT from '../ButtonCT';
import { useMergeState } from '../../Utils/customHooks';

function ModalPickScent(props) {
  const dispatch = useDispatch();
  const scents = useSelector(state => state.scents);
  const [valueFilter, setValueFilter] = useState('Scent Family');
  const [valueSearch, setValueSearch] = useState('');
  const [scentSelected, setScentSelected] = useState(props.scentSelected);
  const [dataScentsPerfume, setDataScentsPerfume] = useState();
  const [newScent, setNewScent] = useState();
  const [state, setState] = useMergeState({
    ingredientMini: undefined,
    dataSelected: undefined,
  });

  const onChangeValueFilter = (value) => {
    setValueFilter(value);
  };

  const onChangeValueSearch = (value) => {
    setValueSearch(value);
  };

  const onClickIngredientMini = async (id) => {
    if (typeof id === 'object') {
      setState({ ingredientMini: id });
      return;
    }
    try {
      dispatch(loadingPage(true));
      const result = await fetchIngredient(id);
      if (result && !result.isError) {
        setState({ ingredientMini: result });
        dispatch(loadingPage(false));
        return;
      }
      throw new Error(result.message);
    } catch (err) {
      dispatch(loadingPage(false));
      toastrError(err.message);
    }
  };

  const onClick = (data) => {
    console.log('data', data);
    // props.onClickScent(data);
    setState({
      dataSelected: data,
    });
    onClickIngredientMini(data.ingredient);
  };

  const onClickCancelIngredients = () => {

  };

  const onClickApply = () => {
    props.onClickScent(state.dataSelected);
  };

  useEffect(() => {
    if (scents?.length > 0) {
      const data = preDataScents(scents);
      setDataScentsPerfume(data);
    }
  }, [scents]);

  useEffect(() => {
    if (dataScentsPerfume?.length > 0) {
      const newScentFilter = applyFilter(dataScentsPerfume, valueFilter);
      const news = applySearch(newScentFilter, valueSearch);
      setNewScent(news);
    }
  }, [dataScentsPerfume, valueFilter, valueSearch]);

  console.log('props.index', props.index);
  const pickYour1Bt = getNameFromButtonBlock(props.buttonBlocks, 'Pick your 1st ingredient');
  const pickYour2Bt = getNameFromButtonBlock(props.buttonBlocks, 'Pick your 2nd ingredient');
  const applyBt = getNameFromButtonBlock(props.buttonBlocks, 'APPLY');

  const renderIngredientLocal = () => (
    <div className="div-ingredient-local">
      <div className="div-content-local">
        <IngredientPopUp
          isShow
          ingredientMinimum
          data={state.ingredientMini}
          history={() => {}}
          onClickCancelIngredientsMini={() => setState({ ingredientMini: undefined })}
          onClickApplyMini={onClickApply}
        />
      </div>
      <div className="div-list-button">
        <ButtonCT
          className="bt-apply"
          name={applyBt}
          onClick={onClickApply}
        />
      </div>
    </div>
  );

  return (
    <Modal isOpen={props.isOpen} className={classnames('modal-pick-scent', addFontCustom())}>
      <button
        onClick={props.onCloseModal}
        className="button-bg__none bt-close"
        type="button"
      >
        <img src={state.ingredientMini ? icCloseWhite : icClose} alt="icon" />
      </button>
      <ModalBody className={classnames('modal-body-pick-scent', state.ingredientMini ? 'full' : '')}>
        {
          state.ingredientMini ? (
            renderIngredientLocal()
          ) : (
            <React.Fragment>
              <div className="header">
                {props.indexSelected === 0 ? pickYour1Bt : pickYour2Bt}
              </div>
              <div className="div-filter div-row">
                <FilterSearchScent
                  isVersionWeb
                  valueFilter={valueFilter}
                  valueSearch={valueSearch}
                  onChangeValueFilter={onChangeValueFilter}
                  onChangeValueSearch={onChangeValueSearch}
                  buttonBlocks={props.buttonBlocks}
                  icDropDown={icDropDown}
                  indexSelected={props.indexSelected}
                />
              </div>
              <div className="div-scents">
                <div className="div-scroll-ingredient">
                  {
                    _.map(newScent, x => (
                      <ScentLine
                        onClickIngredient={props.onClickIngredient}
                        data={x}
                        onClick={onClick}
                        scentSelected={scentSelected}
                        buttonBlocks={props.buttonBlocks}
                      />
                    ))
                  }
                </div>
              </div>
            </React.Fragment>
          )
        }
      </ModalBody>
    </Modal>
  );
}

export default ModalPickScent;
