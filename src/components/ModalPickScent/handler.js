import _ from 'lodash';
import { GET_INGREDIENT_ID_PRODUCTS } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';

export const applyFilter = (dataScents, value) => {
  const datas = _.find(dataScents, x => x.title.includes(value) || x.title.includes(value?.toLowerCase()));
  if (value === 'Alphabet' || value === 'Popularity') {
    return datas ? datas.datas : [];
  }
  const datasNew = datas ? datas.datas : [];
  const scentT1 = _.filter(datasNew, x => x.group?.toLowerCase() === value?.toLowerCase());
  return scentT1;
};

export const applySearch = (dataScents, value) => {
  if (!value) {
    return dataScents;
  }
  const newDataScents = _.cloneDeep(dataScents);
  _.forEach(newDataScents, (d) => {
    const { datas } = d;
    d.datas = _.filter(datas, x => x.name?.toLowerCase().includes(value?.toLowerCase()));
  });
  return newDataScents;
};

export const fetchIngredient = (id) => {
  const option = {
    url: GET_INGREDIENT_ID_PRODUCTS.replace('{id}', id),
    method: 'GET',
  };
  return fetchClient(option);
};
