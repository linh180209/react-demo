import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';
import { Input, Label } from 'reactstrap';
import _ from 'lodash';
import DatePicker from 'react-datepicker';
import './input-custom.scss';
import 'react-datepicker/dist/react-datepicker.css';

function InputCT(props) {
  const onChange = (e) => {
    const { value } = e.target;
    props.onChange(value, props.name);
  };

  const onChangeDate = (e) => {
    props.onChange(e, props.name);
  };

  return (
    <div className={classnames('input-custom', props.className)}>
      {
        props.type === 'INPUT_MASK' ? (
          <InputMask
            type="tel"
            name={props.name}
            placeholder={props.placeholder}
            mask={props.mask}
            maskChar={null}
            defaultValue={props.defaultValue}
            onChange={onChange}
          />
        ) : props.type === 'select' ? (
          <Input
            type="select"
            data-gtmtracking={props.gtmtracking}
            placeholder={props.placeholder}
            name={props.name}
            onChange={onChange}
            // style={{ height: '50px' }}
          >
            {
              _.map(props.options, x => (
                <option value={x.value} selected={x.value === props.value}>
                  {x.label}
                </option>
              ))
            }
          </Input>
        ) : props.type === 'Date' ? (
          <DatePicker
            dateFormat={props.dateFormat}
            minDate={props.minDate}
            onChange={onChangeDate}
            selected={props.value}
            placeholderText={props.placeholder}
          />
        ) : props.type === 'checkbox' ? (
          <Label>
            <Input
              name={props.name}
              id={props.id}
              type="checkbox"
              className="custom-input-filter__checkbox"
              onChange={onChange}
              checked={props.checked}
            />
            {' '}
            <Label
              for={props.id}
              style={{ pointerEvents: 'none' }}
            />
            {props.text}
          </Label>
        ) : (
          <Input
            type={props.type}
            data-gtmtracking={props.gtmtracking}
            placeholder={props.placeholder}
            name={props.name}
            value={props.value}
            onChange={onChange}
          />
        )
      }
      {
        props.iconRight && (
          <img className="icon-right" src={props.iconRight} alt="icon" />
        )
      }
      {
        props.textRight && (
          <span className="text-right">{props.textRight}</span>
        )
      }
    </div>
  );
}

InputCT.defaultProps = {
  className: undefined,
  gtmtracking: '',
  placeholder: undefined,
  name: undefined,
  onChange: () => {},
};

InputCT.propTypes = {
  className: PropTypes.string,
  gtmtracking: PropTypes.string,
  placeholder: PropTypes.string,
  name: PropTypes.string,
  onChange: PropTypes.func,
};

export default InputCT;
