import React, { useState, useEffect } from 'react';
import './radio-button.scss';
import classnames from 'classnames';

function RadioButton(props) {
  return (
    <div
      onClick={() => props.onChange && props.onChange(props.value, props.name)}
      className={classnames('radio-button', props.selected ? 'active' : '', props.className)}
    >
      <div className="point">
        <button type="button" />
      </div>
      <span>
        {props.label}
      </span>
    </div>
  );
}

export default RadioButton;
