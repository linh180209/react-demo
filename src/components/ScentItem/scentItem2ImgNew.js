import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import promote from '../../image/icon/promote.png';
// import heart from '../../image/icon/heart.svg';
import info from '../../image/icon/info.png';
import bottleDropper from '../../image/bottle-and-dropper2.png';
import { getAltImage, getNameFromCommon, generaCurrency } from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { isMobile, isTablet } from '../../DetectScreen';

class ScentItem2ImgNew extends Component {
    onClickRightButton = () => {
      if (this.props.onClickRightButton) {
        this.props.onClickRightButton(this.props.data);
      }
    };

    onClickLeftButton = () => {
      if (this.props.onClickLeftButton) {
        this.props.onClickLeftButton(this.props.data);
      }
    };

    onClickImage = () => {
      if (this.props.onClickImage) {
        const { data } = this.props;
        this.props.onClickImage(data);
      }
    };

    render() {
      const {
        data, className, isBigSize, isShadow, cmsCommon,
      } = this.props;
      const addToCart = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
      const viewProduct = getNameFromCommon(cmsCommon, 'VIEW_PRODUCT');
      const {
        name,
        image_urls: imageUrl,
        created,
        isLike,
        buttonName,
        mainTitle,
        price,
        isInfo,
        name1,
        imageKnowMore1,
        imageKnowMore2,
        name2,
        info1,
        info2,
        buttonNameLeft,
        shortTitle,
      } = data;
      const widthMobile = '260px';
      const heightMobile = '385px';

      const mobileOutCome = (
        <React.Fragment>
          <div className="item-title">
            <span className="custom-title">
              {mainTitle}
            </span>
          </div>
          <div className="div-3image">
            <div className="div-img-1 div-after-shadow">
              <img
                loading="lazy"
                className="img-1"
                src={imageUrl[0]}
                alt={getAltImage(imageUrl[0])}
              />
              {
                        isInfo
                          ? (
                            <button
                              type="button"
                              className="button-bg__none ma-0 pa-0"
                              style={{
                                position: 'absolute', top: '2%', left: '8%', zIndex: 10, transform: 'translate3d(0, 0, 100px)',
                              }}
                              onClick={() => { this.props.onClickInfo(info1); }}
                            >
                              <img style={{ width: '15px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }} src={info} alt="info" />
                            </button>
                          ) : <div />
                      }
            </div>
            <div className="div-img-2 div-after-shadow">
              <img
                loading="lazy"
                className="img-2"
                src={imageUrl[1]}
                alt={getAltImage(imageUrl[1])}
              />
              {
                        isInfo
                          ? (
                            <button
                              type="button"
                              className="button-bg__none ma-0 pa-0"
                              style={{
                                position: 'absolute', top: '2%', right: '8%', zIndex: 10, transform: 'translate3d(0, 0, 100px)',
                              }}
                              onClick={() => { this.props.onClickInfo(info2); }}
                            >
                              <img style={{ width: '15px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }} src={info} alt="info" />
                            </button>
                          ) : <div />
                      }
            </div>
            <div className="div-img-3">
              <img
                loading="lazy"
                onClick={() => this.onClickImage()}
                src={bottleDropper}
                alt={getAltImage(bottleDropper)}
                style={{ cursor: 'pointer' }}
              />
            </div>
          </div>
          <div className="item-detail">
            <div className={shortTitle !== null ? 'item-short-title' : ''}>
              <span className={shortTitle !== null ? 'custom-short-title' : ''}>
                {shortTitle !== null ? shortTitle : ''}
              </span>
            </div>
            <div className="item-price">
              <span className="custom-item-price">
                {generaCurrency(price)}
              </span>
            </div>
            <div className="item-btn-options">
              <button type="button" onClick={() => this.onClickLeftButton()}>
                <span>
                  {viewProduct}
                </span>
              </button>
              <span>
                |
              </span>
              <button type="button" onClick={() => this.onClickRightButton()}>
                <span>
                  {addToCart}
                </span>
              </button>
            </div>
          </div>
        </React.Fragment>
      );

      const tabletOutCome = (
        <React.Fragment>
          <div className="item-title">
            <span className="custom-title">
              {mainTitle}
            </span>
          </div>
          <div className="div-3image">
            <div className="div-img-1 div-after-shadow">
              <img
                loading="lazy"
                className="img-1"
                src={imageUrl[0]}
                alt={getAltImage(imageUrl[0])}
              />
              {
                        isInfo
                          ? (
                            <button
                              type="button"
                              className="button-bg__none ma-0 pa-0"
                              style={{
                                position: 'absolute', top: '2%', left: '8%', zIndex: 10, transform: 'translate3d(0, 0, 100px)',
                              }}
                              onClick={() => { this.props.onClickInfo(info1); }}
                            >
                              <img style={{ width: '24px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }} src={info} alt="info" />
                            </button>
                          ) : <div />
                      }
            </div>
            <div className="div-img-3">
              <img
                loading="lazy"
                onClick={() => this.onClickImage()}
                src={bottleDropper}
                alt={getAltImage(bottleDropper)}
                style={{ cursor: 'pointer' }}
              />
            </div>
            <div className="div-img-2 div-after-shadow">
              <img
                loading="lazy"
                className="img-2"
                src={imageUrl[1]}
                alt={getAltImage(imageUrl[1])}
              />
              {
                        isInfo
                          ? (
                            <button
                              type="button"
                              className="button-bg__none ma-0 pa-0"
                              style={{
                                position: 'absolute', top: '2%', right: '8%', zIndex: 10, transform: 'translate3d(0, 0, 100px)',
                              }}
                              onClick={() => { this.props.onClickInfo(info2); }}
                            >
                              <img style={{ width: '24px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }} src={info} alt="info" />
                            </button>
                          ) : <div />
                      }
            </div>
          </div>
          <div className="item-detail">
            <div className={shortTitle !== null ? 'item-short-title' : ''}>
              <span className={shortTitle !== null ? 'custom-short-title' : ''}>
                {shortTitle !== null ? shortTitle : ''}
              </span>
            </div>
            <div className="item-price">
              <span className="custom-item-price">
                {generaCurrency(price)}
              </span>
            </div>
            <div className="item-btn-options">
              <button type="button" onClick={() => this.onClickLeftButton()}>
                <span>
                  {viewProduct}
                </span>
              </button>
              <span>
                |
              </span>
              <button type="button" onClick={() => this.onClickRightButton()}>
                <span>
                  {addToCart}
                </span>
              </button>
            </div>
          </div>
        </React.Fragment>
      );
      const htmlOutCome = (
        <React.Fragment>
          <div className="item-title">
            <span className="custom-title">
              {mainTitle}
            </span>
          </div>
          <div className="div-3image">
            <div className="div-img-1 div-after-shadow">
              <img
                loading="lazy"
                className="img-1"
                src={imageUrl[0]}
                alt={getAltImage(imageUrl[0])}
              />
              {
                        isInfo
                          ? (
                            <button
                              type="button"
                              className="button-bg__none ma-0 pa-0"
                              style={{
                                position: 'absolute', top: '1%', left: '8%', zIndex: 10, transform: 'translate3d(0, 0, 100px)',
                              }}
                              onClick={() => { this.props.onClickInfo(info1); }}
                            >
                              <img style={{ width: '24px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }} src={info} alt="info" />
                            </button>
                          ) : <div />
                      }
            </div>
            <div className="div-img-3">
              <img
                loading="lazy"
                onClick={() => this.onClickImage()}
                src={bottleDropper}
                alt={getAltImage(bottleDropper)}
                style={{ cursor: 'pointer' }}
              />
            </div>
            <div className="div-img-2 div-after-shadow">
              <img
                loading="lazy"
                className="img-2"
                src={imageUrl[1]}
                alt={getAltImage(imageUrl[1])}
              />
              {
                        isInfo
                          ? (
                            <button
                              type="button"
                              className="button-bg__none ma-0 pa-0"
                              style={{
                                position: 'absolute', top: '1%', right: '8%', zIndex: 10, transform: 'translate3d(0, 0, 100px)',
                              }}
                              onClick={() => { this.props.onClickInfo(info2); }}
                            >
                              <img style={{ width: '24px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }} src={info} alt="info" />
                            </button>
                          ) : <div />
                      }
            </div>
          </div>
          <div className="item-detail">
            <div className={shortTitle !== null ? 'item-short-title' : ''}>
              <span className={shortTitle !== null ? 'custom-short-title' : ''}>
                {shortTitle !== null ? shortTitle : ''}
              </span>
            </div>
            <div className="item-price">
              <span className="custom-item-price">
                {generaCurrency(price)}
              </span>
            </div>
            <div className="item-btn-options">
              <button type="button" onClick={() => this.onClickLeftButton()}>
                <span>
                  {viewProduct}
                </span>
              </button>
              <span>
                |
              </span>
              <button type="button" onClick={() => this.onClickRightButton()}>
                <span>
                  {addToCart}
                </span>
              </button>
            </div>
          </div>
        </React.Fragment>
      );

      return (
        <div
          className={isMobile && !isTablet ? `${className} div-scentItem-mobile` : `${className} div-scentItem`}
          style={
                    isMobile && !isTablet
                      ? { width: widthMobile, height: heightMobile } : isTablet ? { margin: '0 auto' }
                        : {
                          display: 'flex',
                          alignItems: 'center',
                        }}
        >

          {
                  isMobile ? mobileOutCome : isTablet ? tabletOutCome : htmlOutCome
                }
        </div>
      );
    }
}
ScentItem2ImgNew.defaultProps = {
  className: '',
  isBigSize: false,
};

ScentItem2ImgNew.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    shortTitle: PropTypes.string,
    image_urls: PropTypes.array,
    created: PropTypes.string,
    buttonName: PropTypes.string,
    variant_values: PropTypes.array,
    titleTop: PropTypes.string,
    mainTitle: PropTypes.array,
    isLike: PropTypes.bool,
    price: PropTypes.string,
    isInfo: PropTypes.bool,
    info1: PropTypes.shape,
    info2: PropTypes.shape,
    buttonNameLeft: PropTypes.string,
  }).isRequired,
  isBigSize: PropTypes.bool,
  className: PropTypes.string,
  onClickRightButton: PropTypes.func.isRequired,
  onClickImage: PropTypes.func.isRequired,
  onClickLeftButton: PropTypes.func.isRequired,
  onClickInfo: PropTypes.func.isRequired,
  cmsCommon: PropTypes.shape().isRequired,
};

export default ScentItem2ImgNew;
