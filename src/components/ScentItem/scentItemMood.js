import React, { Component } from 'react';
import PropTypes from 'prop-types';
import promote from '../../image/icon/promote.png';
import heart from '../../image/icon/heart.svg';
import info from '../../image/icon/info.svg';
import { getAltImage, generaCurrency } from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { isMobile, isTablet } from '../../DetectScreen';

class ScentItemMood extends Component {
  onClickRightButton = () => {
    if (this.props.onClickRightButton) {
      this.props.onClickRightButton(this.props.data);
    }
  }

  onClickImage = () => {
    if (this.props.onClickImage) {
      const { data } = this.props;
      this.props.onClickImage(data);
    }
  }

  render() {
    const {
      data, className,
    } = this.props;
    const {
      name, image_urls: imageUrl, created, isLike, buttonName, price, name1, name2, imageKnowMore1, imageKnowMore2, buttonNameLeft,
    } = data;
    const styleImage = {
      objectFit: 'cover',
      objectPosition: 'center',
      width: '100%',
      height: isMobile && !isTablet ? '123.3px' : '185px',
    };
    return (
      <div
        className={isMobile && !isTablet ? `${className} div-scentItem-mobile` : `${className} div-scentItem`}
        style={isMobile && !isTablet ? { height: '200px' } : { height: '300px' }}
      >
        <div
          className="div-2image"
        >
          <div className="div-img-1">
            <img
              loading="lazy"
              src={imageUrl[0]}
              alt={getAltImage(imageUrl[0])}
              style={styleImage}
              onClick={this.onClickImage}
              style={{
                cursor: 'pointer', width: '100%', height: '100%', objectFit: 'cover',
              }}
            />
            <button
              type="button"
              className="button-bg__none ma-0 pa-0"
              style={isMobile && !isTablet ? { position: 'absolute', bottom: '10px', left: '0px' } : { position: 'absolute', bottom: '10px', left: '5px' }}
              onClick={() => this.props.onOpenKnowMore({ name: name1, imageKnowMore: imageKnowMore1 })}
            >
              <img style={{ width: '24px' }} src={info} alt="infro" />
            </button>
          </div>
          <div className="div-img-2">
            <img
              loading="lazy"
              src={imageUrl[1]}
              alt={getAltImage(imageUrl[1])}
              style={styleImage}
              onClick={this.onClickImage}
              style={{
                cursor: 'pointer', width: '100%', height: '100%', objectFit: 'cover',
              }}
            />
            <button
              type="button"
              className="button-bg__none ma-0 pa-0"
              style={isMobile && !isTablet ? { position: 'absolute', bottom: '10px', left: '0px' } : { position: 'absolute', bottom: '10px', left: '5px' }}
              onClick={() => this.props.onOpenKnowMore({ name: name2, imageKnowMore: imageKnowMore2 })}
            >
              <img style={{ width: '24px' }} src={info} alt="infro" />
            </button>
          </div>
        </div>

        <div className="div-body">
          <div className="block-1">
            <span className="ml-2" style={isMobile && !isTablet ? { fontWeight: '600', fontSize: '9px' } : { fontWeight: '600', fontSize: '15px' }}>
              {name}
            </span>
            <div
              className={created ? 'ml-2' : 'hiden'}
            >
              <span>
                Created by:
              </span>
              {' '}
              <span>
                {created || 'LOSI SHERRARD'}
              </span>
            </div>
          </div>
          <span className={price ? 'mr-2' : 'hiden'} style={isMobile && !isTablet ? { textAlign: 'right', fontSize: '9px' } : { textAlign: 'right' }}>
            {generaCurrency(price)}
          </span>
          <div className="block-3" style={{ justifyContent: 'space-between', paddingBottom: '2px' }}>
            {/* <button
              type="button"
              className={isLike ? 'ml-2' : 'hiden'}
              onClick={this.onClickLeftButton}
            >
              <img className="hiden" src={heart} alt="heart" style={isMobile && !isTablet ? { width: '20px', height: '20px' } : {}} />
            </button> */}

            <button
              type="button"
              className="ml-2"
              onClick={() => this.props.onClickLeftButton(this.props.data)}
              style={isMobile && !isTablet ? { fontWeight: '600', fontSize: '0.5rem' } : { fontWeight: '600' }}
            >
              {
                buttonNameLeft
              }
            </button>

            <button
              type="button"
              className="mr-2"
              onClick={() => this.onClickRightButton(this.props.data)}
              style={isMobile && !isTablet ? { fontWeight: '600', fontSize: '0.5rem' } : { fontWeight: '600', fontSize: '0.7rem' }}
            >
              {
                buttonName || (
                  <div>
                    <img src={promote} alt="promote" />
                    <span>
                      PROMOTE CREATION
                    </span>
                  </div>
                )
              }

            </button>
          </div>
        </div>
      </div>
    );
  }
}
ScentItemMood.defaultProps = {
  className: '',
};

ScentItemMood.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    image_urls: PropTypes.array,
    created: PropTypes.string,
    buttonName: PropTypes.string,
    variant_values: PropTypes.array,
    titleTop: PropTypes.string,
    isLike: PropTypes.bool,
    price: PropTypes.string,
    name1: PropTypes.string,
    name2: PropTypes.string,
    description1: PropTypes.string,
    description2: PropTypes.string,
    imageKnowMore1: PropTypes.string,
    imageKnowMore2: PropTypes.string,
  }).isRequired,
  className: PropTypes.string,
  onClickRightButton: PropTypes.func.isRequired,
  onClickLeftButton: PropTypes.func.isRequired,
  onClickImage: PropTypes.func.isRequired,
  onOpenKnowMore: PropTypes.func.isRequired,
};

export default ScentItemMood;
