import React, { Component } from 'react';
import PropTypes from 'prop-types';
import promote from '../../image/icon/promote.png';
import heart from '../../image/icon/heart.svg';
import info from '../../image/icon/info.svg';
import { getAltImage, generaCurrency } from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { isMobile, isTablet } from '../../DetectScreen';

class ScentItem2Img extends Component {
  onClickRightButton = () => {
    if (this.props.onClickRightButton) {
      this.props.onClickRightButton(this.props.data);
    }
  }

  onClickLeftButton = () => {
    if (this.props.onClickLeftButton) {
      this.props.onClickLeftButton(this.props.data);
    }
  }

  onClickImage = () => {
    if (this.props.onClickImage) {
      const { data } = this.props;
      this.props.onClickImage(data);
    }
  }

  render() {
    const {
      data, className, isBigSize,
    } = this.props;
    const {
      name, image_urls: imageUrl, created, isLike, buttonName, price, isInfo, info1, info2,
      buttonNameLeft, shortTitle,
    } = data;
    const widthMobile = `${260 / 1.2}px`;
    const heightMobile = `${385 / 1.2}px`;
    return (
      <div className={`${className} div-scentItem`} style={isMobile && !isTablet ? { width: widthMobile, height: heightMobile } : {}}>
        <div
          className="div-2image"
          style={{ position: 'relative' }}
        >
          <div className="div-img-1" style={{ position: 'relative' }}>
            <img loading="lazy" onClick={this.onClickImage} src={imageUrl[0]} alt={getAltImage(imageUrl[0])} style={{ cursor: 'pointer' }} />
            {
              isInfo
                ? (
                  <button
                    type="button"
                    className="button-bg__none ma-0 pa-0"
                    style={isMobile && !isTablet ? { position: 'absolute', bottom: '10px', left: '0px' } : { position: 'absolute', bottom: '10px', left: '5px' }}
                    onClick={() => { this.props.onClickInfo(info1); }}
                  >
                    <img style={{ width: '24px' }} src={info} alt="info" />
                  </button>
                ) : <div />
            }
          </div>
          <div className="div-img-2" style={{ position: 'relative' }}>
            <img loading="lazy" onClick={this.onClickImage} src={imageUrl[1]} alt="img" style={{ cursor: 'pointer' }} />
            {
              isInfo
                ? (
                  <button
                    type="button"
                    className="button-bg__none ma-0 pa-0"
                    style={isMobile && !isTablet ? { position: 'absolute', bottom: '10px', left: '0px' } : { position: 'absolute', bottom: '10px', left: '5%' }}
                    onClick={() => { this.props.onClickInfo(info2); }}
                  >
                    <img style={{ width: '24px' }} src={info} alt="info" />
                  </button>
                ) : <div />
            }
          </div>
        </div>

        <div className="div-body">
          <div className="block-1">
            <span
              className="ml-2"
              style={{
                fontWeight: '600',
                fontSize: isMobile && !isTablet ? '1rem' : '1.2rem',
                textAlign: 'center',
              }}
            >
              {shortTitle}
            </span>
            <div className={created ? 'ml-2' : 'hiden'}>
              <span>
                Created by:
              </span>
              {' '}
              <span>
                {created || 'LOSI SHERRARD'}
              </span>
            </div>
          </div>
          <span className={price ? 'mr-2' : 'hiden'} style={{ textAlign: 'right' }}>
            {generaCurrency(price)}
          </span>
          <div className="block-3" style={{ justifyContent: 'space-between' }}>
            {/* <button
              type="button"
              className={isLike ? 'ml-2' : 'hiden'}
              onClick={this.onClickLeftButton}
            >
              <img className="hidden" style={{ width: '24px' }} src={heart} alt="heart" />
            </button> */}
            <button
              type="button"
              className="ml-2"
              onClick={() => this.props.onClickLeftButton(data)}
              style={isMobile && !isTablet ? (isBigSize ? { fontWeight: '600', fontSize: '0.7rem' } : { fontWeight: '600', fontSize: '0.5rem' }) : { fontWeight: '600' }}
            >
              {
                buttonNameLeft
              }
            </button>

            <button
              type="button"
              className="mr-2"
              onClick={() => this.onClickRightButton(this.props.data)}
              style={isMobile && !isTablet ? (isBigSize ? { fontWeight: '600', fontSize: '0.7rem' } : { fontWeight: '600', fontSize: '0.5rem' }) : { fontWeight: '600' }}
            >
              {
                buttonName || (
                  <div>
                    <img src={promote} alt="promote" />
                    <span>
                      PROMOTE CREATION
                    </span>
                  </div>
                )
              }

            </button>
          </div>
        </div>
      </div>
    );
  }
}
ScentItem2Img.defaultProps = {
  className: '',
  isBigSize: false,
};

ScentItem2Img.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    shortTitle: PropTypes.string,
    image_urls: PropTypes.array,
    created: PropTypes.string,
    buttonName: PropTypes.string,
    variant_values: PropTypes.array,
    titleTop: PropTypes.string,
    isLike: PropTypes.bool,
    price: PropTypes.string,
    isInfo: PropTypes.bool,
    info1: PropTypes.shape,
    info2: PropTypes.shape,
    buttonNameLeft: PropTypes.string,
  }).isRequired,
  isBigSize: PropTypes.bool,
  className: PropTypes.string,
  onClickRightButton: PropTypes.func.isRequired,
  onClickImage: PropTypes.func.isRequired,
  onClickLeftButton: PropTypes.func.isRequired,
  onClickInfo: PropTypes.func.isRequired,
};

export default ScentItem2Img;
