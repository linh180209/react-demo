import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../../styles/scentItem.scss';
import { getAltImage, getNameFromCommon, getCmsCommon } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

class ScentItem extends Component {
  // onOpenKnowMore = () => {
  //   if (this.props.onOpenKnowMore) {
  //     this.props.onOpenKnowMore(this.props.data);
  //   }
  // }

  onClickIngredient = () => {
    if (this.props.onClickIngredient) {
      this.props.onClickIngredient(this.props.data.ingredient);
    }
  }


  onClickRightButton = () => {
    if (this.props.onClickRightButton) {
      this.props.onClickRightButton(this.props.data);
    }
  }

  render() {
    const {
      data, className, isShowGender, isMMO, isFavorite,
    } = this.props;
    const {
      name, image_urls: imageUrl, created, buttonName,
      variant_values: variantValues, titleTop, description, header2, content2,
    } = data;
    const cmsCommon = getCmsCommon(this.props.cms);
    const knowBt = getNameFromCommon(cmsCommon, 'KNOW_MORE');
    const addCartBt = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
    const maleBt = getNameFromCommon(cmsCommon, 'Male');
    const forManBt = getNameFromCommon(cmsCommon, 'FOR_MAN');
    const forWomanBt = getNameFromCommon(cmsCommon, 'FOR_WOMAN');
    const gender = variantValues ? (variantValues[0] === maleBt ? forManBt : forWomanBt) : undefined;
    return (
      <div className={isMobile ? `${className} div-scentItem-mobile-mmo` : `${className} div-scentItem-mmo`}>
        <div className="div-img" style={{ position: 'relative' }}>
          <img
            loading="lazy"
            src={imageUrl && imageUrl.length > 0 ? imageUrl[0] : ''}
            alt={getAltImage(imageUrl && imageUrl.length > 0 ? imageUrl[0] : undefined)}
            onClick={isMMO ? this.onClickRightButton : undefined}
            style={{ cursor: isMMO ? 'pointer' : '' }}
          />
        </div>
        {
          isShowGender
            ? (
              <div className="div-gender">
                {gender}
              </div>
            ) : (titleTop ? (
              <div className="div-gender">
                {titleTop}
              </div>
            ) : <div />)
        }

        <div className="div-body">
          <div className="block-1">
            <span className="ml-2">
              {name}
            </span>
            <div className="ml-2">
              <span>
                {header2 || 'Created by:'}
              </span>
              {' '}
              <span>
                {content2}
              </span>
            </div>
          </div>
          {
            isMMO
              ? (
                <div className="block-2 ml-2">
                  <span>
                    {isMMO ? '' : description}
                  </span>
                </div>
              ) : (<div />)
          }

          <div className="block-3">
            {
              isMMO
                ? (
                  <button
                    type="button"
                    className="ml-2"
                    onClick={this.onClickIngredient}
                  >
                    <strong>
                      {knowBt}
                    </strong>
                  </button>
                ) : (
                  isFavorite ? (
                    <button
                      type="button"
                      className="ml-2"
                      style={{
                        fontSize: '1.1rem',
                      }}
                    >
                      <i className="far fa-star" />
                    </button>
                  ) : (
                    <span className="ml-2">
                      {/* $96.99 */}
                    </span>
                  )
                )
            }
            <button
              type="button"
              className="mr-2"
              data-gtmtracking={this.props.dataGtmtracking}
              onClick={this.onClickRightButton}
            >
              <strong>
                { buttonName || `+ ${addCartBt ? addCartBt.toUpperCase() : ''}`}
              </strong>
            </button>
          </div>
        </div>
      </div>
    );
  }
}
ScentItem.defaultProps = {
  className: '',
  isShowGender: false,
  isMMO: false,
  isFavorite: false,
};

ScentItem.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    image_urls: PropTypes.array,
    created: PropTypes.string,
    buttonName: PropTypes.string,
    variant_values: PropTypes.array,
    titleTop: PropTypes.string,
    content2: PropTypes.string,
    header2: PropTypes.string,
    ingredient: PropTypes.shape(),
  }).isRequired,
  className: PropTypes.string,
  isShowGender: PropTypes.bool,
  isMMO: PropTypes.bool,
  // onOpenKnowMore: PropTypes.func.isRequired,
  onClickRightButton: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  isFavorite: PropTypes.bool,
  cms: PropTypes.shape().isRequired,
};

export default ScentItem;
