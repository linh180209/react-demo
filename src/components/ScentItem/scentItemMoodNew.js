import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../../styles/scentMood.scss';
import info from '../../image/icon/info.png';
import bottleDropper from '../../image/bottle-and-dropper3.png';
import { getAltImage, generaCurrency } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

class ScentItemMoodNew extends Component {
    onClickRightButton = () => {
      if (this.props.onClickRightButton) {
        this.props.onClickRightButton(this.props.data);
      }
    };

    onClickImage = () => {
      if (this.props.onClickImage) {
        const { data } = this.props;
        this.props.onClickImage(data);
      }
    };

    render() {
      const { data, className, sizeProduct } = this.props;
      const {
        name,
        image_urls: imageUrl,
        isLike,
        buttonName,
        price,
        name1,
        name2,
        ingredient1,
        ingredient2,
        buttonNameLeft,
      } = data;
      const styleImage = {
        width: '100%',
        height: isMobile && !isTablet ? '123.3px' : '185px',
      };

      const mobileMood = (
        <React.Fragment>
          <div className="div-3image">
            <div className="div-img-1 div-after-shadow">
              <img
                loading="lazy"
                className="img-1"
                src={imageUrl[0]}
                alt={getAltImage(imageUrl[0])}
                style={styleImage}
                onClick={this.onClickImage}
                style={sizeProduct === 1 ? {
                  height: '185px',
                } : {}}
              />

              <button
                type="button"
                className="button-bg__none ma-0 pa-0"
                style={{
                  position: 'absolute',
                  top: 0,
                  left: '10%',
                  zIndex: 10,
                  transform: 'translate3d(0, 0, 100px)',
                  position: 'absolute',
                  top: '2%',
                  left: '8%',

                }}
                onClick={() => this.props.onClickIngredient(ingredient1)}
              >
                <img
                  style={{ width: '15px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }}
                  src={info}
                  alt="infro"
                />
              </button>
            </div>

            <div className="div-img-3">
              <img
                loading="lazy"
                loading="lazy"
                onClick={() => this.onClickImage()}
                src={bottleDropper}
                alt={getAltImage(bottleDropper)}
              />
            </div>
            <div className="div-img-2">
              <img
                loading="lazy"
                className="img-2"
                src={imageUrl[1]}
                alt={getAltImage(imageUrl[1])}
                style={styleImage}
                onClick={this.onClickImage}
                style={sizeProduct === 1 ? {
                  height: '185px',
                } : {}}
              />
              <button
                type="button"
                className="button-bg__none ma-0 pa-0"
                style={{
                  position: 'absolute',
                  top: 0,
                  right: '10%',
                  zIndex: 10,
                  transform: 'translate3d(0, 0, 100px)',
                  position: 'absolute',
                  top: '2%',
                  right: '8%',

                }}
                onClick={() => this.props.onClickIngredient(ingredient2)}
              >
                <img
                  style={{ width: '15px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }}
                  src={info}
                  alt="infro"
                />
              </button>
            </div>
          </div>
          <div className="item-detail">
            <div className="item-price">
              <span className="custom-item-price">
                {generaCurrency(price)}
              </span>
            </div>
            <div className="item-btn-options">
              <button
                type="button"
                onClick={() => this.props.onClickLeftButton(this.props.data)
                            }
              >
                <span>
                  view product
                </span>
              </button>
              <span>
                |
              </span>
              <button
                type="button"
                onClick={() => this.onClickRightButton(this.props.data)
                            }
              >
                <span>
                  add to cart
                </span>
              </button>
            </div>
          </div>
        </React.Fragment>
      );

      const tabletMood = (
        <React.Fragment>
          <div className="div-3image">
            <div className="div-img-1 div-after-shadow">
              <img
                loading="lazy"
                className="img-1"
                src={imageUrl[0]}
                alt={getAltImage(imageUrl[0])}
                style={styleImage}
                onClick={this.onClickImage}
                style={sizeProduct === 1 ? {
                  height: '277px',
                } : {}}
              />

              <button
                type="button"
                className="button-bg__none ma-0 pa-0"
                style={{
                  position: 'absolute',
                  top: '2%',
                  left: '9%',
                  zIndex: 10,
                  transform: 'translate3d(0, 0, 100px)',
                  position: 'absolute',
                  top: '2%',
                  left: '8%',
                }}
                onClick={() => this.props.onClickIngredient(ingredient1)}
              >
                <img
                  style={{ width: '24px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }}
                  src={info}
                  alt="infro"
                />
              </button>
            </div>

            <div className="div-img-3">
              <img
                loading="lazy"
                onClick={() => this.onClickImage()}
                src={bottleDropper}
                alt={getAltImage(bottleDropper)}
              />
            </div>
            <div className="div-img-2">
              <img
                loading="lazy"
                className="img-2"
                src={imageUrl[1]}
                alt={getAltImage(imageUrl[1])}
                style={styleImage}
                onClick={this.onClickImage}
                style={sizeProduct === 1 ? {
                  height: '277px',
                } : {}}
              />
              <button
                type="button"
                className="button-bg__none ma-0 pa-0"
                style={{
                  position: 'absolute',
                  top: '2%',
                  right: '9%',
                  zIndex: 10,
                  transform: 'translate3d(0, 0, 100px)',
                  position: 'absolute',
                  top: '2%',
                  right: '8%',
                }}
                onClick={() => this.props.onClickIngredient(ingredient2)}
              >
                <img
                  style={{ width: '24px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }}
                  src={info}
                  alt="infro"
                />
              </button>
            </div>
          </div>
          <div className="item-detail">
            <div className="item-price">
              <span className="custom-item-price">
                {generaCurrency(price)}
              </span>
            </div>
            <div
              className="item-btn-options"
            >
              <button
                type="button"
                onClick={() => this.props.onClickLeftButton(this.props.data)
                            }
              >
                <span>
                  view product
                </span>
              </button>
              <span>
                |
              </span>
              <button
                type="button"
                onClick={() => this.onClickRightButton(this.props.data)
                            }
              >
                <span>
                  add to cart
                </span>
              </button>
            </div>
          </div>
        </React.Fragment>
      );
      const htmlMood = (
        <React.Fragment>
          <div className="div-3image">
            <div className="div-img-1 div-after-shadow">
              <img
                loading="lazy"
                className="img-1"
                src={imageUrl[0]}
                alt={getAltImage(imageUrl[0])}
                style={styleImage}
                onClick={this.onClickImage}
                style={sizeProduct === 1 ? {
                  height: '277px',
                } : {}}
              />

              <button
                type="button"
                className="button-bg__none ma-0 pa-0"
                style={{
                  position: 'absolute',
                  top: '2%',
                  left: '9%',
                  zIndex: 10,
                  transform: 'translate3d(0, 0, 100px)',
                  position: 'absolute',
                  top: '1%',
                  left: '10%',
                }}
                onClick={() => this.props.onClickIngredient(ingredient1)}
              >
                <img
                  style={{ width: '24px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }}
                  src={info}
                  alt="infro"
                />
              </button>
            </div>

            <div className="div-img-3">
              <img
                loading="lazy"
                onClick={() => this.onClickImage()}
                src={bottleDropper}
                alt={getAltImage(bottleDropper)}
              />
            </div>
            <div className="div-img-2">
              <img
                loading="lazy"
                className="img-2"
                src={imageUrl[1]}
                alt={getAltImage(imageUrl[1])}
                style={styleImage}
                onClick={this.onClickImage}
                style={sizeProduct === 1 ? {
                  height: '277px',
                } : {}}
              />
              <button
                type="button"
                className="button-bg__none ma-0 pa-0"
                style={{
                  position: 'absolute',
                  top: '2%',
                  right: '9%',
                  zIndex: 10,
                  transform: 'translate3d(0, 0, 100px)',
                  position: 'absolute',
                  top: '2%',
                  right: '10%',
                }}
                onClick={() => this.props.onClickIngredient(ingredient2)}
              >
                <img
                  style={{ width: '24px', zIndex: 10, transform: 'translate3d(0, 0, 100px)' }}
                  src={info}
                  alt="infro"
                />
              </button>
            </div>
          </div>
          <div className="item-detail">
            <div className="item-price">
              <span className="custom-item-price">
                {generaCurrency(price)}
              </span>
            </div>
            <div
              className="item-btn-options"
            >
              <button
                type="button"
                onClick={() => this.props.onClickLeftButton(this.props.data)
                            }
              >
                <span>
                  view product
                </span>
              </button>
              <span>
                |
              </span>
              <button
                type="button"
                onClick={() => this.onClickRightButton(this.props.data)
                            }
              >
                <span>
                  add to cart
                </span>
              </button>
            </div>
          </div>
        </React.Fragment>
      );
      return (
        <div
          className={
                    isMobile && !isTablet
                      ? `${className} div-scentItemMood-mobile`
                      : `${className} div-scentItemMood`
                }
          style={
                    isMobile && !isTablet
                      ? { height: sizeProduct === 1 ? '390px' : '260px', width: sizeProduct === 1 ? '316px' : '211px' }
                      : { height: sizeProduct === 1 ? '480px' : '320px', width: sizeProduct === 1 ? '390px' : '260px' }
                }
        >
          {isTablet ? tabletMood : isMobile ? mobileMood : htmlMood}
        </div>
      );
    }
}

ScentItemMoodNew.defaultProps = {
  className: '',
};

ScentItemMoodNew.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    image_urls: PropTypes.array,
    created: PropTypes.string,
    buttonName: PropTypes.string,
    variant_values: PropTypes.array,
    titleTop: PropTypes.string,
    isLike: PropTypes.bool,
    price: PropTypes.string,
    name1: PropTypes.string,
    name2: PropTypes.string,
    description1: PropTypes.string,
    description2: PropTypes.string,
    ingredient1: PropTypes.string,
    ingredient2: PropTypes.string,
  }).isRequired,
  sizeProduct: PropTypes.number.isRequired,
  className: PropTypes.string,
  onClickRightButton: PropTypes.func.isRequired,
  onClickLeftButton: PropTypes.func.isRequired,
  onClickImage: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
};

export default ScentItemMoodNew;
