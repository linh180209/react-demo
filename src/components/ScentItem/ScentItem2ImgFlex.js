import React, { Component } from 'react';
import PropTypes from 'prop-types';
import promote from '../../image/icon/promote.png';
import heart from '../../image/icon/heart.svg';
import infro from '../../image/icon/info.svg';
import { getAltImage, generaCurrency } from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { isMobile } from '../../DetectScreen';

class ScentItem2ImgFlex extends Component {
  onClickRightButton = () => {
    if (this.props.onClickRightButton) {
      this.props.onClickRightButton(this.props.data);
    }
  }

  onClickLeftButton = () => {
    if (this.props.onClickLeftButton) {
      this.props.onClickLeftButton(this.props.data);
    }
  }

  onClickImage = () => {
    if (this.props.onClickImage) {
      const { data } = this.props;
      this.props.onClickImage(data);
    }
  }

  render() {
    const {
      data, className,
    } = this.props;
    const {
      name, image_urls: imageUrl, created, isLike, buttonName, price, isInfo, info1, info2,
    } = data;
    return (
      <div className={isMobile ? `${className} div-scentItem-mobile` : `${className} div-scentItem`}>
        <div
          className="div-2image"
          style={{ position: 'relative' }}
        >
          <div onClick={this.onClickImage} className="div-img-1" style={{ position: 'relative', cursor: 'pointer' }}>
            <img loading="lazy" src={imageUrl[0]} alt={getAltImage(imageUrl[0])} />
          </div>
          <div onClick={this.onClickImage} className="div-img-2" style={{ position: 'relative', cursor: 'pointer' }}>
            <img loading="lazy" src={imageUrl[1]} alt={getAltImage(imageUrl[1])} />
          </div>
          {
              isInfo
                ? (
                  <button
                    type="button"
                    className="button-bg__none ma-0 pa-0"
                    style={{ position: 'absolute', bottom: '10px', left: '5px' }}
                    onClick={() => { this.props.onClickInfo(info1); }}
                  >
                    <img style={{ width: '24px' }} src={infro} alt="infro" />
                  </button>
                ) : <div />
            }
          {
              isInfo
                ? (
                  <button
                    type="button"
                    className="button-bg__none ma-0 pa-0"
                    style={{ position: 'absolute', bottom: '10px', right: '33%' }}
                    onClick={() => { this.props.onClickInfo(info2); }}
                  >
                    <img style={{ width: '24px' }} src={infro} alt="infro" />
                  </button>
                ) : <div />
            }
        </div>

        <div className="div-body">
          <div className="block-1">
            <span className="ml-2" style={{ fontWeight: '600', fontSize: '15px' }}>
              {name}
            </span>
            <div className={created ? 'ml-2' : 'hiden'}>
              <span>
                Created by:
              </span>
              {' '}
              <span>
                {created || 'LOSI SHERRARD'}
              </span>
            </div>
          </div>
          <span className={price ? 'mr-2' : 'hiden'} style={{ textAlign: 'right' }}>
            {generaCurrency(price)}
          </span>
          <div className="block-3" style={{ justifyContent: 'space-between' }}>
            <button
              type="button"
              className={isLike ? 'ml-2' : 'hiden'}
              onClick={this.onClickLeftButton}
            >
              <img style={{ width: '24px' }} src={heart} alt="heart" />
            </button>

            <button
              type="button"
              className="mr-2"
              onClick={() => this.onClickRightButton(this.props.data)}
              style={{ fontWeight: '600' }}
            >
              {
                buttonName || (
                  <div>
                    <img src={promote} alt="promote" />
                    <span>
                      PROMOTE CREATION
                    </span>
                  </div>
                )
              }

            </button>
          </div>
        </div>
      </div>
    );
  }
}
ScentItem2Img.defaultProps = {
  className: '',
};

ScentItem2Img.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    image_urls: PropTypes.array,
    created: PropTypes.string,
    buttonName: PropTypes.string,
    variant_values: PropTypes.array,
    titleTop: PropTypes.string,
    isLike: PropTypes.bool,
    price: PropTypes.string,
    isInfo: PropTypes.bool,
    info1: PropTypes.shape,
    info2: PropTypes.shape,
  }).isRequired,
  className: PropTypes.string,
  onClickRightButton: PropTypes.func.isRequired,
  onClickImage: PropTypes.func.isRequired,
  onClickLeftButton: PropTypes.func.isRequired,
  onClickInfo: PropTypes.func.isRequired,
};

export default ScentItem2ImgFlex;
