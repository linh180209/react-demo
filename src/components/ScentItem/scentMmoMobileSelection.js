import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getAltImage, getCmsCommon, getNameFromCommon } from '../../Redux/Helpers';

class ScentItemMobileSelection extends Component {
  // onOpenKnowMore = () => {
  //   if (this.props.onOpenKnowMore) {
  //     this.props.onOpenKnowMore(this.props.data);
  //   }
  // }

  onClickIngredient = () => {
    if (this.props.onClickIngredient) {
      this.props.onClickIngredient(this.props.data.ingredient);
    }
  }

  onClickRightButton = () => {
    if (this.props.onClickRightButton) {
      this.props.onClickRightButton(this.props.data);
    }
  }

  render() {
    const {
      data, className,
    } = this.props;
    const {
      name, image_urls: imageUrl, buttonName, content2,
    } = data;
    const cmsCommon = getCmsCommon(this.props.cms);
    const knowBt = getNameFromCommon(cmsCommon, 'KNOW_MORE');
    const addCartBt = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
    const familyBt = getNameFromCommon(cmsCommon, 'Family');
    return (
      <div
        style={{
          width: '300px', height: '85px', borderRadius: '20px', border: '1px solid #777',
        }}
        className={`${className} div-row`}
      >
        <img
          loading="lazy"
          style={{
            width: '75px', height: '100%', objectFit: 'cover', objectPosition: 'center', borderRadius: '20px 0px 0px 20px',
          }}
          src={imageUrl && imageUrl.length > 0 ? imageUrl[0] : ''}
          alt={getAltImage(imageUrl && imageUrl.length > 0 ? imageUrl[0] : undefined)}
        />
        <div className="div-col ml-2" style={{ flex: '1' }}>
          <span>
            {name}
          </span>
          <div className="div-row">
            <span style={{
              fontStyle: 'italic', color: '#777', fontSize: '0.8rem',
            }}
            >
              {familyBt}
              :
              {' '}
            </span>
            <span style={{ fontSize: '0.8rem', marginLeft: '5px' }}>
              {content2}
            </span>
          </div>
          <div className="div-row justify-between mt-3">
            <button
              type="button"
              className="pa0 ma0 button-bg__none"
              style={{ fontSize: '0.8rem' }}
              onClick={this.onClickIngredient}
            >
              <strong>
                {knowBt}
              </strong>
            </button>

            <button
              type="button"
              className="mr-2 pa0 ma0 button-bg__none"
              style={{ fontSize: '0.8rem' }}
              onClick={this.onClickRightButton}
              data-gtmtracking={this.props.dataGtmtracking}
            >
              <strong>
                { buttonName || `+ ${addCartBt}`}
              </strong>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

ScentItemMobileSelection.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    image_urls: PropTypes.array,
    created: PropTypes.string,
    buttonName: PropTypes.string,
    variant_values: PropTypes.array,
    titleTop: PropTypes.string,
    content2: PropTypes.string,
    header2: PropTypes.string,
    ingredient: PropTypes.shape,
  }).isRequired,
  className: PropTypes.string,
  isShowGender: PropTypes.bool,
  isMMO: PropTypes.bool,
  // onOpenKnowMore: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  onClickRightButton: PropTypes.func.isRequired,
  isFavorite: PropTypes.bool,
};

export default ScentItemMobileSelection;
