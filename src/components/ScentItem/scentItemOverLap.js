import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../../styles/scentItem.scss';

import { getAltImage, generaCurrency } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

class ScentItemOverlap extends Component {
  onClickRightButton = () => {
    if (this.props.onClickRightButton) {
      this.props.onClickRightButton(this.props.data);
    }
  }

  onClickImage = () => {
    if (this.props.onClickImage) {
      const { data } = this.props;
      this.props.onClickImage(data);
    }
  }

  render() {
    const {
      data, className, isMyOrder,
    } = this.props;
    const {
      name, image_urls: imageUrl, description, buttonName, price, isSampleProduct, buttonNameLeft,
    } = data;
    const textAlign = isMyOrder ? 'left' : 'center';
    const widthMobile = `${260 / 1.5}px`;
    const heightMobile = `${385 / 1.5}px`;
    return (
      <div className={`${className} div-scentItem-mmo`} style={isMobile && !isTablet ? { width: widthMobile, height: heightMobile } : {}}>
        <div
          className="div-2image"
          style={{ position: 'relative' }}
        >
          <div
            className="div-img-1"
            style={isSampleProduct ? { width: '100%' } : {}}
          >
            <img
              loading="lazy"
              src={imageUrl[0]}
              alt={getAltImage(imageUrl[0])}
              onClick={isSampleProduct ? undefined : this.onClickImage}
              style={isSampleProduct ? {
                width: '100%',
                height: '100%',
                objectFit: 'center',
                borderTopRightRadius: '20px',
                cursor: 'pointer',
              } : {}}
            />
          </div>
          <div
            className={isSampleProduct ? 'hidden' : 'div-img-2'}
          >
            <img
              loading="lazy"
              src={imageUrl[1]}
              alt={getAltImage(imageUrl[1])}
              onClick={this.onClickImage}
              style={{ cursor: 'pointer' }}
            />
          </div>
        </div>

        <div className="div-body">
          <div className="block-1">
            <span className="ml-2" style={isMobile && !isTablet ? { fontWeight: '600', fontSize: '0.6rem' } : { fontWeight: '600', fontSize: '15px' }}>
              {name}
            </span>
            <span
              className="ml-2 mr-2"
              style={isMobile && !isTablet ? {
                marginTop: '7px', textAlign, fontSize: '0.5rem',
              } : { marginTop: '7px', textAlign }}
            >
              {description}
            </span>
          </div>
          <span className={price ? 'mr-2' : 'hiden'} style={isMobile && !isTablet ? { textAlign: 'right', fontSize: '0.6rem' } : { textAlign: 'right' }}>
            {generaCurrency(price)}
          </span>
          <div className="block-3" style={{ justifyContent: 'flex-between' }}>
            <button
              type="button"
              className="ml-2"
              onClick={() => this.props.onClickLeftButton(data)}
              style={isMobile && !isTablet ? { fontWeight: '600', fontSize: '0.5rem' } : { fontWeight: '600' }}
            >
              {
                buttonNameLeft
              }
            </button>
            <button
              type="button"
              className="mr-2"
              onClick={() => this.props.onClickRightButton(data)}
              style={isMobile && !isTablet ? { fontWeight: '600', fontSize: '0.5rem' } : { fontWeight: '600' }}
            >
              {
                buttonName
              }
            </button>
          </div>
        </div>
      </div>
    );
  }
}
ScentItemOverlap.defaultProps = {
  className: '',
  isMyOrder: false,
};

ScentItemOverlap.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    image_urls: PropTypes.array,
    created: PropTypes.string,
    buttonName: PropTypes.string,
    buttonNameLeft: PropTypes.string,
    variant_values: PropTypes.array,
    titleTop: PropTypes.string,
    isLike: PropTypes.bool,
    price: PropTypes.string,
    description: PropTypes.string,
    isSampleProduct: PropTypes.bool,
  }).isRequired,
  isMyOrder: PropTypes.bool,
  className: PropTypes.string,
  onClickRightButton: PropTypes.func.isRequired,
  onClickLeftButton: PropTypes.func.isRequired,
  onClickImage: PropTypes.func.isRequired,
};

export default ScentItemOverlap;
