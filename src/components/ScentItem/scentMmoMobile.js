import React, { Component } from 'react';
import PropTypes from 'prop-types';
import closeIcon from '../../image/icon/close.svg';
import { getAltImage } from '../../Redux/Helpers';

class ScentMmoMobile extends Component {
  render() {
    const { data } = this.props;
    const { image_urls: imageUrl, name } = data;
    return (
      <div
        className="div-row justify-center items-center"
        style={{
          width: '140px', height: '50px', border: '1px solid', borderRadius: '14px',
        }}
      >
        <img
          loading="lazy"
          src={imageUrl && imageUrl.length > 0 ? imageUrl[0] : ''}
          alt={getAltImage(imageUrl && imageUrl.length > 0 ? imageUrl[0] : undefined)}
          style={{
            width: '35px',
            height: '100%',
            objectFit: 'cover',
            objectPosition: 'center',
            borderRadius: '14px 0px 0px 14px',
          }}
        />
        <span
          style={{
            fontSize: '0.6rem', flex: '1', textAlign: 'center', paddingLeft: '5px',
          }}
        >
          {name}
        </span>
        <button
          type="button"
          className="button-bg__none"
          onClick={() => this.props.onClickRightButton(this.props.data)}
        >
          <img src={closeIcon} style={{ width: '12px' }} alt="closeIcon" />
        </button>
      </div>
    );
  }
}

ScentMmoMobile.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    image_urls: PropTypes.array,
    created: PropTypes.string,
    buttonName: PropTypes.string,
    variant_values: PropTypes.array,
    titleTop: PropTypes.string,
    content2: PropTypes.string,
    header2: PropTypes.string,
  }).isRequired,
  className: PropTypes.string,
  isMMO: PropTypes.bool,
  // onOpenKnowMore: PropTypes.func.isRequired,
  onClickRightButton: PropTypes.func.isRequired,
  // onClickIngredient: PropTypes.func.isRequired,
};
export default ScentMmoMobile;
