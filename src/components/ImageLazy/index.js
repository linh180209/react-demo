import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import axios from 'axios';
import ReactLoading from 'react-loading';
import { isIOS } from 'react-device-detect';
import { isMobile } from '../../DetectScreen';

class ImageLazy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      liked: false,
    };
  }

  componentDidMount = () => {
    // const { url } = this.props;
    // this.fetchDataPdf(url);
  }

  componentDidUpdate = () => {
    // const { isFetchData, url } = this.state;
    // if (isFetchData) {
    //   this.fetchDataPdf(url);
    //   this.setState({ isFetchData: false });
    // }
  }

  onClickItem = () => {
    const { title, content } = this.props;
    this.props.onOpenKnowMore(title, content);
  }

  onClickLike = () => {
    this.props.onClickFavorite(this.props.id);
  }

  static getDerivedStateFromProps = (nextProps, prevState) => {
    const { url } = nextProps;
    if (url !== prevState.url) {
      return { url, loaded: false };
    }
    return null;
  }

  loadImageDone = () => {
    this.setState({ loaded: true });
  }

  render() {
    const { url, loaded } = this.state;
    const { alt, isLikeProducts } = this.props;
    const style = isMobile && isIOS ? {
      position: 'absolute',
      bottom: '0.4rem',
      left: '0.5rem',
    } : {};
    if (isLikeProducts) {
      _.assign(style, { color: 'red' });
    } else {
      _.assign(style, { color: '#fff' });
    }
    return (
      <div style={!isMobile ? {
        height: '60vh', position: 'relative', flexDirection: 'column', justifyContent: 'center',
      } : {
        width: '100%', position: 'relative', flexDirection: 'column', justifyContent: 'center',
      }}
      >
        <img
          loading="lazy"
          src={url}
          alt={alt}
          onLoad={this.loadImageDone}
        />
        <div style={loaded ? {
          width: '0px',
          height: '0px',
        } : (!isMobile ? {
          position: 'absolute',
          height: '100%',
          width: '33.7vh',
          top: '0px',
          right: '0px',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        } : {
          position: 'absolute',
          height: '40vh',
          width: '100%',
          top: '0px',
          right: '0px',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        })}
        >
          {
            !loaded ? (
              <ReactLoading className="react-loading" type="spinningBubbles" color="#1a1a1a" height={50} width={50} />
            ) : <div />
            }
        </div>
        <div>
          <button
            type="button"
            className="bt-know-more"
            onClick={this.onClickItem}
          >
            <i
              className="fa fa-info"
              style={isMobile && isIOS ? {
                position: 'absolute',
                bottom: '0.4rem',
                right: '0.8rem',
              } : {}}
            />
          </button>
          <button
            type="button"
            className="bt-like"
            onClick={this.onClickLike}
          >
            <i
              className="fa fa-heart"
              style={style}
            />
          </button>
        </div>
      </div>
    );
  }
}

ImageLazy.propTypes = {
  url: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  onOpenKnowMore: PropTypes.func.isRequired,
  onClickFavorite: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  isLikeProducts: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
};

export default ImageLazy;
