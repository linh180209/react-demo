import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { Progress } from 'reactstrap';
import classnames from 'classnames';
import './styles.scss';
import { generaCurrency, getNameFromButtonBlock } from '../../Redux/Helpers';
import icBottle from '../../image/icon/ic-bottle-buy-more.svg';
import icGift from '../../image/icon/ic-gift.svg';
import icFreeShip from '../../image/icon/ic-free-ship.svg';
import icCheck from '../../image/icon/ic-check-bg-green.svg';


function BuyMoreGiftShipping(props) {
  const [datas, setDatas] = useState([]);

  const itemRender = (data, id, classCustome) => {
    const spendBt = getNameFromButtonBlock(props.buttonBlock, 'Spend');
    const giftBt = getNameFromButtonBlock(props.buttonBlock, 'to qualify for a free gift');
    const shippingBt = getNameFromButtonBlock(props.buttonBlock, 'to qualify for free shipping');
    const freeShippingBt = getNameFromButtonBlock(props.buttonBlock, 'Free shipping');
    return (
      <div id={`div-${id}`} className={classnames('item-render', classCustome)}>
        {
          data?.type === 'shipping' && data?.value === 0 ? (
            <span>
              {freeShippingBt}
            </span>
          ) : (
            <span>
              {spendBt}
              {' '}
              <b>{generaCurrency(data?.value)}</b>
              {' '}
              {data?.type === 'shipping' ? shippingBt : giftBt}
            </span>
          )
        }

        <div id={id} className="div-icon">
          <img src={data?.type === 'shipping' ? icFreeShip : icGift} alt="icon" />
          {
          data?.numberGift > 1 && (
            <div className="number">
              {data?.numberGift}
            </div>
          )
        }
        </div>
      </div>
    );
  };

  const getPositionProgress = (isFullItem) => {
    let startX1 = 0;
    let startY1 = 0;
    let rotate1 = 0;
    let length1 = 0;
    let startX2 = 0;
    let startY2 = 0;
    let rotate2 = 0;
    let length2 = 0;
    const ele = document.getElementById('id-content-info');
    const ele1 = document.getElementById('item-render-1');
    const ele2 = document.getElementById('item-render-2');
    const ele3 = document.getElementById('item-render-3');
    if (ele && ele1 && ele2) {
      const rectEle = ele.getBoundingClientRect();
      const rectEle1 = ele1.getBoundingClientRect();
      const rectEle2 = ele2.getBoundingClientRect();


      startX1 = rectEle1.right - rectEle.left;
      startY1 = rectEle1.top - rectEle.top + 10;
      const a = rectEle2.left - rectEle1.right;
      const b = rectEle2.top - rectEle1.top;
      length1 = Math.sqrt(a * a + b * b);
      rotate1 = b / length1 * 180 / Math.PI;
      if (ele3) {
        const rectEle3 = ele3.getBoundingClientRect();
        startX2 = rectEle2.right - rectEle.left;
        startY2 = rectEle2.top - rectEle.top + 10;
        const a1 = rectEle3.left - rectEle2.right;
        const b1 = rectEle3.top - rectEle2.top;
        length2 = Math.sqrt(a1 * a1 + b1 * b1);
        rotate2 = b1 / length2 * 180 / Math.PI;
      }
    }

    return {
      startX1,
      startY1,
      length1,
      rotate1,
      startX2,
      startY2,
      length2,
      rotate2,
    };
  };

  useEffect(() => {
    const listData = _.map(_.orderBy(props.giftFrees, ['amount'], 'asc') || [], (x, index) => ({ value: x.amount, type: 'gift', numberGift: index + 1 }));
    if (props.shippingSelection && parseFloat(props.shippingSelection?.threshold) > 0) {
      listData.push({
        value: parseFloat(props.shippingSelection?.threshold),
        type: 'shipping',
      });
    }
    console.log('listData', listData);
    const sortList = _.orderBy(listData, ['value'], ['asc']);
    if (sortList.length < 3) {
      sortList.unshift({
        value: 0,
        type: 'shipping',
      });
    }
    setDatas(sortList);
  }, [props.giftFrees, props.shippingSelection]);

  let currentItem = datas && datas.length > 0 ? datas[0] : undefined;
  let gotItem;
  let indexGotItem;
  for (let i = 0; i < datas.length; i += 1) {
    if (datas[i].value > props.totalPerfumeExlixir) {
      currentItem = datas[i];
      break;
    } else {
      gotItem = datas[i];
      indexGotItem = i;
    }
  }

  const isFullItem = datas?.length > 2;

  const {
    startX1, startY1, length1, rotate1,
    startX2, startY2, length2, rotate2,
  } = getPositionProgress(isFullItem);

  // const max = datas && datas.length > 0 ? datas[datas.length - 1].value : 0;
  const percent1 = datas?.length > 1 && props.totalPerfumeExlixir > datas[0].value ? props.totalPerfumeExlixir / datas[1]?.value * 100 : 0;
  const percent2 = datas?.length > 2 && props.totalPerfumeExlixir > datas[1].value ? (props.totalPerfumeExlixir - datas[1]?.value) / (datas[2].value - datas[1].value) * 100 : 0;

  const currentPercent = datas && datas.length > 0 && props.totalPerfumeExlixir ? props.totalPerfumeExlixir / datas[datas.length - 1].value * 100 : 0;
  // const moreSpend = currentItem && props.totalPerfumeExlixir ? currentItem.value - props.totalPerfumeExlixir : '';
  // const numberGifted = gotItem && gotItem.type === 'gift' ? gotItem.numberGift : 1;
  // const isGifted = gotItem && gotItem.type === 'gift' || indexGotItem > 0;
  // const isFreeShipping = !!(gotItem && props.shippingSelection && gotItem.value >= parseFloat(props.shippingSelection?.threshold));

  const yourFreeBt = getNameFromButtonBlock(props.buttonBlock, 'Your Free Gifts & Free Shipping');
  const spendBt = getNameFromButtonBlock(props.buttonBlock, 'Spend or more to qualify');
  // const gotoBt = getNameFromButtonBlock(props.buttonBlock, 'go to');
  // const giftShippingFreeBt = getNameFromButtonBlock(props.buttonBlock, 'Qualified for free gift and free shipping');
  // const giftSelectedBt = getNameFromButtonBlock(props.buttonBlock, 'Free gifts selected');
  // const pleaseSelectBt = getNameFromButtonBlock(props.buttonBlock, 'Please select your Free gifts (left)');
  // const giftFreeBt = getNameFromButtonBlock(props.buttonBlock, 'Qualified for free gift');
  // const shippingFreeBt = getNameFromButtonBlock(props.buttonBlock, 'Qualified for free shipping');
  return (
    <div className="buy-more-gift-shipping">

      <h3>{yourFreeBt}</h3>
      {currentItem && currentPercent < 100 && <span className="message1">{spendBt.replace('{price}', generaCurrency(currentItem?.value))}</span>}
      <div id="id-content-info" className={classnames('content-info', !isFullItem ? 'two-item' : '')}>
        {
          _.map(datas, (d, index) => (
            itemRender(d, `item-render-${index + 1}`, index === 1 ? 'class-bottom' : '')
          ))
        }
        <Progress
          value={percent1}
          className="cus-progress"
          id="cus-progress-1"
          max={100}
          min={0}
          style={{
            left: startX1 - 5,
            top: startY1 + 2,
            width: length1 + 10,
            transform: `rotate(${rotate1}deg)`,
          }}
        />
        {
          datas.length > 2 && (
            <Progress
              value={percent2}
              className="cus-progress"
              id="cus-progress-2"
              max={100}
              min={0}
              style={{
                left: startX2 - 5,
                top: startY2 + 4,
                width: length2 + 10,
                transform: `rotate(${rotate2}deg)`,
              }}
            />
          )
        }

      </div>
      {/* <div className="div-progress">
        <Progress value={currentPercent} className="cus-progress" max={100} min={0} />
        {
          _.map(datas, (d, index) => (
            <div
              className={classnames(
                'number-percent',
                (d.value <= props.totalPerfumeExlixir ? 'active' : ''),
                (props.isShowBottle ? 'div-bottle' : ''),
                (d.type === 'shipping' ? 'ic-truck' : ''),
              )}
              style={{
                left: `${parseFloat(d.value) / max * 100}%`,
              }}
            >

              <div className="show-bottle">
                <div className="bottle">
                  {
                    d.type === 'shipping' ? (
                      <img src={icFreeShip} alt="shipping" />
                    ) : (
                      _.map(_.range(0, d.numberGift), k => (
                        <img src={icGift} alt="bottle" />
                      ))
                    )
                  }
                </div>
              </div>
            </div>
          ))
        }
      </div>
      {
        gotItem ? (
          <div className="message-got">
            <div className="div-icon div-row items-center">
              <img src={icCheck} alt="icon" />
              <span className="text-qualify">
                {isGifted && isFreeShipping ? giftShippingFreeBt : isGifted ? giftFreeBt : isFreeShipping ? shippingFreeBt : ''}
              </span>
            </div>
          </div>
        ) : (
          moreSpend && <span className="message2">{gotoBt.replace('{price}', generaCurrency(moreSpend))}</span>
        )
      } */}

    </div>
  );
}

export default BuyMoreGiftShipping;
