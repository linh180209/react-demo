import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { Label, Input } from 'reactstrap';
import arrowDown from '../../image/icon/arrow-down-filter.svg';
import arrowUp from '../../image/icon/arrow-up-filter.svg';

class MenuFilterItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: true,
    };
  }

  onToggle = () => {
    if (this.props.isMobile) {
      return;
    }
    const { isOpen } = this.state;
    this.setState({ isOpen: !isOpen });
  }

  onChange = (e) => {
    const { checked, name } = e.target;
    const { datas } = this.props;
    const { title } = datas;
    if (checked) {
      this.props.addSearchFilter(name, title);
    } else {
      this.props.removeSearchFilter(name, title);
    }
  }

  render() {
    const { isOpen } = this.state;
    const { isMobile, datas, valueSearch } = this.props;
    const { title, data } = datas;
    const filterValue = _.find(valueSearch.filter, x => x.title === title);
    return (
      <div className="div-col mt-3 text-underline">
        <button
          type="button"
          className="button-bg__none pa0"
          onClick={this.onToggle}
        >
          <div className="div-row justify-between items-center">
            <span className="text-filter-header text-filter-active">
              {title}
            </span>
            <img loading="lazy" src={isOpen ? arrowUp : arrowDown} alt="icon" style={{ height: '10px', cursor: 'pointer', display: isMobile ? 'none' : '' }} />
          </div>
        </button>
        <div className={isOpen ? 'mt-3' : ''} />
        {
          isOpen ? (
            _.map(_.orderBy(data, 'title', 'asc'), (d, index) => (
              <div className={`${index === 0 ? '' : 'mt-1'}`}>
                <Label check style={{ cursor: 'pointer' }} className="text-filter-active">
                  <Input
                    name={d.title}
                    id={`checkbox${index + 1}`}
                    type="checkbox"
                    className="custom-input-filter__checkbox"
                    onChange={this.onChange}
                    checked={!!(filterValue && filterValue.data && filterValue.data.includes(d.title))}
                  />
                  {' '}
                  <Label
                    for={`checkbox${index + 1}`}
                    style={{ pointerEvents: 'none' }}
                  />
                  {' '}
                  {d.title}
                  {' '}
                  {`(${d.items.length})`}
                </Label>
              </div>
            ))) : <div />
        }
        <div className="mb-3" />
      </div>
    );
  }
}

MenuFilterItem.propTypes = {
  isMobile: PropTypes.bool.isRequired,
  datas: PropTypes.object.isRequired,
  removeSearchFilter: PropTypes.func.isRequired,
  addSearchFilter: PropTypes.func.isRequired,
  valueSearch: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default MenuFilterItem;
