import React, { Component } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Col,
  Label,
  Input,
} from 'reactstrap';
import classnames from 'classnames';
import InputMask from 'react-input-mask';
import toastr from 'toastr';
import Loading from '../Loading';
import { addFontCustom } from '../../Redux/Helpers';

export default class ModalRegister extends Component {
  constructor(props) {
    super(props);

    this.data = {
      userName: '',
      passWord: '',
      firstName: '',
      lastName: '',
      address: '',
      city: '',
      country: '',
      phone: '',
      email: '',
      DoB: '',
    };

    this.state = {
      modal: false,
      invalidField: '',
    };

    this.refLoading = React.createRef();
  }

  onChange = (e) => {
    const { value, name } = e.target;
    const { invalidField } = this.state;
    if (invalidField !== '') {
      this.setState({ invalidField: '' });
    }
    this.data[name] = value;
  }

  checkInfo = () => {
    if (!this.data.userName) {
      this.setState({ invalidField: 'userName' });
      toastr.error('Please fill out the missing fields', 'Error');
      return;
    }
    if (!this.data.passWord) {
      this.setState({ invalidField: 'passWord' });
      toastr.error('Please fill out the missing fields', 'Error');
      return;
    }
    if (!this.data.firstName) {
      this.setState({ invalidField: 'firstName' });
      toastr.error('Please fill out the missing fields', 'Error');
      return;
    }
    if (!this.data.lastName) {
      this.setState({ invalidField: 'lastName' });
      toastr.error('Please fill out the missing fields', 'Error');
      return;
    }
    if (!this.data.address) {
      this.setState({ invalidField: 'address' });
      toastr.error('Please fill out the missing fields', 'Error');
      return;
    }
    if (!this.data.city) {
      this.setState({ invalidField: 'city' });
      toastr.error('Please fill out the missing fields', 'Error');
      return;
    }
    if (!this.data.country) {
      this.setState({ invalidField: 'country' });
      toastr.error('Please fill out the missing fields', 'Error');
      return;
    }
    if (!this.data.phone) {
      this.setState({ invalidField: 'phone' });
      toastr.error('Please fill out the missing fields', 'Error');
      return;
    }
    if (!this.data.email) {
      this.setState({ invalidField: 'email' });
      toastr.error('Please fill out the missing fields', 'Error');
      return;
    }
    if (!this.data.DoB) {
      this.setState({ invalidField: 'DoB' });
      toastr.error('Please fill out the missing fields', 'Error');
    } else {
      this.data = {
        userName: '',
        passWord: '',
        firstName: '',
        lastName: '',
        address: '',
        city: '',
        country: '',
        phone: '',
        email: '',
        DoB: '',
      };

      this.refLoading.current.toggleLoading();
      setTimeout(() => {
        toastr.success('Valid information', 'Success');
        this.toggleModal();
        this.refLoading.current.toggleLoading();
      }, 2000);
    }
  }

  toggleModal = () => {
    const { modal } = this.state;
    this.setState({
      modal: !modal,
    });
  }

  createClick = () => {
    this.checkInfo();
  }

  render() {
    const { modal } = this.state;
    const { invalidField } = this.state;
    const {
      userName, passWord, firstName, lastName, address, city, country, phone, email, DoB,
    } = this.data;
    return (
      <div className="register">
        <Loading ref={this.refLoading} />
        <Modal className={classnames('register__modal', addFontCustom())} isOpen={modal} toggle={this.toggleModal}>
          <ModalHeader toggle={this.toggleModal}>
            Register Form
          </ModalHeader>
          <ModalBody>
            <Form className="form-horizontal">
              <FormGroup row>
                <Col md="3">
                  <Label htmlFor="text-input">
                    Username
                  </Label>
                </Col>
                <Col xs="12" md="9">
                  <Input
                    className={invalidField === 'userName' ? 'is-invalid' : ''}
                    type="text"
                    name="userName"
                    placeholder="Username"
                    defaultValue={userName}
                    onChange={this.onChange}
                  />
                </Col>
              </FormGroup>

              <FormGroup row className="mt-4">
                <Col md="3">
                  <Label htmlFor="text-input">
                    Password
                  </Label>
                </Col>
                <Col xs="12" md="9">
                  <Input
                    className={invalidField === 'passWord' ? 'is-invalid' : ''}
                    type="password"
                    name="passWord"
                    placeholder="Password"
                    defaultValue={passWord}
                    onChange={this.onChange}
                  />
                </Col>
              </FormGroup>

              <FormGroup row className="mt-4">
                <Col md="3">
                  <Label htmlFor="text-input">
                    First Name
                  </Label>
                </Col>
                <Col xs="12" md="9">
                  <Input
                    className={invalidField === 'firstName' ? 'is-invalid' : ''}
                    type="text"
                    name="firstName"
                    placeholder="First Name"
                    defaultValue={firstName}
                    onChange={this.onChange}
                  />
                </Col>
              </FormGroup>

              <FormGroup row className="mt-4">
                <Col md="3">
                  <Label htmlFor="text-input">
                    Last Name
                  </Label>
                </Col>
                <Col xs="12" md="9">
                  <Input
                    className={invalidField === 'lastName' ? 'is-invalid' : ''}
                    type="text"
                    name="lastName"
                    placeholder="Last Name"
                    defaultValue={lastName}
                    onChange={this.onChange}
                  />
                </Col>
              </FormGroup>

              <FormGroup row className="mt-4">
                <Col md="3">
                  <Label htmlFor="text-input">
                    Address
                  </Label>
                </Col>
                <Col xs="12" md="9">
                  <Input
                    className={invalidField === 'address' ? 'is-invalid' : ''}
                    type="text"
                    name="address"
                    placeholder="Address"
                    defaultValue={address}
                    onChange={this.onChange}
                  />
                </Col>
              </FormGroup>

              <FormGroup row className="mt-4">
                <Col md="3">
                  <Label htmlFor="text-input">
                    City
                  </Label>
                </Col>
                <Col xs="12" md="9">
                  <Input
                    className={invalidField === 'city' ? 'is-invalid' : ''}
                    type="text"
                    name="city"
                    placeholder="City"
                    defaultValue={city}
                    onChange={this.onChange}
                  />
                </Col>
              </FormGroup>

              <FormGroup row className="mt-4">
                <Col md="3">
                  <Label htmlFor="text-input">
                    Country
                  </Label>
                </Col>
                <Col xs="12" md="9">
                  <Input
                    className={invalidField === 'country' ? 'is-invalid' : ''}
                    type="text"
                    name="country"
                    placeholder="Country"
                    defaultValue={country}
                    onChange={this.onChange}
                  />
                </Col>
              </FormGroup>

              <FormGroup row className="mt-4">
                <Col md="3">
                  <Label htmlFor="text-input">
                    Phone
                  </Label>
                </Col>
                <Col xs="12" md="9">
                  <InputMask
                    className={invalidField === 'phone' ? 'is-invalid form-control' : 'form-control'}
                    type="tel"
                    name="phone"
                    placeholder="Phone"
                    mask="9999-999-9999"
                    maskChar={null}
                    defaultValue={phone}
                    onChange={this.onChange}
                  />
                </Col>
              </FormGroup>

              <FormGroup row className="mt-4">
                <Col md="3">
                  <Label htmlFor="text-input">
                    Email
                  </Label>
                </Col>
                <Col xs="12" md="9">
                  <Input
                    className={invalidField === 'email' ? 'is-invalid' : ''}
                    type="email"
                    name="email"
                    placeholder="Email"
                    defaultValue={email}
                    onChange={this.onChange}
                  />
                </Col>
              </FormGroup>

              <FormGroup row className="mt-4">
                <Col md="3">
                  <Label htmlFor="text-input">
                    DoB
                  </Label>
                </Col>
                <Col xs="12" md="9">
                  <InputMask
                    className={invalidField === 'DoB' ? 'is-invalid form-control' : 'form-control'}
                    name="DoB"
                    placeholder="DoB"
                    mask="99/99/9999"
                    maskChar={null}
                    defaultValue={DoB}
                    onChange={this.onChange}
                  />
                </Col>
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter className="modal-footer-edit">
            <Button color="danger" className="btn--cancel" onClick={this.toggleModal}>
              Cancel
            </Button>
            {' '}
            <Button color="primary" className="btn--create" onClick={this.createClick}>
              Create
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
