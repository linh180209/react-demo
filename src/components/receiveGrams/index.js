import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import {
  logoBlack,
} from '../../imagev2/svg';
import { GET_USER_ACTIONS } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { useMergeState } from '../../Utils/customHooks';
import icLogo from '../../image/icon/logo-gold.svg';
import { generaCurrency, getNameFromButtonBlock } from '../../Redux/Helpers';
import './styles.scss';

function ReceiveGrams(props) {
  const [state, setState] = useMergeState({
    pointUser: 1,
    spendUser: 1,
  });

  const handleShowPoint = () => {
    const options = {
      url: GET_USER_ACTIONS,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result && !result.isError) {
        const ele = _.find(result, x => x.type === 'buy');
        if (ele) {
          setState({ pointUser: ele?.point, spendUser: ele?.spend });
        }
      }
    }).catch((err) => {
      console.log(err.mesage);
    });
  };

  useEffect(() => {
    handleShowPoint();
  }, []);

  const receiveGrams = getNameFromButtonBlock(props.buttonBlocks, 'receive_grams');
  const estimatedBt = getNameFromButtonBlock(props.buttonBlocks, 'estimated shipping cost');
  const EarnBt = getNameFromButtonBlock(props.buttonBlocks, 'Earn');
  const pointOnBt = getNameFromButtonBlock(props.buttonBlocks, 'point on this purchase');
  const point = props.price ? parseInt(props.price * state.pointUser / state.spendUser, 10) : 0;
  if (props.isPointProductDetail) {
    return (
      <div className="line-point div-row">
        <img src={logoBlack} alt="icon" />
        <span>
          {EarnBt}
          {' '}
          <b>
            {point}
            G
          </b>
          {' '}
          {pointOnBt}
        </span>
      </div>
    );
  }
  return (
    <div>
      <div className="div-point-detail div-row">
        <img src={logoBlack} alt="logo" />
        <span className="header_5">{receiveGrams?.replace('{point}', point)}</span>
      </div>
      {
        !props.isNotShowShipping && (
          <div className="div-estimate-shipping div-row">
            <h4 className="header_4">
              {estimatedBt.replace('{country}', props.countryName)}
            </h4>
            <h4 className="header_4">
              {generaCurrency(props.shippingValue, true)}
            </h4>
          </div>
        )
      }

    </div>

  );
}

export default ReceiveGrams;
