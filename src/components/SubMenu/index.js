import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import _ from 'lodash';

class SubMenu extends Component {
  render() {
    const { listButton } = this.props;
    return (
      <div className="sub-menu">
        <Row className="row-menu">
          <Col md="3" />
          <Col md="9" xs="12" className="div-item">
            {
              _.map(listButton, item => (
                <button type="button">
                  {item.nameBt}
                </button>
              ))
            }
          </Col>
        </Row>
      </div>
    );
  }
}

SubMenu.propTypes = {
  listButton: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default SubMenu;
