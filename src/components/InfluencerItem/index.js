import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InfluencerItem extends Component {
  render() {
    const { className } = this.props;
    return (
      <div className={`${className} div-influencerItem`}>
        <div className="div-img" />
        <div className="div-span">
          <span>
            Influencer Name
          </span>
        </div>
      </div>
    );
  }
}

InfluencerItem.defaultProps = {
  className: '',
};

InfluencerItem.propTypes = {
  className: PropTypes.string,

};
export default InfluencerItem;
