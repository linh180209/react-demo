import React, { Component, lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import ReactHtmlPare from 'react-html-parser';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import 'react-image-gallery/styles/css/image-gallery.css';
import '../../../styles/testimonials.scss';
import icTop from '../../../image/icon/ic-to-top.svg';
import { generateUrlWeb } from '../../../Redux/Helpers';
import { isBrowser, isMobile } from '../../../DetectScreen';

const LazyImage = lazy(() => import('../../LazyImage'));

class Testimonials extends Component {
  renderItem = data => (
    <div className="div-testimonials-items div-col justify-center items-center">
      <div className="div-text tc">
        {ReactHtmlPare(data.value.description)}
      </div>
      {
        isMobile
          ? (
            <div className="div-image div-col justify-center items-center mt-3">
              <Suspense fallback={<div />}>
                <LazyImage src={data.value.image.image} />
              </Suspense>
            </div>
          ) : (<div />)
      }
      {/* <div className="div-image div-col justify-center items-center mt-3">
        <Suspense fallback={<div />}>
          <LazyImage src={data.value.image.image} />
        </Suspense>
      </div> */}
    </div>
  )

  render() {
    const { heroCms, scrollToTop } = this.props;
    const data = heroCms.value;
    const { button, blogs } = data;
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      // autoplay: true,
      arrows: !isMobile,
      autoplaySpeed: 6000,
      pauseOnHover: false,
    };
    if (isBrowser) {
      _.assign(settings, {
        dotsClass: 'slick-dots slick-thumb dots-custome-landing',
        appendDots: dots => (
          <div
            style={{
              padding: '10px',
            }}
          >
            <ul style={{ margin: '0px' }}>
              {dots}
            </ul>
          </div>
        ),
        customPaging: i => (
          <div>
            <Suspense fallback={<div />}>
              <LazyImage src={blogs[i].value.image.image} />
            </Suspense>
            <div className="line-active" />
          </div>
        ),
      });
    }

    return (
      <div className="div-testimonials">
        <hr />
        <Slider {...settings}>
          {
          _.map(blogs, d => (
            this.renderItem(d)
          ))
        }
        </Slider>
        <div className="div-button">
          <Link to={button ? generateUrlWeb(button.link) : ''}>
            {button ? button.name : ''}
          </Link>
        </div>
        <div className={scrollToTop ? 'div-bt-to-top' : 'hidden'}>
          <button type="button" className="button-bg__none" onClick={this.props.scrollToTop}>
            <img loading="lazy" src={icTop} alt="top" />
          </button>
          <span>back to the top</span>
        </div>
      </div>
    );
  }
}

Testimonials.propTypes = {
  cms: PropTypes.arrayOf().isRequired,
};

export default Testimonials;
