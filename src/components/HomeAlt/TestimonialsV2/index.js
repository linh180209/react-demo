import React, { Component, lazy, Suspense } from 'react';
// import PropTypes from 'prop-types';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import ReactHtmlPare from 'react-html-parser';
import _ from 'lodash';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import 'react-image-gallery/styles/css/image-gallery.css';
import '../../../styles/testimonials-v2.scss';
import icTop from '../../../image/icon/ic-to-top.svg';
import { generateUrlWeb } from '../../../Redux/Helpers';
import { isBrowser, isMobile } from '../../../DetectScreen';
import ButtonCT from '../../ButtonCT';

const LazyImage = lazy(() => import('../../LazyImage'));

class TestimonialsV2 extends Component {
  renderItem = data => (
    <div className="div-testimonials-items div-col justify-center items-center">
      {
        isMobile
          ? (
            <div className="div-image div-col justify-center items-center">
              <Suspense fallback={<div />}>
                <LazyImage src={data.value.image.image} />
              </Suspense>
            </div>
          ) : (<div />)
      }
      <h4 className="header_4 tc">
        {ReactHtmlPare(data.value.description)}
      </h4>
    </div>
  )

  render() {
    const { heroCms, scrollToTop } = this.props;
    const data = heroCms.value;
    const { button, blogs } = data;
    const settings = {
      dots: true,
      arrows: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplaySpeed: 6000,
      pauseOnHover: false,
    };
    if (isBrowser) {
      _.assign(settings, {
        dotsClass: 'slick-dots slick-thumb dots-custome-landing',
        appendDots: dots => (
          <div
            style={{
              padding: '10px',
            }}
          >
            <ul style={{ margin: '0px' }}>
              {dots}
            </ul>
          </div>
        ),
        customPaging: i => (
          <div>
            <Suspense fallback={<div />}>
              <LazyImage src={blogs[i].value.image.image} />
            </Suspense>
            <div className="line-active" />
          </div>
        ),
      });
    }

    return (
      <div className={classnames('div-testimonials-v2', isMobile ? 'ter-mobile' : '')}>
        <hr />
        <Slider {...settings}>
          {
          _.map(blogs, d => (
            this.renderItem(d)
          ))
        }
        </Slider>
        <div className="div-button">
          {/* <Link to={button ? generateUrlWeb(button.link) : ''} className="button-homepage-new-5 button-a animated-hover">
            {button ? button.name : ''}
          </Link> */}
          <ButtonCT
            typeBT="LINK"
            className={classnames('link-text link-text-landingpage', 'animated-hover')}
            name={button?.name}
            href={generateUrlWeb(button?.link)}
          />
        </div>
        <div className={scrollToTop ? 'div-bt-to-top' : 'hidden'}>
          <button type="button" className="button-bg__none" onClick={this.props.scrollToTop}>
            <img loading="lazy" src={icTop} alt="top" />
          </button>
          <span>back to the top</span>
        </div>
      </div>
    );
  }
}

export default TestimonialsV2;
