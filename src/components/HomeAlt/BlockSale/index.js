import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';
import BlockSaleItem from './blockSaleItem';
import '../../../styles/block-sale.scss';

class BlockSale extends Component {
  render() {
    return (
      <div className="div-block-sale">
        <Row>
          <Col md="4" xs="12" className="div-col-item">
            <BlockSaleItem />
          </Col>
          <Col md="4" xs="12" className="div-col-item">
            <BlockSaleItem />
          </Col>
          <Col md="4" xs="12" className="div-col-item">
            <BlockSaleItem />
          </Col>
        </Row>
      </div>
    );
  }
}

export default BlockSale;
