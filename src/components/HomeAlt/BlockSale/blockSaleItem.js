import React, { Component } from 'react';
import icTimeSale from '../../../image/icon/ic-time-sale.svg';

export default class BlockSaleItem extends Component {
  render() {
    return (
      <div className="div-block-sale-item">
        <img loading="lazy" src={icTimeSale} alt="icTime" />
        <div className="div-text">
          <span>
            <b>
              Try for Free
            </b>
          </span>
          <span>
            Try for 48 Hours and return free of charge!
          </span>
        </div>
      </div>
    );
  }
}
