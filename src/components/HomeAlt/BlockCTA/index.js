import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import ReactHtmlParser from 'react-html-parser';
import { Link } from 'react-router-dom';
import '../../../styles/block-cta.scss';
import { generateUrlWeb, getAltImageV2 } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';
import icNextW from '../../../image/icon/ic-next-w.svg';
import icNextB from '../../../image/icon/ic-next-b.svg';
import LazyImage from '../../LazyImage';
import ButtonCT from '../../ButtonCT';

function BlockCTA(props) {
  const isRight = props.data.value.image_background.caption === 'text-black';
  const isRightBt = props.data.value.image_background.caption === 'text-black';
  return (
    <div className={classnames('block-cta', isRight ? 'right' : '')}>
      <div className="div-image">
        <LazyImage src={props.data.value.image_background.image} alt={getAltImageV2(props.data.value.image_background)} />
        {/* <img src={props.data.value.image_background.image} alt={getAltImageV2(props.data.value.image_background)} /> */}
        {/* {
          isMobile && (
            <div className="div-link">
              <Link to={generateUrlWeb(props.data.value.button.link)} className={classnames((isRightBt ? 'text-black' : ''), 'button-a animated-hover')}>
                {props.data.value.button.name}
                <img loading="lazy" src={isRightBt ? icNextB : icNextW} alt="icon" />
              </Link>
            </div>

          )
        } */}
      </div>
      <div className={classnames('text div-col', props.isDisabledText ? 'hidden' : '')}>
        <h2>
          {props.data.value.texts.length > 0 ? props.data.value.texts[0].value : ''}
        </h2>
        <span>
          {ReactHtmlParser(props.data.value.description)}
        </span>
        {
          !isMobile && (
            <ButtonCT
              typeBT="LINK"
              className={classnames(isRight ? '' : 'bg-white', 'animated-hover')}
              name={props.data.value.button.name}
              href={generateUrlWeb(props.data.value.button.link)}
            />
          )
        }
      </div>
    </div>
  );
}

export default BlockCTA;
