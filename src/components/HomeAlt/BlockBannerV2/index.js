import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Slider from 'react-slick';

import ReactHtmlParser from 'react-html-parser';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import '../../../styles/block-banner-v2.scss';
import { generateUrlWeb, getAltImageV2, isRightAlignedText } from '../../../Redux/Helpers';
import { isBrowser, isMobile } from '../../../DetectScreen';
import LazyImage from '../../LazyImage';
import ButtonCT from '../../ButtonCT';
import icLocal from '../../../image/icon/local-yellow.svg';
import icTime from '../../../image/icon/time-yellow.svg';
import icPhone from '../../../image/icon/phone-yellow.svg';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

function BlockBannerV2(props) {
  const [isReadMore, setIsReadMore] = useState(false);
  const onClickButton = useCallback(() => {
    const { item } = props.data.value.item;
    if (!_.isEmpty(item)) {
      const { id, name, price } = item;
      const data = {
        item: id,
        name,
        price,
        quantity: 1,
        is_featured: item.is_featured,
      };
      const dataTemp = {
        idCart: props.basket.id,
        item: data,
      };
      if (!props.basket.id) {
        props.createBasketGuest(dataTemp);
      } else {
        props.addProductBasket(dataTemp);
      }
    }
  }, [props.data]);

  const isLeftOrCenter = props.data.value.image.caption.includes('right');
  const isCenter = props.data.value.image.caption === 'center';
  const isTextCenter = props.data.value.image.caption.includes('center');

  const blockText = (
    <div className={classnames('block-text div-col', isRightAlignedText() ? 'tr' : '')}>
      {
        !isCenter && (
          <h2 className={classnames('header_2', isRightAlignedText() ? 'w-100' : '')}>
            {props.data.value.header.header_text}
          </h2>
        )
      }
      <div className={classnames('header_4', isRightAlignedText() ? 'tr' : '')}>
        {ReactHtmlParser(props.data.value.description)}
      </div>
      {
        props.data.value?.buttons?.length > 0 && (
          _.map(props.data.value?.buttons, d => (
            <div className="info-text div-row">
              <img src={d?.value?.name === 'address' ? icLocal : d?.value?.name === 'phone' ? icPhone : icTime} alt="icon" />
              <h4 className="header_4">{d?.value?.text}</h4>
            </div>
          ))
        )
      }
      {
        props.data.value.button.name && (
        <ButtonCT
          typeBT="LINK"
          className={classnames(isLeftOrCenter && !isTextCenter ? 'button-black' : 'link-text link-text-landingpage', 'animated-hover')}
          name={props.data.value.button.name}
          href={generateUrlWeb(props.data.value.button.link)}
        />
        )
      }
    </div>
  );
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    arrows: isBrowser,
    autoplaySpeed: 5000,
    pauseOnHover: false,
  };

  const blockImage = (
    <div className="block-image div-row">
      {
        props.data.value.images.length > 0 ? (
          <Slider {...settings}>
            {
              _.map(props.data.value.images, d => (
                <div className="item-image">
                  <LazyImage src={d.value.image} alt={getAltImageV2(d.value.image)} />
                </div>
              ))
            }
          </Slider>
        ) : (
          <LazyImage src={props.data.value.image.image} alt={getAltImageV2(props.data.value.image)} />
        )
      }
    </div>
  );

  return (
    <React.Fragment>
      <div className={classnames('block-banner-v2', isTextCenter ? 'text-center' : '', (isLeftOrCenter && !isRightAlignedText() ? '' : 'text-right'), (isCenter && isBrowser ? 'browser-center' : ''))}>
        {
          isCenter && (
            <h2 className="header_2">
              {props.data.value.header.header_text}
            </h2>
          )
        }
        {
          isLeftOrCenter && !isMobile ? (
            <React.Fragment>
              {blockText}
              {blockImage}
            </React.Fragment>
          ) : (
            <React.Fragment>
              {blockImage}
              {blockText}
            </React.Fragment>
          )
        }
      </div>
      {
        props.isOurTeamReadMore && props.data.value.events.length > 0 && isReadMore && (
          <div className="more-text-info animated faster fadeIn">
            {ReactHtmlParser(props.data.value.events[0].value.text)}
            <button className="button-bg__none button-homepage-new-4 animated-hover button-a" type="button" onClick={() => setIsReadMore(false)}>
              {props.data.value.events[0].value.header.header_text}
            </button>
          </div>
        )
      }
    </React.Fragment>

  );
}

export default BlockBannerV2;
