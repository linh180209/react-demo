/* eslint-disable jsx-a11y/media-has-caption */
import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { isEdge } from 'react-device-detect';
import { Link } from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';
import BackGroundVideo from '../../backgroundVideo';
import { generateUrlWeb, onClickLink } from '../../../Redux/Helpers';
import { isMobile, isTablet } from '../../../DetectScreen';

import '../../../styles/block-video.scss';

class BlockVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      heroCms: undefined,
      videoBlock: undefined,
    };
  }

  componentDidMount() {
    const { heroCms } = this.props;
    const { videos } = heroCms.value;
    const valueFindVideo = isMobile ? 'phone' : (isTablet ? 'tablet' : 'web');
    const dataVideo = _.find(videos, x => x.value.type === valueFindVideo);
    const videoBlock = dataVideo ? dataVideo.value : undefined;
    this.setState({ heroCms: heroCms.value, videoBlock });
  }

  onClick = (link) => {
    const { login, history } = this.props;
    onClickLink(link, login, history);
  };

  render() {
    const { heroCms, videoBlock } = this.state;
    const { isPositionRight } = this.props;
    if (_.isEmpty(heroCms)) {
      return null;
    }
    const blockText = (
      <div
        className="div-text div-col justify-center items-center w-50 h-100"
        style={{ backgroundColor: `${heroCms.background_color}`, zIndex: '2' }}
      >
        <div className="w-80" style={{ color: '#ffffff' }}>
          <h1>
            {heroCms.header ? heroCms.header.header_text : ''}
          </h1>
          <h1>
            {heroCms.header2 ? heroCms.header2.header_text : ''}
          </h1>
          <div className="mt-5 mb-5">
            {ReactHtmlParser(heroCms.description)}
          </div>
          <div>
            <Link
              to={heroCms.button ? generateUrlWeb(heroCms.button.link) : ''}
              className="button-homepage-new-3 color-white"
            >
              {heroCms.button ? heroCms.button.text : ''}
            </Link>
          </div>
        </div>
      </div>
    );

    const blockMedia = (
      <div className="div-col div-video justify-center items-center w-50 h-100" style={{ zIndex: '1' }}>
        {
          videoBlock && videoBlock.video
            ? (
              <video
                ref={this.refVideo}
                id="myVideo"
                style={{
                  objectFit: 'cover',
                  width: isEdge ? 'auto' : '100%',
                  height: '100%',
                }}
                muted="true"
                playsinline="true"
                autoPlay="true"
                loop="true"
                preload="auto"
                src={videoBlock ? videoBlock.video : ''}
                poster={videoBlock ? videoBlock.placeholder : ''}
              />
            ) : (
              <BackGroundVideo urlImage={heroCms && heroCms.image ? heroCms.image.image : ''} isVideo={false} />
            )
        }

      </div>
    );
    const htmlWeb = (
      <div className="div-row block-video">
        {
          isPositionRight ? (
            <React.Fragment>
              {blockText}
              {blockMedia}
            </React.Fragment>

          ) : (
            <React.Fragment>
              {blockMedia}
              {blockText}
            </React.Fragment>
          )
        }

      </div>
    );
    const htmlMobile = (
      <div className="div-Col block-video">
        <div className="div-video div-col justify-center items-center w-100">
          {
            videoBlock && videoBlock.video
              ? (
                <video
                  ref={this.refVideo}
                  id="myVideo"
                  style={{
                    objectFit: 'cover',
                    width: '100%',
                    height: '100%',
                  }}
                  muted="true"
                  playsinline="true"
                  autoPlay="true"
                  loop="true"
                  preload="auto"
                  src={videoBlock ? videoBlock.video : ''}
                  poster={videoBlock ? videoBlock.placeholder : ''}
                />
              ) : (
                <BackGroundVideo urlImage={heroCms && heroCms.image ? heroCms.image.image : ''} isVideo={false} />
              )
          }
        </div>
        <div
          className="div-text div-col justify-center items-center w-100"
          style={{ backgroundColor: `${heroCms.background_color}` }}
        >
          <div className="w-80 div-col justify-center items-center" style={{ color: '#ffffff' }}>
            <h1>
              {heroCms.header ? heroCms.header.header_text : ''}
            </h1>
            <h1>
              {heroCms.header2 ? heroCms.header2.header_text : ''}
            </h1>
            <div className="mt-3 mb-3 tc">
              {ReactHtmlParser(heroCms.description)}
            </div>
            <Link
              className="button-homepage-new-3 color-white"
              to={heroCms.button ? generateUrlWeb(heroCms.button.link) : ''}
              type="none"
            >
              {heroCms.button ? heroCms.button.text : ''}
            </Link>
          </div>
        </div>
      </div>
    );

    return (
      <div>
        {
          isMobile ? (htmlMobile) : (htmlWeb)
        }
      </div>
    );
  }
}

BlockVideo.propTypes = {
  heroCms: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  isPositionRight: PropTypes.bool.isRequired,
};

export default BlockVideo;
