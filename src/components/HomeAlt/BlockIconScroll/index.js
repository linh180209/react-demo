import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import '../../../styles/block-icon-scroll.scss';
import { getAltImageV2, getNameFromButtonBlock } from '../../../Redux/Helpers';
import { isBrowser, isMobile } from '../../../DetectScreen';

function BlockIconScroll(props) {
  const itemRender = (d, index) => (
    <div className="icon-scroll-item">
      <img loading="lazy" src={d.value.image.image} alt={getAltImageV2(d.value.image)} />
      <div className="text div-col">
        <h3 className="header_3">
          {`${index + 1}. `}
          {d.value.image.caption}
        </h3>
        <h4 className="header_4">
          {d.value.text}
        </h4>
      </div>
    </div>
  );
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows: false,
    autoplaySpeed: 5000,
    pauseOnHover: false,
  };
  const howitwordBt = getNameFromButtonBlock(props.buttonBlocks, 'How it Works');
  return (
    <div className="icon-scroll">
      <h2 className="header_2">{howitwordBt}</h2>
      {
        isBrowser ? (
          <div className="list-item">
            {
              _.map(props.data ? props.data.value.icons : [], (d, index) => (
                itemRender(d, index)
              ))
            }
          </div>
        ) : (
          <Slider {...settings}>
            {
              _.map(props.data ? props.data.value.icons : [], (d, index) => (
                itemRender(d, index)
              ))
            }
          </Slider>
        )
      }
    </div>
  );
}

export default BlockIconScroll;
