import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import _ from 'lodash';
import BlockProductItem from './blockProductItem';
import { isBrowser } from '../../../DetectScreen';
import '../../../styles/div-block-product.scss';

class BlockProduct extends Component {
  constructor(props) {
    super(props);
    const { heroCms } = props;
    this.state = {
      heroCms: heroCms.value,
    };
  }

  render() {
    const { heroCms } = this.state;
    const {
      history, login, basket, createBasketGuest, addProductBasket, cmsCommon,
    } = this.props;
    const {
      image_background: imgBg, products,
    } = heroCms;

    return (
      <div
        className="div-block-product"
        style={{
          backgroundImage: `url(${imgBg ? imgBg.image : ''})`,
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
        }}
      >
        <Row>
          {
            _.map(products || [], d => (
              <Col md={isBrowser ? '6' : '12'} xs="12">
                <BlockProductItem
                  data={d.value}
                  history={history}
                  login={login}
                  basket={basket}
                  createBasketGuest={createBasketGuest}
                  addProductBasket={addProductBasket}
                  cmsCommon={cmsCommon}
                />
              </Col>
            ))
          }
        </Row>
      </div>
    );
  }
}

BlockProduct.propTypes = {
  heroCms: PropTypes.shape(PropTypes.object).isRequired,
  cmsCommon: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  basket: PropTypes.shape(PropTypes.object).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
};

export default BlockProduct;
