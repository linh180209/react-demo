import React, { Component, lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';
import {
  getAltImage, onClickLink, getUrlGotoProductLandingPage, generateUrlWeb,
} from '../../../Redux/Helpers';

const LazyImage = lazy(() => import('../../LazyImage'));

class BlockProductItem extends Component {
  onClick = (link) => {
    const { login, history } = this.props;
    onClickLink(link, login, history);
  };

  gotoProduct = () => {
    const { product } = this.props.data;
    const url = getUrlGotoProductLandingPage(product.product);
    if (url) {
      this.props.history.push(generateUrlWeb(url));
    }
  }

  addToCart = () => {
    const { basket } = this.props;
    const { product } = this.props.data;
    const dataProduct = product.product;
    const item = dataProduct && dataProduct.items.length > 0 ? dataProduct.items[0] : undefined;
    if (item) {
      const {
        price, id,
      } = item;
      const data = {
        item: id,
        is_featured: item.is_featured,
        name: dataProduct ? dataProduct.name : '',
        price,
        quantity: 1,
        total: parseFloat(price) * 1,
        image_urls: dataProduct.images,
      };
      const dataTemp = {
        idCart: basket.id,
        item: data,
      };
      if (!basket.id) {
        this.props.createBasketGuest(dataTemp);
      } else {
        this.props.addProductBasket(dataTemp);
      }
    }
  }

  render() {
    const {
      button, header, text, image,
    } = this.props.data;
    const { cmsCommon } = this.props;
    const buttonAddToCart = cmsCommon ? _.find(cmsCommon.body, x => x.value.name === 'ADD_TO_CART') : undefined;

    return (
      <div className="block-product-item div-col justify-center items-center">
        <h2 className="mt-5 mb-3 tc">
          {header ? header.header_text : ''}
        </h2>
        {/* <img
          src={image ? image.image : ''}
          alt={getAltImage(image ? image.image : '')}
          onClick={this.gotoProduct}
        /> */}
        <Suspense fallback={<div />}>
          <LazyImage src={image ? image.image : ''} onClick={this.gotoProduct} className="cursor-pointer" />
        </Suspense>
        <div className="mt-3 mb-3 tc" style={{ color: '#0D0D0D' }}>
          {ReactHtmlParser(text)}
        </div>
        <button
          type="button"
          className="button-homepage-new mb-3"
          onClick={this.addToCart}
        >
          {buttonAddToCart ? buttonAddToCart.value.text : ''}
        </button>
        <button
          type="button"
          className="button-homepage-new-3 mb-5"
          onClick={() => this.onClick(button ? button.link : '')}
        >
          {button ? button.text : ''}
        </button>
      </div>
    );
  }
}

BlockProductItem.propTypes = {
  data: PropTypes.shape(PropTypes.object).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  basket: PropTypes.shape(PropTypes.object).isRequired,
  cmsCommon: PropTypes.shape(PropTypes.object).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default BlockProductItem;
