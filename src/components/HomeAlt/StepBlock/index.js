import React, { useState, useEffect } from 'react';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import '../../../styles/step-block-home-alt.scss';
import ButtonCT from '../../ButtonCT';
import PopupAppointment from '../../PopupAppointment';

function StepBlockHomeAlt(props) {
  const [isOpen, setIsOpen] = useState(false);

  const onChange = (value, name) => {
    console.log('onChange', value, name);
  };

  const { buttons, header, image_background: imageBackground } = props.data?.value;
  const descriptionBt = getNameFromButtonBlock(buttons, 'description');
  const bookBt = getNameFromButtonBlock(buttons, 'book a session');

  const modalPopup = (
    <PopupAppointment
      isOpen={isOpen}
      buttonBlocks={buttons}
      imageBackground={imageBackground}
      onClose={() => setIsOpen(false)}
    />
  );
  return (
    <div className="step-block-home-alt">
      {modalPopup}
      <div className="text-header">
        <h2 className="header_2">
          {header?.header_text}
        </h2>
        <h4 className="header_4">
          {descriptionBt}
        </h4>
      </div>
      <ButtonCT
        // typeBT="LINK"
        className="button-custom button-a link-text link-text-landingpage animated-hover"
        name={bookBt}
        onClick={() => setIsOpen(true)}
      />
    </div>
  );
}

export default StepBlockHomeAlt;
