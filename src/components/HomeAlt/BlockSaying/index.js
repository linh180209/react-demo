import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ScrollItem from './scrollItem';
import '../../../styles/block-saying.scss';

class BlockSaying extends Component {
  render() {
    const { heroCms, history, login } = this.props;
    const data = heroCms ? heroCms.value : {};
    const { header, feedbacks, text } = data;
    return (
      <div className="div-block-saying title-global">
        <h1>
          {header ? header.header_text : ''}
        </h1>
        {/* <span>
          {text}
        </span> */}
        <div className="div-scroll">
          {
            _.map(feedbacks || [], (d, index) => (
              <ScrollItem data={d} isEnd={index === 9} history={history} login={login} />
            ))
          }
        </div>
      </div>
    );
  }
}

BlockSaying.propTypes = {
  heroCms: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
};

export default BlockSaying;
