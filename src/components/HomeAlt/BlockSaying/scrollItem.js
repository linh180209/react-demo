/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component, lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import { getAltImage, onClickLink } from '../../../Redux/Helpers';

const LazyImage = lazy(() => import('../../LazyImage'));

class ScrollItem extends Component {
  onClick = (link) => {
    const { login, history } = this.props;
    onClickLink(link, login, history);
  };

  render() {
    const { isEnd, data } = this.props;
    const {
      avatar, link, text, name,
    } = data.value;
    return (
      <div className="div-scroll-item">
        <div>
          {/* <Suspense fallback={<div />}>
            <LazyImage src={avatar.image} onClick={() => this.onClick(link)} className="cursor-pointer" />
          </Suspense> */}
          <LazyImage src={avatar.image} alt={getAltImage(avatar.image)} onClick={() => this.onClick(link)} className="cursor-pointer" />
          {/* <img src={avatar.image} alt={getAltImage(avatar.image)} onClick={() => this.onClick(link)} className="cursor-pointer" /> */}
          <span className="text-name">
            {name}
          </span>
        </div>
        <span className={isEnd ? 'end' : ''}>
          {text}
        </span>
      </div>
    );
  }
}

ScrollItem.propTypes = {
  isEnd: PropTypes.bool.isRequired,
  data: PropTypes.shape().isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
};

export default ScrollItem;
