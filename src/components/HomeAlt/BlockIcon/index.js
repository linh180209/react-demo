import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Col, Row } from 'reactstrap';
import _ from 'lodash';
import '../../../styles/block_icon.scss';
import { getAltImageV2 } from '../../../Redux/Helpers';

class BlockIcon extends Component {
  render() {
    const { heroCms } = this.props;
    if (_.isEmpty(heroCms)) {
      return null;
    }
    const data = heroCms.value;
    const { image_background: imageBlock, opacity, icons } = data;
    return (
      <div className="div-block-icon" style={{ backgroundImage: `linear-gradient(to bottom, rgba(21, 36, 54, 0.5), rgba(21, 36, 54, ${opacity})), url(${imageBlock ? imageBlock.image : ''})` }}>
        <Row className="w-100">
          {_.map(icons, x => (
            <Col sm="12" md="12" lg="3" xl="3" className="div-block-icon-item">
              <img loading="lazy" src={x.value ? x.value.image.image : ''} alt={getAltImageV2(x.value ? x.value.image : undefined)} />
              <span>
                {x.value ? x.value.text : ''}
              </span>
            </Col>
          ))}
        </Row>
      </div>
    );
  }
}

BlockIcon.propTypes = {
  heroCms: PropTypes.shape(PropTypes.object).isRequired,
};

export default BlockIcon;
