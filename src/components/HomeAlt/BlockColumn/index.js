import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import '../../../styles/block-column.scss';
import { generateUrlWeb } from '../../../Redux/Helpers';

function BlockColumn(props) {
  return (
    <div
      className={classnames('block-column div-col', (props.data.value.header.header_size ? 'big-size' : ''))}
      style={{ backgroundImage: `url(${props.data.value.images[0].value.image})` }}
    >
      <h2>
        {props.data.value.header.header_text}
      </h2>
      {
        props.data.value.button.name && (
          <Link to={generateUrlWeb(props.data.value.button.link)} className={classnames('button-homepage-new-6 text-black', 'button-a animated-hover')}>
            {props.data.value.button.name}
          </Link>
        )
      }
    </div>
  );
}
export default BlockColumn;
