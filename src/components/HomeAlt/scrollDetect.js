/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';

class ScrollDetect extends Component {
  isBottom = el => el.getBoundingClientRect().bottom <= window.innerHeight

  componentDidMount() {
    document.addEventListener('scroll', this.trackScrolling);
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.trackScrolling);
  }

  trackScrolling = () => {
    const wrappedElement = document.getElementById(this.props.id);
    if (this.isBottom(wrappedElement)) {
      this.props.detectScrollTo();
      document.removeEventListener('scroll', this.trackScrolling);
    }
  };

  render() {
    const { id } = this.props;
    return (
      <div id={id} />
    );
  }
}

ScrollDetect.propTypes = {
  id: PropTypes.string.isRequired,
  detectScrollTo: PropTypes.func.isRequired,
};

export default ScrollDetect;
