import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import '../../../styles/block-icon-line.scss';
import { getAltImageV2 } from '../../../Redux/Helpers';

function BlockIconLine(props) {
  const { icons } = props.data.value;
  return (
    <div className="block-icon-line">
      {
        _.map(icons || [], d => (
          <div className="items div-row">
            <img loading="lazy" src={d.value.image.image} alt={getAltImageV2(d.value.image)} />
            <span>
              {d.value.text}
            </span>
          </div>
        ))
      }
    </div>
  );
}

export default BlockIconLine;
