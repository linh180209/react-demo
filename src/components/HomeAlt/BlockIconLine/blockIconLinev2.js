import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import '../../../styles/block-icon-line-v2.scss';
import { getAltImageV2 } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';

function BlockIconLineV2(props) {
  const { icons } = props?.data?.value || {};
  const settings = {
    dots: false,
    arrows: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
    pauseOnHover: false,
  };

  const renderItem = d => (
    <div className="items div-row">
      <img loading="lazy" src={d.value.image.image} alt={getAltImageV2(d.value.image)} />
      <h3 className="header_3">
        {d.value.text}
      </h3>
    </div>
  );

  return (
    <div className="block-icon-line-v2">
      {
        isMobile ? (
          <Slider {...settings}>
            {
              _.map(icons || [], d => (
                renderItem(d)
              ))
            }
          </Slider>
        ) : (
          _.map(icons || [], d => (
            renderItem(d)
          ))
        )
      }
    </div>
  );
}

export default BlockIconLineV2;
