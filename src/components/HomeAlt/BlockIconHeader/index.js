import React, { Component } from 'react';
import _ from 'lodash';
import reactHtmlPare from 'react-html-parser';
import '../../../styles/block-icon-header.scss';
import { getAltImageV2 } from '../../../Redux/Helpers';

class BlockIconHeader extends Component {
  render() {
    const { heroCms, isHidenHeader } = this.props;
    if (_.isEmpty(heroCms)) {
      return null;
    }
    const {
      header, benefits,
    } = heroCms.value;
    return (
      <div className="div-block-icon-header">
        <h3 className={isHidenHeader ? 'hidden' : ''}>
          {header ? header.header_text : ''}
        </h3>
        <div className="div-list-icon">
          {
            _.map(benefits, x => (
              <div className="div-item">
                <img loading="lazy" src={x.value.image ? x.value.image.image : ''} alt={getAltImageV2(x.value.image)} />
                <h4>
                  {x.value.header.header_text}
                </h4>
                <span>
                  {reactHtmlPare(x.value.text)}
                </span>
              </div>
            ))
          }
        </div>
      </div>
    );
  }
}

export default BlockIconHeader;
