import React, { Component, PureComponent } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import ReactHtmlParser from 'react-html-parser';
import _ from 'lodash';
import CTABlockItem from './ctaBlockItem';
import { isBrowser } from '../../../DetectScreen';
import '../../../styles/block-cta-big.scss';

class CTABlockBig extends PureComponent {
  constructor(props) {
    super(props);
    const { heroCms } = props;
    this.state = {
      heroCms: heroCms.value,
    };
  }

  render() {
    const { heroCms } = this.state;
    const { history, login, isLanding } = this.props;
    const {
      header, image_background: imgBg, app, text,
    } = heroCms;
    return (
      <div
        className="cta-block-big div-col justify-center items-center mt-5"
        style={{
          backgroundImage: `url(${imgBg ? imgBg.image : ''})`,
          backgroundSize: 'cover',
          backgroundRepeat: 'no-repeat',
          position: 'relative',
        }}
      >
        <div id="id-creation" className="div-point_scroll" />
        <h1>
          {header ? header.header_text : ''}
        </h1>
        <div className="description">
          {ReactHtmlParser(text)}
        </div>
        <Row className="class-row div-list mt-5">
          {
            _.map(app || [], d => (
              <Col md={isBrowser ? '4' : '12'} xs="12">
                <CTABlockItem data={d.value} history={history} login={login} isLanding={isLanding} />
              </Col>
            ))
          }

        </Row>
      </div>
    );
  }
}

CTABlockBig.propTypes = {
  heroCms: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  isLanding: PropTypes.bool.isRequired,
};

export default CTABlockBig;
