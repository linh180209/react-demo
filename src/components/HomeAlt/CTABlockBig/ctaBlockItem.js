import React, { Component, lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import ReactHtmlParser from 'react-html-parser';
import { getAltImage, onClickLink } from '../../../Redux/Helpers';

const LazyImage = lazy(() => import('../../LazyImage'));

class CTABlockItem extends Component {
  onClick = (link) => {
    const { login, history } = this.props;
    onClickLink(link, login, history);
  };

  render() {
    const {
      button, header, image, text,
    } = this.props.data;
    const { isLanding } = this.props;
    return (
      <div className="cta-block-item div-col justify-center items-center">
        <div className="div-img div-col justify-center items-center">
          {/* <img
            src={image ? image.image : ''}
            alt={getAltImage(image ? image.image : '')}
            onClick={() => this.onClick(button ? button.link : '')}
          /> */}
          <Suspense fallback={<div />}>
            <LazyImage src={image ? image.image : ''} onClick={() => this.onClick(button ? button.link : '')} className="cursor-pointer" />
          </Suspense>
        </div>
        <div className="mt-3 mb-3 tc" style={{ color: '#0D0D0D' }}>
          {ReactHtmlParser(text)}
        </div>
        <button
          type="button"
          className={`${isLanding ? 'hidden' : 'button-homepage-new mb-5'}`}
          onClick={() => this.onClick(button ? button.link : '')}
        >
          {button ? button.text : ''}
        </button>
      </div>
    );
  }
}

CTABlockItem.propTypes = {
  data: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  isLanding: PropTypes.bool.isRequired,
};

export default CTABlockItem;
