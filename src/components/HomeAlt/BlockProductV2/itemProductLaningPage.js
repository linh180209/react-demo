import React, { Component } from 'react';
import _ from 'lodash';
import icChecked from '../../../image/icon/check-yellow.svg';
import { generaCurrency, getNameFromButtonBlock } from '../../../Redux/Helpers';
import LazyImage from '../../LazyImage';
import { isBrowser, isMobile } from '../../../DetectScreen';

class ItemProductLaningPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isHover: false,
    };
  }

  onMouseEnter = () => {
    this.setState({ isHover: true });
  }

  onMouseLeave = () => {
    this.setState({ isHover: false });
  }

  onClickAddTocart = () => {
    const { basket, product } = this.props;
    const {
      id, name, price, is_featured: isFeatured,
    } = product.item;
    const data = {
      item: id,
      price,
      quantity: 1,
      name,
      is_featured: isFeatured,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    if (!basket.id) {
      this.props.createBasketGuest(dataTemp);
    } else {
      this.props.addProductBasket(dataTemp);
    }
  }

  render() {
    const { isHover } = this.state;
    const {
      product, basket, buttonBlocks, isViewProduct,
    } = this.props;
    const {
      description, item: productItem, name,
    } = product;
    const { items } = basket || { items: [] };
    const isAddedCart = _.find(items, x => x.item.id === productItem.id);
    const addToCartBt = getNameFromButtonBlock(buttonBlocks, 'add_to_cart');
    const viewProductBt = getNameFromButtonBlock(buttonBlocks, 'View product');
    return (
      <div
        className="div-product-home-item"
        onMouseEnter={this.onMouseEnter}
        onMouseLeave={this.onMouseLeave}
      >
        <div className="div-image">
          <LazyImage src={productItem.image} alt="image" onClick={() => this.props.onClickGotoProduct(productItem)} />
          {/* <img src={productItem.image} alt="image" onClick={() => this.props.onClickGotoProduct(productItem)} /> */}
        </div>
        <h3 className="header_3">
          {name}
        </h3>
        {/* <button
          type="button"
          onClick={isAddedCart ? undefined : this.onClickAddTocart}
          className={(isAddedCart || isHover) && isBrowser ? 'add-cart' : 'hidden'}
        >
          <img loading="lazy" src={icChecked} alt="checked" className={isAddedCart ? '' : 'hidden'} />
          {`${isAddedCart ? '' : '+'} ${addToCartBt}`}
        </button> */}
        <h4 className="header_4">
          {description}
        </h4>
        <h4 className="header_4 price">
          <span>
            {generaCurrency(productItem.price_sale || productItem.price)}
          </span>
          {
            isMobile && productItem.price_sale && (
              <span>
                {generaCurrency(productItem.price_sale)}
              </span>
            )
          }
        </h4>
        {
          isViewProduct ? (
            <button
              onClick={() => this.props.onClickGotoProduct(productItem)}
              type="button"
              className="add-cart"
            >
              {viewProductBt}
            </button>
          ) : (
            <button
              onClick={isAddedCart ? undefined : this.onClickAddTocart}
              type="button"
              className="add-cart"
            >
              <img loading="lazy" src={icChecked} alt="checked" className={isAddedCart ? '' : 'hidden'} />
              {`${isAddedCart ? '' : '+'} ${addToCartBt}`}
            </button>
          )
        }
      </div>
    );
  }
}

export default ItemProductLaningPage;
