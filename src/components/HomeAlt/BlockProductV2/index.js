import React, { useState, useEffect } from 'react';
import { Col, Row } from 'reactstrap';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import '../../../styles/block-product-v2.scss';
import ItemProductLaningPage from './itemProductLaningPage';
import { generateUrlWeb, onClickProduct } from '../../../Redux/Helpers';
import ButtonCT from '../../ButtonCT';

function BlockProductV2(props) {
  const onClickGotoProduct = (data) => {
    const url = onClickProduct(data);
    if (url) {
      props.history.push(generateUrlWeb(url));
    }
  };
  console.log('props.data.value', props.data.value);
  return (
    <div className="block-product-v2 div-col">
      <div className="header div-col">
        <h2 className="header_2">
          {props.data.value.header.header_text}
        </h2>
        {
          props.data.value?.text && (
            <h3 className="header_3">
              {props.data.value?.text}
            </h3>
          )
        }
      </div>
      <div className="list-product">
        <Row>
          {
            _.map(props.data.value.items, x => (
              <Col md="3" xs="6" className="col-item-product">
                <ItemProductLaningPage
                  product={x.value}
                  basket={props.basket}
                  addProductBasket={props.addProductBasket}
                  createBasketGuest={props.createBasketGuest}
                  loadingPage={props.loadingPage}
                  onClickGotoProduct={onClickGotoProduct}
                  buttonBlocks={props.buttonBlocks}
                  isViewProduct={props.isViewProduct}
                />
              </Col>
            ))
          }
        </Row>
      </div>
      <ButtonCT
        typeBT="LINK"
        className={classnames('link-text link-text-landingpage', 'animated-hover')}
        name={props.data.value.button.name}
        href={generateUrlWeb(props.data.value.button.link)}
      />
    </div>
  );
}

export default BlockProductV2;
