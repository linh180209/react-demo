import React, { Component } from 'react';
import _ from 'lodash';
import {
  Player, ControlBar, Shortcut, BigPlayButton, LoadingSpinner,
} from 'video-react';
import '../../../styles/block-quiz-start.scss';
import { generateUrlWeb } from '../../../Redux/Helpers';
import { isMobileOnly, isTablet } from '../../../DetectScreen';

class BlockQuizStart extends Component {
  render() {
    const { heroCms } = this.props;
    const { videos } = heroCms.value;
    const typeVideo = isMobileOnly ? 'phone' : isTablet ? 'tablet' : 'web';
    const video = _.find(videos, x => x.value.type === typeVideo);
    return (
      <div className="div-block-quiz-start">
        <h1>
          {heroCms ? heroCms.value.image.caption : ''}
        </h1>
        <div className="div-image">
          <Player
            playsInline
            loop
            muted
            autobuffer
            disablePauseOnClick
            autoPlay
            src={video?.value?.video}
            fluid={false}
            height="100%"
            width="100%"
          >
            <ControlBar disableCompletely disableDefaultControls />
            <LoadingSpinner />
            <Shortcut dblclickable clickable={false} />
            <BigPlayButton position="center" />
          </Player>
          {/* <img src={heroCms ? heroCms.value.image.image : ''} alt="gif" /> */}
        </div>
        <button type="button" onClick={() => this.props.history.push(generateUrlWeb(heroCms ? heroCms.value.link : ''))}>
          {heroCms ? heroCms.value.text : ''}
        </button>
      </div>
    );
  }
}

export default BlockQuizStart;
