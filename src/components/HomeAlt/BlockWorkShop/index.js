/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/alt-text */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import '../../../styles/block-workshop.scss';
import icNext from '../../../image/icon/ic-next-whiteblue.svg';
import { generateUrlWeb, getAltImageV2 } from '../../../Redux/Helpers';
import LazyImage from '../../LazyImage';
import { isMobile, isMobileOnly } from '../../../DetectScreen';
// import LazyImage from '../../LazyImage';
// const LazyImage = lazy(() => import('../../LazyImage'));
class BlockWorkShop extends Component {
  render() {
    const { heroCms, isHidenHeader } = this.props;
    const data = heroCms ? heroCms.value : {};
    const { header, button, ateliers } = data;
    const dataShow = isMobile ? ateliers.slice(0, 4) : ateliers;
    return (
      <div className="div-block-workshop">
        <h1 className={isHidenHeader ? 'hidden' : 'title-global'}>
          {header ? header.header_text : ''}
        </h1>
        <div className={`workshop__list ${!isMobileOnly ? 'px-5' : ''}`}>
          <div className="row">
            {_.map(dataShow, item => (
              <div className={`col ${isMobileOnly ? 'mobi--col' : ''}`}>
                <div className="workshop__list__item" onClick={() => { this.props.history.push(generateUrlWeb(item.value ? item.value.buttons[0].value.link : '')); }}>
                  <div className="__image">
                    {/* <Suspense fallback={<div />}> */}
                    {/* <LazyImage src={item.value.images[0].value.image} className="img-fluid" /> */}
                    {/* </Suspense> */}
                    <LazyImage src={item.value.images[0].value.image} className="img-fluid" alt={getAltImageV2(item.value.images[0].value)} />
                    {/* <img src={item.value.images[0].value.image} className="img-fluid" alt={getAltImageV2(item.value.images[0].value)} /> */}
                  </div>
                  <div className="__title mt-1">
                    {item.value.header.header_text}
                  </div>
                  <div className="__price mt-2">
                    {item.value.short_price_text}
                  </div>
                  <div className="__description mt-2">
                    {item.value.short_text}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
        <div className={isHidenHeader ? 'hidden' : 'div-button'}>
          <Link to={button ? generateUrlWeb(button.link) : ''}>
            {button ? button.name : ''}
            <img loading="lazy" src={icNext} alt="icNext" className={isMobile ? 'hidden' : ''} />
          </Link>
        </div>
      </div>
    );
  }
}

BlockWorkShop.propTypes = {
  heroCms: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default BlockWorkShop;
