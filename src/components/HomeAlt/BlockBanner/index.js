import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Slider from 'react-slick';
import ReactHtmlParser from 'react-html-parser';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import '../../../styles/block-banner.scss';
import { generateUrlWeb, getAltImageV2 } from '../../../Redux/Helpers';
import { isBrowser, isMobile } from '../../../DetectScreen';
import LazyImage from '../../LazyImage';
import ButtonCT from '../../ButtonCT';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

function BlockBanner(props) {
  const [isReadMore, setIsReadMore] = useState(false);
  const onClickButton = useCallback(() => {
    const { item } = props.data.value.item;
    if (!_.isEmpty(item)) {
      const { id, name, price } = item;
      const data = {
        item: id,
        is_featured: item.is_featured,
        name,
        price,
        quantity: 1,
      };
      const dataTemp = {
        idCart: props.basket.id,
        item: data,
      };
      if (!props.basket.id) {
        props.createBasketGuest(dataTemp);
      } else {
        props.addProductBasket(dataTemp);
      }
    }
  }, [props.data]);

  const isLeftOrCenter = props.data.value.image.caption === 'right';
  const isCenter = props.data.value.image.caption === 'center';

  const blockText = (
    <div className="block-text div-col">
      {
        !isCenter && (
          <h2>
            {props.data.value.header.header_text}
          </h2>
        )
      }
      <div className="description">
        <p>
          {ReactHtmlParser(props.data.value.description)}
        </p>
      </div>
      {
        props.data.value.icons.length > 0 && (
          <div className="signature">
            <img loading="lazy" src={props.data.value.icons[0].value.image.image} alt={getAltImageV2(props.data.value.icons[0].value.image)} />
            <h3>
              {props.data.value.icons[0].value.header.header_text}
            </h3>
            <span>
              {props.data.value.icons[0].value.text}
            </span>
          </div>
        )
      }
      {
        props.isOurTeamReadMore && props.data.value.events.length > 0 ? (
          !isReadMore && (
            <button className="button-bg__none button-homepage-new-4 animated-hover button-a" type="button" onClick={() => setIsReadMore(true)}>
              {props.data.value.button.name}
            </button>
          )
        ) : (
          // <Link
          //   to={generateUrlWeb(props.data.value.button.link)}
          //   className={classnames((isLeftOrCenter ? 'button-homepage-new-4' : 'button-homepage-new-5'), 'button-a animated-hover', (props.data.value.button.name ? '' : 'hidden'))}
          // >
          //   {props.data.value.button.name}
          // </Link>
          props.data.value.button.name && (
          <ButtonCT
            typeBT="LINK"
            className={classnames(isLeftOrCenter ? 'link-text' : 'outline-bt', 'animated-hover')}
            name={props.data.value.button.name}
            href={generateUrlWeb(props.data.value.button.link)}
          />
          )
        )
      }
    </div>
  );
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    arrows: isBrowser,
    autoplaySpeed: 5000,
    pauseOnHover: false,
  };

  const blockImage = (
    <div className="block-image div-row">
      {
        props.data.value.images.length > 0 ? (
          <Slider {...settings}>
            {
              _.map(props.data.value.images, d => (
                <div className="item-image">
                  <LazyImage src={d.value.image} alt={getAltImageV2(d.value.image)} />
                </div>
              ))
            }
          </Slider>
        ) : (
          <LazyImage src={props.data.value.image.image} alt={getAltImageV2(props.data.value.image)} />
        )
      }
    </div>
  );

  return (
    <React.Fragment>
      <div className={classnames('block-banner', (isLeftOrCenter ? '' : 'text-right'), (isCenter && isBrowser ? 'browser-center' : ''))}>
        {
          isCenter && (
            <h2>
              {props.data.value.header.header_text}
            </h2>
          )
        }
        {
          isLeftOrCenter && !isMobile ? (
            <React.Fragment>
              {blockText}
              {blockImage}
            </React.Fragment>
          ) : (
            <React.Fragment>
              {blockImage}
              {blockText}
            </React.Fragment>
          )
        }
      </div>
      {
        props.isOurTeamReadMore && props.data.value.events.length > 0 && isReadMore && (
          <div className="more-text-info animated faster fadeIn">
            {ReactHtmlParser(props.data.value.events[0].value.text)}
            <button className="button-bg__none button-homepage-new-4 animated-hover button-a" type="button" onClick={() => setIsReadMore(false)}>
              {props.data.value.events[0].value.header.header_text}
            </button>
          </div>
        )
      }
    </React.Fragment>

  );
}

export default BlockBanner;
