/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component, lazy, Suspense } from 'react';
// import PropTypes from 'prop-types';
import Slider from 'react-slick';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';
import { isEdge } from 'react-device-detect';
import smoothscroll from 'smoothscroll-polyfill';
import 'react-image-gallery/styles/css/image-gallery.css';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import '../../../styles/block-carousel-v2.scss';
import icScroll from '../../../image/icon/ic-scroll-landing-white.svg';
import {
  getAltImage, onClickLink, generaCurrency, getAltImageV2, generateUrlWeb, isRightAlignedText,
} from '../../../Redux/Helpers';
import { isBrowser, isTablet } from '../../../DetectScreen';
import ButtonCT from '../../ButtonCT';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const LazyImage = lazy(() => import('../../LazyImage'));

class BlockCarouselV2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    const { heroCms } = this.props;
    smoothscroll.polyfill();
    const data = _.filter(heroCms.value, x => x.type === 'banners');
    this.setState({ data: _.map(data, d => d.value) });
  }

  onClick = (link) => {
    const { login, history } = this.props;
    onClickLink(link, login, history);
  };

  onClickScroll = () => {
    window.scrollTo({ top: (window.innerHeight - 110), behavior: 'smooth' });
  }

  htmlWeb = (d) => {
    const { videos, images } = d;
    const type = isBrowser ? 'web' : isTablet ? 'tablet' : 'phone';
    const video = _.find(videos, x => x.value.type === type);
    const image = _.find(images, x => x.value.type === type);
    console.log('d.text_color', d.text_color);
    return (
      <div className="div-custome-silde-v2" style={{ width: '100%', height: '100%', display: 'flex' }}>
        {
        !_.isEmpty(image)
          ? (
            <Suspense fallback={(
              <div />
          )}
            >
              <LazyImage src={image.value.image} className="cursor-pointer" alt={getAltImageV2(image.value)} />
            </Suspense>
          ) : (
            <video
              style={{
                objectFit: 'cover',
                width: isEdge ? 'auto' : '100%',
                position: 'absolute',
                // height: '100%',
              }}
              muted="true"
              playsinline="true"
              autoPlay="true"
              loop="true"
              preload="auto"
              src={video?.value?.video}
              poster={video?.value?.placeholder}
            />
          )
      }

        {/* <img src={d.image ? d.image.image : ''} alt={getAltImage(d.image ? d.image.image : '')} /> */}
        <div className={classnames('div-text', isRightAlignedText() ? 'div-right' : '')} style={{ color: d.text_color }}>
          <h1 className="header_1 animated fadeIn delay-2s w-100" style={{ color: d.text_color }}>
            {d.header ? d.header.header_text : ''}
            {' '}
            <br />
            {' '}
            {d.header2 ? d.header2?.header_text : ''}
          </h1>
          <h3 className="header_3 div-description animated fadeIn delay-2s w-100" style={{ color: d.text_color }}>
            {ReactHtmlParser(d.description.replace('PRICE_HERE', generaCurrency(d.item ? d.item.item.price : '')))}
          </h3>
          <div className="animated fadeIn delay-2s">
            <ButtonCT
              typeBT={d.button?.link?.includes('https') ? 'LINK-OUT' : 'LINK'}
              className={classnames('button-black', d.text_color === '#FFFFFF' ? 'bg-white-bt' : '', 'animated-hover')}
              name={d.button?.text}
              href={d.button?.link?.includes('https') ? d.button?.link : generateUrlWeb(d.button?.link)}
            />
          </div>
        </div>
      </div>
    );
  }

  htmlMobile = (d) => {
    const { videos, images } = d;
    const type = isBrowser ? 'web' : isTablet ? 'tablet' : 'phone';
    const image = _.find(images, x => x.value.type === type);
    const video = _.find(videos, x => x.value.type === type);
    return (
      <div className="div-custome-silde-mobile-v2">
        <div className="div-image-text">
          {
          !_.isEmpty(images)
            ? (
              <Suspense fallback={(
                <h2 />
              )}
              >
                <LazyImage
                  src={image.value.image}
                  link={d.button ? d.button.link : ''}
                  // onClick={() => this.onClick(d.button ? d.button.link : '')}
                  // style={{ cursor: 'pointer' }}
                />
              </Suspense>
            ) : (
              <video
                style={{
                  objectFit: 'cover',
                  width: isEdge ? 'auto' : '100%',
                  height: '100%',
                }}
                muted="true"
                playsinline="true"
                autoPlay="true"
                loop="true"
                preload="auto"
                src={video?.value?.video}
                // poster={video.value.placeholder}
              />
            )
        }
          <div className="div-text-absolute div-text" style={{ color: d.text_color }}>
            <div className="title-text">
              <h1 className="header_1">
                {d.header ? d.header.header_text : ''}
              </h1>
              {
                d.header2?.header_text && (
                  <h2 className="header_1">
                    {d.header ? d.header2.header_text : ''}
                  </h2>
                )
              }
            </div>


            <h3 className="header_3 animated fadeIn">
              {ReactHtmlParser(d.description2?.replace('PRICE_HERE', generaCurrency(d.item ? d.item.item.price : '')))}
            </h3>
            <ButtonCT
              typeBT="LINK"
              className={classnames('button-black')}
              name={d.button?.text}
              href={generateUrlWeb(d.button?.link)}
            />
          </div>
        </div>
      </div>
    );
  }


  render() {
    const { data } = this.state;
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      // autoplay: true,
      arrows: false,
      autoplaySpeed: 5000,
      pauseOnHover: false,
    };
    return (
      <div className="block-carousel-v2">
        <div className="div-image">
          <Slider {...settings}>
            {
              _.map(data, d => (
                isBrowser ? this.htmlWeb(d) : this.htmlMobile(d)
              ))
            }
          </Slider>
          <img loading="lazy" className={isBrowser ? 'ic-scroll-down' : 'hidden'} src={icScroll} alt="scroll" onClick={this.onClickScroll} />
        </div>
      </div>
    );
  }
}

export default BlockCarouselV2;
