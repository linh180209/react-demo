import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ScrollItem from './scrollItem';
import '../../../styles/block-saying-v2.scss';

class BlockSayingV2 extends Component {
  render() {
    const { heroCms, history, login } = this.props;
    const data = heroCms ? heroCms.value : {};
    const { header, feedbacks, text } = data;
    return (
      <div className="div-block-saying-v2">
        <h2 className="header_2">
          {header ? header.header_text : ''}
        </h2>
        {/* <span>
          {text}
        </span> */}
        <div className="div-scroll">
          {
            _.map(feedbacks || [], (d, index) => (
              <ScrollItem data={d} isFirst={index === 0} history={history} login={login} />
            ))
          }
        </div>
      </div>
    );
  }
}

export default BlockSayingV2;
