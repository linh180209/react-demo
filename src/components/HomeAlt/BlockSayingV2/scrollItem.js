/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component, lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { getAltImage, isRightAlignedText, onClickLink } from '../../../Redux/Helpers';

const LazyImage = lazy(() => import('../../LazyImage'));

class ScrollItem extends Component {
  onClick = (link) => {
    const { login, history } = this.props;
    onClickLink(link, login, history);
  };

  render() {
    const { isFirst, data } = this.props;
    const {
      avatar, link, text, name,
    } = data.value;
    return (
      <div className={classnames('div-scroll-item', isFirst ? 'item-first' : '')}>
        <div>
          {/* <Suspense fallback={<div />}>
            <LazyImage src={avatar.image} onClick={() => this.onClick(link)} className="cursor-pointer" />
          </Suspense> */}
          <LazyImage src={avatar.image} alt={getAltImage(avatar.image)} onClick={() => this.onClick(link)} className="cursor-pointer" />
          {/* <img src={avatar.image} alt={getAltImage(avatar.image)} onClick={() => this.onClick(link)} className="cursor-pointer" /> */}
          <h4 className="header_4 text-name">
            {name}
          </h4>
        </div>
        <h4 className={classnames('header_4', isRightAlignedText() ? 'tr' : 'tl')}>
          {text}
        </h4>
      </div>
    );
  }
}

ScrollItem.propTypes = {
  isEnd: PropTypes.bool.isRequired,
  data: PropTypes.shape().isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
};

export default ScrollItem;
