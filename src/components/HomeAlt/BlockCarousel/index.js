/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component, lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import Slider from 'react-slick';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';
import { isEdge } from 'react-device-detect';
import smoothscroll from 'smoothscroll-polyfill';
import 'react-image-gallery/styles/css/image-gallery.css';
import { Link } from 'react-router-dom';
import '../../../styles/block-carousel.scss';
import icScroll from '../../../image/icon/ic-scroll-landing-black.svg';
import {
  getAltImage, onClickLink, generaCurrency, generateUrlWeb,
} from '../../../Redux/Helpers';
import { isBrowser, isTablet } from '../../../DetectScreen';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const LazyImage = lazy(() => import('../../LazyImage'));

class BlockCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    const { heroCms } = this.props;
    smoothscroll.polyfill();
    const data = _.filter(heroCms.value, x => x.type === 'banners');
    this.setState({ data: _.map(data, d => d.value) });
  }

  onClick = (link) => {
    const { login, history } = this.props;
    onClickLink(link, login, history);
  };

  onClickScroll = () => {
    window.scrollTo({ top: (window.innerHeight - 110), behavior: 'smooth' });
  }

  htmlWeb = (d) => {
    const { videos } = d;
    const type = isBrowser ? 'web' : isTablet ? 'tablet' : 'phone';
    const video = _.find(videos, x => x.value.type === type);
    return (
      <div className="div-custome-silde" style={{ width: '100%', height: '100%', display: 'flex' }}>
        {
        _.isEmpty(video.value.video)
          ? (
            <Suspense fallback={(
              <div />
          )}
            >
              <LazyImage src={video.value.placeholder} className="cursor-pointer" />
            </Suspense>
          ) : (
            <video
              style={{
                objectFit: 'cover',
                width: isEdge ? 'auto' : '100%',
                position: 'absolute',
                // height: '100%',
              }}
              muted="true"
              playsinline="true"
              autoPlay="true"
              loop="true"
              preload="auto"
              src={video?.value?.video}
              poster={video?.value?.placeholder}
            />
          )
      }

        {/* <img src={d.image ? d.image.image : ''} alt={getAltImage(d.image ? d.image.image : '')} /> */}
        <div className="div-text animated slideInLeft delay-2s" style={{ color: '#0d0d0d' }}>
          <h1 className="animated fadeIn delay-2s  w-100">
            {d.header ? d.header.header_text : ''}
          </h1>
          {/* <span style={{ display: isBrowser ? '' : 'none' }}>
            { d.header2 ? d.header2.header_text : '' }
          </span> */}
          <div className="div-description mb-5 animated fadeIn delay-2s w-100">
            {ReactHtmlParser(d.description.replace('PRICE_HERE', generaCurrency(d.item ? d.item.item.price : '')))}
          </div>
          <div className="animated fadeIn">
            <Link
              to={d.button ? generateUrlWeb(d.button.link) : ''}
            // type="button"
              className="button-homepage-new-2 button-a animated-hover"
            >
              {d.button ? d.button.text : ''}
            </Link>
          </div>
        </div>
      </div>
    );
  }

  htmlMobile = (d) => {
    const { videos } = d;
    const type = isBrowser ? 'web' : isTablet ? 'tablet' : 'phone';
    const video = _.find(videos, x => x.value.type === type);
    return (
      <div className="div-custome-silde-mobile">
        <div className="div-image-text">
          {
          _.isEmpty(video.value.video)
            ? (
              <Suspense fallback={(
                <div />
              )}
              >
                <LazyImage src={video.value.placeholder} link={d.button ? d.button.link : ''} onClick={() => this.onClick(d.button ? d.button.link : '')} style={{ cursor: 'pointer' }} />
              </Suspense>
            ) : (
              <video
                style={{
                  objectFit: 'cover',
                  width: isEdge ? 'auto' : '100%',
                  height: '100%',
                }}
                muted="true"
                playsinline="true"
                autoPlay="true"
                loop="true"
                preload="auto"
                src={video?.value?.video}
                // poster={video.value.placeholder}
              />
            )
        }
          <div className="div-text-absolute div-text animated slideInUp">
            <h1 className="animated fadeIn">
              {d.header ? d.header.header_text : ''}
            </h1>
            <div className="div-description animated fadeIn">
              {ReactHtmlParser(d.description.replace('PRICE_HERE', generaCurrency(d.item ? d.item.item.price : '')))}
            </div>
            <Link
              ontouchstart=""
              to={d.button ? generateUrlWeb(d.button.link) : ''}
              className="button-homepage-new animated-hover animated fadeIn"
            >
              {d.button ? d.button.text : ''}
            </Link>
          </div>
        </div>
      </div>
    );
  }


  render() {
    const { data } = this.state;
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      // autoplay: true,
      arrows: false,
      autoplaySpeed: 5000,
      pauseOnHover: false,
    };
    return (
      <div className="block-carousel">
        <div className="div-image">
          <Slider {...settings}>
            {
              _.map(data, d => (
                isBrowser ? this.htmlWeb(d) : this.htmlMobile(d)
              ))
            }
          </Slider>
          <img loading="lazy" className={isBrowser ? 'ic-scroll-down' : 'hidden'} src={icScroll} alt="scroll" onClick={this.onClickScroll} />
        </div>
      </div>
    );
  }
}

BlockCarousel.propTypes = {
  heroCms: PropTypes.shape(PropTypes.object).isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape(PropTypes.object).isRequired,
};

export default BlockCarousel;
