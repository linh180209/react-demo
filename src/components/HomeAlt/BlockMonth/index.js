import React, { Component } from 'react';
import testImage from '../../../image/test/1.png';
import { getAltImage } from '../../../Redux/Helpers';
import '../../../styles/block-month.scss';

export default class BlockMonth extends Component {
  render() {
    return (
      <div className="div-block-month">
        <div className="div-image">
          <img loading="lazy" src={testImage} alt={getAltImage(testImage)} />
        </div>
        <div className="div-text">
          <span className="header">
            Get a Monthly Scent
          </span>
          <div className="div-money">
            <span className="price">
              $50
            </span>
            <span className="description">
              / Month with Free Shipping
            </span>
          </div>
          <div className="div-button">
            <button type="button">
              choose scent
            </button>
          </div>
        </div>
      </div>
    );
  }
}
