import React, { Component } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import Slider from 'react-slick';
import reactHtmlPare from 'react-html-parser';
import { Link } from 'react-router-dom';
import '../../../styles/block-icon-header-v3.scss';
import { generateUrlWeb, getAltImageV2 } from '../../../Redux/Helpers';
import LazyImage from '../../LazyImage';
import ButtonCT from '../../ButtonCT';
import { isMobile } from '../../../DetectScreen';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

class BlockIconHeaderV3 extends Component {
  renderItem = x => (
    <div className="div-item">
      <LazyImage src={x.value.image ? x.value.image.image : ''} alt={getAltImageV2(x.value.image)} />
      <h3 className="header_3">
        {x.value.header.header_text}
      </h3>
      <h4 className="header_4">
        {reactHtmlPare(x.value.text)}
      </h4>
    </div>
  );

  render() {
    const settings = {
      dots: true,
      arrows: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplaySpeed: 6000,
      pauseOnHover: false,
    };
    const { heroCms, isHidenHeader } = this.props;
    if (_.isEmpty(heroCms)) {
      return null;
    }
    const {
      header, benefits, text, button,
    } = heroCms.value;
    return (
      <div className={classnames('div-block-icon-header-v3', benefits.length < 5 ? 'small-size' : '')}>
        <h2 className={isHidenHeader ? 'hidden' : 'header_2'}>
          {header ? header.header_text : ''}
        </h2>
        {
          isMobile ? (
            <div className="div-list-icon">
              <Slider {...settings}>
                {
                  _.map(benefits, x => (
                    this.renderItem(x)
                  ))
                }
              </Slider>
            </div>
          ) : (
            <div className="div-list-icon">
              {
                _.map(benefits, x => (
                  this.renderItem(x)
                ))
              }
            </div>
          )
        }

        {
          isMobile && (
            <ButtonCT
              typeBT="LINK"
              className={classnames('link-text link-text-landingpage')}
              name={button?.name}
              href={generateUrlWeb(button?.link)}
            />
          )
        }

      </div>
    );
  }
}

export default BlockIconHeaderV3;
