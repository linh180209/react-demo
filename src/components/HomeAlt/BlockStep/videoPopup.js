/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  isIOS,
} from 'react-device-detect';
import icClose from '../../../image/icon/ic-close-popup.svg';
import VideoAcademy from '../../AcademyItem/videoAcademy';
import { isBrowser, isMobile, isTablet } from '../../../DetectScreen';

class VideoPopup extends Component {
  onClickClose = () => {
    const { clickClosePopup } = this.props;
    if (clickClosePopup) {
      clickClosePopup();
    }
  }

  render() {
    const { data, isShow, autoPlay } = this.props;
    const { videos } = data.value;
    const valueFindVideo = isMobile ? 'phone' : (isTablet ? 'tablet' : 'web');
    const dataVideo = _.find(videos, x => x.value.type === valueFindVideo);
    const videoBlock = dataVideo ? dataVideo.value : undefined;
    return (
      <div className={`div-video-popup ${isShow ? 'show' : 'close'}`}>
        <div className="div-video">
          <img loading="lazy" src={icClose} alt="icClose" onClick={this.onClickClose} />
          {
            isShow
              ? (
                <VideoAcademy autoPlay={autoPlay} url={videoBlock ? videoBlock.video : ''} poster={videoBlock ? videoBlock.placeholder : ''} />
              ) : (<div />)
          }

        </div>
      </div>
    );
  }
}

VideoPopup.propTypes = {
  data: PropTypes.shape().isRequired,
  isShow: PropTypes.bool.isRequired,
  clickClosePopup: PropTypes.func.isRequired,
};

export default VideoPopup;
