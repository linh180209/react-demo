/* eslint-disable jsx-a11y/media-has-caption */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import reactHtmlPare from 'react-html-parser';
import _ from 'lodash';
import ScrollDetect from '../scrollDetect';

class BlockStepItem extends Component {
  constructor(props) {
    super(props);
    this.refVideo = React.createRef();
    this.state = {
      showData: false,
    };
  }

  detectScrollTo = () => {
    this.setState({ showData: true });
  }

  render() {
    const { showData } = this.state;
    const { step, data, isBenefitsBlock } = this.props;
    const { header, image, text } = data.value;
    const headerText = isBenefitsBlock ? header.header_text : header;
    // const valueFindVideo = isMobile ? 'phone' : (isTablet ? 'tablet' : 'web');
    // const dataVideo = _.find(videos, x => x.value.type === valueFindVideo);
    // const videoBlock = dataVideo ? dataVideo.value : undefined;
    return (
      <div className="div-block-step-item">
        {/* <div className="div-video">
          <VideoAcademy url={videoBlock ? videoBlock.video : ''} poster={videoBlock ? videoBlock.placeholder : ''} />
        </div> */}
        <ScrollDetect id={isBenefitsBlock ? `id-item-benefits-${step}` : `id-item-how-work-${step}`} detectScrollTo={this.detectScrollTo} />
        {
          isBenefitsBlock ? (
            <img loading="lazy" className={`image-icon ${showData ? 'animate-show' : 'animate-hide'}`} src={image ? image.image : ''} alt="alt" />
          ) : (
            <span className={`header-number ${showData ? 'animate-show' : 'animate-hide'}`}>
              0
              {step}
            </span>
          )
        }

        <span className={`header-text ${showData ? 'animate-show delay-0-5' : 'animate-hide'}`}>
          {headerText}
        </span>
        <span className={`text-description ${showData ? 'animate-show delay-1' : 'animate-hide'}`}>
          {reactHtmlPare(text)}
        </span>
      </div>
    );
  }
}

BlockStepItem.propTypes = {
  step: PropTypes.number.isRequired,
  data: PropTypes.shape().isRequired,
};

export default BlockStepItem;
