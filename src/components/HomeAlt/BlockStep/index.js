import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { ParallaxBanner, withController } from 'react-scroll-parallax';
import { Link } from 'react-router-dom';

import { isIOS } from 'react-device-detect';
import ScrollDetect from '../scrollDetect';
import BlockStepItem from './blockStepItem';
import VideoPopup from './videoPopup';
import icVideo from '../../../image/icon/ic-play-video.svg';
import '../../../styles/block-step-landing.scss';
import { generateUrlWeb, onClickLink } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';

class BlockStep extends Component {
  state = {
    isShowFullVideo: false,
    isShowHeader: false,
  };

  onClick = (link) => {
    const { login, history } = this.props;
    onClickLink(link, login, history);
  };

  detectScrollTo = () => {
    this.setState({ isShowHeader: true });
  }

  render() {
    const { heroCms, isBenefitsBlock } = this.props;
    const { isShowFullVideo, isShowHeader } = this.state;
    if (_.isEmpty(heroCms)) {
      return null;
    }
    const {
      steps, buttons, button, image_background: imageBlock, header, benefits, opacity,
    } = heroCms.value;
    const bt1 = !isBenefitsBlock && buttons && buttons.length > 0 ? buttons[0].value : {};
    const bt2 = !isBenefitsBlock && buttons && buttons.length > 1 ? buttons[1].value : {};
    const videoFull = !isBenefitsBlock && steps.length > 3 ? steps[3] : {};
    const stepVideo = !isBenefitsBlock ? steps.slice(0, 3) : [];
    const element = (
      <div
        className="div-block-step"
        style={{ backgroundImage: `url(${imageBlock ? imageBlock.image : ''})` }}
      >
        {
          isBenefitsBlock
            ? (<div />)
            : (
              <VideoPopup
                autoPlay
                data={videoFull}
                isShow={isShowFullVideo}
                clickClosePopup={() => { this.setState({ isShowFullVideo: false }); }}
              />
            )
        }
        <ScrollDetect id={isBenefitsBlock ? 'id-header-benefits' : 'id-header-how-work'} detectScrollTo={this.detectScrollTo} />
        <h1 className={isShowHeader ? 'animate-show title-global' : 'animate-hide'}>
          {header ? header.header_text : ''}
        </h1>
        <Row>
          {
            _.map(isBenefitsBlock ? benefits : stepVideo, (x, index) => (
              <Col md="4" xs="12" className={`col-item ${index === 0 ? 'left-item' : (index === 1 ? 'center-item' : 'right-item')}`}>
                <BlockStepItem data={x} step={index + 1} isBenefitsBlock={isBenefitsBlock} />
              </Col>
            ))
          }
        </Row>
        <div className="div-button">
          <Link to={isBenefitsBlock ? (button ? generateUrlWeb(button.link) : '') : generateUrlWeb(bt1.link)}>
            {isBenefitsBlock ? (button ? button.name : '') : bt1.name}
          </Link>
          {
            isBenefitsBlock
              ? (<div />) : (
                <button type="button" onClick={() => { this.setState({ isShowFullVideo: true }); }}>
                  <div>
                    <span>{bt2.name}</span>
                    <img loading="lazy" src={icVideo} alt="icVideo" />
                  </div>
                </button>
              )
          }
        </div>
      </div>
    );
    return (
      <React.Fragment>
        {
          isMobile && isIOS ? (
            <ParallaxBanner
              layers={[
                {
                  image: imageBlock ? imageBlock.image : '',
                  amount: 0.4,
                },
              ]}
              style={{
                height: '100%',
              }}
            >
              {element}

            </ParallaxBanner>
          ) : (element)
      }
      </React.Fragment>

    );
  }
}

BlockStep.defaultProps = {
  isBenefitsBlock: false,
};

BlockStep.propTypes = {
  heroCms: PropTypes.shape().isRequired,
  history: PropTypes.shape().isRequired,
  login: PropTypes.shape().isRequired,
  isBenefitsBlock: PropTypes.bool,
};

export default withController(BlockStep);
