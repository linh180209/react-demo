import React, { Component } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import Slider from 'react-slick';
import reactHtmlPare from 'react-html-parser';
import { Link } from 'react-router-dom';
import '../../../styles/block-icon-header-v2.scss';
import { generateUrlWeb, getAltImageV2 } from '../../../Redux/Helpers';
import LazyImage from '../../LazyImage';
import ButtonCT from '../../ButtonCT';
import { isMobile } from '../../../DetectScreen/detectIFrame';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

class BlockIconHeaderV2 extends Component {
  renderItem = x => (
    <div className="div-item">
      <LazyImage src={x.value.image ? x.value.image.image : ''} alt={getAltImageV2(x.value.image)} />
      <h4 className={this.props.classSub1}>
        {x.value.header.header_text}
      </h4>
      <div className={classnames('tc', this.props.classSub2)}>
        {reactHtmlPare(x.value.text)}
      </div>
    </div>
  );

  render() {
    const settings = {
      dots: true,
      arrows: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplaySpeed: 6000,
      pauseOnHover: false,
    };
    const {
      heroCms, isHidenHeader, classHeader, classSub1, classSub2,
    } = this.props;
    if (_.isEmpty(heroCms)) {
      return null;
    }
    const {
      header, benefits, text, button,
    } = heroCms.value;
    return (
      <div className={classnames('div-block-icon-header-v2', benefits.length < 5 ? 'small-size' : '')}>
        <h2 className={classnames(isHidenHeader ? 'hidden' : '', classHeader)}>
          {header ? header.header_text : ''}
        </h2>
        {
          text && (
            <span>
              {text}
            </span>
          )
        }
        {
          isMobile && this.props.isSlider ? (
            <div className="div-list-icon">
              <Slider {...settings}>
                {
                  _.map(benefits, x => (
                    this.renderItem(x)
                  ))
                }
              </Slider>
            </div>
          ) : (
            <div className="div-list-icon">
              {
            _.map(benefits, x => (
              this.renderItem(x)
            ))
          }
            </div>
          )
        }

        {
          button && button.name && (
            <ButtonCT
              typeBT="LINK"
              className={classnames('outline-bt', 'animated-hover')}
              name={button?.name}
              href={generateUrlWeb(button?.link)}
            />
          )
        }

      </div>
    );
  }
}

export default BlockIconHeaderV2;
