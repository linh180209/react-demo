import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import '../../../styles/block-workshop-v2.scss';
import WorkshopList from '../../../views/Workshop/components/WorkshopList';
import { generateUrlWeb } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';
import ButtonCT from '../../ButtonCT';

function BlockWorkShopV2(props) {
  return (
    <div className="block-workshop-v2 div-col">
      <h2 className="header_2">
        {props.data.value.header.header_text}
      </h2>
      <h3 className="header_3">
        {props.data.value.text}
      </h3>
      <WorkshopList
        buttonBlocks={props.buttonBlocks}
        isHomePage
        items={_.map(props.data.value.ateliers, d => d.value)}
        showDetails={(index) => { props.history.push(generateUrlWeb(props.data.value.ateliers[index].value.buttons.length > 0 ? props.data.value.ateliers[index].value.buttons[0].value.link : '')); }}
      />
      {
        isMobile && (
          <ButtonCT
            typeBT="LINK"
            className="link-text link-text-landingpage"
            name={props.data?.value?.button?.name}
            href={generateUrlWeb(props.data?.value?.button?.link)}
          />
        )
      }
    </div>
  );
}

export default BlockWorkShopV2;
