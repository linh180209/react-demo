import React, { useState, useEffect } from 'react';
import { Modal, ModalBody } from 'reactstrap';
import classnames from 'classnames';
import CloseIcon from '@mui/icons-material/Close';
import * as EmailValidator from 'email-validator';
import { isBrowser, isMobile } from '../../../../DetectScreen';
import InputCT from '../../../../componentsv2/inputCT';
import ButtonCT from '../../../../componentsv2/buttonCT';
import { toastrError, toastrSuccess } from '../../../../Redux/Helpers/notification';
import { getNameFromButtonBlock } from '../../../../Redux/Helpers';
import './styles.scss';
import fetchClient from '../../../../Redux/Helpers/fetch-client';
import { SEND_NOTIFI_STOCK } from '../../../../config';

function NotifiMe(props) {
  const [email, setEmail] = useState();
  const onChangeEmail = (e) => {
    const { value } = e.target;
    setEmail(value);
  };
  const onClickEmail = () => {
    if (!EmailValidator.validate(email)) {
      toastrError('The email is not available');
      return;
    }
    const option = {
      url: SEND_NOTIFI_STOCK.replace('{id}', props.data?.item_id),
      method: 'POST',
      body: {
        email,
      },
    };

    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        props.onClose();
      } else {
        throw new Error(result.message);
      }
    }).catch((err) => {
      toastrError(err.message);
    });
  };

  const stockBt = getNameFromButtonBlock(props.buttonBlocks, 'stock notification');
  const enterEmailBt = getNameFromButtonBlock(props.buttonBlocks, 'Enter your email');
  const emailAddressBt = getNameFromButtonBlock(props.buttonBlocks, 'Email Address');
  const notifymeBt = getNameFromButtonBlock(props.buttonBlocks, 'notify me');
  return (
    <Modal className="notifi-me" isOpen centered>
      <ModalBody>
        <div className="header-notifi">
          <div className="other-Tagline">
            <b>{stockBt}</b>
          </div>
          <button
            onClick={props.onClose}
            type="button"
            className="button-bg__none"
          >
            <CloseIcon style={{ color: '#2c2c2c' }} />
          </button>
        </div>
        <div className={classnames('body-text-s-regular', isBrowser ? 'm-size' : '')}>
          {enterEmailBt}
        </div>
        <div className="list-input">
          <InputCT
            type="text"
            placeholder={emailAddressBt}
            value={email}
            onChange={onChangeEmail}
            className={classnames(isMobile ? 'medium' : '')}
          />
          <ButtonCT name={notifymeBt} onClick={onClickEmail} size={isMobile ? 'medium' : ''} />
        </div>
      </ModalBody>
    </Modal>
  );
}

export default NotifiMe;
