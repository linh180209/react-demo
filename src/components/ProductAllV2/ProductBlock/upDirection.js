import * as React from 'react';
import { Range, Direction, getTrackBackground } from 'react-range';

const STEP = 0.1;
const MIN = 0;
const MAX = 3;

class UpDirection extends React.Component {
  onChange = (values) => {
    if (values && values.length > 0) {
      this.props.onChange(values[0]);
    }
  }

  render() {
    return (
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          height: '180px',
          flexDirection: 'column',
        }}
      >
        <Range
          direction={Direction.Up}
          values={[this.props.value]}
          step={STEP}
          min={MIN}
          max={MAX}
          onChange={this.onChange}
          renderTrack={({ props, children }) => (
            <div
              onMouseDown={props.onMouseDown}
              onTouchStart={props.onTouchStart}
              style={{
                ...props.style,
                flexGrow: 1,
                width: '15px',
                display: 'flex',
                height: '100%',
                zIndex: '3',
                marginLeft: '10px',
              }}
            >
              <div
                ref={props.ref}
                style={{
                  width: '5px',
                  height: '100%',
                  borderRadius: '4px',
                  background: getTrackBackground({
                    values: [this.props.value],
                    colors: ['#C4C4C4', '#C4C4C4'],
                    min: MIN,
                    max: MAX,
                    direction: Direction.Up,
                  }),
                  alignSelf: 'center',
                }}
              >
                {children}
              </div>
            </div>
          )}
          renderThumb={({ props, isDragged }) => (
            <div
              {...props}
              style={{
                ...props.style,
                height: '16px',
                width: '16px',
                borderRadius: '8px',
                backgroundColor: '#0d0d0d',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />
          )}
        />
        {/* <output style={{ marginTop: '50px', width: '56px' }} id="output">
          {this.state.values[0].toFixed(1)}
        </output> */}
      </div>
    );
  }
}

export default UpDirection;
