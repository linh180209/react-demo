import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import './pick-ingredient.scss';

import icBack from '../../../image/icon/ic-prev-silder.svg';
import icInfo from '../../../image/icon/ic-info-new.svg';
import icSearch from '../../../image/icon/search-b.svg';
import { GET_INGREDIENT_ID_PRODUCTS } from '../../../config';
import fetchClient, { fetchClientAlwaysUS } from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import { getNameFromButtonBlock, segmentProductViewedScentSelected, sendSelectScentXClickHotjarEvent } from '../../../Redux/Helpers';
import FilterSearchScent from './filterSearchScent';
import ScentLine from './scentLine';
import { isBrowser, isMobile } from '../../../DetectScreen';
import ButtonCT from '../../ButtonCT';
import ButtonCTV2 from '../../../componentsv2/buttonCT';
import IngredientPopUpV2 from '../../../componentsv2/ingredientPopUpV2';
import IngredientPopUp from '../../IngredientDetail/ingredientPopUp';
import NotifiMe from './notifiMe';

class PickIngredient extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scentSelected: !props.isPickIngredientDetailV2 ? props.dataSelection : {},
      valueFilter: props.isShowHome ? 'Scent Family' : (props.isDualCandles ? 'Popularity' : 'Scent Family'),
      valueSearch: '',
      ingredientMini: props.isOnlyShowIngredientLocal ? {} : undefined,
      dataShowIngredient: props.isPickIngredientDetailV2 ? props.dataSelection : undefined,
      isShowNotifiMe: false,
    };
    this.refFilterSearchScent = React.createRef();
  }

  componentDidMount = () => {
    if (this.props.shareRefFilterSearchScent) {
      this.props.shareRefFilterSearchScent(this.refFilterSearchScent);
    }
    if (this.props.dataSelection?.ingredient) {
      this.onClickIngredientMini(this.props.dataSelection.ingredient);
    }
  };

  onClickShowIngredientV2 = (data) => {
    if (this.props.updateDataCurrentSelect) {
      this.props.updateDataCurrentSelect(data);
    }
  };

  onClick = (data) => {
    console.log('data ==> ', data, this.props.isShowIngredientDetail);
    const { scentSelected } = this.state;
    const { dataSelection } = this.props;
    if (data.id === scentSelected.id) {
      this.setState({ scentSelected: {} });
      this.props.onClickChangeImageCandle(true, dataSelection.index);
      this.onClickShowIngredientV2({});
    } else {
      this.setState({ scentSelected: data });
      this.props.onClickChangeImageCandle(false, dataSelection.index);
      if (this.props.isPickIngredientDetailV2) {
        this.setState({ dataShowIngredient: data });
      } else if (this.props.isShowIngredientDetail) {
        this.onClickIngredientMini(data.ingredient);
      }
      this.onClickShowIngredientV2(data);
    }
  }

  onClickCancelIngredients = () => {
    const { dataSelection } = this.props;
    this.setState({ scentSelected: {}, ingredientMini: undefined });
    this.props.onClickChangeImageCandle(true, dataSelection.index);
    if (this.props.isOnlyShowIngredientLocal) {
      this.props.onClickCloseScent();
    }
  }

  onClickApply = async () => {
    const { scentSelected } = this.state;
    const { dataSelection } = this.props;
    sendSelectScentXClickHotjarEvent(dataSelection?.index);
    try {
      const option = {
        url: GET_INGREDIENT_ID_PRODUCTS.replace('{id}', scentSelected?.ingredient?.id ?? scentSelected?.ingredient),
        method: 'GET',
      };
      const response = await fetchClientAlwaysUS(option);
      const englishIngredient = response;
  
      segmentProductViewedScentSelected(scentSelected, dataSelection?.index, englishIngredient);
      this.props.onClickApply(scentSelected, dataSelection.index);
    } catch (error) {
      this.props.loadingPage(false);
    }
    
  }

  onNotifiMe = () => {
    this.setState({ isShowNotifiMe: true });
  }

  onCloseNotifiMe = () => {
    this.setState({ isShowNotifiMe: false });
  }

  onClickIngredientMini = (id) => {
    console.log('onClickIngredientMini', id);
    if (typeof id === 'object') {
      this.setState({ ingredientMini: id });
      return;
    }
    const option = {
      url: GET_INGREDIENT_ID_PRODUCTS.replace('{id}', id),
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.setState({ ingredientMini: result });
        this.props.loadingPage(false);
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      this.props.loadingPage(false);
      toastrError(err.message);
    });
  }

  onClickIngredient = (id) => {
  
    if (typeof id === 'object') {
      this.props.onClickIngredient(id);
      return;
    }
    const option = {
      url: GET_INGREDIENT_ID_PRODUCTS.replace('{id}', id),
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.props.onClickIngredient(result);
        this.props.loadingPage(false);
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      this.props.loadingPage(false);
      toastrError(err.message);
    });
  }

  onChangeValueFilter = (value) => {
    this.setState({ valueFilter: value });
  }

  onChangeValueSearch = (value) => {
    this.setState({ valueSearch: value });
  }

  applyFilter = (dataScents, value) => {
    const datas = _.find(dataScents, x => x.title.includes(value) || x.title.includes(value.toLowerCase()));
    if (value === 'Alphabet' || value === 'Popularity') {
      return datas ? datas.datas : [];
    }
    const datasNew = datas ? datas.datas : [];
    const scentT1 = _.filter(datasNew, x => x.group.toLowerCase() === value.toLowerCase());
    return scentT1;
  }

  applySearch = (dataScents, value) => {
    if (!value) {
      return dataScents;
    }
    const newDataScents = _.cloneDeep(dataScents);
    _.forEach(newDataScents, (d) => {
      const { datas } = d;
      d.datas = _.filter(datas, x => x.name.toLowerCase().includes(value.toLowerCase()));
    });
    return newDataScents;
  }

  renderIngredientLocalV2 = () => (
    <div className="div-ingredient-local">
      <IngredientPopUpV2
        onCancel={() => {
          this.setState({ dataShowIngredient: undefined });
          this.onClick(this.state.scentSelected);
        }}
        onClickApply={this.onClickApply}
        data={this.state.dataShowIngredient}
        buttonBlocks={this.props.buttonBlocks}
      />
    </div>
  )

  renderIngredientLocal = () => (
    <div className="div-ingredient-local">
      <div className="div-content-local">
        <IngredientPopUp
          isShow
          ingredientMinimum
          data={this.state.ingredientMini}
          history={this.props.history}
          onClickCancelIngredientsMini={this.onClickCancelIngredients}
          onClickApplyMini={this.onClickApply}
        />
      </div>
      <div className="div-list-button">
        <ButtonCT
          name={getNameFromButtonBlock(this.props.buttonBlocks, 'APPLY')}
          className="button-black"
          onClick={this.onClickApply}
        />
      </div>
    </div>
  )

  render() {
    const {
      onClickCloseScent, dataSelection, dataScents, buttonBlocks,
      isDualCandles, isFreeStyle, isHidenHeader, scentRecommend,
      onClickIngredientProductId, isShowIngredientDetail, isPickIngredientDetailV2,
    } = this.props;
    const {
      scentSelected, valueFilter, valueSearch, ingredientMini, dataShowIngredient,
    } = this.state;
    const newScentFilter = this.applyFilter(dataScents, valueFilter);
    const newScent = this.applySearch(newScentFilter, valueSearch);

    const pickBt = getNameFromButtonBlock(buttonBlocks, 'Pick_a_Ingredient');
    const viewBt = getNameFromButtonBlock(buttonBlocks, 'view_more_info');
    const cancelBt = getNameFromButtonBlock(buttonBlocks, 'CANCEL');
    const applyBt = getNameFromButtonBlock(buttonBlocks, 'APPLY');
    const recommendBt = getNameFromButtonBlock(buttonBlocks, 'RECOMMENDED');
    const notifymeBt = getNameFromButtonBlock(buttonBlocks, 'notify me');
    if (dataShowIngredient?.id && isMobile) {
      return this.renderIngredientLocalV2();
    }
    if (ingredientMini && !isPickIngredientDetailV2) {
      return this.renderIngredientLocal();
    }
    return (
      <div className={classnames('div-pick-ingredient', (isFreeStyle ? '' : 'animated faster fadeInRight'), this.props.className, isDualCandles ? 'dual-candles' : '')}>
        <div className={isHidenHeader ? 'hidden' : 'div-header-ingredient'}>
          {
              !isFreeStyle && (
                <button type="button" onClick={onClickCloseScent}>
                  <img src={icBack} alt="back" />
                </button>
              )
            }
          <div className="div-text-header">
            <h3>
              {pickBt}
              {' '}
              {dataSelection ? dataSelection.subTitle : ''}
            </h3>
          </div>
        </div>
        {
            this.props.titleHeader && (
              <div className="title-header">
                {this.props.titleHeader}
              </div>
            )
          }
        <div className={isMobile || isDualCandles ? 'hidden' : 'div-filter-search'}>
          <FilterSearchScent
            isSortScentNotes
            ref={this.refFilterSearchScent}
            onChangeFilter={this.props.onChangeFilter}
            onChangeSearch={this.props.onChangeSearch}
            valueFilter={valueFilter}
            valueSearch={valueSearch}
            onChangeValueFilter={this.onChangeValueFilter}
            onChangeValueSearch={this.onChangeValueSearch}
            buttonBlocks={buttonBlocks}
            isPickIngredientDetailV2={this.props.isPickIngredientDetailV2}
          />
        </div>
        {isMobile && !isDualCandles ? (
          <FilterSearchScent
            isSortScentNotes
            isVersionWeb
            ref={this.refFilterSearchScent}
            onChangeFilter={this.props.onChangeFilter}
            onChangeSearch={data => this.props.onChangeSearch(isFreeStyle ? newScent : data)}
            valueFilter={valueFilter}
            valueSearch={valueSearch}
            onChangeValueFilter={this.onChangeValueFilter}
            onChangeValueSearch={this.onChangeValueSearch}
            buttonBlocks={buttonBlocks}
            isPickIngredientDetailV2={this.props.isPickIngredientDetailV2}
          />
        ) : null}
        <div className={classnames('div-scroll-ingredient', (isDualCandles ? 'small-height' : ''), (isShowIngredientDetail ? 'ingredient-mini-mobile' : ''))}>
          {
              !_.isEmpty(scentRecommend) && (
                <div className={classnames('recommend-scent-mobile div-col', this.props.isPickIngredientDetailV2 ? 'recommend-v2' : '')}>
                  <h4>
                    {recommendBt.replace('{scent_name}', scentRecommend?.name)}
                  </h4>
                  {
                    this.props.isPickIngredientDetailV2 ? (
                      <div className="scent-recommend div-row">
                        <ScentLine
                          onClickIngredient={this.props.onClickIngredient}
                          data={scentRecommend.suggestions}
                          onClick={this.onClick}
                          scentSelected={scentSelected}
                          className={isDualCandles ? 'item-for-candle' : ''}
                          buttonBlocks={buttonBlocks}
                          isRecommended
                          isNoInfo
                        />
                      </div>
                    ) : (
                      <div className="scent-recommend div-row">
                        {
                        _.map(scentRecommend.suggestions, d => (
                          <div
                            data-gtmtracking={this.props.dataGtmtrackingRecommend}
                            className="item-recommend"
                            onClick={() => this.props.onClickApply(d, dataSelection.index)}
                          >
                            <img loading="lazy" src={d.image} alt="scent" />
                            <button
                              className="button-bg__none"
                              type="button"
                              onClick={(e) => {
                                e.stopPropagation();
                                this.props.onClickIngredientProductId({ productId: d.id });
                              }}
                            >
                              <img src={icInfo} alt="info" />
                            </button>
                          </div>
                        ))
                      }
                      </div>
                    )
                  }
                </div>
              )
            }
          {
              _.map(newScent, x => (
                <ScentLine
                  onClickIngredient={this.props.onClickIngredient}
                  data={x}
                  onClick={this.onClick}
                  scentSelected={scentSelected}
                  className={isDualCandles ? 'item-for-candle' : ''}
                  buttonBlocks={buttonBlocks}
                  isNoInfo={isPickIngredientDetailV2}
                />
              ))
            }
        </div>
        <div className={isShowIngredientDetail || isPickIngredientDetailV2 ? 'hidden' : 'div-button'}>
          {
              isDualCandles ? (
                <div />
              ) : (
                <div className={`text-left ${scentSelected.id ? '' : 'hidden-class'}`}>
                  <h3>
                    {scentSelected.name}
                  </h3>
                  <button type="button" onClick={() => this.onClickIngredient(scentSelected.ingredient)}>
                    <img src={icSearch} alt="icSearch" />
                    {viewBt}
                  </button>
                </div>
              )
            }
          <div className="div-bt-right">
            <button className="bt-cancel" type="button" onClick={onClickCloseScent}>
              {cancelBt}
            </button>
            <button
              data-gtmtracking={this.props.dataGtmtracking}
              disabled={!scentSelected.id && !dataSelection.id}
              className={!scentSelected.id && !dataSelection.id ? 'bt-apply disable' : 'bt-apply'}
              type="button"
              onClick={this.onClickApply}
            >
              {applyBt}
            </button>
          </div>

        </div>
        {
            isPickIngredientDetailV2 && isBrowser && (
              <div className="list-button div-row">
                <ButtonCTV2 name={cancelBt} variant="outlined" onClick={onClickCloseScent} />
                <ButtonCTV2 name={scentSelected?.is_out_of_stock ? notifymeBt : applyBt} onClick={scentSelected?.is_out_of_stock ? this.onNotifiMe : this.onClickApply} />
              </div>
            )
          }
        {
            this.state.isShowNotifiMe && (
              <NotifiMe buttonBlocks={this.props.buttonBlocks} onClose={this.onCloseNotifiMe} data={scentSelected} />
            )
          }
      </div>
    );
  }
}

PickIngredient.defaultProps = {
  onClickChangeImageCandle: () => {},
  isShowIngredientDetail: false,
};

PickIngredient.propTypes = {
  onClickCloseScent: PropTypes.func.isRequired,
  dataScents: PropTypes.arrayOf().isRequired,
  loadingPage: PropTypes.func.isRequired,
  isDualCandles: PropTypes.bool.isRequired,
  onClickChangeImageCandle: PropTypes.func,
  isShowIngredientDetail: PropTypes.bool,
};

export default PickIngredient;
