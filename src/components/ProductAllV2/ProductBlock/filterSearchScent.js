import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ImportExportIcon from '@mui/icons-material/ImportExport';
import SearchIcon from '@mui/icons-material/Search';
import classnames from 'classnames';
import _ from 'lodash';
import React, { Component } from 'react';
import onClickOutside from 'react-onclickoutside';
import {
  Dropdown, DropdownMenu, DropdownToggle,
} from 'reactstrap';
import ButtonCT from '../../../componentsv2/buttonCT';
import { isMobile767fn } from '../../../DetectScreen';
import icFilter from '../../../image/icon/ic-down-filter.svg';
import icSearch from '../../../image/icon/search-product.svg';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';

class FilterSearchScent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
      valueSearch: props.valueSearch,
      isShowInputSearch: false,
      valueFilter: props.valueFilter,
      isShowPopUpSearchMobile: false,
      isShowPopupFiltermobile: false,
    };
    this.valueFilters = [
      {
        title: 'Family',
        value: 'Scent Family',
      },
      {
        title: 'Popularity',
        value: 'Popularity',
      },
      {
        title: 'Alphabet (A-Z)',
        value: 'Alphabet',
      },
      {
        title: 'Mood',
        value: 'Mood',
      },
    ];
    if (props.isFreeStyle || props.isSortScentNotes) {
      this.valueFilters.push({
        title: 'Top - Heart -  Bottom',
        value: 'Scent Note',
      });
    }
    this.refInput = React.createRef();
  }

  toggle = () => {
    const { dropdownOpen } = this.state;
    this.setState({
      dropdownOpen: !dropdownOpen,
    });
  }

  onChange = (value) => {
    this.setState({ valueFilter: value, dropdownOpen: false, isShowPopupFiltermobile: false });
    this.props.onChangeValueFilter(value);
  }

  onChangeSearch = (e) => {
    const { value } = e.target;
    this.setState({ valueSearch: value });
    if (!this.timeOut) {
      this.timeOut = setTimeout(() => {
        this.props.onChangeValueSearch(value);
      }, 300);
      this.timeOut = null;
    }
  }

  handleClickOutside = () => {
    const { isShowInputSearch, valueSearch } = this.state;
    if (!valueSearch && isShowInputSearch) {
      this.setState({ isShowInputSearch: false });
    }
  }

  openSearch = () => {
    this.setState({ isShowInputSearch: true });
    setTimeout(() => {
      this.refInput.current.focus();
    }, 500);
  }

  onClickFilter = () => {
    this.setState({ isShowPopupFiltermobile: true, isShowPopUpSearchMobile: false });
  }

  onChangeValueSearchFilter = (data) => {
    const { type, value } = data;
    if (type === 'Filter') {
      this.setState({ valueFilter: value });
      this.props.onChangeValueFilter(value);
    } else {
      this.setState({ valueSearch: value });
      this.props.onChangeValueSearch(value);
    }
  }

  render() {
    const {
      valueSearch, valueFilter,
    } = this.state;
    const {
      buttonBlocks, isFreeStyle, isAddTitleFilter, isVersionWeb, icDropDown, isPickIngredientDetailV2, isPickIngredientDetailV3,
    } = this.props;
    const searchBt = getNameFromButtonBlock(buttonBlocks, 'SEARCH');
    const filterBt = getNameFromButtonBlock(buttonBlocks, 'FILTER');
    const htmlWeb = (
      <div className={classnames('div-filter-search-scent', isPickIngredientDetailV2 || isPickIngredientDetailV3 ? 'search-scent-detail-v2' : '')}>
        <div className="div-filter">
          {
            (isFreeStyle || isAddTitleFilter) && (
              <h4>
                {filterBt}
              </h4>
            )
          }
          <Dropdown
            isOpen={this.state.dropdownOpen}
            toggle={this.toggle}
            className="dropdown-filter-product"
          >
            <DropdownToggle
              tag="span"
              onClick={this.toggle}
              data-toggle="dropdown"
              aria-expanded={this.state.dropdownOpen}
            >
              {
                isPickIngredientDetailV2 || isPickIngredientDetailV3 ? (
                  <ButtonCT
                    name={valueFilter}
                    startIcon={<ImportExportIcon style={{ color: '#2c2c2c' }} />}
                    endIcon={<ArrowDropDownIcon style={{ color: '#2c2c2c' }} />}
                    size="small"
                    color="secondary"
                    className="bt-fillter"
                    onClick={this.toggle}
                  />
                ) : (
                  <div className="header-filter">
                    <span>
                      {valueFilter}
                    </span>
                    <img loading="lazy" src={icDropDown || icFilter} alt="filter" />
                  </div>
                )
              }
            </DropdownToggle>
            <DropdownMenu>
              {
              _.map(this.valueFilters, d => (
                <div
                  onClick={() => {
                    this.onChange(d.value);
                  }}
                  className={classnames('dropdown-item-filter', this.state.valueFilter === d.value ? 'active' : '')}
                >
                  <span>
                    {`${d.title}`}
                  </span>
                </div>
              ))
            }
            </DropdownMenu>
          </Dropdown>
        </div>
        <div className="div-search">
          <input
            ref={this.refInput}
            type="text"
            value={valueSearch}
            onChange={this.onChangeSearch}
            placeholder={searchBt}
          />
          {
            isPickIngredientDetailV2 || isPickIngredientDetailV3 ? (
              <SearchIcon style={{ color: '#8D826E' }} />
            ) : (
              <img
                loading="lazy"
                src={icSearch}
                alt="search"
              />
            )
          }
        </div>
      </div>
    );
    const htmlMobile = (
      <div className="div-filter-search-scent-mobile">
        <button type="button" onClick={() => this.props.onChangeFilter(true)}>
          {valueFilter}
          <img loading="lazy" src={icFilter} alt="filter" />
        </button>
        <button type="button" onClick={() => this.props.onChangeSearch(true)}>
          <img loading="lazy" src={icSearch} alt="search" />
          {valueSearch || 'Search'}
        </button>
      </div>
    );
    return (
      <React.Fragment>
        {
          !isPickIngredientDetailV3 && !isVersionWeb && isMobile767fn() && !this.props.isLandscape ? htmlMobile : htmlWeb
        }
      </React.Fragment>

    );
  }
}

export default isMobile767fn() ? FilterSearchScent : onClickOutside(FilterSearchScent);
