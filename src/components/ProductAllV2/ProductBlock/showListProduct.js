/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Row, Col } from 'reactstrap';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './show-list-product.scss';
import icBack from '../../../image/icon/back-product.svg';
import icInfo from '../../../image/icon/testMore.png';
import icChecked from '../../../image/icon/check-yellow.svg';
import { GET_PRODUCT_TYPE_LINK, GET_INGREDIENT_ID_PRODUCTS } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import { generaCurrency, generateUrlWeb, getNameFromButtonBlock } from '../../../Redux/Helpers';
import { isBrowser, isMobile } from '../../../DetectScreen';

class ItemProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isHover: false,
    };
  }

  onMouseEnter = () => {
    this.setState({ isHover: true });
  }

  onMouseLeave = () => {
    this.setState({ isHover: false });
  }

  onClickAddTocart = () => {
    const { basket, product } = this.props;
    const { id, name, price } = product;
    const data = {
      item: id,
      is_featured: product.is_featured,
      price,
      quantity: 1,
      name,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    if (!basket.id) {
      this.props.createBasketGuest(dataTemp);
    } else {
      this.props.addProductBasket(dataTemp);
    }
  }

  onClickIngredient = (id) => {
    const option = {
      url: GET_INGREDIENT_ID_PRODUCTS.replace('{id}', id),
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.props.onClickIngredient(result);
        this.props.loadingPage(false);
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      this.props.loadingPage(false);
      toastrError(err.message);
    });
  }

  render() {
    const { isHover } = this.state;
    const { product, basket, bottlePerfume } = this.props;
    const imagePerfume = _.find(bottlePerfume ? bottlePerfume.images : [], x => x.type === 'main');
    const {
      name, image, price, variant_values: variantValues, type, id, ingredient, scents,
      short_description: shortDescription,
    } = product;
    const { items } = basket || { items: [] };

    // remove switch button add to cart
    // const isAddedCart = _.find(items, x => x.item.id === id);
    const isAddedCart = false;

    const des = type.name === 'Kit' && scents && scents.length > 0 ? _.join(scents, ', ') : type.name === 'hand_sanitizer' || type.name === 'single_candle' || type.name === 'holder' ? undefined : shortDescription;
    const isHiddenInfo = !type || type.name !== 'Elixir';
    // const imageProduct = type && type.name === 'Perfume' ? imagePerfume ? imagePerfume.image : '' : image;
    const imageProduct = image;
    return (
      <div
        className="div-product-item"
        // onMouseEnter={this.onMouseEnter}
        // onMouseLeave={this.onMouseLeave}
      >
        <div className="div-image">
          <img loading="lazy" src={imageProduct} alt="product" onClick={() => this.props.onClickGotoProduct(product)} />
          <button
            type="button"
            className={isHiddenInfo ? 'hidden' : ''}
            onClick={() => this.onClickIngredient(ingredient)}
          >
            <img src={icInfo} alt="info" />
          </button>
        </div>
        <span className={(isAddedCart || isHover) && isBrowser ? 'hidden' : 'name'}>
          {des}
        </span>
        <button
          type="button"
          onClick={isAddedCart ? undefined : this.onClickAddTocart}
          className={(isAddedCart || isHover) && isBrowser ? 'add-cart' : 'hidden'}
        >
          <img src={icChecked} alt="checked" className={isAddedCart ? '' : 'hidden'} />
          {`${isAddedCart ? '' : '+'} Add To Cart`}
        </button>
        <span className={name ? 'info' : 'hidden'}>
          {name}
        </span>
        <span className="price">
          {generaCurrency(price)}
        </span>
        <button
          onClick={isAddedCart ? undefined : this.onClickAddTocart}
          type="button"
          className={isMobile ? 'add-cart' : 'hidden'}
        >
          <img src={icChecked} alt="checked" className={isAddedCart ? '' : 'hidden'} />
          {`${isAddedCart ? '' : '+'} Add To Cart`}
        </button>
      </div>
    );
  }
}
class ShowListProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datas: [],
      isGetData: false,
    };
    this.refSilder = React.createRef();
  }

  componentDidMount() {
    const { typeSelection } = this.props;
    this.getDataProduct(typeSelection ? typeSelection.toLowerCase() : undefined);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { typeSelection } = nextProps;
    if (typeSelection && typeSelection !== prevState.typeSelection) {
      return { typeSelection, isGetData: true };
    }
    return null;
  }

  componentDidUpdate(prevProps, prevState) {
    const { isGetData, datas } = this.state;
    if (isGetData) {
      this.state.isGetData = false;
      if (this.refSilder && datas && datas.length > 0) {
        this.refSilder.current.slickGoTo(0);
      }
      setTimeout(() => {
        this.getDataProduct(this.props.typeSelection.toLowerCase());
      }, 500);
    }
  }

  fetchHolderCandle = async () => {
    const option = {
      url: GET_PRODUCT_TYPE_LINK.replace('{type}', 'holder'),
      method: 'GET',
    };
    return fetchClient(option);
  }

  getDataProduct = async (type) => {
    let holder = [];
    if (type === 'single_candle') {
      holder = await this.fetchHolderCandle();
    }
    const { dataProduct } = this.props;
    const datas = type === 'hand_sanitizer' ? dataProduct.handSanitizer
      : type === 'kit' ? dataProduct.kit
        : type === 'best_seller' ? dataProduct.bestSeller
          : type === 'home_scents' ? dataProduct.homeScents
            : type === 'single_candle' ? dataProduct.singleCandle : [];
    if (datas && datas.length > 0) {
      this.setState({ datas: datas.concat(holder) });
      return;
    }
    let resultData = holder;
    const option = {
      url: GET_PRODUCT_TYPE_LINK.replace('{type}', type),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        if (type === 'best_seller') {
          const perfumeP = _.filter(result, x => x.type.name === 'Perfume');
          const waxP = _.filter(result, x => x.type.name === 'Wax_Perfume');
          const otherP = _.filter(result, x => x.type.name !== 'Perfume' && x.type.name !== 'Wax_Perfume');
          this.setState({ datas: perfumeP.concat(waxP).concat(otherP) });
          _.assign(datas, perfumeP.concat(waxP).concat(otherP));
        } else {
          resultData = resultData.concat(result);
          this.setState({ datas: resultData });
          _.assign(datas, resultData);
          // if type == kit donwload more Bundle
          if (type === 'kit') {
            const optionBundle = {
              url: GET_PRODUCT_TYPE_LINK.replace('{type}', 'bundle'),
              method: 'GET',
            };
            fetchClient(optionBundle).then((bundle) => {
              if (bundle && !bundle.isError) {
                resultData = resultData.concat(_.orderBy(bundle, ['price'], ['desc']));
                this.setState({ datas: resultData });
                _.assign(datas, resultData);
              }
            });
          }
        }
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      toastrError(err.message);
    });
  }

  onClickBack = () => {
    const { typeSelection } = this.props;
    if (['best_seller', 'kit'].includes(typeSelection)) {
      this.props.history.push(generateUrlWeb('/all-products/explore_perfume'));
    } else if (['hand_sanitizer', 'single_candle', 'home_scents'].includes(typeSelection)) {
      this.props.history.push(generateUrlWeb('/all-products/explore_home'));
    } else {
      this.props.history.goBack();
    }
  }

  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      pauseOnHover: false,
      beforeChange: (current, next) => {
        console.log('current, next', current, next);
      },
    };
    const { datas } = this.state;
    const { typeSelection, buttonBlocks, titleSelection } = this.props;
    const numberProductPage = isMobile ? datas.length : datas.length;
    const dataFormat = [];
    for (let i = 0; i < datas.length; i += numberProductPage) {
      dataFormat.push(datas.slice(i, i + numberProductPage));
    }

    const handDesciption = getNameFromButtonBlock(buttonBlocks, 'handle_description');
    const kitDesciption = getNameFromButtonBlock(buttonBlocks, 'kit_description');
    const bestDesciption = getNameFromButtonBlock(buttonBlocks, 'best_seller_description');
    const homeScentsDesciption = getNameFromButtonBlock(buttonBlocks, 'home_scents_description');
    const candleDescription = getNameFromButtonBlock(buttonBlocks, 'candle_description');
    const candleSub = getNameFromButtonBlock(buttonBlocks, 'candle_sub');
    const desTitle = typeSelection ? (typeSelection.toLowerCase() === 'single_candle' ? candleDescription : typeSelection.toLowerCase() === 'home_scents' ? homeScentsDesciption : typeSelection.toLowerCase() === 'hand_sanitizer' ? handDesciption : typeSelection.toLowerCase() === 'kit' ? kitDesciption : typeSelection.toLowerCase() === 'best_seller' ? bestDesciption : '') : '';

    return (
      <div className="div-show-list-product animated faster fadeInUp">
        <div className="div-card-list-product">
          <button
            onClick={this.onClickBack}
            type="button"
            className="bt-back"
          >
            <img src={icBack} alt="back" />
            {isMobile ? '' : 'Back'}
          </button>
          <div className="div-header-list-product">
            <h3>
              {titleSelection}
            </h3>
            <div className="div-line" />
            {
              typeSelection && typeSelection.toLowerCase() === 'single_candle' && (
                <h4>
                  {candleSub}
                </h4>
              )
            }
            <span>
              {desTitle}
            </span>
          </div>
          <div className="div-body">
            {
              dataFormat.length > 0 ? (
                <Slider ref={this.refSilder} {...settings}>
                  {
                  _.map(dataFormat, array => (
                    <div className="div-silder-product">
                      <Row>
                        {
                          _.map(array, x => (
                            <Col md="4" xs="6" className="col-item-product">
                              <ItemProduct
                                product={x}
                                basket={this.props.basket}
                                addProductBasket={this.props.addProductBasket}
                                createBasketGuest={this.props.createBasketGuest}
                                loadingPage={this.props.loadingPage}
                                onClickIngredient={this.props.onClickIngredient}
                                onClickGotoProduct={this.props.onClickGotoProduct}
                                bottlePerfume={this.props.bottlePerfume}
                                buttonBlocks={buttonBlocks}
                              />
                            </Col>
                          ))
                        }
                      </Row>
                    </div>
                  ))
                }
                </Slider>
              ) : (<div />)
            }


          </div>
        </div>
      </div>
    );
  }
}

ShowListProduct.propTypes = {
  dataProduct: PropTypes.shape().isRequired,
  basket: PropTypes.shape().isRequired,
  addProductBasket: PropTypes.func.isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  onCloseListProduct: PropTypes.func.isRequired,
  loadingPage: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default ShowListProduct;
