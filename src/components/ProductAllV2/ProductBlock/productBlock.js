import React, { Component } from 'react';
import _ from 'lodash';
import { Row, Col } from 'reactstrap';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import PropTypes from 'prop-types';
import smoothscroll from 'smoothscroll-polyfill';
import '../../../styles/product_block_v2.scss';

import icSearchBlack from '../../../image/icon/search-product-black.svg';
import CreatePerfumeAdapt from './createPerfumeAdapt';
import {
  generateUrlWeb, getAltImageV2, getNameFromButtonBlock, scrollTop,
} from '../../../Redux/Helpers/index';
import {
  GET_ALL_SCENTS_PRODUCTS_HOME_SCENTS, GET_PRICES_PRODUCTS, GET_BOTTLE_PRODUCTS, GET_DROPDOWN_WAX_PERFUME, GET_DISCOVER_ITEM_URL, GET_ALL_SCENTS_PRODUCTS_PERFUME, GET_ALL_SCENTS_PRODUCTS_WAX, GET_ALL_SCENTS_PRODUCTS_CANDLE, GET_MASK_SANITIZER_ITEM_URL, GET_MASK_TOILET_DROPPER_ITEM_URL,
} from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import ShowListProduct from './showListProduct';
import icNext from '../../../image/icon/ic-next-w.svg';
import { isMobile } from '../../../DetectScreen';

class GlobalProduct extends Component {
  render() {
    const {
      name, image, onClickProduct, originalImage,
    } = this.props;
    return (
      <div className="global-item" onClick={onClickProduct}>
        <img loading="lazy" src={image} alt={getAltImageV2(originalImage)} />
        <button type="button">
          {name}
          <img src={icNext} alt="next" />
        </button>
      </div>
    );
  }
}
class LineProproduct extends Component {
  render() {
    const { datas, onClickProduct } = this.props;
    return (
      <div className={`div-line-product ${this.props.className}`}>
        {
          _.map(datas, x => (
            <div className="div-product-item" onClick={() => onClickProduct(x)}>
              <img loading="lazy" src={x.image.image} alt={getAltImageV2(x.image)} />
              <span>
                {x.image.caption}
              </span>
              <div className="line-scent" />
            </div>
          ))
        }
      </div>
    );
  }
}

class ProductItemMobile extends Component {
  render() {
    const { data, onClickProduct } = this.props;
    return (
      <div className="div-product-item-mobile">
        <div className="div-card-item" onClick={() => onClickProduct(data)}>
          <img loading="lazy" src={data.image.image} alt="scent" />
          <span>
            {data.image.caption}
          </span>
          <div className="line-scent" />
        </div>
      </div>
    );
  }
}
const preDataGroup = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { tags } = x;
    _.forEach(tags, (tag) => {
      const group = _.find(dataScents, d => d.tag === tag.name);
      if (group) {
        group.datas.push(x);
      } else {
        dataScents.push({
          tag: tag.name,
          group: tag.group,
          datas: [x],
        });
      }
    });
  });
  return dataScents;
};

const preDataAlphabet = (scents) => {
  const dataScents = [];
  _.forEach(scents, (x) => {
    const { name } = x;
    const firstChart = name.charAt(0);
    const group = _.find(dataScents, d => d.tag.toLowerCase() === firstChart.toLowerCase());
    if (group) {
      group.datas.push(x);
    } else {
      dataScents.push({
        tag: firstChart,
        datas: [x],
      });
    }
  });
  return dataScents;
};

const preDataScents = (scents) => {
  const dataScents = [];

  dataScents.push({
    datas: _.orderBy(preDataGroup(scents), ['tag'], ['asc']),
    title: ['Scent Family', 'Gender', 'mood'],
  });

  dataScents.push({
    datas: _.orderBy(preDataAlphabet(scents), ['tag'], ['asc']),
    title: ['Alphabet'],
  });

  dataScents.push({
    datas: [{
      tag: '',
      datas: _.orderBy(scents, ['popularity'], ['desc']),
    }],
    title: ['Popularity'],
  });
  return dataScents;
};

class ProductBlock extends Component {
  constructor(props) {
    super(props);
    const anchor = props.params && props.params.tags ? props.params.tags : undefined;
    this.typeSelection = anchor !== 'explore_home' && anchor !== 'explore_perfume' ? anchor : undefined;

    this.state = {
      anchor,
      isFirstSlide: true,
      isShowProductDetail: this.typeSelection === 'home_scents' || this.typeSelection === 'creation_perfume' || this.typeSelection === 'miniature_perfume' || this.typeSelection === 'dual_crayons' || this.typeSelection === 'solid_perfume' || this.typeSelection === 'dual_candles',
      isShowListProduct: this.typeSelection === 'hand_sanitizer' || this.typeSelection === 'kit' || this.typeSelection === 'best_seller' || this.typeSelection === 'single_candle',
      isShowGlobalProduct: !anchor,
      indexSelectProduct: anchor ? anchor === 'explore_home' || this.typeSelection === 'dual_candles' || this.typeSelection === 'single_candle' || this.typeSelection === 'home_scents' || this.typeSelection === 'hand_sanitizer' ? 1 : 0 : undefined, // 0 select Perfume, 1 selecte Home
      pricesPerfume: [],
      pricesSolid: [],
      pricesCandle: [],
      valueDropdownWax: [],
      bottlePerfume: {},
      dataProduct: {
        handSanitizer: [],
        kit: [],
        bestSeller: [],
      },
      isShowPopUpSearchMobile: false,
      isShowPopupFiltermobile: false,
      dataScentsWax: [],
      dataScentsPerfume: [],
      dataScentsHome: [],
    };

    this.valueFilters = [
      {
        title: 'Family',
        value: 'Scent Family',
      },
      {
        title: 'Popularity',
        value: 'Popularity',
      },
      {
        title: 'Alphabet (A-Z)',
        value: 'Alphabet',
      },
      {
        title: 'Mood',
        value: 'Mood',
      },
    ];
    this.refInput = React.createRef();
  }

  componentDidMount() {
    this.fetchAllScentWax();
    this.fetchAllScentCandle();
    this.fetchAllScent();
    this.fetchAllScentHome();
    const {
      scents, scentsWax, scentsCandle, scentsHome,
    } = this.props;
    const dataScentsPerfume = preDataScents(scents);
    const dataScentsWax = preDataScents(scentsWax);
    const dataScentsCandle = preDataScents(scentsCandle);
    const dataScentsHome = preDataScents(scentsHome);
    this.setState({
      dataScentsPerfume, dataScentsWax, dataScentsCandle, dataScentsHome,
    });
    if (this.typeSelection) {
      this.onClickProduct({ text: this.typeSelection });
    }
    if (this.state.indexSelectProduct !== undefined) {
      this.props.onChangeShowStep(this.state.indexSelectProduct === 1 ? 2 : 1);
    }
  }

  componentDidUpdate() {
    const { isChangeParams, typeSelection } = this.state;
    if (isChangeParams) {
      this.state.isChangeParams = false;
      this.typeSelection = typeSelection;
      if (this.typeSelection) {
        this.onClickProduct({ text: this.typeSelection });
      }
      if (this.state.indexSelectProduct !== undefined) {
        this.props.onChangeShowStep(this.state.indexSelectProduct === 1 ? 2 : 1);
      }
    }
  }


  static getDerivedStateFromProps(nextProps, prevState) {
    const returnObject = {};
    const {
      scents, scentsWax, scentsCandle, params, scentsHome,
    } = nextProps;
    if (scents !== prevState.scents) {
      const dataScentsPerfume = preDataScents(scents);
      _.assign(returnObject, { scents, dataScentsPerfume });
    }
    if (scentsWax !== prevState.dataScentsWax) {
      const dataScentsWax = preDataScents(scentsWax);
      _.assign(returnObject, { scentsWax, dataScentsWax });
    }
    if (scentsCandle !== prevState.scentsCandle) {
      const dataScentsCandle = preDataScents(scentsCandle);
      _.assign(returnObject, { scentsCandle, dataScentsCandle });
    }

    if (scentsHome !== prevState.scentsHome) {
      const dataScentsHome = preDataScents(scentsHome);
      _.assign(returnObject, { scentsHome, dataScentsHome });
    }

    const anchor = params && params.tags ? params.tags : undefined;
    if (anchor !== prevState.anchor) {
      const typeSelection = anchor !== 'explore_home' && anchor !== 'explore_perfume' ? anchor : undefined;

      // const typeSelection = params && params.tags ? params.tags : undefined;
      const isShowProductDetail = typeSelection === 'home_scents' || typeSelection === 'creation_perfume' || typeSelection === 'miniature_perfume' || typeSelection === 'dual_crayons' || typeSelection === 'solid_perfume' || typeSelection === 'dual_candles';
      const isShowListProduct = typeSelection === 'hand_sanitizer' || typeSelection === 'kit' || typeSelection === 'best_seller' || typeSelection === 'single_candle';
      const isShowGlobalProduct = !anchor;
      const indexSelectProduct = anchor ? anchor === 'explore_home' || typeSelection === 'dual_candles' || typeSelection === 'single_candle' || typeSelection === 'home_scents' || typeSelection === 'hand_sanitizer' ? 1 : 0 : undefined; // 0 select Perfume, 1 selecte Home

      _.assign(returnObject, {
        isShowGlobalProduct, isShowListProduct, isShowProductDetail, anchor, isChangeParams: true, typeSelection, indexSelectProduct,
      });
    }
    return !_.isEmpty(returnObject) ? returnObject : null;
  }

  // componentDidUpdate(prevProps, prevState) {
  //   const { anchor } = this.state;
  //   if (anchor && anchor !== prevState.anchor) {
  //     if (anchor === 'creation_perfume' || anchor === 'miniature_perfume' || anchor === 'dual_candles') {
  //       this.handleForPerfume();
  //     } else if (anchor === 'solid_perfume') {
  //       this.handleForSolid();
  //     }
  //   }
  // }

  fetchAllScentHome = () => {
    const { scentsHome } = this.props;
    if (scentsHome && scentsHome.length > 0) {
      return;
    }
    const option = {
      url: GET_ALL_SCENTS_PRODUCTS_HOME_SCENTS,
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      this.props.loadingPage(false);
      if (result && !result.isError) {
        this.props.addScentsHomeToStore(result);
        return;
      }
      throw new Error(result.message);
    }).catch((error) => {
      this.props.loadingPage(false);
      toastrError(error.message);
    });
  }

  fetchAllScentCandle = () => {
    const { scentsCandle } = this.props;
    if (scentsCandle && scentsCandle.length > 0) {
      return;
    }
    const option = {
      url: GET_ALL_SCENTS_PRODUCTS_CANDLE,
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      this.props.loadingPage(false);
      if (result && !result.isError) {
        this.props.addScentsCandleToStore(result);
        return;
      }
      throw new Error(result.message);
    }).catch((error) => {
      this.props.loadingPage(false);
      toastrError(error.message);
    });
  }

  fetchAllScentWax = () => {
    const { scentsWax } = this.props;
    if (scentsWax && scentsWax.length > 0) {
      return;
    }
    const option = {
      url: GET_ALL_SCENTS_PRODUCTS_WAX,
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      this.props.loadingPage(false);
      if (result && !result.isError) {
        this.props.addScentsWaxToStore(result);
        return;
      }
      throw new Error(result.message);
    }).catch((error) => {
      this.props.loadingPage(false);
      toastrError(error.message);
    });
  }

  fetchAllScent = () => {
    const { scents } = this.props;
    if (scents && scents.length > 0) {
      return;
    }
    const option = {
      url: GET_ALL_SCENTS_PRODUCTS_PERFUME,
      method: 'GET',
    };
    this.props.loadingPage(true);
    fetchClient(option).then((result) => {
      this.props.loadingPage(false);
      if (result && !result.isError) {
        this.props.addScentsToStore(result);
        return;
      }
      throw new Error(result.message);
    }).catch((error) => {
      this.props.loadingPage(false);
      toastrError(error.message);
    });
  }

  getPrice = async (type) => {
    try {
      const option = {
        url: GET_PRICES_PRODUCTS.replace('{type}', type),
        method: 'GET',
      };
      const result = await fetchClient(option);
      return result;
    } catch (error) {
      toastrError('error', error.message);
    }
  }

  getBottle = async () => {
    try {
      const option = {
        url: GET_BOTTLE_PRODUCTS,
        method: 'GET',
      };
      const result = await fetchClient(option);
      return result;
    } catch (error) {
      toastrError('error', error.message);
    }
  }

  changeSilde = (currentSlide) => {
    this.setState({ isFirstSlide: currentSlide === 0 });
  }

  srollToProduct = () => {
    const ele = document.getElementById('scroll-product');
    if (ele) {
      smoothscroll.polyfill();
      ele.scrollIntoView({ behavior: 'smooth' });
    }
  }

  handleForPerfume = () => {
    const pending = [this.getPrice('perfume'), this.getBottle(), this.getPrice('dual_candles'), this.getPrice('home_scents')];
    Promise.all(pending).then((results) => {
      this.setState({
        pricesPerfume: results[0], bottlePerfume: results[1][0], pricesCandle: results[2][0], pricesHomeScent: results[3][0],
      });
    }).catch((err) => {
      console.log('err', err);
    });
  }

  getDropDownWax = () => {
    const option = {
      url: GET_DROPDOWN_WAX_PERFUME,
      method: 'GET',
    };
    return fetchClient(option);
  }

  handleForSolid = () => {
    const pending = [this.getPrice('wax_perfume'), this.getDropDownWax()];
    Promise.all(pending).then((results) => {
      this.setState({ pricesSolid: _.orderBy(results[0], ['apply_to_sample'], ['asc']), valueDropdownWax: results[1].sort().reverse() });
    }).catch((err) => {
      console.log('err', err);
    });
  }

  fetchDiscoveryId = () => {
    const option = {
      url: GET_DISCOVER_ITEM_URL,
      method: 'GET',
    };
    return fetchClient(option);
  }

  fetchMaskSanitizerId = () => {
    const option = {
      url: GET_MASK_SANITIZER_ITEM_URL,
      method: 'GET',
    };
    return fetchClient(option);
  }

  fetchToiletDropperId = () => {
    const option = {
      url: GET_MASK_TOILET_DROPPER_ITEM_URL,
      method: 'GET',
    };
    return fetchClient(option);
  }

  onClickProduct = async (data) => {
    const { text } = data;
    if (text !== this.typeSelection) {
      scrollTop();
      // update description at page home
      this.props.changeDescription(text);
    }
    if (text === 'discovery_box') {
      const dataD = await this.fetchDiscoveryId();
      if (text !== this.typeSelection) {
        this.props.history.push(generateUrlWeb(`/product/discovery_box/${dataD[0].product}`));
      }
    } else if (text === 'toilet_dropper') {
      const dataD = await this.fetchToiletDropperId();
      if (text !== this.typeSelection) {
        this.props.history.push(generateUrlWeb(`/product/toilet_dropper/${dataD[0].product}`));
      }
    } else if (text === 'mask_sanitizer') {
      const dataD = await this.fetchMaskSanitizerId();
      if (text !== this.typeSelection) {
        this.props.history.push(generateUrlWeb(`/product/mask_sanitizer/${dataD[0].product}`));
      }
    } else if (text === 'single_candle' || text === 'creation_perfume' || text === 'miniature_perfume' || text === 'dual_crayons' || text === 'dual_candles' || text === 'home_scents' || text === 'hand_sanitizer' || text === 'mini_candles' || text === 'car_diffuser' || text === 'oil_burner') {
      this.handleForPerfume();
      if (text !== this.typeSelection) {
        // this.props.history.push(generateUrlWeb(`/all-products/${text}`));
        this.props.history.push(generateUrlWeb(`/product/${text}`));
      }

      // this.setState({ isShowProductDetail: true, isShowListProduct: false });
    } else if (text === 'solid_perfume') {
      this.handleForSolid();
      if (text !== this.typeSelection) {
        // this.props.history.push(generateUrlWeb('/all-products/solid_perfume'));
        this.props.history.push(generateUrlWeb('/product/wax'));
      }
      // this.setState({ isShowProductDetail: true, isShowListProduct: false });
    } else if (text === 'reed_diffuser') {
      if (text !== this.typeSelection) {
        // this.props.history.push(generateUrlWeb('/all-products/solid_perfume'));
        this.props.history.push(generateUrlWeb('/product/reed_diffuser'));
      }
      // this.setState({ isShowProductDetail: true, isShowListProduct: false });
    } else {
      this.getBottle().then((result) => {
        if (result && !result.isError) {
          this.setState({ bottlePerfume: result[0] });
        }
      });
      if (text !== this.typeSelection) {
        this.props.history.push(generateUrlWeb(`/all-products/${text}`));
      }
      // this.setState({ isShowProductDetail: false, isShowListProduct: true });
    }
    this.typeSelection = text;
  }

  onCloseProductDetail = () => {
    this.setState({ isShowProductDetail: false });
  }

  onCloseListProduct = () => {
    this.setState({ isShowListProduct: false });
  }

  onChangeFilter = (value) => {
    this.setState({ isShowPopupFiltermobile: value, isShowPopUpSearchMobile: false });
  }

  onChangeValueFilter = (value) => {
    this.refFilterSearchScent.current.onChangeValueSearchFilter({ value, type: 'Filter' });
    this.setState({ isShowPopupFiltermobile: false });
  }

  onChangeSearch = (value) => {
    this.setState({ isShowPopupFiltermobile: false, isShowPopUpSearchMobile: value });
    if (value) {
      setTimeout(() => {
        this.refInput.current.focus();
      }, 500);
    }
  }

  onChangeValueSearch = () => {
    this.refFilterSearchScent.current.onChangeValueSearchFilter({ value: this.valueInputSearch, type: 'Search' });
    this.setState({ isShowPopUpSearchMobile: false });
  }

  shareRefFilterSearchScent = (ref) => {
    this.refFilterSearchScent = ref;
  }

  onChangeInputSearch = (e) => {
    const { value } = e.target;
    this.valueInputSearch = value;
  }

  render() {
    const {
      isFirstSlide, isShowProductDetail, dataProduct,
      pricesPerfume, bottlePerfume, pricesSolid, pricesCandle,
      valueDropdownWax, isShowListProduct, isShowPopupFiltermobile, isShowPopUpSearchMobile,
      dataScentsPerfume, dataScentsWax, isShowGlobalProduct, indexSelectProduct, dataScentsCandle,
      dataScentsHome, isDualCandles, pricesHomeScent,
    } = this.state;
    const dataScents = (this.typeSelection === 'creation_perfume' || this.typeSelection === 'miniature_perfume' || this.typeSelection === 'dual_crayons') ? dataScentsPerfume : (this.typeSelection === 'dual_candles' || this.typeSelection === 'single_candle') ? dataScentsCandle : this.typeSelection === 'home_scents' ? dataScentsHome : dataScentsWax;
    const {
      data, buttonBlocks, headerText, cms, imageBlocks,
    } = this.props;
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      pauseOnHover: false,
      afterChange: (currentSlide) => {
        this.changeSilde(currentSlide);
      },
    };

    const dicoverBt = getNameFromButtonBlock(buttonBlocks, 'DISCOVER');
    const searchBt = getNameFromButtonBlock(buttonBlocks, 'SEARCH');
    const dataListDetail = _.filter(data, x => x.text !== this.typeSelection && (isMobile ? x.text !== 'best_seller' : true));

    const dataListPerfume = _.filter(data, x => x.link === 'perfume');
    const dataListDetailPerfume = _.filter(dataListPerfume, x => x.text !== this.typeSelection && (isMobile ? x.text !== 'best_seller' : true));
    const dataListHome = _.filter(data, x => x.link === 'home');
    const dataListDetailHome = _.filter(dataListHome, x => x.text !== this.typeSelection && (isMobile ? x.text !== 'best_seller' : true));
    const dataListDetailShow = indexSelectProduct === 0 ? dataListDetailPerfume : dataListDetailHome;

    const dataSelection = _.find(data, x => x.text === this.typeSelection);

    const headerBlock = (
      <div className="div-header animated faster fadeIn">
        <h1>
          {headerText}
        </h1>
        {/* <button className="bt-header" type="button" onClick={this.srollToProduct}>
          {dicoverBt}
        </button> */}
      </div>
    );
    const listProduct = (
      <div className="div-cart-product">
        <div className="div-scent">
          <div className="wrap-line-product">
            <LineProproduct datas={data.slice(0, 3)} onClickProduct={this.onClickProduct} />
          </div>
          <div className={data.slice(3, 6).length > 0 ? 'wrap-line-product' : 'hiden'}>
            <LineProproduct datas={data.slice(3, 6)} onClickProduct={this.onClickProduct} />
          </div>
          <div className={data.slice(6, 9).length > 0 ? 'wrap-line-product' : 'hidden'}>
            <LineProproduct datas={data.slice(6, 9)} onClickProduct={this.onClickProduct} />
          </div>
        </div>
      </div>
    );

    const listProductPerfume = (
      <div className="div-cart-product animated faster fadeInLeft">
        <div className="div-scent">
          <div className="wrap-line-product">
            <LineProproduct datas={dataListPerfume.slice(0, 3)} onClickProduct={this.onClickProduct} />
          </div>
          <div className={dataListPerfume.slice(3, 6).length > 0 ? 'wrap-line-product' : 'hiden'}>
            <LineProproduct datas={dataListPerfume.slice(3, 6)} onClickProduct={this.onClickProduct} />
          </div>
          <div className="line-v-1" />
          <div className="line-v-2" />
        </div>
      </div>
    );

    const listProductHome = (
      <div className="div-cart-product animated faster fadeInRight">
        <div className="div-scent">
          <div className="wrap-line-product">
            <LineProproduct datas={dataListHome.slice(0, 3)} onClickProduct={this.onClickProduct} />
          </div>
          <div className={dataListHome.slice(3, 6).length > 0 ? 'wrap-line-product' : 'hiden'}>
            <LineProproduct datas={dataListHome.slice(3, 6)} onClickProduct={this.onClickProduct} />
          </div>
          <div className="line-v-1" />
          <div className="line-v-2" />
        </div>
      </div>
    );

    const listProductDetail = (
      <div className={`div-cart-product-detail ${isFirstSlide ? 'hiddenPrev' : 'hiddenNext'}`}>
        <Slider {...settings}>
          <LineProproduct datas={dataListDetailShow.slice(0, 4)} className="detail" onClickProduct={this.onClickProduct} />
          {
            dataListDetailShow.length > 4 && (
              <LineProproduct datas={dataListDetailShow.slice(4, 6)} className="detail second-detail" onClickProduct={this.onClickProduct} />
            )
          }
        </Slider>
      </div>
    );
    const productGlobal = (
      <div className="global-product">
        {_.map(imageBlocks || [], (d, index) => (
          <GlobalProduct
            name={d.caption}
            image={d.image}
            originalImage={d}
            onClickProduct={() => {
              if (index === 0) {
                this.props.history.push(generateUrlWeb('/all-products/explore_perfume'));
              } else {
                this.props.history.push(generateUrlWeb('/all-products/explore_home'));
              }
              // if (isMobile) {
              //   scrollTop();
              // }
              // this.setState({ indexSelectProduct: index, isShowGlobalProduct: false }, () => {
              //   this.props.onChangeShowStep(index + 1);
              // });
            }}
          />
        ))}
      </div>
    );
    return (
      <div className="div-product-block-v2">
        <div className="div-background">
          <div className="div-image-bg">
            <img loading="lazy" src={this.props.imageBackground} alt="" />
            <div className="div-tranparent" />
          </div>
        </div>
        {/* can't use on Filter Search Scent */}
        <div
          className={isShowPopUpSearchMobile || isShowPopupFiltermobile ? 'div-popup-filter-search-mobile animated faster fadeIn' : 'hidden'}
        >
          <div className="bg-close" onClick={() => this.setState({ isShowPopUpSearchMobile: false, isShowPopupFiltermobile: false })} />
          <div className={isShowPopupFiltermobile ? 'div-popup-filter' : 'hidden'}>
            {
              _.map(this.valueFilters, d => (
                <button
                  type="button"
                  onClick={() => {
                    this.onChangeValueFilter(d.value);
                  }}
                >
                  {d.title}
                </button>
              ))
            }
          </div>
          <div className={isShowPopUpSearchMobile ? 'div-popup-search' : 'hidden'}>
            <div className="div-input">
              <img src={icSearchBlack} alt="search" />
              <input onChange={this.onChangeInputSearch} ref={this.refInput} type="text" />
            </div>
            <button type="button" onClick={this.onChangeValueSearch}>
              {searchBt}
            </button>
          </div>
        </div>

        <div className="div-data">
          {
            isShowProductDetail ? (
              <CreatePerfumeAdapt
                onCloseProductDetail={this.onCloseProductDetail}
                dataScents={dataScents}
                loadingPage={this.props.loadingPage}
                onClickIngredient={this.props.onClickIngredient}
                pricesPerfume={pricesPerfume}
                pricesSolid={pricesSolid}
                pricesCandle={pricesCandle}
                pricesHomeScent={pricesHomeScent}
                valueDropdownWax={valueDropdownWax}
                bottlePerfume={bottlePerfume}
                cms={this.props.cms}
                typeSelection={this.typeSelection}
                basket={this.props.basket}
                addProductBasket={this.props.addProductBasket}
                createBasketGuest={this.props.createBasketGuest}
                buttonBlocks={buttonBlocks}
                onChangeFilter={this.onChangeFilter}
                onChangeSearch={this.onChangeSearch}
                shareRefFilterSearchScent={this.shareRefFilterSearchScent}
                onClickGotoProduct={this.props.onClickGotoProduct}
                history={this.props.history}
              />
            ) : isShowListProduct ? (
              <ShowListProduct
                typeSelection={this.typeSelection}
                dataProduct={dataProduct}
                basket={this.props.basket}
                addProductBasket={this.props.addProductBasket}
                createBasketGuest={this.props.createBasketGuest}
                onCloseListProduct={this.onCloseListProduct}
                onClickIngredient={this.props.onClickIngredient}
                loadingPage={this.props.loadingPage}
                buttonBlocks={buttonBlocks}
                titleSelection={dataSelection ? dataSelection.image.caption : ''}
                onClickGotoProduct={this.props.onClickGotoProduct}
                bottlePerfume={bottlePerfume}
                history={this.props.history}
              />
            ) : (headerBlock)
          }
          {
            isMobile
              ? isShowGlobalProduct ? (
                productGlobal
              ) : (
                <React.Fragment>
                  <div className="div-product-mobile">
                    <div id="scroll-product" className="scroll-product" />
                    <Row>
                      {
                      _.map(isShowProductDetail || isShowListProduct ? (dataListDetailShow) : (indexSelectProduct === 0 ? dataListPerfume : dataListHome), (x, index) => (
                        <Col lg="6" xs={(isShowProductDetail || isShowListProduct) && dataListDetailShow.length % 2 !== 0 && index === dataListDetailShow.length - 1 ? '12' : '6'} className={(isShowProductDetail || isShowListProduct) && dataListDetailShow.length % 2 !== 0 && index === dataListDetailShow.length - 1 ? 'full' : (index % 2 === 0 ? 'start' : 'end')}>
                          <ProductItemMobile data={x} onClickProduct={this.onClickProduct} />
                        </Col>
                      ))
                    }
                    </Row>
                  </div>
                  {
                    !((isShowProductDetail || isShowListProduct)) && imageBlocks.length > 0 && (
                      <GlobalProduct
                        image={indexSelectProduct === 0 ? imageBlocks[1].image : imageBlocks[0].image}
                        name={indexSelectProduct === 0 ? imageBlocks[1].caption : imageBlocks[0].caption}
                        originalImage={indexSelectProduct === 0 ? imageBlocks[1] : imageBlocks[0]}
                        onClickProduct={() => {
                          if (isMobile) {
                            scrollTop();
                          }
                          this.setState({ indexSelectProduct: indexSelectProduct === 0 ? 1 : 0 }, () => {
                            this.props.onChangeShowStep(indexSelectProduct === 0 ? 2 : 1);
                          });
                        }}
                      />
                    )
                  }
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {
                  isShowGlobalProduct ? (
                    productGlobal
                  ) : (isShowProductDetail || isShowListProduct) ? (
                    <div className={`div-product ${data.slice(6, 9).length === 0 ? 'div-product-2line' : ''} ${isShowProductDetail || isShowListProduct ? 'detail' : ''}`}>
                      <div id="scroll-product" className="scroll-product" />
                      {
                        listProductDetail
                      }
                    </div>
                  ) : indexSelectProduct === 0 && imageBlocks.length > 0 ? (
                    <div className="div-list-product-v2">
                      {listProductPerfume}
                      <GlobalProduct
                        image={imageBlocks[1].image}
                        name={imageBlocks[1].caption}
                        originalImage={imageBlocks[1]}
                        onClickProduct={() => {
                          this.props.history.push(generateUrlWeb('/all-products/explore_home'));
                        }}
                      />
                    </div>
                  ) : indexSelectProduct === 1 && imageBlocks.length > 0 ? (
                    <div className="div-list-product-v2">
                      <GlobalProduct
                        image={imageBlocks[0].image}
                        name={imageBlocks[0].caption}
                        originalImage={imageBlocks[0]}
                        onClickProduct={() => {
                          this.props.history.push(generateUrlWeb('/all-products/explore_perfume'));
                        }}
                      />
                      {listProductHome}
                    </div>
                  ) : (<div />)
                }
                </React.Fragment>
              )
          }
        </div>
      </div>
    );
  }
}

ProductBlock.propTypes = {
  data: PropTypes.arrayOf().isRequired,
  scents: PropTypes.arrayOf().isRequired,
  scentsWax: PropTypes.arrayOf().isRequired,
  scentsHome: PropTypes.arrayOf().isRequired,
  loadingPage: PropTypes.func.isRequired,
  addScentsToStore: PropTypes.func.isRequired,
  addScentsWaxToStore: PropTypes.func.isRequired,
  addScentsHomeToStore: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  cms: PropTypes.shape().isRequired,
  basket: PropTypes.shape().isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  changeDescription: PropTypes.func.isRequired,
  history: PropTypes.func.isRequired,
  imageBlocks: PropTypes.arrayOf().isRequired,
};

export default ProductBlock;
