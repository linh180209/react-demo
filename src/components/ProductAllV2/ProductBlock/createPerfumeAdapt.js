import React, { Component } from 'react';
import _ from 'lodash';
import Slider from 'react-slick';
import PropTypes from 'prop-types';
import ReactCrop from 'react-image-crop';
import classnames from 'classnames';


import { Col, Row } from 'react-bootstrap';
import icProduct1 from '../../../image/icon/product1.svg';
import icProduct2 from '../../../image/icon/product2.svg';
import icCandleHolder from '../../../image/icon/ic-candle-holder.png';
import icCandle1 from '../../../image/icon/ic-candle-1.svg';
import icCandle2 from '../../../image/icon/ic-candle-2.svg';

import icCustome from '../../../image/icon/custome-product.svg';
import icDelete from '../../../image/icon/delete-product.svg';
import ProgressLine from '../../../views/ResultScentV2/progressLine';
import ProgressCircle from '../../../views/ResultScentV2/progressCircle';
import icNext from '../../../image/icon/next-product.svg';
import icBack from '../../../image/icon/back-product.svg';
import icClose from '../../../image/icon/close-product.svg';
import PickIngredient from './pickIngredient';
import AddCustomization from './addCustomization';
import {
  generaCurrency, generateUrlWeb, getCmsCommon, getNameFromButtonBlock,
} from '../../../Redux/Helpers';
import HtmlRight from './htmlRight';
import { GET_PRODUCT_FOR_CART, GET_SCENTED_CANDLES, GET_SCENTED_HOME } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { COLOR_CUSTOME } from '../../../constants';
import bottleCandle from '../../../image/bottleCandle.png';
import candle1 from '../../../image/candle1.png';
import candle2 from '../../../image/candle2.png';
import candle21 from '../../../image/candle2-1.png';
import candle3 from '../../../image/candle3.png';
import PremadeProduct from './premadeProduct';
import { isBrowser, isMobile } from '../../../DetectScreen';

class ItemPerfume extends Component {
  onClick = () => {
    const { data, title, index } = this.props;
    _.assign(data, { subTitle: title, index });
    if (this.props.onClick) {
      this.props.onClick(data);
    }
  }

  onClickDelete = (e) => {
    e.stopPropagation();
    this.props.onDeleteSelect();
  }

  render() {
    const {
      title, description, textButton, image, isSelected, data, textImage,
      isTypeChoose, isChoosed, isShowDelete, disabledButton,
    } = this.props;
    const sTextImage = textImage ? textImage.split(' ') : [];
    return (
      <div className="div-item-perfume-adapt" onClick={this.onClick}>
        {
          textImage ? (
            <div className="div-text-image">
              <span>{sTextImage.length > 0 ? sTextImage[0] : ''}</span>
              <span>{sTextImage.length > 1 ? sTextImage[1] : ''}</span>
            </div>
          ) : (
            <img loading="lazy" src={image} alt="scent" className={isSelected ? '' : 'icon'} />
          )
        }

        <div className="div-text-info">
          <div className="div-text">
            <h4>
              {title}
            </h4>
            <span>
              {description}
            </span>
          </div>
          {
            isShowDelete
              ? (
                <button type="button" className="bt-delete" onClick={this.onClickDelete}>
                  <img loading="lazy" src={icDelete} alt="delete" />
                  Delete
                </button>
              ) : (null)
          }
          {
            textButton ? (
              <button type="button" className={classnames('bt-text', (disabledButton ? 'disabled' : ''))} disabled={disabledButton}>
                {textButton}
              </button>
            ) : isTypeChoose ? (
              <div className={`div-circle-choose ${isChoosed ? 'active' : ''}`}>
                <div className="div-circle-big" />
                <div className="div-circle-small" />
              </div>
            ) : (
              <img loading="lazy" src={icNext} alt="next" />
            )
          }
        </div>
      </div>
    );
  }
}

class ChooseSize extends Component {
  constructor(props) {
    super(props);
    this.oldSize = this.props.dataProductSolid.size;
  }

  onClick = (data) => {
    const { size } = data;
    this.props.dataProductSolid.size = size;
    this.forceUpdate();
  }

  onClickApply = () => {
    this.props.onClickClose();
  }

  onClickCancel = () => {
    this.props.dataProductSolid.size = this.oldSize;
    this.props.onClickClose();
  }

  render() {
    const {
      buttonBlocks, valueDropdownWax, dataProductSolid, pricesSolid,
    } = this.props;
    const chooseSizeBt = getNameFromButtonBlock(buttonBlocks, 'Choose_a_Size');
    const solidPerfumeBt = getNameFromButtonBlock(buttonBlocks, 'solid_perfume_size');
    const cancelBt = getNameFromButtonBlock(buttonBlocks, 'CANCEL');
    const applyBt = getNameFromButtonBlock(buttonBlocks, 'APPLY');
    const { product } = dataProductSolid;
    const { items } = product;
    return (
      <div className="div-choose-size animated faster fadeInRight">
        <div className="div-header">
          <button type="button" onClick={this.onClickCancel}>
            <img loading="lazy" src={icBack} alt="back" />
          </button>
          <div className="div-text-header">
            <div className="line-1">
              <h3>
                {chooseSizeBt}
              </h3>
            </div>
            <span>
              {solidPerfumeBt}
            </span>
          </div>
        </div>

        <div className="div-body">
          {
            _.map(valueDropdownWax, (x, index) => (
              <ItemPerfume
                title={index === 0 ? `SOLID PERFUME - ${generaCurrency(items ? _.find(items, d => !d.is_sample).price : pricesSolid[index].price)}` : `MINI SOLID PERFUME - ${generaCurrency(items ? _.find(items, d => d.is_sample).price : pricesSolid[index].price)}`}
                description={index === 0 ? 'Normal Size' : 'Mini Size'}
                data={{ size: x }}
                index={index}
                textImage={x}
                isChoosed={x === dataProductSolid.size}
                isTypeChoose
                onClick={this.onClick}
              />
            ))
          }
        </div>

        <div className="div-button">
          <div className="text-left" />
          <div className="div-bt-right">
            <button className="bt-cancel" type="button" onClick={this.onClickCancel}>
              {cancelBt}
            </button>
            <button className="bt-apply" type="button" onClick={this.onClickApply}>
              {applyBt}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

class CreatePerfumeAdapt extends Component {
  constructor(props) {
    super(props);
    const {
      image, name, isBlack, isDisplayName,
    } = props.infoGift || {};
    this.state = {
      isShowSelectScent: false,
      dataScentSelection: undefined,
      isShowRollOn: props.typeSelection === 'miniature_perfume',
      isSolidPerfume: props.typeSelection === 'solid_perfume',
      isDualCandles: props.typeSelection === 'dual_candles',
      isShowHome: props.typeSelection === 'home_scents',
      imageDualCandles: bottleCandle,
      isShowCustome: false,
      isShowChooseSize: false,
      quantity: '1',
      dataProduct: {
        product: {},
        scents: [],
        custome: {
          currentImg: image || '',
          srcImage: '',
          nameBottle: isDisplayName ? name : '',
          isBlack: _.isNil(isBlack) ? false : isBlack,
          croppedImageUrlDone: '',
        },
      },
      dataProductRollon: {
        product: {},
        scents: [],
        custome: {
          currentImg: '',
          srcImage: '',
          nameBottle: '',
          isBlack: false,
          croppedImageUrlDone: '',
        },
      },
      dataProductSolid: {
        product: {},
        scents: [],
        size: undefined,
        custome: {
          currentImg: '',
          srcImage: '',
          nameBottle: '',
          isBlack: false,
          croppedImageUrlDone: '',
        },
      },
      dataProductCandle: {
        product: {},
        scents: [],
        size: undefined,
        custome: {
          currentImg: '',
          srcImage: '',
          nameBottle: '',
          isBlack: false,
          croppedImageUrlDone: '',
        },
      },
      dataProductHome: {
        product: {},
        scents: [],
        custome: {
          currentImg: '',
          srcImage: '',
          nameBottle: '',
          isBlack: false,
          croppedImageUrlDone: '',
        },
      },
      scentedCandles: [],
      scentedHomes: [],
    };
    this.refPickIngredient = React.createRef();
    this.refAddCustomization = React.createRef();
  }

  componentDidMount() {
    if (this.state.isDualCandles) {
      this.fetchScentedCanldes();
    }
    if (this.state.isShowHome) {
      this.fetchScentedHomes();
    }
  }

  componentDidUpdate(prevProps) {
    const { typeSelection } = this.props;
    if (typeSelection && typeSelection !== prevProps.typeSelection) {
      if (this.state.isDualCandles) {
        this.fetchScentedCanldes();
      }
      if (this.state.isShowHome) {
        this.fetchScentedHomes();
      }
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { typeSelection } = nextProps;
    if (typeSelection !== prevState.typeSelection) {
      _.assign(objectReturn, {
        typeSelection,
        isShowRollOn: typeSelection === 'miniature_perfume',
        isSolidPerfume: typeSelection === 'solid_perfume',
        isDualCandles: typeSelection === 'dual_candles',
        isShowHome: typeSelection === 'home_scents',
        isShowSelectScent: false,
        isShowCustome: false,
      });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  onChangeValueSearchFilter = (data) => {
    this.refPickIngredient.current.onChangeValueSearchFilter(data);
  }

  onCloseDetail = () => {
    // this.props.onCloseProductDetail();
    if (['creation_perfume', 'miniature_perfume', 'solid_perfume'].includes(this.props.typeSelection)) {
      this.props.history.push(generateUrlWeb('/all-products/explore_perfume'));
    } else if (['dual_candles', 'home_scents'].includes(this.props.typeSelection)) {
      this.props.history.push(generateUrlWeb('/all-products/explore_home'));
    } else {
      this.props.history.goBack();
    }
  }

  onClickScent = (data) => {
    this.setState({
      isShowSelectScent: true, isShowCustome: false, isShowChooseSize: false, dataScentSelection: data,
    }, () => {
      if (this.state.isDualCandles && [0, 1, 2].includes(this.state.dataProductCandle.scents.length)) {
        this.onClickChangeImageCandle();
      }
    });
  }

  onClickCustome = (data) => {
    this.setState({ isShowCustome: true, isShowSelectScent: false, isShowChooseSize: false });
  }

  onClickChooseSize = () => {
    this.setState({ isShowChooseSize: true, isShowCustome: false, isShowSelectScent: false });
  }

  onClickClose = () => {
    this.setState({ isShowChooseSize: false, isShowCustome: false, isShowSelectScent: false }, () => {
      this.onClickChangeImageCandle(true);
    });
  }

  getProduct = async (scents, type) => {
    const option = {
      url: `${GET_PRODUCT_FOR_CART}?combo=${scents[0].id}${scents.length > 1 ? `,${scents[1].id}` : ''}&type=${type}`,
      method: 'GET',
    };
    return fetchClient(option);
  }

  getProductCart = async (scents, isShowRollOn, isSolidPerfume, isDualCandles, isShowHome) => {
    try {
      // const type = isDualCandles ? (scents.length > 1 ? 'dual_candles' : 'single_candle') : isSolidPerfume ? 'wax_perfume' : isShowHome ? 'home_scents' : (scents.length > 1 ? 'perfume' : isShowRollOn ? 'scent' : 'perfume');
      const type = isDualCandles ? (scents.length > 1 ? 'dual_candles' : 'single_candle') : isSolidPerfume ? 'wax_perfume' : isShowHome ? 'home_scents' : (scents.length > 1 ? 'perfume' : isShowRollOn ? 'perfume' : 'perfume');
      const product = await this.getProduct(scents, type);
      return product;
    } catch (error) {
      console.error(error.mesage);
    }
  }

  onClickChangeImageCandle = (isRemove, index) => {
    if (!this.state.isDualCandles) {
      return;
    }
    const { imageDualCandles, dataProductCandle, isShowSelectScent } = this.state;
    const { scents } = dataProductCandle;
    if (!isRemove) {
      if (scents.length === 0 && imageDualCandles === bottleCandle) {
        this.setState({ imageDualCandles: candle1 });
      } else if (scents.length === 0 && imageDualCandles === candle1) {
        this.setState({ imageDualCandles: candle2 });
      } else if (scents.length === 1 && imageDualCandles === candle2 && !scents[index]) {
        this.setState({ imageDualCandles: candle21 });
      } else if (scents.length === 1 && imageDualCandles === candle21 && !scents[index]) {
        this.setState({ imageDualCandles: candle3 });
      } else if (isShowSelectScent && scents.length === 2) {
        this.setState({ imageDualCandles: candle3 });
      } else if (!isShowSelectScent && scents.length === 2) {
        this.setState({ imageDualCandles: bottleCandle });
      }
    } else if (isRemove) {
      if (!isShowSelectScent && scents.length === 0) {
        this.setState({ imageDualCandles: bottleCandle });
      } else if (!isShowSelectScent && scents.length === 1) {
        this.setState({ imageDualCandles: candle2 });
      } else if (!isShowSelectScent && scents.length === 2) {
        this.setState({ imageDualCandles: bottleCandle });
      } else if (imageDualCandles === candle3) {
        this.setState({ imageDualCandles: candle2 });
      } else if (imageDualCandles === candle2) {
        this.setState({ imageDualCandles: candle1 });
      }
    }
  }

  onClickApply = async (data, index) => {
    const {
      isShowRollOn, dataProductRollon, dataProduct, isSolidPerfume, dataProductSolid, isDualCandles, dataProductCandle, isShowHome, dataProductHome,
    } = this.state;
    const { scents } = isShowHome ? dataProductHome : isDualCandles ? dataProductCandle : isSolidPerfume ? dataProductSolid : (isShowRollOn ? dataProductRollon : dataProduct);

    // case remove data
    if (_.isEmpty(data) && scents && scents.length > index) {
      scents.splice(index, 1);
      this.setState({ isShowSelectScent: false });
      return;
    }

    // case add data
    if (index < scents.length) {
      scents[index] = data;
    } else {
      scents.push(data);
    }
    const product = await this.getProductCart(scents, isShowRollOn, isSolidPerfume, isDualCandles, isShowHome);
    if (isSolidPerfume) {
      this.state.dataProductSolid.product = product;
    } else if (isShowRollOn) {
      this.state.dataProductRollon.product = product;
    } else if (isDualCandles) {
      this.state.dataProductCandle.product = product;
    } else if (isShowHome) {
      this.state.dataProductHome.product = product;
    } else {
      this.state.dataProduct.product = product;
    }
    this.setState({ isShowSelectScent: false }, () => {
      if (scents.length === 2) {
        this.onClickChangeImageCandle();
      }
    });
  }

  updateUI = () => {
    this.forceUpdate();
  }

  onChangeQuantity = (e) => {
    const { value } = e.target;
    this.setState({ quantity: value });
  }

  onClickChoiseGift = () => {
    const { isShowRollOn, isSolidPerfume, dataProductSolid } = this.state;
    const dataProduct = isSolidPerfume ? dataProductSolid : isShowRollOn ? this.state.dataProductRollon : this.state.dataProduct;
    const { product, custome, scents } = dataProduct;
    const { items } = product;
    const item = isSolidPerfume ? _.find(items, x => x.variant_values[0] === dataProductSolid.size) : _.find(items, x => x.is_sample === isShowRollOn);
    this.props.onClickChoiseGift(item, custome);
  }

  viewProductDetail = () => {
    const {
      isShowRollOn, isSolidPerfume, dataProductSolid, dataProductCandle, isDualCandles, isShowHome,
    } = this.state;
    const dataProduct = isDualCandles ? dataProductCandle : isSolidPerfume ? dataProductSolid : isShowRollOn ? this.state.dataProductRollon : isShowHome ? this.state.dataProductHome : this.state.dataProduct;
    const { product } = dataProduct;
    _.assign(product, {
      product: product.id,
      type: {
        alt_name: isDualCandles ? 'Dual Candles' : isSolidPerfume ? 'Solid Perfume' : isShowRollOn ? 'Scent' : isShowHome ? 'Home Scents' : 'Perfume',
        name: isDualCandles ? 'dual_candles' : isSolidPerfume ? 'Wax_Perfume' : isShowRollOn ? 'Scent' : isShowHome ? 'home_scents' : 'Perfume',
      },
    });
    if (this.props.onClickGotoProduct) {
      this.props.onClickGotoProduct(product);
    }
  }

  onClickAddtoCart = async () => {
    const { basket, createBasketGuest, addProductBasket } = this.props;
    const {
      isShowRollOn, isSolidPerfume, dataProductSolid, isDualCandles, dataProductCandle, isShowHome,
    } = this.state;
    const dataProduct = isDualCandles ? dataProductCandle : isSolidPerfume ? dataProductSolid : isShowRollOn ? this.state.dataProductRollon : isShowHome ? this.state.dataProductHome : this.state.dataProduct;
    const { product, custome, scents } = dataProduct;
    const { items } = product;
    const item = isShowHome ? items[0] : isDualCandles ? items[0] : isSolidPerfume ? _.find(items, x => x.variant_values[0] === dataProductSolid.size) : _.find(items, x => x.is_sample === isShowRollOn);
    const bottleImage = custome.currentImg
      ? await fetch(custome.currentImg).then(r => r.blob())
      : undefined;
    const data = {
      item: item.id,
      is_featured: item.is_featured,
      is_black: custome.isBlack,
      is_display_name: (!bottleImage && !!custome.nameBottle) || !!custome.nameBottle,
      is_customized: !!bottleImage || !!custome.imagePremade,
      name: custome.nameBottle ? custome.nameBottle : product.name,
      price: item.price,
      quantity: this.state.quantity,
      imagePremade: custome.imagePremade,
      color: custome.isBlack ? COLOR_CUSTOME.BLACK : COLOR_CUSTOME.WHITE,
      file: bottleImage
        ? new File([bottleImage], 'product.png')
        : undefined,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    if (!basket.id) {
      createBasketGuest(dataTemp);
    } else {
      addProductBasket(dataTemp);
    }
  }

  onSwitchSolid = (index) => {
    const { dataProductSolid } = this.state;
    if (index === 0) {
      dataProductSolid.size = this.props.valueDropdownWax[1];
    } else {
      dataProductSolid.size = this.props.valueDropdownWax[0];
    }
    this.setState({ dataProductSolid });
  }

  onSwitchRollOn = async () => {
    const {
      isShowRollOn, isSolidPerfume, dataProductRollon, dataProduct,
    } = this.state;
    if (this.state.isShowRollOn) {
      dataProduct.scents = _.cloneDeep(dataProductRollon.scents);
      const product = await this.getProductCart(dataProduct.scents, !isShowRollOn, isSolidPerfume);
      dataProduct.product = product;
    } else if (this.state.isSolidPerfume) {
      dataProductRollon.scents = _.cloneDeep(dataProduct.scents);
      const product = await this.getProductCart(dataProductRollon.scents, !isShowRollOn, isSolidPerfume);
      dataProductRollon.product = product;
    }
    this.setState({ isShowRollOn: !isShowRollOn });
  }

  onDeleteSelect = (scents, index) => {
    scents.splice(index, 1);
    this.forceUpdate();
    this.onClickChangeImageCandle(true);
  }

  onDeleteCustomeBottle = (custome) => {
    custome.currentImg = '';
    custome.srcImage = '';
    custome.nameBottle = '';
    custome.isBlack = false;
    custome.croppedImageUrlDone = '';
    this.forceUpdate();
  }

  setEditorRef = (editor) => {
    if (this.refAddCustomization && this.refAddCustomization.current) {
      this.refAddCustomization.current.setEditorRef(editor);
    }
  }

  fetchScentedCanldes = () => {
    const option = {
      url: GET_SCENTED_CANDLES,
      method: 'GET',
    };
    fetchClient(option).then((response) => {
      this.setState({ scentedCandles: response });
    });
  }

  fetchScentedHomes = () => {
    const option = {
      url: GET_SCENTED_HOME,
      method: 'GET',
    };
    fetchClient(option).then((response) => {
      this.setState({ scentedHomes: response });
    });
  }

  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      pauseOnHover: false,
      vertical: !isMobile,
      verticalSwiping: !isMobile,
    };
    const {
      isShowSelectScent, dataScentSelection, dataProduct, dataProductCandle,
      dataProductRollon, dataProductSolid, isShowRollOn, isShowCustome, quantity,
      typeSelection, isSolidPerfume, isShowChooseSize, isDualCandles, scentedCandles, isShowHome,
      dataProductHome, scentedHomes,
    } = this.state;

    const {
      pricesCandle, pricesPerfume, bottlePerfume, cms, buttonBlocks, isMmoGift, isHiddenBack,
      valueDropdownWax, pricesSolid, pricesHomeScent,
    } = this.props;

    const cmsCommon = getCmsCommon(cms);
    const createBt = getNameFromButtonBlock(buttonBlocks, 'Creation_Perfume');
    const solidPerfumeBt = getNameFromButtonBlock(buttonBlocks, 'Solid_Perfume');
    const miniaturePerfumeBt = getNameFromButtonBlock(buttonBlocks, 'Miniature_Perfume');
    const eauDeBt = getNameFromButtonBlock(buttonBlocks, 'eau_de_parfum_25ml');
    const addToBagBt = getNameFromButtonBlock(buttonBlocks, 'ADD_TO_BAG');
    const tryForBt = getNameFromButtonBlock(buttonBlocks, 'Try_a_sample_for');
    const getBottleBt = getNameFromButtonBlock(buttonBlocks, 'Get_25ml_bottle_for');
    const ingredient1 = getNameFromButtonBlock(buttonBlocks, '1ST_INGREDIENT');
    const ingredient2 = getNameFromButtonBlock(buttonBlocks, '2ND_INGREDIENT');
    const addBt = getNameFromButtonBlock(buttonBlocks, 'ADD');
    const freeBt = getNameFromButtonBlock(buttonBlocks, 'FREE');
    const optionalBt = getNameFromButtonBlock(buttonBlocks, 'Optional');
    const bottleCTBt = getNameFromButtonBlock(buttonBlocks, 'BOTTLE_CUSTOMIZED');
    const customeBT = getNameFromButtonBlock(buttonBlocks, 'CUSTOMIZE_BOTTLE');
    const soildPerfumeBt = getNameFromButtonBlock(buttonBlocks, 'SOLID_PERFUME');
    const miniSoildBt = getNameFromButtonBlock(buttonBlocks, 'MINI_SOLID_PERFUME');
    const normalSizeBt = getNameFromButtonBlock(buttonBlocks, 'Normal_Size');
    const miniSizeBt = getNameFromButtonBlock(buttonBlocks, 'Mini_Size');
    const try2mlBt = getNameFromButtonBlock(buttonBlocks, 'Try_a_2ml_mini_for');
    const try4mlBt = getNameFromButtonBlock(buttonBlocks, 'Try_a_4ml_for');
    const naturnalBt = getNameFromButtonBlock(buttonBlocks, 'natural_No_Alcohol_Perfume');
    const eaude5Bt = getNameFromButtonBlock(buttonBlocks, 'eau_de_parfum_5ml');
    const chooseIngredientBt = getNameFromButtonBlock(buttonBlocks, 'CHOOSE_INGREDIENT');
    const choosePerfumeBt = getNameFromButtonBlock(buttonBlocks, 'CHOOSE_PERFUME');
    const viewDetailBt = getNameFromButtonBlock(buttonBlocks, 'VIEW_DETAIL');

    const scentDualBt = getNameFromButtonBlock(buttonBlocks, 'Scented_Dual_Candle');
    const scentCandleBt = getNameFromButtonBlock(buttonBlocks, 'SCENTED_CANDLE');
    const demiLunesBt = getNameFromButtonBlock(buttonBlocks, 'Demi_Lunes_Half_Moons');
    const candleHolerBt = getNameFromButtonBlock(buttonBlocks, 'CANDLE_HOLDER');
    const includeBt = getNameFromButtonBlock(buttonBlocks, 'INCLUDED');
    const scentedCandleBt = getNameFromButtonBlock(buttonBlocks, 'scented_canldes');
    const homeScentCollectionBt = getNameFromButtonBlock(buttonBlocks, 'Home_Scent_Collection');
    const requiredBt = getNameFromButtonBlock(buttonBlocks, 'Required');
    const homeScentsBt = getNameFromButtonBlock(buttonBlocks, 'Home_Scent_Spray');
    const naturalHomeBt = getNameFromButtonBlock(buttonBlocks, '100%_Natural_Home_Scent');

    const newDataScents = isDualCandles ? _.cloneDeep(this.props.dataScents) : this.props.dataScents;
    const ele = _.find(newDataScents, x => x.title.includes('Popularity'));
    if (ele) {
      _.remove(ele.datas[0].datas, x => _.find(dataProductCandle.scents, d => d.id === x.id));
    }

    dataProductSolid.size = dataProductSolid.size || (valueDropdownWax.length > 0 ? valueDropdownWax[0] : '');
    const indexSolid = isSolidPerfume && valueDropdownWax.length > 0 ? (dataProductSolid.size === valueDropdownWax[0] ? 0 : 1) : 0;
    const titleSolid = isSolidPerfume && valueDropdownWax.length > 0
      ? (indexSolid === 0
        ? { title: soildPerfumeBt, sub: normalSizeBt }
        : { title: miniSoildBt, sub: miniSizeBt })
      : {};

    const { scents, custome } = isShowHome ? dataProductHome : isDualCandles ? dataProductCandle : isSolidPerfume ? dataProductSolid : (isShowRollOn ? dataProductRollon : dataProduct);
    const priceCurrent = isShowHome ? pricesHomeScent : isDualCandles ? pricesCandle : isSolidPerfume
      ? (indexSolid === 0 ? pricesSolid[0] : pricesSolid[1])
      : _.find(pricesPerfume, x => (isShowRollOn ? x.apply_to_sample : !x.apply_to_sample));
    const priceOption = isSolidPerfume
      ? (indexSolid === 0 ? pricesSolid[1] : pricesSolid[0])
      : _.find(pricesPerfume, x => (isShowRollOn ? !x.apply_to_sample : x.apply_to_sample));

    const textTrySolid = indexSolid === 0 ? try2mlBt : try4mlBt;
    const isEnableCart = (isDualCandles ? scents && scents.length > 1 : scents && scents.length > 0) && Number(this.state.quantity) !== 0;
    const isEnableChooseGift = dataProduct && dataProduct.scents && dataProduct.scents.length > 0;
    const optionCreate = (
      <div className="div-option-create animated faster fadeInLeft">
        <div className="div-header">
          <div className="line-1">
            <h3>
              {isSolidPerfume ? solidPerfumeBt : isShowRollOn ? miniaturePerfumeBt : isDualCandles ? scentDualBt : isShowHome ? homeScentsBt : createBt}
            </h3>
            <span className={isMmoGift ? 'hidden' : ''}>
              {generaCurrency(priceCurrent ? priceCurrent.price : '')}
            </span>
          </div>
          <div className="line-2">
            <span>
              {isSolidPerfume ? naturnalBt : isShowRollOn ? eaude5Bt : (isDualCandles ? (scents && scents.length > 0 ? scentCandleBt : demiLunesBt) : isShowHome ? naturalHomeBt : eauDeBt)}
            </span>
            <button
              type="button"
              className={isMmoGift || isMobile || isDualCandles || isShowHome ? 'hidden' : ''}
              onClick={() => (isSolidPerfume ? this.onSwitchSolid(indexSolid) : this.onSwitchRollOn())}
            >
              {`${isSolidPerfume ? textTrySolid : isShowRollOn ? getBottleBt : tryForBt} ${generaCurrency(priceOption ? priceOption.price : '')}`}
            </button>
          </div>

        </div>
        <div className="div-list-option">
          <ItemPerfume
            title={scents && scents.length > 0 ? scents[0].name : (isSolidPerfume ? chooseIngredientBt : ingredient1)}
            description={isDualCandles ? requiredBt : (scents && scents.length > 0 && !isSolidPerfume ? ingredient1 : '')}
            textButton={scents && scents.length > 0 ? undefined : addBt}
            image={scents && scents.length > 0 ? scents[0].image : isDualCandles ? icCandle1 : icProduct1}
            isSelected={scents && scents.length > 0}
            data={scents && scents.length > 0 ? scents[0] : {}}
            onClick={this.onClickScent}
            index={0}
            isShowDelete={scents && scents.length > 0}
            onDeleteSelect={() => this.onDeleteSelect(scents, 0)}
          />
          <ItemPerfume
            title={scents && scents.length > 1 ? scents[1].name : (isSolidPerfume ? titleSolid.title : ingredient2)}
            description={isDualCandles ? requiredBt : isSolidPerfume ? titleSolid.sub : (scents && scents.length > 1 ? `${ingredient2} (${optionalBt})` : `(${optionalBt})`)}
            textButton={scents && scents.length > 1 || isSolidPerfume ? undefined : addBt}
            image={scents && scents.length > 1 ? scents[1].image : isDualCandles ? icCandle2 : icProduct2}
            isSelected={scents && scents.length > 1}
            data={scents && scents.length > 1 ? scents[1] : {}}
            onClick={isSolidPerfume ? this.onClickChooseSize : this.onClickScent}
            index={1}
            textImage={isSolidPerfume ? dataProductSolid.size : ''}
            isShowDelete={scents && scents.length > 1}
            onDeleteSelect={() => this.onDeleteSelect(scents, 1)}
          />
          {
            !isShowRollOn && !isSolidPerfume && !isShowHome
              ? (
                <ItemPerfume
                  title={isDualCandles ? candleHolerBt : custome.currentImg ? bottleCTBt : customeBT}
                  description={isDualCandles ? includeBt : custome.nameBottle || freeBt}
                  textButton={isDualCandles ? freeBt : (custome.currentImg || custome.nameBottle ? undefined : addBt)}
                  image={isDualCandles ? icCandleHolder : (custome.currentImg || icCustome)}
                  onClick={isDualCandles ? undefined : this.onClickCustome}
                  isSelected={!!custome.currentImg}
                  index={2}
                  isShowDelete={!!custome.currentImg || !!custome.nameBottle}
                  onDeleteSelect={() => this.onDeleteCustomeBottle(custome)}
                  disabledButton={isDualCandles}
                />
              ) : (<div />)
          }

        </div>
        {
          isBrowser ? (
            isMmoGift ? (
              <div className="div-bottom-price-gift">
                <button
                  disabled={!isEnableChooseGift}
                  type="button"
                  className={isEnableChooseGift ? 'bt-bg-drak' : 'bt-bg-drak disabled'}
                  onClick={this.onClickChoiseGift}
                >
                  {choosePerfumeBt}
                </button>
              </div>
            ) : (
              <div className="div-bottom-price">
                {/* <div className="div-text">
                  <h3>
                    {generaCurrency(priceCurrent ? priceCurrent.price : '')}
                  </h3>
                  <button
                    type="button"
                    onClick={() => (isSolidPerfume ? this.onSwitchSolid(indexSolid) : this.onSwitchRollOn())}
                  >
                    {`${isSolidPerfume ? textTrySolid : isShowRollOn ? getBottleBt : tryForBt} ${generaCurrency(priceOption ? priceOption.price : '')}`}
                  </button>
                </div> */}

                <div className="div-button">
                  <input type="number" value={this.state.quantity} onChange={this.onChangeQuantity} />
                  <div>
                    <button
                      type="button"
                      className={`view-detail ${isEnableCart ? '' : 'disabled'}`}
                      disabled={!isEnableCart}
                      onClick={this.viewProductDetail}
                    >
                      {viewDetailBt}
                    </button>
                    <button
                      type="button"
                      className={isEnableCart ? '' : 'disable'}
                      disabled={!isEnableCart}
                      onClick={this.onClickAddtoCart}
                    >
                      {addToBagBt}
                    </button>
                  </div>

                </div>
              </div>
            )
          ) : (
            isMmoGift ? (
              <div className="div-bottom-price-gift">
                <button
                  disabled={!isEnableChooseGift}
                  type="button"
                  className={isEnableChooseGift ? 'bt-bg-drak' : 'bt-bg-drak disabled'}
                  onClick={this.onClickChoiseGift}
                >
                  {choosePerfumeBt}
                </button>
              </div>
            ) : (
              <div className="div-bottom-price-mobile">
                <div className="div-button">
                  <button
                    type="button"
                    className={isEnableCart ? 'view-detail' : 'view-detail disabled'}
                    disabled={!isEnableCart}
                    onClick={this.viewProductDetail}
                  >
                    {viewDetailBt}
                  </button>
                  <button
                    type="button"
                    className={isEnableCart ? 'add-cart' : 'add-cart disable'}
                    disabled={!isEnableCart}
                    onClick={this.onClickAddtoCart}
                  >
                    {addToBagBt}
                  </button>
                </div>
                <div className="div-quanity">
                  <button
                    type="button"
                    disabled={Number(quantity) < 2}
                    style={{ minWidth: '21px' }}
                    onClick={() => this.setState({ quantity: Number(quantity) - 1 })}
                  >
                    {Number(quantity) < 2 ? ' ' : '-'}
                  </button>
                  <span>
                    {quantity}
                  </span>
                  <button
                    type="button"
                    onClick={() => this.setState({ quantity: Number(quantity) + 1 })}
                  >
                    +
                  </button>
                </div>
                <button
                  type="button"
                  className={isDualCandles || isShowHome ? 'hidden' : ''}
                  onClick={() => (isSolidPerfume ? this.onSwitchSolid(indexSolid) : this.onSwitchRollOn())
              }
                >
                  {`${isSolidPerfume ? textTrySolid : isShowRollOn ? getBottleBt : tryForBt} ${generaCurrency(priceOption ? priceOption.price : '')}`}
                </button>
              </div>
            )
          )
        }
      </div>
    );

    const htmlLeft = (
      <div className="div-left">
        {
          isShowSelectScent ? (
            <PickIngredient
              ref={this.refPickIngredient}
              onClickCloseScent={this.onClickClose}
              dataSelection={dataScentSelection}
              dataScents={newDataScents}
              onClickApply={this.onClickApply}
              loadingPage={this.props.loadingPage}
              onClickIngredient={this.props.onClickIngredient}
              typeSelection={this.props.typeSelection}
              buttonBlocks={buttonBlocks}
              onChangeFilter={this.props.onChangeFilter}
              onChangeSearch={this.props.onChangeSearch}
              shareRefFilterSearchScent={this.props.shareRefFilterSearchScent}
              isDualCandles={isDualCandles}
              isShowHome={isShowHome}
              onClickChangeImageCandle={this.onClickChangeImageCandle}
            />
          ) : isShowCustome ? (
            <AddCustomization
              ref={this.refAddCustomization}
              onClickCloseCustome={this.onClickClose}
              bottlePerfume={bottlePerfume}
              dataProduct={dataProduct}
              updateUI={this.updateUI}
              isShowRollOn={isShowRollOn}
              buttonBlocks={buttonBlocks}
            />
          ) : isShowChooseSize ? (
            <ChooseSize
              onClickClose={this.onClickClose}
              buttonBlocks={buttonBlocks}
              valueDropdownWax={valueDropdownWax}
              dataProductSolid={dataProductSolid}
              pricesSolid={pricesSolid}
            />
          ) : (
            optionCreate
          )
        }

        {/* <PickIngredient /> */}
        {/* <AddCustomization /> */}
        {/* <ChooseSize buttonBlocks={buttonBlocks}/> */}
      </div>
    );

    const htmlRight = (
      <HtmlRight
        dataProductSolid={dataProductSolid}
        isSolidPerfume={isSolidPerfume}
        isShowRollOn={isShowRollOn}
        isDualCandles={isDualCandles}
        isShowHome={isShowHome}
        dataProductRollon={dataProductRollon}
        dataProductCandle={dataProductCandle}
        dataProductHome={dataProductHome}
        dataProduct={dataProduct}
        cmsCommon={cmsCommon}
        updateUI={this.updateUI}
        valueDropdownWax={valueDropdownWax}
        buttonBlocks={buttonBlocks}
        setEditorRef={this.setEditorRef}
        isShowCustome={isShowCustome}
        imageDualCandles={this.state.imageDualCandles}
      />
    );

    return (
      <div className={`div-create-perfume-adapt animated faster fadeInUp ${(isDualCandles || isShowHome) && isBrowser ? 'height-premade' : ''}`}>
        <div className="div-card ">
          <div className="div-wrap">
            <buttton className={isHiddenBack ? 'hidden' : 'bt-close'} onClick={this.onCloseDetail}>
              <img loading="lazy" src={icClose} alt="close" />
            </buttton>
            {
              isMobile ? (
                <React.Fragment>
                  {htmlRight}
                  {htmlLeft}
                </React.Fragment>

              ) : (
                <React.Fragment>
                  {htmlLeft}
                  {htmlRight}
                </React.Fragment>
              )
            }
          </div>
          {
            isDualCandles && (<PremadeProduct title={scentedCandleBt} data={scentedCandles} onClickGotoProduct={this.props.onClickGotoProduct} />)
          }
          {
            isShowHome && (<PremadeProduct title={homeScentCollectionBt} data={scentedHomes} onClickGotoProduct={this.props.onClickGotoProduct} />)
          }
        </div>
      </div>
    );
  }
}

CreatePerfumeAdapt.propTypes = {
  onCloseProductDetail: PropTypes.bool.isRequired,
  dataScents: PropTypes.arrayOf().isRequired,
  loadingPage: PropTypes.func.isRequired,
  onClickIngredient: PropTypes.func.isRequired,
  pricesPerfume: PropTypes.arrayOf().isRequired,
  bottlePerfume: PropTypes.shape().isRequired,
  pricesHomeScent: PropTypes.shape().isRequired,
  cms: PropTypes.shape().isRequired,
  basket: PropTypes.shape().isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  addProductBasket: PropTypes.func.isRequired,
  pricesSolid: PropTypes.arrayOf().isRequired,
  onClickGotoProduct: PropTypes.func.isRequired,
  history: PropTypes.shape().isRequired,
};

export default CreatePerfumeAdapt;
