import React, { Component } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';
import { Col, Row } from 'react-bootstrap';
import classnames from 'classnames';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import '../../../styles/pemade-product.scss';
import { isBrowser } from '../../../DetectScreen/detectIFrame';
import { generaCurrency } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';
import ItemProductPage from '../../../componentsv2/itemProductPage';
import HeaderCT from '../../../componentsv2/headerCT';

class PremadeProduct extends Component {
  componentDidMount() {
    if (this.props.fetchData && (!this.props.data || this.props.data.length === 0)) {
      this.props.fetchData();
    }
  }

  onClickAddToCart = (data) => {
    const {
      basket, createBasketGuest, addProductBasket,
    } = this.props;
    const { id, name, price } = data;
    const dataT = {
      item: id,
      name,
      price,
      quantity: 1,
      is_featured: data.is_featured,
    };
    const dataTemp = {
      idCart: basket.id,
      item: dataT,
    };
    if (!basket.id) {
      createBasketGuest(dataTemp);
    } else {
      addProductBasket(dataTemp);
    }
  };

  render() {
    const settings = {
      dots: this.props.isShowDots || isMobile,
      infinite: false,
      slidesToShow: isMobile ? 1 : 4,
      slidesToScroll: 1,
      pauseOnHover: false,
    };
    const { data, isProductDetailV2 } = this.props;
    const dataMobile = [];
    for (let i = 0; i < (data ? data.length : 0); i += 4) {
      dataMobile.push(data.slice(i, i + 4));
    }
    return (
      <div className={classnames('div-pemade-product div-col', isProductDetailV2 ? 'product-detail-v2' : '')}>
        <div className="header div-row">
          {
            isProductDetailV2 ? (
              <HeaderCT type={isMobile ? 'Heading-S' : 'Heading-L'}>
                {this.props.title}
              </HeaderCT>
            ) : (
              <h3>{this.props.title}</h3>
            )
          }
        </div>
        {
          isBrowser ? (
            <div className="slice-image w-100">
              <Slider {...settings}>
                {
                  _.map(data || [], d => (
                    isProductDetailV2 ? (
                      <ItemProductPage
                        className="product-base product-fillter"
                        icon={d?.type?.parent?.icon}
                        type={d?.type?.name}
                        name={d?.short_description}
                        scentName={d?.name}
                        price={generaCurrency(d?.price)}
                        image={d?.image}
                        imageBg={d.image_background}
                        imageScent1={d?.image_scent1}
                        imageScent2={d?.image_scent2}
                        nameInSide="add to cart"
                        onClick={() => this.onClickAddToCart(d)}
                        endIcon={<AddShoppingCartIcon />}
                        buttonSize={isMobile ? 'small' : undefined}
                        onClickImage={() => this.props.onClickGotoProduct(d)}
                      />
                    ) : (
                      <div className="items-scent w-100">
                        <div className="item-premade div-col">
                          <img loading="lazy" src={d?.image} alt="product" onClick={() => this.props.onClickGotoProduct(d)} />
                          <h4 className="header_3">{d?.short_description}</h4>
                          <span className="header_4">{d?.name}</span>
                          <span className="header_4 price">{generaCurrency(d?.price)}</span>
                        </div>
                      </div>
                    )
                  ))
                }
              </Slider>
            </div>
          ) : (
            <div className="slice-image w-100">
              <Slider {...settings}>
                {
                  _.map(dataMobile, dataM => (
                    <div className="list-mobile">
                      <Row className="w-100">
                        {
                          _.map(dataM, d => (
                            <Col md="3" xs="6">
                              {
                                isProductDetailV2 ? (
                                  <ItemProductPage
                                    className="product-base product-fillter"
                                    icon={d?.type?.parent?.icon}
                                    type={d?.type?.name}
                                    name={d?.short_description}
                                    scentName={d?.name}
                                    price={generaCurrency(d?.price)}
                                    image={d?.image}
                                    imageBg={d.image_background}
                                    imageScent1={d?.image_scent1}
                                    imageScent2={d?.image_scent2}
                                    nameInSide="add to cart"
                                    onClick={() => this.onClickAddToCart(d)}
                                    endIcon={<AddShoppingCartIcon />}
                                    buttonSize={isMobile ? 'small' : undefined}
                                    onClickImage={() => this.props.onClickGotoProduct(d)}
                                  />
                                ) : (
                                  <div className="item-premade div-col">
                                    <img loading="lazy" src={d.image} alt="product" onClick={() => this.props.onClickGotoProduct(d)} />
                                    <h4 className="header_3">{d.short_description}</h4>
                                    <span className="header_4">{d.name}</span>
                                    <span className="header_4 price">{generaCurrency(d.price)}</span>
                                  </div>
                                )
                              }
                            </Col>
                          ))
                        }
                      </Row>
                    </div>
                  ))
                }
              </Slider>
            </div>
          )
        }
      </div>
    );
  }
}


export default PremadeProduct;
