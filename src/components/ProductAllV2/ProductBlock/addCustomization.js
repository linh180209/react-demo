import React, { Component } from 'react';
import {
  Dropdown, DropdownMenu, DropdownToggle,
} from 'reactstrap';
import PropTypes from 'prop-types';
import loadImage from 'blueimp-load-image';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import _ from 'lodash';
import Dropzone from 'react-dropzone';
import './add-customization.scss';
import icBack from '../../../image/icon/back-product.svg';
import icDown from '../../../image/icon/ic-down.svg';
import icDelete from '../../../image/icon/icDeleteBottle.svg';
import icChecked from '../../../image/icon/checked-scent.svg';
import icUploadFile from '../../../image/icon/ic-upload-file.svg';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';
import { isMobile } from '../../../DetectScreen';

class SliderItemScent extends Component {
  render() {
    const { datas, custome, updateUI } = this.props;
    return (
      <div className="div-slider-item">
        {
          _.map(datas, x => (
            <button
              type="button"
              onClick={() => {
                if (custome.currentImg === x.image) {
                  custome.currentImg = undefined;
                  _.assign(custome, { imagePremade: undefined });
                } else {
                  custome.currentImg = x.image;
                  _.assign(custome, { imagePremade: x.id });
                }
                custome.srcImage = undefined;
                this.props.updateUI();
              }}
            >
              <div className="div-scent">
                <img loading="lazy" className={custome.currentImg === x.image ? 'scent selected' : 'scent'} src={x.thumb} alt="scent" />
                {
                  custome.currentImg === x.image
                    ? <img loading="lazy" className="ic-checked" src={icChecked} alt="checked" />
                    : (<div />)
                }
              </div>
            </button>
          ))
        }
      </div>
    );
  }
}

class AddCustomization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
    };
    this.oldCustome = _.cloneDeep(props.dataProduct.custome);
  }

  toggle = () => {
    const { dropdownOpen } = this.state;
    this.setState({
      dropdownOpen: !dropdownOpen,
    });
  }

  onChangeText = (e) => {
    const { value } = e.target;
    if (this.props.dataProduct.custome.nameBottle.length >= 15 && value.length >= 15) {
      return;
    }
    this.props.dataProduct.custome.nameBottle = value;
    this.props.updateUI();
  }

  onClickCancel = () => {
    this.props.dataProduct.custome.nameBottle = this.oldCustome.nameBottle;
    this.props.dataProduct.custome.isBlack = this.oldCustome.isBlack;
    this.props.dataProduct.custome.currentImg = this.oldCustome.currentImg;
    this.props.dataProduct.custome.srcImage = this.oldCustome.srcImage;
    _.assign(this.props.dataProduct.custome, { imagePremade: undefined });
    this.props.onClickCloseCustome();
  }

  onClickDelete = () => {
    this.props.dataProduct.custome.nameBottle = '';
    this.props.dataProduct.custome.isBlack = false;
    this.props.dataProduct.custome.currentImg = undefined;
    this.props.dataProduct.custome.srcImage = undefined;
    this.props.updateUI();
  }

  onClickApply = async () => {
    if (this.props.dataProduct.custome.srcImage) {
      const canvas = this.editor.getImage().toDataURL();
      fetch(canvas)
        .then(res => res.blob())
        .then((blob) => {
          this.props.dataProduct.custome.currentImg = window.URL.createObjectURL(blob);
          this.props.dataProduct.custome.srcImage = undefined;
          this.props.onClickCloseCustome();
        });
    } else {
      this.props.onClickCloseCustome();
    }
  }

  setEditorRef = (editor) => {
    this.editor = editor;
  }

  onSelectFile = (files) => {
    if (files && files.length > 0) {
      loadImage(
        files[0],
        (img) => {
          const base64data = img.toDataURL('image/png');
          this.props.dataProduct.custome.srcImage = base64data;
          this.props.dataProduct.custome.currentImg = undefined;
          this.props.dataProduct.custome.croppedImageUrlDone = undefined;
          this.props.updateUI();
        },
        { orientation: 1 },
      );
    }
  };

  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      pauseOnHover: false,
    };
    const {
      onClickCloseCustome, bottlePerfume, dataProduct, buttonBlocks,
    } = this.props;
    const addCustomeBt = getNameFromButtonBlock(buttonBlocks, 'Add_Customization');
    const designBottlebt = getNameFromButtonBlock(buttonBlocks, 'design_your_bottle');
    const addTextBt = getNameFromButtonBlock(buttonBlocks, 'Add_Text');
    const textColorBt = getNameFromButtonBlock(buttonBlocks, 'Text_Color');
    const blackBt = getNameFromButtonBlock(buttonBlocks, 'Black');
    const whiteBt = getNameFromButtonBlock(buttonBlocks, 'White');
    const enterAName = getNameFromButtonBlock(buttonBlocks, 'Enter_a_name');
    const preMakeBt = getNameFromButtonBlock(buttonBlocks, 'Pre_Made_Designs');
    const upLoadbt = getNameFromButtonBlock(buttonBlocks, 'Upload_Image');
    const drapBt = getNameFromButtonBlock(buttonBlocks, 'Drag_and_drop');
    const cancelBt = getNameFromButtonBlock(buttonBlocks, 'CANCEL');
    const applyBt = getNameFromButtonBlock(buttonBlocks, 'APPLY');
    const { custome } = dataProduct;
    this.imageSuggestion = _.filter(bottlePerfume ? bottlePerfume.images : [], x => x.type === 'suggestion');
    return (
      <div className="div-add-customization">
        <div className="div-header-customization">
          <button type="button" onClick={onClickCloseCustome}>
            <img loading="lazy" src={icBack} alt="back" />
          </button>
          <div className="div-text-header">
            <div className="line-1">
              <h3>
                {addCustomeBt}
              </h3>
            </div>
            <span>
              {designBottlebt}
            </span>
          </div>
        </div>
        <div className="div-scroll-customization">
          <div className="div-wrap-text-color">
            <div className="div-text-color">
              <div className="div-text animated fadeIn">
                <span>
                  {addTextBt}
                </span>
                <div className="div-input short mt-2">
                  <div className="div-input-text">
                    <input type="text" placeholder="My perfume" value={custome.nameBottle} onChange={this.onChangeText} />
                    <span>
                      {`${custome.nameBottle.length}/15`}
                    </span>
                  </div>
                </div>
              </div>
              <div className="div-color">
                <span>
                  {textColorBt}
                </span>
                <Dropdown
                  isOpen={this.state.dropdownOpen}
                  toggle={this.toggle}
                  className="dropdown-text-color mt-2"
                >
                  <DropdownToggle
                    tag="span"
                    onClick={this.toggle}
                    data-toggle="dropdown"
                    aria-expanded={this.state.dropdownOpen}
                  >
                    <div className="div-row items-center justify-between items-center border-checkout" style={{ height: '56px' }}>
                      <div className="drop-down-item none-border">
                        <div className={`div-square ${custome.isBlack ? 'black' : ''}`} />
                        <span className="ml-3">
                          {custome.isBlack ? 'Black' : 'White'}
                        </span>
                      </div>
                      <img loading="lazy" src={icDown} alt="icDown" />
                    </div>

                  </DropdownToggle>
                  <DropdownMenu>

                    <div
                      onClick={() => {
                        custome.isBlack = true;
                        this.props.updateUI();
                        this.setState({ dropdownOpen: false });
                      }}
                    >
                      <div
                        className="drop-down-item"
                      >
                        <div className="div-square black" />
                        <span className="ml-3">
                          {/* {`${d.name}`} */}
                          {blackBt}
                        </span>
                      </div>
                    </div>
                    <div
                      onClick={() => {
                        custome.isBlack = false;
                        this.props.updateUI();
                        this.setState({ dropdownOpen: false });
                      }}
                    >
                      <div className="drop-down-item">
                        <div className="div-square" />
                        <span className="ml-3">
                          {/* {`${d.name}`} */}
                          {whiteBt}
                        </span>
                      </div>
                    </div>
                  </DropdownMenu>
                </Dropdown>
              </div>
            </div>
            <span className="sp-text">{enterAName}</span>
          </div>

          <div className="div-pre-make-design">
            <span>{preMakeBt}</span>
            <div className="div-slider-scent">
              <Slider {...settings}>
                {
                  _.map(_.range(Math.round(this.imageSuggestion.length / 15)), (x, index) => (
                    <SliderItemScent datas={this.imageSuggestion.slice(index * 15, index * 15 + 15)} custome={custome} updateUI={this.props.updateUI} />
                  ))
                }
              </Slider>
            </div>
          </div>

          <div className="div-upload-image">
            <span>
              {upLoadbt}
            </span>
            <Dropzone ref={this.refCrop} onDrop={acceptedFiles => this.onSelectFile(acceptedFiles)} accept=".jpeg,.png,.jpg" multiple={false}>
              {({ getRootProps, getInputProps }) => (
                <section>
                  <div {...getRootProps()}>
                    <input {...getInputProps()} />
                    <img loading="lazy" src={icUploadFile} alt="uploadFile" />
                    <span>
                      {drapBt}
                    </span>
                  </div>
                </section>
              )}
            </Dropzone>
          </div>
        </div>

        <div className="div-button">
          <div className="text-left">
            <button type="button" className={isMobile ? 'hidden' : 'button-bg__none'} onClick={this.onClickDelete}>
              <img loading="lazy" src={icDelete} alt="icon" />
            </button>
          </div>
          <div className="div-bt-right">
            <button className="bt-cancel" type="button" onClick={this.onClickCancel}>
              {cancelBt}
            </button>
            <button className="bt-apply" type="button" onClick={this.onClickApply}>
              {applyBt}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

AddCustomization.propTypes = {
  bottlePerfume: PropTypes.shape().isRequired,
  typeSelection: PropTypes.string.isRequired,
};

export default AddCustomization;
