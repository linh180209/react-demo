import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactCrop from 'react-image-crop';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import AvatarEditor from 'react-avatar-editor';
import InputRange from 'react-input-range';
import { Range } from 'react-range';
import UpDirection from './upDirection';

import ProgressLine from '../../../views/ResultScentV2/progressLine';
import ProgressCircle from '../../../views/ResultScentV2/progressCircle';
import {
  generaCurrency, getCmsCommon, getNameFromCommon, getNameFromButtonBlock,
} from '../../../Redux/Helpers';
import icBottleNew from '../../../image/bottle-new.png';
import icBottle from '../../../image/img-bottle.png';
import bottleRollOn from '../../../image/bottle_roll-on.png';
import bottleHome from '../../../image/bottle_home.png';
import bottleCandle from '../../../image/bottleCandle.png';
import logoBlack from '../../../image/icon/logo-custome-black.svg';
import logoWhite from '../../../image/icon/logo-custome-white.svg';
import icCrop from '../../../image/icon/ic_crop.svg';
import icUndo from '../../../image/icon/ic_undo.svg';
import solidBottle from '../../../image/solid.png';
import solidBottleMin from '../../../image/solid_min.png';
import icNext from '../../../image/icon/arrow-down-product.svg';
import icDrop from '../../../image/icon/drop-select.svg';
import icDelete from '../../../image/icon/icDeleteBottle.svg';
import { isBrowser, isMobile } from '../../../DetectScreen';

const widthCrop = 70;
const heightCrop = 225;

class HtmlRight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      crop: {
        unit: 'px',
        width: widthCrop,
        height: heightCrop,
        aspect: widthCrop / heightCrop,
      },
      croppedImageUrl: '',
      scale: 1.2,
      valueText: 1.2,
      isShowLine: false,
    };
    this.slider = React.createRef();
  }

  getCroppedImg(image, crop, fileName) {
    const canvas = document.createElement('canvas');
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    canvas.width = crop.width * scaleX;
    canvas.height = crop.height * scaleY;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width * scaleX,
      crop.height * scaleY,
    );

    return new Promise((resolve, reject) => {
      canvas.toBlob((blob) => {
        if (!blob) {
          return;
        }
        blob.name = fileName;
        this.fileUrl = window.URL.createObjectURL(blob);
        resolve(this.fileUrl);
      }, 'image/png');
    });
  }

  onImageLoaded = (image) => {
    this.imageRef = image;
  };

  onCropComplete = (crop) => {
    this.makeClientCrop(crop);
  };

  onCropChange = (crop) => {
    this.setState({ crop });
  };

  onCropComplete = (crop) => {
    this.makeClientCrop(crop);
  };

  onClickCrop = () => {
    const { croppedImageUrl } = this.state;
    this.props.dataProduct.custome.currentImg = croppedImageUrl;
    this.props.dataProduct.custome.srcImage = undefined;
    this.setState({ croppedImageUrl: '' });
    this.props.updateUI();
  }

  onClickDelete = () => {
    this.props.dataProduct.custome.nameBottle = '';
    this.props.dataProduct.custome.isBlack = false;
    this.props.dataProduct.custome.currentImg = undefined;
    this.props.dataProduct.custome.srcImage = undefined;
    this.props.updateUI();
  }

  onCLickUndo = () => {
    this.props.dataProduct.custome.currentImg = undefined;
    this.props.dataProduct.custome.srcImage = undefined;
    this.setState({
      croppedImageUrl: undefined,
    });
  }

  onClickNextSlide = () => {
    this.slider.current.slickNext();
  }

  onChangeImage = () => {
    const { isShowLine } = this.state;
    if (isShowLine) {
      return;
    }
    this.setState({ isShowLine: true });
    setTimeout(() => {
      this.setState({ isShowLine: false });
    }, 3000);
  }

  onChangeScale = (e) => {
    const { value } = e.target;
    this.setState({ valueText: value });
    if (isNaN(value)) {
      return;
    }
    this.setState({ scale: Number(value).toFixed(1) });
  }

  async makeClientCrop(crop) {
    if (this.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await this.getCroppedImg(
        this.imageRef,
        crop,
        'newFile.jpeg',
      );
      this.setState({ croppedImageUrl });
    }
  }

  render() {
    const settings = {
      dots: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      pauseOnHover: false,
      vertical: !isMobile,
      verticalSwiping: !isMobile,
    };
    const {
      isShowRollOn, dataProductRollon, dataProduct, cmsCommon, dataProductSolid, isSolidPerfume, valueDropdownWax, dataProductCandle,
      buttonBlocks, isShowCustome, isDualCandles, imageDualCandles, isShowHome, dataProductHome,
    } = this.props;
    const { crop, croppedImageUrl, isShowLine } = this.state;
    const { scents, custome, product } = isShowHome ? dataProductHome : isDualCandles ? dataProductCandle : isSolidPerfume ? dataProductSolid : (isShowRollOn ? dataProductRollon : dataProduct);
    const { profile } = product || {};
    const nameText = getNameFromCommon(cmsCommon, 'Name');
    const eauBt = getNameFromCommon(cmsCommon, 'Eau_de_parfum');
    const mlBt = getNameFromCommon(cmsCommon, '25ml');
    const cropBt = getNameFromCommon(cmsCommon, 'Crop');
    const undoBt = getNameFromCommon(cmsCommon, 'Undo');
    const strengthBt = getNameFromCommon(cmsCommon, 'STRENGTH');
    const durationBt = getNameFromCommon(cmsCommon, 'DURATION');
    const scentProfileBt = getNameFromButtonBlock(buttonBlocks, 'SCENT_PROFILE');
    const scrollToBt = getNameFromButtonBlock(buttonBlocks, 'scroll_to_view_characteristics');
    const scrollPerfumeBt = getNameFromButtonBlock(buttonBlocks, 'scroll_to_see_perfume_characteristics');
    const imageSolid = isSolidPerfume && valueDropdownWax.length > 0 && dataProductSolid.size === valueDropdownWax[0] ? solidBottle : solidBottleMin;
    const isShowText = !custome.currentImg && !custome.srcImage;
    const bottomCustome = (
      <div className="div-cutome-bottle">
        {/* {
          croppedImageUrl ? (
            <div className="div-button-image">
              <button type="button" onClick={this.onClickCrop}>
                <img src={icCrop} alt="crop" />
                {cropBt}
              </button>
              <button type="button" onClick={this.onCLickUndo}>
                <img src={icUndo} alt="undo" />
                {undoBt}
              </button>
            </div>
          ) : (<div />)
        } */}
        <div className={isShowCustome ? 'div-button-delete' : 'hidden'}>
          <button type="button" className="button-bg__none" onClick={this.onClickDelete}>
            <img src={icDelete} alt="icon" />
          </button>
        </div>
        <div className={custome.srcImage ? 'div-line-scale' : 'hidden'}>
          <span className="tx-title">
            SCALE
          </span>
          {
            isMobile ? (
              <UpDirection value={this.state.scale} onChange={value => this.setState({ scale: value.toFixed(1), valueText: value.toFixed(1) })} />
            ) : (
              <InputRange
                maxValue={3}
                minValue={1}
                step={0.1}
                value={this.state.scale}
                onChange={value => this.setState({ scale: value.toFixed(1), valueText: value.toFixed(1) })}
              />
            )
          }
          <input type="number" className={isMobile ? 'hidden' : 'input-scale'} value={this.state.valueText} onChange={this.onChangeScale} />
        </div>
        <div className="div-image-bottle">
          <div className="div-backgroud" />
          <img
            loading="lazy"
            src={isSolidPerfume ? imageSolid : (isShowRollOn ? bottleRollOn : isDualCandles ? imageDualCandles : isShowHome ? bottleHome : icBottleNew)}
            alt="icBottle"
            className={`div-bottle ${isShowText ? 'show-bottle' : 'hidden-bottle'} ${isShowHome ? 'bottle-home' : ''}`}
          />

          <img
            loading="lazy"
            src={icBottle}
            alt="icBottle"
            className={`div-bottle ${isShowText || isDualCandles ? 'hidden-bottle' : 'show-bottle'}`}
          />


          {
            isShowText && !isDualCandles
              ? (
                <React.Fragment>
                  <div className={`${isShowRollOn ? 'space-roll-on' : ''} ingredients-name ${custome.nameBottle ? 'ingredients-name-text' : ''} ${isSolidPerfume ? 'hidden' : ''}`}>
                    <input
                      type="text"
                      className="text-name"
                      disabled
                      value={custome.nameBottle}
                    />
                    <span>
                      {scents && scents.length > 0 ? scents[0].name : ''}
                    </span>
                    <span className={scents && scents.length > 1 ? '' : 'hidden'}>
                      X
                    </span>
                    <span>
                      {scents && scents.length > 1 ? scents[1].name : ''}
                    </span>
                  </div>
                </React.Fragment>
              ) : (
                <React.Fragment>
                  {
                    custome.srcImage && (
                      <div
                        id="select-image"
                        className="div-select-image-product-all"
                        style={{ borderRadius: 28 }}
                      >
                        <AvatarEditor
                          ref={this.props.setEditorRef}
                          image={custome.srcImage}
                          width={widthCrop}
                          height={heightCrop}
                          border={0}
                          borderRadius={28}
                          color={[255, 255, 255, 0.6]} // RGBA
                          scale={this.state.scale}
                          rotate={0}
                          onImageChange={this.onChangeImage}
                        />
                        <img src={icDrop} className={isShowLine ? 'hidden' : 'drop-suggestion animated faster fadeIn'} alt="drop" />
                        <div className={isShowLine ? 'div-ver left animated faster fadeIn' : 'hidden'} />
                        <div className={isShowLine ? 'div-ver right animated faster fadeIn' : 'hidden'} />
                        <div className={isShowLine ? 'div-hor top animated faster fadeIn' : 'hidden'} />
                        <div className={isShowLine ? 'div-hor bottom animated faster fadeIn' : 'hidden'} />
                      </div>
                    )
                  }
                  {
                    !isDualCandles && (
                      <React.Fragment>
                        <img
                          loading="lazy"
                          src={custome.currentImg}
                          alt="testName"
                          className={custome.currentImg ? 'div-name animated faster fadeIn' : 'hidden'}
                        />
                        <input
                          type="text"
                          className={`text-name animated faster fadeIn ${custome.isBlack ? 'black' : ''}`}
                          placeholder={nameText}
                          disabled
                          value={custome.nameBottle}
                        />
                        <img
                          loading="lazy"
                          src={custome.isBlack ? logoBlack : logoWhite}
                          alt=""
                          className="image-logo  animated faster fadeIn"
                        />
                        <span className="text-des-custome animated faster fadeIn" style={{ color: custome.isBlack ? '#000000' : '#ffffff' }}>
                          {eauBt}
                        </span>
                        <span className="text-size-custome animated faster fadeIn" style={{ color: custome.isBlack ? '#000000' : '#ffffff' }}>
                          {mlBt}
                        </span>
                      </React.Fragment>
                    )
                  }
                </React.Fragment>
              )
          }
        </div>
        <div className={profile && isBrowser && !custome.srcImage ? 'div-arrow-next' : 'hidden'} onClick={this.onClickNextSlide}>
          <span>
            {isDualCandles ? scrollToBt : scrollPerfumeBt}
          </span>
          <img src={icNext} alt="next" />
        </div>
      </div>
    );

    const scentProfile = (
      <div className="div-scent-profile">
        <h3>
          {scentProfileBt}
        </h3>
        <div className="div-process-line">
          {
            _.map(profile ? profile.accords : [], x => (
              <ProgressLine data={x} isShowPercent />
            ))
          }
        </div>
        <div className="div-process-circle">
          <ProgressCircle title={strengthBt} percent={profile ? parseInt(parseFloat(profile.strength) * 100, 10) : 0} />
          <ProgressCircle title={durationBt} percent={profile ? parseInt(parseFloat(profile.duration) * 100, 10) : 0} />
        </div>
      </div>
    );

    return (
      <div
        className="div-right"
      >
        {/* <div className={custome.srcImage ? 'div-react-crop' : 'hidden'}>
          <ReactCrop
            src={custome.srcImage}
            crop={crop}
            onImageLoaded={this.onImageLoaded}
            onComplete={this.onCropComplete}
            onChange={this.onCropChange}
            minWidth={widthCrop}
            minHeight={heightCrop}
          />
        </div> */}

        <Slider ref={this.slider} {...settings}>
          {bottomCustome}
          {profile ? scentProfile : null}
        </Slider>
      </div>
    );
  }
}

HtmlRight.defaultProps = {
  isDualCandles: false,
};

HtmlRight.propTypes = {
  isShowRollOn: PropTypes.bool.isRequired,
  isDualCandles: PropTypes.bool,
};

export default HtmlRight;
