import React, { Component } from 'react';
import _ from 'lodash';
import classnames from 'classnames';
import { outStock } from '../../../imagev2/png';
import icChecked from '../../../image/icon/checked-scent.svg';
import icInfo from '../../../image/icon/icNewInfo.svg';
import { getNameFromButtonBlock } from '../../../Redux/Helpers';

class ScentLine extends Component {
  onClickInfo = (data) => {
    const { onClickIngredient } = this.props;
    if (onClickIngredient) {
      if (data.ingredient?.id) {
        onClickIngredient({ id: data.ingredient?.id });
      } else {
        onClickIngredient({ id: data.ingredient });
      }
    }
  }

  render() {
    const {
      data, scentSelected, className, isNoInfo, buttonBlocks, isRecommended,
    } = this.props;
    const outStockBt = getNameFromButtonBlock(buttonBlocks, 'Out Of Stock');
    const lowStockBt = getNameFromButtonBlock(buttonBlocks, 'Low In Stock');
    return (
      <div className={classnames((data.datas && data.datas.length > 0 || isRecommended) ? 'div-scent-line' : 'hidden', (className))}>
        {
          !isRecommended && (
            <span>
              {data.tag}
            </span>
          )
        }
        <div className="list-scent-item">
          {
            _.map(isRecommended ? data : data.datas, x => (
              <button className={classnames(x.is_out_of_stock ? 'out-stock' : x.is_low_stock ? 'low-stock' : '', scentSelected?.id === x.id ? 'active' : '')} type="button" onClick={() => this.props.onClick(x)}>
                <div className={classnames('div-item')} data-message={x.is_out_of_stock ? outStockBt : x.is_low_stock ? lowStockBt : ''}>
                  <img src={x.image || _.find(x.images, d => d.type === 'main')?.image} alt="scent" className={scentSelected?.id === x.id ? 'chose' : ''} />
                  <div className={scentSelected?.id === x.id ? 'div-choose' : 'hidden'}>
                    <img src={icChecked} alt="checked" />
                  </div>
                  {
                    x.is_out_of_stock && (
                      <img className="icon-out-sock" src={outStock} alt="outStock" />
                    )
                  }
                  <div
                    className={isNoInfo ? 'hidden' : 'button-bg__none info'}
                    onClick={(e) => {
                      e.stopPropagation();
                      this.onClickInfo(x);
                    }}
                  >
                    <img src={icInfo} alt="info" className="w-100 h-100" />
                  </div>
                </div>

              </button>
            ))
          }
        </div>
      </div>
    );
  }
}

export default ScentLine;
