import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';

class TextBlock extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className="div-text-block">
        {ReactHtmlParser(data ? data.value : '')}
      </div>
    );
  }
}

export default TextBlock;
