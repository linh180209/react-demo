import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import ReactHtmlParser from 'react-html-parser';
import { generateUrlWeb } from '../../Redux/Helpers';

class BecomeBlock extends Component {
  render() {
    const { data } = this.props;
    return (
      <div className="div-become-block" style={{ background: `url(${data ? data.banner.image.image : ''})` }}>
        <div className="div-content">
          <h1>
            {data ? data.banner.text : ''}
          </h1>
          <span>
            {ReactHtmlParser(data ? data.banner.description : '')}
          </span>
          <Link to={data ? generateUrlWeb(data.button.link) : ''}>
            {data ? data.button.text : ''}
          </Link>
        </div>
      </div>
    );
  }
}

BecomeBlock.propTypes = {
  data: PropTypes.shape().isRequired,
};

export default BecomeBlock;
