import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactHtmlParser from 'react-html-parser';

class TextImageBlock extends Component {
  render() {
    const { isRight, data } = this.props;
    return (
      <div className={`div-text-image-block ${isRight ? 'end' : 'start'}`} style={{ backgroundImage: `url(${data ? data.image.image : ''})` }}>
        <div className="div-wrap-text">
          <div className="div-text">
            <h3>
              {data ? data.text : ''}
            </h3>
            <span>
              {ReactHtmlParser(data ? data.description : '')}
            </span>
          </div>
        </div>

      </div>
    );
  }
}

TextImageBlock.propTypes = {
  data: PropTypes.shape().isRequired,
};

export default TextImageBlock;
