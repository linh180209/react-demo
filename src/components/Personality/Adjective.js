import React, { Component, useState, useEffect } from 'react';
import '../../styles/result-new.scss';

const RenderItem = (props) => {
  const { item } = props;
  const [percent, setPercent] = useState(0);
  useEffect(() => {
    setTimeout(() => {
      setPercent(item.score);
    }, 1000);
  });
  return (
    <div className="__list-item">
      <div className="_list-item-content d-flex justify-content-start">
        <span className="__title">
          {item.name}
        </span>
      </div>
      <div className="_list-item-progress">
        <div
          className="_list-item-progress-percentage"
          style={{
            width: `${percent}%`,
            backgroundColor: `${item.color}`,
          }}
        >
          <span
            className="_list-item-progress-dot"
            style={{
              backgroundColor: `${item.color}`,
            }}
          />
        </div>
      </div>
    </div>
  );
};

class Adjective extends Component {
  render() {
    const {
      adjectives, scents, leftTitle, rightTitle,
    } = this.props;
    return (
      <div className="result-adjective w-100">
        <div className="row">
          <div className="col-6">
            <div className="__header d-flex justify-content-center">
              {leftTitle}
            </div>
            <div className="__list">
              {adjectives.map(item => <RenderItem item={item} />)}
            </div>
          </div>
          <div className="col-6">
            <div className="__header d-flex justify-content-center">
              {rightTitle}
            </div>
            <div className="__list">
              {scents.map(item => <RenderItem item={item} />)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Adjective;
