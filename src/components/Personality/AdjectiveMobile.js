import React, { Component, useEffect, useState } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import '../../styles/result-new.scss';

const sliderSettings = {
  dots: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
};

const RenderItem = (props) => {
  const { item } = props;
  const [percent, setPercent] = useState(0);
  useEffect(() => {
    setTimeout(() => {
      setPercent(item.score);
    }, 500);
  });
  return (
    <div className="__list-item">
      <div className="_list-item-content d-flex justify-content-start">
        <span className="__title">
          {item.name}
        </span>
      </div>
      <div className="_list-item-progress">
        <div
          className="_list-item-progress-percentage"
          style={{
            width: `${percent}%`,
            backgroundColor: `${item.color}`,
          }}
        >
          <span
            className="_list-item-progress-dot"
            style={{
              backgroundColor: `${item.color}`,
            }}
          />
        </div>
      </div>
    </div>
  );
};


class Adjective extends Component {
  render() {
    const {
      adjectives, scents, leftTitle, rightTitle,
    } = this.props;
    return (
      <div className="result-adjective">
        <div className="row">
          <div className="col-12">
            <Slider {...sliderSettings}>
              <div>
                <div className="__header d-flex justify-content-center">
                  {leftTitle}
                </div>
                <div className="__list">
                  {adjectives.map(item => <RenderItem item={item} />)}
                </div>
              </div>
              <div>
                <div className="__header d-flex justify-content-center">
                  {rightTitle}
                </div>
                <div className="__list">
                  {scents.map(item => <RenderItem item={item} />)}
                </div>
              </div>
            </Slider>
          </div>
        </div>
      </div>
    );
  }
}

export default Adjective;
