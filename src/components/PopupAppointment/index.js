import React, { useRef, useEffect } from 'react';
import { connect } from 'react-redux';
import { Modal, ModalBody } from 'reactstrap';
import _ from 'lodash';
import classnames from 'classnames';
import moment from 'moment';
import * as EmailValidator from 'email-validator';
import {
  getAltImageV2, getNameFromButtonBlock, addFontCustom, parseToUTC,
} from '../../Redux/Helpers';
import icClose from '../../image/icon/ic-close-region.svg';
import icDate from '../../image/icon/ic-date.svg';
import RadioButton from '../Input/radioButton';
import InputCT from '../Input/inputCT';
import './styles.scss';
import { useMergeState } from '../../Utils/customHooks';
import auth from '../../Redux/Helpers/auth';
import { CREATE_APPOINTMENTS } from '../../config';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import fetchClient from '../../Redux/Helpers/fetch-client';

function PopupAppointment(props) {
  const timeSlots = useRef(
    [{
      value: 8,
      label: '8AM',
    }, {
      value: 9,
      label: '9AM',
    }, {
      value: 10,
      label: '10AM',
    }, {
      value: 11,
      label: '11AM',
    }, {
      value: 12,
      label: '12PM',
    }, {
      value: 13,
      label: '1PM',
    }, {
      value: 14,
      label: '2PM',
    }, {
      value: 15,
      label: '3PM',
    }, {
      value: 16,
      label: '4PM',
    }, {
      value: 17,
      label: '5PM',
    }, {
      value: 18,
      label: '6PM',
    }, {
      value: 19,
      label: '7PM',
    }, {
      value: 20,
      label: '8PM',
    }, {
      value: 21,
      label: '9PM',
    }],
  );

  const [state, setState] = useMergeState({
    firstName: undefined,
    lastName: undefined,
    email: undefined,
    phone: undefined,
    gender: 1,
    country: 1,
    language: 'en',
    dob: undefined,
    message: undefined,
    dateRequested: new Date(),
    timeSlot: timeSlots.current[0].value,
    listCountry: [],
  });

  const onChange = (value, name) => {
    setState({ [name]: value });
  };

  const onChangeLanguage = (value, name) => {
    setState({ language: name });
  };

  const onClickSubmit = () => {
    const valueUTC = parseToUTC(moment(state.dateRequested).format('MM/DD/YYYY'), 'MM/DD/YYYY').startOf('day').valueOf() / 1000;
    const body = {
      first_name: state.firstName,
      last_name: state.lastName,
      email: state.email,
      phone: state.phone,
      gender: state.gender,
      country: state.country,
      language: state.language,
      date_of_birth: state.dob ? moment(state.dob, 'MM/DD/YYYY').valueOf() / 1000 : undefined,
      message: state.message,
      date_requested: parseInt(valueUTC, 10) + state.timeSlot * 60 * 60,
    };
    const option = {
      url: CREATE_APPOINTMENTS,
      method: 'POST',
      body,
    };
    fetchClient(option).then((result) => {
      props.onClose();
      if (result && !result.isError) {
        toastrSuccess('Submit');
        return;
      }
      throw new Error(result.message);
    }).catch((err) => {
      props.onClose();
      toastrError(err);
    });
  };

  useEffect(() => {
    const listCountry = _.map(props.countries, d => (
      {
        value: d.id,
        label: d.name,
        code: d.code,
      }
    ));
    const ele = _.find(listCountry, x => x.code === auth.getCountry());
    if (ele) {
      setState({ country: ele.value, listCountry });
    }
  }, [props.countries]);

  const bookaFreeBt = getNameFromButtonBlock(props.buttonBlocks, 'Book a Free Consultation');
  const fillBt = getNameFromButtonBlock(props.buttonBlocks, 'Fill in the details');
  const minuteBt = getNameFromButtonBlock(props.buttonBlocks, 'Minute call');
  const mrsBt = getNameFromButtonBlock(props.buttonBlocks, 'mrs');
  const mrBt = getNameFromButtonBlock(props.buttonBlocks, 'mr');
  const firstNameBt = getNameFromButtonBlock(props.buttonBlocks, 'First Name');
  const lastNameBt = getNameFromButtonBlock(props.buttonBlocks, 'Last Name');
  const birthdayBt = getNameFromButtonBlock(props.buttonBlocks, 'Birthday');
  const phoneBt = getNameFromButtonBlock(props.buttonBlocks, 'Phone Number');
  const emailBt = getNameFromButtonBlock(props.buttonBlocks, 'Email Address');
  const requiredBt = getNameFromButtonBlock(props.buttonBlocks, 'Required');
  const countryBt = getNameFromButtonBlock(props.buttonBlocks, 'Country');
  const dateBt = getNameFromButtonBlock(props.buttonBlocks, 'Date');
  const timeBt = getNameFromButtonBlock(props.buttonBlocks, 'Time Slot');
  const yourBt = getNameFromButtonBlock(props.buttonBlocks, 'Your prefered language');
  const englishBt = getNameFromButtonBlock(props.buttonBlocks, 'English');
  const frenchBt = getNameFromButtonBlock(props.buttonBlocks, 'French');
  const koreanBt = getNameFromButtonBlock(props.buttonBlocks, 'Korean');
  const vietnameBt = getNameFromButtonBlock(props.buttonBlocks, 'Vietnamese');
  const yourScentBt = getNameFromButtonBlock(props.buttonBlocks, 'Your scent requests');
  const submitBt = getNameFromButtonBlock(props.buttonBlocks, 'submit');
  return (
    <Modal className={classnames('modal-book-free-home', addFontCustom())} isOpen={props.isOpen} centered>
      <ModalBody>
        <div className="body-book">
          <button
            onClick={props.onClose}
            className="button-bg__none bt-close"
            type="button"
          >
            <img src={icClose} alt="close" />
          </button>
          <img src={props.imageBackground?.image} alt={getAltImageV2(props.imageBackground)} />
          <h2>{bookaFreeBt}</h2>
          <div className="des">
            <span>
              {fillBt}
            </span>
            <span>
              {minuteBt}
            </span>
          </div>

          <div className="gender-input">
            <RadioButton
              name="gender"
              label={mrBt}
              value={1}
              selected={state.gender === 1}
              onChange={onChange}
            />
            <RadioButton
              name="gender"
              label={mrsBt}
              className="ml-5"
              value={2}
              onChange={onChange}
              selected={state.gender === 2}
            />
          </div>
          <div className="line-input line-2">
            <InputCT
              type="text"
              name="firstName"
              placeholder={firstNameBt}
              onChange={onChange}
              value={state.firstName}
            />
            <InputCT
              type="text"
              name="lastName"
              placeholder={lastNameBt}
              onChange={onChange}
              value={state.lastName}
            />
          </div>
          <div className="line-input line-1">
            <InputCT
              type="INPUT_MASK"
              name="dob"
              placeholder={birthdayBt}
              mask="99/99/9999"
              iconRight={icDate}
              onChange={onChange}
              value={state.dob}
            />
          </div>
          <div className="line-input line-1">
            <InputCT
              type="text"
              name="phone"
              placeholder={phoneBt}
              textRight={requiredBt}
              onChange={onChange}
              value={state.phone}
            />
          </div>
          <div className="line-input line-1">
            <InputCT
              type="email"
              name="email"
              placeholder={emailBt}
              textRight={requiredBt}
              onChange={onChange}
              value={state.email}
            />
          </div>
          <div className="line-input line-1">
            <InputCT
              type="select"
              name="country"
              placeholder={countryBt}
              options={state.listCountry}
              onChange={onChange}
              value={state.country}
            />
          </div>
          <div className="line-input line-2">
            <InputCT
              type="Date"
              name="dateRequested"
              placeholder={dateBt}
              iconRight={icDate}
              onChange={onChange}
              value={state.dateRequested}
              minDate={new Date()}
            />
            <InputCT
              type="select"
              name="timeSlot"
              placeholder={timeBt}
              onChange={onChange}
              value={state.timeSlot?.value}
              options={timeSlots.current}
            />
          </div>
          <div className="preferd-language">
            <span>
              {yourBt}
            </span>
            <div className="group-choose">
              <InputCT
                type="checkbox"
                name="en"
                text={englishBt}
                id="english"
                checked={state.language === 'en'}
                onChange={onChangeLanguage}
              />
              <InputCT
                type="checkbox"
                name="fr"
                text={frenchBt}
                id="french"
                checked={state.language === 'fr'}
                onChange={onChangeLanguage}
              />
              <InputCT
                type="checkbox"
                name="kr"
                text={koreanBt}
                id="korean"
                checked={state.language === 'kr'}
                onChange={onChangeLanguage}
              />
              <InputCT
                type="checkbox"
                name="vn"
                text={vietnameBt}
                id="vietnamese"
                checked={state.language === 'vn'}
                onChange={onChangeLanguage}
              />
            </div>
          </div>
          <div className="line-input line-1 scent-request">
            <InputCT
              type="textarea"
              name="message"
              placeholder={yourScentBt}
              onChange={onChange}
              value={state.message}
            />
          </div>
          <div className={classnames('button-submit', !state.phone || !EmailValidator.validate(state.email) ? 'bt-disabled' : '')}>
            <button
              disabled={!state.phone || !EmailValidator.validate(state.email)}
              type="button"
              onClick={onClickSubmit}
            >
              {submitBt}
            </button>
          </div>

        </div>
      </ModalBody>
    </Modal>
  );
}

function mapStateToProps(state) {
  return {
    countries: state.countries,
  };
}

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(PopupAppointment);
