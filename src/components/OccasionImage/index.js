import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getAltImage } from '../../Redux/Helpers';

class OccasionImage extends Component {
  onClick = () => {
    console.log('On Click');
  }

  render() {
    const { url, type } = this.props.data;
    return (
      <div className="container--inline mt-4">
        <img loading="lazy" onClick={this.onClick} src={url} alt={getAltImage(url)} style={{ cursor: 'pointer' }} />
        <span className="text__overlay--bottom">
          {type}
        </span>
      </div>
    );
  }
}

OccasionImage.defaultProps = {
};

OccasionImage.propTypes = {
  data: PropTypes.shape({
    url: PropTypes.string,
    type: PropTypes.string,
  }).isRequired,
};

export default OccasionImage;
