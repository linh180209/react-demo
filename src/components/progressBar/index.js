import React from 'react';
import PropTypes from 'prop-types';

const Progressbar = ({ data, step, classCustom }) => (
  <div className={classCustom || 'progressbar'}>
    <ul style={{ marginLeft: '-50px' }}>
      {
        data.map(item => ((item.id < step) ? (
          <li className="active" style={{ width: `${100 / data.length}%` }} key={item.id}>
            {item.name}
          </li>
        )
          : (
            <li style={{ width: `${100 / data.length}%` }} key={item.id}>
              {item.name}
            </li>
          )))
      }
    </ul>
  </div>
);

Progressbar.defaultProps = {
  classCustom: '',
};

Progressbar.propTypes = {
  step: PropTypes.number.isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  classCustom: PropTypes.string,
};

export default Progressbar;
