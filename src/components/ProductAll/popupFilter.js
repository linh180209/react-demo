/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import backIcon from '../../image/icon/back-icon.svg';
import MenuFilterItem from '../MenuFilterItem';
import {
  getNameFromCommon, getCmsCommon,
} from '../../Redux/Helpers/index';
import '../../styles/product-all.scss';

class PopUpFilter extends Component {
  constructor(props) {
    super(props);
    this.oldSearch = JSON.parse(JSON.stringify(props.valueSearch));
  }

  componentDidMount = () => {
    document.addEventListener('touchmove', this.handleScroll);
  }

  handleScroll = (e) => {
    e.preventDefault();
  }

  componentWillUnmount = () => {
    // this.targetElement = document.getElementById('id-product');
    // enableBodyScroll(this.targetElement);
    if (window && this.handleScroll) {
      window.removeEventListener('touchmove', this.handleScroll);
    }
  }

  onClickClose = () => {
    if (this.props.onClickClose) {
      this.props.onClickClose();
      this.props.assignSearchFilter(this.oldSearch);
    }
  }

  resetFilter = () => {
    this.oldSearch.filter = [{
      title: 'famille',
      data: [],
    }];
    this.props.assignSearchFilter(this.oldSearch);
  }

  applyFilter = () => {
    if (this.props.onClickClose) {
      this.props.onClickClose();
    }
  }

  render() {
    const { dataFilter, valueSearch, cms } = this.props;
    const familles = _.find(dataFilter, x => x.title === 'famille');
    const filters = _.filter(dataFilter, x => x.title !== 'categories' && x.title !== 'famille');
    const cmsCommon = getCmsCommon(cms);
    const filterBt = getNameFromCommon(cmsCommon, 'FILTER');
    const resetBt = getNameFromCommon(cmsCommon, 'RESET');
    const applyBt = getNameFromCommon(cmsCommon, 'APPLY');
    return (
      <div id="id-popup-filter" className="popup-filter div-col">
        <div className="div-body">
          <div className="div-header div-col justify-center items-center">
            <button
              type="button"
              className="button-bg__none"
              onClick={this.onClickClose}
            >
              <div className="w-100 h-100 div-col justify-center items-start">
                <img loading="lazy" src={backIcon} alt="back-icon" />
              </div>
            </button>
            <span>
              {filterBt}
            </span>
          </div>
          <div className="div-line mt-3" />
          <div className="div-col div-filter" style={{ marginBottom: '170px' }}>
            <React.Fragment>
              {/* <MenuFilterItem isMobile datas={familles} removeSearchFilter={this.props.removeSearchFilter} addSearchFilter={this.props.addSearchFilter} valueSearch={valueSearch} /> */}
              {
              _.map(filters, d => (
                <MenuFilterItem isMobile datas={d} removeSearchFilter={this.props.removeSearchFilter} addSearchFilter={this.props.addSearchFilter} valueSearch={valueSearch} />
              ))
            }
            </React.Fragment>

          </div>
          <div className="div-button-apply div-row">
            <button
              type="button"
              className="bt-left"
              onClick={this.resetFilter}
            >
              {resetBt}
            </button>
            <button
              type="button"
              className="bt-right"
              onClick={this.applyFilter}
            >
              {applyBt}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

PopUpFilter.propTypes = {
  onClickClose: PropTypes.func.isRequired,
  removeSearchFilter: PropTypes.func.isRequired,
  addSearchFilter: PropTypes.func.isRequired,
  assignSearchFilter: PropTypes.func.isRequired,
  dataFilter: PropTypes.arrayOf(PropTypes.object).isRequired,
  valueSearch: PropTypes.arrayOf(PropTypes.object).isRequired,
  cms: PropTypes.shape().isRequired,
};

export default PopUpFilter;
