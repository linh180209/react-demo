import React, { Component } from 'react';
import _ from 'lodash';
import { Label, Input } from 'reactstrap';

class ItemsFilter extends Component {
  render() {
    return (
      <div className="div-col">
        <span>
          Feeling
        </span>
        {
          _.map(new Array(3), (d, index) => (
            <div className={`${index === 0 ? '' : 'mt-1'}`}>
              <Label check style={{ cursor: 'pointer' }} className="text-filter-active">
                <Input id={`checkbox${index + 1}`} type="checkbox" className="custom-input-filter__checkbox" />
                {' '}
                <Label
                  for={`checkbox${index + 1}`}
                  style={{ pointerEvents: 'none' }}
                />
                {' '}
                Check Me!
              </Label>
            </div>
          ))
        }
      </div>
    );
  }
}

export default ItemsFilter;
