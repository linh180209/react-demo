import React, { Component } from 'react';
import onClickOutside from 'react-onclickoutside';
import PropTypes from 'prop-types';
import _ from 'lodash';
import {
  getNameFromCommon, getCmsCommon,
} from '../../Redux/Helpers/index';
import '../../styles/product-all.scss';
import checkIcon from '../../image/icon/check-icon.svg';

class PopUpChoose extends Component {
  onChange = (name) => {
    if (this.props.onChange) {
      this.props.onChange(name);
    }
    this.props.onClickClosePopup();
  }

  handleClickOutside = () => {
    if (this.props.onClickClosePopup) {
      this.props.onClickClosePopup();
    }
  }


  render() {
    const { nameChoise, dataFilter, cms } = this.props;
    const cmsCommon = getCmsCommon(cms);
    const allBt = getNameFromCommon(cmsCommon, 'ALL');
    const categories = _.find(dataFilter, x => x.title === 'categories');
    return (
      <div className="div-popup-choose div-col justify-center items-center">
        <div className="div-content">
          <React.Fragment>
            {
                 _.map(categories.data, d => (
                   <button
                     type="button"
                     onClick={() => this.onChange(d.title)}
                   >
                     {d.title}
                     {' '}
                     <img loading="lazy" src={checkIcon} style={{ width: '16px' }} alt="check-icon" className={nameChoise === d.title ? '' : 'hidden'} />
                   </button>
                 ))
              }
            <button
              type="button"
              onClick={() => this.onChange(allBt)}
            >
              {allBt}
              {' '}
              <img loading="lazy" src={checkIcon} style={{ width: '16px' }} alt="check-icon" className={nameChoise === 'ALL' ? '' : 'hidden'} />
            </button>
          </React.Fragment>
        </div>
      </div>
    );
  }
}

PopUpChoose.propTypes = {
  nameChoise: PropTypes.string.isRequired,
  onClickClosePopup: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  dataFilter: PropTypes.arrayOf(PropTypes.object).isRequired,
  cms: PropTypes.shape().isRequired,
};

export default onClickOutside(PopUpChoose);
