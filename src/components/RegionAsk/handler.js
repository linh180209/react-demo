import _ from 'lodash';
import axios from 'axios';
import { GET_SHIPPING_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';

export const getShippingCountry = () => {
  const options = {
    url: GET_SHIPPING_URL,
    method: 'GET',
  };
  return fetchClient(options);
};

export const fetchCountryShipping = () => {
  const defaultHeaders = {
    'Content-Type': 'application/json',
    // 'accept-country': auth.getCountry(),
  };
  const options = {
    method: 'GET',
    url: GET_SHIPPING_URL,
    headers: defaultHeaders,
  };
  return axios(options);
};

export const formatCountry = (listCountry) => {
  const countries = [];
  _.forEach(listCountry, (x) => {
    countries.push(
      {
        label: x?.name,
        value: x?.code,
      },
    );
  });
  return countries;
};
