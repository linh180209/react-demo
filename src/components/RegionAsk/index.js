import React, { useState, useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import _ from 'lodash';
import './styles.scss';
import moment from 'moment';
import icClose from '../../image/icon/ic-close-video.svg';
import ButtonCT from '../ButtonCT';
import icDown from '../../image/icon/ic-down.svg';
import auth from '../../Redux/Helpers/auth';
import { useMergeState } from '../../Utils/customHooks';
import { changeUrl, getNameFromButtonBlock } from '../../Redux/Helpers';
import { fetchCountryShipping, formatCountry } from './handler';
import { toastrError } from '../../Redux/Helpers/notification';
// import PopupDelivery from '../../views/HomeAltV2/PopupDelivery';
// import PopupCountry from '../../views/HomeAltV2/PopupCountry';

function RegionAsk(props) {
  const [state, setState] = useMergeState({
    listCountry: formatCountry(props.countries) || [],
    nSymbol: _.find(formatCountry(props.countries) || [], x => x.value === auth.getCountryHeader()),
    isShowChangeCountry: false,
    isShowMessage: false,
    codeCountry: undefined,
    nSymbolCurrent: _.find(props.countries || [], x => x.code === auth.getCountry()),
  });

  const generateSelectInputComponents = () => {
    const inputComponents = {
      DropdownIndicator: selectProps => (
        <img style={{ marginBottom: '5px' }} src={icDown} alt="Arrow down icon" />
      ),
    };
    return inputComponents;
  };

  const onChange = (symbol) => {
    setState({ nSymbol: symbol });
    console.log('nSymbol', symbol);
  };

  const onClose = () => {
    props.updateShowAskRegion(false);
    auth.setAskRegion(moment().valueOf());
    setState({ isShowMessage: false });
  };

  const onClickContinute = () => {
    const ele = _.find(props.countries, x => x.code?.toLowerCase() === state.nSymbol.value?.toLowerCase());
    auth.setCountry(JSON.stringify(ele));
    auth.setAskRegion(false);
    props.updateShowAskRegion(false);
    changeUrl(state.nSymbol?.value);
    setState({ isShowMessage: false });
  };

  const fetchShipping = async () => {
    fetchCountryShipping().then((result) => {
      console.log('result shiping', result);
      const { headers, data } = result;
      setState({ listShipping: data, codeCountry: headers['accept-country'].toLowerCase() });
    }).catch((error) => {
      toastrError(error.message);
    });
    // try {
    //   const list = await getShippingCountry();
    //   setState({ listShipping: list });
    // } catch (error) {
    //   toastrError(error.message);
    // }
  };

  const handleShowPopup = () => {
    // const ele = _.find(state.listShipping, x => x?.country?.code === state.codeCountry);
    const ele = _.find(state.listShipping, x => x?.country?.code === auth.getCountry());
    if (!ele && auth.getDeliveryCountry()) {
      setState({ isShowChangeCountry: true });
    } else if (props.showAskRegion) {
      setState({ isShowMessage: true });
    }
  };

  useEffect(() => {
    const listCountry = formatCountry(props.countries) || [];
    const nSymbol = _.find(listCountry || [], x => x.value === auth.getCountryHeader());
    const nSymbolCurrent = _.find(props.countries, x => x.code === auth.getCountry());
    setState({
      listCountry,
      nSymbol,
      nSymbolCurrent,
    });
  }, [props.countries]);

  useEffect(() => {
    setTimeout(() => {
      fetchShipping();
    }, 1000);
  }, []);

  useEffect(() => {
    if (state.listShipping?.length > 0 && state.codeCountry) {
      handleShowPopup();
    }
  }, [state.listShipping, state.codeCountry]);

  const chooseBt = getNameFromButtonBlock(props.buttonBlocks, 'Choose another country');
  const continueBt = getNameFromButtonBlock(props.buttonBlocks, 'Continue');

  return (
    <div>
      {/* {state.isShowChangeCountry && <PopupDelivery listCountry={state.listCountry} buttonBlocks={props.buttonBlocks} country={state.nSymbolCurrent} codeCountry={state.nSymbolCurrent?.code || ''} />} */}
      {/* {state.isShowMessage && <PopupCountry onClose={onClose} nSymbol={state.nSymbol} buttonBlocks={props.buttonBlocks} onClickContinute={onClickContinute} />} */}
      {
        props.showAskRegion && !props.isNotShowRegion && (
          <div className="region-ask">
            <div className="text">
              {chooseBt}
            </div>
            <div className="content">
              <div className="dropdown-region">
                <Select
                  value={state.nSymbol}
                  onChange={onChange}
                  options={state.listCountry}
                  className="select-country"
                  components={generateSelectInputComponents()}
                />
              </div>
              <ButtonCT name={continueBt} className="bt-continute" onClick={onClickContinute} />
            </div>

            <button className="buton-bg__none bt-close" type="button" onClick={onClose}>
              <img src={icClose} alt="icon" />
            </button>
          </div>
        )
      }
    </div>
    // <div className="region-ask">
    //   <div className="text">
    //     {chooseBt}
    //   </div>
    //   <div className="content">
    //     <div className="dropdown-region">
    //       <Select
    //         value={state.nSymbol}
    //         onChange={onChange}
    //         options={state.listCountry}
    //         className="select-country"
    //         components={generateSelectInputComponents()}
    //       />
    //     </div>
    //     <ButtonCT name={continueBt} className="bt-continute" onClick={onClickContinute} />
    //   </div>

    //   <button className="buton-bg__none bt-close" type="button" onClick={onClose}>
    //     <img src={icClose} alt="icon" />
    //   </button>
    // </div>
  );
}

RegionAsk.defaultProps = {

};

RegionAsk.propTypes = {

};

export default RegionAsk;
