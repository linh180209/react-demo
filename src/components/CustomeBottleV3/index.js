/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import _, { set } from 'lodash';
import { Modal, ModalBody } from 'reactstrap';
import classnames from 'classnames';

import '../../styles/custome-bottle-v3.scss';
import icClose from '../../image/icon/ic-close-x.svg';
import icCloseMobile from '../../image/icon/ic-prev-silder.svg';
import icBack from '../../image/icon/icBack-custome.svg';
import MainPage from './mainPage';
import PageCustome from './pageCustome';
import addCmsRedux from '../../Redux/Actions/cms';
import {
  fetchCMSHomepage, hiddenChatIcon, addFontCustom, visibilityChatIcon,
} from '../../Redux/Helpers';
import { COLOR_CUSTOME, FONT_CUSTOME } from '../../constants';
import GuiUploadImage from './guiUploadImage';
import { isBrowser, isMobile } from '../../DetectScreen';

const getCms = (cms) => {
  if (cms) {
    const { body } = cms;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    return {
      buttonBlocks,
    };
  }
  return {
    buttonBlocks: [],
  };
};

const getDataCustome = (propsState) => {
  let name;
  let imageCustome;
  let color;
  let font;
  if (propsState) {
    const { custome } = propsState;
    if (custome) {
      name = custome.name;
      imageCustome = custome.image;
      color = custome.color;
      font = custome.font;
    }
  }
  return {
    name, imageCustome, color, font,
  };
};

class CustomeBottleV3 extends Component {
  constructor(props) {
    super(props);
    const {
      name, imageCustome, color, font,
    } = getDataCustome(
      props.propsState,
    );
    this.state = {
      arrayThumbs: [],
      buttonBlocks: [],
      uploadImage: '',
      isUploadImage: false,
      isShowGuiUpload: false,
      isShowGuiEdit: false,
      listFilterThumbs: [],
      customeBottle: {
        image: imageCustome || props.currentImg || '',
        name: name || props.nameBottle || '',
        color: color || props.color || COLOR_CUSTOME.BLACK,
        font: font || props.font || FONT_CUSTOME.JOST,
        imagePremade: undefined,
      },
      innerHeight: window.innerHeight,
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      currentImg, nameBottle, color, font,
    } = nextProps;
    if (currentImg !== prevState.currentImg || nameBottle !== prevState.nameBottle || color !== prevState.color || font !== prevState.font) {
      return {
        currentImg,
        nameBottle,
        color,
        font,
        customeBottle: {
          image: currentImg, name: nameBottle, color, font, imagePremade: undefined,
        },
      };
    }
    return null;
  }

  componentDidMount = () => {
    this.processThumbnail();
    this.fetchCMS();
    if (isMobile) {
      hiddenChatIcon();
    }
    window.addEventListener('resize', this.handleResize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.handleResize);
  }

  handleResize = () => {
    if (this.state.innerHeight - window.innerHeight > 100) {
      return;
    }
    this.setState({ innerHeight: window.innerHeight });
  }

  processThumbnail = () => {
    const datas = [];
    const listFilterThumbs = [];
    const { arrayThumbs } = this.props;
    _.forEach(arrayThumbs, (d) => {
      const ele = _.find(datas, x => x.category === d.category);
      if (!ele) {
        _.assign(d, { categoryCT: d.category });
        if (d.category) {
          listFilterThumbs.push(d.category);
        }
        datas.push({
          category: d.category,
          data: [d],
        });
      } else {
        ele.data.push(d);
      }
    });
    let dataSort = [];
    _.forEach(_.orderBy(datas, ['category'], ['asc']), (d) => {
      dataSort = dataSort.concat(d.data);
    });
    // listFilterThumbs.unshift('All');
    this.setState({ arrayThumbs: dataSort, listFilterThumbs });
  }

  componentWillUnmount = () => {
    if (isMobile) {
      visibilityChatIcon();
    }
  }

  fetchCMS = async () => {
    const { cms } = this.props;
    const customizeBottle = _.find(cms, x => x.title === 'Customize Bottle');
    if (!customizeBottle) {
      const cmsData = await fetchCMSHomepage('customize-bottle');
      this.props.addCmsRedux(cmsData);
      const dataCms = getCms(cmsData);
      this.setState(dataCms);
    } else {
      const dataCms = getCms(customizeBottle);
      this.setState(dataCms);
    }
  }

  onChangeCustomeBottle = (data) => {
    const { customeBottle } = this.state;
    _.assign(customeBottle, data);
    this.forceUpdate();
  }

  onCloseUploadImage = () => {
    this.setState({ isUploadImage: false });
  }

  onSetImageUpload = (image) => {
    this.setState({ uploadImage: image, isUploadImage: true });
  }

  onScreenUploadImage = (flag) => {
    this.setState({ isUploadImage: flag });
  }

  onChangeGuiUpload = (flag) => {
    this.setState({ isShowGuiUpload: flag });
  }

  onChangeGuiEdit = (flag) => {
    this.setState({ isShowGuiEdit: flag });
  }

  render() {
    const {
      customeBottle, uploadImage, isUploadImage, buttonBlocks,
      isShowGuiUpload, isShowGuiEdit, listFilterThumbs,
    } = this.state;
    const { name1, name2 } = this.props;
    const { arrayThumbs } = this.state;
    return (
      <Modal className={classnames('modal-full-screen', addFontCustom())} isOpen fade={false}>
        <ModalBody>
          <div className="div-custome-bottle-v3" style={isMobile ? { height: `${this.state.innerHeight}px` } : {}}>
            {
              isShowGuiUpload ? (<GuiUploadImage onChangeGui={this.onChangeGuiUpload} buttonBlocks={buttonBlocks} />) : null
            }
            {
              isShowGuiEdit ? (<GuiUploadImage isGuiEdit onChangeGui={this.onChangeGuiEdit} buttonBlocks={buttonBlocks} />) : null
            }
            <div className="div-content">
              <div className={isUploadImage ? 'div-sroll fix-height' : 'div-sroll'}>
                <img
                  data-gtmtracking={this.props.dataGtmtrackingClose}
                  loading="lazy"
                  className={classnames(isMobile ? 'bt-close bt-left' : 'bt-close', isMobile && !isUploadImage ? 'close-normal' : '')}
                  src={isUploadImage ? icBack : isBrowser ? icClose : icCloseMobile}
                  alt="bt-close"
                  onClick={isUploadImage ? this.onCloseUploadImage : this.props.closePopUp}
                />
                {
                  !isUploadImage ? (
                    <MainPage
                      listFilterThumbs={listFilterThumbs}
                      arrayThumbs={arrayThumbs}
                      onChangeCustomeBottle={this.onChangeCustomeBottle}
                      customeBottle={customeBottle}
                      onSetImageUpload={this.onSetImageUpload}
                      closePopUp={this.props.closePopUp}
                      onSave={() => this.props.onSave(customeBottle)}
                      buttonBlocks={buttonBlocks}
                      idCartItem={this.props.idCartItem}
                      onClickUpdatetoCart={this.props.onClickUpdatetoCart}
                      onScreenUploadImage={this.onScreenUploadImage}
                      onChangeGuiUpload={this.onChangeGuiUpload}
                      onChangeGuiEdit={this.onChangeGuiEdit}
                      name1={name1}
                      name2={name2}
                      dataGtmtracking={this.props.dataGtmtracking}
                    />
                  ) : (
                    <PageCustome
                      onChangeCustomeBottle={this.onChangeCustomeBottle}
                      onSetImageUpload={this.onSetImageUpload}
                      uploadImage={uploadImage}
                      customeBottle={customeBottle}
                      onCloseUploadImage={this.onCloseUploadImage}
                      buttonBlocks={buttonBlocks}
                      onChangeGuiEdit={this.onChangeGuiEdit}
                      name1={name1}
                      name2={name2}
                    />
                  )
                }
              </div>
            </div>
          </div>
        </ModalBody>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return {
    cms: state.cms,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
};


export default connect(mapStateToProps, mapDispatchToProps)(CustomeBottleV3);
