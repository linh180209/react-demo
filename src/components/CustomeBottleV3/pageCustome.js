import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import InputRange from 'react-input-range';
import 'react-input-range/lib/css/index.css';
import _ from 'lodash';
import loadImage from 'blueimp-load-image';
import { isIOS } from 'react-device-detect';
import FixedBottom from '../FixedBottom';
import BottleCustom from '../B2B/CardItem/bottleCustom';

import icDelete from '../../image/icon/ic-delete-white.svg';
import icUpload from '../../image/icon/ic-upload-white.svg';
import icScale from '../../image/icon/ic-scale-custome.svg';
import icText from '../../image/icon/ic-text-change.svg';
import icX from '../../image/icon/ic-x.svg';
import icChecked from '../../image/icon/ic-checked-custome.svg';
import icNoneSelect from '../../image/icon/none-select-custome.svg';
import icDefaultEdit from '../../image/icon/ic-bg-edit.svg';
import { COLOR_CUSTOME, FONT_CUSTOME } from '../../constants';
import { getNameFromButtonBlock } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

class PageCustome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadImage: props.uploadImage,
      step: 0,
      value: 1,
      scale: 1.2,
      valueText: 1.2,
      customeBottle: _.cloneDeep(props.customeBottle),
    };
    this.refCrop = React.createRef();
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateSize);
  }

  updateSize = () => {
    this.forceUpdate();
  }

  onSelectFile = (files) => {
    if (files && files.length > 0) {
      loadImage(
        files[0],
        (img) => {
          const base64data = img.toDataURL('image/png');
          this.setState({ uploadImage: base64data });
          if (this.isShowGui) {
            this.props.onChangeGuiEdit(true);
          }
          this.isShowGui = false;
        },
        { orientation: 1 },
      );
    }
  };

  onChangeColor = (color) => {
    const { customeBottle } = this.state;
    customeBottle.color = color;
    this.setState({ customeBottle });
  }

  onChangeText = (e) => {
    const { value } = e.target;
    if (value.length > 15) {
      return;
    }
    const { customeBottle } = this.state;
    customeBottle.name = value;
    this.setState({ customeBottle });
  }

  onChangeFont = (font) => {
    const { customeBottle } = this.state;
    customeBottle.font = font;
    this.setState({ customeBottle });
  }

  onChangeScale = (e) => {
    const { value } = e.target;
    this.setState({ valueText: value });
    if (isNaN(value)) {
      return;
    }
    this.setState({ scale: Number(value).toFixed(1) });
  }

  onClickDelete = () => {
    const { customeBottle } = this.state;
    customeBottle.image = '';
    customeBottle.imagePremade = undefined;
    customeBottle.font = FONT_CUSTOME.JOST;
    customeBottle.color = COLOR_CUSTOME.BLACK;
    this.setState({
      scale: 1.2, valueText: 1.2, uploadImage: '', customeBottle,
    });
  }

  onClickCancel = () => {
    const { step, scaleTemp, customeBottleTemp } = this.state;
    if (step === 2) {
      this.setState({ step: 0, scale: scaleTemp });
    } else if (step === 3) {
      this.setState({ step: 0, customeBottle: customeBottleTemp });
    }
  }

  onClickApply = () => {
    this.setState({ step: 0 });
  }

  onClickContinute = () => {
    const { customeBottle, uploadImage } = this.state;
    if (this.editor && uploadImage) {
      const canvas = this.editor.getImage().toDataURL();
      fetch(canvas)
        .then(res => res.blob())
        .then((blob) => {
          customeBottle.image = window.URL.createObjectURL(blob);
          this.props.onChangeCustomeBottle(customeBottle);
          this.props.onCloseUploadImage();
        });
    } else {
      this.props.onChangeCustomeBottle(customeBottle);
      this.props.onCloseUploadImage();
    }
  }

  onClickUploadImage = () => {
    if (this.refCrop && isMobile) {
      this.refCrop.current.open();
    }
  }

  setEditorRef = editor => this.editor = editor;

  onClickBottle = () => {
    this.isShowGui = true;
    this.onClickUploadImage();
  }

  onDropRejected = () => {
    console.log('onDropRejected');
  }

  render() {
    const { buttonBlocks, name1, name2 } = this.props;
    const listFont = [FONT_CUSTOME.JOST, FONT_CUSTOME.PLAYFAIR_DISPLAY, FONT_CUSTOME.DANCING_SCRIPT, FONT_CUSTOME.BALSAMIQ_SANS, FONT_CUSTOME.PERMANENT_MARKER, FONT_CUSTOME.NUNITO];
    const {
      customeBottle, step, uploadImage, scale,
    } = this.state;
    const { color, font, name } = customeBottle;
    const customizaBt = getNameFromButtonBlock(buttonBlocks, 'Customize_your');
    const uploadBt = getNameFromButtonBlock(buttonBlocks, 'Upload');
    const textBt = getNameFromButtonBlock(buttonBlocks, 'TEXT');
    const scaleBt = getNameFromButtonBlock(buttonBlocks, 'Scale');
    const continueBt = getNameFromButtonBlock(buttonBlocks, 'continue');
    const uploadPhotoBt = getNameFromButtonBlock(buttonBlocks, 'UPLOAD_PHOTO');
    const drapBt = getNameFromButtonBlock(buttonBlocks, 'Drag_photos_here');
    const orBt = getNameFromButtonBlock(buttonBlocks, 'or');
    const chooseBt = getNameFromButtonBlock(buttonBlocks, 'Choose_Files');
    const scalePhotoBt = getNameFromButtonBlock(buttonBlocks, 'SCALE_PHOTO');
    const colorBt = getNameFromButtonBlock(buttonBlocks, 'color');
    const fontBt = getNameFromButtonBlock(buttonBlocks, 'font');
    const abcBt = getNameFromButtonBlock(buttonBlocks, 'ABC');
    const clickBt = getNameFromButtonBlock(buttonBlocks, 'Click_on_the');
    const tapAndDrapBt = getNameFromButtonBlock(buttonBlocks, 'Tap_and_Drag');
    const eauBt = getNameFromButtonBlock(buttonBlocks, 'Eau_de_parfum');
    const mlBt = getNameFromButtonBlock(buttonBlocks, '25ml');
    const noneSelect = (
      <div className="div-none-select animated faster fadeIn">
        <div className="div-image">

          <img loading="lazy" src={icNoneSelect} alt="bg" />
        </div>
        <span>
          {customizaBt}
        </span>
      </div>
    );
    const btBottom = (
      <div className="div-list-bt">
        <div className="div-list-option">
          <button type="button" className="bg-black" onClick={this.onClickDelete}>
            <img loading="lazy" src={icDelete} alt="delete" />
          </button>
          <button type="button" className={step === 1 ? 'active' : ''} onClick={() => this.setState({ step: 1 })}>
            <img loading="lazy" src={icUpload} alt="upload" />
            <span>
              {uploadBt}
            </span>
          </button>
          <button type="button" className={step === 2 ? 'mid active' : 'mid'} onClick={() => this.setState({ step: 2 })}>
            <img loading="lazy" src={icScale} alt="scale" />
            <span>
              {scaleBt}
            </span>
          </button>
          <button type="button" className={step === 3 ? 'active' : ''} onClick={() => this.setState({ step: 3 })}>
            <img loading="lazy" src={icText} alt="scale" />
            <span>
              {textBt}
            </span>
          </button>
        </div>
        <div className="bt-continute">
          <button type="button" onClick={this.onClickContinute}>
            <img loading="lazy" src={icChecked} alt="checked" />
            <span>
              {continueBt}
            </span>
          </button>
        </div>
      </div>
    );
    const uploadPhoto = (
      <div className="div-upload-photo animated faster fadeIn">
        <h3>
          {uploadPhotoBt}
        </h3>
        <div className="div-upload-image">
          <Dropzone ref={this.refCrop} onDrop={acceptedFiles => this.onSelectFile(acceptedFiles)} accept=".jpeg,.png,.jpg" multiple={false}>
            {({ getRootProps, getInputProps }) => (
              <section className="div-section-more">
                <div {...getRootProps()}>
                  <input {...getInputProps()} />
                  <span>
                    {drapBt}
                  </span>
                  <span className="mt-2">
                    {orBt}
                  </span>
                  <button type="button" className="mt-2">
                    {chooseBt}
                  </button>
                </div>
              </section>
            )}
          </Dropzone>
        </div>
      </div>
    );
    const scalePhoto = (
      <div className="div-scale-photo animated faster fadeIn">
        <h3 className={isMobile ? 'hidden' : ''}>
          {scalePhotoBt}
        </h3>
        <div className="div-wrap-scale">
          <div className="div-scale">
            <InputRange
              maxValue={3}
              minValue={1}
              step={0.1}
              value={this.state.scale}
              onChange={value => this.setState({ scale: value.toFixed(1), valueText: value.toFixed(1) })}
            />
            <input type="number" className={isMobile ? 'hidden' : 'input-scale'} value={this.state.valueText} onChange={this.onChangeScale} />
          </div>
        </div>
      </div>
    );
    const addText = (
      <div className="div-add-text animated faster fadeIn">
        <h3 className={isMobile ? 'hidden' : ''}>
          {scalePhotoBt}
        </h3>
        <div className="wrap-text">
          <div className="content-text-edit">
            <div className="div-text-edit">
              <span className="title-text">
                {textBt}
              </span>
              <div className="div-input">
                <input type="text" placeholder="My Perfume" value={name} onChange={this.onChangeText} />
                <span>
                  {`${name ? name.length : '0'}/15`}
                </span>
              </div>
            </div>
            <div className="div-add-color">
              <span className="title-text">
                {colorBt}
              </span>
              <div className="list-button">
                <div className={`outline-bt ${color === COLOR_CUSTOME.BLACK ? 'selected' : ''}`}>
                  <button className="bt-color black" type="button" onClick={() => this.onChangeColor(COLOR_CUSTOME.BLACK)} />
                </div>
                <div className={`outline-bt ${color === COLOR_CUSTOME.WHITE ? 'selected' : ''}`}>
                  <button className="bt-color white" type="button" onClick={() => this.onChangeColor(COLOR_CUSTOME.WHITE)} />
                </div>
                <div className={`outline-bt ${color === COLOR_CUSTOME.GOLD ? 'selected' : ''}`}>
                  <button className="bt-color yellow" type="button" onClick={() => this.onChangeColor(COLOR_CUSTOME.GOLD)} />
                </div>
              </div>
            </div>
            <div className="div-font-text">
              <span className="title-text">
                {fontBt}
              </span>
              <div className="wrap-list-font">
                <div className="div-list-font">
                  <div className="list-font">
                    {
                      _.map(isMobile ? listFont : listFont.slice(0, 3), d => (
                        <button type="button" style={{ fontFamily: d }} className={d === font ? 'active' : ''} onClick={() => this.onChangeFont(d)}>
                          {abcBt}
                        </button>
                      ))
                    }
                  </div>
                </div>
                <div className={isMobile ? 'hidden' : 'div-list-font'}>
                  <div className="list-font">
                    {
                    _.map(listFont.slice(4, 6), d => (
                      <button type="button" style={{ fontFamily: d }} className={d === font ? 'active' : ''} onClick={() => this.onChangeFont(d)}>
                        {abcBt}
                      </button>
                    ))
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
    return (
      <div className="div-custome-v3 animated faster fadeInRight" style={isMobile && !isIOS ? { height: window.innerHeight - 40 } : isMobile ? { height: window.innerHeight } : {}}>
        <div className={isMobile && step > 0 ? 'div-custome-left bg-gray' : 'div-custome-left'}>
          <div className="div-bottle-left">
            <BottleCustom
              isResponsive={isMobile}
              ratioHeight={isMobile ? 0.58 : undefined}
              heightDefault={isMobile ? '58%' : undefined}
              classWrap={isMobile ? 'wrap-custome-bottle' : ''}
              isCustomeV3
              setEditorRef={this.setEditorRef}
              uploadImage={uploadImage}
              onGotoProduct={this.onClickBottle}
              image={customeBottle.image || icDefaultEdit}
              color={customeBottle.color}
              font={customeBottle.font}
              scale={scale}
              eauBt={eauBt}
              mlBt={mlBt}
              isDisplayName
              name={customeBottle.name}
              combos={[]}
              name1={name1}
              name2={name2}
            />
          </div>
          <span className={isMobile ? 'hidden' : 'div-text-use'}>
            {clickBt}
          </span>
        </div>
        {
          isMobile ? (
            // <div className="bottom-list-button">
            <FixedBottom>
              <div className="bottom-list-button">
                {
                  step > 1 ? (
                    <div className="div-content-custome animated faster fadeInUp">
                      {step === 2 ? scalePhoto : step === 3 ? addText : null}
                    </div>
                  ) : (
                    <span className="div-text-use">
                      {uploadImage ? tapAndDrapBt : clickBt}
                    </span>
                  )
                }
                <div className="list-button">
                  <button type="button" className="bt-black" onClick={step > 1 ? this.onClickCancel : this.onClickDelete}>
                    <img src={step > 1 ? icX : icDelete} alt="delete" />
                  </button>
                  {
                  step > 1 ? (
                    <div className="div-text">
                      <span>{step === 2 ? scaleBt : step === 3 ? textBt : ''}</span>
                    </div>
                  ) : (
                    <div className="list-bt-edit">
                      <Dropzone ref={this.refCrop} onDrop={acceptedFiles => this.onSelectFile(acceptedFiles)} accept=".jpeg,.png,.jpg" multiple={false}>
                        {({ getRootProps, getInputProps }) => (
                          <section className="div-section-more">
                            <div {...getRootProps()}>
                              <input {...getInputProps()} />
                              <button type="button">
                                <img loading="lazy" src={icUpload} alt="upload" />
                              </button>
                            </div>
                          </section>
                        )}
                      </Dropzone>
                      <button type="button" className="bt-mid" onClick={() => this.setState({ step: 2, scaleTemp: scale })}>
                        <img loading="lazy" src={icScale} alt="scale" />
                      </button>
                      <button type="button" onClick={() => this.setState({ step: 3, customeBottleTemp: _.cloneDeep(customeBottle) })}>
                        <img loading="lazy" src={icText} alt="text" />
                      </button>
                    </div>
                  )
                }
                  <button type="button" className="bt-black" onClick={step > 1 ? this.onClickApply : this.onClickContinute}>
                    <img loading="lazy" src={icChecked} alt="checked" />
                  </button>
                </div>
              </div>
            </FixedBottom>
          ) : (
            <div className="div-custome-right">
              <div className="div-top-image">
                {step === 0 ? noneSelect : step === 1 ? uploadPhoto : step === 2 ? scalePhoto : addText}
              </div>
              {btBottom}
            </div>
          )
        }

      </div>
    );
  }
}

export default PageCustome;
