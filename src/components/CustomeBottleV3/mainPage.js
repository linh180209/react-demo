/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import loadImage from 'blueimp-load-image';
import Slider from 'react-slick';
import _ from 'lodash';

import { Dropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import BottleCustom from '../B2B/CardItem/bottleCustom';
import icUpload from '../../image/icon/ic-upload-v3.svg';
import icClose from '../../image/icon/ic-close-x.svg';
import icBottleNew from '../../image/bottle-new.png';
import icDelete from '../../image/icon/icDeleteBottle.svg';

import { COLOR_CUSTOME, FONT_CUSTOME } from '../../constants';
import { getNameFromButtonBlock } from '../../Redux/Helpers';
import icDown from '../../image/icon/icDropdownCT.svg';
import { isBrowser, isMobile } from '../../DetectScreen';
import ButtonCT from '../ButtonCT';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
      filterThumbs: undefined,
    };
  }

  onClickChooseImage = (image) => {
    const { customeBottle, arrayThumbs } = this.props;
    customeBottle.image = image;
    const ele = _.find(arrayThumbs, x => x.image === image);
    if (ele) {
      customeBottle.imagePremade = ele.id;
    }
    this.props.onChangeCustomeBottle(customeBottle);
  }

  itemImageSelection = (listImage) => {
    const newArray = [];
    if (listImage) {
      for (let i = 0; i < listImage.length; i += 4) {
        newArray.push(listImage.slice(i, i + 4));
      }
    }
    return (
      <div className="item-slice">
        {
        _.map(newArray, d => (
          <div className="line-image">
            {
              _.map(d, x => (
                <div className="div-image" category={x.categoryCT} onClick={() => this.onClickChooseImage(x.image)}>
                  {/* <img className={this.state.croppedImageUrlDone === x.image ? 'active' : ''} src={x.thumb} alt="bottle" onClick={() => this.setState({ croppedImageUrlDone: x.image, croppedImageUrl: undefined, srcImage: undefined })} /> */}
                  <LazyLoadImage src={x.thumb} alt="test" />
                  {/* <img src={x.thumb} alt="test" onClick={() => this.onClickChooseImage(x.image)} /> */}
                </div>
              ))
            }
          </div>
        ))
      }
      </div>
    );
  };

  onChangeColor = (color) => {
    const { customeBottle } = this.props;
    customeBottle.color = color;
    this.props.onChangeCustomeBottle(customeBottle);
  }

  onChangeFont = (font) => {
    const { customeBottle } = this.props;
    customeBottle.font = font;
    this.props.onChangeCustomeBottle(customeBottle);
  }

  onClearBottom = () => {
    const { customeBottle } = this.props;
    customeBottle.font = FONT_CUSTOME.JOST;
    customeBottle.color = COLOR_CUSTOME.BLACK;
    customeBottle.name = '';
    customeBottle.image = '';
    customeBottle.imagePremade = undefined;
    this.props.onChangeCustomeBottle(customeBottle);
  }

  onChangeText = (e) => {
    const { value } = e.target;
    if (value.length > 15) {
      return;
    }
    const { customeBottle } = this.props;
    customeBottle.name = value;
    this.props.onChangeCustomeBottle(customeBottle);
  }

  onSelectFile = (files) => {
    if (files && files.length > 0) {
      loadImage(
        files[0],
        (img) => {
          const base64data = img.toDataURL('image/png');
          this.props.onSetImageUpload(base64data);
          if (isMobile) {
            this.props.onChangeGuiEdit(true);
          }
        },
        { orientation: 1 },
      );
    }
  };

  onSave = () => {
    const { customeBottle } = this.props;
    this.props.onSave(customeBottle);
    if (this.props.idCartItem) {
      setTimeout(() => {
        this.props.onClickUpdatetoCart();
        if (this.props.propsState) {
          this.props.propsState.custome.name = customeBottle.name;
          this.props.propsState.custome.image = customeBottle.image;
          this.props.propsState.custome.color = customeBottle.color;
          this.props.propsState.custome.font = customeBottle.font;
          this.props.propsState.custome.imagePremade = customeBottle.imagePremade;
        }
      }, 500);
    }
    this.props.closePopUp();
  }

  onClickBottle = () => {
    if (isMobile) {
      this.props.onScreenUploadImage(true);
      this.props.onChangeGuiUpload(true);
    }
  }

  toggle = () => {
    const { dropdownOpen } = this.state;
    this.setState({
      dropdownOpen: !dropdownOpen,
    });
  }

  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: false,
      arrows: false,
      autoplaySpeed: 5000,
      pauseOnHover: false,
    };
    const { filterThumbs } = this.state;
    const {
      arrayThumbs, customeBottle, buttonBlocks, name1, name2, listFilterThumbs,
    } = this.props;
    const allBt = getNameFromButtonBlock(buttonBlocks, 'All');
    const arrayThumbsFilter = (!filterThumbs || filterThumbs === allBt) ? arrayThumbs : _.filter(arrayThumbs, x => x.category === filterThumbs);
    const newArray = [];
    if (arrayThumbsFilter) {
      for (let i = 0; i < arrayThumbsFilter.length; i += 12) {
        newArray.push(arrayThumbsFilter.slice(i, i + 12));
      }
    }
    const listFont = [FONT_CUSTOME.JOST, FONT_CUSTOME.PLAYFAIR_DISPLAY, FONT_CUSTOME.DANCING_SCRIPT, FONT_CUSTOME.BALSAMIQ_SANS, FONT_CUSTOME.PERMANENT_MARKER, FONT_CUSTOME.NUNITO];
    const {
      color, font, image, name,
    } = customeBottle;
    const isCustome = name || image;
    const creatorEle = _.find(arrayThumbs || [], x => x.image === image);

    const customizeBottleBt = getNameFromButtonBlock(buttonBlocks, 'CUSTOMIZE_YOUR_BOTTLE');
    const customizeBottleMBBt = getNameFromButtonBlock(buttonBlocks, 'CUSTOMIZE_BOTTLE');
    const eauBt = getNameFromButtonBlock(buttonBlocks, 'Eau_de_parfum');
    const mlBt = getNameFromButtonBlock(buttonBlocks, '25ml');
    const dropBt = getNameFromButtonBlock(buttonBlocks, 'Drag_and_drop_or_browse_your_files');
    const textBt = getNameFromButtonBlock(buttonBlocks, 'TEXT');
    const myPerfumeBt = getNameFromButtonBlock(buttonBlocks, 'My_perfume');
    const colorBt = getNameFromButtonBlock(buttonBlocks, 'color');
    const fontBt = getNameFromButtonBlock(buttonBlocks, 'font');
    const abcBt = getNameFromButtonBlock(buttonBlocks, 'ABC');
    const saveBt = getNameFromButtonBlock(buttonBlocks, 'SAVE');
    const cancelBt = getNameFromButtonBlock(buttonBlocks, 'CANCEL');
    return (
      <div className="div-main-page-custome-bottle animated faster fadeInLeft">
        <div className="div-header">
          <h1>
            {isMobile ? customizeBottleMBBt : customizeBottleBt}
          </h1>
        </div>
        <div className="div-custome-image div-custome-image-v3">
          <button
            onClick={this.onClearBottom}
            type="button"
            className={isCustome ? 'div-delete' : 'hidden'}
          >
            <img loading="lazy" src={icDelete} alt="delete" />
          </button>
          <div className={creatorEle && creatorEle.creator ? 'auth-design' : 'hidden'}>
            <span>
              Artwork by
            </span>
            <span>
              {creatorEle ? creatorEle.creator : ''}
            </span>
          </div>
          <div className="div-bottle">
            <div className="div-image-bottle-v3">
              <BottleCustom
                isCustomeV3
                onGotoProduct={this.onClickBottle}
                image={customeBottle.image}
                color={customeBottle.color}
                font={customeBottle.font}
                eauBt={eauBt}
                mlBt={mlBt}
                isDisplayName
                name={customeBottle.name}
                combos={[]}
                name1={name1}
                name2={name2}
              />
            </div>
          </div>
          <div className="div-image">
            <div className="bt-upload-image">
              <Dropzone onDrop={acceptedFiles => this.onSelectFile(acceptedFiles)} accept=".jpeg,.png,.jpg" multiple={false}>
                {({ getRootProps, getInputProps }) => (
                  <section className="div-section-more">
                    <div {...getRootProps()}>
                      <input {...getInputProps()} />
                      <img loading="lazy" src={icUpload} alt="uploadFile" />
                      <span className={isMobile ? 'hidden' : ''}>
                        {dropBt}
                      </span>
                    </div>
                  </section>
                )}
              </Dropzone>
            </div>
            <div className="div-slice-image">
              {
                isMobile ? (
                  <ul className="list-image-selection">
                    {
                      _.map(arrayThumbsFilter, x => (
                        <ui className="div-image" category={x.categoryCT}>
                          <img loading="lazy" src={x.thumb} alt="test" onClick={() => this.onClickChooseImage(x.image)} />
                        </ui>
                      ))
                    }
                  </ul>
                ) : (
                  <Slider {...settings}>
                    {
                      _.map(newArray, d => (
                        this.itemImageSelection(d)
                      ))
                    }
                  </Slider>
                )
              }

            </div>
          </div>
        </div>
        <div className="div-text-custome">
          <div className="div-text-color">
            <div className="div-text">
              <span className="title-text">
                {textBt}
              </span>
              <div className="div-input">
                <input type="text" placeholder={myPerfumeBt} onChange={this.onChangeText} value={customeBottle.name} />
                <span>
                  {`${customeBottle.name ? customeBottle.name.length : '0'}/15`}
                </span>
              </div>
            </div>
            <div className="div-color">
              <span className="title-text">
                {colorBt}
              </span>
              <div className="div-list-color">
                <div className={`outline-bt ${color === COLOR_CUSTOME.BLACK ? 'selected' : ''}`}>
                  <button className="bt-color black" type="button" onClick={() => this.onChangeColor(COLOR_CUSTOME.BLACK)} />
                </div>
                <div className={`outline-bt ${color === COLOR_CUSTOME.WHITE ? 'selected' : ''}`}>
                  <button className="bt-color white" type="button" onClick={() => this.onChangeColor(COLOR_CUSTOME.WHITE)} />
                </div>
                <div className={`outline-bt ${color === COLOR_CUSTOME.GOLD ? 'selected' : ''}`}>
                  <button className="bt-color yellow" type="button" onClick={() => this.onChangeColor(COLOR_CUSTOME.GOLD)} />
                </div>
              </div>
            </div>
          </div>
          <div className="div-font">
            <span className="title-text">
              {fontBt}
            </span>
            <div className="list-font">
              {
                isMobile ? (
                  <React.Fragment>
                    <div className="list-font-item">
                      {
                      _.map(listFont, d => (
                        <button type="button" style={{ fontFamily: d }} className={d === font ? 'active' : ''} onClick={() => this.onChangeFont(d)}>
                          {abcBt}
                        </button>
                      ))
                    }
                    </div>
                  </React.Fragment>
                ) : (
                  _.map(listFont, d => (
                    <button type="button" style={{ fontFamily: d }} className={d === font ? 'active' : ''} onClick={() => this.onChangeFont(d)}>
                      {abcBt}
                    </button>
                  ))
                )
              }
            </div>
          </div>
          <div className="div-button">
            <ButtonCT
              dataGtmtracking={this.props.dataGtmtracking || 'customize-bottle-step-2-save'}
              className="button-black"
              onClick={this.onSave}
              name={saveBt}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default MainPage;
