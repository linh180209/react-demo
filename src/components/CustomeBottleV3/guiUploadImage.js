import React, { Component } from 'react';
import icBack from '../../image/icon/icBackGui.svg';
import icImageDefault from '../../image/icon/ic-image-default.svg';
import icButtons from '../../image/icon/ic-image-button.svg';
import icEditImage from '../../image/icon/ic-edit-image.svg';
import icChecked from '../../image/icon/ic-checked-gui.svg';
import { getNameFromButtonBlock, hiddenChatIcon } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

class GuiUploadImage extends Component {
  componentDidMount() {
    window.addEventListener('resize', this.updateSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateSize);
  }

  updateSize = () => {
    this.forceUpdate();
  }

  render() {
    const { onChangeGui, isGuiEdit, buttonBlocks } = this.props;
    const tabHereBt = getNameFromButtonBlock(buttonBlocks, 'Tap_here');
    const tapToStart = getNameFromButtonBlock(buttonBlocks, 'tap_to_start');
    const tabhereAddBt = getNameFromButtonBlock(buttonBlocks, 'Tap_here_to');
    const tabhereConfirm = getNameFromButtonBlock(buttonBlocks, 'Tap_here_to_confirm');
    const heightScreen = window.innerHeight - 40;
    if (isMobile) {
      hiddenChatIcon();
    }
    return (
      <div
        className="div-gui-upload-image animated faster fadeIn"
        // style={{ height: heightScreen }}
        onClick={() => onChangeGui(false)}
      >
        <div className="div-back">
          <img loading="lazy" src={icBack} alt="back" />
          <span>
            {tabHereBt}
          </span>
        </div>
        <div className="div-bottom">
          <img loading="lazy" src={isGuiEdit ? icEditImage : icImageDefault} alt="default" className="img-gui" />
          <span className="text-upload">
            {
              isGuiEdit ? 'Adjust your image by dragging Left or Right' : 'Tap + to upload photos'
            }
          </span>
          <button type="button" className="bt-tap-start">
            {tapToStart}
          </button>

          <span className={isGuiEdit ? 'hidden' : ''}>
            {tabhereAddBt}
          </span>
          {
            isGuiEdit ? (
              <div className="bt-checked">
                <span>
                  {tabhereConfirm}
                </span>
                <img loading="lazy" src={icChecked} alt="checked" />
              </div>
            ) : (
              <img loading="lazy" src={icButtons} alt="list-bt" />
            )
          }
        </div>

      </div>
    );
  }
}

export default GuiUploadImage;
