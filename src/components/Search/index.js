import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Search extends Component {
  render() {
    return (
      <div className="container-search">
        <input type="search" placeholder="" />
        <i className="fa fa-search" />
      </div>
    );
  }
}

Search.propTypes = {

};

export default Search;
