import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody } from 'reactstrap';
import onClickOutside from 'react-onclickoutside';
import { isIOS } from 'react-device-detect';
import { getAltImage } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

class CardKnowMore extends Component {
  handleClickOutside = () => {
    this.props.onCloseKnowMore();
  }

  render() {
    const { isShowKnowMore, data } = this.props;
    const { imageKnowMore } = data;
    return (
      <div className={isShowKnowMore ? 'card_know_more card_know_more_open' : 'card_know_more card_know_more_close'}>
        <Card style={{ height: '100%', overflowY: 'auto' }}>
          <CardBody>
            <div className="know_more-content" style={isMobile && isIOS ? { marginTop: '0px' } : {}}>
              <div className="button-back">
                <button
                  type="button"
                  onClick={this.props.onCloseKnowMore}
                >
                  <i className="fa fa-arrow-right" />
                </button>
              </div>
              <img loading="lazy" src={imageKnowMore} alt={getAltImage(imageKnowMore)} />
              {/* <span className="title">
                {title}
              </span>
              <div className="text">
                {content}
              </div> */}
            </div>
          </CardBody>
        </Card>
      </div>
    );
  }
}

CardKnowMore.defaultProps = {
  isShowKnowMore: false,
};

CardKnowMore.propTypes = {
  onCloseKnowMore: PropTypes.func.isRequired,
  isShowKnowMore: PropTypes.bool,
  data: PropTypes.shape({
    title: PropTypes.string,
    content: PropTypes.string,
    imageKnowMore: PropTypes.string,
  }).isRequired,
};

export default onClickOutside(CardKnowMore);
