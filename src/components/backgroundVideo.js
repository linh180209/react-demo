import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getAltImage } from '../Redux/Helpers';

class BackGroundVideo extends Component {
  constructor(props) {
    super(props);
    this.refVideo = React.createRef();
  }

  componentDidMount() {
    this.handlePlayVideoManual();
    const element = document.getElementById('myVideo');
    if (element) {
      element.setAttribute('muted', 'true');
    }
    if (this.props.onEnd) {
      document.getElementById('myVideo').addEventListener('ended', this.onEnd);
    }
    if (this.props.getCurrentTime) {
      this.getTimeInterval = setInterval(() => {
        const ele = document.getElementById('myVideo');
        if (ele) {
          this.props.getCurrentTime(ele.currentTime, ele.duration);
        }
      }, 1000);
    }
  }

  handlePlayVideoManual = () => {
    const element = document.getElementById('myVideo');
    if (element && element.paused) {
      element.play();
    }
  }

  handlePauseVideoManual = () => {
    const element = document.getElementById('myVideo');
    if (element && !element.paused) {
      element.pause();
    }
  }

  componentWillUnmount() {
    if (this.props.onEnd) {
      document.getElementById('myVideo').removeEventListener('ended', this.onEnd, false);
    }
    if (this.getTimeInterval) {
      clearInterval(this.getTimeInterval);
    }
  }

  onEnd = () => {
    if (this.props.onEnd) {
      this.props.onEnd();
    }
  }

  render() {
    const {
      url, loop, urlImage, isVideo, placeholder, autoPlay, styleVideo,
    } = this.props;
    // this.isPlayVideoManual();
    return (
      <div className="background--letsguideyou">
        {
          !isVideo
            ? (
              <img loading="lazy" src={urlImage} alt={getAltImage(urlImage)} style={{ width: '100%', height: '100%' }} />
            )
            : (
              // eslint-disable-next-line jsx-a11y/media-has-caption
              <video
                ref={this.refVideo}
                id="myVideo"
                style={{
                  objectFit: styleVideo || 'cover',
                  width: '100%',
                  height: '100%',
                }}
                muted="true"
                playsinline="true"
                autoPlay={autoPlay}
                loop={loop}
                preload="auto"
                // src={url}
                poster={placeholder}
              >
                <source src={url} />
              </video>
            )
        }
      </div>
    );
  }
}
BackGroundVideo.defaultProps = {
  loop: true,
  autoPlay: true,
  onEnd: undefined,
  urlImage: '',
  isVideo: true,
  styleVideo: undefined,
};

BackGroundVideo.propTypes = {
  url: PropTypes.string.isRequired,
  urlImage: PropTypes.string,
  loop: PropTypes.bool,
  autoPlay: PropTypes.bool,
  onEnd: PropTypes.func,
  isVideo: PropTypes.bool,
  placeholder: PropTypes.string.isRequired,
  styleVideo: PropTypes.string,
};

export default BackGroundVideo;
