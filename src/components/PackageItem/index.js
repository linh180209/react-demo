import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getAltImage } from '../../Redux/Helpers';

class PackageItem extends Component {
  render() {
    const { url, name, text } = this.props;
    return (
      <div
        className="package-item"
      >
        <img
          loading="lazy"
          src={url}
          alt={getAltImage(url)}
        />
        <div>
          <strong>
            {name}
          </strong>
          {' '}
          {text}
        </div>
      </div>
    );
  }
}

PackageItem.propTypes = {
  url: PropTypes.number.isRequired,
  name: PropTypes.number.isRequired,
  text: PropTypes.number.isRequired,
};

export default PackageItem;
