import React, { Component } from 'react';
import Slider from 'react-rangeslider';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';
import classnames from 'classnames';

import 'react-rangeslider/lib/index.css';
import ButtonNext from '../ButtonNext';
import Title from '../StepQuestion/title';

class RangeBar extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      horizontal: props.defaultValue || 30,
      dob: props.defaultValue,
    };
  }

  componentDidMount() {
    const { name, no, id } = this.props;
    const { horizontal } = this.state;
    this.props.onChange(horizontal, name, no, id);
  }

  // onClickNext = () => {
  //   const { name, no, id } = this.props;
  //   const { horizontal } = this.state;
  //   this.props.onChange(horizontal, name, no - 1, id);
  // }

  handleChangeHorizontal = (value) => {
    this.setState({
      horizontal: value,
    });
    const { name, no, id } = this.props;
    this.props.onChange(value, name, no, id);
  };

  onChangeDob = (e) => {
    const { value } = e.target;
    const { name, no, id } = this.props;
    this.setState({ dob: value });
    this.props.onChange(value, name, no, id);
  }

  focusInput = () => {

  }

  cancelFocus = () => {

  }

  render() {
    const { horizontal, dob } = this.state;
    const horizontalLabels = {
      1: '1',
      99: '99',
    };

    const {
      no, title, nameBt, isDob, isShowErrorDob,
    } = this.props;
    return (
      <div id={this.props.id} className="slider custom-labels w-100">
        <Title no={no} title={title} />
        {
            isDob ? (
              <div className={classnames('div-dob', isShowErrorDob ? 'error' : '')}>
                <InputMask
                  className="border-checkout input-dob"
                  type="text"
                  name="dob"
                  placeholder="DD/MM/YYYY"
                  mask="99/99/9999"
                  maskChar={null}
                  defaultValue={dob}
                  value={dob}
                  onChange={this.onChangeDob}
                />
              </div>
            ) : (
              <div className="div-content-age">
                <Slider
                  min={1}
                  max={99}
                  value={horizontal || 30}
                  labels={horizontalLabels}
                  handleLabel={horizontal}
                  onChange={this.handleChangeHorizontal}
                />
                <div className="div-text div-row justify-between">
                  <span>
                    1
                  </span>
                  <span>
                    99
                  </span>
                </div>
              </div>
            )
          }

        {/* <div className="div-col justify-center items-center mt-2">
          <ButtonNext name={nameBt} onNext={this.onClickNext} />
        </div> */}
      </div>
    );
  }
}

RangeBar.propTypes = {
  no: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  nameBt: PropTypes.string.isRequired,
  defaultValue: PropTypes.string.isRequired,
};

export default RangeBar;
