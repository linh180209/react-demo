import React, { Component } from 'react';
import PropTypes from 'prop-types';

class AddressItem extends Component {
  render() {
    const {
      title, name, address, city, country,
    } = this.props;
    return (
      <div className="address-item">
        <span className="title">
          {title}
        </span>
        <div className="content">
          <span>
            {name}
          </span>
          <span>
            {address}
          </span>
          <span>
            {city}
          </span>
          <span>
            {country}
          </span>
        </div>
        <div className="div-button ml-5">
          <button
            type="button"
          >
            <i className="fa fa-times" />
          </button>
        </div>
      </div>
    );
  }
}

AddressItem.propTypes = {
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  address: PropTypes.string.isRequired,
  city: PropTypes.string.isRequired,
  country: PropTypes.string.isRequired,
};
export default AddressItem;
