import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';

import packaging from '../../image/packaging.png';
import PackageItem from '../PackageItem';
import bg from '../../image/bg6.svg';
import package1 from '../../image/package1.png';
import package2 from '../../image/package2.png';
import { generateUrlWeb } from '../../Redux/Helpers';

class Packaging extends Component {
  constructor(props) {
    super(props);
    this.data = [
      {
        url: package1,
        name: 'Our Bottles',
        text: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes',
      },
      {
        url: package2,
        name: 'Our Bottles',
        text: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes',
      },
    ];
    this.state = {
      item1: undefined,
      item2: undefined,
    };
  }

  componentDidMount = () => {
    const { packagingCms } = this.props;
    const packagingData = packagingCms && packagingCms.length > 0 ? packagingCms[0] : undefined;
    const items = packagingData ? packagingData.value.product.items : undefined;
    const item1 = items && items.length > 0 ? items[0] : undefined;
    const item2 = items && items.length > 1 ? items[1] : undefined;
    if (item1) {
      item1.text = item1.description;
      item1.url = item1.images ? item1.images[0].image : '';
    }
    if (item2) {
      item2.text = item2.description;
      item2.url = item1.images ? item2.images[0].image : '';
    }
    this.setState({ item1, item2 });
  };

  onGotoMore = () => {
    this.props.history.push(generateUrlWeb('/b2b/packaging'));
  }

  render() {
    const { isDetail } = this.props;
    const { item1, item2 } = this.state;
    return (
      <div id="packaging" className="packaging">
        {/* <img style={{ height: `${maxHeight}px` }} className="bg" src={bg} alt="bg" /> */}
        <div className="content">
          <div className="image">
            <div className="line-title" />
            <img loading="lazy" src={packaging} alt="img" />
          </div>
          <div className="package">
            {/* {
            _.map(this.data, d => (
              <PackageItem
                url={d.url}
                name={d.name}
                text={d.text}
              />
            ))
          } */}
            {
            item1
              ? (
                <PackageItem
                  url={item1.url}
                  name={item1.name}
                  text={item1.text}
                />
              ) : (<div />)
          }
            {
            item2
              ? (
                <PackageItem
                  url={item2.url}
                  name={item2.name}
                  text={item2.text}
                />
              ) : (<div />)
          }
          </div>
        </div>
        <div className="div-button">
          {
            !isDetail ? (
              <button
                type="button"
                onClick={this.onGotoMore}
              >
                View More
              </button>
            ) : (<div />)
          }

        </div>
      </div>
    );
  }
}

Packaging.defaultProps = {
  isDetail: false,
};

Packaging.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  isDetail: PropTypes.func,
  packagingCms: PropTypes.shape(PropTypes.object).isRequired,
};

export default withRouter(Packaging);
