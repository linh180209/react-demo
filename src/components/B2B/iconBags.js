import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import bags from '../../image/bags.png';
import iconClose from '../../image/icon_close.svg';
import arrow from '../../image/right-arrow-card.svg';
import CardItem from './CardItem';
import { deleteProductBasket, updateProductBasket } from '../../Redux/Actions/basket';
import auth from '../../Redux/Helpers/auth';
import { generaCurrency, generateUrlWeb } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

class IconBags extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowCard: false,
      basket: {
        items: [],
      },
    };
  }

  onCloseCard = () => {
    this.setState({ isShowCard: false });
  }

  onOpenCard = () => {
    this.setState({ isShowCard: true });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { basket } = nextProps;
    if (basket && basket !== prevState.basket) {
      _.assign(objectReturn, { basket });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  render() {
    const { isShowCard } = this.state;
    const { className } = this.props;
    const { items, id } = this.state.basket;
    const total = _.reduce(items, (sum, d) => sum + d.total, 0);
    return (
      <div className={isShowCard ? 'cd-cart cd-cart--open' : 'cd-cart'}>
        <button
          type="button"
          className={`b2b-bt-fix_top ${className}`}
          onClick={this.onOpenCard}
        >
          <img loading="lazy" className="img-bags_menu" src={bags} alt={bags} />
          <span className="span-menu_number">
            {items.length}
          </span>
        </button>
        <div className="cd-card__layout" style={isMobile ? { right: '0px' } : {}}>
          <header className="cd-cart__header">
            <h2>
              Cart
            </h2>
            <button
              type="button"
              className="button--bg__none"
            >
              <img loading="lazy" style={{ width: '10px' }} src={iconClose} alt="iconClose" />
            </button>
          </header>
          <div className="cd-cart__body">
            <ul>
              {
                _.map(items, d => (
                  <li className="cd-cart__product">
                    <CardItem
                      updateProductBasket={this.props.updateProductBasket}
                      deleteProductBasket={this.props.deleteProductBasket}
                      data={d}
                      idCart={id}
                      cms={this.props.cms}
                    />
                  </li>
                ))
              }
            </ul>
          </div>
          <footer className="cd-cart__footer">
            <div className="value">
              <button
                type="button"
                className="cd-cart__checkout"
                onClick={() => { this.props.history.push(generateUrlWeb('/b2b/checkout')); }}
              >
                <em>
                  {'Checkout - '}
                  <span>
                    {generaCurrency(total)}
                  </span>
                  {/* <img src={arrow} alt="arrow" /> */}
                </em>
              </button>
            </div>
            <button
              className="button"
              type="button"
              onClick={this.onCloseCard}
            >
              <img loading="lazy" src={iconClose} alt="icon" />
            </button>
          </footer>
        </div>
      </div>
    );
  }
}

IconBags.defaultProps = {
  className: '',
};

IconBags.propTypes = {
  className: PropTypes.string,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  updateProductBasket: PropTypes.func.isRequired,
  deleteProductBasket: PropTypes.func.isRequired,
  cms: PropTypes.shape().isRequired,
};

function mapStateToProps(state) {
  return {
    basket: state.basket,
    cms: state.cms,
  };
}

const mapDispatchToProps = {
  deleteProductBasket,
  updateProductBasket,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(IconBags));
