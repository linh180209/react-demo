import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import account from '../../image/account.png';
import tittleAccount from '../../image/titleAccount.png';
import OrderHistoryItem from '../OrderHistoryItem';
import AddressItem from './AddressItem';
import BackgroundVideo from '../backgroundVideo';
import logo from '../../image/bg6.svg';
import { logoutRequest } from '../../Redux/Actions/login';
import { generateUrlWeb, gotoShopHome, trackGTMLogout } from '../../Redux/Helpers';

class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      typeActive: 'accountDetails',
      isAddress: false,
      imageUrl: '',
    };
    this.dataAddAddress = [
      {
        name: 'firstName',
        title: 'First Name',
        className: 'text-short',
      },
      {
        name: 'address',
        title: 'Address',
        className: 'text-long',
      },
      {
        name: 'lastName',
        title: 'Last Name',
        className: 'text-short',
      },
      {
        name: 'city',
        title: 'TOWN/CITY',
        className: 'text-long',
      },
      {
        name: 'mobile',
        title: 'MOBILE',
        className: 'text-short',
      },
      {
        name: 'country',
        title: 'COUNTRY',
        className: 'text-long',
      },
      {
        name: null,
        title: '',
        className: 'text-short',
      },
      {
        name: 'postCode',
        title: 'POSTCODE',
        className: 'text-long',
      },
    ];
  }

  componentDidMount = () => {
    const { accountCms } = this.props;
    const { image_background: imageUrl } = accountCms;
    this.setState({ imageUrl });
  };

  onChange = (e) => {
    const { name } = e.target;
    this.setState({ typeActive: name });
  }

  clickLogout = () => {
    const { login } = this.props;
    const { user } = login;
    trackGTMLogout(user.id);
    this.props.logoutRequest();
    // this.props.history.push(generateUrlWeb('/'));
    gotoShopHome();
  }

  render() {
    const { typeActive, isAddress, imageUrl } = this.state;
    const { login } = this.props;
    const { user } = login;
    const accountDetails = (
      <div className="div-detail">
        <Row>
          <Col md="12" xs="12">
            <div className="account_details-info">
              <Row>
                <Col md="5" xs="12" className="mb-2">
                  <span>
                    First Name
                  </span>
                </Col>
                <Col md="7" xs="12" className="mb-2">
                  <span>
                    Last Name
                  </span>
                </Col>
                <Col md="5" xs="12" className="mb-3">
                  <span>
                    <strong>
                      {user.first_name}
                    </strong>
                  </span>
                </Col>
                <Col md="7" xs="12" className="mb-3">
                  <span>
                    <strong>
                      {user.last_name}
                    </strong>
                  </span>
                </Col>
                <Col md="5" xs="12" className="mb-2">
                  <span>
                    Contact
                  </span>
                </Col>
                <Col md="7" xs="12" className="mb-2">
                  <span>
                    Email
                  </span>
                </Col>
                <Col md="5" xs="12" className="mb-3">
                  <span>
                    <strong>
                      {user.contact}
                    </strong>
                  </span>
                </Col>
                <Col md="7" xs="12" className="mb-3">
                  <span>
                    <strong>
                      {user.email}
                    </strong>
                  </span>
                </Col>
                <Col md="5" xs="12">
                  <button
                    type="button"
                    className="account-bt_edit mt-2"
                  >
                    Edit Details
                  </button>
                </Col>
                <Col md="7" xs="12">
                  <button
                    type="button"
                    className="account-bt_edit mt-2"
                    onClick={this.clickLogout}
                  >
                    Logout
                  </button>
                </Col>
              </Row>
            </div>
          </Col>
          {/* <Col md="6" xs="12">
            <div className="account_details-image">
              <img src={account} alt="logo" />
            </div>
          </Col> */}
        </Row>
      </div>
    );

    const accountAddAdress = (
      <div className="div-detail">
        <Row>
          <Col md="6" xs="12">
            <div className="account_edit-text">
              <span>
                CONTACT DETAILS
              </span>
            </div>
          </Col>
          <Col md="6" xs="12">
            <div className="account_edit-text">
              <span>
                ADDRESS DETAILS
              </span>
            </div>
          </Col>
          {
            _.map(this.dataAddAddress, (d) => {
              if (d.name) {
                return (
                  <Col md="6" xs="12">
                    <div className="account_edit-text">
                      <span>
                        {d.title}
                      </span>
                      <input
                        className={d.className}
                        type="text"
                        name={d.name}
                      />
                    </div>
                  </Col>
                );
              }
              return (
                <Col md="6" xs="12" />
              );
            })
          }
          <Col md="12" xs="12">
            <button
              type="button"
              className="account-bt_edit mt-4"
              onClick={() => { this.setState({ isAddress: false }); }}
            >
              Save
            </button>
          </Col>
        </Row>
      </div>
    );
    const addressHistory = (
      <div className="div-detail">
        <Row style={{ width: '100%' }}>
          <div className="address-content">
            <AddressItem
              title="Address 1"
              name="JHOIN"
              address="#03-678 ORCHARD ROAD"
              city="NEW BUILDING"
              country="SIGAPORE 6789"
            />
          </div>
          <div className="address-add">
            <button
              type="button"
              onClick={() => { this.setState({ isAddress: true }); }}
            >
              <strong>
                + ADD ADDRESS
              </strong>
            </button>
          </div>
        </Row>
      </div>
    );
    const orderHistory = (
      <div style={{ width: '100%' }}>
        <OrderHistoryItem />
        <OrderHistoryItem />
      </div>
    );

    return (
      <div className="account">
        <BackgroundVideo urlImage={imageUrl} isVideo={false} />
        <div className="div-image">
          <img loading="lazy" src={tittleAccount} alt="logo" />
          <div className="line-logo" />
        </div>
        <div className="body">
          <div className="header">
            <button
              type="button"
              name="accountDetails"
              onClick={this.onChange}
              className={typeActive === 'accountDetails' ? 'button_homepage active' : 'button_homepage'}
            >
              Account Details
            </button>
            <button
              type="button"
              name="addressBook"
              onClick={this.onChange}
              className={typeActive === 'addressBook' ? 'button_homepage active' : 'button_homepage'}
            >
              Address Book
            </button>
            <button
              type="button"
              name="orderHistory"
              onClick={this.onChange}
              className={typeActive === 'orderHistory' ? 'button_homepage active' : 'button_homepage'}
            >
              Order History
            </button>
          </div>
          <div className="content">
            {
              typeActive === 'accountDetails'
                ? (accountDetails)
                : (typeActive === 'addressBook' && isAddress ? accountAddAdress : (typeActive === 'addressBook' && !isAddress ? addressHistory : orderHistory))
            }
          </div>
          {/* {
            typeActive === 'accountDetails'
              ? (
                <div className="image">
                  <img src={account} alt="account" />
                </div>
              ) : (<div />)
          } */}
        </div>
      </div>
    );
  }
}

Account.propTypes = {
  accountCms: PropTypes.arrayOf(PropTypes.object).isRequired,
  logoutRequest: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

const mapDispatchToProps = {
  logoutRequest,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Account));
