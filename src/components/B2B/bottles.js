import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import titleBottles from '../../image/titleBottles.png';
import bags from '../../image/bags.png';
import { generateUrlWeb, getAltImage } from '../../Redux/Helpers';

class Bottles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: undefined,
      imageUrl: undefined,
    };
  }

  componentDidMount = () => {
    const { bottleCms } = this.props;
    const productBlock = _.find(bottleCms, x => x.type === 'product_block');
    const description = productBlock ? productBlock.value.product.description : undefined;
    const imageUrl = productBlock && productBlock.value.product.images.length > 0 ? productBlock.value.product.images[0].image : undefined;
    this.setState({ description, imageUrl });
  };

  gotoMore = () => {
    this.props.history.push(generateUrlWeb('/b2b/bottles'));
  }

  render() {
    const { isDetail, isShowBag } = this.props;
    const { description, imageUrl } = this.state;
    return (
      <div className="bottles">
        <div className="bg-bottles" />
        <div className="body">
          <img loading="lazy" className="image" src={imageUrl} alt={getAltImage(imageUrl)} />
          <div className="content">
            <div className="div-title">
              <div className="line-title" />
              <img loading="lazy" src={titleBottles} alt="titleBottles" />
            </div>
            <div className="div-text">
              {description}
              <div className="line-bottles" />
            </div>
            <div className="control">
              <div className={isShowBag ? 'div-span' : 'div-span hiden'}>
                <span>
                  Quantity:
                  {' '}
                </span>
                <span className="span-down">
                  <select className="b2b-select">
                    {
                  _.map(new Array(45), (d, index) => (
                    <option>
                      {index}
                    </option>
                  ))
                  }
                  </select>
                </span>
              </div>
              <button
                type="button"
                className={isShowBag ? '' : 'hiden'}
              >
                <img loading="lazy" className="img-bags_button" src={bags} alt="bags" />
              </button>
              <div className="div-button">
                {
                    !isDetail
                      ? (
                        <button
                          type="button"
                          onClick={this.gotoMore}
                        >
                          VIEW MORE
                        </button>
                      )
                      : (<div />)
                  }

              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Bottles.defaultProps = {
  isDetail: false,
  isShowBag: true,
};

Bottles.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  isDetail: PropTypes.string,
  isShowBag: PropTypes.bool,
  bottleCms: PropTypes.arrayOf(PropTypes.object).isRequired,
};
export default withRouter(Bottles);
