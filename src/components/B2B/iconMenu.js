import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import onClickOutside from 'react-onclickoutside';

class IconMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShow: false,
    };
  }

  onOpen = () => {
    this.setState({ isShow: true });
  }

  onClose = () => {
    this.setState({ isShow: false });
  }

  handleClickOutside = () => {
    this.onClose();
  }

  render() {
    const { isShow } = this.state;
    return (
      <div>
        <button
          type="button"
          className="b2b-bt-fix_top b2b-bt-fix_top-left"
          onClick={this.onOpen}
        >
          <i className="fa fa-bars" />
        </button>
        {
          isShow
            ? (
              <div className="b2b-menu">
                {/* <button
                  type="button"
                  className="bt-boder-none"
                  onClick={this.onClose}
                >
                  <strong>
                  X
                  </strong>
                </button> */}
                <div className="b2b-menu-content">
                  <Link to="/">
              Moto
                  </Link>
                  <Link to="/b2b/capsule">
              Capsule
                  </Link>
                  <Link to="/b2b/bottles">
              Bottle
                  </Link>
                  <Link to="/b2b/packaging">
              Packaging
                  </Link>
                  <Link to="/b2b/alcohol">
              Alcohol
                  </Link>
                  <Link to="/b2b/account">
              Profile
                  </Link>
                </div>
              </div>
            ) : (<div />)
        }
      </div>
    );
  }
}

export default onClickOutside(IconMenu);
