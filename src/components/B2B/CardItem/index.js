/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import moment from 'moment';
import _ from 'lodash';
import {
  getUrlGotoProduct, getAltImage, getCmsCommon, getNameFromCommon, generaCurrency, trackGTMRemoveCart, trackGTMAddToCart, generateUrlWeb, sendEngraveBottleClickHotjarEvent,
} from '../../../Redux/Helpers/index';
import icSub from '../../../image/icon/ic-sub.svg';
import icPlus from '../../../image/icon/ic-plus.svg';
import icPen from '../../../image/icon/ic-pen.svg';
import icDelete from '../../../image/icon/icDelete.png';
import BottleCustom from './bottleCustom';
import bottleHomeScent from '../../../image/bottle_home_card.png';
import rollOnBottle from '../../../image/roll_on_bottle.png';
import bottleBundleCreation from '../../../image/bundle_creation_thumnail.jpeg';
import CustomeBottleV3 from '../../CustomeBottleV3';
import { isMobile, isBrowser } from '../../../DetectScreen';

class CardItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: props.data.quantity,
      oldQuantity: props.data.quantity,
      oldData: props.data,
      isOpenCustomBottle: false,
      dataCustom: {
        currentImg: props.data.image,
        nameBottle: props.data.is_display_name ? props.data.name : '',
        font: props.data.font,
        color: props.data.color,
        imagePremade: undefined,
      },
    };
    this.timeOutCall = undefined;
  }


  onChange = (e) => {
    const { value } = e.target;
    const { idCart } = this.props;
    this.setState({ quantity: value });
    const data = _.assign({}, this.props.data);
    data.quantity = parseFloat(value, 10);
    data.total = parseFloat(data.quantity, 10) * parseFloat(data.price, 10);
    if (this.props.updateProductBasket) {
      this.props.updateProductBasket({
        idCart,
        item: data,
      });
    }
  }

  onChangeQuantity = (value, increase) => {
    const { idCart } = this.props;
    const data = _.assign({}, this.props.data);
    data.quantity = parseFloat(value, 10);
    if (this.props.updateProductBasket) {
      this.props.updateProductBasket({
        idCart,
        item: data,
        increase,
      });
    }
    // }, 1000);
    this.setState({ quantity: value });
  }

  onIncreateQuantity = () => {
    const { quantity } = this.state;
    this.onChangeQuantity(quantity + 1, true);
    this.setState({ quantity: quantity + 1 });

    // tracking GTM
    // const data = _.cloneDeep(this.props.data);
    // data.quantity = 1;
    // trackGTMAddToCart(data);
  }

  onDecreateQuantity = () => {
    const { quantity } = this.state;
    if (quantity > 1) {
      this.onChangeQuantity(quantity - 1);
      this.setState({ quantity: quantity - 1 });

      // tracking GTM
      const data = _.cloneDeep(this.props.data);
      data.quantity = 1;
      trackGTMRemoveCart(data);
    }
  }

  onClickDelete = () => {
    if (this.props.deleteProductBasket) {
      const { idCart, data } = this.props;
      this.props.deleteProductBasket({
        idCart,
        item: data,
      });
    }
  }

  onGotoProduct = (isCustome) => {
    const { data, isB2C } = this.props;
    if (!isB2C) {
      return;
    }
    const dataUrl = getUrlGotoProduct(data);
    let urlPro = dataUrl.url;
    const { meta, id, quantity } = data;
    if (meta && meta.url) {
      urlPro = meta.url;
    }
    if (urlPro) {
      const {
        image, name, item, is_black: isBlack, is_display_name: isDisplayName, font, color,
      } = data;
      const custome = dataUrl.isPerfume ? {
        image,
        name: isDisplayName ? name : '',
        isCustome,
        font,
        color,
        idCartItem: id,
        quantity,
      } : undefined;
      this.props.history.push(generateUrlWeb(urlPro), { custome });
      if (this.props.onCloseCard) {
        this.props.onCloseCard();
      }
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { data } = nextProps;
    const objectReturn = {};
    if (data && (data.quantity !== prevState.oldQuantity || data !== prevState.oldData)) {
      _.assign(objectReturn, { quantity: data.quantity, oldQuantity: data.quantity, oldData: data });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  getCountry = (code) => {
    const { listCountry } = this.props;
    if (!code || !listCountry) {
      return '';
    }
    const ele = _.find(listCountry, x => x.code === code);
    return ele ? ele.name : '';
  }

  getTimeEmail = (epoch) => {
    if (!epoch) {
      return '';
    }
    return moment.unix(epoch).format('DD/MM/YYYY');
  }

  handleCropImage = (img) => {
    const { dataCustom } = this.state;
    dataCustom.currentImg = img;
    this.setState({ dataCustom });
  };

  onSaveCustomer = (data) => {
    const { dataCustom } = this.state;
    const {
      image, name, color, font, imagePremade,
    } = data;
    dataCustom.color = color;
    dataCustom.font = font;
    dataCustom.imagePremade = imagePremade;
    if (_.isEmpty(image)) {
      dataCustom.currentImg = undefined;
      dataCustom.nameBottle = name;
      this.forceUpdate();
      return;
    }
    dataCustom.currentImg = image;
    dataCustom.nameBottle = name;
    this.forceUpdate();
  }

  prepareDataItemCart = async (dataSelect) => {
    const {
      item: itemCombo, quantity, meta, id: idItem,
    } = dataSelect;
    const { dataCustom } = this.state;
    const bottleImage = dataCustom.currentImg
      ? await fetch(dataCustom.currentImg).then(r => r.blob())
      : undefined;
    if (itemCombo) {
      const { price, id } = itemCombo;
      const data = {
        id: idItem,
        item: id,
        font: dataCustom.font,
        color: dataCustom.color,
        imagePremade: dataCustom.imagePremade,
        is_display_name: (!bottleImage && !!dataCustom.nameBottle) || !!dataCustom.nameBottle,
        is_customized: !!bottleImage || !!dataCustom.imagePremade,
        name: dataCustom.nameBottle ? dataCustom.nameBottle : dataSelect.name,
        price,
        quantity,
        file: bottleImage
          ? new File([bottleImage], 'product.png')
          : undefined,
        meta,
      };
      const dataTemp = {
        idCart: this.props.idCart,
        item: data,
      };
      return dataTemp;
    }
  };

  handleUpdateCart = async (dataSelect, index = 0) => {
    const {
      updateProductBasket,
    } = this.props;
    const dataTemp = await this.prepareDataItemCart(dataSelect, index);
    if (this.props.idCart) {
      _.assign(dataTemp, { isUpdateFromProductDetail: true });
      updateProductBasket(dataTemp);
    }
  }

  onClickUpdatetoCart = async () => {
    const { data } = this.props;
    this.handleUpdateCart(data);
  }

  render() {
    const {
      data, isCheckOut, cms, isCheckOutClub, isCheckOutGift, isPaySuccess,
    } = this.props;
    const { quantity, isOpenCustomBottle, dataCustom } = this.state;
    const {
      item, total, combo, image, additional_fee: additionalFee,
      image_display: imgDisplay, meta, name, is_customized: isCustomized,
      is_display_name: isDisplayName, is_black: isBlack, font, color, is_free_gift: isFree,
      is_free, external_product: externalProduct,
    } = data;
    const isGift = item.product.type.name === 'Gift';
    const isShowBottle = ((item.product.type.name === 'Creation' && isCustomized) || item.product.type.name === 'Perfume' || item.product.type.name === 'bundle_creation' || item.product.type.name === 'perfume_diy' || item.product.type.name === 'Elixir' || (isGift && isCustomized)) && item.is_sample === false;
    const isBundleCreation = item.product.type.name === 'bundle_creation';
    const isHomeScent = item.product.type.name === 'home_scents' && combo && combo.length > 0;
    const isRollOn = (item.product.type.name === 'Perfume' || item.product.type.name === 'Scent') && item.is_sample === true;
    const combos = combo ? _.filter(combo, x => x.product.type === 'Scent') : [];
    const itemBottle = _.find(combo, x => x.product.type === 'Bottle');
    const arrayThumbs = [];
    if (itemBottle && itemBottle.product.images) {
      _.forEach(itemBottle.product.images, (x) => {
        if (x && x.type === 'suggestion') {
          arrayThumbs.push(x);
        }
      });
    }
    const cmsCommon = getCmsCommon(cms);
    const customDesignBt = getNameFromCommon(cmsCommon, 'CUSTOM_DESIGN');
    const editCustomBt = getNameFromCommon(cmsCommon, 'Edit_Customization');
    const removeBt = getNameFromCommon(cmsCommon, 'Remove');
    const textBt = getNameFromCommon(cmsCommon, 'Text');
    const imageTextBt = getNameFromCommon(cmsCommon, 'Image_Text');
    const freeBt = getNameFromCommon(cmsCommon, 'FREE');
    const eauBt = getNameFromCommon(cmsCommon, 'Eau_de_parfum');
    const mlBt = getNameFromCommon(cmsCommon, '25ml');
    const mouthBt = getNameFromCommon(cmsCommon, 'Month');

    const isNotShowSub = quantity < 2 || is_free || isPaySuccess;
    const isNotShowPlus = isFree || is_free || isPaySuccess;
    const receiver = meta && meta.receiver ? meta.receiver : {};
    const isImageText = !!image;
    const isText = name && !image;
    const htmlGift = (
      <React.Fragment>
        <span className="sp-text mt-2">
          {`To: ${receiver.name}`}
        </span>
        <span className={receiver.country && !isGift ? 'sp-text' : 'hidden'}>
          {`Country: ${this.getCountry(receiver.country)}`}
        </span>
        <span className="sp-text">
          {`Emailed on: ${this.getTimeEmail(receiver.date_sent)}`}
        </span>
      </React.Fragment>

    );
    // const imgDisplay = imageBottle ? imageBottle.image : '';
    const htmlRollOn = (
      <div className="div-col div-custome mt-0">
        <div className="div-col title-name">
          {
            _.map(combos, (d, index) => (
              <span>
                <i>{`${index > 0 ? '& ' : ''}${d.name}`}</i>
              </span>
            ))
          }
        </div>
      </div>
    );

    const htmlCustome = (
      <div className="div-col div-custome">
        {/* <div className="div-col title-name">
          {
            _.map(combos, (d, index) => (
              <span>
                <i>{`${index > 0 ? '& ' : ''}${d.name}`}</i>
              </span>
            ))
          }
        </div> */}
        <span className={isRollOn ? 'hidden' : 'sp-text'}>
          {data && (!isShowBottle || (isShowBottle && isDisplayName) || externalProduct?.name) ? (externalProduct?.name ? externalProduct?.name : data.name) : ''}
        </span>
        <div className="div-edit">
          <button
            // onClick={() => this.onGotoProduct(true)}
            data-gtmtracking="customize-bottle-step-1"
            onClick={() => {
              if (this.props.onOpenCustomBottle) {
                this.props.onOpenCustomBottle();
              }
              sendEngraveBottleClickHotjarEvent();
              this.setState({ isOpenCustomBottle: true });
            }}
            type="button"
            className={isPaySuccess ? 'hidden' : 'button-bg__none'}
          >
            <img loading="lazy" data-gtmtracking="customize-bottle-step-1" src={icPen} alt="icon" className="mr-1" />
            {editCustomBt}
          </button>
        </div>

      </div>
    );
    const htmlCart = (
      <React.Fragment>
        <div className="w-25 div-col items-center justify-between image-custome">

          {
            isBundleCreation ? (
              <div style={{ position: 'relative', display: 'flex', justifyContent: 'center' }}>
                <img
                  loading="lazy"
                  src={bottleBundleCreation}
                  alt={getAltImage(imgDisplay)}
                  style={{
                    width: isMobile ? '100%' : '80%',
                    cursor: 'pointer',
                    objectFit: 'contain',
                    height: isMobile ? '100px' : '90px',
                  }}
                  onClick={() => this.onGotoProduct(false)}
                />
              </div>
            ) : (isShowBottle || isHomeScent) ? (
              <BottleCustom
                isImageText={isImageText}
                onGotoProduct={() => this.onGotoProduct(false)}
                image={image}
                isBlack={isBlack}
                font={font}
                color={color}
                eauBt={eauBt}
                mlBt={mlBt}
                isDisplayName={isDisplayName}
                name={name}
                combos={combos}
                bottleImage={isHomeScent ? bottleHomeScent : undefined}
              />
            ) : isRollOn ? (
              <React.Fragment>
                <div style={{ position: 'relative', display: 'flex', justifyContent: 'center' }}>
                  <img
                    loading="lazy"
                    src={rollOnBottle}
                    alt={getAltImage(imgDisplay)}
                    style={{
                      width: isMobile ? '100%' : '80%',
                      cursor: 'pointer',
                      objectFit: 'contain',
                      height: isMobile ? '100px' : '90px',
                    }}
                    onClick={() => this.onGotoProduct(false)}
                  />
                </div>
              </React.Fragment>
            ) : (
              <img
                loading="lazy"
                src={imgDisplay}
                alt={getAltImage(imgDisplay)}
                style={{
                  width: '80%',
                  cursor: 'pointer',
                  objectFit: 'contain',
                  height: '131px',
                }}
                onClick={() => this.onGotoProduct(false)}
              />
            )
          }
        </div>
        <div className="w-75 div-col div-content">
          <div className="div-header">
            <span className="sp-title">
              {item.product.type.alt_name}
            </span>
            {
              isMobile || isCheckOut ? (
                <div />
              )
                : (
                  <div className="div-row div-remove justify-between items-center">
                    {
                      isCheckOutClub || isCheckOutGift || isGift ? (
                        <div style={{ width: '89px' }} />
                      ) : (
                        <div className={isCheckOutClub || isCheckOutGift || isGift ? 'hidden' : 'div-button'}>
                          <button
                            disabled={isNotShowSub}
                            type="button"
                            className="button-bg__none pr-4"
                            onClick={this.onDecreateQuantity}
                          >
                            {
                              isNotShowSub
                                ? (
                                  <div style={{ width: '11px' }} />
                                )
                                : (
                                  <img loading="lazy" src={icSub} alt="icSub" />
                                )
                            }
                          </button>
                          <span>
                            {quantity}
                          </span>
                          <button
                            type="button"
                            disabled={isNotShowPlus}
                            className="button-bg__none pl-4"
                            onClick={this.onIncreateQuantity}
                          >
                            {
                              isNotShowPlus
                                ? (
                                  <div style={{ width: '11px' }} />
                                )
                                : (
                                  <img loading="lazy" src={icPlus} alt="icPlus" />
                                )
                            }
                          </button>
                        </div>
                      )
                    }

                    <div className={isCheckOut ? 'hidden' : 'sp-text-price'}>
                      <span>
                        <b>{`${generaCurrency(total)}${isCheckOutClub ? `/${mouthBt}` : ''}`}</b>
                      </span>
                    </div>

                  </div>
                )
            }
            <button
              onClick={this.onClickDelete}
              type="button"
              className={isCheckOut || isCheckOutClub || isPaySuccess ? 'hidden' : 'bt-delete button-bg__none'}
            >
              <img loading="lazy" src={icDelete} alt="delete" />
            </button>
          </div>
          <div className={isShowBottle ? 'div-col title-name' : 'hidden'}>
            {
              _.map(combos, (d, index) => (
                <span>
                  <i>{`${index > 0 ? '& ' : ''}${d.name}`}</i>
                </span>
              ))
            }
          </div>
          <span className={isRollOn || isShowBottle ? 'hidden' : 'sp-text'}>
            {data && (!isShowBottle || (isShowBottle && isDisplayName)) ? data.name : ''}
          </span>
          {/* <span className={isCheckOut ? 'hidden' : 'sp-text'}>
            <b>{`${generaCurrency(total)}${isCheckOutClub ? `/${mouthBt}` : ''}`}</b>
          </span> */}
          {isShowBottle ? htmlCustome : ''}
          {isGift ? htmlGift : ''}
          {isRollOn && htmlRollOn}
          {
            isBrowser || isCheckOut ? (
              <div />
            )
              : (
                <div className="div-row div-remove mt-4 w-100 justify-between items-center">
                  <div className={isCheckOutClub || isCheckOutGift || isGift ? 'hidden' : 'div-button'}>
                    <button
                      disabled={isNotShowSub}
                      type="button"
                      className="button-bg__none pr-4"
                      onClick={this.onDecreateQuantity}
                    >
                      {
                        isNotShowSub
                          ? (
                            <div style={{ width: '11px' }} />
                          )
                          : (
                            <img loading="lazy" src={icSub} alt="icSub" />
                          )
                      }
                    </button>
                    <span>
                      {quantity}
                    </span>
                    <button
                      type="button"
                      disabled={isNotShowPlus}
                      className="button-bg__none pl-4"
                      onClick={this.onIncreateQuantity}
                    >
                      {
                        isNotShowPlus
                          ? (
                            <div style={{ width: '11px' }} />
                          )
                          : (
                            <img loading="lazy" src={icPlus} alt="icPlus" />
                          )
                      }
                    </button>
                  </div>
                  <span className={isCheckOut ? 'hidden' : 'sp-text'}>
                    <b>{`${generaCurrency(total)}${isCheckOutClub ? `/${mouthBt}` : ''}`}</b>
                  </span>
                </div>
              )
          }
        </div>
      </React.Fragment>
    );
    const htmlCheckOutWeb = (
      <div className={`div-row div-checkout-items-web ${isCheckOutClub ? 'div-checkout-items-web-club' : ''}`}>
        <div className="div-info div-row">
          {htmlCart}
        </div>
        <div className="div-price div-col justify-between">
          <div className="div-row div-remove justify-between w-100">
            <div className="div-button" style={isCheckOutGift || isGift ? { border: 'none' } : {}}>
              {
                isCheckOutGift || isGift ? (<div />) : (
                  <React.Fragment>
                    <button
                      disabled={isNotShowSub}
                      type="button"
                      className="button-bg__none pr-3"
                      onClick={this.onDecreateQuantity}
                    >
                      {
                  isNotShowSub
                    ? (
                      <div style={{ width: '11px' }} />
                    )
                    : (
                      <img loading="lazy" src={icSub} alt="icSub" />
                    )
                }
                    </button>
                    <span>
                      {quantity}
                    </span>
                    <button
                      type="button"
                      disabled={isNotShowPlus}
                      className="button-bg__none pl-3"
                      onClick={this.onIncreateQuantity}
                    >
                      {
                  isNotShowPlus
                    ? (
                      <div style={{ width: '11px' }} />
                    )
                    : (
                      <img loading="lazy" src={icPlus} alt="icPlus" />
                    )
                }
                    </button>
                  </React.Fragment>
                )
              }
            </div>
            <span>
              {`${generaCurrency(total)}${isCheckOutClub ? `/${mouthBt}` : ''}`}
            </span>
          </div>
          <div className={isCheckOutClub ? 'hidden' : 'div-bt-remove'}>
            <button
              onClick={this.onClickDelete}
              type="button"
              className="button-bg__none pa0"
              style={{
                float: 'right',
              }}
            >
              {removeBt}
            </button>
          </div>
        </div>
      </div>
    );
    return (
      <div className="new-cartItem div-row" style={this.props.style}>
        {
          isOpenCustomBottle ? (
            <CustomeBottleV3
              cmsCommon={cmsCommon}
              arrayThumbs={arrayThumbs}
              handleCropImage={this.handleCropImage}
              currentImg={dataCustom.currentImg}
              nameBottle={dataCustom.nameBottle}
              font={dataCustom.font}
              color={dataCustom.color}
              closePopUp={() => {
                if (this.props.onCloseCustomBottle) {
                  this.props.onCloseCustomBottle();
                }
                this.setState({ isOpenCustomBottle: false });
              }
              }
              onSave={this.onSaveCustomer}
              // itemBottle={itemBottle}
              // cmsTextBlocks={textBlock}
              name1={combos && combos.length > 0 ? combos[0].name : ''}
              name2={combos && combos.length > 1 ? combos[1].name : ''}
              onClickUpdatetoCart={this.onClickUpdatetoCart}
              idCartItem={this.props.idCart}
            />
          ) : (<div />)
        }
        {isCheckOut ? htmlCheckOutWeb : htmlCart}
      </div>
    );
  }
}

CardItem.defaultProps = {
  isPaySuccess: false,
};

CardItem.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    total: PropTypes.number,
    image_urls: PropTypes.string,
    quantity: PropTypes.number,
    original_quantity: PropTypes.number,
    id: PropTypes.string,
    item: PropTypes.string,
  }).isRequired,
  deleteProductBasket: PropTypes.func.isRequired,
  updateProductBasket: PropTypes.func.isRequired,
  onCloseCard: PropTypes.func.isRequired,
  idCart: PropTypes.number.isRequired,
  isB2C: PropTypes.bool.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  isCheckOut: PropTypes.bool.isRequired,
  isCheckOutClub: PropTypes.bool.isRequired,
  cms: PropTypes.shape().isRequired,
  style: PropTypes.shape().isRequired,
  listCountry: PropTypes.arrayOf().isRequired,
  isPaySuccess: PropTypes.bool,
};

export default withRouter(CardItem);
