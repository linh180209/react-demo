import React, { Component } from 'react';
import PropTypes from 'prop-types';
import AvatarEditor from 'react-avatar-editor';
import { LazyLoadImage } from 'react-lazy-load-image-component';

import '../../../styles/bottle_custom.scss';
import { COLOR_CUSTOME, FONT_CUSTOME } from '../../../constants';
import icBottleImage from '../../../image/bottle-new.png';
// import bottleResultGift from '../../../image/bottle-custome.gif';
import icBottle from '../../../image/img-bottle.png';
import logoBlack from '../../../image/icon/logo-header-black.svg';
import logoWhite from '../../../image/icon/logo-header-white.svg';
import logoGold from '../../../image/icon/logo-header-gold.svg';
import icBgImage from '../../../image/icon/img-none-select.svg';
import icDrop from '../../../image/icon/drop-select.svg';
import icLine from '../../../image/icon/lines.svg';
import { isMobile } from '../../../DetectScreen';

class BottleCustom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowLine: false,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      let widthImage = -1;
      const { ratioHeight } = this.props;

      const imgBottleId = document.getElementById('img-bottle');
      if (imgBottleId) {
        widthImage = 248 * (imgBottleId.clientHeight / 1000);
      }
      const heightBottle = ratioHeight ? parseInt(window.innerHeight * ratioHeight, 10) : 0;
      this.setState({
        widthImage, heightBottle,
      });
    }, 500);
  }

  onChangeImage = () => {
    const { isShowLine } = this.state;
    if (isShowLine) {
      return;
    }
    this.setState({ isShowLine: true });
    setTimeout(() => {
      this.setState({ isShowLine: false });
    }, 3000);
  }

  render() {
    const {
      isShowLine, widthImage, heightBottle,
    } = this.state;
    const {
      isCustomeV3, isImageText, onGotoProduct, image, isBlack, eauBt, mlBt, isDisplayName, name, combos,
      classWrap, classCustomOnly, classTextIngredients, classImageName, classImageLogo,
      classDesCustom, classSizeCustom, classNameCustom, isPersonalResult, color, font,
      uploadImage, scale, isResponsive, heightDefault, isDisableNameScent, name1, name2,
    } = this.props;
    let heightCrop = 0;
    let widthCrop = 0;
    const selectImageId = document.getElementById('select-image');
    if (selectImageId) {
      heightCrop = selectImageId.clientHeight;
      widthCrop = selectImageId.clientWidth;
    }
    const isOnlyName = name && !image && !uploadImage;
    return (
      <div
        className={`div-image-bottle-custom ${classWrap}`}
        onClick={onGotoProduct}
        style={heightBottle ? { height: `${heightBottle}px` } : heightDefault ? { height: heightDefault } : {}}
      >
        {
          isCustomeV3 ? (
            <React.Fragment>
              <LazyLoadImage id="img-bottle" className="img-bottle" src={isOnlyName ? icBottleImage : icBottle} alt="bottle" style={widthImage > 0 && isResponsive ? { width: `${widthImage}px` } : {}} />
              {/* <img id="img-bottle" className="img-bottle" src={isOnlyName ? icBottleImage : icBottle} alt="bottle" style={widthImage > 0 && isResponsive ? { width: `${widthImage}px` } : {}} /> */}
              {
                uploadImage ? (
                  <div
                    id="select-image"
                    className="div-select-image"
                    onMouseEnter={() => this.setState({ isShowLine: true })}
                    onMouseLeave={() => this.setState({ isShowLine: false })}
                    style={isMobile ? { borderRadius: 30 } : {}}
                  >
                    <AvatarEditor
                      ref={this.props.setEditorRef}
                      image={uploadImage}
                      width={isMobile ? widthCrop : (this.props.withCropImage ? this.props.withCropImage : window.innerWidth > 1440 ? 147 : 102)}
                      height={isMobile ? heightCrop : (this.props.heightCropImage ? this.props.heightCropImage : window.innerWidth > 1440 ? 460 : 332)}
                      border={0}
                      borderRadius={isMobile ? 30 : 20}
                      color={[255, 255, 255, 0.6]} // RGBA
                      scale={scale}
                      rotate={0}
                      onImageChange={this.onChangeImage}
                    />
                    <img loading="lazy" src={icDrop} className={isShowLine ? 'hidden' : 'drop-suggestion animated faster fadeIn'} alt="drop" />
                    <div className={isShowLine ? 'div-ver left animated faster fadeIn' : 'hidden'} />
                    <div className={isShowLine ? 'div-ver right animated faster fadeIn' : 'hidden'} />
                    <div className={isShowLine ? 'div-hor top animated faster fadeIn' : 'hidden'} />
                    <div className={isShowLine ? 'div-hor bottom animated faster fadeIn' : 'hidden'} />
                  </div>

                ) : (
                  <img
                    loading="lazy"
                    className={`img-name ${classImageName} ${isOnlyName ? 'hidden' : ''}`}
                    src={image || icBgImage}
                    alt="name"
                  />
                )
              }
              {
                (isOnlyName)
                  ? (
                    <React.Fragment>
                      <span className={isDisplayName ? `custome-only-name ${classCustomOnly}` : 'hidden'} style={{ fontFamily: font, color: color === COLOR_CUSTOME.GOLD ? '#B79B53' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#000000' }}>
                        {name}
                      </span>
                      <div className={isDisplayName ? `div-ingredients custome-only-name-v3 ${classTextIngredients}` : 'hidden'} style={{ color: color === COLOR_CUSTOME.GOLD ? '#B79B53' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#000000' }}>
                        <span style={{ color: color === COLOR_CUSTOME.BLACK ? '#000000' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#B79B53' }}>
                          {name1}
                        </span>
                        <span
                          style={{ color: color === COLOR_CUSTOME.BLACK ? '#000000' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#B79B53' }}
                          className={name1 && name2 ? '' : 'hidden'}
                        >
                          X
                        </span>
                        <span
                          style={{ color: color === COLOR_CUSTOME.BLACK ? '#000000' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#B79B53' }}
                        >
                          {name2}
                        </span>
                      </div>
                    </React.Fragment>
                  )
                  : (
                    <React.Fragment>
                      <img
                        loading="lazy"
                        src={color === COLOR_CUSTOME.BLACK ? logoBlack : color === COLOR_CUSTOME.WHITE ? logoWhite : logoGold}
                        alt=""
                        className={`image-logo  animated faster fadeIn ${classImageLogo}`}
                      />
                      <span className={`text-des-custome animated faster fadeIn ${classDesCustom}`} style={{ color: color === COLOR_CUSTOME.BLACK ? '#000000' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#B79B53' }}>
                        {eauBt}
                      </span>
                      <span className={`text-size-custome animated faster fadeIn ${classSizeCustom}`} style={{ color: color === COLOR_CUSTOME.BLACK ? '#000000' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#B79B53' }}>
                        {mlBt}
                      </span>
                      {/* <img className="img-custome" src={image} alt="image-custome" /> */}
                      <span className={`custome-name ${classNameCustom}`} style={{ fontFamily: font, color: color === COLOR_CUSTOME.BLACK ? '#000000' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#B79B53' }}>
                        {isDisplayName ? name : ''}
                      </span>
                    </React.Fragment>
                  )
              }
            </React.Fragment>
          ) : isImageText
            ? (
              <React.Fragment>
                <img loading="lazy" className="img-bottle" src={icBottle} alt="bottle" />
                <img
                  loading="lazy"
                  className={`img-name ${classImageName}`}
                  src={image || icBgImage}
                  alt="name"
                />
                <img
                  loading="lazy"
                  src={color === COLOR_CUSTOME.BLACK ? logoBlack : color === COLOR_CUSTOME.WHITE ? logoWhite : logoGold}
                  alt=""
                  className={`image-logo  animated faster fadeIn ${classImageLogo}`}
                />
                <span className={`text-des-custome animated faster fadeIn ${classDesCustom}`} style={{ color: color === COLOR_CUSTOME.BLACK ? '#000000' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#B79B53' }}>
                  {eauBt}
                </span>
                <span className={`text-size-custome animated faster fadeIn ${classSizeCustom}`} style={{ color: color === COLOR_CUSTOME.BLACK ? '#000000' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#B79B53' }}>
                  {mlBt}
                </span>
                {/* <img className="img-custome" src={image} alt="image-custome" /> */}
                <span className={`custome-name ${classNameCustom}`} style={{ fontFamily: font, color: color === COLOR_CUSTOME.BLACK ? '#000000' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#B79B53' }}>
                  {isDisplayName ? name : ''}
                </span>
              </React.Fragment>
            ) : (
              <React.Fragment>
                {/* <img className="img-bottle" src={isPersonalResult ? bottleResultGift : icBottleImage} alt="bottle" /> */}
                <img loading="lazy" className="img-bottle" src={this.props.bottleImage || icBottleImage} alt="bottle" />
                <span className={isDisableNameScent ? 'hidden' : isDisplayName ? `custome-only-name ${classCustomOnly}` : 'hidden'} style={{ fontFamily: font, color: color === COLOR_CUSTOME.GOLD ? '#B79B53' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#000000' }}>
                  {name}
                </span>
                {
                  !isDisableNameScent ? (
                    <div className={isDisplayName && isOnlyName ? `div-ingredients ${classTextIngredients}` : `div-ingredients onMid ${classTextIngredients}`} style={{ color: color === COLOR_CUSTOME.GOLD ? '#B79B53' : color === COLOR_CUSTOME.WHITE ? '#ffffff' : '#000000' }}>
                      <span>
                        {combos && combos.length > 0 ? combos[0].name : ''}
                      </span>
                      <span className={combos && combos.length > 1 ? '' : 'hidden'}>
                        X
                      </span>
                      <span>
                        {combos && combos.length > 1 ? combos[1].name : ''}
                      </span>
                    </div>
                  ) : null
                }

              </React.Fragment>
            )
        }
      </div>
    );
  }
}
BottleCustom.defaultProps = {
  isPersonalResult: false,
  bottleImage: undefined,
};

BottleCustom.propTypes = {
  isPersonalResult: PropTypes.bool,
  bottleImage: PropTypes.string,
};

export default BottleCustom;
