import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import logomark from '../../image/icon/logo-menu.png';
import { generateUrlWeb } from '../../Redux/Helpers';


class ItemLogo extends Component {
  render() {
    return (
      <div className="b2b-bt-fix_top b2b-bt-fix_top-center" style={{ top: '40px' }}>
        <button
          type="button"
          style={{
            background: 'transparent',
            border: 'none',
          }}
          onClick={() => { this.props.history.push(generateUrlWeb('/b2b/landing')); }}
        >
          <img
            loading="lazy"
            src={logomark}
            alt="logomark"
            style={{
              width: '80px',
            }}
          />
        </button>
      </div>
    );
  }
}

ItemLogo.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default withRouter(ItemLogo);
