import React, { Component } from 'react';
import PropTypes from 'prop-types';

import logo from '../../image/logo_21.png';
import BackGroundVideo from '../backgroundVideo';
import bg from '../../image/bg6.svg';
import IconMenu from './iconMenu';
import IconBags from './iconBags';
import ItemLogo from './iconLogo';

class Home extends Component {
  render() {
    const { imageBg } = this.props;
    return (
      <div className="b2blandingHome">
        <BackGroundVideo urlImage={imageBg} isVideo={false} />
        <img loading="lazy" className="logo" src={logo} alt="logo" />
        <IconMenu />
        {/* <ItemLogo /> */}
        {/* <IconBags className="b2b-bt-fix_top-right" /> */}
      </div>
    );
  }
}

Home.propTypes = {
  imageBg: PropTypes.string.isRequired,
};

export default Home;
