import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import InputEdit from './inputEdit';

class CoBlockData extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isEdit: false,
    };
  }

  onClickEdit = () => {
    this.setState(prev => ({ isEdit: !prev.isEdit }));
  }

  render() {
    const { title, datas } = this.props;
    const { isEdit } = this.state;
    return (
      <div className="co-block-data mt-4">
        <span>
          <strong>
            {title}
          </strong>
        </span>
        <div className="content mt-2">
          <div className="text">
            {
              _.map(datas, d => (isEdit ? (<InputEdit />)
                : (
                  <span>
                    {d}
                  </span>
                )))
            }
          </div>
          <button
            type="button"
            className="checkout-bt"
            onClick={this.onClickEdit}
          >
            {isEdit ? 'Save' : 'Edit'}
          </button>
        </div>
      </div>
    );
  }
}

CoBlockData.defaultProps = {
  title: 'AAA',
  datas: [
    'BBB',
    'CCC',
    'DDD',
  ],
};

CoBlockData.propTypes = {
  title: PropTypes.string,
  datas: PropTypes.arrayOf(PropTypes.string),
};
export default CoBlockData;
