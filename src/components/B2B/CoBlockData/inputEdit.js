import React, { Component } from 'react';
import PropTypes from 'prop-types';

class InputEdit extends Component {
  render() {
    const { title, defaultValue, name } = this.props;
    return (
      <div className="checkout-input-edit mr-3">
        <span>
          {title}
        </span>
        <input
          type="text"
          name={name}
          defaultValue={defaultValue}
        />
      </div>
    );
  }
}

InputEdit.defaultProps = {
  title: 'Title',
  defaultValue: 'Lorem ipsum dolor',
  name: 'name',
};

InputEdit.propTypes = {
  title: PropTypes.string,
  defaultValue: PropTypes.string,
  name: PropTypes.string,
};
export default InputEdit;
