import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import alcohol from '../../image/alcohol.png';
import titleAlcohol from '../../image/titleAlcohol.png';
import bags from '../../image/bags.png';
import { generateUrlWeb, getAltImage } from '../../Redux/Helpers';

class Alcohol extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: undefined,
      imageUrl: undefined,
    };
  }

  componentDidMount = () => {
    const { alcoholCms } = this.props;
    const productBlock = _.find(alcoholCms, x => x.type === 'product_block');
    const description = productBlock ? productBlock.value.product.description : undefined;
    const imageUrl = productBlock && productBlock.value.product.images.length > 0 ? productBlock.value.product.images[0].image : undefined;
    this.setState({ description, imageUrl });
  };

  onGotoMore = () => {
    this.props.history.push(generateUrlWeb('/b2b/alcohol'));
  }

  render() {
    const { isDetail, isShowBag } = this.props;
    const { description, imageUrl } = this.state;
    return (
      <div className="alcohol">
        <div className="body">
          <img loading="lazy" className="image" src={imageUrl} alt={getAltImage()} />
          <div className="content">
            <div className="div-title">
              <div className="div-line" />
              <img loading="lazy" src={titleAlcohol} alt="titleBottles" />
            </div>
            <div className="div-text">
              {description}
              <div className="line-bottles" />
            </div>
            <div className="control">
              <span className={isShowBag ? 'mb-2' : 'hiden'}>
                Vol. 1 Liters
              </span>
              <div className={isShowBag ? 'div-span' : 'div-span hiden'}>
                <span>
                  Quantity:
                  {' '}
                </span>
                <span className="span-down">
                  <select className="b2b-select">
                    {
                  _.map(new Array(45), (d, index) => (
                    <option>
                      {index}
                    </option>
                  ))
                  }
                  </select>
                </span>
              </div>
              <button
                type="button"
                className={isShowBag ? '' : 'hiden'}
              >
                <img loading="lazy" className="img-bags_button" src={bags} alt="bags" />
              </button>
              {
                !isDetail
                  ? (
                    <div className="div-button">
                      <button
                        type="button"
                        onClick={this.onGotoMore}
                      >
                        VIEW MORE
                      </button>
                    </div>
                  ) : (<div />)
              }

            </div>
          </div>
        </div>
      </div>
    );
  }
}

Alcohol.defaultProps = {
  isDetail: false,
  isShowBag: true,
};

Alcohol.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  isDetail: PropTypes.func,
  isShowBag: PropTypes.bool,
  alcoholCms: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default withRouter(Alcohol);
