import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import BackGroundVideo from '../backgroundVideo';
import { convertHeaderToSize, getAltImage } from '../../Redux/Helpers';

class Information extends Component {
  constructor(props) {
    super(props);
    this.state = {
      styleHeader: undefined,
      headerText: undefined,
      textInfo: undefined,
      urlImage1: undefined,
      urlImage2: undefined,
      bgImage: undefined,
    };
  }

  componentDidMount = () => {
    const { introCms } = this.props;
    const headerBlock = _.find(introCms, x => (x.type === 'header_block'));
    const textBlock = _.find(introCms, x => (x.type === 'text_block'));
    const imageBlock = _.filter(introCms, x => (x.type === 'image_block'));
    const backgroundBlock = _.find(imageBlock, x => x.value.caption === 'background');
    const imageDatas = _.filter(imageBlock, x => x.value.caption.includes('imageInfo'));
    const headerText = headerBlock ? headerBlock.value.header_text : undefined;
    const headerSize = headerBlock ? headerBlock.value.header_size : undefined;
    const styleHeader = {
      fontSize: headerSize ? convertHeaderToSize(headerSize) : undefined,
    };
    const textInfo = textBlock ? textBlock.value : '';
    const urlImage1 = imageDatas && imageDatas.length > 0 ? imageDatas[0].value.image : undefined;
    const urlImage2 = imageDatas && imageDatas.length > 1 ? imageDatas[1].value.image : undefined;
    const bgImage = backgroundBlock ? backgroundBlock.value.image : undefined;
    this.setState({
      styleHeader, headerText, textInfo, urlImage1, urlImage2, bgImage,
    });
  };

  render() {
    const {
      styleHeader, headerText, textInfo, urlImage1, urlImage2, bgImage,
    } = this.state;
    return (
      <div id="information" className="information">
        <BackGroundVideo urlImage={bgImage} isVideo={false} />
        <div className="block1">
          <div className="div-img">
            <img loading="lazy" src={urlImage1} alt={getAltImage(urlImage1)} />
          </div>
          <div className="div-text">
            <div>
              <strong style={styleHeader}>
                {headerText}
              </strong>
              {' '}
              {textInfo}
            </div>
            <div className="div-line">
              <div />
            </div>
          </div>
        </div>
        <div className="block2">
          <img loading="lazy" src={urlImage2} alt={getAltImage(urlImage2)} />
        </div>
      </div>
    );
  }
}

Information.propTypes = {
  introCms: PropTypes.shape(PropTypes.object).isRequired,
};

export default Information;
