import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { withRouter } from 'react-router-dom';
import CapsuleItem from '../CapsuleItem';
import bg from '../../image/bg7.svg';
import capsules from '../../image/capsules.png';
import { convertHeaderToSize, generateUrlWeb } from '../../Redux/Helpers';

class Capsule extends Component {
  constructor(props) {
    super(props);
    this.state = {
      styleHeader: undefined,
      headerText: undefined,
      datas: [],
    };
  }

  componentDidMount = () => {
    const { capsuleCms } = this.props;
    const headerBlock = _.find(capsuleCms, x => (x.type === 'header_block'));
    const productBlock = _.filter(capsuleCms, x => (x.type === 'product_block'));
    const headerText = headerBlock ? headerBlock.value.header_text : undefined;
    const headerSize = headerBlock ? headerBlock.value.header_size : undefined;
    const styleHeader = {
      fontSize: headerSize ? convertHeaderToSize(headerSize) : undefined,
    };
    const datas = [];
    if (productBlock) {
      _.forEach(productBlock, (p) => {
        const capsule = p.value.product;
        if (capsule) {
          datas.push({
            urlImage: capsule.images ? capsule.images[0].image : '',
            description: capsule.description,
            width: 180,
            height: 350,
            name: capsule.name,
          });
        }
      });
    }
    this.setState({ headerText, styleHeader, datas });
  };


  onGotoMore = () => {
    this.props.history.push(generateUrlWeb('/b2b/capsule'));
  }

  render() {
    const { isShowBag } = this.props;
    const { headerText, styleHeader, datas } = this.state;
    return (
      <div id="capsure" className="capsure">
        <div className="div-bg-text">
          <div className="div-line1" />
          <img loading="lazy" src={capsules} alt="capsule" />
          <div className="div-line2" />
        </div>
        <div className="body">
          <div className="b2btitle" style={styleHeader}>
            {headerText}
          </div>
          <div className="content">
            <div className="content-capsule">
              {
                _.map(datas, d => (
                  <CapsuleItem
                    width={d.width}
                    height={d.height}
                    urlImage={d.urlImage}
                    isShowBag={isShowBag}
                    description={d.description}
                    name={d.name}
                  />
                ))
              }
            </div>
            <div className="div-button">
              <button
                type="button"
                onClick={this.onGotoMore}
              >
                VIEW MORE
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Capsule.defaultProps = {
};

Capsule.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  isShowBag: PropTypes.bool.isRequired,
  capsuleCms: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default withRouter(Capsule);
