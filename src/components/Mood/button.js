import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { addProductBasket, createBasketGuest } from '../../Redux/Actions/basket';
import ScentItemMoodNew from '../ScentItem/scentItemMoodNew';
import { generateUrlWeb, getAltImageV2 } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

class ButtonMood extends Component {
    state = {
      isExtent: false,
    };

    onClick = () => {
      const { data } = this.props;
      this.props.onClick(data.id);
    };

    onClickImageCenter = (data) => {
      const url = this.generateUrlProduct(data);
      if (url) {
        this.props.history.push(generateUrlWeb(url));
      }
    };

    onClickLeftButton = (product) => {
      this.onClickImageCenter(product);
    };

    static getDerivedStateFromProps(nextProps, prevState) {
      const { login } = nextProps;
      const objectReturn = {};
      if (login !== prevState.login) {
        _.assign(objectReturn, { login });
      }
      return !_.isEmpty(objectReturn) ? objectReturn : null;
    }

    generateUrlProduct = (data) => {
      const { images, name } = this.props.data;
      const imgProduct = _.filter(images, x => x.type === null);
      if (data && data.combos && data.combos.length > 1) {
        const queryProducts = [];
        queryProducts.push({
          name,
          newName: this.props.data.name,
          ids: [data.combos[0].product.id, data.combos[1].product.id],
          images: imgProduct,
        });
        const queryString = encodeURIComponent(JSON.stringify(queryProducts));
        return `/products-questionnaire?products=${queryString}`;
        // this.props.history.push(`/products/${data.combos[0].product.id}/${data.combos[1].product.id}`, { imgProduct });
      }
      return null;
    }

    addCart = (product) => {
      const { item } = product;
      // eslint-disable-next-line no-shadow
      const { createBasketGuest, addProductBasket, basket } = this.props;
      const data = {
        item: item.id,
        name: this.props.data.name,
        price: item.price,
        is_featured: item.is_featured,
        quantity: 1,
        total: parseFloat(product.price) * 1,
        image_urls: product.image_urls,
        combos: product.combos,
        meta: {
          url: this.generateUrlProduct(product),
        },
      };
      const dataTemp = {
        idCart: basket.id,
        item: data,
      };
      if (!basket.id) {
        createBasketGuest(dataTemp);
      } else {
        addProductBasket(dataTemp);
      }
    };

    clickExtend = () => {
      const { isExtent } = this.state;
      this.setState({ isExtent: !isExtent });
    };

    generateProduct = (data) => {
      const { login } = this.props;
      const gender = login && login.user && login.user.gender
        ? login.user.gender
        : 'unisex';
      const products = [];
      _.forEach(data, (d) => {
        const { combo } = d;
        const combos = _.filter(combo, x => x.product.type === 'Scent');

        const url1 = combos && combos.length > 0
          ? combos[0].product.images
            ? _.find(
              combos[0].product.images,
              x => x.type === gender.toLowerCase(),
            ).image
            : ''
          : '';
        const url2 = combos && combos.length > 1
          ? combos[1].product.images
            ? _.find(
              combos[1].product.images,
              x => x.type === gender.toLowerCase(),
            ).image
            : ''
          : '';
        const ingredient1 = combos && combos.length > 0 ? combos[0].product.ingredient : undefined;
        const ingredient2 = combos && combos.length > 1 ? combos[1].product.ingredient : undefined;
        const description1 = combos && combos.length > 0
          ? combos[0].product
            ? combos[0].product.description
            : ''
          : '';
        const description2 = combos && combos.length > 1
          ? combos[1].product
            ? combos[1].product.description
            : ''
          : '';
        const name1 = combos && combos.length > 0
          ? combos[0].product
            ? combos[0].product.name
            : ''
          : '';
        const name2 = combos && combos.length > 1
          ? combos[1].product
            ? combos[1].product.name
            : ''
          : '';
        const isInfo = true;
        products.push({
          name: combo.name,
          item: d.items[0],
          urls: [url1, url2],
          description1,
          description2,
          ingredient1,
          ingredient2,
          name1,
          name2,
          // isShowGender
          combos,
          isInfo,
          id: d.id,
        });
      });
      return products;
    };

    render() {
      const { data } = this.props;
      const {
        images, description, name, products,
      } = data;
      const imageDisplay = _.find(images, x => (x.type ? x.type.toLowerCase() : x.type) === 'main');
      const { isExtent } = this.state;
      const dataProducts = this.generateProduct(products);
      return (
        <div className="div-bt-mood">
          <img
            loading="lazy"
            src={imageDisplay ? imageDisplay.image : ''}
            alt={getAltImageV2(imageDisplay)}
            style={{ opacity: isExtent ? 0.5 : 1 }}
          />
          <button
            type="button"
            className="div-content button-bg__none"
            onClick={this.clickExtend}
          >
            <span>
              {name}
            </span>
            <span
              style={{
                fontSize: '1.7rem',
                textAlign: 'center',
                width: '300px',
              }}
            >
              {description}
            </span>
            <button type="button">
              {isExtent ? '-' : '+'}
            </button>
          </button>
          {isExtent ? (
            <div
              className="div-extend"
              style={isMobile && !isTablet ? { justifyContent: 'space-around' } : {}}
            >
              {_.map(dataProducts, d => (
                <ScentItemMoodNew
                  sizeProduct={dataProducts.length}
                  onClickImage={this.onClickImageCenter}
                  onClickRightButton={this.addCart}
                  onClickLeftButton={this.onClickLeftButton}
                  onClickIngredient={this.props.onClickIngredient}
                  isShowGender
                  data={{
                    name: d.item.name,
                    image_urls: d.urls,
                    buttonName: '+ ADD TO CART',
                    buttonNameLeft: 'VIEW PRODUCT',
                    isLike: true,
                    price: d.item.price,
                    item: d.item,
                    id: d.id,
                    combos: d.combos,
                    description1: d.description1,
                    description2: d.description2,
                    ingredient1: d.ingredient1,
                    ingredient2: d.ingredient2,
                    name1: d.name1,
                    name2: d.name2,
                    isInfo: d.isInfo,
                  }}
                />
              ))
                        }
            </div>
          )
            : (<div style={{ height: '183px' }} />)}
        </div>
      );
    }
}

ButtonMood.propTypes = {
  data: PropTypes.shape({
    image_urls: PropTypes.arrayOf(PropTypes.string),
    description: PropTypes.string,
    name: PropTypes.string,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  login: PropTypes.shape(PropTypes.object).isRequired,
  basket: PropTypes.arrayOf(PropTypes.object).isRequired,
  addProductBasket: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  onClickIngredient: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  createBasketGuest,
  addProductBasket,
};

function mapStateToProps(state) {
  return {
    basket: state.basket,
    login: state.login,
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(ButtonMood),
);
