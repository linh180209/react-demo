import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { Progress } from 'reactstrap';
import classnames from 'classnames';
import '../../styles/buy-more-discount.scss';
import { getNameFromButtonBlock } from '../../Redux/Helpers';
import icBottle from '../../image/icon/ic-bottle-buy-more.svg';
import { GET_STORE_DISCOUNT } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';

function BuyMoreDiscounts(props) {
  const [storeDiscount, setStoreDiscount] = useState({});

  const fetchDiscount = () => {
    const option = {
      url: GET_STORE_DISCOUNT,
      method: 'GET',
    };
    fetchClient(option).then((response) => {
      setStoreDiscount(response);
    });
  };

  useEffect(() => {
    fetchDiscount();
  }, []);

  const { rules } = storeDiscount || {};
  let currentDiscount = {
    discount: 0,
    quantity: 0,
  };
  _.forEach(rules || [], (x) => {
    if (x.quantity <= props.totalPerfumeExlixir && x.quantity > currentDiscount.quantity) {
      currentDiscount = x;
    }
  });
  const rulesSort = _.orderBy(rules || [], ['discount'], ['asc']);
  const max = rulesSort.length > 0 ? (parseFloat(rulesSort[rulesSort.length - 1]?.discount) + 0.1) * 100 : 0;
  const currentPercent = currentDiscount?.discount && props.totalPerfumeExlixir === currentDiscount.quantity ? currentDiscount?.discount * 100
    : rulesSort.length > 0 ? props.totalPerfumeExlixir / (rulesSort[rulesSort.length - 1].quantity + 1) * max : 0;
  let numberMore;
  let percentMore;
  const indexAddMore = _.findIndex(rulesSort, x => x.discount === currentDiscount?.discount);
  if (currentDiscount?.discount === 0 && rulesSort.length > 0) {
    numberMore = rulesSort[0].quantity - props.totalPerfumeExlixir;
    percentMore = parseInt(rulesSort[0].discount * 100, 10);
  } else if (indexAddMore === rulesSort?.length) {
    numberMore = 0;
  } else if (indexAddMore < rulesSort?.length - 1) {
    numberMore = rulesSort[indexAddMore + 1].quantity - props.totalPerfumeExlixir;
    percentMore = parseInt(rulesSort[indexAddMore + 1]?.discount * 100, 10);
  } else if (indexAddMore >= rulesSort?.length - 1 && rulesSort.length > 0) {
    numberMore = -1;
    percentMore = parseInt(rulesSort[rulesSort.length - 1].discount * 100, 10);
  }
  const buyBt = getNameFromButtonBlock(props.buttonBlocks, 'BUY_MORE_AND_GET_MORE_DISCOUNTS');
  const messageBt1 = getNameFromButtonBlock(props.buttonBlocks, 'message_discount_1');
  const messageBt2 = getNameFromButtonBlock(props.buttonBlocks, 'message_discount_2');
  return (
    <div className="buy-more-discount">
      <div className="header-h2">
        {buyBt}
      </div>
      <div className="div-progress">
        <Progress value={currentPercent} className="cus-progress" max={max} min={0} />
        {
          _.map(rulesSort, d => (
            <div
              className={classnames(
                'number-percent',
                (d.quantity <= props.totalPerfumeExlixir ? 'active' : ''),
                (props.isShowBottle ? 'div-bottle' : ''),
              )}
              style={{
                left: `${parseFloat(d.discount) * 100 / max * 100}%`,
              }}
            >
              {
                props.isShowBottle ? (
                  <div className="show-bottle">
                    <div className="bottle">
                      {
                        _.map(_.range(d.quantity), i => (
                          <img src={icBottle} alt="bottle" />
                        ))
                      }
                    </div>
                    <span>
                      {parseInt(d.discount * 100, 10)}
                      %
                    </span>
                  </div>
                ) : (
                  <div>
                    <span>
                      {parseInt(d.discount * 100, 10)}
                      %
                    </span>
                  </div>
                )
              }
            </div>
          ))
        }
      </div>
      <span className={props.isShowBottle ? 'span-bottle' : ''}>
        { numberMore <= 0 ? messageBt2.replace('{percent}', percentMore)
          : messageBt1.replace('{percent}', percentMore).replace('{number}', numberMore)}
        {/* {addBt}
        {' '}
        {parseInt(currentDiscount.discount * 100, 10)}
        {offBt} */}
      </span>
    </div>
  );
}

export default BuyMoreDiscounts;
