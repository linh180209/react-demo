import React, { Component } from 'react';
import PropTypes from 'prop-types';
import bags from '../../image/bags.png';
import { getAltImage } from '../../Redux/Helpers';

class CapsuleItem extends Component {
  onClick = () => {
    const { onClickOpen, data } = this.props;
    if (onClickOpen) {
      onClickOpen(data);
    }
  }

  render() {
    const {
      width, height, urlImage, isShowBag, description, name,
    } = this.props;
    return (
      <div style={{ width, height }} className="capsureItem">
        <img loading="lazy" className="image" src={urlImage} alt={getAltImage(urlImage)} />
        <div className="text">
          <strong>
            {name}
          </strong>
          {' '}
          {description}
        </div>
        <div className="button">
          <button
            type="button"
            onClick={this.onClick}
            className={isShowBag ? '' : 'hiden'}
          >
            <img loading="lazy" className="img-bags_button" src={bags} alt="bags" />
          </button>
        </div>
      </div>
    );
  }
}

CapsuleItem.defaultProps = {
  isShowBag: true,
  description: '',
  name: '',
};

CapsuleItem.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  urlImage: PropTypes.string.isRequired,
  onClickOpen: PropTypes.func.isRequired,
  isShowBag: PropTypes.bool,
  description: PropTypes.string,
  name: PropTypes.string,
  data: PropTypes.shape(PropTypes.object).isRequired,
};

export default CapsuleItem;
