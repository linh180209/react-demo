import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';
import { generaCurrency, getNameFromCommon } from '../../Redux/Helpers';
import auth from '../../Redux/Helpers/auth';
import { isMobile, isTablet } from '../../DetectScreen';

class RecommendScent extends Component {
  onClickAddToCard = () => {
    const {
      data, addProductBasket, createBasketGuest, basket,
    } = this.props;
    const item = {
      item: data.id,
      name: data.name,
      is_featured: data.is_featured,
      price: data.price,
      quantity: 1,
      total: parseFloat(data.price) * 1,
      image_urls: data.images,
    };
    const dataTemp = {
      idCart: basket.id,
      item,
    };
    if (!basket.id) {
      createBasketGuest(dataTemp);
    } else {
      addProductBasket(dataTemp);
    }
  }

  onClickGotoRollOn = () => {
    // disable on click
    // const {
    //   data,
    // } = this.props;
    // const { product } = data;
    // this.props.history.push(`/products?type=Scent&id=${product.id}`);
  }

  render() {
    const {
      data, bottle, type, cmsCommon,
    } = this.props;
    if (!data) {
      return (<div />);
    }
    const {
      name, images, price, description,
    } = data;
    const image = images && images.length > 0 ? images[0] : '';
    const imageBottle = bottle ? _.find(bottle.product.images, x => x.type === 'sample') : '';
    const isCreation = type === undefined || type === 'Perfume';
    const addToCartBt = getNameFromCommon(cmsCommon, 'ADD_TO_CART');
    const htmlWeb = (
      <div
        className="div-row"
        style={{
          width: isTablet ? '360px' : '400px',
        }}
      >
        <div style={{ width: '50%', height: '145px' }}>
          <img
            loading="lazy"
            src={isCreation ? (imageBottle ? imageBottle.image : '') : image}
            alt="bottle"
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'contain',
              objectPosition: 'center',
              cursor: 'pointer',
            }}
            onClick={this.onClickGotoRollOn}
          />
        </div>
        <div className="div-col pl-3" style={{ alignItems: 'left', width: '50%' }}>
          <span style={{
            color: '#000',
            fontSize: '1.1rem',
          }}
          >
            {name}
          </span>
          <div className="div-col" style={{ flex: '1', justifyContent: 'flex-end', alignItems: 'center' }}>
            <span
              style={{
                fontSize: '0.8rem',
                color: '#3D3D3D',
                flex: '1',
                width: '100%',
              }}
              className="recommend-item-text"
            >
              {imageBottle ? bottle ? ReactHtmlParser(bottle.description) : '' : description}
            </span>
            <div
              className="div-row mt-1"
              style={{
                justifyContent: 'space-between',
                width: '100%',
              }}
            >
              <span style={{
                color: '#000',
                fontSize: '1.2rem',
              }}
              >
                {generaCurrency(price)}
              </span>
              <button
                type="button"
                className="button-bg__none"
                style={{
                  fontSize: '0.8rem',
                  textTransform: 'uppercase',
                }}
                onClick={this.onClickAddToCard}
              >
                {`+ ${addToCartBt}`}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
    const htmlMobile = (
      <div
        className="div-col"
      >
        <div style={{ width: '100%', height: '100px' }}>
          <img
            src={isCreation ? (imageBottle ? imageBottle.image : '') : image}
            alt="bottle"
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'contain',
              objectPosition: 'center',
              cursor: 'pointer',
            }}
            onClick={this.onClickGotoRollOn}
          />
        </div>
        <div
          className="div-col mt-3"
          style={{
            alignItems: 'left', width: '100%', color: '#0D0D0D',
          }}
        >
          <span style={{
            fontSize: '0.875rem',
            letterSpacing: '0.2em',
            textAlign: 'center',
          }}
          >
            {name}
          </span>
          <div className="div-col" style={{ flex: '1', justifyContent: 'flex-end', alignItems: 'center' }}>
            <span
              style={{
                padding: '1.125rem 1.375rem',
                fontSize: '0.875rem',
                textAlign: 'center',
              }}
              className="recommend-item-text"
            >
              {imageBottle ? bottle ? ReactHtmlParser(bottle.description) : '' : description}
            </span>
            <div
              className="mt-1"
              style={{
                display: 'contents',
                justifyContent: 'center',
                width: '100%',
              }}
            >
              <p style={{
                fontSize: '1rem',
                textTransform: 'uppercase',
                fontWeight: '600',
              }}
              >
                {generaCurrency(price)}
              </p>
              <button
                type="button"
                className="button-bg__none"
                style={{
                  textDecorationLine: 'underline',
                  fontSize: '0.875rem',
                  paddingBottom: '2rem',
                  textTransform: 'uppercase',
                }}
                onClick={this.onClickAddToCard}
              >
                {`+ ${addToCartBt}`}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
    return (
      <div>
        {
          isMobile && !isTablet ? htmlMobile : htmlWeb
        }
      </div>
    );
  }
}

RecommendScent.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    price: PropTypes.number,
    images: PropTypes.arrayOf(PropTypes.string),
    description: PropTypes.string,
  }).isRequired,
  addProductBasket: PropTypes.func.isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  basket: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape().isRequired,
  bottle: PropTypes.shape().isRequired,
  type: PropTypes.string.isRequired,
};

export default RecommendScent;
