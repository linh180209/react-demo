import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import _ from 'lodash';
import classnames from 'classnames';
import ReactHtmlParser from 'react-html-parser';
import { Collapse } from 'react-collapse';
import RecommendScent from './recommendScent';
import sub from '../../image/icon/sub_black.svg';
import add from '../../image/icon/add_small.svg';
import { isBrowser, isMobile, isTablet } from '../../DetectScreen';
import { isRightAlignedText } from '../../Redux/Helpers';


class RecommendItem extends Component {
  state = {
    isShow: this.props.isShow,
  }

  onToggle = () => {
    const { isShow } = this.state;
    this.setState({ isShow: !isShow });
  }

  render() {
    const {
      title, data, addProductBasket, basket, createBasketGuest, description, bottle, type,
    } = this.props;
    const { isShow } = this.state;
    const colorTitle = isShow ? '#000' : '#0D0D0D';
    return (
      <div className="div-recommend" id="idSample" style={{ borderBottom: '1px solid rgba(38, 38, 38, 0.1)' }}>
        <button
          type="button"
          className="div-row button-bg__none"
          style={{
            position: 'relative',
            width: '100%',
          }}
          onClick={this.onToggle}
        >
          <button
            type="button"
            className="button-bg__none bt-close"
            style={{
              position: 'absolute',
              top: '50%',
              transform: 'translateY(-50%)',
            }}
            onClick={this.onToggle}
          >
            <img src={isShow ? sub : add} alt="sub" />
          </button>

          <span
            className="header_3"
            style={{
              color: colorTitle,
              width: '100%',
              textAlign: isRightAlignedText() ? 'right' : 'start',
            }}
          >
            { title }
          </span>
        </button>
        {
          description ? (
            <Collapse
              isOpened={isShow}
            >
              <div
                className={classnames('header_4 text', isRightAlignedText() ? 'tr' : 'tl')}
              >
                {ReactHtmlParser(description)}
              </div>
            </Collapse>
          ) : (
            isMobile
              ? (
                <Collapse
                  isOpened={isShow}
                  // className={isShow ? 'div-row' : 'hiden'}

                >
                  <Row style={{ marginBottom: '30px', justifyContent: 'space-between' }}>
                    {
                      _.map(data, d => (
                        <Col xs="12">
                          <RecommendScent
                            data={d}
                            bottle={bottle}
                            addProductBasket={addProductBasket}
                            createBasketGuest={createBasketGuest}
                            basket={basket}
                            history={this.props.history}
                            type={type}
                            cmsCommon={this.props.cmsCommon}
                          />
                        </Col>
                      ))
                    }
                  </Row>
                </Collapse>
              ) : (
                <Collapse
                  isOpened={isShow}
                  // className={isShow ? 'div-row' : 'hiden'}
                  // style={{ marginBottom: '30px', justifyContent: 'space-between' }}
                >
                  <Row style={{ marginBottom: '30px', justifyContent: 'space-between' }}>
                    {
                      _.map(data, d => (
                        <Col md="6" xs="6" className="mb-4">
                          <RecommendScent
                            data={d}
                            bottle={bottle}
                            addProductBasket={addProductBasket}
                            createBasketGuest={createBasketGuest}
                            basket={basket}
                            history={this.props.history}
                            type={type}
                            cmsCommon={this.props.cmsCommon}
                          />
                        </Col>
                      ))
                    }
                  </Row>
                  {/* <RecommendScent
                    data={data && data.length > 0 ? data[0] : undefined}
                    addProductBasket={addProductBasket}
                    createBasketGuest={createBasketGuest}
                    basket={basket}
                  />
                  <RecommendScent
                    data={data && data.length > 1 ? data[1] : undefined}
                    addProductBasket={addProductBasket}
                    createBasketGuest={createBasketGuest}
                    basket={basket}
                  /> */}
                </Collapse>
              )
          )
        }
      </div>
    );
  }
}

RecommendItem.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  addProductBasket: PropTypes.func.isRequired,
  basket: PropTypes.shape(PropTypes.object).isRequired,
  createBasketGuest: PropTypes.func.isRequired,
  description: PropTypes.string.isRequired,
  isShow: PropTypes.bool.isRequired,
  history: PropTypes.shape().isRequired,
  bottle: PropTypes.shape().isRequired,
  type: PropTypes.string.isRequired,
};

export default RecommendItem;
