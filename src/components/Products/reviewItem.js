import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import like from '../../image/icon/like.png';
import likeRed from '../../image/icon/like_red.png';
import avatar from '../../image/icon/logo-stamp.png';
import { getAltImage } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

class ReviewItem extends Component {
  render() {
    const {
      user, title, rating, comment, date_modified: date,
    } = this.props.data;
    return (
      <div className="div-col" style={{ marginBottom: '40px', width: '100%' }}>
        <div className="div-row">
          <div>
            <img
              src={user && user.avatar ? user.avatar : avatar}
              alt={getAltImage(user && user.avatar ? user.avatar : avatar)}
              style={{
                width: '70px',
                height: '70px',
                borderRadius: '50%',
                objectFit: 'cover',
              }}
            />
          </div>
          <div
            className="div-col"
            style={{
              justifyContent: 'space-around',
              marginLeft: '20px',
            }}
          >
            <span
              className={isMobile && !isTablet ? '' : 'hidden'}
              style={{
                color: '#BCBCBC',
                fontSize: '0.8rem',
              }}
            >
              {date ? moment(date).format('DD.MM.YYYY') : ''}

            </span>
            <span style={isMobile && !isTablet ? {
              fontSize: '1rem',
              color: '#000',
            } : {
              fontSize: '1.1rem',
              color: '#000',
            }}
            >
              {title}
            </span>
            <div style={{
              display: 'inline-flex',
              alignItems: 'center',
            }}
            >
              <img src={rating >= 1 ? likeRed : like} alt="like1" className="icon_size" />
              <img src={rating >= 2 ? likeRed : like} alt="like2" className="icon_size" />
              <img src={rating >= 3 ? likeRed : like} alt="like3" className="icon_size" />
              <div
                className="ml-2 mr-2"
                style={{
                  width: '1px',
                  height: '16px',
                  background: '#BCBCBC',
                }}
              />
              <span
                style={isMobile && !isTablet ? {
                  fontStyle: 'italic',
                  color: '#BCBCBC',
                  fontSize: '0.8rem',
                } : {
                  fontStyle: 'italic',
                  color: '#BCBCBC',
                }}
              >
                Review by:
              </span>
              <span
                className="ml-2"
                style={isMobile && !isTablet ? {
                  color: '#000',
                  fontSize: '0.8rem',
                } : {
                  color: '#000',
                }}
              >
                {user ? `${user.first_name} ${user.last_name}` : ''}
              </span>
              <div
                className={isMobile && !isTablet ? 'hidden' : 'ml-2 mr-2'}
                style={{
                  width: '1px',
                  height: '16px',
                  background: '#BCBCBC',
                }}
              />
              <span
                className={isMobile && !isTablet ? 'hidden' : ''}
                style={{
                  color: '#BCBCBC',
                }}
              >
                {date ? moment(date).format('DD.MM.YYYY') : ''}

              </span>
            </div>
          </div>
        </div>
        <span style={
          isMobile && !isTablet ? {
            marginTop: '20px',
            color: '#3D3D3D',
            fontSize: '0.8rem',
          }
            : { marginTop: '20px', color: '#3D3D3D' }
          }
        >
          {comment}
        </span>
      </div>
    );
  }
}

ReviewItem.propTypes = {
  data: PropTypes.shape({
    comment: PropTypes.string,
    id: PropTypes.string,
    product: PropTypes.shape(PropTypes.object),
    rating: PropTypes.number,
    title: PropTypes.string,
    user: PropTypes.shape(PropTypes.object),
    date_modified: PropTypes.string,
  }).isRequired,
};

export default ReviewItem;
