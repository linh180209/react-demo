import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { generateUrlWeb } from '../../Redux/Helpers';

class AskCookies extends Component {
  onLink = () => {
    const { history } = this.props;
    history.push(generateUrlWeb('/terms-conditions'));
  }

  render() {
    return (
      <div className="cookieConsentContainer">
        <div className="cookieTitle">
          <span>Cookies</span>
        </div>
        <div className="cookieDesc">
          <span>
            By using this website, you automatically accept that we use cookies.
            {' '}
            <u onClick={this.onLink}>Learn more</u>
          </span>
          {/* <span className="cookieDetail" onClick={this.onLink}>Learn More</span> */}
        </div>
        <div className="cookieButton">
          <button
            type="button"
            onClick={this.props.onClick}
          >
            Understood
          </button>
        </div>
      </div>
    );
  }
}

AskCookies.propTypes = {
  onClick: PropTypes.func.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default withRouter(AskCookies);
