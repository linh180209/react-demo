import React, { useState, useEffect } from 'react';
import { Modal, ModalBody } from 'reactstrap';
import classnames from 'classnames';
import icClose from '../../image/icon/ic-close-video.svg';
import VideoAcademy from '../AcademyItem/videoAcademy';
import '../../styles/_modal-video.scss';
import { addFontCustom } from '../../Redux/Helpers';

function ModalVideo(props) {
  return (
    <Modal className={classnames('modal-video', addFontCustom(), props.limitHeight ? 'limit-height' : '')} isOpen={props.isOpen} centered>
      <ModalBody>
        <div className="video-item">
          <button className="button-bg__none bt-close" type="button" onClick={props.onClose}>
            <img loading="lazy" src={icClose} alt="Close" />
          </button>
          <span>{props.name}</span>
          <VideoAcademy url={props.video} poster={props.poster} isControl />
        </div>
      </ModalBody>
    </Modal>
  );
}

export default ModalVideo;
