import React, { Component } from 'react';
import orderItem from '../../image/orderItem.png';
import { getAltImage } from '../../Redux/Helpers';

export default class OrderHistoryItem extends Component {
  render() {
    return (
      <div className="order-history-item">
        <div className="body1">
          <span>
            <strong>
              Order number: 6523456722
            </strong>
          </span>
          <div className="div-line" />
          <span>
            <strong>
              Order Date: 16 - 4 - 2018
            </strong>
          </span>
          <div className="div-line" />
          <span>
            <strong>
              Shipped Date: 17 - 4 - 2018
            </strong>
          </span>
          <div className="div-line" />
          <span className="title-item">
            <strong>
              Items:
            </strong>
          </span>
          <div className="item">
            <img loading="lazy" src={orderItem} alt={getAltImage(orderItem)} />
            <span>
              Alcohol
              <br />
              <strong>
                $50.00
              </strong>
              <br />
              QTY: 3
            </span>
          </div>
          <span>
            <strong>
              Delivery Address
            </strong>
          </span>
          <span>
            #13-090
          </span>
          <span>
            32 orchard road
          </span>
          <span>
            Singapore 981032
          </span>
          <span className="title-margin">
            <strong>
              Delivered On
            </strong>
          </span>
          <span>
            21 - 4 - 2018
          </span>
        </div>
        <div className="body2">
          <span>
            <strong>
              Order Total
            </strong>
          </span>
          <span>
            Sub-total: $150
          </span>
          <span>
            Delivery: Free
          </span>
          <span>
            <strong>
              Total: $67.62
            </strong>
          </span>
        </div>
      </div>
    );
  }
}
