/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import { deleteProductBasket, updateProductBasket } from '../../Redux/Actions/basket';
import MenuMobile from './MenuItem/menuMobile';
import { generateUrlWeb, onClickLink } from '../../Redux/Helpers';

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowLogin: false,
      isShowCard: false,
      menu: [],
      itemClick: {
        name: '',
        subMenu: [],
      },
    };
    const { login } = this.props;
    this.isB2B = login && login.user && login.user.is_b2b;
  }

  componentDidMount = () => {
    const { cms } = this.props;
    this.initData(cms);
  };

  componentDidUpdate = () => {
    if (this.state.menu.length === 0) {
      this.initData(this.props.cms);
    }
  }

  initData = (cms) => {
    const menuPage = _.find(cms, x => x.title === 'Hamburger');
    if (menuPage) {
      const menuBlock = _.filter(menuPage.body, x => x.type === 'menu_block');
      const menu = [];
      _.forEach(menuBlock, (d) => {
        const { value } = d;
        const { submenu } = value;
        const submenus = [];
        for (let i = 0; i < submenu.length; i += 1) {
          const sub = submenu[i];
          submenus.push({
            name: sub.value.text,
            url: sub.value.link,
            onClickLink: sub.value.link.includes('https') ? this.openNewTab : this.onClickLink,
          });
        }
        menu.push({
          name: value.text,
          url: value.link,
          onClickLink: value.link.includes('https') ? this.openNewTab : submenus.length > 0 ? this.onCLickSubMenu : this.onClickLink,
          subMenu: submenus,
        });
      });
      this.setState({ menu });
    }
  }

  onClickMenu = (name) => {
    const { menu } = this.state;
    const item = _.find(menu, x => x.name === name);
    if (item) {
      const el = document.getElementById('subMenu');
      if (el) {
        el.classList.remove('submenu');
        // eslint-disable-next-line no-unused-expressions
        el.offsetWidth;
        el.classList.add('submenu');
        this.setState({ itemClick: item });
      }
    }
  }

  onClose = () => {
    this.props.onClose();
  }

  onClickAccount = () => {
    const { login } = this.props;
    if (login && login.user && !login.user.is_b2b) {
      this.props.history.push(generateUrlWeb('/account'));
      this.onClose();
    } else {
      this.onOpenLoginPage();
    }
  }

  onOpenLoginPage = () => {
    this.setState({ isShowLogin: true });
  }

  onCloseLoginPage = () => {
    this.setState({ isShowLogin: false });
  }

  onCloseCard = () => {
    this.setState({ isShowCard: false });
  }

  onOpenCard = () => {
    this.setState({ isShowCard: true });
  }

  onCLickSubMenu = (data) => {
    const { itemClick } = this.state;
    const { name, subMenu } = data;
    _.assign(itemClick, { name, subMenu });
    this.setState({ itemClick });
  }

  onClickLink = (data) => {
    const { url } = data;
    onClickLink(url, this.props.login, this.props.history);
    this.onClose();
  }

  openNewTab = (data) => {
    const { url } = data;
    window.open(url);
  }

  gotoB2bLogin = () => {
    const { login } = this.props;
    if (login && login.user && login.user.is_b2b) {
      this.props.history.push(generateUrlWeb('/b2b/landing'));
    } else {
      this.props.history.push(generateUrlWeb('/b2b/login'));
    }
  }

  gotoCheckOut = () => {
    this.props.history.push(generateUrlWeb('/checkout'));
  }

  onClickOut = () => {
    console.log('onClickOut');
  }

  render() {
    const {
      menu, itemClick, isShowLogin, isShowCard,
    } = this.state;
    const { login, basket, isShowMenu } = this.props;
    const { items, id } = basket;
    const total = _.reduce(items, (sum, d) => sum + d.total, 0);
    const displayName = login && login.user && !login.user.is_b2b ? `${login.user.first_name} ${login.user.last_name}` : 'MY ACCOUNT';
    const totalBasket = !this.isB2B ? (basket && basket.items ? basket.items.length : 0) : 0;
    const htmlMobile = (
      <MenuMobile displayName={displayName} menu={menu} onClose={this.onClose} isShowMenu={isShowMenu} countries={this.props.countries} visibleMessage={this.props.visibleMessage} buttonBlocks={this.props.buttonBlocks} history={this.props.history} />
    );
    return (
      <div className={`div-menu-wrap ${isShowMenu ? 'animation-all animation-open-menu' : 'animation-all animation-close-menu'}`}>
        {
          htmlMobile
        }
      </div>
    );
  }
}

Menu.propTypes = {
  onClose: PropTypes.bool.isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  login: PropTypes.shape({
    user: PropTypes.shape({
      is_b2b: PropTypes.bool,
    }),
  }).isRequired,
  isShowMenu: PropTypes.bool.isRequired,
  basket: PropTypes.arrayOf(PropTypes.object).isRequired,
  updateProductBasket: PropTypes.func.isRequired,
  deleteProductBasket: PropTypes.func.isRequired,
  menu: PropTypes.arrayOf().isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    basket: state.basket,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  deleteProductBasket,
  updateProductBasket,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Menu));
