import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import '../../styles/block-menu-4-m.scss';
import BlockMenuItem2 from './blockMenuItem2';

function BlockMenu4M(props) {
  const ele = document.getElementById('div-text-menu');
  let topCurrent = 127;
  if (ele) {
    topCurrent = ele.getBoundingClientRect().top + 40;
  }
  const minHeight = window.innerHeight - topCurrent;
  return (
    <div className="block-menu-4-m">
      <div className="scroll-menu">
        <div className="list-block" style={{ height: `${minHeight}px` }}>
          <div className="list-item">
            {
              _.map(_.range(5), d => (
                <BlockMenuItem2 />
              ))
            }
          </div>
          <Link className="button-homepage-new-7" to="/">
            VISIT BLOG
          </Link>
        </div>
      </div>

    </div>
  );
}

export default BlockMenu4M;
