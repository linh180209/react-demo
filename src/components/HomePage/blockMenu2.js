import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import '../../styles/block-menu-2.scss';
import { generateUrlWeb } from '../../Redux/Helpers';
import BlockMenuItem2 from './blockMenuItem2';
import { isMobile } from '../../DetectScreen/detectIFrame';

function BlockMenu2(props) {
  const ele = document.getElementById('div-text-menu');
  let topCurrent = 127;
  if (ele) {
    topCurrent = ele.getBoundingClientRect().top + 40;
  }
  const minHeight = window.innerHeight - topCurrent;
  const dropdowns = props.data ? props.data.dropdowns2 : undefined;
  if (!dropdowns || dropdowns.length < 2) {
    return <div />;
  }
  return (
    <div className="block-menu-2">
      <div className="scroll-menu">
        {
          isMobile ? (
            <div className="list-block" style={{ height: `${minHeight}px` }}>
              <div className="list-image">
                <BlockMenuItem2 data={dropdowns[0]} />
                <BlockMenuItem2 data={dropdowns[1]} />
              </div>
              <div className="link-text">
                <div className="header-h3">
                  {dropdowns[0].value.header.header_text}
                </div>
                {
                  _.map(dropdowns[0].value.links.links, d => (
                    <Link to={generateUrlWeb(d.value.link)}>
                      {d.value.text}
                    </Link>
                  ))
                }
              </div>
              <div className="link-text">
                <div className="header-h3">
                  {dropdowns[1].value.header.header_text}
                </div>
                {
                  _.map(dropdowns[1].value.links.links, d => (
                    <Link to={generateUrlWeb(d.value.link)}>
                      {d.value.text}
                    </Link>
                  ))
                }
              </div>
              <div style={{ height: '100px' }} />
            </div>
          ) : (
            <React.Fragment>
              <BlockMenuItem2 data={dropdowns[0]} />
              <div className="line-col" />
              <BlockMenuItem2 data={dropdowns[1]} />
            </React.Fragment>
          )
        }
        {
          isMobile && (
            <div className="bt-all-product">
              <Link to={generateUrlWeb(props.data.dropdowns1[0].value.button.link)} className="button-homepage-new-7">
                {props.data.dropdowns1[0].value.button.name}
              </Link>
            </div>
          )
        }
      </div>

    </div>
  );
}

export default BlockMenu2;
