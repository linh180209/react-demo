import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Card, Button, CardText, CardBody,
} from 'reactstrap';


class CardOption extends Component {
  render() {
    const {
      title, name, description, nameBt,
    } = this.props;
    return (
      <div className="container-card">
        <Card>
          <CardBody>
            <CardText>
              {' '}
              {title}
            </CardText>
            <CardText>
              {' '}
              {name}
            </CardText>
            <CardText>
              {description}
            </CardText>
          </CardBody>
        </Card>
        <Button
          className="bt-custome"
        >
          {nameBt}
        </Button>
      </div>
    );
  }
}

CardOption.propTypes = {
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  nameBt: PropTypes.string.isRequired,
};
export default CardOption;
