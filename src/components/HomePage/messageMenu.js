import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import classnames from 'classnames';
import ReactHtmlParser from 'react-html-parser';
import '../../styles/messageMenu.scss';
import addCmsRedux from '../../Redux/Actions/cms';
import updateShippings from '../../Redux/Actions/shippings';
import { fetchCMSHomepage, generaCurrency, getNameFromButtonBlock } from '../../Redux/Helpers';
import { GET_SHIPPING_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import auth from '../../Redux/Helpers/auth';

// import '../../styles/messageMenu.scss';

const getCms = (data) => {
  const { body } = data;
  const richBlocks = _.filter(body, x => x.type === 'rich_text_block');
  const buttonBlocks = _.filter(body, x => x.type === 'button_block');
  return { richBlocks, buttonBlocks };
};

function SampleNextArrow(props) {
  // eslint-disable-next-line react/prop-types
  const { onClick } = props;
  return (
    <button
      type="button"
      className="bt-arrow-menu bt-arrow-menu-right"
      onClick={onClick}
    >
      <i className="fa fa-chevron-right" />
    </button>
  );
}

function SamplePrevArrow(props) {
  // eslint-disable-next-line react/prop-types
  const { onClick } = props;
  return (
    <button
      type="button"
      className="bt-arrow-menu bt-arrow-menu-left"
      onClick={onClick}
    >
      <i className="fa fa-chevron-left" />
    </button>
  );
}

class MessageMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      shippingInfo: undefined,
      buttonBlocks: [],
    };
  }

  componentDidMount() {
    this.fetchDataInit();
    this.fetchShipping();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const messageBlock = _.find(cms, x => x.title === 'Message bar');
    if (!messageBlock) {
      const cmsData = await fetchCMSHomepage('message-bar');
      this.props.addCmsRedux(cmsData);
      const body = getCms(cmsData);
      const data = _.map(body.richBlocks, x => x.value);
      this.setState({
        data,
        buttonBlocks: body.buttonBlocks,
      });
    } else {
      const body = getCms(messageBlock);
      const data = _.map(body.richBlocks, x => x.value);
      this.setState({
        data,
        buttonBlocks: body.buttonBlocks,
      });
    }
  }

  fetchShipping = () => {
    if (this.props.shippings?.length > 0) {
      const shippingInfo = _.find(this.props.shippings, d => d.country.code === auth.getCountry());
      this.setState({ shippingInfo });
      return;
    }
    const options = {
      url: GET_SHIPPING_URL,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result) {
        const shippingInfo = _.find(result, d => d.country.code === auth.getCountry());
        this.setState({ shippingInfo });
        this.props.updateShippings(result);
      }
    }).catch((err) => {
      console.log('error', err);
    });
  }

  render() {
    const { data, buttonBlocks, shippingInfo } = this.state;
    const freeDeliveryBt = getNameFromButtonBlock(buttonBlocks, 'FREE_DELIVERY');
    const freeDeliveryOverBt = getNameFromButtonBlock(buttonBlocks, 'FREE_DELIVERY_OVER');
    const settings = {
      className: 'custome-message',
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      adaptiveHeight: true,
      autoplay: true,
      arrows: false,
      autoplaySpeed: 5000,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
    };
    return (
      <div className={classnames('message-menu pt-2 pb-2 w-100', this.props.className)} style={{ background: 'black' }}>
        <Slider {...settings}>
          {
          _.map(data.concat(
            {},
          ), (d, index) => {
            if (_.isEmpty(d)) {
              return (
                <h4 className="header_4 animated fadeIn text-message" key={0} style={{ color: '#ffffff' }}>
                  {
                    shippingInfo && parseFloat(shippingInfo.threshold) > 0 ? (
                      `${freeDeliveryOverBt.replace('{price}', generaCurrency(shippingInfo.threshold, true))}`
                    ) : (
                      freeDeliveryBt
                    )
                  }
                </h4>
              );
            }
            return (
              <h4 className="header_4 animated fadeIn text-message" key={index + 1} style={{ color: '#ffffff' }}>
                {ReactHtmlParser(d)}
              </h4>
            );
          })
          }
        </Slider>
      </div>
    );
  }
}

MessageMenu.propTypes = {
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  addCmsRedux: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
    countries: state.countries,
    shippings: state.shippings,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  updateShippings,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageMenu);
