import React, { Component } from 'react';
import PropTypes, { object } from 'prop-types';
import _ from 'lodash';
import ItemSubmenu from '../MenuItem/itemMobilemenu';

class SubMenu extends Component {
  render() {
    const { className } = this.props;
    const { data } = this.props;
    return (
      <nav className={`div-submenu ${className}`}>
        {
          _.map(data, item => (
            <button
              type="button"
              onClick={() => this.props.onClickLink(item.value.link)}
            >
              {item.value.text}
            </button>
          ))
        }
      </nav>
    );
  }
}

SubMenu.propTypes = {
  className: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  onClickLink: PropTypes.func.isRequired,
};

export default SubMenu;
