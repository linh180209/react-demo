import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

import { convertHeaderToSize, getAltImage } from '../../Redux/Helpers';
import bg8 from '../../image/bg8.svg';
import { isMobile, isTablet } from '../../DetectScreen';

class Academy extends Component {
  render() {
    const { academyCms } = this.props;
    if (!academyCms) {
      return (<div />);
    }
    const {
      header_size: headerSize, header_text: headerText, child_sections: childSections, buttons,
    } = this.props.academyCms;
    const styleHeader = {
      fontSize: headerSize ? convertHeaderToSize(headerSize) : undefined,
    };
    const textButton = buttons[0] ? buttons[0].value.text : '';
    const blocks1 = childSections.length > 0 ? childSections[0].value : undefined;
    const imageBlock = _.find(blocks1, x => x.type === 'image_block');
    const blocks2 = childSections.length > 1 ? childSections[1].value : undefined;
    const textBlock = _.find(blocks2, x => x.type === 'text_block');
    return (
      <div className="div-academy" style={isMobile && !isTablet ? { marginTop: '0px' } : {}}>
        {imageBlock
          ? (<img style={isMobile && !isTablet ? { top: '0px', height: '100%' } : {}} className="img-bg" src={imageBlock.value.image_url} alt={getAltImage(imageBlock.value.image_url)} />)
          : (<img style={isMobile && !isTablet ? { top: '0px', height: '100%' } : {}} className="img-bg" src={bg8} alt="bg" />)
        }
        {
          headerText
            ? (
              <span className="text-title" style={styleHeader}>
                {headerText}
              </span>
            ) : (<div />)
        }
        {
          textBlock
            ? (
              <div className="text pb-5 pt-5">
                {textBlock.value}
              </div>
            ) : (<div />)
        }
        {
          textButton ? (
            <div className="div-button" style={{ zIndex: '1' }}>
              <a
                href={buttons[0].value.link}
                className="button_homepage"
              >
                {textButton}
              </a>
            </div>
          ) : (<div />)
        }
      </div>
    );
  }
}

Academy.propTypes = {
  academyCms: PropTypes.shape({
    child_sections: PropTypes.arrayOf(PropTypes.object),
    header_size: PropTypes.string,
    header_text: PropTypes.string,
  }).isRequired,
};
export default Academy;
