import classnames from 'classnames';
import _ from 'lodash';
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ASSIGN_PROMOTION_CODE_CART } from '../../../config';
import { isMobile, isTablet } from '../../../DetectScreen';
import icError from '../../../image/icon/info-error.svg';
import icSuccess from '../../../image/icon/info-success.svg';
import icRemove from '../../../image/icon/ic-remove-promo.svg';
import icSub from '../../../image/icon/ic-sub.svg';
import icPlus from '../../../image/icon/ic-plus.svg';
import { useMergeState } from '../../../Utils/customHooks';
import './styles.scss';
import loadingPage from '../../../Redux/Actions/loading';
import { loadAllBasket } from '../../../Redux/Actions/basket';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../../Redux/Helpers/notification';
import auth from '../../../Redux/Helpers/auth';
import { isCheckNull, segmentCouponApplied } from '../../../Redux/Helpers';


function PromoCode(props) {
  const dispatch = useDispatch();
  const basket = useSelector(state => state.basket);
  const [isCheckPromotion, setIsCheckPromotion] = useState(false);
  const [state, setState] = useMergeState({
    isPromoError: false,
    promotion: basket?.promo,
    isOPenPromo: false,
    promotionCode: basket?.promo?.code,
  });

  const onCheckPromo = async (disableToast) => {
    const proCodeAvaiBt = 'Promotion code isavaialble';
    const proCodeNotAvaiBt = 'Promotion code is not avaialble.';
    try {
      dispatch(loadingPage(true));

      // segmentCouponEntered(basket?.id, state.promotionCode);

      const options = {
        url: `${ASSIGN_PROMOTION_CODE_CART.replace('{id}', basket?.id)}`,
        method: 'PUT',
        body: {
          promo: state.promotionCode,
        },
      };
      const result = await fetchClient(options, true);
      console.log('result', result);
      if (result?.isError) {
        throw new Error();
      } else {
        dispatch(loadAllBasket(result));
        segmentCouponApplied(basket?.id, result?.promo?.id,
          state.promotionCode, result);
        if (!disableToast) {
          toastrSuccess(proCodeAvaiBt);
        }
        setState({
          isPromoError: false,
          promotion: result,
          isOPenPromo: false,
        });
      }
    } catch (error) {
      toastrError(proCodeNotAvaiBt);
      setState({
        isPromoError: true,
        promotion: undefined,
      });
    } finally {
      dispatch(loadingPage(false));
    }
  };

  const onChangePromo = (e) => {
    const { value } = e.target;
    setState({ promotionCode: value });
  };

  const removePromotion = async () => {
    try {
      dispatch(loadingPage(true));
      const options = {
        url: `${ASSIGN_PROMOTION_CODE_CART.replace('{id}', basket?.id)}`,
        method: 'PUT',
        body: {
          promo: null,
        },
      };
      const result = await fetchClient(options, true);
      if (result?.error) {
        throw new Error();
      } else {
        dispatch(loadAllBasket(result));
        setState({
          isPromoError: false,
          promotion: null,
        });
      }
    } catch (error) {
      toastrError(error.message);
      setState({
        isOPenPromo: false,
      });
    } finally {
      dispatch(loadingPage(false));
    }
  };

  useEffect(() => {
    const discountCode = auth.getDiscountCode();
    if (!_.isEmpty(basket) && !isCheckNull(discountCode) && basket?.promo?.code !== discountCode) {
      setState({ promotionCode: discountCode });
      setIsCheckPromotion(true);
      auth.setDiscountCode('');
    } else {
      setState({ promotion: basket?.promo, promotionCode: basket?.promo?.code || '' });
    }
  }, [basket]);

  useEffect(() => {
    if (isCheckPromotion) {
      onCheckPromo();
      setIsCheckPromotion(false);
    }
  }, [isCheckPromotion]);

  return (
    <div className={`div-expand-cart ${state.isOPenPromo ? 'open-expand' : 'close-expand'}`}>
      <div className={classnames('div-header-expand', state.isOPenPromo ? 'hidden' : '')} onClick={() => (!_.isEmpty(state.promotion) ? {} : setState({ isOPenPromo: !state.isOPenPromo }))}>
        {
          !_.isEmpty(state.promotion) ? (
            <div className="div-promo-apply">
              <span>{state.promotionCode}</span>
              <button type="button" className="button-bg__none" onClick={removePromotion}>
                <img src={icRemove} alt="remove" />
              </button>
            </div>
          ) : (
            <React.Fragment>
              <span>
                {/* {promoGiftBt} */}
                Promo/Gift Certificate
              </span>
              <button
                type="button"
                onClick={() => setState({ isOPenPromo: !state.isOPenPromo })}
              >
                <img src={state.isOPenPromo ? icSub : icPlus} alt="icon" />
              </button>
            </React.Fragment>

          )
        }
      </div>
      <div id="id-promotion" className="div-content-expand-card">
        <div className="div-row div-promo">
          <div className="group">
            <input
              className={classnames('input-material input_underline border-checkout', state.promotionCode ? 'input-text-bottom' : '', state.isPromoError ? 'error' : '', !_.isEmpty(state.promotion) && !state.isPromoError ? 'success' : '')}
              type="text"
              value={state.promotionCode}
              default={state.promotionCode}
              onChange={onChangePromo}
              required
            />
            <label className={classnames('label-material', state.promotionCode ? 'has-text' : '')}>
              {/* {promoGiftBt} */}
              Promo/Gift Certificate
            </label>
            <img src={state.isPromoError ? icError : icSuccess} className={classnames(state.isPromoError || !_.isEmpty(state.promotion) && !state.isPromoError ? 'icon-show' : 'hidden')} alt="icon" />
          </div>
          <button
            type="button"
            disabled={!state.promotionCode}
            className={`${isMobile && !isTablet ? 'w-30 border-checkout' : ''} ${!state.promotionCode ? 'disabled' : ''}`}
            onClick={onCheckPromo}
          >
            {/* {applyBt} */}
            Apply
          </button>
        </div>
      </div>
    </div>
  );
}

export default PromoCode;
