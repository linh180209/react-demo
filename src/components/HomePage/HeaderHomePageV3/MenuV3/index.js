/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import DehazeIcon from '@mui/icons-material/Dehaze';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import PersonIcon from '@mui/icons-material/Person';
import ShoppingBasketOutlinedIcon from '@mui/icons-material/ShoppingBasketOutlined';
import classnames from 'classnames';
import _ from 'lodash';
import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import ButtonCT from '../../../../componentsv2/buttonCT';
import LinkCT from '../../../../componentsv2/linkCT';
import { isMobile991fn } from '../../../../DetectScreen';
import { logo } from '../../../../imagev2/png';
import {
  generateUrlWeb,
  getLink,
  getLinkFromButtonBlock,
  getNameFromButtonBlock,
  gotoHomeAllProduct,
  gotoUrl,
  segmentNavbarPersonalityQuizInitiated
} from '../../../../Redux/Helpers';
import { useMergeState } from '../../../../Utils/customHooks';
import useWindowEvent from '../../../../Utils/useWindowEvent';
import './styles.scss';

function MenuV3(props) {
  const history = useHistory();
  const basket = useSelector(state => state.basket);
  const [state, setState] = useMergeState({
    isRefresh: false,
  });

  const onClickMenu = () => {
    gotoHomeAllProduct(history);
  };

  const gotoLogin = () => {
    history.push(generateUrlWeb('/login'));
  };

  const gotoCart = () => {
    history.push(generateUrlWeb('/cart'));
  };

  const onClickOpenDrawer = () => {
    // setIsOpenDrawer(true);
    props.toggleOpenMenu();
  };

  const gotoLink = (link) => {
    gotoUrl(link, history, true);
  };

  const onChangeSize = () => {
    setState({ isRefresh: !state.isRefresh });
  };

  useWindowEvent('resize', onChangeSize, window);

  if (isMobile991fn()) {
    const allProductLink = getLinkFromButtonBlock(
      props.buttonBlocks,
      'All Products',
    );
    return (
      <div
        className={classnames(
          'menu-mobile-v3',
          props.isRemoveMessage ? 'mt-0' : '',
        )}
      >
        {props.isEcommerce ? (
          <div className="icon-draw" onClick={() => gotoLink(allProductLink)}>
            <ArrowBackIcon />
          </div>
        ) : (
          <div className="icon-draw" onClick={onClickOpenDrawer}>
            <DehazeIcon />
          </div>
        )}

        <div className="logo">
          <img src={logo} alt="logo" onClick={onClickMenu} />
        </div>
        <div className="div-icon div-row">
          <div className="click-icon" onClick={gotoLogin}>
            <PersonIcon />
          </div>
          <div className="click-icon" onClick={gotoCart}>
            <ShoppingBasketOutlinedIcon />
            {basket?.items?.length > 0 && (
              <div className="number-total">{basket?.items?.length}</div>
            )}
          </div>
        </div>
      </div>
    );
  }
  if (props.isSimple) {
    return (
      <div className="menu-simple-v3">
        <img src={logo} alt="logo" onClick={onClickMenu} />
      </div>
    );
  }

  const createBt = getNameFromButtonBlock(
    props.buttonBlocks,
    'Create My Scent',
  );
  const createLink = getLinkFromButtonBlock(
    props.buttonBlocks,
    'Create My Scent',
  );
  const workshopBt = getNameFromButtonBlock(props.buttonBlocks, 'Workshop');
  const workshopLink = getLinkFromButtonBlock(props.buttonBlocks, 'Workshop');
  const allProductBt = getNameFromButtonBlock(
    props.buttonBlocks,
    'All Products',
  );
  const allProductLink = getLinkFromButtonBlock(
    props.buttonBlocks,
    'All Products',
  );

  return (
    <div
      className={classnames(
        'menu-simple-v3 menu-full',
        props.isRemoveMessage ? 'mt-0' : '',
      )}
    >
      <div className="content-menu-full">
        <div className="image-logo">
          <img src={logo} alt="logo" onClick={onClickMenu} />
        </div>
        {!props.isEcommerce ? (
          <div className="list-button">
            {_.map(props.mergeMenuBlock, (d, index) => (d?.value?.dropdowns1.length === 0
              && d?.value?.dropdowns2.length === 0 ? (
                <LinkCT
                  onMouseEnter={() => props.setStepMenuDesktop(index + 1)}
                  to={getLink(d?.value?.link)}
                  className={classnames(
                    'button-bg__none bt-menu link-menu',
                    props.stepDesktop === index + 1 ? 'active' : '',
                  )}
                >
                  {d?.value?.header?.header_text}
                </LinkCT>
              ) : (
                // Shop
                <button
                  className={classnames(
                    'button-bg__none bt-menu',
                    props.stepDesktop === index + 1 ? 'active' : '',
                  )}
                  type="button"
                  onClick={() => props.setStepMenuDesktop(index + 1)}
                  onMouseEnter={() => props.setStepMenuDesktop(index + 1)}
                >
                  {d?.value?.header?.header_text}
                  <ExpandMoreIcon style={{ color: '#2c2c2c' }} />
                </button>
              )))}
          </div>
        ) : (
          <div className="list-button" />
        )}
        {!props.isEcommerce ? (
          <div className="button-custome">
            {createBt && (
              <ButtonCT
                name={createBt}
                onClick={() => gotoLink(createLink)}
                size="medium"
                onMouseUp={() => segmentNavbarPersonalityQuizInitiated()}
              />
            )}
            {workshopBt && (
              <ButtonCT
                name={workshopBt}
                color="secondary"
                size="medium"
                onClick={() => gotoLink(workshopLink)}
              />
            )}
          </div>
        ) : (
          <div className="button-custome">
            {allProductBt && (
              <button
                type="button"
                className="button-bg__none bt-all-product"
                onClick={() => gotoLink(allProductLink)}
              >
                {allProductBt}
                <ArrowForwardIcon />
              </button>
            )}
          </div>
        )}
      </div>
    </div>
  );
}

export default React.memo(MenuV3);
