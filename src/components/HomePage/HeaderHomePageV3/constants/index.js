export const COUNTRIES_ACCEPT_GIFTING_WORKSHOP = ['sg', 'vn', 'ae'];
export const [CREATE_KEY, GIFTING_KEY, ABOUT_KEY] = ['CREATE', 'GIFTING', 'ABOUT'];
export const [ALL_WORKSHOPS, ALL_GIFTS, GIFTS_EXCEP_WORKSHOP] = [5, 4, 3];
export const GIFT_WORKSHOP_NAME = 'Gift Workshop Experience';