import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import classnames from "classnames";
import _, { isEmpty } from "lodash";
import React from "react";
import ReactHtmlParser from "react-html-parser";
import LinkCT from "../../../../componentsv2/linkCT";
import { isMobile991fn } from "../../../../DetectScreen";
import ArrowRightIcon from "../../../../image/icon/menu-arrow-right.svg";
import { icBottle } from '../../../../imagev2/svg';
import { generateUrlWeb, segmentNavbarPersonalityQuizInitiated, segmentNavbarSubCategorySelected, toTitleCase } from "../../../../Redux/Helpers";
import auth from "../../../../Redux/Helpers/auth";
import { ALL_GIFTS, ALL_WORKSHOPS, COUNTRIES_ACCEPT_GIFTING_WORKSHOP, CREATE_KEY, GIFTING_KEY, GIFTS_EXCEP_WORKSHOP, GIFT_WORKSHOP_NAME } from "../constants";
import "./styles.scss";

function ItemMenu(props) {
  const getLink = (url, data = {}) => {
    if (url && url.includes("https://")) {
      return url;
    }
    return generateUrlWeb(url);
  };

  const itemMenu1 = (data) =>
    data?.length > 0 && (
      <React.Fragment>
        <LinkCT
          to={getLink(data[0]?.value?.button?.link)}
          className="link-menu-bold"
          onMouseUp={() => segmentNavbarSubCategorySelected({
            main_category: "Shop",
            sub_category: data[0]?.value?.button?.text
          })}
        >
          {data[0]?.value?.button?.text}
          <img src={ArrowRightIcon} alt="icon" />
        </LinkCT>

        {_.map(data.slice(1, data.length), (d, idx) =>
          idx === data.length - 2 ? (
            <div className="item-menu-mini div-col" key={idx}>
              <div className="div-link-text">
                <div className="div-image">
                  <img src={d?.value?.links?.image?.image} alt="shop" />
                  <div className="div-content">
                    <div className="div-top">
                      <div className="div-top-title">
                        {d?.value?.header?.header_text}
                      </div>
                      <div className="div-top-description">
                        {d?.value?.links?.text}
                      </div>
                    </div>
                    <LinkCT
                      className="div-bottom"
                      to={getLink(d?.value?.button?.link)}
                      onMouseUp={() => segmentNavbarPersonalityQuizInitiated()}
                    >
                      <button className="guide-btn" type="button">
                        {d?.value?.button?.text}
                      </button>
                    </LinkCT>
                  </div>
                </div>
              </div>
            </div>
          ) : (
            <div className="item-menu-mini div-col" key={idx}>
              <div className="div-link-text">
                <div className="c-t-header">
                  {d?.value?.header?.header_text}
                </div>
                <div className="line-menu" />
                {d?.value?.button?.text && d?.value?.button?.link && (
                  <LinkCT
                    to={getLink(d?.value?.button?.link)}
                    className="link-menu link-menu-black"
                    onMouseUp={() => segmentNavbarSubCategorySelected({
                      main_category: toTitleCase(d?.value?.header?.header_text),
                      sub_category: d?.value?.button?.text
                    })}
                  >
                    {d?.value?.button?.text}
                    <img src={ArrowRightIcon} alt="icon" />
                  </LinkCT>
                )}
                {d?.value?.links?.links.length > 0 &&
                  _.map(d?.value?.links.links, (l, index) => (
                    <LinkCT
                      key={index}
                      to={getLink(l?.value?.link)}
                      className="link-menu"
                      onMouseUp={() => segmentNavbarSubCategorySelected({
                        main_category: toTitleCase(d?.value?.header?.header_text),
                        sub_category: l?.value?.text
                      })}
                    >
                      {l?.value?.text}
                    </LinkCT>
                  ))}
              </div>
            </div>
          )
        )}
      </React.Fragment>
    );

  const itemMenu2 = (data, lastIdx) => (
    <div className="item-menu-mini div-col">
      {data?.length > 0 && (
        <div className="menu-wrapper">
          {/* All gifts */}
          {lastIdx !== ALL_WORKSHOPS && (
            <LinkCT
              to={getLink(data[data.length - 1]?.value?.link)}
              className="link-menu without-image"
              onMouseUp={() => segmentNavbarSubCategorySelected({
                main_category: lastIdx === ALL_WORKSHOPS ? "Workshop" : "Gifting",
                sub_category: data[data.length - 1]?.value?.text,
              })}
            >
              <div className="link-title text-uppercase">{data[data.length - 1]?.value?.text}</div>
              <div className="link-image">
                <img src={ArrowRightIcon} alt="icon" />
              </div>
            </LinkCT>
          )}

          <div className={`div-link-grid div-${lastIdx === ALL_WORKSHOPS ? ALL_WORKSHOPS + 1 : lastIdx}-item`}>
            {_.map(lastIdx === ALL_WORKSHOPS ? data : data.slice(0, lastIdx), (ct, idx) => (
              <LinkCT
                key={idx}
                to={getLink(ct?.value?.link)}
                className={`link-menu menu-${idx + 1}`}
                onMouseUp={() => segmentNavbarSubCategorySelected({
                  main_category: lastIdx === ALL_WORKSHOPS ? "Workshop" : "Gifting",
                  sub_category: ct?.value?.text,
                })}
              >
                <div className="image-link">
                  <img src={ct?.value?.image?.image} alt="navbar-image" />
                </div>
                <div className="link-title">
                  {ct?.value?.text}
                  <ArrowForwardIcon style={{ color: "#2C2C22" }} />
                </div>
              </LinkCT>
            ))}
          </div>
        </div>
      )}
    </div>
  );

  const renderItemMenu3s1 = (data) => (
    <div className="item-menu-block" key={data?.id}>
      <div className="item-menu-title">{data?.value?.text}</div>
      <div className="item-menu-list">
        {data?.value?.ctas?.length > 0 &&
          _.map(data.value.ctas, (ct, idx) => (
            <LinkCT
              className="item-menu-child"
              key={idx}
              to={getLink(ct?.value?.link)}
              onMouseUp={() => segmentNavbarSubCategorySelected({
                main_category: "About",
                sub_category: ct?.value?.text
              })}
            >
              <div className="image-child">
                <img src={ct?.value?.image?.image} alt="about" />
              </div>
              <div className="detail-child">
                <div className="title-child">{ct?.value?.text}</div>
                <div className="description-child">
                  {ReactHtmlParser(ct?.value?.description)}
                </div>
              </div>
            </LinkCT>
          ))}
      </div>
    </div>
  );

  const renderItemMenu3s2 = (data) => (
    <div className="item-menu-block" key={data?.id}>
      {!isMobile991fn() && (
        <div className="item-menu-banner">
          <img src={data?.value?.links?.image?.image} alt="about" />
        </div>
      )}
      <div className="item-menu-title">{data?.value?.header?.header_text}</div>

      <div className="item-menu-list">
        {data?.value?.links?.links?.length > 0 &&
          _.map(data?.value?.links?.links, (li, idx) => (
            <LinkCT
              key={idx}
              className="item-menu-child"
              to={getLink(li?.value?.link)}
              onMouseUp={() => {
                  const regexx = /https\:\/\/www\.maison21g\.com\/[a-z]{2}\/landingquiz/g;
                  if (regexx.test(`${li?.value?.link}`)) {
                    segmentNavbarPersonalityQuizInitiated()
                  } else {
                    segmentNavbarSubCategorySelected({
                      main_category: "About",
                      sub_category: ReactHtmlParser(li?.value?.text)?.[0]
                    })
                  }
                }
              }
            >
              <div className="detail-child">
                <div className="description-child text-bigger">
                  {ReactHtmlParser(li?.value?.text)}
                </div>
              </div>
            </LinkCT>
          ))}
      </div>
    </div>
  );

  const itemMenu3About = (datas1, datas2) => (
    <div className="item-menu-mini div-col">
      {datas1?.length > 0 && _.map(datas1, (s1) => renderItemMenu3s1(s1))}

      {datas2?.length > 0 && _.map(datas2, (s2) => renderItemMenu3s2(s2))}
    </div>
  );

  const itemMenu3Create = (button, dataMenu) => ( // Create
  <div className="menu-item-wrapper menu-item-product menu-create">
    <LinkCT to={getLink(button?.link)} className="menu-create-title">
      <div className="menu-create-icon">
        <img src={icBottle} alt='icon' />
      </div>
      <div className="menu-create-text">{button?.text}</div>
      <div className="menu-create-arrow">
        <img src={ArrowRightIcon} alt='icon' />
      </div>
    </LinkCT>

    <ul className="menu-create-content">
      {_.map(dataMenu, x => (
        <li key={x?.id} className="menu-item">
          <LinkCT
            className='menu-item-link'
            to={getLink(x?.value?.button?.link)}
            onMouseUp={() => segmentNavbarSubCategorySelected({
              main_category: 'Create',
              sub_category: x?.value?.button?.text
            })}
          >
            <div className="menu-item-image">
              <img src={x?.value?.links?.image?.image} alt='image' />
              <div className="menu-item-icon">
                <img src={x?.icon} alt='icon' />
              </div>
            </div>

            <div className="menu-item-name">
              <div className="text">{x?.value?.button?.text}</div>
              <img src={ArrowRightIcon} alt='icon' />
            </div>
          </LinkCT>
        </li>
      ))}
    </ul>
  </div>
)

  const handleRenderMenuItems = (dataMenu) => {
    switch(dataMenu?.link) {
      case 'menu-1': // Shop
      return itemMenu1(dataMenu?.dropdowns2);

      case 'menu-2': // Gifting & Workshop
        // On VN, SG, AE has 4 giftings
        // Another has 3 giftings (except workshop)

        if(dataMenu?.dropdowns1?.length > 0 && dataMenu?.dropdowns1[0]?.value?.text === GIFTING_KEY) {
          const countryCode = auth.getCountry();
          if(_.includes(COUNTRIES_ACCEPT_GIFTING_WORKSHOP, countryCode)) {
            return _.map(dataMenu?.dropdowns1, (dr) => itemMenu2(dr?.value?.ctas, ALL_GIFTS));
          }else {
            const allGiftings = dataMenu?.dropdowns1?.length > 0 ? dataMenu?.dropdowns1[0]?.value?.ctas : [];
            const filterGiftings = _.filter(allGiftings, gift => gift?.value?.text !== GIFT_WORKSHOP_NAME);

            return itemMenu2(filterGiftings, GIFTS_EXCEP_WORKSHOP);
          }

          // Workshop
        }else return dataMenu?.dropdowns1.map((dr) => itemMenu2(dr?.value?.ctas, ALL_WORKSHOPS));

      case 'menu-3': // Create & About
      if(dataMenu?.dropdowns1?.length > 0 && dataMenu?.dropdowns1[0]?.value?.text === CREATE_KEY) {
        const button = dataMenu?.dropdowns1?.length > 0 ? dataMenu?.dropdowns1[0]?.value?.button : '';
        const data = _.map(dataMenu?.dropdowns2 || [], x => {
          const item = _.find(dataMenu?.dropdowns1?.length > 0 ? dataMenu?.dropdowns1[0]?.value?.ctas : [], y => y?.value?.text === x?.value?.button?.name);
          if(!isEmpty(item)) {
            return {
              ...x,
              icon: item?.value?.image?.image,
            };
          }
        });

        return itemMenu3Create(button, data);
      }else return itemMenu3About(dataMenu?.dropdowns1, dataMenu?.dropdowns2);

      default: return <div />
    }
  }

  const isOpen = props.indexOpen === props.id;
  return (
    <div className="item-menu-v3 div-col">
      {props.data?.dropdowns2?.length === 0 &&
      props.data?.dropdowns1?.length === 0 ? (
        <LinkCT
          to={getLink(props.data?.link)}
          className="header-1 link-direct"
          onMouseUp={() => segmentNavbarSubCategorySelected({
            main_category: "Club21G",
            sub_category: "Club21G"
          })}
          >
          {props.data?.header?.header_text}
        </LinkCT>
      ) : (
        <div
          className={classnames("header-1", isOpen ? "opened" : "")}
          onClick={() => props.onClickIndexMenu(props.id)}
        >
          {props.data?.header?.header_text}
          <KeyboardArrowDownIcon style={{ color: "#222222" }} />
        </div>
      )}

      {isOpen && (
        <div
          className={classnames(
            "content-header",
            props.data?.link === "menu-1" ? "scroll-able" : ""
          )}
        >
          {handleRenderMenuItems(props.data)}
        </div>
      )}
    </div>
  );
}

export default React.memo(ItemMenu);
