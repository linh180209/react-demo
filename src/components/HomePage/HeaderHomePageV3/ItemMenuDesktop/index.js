import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import classNames from 'classnames';
import _, { isEmpty } from 'lodash';
import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import LinkCT from '../../../../componentsv2/linkCT';
import ArrowRightIcon from '../../../../image/icon/menu-arrow-right.svg';
import {
  generateUrlWeb, segmentNavbarPersonalityQuizInitiated, segmentNavbarSubCategorySelected, toTitleCase,
} from '../../../../Redux/Helpers';
import auth from '../../../../Redux/Helpers/auth';
import {
  ALL_GIFTS, ALL_WORKSHOPS, COUNTRIES_ACCEPT_GIFTING_WORKSHOP, CREATE_KEY, GIFTING_KEY, GIFTS_EXCEP_WORKSHOP, GIFT_WORKSHOP_NAME,
} from '../constants';
import './styles.scss';
import { icBottle } from '../../../../imagev2/svg';

function ItemMenuDesktop(props) {
  const getLink = (url) => {
    if (url && url.includes('https://')) {
      return url;
    }
    return generateUrlWeb(url);
  };

  const itemMenu5 = (item, idx, lastIdx) => (
    <LinkCT
      key={idx}
      to={getLink(item?.value?.link)}
      className={classNames('item-menu-5', `item-${idx + 1}`)}
      onMouseUp={() => segmentNavbarSubCategorySelected({
        main_category: lastIdx === ALL_WORKSHOPS ? 'Workshop' : 'Gifting',
        sub_category: item?.value?.text,
      })}
    >
      <div className="image-left">
        <img src={item?.value?.image?.image} alt="icon" />
      </div>
      <div className="div-text-bottom">
        <div className="bt-next">
          {item?.value?.text}
          <ArrowForwardIcon style={{ color: '#2C2C22' }} />
        </div>
      </div>
    </LinkCT>
  );

  const guideMeBox = (item, idx) => ( // Guide Me -- under shop
    <div className="item-menu-1-4" key={idx}>
      <div className="div-right-image">
        <div className="image-ct">
          <img loading="lazy" src={item?.value?.links?.image?.image} alt="guide-img" />
          <div className="content-inside">
            <div className="content-inside-top">
              <div className="main-title">
                {item?.value?.header?.header_text}
              </div>
              <div className="sub-title">{item?.value?.links?.text}</div>
            </div>
            <LinkCT
              to={getLink(item.value?.button?.link)}
              className="content-inside-bottom"
              onMouseUp={() => segmentNavbarPersonalityQuizInitiated()}
            >
              <button className="guide-btn" type="button">
                {item?.value?.button?.text}
              </button>
            </LinkCT>
          </div>
        </div>
      </div>
    </div>
  );

  const renderItemMenu2 = dataSlice => (
    <div className="menu-list-product">
      {/* {dataSlice?.map?.((item, idx) => (idx === dataSlice.length - 1 ? guideMeBox(item, idx) : ( */}
      {dataSlice.slice(0, 3)?.map?.((item, idx) => ((
      // List another menu item
        <div className="item-menu-1-4" key={idx}>
          <div className="div-left-list">
            <div className="sub-header">
              {item?.value?.header?.header_text}
            </div>
            <div className="list-link">
              {item?.value?.button?.name && (
              <LinkCT
                className="submenu-link-product"
                to={getLink(item?.value?.button?.link)}
                onMouseUp={() => segmentNavbarSubCategorySelected({
                  main_category: toTitleCase(item?.value?.header?.header_text),
                  sub_category: item?.value?.button?.text,
                })}
              >
                <div className="menu-title-product">
                  {item?.value?.button?.text}
                </div>
                <img
                  loading="lazy"
                  className="arrow-icon"
                  src={ArrowRightIcon}
                  alt="arrow-icon"
                />
              </LinkCT>
                )}
              {item?.value?.links?.links?.length > 0
                  && item.value.links.links?.map((l, index) => (
                    <LinkCT
                      key={index}
                      to={getLink(l?.value?.link)}
                      className="item-link"
                      onMouseUp={() => segmentNavbarSubCategorySelected({
                        main_category: toTitleCase(item?.value?.header?.header_text),
                        sub_category: l?.value?.text,
                      })}
                    >
                      {l?.value?.text}
                    </LinkCT>
                  ))}
            </div>
          </div>
        </div>
      ))
      )}
    </div>
  );

  const itemMenu1 = datas2 => datas2?.length > 0
    && datas2[0]?.value?.header?.header_text && (
      <div className="menu-item-wrapper menu-item-product">
        <LinkCT
          className="menu-link-product"
          to={getLink(datas2[0]?.value?.button?.link)}
          onMouseUp={() => segmentNavbarSubCategorySelected({
            main_category: 'Shop',
            sub_category: 'Shop all products',
          })}
        >
          <div className="menu-title-product">
            {datas2[0]?.value?.header?.header_text}
          </div>
          <img loading="lazy" className="arrow-icon" src={ArrowRightIcon} alt="arrow-icon" />
        </LinkCT>

        {renderItemMenu2(datas2.slice(1, datas2.length))}
      </div>
    );

  const itemMenu2 = (data, lastIdx) => data?.length > 0 && (
  <div className="menu-flex-row menu-item-wrapper">
    <div className={`div-grid-item div-${lastIdx === ALL_WORKSHOPS ? ALL_WORKSHOPS + 1 : lastIdx}-item`}>
      {data.slice(0, lastIdx).map((d, idx) => itemMenu5(d, idx, lastIdx))}
    </div>
    <div className="div-divider" />
    <LinkCT
      to={getLink(data[data.length - 1]?.value?.link)}
      className={`div-image ${lastIdx === GIFTS_EXCEP_WORKSHOP ? 'image-equal-height' : ''}`}
      onMouseUp={() => segmentNavbarSubCategorySelected({
        main_category: lastIdx === ALL_WORKSHOPS ? 'Workshop' : 'Gifting',
        sub_category: lastIdx === ALL_WORKSHOPS ? 'View all workshops' : 'All Giftings',
      })}
    >
      <div className="image-ct">
        <img loading="lazy" src={data[data.length - 1]?.value?.image?.image} alt="icon" />
      </div>
      <div className="bt-next">
        {data[data.length - 1]?.value?.text}
        <ArrowForwardIcon style={{ color: '#2C2C22' }} />
      </div>
    </LinkCT>
  </div>
  );

  const renderItemMenu31 = data => ( // About -- about, our boutiques, our ambassadors
    <div className="div-content-item div-content-item-1">
      <div className="item-title">{data?.value?.text}</div>
      <div className="item-list">
        {data?.value?.ctas?.length > 0
          && data?.value?.ctas?.map?.((ct, idx) => (
            <LinkCT
              className="item-child"
              key={idx}
              to={getLink(ct?.value?.link)}
              onMouseUp={() => segmentNavbarSubCategorySelected({
                main_category: 'About',
                sub_category: ct?.value?.text,
              })}
            >
              <div className="item-child-image">
                <img loading="lazy" src={ct?.value?.image?.image} alt="about" />
              </div>
              <div className="item-child-detail">
                <div className="item-child-title">{ct?.value?.text}</div>
                <div className="item-child-des">
                  {ReactHtmlParser(ct?.value?.description)}
                </div>
              </div>
            </LinkCT>
          ))}
      </div>
    </div>
  );

  const renderItemMenu32 = (data, idx) => ( // About -- our resources & take quiz - the right side
    <div className={`div-content-item div-content-item-${2 + idx}`}>
      <div className="div-img">
        <img loading="lazy" src={data?.value?.links?.image?.image} alt="about" />
      </div>

      <div className="div-content-item">
        {data?.value?.text
          ? (
            <LinkCT
              className="item-link"
              to={getLink(data?.value?.text)}
              onMouseUp={
              () => {
                segmentNavbarSubCategorySelected({
                  main_category: 'About',
                  sub_category: 'Our Resources',
                });
              }
            }
            >
              <div className="item-title">
                {data?.value?.header?.header_text}
                <img className="arrow-icon" src={ArrowRightIcon} alt="arrow-icon" />
              </div>
            </LinkCT>
)
          : <div className="item-title">{data?.value?.header?.header_text}</div>
        }
        <div className="item-list">
          {data?.value?.links?.links?.length > 0
            && data?.value?.links?.links?.map?.((li, index) => (
              <LinkCT
                className="item-child item-without-image"
                to={getLink(li?.value?.link)}
                onMouseUp={() => {
                  const regexx = /https\:\/\/www\.maison21g\.com\/[a-z]{2}\/landingquiz/g;
                  if (regexx.test(`${li?.value?.link}`)) {
                    segmentNavbarPersonalityQuizInitiated();
                  } else {
                    segmentNavbarSubCategorySelected({
                      main_category: 'About',
                      sub_category: ReactHtmlParser(li?.value?.text)?.[index],
                    });
                  }
                }
              }
              >
                {li?.value?.text}
              </LinkCT>
            ))}
        </div>
      </div>
    </div>
  );

  const itemMenu3About = (datas1, datas2) => (
    <div
      className={`${
        datas1?.length > 0 || datas2?.length > 0 ? 'menu-item-wrapper' : ''
      }`}
    >
      <div
        className={`menu-item-content ${
          datas2?.length <= 1 ? 'style-2' : 'style-3'
        }`}
      >
        {datas1?.length > 0 && datas1.map(d1 => renderItemMenu31(d1))}
        {datas2?.length > 0 && datas2.map((d2, idx) => (idx === datas2?.length - 1 ? guideMeBox(d2, idx) : renderItemMenu32(d2, idx)))}
      </div>
    </div>
  );

  const itemMenu3Create = (button, dataMenu) => ( // Create
    <div className="menu-item-wrapper menu-item-product menu-create">
      <LinkCT className="menu-create-title" to={getLink(button?.link)}>
        <div className="menu-create-icon">
          <img src={icBottle} alt="icon" />
        </div>
        <div className="menu-create-text">{button?.text}</div>
        <div className="menu-create-arrow">
          <img src={ArrowRightIcon} alt="icon" />
        </div>
      </LinkCT>

      <ul className="menu-create-content">
        {_.map(dataMenu, x => (
          <li key={x?.id} className="menu-item">
            <LinkCT
              className="menu-item-link"
              to={getLink(x?.value?.button?.link)}
              onMouseUp={() => segmentNavbarSubCategorySelected({
                main_category: 'Create',
                sub_category: x?.value?.button?.text,
              })}
            >
              <div className="menu-item-image">
                <img src={x?.value?.links?.image?.image} alt="image" />
                <div className="menu-item-icon">
                  <img src={x?.icon} alt="icon" />
                </div>
              </div>

              <div className="menu-item-name">
                <div className="text">{x?.value?.button?.text}</div>
                <img src={ArrowRightIcon} alt="icon" />
              </div>
            </LinkCT>
          </li>
        ))}
      </ul>
    </div>
  );

  const handleRenderMenuItems = (dataMenu) => {
    switch (dataMenu?.link) {
      case 'menu-1': // Shop
        console.log('dataMenu?.dropdowns2', dataMenu?.dropdowns2);
        return itemMenu1(dataMenu?.dropdowns2);

      case 'menu-2': // Gifting & Workshop
        // On VN, SG, AE has 4 giftings
        // Another has 3 giftings (except workshop)

        if (dataMenu?.dropdowns1?.length > 0 && dataMenu?.dropdowns1[0]?.value?.text === GIFTING_KEY) {
          const countryCode = auth.getCountry();
          if (_.includes(COUNTRIES_ACCEPT_GIFTING_WORKSHOP, countryCode)) {
            return _.map(dataMenu?.dropdowns1, dr => itemMenu2(dr?.value?.ctas, ALL_GIFTS));
          }
          const allGiftings = dataMenu?.dropdowns1?.length > 0 ? dataMenu?.dropdowns1[0]?.value?.ctas : [];
          const filterGiftings = _.filter(allGiftings, gift => gift?.value?.text !== GIFT_WORKSHOP_NAME);

          return itemMenu2(filterGiftings, GIFTS_EXCEP_WORKSHOP);


          // Workshop
        } return dataMenu?.dropdowns1.map(dr => itemMenu2(dr?.value?.ctas, ALL_WORKSHOPS));

      case 'menu-3': // Create & About
        if (dataMenu?.dropdowns1?.length > 0 && dataMenu?.dropdowns1[0]?.value?.text === CREATE_KEY) {
          const button = dataMenu?.dropdowns1?.length > 0 ? dataMenu?.dropdowns1[0]?.value?.button : '';
          const data = _.map(dataMenu?.dropdowns2 || [], (x) => {
            const item = _.find(dataMenu?.dropdowns1?.length > 0 ? dataMenu?.dropdowns1[0]?.value?.ctas : [], y => y?.value?.text === x?.value?.button?.name);
            if (!isEmpty(item)) {
              return {
                ...x,
                icon: item?.value?.image?.image,
              };
            }
          });

          return itemMenu3Create(button, data);
        } return itemMenu3About(dataMenu?.dropdowns1, dataMenu?.dropdowns2);

      default: return <div />;
    }
  };

  return (
    <div className="item-menuV3-desktop">
      {handleRenderMenuItems(props.data)}
    </div>
  );
}

export default ItemMenuDesktop;
