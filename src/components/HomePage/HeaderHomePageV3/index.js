import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { isMobile991fn } from '../../../DetectScreen';
import addCmsRedux from '../../../Redux/Actions/cms';
import { fetchCMSHomepage } from '../../../Redux/Helpers';
import { toastrError } from '../../../Redux/Helpers/notification';
import { useMergeState } from '../../../Utils/customHooks';
import DrawerMenuV3 from './DrawMenuV3';
import MenuV3 from './MenuV3';
import MessageMenuV3 from './messageMenuV3';
import './styles.scss';

const getCmsMenu = (cmsMenu) => {
  if (cmsMenu) {
    const mergeMenuBlock = _.filter(
      cmsMenu.body,
      x => x.type === 'new_mega_menu_block',
    );
    const buttonBlocks = _.filter(
      cmsMenu.body,
      x => x.type === 'button_block',
    );
    return { mergeMenuBlock, buttonBlocks };
  }
  return { mergeMenuBlock: [], buttonBlocks: [] };
};

function HeaderHomePageV3(props) {
  const dispatch = useDispatch();
  const cms = useSelector(state => state.cms);

  const [state, setState] = useMergeState({
    mergeMenuBlock: [],
  });

  const [isOpenMenuMobile, setIsOpenMenuMobile] = useState();
  const [stepDesktop, setStepDesktop] = useState(0);

  const toggleOpenMenu = () => {
    if (isMobile991fn()) {
      setIsOpenMenuMobile(!isOpenMenuMobile);
    } else {
      setStepDesktop(0);
    }
  };

  const setStepMenuDesktop = (step) => {
    if (step !== stepDesktop) {
      setStepDesktop(step);
    }
  };

  const fetchData = async () => {
    if (cms) {
      const cmsHamburger = _.find(cms, x => x.title === 'Hamburger-v4');

      if (!cmsHamburger) {
        try {
          const result = await fetchCMSHomepage('hamburger-v4');
          dispatch(addCmsRedux(result));
          const menuBlock = getCmsMenu(result);

          setState(menuBlock);
        } catch (error) {
          toastrError(error.message);
        }
      } else {
        const menuBlock = getCmsMenu(cmsHamburger);
        setState(menuBlock);
      }
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <React.Fragment>
      {!props.isRemoveMessage && (
        <nav className="nav-home-page">
          <MessageMenuV3 />
        </nav>
      )}
      <MenuV3
        setStepMenuDesktop={setStepMenuDesktop}
        stepDesktop={stepDesktop}
        isSimple={props.isSimple}
        toggleOpenMenu={toggleOpenMenu}
        mergeMenuBlock={state.mergeMenuBlock}
        buttonBlocks={state.buttonBlocks}
        isRemoveMessage={props.isRemoveMessage}
        isEcommerce={props.isEcommerce}
      />
      {!props.isEcommerce && (
        <DrawerMenuV3
          mergeMenuBlock={state.mergeMenuBlock}
          buttonBlocks={state.buttonBlocks}
          isOpenMenuMobile={isOpenMenuMobile || stepDesktop > 0}
          toggleOpenMenu={toggleOpenMenu}
          stepDesktop={stepDesktop}
          isRemoveMessage={props.isRemoveMessage}
        />
      )}
    </React.Fragment>
  );
}

export default HeaderHomePageV3;
