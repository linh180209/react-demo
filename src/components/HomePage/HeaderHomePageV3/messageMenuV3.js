import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import ShoppingBasketOutlinedIcon from '@mui/icons-material/ShoppingBasketOutlined';
import _ from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';
import { GET_SHIPPING_URL } from '../../../config';
import AppFlowActions from '../../../constants';
import addCmsRedux from '../../../Redux/Actions/cms';
import updateShippings from '../../../Redux/Actions/shippings';
import {
  fetchCMSHomepage,
  generaCurrency,
  generateUrlWeb,
  getNameFromButtonBlock,
} from '../../../Redux/Helpers';
import auth from '../../../Redux/Helpers/auth';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { useMergeState } from '../../../Utils/customHooks';
import useEmitter from '../../../Utils/useEmitter';
import Region from '../../Region';
import PopupAddToCart from '../PopupAddToCart';

const getCms = (data) => {
  const { body } = data;
  const richBlocks = _.filter(body, x => x.type === 'rich_text_block');
  const buttonBlocks = _.filter(body, x => x.type === 'button_block');
  return { richBlocks, buttonBlocks };
};

const getCmsCardPage = (data) => {
  if (data) {
    const { body } = data;
    const buttonBlockCard = _.filter(body, x => x.type === 'button_block');
    const paragraphBlockCard = _.find(
      body,
      x => x.type === 'paragraph_block',
    ).value;
    const iconsBlockCard = _.find(
      body,
      x => x.type === 'icons_block' && x.value?.image_background?.caption === '',
    ).value;
    return { buttonBlockCard, paragraphBlockCard, iconsBlockCard };
  }
  return {
    buttonBlockCard: [],
    paragraphBlockCard: undefined,
    iconsBlockCard: undefined,
  };
};

function SampleNextArrow(props) {
  // eslint-disable-next-line react/prop-types
  const { onClick } = props;
  return (
    <button
      type="button"
      className="bt-arrow-menu bt-arrow-menu-right"
      onClick={onClick}
    >
      <i className="fa fa-chevron-right" />
    </button>
  );
}

function SamplePrevArrow(props) {
  // eslint-disable-next-line react/prop-types
  const { onClick } = props;
  return (
    <button
      type="button"
      className="bt-arrow-menu bt-arrow-menu-left"
      onClick={onClick}
    >
      <i className="fa fa-chevron-left" />
    </button>
  );
}

function MessageMenuV3(props) {
  const history = useHistory();
  const cms = useSelector(state => state.cms);
  const shippings = useSelector(state => state.shippings);
  const countries = useSelector(state => state.countries);
  const basket = useSelector(state => state.basket);
  const oldSizeBasket = useRef(-1);

  const dispatch = useDispatch();

  const [state, setState] = useMergeState({
    data: [],
    buttonBlocks: [],
  });
  const [isOpenAddtoCartPopup, setIsOpenAddtoCartPopup] = useState(false);
  const [dataPopup, setDataPopup] = useState();

  const fetchDataInit = async () => {
    const messageBlock = _.find(cms, x => x.title === 'Message bar');
    const cmsHamburger = _.find(cms, x => x.title === 'Card Page');
    if (!messageBlock) {
      const pending = [
        fetchCMSHomepage('message-bar'),
        fetchCMSHomepage('card-page'),
      ];
      const cmsDatas = await Promise.all(pending);
      dispatch(addCmsRedux(cmsDatas[0]));
      dispatch(addCmsRedux(cmsDatas[1]));
      const body = getCms(cmsDatas[0]);
      const cardBlock = getCmsCardPage(cmsDatas[1]);
      const data = _.map(body.richBlocks, x => x.value);
      setState(
        _.assign(
          {
            data,
            buttonBlocks: body.buttonBlocks,
          },
          cardBlock,
        ),
      );
    } else {
      const body = getCms(messageBlock);
      const cardBlock = getCmsCardPage(cmsHamburger);
      const data = _.map(body.richBlocks, x => x.value);
      setState(
        _.assign(
          {
            data,
            buttonBlocks: body.buttonBlocks,
          },
          cardBlock,
        ),
      );
    }
  };

  const fetchShipping = () => {
    if (shippings?.length > 0) {
      const shippingInfo = _.find(
        shippings,
        d => d.country.code === auth.getCountry(),
      );
      setState({ shippingInfo });
      return;
    }
    const options = {
      url: GET_SHIPPING_URL,
      method: 'GET',
    };
    fetchClient(options)
      .then((result) => {
        if (result) {
          const shippingInfo = _.find(
            result,
            d => d.country.code === auth.getCountry(),
          );
          setState({ shippingInfo });
          dispatch(updateShippings(result));
        }
      })
      .catch((err) => {
        console.log('error', err);
      });
  };

  const getCurrencyCountry = () => {
    const ele = _.find(countries || [], x => x?.code === auth.getCountry());
    return ele?.name;
  };

  const gotoLogin = () => {
    history.push(generateUrlWeb('/login'));
  };

  const gotoCart = () => {
    history.push(generateUrlWeb('/cart'));
  };

  const closeAddtoCartPopup = () => {
    setIsOpenAddtoCartPopup(false);
    setDataPopup(null);
  };

  const viewShoppingBag = () => {
    // setIsOpenAddtoCartPopup(false)
  };

  useEffect(() => {
    const newSize = basket.id && basket.items
      ? _.filter(
            basket?.items || [],
            d => d.is_free === false && d.is_free_gift === false,
      ).length
      : -1;
    if (newSize > oldSizeBasket.current && oldSizeBasket.current !== -1) {
      if (!window.location.href.includes('/checkout')) {
        setIsOpenAddtoCartPopup(true);
      }
    }
    oldSizeBasket.current = newSize;
  }, [basket]);

  useEmitter(
    AppFlowActions.ADD_TO_CART_POPUP,
    (data) => {
      setDataPopup(data);
    },
    [],
  );

  useEffect(() => {
    fetchDataInit();
    fetchShipping();
  }, []);

  const settings = {
    className: 'custome-message',
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    autoplay: true,
    arrows: false,
    autoplaySpeed: 5000,
    // nextArrow: <SampleNextArrow />,
    // prevArrow: <SamplePrevArrow />,
  };
  const basketNotFree = _.filter(
    basket?.items || [],
    d => d.is_free === false,
  );

  const freeDeliveryBt = getNameFromButtonBlock(
    state.buttonBlocks,
    'FREE_DELIVERY',
  );
  const freeDeliveryOverBt = getNameFromButtonBlock(
    state.buttonBlocks,
    'FREE_DELIVERY_OVER',
  );
  return (
    <div className="message-menu-v2">
      <PopupAddToCart
        buttonBlocks={state.buttonBlockCard}
        data={dataPopup || basketNotFree.length > 0 ? basketNotFree[0] : {}}
        cms={props.cms}
        isOpen={dataPopup || isOpenAddtoCartPopup}
        onClose={closeAddtoCartPopup}
        viewShoppingBag={gotoCart}
      />
      <div className="message-content div-row">
        <Region
          countries={countries}
          buttonBlocks={state.buttonBlockCard}
          history={history}
          isVersion2
        />
        <Slider {...settings}>
          {_.map(state.data.concat({}), (d, index) => {
            if (_.isEmpty(d)) {
              return (
                <h4
                  className="header_4 animated fadeIn text-message"
                  key={0}
                  style={{ color: '#ffffff' }}
                >
                  {state.shippingInfo
                  && parseFloat(state.shippingInfo.threshold) > 0
                    ? `${freeDeliveryOverBt.replace(
                      '{price}',
                      generaCurrency(state.shippingInfo.threshold, true),
                    )}`
                    : freeDeliveryBt}
                </h4>
              );
            }
            return (
              <h4
                className="header_4 animated fadeIn text-message"
                key={index + 1}
                style={{ color: '#ffffff' }}
              >
                {ReactHtmlParser(d)}
              </h4>
            );
          })}
        </Slider>
        <div className="right-block div-row">
          <div className="click-icon" onClick={gotoLogin}>
            <PersonOutlineIcon />
          </div>
          <div className="click-icon" onClick={gotoCart}>
            <ShoppingBasketOutlinedIcon />
            {basket?.items?.length > 0 && (
              <div className="number-total">{basket?.items?.length}</div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default React.memo(MessageMenuV3);
