import classnames from 'classnames';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import ButtonCT from '../../../../componentsv2/buttonCT';
import { getHeight, isMobile480fn, isMobile991fn } from '../../../../DetectScreen';
import {
  generateUrlWeb,
  getLinkFromButtonBlock,
  getNameFromButtonBlock,
  gotoUrl,
  segmentNavbarPersonalityQuizInitiated,
} from '../../../../Redux/Helpers';
import { disableScrollBody, enableScrollBody } from '../../../../Utils';
import { useMergeState } from '../../../../Utils/customHooks';
import useWindowEvent from '../../../../Utils/useWindowEvent';
import ItemMenu from '../ItemMenu';
import ItemMenuDesktop from '../ItemMenuDesktop';
import './styles.scss';

function DrawerMenuV2(props) {
  const history = useHistory();
  const [indexOpen, setIndexOpen] = useState();
  const [state, setState] = useMergeState({
    isRefresh: false,
  });

  const onClickIndexMenu = (index) => {
    if (indexOpen === index) {
      setIndexOpen(-1);
    } else {
      setIndexOpen(index);
    }
  };

  const getHeightRoot = () => {
    const ele = document.getElementById('root');
    return ele?.offsetHeight;
  };

  const gotoLink = (link) => {
    // segmentNavbarSubCategorySelected({
    //   link: link,
    //   buttonBlocks:  props.buttonBlocks
    // });
    // history.push(generateUrlWeb(link));
    gotoUrl(link, history, true);
  };

  const onChangeSize = () => {
    setState({ isRefresh: !state.isRefresh });
  };

  useWindowEvent('resize', onChangeSize, window);

  useEffect(() => {
    if (!props.isOpenMenuMobile) {
      setIndexOpen(-1);
      enableScrollBody();
    } else {
      disableScrollBody();
    }
    return () => {
      enableScrollBody();
    };
  }, [props.isOpenMenuMobile]);

  const createBt = getNameFromButtonBlock(
    props.buttonBlocks,
    'Create My Scent',
  );
  const createLink = getLinkFromButtonBlock(
    props.buttonBlocks,
    'Create My Scent',
  );
  const workshopBt = getNameFromButtonBlock(props.buttonBlocks, 'Workshop');
  const workshopLink = getLinkFromButtonBlock(props.buttonBlocks, 'Workshop');
  const heightMenu = isMobile480fn()
    ? props.isRemoveMessage
      ? 72
      : 105
    : props.isRemoveMessage
      ? 72
      : 108;

  return (
    <div
      className={classnames(
        'drawer-menu-v3',
        props.isRemoveMessage ? 'menu-simple' : '',
      )}
      style={{
        height: getHeightRoot() - heightMenu,
        visibility: props.isOpenMenuMobile ? 'visible' : 'hidden',
      }}
    >
      {isMobile991fn()
      // mobile
        ? (
          <div
            className={classnames(
              'drawer-content',
              props.isOpenMenuMobile ? 'open' : '',
            )}
            style={isMobile480fn() ? { maxHeight: `${getHeight() - 105}px`, overflowY: 'auto' } : {}}
          >
            {_.map(props.mergeMenuBlock, (d, index) => (
              <ItemMenu
                data={d?.value}
                indexOpen={indexOpen}
                id={index}
                onClickIndexMenu={onClickIndexMenu}
              />
            ))}
            <div className="button-menu div-col">
              {createBt && (
              <ButtonCT
                name={createBt}
                size="medium"
                onClick={() => gotoLink(createLink)}
                onMouseUp={() => segmentNavbarPersonalityQuizInitiated()}
              />
              )}
              {workshopBt && (
              <ButtonCT
                name={workshopBt}
                size="medium"
                color="secondary"
                onClick={() => gotoLink(workshopLink)}
              />
              )}
            </div>
          </div>
        )
      // desktop
        : (
          <div
            className={classnames(
              'drawer-content',
              props.isOpenMenuMobile ? 'open' : '',
            )}
          >
            <div className="content-desktop">
              {props.stepDesktop > 0 && (
              <ItemMenuDesktop
                data={props.mergeMenuBlock[props.stepDesktop - 1]?.value}
              />
              )}
            </div>
          </div>
        )}

      <div
        className="out-click"
        onClick={props.toggleOpenMenu}
        onMouseEnter={() => props.toggleOpenMenu()}
      />
    </div>
  );
}

export default React.memo(DrawerMenuV2);
