import React, { useState, useEffect } from 'react';
import icGift from '../../image/icon/icFreeGift.png';
import icClose from '../../image/icon/ic-close-free-gift.svg';
import icNext from '../../image/icon/ic-next-free-gift.svg';
import { generaCurrency, getNameFromButtonBlock } from '../../Redux/Helpers';
import icCheck from '../../image/icon/ic-check-bg-green.svg';

function FreeGiftHeader(props) {
  const isEnoughed = props.price >= props.amount;

  const getFreeGiftBt = getNameFromButtonBlock(props.buttonBlocks, 'get_free_gifts');
  const freeSamplesBt = getNameFromButtonBlock(props.buttonBlocks, 'free_sameples');
  const whileStockBt = getNameFromButtonBlock(props.buttonBlocks, 'While_stocks_last');
  const qualifiedBt = getNameFromButtonBlock(props.buttonBlocks, 'Qualified');
  const freeGiftBt = getNameFromButtonBlock(props.buttonBlocks, 'Free_gifts_selected');
  const spendBt = getNameFromButtonBlock(props.buttonBlocks, 'Spend');
  const orMoreBt = getNameFromButtonBlock(props.buttonBlocks, 'or_more_to_qualify');
  const togoBt = getNameFromButtonBlock(props.buttonBlocks, 'to_go');

  return (
    <div className="free-gift div-row">
      <div className="div-left">
        <img loading="lazy" src={icGift} alt="gift" />
      </div>
      <div className="div-right div-col">
        <div className="div-text div-row">
          <div className="div-col">
            <h3>
              {getFreeGiftBt}
            </h3>
            <span>
              {freeSamplesBt}
              {' '}
              {generaCurrency(props.amount)}
              ! (
              {whileStockBt}
              )
            </span>
          </div>
          <button type="button" onClick={props.onClickOpen}>
            <img loading="lazy" src={icNext} alt="icon" />
          </button>
        </div>
        {
          isEnoughed ? (
            <div className="div-text-bottom div-row">
              <div className="div-check div-row mt-0 mb-0">
                <img loading="lazy" src={icCheck} alt="check" />
                <span className="text-green">
                  {qualifiedBt}
                </span>
              </div>
              <span>
                {props.numberGift}
                {' '}
                {freeGiftBt}
              </span>
            </div>
          ) : (
            <div className="div-text-bottom div-row">
              <span>
                {spendBt}
                {' '}
                <b>{generaCurrency(props.amount)}</b>
                {' '}
                {orMoreBt}
              </span>
              <span>
                <b>
                  {generaCurrency(props.amount - props.price)}
                  {' '}
                  {togoBt}
                </b>
              </span>
            </div>
          )
        }
      </div>
    </div>
  );
}

export default FreeGiftHeader;
