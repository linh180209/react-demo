import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getAltImage } from '../../../Redux/Helpers';

class Image extends Component {
  render() {
    const {
      url, name, id, ratio, isAnimation,
    } = this.props;
    return (
      <div id={id} className="content-image">
        <div className={ratio === '4:3' ? 'div-image-4_3' : 'cover-image-530_580'}>
          <img
            loading="lazy"
            className={isAnimation ? 'image animation' : 'image'}
            src={url}
            alt={getAltImage(url)}
          />
          <div className="cover-image" />
        </div>
        <span>
          {name}
        </span>
      </div>
    );
  }
}

Image.defaultProps = {
  id: '',
  isAnimation: false,
};

Image.propTypes = {
  url: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.string,
  ratio: PropTypes.string.isRequired,
  isAnimation: PropTypes.bool,
};

export default Image;
