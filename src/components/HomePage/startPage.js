import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

import { convertHeaderToSize, onClickLink } from '../../Redux/Helpers';
import logoInfo from '../../image/logo_white.png';
import BackGroundVideo from '../backgroundVideo';
import { isMobile, isTablet } from '../../DetectScreen';

class StartPage extends Component {
  onClickStart = (link) => {
    const { login, history } = this.props;
    onClickLink(link, login, history);
  }

  render() {
    const { heroCms } = this.props;
    if (!heroCms) {
      return (<div />);
    }
    const {
      buttons, header_size: headerSize, header_text: headerText, child_sections: childSections,
    } = heroCms;
    const styleHeader = {
      fontSize: convertHeaderToSize(headerSize),
    };
    const subHeaderItem = childSections && childSections.length > 0 ? _.find(childSections[0].value, x => x.type === 'text_block') : undefined;
    const subHeader = subHeaderItem ? subHeaderItem.value : '';
    _.assign(styleHeader, {
      color: '#000',
      textAlign: 'center',
    });
    const textButton = buttons && buttons.length > 0 ? buttons[0].value : '';
    const blocks = childSections ? childSections[0].value : undefined;

    const dataVideos = _.filter(blocks, x => x.type === 'video_block');
    const valueFindVideo = isMobile ? 'phone' : (isTablet ? 'tablet' : 'web');
    const dataVideo = _.find(dataVideos, x => x.value.type === valueFindVideo);
    const videoBlock = dataVideo ? dataVideo.value : undefined;
    return (
      <div id="startPage" className="div-startPage">
        <div id="idComing" className="div-comingsoon div-col justify-center items-center">
          <BackGroundVideo url={videoBlock ? videoBlock.video_url : ''} placeholder={videoBlock ? videoBlock.placeholder : ''} />
          <div className="div-col justify-center items-center">
            <img loading="lazy" src={logoInfo} alt="logo" style={{ height: '150px' }} />
            <h1 style={{
              color: '#ffffff',
              textAlign: 'center',
              fontSize: isMobile && !isTablet ? '1.5rem' : '2.3rem',
              marginTop: '50px',
            }}

            >
              {headerText}
            </h1>
            <h2
              style={{
                fontFamily: 'PT Sans',
                fontStyle: 'italic',
                color: '#ffffff',
                textAlign: 'center',

                fontSize: isMobile && !isTablet ? '1.1rem' : '1.7rem',
              }}
              className="mb-5"
            >
              {subHeader}
            </h2>
            <button
              type="button"
              className="button_home_page pt-2 pb-2 pl-4 pr-4"
              onClick={() => this.onClickStart(textButton ? textButton.link : '')}
              style={{
                color: '#ffffff',
                borderColor: '#ffffff',
              }}
            >
              {textButton ? textButton.text : ''}
            </button>
          </div>
        </div>
      </div>
    );
  }
}

StartPage.propTypes = {
  heroCms: PropTypes.shape({
    header_size: PropTypes.string,
    header_text: PropTypes.string,
    video_url: PropTypes.string,
    buttons: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  login: PropTypes.shape({
    token: PropTypes.string,
    user: PropTypes.shape({
      outcome: PropTypes.object,
    }),
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

export default withRouter(connect(mapStateToProps, null)(StartPage));
