import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ProductItem from './productItem';
import { fetchDataImage } from '../../Redux/Helpers/fetch-client';

class ProductItemMobile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      indexActive: 0,
    };
    this.isFetchImage = false;
  }


  componentDidUpdate() {
    const { datas } = this.props;
    if (datas.length > 0 && !this.isFetchImage) {
      setTimeout(() => {
        const pending = [];
        _.forEach(datas, (d) => {
          pending.push(fetchDataImage(d.url));
        });
        Promise.all(pending);
      }, 1000);
      this.isFetchImage = true;
    }
  }

  onClickNext = (indexActive) => {
    this.setState({ indexActive });
  }

  render() {
    const { datas, gotoClick } = this.props;
    const { indexActive } = this.state;
    const data = datas.length > indexActive ? datas[indexActive] : undefined;
    return (
      <ProductItem
        url={data ? data.url : ''}
        name={data ? data.name : ''}
        description={data ? data.description : ''}
        link={data ? data.link : ''}
        price={data ? data.price : ''}
        isSilde
        indexActive={indexActive}
        onClickNext={this.onClickNext}
        total={datas.length}
        gotoClick={gotoClick}
      />
    );
  }
}

ProductItemMobile.propTypes = {
  datas: PropTypes.arrayOf(PropTypes.object).isRequired,
  gotoClick: PropTypes.func.isRequired,
};

export default ProductItemMobile;
