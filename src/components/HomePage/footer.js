import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import _ from 'lodash';
// import ReactGA from 'react-ga';
import {
  getUrlScentDesigner, onClickLink, getAltImage, fetchCMSHomepage, validateEmail, getNameFromButtonBlock, generateUrlWeb, getLink, gotoHomeAllProduct,
} from '../../Redux/Helpers';
import { SUBSCRIBER_NEWLETTER_URL, GUEST_CREATE_BASKET_URL, GET_BASKET_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import { loginUpdateUserInfo } from '../../Redux/Actions/login';
import addCmsRedux from '../../Redux/Actions/cms';
import { loadAllBasket } from '../../Redux/Actions/basket';
import auth from '../../Redux/Helpers/auth';
import '../../styles/_homepage.scss';
import logo_footer from '../../image/logo.png';
import paypal from '../../image/Paypal.png';
import master_card from '../../image/Exclude.png';
import visa from '../../image/Visa.png';
import logoCongthuong from '../../image/logo-congthuong.png';
import intagram from '../../image/icon/ic-instagram-whilte.svg';
import facebook from '../../image/icon/ic-facebook-white.svg';
import { isBrowser } from '../../DetectScreen';
import LinkCT from '../../componentsv2/linkCT';

const getCMS = (data) => {
  // console.log('cms footer', data);
  if (data) {
    const { header_text: headerText, body, image_background: imageBg } = data;
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');
    const btBlock = _.filter(body, x => x.type === 'button_block').map(ele => ele.value);
    const textBlock = _.filter(body, x => x.type === 'text_block').map(ele => ele.value);
    const ctaBlock = _.filter(body, x => x.type === 'cta_block').map(ele => ele.value);
    const labelBlock = _.filter(body, x => x.type === 'label_and_text_block').map(ele => ele.value);
    const imageBlock = _.filter(body, x => x.type === 'image_block').map(ele => ele.value);
    return {
      headerText, imageBg, btBlock, textBlock, ctaBlock, labelBlock, imageBlock, buttonBlocks,
    };
  }
  return {
    headerText: undefined, imageBg: undefined, btBlock: [], textBlock: [], ctaBlock: [], labelBlock: [], imageBlock: [], buttonBlocks: [],
  };
};

class Footer extends Component {
  state = {
    email: undefined,
    headerText: undefined,
    imageBg: undefined,
    btBlock: [],
    textBlock: [],
    ctaBlock: [],
    labelBlock: [],
    imageBlock: [],
    buttonBlocks: [],
  }

  componentDidMount = () => {
    this.fetchDataInit();
  }

  scrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  gotoB2bLogin = () => {
    const { login } = this.props;
    if (login && login.user && login.user.is_b2b) {
      this.props.history.push(generateUrlWeb('/b2b/landing'));
    } else {
      this.props.history.push(generateUrlWeb('/b2b/login'));
    }
  }

  gotoPage = (link) => {
    const { login, history } = this.props;
    onClickLink(link, login, history);
  }

  onClickFQA = () => {
    this.props.history.push(generateUrlWeb('/faq'));
  }

  onClickTermsConditions = () => {
    this.props.history.push(generateUrlWeb('/terms-conditions'));
  }

  onClickAboutUs = () => {
    this.props.history.push(generateUrlWeb('/about-us'));
  }

  onClickShippingInfomation = () => {
    this.props.history.push(generateUrlWeb('/shipping-infomation'));
  }

  onClickDesignScent = () => {
    const url = getUrlScentDesigner(this.props.login);
    this.props.history.push(generateUrlWeb(url));
  }

  onChangeEmail = (e) => {
    const { value } = e.target;
    this.setState({ email: value });
  }

  assignEmailToCart = (email) => {
    const { basket } = this.props;
    if (!basket.id) {
      const options = {
        url: GUEST_CREATE_BASKET_URL,
        method: 'POST',
        body: {
          subtotal: 0,
          total: 0,
          email,
        },
      };
      fetchClient(options).then((result) => {
        if (result && !result.isError) {
          this.props.loadAllBasket(result);
        }
      });
      return;
    }
    const option = {
      url: GET_BASKET_URL.replace('{cartPk}', basket.id),
      method: 'PUT',
      body: {
        email,
      },
    };
    fetchClient(option);
  }

  postNewLetter = (email) => {
    if (!validateEmail(email)) {
      toastrError('Email is not avaiable');
      return;
    }
    // ReactGA.event({
    //   category: 'Newsletter',
    //   action: 'Submit',
    // });

    this.assignEmailToCart(email);

    const { login } = this.props;
    const options = {
      url: SUBSCRIBER_NEWLETTER_URL,
      method: 'POST',
      body: {
        email,
        user: login && login.user ? login.user.id : undefined,
      },
    };
    fetchClient(options).then((result) => {
      // console.log('result', result);
      if (result.isError) {
        if (result.email) {
          toastrError('You already have registered');
        } else {
          toastrError('You have already subscribed to our newsletter');
        }
        return;
      }
      auth.setEmail(email);
      toastrSuccess('Email sent successfully');
      if (options.body.user) {
        this.props.loginUpdateUserInfo({ is_get_newsletters: true });
      }
    }).catch((err) => {
      toastrError(err);
    });
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const footerCms = _.find(cms, x => x.title === 'Footer');
    if (!footerCms) {
      try {
        const cmsData = await fetchCMSHomepage('footer');
        this.props.addCmsRedux(cmsData);
        const {
          headerText, imageBg, btBlock, textBlock, ctaBlock, labelBlock, imageBlock, buttonBlocks,
        } = getCMS(cmsData);
        this.setState({
          headerText, imageBg, btBlock, textBlock, ctaBlock, labelBlock, imageBlock, buttonBlocks,
        });
      } catch (error) {
        toastrError(error.message);
      }
    } else {
      const {
        headerText, imageBg, btBlock, textBlock, ctaBlock, labelBlock, imageBlock, buttonBlocks,
      } = getCMS(footerCms);
      this.setState({
        headerText, imageBg, btBlock, textBlock, ctaBlock, labelBlock, imageBlock, buttonBlocks,
      });
    }
  }

  render() {
    const { isB2b, login } = this.props;
    const { email } = this.state;
    const isShowNewLetter = !login || !login.user || !login.user.is_get_newsletters;
    const {
      headerText, imageBg, btBlock, textBlock, ctaBlock, labelBlock, imageBlock, buttonBlocks,
    } = this.state;
    const signUpBt = getNameFromButtonBlock(buttonBlocks, 'SIGN_UP');
    const yourBt = getNameFromButtonBlock(buttonBlocks, 'Your_E-mail');
    const supportBt = getNameFromButtonBlock(buttonBlocks, 'SUPPORT_HEADER');
    const shopBt = getNameFromButtonBlock(buttonBlocks, 'SHOP_SCENTS');

    const htmlWeb = (
      <div className="div-footer">
        <div className="content">
          <div className="div-left">
            {/* <div className="item">
              <img loading="lazy" src={logo_footer} />
            </div> */}
            {labelBlock.length && (
              <React.Fragment>
                <ul>
                  <li className="header_4 regular">
                    <span>{labelBlock.find(ele => ele.label === 'address1') ? labelBlock.find(ele => ele.label === 'address1').text : ''}</span>
                  </li>
                  <li className="header_4 space-line">
                    <span>{labelBlock.find(ele => ele.label === 'openHour1') ? labelBlock.find(ele => ele.label === 'openHour1').text : ''}</span>
                  </li>
                  <li className="header_4 regular space-block">
                    <span>{labelBlock.find(ele => ele.label === 'address2') ? labelBlock.find(ele => ele.label === 'address2').text : ''}</span>
                  </li>
                  <li className="header_4 space-line">
                    <span>{labelBlock.find(ele => ele.label === 'openHour2') ? labelBlock.find(ele => ele.label === 'openHour2').text : ''}</span>
                  </li>
                  <li className="header_4 regular space-block">
                    <span>{labelBlock.find(ele => ele.label === 'address3') ? labelBlock.find(ele => ele.label === 'address3').text : ''}</span>
                  </li>
                  <li className="header_4 space-line">
                    <span>{labelBlock.find(ele => ele.label === 'openHour3') ? labelBlock.find(ele => ele.label === 'openHour3').text : ''}</span>
                  </li>
                </ul>
              </React.Fragment>
            )}
            <div className={auth.getCountry() === 'vn' ? 'logo-conthuong' : 'hidden'}>
              <a href="http://online.gov.vn/Home/WebDetails/80061">
                <img loading="lazy" src={logoCongthuong} alt="logo" />
              </a>
            </div>
          </div>
          <div className="div-center">
            {btBlock.length && (
              <div className="item">
                <div>
                  <h4 className="header_4">{shopBt}</h4>
                  {btBlock.filter(ele => ele.name === 'shop').map((button, index) => (
                    <LinkCT to={getLink(button.link)} key={index} className="link-connect header_4">
                      {button.text}
                    </LinkCT>
                  ))}
                </div>
                <div className="__right">
                  <h4 className="header_4">{supportBt}</h4>
                  {btBlock.filter(ele => ele.name === 'support').map((button, index) => (
                    <LinkCT to={getLink(button.link)} key={index} className="link-connect header_4">
                      {button.text}
                    </LinkCT>
                  ))}
                </div>
              </div>
            )}
          </div>
          <div className="div-right">
            <div>
              {headerText && (
              <h2 className="header_2">{headerText}</h2>
              )}
              <h4 className="header_4">{textBlock.length ? textBlock[0] : ''}</h4>
            </div>

            <div className="input-group stylish-input-group">
              <input type="email" onChange={this.onChangeEmail} className="form-control" placeholder={yourBt} aria-label="Your Email" aria-describedby="basic-addon2" />
              <div className="input-group-append">
                <button
                  type="submit"
                  onClick={() => this.postNewLetter(email)}
                  disabled={_.isEmpty(email)}
                >
                  {signUpBt}
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="space-content">
          <hr className="line-footer" />
        </div>
        <div className="connect-icon">
          <div className="div-left-icon">
            <span className="phone-number">
              {labelBlock.find(ele => ele.label === 'phone') && (
              <a href={`tel:${labelBlock.find(ele => ele.label === 'phone').text}`}>
                {labelBlock.find(ele => ele.label === 'phone').text}
              </a>
              )}
            </span>
            <span className="email">
              {labelBlock.find(ele => ele.label === 'email') && (
                <a href={`mailto:${labelBlock.find(ele => ele.label === 'email').text}`}>
                  {labelBlock.find(ele => ele.label === 'email').text}
                </a>
              )}
            </span>
          </div>
          { !isBrowser
              && (
              <div className="div-right-icon">
                {ctaBlock && (
                <div className="item">
                  {ctaBlock.map((ele, index) => (
                    <a index={index} href={ele.link}>
                      <img loading="lazy" src={ele.image.image} alt="" />
                    </a>
                  ))}
                </div>
                )}
              </div>
              )
          }
          <div className="div-center-icon d-flex justify-content-start">
            <div className="item">
              <div>
                <img loading="lazy" src={paypal} alt="" />
              </div>
              <div>
                <img loading="lazy" src={master_card} alt="" />
              </div>
              <div>
                <img loading="lazy" src={visa} alt="" />
              </div>
            </div>
            <a href={gotoHomeAllProduct(this.props.history)} className="header_4 link-connect">
              {textBlock.length ? textBlock[1] : ''}
            </a>
          </div>
          { isBrowser
            && (
            <div className="div-right-icon">
              {ctaBlock && (
              <div className="item">
                {ctaBlock.map((ele, index) => (
                  <a index={index} href={ele.link}>
                    <img loading="lazy" src={ele.image.image} alt="" />
                  </a>
                ))}
              </div>
              )}
            </div>
            )
          }
        </div>
      </div>
    );

    return (
      <div>
        {htmlWeb}
      </div>
    );
  }
}

Footer.defaultProps = {
  isB2b: false,
};

Footer.propTypes = {
  isB2b: PropTypes.bool,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  login: PropTypes.shape({
    user: PropTypes.shape({
      is_b2b: PropTypes.bool,
    }),
  }).isRequired,
  cms: PropTypes.arrayOf(PropTypes.object).isRequired,
  loginUpdateUserInfo: PropTypes.func.isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  basket: PropTypes.shape().isRequired,
  loadAllBasket: PropTypes.func.isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
    cms: state.cms,
    basket: state.basket,
  };
}

const mapDispatchToProps = {
  loginUpdateUserInfo,
  addCmsRedux,
  loadAllBasket,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Footer));
