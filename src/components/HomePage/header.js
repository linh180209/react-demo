import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import onClickOutside from 'react-onclickoutside';
import _ from 'lodash';
import classnames from 'classnames';
// import {
//   isIOS, isMobile, isSafari, isBrowser,
// } from 'react-device-detect';
import reactHtmlParser from 'react-html-parser';
import { isIOS, isSafari } from 'react-device-detect';
import { isBrowser, isLargerScreen, isMobile } from '../../DetectScreen';
import auth from '../../Redux/Helpers/auth';

import emitter from '../../Redux/Helpers/eventEmitter';
import CardItem from '../B2B/CardItem';
import LoginRegister from '../../views/Register/loginRegister';
import iconClose from '../../image/icon_close.svg';
import {
  deleteProductBasket, updateProductBasket, addProductGiftBasket, createBasketGuest, addProductBasket,
} from '../../Redux/Actions/basket';
import updateShowAskRegion from '../../Redux/Actions/showAskRegion';
import {
  onClickLink, getCmsCommon, getNameFromCommon, generaCurrency, fetchCMSHomepage, googleAnalitycs, getNameFromButtonBlock, getAltImageV2, generateUrlWeb, isAllGift, trackGTMCheckOut, gotoShopHome,
} from '../../Redux/Helpers';
import addCmsRedux from '../../Redux/Actions/cms';
import updateShippings from '../../Redux/Actions/shippings';
import { toastrError } from '../../Redux/Helpers/notification';
import MessageMenu from './messageMenu';
import Region from '../Region';
import icPayLater from '../../image/icon/icPayLater.svg';
import icBackRight from '../../image/icon/icBackRight.svg';
import BuyMoreDiscounts from '../BuyMoreDiscounts';
import FreeGiftHeader from './freeGiftHeader';
import GiftFree from './giftFree';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { GET_GIFT_FREE, GET_SHIPPING_URL } from '../../config';
import Menuheader from './menuHeader';
import UpSell from './upSell';
import RegionAsk from '../RegionAsk';
import ReceiveGrams from '../receiveGrams';
import PromoCode from './PromoCode';
import PopupAddToCart from './PopupAddToCart';

const getCmsMenu = (cms) => {
  const cmsMenu = _.find(cms, x => x.title === 'Hamburger-v2');
  if (cmsMenu) {
    const menuBlock = _.filter(cmsMenu.body, x => x.type === 'menu_block');
    const mergeMenuBlock = _.filter(cmsMenu.body, x => x.type === 'new_mega_menu_block');
    const menuButtonBlock = _.filter(cmsMenu.body, x => x.type === 'button_block');
    return { menuBlock, menuButtonBlock, mergeMenuBlock };
  }
  return { menuBlock: [], menuButtonBlock: [], mergeMenuBlock: [] };
};

const getCMSFooter = (data) => {
  if (data) {
    const { header_text: headerText, body, image_background: imageBg } = data;
    const btBlock = _.filter(body, x => x.type === 'button_block');
    const textBlock = _.find(body, x => x.type === 'text_block');
    const ctaBlock = _.filter(body, x => x.type === 'cta_block');
    const labelBlock = _.filter(body, x => x.type === 'label_and_text_block');
    const imageBlock = _.filter(body, x => x.type === 'image_block');
    return {
      headerText, imageBg, btBlock, textBlock, ctaBlock, labelBlock, imageBlock,
    };
  }
  return {
    headerText: undefined, imageBg: undefined, btBlock: [], textBlock: undefined, ctaBlock: [], labelBlock: [], imageBlock: [],
  };
};

const getCmsCardPage = (data) => {
  if (data) {
    const { body } = data;
    const buttonBlockCard = _.filter(body, x => x.type === 'button_block');
    const paragraphBlockCard = _.find(body, x => x.type === 'paragraph_block').value;
    const iconsBlockCard = _.find(body, x => x.type === 'icons_block' && x.value?.image_background?.caption === '').value;
    return { buttonBlockCard, paragraphBlockCard, iconsBlockCard };
  }
  return {
    buttonBlockCard: [],
    paragraphBlockCard: undefined,
    iconsBlockCard: undefined,
  };
};

class HeaderHomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowSubmenu: false,
      submenu: [],
      initCreate: true,
      isShowMenu: false,
      isShowLogin: false,
      isShowRegister: props.isShowLogin,
      isShowCard: false,
      urlRedirect: undefined,
      oldSize: -1,
      menuBlock: [],
      mergeMenuBlock: [],
      menuButtonBlock: [],
      listCountry: props.countries,
      isShowLearnMore: false,
      isShowYourGift: false,
      buttonBlockCard: [],
      paragraphBlockCard: undefined,
      iconsBlockCard: undefined,
      visible: props.visible === undefined ? true : props.visible,
      giftFrees: [],
      shippingInfo: undefined,
      isOpenAddtoCartPopup: false,
    };
    const { login } = this.props;
    this.isB2B = login && login.user && login.user.is_b2b;
    this.oldCurrentScrollPos = 0;
    this.isSentGTM = false;
  }

  componentDidMount() {
    this.fetchShipping();
    this.fetchData();
    this.fetCMSCardPage();
    this.fetchFreeGift();
    // this.fetchCountry();
    this.emitterOpenLogin = emitter.addListener('eventOpenLogin', () => {
      this.setState({ isShowLogin: true });
    });
    googleAnalitycs('/');
    window.addEventListener('resize', this.updateSize);
    window.addEventListener('scroll', this.handleScroll);
  }

  componentDidUpdate(prevProps, prevState) {
    const { isShowCard } = this.state;
    if (isShowCard && isShowCard !== prevState.isShowCard && !this.isSentGTM) {
      trackGTMCheckOut(1, this.props.basket);
      this.isSentGTM = true;
    } else if (!isShowCard) {
      this.isSentGTM = false;
    }
  }

  componentWillUnmount() {
    if (this.emitterOpenLogin) {
      this.emitterOpenLogin.remove();
    }
    window.removeEventListener('resize', this.updateSize);
    window.addEventListener('scroll', this.handleScroll);
  }

  updateSize = () => {
    if (this.oldHeight !== window.innerHeight) {
      this.oldHeight = window.innerHeight;
      this.forceUpdate();
    }
  }

  onCloseCard = () => {
    this.setState({ isShowCard: false, isShowLearnMore: false, isShowYourGift: false });
  }

  onOpenCard = () => {
    this.setState({ isShowCard: true });
  }

  onCloseMenu = () => {
    this.setState({ isShowMenu: false });
  }

  onClickMyAccount = (type) => {
    const { login } = this.props;
    if (!login || !login.token || login.user.is_b2b) {
      if (type === 'register') {
        this.onOpenRegisterPage();
      } else {
        this.onOpenLoginPage();
      }
    } else if (login && !login.user.is_b2b) {
      this.props.history.push(generateUrlWeb('/account'));
    }
  }

  onGoHome = () => {
    // this.props.history.push(generateUrlWeb('/'));
    gotoShopHome();
  }

  onOpenLoginPage = (urlRedirect = undefined) => {
    this.setState({ isShowLogin: true, urlRedirect, isShowCard: false });
  }

  onOpenRegisterPage = (urlRedirect = undefined) => {
    this.setState({ isShowRegister: true, urlRedirect, isShowCard: false });
  }

  onCloseLoginPage = () => {
    this.setState({ isShowLogin: false, isShowRegister: false });
  }

  onClickLink = (url) => {
    onClickLink(url, this.props.login, this.props.history);
    this.handleClickOutside();
  }

  onClickMenu = (data) => {
    // if (data.value.submenu.length === 0) {
    this.onClickLink(data.value.link);
    // }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { basket, countries } = nextProps;
    if (basket && basket !== prevState.basket) {
      const newSize = basket.id && basket.items ? _.filter(basket?.items || [], d => d.is_free === false && d.is_free_gift === false).length : -1;
      if (newSize > prevState.oldSize && prevState.oldSize !== -1) {
        if (!window.location.href.includes('/checkout')) {
          _.assign(objectReturn, { isOpenAddtoCartPopup: true, isShowLearnMore: false });
        }
      }
      _.assign(objectReturn, { basket, oldSize: newSize });
      // _.assign(objectReturn, { basket });
    }
    if (countries && countries.length > 0 && countries !== prevState.listCountry) {
      _.assign(objectReturn, { basket, listCountry: countries });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  gotoProduct = () => {
    onClickLink('/all-products', this.props.login, this.props.history);
    this.handleClickOutside();
  }

  fetCMSCardPage = async () => {
    const { cms } = this.props;
    if (cms) {
      const cmsHamburger = _.find(cms, x => x.title === 'Card Page');
      if (!cmsHamburger) {
        try {
          const result = await fetchCMSHomepage('card-page');
          this.props.addCmsRedux(result);
          const cardBlock = getCmsCardPage(result);
          this.setState(cardBlock);
        } catch (error) {
          toastrError(error.message);
        }
      } else {
        const cardBlock = getCmsCardPage(cmsHamburger);
        this.setState(cardBlock);
      }
    }
  }

  fetchFreeGift = () => {
    const options = {
      url: GET_GIFT_FREE,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      this.setState({ giftFrees: result });
    }).catch((err) => {
      console.log('err', err);
    });
  }

  fetchShipping = () => {
    const options = {
      url: GET_SHIPPING_URL,
      method: 'GET',
    };
    fetchClient(options).then((result) => {
      if (result) {
        this.props.updateShippings(result);
        const shippingInfo = _.find(result, d => d.country.code === auth.getCountry());
        this.setState({ shippingInfo });
      }
    }).catch((err) => {
      console.log('error', err);
    });
  }

  fetchData = async () => {
    const { cms } = this.props;
    if (cms) {
      const cmsHamburger = _.find(cms, x => x.title === 'Hamburger-v2');
      if (!cmsHamburger) {
        try {
          const result = await fetchCMSHomepage('hamburger-v2');
          this.props.addCmsRedux(result);
          const menuBlock = getCmsMenu([result]);
          this.setState(menuBlock);
        } catch (error) {
          toastrError(error.message);
        }
      } else {
        const menuBlock = getCmsMenu(cms);
        this.setState(menuBlock);
      }
    }
  }

  openMenu = () => {
    // this.props.onOpen();
    this.setState({ isShowMenu: true });
  }

  handleMenu = (e) => {
    this.setState({ isShowSubmenu: true, initCreate: false });
  }

  handleClickOutside = () => {
    this.setState({ isShowSubmenu: false });
  }

  gotoCheckOut = () => {
    this.props.history.push(generateUrlWeb('/checkout'));
    // const { login } = this.props;
    // if (login && login.user && login.user.id) {
    //   this.props.history.push('/checkout');
    // } else {
    //   this.onOpenLoginPage('/checkout');
    // }
  }

  toggleHover = (data) => {
    if (data.value.submenu.length > 0) {
      this.setState({ submenu: data.value.submenu, isShowSubmenu: true, initCreate: false });
    }
  }

  toggleLeave = () => {
    this.handleClickOutside();
  }

  openLearMore = () => {
    this.setState({ isShowLearnMore: true });
  }

  closeLearMore = () => {
    this.setState({ isShowLearnMore: false });
  }

  handleScroll = () => {
    const { isDisableScrollShow } = this.props;
    if (isDisableScrollShow) {
      return;
    }
    const currentScrollPos = window.pageYOffset;
    const isScrollUp = this.oldCurrentScrollPos > currentScrollPos;
    this.oldCurrentScrollPos = currentScrollPos;
    const visible = currentScrollPos < (isMobile ? 87 : isLargerScreen ? 227 : 170);
    const objectState = {};
    if (visible !== this.state.visible) {
      _.assign(objectState, { visible });
    }
    if ((isScrollUp && isMobile) !== this.state.isScrollUp) {
      _.assign(objectState, { isScrollUp: isScrollUp && isMobile });
    }
    if (!_.isEmpty(objectState)) {
      this.setState(objectState);
    }
    // this.setState({
    //   visible,
    //   isScrollUp: isScrollUp && isMobile,
    // });
  };

  onClickOutCart = () => {
    this.setState({ isShowCard: false, isShowLearnMore: false });
  }

  isShowGiftBox = items => (
    !_.find(items, x => x.item.product.type.name === 'discovery_box')
  )

  gotoTermConditions = () => {
    if (auth.getCountry() === 'vn') {
      this.props.history.push(generateUrlWeb('/shipping-infomation'));
    } else {
      this.props.history.push(generateUrlWeb('/terms-conditions'));
    }
  }

  closeAddtoCartPopup = () => {
    this.setState({ isOpenAddtoCartPopup: false });
  }

  viewShoppingBag = () => {
    this.setState({ isOpenAddtoCartPopup: false, isShowCard: true });
  }

  render() {
    const {
      urlRedirect, menuBlock, listCountry, isShowLearnMore, buttonBlockCard, paragraphBlockCard, iconsBlockCard, isShowYourGift,
      giftFrees, shippingInfo, menuButtonBlock, mergeMenuBlock,
    } = this.state;
    const {
      login, basket, cms, isSpecialMenu, isShowBlackSlider, showAskRegion,
    } = this.props;
    if (!basket) {
      return null;
    }
    const basketNotFree = _.filter(basket?.items || [], d => d.is_free === false);
    const isScrolled = !this.state.visible && !this.props.isDisableScrollShow && !this.state.isScrollUp;

    const path = this.props.location.pathname.slice(1);
    const footerCms = _.find(cms, x => x.title === 'Footer');
    const cmsCommon = getCmsCommon(cms);
    const yourBagsBt = getNameFromCommon(cmsCommon, 'YOUR_BAG_IS_CURRENTLY_EMPTY');
    const continueShopBt = getNameFromCommon(cmsCommon, 'CONTINUE_SHOPPING');
    const totalBt = getNameFromCommon(cmsCommon, 'TOTAL');
    const checkOutBt = getNameFromCommon(cmsCommon, 'CHECKOUT');
    const continueBt = getNameFromCommon(cmsCommon, 'CONTINUE_SHOPPING');
    const myBagBt = getNameFromCommon(cmsCommon, 'MY_BAG');
    const getNowPayLaterBt = getNameFromButtonBlock(buttonBlockCard, 'get_now_pay_later');
    const newGetNowBt = getNameFromButtonBlock(buttonBlockCard, 'NEW_Get_now_pay_later');
    const learnMoreBt = getNameFromButtonBlock(buttonBlockCard, 'Learn_more');
    const getNowPayLater = getNameFromButtonBlock(buttonBlockCard, 'Get_Now_Pay_Later');
    const shippingAndTextBt = getNameFromButtonBlock(buttonBlockCard, 'Shipping_and_taxes_calculated_at_checkout');
    const freeDeliveryBt = getNameFromButtonBlock(buttonBlockCard, 'FREE_DELIVERY');
    const freeDeliveryOverBt = getNameFromButtonBlock(buttonBlockCard, 'FREE_DELIVERY_OVER');
    const {
      ctaBlock, labelBlock,
    } = getCMSFooter(footerCms);
    const nameDisplay = login && login.user && !login.user.is_b2b ? `${!_.isEmpty(login.user.first_name) ? login.user.first_name : ''} ${!_.isEmpty(login.user.last_name) ? login.user.last_name : ''}` : '';
    const totalBasket = !this.isB2B ? (basket && basket.items ? _.reduce(basket.items, (sum, x) => sum + x.quantity, 0) : 0) : 0;
    const {
      isShowSubmenu, initCreate, isShowMenu, isShowLogin, isShowRegister, isShowCard,
      isOpenCustomMobileiOS,
    } = this.state;

    const {
      items, id, discount, subtotal, store_discount: storeDiscount, amt_freeship: amtFreeship,
    } = this.state.basket;
    const totalPerfumeExlixir = _.filter(items, x => (x.item.product.type.name === 'Perfume' || x.item.product.type.name === 'Elixir') && !x.item.is_sample);

    const newPrice = parseFloat(discount) > 0 ? parseFloat(subtotal) - parseFloat(discount) : -1;
    const finalPrice = newPrice === -1 ? parseFloat(subtotal) : newPrice;
    const giftFreeProduct = _.filter(items, x => x.is_free_gift);
    const isOnlyGift = isAllGift(items);
    const menuLeft = menuBlock.slice(0, 4);
    const menuRight = menuBlock.slice(4, 8);

    const shippingValue = shippingInfo && parseFloat(shippingInfo.threshold) > 0 && (parseFloat(shippingInfo.threshold) < parseFloat(finalPrice)) ? 0 : shippingInfo?.shipping;

    const htmlCartEmpty = (
      <div className="div-empty div-col justify-center items-center w-100 h-100">
        <span>
          {yourBagsBt}
        </span>
        <button
          onClick={this.gotoProduct}
          type="button"
          className="button-bg__none pa0"
          style={{ borderBottom: '1px solid #000000' }}
        >
          {continueShopBt}
        </button>
      </div>
    );
    const htmlBag = (
      <React.Fragment>
        <ul>
          {
            _.map(items, (d, index) => (
              <li className="cd-cart__product" key={index}>
                <CardItem
                  updateProductBasket={this.props.updateProductBasket}
                  deleteProductBasket={this.props.deleteProductBasket}
                  data={d}
                  idCart={id}
                  isB2C
                  onCloseCard={this.onCloseCard}
                  cms={this.props.cms}
                  listCountry={listCountry}
                  onOpenCustomBottle={() => {
                    if (isMobile && isIOS || isSafari) {
                      this.setState({ isOpenCustomMobileiOS: true });
                    }
                  }}
                  onCloseCustomBottle={() => {
                    if (isMobile && isIOS || isSafari) {
                      this.setState({ isOpenCustomMobileiOS: false });
                    }
                  }}
                />
              </li>
            ))
          }
        </ul>
        {
          !isOnlyGift && auth.getCountry() === 'sg'
          && this.isShowGiftBox(items) && (
          <UpSell
            createBasketGuest={this.props.createBasketGuest}
            addProductBasket={this.props.addProductBasket}
            login={this.props.login}
            basket={this.props.basket}
            buttonBlocks={buttonBlockCard}
          />
          )
        }

        <PromoCode />

        <div className="div-total">
          <div className="div-row justify-between items-center">
            <span className="title-total">
              <b>
                {totalBt}
              </b>
            </span>
            {
              newPrice === -1
                ? (
                  <span className="price">
                    <b>
                      {generaCurrency(subtotal)}
                    </b>
                  </span>
                ) : (
                  <span>
                    <span className="old-price">
                      <b>
                        {generaCurrency(subtotal)}
                      </b>
                    </span>
                    <span className="ml-2 price">
                      <b>
                        {generaCurrency(newPrice)}
                      </b>
                    </span>
                  </span>
                )
            }
          </div>
          <ReceiveGrams
            shippingValue={shippingValue}
            countryName={shippingInfo?.country?.name}
            buttonBlocks={buttonBlockCard}
            price={newPrice === -1 ? subtotal : newPrice}
          />
        </div>
        {
          isMobile && !isOnlyGift && giftFrees && giftFrees.length > 0 && (
            <div className="gift-free-mobile">
              <GiftFree
                amount={giftFrees && (giftFrees.length > 0) ? giftFrees[0].amount : 0}
                giftFrees={giftFrees}
                price={finalPrice}
                addProductGiftBasket={this.props.addProductGiftBasket}
                basket={this.props.basket}
                giftFreeProduct={giftFreeProduct}
                buttonBlocks={buttonBlockCard}
                isCollapse={isMobile}
                isShow={isShowCard}
              />
            </div>
          )
        }
        {
          isBrowser && !isOnlyGift && giftFreeProduct.length !== giftFrees.length && giftFrees && giftFrees.length > 0 && (
            <FreeGiftHeader
              amount={giftFrees && (giftFrees.length > 0) ? giftFrees[0].amount : 0}
              price={finalPrice}
              onClickOpen={() => this.setState({ isShowYourGift: true })}
              buttonBlocks={buttonBlockCard}
              numberGift={`${giftFreeProduct.length}/${giftFrees.length}`}
            />
          )
        }

        <div className="get-now-pay-later">
          <button type="button" className="button-bg__none" onClick={this.gotoTermConditions}>
            {getNowPayLater}
            <img loading="lazy" src={icPayLater} alt="pay-later" />
          </button>
        </div>

        <div className="line-button">
          {
            isBrowser && (
              <div className="div-bt-continue">
                <button
                  onClick={this.onCloseCard}
                  type="button"
                >
                  {continueBt}
                </button>
              </div>
            )
          }

          <div className="div-bt-checkout">
            <button
              onClick={this.gotoCheckOut}
              type="button"
            >
              {checkOutBt}
            </button>
          </div>
        </div>

        <div className="div-learn-more">
          <span>
            {newGetNowBt}
          </span>
          <button type="button" onClick={this.openLearMore}>
            {learnMoreBt}
          </button>
        </div>
        <div style={{ height: isIOS ? '40px' : '0px' }} />
      </React.Fragment>
    );

    const htmlYourGift = (
      <div className={`cd-card__layout div-your-gift animated faster ${isShowYourGift ? 'fadeInRight' : 'fadeOutRight'} ${isScrolled && !isMobile ? isShowBlackSlider ? 'top-scroll' : 'top-scroll-small' : ''}`}>
        <GiftFree
          amount={giftFrees && (giftFrees.length > 0) ? giftFrees[0].amount : 0}
          onClickClose={() => this.setState({ isShowYourGift: false })}
          giftFrees={giftFrees}
          price={finalPrice}
          addProductGiftBasket={this.props.addProductGiftBasket}
          basket={this.props.basket}
          giftFreeProduct={giftFreeProduct}
          buttonBlocks={buttonBlockCard}
          isCollapse={isMobile}
          isShow={isShowCard}
        />
      </div>
    );

    const htlmLearnMore = (
      <div className={`cd-card__layout div-more animated faster ${isShowLearnMore ? 'fadeInRight' : isMobile ? 'fadeOutLeft' : 'fadeOutRight'} ${isBrowser ? (isShowYourGift ? 'show-gift' : 'not-gift') : ''} ${isScrolled && !isMobile ? isShowBlackSlider ? 'top-scroll' : 'top-scroll-small' : ''}`}>
        {
            isShowCard && (
              <React.Fragment>
                <div className="div-back-button">
                  <button type="button" className="button-bg__none" onClick={this.closeLearMore}>
                    <img loading="lazy" src={icBackRight} alt="back" />
                  </button>
                </div>
                <div className="div-scroll">
                  <div className="header-h3">
                    {getNowPayLaterBt}
                  </div>
                  <div className="div-list-icon">
                    {
                _.map(iconsBlockCard ? iconsBlockCard.icons : [], (x, index) => (
                  <div className="item">
                    <img loading="lazy" src={x.value.image.image} alt={getAltImageV2(x.value.image)} />
                    <span>
                      {`${index + 1}. ${x.value.text}`}
                    </span>
                  </div>
                ))
              }
                  </div>
                  <div className="div-des">
                    {paragraphBlockCard ? reactHtmlParser(paragraphBlockCard) : ''}
                  </div>
                </div>
              </React.Fragment>
            )
          }
      </div>
    );
    let marginTopCT = 0;
    if (isScrolled) {
      if (isMobile) {
        if (isShowBlackSlider) {
          marginTopCT = -87;
        } else {
          marginTopCT = -47;
        }
      } else if (isShowBlackSlider) {
        if (isLargerScreen) {
          marginTopCT = -182;
        } else {
          marginTopCT = -150;
        }
      } else if (isLargerScreen) {
        marginTopCT = -142;
      } else {
        marginTopCT = -110;
      }
    }
    if (showAskRegion && !this.props.isNotShowRegion) {
      if (isMobile) {
        marginTopCT += 120;
      } else if (isLargerScreen) {
        marginTopCT += 103;
      } else {
        marginTopCT += 80;
      }
    }

    return (
      <React.Fragment>

        <RegionAsk
          buttonBlocks={buttonBlockCard}
          countries={this.props.countries}
          updateShowAskRegion={this.props.updateShowAskRegion}
          showAskRegion={showAskRegion}
          isNotShowRegion={this.props.isNotShowRegion}
        />

        <PopupAddToCart
          buttonBlocks={buttonBlockCard}
          data={basketNotFree.length > 0 ? basketNotFree[0] : {}}
          cms={this.props.cms}
          isOpen={this.state.isOpenAddtoCartPopup}
          onClose={this.closeAddtoCartPopup}
          viewShoppingBag={this.viewShoppingBag}
        />

        <nav
          className="div-headerHome"
          id={path}
          onMouseLeave={this.toggleLeave}
          style={
          {
            transition: '0.6s',
            marginTop: `${marginTopCT}px`,
            // marginTop: isScrolled ? (isMobile ? (isShowBlackSlider ? '-87px' : '-47px') : isShowBlackSlider ? (isLargerScreen ? '-182px' : '-150px') : (isLargerScreen ? '-142px' : '-110px')) : '0',
          }
        }
        >
          {isShowBlackSlider && <MessageMenu />}
          {
          isShowBlackSlider ? (
            <div className={classnames(isMobile ? 'hidden' : 'div-region-web')}>
              <Region countries={this.props.countries} buttonBlocks={buttonBlockCard} history={this.props.history} />
            </div>
          ) : (null)
        }
          <LoginRegister onClose={this.onCloseLoginPage} showRegister={isShowRegister} urlRedirect={urlRedirect} isOpen={isShowLogin || isShowRegister} />
          <Menuheader
            visible={this.state.visible}
            openMenu={this.openMenu}
            menuLeft={menuLeft}
            menuRight={menuRight}
            onClickLink={this.onClickLink}
            onClickMenu={this.onClickMenu}
            menuBlock={menuBlock}
            menuButtonBlock={menuButtonBlock}
            onClickMyAccount={this.onClickMyAccount}
            onOpenCard={this.onOpenCard}
            nameDisplay={nameDisplay}
            login={login}
            totalBasket={totalBasket}
            mergeMenuBlock={mergeMenuBlock}
            className={`${isSpecialMenu ? 'small-height' : ''} ${isShowBlackSlider ? '' : 'position-top'}`}
            isSpecialMenu={isSpecialMenu}
            isShowLoginDrawer={isScrolled && !isMobile}
            countries={this.props.countries}
            buttonBlocks={buttonBlockCard}
            history={this.props.history}
            isHiddenAccount={this.props.isHiddenAccount}
          />


          {/* <Menu onClose={this.onCloseMenu} cms={this.props.cms} isShowMenu={isShowMenu} visibleMessage={this.state.visible} buttonBlocks={buttonBlockCard} /> */}

          <div className={`cd-cart ${isShowCard ? 'cd-cart--open' : ''}`}>
            <div className="div-bg-click" onClick={this.onClickOutCart} />

            {isBrowser && htlmLearnMore}
            {isBrowser && htmlYourGift}
            <div
              id="id-cd-card__layout"
              className={`cd-card__layout ${isOpenCustomMobileiOS ? 'full' : ''} ${isShowYourGift ? 'show-gift' : 'not-gift'} ${totalPerfumeExlixir.length ? 'show-discount' : ''} ${isScrolled && !isMobile ? isShowBlackSlider ? 'top-scroll' : 'top-scroll-small' : ''}`}
              style={!isBrowser ? { right: '0px', height: window.innerHeight, marginTop: `${marginTopCT * -1}px` } : {}}
            >
              {
              !isOpenCustomMobileiOS ? (
                <React.Fragment>
                  <header className="cd-cart__header">
                    <div className="div-title">
                      <div className="header-h2">
                        {myBagBt}
                      </div>
                      <span>
                        (
                        {items ? items.length : 0}
                        )
                      </span>
                    </div>
                    <button
                      type="button"
                      className="button-bg__none"
                      onClick={this.onCloseCard}
                    >
                      <img loading="lazy" style={{ width: '16px', marginRight: '20px' }} src={iconClose} alt="iconClose" />
                    </button>
                  </header>
                </React.Fragment>

              ) : <div />
            }

              <div className={_.isEmpty(items) ? 'cd-cart__body div-col justify-center items-center' : 'cd-cart__body'}>
                {/* <div className={totalPerfumeExlixir.length ? 'div-buy-more' : 'hidden'}>
                  <BuyMoreDiscounts
                    totalPerfumeExlixir={_.reduce(totalPerfumeExlixir, (sum, x) => sum + x.quantity, 0)}
                    buttonBlocks={buttonBlockCard}
                    isShowBottle
                  />
                </div> */}
                <div className="div-free-delivery">
                  {
                  shippingInfo && parseFloat(shippingInfo.threshold) > 0 ? (
                    `${freeDeliveryOverBt.replace('{price}', generaCurrency(shippingInfo.threshold, true))}`
                  ) : (
                    freeDeliveryBt
                  )
                }
                </div>
                {isShowCard ? (_.isEmpty(items) ? htmlCartEmpty : htmlBag) : null}
              </div>
            </div>
            {isMobile && isShowLearnMore && htlmLearnMore}
          </div>
        </nav>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    login: state.login,
    basket: state.basket,
    cms: state.cms,
    countries: state.countries,
    showAskRegion: state.showAskRegion,
  };
}

const mapDispatchToProps = {
  deleteProductBasket,
  updateProductBasket,
  addCmsRedux,
  addProductGiftBasket,
  addProductBasket,
  createBasketGuest,
  updateShowAskRegion,
  updateShippings,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(onClickOutside(HeaderHomePage)));
