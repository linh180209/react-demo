import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';
import { Button } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { toastrError, toastrSuccess } from '../../Redux/Helpers/notification';
import { generateUrlWeb, gotoShopHome, validateEmail } from '../../Redux/Helpers';
import { SIGN_UP_URL, LOGIN_FACEBOOK_URL, LOGIN_GOOGLE_URL } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { loginCompleted } from '../../Redux/Actions/login';

class Register extends Component {
  constructor(props) {
    super(props);
    this.data = {
      name: '',
      email: '',
      password: '',
    };
  }

  onSubmit = (e) => {
    e.preventDefault();
    if (this.isValid(this.data)) {
      this.signUp(this.data);
    }
  }

  onChange = (e) => {
    const { value, name } = e.target;
    this.data[name] = value;
  };

  onFailure = (error) => {
    console.log('error', error);
  }

  signUp = (data) => {
    const { name, email, password } = data;
    const options = {
      url: SIGN_UP_URL,
      method: 'POST',
      body: {
        first_name: name,
        email,
        password,
      },
    };
    fetchClient(options).then((result) => {
      if (!result.isError) {
        toastrSuccess('You are registered successfully');
        const { history } = this.props;
        // history.push(generateUrlWeb('/'));
        gotoShopHome();
        return;
      }
      throw (new Error(result.message));
    }).catch((e) => {
      toastrError(e.message);
    });
  }

  isValid = (data) => {
    const { name, email, password } = data;
    if (!name || !email || !password) {
      toastrError('Please fill all information!');
      return false;
    }
    if (!validateEmail(email)) {
      toastrError('Please enter a valid email address');
      return false;
    }
    if (password.length < 8) {
      toastrError('This password is too short. It must contain at least 8 characters');
      return false;
    }
    return true;
  }

  responseFacebook = (response) => {
    const { accessToken } = response;
    if (accessToken) {
      this.loginSocial(accessToken, LOGIN_FACEBOOK_URL);
    }
  }

  responseGoogle = (response) => {
    const { accessToken } = response;
    if (accessToken) {
      this.loginSocial(accessToken, LOGIN_GOOGLE_URL);
    }
  }

  loginSocial = (accessToken, url) => {
    const options = {
      url,
      method: 'POST',
      body: {
        access_token: accessToken,
      },
    };
    fetchClient(options).then((result) => {
      if (!result.isError) {
        this.props.loginCompleted(result);
        toastrSuccess('You are login successfully');
        const { history } = this.props;
        // history.push(generateUrlWeb('/'));
        gotoShopHome();
        return;
      }
      throw new Error(result.message);
    }).catch((e) => {
      toastrError(e.message);
    });
  }

  render() {
    return (
      <div className="div-register">
        <form onSubmit={this.onSubmit}>
          <div>
            <span className="title">
              Register
            </span>
            <span className="title_input">
              NAME
            </span>
            <input
              type="text"
              name="name"
              onChange={this.onChange}
            />
            <span className="title_input">
              EMAIL
            </span>
            <input
              type="text"
              name="email"
              onChange={this.onChange}
            />
            <span className="title_input">
              PASSWORD
            </span>
            <input
              type="text"
              name="password"
              onChange={this.onChange}
            />
            <div className="div-line">
              <div />
              <span>
                OR
              </span>
              <div />
            </div>
            <div className="div-button">
              <FacebookLogin
                appId="276035609956267"
                autoLoad={false}
                fields="name,email,picture"
                cssClass="btn_facebook btn"
                callback={this.responseFacebook}
              />
              <GoogleLogin
                clientId="1051429470092-1ktl08tfm23q3b7i2au9ivv6pnil51g1.apps.googleusercontent.com"
                onSuccess={this.responseGoogle}
                onFailure={this.onFailure}
                render={renderProps => (
                  <Button className="btn_google" onClick={renderProps.onClick}>
                    Login with Google
                  </Button>
                )}
              />
            </div>
            <div className="div-bt_register">
              <Button
                type="submit"
                color="success"
                onClick={this.onSubmit}
              >
                REGISTER
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

Register.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  loginCompleted: PropTypes.func.isRequired,
};

const mapDispatchToProps = {
  loginCompleted,
};
export default withRouter(connect(null, mapDispatchToProps)(Register));
