import React, { useEffect } from 'react';
import classnames from 'classnames';
import { isSafari } from 'react-device-detect';
import BlockMenu1 from './blockMenu1';
import BlockMenu2 from './blockMenu2';
import BlockMenu3 from './blockMenu3';
import BlockMenu4M from './blockMenu4M';
import { isBrowser, isMobile } from '../../DetectScreen';

function DrawerMenuTop(props) {
  useEffect(() => {
    if (props.isOpen && isMobile) {
      document.body.style.overflow = 'hidden';
    }
    return (() => {
      document.body.style.overflow = '';
    });
  }, [props.isOpen]);
  return (
    <div id={`div-sub-menu-${props.index}`} className={classnames('div-sub-menu', (props.isOpen ? 'open-menu' : ''))}>
      {
        props.dataMenu && props.dataMenu.value.link === 'block-create' && (
          <BlockMenu1 data={props.dataMenu.value} id="block-create" />
        )
      }
      {
        props.dataMenu && props.dataMenu.value.link === 'block-product' && (
          <BlockMenu2 data={props.dataMenu.value} id="block-product" />
        )
      }
      {
        props.dataMenu && props.dataMenu.value.link === 'block-workshops' && (
          <BlockMenu1 data={props.dataMenu.value} classNameMobile="mobile-workshops" classNameItemMobile="mobile-workshops-item" id="block-workshops" />
        )
      }
      {
        props.dataMenu && props.dataMenu.value.link === 'block-gifting' && (
          <BlockMenu1 data={props.dataMenu.value} id="block-gifting" />
        )
      }
      {
        props.dataMenu && props.dataMenu.value.link === 'block-ambassadors' && (
          <BlockMenu1 data={props.dataMenu.value} classNameMobile="mobile-workshops" classNameItemMobile="mobile-workshops-item" id="block-ambassadors" />
        )
      }
      {
        props.dataMenu && props.dataMenu.value.link === 'block-club21g' && (
          <BlockMenu1 data={props.dataMenu.value} classNameMobile="mobile-club21g" classNameList="list-club21g" classNameItem="club21g-item" isClub21g id="block-club21g" />
        )
      }
      {
        props.dataMenu && props.dataMenu.value.link === 'block-about' && (
          <BlockMenu1 data={props.dataMenu.value} classWrap="wrap-mobile-about" classNameList="list-about" classNameRight="w-100" isAbout id="block-about" />
        )
      }
      {
        props.dataMenu && props.dataMenu.value.link === 'block-blog' && (
          <BlockMenu3 data={props.dataMenu.value} id="block-blog" />
        )
      }
      {/* <BlockMenu2 /> */}
      {/* <BlockMenu3 /> */}
      {/* <BlockMenu4M /> */}
    </div>
  );
}

export default DrawerMenuTop;
