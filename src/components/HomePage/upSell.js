import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { GET_GIFT_WRAP } from '../../config';
import { generaCurrency, getNameFromButtonBlock } from '../../Redux/Helpers';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';

function UpSell(props) {
  const [dataWrap, setDataWrap] = useState(undefined);

  const onClickAddToCart = () => {
    const {
      basket, createBasketGuest, addProductBasket,
    } = props;
    const { id, name, price } = dataWrap;
    const data = {
      item: id,
      name,
      price,
      quantity: 1,
      is_featured: dataWrap.is_featured,
    };
    const dataTemp = {
      idCart: basket.id,
      item: data,
    };
    if (!props.basket.id) {
      createBasketGuest(dataTemp);
    } else {
      addProductBasket(dataTemp);
    }
  };

  const getGiftWrap = () => {
    const option = {
      url: GET_GIFT_WRAP,
      method: 'GET',
    };
    fetchClient(option).then((response) => {
      if (response && !response.isError) {
        const data = _.find(response, d => d.variant_values.length > 0 && d.variant_values[0] === 'Christmas');
        setDataWrap(data);
      } else {
        throw new Error(response.message);
      }
    }).catch((error) => {
      toastrError(error);
    });
  };

  useEffect(() => {
    getGiftWrap();
  }, []);

  const addToCartBt = getNameFromButtonBlock(props.buttonBlocks, 'ADD_TO_BAG');
  const addALimitedBt = getNameFromButtonBlock(props.buttonBlocks, 'Add_a_limited_gift_box_for');
  return (
    <div className="div-up-sell">
      <img loading="lazy" src={dataWrap ? dataWrap.image : ''} alt="wrap" />
      <div className="div-right">
        <div className="text-name">
          <h3>
            {dataWrap ? dataWrap.name : ''}
          </h3>
          <span>
            {dataWrap ? `${addALimitedBt} ${generaCurrency(dataWrap.price)}` : ''}
          </span>
        </div>
        <button type="button" onClick={onClickAddToCart}>
          {addToCartBt}
        </button>
      </div>

    </div>
  );
}

export default UpSell;
