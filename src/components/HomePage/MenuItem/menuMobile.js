import React, { Component } from 'react';
import PropTypes from 'prop-types';
import onClickOutside from 'react-onclickoutside';
import ListItem from './listItem';
import Region from '../../Region';

class MenuMobile extends Component {
  handleClickOutside = () => {
    if (this.props.isShowMenu) {
      this.props.onClose();
    }
  }

  render() {
    const {
      displayName, menu, isShowMenu, visibleMessage,
    } = this.props;
    return (
      <nav
        className="div-menu-mobile div-col items-center"
        style={{
          height: '100vh',
        }}
      >
        <div
          className="div-col w-100 h-100"
          style={{ transition: '0.6s', marginTop: visibleMessage ? '40px' : '0px' }}
        >
          <div className="header div-col justify-center">
            {displayName}
          </div>
          <div>
            <React.Fragment>
              <ListItem menu={menu} />
            </React.Fragment>
          </div>
          <div className="div-col w-100 justify-center items-center mt-5">
            <Region className="region-mobile" handleClickOutside={this.handleClickOutside} countries={this.props.countries} buttonBlocks={this.props.buttonBlocks} history={this.props.history} />
            <div style={{ height: '100px', width: '100%' }} />
          </div>
        </div>
      </nav>
    );
  }
}

MenuMobile.propTypes = {
  onClose: PropTypes.string.isRequired,
  displayName: PropTypes.string.isRequired,
  menu: PropTypes.arrayOf().isRequired,
  isShowMenu: PropTypes.string.isRequired,
};

export default onClickOutside(MenuMobile);
