import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

import upIcon from '../../../image/icon/arrow-up-filter.svg';
import downIcon from '../../../image/icon/arrow-down-filter.svg';

class ItemMobilemenu extends Component {
  state = {
    isOpen: false,
  }

  toggle = () => {
    const { isOpen } = this.state;
    const { data } = this.props;
    this.setState({ isOpen: !isOpen });
    const { onClick } = this.props;
    if (onClick) {
      this.props.onClick(data);
    }
  }

  render() {
    const { isOpen } = this.state;
    const { data } = this.props;
    const { name, subMenu } = data;
    return (
      <div className="div-col div-itemsubmenu">
        <div
          className="item-header div-row items-center justify-between"
          onClick={this.toggle}
        >
          {name.includes('...') ? name.replace('...', '') : name}
          {
            !_.isEmpty(subMenu)
              ? (<img loading="lazy" style={{ width: '12px' }} src={isOpen ? upIcon : downIcon} alt="icon" />)
              : (<div />)
          }

        </div>
        {
          !_.isEmpty(subMenu)
            ? (
              <div className={isOpen ? 'items-transition-open' : 'items-transition-close'} style={isOpen ? { height: `${subMenu.length * 53}px`, visibility: 'visible' } : { height: '0px', visibility: 'hidden' }}>
                {
                  _.map(subMenu, d => (
                    <div
                      onClick={() => d.onClickLink(d)}
                      className="item-content div-col justify-center"
                    >
                      {d.name}
                    </div>
                  ))
                }
              </div>
            ) : (<div />)
        }

      </div>
    );
  }
}
ItemMobilemenu.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    subMenu: PropTypes.arrayOf(PropTypes.object),
    url: PropTypes.string,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ItemMobilemenu;
