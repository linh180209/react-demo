import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import userIcon from '../../../image/icon/ic-user-black.svg';
import { logoutRequest } from '../../../Redux/Actions/login';

import './styles.scss';
import {
  trackGTMLogout, getNameFromButtonBlock, generateUrlWeb, gotoShopHome,
} from '../../../Redux/Helpers';

class LoginMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
    };
  }

  showModal = () => {
    this.setState({
      showModal: true,
    });
  }

  hideModal = () => {
    this.setState({
      showModal: false,
    });
  }

  clickLogout = () => {
    // track GTM
    trackGTMLogout(this.props.userId);
    this.props.logoutRequest();
    // this.props.history.push(generateUrlWeb('/'));
    gotoShopHome();
  }

  render() {
    const { onClickLogin, nameDisplay, buttonBlocks } = this.props;
    const { showModal } = this.state;
    const isBrowser = window.innerWidth > 1024;

    const loginBt = getNameFromButtonBlock(buttonBlocks, 'Login');
    const registerBt = getNameFromButtonBlock(buttonBlocks, 'Register');
    const hiBt = getNameFromButtonBlock(buttonBlocks, 'Hi');
    const logoutBt = getNameFromButtonBlock(buttonBlocks, 'Log_Out');
    const myAccountbt = getNameFromButtonBlock(buttonBlocks, 'My_Account');
    const accountDetailBt = getNameFromButtonBlock(buttonBlocks, 'Account_Details');
    return (
      <div
        className="login-menu d-flex flex-column"
        onMouseLeave={this.hideModal}
      >
        <button
          type="button"
          className="bt-menu"
          onClick={onClickLogin}
          // onMouseEnter={this.showModal}
          // onClick={onClickLogin}
        >
          {/* {isBrowser && nameDisplay ? nameDisplay : (<img src={userIcon} alt="userIcon" />)} */}
          <img loading="lazy" src={userIcon} style={{ width: '26px' }} className="img-fluid" alt="userIcon" />
        </button>
        {/* {isBrowser && (
          <div className="login-modal">
            <div className={`main-modal ${showModal ? 'fadeIn' : 'fadeOut'}`}>
              <div className="overflow-hidden">
                <div className="__header">
                  {isBrowser && !nameDisplay && (
                    <div className="d-flex justify-content-center mr-5">
                      <u onClick={() => { onClickLogin('login'); }}>
                        {loginBt}
                      </u>
                      <span className="mx-3">
                        |
                      </span>
                      <u onClick={() => { onClickLogin('register'); }}>
                        {registerBt}
                      </u>
                    </div>
                  )}
                  {isBrowser && nameDisplay && (
                    <div className="d-flex justify-content-between">
                      <b>
                        {hiBt}
                        {' '}
                        {nameDisplay}
                      </b>
                      <u onClick={this.clickLogout}>
                        {logoutBt}
                      </u>
                    </div>
                  )}
                </div>
                <div className="__content">
                  <ul>
                    <li onClick={onClickLogin}>
                      <i className="fa fa-user pr-3" />
                      {myAccountbt}
                    </li>
                    <li onClick={onClickLogin}>
                      <i className="fa fa-cog pr-3" />
                      {accountDetailBt}
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        )} */}
      </div>
    );
  }
}


function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

const mapDispatchToProps = {
  logoutRequest,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginMenu));
