import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { isMobile } from '../../../DetectScreen';

class MenuItemMobile extends Component {
  onClick = (data) => {
    const { onClick } = this.props;
    if (onClick) {
      onClick(data);
    }
  }

  render() {
    const { data, isActive } = this.props;
    const { name, subMenu } = data;
    const marginBt = isMobile ? 'mb-3' : 'mb-4';
    return (
      <div className={`menuItem-mobile div-col justify-center items-center ${marginBt}`}>
        <button onClick={() => this.onClick(data)} type="button" className="button_homepage menu color-white">
          {name}
        </button>
        {
          subMenu && subMenu.length > 0 ? (
            <div className={isActive ? 'details-menu_white details-menu_white-active mt-2' : 'details-menu_white mt-2'} />
          ) : (<div />)
        }
        {
          isActive ? (
            _.map(subMenu, d => (
              <button onClick={() => d.onClickLink(d)} type="button" className="button_homepage menu color-white mt-3">
                {d.name}
              </button>
            ))
          ) : (<div />)
        }
        {
          isActive && subMenu.length > 0
            ? (
              <div className="mt-3" style={{ width: '100px', height: '1px', background: '#ffffff' }} />
            ) : (<div />)
        }
      </div>
    );
  }
}

MenuItemMobile.defaultProps = {
  isActive: false,
};

MenuItemMobile.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    subMenu: PropTypes.arrayOf(PropTypes.object),
    url: PropTypes.string,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
  isActive: PropTypes.bool,
};

export default MenuItemMobile;
