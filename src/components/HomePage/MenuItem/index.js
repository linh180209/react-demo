import React, { Component } from 'react';
import PropTypes from 'prop-types';

class MenuItem extends Component {
  onClick = (data) => {
    const { onClick } = this.props;
    if (onClick) {
      onClick(data);
    }
  }

  render() {
    const { data, isActive } = this.props;
    const { name, subMenu } = data;
    return (
      <div className="menuItem">
        <button onClick={() => this.onClick(data)} type="button" className="button_homepage menu color-white" style={{ display: 'inline-flex', alignItems: 'center' }}>
          {name}
          {
          subMenu && subMenu.length > 0
            ? (
              <div className={isActive ? 'details-menu_white details-menu_white-active' : 'details-menu_white'} />
            )
            : (<div />)
        }
        </button>
      </div>
    );
  }
}

MenuItem.defaultProps = {
  isActive: false,
};

MenuItem.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string,
    subMenu: PropTypes.arrayOf(PropTypes.string),
    url: PropTypes.string,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
  isActive: PropTypes.bool,
};

export default MenuItem;
