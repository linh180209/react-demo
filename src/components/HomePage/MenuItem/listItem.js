import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import ItemMobilemenu from './itemMobilemenu';

class ListItem extends Component {
  render() {
    const { menu } = this.props;
    return (
      <div>
        {
          _.map(menu, (item, index) => (
            <ItemMobilemenu
              key={index}
              onClick={item.onClickLink}
              data={item}
            />
          ))
        }
      </div>
    );
  }
}

ListItem.propTypes = {
  menu: PropTypes.arrayOf().isRequired,
};

export default ListItem;
