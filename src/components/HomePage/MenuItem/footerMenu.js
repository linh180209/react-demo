import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { connect } from 'react-redux';
import {
  isIOS,
} from 'react-device-detect';
import { getAltImage, getAltImageV2 } from '../../../Redux/Helpers';
import icEmail from '../../../image/icon/ic-email.svg';

const getCMSFooter = (data) => {
  // console.log('cms footer', data);
  if (data) {
    const { header_text: headerText, body, image_background: imageBg } = data;
    const btBlock = _.filter(body, x => x.type === 'button_block');
    const textBlock = _.find(body, x => x.type === 'text_block');
    const ctaBlock = _.filter(body, x => x.type === 'cta_block');
    const labelBlock = _.filter(body, x => x.type === 'label_and_text_block');
    const imageBlock = _.filter(body, x => x.type === 'image_block');
    return {
      headerText, imageBg, btBlock, textBlock, ctaBlock, labelBlock, imageBlock,
    };
  }
  return {
    headerText: undefined, imageBg: undefined, btBlock: [], textBlock: undefined, ctaBlock: [], labelBlock: [], imageBlock: [],
  };
};

class FooterMenu extends Component {
  render() {
    const { cms } = this.props;
    const footerCms = _.find(cms, x => x.title === 'Footer');
    const {
      ctaBlock, labelBlock,
    } = getCMSFooter(footerCms);

    return (
      <div>
        <div className="div-social div-row">
          {
            _.map(ctaBlock, d => (
              <a href={d.value.link}>
                <img loading="lazy" src={d.value.image.image} alt={getAltImageV2(d.value.image)} style={{ height: '14px' }} />
              </a>
            ))
          }
        </div>
        <div className="div-col justify-center items-center div-info mb-5">
          <span>
            {labelBlock && labelBlock.length > 0 ? labelBlock[0].value.text : ''}
          </span>
          <div>
            <img
              loading="lazy"
              src={icEmail}
              alt="email"
            />
            <span>
              {labelBlock && labelBlock.length > 1 ? labelBlock[1].value.text : ''}
            </span>
          </div>
        </div>
        <div style={{ height: isIOS ? '40px' : '0px' }} />
      </div>
    );
  }
}

PropTypes.propTypes = {
  cms: PropTypes.arrayOf().isRequired,
};

function mapStateToProps(state) {
  return {
    cms: state.cms,
  };
}

export default connect(mapStateToProps)(FooterMenu);
