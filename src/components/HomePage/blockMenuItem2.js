import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import '../../styles/block-menu-item-2.scss';
import icNext from '../../image/icon/ic-next-white.svg';
import { generateUrlWeb, getAltImageV2 } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

function BlockMenuItem2(props) {
  return (
    <div className={`block-menu-item-2 ${props.className}`}>
      {
        isMobile ? (
          <Link className="block-mobile" to={generateUrlWeb(props.data.value.button.link)}>
            <img loading="lazy" src={props.data.value.links.image.image} alt={getAltImageV2(props.data.value.links.image)} />
            <div className="div-text">
              {props.data.value.header.header_text}
              {
                props.isBlog && (
                  <React.Fragment>
                    <div className="header-h4">
                      {props.data.value.categories}
                    </div>
                    <span>
                      {props.data.value.time}
                    </span>
                  </React.Fragment>
                )
              }
            </div>
          </Link>
        ) : (
          <React.Fragment>
            <div className="block-left">
              <Link className="div-image" to={generateUrlWeb(props.data.value.button.link)}>
                <img loading="lazy" src={props.data.value.links.image.image} alt={getAltImageV2(props.data.value.links.image)} />
                <div className="hover-text">
                  <span>
                    {props.data.value.links.image.caption}
                  </span>
                  <img loading="lazy" src={icNext} alt="icon" />
                </div>
              </Link>
              <span>
                {props.data.value.text}
              </span>
              <Link className="button-homepage-new-4 button-a animated-hover" to={generateUrlWeb(props.data.value.button.link)}>
                {props.data.value.button.name}
              </Link>
            </div>
            <div className="block-right">
              <div className="header-h3">
                {props.data.value.header.header_text}
              </div>
              <div className="list-menu">
                {
                  _.map(props.data.value.links.links, d => (
                    <Link to={generateUrlWeb(d.value.link)}>
                      {d.value.text}
                    </Link>
                  ))
                }
              </div>

            </div>
          </React.Fragment>
        )
      }
    </div>
  );
}

export default BlockMenuItem2;
