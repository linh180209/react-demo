import React, { useState, useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import Slider from 'react-slick';
import moment from 'moment';
import '../../styles/block-menu-3.scss';
import BlockMenuItem1 from './blockMenuItem1';
import BlockMenuItem2 from './blockMenuItem2';
import { fetchCMSHomepage, generateUrlWeb } from '../../Redux/Helpers';
import { GET_ALL_ARTICLES } from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import { toastrError } from '../../Redux/Helpers/notification';
import { useMergeState } from '../../Utils/customHooks';
import icPrev from '../../image/icon/prev-menu.svg';
import icNext from '../../image/icon/next-menu.svg';
import { isMobile } from '../../DetectScreen';

function BlockMenu3(props) {
  const offsetLeft = useRef();
  const [state, setState] = useMergeState({
    dataTopStories: [],
    labelBlocks: [],
    isStarted: true,
    isEnded: false,
  });

  // useEffect(() => {
  //   const ele = document.getElementById('end-list');
  //   if (ele) {
  //     const rectEnd = ele.getBoundingClientRect();
  //     console.log('rectEnd', rectEnd);
  //     offsetLeft.current = rectEnd.left - window.screen.width;
  //     console.log('offsetLeft.current', offsetLeft.current);
  //   }
  // }, []);

  const onscroll = () => {
    const ele = document.getElementById('scroll-list');
    if (ele) {
      const object = {};
      if (ele.scrollLeft < 10 && !state.isStarted) {
        _.assign(object, { isStarted: true });
      } else if (state.isStarted) {
        _.assign(object, { isStarted: false });
      }

      const maxOffest = ele.scrollWidth - ele.clientWidth;
      if (Math.abs(ele.scrollLeft - maxOffest) < 10 && !state.isEnded) {
        _.assign(object, { isEnded: true });
      } else if (state.isEnded) {
        _.assign(object, { isEnded: false });
      }
      setState(object);
    }
  };

  const scrollToStart = () => {
    const ele = document.getElementById('scroll-list');
    ele.scrollLeft = 0;
    setTimeout(() => {
      setState({ isEnded: false, isStarted: true });
    }, 500);
  };

  const scrollToEnd = () => {
    const ele = document.getElementById('scroll-list');
    ele.scrollLeft = ele.scrollWidth - ele.clientWidth;
    setTimeout(() => {
      setState({ isEnded: true, isStarted: false });
    }, 500);
  };

  // const fetchArticles = () => {
  //   const options = {
  //     url: GET_ALL_ARTICLES,
  //     method: 'GET',
  //   };
  //   return fetchClient(options);
  // };
  // topStoriesBlocks = data;
  useEffect(() => {
    fetchCMSHomepage('blog').then((dataArticles) => {
      const { body } = dataArticles;
      const labelBlocks = _.filter(body, x => x.type === 'label_block');
      const topStoriesBlocks = _.filter(
        body,
        x => x.type === 'top_stories_block',
      )[0].value;
      const todayTopStories = _.filter(
        labelBlocks,
        x => x.value.name === 'today_top_stories',
      );
      const dataFilterTopStories = topStoriesBlocks && dataArticles
        ? dataArticles.posts.filter(
          elem => topStoriesBlocks.find(({ value }) => elem.slug === value)
                && elem.slug,
        )
        : '';
      setState({
        dataTopStories: _.orderBy(dataFilterTopStories, ['first_published_at'], ['desc']),
        labelBlocks,
      });
    }).catch((err) => {
      toastrError(err);
    });
  }, []);

  const todayTopStories = _.filter(
    state.labelBlocks,
    x => x.value.name === 'today_top_stories',
  );
  const dropdowns2 = props.data ? props.data.dropdowns2 : undefined;
  const dropdowns1 = props.data ? props.data.dropdowns1 : undefined;
  if (!dropdowns2) {
    return (<div />);
  }

  const ele = document.getElementById('div-text-menu');
  let topCurrent = 127;
  if (ele) {
    topCurrent = ele.getBoundingClientRect().top + 40;
  }
  const minHeight = window.innerHeight - topCurrent;
  const linkData = data => (
    <div className="link-block">
      <div className="header-h3">
        {data.value.header.header_text}
      </div>
      {
        _.map(data.value.links.links, d => (
          <Link to={generateUrlWeb(d.value.link)}>
            {d.value.text}
          </Link>
        ))
      }
      <Link className="button-homepage-new-4 button-a animated-hover" to={generateUrlWeb(data.value.button.link)}>
        {data.value.button.name}
      </Link>
    </div>
  );
  return (
    <div className="block-menu-3">
      <div className="scroll-menu">
        {
          isMobile ? (
            <div className="list-block" style={{ height: `${minHeight}px` }}>
              <div className="list-image">
                {
                    _.map(state.dataTopStories, d => (
                      <BlockMenuItem2
                        className="big"
                        isBlog
                        data={{
                          value: {
                            button: {
                              link: generateUrlWeb(`/blog/${d.slug}`),
                            },
                            links: {
                              image: {
                                image: d.image,
                                caption: dropdowns2[0].value.text,
                              },
                            },
                            header: {
                              header_text: d.title,
                            },
                            description: d.description,
                            categories: d.categories[0],
                            time: moment(d.first_published_at).format('DD/MM/YYYY'),
                            // time: moment(d.first_published_at).fromNow(),
                          },
                        }}
                      />
                    ))
                  }
              </div>
              {
                _.map(dropdowns2, d => (
                  <div className="link-text">
                    <div className="header-h3">
                      {d.value.header.header_text}
                    </div>
                    {
                      _.map(d.value.links.links, x => (
                        <Link to={generateUrlWeb(x.value.link)}>
                          {x.value.text}
                        </Link>
                      ))
                    }
                  </div>
                ))
              }
              {/* <div className="link-text">
                <h3>
                  PERFUMES
                </h3>
                {
                  _.map(_.range(5), d => (
                    <Link to="/">
                      Perfume Creation
                    </Link>
                  ))
                }
              </div>
              <div className="link-text">
                <h3>
                  PERFUMES
                </h3>
                {
                  _.map(_.range(5), d => (
                    <Link to="/">
                      Perfume Creation
                    </Link>
                  ))
                }
              </div> */}
              <div className="button-bottom">
                <Link to={generateUrlWeb(dropdowns1[0].value.button.link)} className="button-homepage-new-7">
                  {dropdowns1[0].value.button.name}
                </Link>
              </div>
            </div>
          ) : (
            <React.Fragment>
              <div className="block-left">
                <div className="div-link">
                  {linkData(dropdowns2[0])}
                  <div className="line-vertical" />
                  {linkData(dropdowns2[1])}
                </div>
              </div>
              <div className="block-right">
                <div className="div-header">
                  <div className="header-h3">
                    {todayTopStories && todayTopStories.length > 0 ? todayTopStories[0].value.label : ''}
                  </div>
                  <div className="div-arrow">
                    <button
                      className={`button-bg__none ${state.isStarted ? 'inactive' : ''}`}
                      type="button"
                      onClick={scrollToStart}
                    >
                      <img loading="lazy" src={icPrev} alt="icon" />
                    </button>
                    <button
                      className={`button-bg__none ${state.isEnded ? 'inactive' : ''}`}
                      type="button"
                      onClick={scrollToEnd}
                    >
                      <img loading="lazy" src={icNext} alt="icon" />
                    </button>
                  </div>
                </div>

                <div id="scroll-list" className="scroll-list-block" onScroll={onscroll}>
                  {
                    _.map(state.dataTopStories, d => (
                      <BlockMenuItem1
                        isBlog
                        data={{
                          value: {
                            link: generateUrlWeb(`/blog/${d.slug}`),
                            image: {
                              image: d.image,
                              caption: dropdowns2[0].value.text,
                            },
                            text: d.title,
                            description: d.description,
                            categories: d.categories[0],
                            time: moment(d.first_published_at).format('DD/MM/YYYY'),
                            // time: moment(d.first_published_at).fromNow(),
                          },
                        }}
                      />
                    ))
                  }
                </div>
              </div>
            </React.Fragment>
          )
        }
      </div>
    </div>
  );
}

export default BlockMenu3;
