import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import {
  Player, ControlBar, Shortcut, BigPlayButton,
} from 'video-react';
import CircularProgressbar from 'react-circular-progressbar';
import { Markup } from 'interweave';
import { getAltImage, onClickLink } from '../../Redux/Helpers/index';
import { isBrowser, isMobile, isTablet } from '../../DetectScreen';

class HowItWork extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowAnimation: false,
      progress: 0,
      headerText: undefined,
      textBlock: undefined,
      videoBlock: undefined,
      button: undefined,
      paragraph: [],
      imageUrlBg: undefined,
    };
    this.listTimeStamp = [];
    this.listImageBg = [];
    this.stepPlay = 0;
  }

  componentDidMount() {
    document.addEventListener('scroll', this.trackScrolling);
    const { howitWorkCms } = this.props;
    const {
      buttons, child_sections: childSections, header_text: headerText, header_size: headerSize,
      image_url: imageUrlBg, placeholder,
    } = howitWorkCms;
    const button = buttons ? buttons[0] : undefined;
    const blocks = childSections ? childSections[0].value : undefined;
    const textBlock = _.find(blocks, x => x.type === 'text_block');
    const dataVideos = _.filter(blocks, x => x.type === 'video_block');
    const valueFindVideo = isMobile ? 'phone' : (isTablet ? 'tablet' : 'web');
    const dataVideo = _.find(dataVideos, x => x.value.type === valueFindVideo);
    const dataTimeStamp = _.filter(blocks, x => x.type === 'timestamp_block');
    const dataImageBg = _.filter(blocks, x => x.type === 'image_block');
    const paragraph = _.filter(blocks, x => x.type === 'paragraph_block');
    this.listImageBg = this.getListImageBg(dataImageBg);
    this.listTimeStamp = this.getListTimeStamp(dataTimeStamp);
    const videoBlock = dataVideo ? dataVideo.value : undefined;

    this.setState({
      headerText, textBlock, videoBlock, button, paragraph, imageUrlBg, placeholder,
    });
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.trackScrolling);
    if (this.getTime) {
      clearInterval(this.getTime);
    }
  }

  getListImageBg = (data) => {
    const list = [];
    _.forEach(data, (d) => {
      list.push(d.value.image_url);
    });
    return list;
  }

  getListTimeStamp = (datas) => {
    const list = [0];
    _.forEach(datas, (d) => {
      list.push(d.value.timestamp);
    });
    return list;
  }

  handleVideo = () => {
    const { player } = this.refs.player.getState();
    const { currentTime } = player;
    for (let i = 0; i < this.listTimeStamp.length - 1; i += 1) {
      if (this.listTimeStamp[i] <= currentTime && currentTime < this.listTimeStamp[i + 1]) {
        if (this.stepPlay !== i) {
          this.refeshAnimation();
        }
        this.stepPlay = i;
        break;
      }
    }
    const totalTime = this.listTimeStamp[this.stepPlay + 1] - this.listTimeStamp[this.stepPlay];
    const progress = (currentTime - this.listTimeStamp[this.stepPlay]) / totalTime * 100;
    this.setState({ progress });
  }

  trackScrolling = () => {
    const wrappedElement = document.getElementById('bt-howitwork');
    if (this.state.isShowAnimation) {
      // const {paused} = this.refs.player.getState();
      // if( paused ) {
      this.refs.player.play();
      // }
      return;
    }
    if (this.isBottom(wrappedElement)) {
      this.setState({ isShowAnimation: true });
      this.refs.player.play();
      // document.removeEventListener('scroll', this.trackScrolling);
      this.getTime = setInterval(this.handleVideo, 500);
    }
  };

  isBottom = el => el.getBoundingClientRect().bottom <= window.innerHeight

  handlePrev = () => {
    if (this.stepPlay > 0) {
      this.refs.player.seek(this.listTimeStamp[this.stepPlay - 1]);
    } else {
      this.refs.player.seek(this.listTimeStamp[0]);
    }
  }

  handleNext = () => {
    if (this.stepPlay < this.listTimeStamp.length - 2) {
      this.refs.player.seek(this.listTimeStamp[this.stepPlay + 1]);
    } else {
      this.refs.player.seek(this.listTimeStamp[this.stepPlay]);
    }
  }

  refeshAnimation = () => {
    const el = document.getElementById('idTitle');
    el.classList.remove('title-hiw-ani');
    // eslint-disable-next-line no-unused-expressions
    el.offsetWidth;
    el.classList.add('title-hiw-ani');
  }

  handleButton = (url) => {
    const { history } = this.props;
    onClickLink(url, null, history);
  }

  render() {
    const {
      isShowAnimation, progress, headerText, textBlock, videoBlock, button,
      imageUrlBg,
      paragraph, placeholder,
    } = this.state;
    const styleVideo = isBrowser ? { width: '50%' } : (isTablet ? { width: '80%' } : { width: '100%', marginTop: '40px' });
    return (
      <div
        className="div-howitwork"
      >
        {
          <img
            loading="lazy"
            className={isShowAnimation ? 'img_bg-howitwok' : 'hiden'}
            src={imageUrlBg}
            alt={getAltImage(imageUrlBg)}
            style={isMobile ? {
              height: '100%',
            } : {}}
          />
        }
        {
          headerText
            ? (
              <span
                className="text-title mb-4"
              >
                {headerText}
              </span>
            ) : (<div />)
        }
        {/* {
          textBlock
            ? (
              <div className="div-text" style={isMobile && !isTablet ? { marginTop: '30px', marginBottom: '30px' } : {}}>
                {textBlock.value}
              </div>
            ) : (<div />)
        } */}
        {
          videoBlock
            ? (
              <div
                className="div-video disable-button"
                style={styleVideo}
              >
                <Player
                  ref="player"
                  playsInline
                  loop
                  muted
                  autobuffer
                  disablePauseOnClick
                  src={videoBlock ? videoBlock.video_url : ''}
                  poster={videoBlock ? videoBlock.placeholder : ''}
                >
                  <ControlBar disableCompletely disableDefaultControls />
                  <Shortcut dblclickable clickable={false} />
                  <BigPlayButton />
                </Player>
                <div>
                  <CircularProgressbar
                    strokeWidth={4}
                    className="progressbar progressbar-left"
                    percentage={0}
                    styles={{
                      path: {
                        stroke: 'rgba(26, 26, 26)',
                      },
                    }}
                  />
                  <button
                    className="progressbar progressbar-left howitwork-bt_icon"
                    type="button"
                    onClick={this.handlePrev}
                  >
                    <i className="fa fa-chevron-left" />
                  </button>
                </div>
                <div>
                  <CircularProgressbar
                    strokeWidth={4}
                    className="progressbar progressbar-right"
                    percentage={progress}
                    styles={{
                      path: {
                        stroke: 'rgba(26, 26, 26)',
                      },
                    }}
                  />
                  <button
                    className="progressbar progressbar-right howitwork-bt_icon"
                    type="button"
                    onClick={this.handleNext}
                  >
                    <i className="fa fa-chevron-right" />
                  </button>
                </div>
                <div
                  id="idTitle"
                  className="div-title-howitwork title-hiw-ani"
                  style={isMobile && !isTablet ? { marginTop: '-30px' } : { }}
                >
                  <Markup
                    content={this.stepPlay === 0 && paragraph.length > 0 ? (
                      paragraph[0].value
                    )
                      : (
                        this.stepPlay === 1 && paragraph.length > 1 ? (
                          paragraph[1].value
                        ) : (
                          this.stepPlay === 2 && paragraph.length > 2 ? (
                            paragraph[2].value
                          ) : <div />
                        )
                      )}
                  />
                </div>
                <div id="bt-howitwork" className="point-step">
                  <div className={this.stepPlay === 0 ? 'point-step_active' : ''} />
                  <div className={this.stepPlay === 1 ? 'point-step_active' : ''} />
                  <div className={this.stepPlay === 2 ? 'point-step_active' : ''} />
                </div>
              </div>
            ) : (<div className="div-video" style={{ height: '450px' }} />)
        }
        {
          button
            ? (
              <div className="div-button">
                <button
                  type="button"
                  className="button_home_page"
                  onClick={() => this.handleButton(button.value.link)}
                >
                  {button.value.text}
                </button>
              </div>
            ) : (<div />)
        }

      </div>
    );
  }
}

HowItWork.propTypes = {
  howitWorkCms: PropTypes.shape({
    child_sections: PropTypes.arrayOf(PropTypes.object),
    buttons: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default withRouter(HowItWork);
