import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ProductItem from './productItem';
import ProductItemMobile from './productItemMobile';
import { onClickLink } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

class ProductPage extends Component {
  state = {
    products: [],
  }

  componentDidMount() {
    const { productCms } = this.props;
    const {
      buttons, child_sections: childSections, header_text: headerText,
    } = productCms;
    const button = buttons ? buttons[0] : undefined;
    const blocks = childSections ? childSections[0].value : undefined;
    const productBlocks = _.filter(blocks, x => x.type === 'product_block');
    const products = [];
    _.forEach(productBlocks, (p) => {
      const { value } = p;
      products.push({
        url: value.image.image,
        name: value.text,
        description: value.description,
        link: value.link,
        price: value.price,
      });
    });
    this.setState({ products, button });
  }

  gotoClick = (link) => {
    const { history } = this.props;
    onClickLink(link, undefined, history);
    // this.props.history.push(link);
  }

  render() {
    const { products, button } = this.state;
    const heightStyle = isTablet ? '55vh' : '60vh';
    return (
      <div
        className="div-col justify-between items-center"
        style={{
          marginTop: isMobile ? '10px' : '15vh',
          marginBottom: isMobile ? '10px' : '15vh',
          marginLeft: '10%',
          marginRight: '10%',
          position: 'relative',
        }}
      >
        <span
          className="text-title"
          style={{
            marginBottom: '50px',
          }}
        >
          OUR PRODUCTS
        </span>
        {
          isMobile && !isTablet ? (
            <Row style={{ zIndex: '1' }}>
              <Col xs="12">
                <ProductItemMobile
                  datas={products}
                  gotoClick={this.gotoClick}
                />
              </Col>
            </Row>
          ) : (
            <Row style={{ zIndex: '1' }}>
              {_.map(products, d => (
                <Col
                  md={isTablet ? '6' : '3'}
                  xs="12"
                  style={{ width: '80%', height: heightStyle }}
                >
                  <ProductItem
                    url={d.url}
                    name={d.name}
                    price={d.price}
                    description={d.description}
                    link={d.link}
                    gotoClick={this.gotoClick}
                  />
                </Col>
              ))}
            </Row>
          )
        }
        {
          isTablet ? (
            <button
              type="button"
              className="button_home_page"
              style={{
                position: 'absolute',
                top: '60%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                zIndex: '9',
              }}
              onClick={() => this.gotoClick(button.value.link)}
            >
              {button ? button.value.text : ''}
            </button>
          ) : (
            <div
              style={{
                height: isMobile ? '2rem' : '10vh',
                zIndex: '9',
              }}
              className="div-col justify-end items-center"
            >
              <button
                type="button"
                className="button_home_page"
                onClick={() => this.gotoClick(button.value.link)}
              >
                {button ? button.value.text : ''}
              </button>
            </div>
          )
        }
      </div>
    );
  }
}

ProductPage.propTypes = {
  productCms: PropTypes.shape(PropTypes.object).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

export default withRouter(ProductPage);
