import React, { Component } from 'react';
import _ from 'lodash';
import {
  Row, Col,
} from 'reactstrap';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { convertHeaderToSize } from '../../Redux/Helpers';
import ScentItem from '../ScentItem';
import bg9 from '../../image/bg9.svg';
import { isMobile, isTablet } from '../../DetectScreen';

class Community extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowAnimation: false,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.trackScrolling);
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.trackScrolling);
  }

  getDataFromBlock = (block) => {
    if (!block) {
      return undefined;
    }
    const dataHeader = _.find(block, x => x.type === 'header_block');
    const headerBlock = dataHeader ? dataHeader.value : undefined;
    const dataText = _.find(block, x => x.type === 'text_block');
    const textBlock = dataText ? dataText.value : undefined;
    const dataButton = _.find(block, x => x.type === 'button_block');
    const buttonBlock = dataButton ? dataButton.value : undefined;
    const dataImage = _.find(block, x => x.type === 'image_block');
    const imageBlock = dataImage ? dataImage.value : undefined;
    return {
      name: headerBlock ? headerBlock.header_text : '',
      created: textBlock,
      buttonName: buttonBlock ? buttonBlock.text : '',
      image_urls: imageBlock ? [imageBlock.image_url] : '',
    };
  }

  trackScrolling = () => {
    const wrappedElement = document.getElementById('idTitle');
    if (this.isBottom(wrappedElement)) {
      this.setState({ isShowAnimation: true });
      document.removeEventListener('scroll', this.trackScrolling);
    }
  };

  isBottom = el => el.getBoundingClientRect().bottom <= window.innerHeight


  render() {
    const { communityCms } = this.props;
    const { isShowAnimation } = this.state;
    if (!communityCms) {
      return (<div />);
    }
    const {
      buttons, child_sections: childSections, header_text: headerText, header_size: headerSize,
    } = communityCms;
    const datas = [];
    const button = buttons ? buttons[0] : undefined;
    const textBlock = childSections.length > 0 ? _.find(childSections[0].value, x => x.type === 'text_block') : undefined;
    const block1 = childSections.length > 1 ? childSections[1].value : undefined;
    const block2 = childSections.length > 2 ? childSections[2].value : undefined;
    const block3 = childSections.length > 3 ? childSections[3].value : undefined;
    const dataB1 = this.getDataFromBlock(block1);
    const dataB2 = this.getDataFromBlock(block2);
    const dataB3 = this.getDataFromBlock(block3);
    if (dataB1) {
      datas.push(dataB1);
    }
    if (dataB2) {
      datas.push(dataB2);
    }
    if (dataB3) {
      datas.push(dataB3);
    }
    const styleHeader = {
      fontSize: headerSize ? convertHeaderToSize(headerSize) : undefined,
    };
    const dataDisplay = isMobile && !isTablet ? (datas.length > 1 ? datas.splice(0, 2) : datas) : (datas.length > 2 ? datas.splice(0, 3) : datas);
    return (
      <div className="div-community">
        <img loading="lazy" className="div-bg" src={bg9} alt="bg" />
        <span id="idTitle" className="text-title" style={styleHeader}>
          {headerText}
        </span>
        <div className="div-text">
          {textBlock.value}
        </div>
        <div className={isMobile && !isTablet ? 'div-product-mobile div-row w-100 justify-around' : 'div-product'}>
          {
            _.map(dataDisplay, (data, index) => (
              <ScentItem
                className={isShowAnimation ? `animation-${index + 1}` : ''}
                data={data}
              />
            ))
          }
        </div>
        <div className="div-button">
          <a
            // type="button"
            className="button_homepage"
            href={button.value.link}
          >
            {button.value.text}
          </a>
        </div>
      </div>
    );
  }
}

Community.propTypes = {
  communityCms: PropTypes.shape({
    buttons: PropTypes.arrayOf(PropTypes.object),
    child_sections: PropTypes.arrayOf(PropTypes.object),
    header_text: PropTypes.string,
  }).isRequired,
};

export default withRouter(Community);
