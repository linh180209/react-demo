import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Dropdown, DropdownMenu, DropdownToggle,
} from 'reactstrap';
import _ from 'lodash';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  isIOS,
} from 'react-device-detect';
import '../../styles/header_partnership.scss';

import icBackWhite from '../../image/icon/ic-black-white.svg';
import logomark from '../../image/icon/logo-menu.svg';
import logomarkWhite from '../../image/icon/logo-21g-white.svg';
import icDownBlack from '../../image/icon/ic-down-partnership-black.svg';
import icChecked from '../../image/icon/icCheckedDropdown.svg';
import icDownWhite from '../../image/icon/ic-down-partnership-white.svg';
import iconClose from '../../image/icon_close.svg';
import logoPartnerWhite from '../../image/icon/logoPartnership-white.svg';
import testFlags from '../../image/test/test13.svg';
import icCart from '../../image/icon/ic-cart.svg';
import icCartBlack from '../../image/icon/ic-cart-black.svg';
import auth from '../../Redux/Helpers/auth';
import icEmail from '../../image/icon/ic-email.svg';
import { deleteProductBasket, updateProductBasket } from '../../Redux/Actions/basket';
import {
  onClickLink, getAltImage, getCmsCommon, getNameFromCommon, generaCurrency, fetchCMSHomepage, getAltImageV2, generateUrlWeb,
} from '../../Redux/Helpers';
import addCmsRedux from '../../Redux/Actions/cms';
import {
  GET_LIST_LANGUAGE,
} from '../../config';
import fetchClient from '../../Redux/Helpers/fetch-client';
import CardItem from '../B2B/CardItem';

const getCMSFooter = (data) => {
  if (data) {
    const { header_text: headerText, body, image_background: imageBg } = data;
    const btBlock = _.filter(body, x => x.type === 'button_block');
    const textBlock = _.find(body, x => x.type === 'text_block');
    const ctaBlock = _.filter(body, x => x.type === 'cta_block');
    const labelBlock = _.filter(body, x => x.type === 'label_and_text_block');
    const imageBlock = _.filter(body, x => x.type === 'image_block');
    return {
      headerText, imageBg, btBlock, textBlock, ctaBlock, labelBlock, imageBlock,
    };
  }
  return {
    headerText: undefined, imageBg: undefined, btBlock: [], textBlock: undefined, ctaBlock: [], labelBlock: [], imageBlock: [],
  };
};
class HeaderPartnership extends Component {
  state = {
    dropdownOpen: false,
    codeLanguage: auth.getLanguage(),
    listLanguage: [],
    isShowCard: false,
    listCountry: this.props.countries,
  }

  componentDidMount() {
    this.fetchLanguage();
    this.fetchData();
    // this.fetchCountry();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const objectReturn = {};
    const { basket, countries } = nextProps;
    if (basket && basket !== prevState.basket) {
      const newSize = basket.id && basket.items ? basket.items.length : -1;
      if (newSize > prevState.oldSize && prevState.oldSize !== -1) {
        _.assign(objectReturn, { isShowCard: true });
      }
      _.assign(objectReturn, { basket, oldSize: newSize });
      // _.assign(objectReturn, { basket });
    }
    if (countries && countries.length > 0 && countries !== prevState.listCountry) {
      _.assign(objectReturn, { basket, listCountry: countries });
    }
    return !_.isEmpty(objectReturn) ? objectReturn : null;
  }

  fetchData = async () => {
    const { cms } = this.props;
    if (cms) {
      const cmsFooter = _.find(cms, x => x.title === 'Footer');
      if (!cmsFooter) {
        try {
          const result = await fetchCMSHomepage('footer');
          this.props.addCmsRedux(result);
        } catch (error) {
          // toastrError(error.message);
        }
      }
    }
  }

  fetchLanguage = () => {
    const option = {
      url: GET_LIST_LANGUAGE,
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      this.setState({ listLanguage: result });
    });
  }

  toggle = () => {
    const { dropdownOpen } = this.state;
    this.setState({
      dropdownOpen: !dropdownOpen,
    });
  }

  gotoCheckOut = () => {
    this.props.history.push(generateUrlWeb('/checkout'));
  }

  onChange = (code) => {
    auth.setLanguage(code);
    this.setState({ codeLanguage: code, dropdownOpen: false });
    window.location.reload();
  }

  onCloseCard = () => {
    this.setState({ isShowCard: false });
  }

  onOpenCard = () => {
    this.setState({ isShowCard: true });
  }

  render() {
    const isBrowser = window.innerWidth > 1024;
    const {
      isBlack, logo, nameBtLeft, funcBtLeft, basket, cms,
    } = this.props;
    if (!basket) {
      return null;
    }
    const { listCountry } = this.state;
    const { codeLanguage, listLanguage, isShowCard } = this.state;
    const currentLanuage = _.find(listLanguage, x => x.code === codeLanguage) || {};
    const totalBasket = basket && basket.items ? basket.items.length : 0;
    const {
      items, id, discount, subtotal,
    } = this.state.basket;
    const newPrice = parseFloat(discount) > 0 ? parseFloat(subtotal) - parseFloat(discount) : -1;
    const footerCms = _.find(cms, x => x.title === 'Footer');
    const cmsCommon = getCmsCommon(cms);
    const yourBagsBt = getNameFromCommon(cmsCommon, 'YOUR_BAG_IS_CURRENTLY_EMPTY');
    const continueShopBt = getNameFromCommon(cmsCommon, 'CONTINUE_SHOPPING');
    const totalBt = getNameFromCommon(cmsCommon, 'TOTAL');
    const checkOutBt = getNameFromCommon(cmsCommon, 'CHECKOUT');
    const continueBt = getNameFromCommon(cmsCommon, 'CONTINUE_SHOPPING');
    const myBagBt = getNameFromCommon(cmsCommon, 'MY_BAG');
    const {
      ctaBlock, labelBlock,
    } = getCMSFooter(footerCms);

    const htmlCartEmpty = (
      <div className="div-empty div-col justify-center items-center w-100 h-100">
        <span>
          {yourBagsBt}
        </span>
        <button
          onClick={this.gotoProduct}
          type="button"
          className="button-bg__none pa0"
          style={{ borderBottom: '1px solid #000000' }}
        >
          {continueShopBt}
        </button>
      </div>
    );
    const htmlBag = (
      <React.Fragment>
        <ul>
          {
            _.map(items, (d, index) => (
              <li className="cd-cart__product" key={index}>
                <CardItem
                  updateProductBasket={this.props.updateProductBasket}
                  deleteProductBasket={this.props.deleteProductBasket}
                  data={d}
                  idCart={id}
                  isB2C
                  onCloseCard={this.onCloseCard}
                  cms={this.props.cms}
                  listCountry={listCountry}
                />
              </li>
            ))
          }
        </ul>
        <div className="div-row justify-between div-total">
          <span>
            <b>
              {totalBt}
            </b>
          </span>
          {
            newPrice === -1
              ? (
                <span>
                  <b>
                    {generaCurrency(subtotal)}
                  </b>
                </span>
              ) : (
                <span>
                  <span className="strike">
                    <b>
                      {generaCurrency(subtotal)}
                    </b>
                  </span>
                  <span className="ml-2">
                    <b>
                      {generaCurrency(newPrice)}
                    </b>
                  </span>
                </span>
              )
          }
        </div>
        <div className="div-bt-checkout mt-4">
          <button
            onClick={this.gotoCheckOut}
            type="button"
          >
            {checkOutBt}
          </button>
        </div>
        <div className="div-bt-continue mt-2">
          <button
            onClick={this.onCloseCard}
            type="button"
          >
            {continueBt}
          </button>
        </div>
        <div className="div-social div-row">
          {
            _.map(ctaBlock, (d, index) => (
              <a key={index} href={d.value.link}>
                <img loading="lazy" src={d.value.image.image} alt={getAltImageV2(d.value.image)} style={{ height: '53px' }} />
              </a>
            ))
          }
        </div>
        <div className="div-col justify-center items-center div-info mb-5">
          <span style={{ fontSize: '20px' }}>
            {labelBlock && labelBlock.length > 0 ? labelBlock[0].value.text : ''}
          </span>
          <div>
            <img
              loading="lazy"
              src={icEmail}
              alt="email"
              style={{ width: '0.9rem', marginRight: '5px' }}
            />
            <span style={{ fontSize: '10px' }}>
              {labelBlock && labelBlock.length > 1 ? labelBlock[1].value.text : ''}
            </span>
          </div>
        </div>
        <div style={{ height: isIOS ? '40px' : '0px' }} />
      </React.Fragment>
    );
    return (
      <div className={`div-header-partnership ${isBlack ? 'black' : ''}`}>
        <div className="div-content-header">
          {
            nameBtLeft
              ? (
                <button className="button-left" type="button" onClick={funcBtLeft}>
                  <img loading="lazy" src={icBackWhite} alt="icBackWhite" />
                  {nameBtLeft}
                </button>
              ) : (<div />)
          }
          <div className="div-logo">
            <img loading="lazy" src={isBlack ? logomarkWhite : logomark} alt="logomark" />
            {
              isBlack ? (
                <img loading="lazy" src={logo} alt="logomark" />
              ) : (<div />)
            }
          </div>
          <div className="div-language">
            <Dropdown
              isOpen={this.state.dropdownOpen}
              toggle={this.toggle}
              className="dropdown-language"
            >
              <DropdownToggle
                tag="span"
                onClick={this.toggle}
                data-toggle="dropdown"
                aria-expanded={this.state.dropdownOpen}
              >
                <div className={`div-value ${isBlack ? 'black' : ''}`} onClick={this.toggle}>
                  <div className="div-name">
                    <img loading="lazy" src={testFlags} alt="img" />
                    <span className={`ml-3 ${!isBlack ? 'black' : ''}`}>
                      {currentLanuage.name}
                    </span>
                  </div>
                  <img loading="lazy" src={isBlack ? icDownWhite : icDownBlack} alt="icDown" />
                </div>

              </DropdownToggle>
              <DropdownMenu className="div-drop-menu">
                {
                  _.map(listLanguage, d => (
                    <div
                      onClick={() => {
                        this.onChange(d.code);
                      }}
                    >
                      <div className={`div-value border-none ${isBlack ? 'black' : ''}`}>
                        <div className="div-name">
                          <img loading="lazy" src={testFlags} alt="img" />
                          <span className="ml-3 black">
                            {d.name}
                          </span>
                        </div>
                        {
                          d.code === codeLanguage
                            ? <img loading="lazy" src={icChecked} alt="icDown" />
                            : (<div />)
                        }

                      </div>
                    </div>
                  ))
                }
              </DropdownMenu>
            </Dropdown>
          </div>
          <button
            type="button"
            className="button-cart"
            onClick={this.onOpenCard}
            style={{ marginRight: '0px' }}
          >
            <div className="div-icon-cart">
              <img loading="lazy" src={isBlack ? icCart : icCartBlack} style={{ width: '24px' }} alt="icCart" />
              <div className="cart-amount w-100">
                <span>
                  <b>
                    {totalBasket}
                  </b>
                </span>
              </div>
            </div>
          </button>
          <div className={isShowCard ? 'cd-cart cd-cart--open' : 'cd-cart'}>
            <div className="cd-card__layout" style={!isBrowser ? { right: '0px' } : {}}>
              <header className="cd-cart__header">
                <h2>
                  {myBagBt}
                </h2>
                <button
                  type="button"
                  className="button-bg__none"
                  onClick={this.onCloseCard}
                >
                  <img loading="lazy" style={{ width: '10px', marginRight: '20px' }} src={iconClose} alt="iconClose" />
                </button>
              </header>
              <div className={_.isEmpty(items) ? 'cd-cart__body div-col justify-center items-center' : 'cd-cart__body'}>
                {_.isEmpty(items) ? htmlCartEmpty : htmlBag}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    login: state.login,
    basket: state.basket,
    cms: state.cms,
    countries: state.countries,
  };
}

const mapDispatchToProps = {
  deleteProductBasket,
  updateProductBasket,
  addCmsRedux,
};

HeaderPartnership.propTypes = {
  isBlack: PropTypes.bool.isRequired,
  listLanguage: PropTypes.arrayOf().isRequired,
  countries: PropTypes.arrayOf().isRequired,
  logo: PropTypes.string.isRequired,
  nameBtLeft: PropTypes.string.isRequired,
  funcBtLeft: PropTypes.func.isRequired,
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HeaderPartnership));
