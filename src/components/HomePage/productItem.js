import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';
import rightIcon from '../../image/right-arrow.svg';
import leftIcon from '../../image/left-arrow.svg';
import { getAltImage } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

class ProductItem extends Component {
  onClickNext = () => {
    const { indexActive, total } = this.props;
    const index = indexActive + 1 === total ? 0 : indexActive + 1;
    this.props.onClickNext(index);
  }

  onClickPrev = () => {
    const { indexActive, total } = this.props;
    const index = indexActive - 1 < 0 ? total - 1 : indexActive - 1;
    this.props.onClickNext(index);
  }

  render() {
    const {
      url, name, description, isSilde, indexActive, total, link, price,
    } = this.props;
    return (
      <div className="div-col justify-end items-center w-100 h-100">
        <div
          style={{
            flex: '1',
          }}
          className="div-col justify-end items-center"
        >
          <div
            style={{
              width: '100%',
              position: 'relative',
            }}
            className="div-col items-center"
          >
            <img
              loading="lazy"
              style={{
                width: '60%',
                cursor: 'pointer',
              }}
              src={url}
              alt={getAltImage(url)}
              onClick={() => { this.props.gotoClick(link); }}
            />
            {
              isMobile && !isTablet ? (
                <img
                  loading="lazy"
                  style={{
                    position: 'absolute',
                    top: '50%',
                    left: '0px',
                    transform: 'translateY(-50%)',
                    zIndex: '1',
                    width: '24px',
                    height: '24px',
                    cursor: 'pointer',
                  }}
                  onClick={this.onClickPrev}
                  src={leftIcon}
                  alt="right"
                />
              ) : (<div />)
            }
            {
              isMobile && !isTablet
                ? (
                  <img
                    loading="lazy"
                    style={{
                      position: 'absolute',
                      top: '50%',
                      right: '0px',
                      transform: 'translateY(-50%)',
                      zIndex: '1',
                      width: '24px',
                      height: '24px',
                      cursor: 'pointer',
                    }}
                    src={rightIcon}
                    onClick={this.onClickNext}
                    alt="left"
                  />
                ) : (<div />)
            }

          </div>
        </div>
        <div
          className="div-col items-center"
        >
          <span
            style={{
              fontSize: '1.2rem',
              color: '#a0a0a0',
              marginTop: '2rem',
            }}
          >
            {name}
          </span>
          <span
            style={{
              color: '#3B3B3B',
              textAlign: 'center',
              width: '80%',
              marginTop: '1rem',
              height: '4rem',
            }}
          >
            {ReactHtmlParser(description)}
          </span>
          <span style={{
            fontWeight: '600',
            marginTop: '1rem',
          }}
          >
            {price}
          </span>
          {
            isSilde ? (
              <div className="div-row justify-between w-40 mt-3 mb-3">
                {
                  _.map(new Array(total), (d, index) => (
                    <div
                      onClick={() => this.props.onClickNext(index)}
                      style={{
                        width: '20px',
                        height: '20px',
                        background: indexActive === index ? '#ACACAC' : '#ffffff',
                        border: '1px solid #ACACAC',
                        borderRadius: '50%',
                        cursor: 'pointer',
                      }}
                    />
                  ))
                }
              </div>
            ) : (<div />)
          }
        </div>
      </div>
    );
  }
}

ProductItem.propTypes = {
  url: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  isSilde: PropTypes.bool.isRequired,
  indexActive: PropTypes.number.isRequired,
  onClickNext: PropTypes.func.isRequired,
  total: PropTypes.number.isRequired,
  link: PropTypes.string.isRequired,
  gotoClick: PropTypes.func.isRequired,
};

export default ProductItem;
