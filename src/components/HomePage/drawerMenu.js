import React, { useState, useEffect } from 'react';
import { SwipeableDrawer } from '@material-ui/core';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import classnames from 'classnames';
import { useMergeState } from '../../Utils/customHooks';
import icClose from '../../image/icon/ic-close-drawer.svg';
import {
  generateUrlWeb, getNameFromButtonBlock, getLinkFromButtonBlock, addFontCustom,
} from '../../Redux/Helpers';
import Region from '../Region';
import { isMobile } from '../../DetectScreen';
import ButtonCT from '../ButtonCT';

function DrawerMenu(props) {
  const [selectMenu, setSelectMenu] = useState({});
  const [isShown, setIsShown] = useState(false);


  useEffect(() => {
    if (props.menuBlock && props.menuBlock.length > 0) {
      setSelectMenu(props.menuBlock[0].value);
    }
  }, [props.menuBlock]);

  const takeQuizNow = getNameFromButtonBlock(props.buttonBlocks, 'take-quiz-now');
  return (
    <SwipeableDrawer
      anchor="left"
      open={props.isOpen}
      onClose={() => props.toggleDrawer(false)}
      onOpen={() => props.toggleDrawer(true)}
    >
      <div className={classnames('drawer-menu-left', addFontCustom())}>
        <button
          onMouseEnter={() => !isMobile && setIsShown(true)}
          onMouseLeave={() => !isMobile && setIsShown(false)}
          className="button-close button-bg__none"
          type="button"
          onClick={() => props.toggleDrawer(false)}
        >
          <img loading="lazy" src={icClose} alt="icon" />
        </button>
        <div className="list-button">
          {
            _.map(props.menuBlock || [], d => (
              <button
                onClick={() => setSelectMenu(d.value)}
                type="button"
                className={classnames('button-bg__none', (d.value.text === selectMenu.text ? 'active' : ''))}
              >
                {d.value.text}
              </button>
            ))
          }
        </div>
        <div className="list-link">
          <div className="scroll-link">
            <div className="links">
              {
              _.map(selectMenu.submenu || [], d => (
                <Link data-message={d.value?.text} className="header_4" to={generateUrlWeb(d.value.link)} onClick={() => props.toggleDrawer(false)}>
                  {d.value.name}
                  {/* {
                    !_.isEmpty(d.value.text) && (
                      <span>
                        {d.value.text}
                      </span>
                    )
                  } */}
                </Link>
              ))
            }
            </div>
            {/* <div className="temp-class" /> */}
            {
              props.menuBlock && props.menuBlock.length > 0 && selectMenu.text === props.menuBlock[0].value.text && (
                <ButtonCT
                  typeBT="LINK"
                  className={classnames('button-black', 'animated-hover')}
                  name={takeQuizNow}
                  href={generateUrlWeb(getLinkFromButtonBlock(props.buttonBlocks, 'take-quiz-now'))}
                />
              )
            }
            {/* {
              props.menuBlock && props.menuBlock.length > 0 && selectMenu.text === props.menuBlock[0].value.text && (
                <Link to={generateUrlWeb(getLinkFromButtonBlock(props.buttonBlocks, 'take-quiz-now'))} type="button">
                  {takeQuizNow}
                </Link>
              )
            } */}
            {
              props.menuBlock && props.menuBlock.length > 0 && selectMenu.text === props.menuBlock[0].value.text && (
                <div className="div-region-web">
                  <Region countries={props.countries} buttonBlocks={props.buttonBlockCard} history={props.history} isBlack onCloseDrawer={() => { props.toggleDrawer(false); props.openRegionOut(); }} />
                </div>
              )
            }
          </div>
        </div>
      </div>
    </SwipeableDrawer>
  );
}

export default DrawerMenu;
