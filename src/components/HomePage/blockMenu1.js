import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import classnames from 'classnames';
import ReactHtmlParser from 'react-html-parser';
import _ from 'lodash';
import '../../styles/block-menu-1.scss';
import { generateUrlWeb } from '../../Redux/Helpers';
import BlockMenuItem1 from './blockMenuItem1';
import icPrev from '../../image/icon/prev-menu.svg';
import icNext from '../../image/icon/next-menu.svg';
import { useMergeState } from '../../Utils/customHooks';
import { isMobile } from '../../DetectScreen';

function BlockMenu1(props) {
  // const settings = {
  //   dots: true,
  //   infinite: true,
  //   speed: 500,
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   autoplay: false,
  //   arrows: false,
  //   autoplaySpeed: 5000,
  //   pauseOnHover: false,
  // };
  const [state, setState] = useMergeState({
    isStarted: true,
    isEnded: false,
  });

  const scrollToStart = () => {
    const ele = document.getElementById(props.id);
    ele.scrollLeft = 0;
    setTimeout(() => {
      setState({ isEnded: false, isStarted: true });
    }, 500);
  };

  const scrollToEnd = () => {
    const ele = document.getElementById(props.id);
    ele.scrollLeft = ele.scrollWidth - ele.clientWidth;
    setTimeout(() => {
      setState({ isEnded: true, isStarted: false });
    }, 500);
  };

  const onscroll = () => {
    const ele = document.getElementById(props.id);
    if (ele) {
      const object = {};
      if (ele.scrollLeft < 10 && !state.isStarted) {
        _.assign(object, { isStarted: true });
      } else if (state.isStarted) {
        _.assign(object, { isStarted: false });
      }

      const maxOffest = ele.scrollWidth - ele.clientWidth;
      if (Math.abs(ele.scrollLeft - maxOffest) < 10 && !state.isEnded) {
        _.assign(object, { isEnded: true });
      } else if (state.isEnded) {
        _.assign(object, { isEnded: false });
      }
      setState(object);
    }
  };
  const ele = document.getElementById('div-text-menu');
  let topCurrent = 127;
  if (ele) {
    topCurrent = ele.getBoundingClientRect().top + 40;
  }
  const minHeight = window.innerHeight - topCurrent;
  const dropdowns1 = props.data.dropdowns1.length > 0 && props.data.dropdowns1[0];
  if (!dropdowns1) {
    return <div style={{ height: '300px' }} />;
  }
  return (
    <div className={`block-menu-1 ${props.classWrap}`}>
      <div className="scroll-menu">
        {
          isMobile ? (
            <div className={`list-block ${props.classNameMobile}`} style={{ height: `${minHeight}px` }}>
              <div className="list-item">
                {
                  _.map(dropdowns1.value.ctas, d => (
                    <BlockMenuItem1
                      data={d}
                      classNameMobile={props.classNameItemMobile}
                      description={props.isClub21g ? dropdowns1.value.description : undefined}
                      header={props.isClub21g ? d.value.text : undefined}
                      isAbout={props.isAbout}
                    />
                  ))
                }
              </div>
              <div className="div-temp" />
              <div className={classnames('button-bottom', (props.isAbout ? 'hidden' : ''))}>
                <Link to={generateUrlWeb(dropdowns1.value.button.link)} className={classnames('button-homepage-new-7')}>
                  {dropdowns1.value.button.name}
                </Link>
              </div>

            </div>
          ) : (
            <React.Fragment>
              <div className={classnames('block-left', (props.isAbout ? 'hidden' : ''))}>
                <div className="header-h2">
                  {dropdowns1.value.text}
                </div>
                <div className="des">
                  {ReactHtmlParser(dropdowns1.value.description)}
                </div>
                <Link to={generateUrlWeb(dropdowns1.value.button.link)} className="button-homepage-new-4 button-a animated-hover">
                  {dropdowns1.value.button.name}
                </Link>
              </div>
              <div className={`block-right ${dropdowns1.value.ctas.length > 3 ? 'scroll-able' : ''} ${props.classNameRight}`}>
                <div className={dropdowns1.value.ctas.length > 3 ? 'div-header' : 'hidden'}>
                  <div />
                  <div className="div-arrow">
                    <button
                      className={`button-bg__none ${state.isStarted ? 'inactive' : ''}`}
                      type="button"
                      onClick={scrollToStart}
                    >
                      <img loading="lazy" src={icPrev} alt="icon" />
                    </button>
                    <button
                      className={`button-bg__none ${state.isEnded ? 'inactive' : ''}`}
                      type="button"
                      onClick={scrollToEnd}
                    >
                      <img loading="lazy" src={icNext} alt="icon" />
                    </button>
                  </div>
                </div>
                <div id={props.id} className={`list-item ${props.classNameList}`} onScroll={onscroll}>
                  {
                    _.map(dropdowns1.value.ctas, d => (
                      <BlockMenuItem1 data={d} className={props.classNameItem} />
                    ))
                  }
                </div>
              </div>
            </React.Fragment>
          )
        }
      </div>

    </div>
  );
}

export default BlockMenu1;
