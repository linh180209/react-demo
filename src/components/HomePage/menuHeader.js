import React, { useState, useEffect } from 'react';
import { Col, Row } from 'reactstrap';
import classnames from 'classnames';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import { gotoHomeAllProduct } from '../../Redux/Helpers';
import LoginMenu from './MenuItem/loginMenu';
import { useMergeState } from '../../Utils/customHooks';
import icCart from '../../image/icon/ic-cart-black.svg';
import icMenuMore from '../../image/icon/ic-menu-more.svg';
import icLogo from '../../image/icon/logo-header.svg';
// import icLogo from '../../image/icon/logo-header.png';
import DrawerMenu from './drawerMenu';
import MenuText from './menuText';
import Region from '../Region';

function Menuheader(props) {
  const [state, setState] = useMergeState({
    isOpenDrawer: false,
    isOpenRegion: false,
  });

  const toggleDrawer = (flags) => {
    setState({ isOpenDrawer: flags });
  };

  const openRegionOut = () => {
    setState({ isOpenRegion: true });
  };

  return (
    <div
      className={classnames('menu-header', (props.visible ? '' : 'bg-on-scroll'), props.className)}
    >
      <DrawerMenu
        buttonBlocks={props.menuButtonBlock}
        menuBlock={props.menuBlock}
        isOpen={state.isOpenDrawer}
        toggleDrawer={toggleDrawer}
        countries={props.countries}
        buttonBlockCard={props.buttonBlocks}
        history={props.history}
        openRegionOut={openRegionOut}
      />
      {
        state.isOpenRegion && <Region countries={props.countries} buttonBlocks={props.buttonBlocks} history={props.history} isBlack isShowPopUpOutSide eventClosePopup={() => setState({ isOpenRegion: false })} />
      }

      <div className="div-icon">
        <button
          onClick={() => toggleDrawer(true)}
          className={props.isShowLoginDrawer ? 'hidden' : 'button-bg__none bt-click'}
          type="button"
        >
          <img loading="lazy" src={icMenuMore} alt="icon" />
        </button>
        <a href={gotoHomeAllProduct(props.history)} className="button-bg__none bt-logo" type="button">
          <img loading="lazy" src={icLogo} alt="icon" />
        </a>
        <div className={props.isShowLoginDrawer ? 'hidden' : 'right-menu'}>
          {
            !props.isHiddenAccount && (
              <LoginMenu buttonBlocks={props.menuButtonBlock} onClickLogin={props.onClickMyAccount} nameDisplay={props.nameDisplay} userId={props.login && props.login.user ? props.login.user.id : ''} />
            )
          }
          <button
            type="button"
            onClick={props.onOpenCard}
            className="button-bg__none"
            style={{ marginRight: '0px' }}
          >
            <div
              className="div-icon-cart"
            >
              <img
                loading="lazy"
                src={icCart}
                style={{ width: '24px' }}
                alt="icCart"
              />
              <div
                className="cart-amount"
              >
                <span>
                  <b>
                    {props.totalBasket}
                  </b>
                </span>
              </div>
            </div>
          </button>
        </div>
      </div>
      <div id="div-text-menu" className={props.isSpecialMenu ? 'hidden' : 'div-text-menu'}>
        <MenuText
          toggleDrawer={toggleDrawer}
          onOpenCard={props.onOpenCard}
          totalBasket={props.totalBasket}
          mergeMenuBlock={props.mergeMenuBlock}
          onClickMyAccount={props.onClickMyAccount}
          nameDisplay={props.nameDisplay}
          userId={props.login && props.login.user ? props.login.user.id : ''}
          isShowLoginDrawer={props.isShowLoginDrawer}
        />
        <div className="line-menu" />
      </div>
    </div>

  );
}

export default Menuheader;
