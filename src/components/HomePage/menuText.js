import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import {
  useLocation,
} from 'react-router-dom';
import classnames from 'classnames';
import { useMergeState } from '../../Utils/customHooks';
import DrawerMenuTop from './drawerMenuTop';
import icMenuMore from '../../image/icon/ic-menu-more.svg';
import LoginMenu from './MenuItem/loginMenu';
import icCart from '../../image/icon/ic-cart-black.svg';
import { isMobile } from '../../DetectScreen';

function MenuText(props) {
  const [state, setState] = useMergeState({
    isOpenMenu: false,
    dataMenu: undefined,
  });
  const location = useLocation();

  const toggleDrawer = (data) => {
    if (data === state.dataMenu) {
      setState({ dataMenu: undefined });
      document.body.style.overflow = '';
    } else {
      setState({ dataMenu: data });
    }
  };

  useEffect(() => {
    setState({ dataMenu: undefined });
    document.body.style.overflow = '';
  }, [location]);

  return (
    <React.Fragment>
      <div className={state.dataMenu && !isMobile ? 'bg-open-menu' : 'hidden'} onClick={() => toggleDrawer()} />
      {
        _.map(props.mergeMenuBlock, (d, index) => (
          <React.Fragment>
            <DrawerMenuTop isOpen={state.dataMenu === d} toggleDrawer={toggleDrawer} dataMenu={d} index={index} />
            {/* <button className={`button-bg__none ${d === state.dataMenu ? 'active' : ''}`} type="button" onClick={() => toggleDrawer(d)}>
              {d.value.header.header_text}
            </button> */}
          </React.Fragment>
        ))
      }
      <div className="menu-text">

        <button
          onClick={() => props.toggleDrawer(true)}
          className={classnames('button-bg__none bt-click', (props.isShowLoginDrawer ? 'active-show' : ''))}
          type="button"
        >
          <img loading="lazy" src={icMenuMore} alt="icon" />
        </button>
        {
        _.map(props.mergeMenuBlock, (d, index) => (
          <React.Fragment>
            {/* <DrawerMenuTop isOpen={state.dataMenu === d} toggleDrawer={toggleDrawer} dataMenu={d} index={index} /> */}
            <button className={`button-bg__none ${d === state.dataMenu ? 'active' : ''}`} type="button" onClick={() => toggleDrawer(d)}>
              <h4 className="header_4">{d.value.header.header_text}</h4>
            </button>
          </React.Fragment>
        ))
      }
        <div className={classnames('right-menu', (props.isShowLoginDrawer ? 'active-show' : ''))}>
          <LoginMenu buttonBlocks={props.menuButtonBlock} onClickLogin={props.onClickMyAccount} nameDisplay={props.nameDisplay} userId={props.login && props.login.user ? props.login.user.id : ''} />
          <button
            type="button"
            onClick={props.onOpenCard}
            className="button-bg__none"
            style={{ marginRight: '0px' }}
          >
            <div
              className="div-icon-cart"
            >
              <img
                loading="lazy"
                src={icCart}
                style={{ width: '24px' }}
                alt="icCart"
              />
              <div
                className="cart-amount"
              >
                <span>
                  <b>
                    {props.totalBasket}
                  </b>
                </span>
              </div>
            </div>
          </button>
        </div>
      </div>
    </React.Fragment>

  );
}

export default MenuText;
