import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { convertHeaderToSize } from '../../Redux/Helpers';
import Image from './Image';
import { isMobile, isTablet } from '../../DetectScreen';

class Influencer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowAnimation: false,
    };
  }

  componentDidMount() {
    document.addEventListener('scroll', this.trackScrolling);
  }

  componentWillUnmount() {
    document.removeEventListener('scroll', this.trackScrolling);
  }

  trackScrolling = () => {
    const wrappedElement = document.getElementById('image');
    if (this.isBottom(wrappedElement)) {
      this.setState({ isShowAnimation: true });
      document.removeEventListener('scroll', this.trackScrolling);
    }
  };

  isBottom = el => el.getBoundingClientRect().bottom <= window.innerHeight

  render() {
    const { isShowAnimation } = this.state;
    const { infuencerCms } = this.props;
    if (!infuencerCms) {
      return (<div />);
    }
    const {
      buttons, child_sections: childSections, header_text: headerText, header_size: headerSize,
    } = infuencerCms;
    const styleHeader = {
      fontSize: headerSize ? convertHeaderToSize(headerSize) : undefined,
    };
    const button = buttons ? buttons[0] : undefined;
    const blocks = childSections ? childSections[0].value : undefined;
    const textBlock = _.find(blocks, x => x.type === 'text_block');
    const userBlocks = _.filter(blocks, x => x.type === 'user_block');
    const userDatas = [];
    _.forEach(userBlocks, (x) => {
      const { value } = x;
      if (value) {
        userDatas.push({
          url: value.avatar ? value.avatar : '',
          name: `${!_.isEmpty(value.first_name) ? value.first_name : ''} ${!_.isEmpty(value.last_name) ? value.last_name : ''}`,
        });
      }
    });
    return (

      <div className={isMobile ? 'div-influencer-mobile' : 'div-influencer'}>
        <span className="text-title" style={styleHeader}>
          {headerText}
        </span>
        <span className="text pt-5 pb-5">
          {textBlock.value}
        </span>
        <div className="div_img">
          <div className="div-left">
            <Image
              id="image"
              url={userDatas.length > 0 ? userDatas[0].url : ''}
              ratio="4:3"
              name={userDatas.length > 0 ? userDatas[0].name : ''}
              isAnimation={isShowAnimation}
            />
            <Image
              url={userDatas.length > 1 ? userDatas[1].url : ''}
              ratio="530:580"
              name={userDatas.length > 1 ? userDatas[1].name : ''}
              isAnimation={isShowAnimation}
            />
          </div>
          <div className="div-mid">
            <Image
              url={userDatas.length > 2 ? userDatas[2].url : ''}
              ratio="530:580"
              name={userDatas.length > 2 ? userDatas[2].name : ''}
              isAnimation={isShowAnimation}
            />
            <Image
              url={userDatas.length > 3 ? userDatas[3].url : ''}
              ratio="4:3"
              name={userDatas.length > 3 ? userDatas[3].name : ''}
              isAnimation={isShowAnimation}
            />

          </div>
          {
            isMobile && !isTablet ? (<div />) : (
              <div className="div-right">
                <Image
                  url={userDatas.length > 4 ? userDatas[4].url : ''}
                  ratio="4:3"
                  name={userDatas.length > 4 ? userDatas[4].name : ''}
                  isAnimation={isShowAnimation}
                />
                <Image
                  url={userDatas.length > 5 ? userDatas[5].url : ''}
                  ratio="530:580"
                  name={userDatas.length > 5 ? userDatas[5].name : ''}
                  isAnimation={isShowAnimation}
                />
              </div>
            )
          }

        </div>
        <a
          href={button.value.link}
          className="button_homepage"
        >
          {button.value.text}
        </a>
        {/* <div className="div-line mt-5 mb-5" /> */}
      </div>
    );
  }
}

Influencer.propTypes = {
  infuencerCms: PropTypes.shape({
    buttons: PropTypes.arrayOf(PropTypes.object),
    child_sections: PropTypes.arrayOf(PropTypes.object),
    header_text: PropTypes.string,
  }).isRequired,
};

export default Influencer;
