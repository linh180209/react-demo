/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useState, useEffect } from 'react';
import {
  Modal, ModalBody, Row, Col,
} from 'reactstrap';
import _ from 'lodash';
import classnames from 'classnames';
import { useHistory } from 'react-router-dom';
import ClearIcon from '@mui/icons-material/Clear';
import icClose from '../../../image/icon/ic-close-black.svg';
import './styles.scss';
import {
  addFontCustom,
  generaCurrency, generateUrlWeb, getAltImage, getCmsCommon, getNameFromButtonBlock, getNameFromCommon, getUrlGotoProductOderPage,
} from '../../../Redux/Helpers';
import BottleCustom from '../../B2B/CardItem/bottleCustom';
import bottleBundleCreation from '../../../image/bundle_creation_thumnail.jpeg';
import bottleHomeScent from '../../../image/bottle_home_card.png';
import rollOnBottle from '../../../image/roll_on_bottle.png';
import { isMobile } from '../../../DetectScreen';
import { GET_RELATED_CART } from '../../../config';
import fetchClient from '../../../Redux/Helpers/fetch-client';
import { toastrError } from '../../../Redux/Helpers/notification';
import ButtonCT from '../../../componentsv2/buttonCT';
import { GIFT_BUNDLES, IS_GIFTING } from '../../../views2/GiftV2/constants';

function PopupAddToCart(props) {
  const history = useHistory();
  const [relatedProduct, setRelatedProduct] = useState([]);

  const fetchDataRelated = () => {
    const option = {
      url: GET_RELATED_CART.replace('{id}', props.data?.item?.id),
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      console.log('result', result);
      if (result && !result.isError) {
        setRelatedProduct(result);
      } else {
        throw new Error(result.message);
      }
    }).catch((error) => {
      toastrError(error.message);
    });
  };

  const gotoProduct = (data) => {
    const { url } = getUrlGotoProductOderPage({ item: data });
    if (url) {
      props.onClose();
      history.push(generateUrlWeb(url));
    }
  };

  useEffect(() => {
    if (props.isOpen) {
      fetchDataRelated();
    }
  }, [props.isOpen]);

  const {
    item, combo, image,
    image_display: imgDisplay, name, is_customized: isCustomized,
    is_display_name: isDisplayName, is_black: isBlack, font, color,
    subtotal, quantity, meta,
  } = props.data || {};

  const isGift = item?.product?.type?.name === 'Gift' || IS_GIFTING.includes(meta?.typeOfGift);
  const isShowBottle = ((item?.product?.type?.name === 'Creation' && isCustomized) || item?.product?.type?.name === 'Perfume' || item?.product?.type?.name === 'bundle_creation' || item?.product?.type?.name === 'perfume_diy' || item?.product?.type?.name === 'Elixir' || (isGift && isCustomized)) && item?.is_sample === false;
  const isBundleCreation = item?.product?.type?.name === 'bundle_creation';
  const isHomeScent = item?.product?.type?.name === 'home_scents' && combo && combo.length > 0;
  const isRollOn = (item?.product?.type?.name === 'Perfume' || item?.product?.type?.name === 'Scent') && item?.is_sample === true;
  const combos = combo ? _.filter(combo, x => x?.product?.type === 'Scent') : [];

  const cmsCommon = getCmsCommon(props.cms);
  const eauBt = getNameFromCommon(cmsCommon, 'Eau_de_parfum');
  const mlBt = getNameFromCommon(cmsCommon, '25ml');
  const addToCardBt = getNameFromButtonBlock(props.buttonBlocks, 'ADDED TO CART');
  const qtyBt = getNameFromButtonBlock(props.buttonBlocks, 'Qty');
  const youMightBt = getNameFromButtonBlock(props.buttonBlocks, 'You might also like');
  const continueBt = getNameFromButtonBlock(props.buttonBlocks, 'CONTINUE SHOPPING');
  const viewShoppingBt = getNameFromButtonBlock(props.buttonBlocks, 'VIEW SHOPPING BAG');
  const thisBt = getNameFromButtonBlock(props.buttonBlocks, 'this product is frequently bought with');
  const isImageText = !!image;

  return (
    <Modal className={classnames('popup-add-to-cart', addFontCustom())} isOpen={props.isOpen} centered>

      <ModalBody>
        <div className="div-body-add-cart">

          <div className="header-title">
            <button className="bt-close button-bg__none" type="button" onClick={props.onClose}>
              <ClearIcon style={{ color: '#2C2C22' }} />
            </button>
            <h2>{addToCardBt}</h2>
          </div>

          <div className="info-item">
            <div className="div-image">
              {
              isBundleCreation ? (
                <div style={{ position: 'relative', display: 'flex', justifyContent: 'center' }}>
                  <img
                    loading="lazy"
                    src={bottleBundleCreation}
                    alt={getAltImage(imgDisplay)}
                    style={{
                      width: isMobile ? '100%' : '80%',
                      cursor: 'pointer',
                      objectFit: 'contain',
                      height: isMobile ? '100px' : '90px',
                    }}
                    // onClick={() => this.onGotoProduct(false)}
                  />
                </div>
              ) : (isShowBottle || isHomeScent) ? (
                <BottleCustom
                  isImageText={isImageText}
                  // onGotoProduct={() => this.onGotoProduct(false)}
                  image={image}
                  isBlack={isBlack}
                  font={font}
                  color={color}
                  eauBt={eauBt}
                  mlBt={mlBt}
                  isDisplayName={isDisplayName}
                  name={name}
                  combos={combos}
                  bottleImage={isHomeScent ? bottleHomeScent : undefined}
                />
              ) : isRollOn ? (
                <React.Fragment>
                  <div style={{ position: 'relative', display: 'flex', justifyContent: 'center' }}>
                    <img
                      loading="lazy"
                      src={rollOnBottle}
                      alt={getAltImage(imgDisplay)}
                      style={{
                        width: isMobile ? '100%' : '80%',
                        cursor: 'pointer',
                        objectFit: 'contain',
                        height: isMobile ? '100px' : '90px',
                      }}
                      // onClick={() => this.onGotoProduct(false)}
                    />
                  </div>
                </React.Fragment>
              ) : (
                <img
                  loading="lazy"
                  src={imgDisplay}
                  alt={getAltImage(imgDisplay)}
                  style={{
                    // width: '80%',
                    cursor: 'pointer',
                  }}
                  // onClick={() => this.onGotoProduct(false)}
                />
              )
            }
            </div>
            <div className="div-name">
              <h3>{meta?.typeOfGift && meta.typeOfGift !== GIFT_BUNDLES ? meta?.giftName : item?.product?.type?.alt_name }</h3>
              <span>{name}</span>
              <div className="info-detail">
                <span>
                  {qtyBt}
                  :
                  {' '}
                  {quantity}
                </span>
                <span>
                  {generaCurrency(subtotal)}
                </span>
              </div>
              <ButtonCT
                name={viewShoppingBt}
                variant="outlined"
                onClick={props.viewShoppingBag}
                color="secondary"
              />
            </div>
          </div>
          {
            relatedProduct?.length > 0 && (
              <React.Fragment>
                <div className="line-row" />
                <div className="list-related">
                  <div className="title-list">
                    {thisBt}
                  </div>
                  <Row className="list-item">
                    {
                      _.map(relatedProduct || [], d => (
                        <Col md="3" xs="6" className="item-related" onClick={() => gotoProduct(d)}>
                          <img src={d?.image} alt="product" />
                          <div className="name">
                            {d.name}
                          </div>
                          <div className="price">
                            {generaCurrency(d?.price)}
                          </div>
                        </Col>
                      ))
                    }
                  </Row>
                </div>
              </React.Fragment>
            )
          }
        </div>
      </ModalBody>
    </Modal>
  );
}

export default PopupAddToCart;
