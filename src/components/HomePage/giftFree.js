import React, { useState, useCallback } from 'react';
import { Collapse, Progress } from 'reactstrap';
import _ from 'lodash';
import classnames from 'classnames';
import '../../styles/gift-free.scss';

import icCloseWeb from '../../image/icon/ic-next-mix.svg';
import icCheck from '../../image/icon/ic-check-bg-green.svg';
import icRoll from '../../image/bottle_roll-on.png';
import icInfo from '../../image/icon/ic-info-i.svg';
import icPackage from '../../image/icon/ic-package.svg';
import { isBrowser } from '../../DetectScreen';
import arrowDown from '../../image/icon/arrow-down-filter.svg';
import arrowUp from '../../image/icon/arrow-up-filter.svg';
import { useMergeState } from '../../Utils/customHooks';
import IngredientPopUp from '../IngredientDetail/ingredientPopUp';
import { generaCurrency, getNameFromButtonBlock } from '../../Redux/Helpers';

function ItemGift(props) {
  const nameIngredients = _.map(props.data.product.combo || [], x => x.name);

  const ml5Bt = getNameFromButtonBlock(props.buttonBlocks, '5ml');
  const familyBt = getNameFromButtonBlock(props.buttonBlocks, 'Family');
  const ingredientsBt = getNameFromButtonBlock(props.buttonBlocks, 'Ingredients');
  return (
    <div
      className={`item-gift div-row ${props.selected ? 'selected' : ''} ${props.disabled ? 'disabled' : ''}`}
      onClick={() => (!props.disabled && props.onClickAddGift(props.data))}
    >
      <div className="div-image div-col">
        {props.isShow && (
          <img loading="lazy" src={props.data.image} alt="icon" />
        )}
      </div>
      <div className="div-text-right div-col justify-center">
        <div className="header-h3">
          {props.data.name}
        </div>
        {/* <div className="div-info div-row mt-2">
          <div className="div-text div-col">
            <span>
              {familyBt}
              :
              {' '}
              {props.data.product.family}
            </span>
            <span>
              {ingredientsBt}
              :
              {' '}
              {_.join(nameIngredients, ' + ')}
            </span>
          </div>
          <button type="button" className="button-bg__none">
            <img src={icInfo} alt="icon" />
          </button>
        </div> */}
      </div>
    </div>
  );
}
function GiftFree(props) {
  const [collapse, setCollapse] = useState(isBrowser && !props.isCollapse);
  const [state, setState] = useMergeState({
    isShowIngredient: false,
    ingredientDetail: undefined,
  });

  const toggleCollapse = useCallback(() => {
    setCollapse(!collapse);
  }, [collapse]);

  const onCloseIngredient = () => {
    setState({ isShowIngredient: false });
  };

  const onClickIngredient = (productId) => {
    setState({ ingredientDetail: { productId } });
  };

  const spendBt = getNameFromButtonBlock(props.buttonBlocks, 'Spend');
  const orMoreBt = getNameFromButtonBlock(props.buttonBlocks, 'or_more_to_qualify');
  const togoBt = getNameFromButtonBlock(props.buttonBlocks, 'to_go');
  const qualifiedBt = getNameFromButtonBlock(props.buttonBlocks, 'Qualified');
  const freeGiftBt = getNameFromButtonBlock(props.buttonBlocks, 'Free_gifts_selected');
  const overBt = getNameFromButtonBlock(props.buttonBlocks, 'Over');
  const spentBt = getNameFromButtonBlock(props.buttonBlocks, 'Spent');
  const yourFreeBt = getNameFromButtonBlock(props.buttonBlocks, 'your_free_gifts');
  const freeSameBt = getNameFromButtonBlock(props.buttonBlocks, 'Free_samples_when_you_spend');
  const whileStockBt = getNameFromButtonBlock(props.buttonBlocks, 'While_stocks_last');

  const headerBuyMore = (amount, percent) => (
    <React.Fragment>
      <span>
        {spendBt}
        {' '}
        {generaCurrency(amount)}
        {' '}
        {orMoreBt}
      </span>
      {
        !props.isCollapse && (
          <React.Fragment>
            <br />
            <span className="mt-1">
              <b>
                {generaCurrency(amount - props.price)}
                {' '}
                {togoBt}
                {' '}
              </b>
            </span>
          </React.Fragment>
        )
      }
      <div className="progress-packaging div-row">
        <Progress value={percent} />
        <img loading="lazy" src={icPackage} alt="icon" />
      </div>
      {
        props.isCollapse && (
          <span className="mt-1">
            <b>
              {generaCurrency(amount - props.price)}
              {' '}
              {togoBt}
              {' '}
            </b>
          </span>
        )
      }
    </React.Fragment>
  );

  const headerHasGiftMobile = totalGift => (
    <div className="div-row justify-between items-center">
      <div className="div-check div-row mt-0 mb-0">
        <img loading="lazy" src={icCheck} alt="check" />
        <span className="text-green">
          {qualifiedBt}
        </span>
      </div>
      <span className="mt-1">
        <b>
          {props.giftFreeProduct.length}
          /
          {totalGift}
          {' '}
          {freeGiftBt}
        </b>
      </span>
    </div>
  );
  const headerHasGift = (amount, type, total) => (
    <React.Fragment>
      <div className="div-check div-row mt-0 mb-0">
        <img loading="lazy" src={icCheck} alt="check" />
        <span className="text-green">
          {qualifiedBt}
          {' '}
          (
          {overBt}
          {' '}
          {generaCurrency(amount)}
          {' '}
          {spentBt}
          )
        </span>
      </div>
      <span className="mt-1">
        <b>
          {/* {_.filter(props.giftFreeProduct, x => x.item.product.type.name === type).length} */}
          {total}
          /1
          {' '}
          {freeGiftBt}
        </b>
      </span>
    </React.Fragment>
  );

  const onClickAddGift = (gift) => {
    const dataAdd = {
      idCart: props.basket.id,
      item: {
        item: gift.id,
        quantity: 1,
        is_free_gift: true,
        name: gift.name,
      },
    };
    const data = [dataAdd];
    const eleDetele = _.find(props.giftFreeProduct, x => x.item.product.type.name === gift.product.type);
    if (eleDetele) {
      data.push({
        idCart: props.basket.id,
        item: {
          item: eleDetele.id,
        },
      });
    }
    props.addProductGiftBasket(data);
  };
  const getNumberGift = (giftFreeProduct, items) => _.reduce(giftFreeProduct, (sum, n) => {
    if (_.find(items, item => n.item.id === item.id)) {
      return sum + 1;
    }
    return sum;
  }, 0);
  // console.log('giftFrees', props.giftFrees);
  return (
    <React.Fragment>
      <IngredientPopUp
        isShow={state.isShowIngredient}
        data={state.ingredientDetail || {}}
        onCloseIngredient={onCloseIngredient}
        history={props.history}
        className="min-top-height"
      />
      <div className={classnames('gift-free div-col', props.className)}>
        <div className="header div-row">
          <div className="header-h2">
            {yourFreeBt}
          </div>
          <button type="button" className="button-bg__none" onClick={props.isCollapse ? toggleCollapse : props.onClickClose}>
            <img loading="lazy" src={props.isCollapse ? (collapse ? arrowUp : arrowDown) : icCloseWeb} alt="icon" />
          </button>
        </div>
        {
          (!props.isCollapse || (props.isCollapse && collapse)) && (
            <React.Fragment>
              <span>
                {freeSameBt}
                {' '}
                {generaCurrency(props.amount)}
                !
              </span>
              <span className="mb-40">
                (
                {whileStockBt}
                )
              </span>
            </React.Fragment>
          )
        }
        {
          props.isCollapse && !collapse && props.giftFrees && props.giftFrees.length > 0 && (props.giftFrees[0].amount <= props.price ? headerHasGiftMobile(props.giftFrees.length) : headerBuyMore(props.giftFrees[0].amount, (props.price * 100 / props.giftFrees[0].amount)))
        }
        <Collapse
          isOpen={collapse}
        >
          {
          _.map(_.orderBy(props.giftFrees, ['amount'], 'asc'), d => (
            <React.Fragment>
              {
                d.amount <= props.price
                  ? headerHasGift(d.amount, d.items[0].product.type, getNumberGift(props.giftFreeProduct, d.items))
                  : headerBuyMore(d.amount, (props.price * 100 / d.amount))
              }
              <div className="list-gift div-col">
                {
                  _.map(d.items, item => (
                    <ItemGift
                      isShow={props.isShow}
                      data={item}
                      onClickIngredient={onClickIngredient}
                      onClickAddGift={onClickAddGift}
                      selected={_.find(props.giftFreeProduct, x => x.item.id === item.id)}
                      disabled={d.amount > props.price}
                      buttonBlocks={props.buttonBlocks}
                    />
                  ))
                }
              </div>
            </React.Fragment>
          ))
        }
        </Collapse>
      </div>
    </React.Fragment>

  );
}

export default GiftFree;
