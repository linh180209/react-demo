import React, { Component } from 'react';
import BlockItem from './blockItem';

class BlockPage extends Component {
  render() {
    return (
      <div className="div-col">
        <BlockItem isRight />
      </div>
    );
  }
}

export default BlockPage;
