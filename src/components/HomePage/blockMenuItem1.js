import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import ReactHtmlParser from 'react-html-parser';
import '../../styles/block-menu-item-1.scss';
import icNext from '../../image/icon/ic-next-white.svg';
import { generateUrlWeb, getAltImageV2 } from '../../Redux/Helpers';
import { isMobile } from '../../DetectScreen';

function BlockMenuItem1(props) {
  return (
    <div className="block-menu-item-1">
      {
        isMobile ? (
          <Link className={`mobile-item ${props.classNameMobile}`} to={generateUrlWeb(props.data.value.link)}>
            <img loading="lazy" src={props.data.value.image.image} alt={getAltImageV2(props.data.value.image)} />
            {
              (!props.description && !props.isAbout) && (
                <div className="text">
                  {props.data.value.text}
                </div>
              )
            }
            {
              props.description && (
                <div className="club21g-des">
                  <div className="header-h3">
                    {props.header}
                  </div>
                  <div className="des">
                    {ReactHtmlParser(props.description)}
                  </div>
                </div>
              )
            }
            {
              props.isAbout && (
                <div className="about-des">
                  <div className="header-h3">
                    {props.data.value.text}
                  </div>
                  <div className="des">
                    {ReactHtmlParser(props.data.value.description)}
                  </div>
                </div>
              )
            }

          </Link>
        ) : (
          <React.Fragment>
            <Link className={`div-image ${props.className}`} to={generateUrlWeb(props.data.value.link)}>
              <img loading="lazy" src={props.data.value.image.image} alt={getAltImageV2(props.data.value.image)} />
              <div className="hover-text">
                <span>
                  {props.data.value.image.caption}
                </span>
                <img loading="lazy" src={icNext} alt="icon" />
              </div>
            </Link>
            {
              props.isBlog && (
                <div className="div-time">
                  <div className="header-h4">
                    {props.data.value.categories}
                  </div>
                  <span>
                    {props.data.value.time}
                    {/* {props.data.value.time.replace(/[^0-9]/g, '')}
                    {' '}
                    {props.data.value.time.replace(/[0-9]/g, '')} */}
                  </span>
                </div>
              )
            }
            {
              props.data.value.text && (
                <div className="header-h3">
                  {props.data.value.text}
                </div>
              )
            }
            <div className="des">
              {ReactHtmlParser(props.data.value.description)}
            </div>
          </React.Fragment>
        )
      }
    </div>
  );
}

export default BlockMenuItem1;
