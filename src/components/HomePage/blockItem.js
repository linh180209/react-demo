import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { generateUrlWeb, getAltImage } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

class BlockItem extends Component {
  state = {
    button: undefined,
    headerText: undefined,
    textBlocks: undefined,
    imageUrl: '',
  }

  componentDidMount() {
    const { cms } = this.props;
    const {
      buttons, child_sections: childSections, header_text: headerText, image_url: imageUrl,
    } = cms;
    const button = buttons ? buttons[0] : undefined;
    const blocks = childSections ? childSections[0].value : undefined;
    const textBlocks = _.filter(blocks, x => x.type === 'text_block');
    this.setState({
      button, headerText, textBlocks, imageUrl,
    });
  }

  render() {
    const { history, isRight, isCenter } = this.props;
    const {
      button, headerText, textBlocks, imageUrl,
    } = this.state;
    const textHtml = (
      <div
        className="div-col justify-center items-center w-50"
      >
        <div className="div-col justify-center items-center w-80">
          <span className="text-title">
            {headerText}
          </span>
          <span style={{
            // color: '#A0A0A0',
            marginTop: '30px',
            marginBottom: '30px',
            textAlign: 'center',
          }}
          >
            {textBlocks ? textBlocks[0].value : ''}
          </span>
          <button
            type="button"
            className="button_home_page"
            onClick={() => this.props.history.push(generateUrlWeb(button.value.link))}
          >
            {button ? button.value.text : ''}
          </button>
        </div>
      </div>
    );

    const imageHtml = (
      <div className="w-50">
        <img loading="lazy" style={{ width: '100%' }} src={imageUrl} alt={getAltImage(imageUrl)} />
      </div>
    );
    const mobileHtml = (
      <div className="div-col justify-center items-center">
        <span className="text-title tc">
          {headerText}
        </span>
        <span style={{
          // color: '#A0A0A0',
          marginTop: '30px',
          textAlign: 'center',
          marginLeft: '10px',
          marginRight: '10px',
        }}
        >
          {textBlocks ? textBlocks[0].value : ''}
        </span>
        <img loading="lazy" style={{ width: '100%' }} src={imageUrl} alt={getAltImage(imageUrl)} />
        <button
          type="button"
          className="button_home_page mb-5"
          onClick={() => this.props.history.push(generateUrlWeb(button.value.link))}
        >
          {button ? button.value.text : ''}
        </button>
      </div>
    );

    const centerBlock = (
      <div className="div-col justify-center items-center">
        <div
          className="div-col justify-center items-center"
          style={{
            width: '40%',
            height: '200px',
            position: 'relative',
          }}
        >
          <img
            loading="lazy"
            style={{
              position: 'absolute',
              width: '100%',
              objectFit: 'cover',
              objectPosition: 'center',
            }}
            src={imageUrl}
            alt={getAltImage(imageUrl)}
          />
          <span className="text-title tc">
            {headerText}
          </span>
        </div>

        <span style={{
          // color: '#A0A0A0',
          marginTop: '70px',
          textAlign: 'center',
          width: '60%',
        }}
        >
          {textBlocks ? textBlocks[0].value : ''}
        </span>
        <button
          type="button"
          className="button_home_page mb-5 mt-5"
          onClick={() => this.props.history.push(generateUrlWeb(button.value.link))}
        >
          {button ? button.value.text : ''}
        </button>
      </div>
    );

    if (isMobile && !isTablet) {
      return (
        <div>
          { mobileHtml }
        </div>
      );
    }

    if (isCenter) {
      return (
        <div>
          {centerBlock}
        </div>
      );
    }
    return (
      <div>
        {
          isRight ? (
            <div className="div-row justify-center items-center">
              {textHtml}
              {imageHtml}
            </div>
          ) : (
            <div className="div-row justify-center items-center">
              {imageHtml}
              {textHtml}
            </div>
          )
        }
      </div>
    );
  }
}

BlockItem.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
  isRight: PropTypes.bool.isRequired,
  isCenter: PropTypes.bool.isRequired,
  cms: PropTypes.shape(PropTypes.object).isRequired,
};

export default withRouter(BlockItem);
