import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import windowSize from 'react-window-size';

import { generateUrlWeb, getAltImage } from '../../Redux/Helpers/index';
import imgTitle from '../../image/title-info.png';
import ScentDesignItem from '../ScentDesignItem';
import { isMobile } from '../../DetectScreen';

class ScentDesign extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isShowMake: true,
    };
    this.dataButton = [
      {
        id: 0,
        title: 'Let us guide',
        description: 'Take the quiz and find your',
        button: 'START QUIZ',
        width: '170px',
        height: '46px',
      },
      {
        id: 1,
        title: 'Based on your mood',
        description: 'Let us guide you base on',
        button: 'MY OCCASSION',
        width: '200px',
        height: '46px',
      },
      {
        id: 2,
        title: 'Make your own',
        description: 'Create your own',
        button: 'MAKE MY OWN',
        width: '200px',
        height: '46px',
      },
    ];
  }

  componentDidMount = () => {
    document.addEventListener('scroll', this.trackScrolling);
  };

  componentWillUnmount = () => {
    document.removeEventListener('scroll', this.trackScrolling);
  }


  onClickButton = (link) => {
    const { history } = this.props;
    history.push(generateUrlWeb(link));
    // switch (id) {
    //   case 0:
    //     {
    //       const { login } = this.props;
    //       const url = getUrlScentDesigner(login);
    //       this.props.history.push(url);
    //     }
    //     break;
    //   case 0:
    //     {
    //       const { history } = this.props;
    //       history.push('/mood');
    //     }
    //     break;
    //   case 1:
    //     {
    //       const { history } = this.props;
    //       history.push('/mmo');
    //     }
    //     break;
    //   default:
    // }
  }

  onClickStart = () => {
    document.getElementById('scentDesign').scrollIntoView({ behavior: 'smooth' });
  }

  trackScrolling = () => {
    const wrappedElement = document.getElementById('id-three');
    if (this.isBottom(wrappedElement)) {
      this.setState({ isShowMake: true });
    }
  };

  isBottom = el => el.getBoundingClientRect().y < window.innerHeight

  render() {
    const { scentDesignCms } = this.props;
    const { isShowMake } = this.state;
    if (!scentDesignCms) {
      return (<div />);
    }
    const {
      image_url: imageUrl, child_sections: childSections,
    } = scentDesignCms;
    const dataButton = [];
    if (childSections) {
      _.forEach(childSections, (d, index) => {
        const { value } = d;
        const title = _.find(value, e => e.type === 'header_block');
        const description = _.find(value, e => e.type === 'text_block');
        const button = _.find(value, e => e.type === 'button_block');
        const imageBl = _.find(value, e => e.type === 'image_block');
        const element = {
          id: index,
          title: title ? title.value.header_text : '',
          description: description ? description.value : '',
          buttonName: button ? button.value.text : '',
          buttonLink: button ? button.value.link : '',
          image: imageBl ? imageBl.value.image : '',
        };
        dataButton.push(element);
      });
    }
    return (
      <div
        id="id-three"
        className="div-startPage height_home_page"
        style={{
          height: isMobile ? `calc(${dataButton.length * 100}vh - ${dataButton.length * 70}px)` : 'calc(100vh - 70px)',
        }}
      >
        <div className="div-bg" style={{ height: '100%' }}>
          {imageUrl
            ? <img loading="lazy" style={{ objectFit: 'cover', width: '100%' }} className={isShowMake ? 'animation_show height_home_page' : 'hiden'} src={imageUrl} alt={getAltImage(imageUrl)} />
            : (<div />)
          }
        </div>
        <div className="div-comingsoon">
          <div className="div-make" style={{ justifyContent: 'center' }}>
            <div className="hiden">
              <img loading="lazy" src={imgTitle} alt="imgTitle" />
              <div className="line-make" />
            </div>
            <div
              className="w-90 h-100"
              style={{
                marginLeft: '5%',
                marginRight: '5%',
              }}
            >
              <Row
                className="w-100"
              >
                {
                  _.map(dataButton, d => (
                    <Col
                      md={isMobile ? '12' : '6'}
                      xs="12"
                      className="height_home_page"
                      style={{
                        position: 'relative',
                        margin: '0px',
                      }}
                    >
                      <ScentDesignItem
                        data={d}
                        onClickButton={this.onClickButton}
                      />
                    </Col>
                  ))
                }
              </Row>
            </div>
            {/* <div className={isMobile ? 'content-make-mobile' : 'content-make'} style={{ flex: 'none' }}>
              {
              _.map(dataButton, d => (
                <div className={isMobile ? 'item mt-4 mb-4' : 'item mt-3'}>
                  <span style={{ textAlign: 'center' }}>
                    {d.title}
                  </span>
                  <span style={{ textAlign: 'center' }}>
                    {d.description}
                  </span>
                  <button
                    type="button"
                    className="mt-3"
                    onClick={() => this.onClickButton(d.id)}
                    style={{
                      width: d.width,
                      height: d.height,
                      backgroundSize: `${d.width} ${d.height}`,
                    }}
                  >
                    {d.button}
                  </button>
                </div>
              ))
            }
            </div> */}
          </div>

        </div>
      </div>
    );
  }
}

ScentDesign.propTypes = {
  scentDesignCms: PropTypes.shape({
    header_size: PropTypes.string,
    header_text: PropTypes.string,
    video_url: PropTypes.string,
    buttons: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  login: PropTypes.shape({
    token: PropTypes.string,
    user: PropTypes.shape({
      outcome: PropTypes.object,
    }),
  }).isRequired,
  history: PropTypes.shape({
    push: PropTypes.func,
  }).isRequired,
};

function mapStateToProps(state) {
  return {
    login: state.login,
  };
}

export default withRouter(connect(mapStateToProps, null)(windowSize(ScentDesign)));
