import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import RecommendItem from '../Products/recommendItem';
import './styles.scss';

function QnABlock(props) {
  return (
    <div className="div-faq-block">
      <div className="div-content-faq">
        {props.data?.header?.header_text && <h3>{props.data?.header?.header_text}</h3>}
        <div className="line-top" />
        {
          _.map(props.data?.faq || [], d => (
            <RecommendItem title={d.value.header.header_text} description={d.value.paragraph} />
          ))
        }
      </div>
    </div>
  );
}

export default QnABlock;
