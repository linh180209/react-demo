import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getAltImage } from '../../Redux/Helpers';
import { isMobile, isTablet } from '../../DetectScreen';

class ScentDesignItem extends Component {
  render() {
    const {
      title, image, description, buttonName, buttonLink,
    } = this.props.data;
    const isPhone = isMobile && !isTablet;
    return (
      <div
        className="w-100 relative"
        style={{ height: '100%' }}
      >
        <img
          loading="lazy"
          className="absolute w-100 h-100"
          style={{
            objectFit: 'cover',
            objectPosition: 'center',
            zIndex: '-1',
          }}
          src={image}
          alt={getAltImage(image)}
        />
        <div
          className="div-col justify-center items-center"
          style={{
            width: '100%',
            textAlign: 'center',
            marginTop: '10%',
          }}
        >
          <span
            className="text-title"
            style={{
              // fontSize: isPhone ? '1.4rem' : '1.8rem',
              width: '100%',
              textAlign: 'center',
            }}
          >
            {title}
          </span>
          <span style={{
            width: '80%',
            textAlign: 'center',
            marginBottom: '15px',
            fontSize: isPhone ? '0.9rem' : '1.2rem',
            // color: '#3B3B3B',
          }}
          >
            {description}
          </span>
        </div>
        <div
          className="div-col justify-center items-center"
          style={{
            width: '100%',
            height: 'auto',
            position: 'absolute',
            bottom: '20%',
            left: '0px',
          }}
        >
          <button
            style={{
              // fontSize: isPhone ? '0.8rem' : '1rem',
              // width: isPhone ? '160px' : '180px',
            }}
            type="button"
            className="button_home_page"
            onClick={() => { this.props.onClickButton(buttonLink); }}
          >
            {buttonName}

          </button>
        </div>
      </div>
    );
  }
}

ScentDesignItem.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    image: PropTypes.string,
    description: PropTypes.string,
    buttonName: PropTypes.string,
    buttonLink: PropTypes.string,
  }).isRequired,
  onClickButton: PropTypes.func.isRequired,
};

export default ScentDesignItem;
