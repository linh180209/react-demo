import React, { Component } from 'react';
import PropTypes from 'prop-types';
import arrow from '../../image/arrow-r.svg';

class ButtonNext extends Component {
  componentDidMount() {
    this.focusInput();
  }

  componentWillUnmount() {
    this.cancelFocus();
  }

  onKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.onNext();
    }
  }

  onNext = () => {
    this.props.onNext();
  }

  focusInput = () => {
    document.addEventListener('keydown', this.onKeyPress, true);
  }

  cancelFocus = () => {
    document.removeEventListener('keydown', this.onKeyPress, true);
  }

  render() {
    const { name } = this.props;
    return (
      <div
        className="text__button--div"
      >
        <button
          className="text__button--bt"
          type="button"
          onClick={this.onNext}
        >
          <div>
            <span>
              {name}
            </span>
            <img loading="lazy" className="text__button--check" src={arrow} alt="check" />
          </div>
        </button>
      </div>
    );
  }
}

ButtonNext.propTypes = {
  onNext: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
};

export default ButtonNext;
