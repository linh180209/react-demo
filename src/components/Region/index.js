/* eslint-disable react/sort-comp */
import React, { Component } from 'react';
import {
  Modal, ModalBody,
} from 'reactstrap';
import classnames from 'classnames';
import PublicIcon from '@mui/icons-material/Public';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import StoreIcon from '@mui/icons-material/Store';
import '../../styles/region.scss';
import Select from 'react-select';
import _ from 'lodash';
import icClose from '../../image/icon/ic-close-region.svg';
import icDown from '../../image/icon/ic-down.svg';
import auth from '../../Redux/Helpers/auth';
import icLocation from '../../image/icon/ic-location.svg';
import icLocationBlack from '../../image/icon/ic-local-black.svg';
import icLocationBlue from '../../image/icon/ic-location-blue.svg';
import { getNameFromButtonBlock, addFontCustom } from '../../Redux/Helpers';
import { isMobile, isLargerScreen } from '../../DetectScreen';
import ButtonCT from '../../componentsv2/buttonCT';

const formatCountry = (listCountry) => {
  const countries = [];
  _.forEach(listCountry, (x) => {
    countries.push(
      {
        label: x.name,
        value: x.code,
      },
    );
  });
  return countries;
};

class Region extends Component {
  state = {
    dropdownOpen: false,
    isShowPopUp: this.props.isShowPopUpOutSide,
    listCountry: formatCountry(this.props.countries) || [],
    nSymbol: _.find(formatCountry(this.props.countries) || [], x => x.value === auth.getCountry()),
  }

  componentDidMount() {
    // this.fetchData();
  }

  changeUrl = (symbol) => {
    let newUrl = window.location.pathname;
    const array = window.location.pathname.split('/');
    if (array && array.length > 1 && array[1].length === 2) {
      newUrl = newUrl.replace(array[1], symbol);
    } else {
      newUrl = `/${symbol}${window.location.pathname}`;
    }
    if (window.location.search) {
      newUrl = `${newUrl}${window.location.search}`;
    }
    this.props.history.push(newUrl);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { countries } = nextProps;
    if (countries && countries.length > 0 && countries !== prevState.countries) {
      return { countries, listCountry: formatCountry(countries), nSymbol: _.find(formatCountry(countries) || [], x => x.value === auth.getCountry()) };
    }
    return null;
  }

  toggle = () => {
    const { dropdownOpen } = this.state;
    this.setState({
      dropdownOpen: !dropdownOpen,
    });
  }

  onChange = (symbol) => {
    this.setState({ nSymbol: symbol, dropdownOpen: false });
  }

  onClickSave = () => {
    this.setState({ isShowPopUp: false });
    const { nSymbol } = this.state;
    const nCountry = _.find(this.props.countries, x => x.code === nSymbol.value) || {};
    auth.setCountry(JSON.stringify(nCountry));
    this.changeUrl(nSymbol.value);
    window.location.reload();
  }

  onClickCancel = () => {
    const { listCountry } = this.state;
    this.setState({ isShowPopUp: false, nSymbol: _.find(listCountry, x => x.value === auth.getCountry()) });
    if (this.props.eventClosePopup) {
      this.props.eventClosePopup();
    }
  }

  onClickOpen = () => {
    if (!this.props.isShowPopUpOutSide) {
      this.setState({ isShowPopUp: true });
    }
    if (this.props.handleClickOutside) { this.props.handleClickOutside(); }
    if (this.props.onCloseDrawer) { this.props.onCloseDrawer(); }
  }

  onClickGotoStore = () => {
    window.location.href = 'https://www.maison21g.com/sg/our-boutique';
  }

  generateSelectInputComponents = () => {
    const inputComponents = {
      DropdownIndicator: selectProps => (
        <img style={{ marginBottom: '5px', width: '20px' }} src={icDown} alt="Arrow down icon" />
      ),
    };
    return inputComponents;
  };

  render() {
    const {
      className, handleClickOutside, buttonBlocks, isBlack, isVersion2,
    } = this.props;
    const {
      isShowPopUp, nSymbol, listCountry,
    } = this.state;
    // const nCountry = _.find(listCountry, x => x.value === nSymbol.value) || {};
    const cancelBt = getNameFromButtonBlock(buttonBlocks, 'CANCEL');
    const saveBt = getNameFromButtonBlock(buttonBlocks, 'SAVE');
    const locationBt = getNameFromButtonBlock(buttonBlocks, 'Location');
    const settingBt = getNameFromButtonBlock(buttonBlocks, 'Settings');
    return (
      <div className={`${className} div-region`}>
        <Modal className={classnames('modal-full-screen', addFontCustom())} isOpen={isShowPopUp}>
          <ModalBody>
            <div className={`div-dialog-region div-col pa-3 animated fadeIn faster ${isVersion2 ? 'version-2' : ''}`}>
              <div className="header">
                <span>
                  {settingBt}
                </span>
                <button
                  type="button"
                  className="button-bg__none"
                  onClick={this.onClickCancel}
                >
                  <img src={icClose} alt="close" />
                </button>
              </div>
              <div className="div-col div-body">
                <div className="div-row infor items-center">
                  {/* <div className="div-col justify-center items-center">
                    <span>
                      {nCurrency.symbol}
                    </span>
                  </div> */}
                  {
                    isVersion2 ? (
                      <PublicIcon style={{ color: '#2c2c2c' }} />
                    ) : (
                      <img src={icLocationBlack} alt="location" style={{ height: '16px' }} />
                    )
                  }
                  <span className="ml-2">
                    {locationBt}
                  </span>
                </div>
                <Select
                  value={nSymbol}
                  onChange={this.onChange}
                  options={listCountry}
                  className="select-country mt-3 mb-4"
                  components={this.generateSelectInputComponents()}
                />
                {
                  isVersion2 ? (
                    <div className="w-100">
                      <ButtonCT name="Save" onClick={this.onClickSave} className="w-100" size="medium" />
                    </div>
                  ) : (
                    <React.Fragment>
                      <button
                        type="button"
                        className="button-save"
                        onClick={this.onClickSave}
                      >
                        {saveBt}
                      </button>
                      <button
                        type="button"
                        className="button-bg__none"
                        onClick={this.onClickCancel}
                      >
                        {cancelBt}
                      </button>
                    </React.Fragment>
                  )
                }

              </div>
            </div>
          </ModalBody>
        </Modal>
        {
          isVersion2 ? (
            <React.Fragment>
              <div className="left-block div-row" onClick={this.onClickOpen}>
                <PublicIcon />
                <div className="text">
                  {nSymbol?.label}
                </div>
                <KeyboardArrowDownIcon />
              </div>
              {
                ['sg'].includes(auth.getCountry()) && (
                  <div className="block-boutique" onClick={this.onClickGotoStore}>
                    <StoreIcon />
                    <div className="text">
                      OUR BOUTIQUES
                    </div>
                  </div>
                )
              }
            </React.Fragment>
          ) : (
            <button
              type="button"
              className={classnames('button-bg__none pa0', (this.props.isShowPopUpOutSide ? 'hidden' : ''))}
              style={isMobile ? { height: '42px' } : {}}
              onClick={this.onClickOpen}
            >
              <div className="div-row justify-center items-center">
                {/* <img src={icFlags} alt="icFlags" /> */}
                {/* <Flag code='SG' height="15"></Flag> */}
                <img
                  style={{
                    height: isLargerScreen ? '14px' : '10px',
                  }}
                  src={!isMobile ? isBlack ? icLocationBlack : icLocation : icLocationBlack}
                  alt="location"
                />
                <h4
                  style={{
                    color: !isMobile ? isBlack ? '#0d0d0d' : '#fff' : '',
                  }}
                  className="header_4 ml-2"
                >
                  {`${nSymbol ? nSymbol.label : ''}`}
                </h4>
              </div>
            </button>
          )
        }

      </div>
    );
  }
}

export default Region;
