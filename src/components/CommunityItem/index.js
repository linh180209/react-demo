import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Row, Col,
  Label, Input,
  Button,
  Form, FormGroup,
  Card, CardHeader, CardBody,
} from 'reactstrap';
import { Player } from 'video-react';
import auth from '../../Redux/Helpers/auth';
import { generaCurrency } from '../../Redux/Helpers';

class CommunityItem extends Component {
  state = {
    total: 1,
  }

  decrease = () => {
    if (this.state.total > 1) {
      this.setState(prevState => (
        { total: prevState.total - 1 }
      ));
    }
  }

  increase = () => {
    this.setState(prevState => (
      { total: prevState.total + 1 }
    ));
  }

  render() {
    const { total } = this.state;
    const { title, type, price } = this.props.data;
    return (
      <div>
        <Card>
          <CardHeader>
            <strong style={{ fontSize: '1.2rem' }}>
              {title}
              {' '}
            </strong>
          </CardHeader>
          <CardBody>
            <Form className="form-horizontal">
              <FormGroup row>
                <Player
                  playsInline
                  src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
                />
              </FormGroup>
              <FormGroup row>
                <div className="card__community">
                  <span className="text__community--type">
                    {type}
                  </span>
                  <span className="text__community--value">
                    {generaCurrency(price)}
                  </span>
                  <div>
                    <button
                      className="text__community--bt_left"
                      type="button"
                      onClick={this.decrease}
                    >
                      -
                    </button>
                    <span
                      className="text__community--total"
                    >
                      {total}
                    </span>
                    <button
                      className="text__community--bt_right"
                      type="button"
                      onClick={this.increase}
                    >
                      +
                    </button>
                  </div>
                </div>
              </FormGroup>
            </Form>
          </CardBody>
        </Card>
      </div>
    );
  }
}
CommunityItem.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string,
    type: PropTypes.string,
    price: PropTypes.number,
  }).isRequired,
};

export default CommunityItem;
