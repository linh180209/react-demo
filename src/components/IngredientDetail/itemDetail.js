import React, { Component } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

class ItemDetail extends Component {
  render() {
    const { title, data } = this.props;

    return (
      <div className="div-item-detail">
        <div className="div-title">
          <h6>
            {title}
          </h6>
        </div>
        <div className="div-list">
          {
            _.map(data, x => (
              <div className="item-list">
                <div className="point" />
                <span>
                  {x}
                </span>
              </div>
            ))
          }
        </div>
      </div>
    );
  }
}
ItemDetail.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.arrayOf().isRequired,
};
export default ItemDetail;
