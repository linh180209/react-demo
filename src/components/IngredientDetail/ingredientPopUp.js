/* eslint-disable react/sort-comp */
/* eslint-disable no-lonely-if */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Row, Col, Modal, ModalBody,
} from 'reactstrap';
import _ from 'lodash';
import classnames from 'classnames';
import {
  isIOS, withOrientationChange,
} from 'react-device-detect';
import ReactHtmlParser from 'react-html-parser';
import { connect } from 'react-redux';
import CircleType from 'circletype';
import '../../styles/ingredient-detail.scss';
import {
  isMobile, isBrowser, isTablet,
} from '../../DetectScreen/detectIFrame';
import icClose from '../../image/icon/ic-close.svg';
import icBack from '../../image/icon/ic-prev-silder-w.svg';
import {
  getAltImage, fetchCMSHomepage, getNameFromButtonBlock, addFontCustom,
} from '../../Redux/Helpers';
import VideoAcademy from '../AcademyItem/videoAcademy';
import addCmsRedux from '../../Redux/Actions/cms';
import updateIngredientsData from '../../Redux/Actions/ingredients';
import { fetchScentLib } from '../../Redux/Helpers/fetchAPI';
import ButtonCT from '../ButtonCT';
import auth from '../../Redux/Helpers/auth';

class IngredientPopUp extends Component {
  constructor(props) {
    super(props);
    this.isScroll = false;
  }

  state = {
    isShowInternal: false,
    isShowText: false,
  };

  componentDidMount() {
    this.fetchDataInit();
    this.getAllIngredients();
    window.addEventListener('resize', this.updateSize);
    if (this.props.isShow) {
      setTimeout(() => {
        window.addEventListener('click', this.detectOutClick);
        this.setState({ isShowText: true }, () => {
          this.setCircleText();
        });
      }, 700);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.isShow && prevProps.isShow !== this.props.isShow) {
      this.showDialog();
      const ele = document.getElementById('div-content');
      if (ele) {
        ele.scrollTo({ top: 0 });
      }
      setTimeout(() => {
        window.addEventListener('click', this.detectOutClick);
        this.setState({ isShowText: true }, () => {
          this.setCircleText();
        });
      }, 500);
    } else if (!this.props.isShow && prevProps.isShow !== this.props.isShow) {
      window.removeEventListener('click', this.detectOutClick);
      this.setState({ isShowText: false });
    }
  }

  componentWillUnmount = () => {
    window.removeEventListener('click', this.detectOutClick);
    window.removeEventListener('resize', this.updateSize);
  }

  updateSize = () => {
    this.forceUpdate();
  }

  setCircleText = () => {
    const ele1 = document.getElementById('span-1');
    if (ele1) {
      new CircleType(document.getElementById('span-1'))
        .radius(180).dir(-1);
    }
    const ele2 = document.getElementById('span-2');
    if (ele2) {
      new CircleType(document.getElementById('span-2'))
        .radius(150).dir(1);
      setTimeout(() => {
        ele2.style.transform = 'rotate(-50deg)';
      }, 0);
    }
    const ele3 = document.getElementById('span-3');
    if (ele3) {
      new CircleType(document.getElementById('span-3'))
        .radius(150).dir(1);
      setTimeout(() => {
        ele3.style.transform = 'rotate(50deg)';
      }, 0);
    }
  }

  handleScroll = (e) => {
    const { isShowFull } = this.state;
    const { isLandscape } = this.props;
    const ele = document.getElementById('div-content');
    if (ele.scrollTop > 10 && isMobile && !isLandscape && !this.isScroll) {
      this.isScroll = true;
      this.setState({ isShowFull: true }, () => {
        // ele.scrollTo({ top: 11 });
      });
    } else if (isShowFull && ele.scrollTop <= 10) {
      this.isScroll = false;
      this.setState({ isShowFull: false });
    }
  }

  detectOutClick = (e) => {
    if (document.getElementById('div-content').contains(e.target)) {
      // Clicked in box
      console.log('detectOutClick in');
    } else {
      // Clicked outside the box
      if (this.props.isShow && !this.props.ingredientMinimum) {
        this.onCloseIngredient();
      }
    }
  }

  onCloseIngredient = () => {
    this.hiddenDialog();
    this.props.onCloseIngredient();
  }

  fetchDataInit = async () => {
    const { cms } = this.props;
    const ingredientBlock = _.find(cms, x => x.title === 'Ingredient Detail');
    if (!ingredientBlock) {
      const cmsData = await fetchCMSHomepage('ingredient-detail');
      this.props.addCmsRedux(cmsData);
    }
  }

  getAllIngredients = async () => {
    const { ingredients } = this.props;
    if (!ingredients || ingredients.length === 0) {
      const dataScentLib = await fetchScentLib();
      this.props.updateIngredientsData(dataScentLib);
    }
  }

  addRemoveClass = (addClass, removeClass) => {
    const element = document.getElementById('div-ingredient-detail');
    if (element && removeClass) {
      element.classList.remove(removeClass);
    }
    const arr = element.className.split(' ');
    if (arr.indexOf(addClass) === -1) {
      element.className += ` ${addClass}`;
    }
  }

  showDialog = () => {
    this.setState({ isShowInternal: true }, (() => {
      // const ele = document.getElementById('div-content');
      // if (ele) {
      //   ele.scrollTop = 0;
      // }
      if (isMobile && !this.props.isLandscape) {
        this.addRemoveClass('fadeInUp', 'fadeOutDown');
      } else {
        this.addRemoveClass('fadeIn', 'fadeOut');
      }
    }));
  }

  hiddenDialog = () => {
    if (isMobile && !this.props.isLandscape) {
      this.addRemoveClass('fadeOutDown', 'fadeInUp');
    } else {
      this.addRemoveClass('fadeOut', 'fadeInUp');
    }
    setTimeout(() => {
      this.isScroll = false;
      this.setState({ isShowInternal: false, isShowFull: false });
    }, 500);
  }

  render() {
    const {
      isShow, data, className, isLandscape, ingredients, ingredientMinimum,
      onClickCancelIngredientsMini,
    } = this.props;
    const isDataHasFull = data?.scent_dna && data?.scent_duration && data?.scent_evolution;
    const { isShowInternal, isShowFull } = this.state;
    const { id, productId } = data;
    const ingredient = isDataHasFull ? { ingredient: data } : id ? _.find(ingredients, x => x.ingredient.id === id) : _.find(ingredients, x => x.id === productId);
    const {
      description, eco, mind_enhancements: mindEnhancements,
      mood_enhancements: moodEnhancements, name,
      scent_dna: scentDna, scent_evolution: scentEvolution,
      scent_strength: scentStrength, top_banner: topBanner,
      title, url, accords, scents, scent_duration: scentDuration,
      duration,
    } = ingredient ? ingredient.ingredient : {};

    const dataEnhancements = mindEnhancements && moodEnhancements ? mindEnhancements.concat(moodEnhancements) : [];
    const ingredientBlock = _.find(this.props.cms, x => x.title === 'Ingredient Detail');
    const { body } = ingredientBlock || [];
    const textBlock = _.filter(body, x => x.type === 'text_block');
    const buttonBlocks = _.filter(body, x => x.type === 'button_block');

    const visitBt = getNameFromButtonBlock(buttonBlocks, 'VISIT_BLOG');
    const benefitsBt = getNameFromButtonBlock(buttonBlocks, 'BENEFITS_FOR');
    const sublteBt = getNameFromButtonBlock(buttonBlocks, 'Sublte');
    const strongBt = getNameFromButtonBlock(buttonBlocks, 'Strong');
    const durationBt = getNameFromButtonBlock(buttonBlocks, 'duration');
    const hourBt = getNameFromButtonBlock(buttonBlocks, 'Hours_10');
    const applyBt = getNameFromButtonBlock(buttonBlocks, 'APPLY');

    const heightScreenContentMobile = window.innerHeight - 224;

    const itemRenderIngredient = (
      <div
        id="div-ingredient-detail"
        className={`div-ingredient-detail detail-min animated faster ${className} ${isMobile ? 'detail-min-mobile' : ''}`}
      >
        <div className="div-banel" style={{ backgroundImage: `url(${topBanner})` }}>
          <div className="div-text">
            <h2>
              {name}
            </h2>
          </div>
          <span>
            {title}
          </span>
          <button
            onClick={onClickCancelIngredientsMini}
            className="button-bg__none bt-close"
            type="button"
          >
            <img src={icBack} alt="back" />
          </button>
        </div>
        <div id="div-content" className="div-content" style={isMobile ? { height: `${window.innerHeight - 224}px` } : {}}>
          <div className="wrap-header">
            <Row className="div-info-v2">
              <Col md="12" xs="12" className="div-col items-center">
                <div className="div-col items-center div-evolution">
                  <div className="div-title">
                    <span>
                      {textBlock && textBlock.length > 1 ? textBlock[1].value : ''}
                    </span>
                  </div>
                  {
                    this.state.isShowText && (
                      <div className="div-image">
                        <img loading="lazy" src={scentEvolution} alt="evolution" />
                        {
                          _.map(accords, x => (
                            <span>{x}</span>
                          ))
                        }
                      </div>
                    )
                  }
                </div>
              </Col>
            </Row>
            <Row className="div-info-v2 list-secondary">
              <Col md="4" xs="6" className="div-col items-center">
                <div className="div-col items-center div-strength">
                  <div className="div-title">
                    <span>
                      {textBlock && textBlock.length > 2 ? textBlock[2].value : ''}
                    </span>
                  </div>
                  {
                      this.state.isShowText && (
                        <div className="div-image">
                          <img loading="lazy" src={scentStrength} alt="strength" />
                          <span>{sublteBt}</span>
                          <span>{strongBt}</span>
                          {/* <span>%</span> */}
                        </div>
                      )
                    }
                </div>
              </Col>
              <Col md="4" xs="6" className="div-col items-center">
                <div className="div-col items-center div-duration">
                  <div className="div-title">
                    <span>
                      {durationBt}
                    </span>
                  </div>
                  {
                    this.state.isShowText && (
                      <div className="div-image">
                        <img loading="lazy" src={scentDuration} alt="evolution" />
                        <span>{hourBt.replace('{duration}', duration)}</span>
                      </div>
                    )
                  }
                </div>
              </Col>
              <Col md="4" xs="12" className="div-col items-center">
                <div className="div-col items-center div-persinal-image">
                  <div className="div-title">
                    <span>
                      {textBlock && textBlock.length > 0 ? textBlock[0].value : ''}
                    </span>
                  </div>
                  {
                    this.state.isShowText && (
                      <div className="div-image">
                        <img loading="lazy" src={scentDna} alt="persional" />
                        {
                          _.map(scents, (x, index) => (
                            <div id={`span-${index + 1}`}>{x}</div>
                          ))
                        }
                      </div>
                    )
                  }
                </div>
              </Col>
            </Row>
          </div>
        </div>
        <div className={isMobile ? 'div-list-button div-row' : 'hidden'}>
          {/* <ButtonCT
            name="Cancel"
            className="outline-bt"
            onClick={this.props.onClickCancelIngredientsMini}
          /> */}
          <ButtonCT
            name={applyBt}
            className="bt-bg-cart"
            onClick={this.props.onClickApplyMini}
          />
        </div>
      </div>
    );

    if (ingredientMinimum) {
      return (
        isMobile ? (
          <Modal className={classnames('modal-full-screen', addFontCustom())} isOpen={isShow} fade={false}>
            <ModalBody>
              {itemRenderIngredient}
            </ModalBody>
          </Modal>
        ) : (
          itemRenderIngredient
        )
      );
    }
    return (
      <Modal className={classnames('modal-full-screen', addFontCustom())} isOpen={isShow} fade={false}>
        <ModalBody>
          {/* <div className="div-bg-full" style={isShowFull && isShowInternal ? { visibility: 'visible', opacity: 1 } : { visibility: 'hidden' }} /> */}
          <div
            id="div-ingredient-detail"
            className={`div-ingredient-detail animated faster ${isShowFull ? isIOS ? 'full-ios' : 'full' : ''} ${className} ${isShowInternal ? '' : 'hidden'} ${auth.getCountry() === 'ae' ? 'text-big-size' : ''}`}
          >
            <button type="button" className="button-bg__none bt-close" onClick={this.onCloseIngredient}>
              <img loading="lazy" src={icClose} alt="ic-close" />
            </button>
            <div className={(isBrowser || (isTablet && isLandscape)) ? 'div-banel' : 'hidden'} style={{ backgroundImage: `url(${topBanner})` }}>
              <div className="div-text">
                <h2>
                  {name}
                </h2>
              </div>
              <span>
                {title}
              </span>
            </div>
            <div id="div-content" className="div-content" onScroll={this.handleScroll}>
              <div className={isMobile && !isLandscape ? 'div-banel' : 'hidden'} style={{ backgroundImage: `url(${topBanner})` }}>
                {/* <button type="button" className={(isBrowser || (isTablet && isLandscape)) ? 'hidden' : 'bt-close-mobile'} onClick={this.onCloseIngredient}>
                  <img src={icCloseMobile} alt="ic-close" />
                </button> */}
                <div className="div-text">
                  <h2>
                    {name}
                  </h2>
                </div>
                <span>
                  {title}
                </span>
              </div>
              <div className="wrap-header">
                <Row className="div-info-v2">
                  <Col md="4" xs="12" className="div-col items-center">
                    <div className="div-col items-center div-persinal-image">
                      <div className="div-title">
                        <span>
                          {textBlock && textBlock.length > 0 ? textBlock[0].value : ''}
                        </span>
                      </div>
                      {
                        this.state.isShowText && (
                          <div className="div-image">
                            <img loading="lazy" src={scentDna} alt="persional" />
                            {
                              _.map(scents, (x, index) => (
                                <div id={`span-${index + 1}`}>{x}</div>
                              ))
                            }
                          </div>
                        )
                      }
                    </div>
                  </Col>
                  <Col md="4" xs="12" className="div-col items-center">
                    <div className="div-col items-center div-evolution">
                      <div className="div-title">
                        <span>
                          {textBlock && textBlock.length > 1 ? textBlock[1].value : ''}
                        </span>
                      </div>
                      {
                        this.state.isShowText && (
                          <div className="div-image">
                            <img loading="lazy" src={scentEvolution} alt="evolution" />
                            {
                              _.map(accords, x => (
                                <span>{x}</span>
                              ))
                            }
                          </div>
                        )
                      }
                    </div>
                  </Col>
                  <Col md="4" xs="12" className="div-col items-center">
                    <div className="div-col items-center div-strength">
                      <div className="div-title">
                        <span>
                          {textBlock && textBlock.length > 2 ? textBlock[2].value : ''}
                        </span>
                      </div>
                      {
                          this.state.isShowText && (
                            <div className="div-image">
                              <img loading="lazy" src={scentStrength} alt="strength" />
                              <span>{sublteBt}</span>
                              <span>{strongBt}</span>
                              {/* <span>%</span> */}
                            </div>
                          )
                        }
                    </div>
                  </Col>
                </Row>
                {/* <Row className="div-info">
                  <Col md="4" xs="12">
                    {
                  (isBrowser || (isTablet && isLandscape)) ? (
                    <ItemIngredient url={scentDna} title={textBlock && textBlock.length > 0 ? textBlock[0].value : ''} />
                  ) : (
                    <ItemIngredient url={scentEvolution} title={textBlock && textBlock.length > 1 ? textBlock[1].value : ''} />
                  )
                }

                  </Col>
                  <Col md="4" xs="12">
                    {
                  (isBrowser || (isTablet && isLandscape)) ? (
                    <ItemIngredient url={scentEvolution} title={textBlock && textBlock.length > 1 ? textBlock[1].value : ''} />
                  ) : (
                    <ItemIngredient url={scentDna} title={textBlock && textBlock.length > 0 ? textBlock[0].value : ''} />
                  )
                }
                  </Col>
                  <Col md="4" xs="12">
                    <ItemIngredient url={scentStrength} title={textBlock && textBlock.length > 2 ? textBlock[2].value : ''} />
                  </Col>
                </Row> */}
              </div>
              { ingredient && ingredient.videos?.length > 0
                && (
                <div className="div-video-ingredient">
                  <VideoAcademy url={ingredient.videos[0].video} poster={ingredient.videos[0].placeholder} isControl />
                </div>
                )
            }
              {/* <div className="div-detail">
              <ItemDetail title={textBlock && textBlock.length > 3 ? textBlock[3].value : ''} data={moodEnhancements} />
              <ItemDetail title={textBlock && textBlock.length > 4 ? textBlock[4].value : ''} data={mindEnhancements} />
            </div> */}
              <div className="div-about">
                <h6>
                  {textBlock && textBlock.length > 5 ? textBlock[5].value : ''}
                </h6>
                <span>
                  {ReactHtmlParser(description)}
                </span>
              </div>

              <div className="div-benefits">
                <h6>{benefitsBt}</h6>
                <div className="benefits">
                  {
                _.map(dataEnhancements, d => (
                  <span>
                    <i className="fa fa-plus" />
                    {' '}
                    {d}
                  </span>
                ))
              }
                </div>

              </div>

              <div className="div-logo">
                <img loading="lazy" src={eco} alt={getAltImage(eco)} />
              </div>

              {/* <div className={this.props.isKrisshop ? 'hidden' : 'div-bt-footer'}>
                <h2>
                  {textBlock && textBlock.length > 7 ? textBlock[7].value : ''}
                </h2>
                <button type="button" className="bt-checkout" onClick={() => onClickLink(url, null, this.props.history)}>
                  {visitBt}
                </button>
              </div> */}
            </div>
          </div>
        </ModalBody>
      </Modal>

    );
  }
}

IngredientPopUp.defaultProps = {
  ingredientMinimum: false,
};

IngredientPopUp.propTypes = {
  isShow: PropTypes.bool.isRequired,
  onCloseIngredient: PropTypes.func.isRequired,
  data: PropTypes.shape({
    bot_banner: PropTypes.string,
    description: PropTypes.string,
    eco: PropTypes.string,
    mind_enhancements: PropTypes.arrayOf(),
    mood_enhancements: PropTypes.arrayOf(),
    name: PropTypes.string,
    scent_dna: PropTypes.string,
    scent_evolution: PropTypes.string,
    scent_strength: PropTypes.string,
    top_banner: PropTypes.string,
    border_color: PropTypes.string,
    title: PropTypes.string,
    url: PropTypes.string,
  }).isRequired,
  history: PropTypes.shape().isRequired,
  addCmsRedux: PropTypes.func.isRequired,
  cms: PropTypes.shape().isRequired,
  className: PropTypes.string.isRequired,
  isLandscape: PropTypes.bool.isRequired,
  isKrisshop: PropTypes.bool.isRequired,
  ingredientMinimum: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    ingredients: state.ingredients,
    cms: state.cms,
  };
}

const mapDispatchToProps = {
  addCmsRedux,
  updateIngredientsData,
};

export default connect(mapStateToProps, mapDispatchToProps)(withOrientationChange(IngredientPopUp));
