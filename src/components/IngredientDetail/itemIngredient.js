import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getAltImage } from '../../Redux/Helpers';

class ItemIngredient extends Component {
  render() {
    const { title, url } = this.props;
    return (
      <div className="item-ingredient">
        <div className="div-title">
          <span>
            {title}
          </span>
        </div>
        <img loading="lazy" src={url} alt={getAltImage(url)} />
      </div>
    );
  }
}

ItemIngredient.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default ItemIngredient;
