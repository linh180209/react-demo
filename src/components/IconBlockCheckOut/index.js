import classnames from 'classnames';
import _ from 'lodash';
import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { Modal, ModalBody } from 'reactstrap';
import { isMobile } from '../../DetectScreen';
import icClose from '../../image/icon/ic-close-region.svg';
import { addFontCustom } from '../../Redux/Helpers';
import { useMergeState } from '../../Utils/customHooks';
import ItemIcon from './itemIcon';
import './styles.scss';

function IconBlockCheckOut(props) {
  const [state, setState] = useMergeState({
    isShowModalDetail: false,
    dataPopup: {},
  });

  const onClosePopup = () => {
    setState({ isShowModalDetail: false });
  };

  const onlickPopup = (data) => {
    console.log('onlickPopup', data);
    setState({
      isShowModalDetail: true,
      dataPopup: data,
    });
  };

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplaySpeed: 5000,
    pauseOnHover: false,
  };
  return (
    <div className="icon-block-checkout">
      <Modal
        isOpen={state.isShowModalDetail}
        className={classnames('modal-detail-info', addFontCustom())}
        centered
      >
        <button
          type="button"
          className="button-bg__none bt-close"
          onClick={onClosePopup}
        >
          <img src={icClose} alt="close" />
        </button>
        <ModalBody className="body-modal-detail">
          <h3>{state.dataPopup?.value?.header?.header_text}</h3>
          <div className="des">
            {
              state.dataPopup?.value?.image?.caption
            }
          </div>
        </ModalBody>
      </Modal>
      {
        isMobile ? (
          <Slider {...settings}>
            {
              _.map(props.data?.icons || [], (d, index) => (
                <ItemIcon data={d} index={index} onlickPopup={onlickPopup} />
              ))
            }
          </Slider>
        ) : (
          _.map(props.data?.icons || [], (d, index) => (
            <ItemIcon data={d} index={index} onlickPopup={onlickPopup} />
          ))
        )
      }
    </div>
  );
}

export default IconBlockCheckOut;
