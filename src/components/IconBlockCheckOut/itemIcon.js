import React, { useState } from 'react';
import { Popover, PopoverBody } from 'reactstrap';
import { isBrowser } from '../../DetectScreen';
import icCheckOut from '../../image/icon/ic-info-checkout.svg';
import { getAltImageV2 } from '../../Redux/Helpers';

function ItemIcon(props) {
  return (
    <div className="item-icon div-row">
      <img src={props.data?.value?.image?.image} alt={getAltImageV2(props.data?.value?.image)} className="icon" />
      <span>
        {props.data?.value?.text}
      </span>
      {
        props.data?.value?.header?.header_text && (
          <React.Fragment>
            <button type="button" className="button-bg__none" onClick={() => props.onlickPopup(props.data)}>
              <img src={icCheckOut} alt="icon" />
            </button>
          </React.Fragment>
        )
      }
    </div>
  );
}

export default ItemIcon;
