import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { Link } from 'react-router-dom';
import './styles.scss';

function ButtonCT(props) {
  return (
    props.typeBT === 'LINK-OUT' ? (
      <a
        href={props.href}
        disabled={props.disabled}
        className={classnames('button-custom button-a', (props.disabled ? 'disabled' : ''), props.className)}
      >
        {props.iconLeft && (
          <img src={props.iconLeft} alt="icon" />
        )}
        {props.name}
        {props.iconRight && (
          <img src={props.iconRight} alt="icon" />
        )}
      </a>
    ) : props.typeBT === 'LINK' ? (
      <Link
        to={props.href}
        disabled={props.disabled}
        className={classnames('button-custom button-a', (props.disabled ? 'disabled' : ''), props.className)}
      >
        {props.iconLeft && (
          <img src={props.iconLeft} alt="icon" />
        )}
        {props.name}
        {props.iconRight && (
          <img src={props.iconRight} alt="icon" />
        )}
      </Link>
    ) : (
      <button
        data-gtmtracking={props.dataGtmtracking}
        type="button"
        disabled={props.disabled}
        onClick={props.onClick}
        className={classnames('button-custom', (props.disabled ? 'disabled' : ''), props.className)}
      >
        {props.iconLeft && (
          <img src={props.iconLeft} alt="icon" />
        )}
        {props.name}
        {props.iconRight && (
          <img src={props.iconRight} alt="icon" />
        )}
      </button>
    )
  );
}

PropTypes.defaultProps = {
  className: '',
  iconRight: undefined,
  iconLeft: undefined,
  typeBT: undefined,
  href: undefined,
  disabled: false,
  onClick: () => {},
};

PropTypes.propTypes = {
  name: PropTypes.string.isRequired,
  className: PropTypes.string,
  iconRight: PropTypes.string,
  iconLeft: PropTypes.string,
  typeBT: PropTypes.string,
  href: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

export default ButtonCT;
