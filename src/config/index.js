/* eslint-disable prefer-destructuring */
/* eslint-disable import/no-cycle */
import auth from '../Redux/Helpers/auth';

const { HOST, IS_DEV: isDev } = process.env;
export const IS_DEV = isDev === 'true';
export const KEY_CHECK_OUT = process.env.KEY_CHECK_OUT;
export const KEY_STRIPE_APPLE = process.env.KEY_STRIPE_APPLE;

export const SENDY_STAGING = 'https://d8760fbbfd5a429d8da4a01876d8d645@o84181.ingest.sentry.io/5410513';
export const SENDY_PROD = 'https://b9ee9ffbe17b41dbbd84f2e05fb3b26a@o84181.ingest.sentry.io/5410514';

export const KEY_GTM = 'GTM-KG6BQ3K';
export const KEY_API_SIMPLY_BOOK_SG = 'b0b67857c28c29fcd5f30c1870dae9da762c5af0ad488ffa2ba93577e087eb69';
export const KEY_API_SIMPLY_BOOK_UAE = 'baf6c98a4d99cbb80f813b089bcb97c2c3854e1db9ce75d906195f39a0a19d27';
export const COMPANY_LOGIN_SG = 'maison21g';
export const COMPANY_LOGIN_UAE = 'maison21guae';
export const SIMPLY_BOOK_URL = 'https://user-api.simplybook.me/';

export const getCompanySimplyBook = () => (auth.getCountry() === 'ae' ? COMPANY_LOGIN_UAE : COMPANY_LOGIN_SG);
export const getKeyApiSimplyBook = () => (auth.getCountry() === 'ae' ? KEY_API_SIMPLY_BOOK_UAE : KEY_API_SIMPLY_BOOK_SG);

export const IS_HIDDEN_CLUB21G = true;

export const LOGIN_URL = `${HOST}/api/token/`;
export const TOKEN_REFRESH = `${HOST}/api/token/refresh/`;
export const GET_USER_DETAIL = `${HOST}/api/users/detail/`;
export const CREATE_USER_URL = `${HOST}/user/login/`;
export const UPDATE_USER_URL = `${HOST}/api/users/{id}/`;
export const GET_ALL_ANSWERS = `${HOST}/api/answers/`;
export const GET_ALL_ANSWERS_FERRARI = `${HOST}/api/answers/?partnership=ferrari`;
export const GET_ALL_QUESTIONS = `${HOST}/api/questions/`;
export const GET_ALL_QUESTIONS_FERRARI = `${HOST}/api/questions/?partnership=ferrari`;
export const GET_CHOISE_URL = `${HOST}/api/choices/`;
export const GET_BENEFIT_CHOISE_URL = `${HOST}/api/benefit-choices/`;
export const GET_PAGE_CMS_URL = `${HOST}/api/cms/pages/`;
export const GET_PAGE_CMS_HOME_PAGE_URL = `${HOST}/api/cms/pages/?slug=`;
export const GET_PAGE_DRAFT_CMS_HOME_PAGE_URL = `${HOST}/api/cms/draft-pages/?slug=`;
export const GET_PAGE_ALL_CMS_URL = `${HOST}/api/cms/pages?limit=100`;
export const POST_OUTCOMES_URL = `${HOST}/api/outcomes/`;
export const SIGN_UP_URL = `${HOST}/api/signup/`;
export const LOGIN_FACEBOOK_URL = `${HOST}/api/auth/facebook/`;
export const LOGIN_GOOGLE_URL = `${HOST}/api/auth/google/`;
export const GET_MOOD_URL = `${HOST}/api/moods/`;
export const PASSWORD_RESET_URL = `${HOST}/api/auth/password/reset/`;
export const PASSWORD_RESET_COMFIRM_URL = `${HOST}/api/auth/password/reset/confirm/`;
export const GET_USER_OUTCOME = `${HOST}/api/users/{user}/outcomes/`;
export const PUT_OUTCOME_URL = `${HOST}/api/outcomes/{id}/`;
export const GET_ALL_SCENT = `${HOST}/api/items/?type=scent`;
export const GET_ALL_PRODUCTS_SCENT = `${HOST}/api/products/mmo/`;
export const GET_ALL_PRODUCTS_BOTTLE = `${HOST}/api/products/?type=bottle`;
export const GET_ALL_PRODUCTS_BOTTLE_CAR = `${HOST}/api/products/?type=car_diffuser_box`;
export const GET_ALL_PRODUCTS_BOTTLE_DIY = `${HOST}/api/products/?type=bottle_diy`;
export const GET_ALL_PRODUCTS_DIFFUSER = `${HOST}/api/products/?type=diffuser`;
export const GET_ALL_PRODUCTS_BOTTLE_BUNDLE = `${HOST}/api/products/?type=bottle_bundle_creation`;
export const GET_ALL_PRODUCTS_HOLDER = `${HOST}/api/products/?type=holder`;
export const GET_ALL_PRODUCTS_BURNER = `${HOST}/api/products/?type=burner`;
export const GET_ALL_PRODUCTS_MINI_CANDLE_BOX = `${HOST}/api/products/?type=mini_candle_holder`;
export const GET_ALL_PRODUCTS_METAL_CAP = `${HOST}/api/products/?type=metal_cap`;
export const CHECK_PROMOTION_CODE = `${HOST}/api/promo/`;
export const CHECK_PROMOTION_CODE_V2 = `${HOST}/api/carts/{id}/check-promo/`;
export const ASSIGN_PROMOTION_CODE_CART = `${HOST}/api/carts/{id}/`;
export const PUST_BASKET_TO_SERVER = `${HOST}/api/carts/`;
export const GET_BASKET_URL = `${HOST}/api/carts/{cartPk}/`;
export const CREATE_BASKET_URL = `${HOST}/api/users/{userId}/carts/`;
export const CLEAR_BASKET_URL = `${HOST}/api/carts/{id}/clear/`;
export const ADD_BASKET_URL = `${HOST}/api/carts/{cartPk}/items/`;
export const PRECHECKOUT_BASKET_URL = `${HOST}/api/carts/{cartPk}/precheckout/`;
export const GUEST_CREATE_BASKET_URL = `${HOST}/api/carts/`;
export const ASSIGN_BASKET_URL = `${HOST}/api/carts/{id}/transfer/`;

export const EDIT_DELETE_BASKET_URL = `${HOST}/api/carts/{cartPk}/items/{id}/`;
export const GET_ADDRESS_URL = `${HOST}/api/users/{user_pk}/addresses/`;
export const GET_LIST_ADDRESS_URL = `${HOST}/api/addresses/`;
export const GET_ADDRESS_SUBSCRIPTION_URL = `${HOST}/api/addresses/subscription/`;
export const UPDATE_ADDRESS_URL = `${HOST}/api/users/{user_pk}/addresses/{id}/`;
export const DELETE_ADDRESS_URL = `${HOST}/api/users/{user_pk}/addresses/{id}/`;
export const UPDATE_ADDRESS_GUEST_URL = `${HOST}/api/addresses/{id}/`;
export const GET_SOCIAL_ACCOUNT_URL = `${HOST}/api/social-accounts/`;
export const GOOGLE_CONNECT_URL = `${HOST}/api/auth/google/connect/`;
export const FACEBOOK_CONNECT_URL = `${HOST}/api/auth/facebook/connect/`;
export const SOCIAL_DISCONNECT_URL = `${HOST}/api/social-accounts/{id}/disconnect/`;
export const GET_COMBO_URL = `${HOST}/api/products/?combo={id1}%2C{id2}&type=perfume_diy`;
export const GET_PRODUCT_URL = `${HOST}/api/products/{id}/`;
export const CREATE_ADDRESS_URL = `${HOST}/api/addresses/`;
export const CHECKOUT_FUNCTION_URL = `${HOST}/api/carts/{id}/checkout/`;
export const PAYPAL_GENERATE_URL = `${HOST}/api/paypal/generate/`;
export const STRIPE_GENERATE_URL = `${HOST}/api/stripe/generate/`;
export const GET_MYORDER_URL = `${HOST}/api/users/{user_pk}/orders/`;
export const CREATE_PRODUCT_URL = `${HOST}/api/creations/`;
export const POST_REVIEWS_GUEST_URL = `${HOST}/api/reviews/`;
export const POST_REVIEWS_URL = `${HOST}/api/users/{user_pk}/reviews/`;
export const GET_REVIEWS_FROM_PRODUCT_URL = `${HOST}/api/reviews/?product={id}`;
export const UPDATE_REVIEWS_URL = `${HOST}/api/reviews/{id}/`;
export const POINT_REDEEM_URL = `${HOST}/api/users/ref/{ref}/points/redeem/`;
export const VERIFICATION_EMAIL_URL = `${HOST}/api/verify/`;
export const GET_SHIPPING_URL = `${HOST}/api/shipping-rules/`;
export const GET_SHIPPING_EXPRESS_URL = `${HOST}/api/shipping-rules/?is_express=true`;
export const SUBSCRIBER_NEWLETTER_URL = `${HOST}/api/subcribers/`;
export const PRODUCTS_ALL_URL = `${HOST}/api/products/?type=kit,wax_perfume,elixir`;
export const PRODUCTS_ALL_V2_URL = `${HOST}/api/v2/products/?type=kit,wax_perfume,elixir,sample,hand_sanitizer,home_scents`;
// export const PRODUCTS_ALL_V2_URL = `${HOST}/api/v2/products/?type=kit,wax_perfume,elixir,sample`;
export const PRODUCTS_ALL_V3_URL = `${HOST}/api/products/all/`;
export const PRODUCTS_V2_URL = `${HOST}/api/v2/products/?type={type}`;
export const GET_ALL_INFLUENCER = `${HOST}/api/influencers/`;
export const GET_INFLUENCER_DETAIL_URL = `${HOST}/api/influencers/{slug}/`;
export const GET_PROMO_RULE_URL = `${HOST}/api/promo/rules/`;
export const GET_CURRENCY_LIST_URL = `${HOST}/api/currency/`;
export const GET_PACKAGING_OPTIONS_URL = `${HOST}/api/v2/products?type=packaging`;
export const GET_CURRENCY_URL = `${HOST}/api/currency/`;
export const GET_ORDER_URL = `${HOST}/api/orders/{id}/`;
export const GET_COUNTRY_URL = `${HOST}/api/countries/`;
export const CHECK_EMAIL_LOGIN_URL = `${HOST}/api/users/?email={email}`;
export const DISLIKES_OUTCOME_URL = `${HOST}/api/outcomes/{id}/disliked/`;
export const DISLIKES_PARTNERSHIP_OUTCOME_URL = `${HOST}/api/outcomes/{id}/partnerships/{slug}/disliked/`;
export const LIKES_PARTNERSHIP_OUTCOME_URL = `${HOST}/api/outcomes/{id}/partnerships/{slug}/liked/`;
export const RESULT_OUTCOME_URL = `${HOST}/api/outcomes/{id}/result/`;
export const LIKED_OUTCOME_URL = `${HOST}/api/outcomes/{id}/liked/`;
export const RESULT_OUTCOME_PARTNERSHIP_URL = `${HOST}/api/outcomes/{id}/partnerships/{slug}/result/`;
export const GET_PROMO_PERFUME_URL = `${HOST}/api/promo-rules/modify/?type=perfume`;
export const GET_YOUR_PERFUME_URL = `${HOST}/api/external-products/?q={search}&limit=100&offset={offset}`;
export const GET_MINI_WAX_PERFUME_URL = `${HOST}/api/items/?type=wax_sample&query={id,name,price,price_sale,type,image,product}`;
export const POST_COMBINATIONS_URL = `${HOST}/api/combinations/`;
export const GET_PARTNERSHIP_INFORMATION = `${HOST}/api/partnerships/{slug}/`;
export const GET_LIST_LANGUAGE = `${HOST}/api/languages/`;
export const CLEAR_OUTCOME_URL = `${HOST}/api/outcomes/{id}/clear/`;
export const CALL_UNSUBCRIBE = `${HOST}/api/subcribers/unsub/?email={email}`;
export const GET_SUBSCRIPTION_ORDER = `${HOST}/api/users/{user}/orders/?is_subscription=true`;
export const GET_SUBSCRIPTION_ITEMS = `${HOST}/api/items/subscriptions/`;
export const GET_ALL_SCENTS_PRODUCTS = `${HOST}/api/products/scents/`;
export const GET_ALL_SCENTS_PRODUCTS_PERFUME = `${HOST}/api/products/scents/?product_type=perfume`;
export const GET_ALL_SCENTS_PRODUCTS_PERFUME_DIY = `${HOST}/api/products/scents/?product_type=scent_diy`;
export const GET_ALL_SCENTS_PRODUCTS_DUAL_CRAYONS = `${HOST}/api/products/scents/?product_type=crayon`;
export const GET_ALL_SCENTS_PRODUCTS_OIL_BURNER = `${HOST}/api/products/scents/?product_type=oil_burner`;
export const GET_ALL_SCENTS_PRODUCTS_REED_DIFFUSER = `${HOST}/api/products/scents/?product_type=reed_diffuser`;
export const GET_ALL_SCENTS_PRODUCTS_WAX = `${HOST}/api/products/scents/?product_type=wax_perfume`;
export const GET_ALL_SCENTS_PRODUCTS_CANDLE = `${HOST}/api/products/scents/?product_type=single_candle`;
export const GET_ALL_SCENTS_PRODUCTS_MINI_CANDLE = `${HOST}/api/products/scents/?product_type=mini_candle`;
export const GET_ALL_SCENTS_PRODUCTS_HOME_SCENTS = `${HOST}/api/products/scents/?product_type=home_scents`;
export const GET_INGREDIENT_ID_PRODUCTS = `${HOST}/api/ingredients/{id}/`;
export const GET_PRICES_PRODUCTS = `${HOST}/api/price-levels/?product_type={type}`;
export const GET_BOTTLE_PRODUCTS = `${HOST}/api/products/?type=bottle`;
export const GET_PRODUCT_FOR_CART = `${HOST}/api/products/get/`;
export const GET_DROPDOWN_WAX_PERFUME = `${HOST}/api/variant-values/?product_type=wax_perfume`;
export const GET_PRODUCT_TYPE_LINK = `${HOST}/api/items/filter/?type={type}`;
export const GET_HISTORY_REVIEW_URL = `${HOST}/api/reviews/`;
export const POST_HISTORY_REVIEW_WORKSHOP_URL = `${HOST}/api/reviews-workshop/`;
export const POST_HISTORY_REVIEW_WORKSHOP_ID_URL = `${HOST}/api/reviews-workshop/{id}/`;
export const GET_HISTORY_REVIEW_WORKSHOP_URL = `${HOST}/api/users/{id}/reviews-workshop/`;
export const GET_TO_REVIEW_URL = `${HOST}/api/reviews/to-be-reviewed/`;
export const GET_LINE_ITEM_URL = `${HOST}/api/line-items/{id}/`;
export const GET_PRODUCT_REVIEW_URL = `${HOST}/api/products/{id}/reviews/`;
export const GET_SHARED_PROMO = `${HOST}/api/shared-promo/`;
export const GET_INQUIRIES_SUBJECTS = `${HOST}/api/inquiry-subjects/`;
export const POST_INQUIRIES = `${HOST}/api/inquiries/`;
export const GET_ALL_SCENT_NOTES = `${HOST}/api/scent-notes/`;
export const GET_BOUTIQUE_SCENT_NOTES = `${HOST}/api/outcomes/{id}/scent-notes/`;
export const GET_DISCOVER_ITEM_URL = `${HOST}/api/items/filter/?type=discovery_box`;
export const GET_MASK_SANITIZER_ITEM_URL = `${HOST}/api/items/filter/?type=mask_sanitizer`;
export const GET_MASK_TOILET_DROPPER_ITEM_URL = `${HOST}/api/items/filter/?type=toilet_dropper`;
export const GET_ALL_ARTICLES = `${HOST}/api/cms/pages/?slug=blog`;
export const GET_VIEW_COUNT = `${HOST}/api/cms/pages/view-count/`;
export const GET_ARTICLE = `${HOST}/api/cms/pages/?slug={slug}`;
export const GET_COMMENT = `${HOST}/api/cms/pages/{slug}/comments/`;
export const POST_COMMENT = `${HOST}/api/cms/pages/{slug}/comments/`;
export const GET_STRIP_CARDS = `${HOST}/api/stripe-cards/`;
export const GET_SCENT_LIB = `${HOST}/api/scent-lib/?is_all=1`;
export const GET_SCENT_DETAIL = `${HOST}/api/scent-lib/{id}/`;
export const GET_SCENT_LIB_GLOSSAIRE = `${HOST}/api/scent-lib/`;
export const GET_GIFT_FREE = `${HOST}/api/free-gifts/`;
export const GET_SCENTED_CANDLES = `${HOST}/api/items/filter/?type=curated_candle`;
export const GET_BEST_SELLER_PERFUME = `${HOST}/api/items/filter/?type=featured_perfume`;
export const GET_SCENTED_HOME = `${HOST}/api/items/filter/?type=featured_home_scents`;
export const GET_SCENTED_DUAL_CRAYONS = `${HOST}/api/items/filter/?type=featured_dual_crayons`;
export const GET_SCENTED_REED_DIFFUSER = `${HOST}/api/items/filter/?type=featured_reed_diffuser`;
export const GET_SCENTED_CAR_DIFFUSER = `${HOST}/api/items/filter/?type=featured_car_diffuser`;
export const GET_SCENTED_MINI_CANDLES = `${HOST}/api/items/filter/?type=featured_mini_candles`;
export const GET_SCENTED_OIL_BURNER = `${HOST}/api/items/filter/?type=featured_oil_burner`;
export const GET_SCENTED_PERFUME_DIY = `${HOST}/api/items/filter/?type=featured_perfume_diy`;
export const GET_GIFT_WRAP = `${HOST}/api/items/filter/?type=gift_wrap`;
export const GET_USER_ACTIONS = `${HOST}/api/rewards/user-actions/`;
export const GET_VOUCHERS = `${HOST}/api/rewards/vouchers/`;
export const GET_USER_VOUCHERS = `${HOST}/api/rewards/user-vouchers/`;
export const GET_USER_POINTS = `${HOST}/api/rewards/user-points/`;
export const GET_ALL_COUNTRY_QUIZ = `${HOST}/api/countries/all/`;
export const GET_ALL_SCENT_FAMILY = `${HOST}/api/tags/?group=scent_family`;
export const GET_ALL_SCENT_FAMILY_V4 = `${HOST}/api/tags/?group=scent_family&quiz=true`;
export const GET_ALL_SCENT_FAMILY_FERRARI = `${HOST}/api/tags/?quiz=true&partnership=ferrari`;
export const GET_ALL_SCENT_FAMILY_V4_PARTNER = `${HOST}/api/tags/?quiz=true&partnership={partner}`;
export const CREATE_APPOINTMENTS = `${HOST}/api/appointments/`;
export const API_INSTAGRAM = `${HOST}/api/proxies/instagram/`;
export const GET_STORE_DISCOUNT = `${HOST}/api/promo-rules/store-discount/`;
export const GET_FAQ_QUESTION = `${HOST}/api/product-types/{type}/`;
export const POST_CHECK_OUT_EXPRESS = `${HOST}/api/carts/{id}/checkout/`;
export const GET_REVIEW_WORK_SHOPS = `${HOST}/api/workshops/{id}/reviews/`;
export const GET_RELATED_CART = `${HOST}/api/items/{id}/related-items/`;
export const GET_FILTER_OPTIONS = `${HOST}/api/items/filter-options/`;
export const GET_PRODUCT_TYPES = `${HOST}/api/product-types/`;
export const GET_ALL_PRODUCTS_FILTER = `${HOST}/api/items/multi-filter/`;
export const SEND_NOTIFI_STOCK = `${HOST}/api/items/{id}/notify-stock/`;
export const GET_VARIANT_VALUE = `${HOST}/api/v1.1/variant-values/?product_type={product_type}`;

export const GET_SIMPLYBOOK_TOKEN_ADMIN = `${HOST}/api/proxies/simplybook/token/`;
export const GET_EXCHANGE_RATE = `${HOST}/api/exchange-rates/`;
