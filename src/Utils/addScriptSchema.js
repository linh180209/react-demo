import _ from 'lodash';
import auth from '../Redux/Helpers/auth';

const ID_SCHEMA_FAQ = 'schema-faq';
const ID_SCHEMA_PRODUCT_DETAIL = 'schema-product-detail';

const removeScript = (id) => {
  const ele = document.getElementById(id);
  if (ele) {
    ele.parentNode.removeChild(ele);
  }
};

export const removeScriptProductDetail = () => {
  removeScript(ID_SCHEMA_PRODUCT_DETAIL);
};

export const removeScriptFAQ = () => {
  removeScript(ID_SCHEMA_FAQ);
};

export const generateScriptFAQ = (faqs, blockFAQ = false) => {
  // remove all script faqs
  removeScriptFAQ();

  if (!faqs) {
    return;
  }

  const s = document.createElement('script');
  s.type = 'application/ld+json';
  s.async = true;
  s.id = ID_SCHEMA_FAQ;
  const mEntity = [];

  if (blockFAQ) {
    faqs.forEach((element) => {
      // const headerText = element.value.header.header_text;
      const bodyFAQ = element?.value?.faq;
      _.map(bodyFAQ, (item) => {
        mEntity.push({
          '@type': 'Question',
          name: `${item.value.header.header_text}`,
          acceptedAnswer: {
            '@type': 'Answer',
            text: `${item.value.paragraph}`,
          },
        });
      });
    });
  } else {
    _.map(faqs, (item) => {
      mEntity.push({
        '@type': 'Question',
        name: `${item.title}`,
        acceptedAnswer: {
          '@type': 'Answer',
          text: `${item.description}`,
        },
      });
    });
  }
  s.innerHTML = `
      {
      "@context": "http://schema.org",
      "@type": "FAQPage",
      "url": "${window.location.href}",
      "name": "Maison 21G",
      "description": "Create Customised Perfume and Fragrances",
      "mainEntity": ${JSON.stringify(mEntity)}
      }
    `;

  document.body.appendChild(s);
};

export const generateScriptProductDetail = (data) => {
  // remove all script product detail
  removeScriptProductDetail();

  const ele = document.createElement('script');
  ele.type = 'application/ld+json';
  ele.async = true;
  ele.id = ID_SCHEMA_PRODUCT_DETAIL;
  ele.innerHTML = `
      {
      "@context": "http://schema.org",
      "@type": "Product",
      "name": ${data?.name},
      "image": ${data?.items[0]?.image_display},
      "productID": ${data?.id},
      "description": ${data?.description},
      "category": ${data?.type?.name},
      "url": ${window.location.href},
      "price": ${data?.items[0]?.price},
      "priceCurrency": ${auth.getCurrency()},
      "brand": "Maison 21G",
      }
    `;
  document.body.appendChild(ele);
};
