const ID_SCRIPT_GOOLE_API = 'id-google-script';

export const isScriptExist = () => {
  const ele = document.getElementById(ID_SCRIPT_GOOLE_API);
  return !!ele;
};

export const addScriptGoogleAPI = () => {
  if (!isScriptExist()) {
    const script = document.createElement('script');
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyCxzlWb-vmVoiaAlif1ih5MfMeJhxXWoYM&libraries=places&language=en';
    script.async = true;
    script.defer = true;
    script.type = 'text/javascript';
    script.id = ID_SCRIPT_GOOLE_API;
    document.body.appendChild(script);
  }
};
