/* eslint-disable import/prefer-default-export */
export const replaceCountryInMetaTags = (currentCountry, defaultText = '') => {
  let replaceText = defaultText;
  const replaceCountry = '{country}';

  if (defaultText && currentCountry) {
    replaceText = defaultText.replaceAll(replaceCountry, currentCountry);
  }

  return replaceText;
};
