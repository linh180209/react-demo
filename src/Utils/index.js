import {
  getHeight, isBrowser, isiOS, isMobile767fn,
} from '../DetectScreen';

export const updateCSSMobile = (className) => {
  if (isMobile767fn()) {
    const elements = document.querySelectorAll(`.${className}`);
    console.log('elements', elements);
    for (let i = 0; i < elements.length; i += 1) {
      elements[i].style.height = `${getHeight()}px`;
    }
  }
};

export const disableScrollBody = () => {
  const name = 'disable-sroll';
  const element = document.getElementById('body');
  const arr = element.className.split(' ');
  if (arr.indexOf(name) === -1) {
    element.className += ` ${name}`;
    if (!isBrowser && isiOS) {
      document.body.style.top = `-${window.scrollY}px`;
    }
  }
};

export const enableScrollBody = () => {
  const name = 'disable-sroll';
  const element = document.getElementById('body');
  const arr = element.className.split(' ');
  if (arr.indexOf(name) !== -1) {
    element.classList.remove('disable-sroll');
    if (!isBrowser && isiOS) {
      const scrollY = document.body.style.top;
      document.body.style.position = '';
      document.body.style.top = '';
      window.scrollTo(0, parseInt(scrollY || '0', 10) * -1);
    }
  }
};
export const test = () => {};
