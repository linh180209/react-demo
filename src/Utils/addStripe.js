const ID_SCRIPT_STRIPE = 'id-stripe';

export const isScriptExist = () => {
  const ele = document.getElementById(ID_SCRIPT_STRIPE);
  return !!ele;
};

export const addScriptStripe = () => {
  if (!isScriptExist()) {
    const script = document.createElement('script');
    script.src = 'https://js.stripe.com/v3';
    script.async = true;
    script.id = ID_SCRIPT_STRIPE;
    document.body.appendChild(script);
  }
};
