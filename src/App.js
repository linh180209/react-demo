import React, { Component } from 'react';
import * as Sentry from '@sentry/react';
import Main from './views/Main';
import './styles/global.scss';
import './styles/globalv2.scss';

import { initializeTracking } from './Redux/Helpers';

// Import Font Awesome Icons Set
// import '../node_modules/font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import '../node_modules/simple-line-icons/css/simple-line-icons.css';
// Import processbar circular
import '../node_modules/react-circular-progressbar/dist/styles.css';
import '../node_modules/video-react/dist/video-react.css';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { IS_DEV, SENDY_STAGING, SENDY_PROD } from './config';

class App extends Component {
  componentDidMount() {
    setTimeout(() => {
      if (!IS_DEV) {
        initializeTracking();
      }
      Sentry.init({
        dsn: IS_DEV ? SENDY_STAGING : SENDY_PROD,
      });
    }, 7 * 1000);
  }

  // addFacebook = () => {
  //   const s = document.createElement('script');
  //   s.type = 'text/javascript';
  //   s.async = true;
  //   s.innerHTML = `
  //     !function(f,b,e,v,n,t,s)
  //     {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  //     n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  //     if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  //     n.queue=[];t=b.createElement(e);t.async=!0;
  //     t.src=v;s=b.getElementsByTagName(e)[0];
  //     s.parentNode.insertBefore(t,s)}(window, document,'script',
  //     'https://connect.facebook.net/en_US/fbevents.js');
  //     fbq('init', '2376335132695671');
  //     fbq('track', 'PageView');
  //   `;
  //   const s1 = document.createElement('noscript');
  //   s1.type = 'text/javascript';
  //   s1.async = true;
  //   s1.innerHTML = `
  //     <img height="1" width="1" style="display:none"
  //     src="https://www.facebook.com/tr?id=2376335132695671&ev=PageView&noscript=1"
  //     />
  //   `;
  //   document.body.appendChild(s);
  //   document.body.appendChild(s1);
  // }

  render() {
    return (
      <div className="container--body">
        <Main />
      </div>
    );
  }
}

export default App;
