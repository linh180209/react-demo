"scripts": {
    "start": "webpack-dev-server --mode development --config webpack.dev.js --inline --history-api-fallback",
    "build": "webpack --mode production -p --progress --config webpack.prod.js --display-used-exports"
 }

-----------------

Node version: v8.9.0
NPM version: 5.5.1

// Condition mobile Landscape when having input

(isMobile && !isIOS) || (isMobile && !isLandscape)

