# Structure Front End

![structure](structure.png)

## 1. Folder "doc"
Include all Front End document

## 2. Folder "src"
Include all source codes

### 2.1 Folder "components"
Include all components (input, button, ...) for old design

### 2.2 Folder "componentsv2"
Include all components (input, button, ...) for new design

### 2.3 Folder "config"
Configuration environment and define all URLs to get API

### 2.4 Folder "constants"
Define constants

### 2.5 Folder "DetectScreen"
Include function detect platform Mobile, Tablet, Browser, iOS, ...

### 2.6 Folder "image"
Include all image for old design

### 2.7 Folder "imagev2"
Include all image for new design

### 2.8 Folder "includes"
Include all fonts, favicon

### 2.9 Folder "Redux"
Include all source code handle redux, redux-saga

### 2.10 Folder "styles"
Include all file CSS

### 2.11 Folder "Utils"
Include all custom function react hook

### 2.12 Folder "views"
Include all the source code all page of old design

### 2.12 Folder "views2"
Include all the source code all page of new design



