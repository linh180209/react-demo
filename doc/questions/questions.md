# Questionnaire
## Diagram
![Diagram](questionnaire.png)

## 1. Call API to get all the questions and answers for the personality
  ### API get the questions and answers
  - Endpoint: /api/answers/, /api/questions/
  - Method: "GET"
  - Code example
  ```
    export const fetchAnswers = () => {
      try {
        const options = {
          url: GET_ALL_ANSWERS,
          method: 'GET',
        };

        return fetchClient(options);
      } catch (error) {
        return null;
      }
    };

    export const fetchQuestions = () => {
      try {
        const options = {
          url: GET_ALL_QUESTIONS,
          method: 'GET',
        };

        return fetchClient(options);
      } catch (error) {
        return null;
      }
    };
  ```
## 2. Prepare and display 3 question Gender, Age, Skin
  This is the hardcode. we only need to create data for 3 the question
  - Code example
  ```
    const questionFree = [
    {
      type: 'qtGender',
      id: 'qt1',
      no: 1,
      title: 'What is your gender ?',
      name: 'gender',
      answer1: 'Male',
      type1: 'M',
      answer2: 'Female',
      type2: 'W',
      answer3: 'Gender-free',
      type3: 'G',
    },
    {
      type: 'qtAge',
      id: 'qt2',
      no: 2,
      title: 'What is your age ?',
      name: 'age',
      addMarginBottom: false,
    },
    {
      type: 'qtSkin',
      id: 'qt3',
      no: 3,
      title: 'What is your skin-tone ?',
      name: 'skin-tone',
    },
  ];
```
## 3. Generate and display the personality question
We have 5 the personality question. The data next question depend on the result before question.
- Code example generate the question personality(noTotal: the question index currently)
```
  getQuestion = (noTotal) => {
    const { answers } = this.props;
    const no = noTotal - questionFree.length;
    let parent;
    if (no === 0) {
      parent = answers;
    } else {
      let temp = answers;
      for (let i = 0; i < no; i += 1) {
        temp = _.find(temp, x => x.title === this.currentResult[i]).children;
      }
      parent = temp;
    }
    if (parent) {
      this.currentItem = parent;
      const object = {
        idAnswer1: parent[0].id,
        idAnswer2: parent[1].id,
        type: 'qtYesNo',
        id: `qt${noTotal + 1}`,
        no: noTotal + 1,
        title: parent[0].question,
        name: `persional${no + 1}`,
        answer1: parent[0].title,
        persionalId1: parent[0].personality ? parent[0].personality.id : '',
        type1: parent[0].title.charAt(0).toUpperCase(),
        answer2: parent[1].title,
        type2: parent[1].title.charAt(0).toUpperCase(),
        persionalId2: parent[1].personality ? parent[1].personality.id : '',
      };
      return object;
    }
    return null;
  }
```

## 4. Create or Update the Outcome
When the user answer anythe question (Gender, Age, Skin, the persionality question), the app shall update the outcome by call API
- Endpoint: 
- Method: PUT(update) or POST(create)
- Code example:
```
  makeBodyPost = (datas, isAddNext = false) => {
    const answers = [];
    const age = _.find(datas, x => x.name === 'age') ? _.find(datas, x => x.name === 'age').value : 30;
    const skinTone = _.find(datas, x => x.name === 'skin-tone');
    const gender = _.find(datas, x => x.name === 'gender');

    for (let i = 3; i < datas.length; i += 1) {
      if (datas[i].idAnswer) {
        answers.push(datas[i].idAnswer);
      }
    }
    this.sendRealTimeData({
      age,
      skin_tone: skinTone ? skinTone.value : undefined,
      gender: gender ? (gender.value.toLowerCase() === 'male' ? 1 : gender.value.toLowerCase() === 'female' ? 2 : 3) : undefined,
      answers: answers.length > 0 ? answers : undefined,
      external_product: this.idYourPerfume,
      is_boutique: this.namePath === 'boutique-quiz',
      partnership: this.slug,
    }, isAddNext);
  }

  sendRealTimeData = (body, isAddNext) => {
    if (this.idOutCome) {
      const options = {
        url: PUT_OUTCOME_URL.replace('{id}', this.idOutCome),
        method: 'PUT',
        body,
      };
      fetchClient(options, true).then((result) => {
        if (isAddNext && result && !result.isError) {
          const { personality_choice: persionaltilyData } = result;
          this.setState({ persionaltilyData, stepQuestion: 10, isClickPrev: false });
        }
      });
    } else {
      fetchClient({
        url: POST_OUTCOMES_URL,
        method: 'POST',
        body,
      }, true).then((result) => {
        this.idOutCome = result.id;
        if (isAddNext) {
          this.setState({ stepQuestion: 10, isClickPrev: false });
        }
      });
    }
  }
```

**NOTE**
If the index question is 8 ( the index next question is 9). We should get personality_choice from the result of API Update Outcome for product of Like page
- Code example:
```
  const options = {
    url: PUT_OUTCOME_URL.replace('{id}', this.idOutCome),
    method: 'PUT',
    body,
  };
  fetchClient(options, true).then((result) => {
    if (index === 9 && result && !result.isError) {
      const { personality_choice: persionaltilyData } = result;
      this.setState({ persionaltilyData, stepQuestion: 10, isClickPrev: false });
    }
  });
```

## 5.Call Like API and DisLike API
- Call Like API the first

  - endpoint: /api/outcomes/{id}/partnerships/{slug}/liked/ or /api/outcomes/{id}/liked/
  - method: GET
  - code example:
  ```
    fetchData = (slug) => {
      const { idOutCome } = this.props;
      const option = {
        url: LIKES_PARTNERSHIP_OUTCOME_URL.replace('{id}', idOutCome).replace('{slug}', slug),
        method: 'GET',
      };
      fetchClient(option).then((result) => {
        if (result && !result.isError) {
          this.setState({ dataProduct: result });
        }
        throw new Error();
      }).catch((err) => {
        console.error('error', err);
      });
    }
  ```
- Then Call Dislike API
  - endpoint: /api/outcomes/{id}/disliked/ or /api/outcomes/{id}/partnerships/{slug}/disliked/
  - method: 'GET'
  - code example:
  ```
  fetchData = () => {
    const {
      selectLikes, idOutCome, history, slugPartnerShip,
    } = this.props;
    if (!idOutCome) {
      history.push('/');
    }
    const URL = slugPartnerShip ? DISLIKES_PARTNERSHIP_OUTCOME_URL.replace('{id}', idOutCome).replace('{slug}', slugPartnerShip) : DISLIKES_OUTCOME_URL.replace('{id}', idOutCome);
    const option = {
      url: selectLikes.length === 0 ? URL : `${URL}?liked=${selectLikes[0].id}${selectLikes.length > 1 ? `,${selectLikes[1].id}` : ''}`,
      method: 'GET',
    };
    fetchClient(option).then((result) => {
      if (result && !result.isError) {
        this.setState({ data: result });
      }
      throw new Error();
    }).catch((err) => {
      console.error('error', err);
    });
  }
  ```

## 6. Show outcome
Call API
- endpoint: /api/outcomes/{id}/
- method: GET
- code example
```
  const option = {
    url: PUT_OUTCOME_URL.replace('{id}', this.outComeId),
    method: 'GET',
  };
  fetchClient(option).then((result) => {
    console.log('result', result);
    if (result && !result.isError) {
      const { product_day: prodDay } = result;
      if (_.isEmpty(prodDay)) {
        this.props.history.push('/');
        return;
      }
      this.setState({ data: result });
      return;
    }
    if (result && result.status === 404) {
      auth.setOutComeId(undefined);
      if (login && login.user && login.user.outcome) {
        login.user.outcome = undefined;
      }
    }
    throw new Error();
  }).catch(() => {
    this.props.history.push('/');
  });
```




    
    