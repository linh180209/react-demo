# Branch Gitlab

![branche](branch.png)

We have 3 main branch "frontEnd", "master", "prod"

- frontEnd branch: develop branch
- master branch: staging branch
- prod branch: production branch

## Rule develop
- Creating new branch from "frontEnd" branch to implement new feature
- Merge new branch into "frontEnd" branch when completed feature
- Deploy on staging: merge "frontEnd" branch into "master" branch
- Deploy on production: merge "frontEnd" branch into "prod" branch

## Run localhost
- Go to "{path}/scentdesigner/frontend/"
- Run command: npm install
- Run command: npm start

URL localhost: http://localhost:3003/