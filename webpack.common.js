const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const FilterWarningsPlugin = require('webpack-filter-warnings-plugin');
const webpack = require('webpack');

// const PreloadWebpackPlugin = require('preload-webpack-plugin');
// const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: `${__dirname}/src/index`,
  output: {
    path: `${__dirname}/public/`,
    publicPath: '/',
    chunkFilename: '[name].[chunkhash:4].js',
    filename: '[name].[chunkhash:4].js',
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name(module) {
            //* : Get the name. E.g. node_modules/packageName/not/this/part.js
            //* : or node_modules/packageName
            const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];

            //* : NPM package names are URL-safe, but some servers don't like @ symbols
            return `npm.${packageName.replace('@', '')}`;
          },
        },
      },
    },
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              plugins: ['lodash'],
              presets: ['@babel/env'],
            },
          },
        ],
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true },
          },
        ],
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader'],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'style-loader', //* : Creates style nodes from JS strings
          },
          {
            loader: 'css-loader', //* : Translates CSS into CommonJS
          },
          {
            loader: 'sass-loader', //* : Compiles Sass to CSS
          },
        ],
      },
      {
        test: /\.(png|woff|woff2|otf|eot|ttf|svg|jpg|jpeg|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 10000,
          },
        }],
      },
      {
        test: /\.(mp3|pdf)$/,
        use: [{
          loader: 'file-loader',
        }],
      },
    ],
  },
  plugins: [
    new FilterWarningsPlugin({
      exclude: /mini-css-extract-plugin/,
    }),
    new HtmlWebPackPlugin({
      template: './src/index.html',
      filename: './index.html',
      inject: true,
    }),
    new MiniCssExtractPlugin({
      publicPath: '/',
      chunkFilename: '[name].[chunkhash:4].css',
      filename: '[name].[chunkhash:4].css',
      // chunkFilename: '[name].css',
      // filename: '[name].css',
    }),
    new CopyWebpackPlugin([
      { from: './src/includes/', to: '' },
    ]),
    new webpack.IgnorePlugin({
      checkResource(resource, context) {
        const isLocaleFromMomentImport = /^\.\/locale$/.test(resource) && /moment$/.test(context);
        const isReactTooltipIndexFile = /^\.\/index\.es\.js$/.test(resource) && /react-tooltip\/dist$/.test(context);

        return (
          isLocaleFromMomentImport
             || isReactTooltipIndexFile
        );
      },
    }),
    new webpack.DefinePlugin({
      'process.env': JSON.stringify(process.env),
    }),
    // new PreloadWebpackPlugin({
    //   rel: 'preload',
    //   include: 'allChunks',
    // }),
  ],
};
